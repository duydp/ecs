﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.Components;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.QuanLyChungTu;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
using GlobalsShare = Company.KDT.SHARE.Components.Globals;
using Company.KDT.SHARE.Components.Messages;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.GoodsItem;
namespace Company.KD.BLL.DataTransferObjectMapper
{

    /// <summary>
    /// Ánh xạ từ BOs(Bussines object)  sang DTOs (Data Transfer Objects)    
    /// </summary>
    public class Mapper_V4
    {
        static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        static string sfmtDate = "yyyy-MM-dd";
        static string sfmtTime = "HH:MM";

        /// <summary>
        /// Chuyển dữ liệu từ ToKhaiMauDich BO sang KD_ToKhaiMauDich DTO
        /// </summary>
        /// <param name="tokhaimaudich">ToKhaiMauDich</param>
        /// <returns>ToKhai</returns>
        /// 
        #region TransferOb tờ KD
        public static ToKhai ToDataTransferObject(ToKhaiMauDich tkmd)
        {

            bool isToKhaiNhap = tkmd.MaLoaiHinh.Substring(0, 1).Equals("N");
            bool isToKhaiSua = tkmd.SoToKhai != 0 && tkmd.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;
            if (isToKhaiSua)
                tkmd.SoTiepNhan = KhaiBaoSua.SoTNSua(tkmd.SoTiepNhan, tkmd.SoToKhai, tkmd.NgayDangKy.Year, tkmd.MaLoaiHinh,
                        tkmd.MaHaiQuan, tkmd.MaDoanhNghiep, LoaiKhaiBao.ToKhai);

            #region Header
            ToKhai tokhai = new ToKhai()
            {

                Issuer = isToKhaiNhap ? DeclarationIssuer.KD_TOKHAI_NHAP : DeclarationIssuer.KD_TOKHAI_XUAT,
                Reference = tkmd.GUIDSTR,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = isToKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                // Số tiếp nhận
                CustomsReference = isToKhaiSua ? Helpers.FormatNumeric(tkmd.SoTiepNhan) : string.Empty,
                //Ngày dang ký chứng thư
                Acceptance = isToKhaiSua ? tkmd.NgayDangKy.ToString(sfmtDateTime) : string.Empty,
                // Ðon vị hải quan khai báo
                DeclarationOffice = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(tkmd.MaHaiQuan.Trim()),
                // Số hàng
                GoodsItem = Helpers.FormatNumeric(tkmd.SoHang),
                LoadingList = Helpers.FormatNumeric(tkmd.SoLuongPLTK),

                // Khối luợng và khối luợng tịnh
                TotalGrossMass = Helpers.FormatNumeric(tkmd.TrongLuong, 4),
                TotalNetGrossMass = Helpers.FormatNumeric(tkmd.TrongLuongNet, 4),
                // Mã Loại Hình
                NatureOfTransaction = tkmd.MaLoaiHinh,
                // Phuong thức thanh toán
                PaymentMethod = tkmd.PTTT_ID,



                #region Install element
                Agents = new List<Agent>(),

                //Nguyên tệ
                CurrencyExchange = new CurrencyExchange { CurrencyType = tkmd.NguyenTe_ID, Rate = Helpers.FormatNumeric(tkmd.TyGiaTinhThue, 4) },

                //Số kiện
                DeclarationPackaging = new Packaging { Quantity = Helpers.FormatNumeric(tkmd.SoKien) },

                //Neu có day du 1 trong 3 chứng từ :  Giấy phép, Hợp đồng, Vận đơn 
                //AdditionalDocuments = new List<AdditionalDocument>(),

                // Hóa Ðon thuong mại
                Invoice = new Invoice { Issue = tkmd.NgayHoaDonThuongMai.Year <= 1900 ? string.Empty : tkmd.NgayHoaDonThuongMai.ToString(sfmtDate), Reference = tkmd.SoHoaDonThuongMai, Type = AdditionalDocumentType.HOA_DON_THUONG_MAI },

                // Doanh Nghiệp Xuất khẩu
                Exporter = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty } :
                new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep },

                //Doanh nghiệp nhập khẩu
                Importer = isToKhaiNhap ? new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep.Trim() } :
                new NameBase { Name = tkmd.TenDonViDoiTac, Identity = tkmd.MaDoanhNghiep },

                // Nguười gởi (Tên chủ hàng)
                RepresentativePerson = new RepresentativePerson { ContactFunction = tkmd.ChucVu, Name = tkmd.TenChuHang },

                // Đề xuất khác | Mã loại thông tin - Ghi chú khác
                AdditionalInformations = new List<AdditionalInformation>(),

                // GoodsShipmet Thông tin hàng hóa
                GoodsShipment = new GoodsShipment(),
                /*
                License = new List<License>(),
                ContractDocument = new List<ContractDocument>(),
                CommercialInvoices = new List<CommercialInvoice>(),
                CertificateOfOrigins = new List<CertificateOfOrigin>(),
                CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>(),
                AttachDocumentItem = new List<AttachDocumentItem>(),
                AdditionalDocumentEx = new List<AdditionalDocument>(),*/

                #region Ân hạng thuế, Đảm bảo nghĩa vụ nộp thuế, bảo lãnh thuế.
                TaxGrace = tkmd.AnHanThue.IsAnHan ? new Company.KDT.SHARE.Components.Messages.TaxGrace()
                {
                    IsGrace = "1",
                    Reason = tkmd.AnHanThue.LyDoAnHan,
                    Value = Helpers.FormatNumeric(tkmd.AnHanThue.SoNgay, 0)
                } : null,

                TaxGuarantee = tkmd.DamBaoNghiaVuNopThue.IsDamBao ? new Company.KDT.SHARE.Components.Messages.TaxGuarantee()
                {
                    IsGuarantee = "1",
                    Type = tkmd.DamBaoNghiaVuNopThue.HinhThuc,
                    Value = Helpers.FormatNumeric(tkmd.DamBaoNghiaVuNopThue.TriGiaDB, Globals.TriGiaNT),
                    Issue = tkmd.DamBaoNghiaVuNopThue.NgayBatDau.ToString(sfmtDate),
                    Expire = tkmd.DamBaoNghiaVuNopThue.NgayKetThuc.ToString(sfmtDate)
                } : null,

                TaxGuaranteeAgent = tkmd.BaoLanhThue != null ? new TaxGuaranteeAgent()
                {
                    IdentityGuarantor = tkmd.BaoLanhThue.DonViBaoLanh,
                    Reference = tkmd.BaoLanhThue.SoGiayBaoLanh,
                    Year = tkmd.BaoLanhThue.NamChungTuBaoLanh.ToString("N0"),
                    Issue = tkmd.BaoLanhThue.NgayHieuLuc.ToString(sfmtDate),
                    Type = tkmd.BaoLanhThue.LoaiBaoLanh,
                    Value = Helpers.FormatNumeric(tkmd.BaoLanhThue.SoTienBaoLanh, 4),
                    RemainValue = Helpers.FormatNumeric(tkmd.BaoLanhThue.SoDuTienBaoLanh, 4),
                    DayValue = tkmd.BaoLanhThue.SoNgayDuocBaoLanh.ToString("N0"),
                    Expire = tkmd.BaoLanhThue.NgayHetHieuLuc.ToString(sfmtDate),
                    SignDate = tkmd.BaoLanhThue.NgayKyBaoLanh.ToString(sfmtDate)
                } : null,


                #endregion
                #region  Số container của tờ khai
                TransportEquipment = new QuantityContainer
                {
                    Quantity20 = "0",
                    Quantity40 = "0",
                    Quantity45 = "0",
                    QuantityOthers = "0",

                },
                //<property>1</property>
                //<grossMass>3</grossMass>
                //<netMass>4</netMass>
                //<quantity>5</quantity>
                //<packagingLocation>6</packagingLocation>

                #endregion

                //TemporaryImportExpire = new TemporaryImportExpire() {Statement = "001",Content = " " },

                #endregion Nrr

            };
            if (Company.KDT.SHARE.Components.Globals.IsKTX)
            {
                if (tkmd.MaLoaiHinh.Contains("V"))
                    tokhai.NatureOfTransaction = tkmd.MaLoaiHinh.Substring(2, 3);
                tokhai.CustomsReferenceManual = tkmd.ChiTietDonViDoiTac;
                tokhai.Clearance = tkmd.NgayDangKy.ToString(sfmtDate);
                tokhai.Channel = tkmd.PhanLuong;
                tokhai.Acceptance = tkmd.NgayDangKy.ToString(sfmtDateTime);
                if (tkmd.TenDonViDoiTac.Contains("/"))
                {
                    if (isToKhaiNhap)
                    {
                        tokhai.Exporter.Identity = tkmd.TenDonViDoiTac.Split('/')[0].ToString();
                        tokhai.Exporter.Name = tkmd.TenDonViDoiTac.Split('/')[1].ToString();
                        tokhai.Importer.Identity = tkmd.MaDoanhNghiep.ToString();
                        tokhai.Importer.Name = tkmd.TenDoanhNghiep.ToString();
                        //tokhai.Importer.Identity = tkmd.TenDonViDoiTac.Split('/')[0].ToString();
                        //tokhai.Importer.Name = tkmd.TenDonViDoiTac.Split('/')[1].ToString();
                        //tokhai.Exporter.Identity = tkmd.MaDoanhNghiep.ToString();
                        //tokhai.Exporter.Name = tkmd.TenDoanhNghiep.ToString();
                    }
                    else
                    {
                        tokhai.Importer.Identity = tkmd.TenDonViDoiTac.Split('/')[0].ToString();
                        tokhai.Importer.Name = tkmd.TenDonViDoiTac.Split('/')[1].ToString();
                        tokhai.Exporter.Identity = tkmd.MaDoanhNghiep.ToString();
                        tokhai.Exporter.Name = tkmd.TenDoanhNghiep.ToString();
                        //tokhai.Exporter.Identity = tkmd.TenDonViDoiTac.Split('/')[0].ToString();
                        //tokhai.Exporter.Name = tkmd.TenDonViDoiTac.Split('/')[1].ToString();
                        //tokhai.Importer.Identity = tkmd.MaDoanhNghiep.ToString();
                        //tokhai.Importer.Name = tkmd.TenDoanhNghiep.ToString();
                    }
                }
                else
                {
                    if (isToKhaiNhap)
                    {
                        tokhai.Importer.Identity = ".";
                        tokhai.Importer.Name = tkmd.TenDonViDoiTac.ToString();
                        tokhai.Exporter.Identity = tkmd.MaDoanhNghiep.ToString();
                        tokhai.Exporter.Name = tkmd.TenDoanhNghiep.ToString();
                    }
                    else
                    {
                        tokhai.Exporter.Identity = "."; ;
                        tokhai.Exporter.Name = tkmd.TenDonViDoiTac.ToString();
                        tokhai.Importer.Identity = tkmd.MaDoanhNghiep.ToString();
                        tokhai.Importer.Name = tkmd.TenDoanhNghiep.ToString();
                    }
                }
            }
            if (tkmd.MaLoaiHinh.Contains("V"))
                tokhai.NatureOfTransaction = tkmd.MaLoaiHinh.Substring(2, 3);
            #region fill  Số container của tờ khai (nếu có)
            //if (tkmd.VanTaiDon != null && tkmd.VanTaiDon.ContainerCollection != null && tkmd.VanTaiDon.ContainerCollection.Count > 0)
            //{
            //    int cont20 = 0;
            //    int cont40 = 0;
            //    int cont45 = 0;
            //    int contOther = 0;
            //    foreach (Container cont in tkmd.VanTaiDon.ContainerCollection)
            //    {
            //        switch (cont.LoaiContainer)
            //        {
            //            case "2" :
            //                cont20++;
            //                break;
            //            case "4":
            //                cont40++;
            //                break;
            //            case "45":
            //                cont45++;
            //                break;
            //            case "0":
            //                contOther++;
            //                break;
            //            default:
            //                break;
            //        }
            //    }
            if (tkmd.SoLuongContainer != null)
            {
                tokhai.TransportEquipment.Quantity20 = Helpers.FormatNumeric(tkmd.SoLuongContainer.Cont20, 0);
                tokhai.TransportEquipment.Quantity40 = Helpers.FormatNumeric(tkmd.SoLuongContainer.Cont40, 0);
                tokhai.TransportEquipment.Quantity45 = Helpers.FormatNumeric(tkmd.SoLuongContainer.Cont45, 0);
                tokhai.TransportEquipment.QuantityOthers = Helpers.FormatNumeric(tkmd.SoLuongContainer.ContKhac, 0);
            }
            //}



            #endregion


            tokhai.AdditionalInformations.Add(
                new AdditionalInformation
                {
                    Statement = "001",
                    Content = new Content() { Text = tkmd.DeXuatKhac }
                }
                );
            if (isToKhaiSua)
                tokhai.AdditionalInformations.Add(new AdditionalInformation()
                {
                    Statement = "005",
                    Content = new Content() { Text = tkmd.LyDoSua }
                });


            #endregion Header

            #region Agents Đại lý khai
            tokhai.Agents = AgentsFrom(tkmd);
            #endregion

            #region AdditionalDocument Hợp đồng - Giấy phép - Vận đơn ngoài tờ khai
            bool isDocument = !string.IsNullOrEmpty(tkmd.SoHopDong) ||
                !string.IsNullOrEmpty(tkmd.SoVanDon) || !string.IsNullOrEmpty(tkmd.SoGiayPhep);
            if (isDocument) tokhai.AdditionalDocuments = new List<Company.KDT.SHARE.Components.AdditionalDocument>();
            // Add AdditionalDocument (thêm hợp đồng)
            if (!string.IsNullOrEmpty(tkmd.SoHopDong))
                tokhai.AdditionalDocuments.Add(new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Issue = tkmd.NgayHopDong.Year > 1900 ? tkmd.NgayHopDong.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoHopDong,
                    Type = AdditionalDocumentType.HOP_DONG,
                    Name = "Hop dong",
                    Expire = tkmd.NgayHetHanHopDong.Year > 1900 ? tkmd.NgayHetHanHopDong.ToString(sfmtDate) : string.Empty
                });
            // Thêm vận đơn
            if (isToKhaiNhap && !string.IsNullOrEmpty(tkmd.SoVanDon))
                tokhai.AdditionalDocuments.Add(new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Issue = tkmd.NgayVanDon.Year > 1900 ? tkmd.NgayVanDon.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoVanDon,
                    Type = AdditionalDocumentType.BILL_OF_LADING_ORIGIN,
                    Name = "Bill of lading"
                });
            // Thêm giấy phép
            if (!string.IsNullOrEmpty(tkmd.SoGiayPhep))
                tokhai.AdditionalDocuments.Add(new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Issue = tkmd.NgayGiayPhep.Year > 1900 ? tkmd.NgayGiayPhep.ToString(sfmtDate) : string.Empty,
                    Reference = tkmd.SoGiayPhep,
                    Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
                    Name = "Giay phep",
                    Expire = tkmd.NgayHetHanGiayPhep.Year > 1900 ? tkmd.NgayHetHanGiayPhep.ToString(sfmtDate) : string.Empty
                });

            #endregion

            #region GoodsShipment thông tin về hàng hóa
            tokhai.GoodsShipment = new GoodsShipment()
            {
                ImportationCountry = isToKhaiNhap ? null : tkmd.NuocNK_ID.Substring(0, 2),
                ExportationCountry = isToKhaiNhap ? tkmd.NuocXK_ID.Substring(0, 2) : null,
                Consignor = new NameBase { Identity = string.Empty, Name = string.Empty },
                Consignee = new NameBase { Identity = string.Empty, Name = string.Empty },
                NotifyParty = new NameBase { Identity = string.Empty, Name = string.Empty },
                DeliveryDestination = new DeliveryDestination { Line = string.Empty },
                EntryCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? tkmd.CuaKhau_ID : string.Empty, Name = isToKhaiNhap ? Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(tkmd.CuaKhau_ID.Trim()) : string.Empty },
                ExitCustomsOffice = new LocationNameBase { Code = isToKhaiNhap ? string.Empty : tkmd.CuaKhau_ID, Name = isToKhaiNhap ? string.Empty : Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(tkmd.CuaKhau_ID.Trim()) },
                Importer = isToKhaiNhap ? null : new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty },
                Exporter = isToKhaiNhap ? new NameBase { Name = tkmd.TenDonViDoiTac, Identity = string.Empty } : null,
                TradeTerm = new TradeTerm { Condition = tkmd.DKGH_ID },
                //CustomsGoodsItems = new List<CustomsGoodsItem>()
            };
            #endregion GoodsShipment

            #region CustomGoodsItem Danh sách hàng khai báo

            //Add CustomGoodsItem
            int soluonghang = 0;
            if (tkmd.HMDCollection != null)
            {
                soluonghang = tkmd.HMDCollection.Count;
                tokhai.GoodsShipment.CustomsGoodsItems = new List<CustomsGoodsItem>();
            }
            for (int i = 0; i < soluonghang; i++)
            {
                HangMauDich hmd = tkmd.HMDCollection[i];
                #region CustomsGoodsItem
                CustomsGoodsItem customsGoodsItem = new CustomsGoodsItem
                 {

                     CustomsValue = Helpers.FormatNumeric(hmd.TriGiaKB, GlobalsShare.TriGiaNT),
                     Sequence = Helpers.FormatNumeric(hmd.SoThuTuHang),
                     StatisticalValue = Helpers.FormatNumeric(hmd.TriGiaTT, GlobalsShare.TriGiaNT),
                     UnitPrice = Helpers.FormatNumeric(hmd.DonGiaKB, GlobalsShare.DonGiaNT),
                     StatisticalUnitPrice = Helpers.FormatNumeric(hmd.DonGiaTT, GlobalsShare.DonGiaNT),
                     Manufacturer = new NameBase { Name = hmd.TenHangSX/*Tên hãng sx*/, Identity = hmd.MaHangSX/*Mã hãng sx*/ },
                     Origin = new Origin { OriginCountry = hmd.NuocXX_ID.Substring(0, 2) },
                     GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hmd.SoLuong, 5), MeasureUnit = VNACCS_Mapper.GetCodeVNACC(hmd.DVT_ID) }

                 };

                //Trị giá trên mỗi dòng hàng (Thường thì chia đều cho tất cả dòng hàng) nếu có 1 dòng hàng thì tính tổng
                //Nếu có nhiều hơn 1 dòng hàng thì chia đều ra cho mỗi dòng hàng
                // ExitToEntryCharge Tổng chi phí bằng (tkmd.PhiVanChuyen + tkmd.PhiKhac + tkmd.PhiBaoHiem)/số kiện hàng
                //FreightCharge chi phí vận tải cũng chia đôi ra
                //Metho = số thứ tự kiện hàng.  
                // Update by KhanhHN 
                // method = phương pháp xác định trị giá. Nếu có tờ khai trị giá thì method = 1
                decimal dTongPhi = tkmd.PhiVanChuyen + tkmd.PhiKhac + tkmd.PhiBaoHiem;
                decimal dPhiVanChuyen = tkmd.PhiVanChuyen;
                customsGoodsItem.CustomsValuation = new CustomsValuation
                {
                    ExitToEntryCharge = Helpers.FormatNumeric(dTongPhi / soluonghang, 4),
                    FreightCharge = Helpers.FormatNumeric(dPhiVanChuyen / soluonghang, 4),
                    OtherChargeDeduction = "0",
                    Method = string.Empty
                };
                #endregion

                #region Commodity Hàng trong tờ khai
                Commodity commodity = new Commodity
                {
                    //Type = "1",
                    Description = hmd.TenHang,
                    Identification = hmd.MaPhu,
                    TariffClassification = hmd.MaHS.Trim(),
                    TariffClassificationExtension = hmd.MaHSMoRong,
                    Brand = hmd.NhanHieu,
                    Grade = hmd.QuyCachPhamChat,
                    Ingredients = hmd.ThanhPhan,
                    ModelNumber = hmd.Model,
                    IsNew = hmd.isHangCu ? "0" : "1",
                    DutyTaxFee = new List<DutyTaxFee>(),
                    isIntegrate = hmd.IsHangDongBo ? "1" : "0",
                    DutyPreference = hmd.CheDoUuDai,
                    //Hệ thống hải quan chưa sử dụng
                    InvoiceLine = new InvoiceLine { ItemCharge = string.Empty, Line = string.Empty }
                };
                #region DutyTaxFee tiền thuế của hàng
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueXNK, 2),
                    DutyRegime = hmd.BieuThueXNK,
                    SpecificTaxBase = string.Empty,
                    Tax = tkmd.HMDCollection[i].ThueTuyetDoi ? "0" : Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatXNK, 1),
                    AbsoluteTax = tkmd.HMDCollection[i].ThueTuyetDoi ? Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueXNK, 4) : string.Empty,
                    Type = DutyTaxFeeType.THUE_XNK
                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueGTGT, 2),
                    DutyRegime = hmd.BieuThueGTGT,
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatGTGT, 1),
                    Type = DutyTaxFeeType.THUE_VAT,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueTTDB, 2),
                    DutyRegime = hmd.BieuThueTTDB,
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatTTDB, 1),
                    Type = DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET,

                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].TriGiaThuKhac, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].TyLeThuKhac, 1),
                    Type = DutyTaxFeeType.THUE_KHAC,


                });
                commodity.DutyTaxFee.Add(new DutyTaxFee
                {
                    AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].PhuThu, 2),
                    DutyRegime = "",
                    SpecificTaxBase = "",
                    Tax = Helpers.FormatNumeric(0, 1),
                    Type = DutyTaxFeeType.THUE_CHENH_LECH_GIA,

                });
                #region Thuế bảo vệ môi trường và thuế chống bán phá giá
                if (tkmd.HMDCollection[i].ThueBVMT > 0)
                {

                    commodity.DutyTaxFee.Add(new DutyTaxFee
                    {
                        AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueBVMT, 2),
                        DutyRegime = hmd.BieuThueBVMT,
                        SpecificTaxBase = "",
                        Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatBVMT, 4),
                        Type = DutyTaxFeeType.THUE_BAO_VE_MOI_TRUONG,

                    });
                }
                if (tkmd.HMDCollection[i].ThueChongPhaGia > 0)
                {
                    commodity.DutyTaxFee.Add(new DutyTaxFee
                    {
                        AdValoremTaxBase = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueChongPhaGia, 2),
                        DutyRegime = hmd.BieuThueCBPG,
                        SpecificTaxBase = "",
                        Tax = Helpers.FormatNumeric(tkmd.HMDCollection[i].ThueSuatChongPhaGia, 4),
                        Type = DutyTaxFeeType.THUE_CHONG_BAN_PHA_GIA,

                    });
                }
                #endregion

                #region Phí hải quan(nếu có)
                tkmd.LoadListLePhiHQ();
                foreach (LePhiHQ lephi in tkmd.LePhiHQCollection)
                {
                    commodity.DutyTaxFee.Add(new DutyTaxFee
                    {
                        AdValoremTaxBase = Helpers.FormatNumeric(lephi.SoTienLePhi, 2),
                        DutyRegime = "",
                        SpecificTaxBase = "",
                        Tax = Helpers.FormatNumeric(0, 4),
                        Type = lephi.MaLePhi,

                    });
                }
                #endregion
                #endregion DutyTaxFee thuế

                customsGoodsItem.Commodity = commodity;
                #endregion Hàng chính

                #region AdditionalDocument if Search(Giấy phép có hàng trong tờ khai)
                //Lấy thông tin giấy phép đầu tiên
                if (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0)
                {
                    Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep = tkmd.GiayPhepCollection[0];
                    customsGoodsItem.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument()
                     {
                         Issue = giayphep.NgayGiayPhep.ToString(sfmtDate), /*Ngày cấp giấy phép yyyy-MM-dd*/
                         Issuer = giayphep.NguoiCap/*Người cấp*/,
                         IssueLocation = giayphep.NoiCap/*Nơi cấp*/,
                         Reference = giayphep.SoGiayPhep/*Số giấy phép*/,
                         Type = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE,
                         Name = "GP"/* Tên giấy phép*/,
                         Expire = giayphep.NgayHetHan.ToString(sfmtDate)
                     };
                }
                #endregion

                #region CertificateOfOrigin if Search(Co có hàng trong tờ khai)

                if (tkmd.COCollection != null && tkmd.COCollection.Count > 0)
                {
                    Company.KDT.SHARE.QuanLyChungTu.CO co = tkmd.COCollection[tkmd.COCollection.Count - 1];
                    customsGoodsItem.CertificateOfOrigin = new Company.KDT.SHARE.Components.CertificateOfOrigin()
                    {
                        Issue = co.NgayCO.ToString(sfmtDate),
                        Issuer = co.ToChucCap,
                        IssueLocation = co.NuocCapCO,
                        Reference = co.SoCO,
                        Type = co.LoaiCO,
                        Name = "CO",
                        Expire = co.NgayHetHan.ToString(sfmtDate),
                        Exporter = co.TenDiaChiNguoiXK,
                        Importer = co.TenDiaChiNguoiNK,
                        ExportationCountry = co.MaNuocXKTrenCO,
                        ImportationCountry = co.MaNuocNKTrenCO,
                        Content = co.ThongTinMoTaChiTiet,
                        IsDebt = Helpers.FormatNumeric(co.NoCo, 0),
                        Submit = co.ThoiHanNop.ToString(sfmtDate)
                    };
                }
                #endregion

                #region AdditionalInformations - ValuationAdjustments Thêm hàng từ tờ khai trị giá 1,2,3
                if (isToKhaiNhap)
                {
                    //Thông tin tờ khai trị giá.
                    #region Tờ khai trị giá PP1
                    if (tkmd.TKTGCollection != null)
                        if (tkmd.TKTGCollection.Count > 0)
                        {
                            ToKhaiTriGia tktg = tkmd.TKTGCollection[0];
                            foreach (HangTriGia hangtrigia in tktg.HTGCollection)
                                if (hangtrigia.STTHang == hmd.SoThuTuHang)
                                {
                                    customsGoodsItem.AdditionalInformations = new List<AdditionalInformation>();
                                    customsGoodsItem.ValuationAdjustments = new List<ValuationAdjustment>();
                                    /* Fill dữ liệu hàng  trong tờ khai trị giá PP1*/
                                    //Nếu có tờ khai trị giá thì method = 1 
                                    customsGoodsItem.CustomsValuation.Method = "1";
                                    #region AdditionalInformations Nội dung tờ khai trị giá
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "TO_SO",
                                        Statement = AdditionalInformationStatement.TO_SO,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.ToSo) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "NGAY_XK",
                                        Statement = AdditionalInformationStatement.NGAY_XK,
                                        Content = new Content() { Text = tktg.NgayXuatKhau.ToString(sfmtDate) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "QUYEN_SD",
                                        Statement = AdditionalInformationStatement.QUYEN_SD,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.QuyenSuDung) }
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "KHONG_XD",
                                        Statement = AdditionalInformationStatement.KHONG_XD,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.KhongXacDinh) }
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "TRA_THEM",
                                        Statement = AdditionalInformationStatement.TRA_THEM,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.TraThem) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "TIEN_TRA_16",
                                        Statement = AdditionalInformationStatement.TIEN_TRA_16,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.TienTra) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "CO_QHDB",
                                        Statement = AdditionalInformationStatement.CO_QHDB,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.CoQuanHeDacBiet) }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "KIEU_QHDB",
                                        Statement = AdditionalInformationStatement.KIEU_QHDB,
                                        Content = new Content() { Text = tktg.KieuQuanHe }
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        StatementDescription = "ANH_HUONG_QH",
                                        Statement = AdditionalInformationStatement.ANH_HUONG_QH,
                                        Content = new Content() { Text = Helpers.FormatNumeric(tktg.AnhHuongQuanHe) }
                                    });
                                    #endregion Nội dung tờ khai trị giá

                                    #region ValuationAdjustments Chi tiết hàng tờ khai trị giá pp1
                                    if (tktg.HTGCollection != null && tktg.HTGCollection.Count > 0)
                                    {
                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Gia_hoa_don,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.GiaTrenHoaDon, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Thanh_toan_gian_tiep,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.KhoanThanhToanGianTiep, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_tra_truoc,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TraTruoc, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_hoa_hong,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.HoaHong, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_bao_bi,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiBaoBi, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_dong_goi,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiDongGoi, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Khoan_tro_giup,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TroGiup, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_NVL,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.NguyenLieu, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_NL,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.VatLieu, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_cong_cu,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.CongCu, GlobalsShare.TriGiaNT)
                                        });
                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tro_giup_thiet_ke,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ThietKe, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_ban_quyen,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.BanQuyen, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_phai_tra,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TienTraSuDung, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_van_tai,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiVanChuyen, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_bao_hiem,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.PhiBaoHiem, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_VT_BH_noi_dia,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiNoiDia, GlobalsShare.TriGiaNT)
                                        });

                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Phi_phat_sinh,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.ChiPhiPhatSinh, GlobalsShare.TriGiaNT)
                                        });


                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Tien_lai,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TienLai, GlobalsShare.TriGiaNT)
                                        });


                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Thue_phi_le_phi,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.TienThue, GlobalsShare.TriGiaNT)
                                        });
                                        customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                        {
                                            Addition = ValuationAdjustmentAddition.Khoan_Giam_Gia,
                                            Percentage = string.Empty,
                                            Amount = Helpers.FormatNumeric(hangtrigia.GiamGia, GlobalsShare.TriGiaNT)
                                        });

                                    }
                                }


                                    #endregion Chi tiết hàng tờ khai trị giá

                        }
                    #endregion Tờ khai trị giá PP1

                    if (tkmd.TKTGPP23Collection != null)
                        if (tkmd.TKTGPP23Collection.Count > 0)
                        {
                            foreach (ToKhaiTriGiaPP23 tkpgP23 in tkmd.TKTGPP23Collection)
                            {
                                if (tkpgP23.STTHang == hmd.SoThuTuHang)
                                {
                                    customsGoodsItem.AdditionalInformations = new List<AdditionalInformation>();
                                    customsGoodsItem.ValuationAdjustments = new List<ValuationAdjustment>();
                                    string maTktg = tkpgP23.MaToKhaiTriGia.ToString();
                                    customsGoodsItem.CustomsValuation.Method = maTktg;
                                    #region AdditionalInformations Nội dung tờ khai trị giá PP23

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.LyDo },
                                        Statement = maTktg + AdditionalInformationStatement.LYDO_KAD_PP1,
                                        StatementDescription = "LYDO_KAD_PP1"

                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.NgayXuat.ToString(sfmtDate) },
                                        Statement = maTktg + AdditionalInformationStatement.NGAY_XK23,
                                        StatementDescription = "NGAY_XK"

                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = Helpers.FormatNumeric(tkpgP23.STTHangTT) },
                                        Statement = maTktg + AdditionalInformationStatement.STTHANG_TT,
                                        StatementDescription = "STTHANG_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = Helpers.FormatNumeric(tkpgP23.SoTKHangTT) },
                                        Statement = maTktg + AdditionalInformationStatement.SOTK_TT,
                                        StatementDescription = "SOTK_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.NgayDangKyHangTT.ToString(sfmtDate) },
                                        Statement = maTktg + AdditionalInformationStatement.NGAY_NK_TT,
                                        StatementDescription = "NGAY_NK_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.MaHaiQuanHangTT },
                                        Statement = maTktg + AdditionalInformationStatement.MA_HQ_TT,
                                        StatementDescription = "MA_HQ_TT"
                                    });


                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.NgayXuatTT.ToString(sfmtDate) },
                                        Statement = maTktg + AdditionalInformationStatement.NGAY_XK_TT,
                                        StatementDescription = "NGAY_XK_TT"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.GiaiTrinh },
                                        Statement = maTktg + AdditionalInformationStatement.GIAI_TRINH,
                                        StatementDescription = "GIAI_TRINH"
                                    });

                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.MaLoaiHinhHangTT },
                                        Statement = maTktg + AdditionalInformationStatement.Ma_LH,
                                        StatementDescription = "Ma_LH"
                                    });
                                    customsGoodsItem.AdditionalInformations.Add(new AdditionalInformation()
                                    {
                                        Content = new Content() { Text = tkpgP23.TenHangTT },
                                        Statement = maTktg + AdditionalInformationStatement.HANG_TUONG_TU,
                                        StatementDescription = "Ma_LH"
                                    });
                                    #endregion Nội dung tờ khai trị giá PP23

                                    #region ValuationAdjustments Chi tiết nội dung trong tờ khai tri giá PP23

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.Tri_gia_hang_TT,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.TriGiaNguyenTeHangTT, GlobalsShare.TriGiaNT)
                                    });

                                    // Cộng ghi số dương trừ ghi số âm(+/-)
                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_cap_do_TM,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongThuongMai != 0 ? tkpgP23.DieuChinhCongThuongMai : -tkpgP23.DieuChinhTruCapDoThuongMai, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_so_luong,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongSoLuong != 0 ? tkpgP23.DieuChinhCongSoLuong : -tkpgP23.DieuChinhTruSoLuong, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_khoan_khac,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongKhoanGiamGiaKhac != 0 ? tkpgP23.DieuChinhCongKhoanGiamGiaKhac : -tkpgP23.DieuChinhTruKhoanGiamGiaKhac, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_phi_van_tai,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongChiPhiVanTai != 0 ? tkpgP23.DieuChinhCongChiPhiVanTai : -tkpgP23.DieuChinhTruChiPhiVanTai, GlobalsShare.TriGiaNT)
                                    });

                                    customsGoodsItem.ValuationAdjustments.Add(new ValuationAdjustment()
                                    {
                                        Addition = maTktg + ValuationAdjustmentAddition.DCC_phi_bao_hiem,
                                        Percentage = string.Empty,
                                        Amount = Helpers.FormatNumeric(tkpgP23.DieuChinhCongChiPhiBaoHiem != 0 ? tkpgP23.DieuChinhCongChiPhiBaoHiem : -tkpgP23.DieuChinhTruChiPhiBaoHiem, GlobalsShare.TriGiaNT)
                                    });

                                    #endregion Chi tiết nội dung trong tờ khai tri giá PP23

                                    /* Fill dữ liệu hàng trong tờ khai trị giá PP23*/

                                }
                            }
                        }


                }
                else
                {
                    /*
                    customsGoodsItem.SpecializedManagement = new SpecializedManagement()
                    {
                        GrossMass = "",
                        Identification = "",
                        MeasureUnit = "",
                        Quantity = "",
                        Type = "",
                        UnitPrice = ""
                    };*/
                }
                #endregion thêm hàng từ tờ khai trị giá 1,2,3

                #region Văn bản miễn thuế
                if (hmd.MienGiamThueCollection != null && hmd.MienGiamThueCollection.Count > 0)
                {
                    ReduceTax miengiam = new ReduceTax
                    {
                        Reference = hmd.MienGiamThueCollection[0].SoVanBanMienGiam,
                        tax = Helpers.FormatNumeric(hmd.MienGiamThueCollection[0].ThueSuatTruocGiam, 4),
                        redureValue = Helpers.FormatNumeric(hmd.MienGiamThueCollection[0].TyLeMienGiam, 4)
                    };
                    customsGoodsItem.ReduceTax = new List<ReduceTax>();
                    customsGoodsItem.ReduceTax.Add(miengiam);
                }
                #endregion
                //SpecializedManagement chuyenNganh = new SpecializedManagement
                //{
                //    Type = " ",
                //    Identification = " ",
                //    Quantity = " ",
                //    MeasureUnit = " ",
                //    UnitPrice = "  ",
                //};
                //customsGoodsItem.SpecializedManagement = chuyenNganh;
                tokhai.GoodsShipment.CustomsGoodsItems.Add(customsGoodsItem);
            }

            #endregion CustomGoodsItem Danh sách hàng khai báo


            #region Danh sách các chứng từ đính kèm

            if (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0)
            {
                tokhai.License = new List<Company.KDT.SHARE.Components.License>();
                #region License Giấy phép
                foreach (Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayPhep in tkmd.GiayPhepCollection)
                {
                    Company.KDT.SHARE.Components.License lic = LicenseFrom(giayPhep);
                    tokhai.License.Add(lic);
                }
                #endregion Giấy phép
            }

            if (tkmd.HopDongThuongMaiCollection != null && tkmd.HopDongThuongMaiCollection.Count > 0)
            {
                tokhai.ContractDocument = new List<Company.KDT.SHARE.Components.ContractDocument>();
                #region ContractDocument  Hợp đồng thương mại
                foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hdThuongMai in tkmd.HopDongThuongMaiCollection)
                {
                    Company.KDT.SHARE.Components.ContractDocument contractDocument = ContractFrom(hdThuongMai);
                    tokhai.ContractDocument.Add(contractDocument);
                }
                #endregion Hợp đồng thương mại
            }

            if (tkmd.HoaDonThuongMaiCollection != null && tkmd.HoaDonThuongMaiCollection.Count > 0)
            {
                tokhai.CommercialInvoices = new List<Company.KDT.SHARE.Components.CommercialInvoice>();
                #region CommercialInvoice Hóa đơn thương mại
                foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoaDonThuongMai in tkmd.HoaDonThuongMaiCollection)
                {
                    Company.KDT.SHARE.Components.CommercialInvoice commercialInvoice = CommercialInvoiceFrom(hoaDonThuongMai);
                    tokhai.CommercialInvoices.Add(commercialInvoice);
                }
                #endregion Hóa đơn thương mại
            }

            if (tkmd.COCollection != null && tkmd.COCollection.Count > 0)
            {
                tokhai.CertificateOfOrigins = new List<Company.KDT.SHARE.Components.CertificateOfOrigin>();
                #region CertificateOfOrigin Thêm CO
                {
                    tokhai.CertificateOfOrigins = new List<Company.KDT.SHARE.Components.CertificateOfOrigin>();
                    foreach (Company.KDT.SHARE.QuanLyChungTu.CO co in tkmd.COCollection)
                    {
                        tokhai.CertificateOfOrigins.Add(CertificateOfOriginFrom(co, tkmd, isToKhaiNhap));
                    }
                }
                #endregion CO
            }


            if (tkmd.VanTaiDon != null)
            #region BillOfLadings Vận đơn
            {

                tokhai.BillOfLadings = new List<Company.KDT.SHARE.Components.BillOfLading>();
                Company.KDT.SHARE.QuanLyChungTu.VanDon vandon = tkmd.VanTaiDon;
                Company.KDT.SHARE.Components.BillOfLading billOfLading;
                if (isToKhaiNhap)
                {
                    billOfLading = new Company.KDT.SHARE.Components.BillOfLading()
                   {
                       Reference = vandon.SoVanDon,
                       Issue = vandon.NgayVanDon.ToString(sfmtDate),
                       IssueLocation = vandon.ID_NuocPhatHanh,
                       IsContainer = vandon.HangRoi ? "0" : "1",
                       TransitLocation = vandon.DiaDiemChuyenTai,
                       Category = vandon.LoaiVanDon,
                   };
                    billOfLading.BorderTransportMeans = new BorderTransportMeans()
                    {
                        Identity = vandon.SoHieuPTVT,
                        Identification = vandon.TenPTVT,
                        Journey = (vandon.LoaiVanDon == LoaiVanDon.DUONG_KHONG || vandon.LoaiVanDon == LoaiVanDon.DUONG_BIEN) ? vandon.SoHieuChuyenDi : null,
                        ModeAndType = tkmd.PTVT_ID,
                        Departure = vandon.NgayKhoiHanh.ToString(sfmtDate),
                        RegistrationNationality = (vandon.LoaiVanDon == LoaiVanDon.DUONG_KHONG && string.IsNullOrEmpty(vandon.QuocTichPTVT)) ? vandon.QuocTichPTVT.Substring(0, 2) : string.Empty
                    };
                    billOfLading.Carrier = (vandon.LoaiVanDon == LoaiVanDon.DUONG_KHONG || vandon.LoaiVanDon == LoaiVanDon.DUONG_BIEN) ? new NameBase()
                    {
                        Name = vandon.TenHangVT,
                        Identity = vandon.MaHangVT
                    } : null;
                    billOfLading.Consignment = new Consignment()
                    {
                        Consignor = new NameBase()
                        {
                            Name = vandon.TenNguoiGiaoHang,
                            Identity = vandon.MaNguoiGiaoHang
                        },
                        Consignee = new NameBase()
                        {
                            Name = vandon.TenNguoiNhanHang,
                            Identity = vandon.MaNguoiNhanHang
                        },
                        NotifyParty = (vandon.LoaiVanDon == LoaiVanDon.DUONG_BO) ? null : new NameBase()
                        {
                            Name = vandon.TenNguoiNhanHangTrungGian,
                            Identity = vandon.MaNguoiNhanHangTrungGian
                        },
                        LoadingLocation = new Company.KDT.SHARE.Components.LoadingLocation()
                        {
                            Name = vandon.TenCangXepHang,
                            Code = vandon.MaCangXepHang,
                            Loading = (vandon.LoaiVanDon == LoaiVanDon.DUONG_BO) ? string.Empty : vandon.NgayKhoiHanh.ToString(sfmtDate)
                        },
                        UnloadingLocation = new UnloadingLocation()
                        {
                            Name = vandon.TenCangDoHang,
                            Code = vandon.MaCangDoHang,
                            Arrival = vandon.NgayDenPTVT.ToString(sfmtDate)
                        },
                        DeliveryDestination = new DeliveryDestination() { Line = vandon.DiaDiemGiaoHang },
                        ConsignmentItemPackaging = new Packaging()
                        {
                            Quantity = Helpers.FormatNumeric(vandon.TongSoKien),
                            Type = string.IsNullOrEmpty(vandon.LoaiKien) ? string.Empty : vandon.LoaiKien,
                            MarkNumber = string.Empty
                        }
                    };
                }
                else
                {
                    billOfLading = new Company.KDT.SHARE.Components.BillOfLading()
                    {
                        Reference = string.Empty,
                        Issue = string.Empty,
                        IssueLocation = string.Empty,
                        IsContainer = vandon.HangRoi ? "0" : "1",
                        TransitLocation = string.Empty,
                        Category = vandon.LoaiVanDon,
                    };
                    billOfLading.BorderTransportMeans = new BorderTransportMeans()
                    {
                        Identity = string.Empty,
                        Identification = string.Empty,
                        Journey = string.Empty,
                        ModeAndType = string.Empty,
                        Departure = string.Empty,
                        RegistrationNationality = string.Empty
                    };
                    billOfLading.Carrier = new NameBase()
                    {
                        Name = string.Empty,
                        Identity = string.Empty
                    };
                    billOfLading.Consignment = new Consignment()
                    {
                        Consignor = new NameBase()
                        {
                            Name = string.Empty,
                            Identity = string.Empty
                        },
                        Consignee = new NameBase()
                        {
                            Name = string.Empty,
                            Identity = string.Empty
                        },
                        NotifyParty = (vandon.LoaiVanDon != LoaiVanDon.DUONG_KHONG) ? null : new NameBase()
                        {
                            Name = string.Empty,
                            Identity = string.Empty
                        },
                        LoadingLocation = new Company.KDT.SHARE.Components.LoadingLocation()
                        {
                            Name = string.Empty,
                            Code = string.Empty,
                            Loading = string.Empty
                        },
                        UnloadingLocation = new UnloadingLocation()
                        {
                            Name = string.Empty,
                            Code = string.Empty,
                            Arrival = string.Empty
                        },
                        DeliveryDestination = new DeliveryDestination() { Line = vandon.DiaDiemGiaoHang },
                        ConsignmentItemPackaging = new Packaging()
                        {
                            Quantity = string.Empty,
                            Type = string.Empty,
                            MarkNumber = string.Empty
                        }
                    };
                }
                #region Đổ dữ liệu bên ngoài GoodsShipment
                tokhai.GoodsShipment.Consignor = billOfLading.Consignment.Consignor;
                tokhai.GoodsShipment.Consignee = billOfLading.Consignment.Consignee;
                tokhai.GoodsShipment.NotifyParty = billOfLading.Consignment.NotifyParty;
                tokhai.GoodsShipment.DeliveryDestination = billOfLading.Consignment.DeliveryDestination;
                tokhai.GoodsShipment.ExitCustomsOffice.Name = vandon.CuaKhauXuat;
                #endregion
                if (vandon.ContainerCollection != null && vandon.ContainerCollection.Count > 0)
                    billOfLading.Consignment.TransportEquipments = new List<TransportEquipment>();

                foreach (Company.KDT.SHARE.QuanLyChungTu.Container container in vandon.ContainerCollection)
                {

                    TransportEquipment transportEquipment = new TransportEquipment()
                    {
                        Characteristic = container.LoaiContainer,
                        EquipmentIdentifications = new EquipmentIdentification()
                        {
                            identification = container.SoHieu
                        },
                        Fullness = Helpers.FormatNumeric(container.Trang_thai, 0),
                        Seal = container.Seal_No,
                        GrossMass = Helpers.FormatNumeric(container.TrongLuong, 4),
                        NetMass = Helpers.FormatNumeric(container.TrongLuongNet, 4),
                        Quantity = Helpers.FormatNumeric(container.SoKien, 2),
                        PackagingLocation = container.DiaDiemDongHang,
                        Property = container.Trang_thai.ToString()
                    };

                    billOfLading.Consignment.TransportEquipments.Add(transportEquipment);
                }

                #region Hàng  trong vận đơn
                if (vandon.ListHangOfVanDon != null && vandon.ListHangOfVanDon.Count > 0)
                {
                    List<ConsignmentItem> hangsVanDon = new List<ConsignmentItem>();

                    foreach (Company.KDT.SHARE.QuanLyChungTu.HangVanDonDetail hangVanDon in vandon.ListHangOfVanDon)
                    {
                        HangMauDich hangToKhai = tkmd.HMDCollection.Find(hang => hang.ID == hangVanDon.HMD_ID);
                        if (hangToKhai != null)
                            hangsVanDon.Add(new ConsignmentItem()
                            {
                                Sequence = Helpers.FormatNumeric(hangVanDon.SoThuTuHang),
                                ConsignmentItemPackaging = new Packaging()
                                {
                                    Quantity = Helpers.FormatNumeric(hangVanDon.SoLuong),
                                    Type = hangVanDon.LoaiKien,
                                    //MarkNumber = hangVanDon.SoHieuKien,
                                },
                                Commodity = new Commodity()
                                {
                                    Description = hangVanDon.TenHang,
                                    Identification = hangToKhai.Ma,//Mã hàng
                                    TariffClassification = hangVanDon.MaHS.Trim()
                                    //TariffClassificationExtension = hangVanDon.MaHS.Trim()
                                },
                                GoodsMeasure = new GoodsMeasure()
                                {
                                    GrossMass = Helpers.FormatNumeric(hangVanDon.TrongLuong, 4),
                                    Quantity = Helpers.FormatNumeric(hangVanDon.SoLuong, 4),
                                    MeasureUnit = VNACCS_Mapper.GetCodeVNACC(hangVanDon.DVT_ID.Substring(0, 3)),
                                    NetMass = Helpers.FormatNumeric(hangVanDon.TrongLuongTinh, 4),
                                    CustomsValue = Helpers.FormatNumeric(hangVanDon.TriGiaKB, 4),

                                },
                                //                                 EquipmentIdentification = new EquipmentIdentification()
                                //                                 {
                                //                                     identification = hangVanDon.SoHieuContainer
                                //                                 }

                            });

                    }
                    billOfLading.Consignment.ConsignmentItems = hangsVanDon;
                }
                #endregion Hàng và Container trong vận đơn

                tokhai.BillOfLadings.Add(billOfLading);

            }

            #endregion BillOfLadings  Vận đơn

            if (tkmd.listChuyenCuaKhau != null && tkmd.listChuyenCuaKhau.Count > 0)
            {
                tokhai.CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>();
                #region CustomsOfficeChangedRequest Đề nghị chuyển cửa khẩu
                foreach (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCuaKhau in tkmd.listChuyenCuaKhau)
                {
                    CustomsOfficeChangedRequest customsOfficeChangedRequest = CustomsOfficeChangedRequestFrom(dnChuyenCuaKhau);
                    tokhai.CustomsOfficeChangedRequest.Add(customsOfficeChangedRequest);
                };
                #endregion Đề nghị chuyển cửa khẩu
            }

            if (tkmd.ChungTuKemCollection != null && tkmd.ChungTuKemCollection.Count > 0)
            {
                tokhai.AttachDocumentItem = new List<AttachDocumentItem>();
                #region AttachDocumentItem Chứng từ đính kèm
                foreach (ChungTuKem fileinChungtuDinhKem in tkmd.ChungTuKemCollection)
                {
                    AttachDocumentItem attachDocumentItem = AttachDocumentItemFrom(tkmd, fileinChungtuDinhKem);
                    tokhai.AttachDocumentItem.Add(attachDocumentItem);
                };
                #endregion Chứng từ đính kèm
            }

            if (tkmd.GiayKiemTraCollection != null && tkmd.GiayKiemTraCollection.Count > 0)
            {
                #region Giấy đăng ký kiểm tra / giấy kết quả kiểm tra

                foreach (GiayKiemTra gkt in tkmd.GiayKiemTraCollection)
                {
                    if (gkt.LoaiGiay == LoaiGiayKiemTra.DANG_KY_KIEM_TRA)
                    {
                        gkt.LoadFull();
                        tokhai.ExaminationRegistration = ExamRegistration(gkt);
                    }
                    else if (gkt.LoaiGiay == LoaiGiayKiemTra.KET_QUA_KIEM_TRA)
                    {
                        gkt.LoadFull();
                        tokhai.ExaminationResult = ExaminationResult(gkt);
                    }


                }
                #endregion
            }

            if (tkmd.ChungThuGD != null && tkmd.ChungThuGD.LoaiKB == 0 && tkmd.ChungThuGD.ListHang.Count > 0)
            {
                #region Chứng thư giám định

                CertificateOfInspection chungthu = CertificateOfInspectionFrom(tkmd.ChungThuGD);
                tokhai.CertificateOfInspection = chungthu;

                #endregion
            }



            #endregion Danh sách giấy phép XNK đi kèm


            #region AdditionalDocumentNos Thêm chứng từ khác

            if (tkmd.ChungTuNoCollection != null && tkmd.ChungTuNoCollection.Count > 0)
            {
                tokhai.AdditionalDocumentNos = new List<Company.KDT.SHARE.Components.AdditionalDocument>();
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuNo ctn in tkmd.ChungTuNoCollection)
                {
                    tokhai.AdditionalDocumentNos.Add(new Company.KDT.SHARE.Components.AdditionalDocument()
                    {
                        Type = ctn.MA_LOAI_CT,
                        Reference = ctn.SO_CT,
                        Issue = ctn.NGAY_CT.ToString(sfmtDate),
                        IssueLocation = ctn.NOI_CAP,
                        Issuer = ctn.TO_CHUC_CAP,
                        Expire = ctn.NgayHetHan.ToString(sfmtDate),
                        IsDebt = Helpers.FormatNumeric(ctn.IsNoChungTu),
                        Submit = ctn.ThoiHanNop.ToString(sfmtDate),
                        AdditionalInformation = new AdditionalInformation()
                        {
                            Content = new Content() { Text = ctn.DIENGIAI }
                        }

                    });
                }

            }
            #endregion
            return tokhai;
        }


        #endregion

        #region Data Mapper
        private static List<Agent> AgentsFrom(ToKhaiMauDich tkmd)
        {
            List<Agent> Agents = new List<Agent>();
            // Edit by Khanhhn - 12/06/2012
            #region Agents Đại lý khai
            // Người khai hải quan
            Agents.Add(new Agent
            {
                Name = Company.KDT.SHARE.Components.Globals.isKhaiDL ? tkmd.TenDaiLyTTHQ : tkmd.TenDoanhNghiep,
                Identity = Company.KDT.SHARE.Components.Globals.isKhaiDL ? tkmd.MaDaiLyTTHQ : tkmd.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            // Đại lý khai Hải Quan
            if (!string.IsNullOrEmpty(tkmd.TenDaiLyTTHQ))
                Agents.Add(new Agent
                {
                    Name = tkmd.TenDaiLyTTHQ,
                    Identity = tkmd.MaDaiLyTTHQ,
                    Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN
                });
            // ủy Thác
            if (!string.IsNullOrEmpty(tkmd.MaDonViUT))
                Agents.Add(new Agent
                {
                    Name = tkmd.TenDonViUT,
                    Identity = tkmd.MaDonViUT,
                    Status = AgentsStatus.UYTHAC,
                });
            // Doanh Nghiệp chịu trách nhiệm nộp thuế
            Agents.Add(new Agent
            {
                Name = tkmd.TenDoanhNghiep,
                Identity = tkmd.MaDoanhNghiep,
                Status = AgentsStatus.NGUOICHIU_TRACHNHIEM_NOPTHUE
            });
            #endregion
            return Agents;
        }
        private static Company.KDT.SHARE.Components.CertificateOfOrigin CertificateOfOriginFrom(CO co, ToKhaiMauDich tkmd, bool isToKhaiNhap)
        {
            #region Điền thông tin CO
            Company.KDT.SHARE.Components.CertificateOfOrigin certificateOfOrigin = new Company.KDT.SHARE.Components.CertificateOfOrigin
            {
                Reference = co.SoCO,
                Type = co.LoaiCO,
                Issuer = co.ToChucCap,
                Issue = co.NgayCO.ToString(sfmtDate),
                IssueLocation = co.NuocCapCO,
                Representative = co.NguoiKy,

                ExporterEx = isToKhaiNhap ? new NameBase() { Name = co.TenDiaChiNguoiXK, Identity = string.Empty } : new NameBase { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep },
                ExportationCountryEx = new LocationNameBase { Code = co.MaNuocXKTrenCO, Name = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(co.MaNuocXKTrenCO) },

                ImporterEx = isToKhaiNhap ? new NameBase() { Name = tkmd.TenDoanhNghiep, Identity = tkmd.MaDoanhNghiep } :
                            new NameBase { Name = co.TenDiaChiNguoiNK, Identity = string.Empty },
                ImportationCountryEx = new LocationNameBase { Code = co.MaNuocNKTrenCO, Name = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(co.MaNuocNKTrenCO) },

                //LoadingLocation = new LoadingLocation { Name = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangXepHang.Trim()), Code = co.CangXepHang.Trim(), Loading = co.NgayKhoiHanh.ToString(sfmtDate) },
                LoadingLocation = new Company.KDT.SHARE.Components.LoadingLocation
                {
                    //Neu la TK Nhap, chi can lay ten Cang xep hang, khong lay thong tin tu ma
                    Name = tkmd.MaLoaiHinh.Substring(0, 1) == "N" ? co.CangXepHang.Trim() : Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangXepHang.Trim()),
                    Code = co.CangXepHang.Trim(),
                    Loading = co.NgayKhoiHanh.ToString(sfmtDate)
                },

                //UnloadingLocation = new UnloadingLocation { Name = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangDoHang.Trim()), Code = co.CangDoHang.Trim() },
                UnloadingLocation = new UnloadingLocation
                {
                    //Neu la TK Xuat, chi can lay ten Cang do hang, khong lay thong tin tu ma
                    Name = tkmd.MaLoaiHinh.Substring(0, 1) == "X" ? co.TenCangDoHang.Trim() : Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(co.CangDoHang.Trim()),
                    Code = co.CangDoHang.Trim()
                },

                IsDebt = Helpers.FormatNumeric(co.NoCo),

                Submit = co.ThoiHanNop.ToString(sfmtDate),

                Description = co.MoTaHangHoa,
                PercentOrigin = Helpers.FormatNumeric(co.HamLuongXuatXu, 4),

                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = co.ThongTinMoTaChiTiet } },
                //Hang in Co
                GoodsItems = new List<GoodsItem>(),
            };
            #endregion CO
            #region Điền hàng trong Co

            //             foreach (Company.KDT.SHARE.QuanLyChungTu.HangCoDetail hangCo in co.ListHMDofCo)
            //             {
            //                 GoodsItem goodsItem = new GoodsItem();
            //                 goodsItem.Sequence = Helpers.FormatNumeric(hangCo.SoThuTuHang);
            //                 goodsItem.StatisticalValue = Helpers.FormatNumeric(hangCo.TriGiaKB, GlobalsShare.TriGiaNT);
            //                 goodsItem.CurrencyExchange = new CurrencyExchange()
            //                 {
            //                     CurrencyType = hangCo.MaNguyenTe
            //                 };
            //                 goodsItem.ConsignmentItemPackaging = new Packaging()
            //                 {
            //                     Quantity = Helpers.FormatNumeric(hangCo.SoLuong),
            //                     Type = hangCo.LoaiKien,
            //                     MarkNumber = hangCo.SoHieuKien
            //                 };
            //                 goodsItem.Commodity = new Commodity()
            //                 {
            //                     Description = hangCo.TenHang,
            //                     Identification = hangCo.MaPhu,
            //                     TariffClassification = hangCo.MaHS.Trim()
            //                 };
            //                 goodsItem.GoodsMeasure = new GoodsMeasure()
            //                 {
            //                     GrossMass = Helpers.FormatNumeric(hangCo.TrongLuong, 3),
            //                     MeasureUnit = hangCo.DVT_ID
            //                 };
            //                 goodsItem.Origin = new Origin()
            //                 {
            //                     OriginCountry = hangCo.NuocXX_ID.Trim()
            //                 };
            //                 goodsItem.Invoice = new Invoice()
            //                 {
            //                     Reference = hangCo.SoHoaDon,
            //                     Issue = hangCo.NgayHoaDon.ToString(sfmtDate)
            //                 };
            //                 certificateOfOrigin.GoodsItems.Add(goodsItem);
            //             }
            #endregion Thêm hàng
            return certificateOfOrigin;
        }
        private static AttachDocumentItem AttachDocumentItemFrom(ToKhaiMauDich tkmd, ChungTuKem chungtukem)
        {

            AttachDocumentItem attachDocumentItem = new AttachDocumentItem()
            {
                Issuer = AdditionalDocumentType.CHUNG_TU_DANG_ANH,
                Sequence = Helpers.FormatNumeric(tkmd.ChungTuKemCollection.IndexOf(chungtukem) + 1, 0),
                Issue = chungtukem.NGAY_CT.ToString(sfmtDate),
                Reference = chungtukem.SO_CT,
                Description = chungtukem.DIENGIAI,
                AttachedFiles = new List<Company.KDT.SHARE.Components.AttachedFile>(),
            };

            if (chungtukem.listCTChiTiet != null && chungtukem.listCTChiTiet.Count > 0)
                foreach (Company.KDT.SHARE.QuanLyChungTu.ChungTuKemChiTiet fileDetail in chungtukem.listCTChiTiet)
                {

                    attachDocumentItem.AttachedFiles.Add(new Company.KDT.SHARE.Components.AttachedFile
                    {
                        FileName = fileDetail.FileName,
                        //Content = new Content { Text = System.Convert.ToBase64String(fileDetail.NoiDung, Base64FormattingOptions.None), Base64 = "bin.base64" },
                    });
                };
            return attachDocumentItem;
        }
        private static Company.KDT.SHARE.Components.License LicenseFrom(Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayPhep)
        {
            Company.KDT.SHARE.Components.License lic = new Company.KDT.SHARE.Components.License
            {
                Issuer = giayPhep.NguoiCap,
                Reference = giayPhep.SoGiayPhep,
                Issue = giayPhep.NgayGiayPhep.ToString(sfmtDate),
                IssueLocation = giayPhep.NoiCap,
                Type = "",
                Category = "1",
                Expire = giayPhep.NgayHetHan.ToString(sfmtDate),
                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = giayPhep.ThongTinKhac } },
                GoodItems = new List<GoodsItem>()
            };

            if (giayPhep.ListHMDofGiayPhep != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HangGiayPhepDetail hangInGiayPhep in giayPhep.ListHMDofGiayPhep)
                {
                    lic.GoodItems.Add(new GoodsItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInGiayPhep.SoThuTuHang, 0),
                        StatisticalValue = Helpers.FormatNumeric(hangInGiayPhep.TriGiaKB, GlobalsShare.TriGiaNT),
                        CurrencyExchange = new CurrencyExchange { CurrencyType = hangInGiayPhep.MaNguyenTe },
                        Commodity = new Commodity
                        {
                            Description = hangInGiayPhep.TenHang,
                            Identification = hangInGiayPhep.MaPhu,
                            TariffClassification = hangInGiayPhep.MaHS.Trim()
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInGiayPhep.SoLuong, 3), MeasureUnit = hangInGiayPhep.DVT_ID },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInGiayPhep.GhiChu } }

                    });
                }
            return lic;
        }
        private static Company.KDT.SHARE.Components.ContractDocument ContractFrom(HopDongThuongMai hdThuongMai)
        {
            Company.KDT.SHARE.Components.ContractDocument contractDocument = new Company.KDT.SHARE.Components.ContractDocument
            {
                Reference = hdThuongMai.SoHopDongTM,
                Issue = hdThuongMai.NgayHopDongTM.ToString(sfmtDate),
                Expire = hdThuongMai.ThoiHanThanhToan.ToString(sfmtDate),
                Payment = new Payment { Method = hdThuongMai.PTTT_ID },
                TradeTerm = new TradeTerm { Condition = hdThuongMai.DKGH_ID },
                DeliveryDestination = new DeliveryDestination { Line = hdThuongMai.DiaDiemGiaoHang },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hdThuongMai.NguyenTe_ID },
                TotalValue = Helpers.FormatNumeric(hdThuongMai.TongTriGia, GlobalsShare.TriGiaNT),
                Buyer = new NameBase { Name = hdThuongMai.TenDonViMua, Identity = hdThuongMai.MaDonViMua },
                Seller = new NameBase { Name = hdThuongMai.TenDonViBan, Identity = hdThuongMai.MaDonViBan },
                AdditionalInformations = new List<AdditionalInformation>(),
                ContractItems = new List<ContractItem>()
            };
            contractDocument.AdditionalInformations.Add(new AdditionalInformation { Content = new Content { Text = hdThuongMai.ThongTinKhac } });
            if (hdThuongMai.ListHangMDOfHopDong != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail hangInHdThuongMai in hdThuongMai.ListHangMDOfHopDong)
                    contractDocument.ContractItems.Add(new ContractItem
                    {
                        unitPrice = Helpers.FormatNumeric(hangInHdThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        statisticalValue = Helpers.FormatNumeric(hangInHdThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Commodity = new Commodity
                        {
                            Description = hangInHdThuongMai.TenHang,
                            Identification = hangInHdThuongMai.MaPhu,
                            TariffClassification = hangInHdThuongMai.MaHS.Trim(),
                        },
                        Origin = new Origin { OriginCountry = hangInHdThuongMai.NuocXX_ID.Substring(0, 2) },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHdThuongMai.SoLuong, 4), MeasureUnit = VNACCS_Mapper.GetCodeVNACC(hangInHdThuongMai.DVT_ID) },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHdThuongMai.GhiChu } }
                    });
            return contractDocument;
        }
        private static Receipt ReceiptFrom(GiayNopTien giayNopTien)
        {
            Receipt receipt = new Receipt()
            {
                Reference = giayNopTien.SoLenh,
                Issue = giayNopTien.NgayPhatLenh.ToString(sfmtDate),
                Payer = new Payer
                {
                    Name = giayNopTien.TenNguoiNop,
                    Identity = giayNopTien.SoCMNDNguoiNop,
                    AddressGNT = new DeliveryDestination { Line = giayNopTien.DiaChi }
                },
                TaxPayer = new Payer
                {
                    Name = giayNopTien.TenDonViNop,
                    Identity = giayNopTien.MaDonViNop,
                    Account = new Account
                    {
                        Number = giayNopTien.SoTKNop,
                        Bank = new NameBase { Name = giayNopTien.TenNganHangNop, Identity = giayNopTien.MaNganHangNop }
                    }
                },
                Payee = new Payer
                {
                    Name = giayNopTien.TenDonViNhan,
                    Identity = giayNopTien.MaDonViNhan,
                    Account = new Account
                    {
                        Number = giayNopTien.SoTKNhan,
                        Bank = new NameBase { Name = giayNopTien.TenNganHangNhan, Identity = giayNopTien.MaNganHangNhan }
                    }
                },
                DutyTaxFee = new List<DutyTaxFee>(),
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = giayNopTien.GhiChu } },
                AdditionalDocument = new List<Company.KDT.SHARE.Components.AdditionalDocument>(),
            };
            foreach (GiayNopTienChiTiet chitiet in giayNopTien.ChiTietCollection)
            {
                receipt.DutyTaxFee.Add(DutyTaxFeeForGiayNopTien(chitiet));
            }
            foreach (GiayNopTienChungTu chungtu in giayNopTien.ChungTuCollection)
            {
                Company.KDT.SHARE.Components.AdditionalDocument add = new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Type = chungtu.LoaiChungTu,
                    Reference = chungtu.SoChungTu,
                    Name = chungtu.TenChungTu,
                    Issue = chungtu.NgayPhatHanh.ToString(sfmtDate),
                };
                receipt.AdditionalDocument.Add(add);
            }
            return receipt;
        }
        private static DutyTaxFee DutyTaxFeeForGiayNopTien(GiayNopTienChiTiet chiTietGiayNopTien)
        {
            DutyTaxFee tax = new DutyTaxFee
            {
                AdValoremTaxBase = Helpers.FormatNumeric(chiTietGiayNopTien.SoTien, 4),
                Deduct = Helpers.FormatNumeric(chiTietGiayNopTien.DieuChinhGiam, 4),
                Type = Helpers.FormatNumeric(chiTietGiayNopTien.SacThue),
                AdditionalInformations = new List<AdditionalInformation>(),
            };
            for (int i = 1; i < 6; i++)
            {
                AdditionalInformation add = new AdditionalInformation
                {
                    Content = new Content()
                };
                switch (i)
                {
                    case 1:
                        add.Statement = "211";
                        add.StatementDescription = "Chuong";
                        add.Content.Text = chiTietGiayNopTien.MaChuong;
                        break;
                    case 2:
                        add.Statement = "212";
                        add.StatementDescription = "Loai";
                        add.Content.Text = chiTietGiayNopTien.Loai;
                        break;
                    case 3:
                        add.Statement = "213";
                        add.StatementDescription = "Khoan";
                        add.Content.Text = chiTietGiayNopTien.Khoan;
                        break;
                    case 4:
                        add.Statement = "214";
                        add.StatementDescription = "Muc";
                        add.Content.Text = chiTietGiayNopTien.Muc;
                        break;
                    case 5:
                        add.Statement = "215";
                        add.StatementDescription = "Tieu Muc";
                        add.Content.Text = chiTietGiayNopTien.TieuMuc;
                        break;
                }
                tax.AdditionalInformations.Add(add);
            }

            return tax;
        }
        /// <summary>
        /// Thông tin tham chiếu đến Tờ khai
        /// </summary>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>DeclarationBase</returns>
        private static DeclarationBase DeclarationDocument(ToKhaiMauDich tkmd)
        {
            return new DeclarationBase()
            {
                Reference = Helpers.FormatNumeric(tkmd.SoToKhai),
                Issue = tkmd.NgayDangKy.ToString(sfmtDate),
                NatureOfTransaction = tkmd.MaLoaiHinh,
                DeclarationOffice = tkmd.MaHaiQuan.Trim()
            };

        }
        /// <summary>
        /// Đề nghị chuyển cửa khẩu
        /// </summary>
        /// <param name="dnChuyenCuaKhau"></param>
        /// <returns></returns>
        private static CustomsOfficeChangedRequest CustomsOfficeChangedRequestFrom(DeNghiChuyenCuaKhau dnChuyenCuaKhau)
        {
            CustomsOfficeChangedRequest customsOfficeChangedRequest = new CustomsOfficeChangedRequest
            {
                AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument { Reference = dnChuyenCuaKhau.SoVanDon, Issue = dnChuyenCuaKhau.NgayVanDon.ToString(sfmtDate) },
                AdditionalInformation = new AdditionalInformation
                {
                    Content = new Content { Text = dnChuyenCuaKhau.ThongTinKhac },
                    ExaminationPlace = dnChuyenCuaKhau.DiaDiemKiemTra.Trim().Length > 0 ? dnChuyenCuaKhau.DiaDiemKiemTra : dnChuyenCuaKhau.DiaDiemKiemTraCucHQThanhPho,
                    Time = dnChuyenCuaKhau.ThoiGianDen.ToString(sfmtDate),
                    Route = dnChuyenCuaKhau.TuyenDuong,
                },
                BorderTransportMeans = new BorderTransportMeans { ModeAndType = dnChuyenCuaKhau.PTVT_ID },
            };
            return customsOfficeChangedRequest;
        }
        private static Company.KDT.SHARE.Components.CommercialInvoice CommercialInvoiceFrom(HoaDonThuongMai hoaDonThuongMai)
        {
            Company.KDT.SHARE.Components.CommercialInvoice commercialInvoice = new Company.KDT.SHARE.Components.CommercialInvoice
            {
                Reference = hoaDonThuongMai.SoHoaDon,
                Issue = hoaDonThuongMai.NgayHoaDon.ToString(sfmtDate),
                Seller = new NameBase { Name = hoaDonThuongMai.TenDonViBan, Identity = hoaDonThuongMai.MaDonViBan },
                Buyer = new NameBase { Name = hoaDonThuongMai.TenDonViMua, Identity = hoaDonThuongMai.MaDonViMua },
                AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument { Reference = "", Issue = "" },
                Payment = new Payment { Method = hoaDonThuongMai.PTTT_ID },
                CurrencyExchange = new CurrencyExchange { CurrencyType = hoaDonThuongMai.NguyenTe_ID },
                TradeTerm = new TradeTerm { Condition = hoaDonThuongMai.DKGH_ID.Trim() },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>(),
                //Xuat xu hang hoa Origin [4]

            };

            if (hoaDonThuongMai.ListHangMDOfHoaDon != null)
                foreach (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail hangInHoaDonThuongMai in hoaDonThuongMai.ListHangMDOfHoaDon)
                    commercialInvoice.CommercialInvoiceItems.Add(new CommercialInvoiceItem
                    {
                        Sequence = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoThuTuHang, 0),
                        UnitPrice = Helpers.FormatNumeric(hangInHoaDonThuongMai.DonGiaKB, GlobalsShare.DonGiaNT),
                        StatisticalValue = Helpers.FormatNumeric(hangInHoaDonThuongMai.TriGiaKB, GlobalsShare.TriGiaNT),
                        Origin = new Origin { OriginCountry = hangInHoaDonThuongMai.NuocXX_ID.Substring(0, 2) },
                        Commodity = new Commodity
                        {
                            Description = hangInHoaDonThuongMai.TenHang,
                            Identification = hangInHoaDonThuongMai.MaPhu,
                            TariffClassification = hangInHoaDonThuongMai.MaHS.Trim()
                        },
                        GoodsMeasure = new GoodsMeasure { Quantity = Helpers.FormatNumeric(hangInHoaDonThuongMai.SoLuong, 4), MeasureUnit = VNACCS_Mapper.GetCodeVNACC(hangInHoaDonThuongMai.DVT_ID.Trim()) },
                        ValuationAdjustment = new ValuationAdjustment
                        {
                            Addition = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaTriDieuChinhTang, GlobalsShare.TriGiaNT),
                            Deduction = Helpers.FormatNumeric(hangInHoaDonThuongMai.GiaiTriDieuChinhGiam, GlobalsShare.TriGiaNT),
                        },
                        AdditionalInformation = new AdditionalInformation { Content = new Content { Text = hangInHoaDonThuongMai.GhiChu } },
                    });
            return commercialInvoice;
        }

        private static ExaminationResult ExaminationResult(GiayKiemTra giaykt)
        {
            ExaminationResult Result = new ExaminationResult()
            {
                Reference = giaykt.SoGiayKiemTra,
                Issue = giaykt.NgayDangKy.ToString(sfmtDate),
                Register = new NameBase() { Identity = giaykt.MaNguoiDuocCap, Name = giaykt.TenNguoiDuocCap },
                //GoodsItem = new List<GoodsItem>()
            };
            foreach (HangGiayKiemTra item in giaykt.HangCollection)
            {

                GoodsItem itemGiayKT = new GoodsItem()
                   {
                       Commodity = new Commodity()
                       {
                           Identification = item.MaHang,
                           Description = item.TenHang,
                           TariffClassification = item.MaHS.Trim()
                       },
                       Origin = new Origin { OriginCountry = item.NuocXX_ID },
                       GoodsMeasure = new GoodsMeasure()
                       {
                           Quantity = Helpers.FormatNumeric(item.SoLuong, 4),
                           MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.DVT_ID)
                       },
                       AdditionalInformation = new AdditionalInformation()
                       {
                           Content = new Content { Text = item.GhiChu }
                       },
                       AdditionalDocument = (!string.IsNullOrEmpty(item.LoaiChungTu) && !string.IsNullOrEmpty(item.TenChungTu)) ? new Company.KDT.SHARE.Components.AdditionalDocument()
                       {
                           Type = item.LoaiChungTu,
                           Reference = item.SoChungTu,
                           Name = item.TenChungTu.Trim(),
                           Issue = item.NgayPhatHanh.ToString(sfmtDate)
                       } : null,
                       Examination = new Examination()
                       {
                           Place = item.DiaDiemKiemTra.Trim(),
                           Examiner = new NameBase()
                       {
                           Name = item.TenCoQuanKT.Trim(),
                           Identity = item.MaCoQuanKT.Trim(),
                           Result = item.KetQuaKT.Trim()
                       }
                       },


                   };
                //Result.GoodsItem.Add(itemGiayKT);
            }
            return Result;
        }

        private static ExaminationRegistration ExamRegistration(GiayKiemTra giaykt)
        {
            //giaykt.LoadFull();
            ExaminationRegistration registration = new ExaminationRegistration()
            {
                Reference = giaykt.SoGiayKiemTra,
                Issue = giaykt.NgayDangKy.ToString(sfmtDate),
                Register = new NameBase() { Identity = giaykt.MaNguoiDuocCap, Name = giaykt.TenNguoiDuocCap },
                //GoodsItem = new List<GoodsItem>()
            };
            foreach (HangGiayKiemTra item in giaykt.HangCollection)
            {

                GoodsItem itemGiayKT = new GoodsItem()
                {
                    Commodity = new Commodity()
                    {
                        Identification = item.MaHang,
                        Description = item.TenHang,
                        TariffClassification = item.MaHS.Trim()
                    },
                    Origin = new Origin { OriginCountry = item.NuocXX_ID },
                    GoodsMeasure = new GoodsMeasure()
                    {
                        Quantity = Helpers.FormatNumeric(item.SoLuong, 4),
                        MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.DVT_ID)
                    },
                    AdditionalInformation = new AdditionalInformation()
                    {
                        Content = new Content { Text = item.GhiChu }
                    },
                    AdditionalDocument = (!string.IsNullOrEmpty(item.LoaiChungTu) && !string.IsNullOrEmpty(item.TenChungTu)) ? new Company.KDT.SHARE.Components.AdditionalDocument()
                    {
                        Type = item.LoaiChungTu,
                        Reference = item.SoChungTu,
                        Name = item.TenChungTu,
                        Issue = item.NgayPhatHanh.ToString(sfmtDate)
                    } : null,
                    Examination = new Examination()
                    {
                        Place = item.DiaDiemKiemTra
                    },
                    Examiner = new NameBase()
                    {
                        Name = item.TenCoQuanKT,
                        Identity = item.MaCoQuanKT,
                        //Result = item.KetQuaKT
                    }

                };
                //registration.GoodsItem.Add(itemGiayKT);
            }
            return registration;
        }

        private static CertificateOfInspection CertificateOfInspectionFrom(ChungThuGiamDinh GiamDinh)
        {
            GiamDinh.LoadHang();
            CertificateOfInspection CTGiamDinh = new CertificateOfInspection()
            {
                Examination = new Examination()
                {
                    Place = GiamDinh.DiaDiem,
                    Examiner = new NameBase { Name = GiamDinh.TenCoQuanGD, Identity = GiamDinh.MaCoQuanGD },
                    ExaminePerson = new NameBase { Name = GiamDinh.CanBoGD },
                    Result = new Result { content = GiamDinh.NoiDung, resultOfExam = GiamDinh.KetQua },

                },
                AdditionalInformation = new AdditionalInformation { Content = new Content { Text = GiamDinh.ThongTinKhac } },
                //GoodsItem = new List<GoodsItem>()
            };
            foreach (HangGiamDinh item in GiamDinh.ListHang)
            {
                GoodsItem hang = new GoodsItem
                {
                    Commodity = new Commodity
                    {
                        Description = item.TenHang,
                        Identification = item.MaPhu,
                        TariffClassification = item.MaHS.Trim()
                    },
                    Origin = new Origin { OriginCountry = item.NuocXX_ID.Trim() },
                    GoodsMeasure = new GoodsMeasure
                    {
                        Quantity = Helpers.FormatNumeric(item.SoLuong, 4),
                        MeasureUnit = VNACCS_Mapper.GetCodeVNACC(item.DVT_ID)
                    },
                    BillOfLading = new Company.KDT.SHARE.Components.BillOfLading
                    {
                        Reference = item.SoVanTaiDon,
                        Issue = item.NgayVanDon.ToString(sfmtDate)
                    },
                    EquipmentIdentification = new EquipmentIdentification
                    {
                        identification = item.SoHieuContainer,
                        description = item.TinhTrangContainer ? "1" : "0"
                    },
                    AdditionalInformation = new AdditionalInformation
                    {
                        Content = new Content { Text = item.GhiChu }
                    }

                };
                //CTGiamDinh.GoodsItem.Add(hang);
            }
            return CTGiamDinh;
        }


        #endregion

        #region Bổ sung chứng từ
        public static BoSungChungTu ToDataTransferBoSung(ToKhaiMauDich tkmd, object NoiDungBoSung
            /*Company.KDT.SHARE.QuanLyChungTu.CO co,
            Company.KDT.SHARE.QuanLyChungTu.ChungTuKem fileinChungtuDinhKem,
            Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep,
            Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong,
            Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon,
            Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCK*/
            )
        {

            bool isToKhaiNhap = tkmd.MaLoaiHinh.Substring(0, 1).Equals("N");

            BoSungChungTu boSungChungTuDto = new BoSungChungTu()
            {
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = string.Empty,
                Acceptance = string.Empty,
                Function = DeclarationFunction.KHAI_BAO,
                DeclarationOffice = tkmd.MaHaiQuan.Trim()
            };
            boSungChungTuDto.Agents = AgentsFrom(tkmd);
            boSungChungTuDto.Importer = new NameBase()
            {
                Identity = tkmd.MaDoanhNghiep,
                Name = tkmd.TenDoanhNghiep
            };
            boSungChungTuDto.DeclarationDocument = DeclarationDocument(tkmd);

            if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.CO))
            #region CO
            {
                Company.KDT.SHARE.QuanLyChungTu.CO co = (Company.KDT.SHARE.QuanLyChungTu.CO)NoiDungBoSung;
                boSungChungTuDto.Importer = new NameBase()
                {
                    Identity = co.MaDoanhNghiep,
                    Name = co.TenDiaChiNguoiNK
                };
                boSungChungTuDto.Issuer = AdditionalDocumentType.EXPORT_CO_FORM_D;
                boSungChungTuDto.Reference = co.GuidStr;

                boSungChungTuDto.CertificateOfOrigins = new List<Company.KDT.SHARE.Components.CertificateOfOrigin>();
                Company.KDT.SHARE.Components.CertificateOfOrigin certificateOfOrigin = CertificateOfOriginFrom(co, tkmd, isToKhaiNhap);
                boSungChungTuDto.CertificateOfOrigins.Add(certificateOfOrigin);
            }
            #endregion

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.GiayPhep))
            #region Giấy phép
            {

                Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep = (Company.KDT.SHARE.QuanLyChungTu.GiayPhep)NoiDungBoSung;
                boSungChungTuDto.Issuer = isToKhaiNhap ? AdditionalDocumentType.IMPORT_LICENCE : AdditionalDocumentType.EXPORT_LICENCE;
                boSungChungTuDto.Reference = giayphep.GuidStr;

                boSungChungTuDto.Importer = new NameBase()
                {
                    Identity = giayphep.MaDonViDuocCap,
                    Name = giayphep.TenDonViDuocCap
                };
                boSungChungTuDto.Licenses = new List<Company.KDT.SHARE.Components.License>();
                Company.KDT.SHARE.Components.License license = LicenseFrom(giayphep);
                boSungChungTuDto.Licenses.Add(license);
            }
            #endregion Giấy phép

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai))
            #region Hợp đồng thương mại
            {
                Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong = (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.HOP_DONG;
                boSungChungTuDto.Reference = hopdong.GuidStr;

                boSungChungTuDto.ContractDocuments = new List<Company.KDT.SHARE.Components.ContractDocument>();
                Company.KDT.SHARE.Components.ContractDocument contract = ContractFrom(hopdong);
                boSungChungTuDto.ContractDocuments.Add(contract);
            }
            #endregion Hợp đồng thương mại

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai))
            #region Hóa đơn thương mại
            {
                Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon = (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.HOA_DON_THUONG_MAI;
                boSungChungTuDto.Reference = hoadon.GuidStr;
                boSungChungTuDto.CommercialInvoices = new List<Company.KDT.SHARE.Components.CommercialInvoice>();
                Company.KDT.SHARE.Components.CommercialInvoice commercialInvoice = CommercialInvoiceFrom(hoadon);
                boSungChungTuDto.CommercialInvoices.Add(commercialInvoice);
            }
            #endregion Hóa đơn thương mại

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau))
            #region Đề nghị chuyển cửa khẩu
            {
                Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau dnChuyenCK = (Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.DE_NGHI_CHUYEN_CUA_KHAU;
                boSungChungTuDto.Reference = dnChuyenCK.GuidStr;
                boSungChungTuDto.NatureOfTransaction = tkmd.MaLoaiHinh.Trim();
                boSungChungTuDto.CustomsOfficeChangedRequest = CustomsOfficeChangedRequestFrom(dnChuyenCK);
                boSungChungTuDto.CustomsReference = tkmd.SoToKhai.ToString();
                boSungChungTuDto.Acceptance = tkmd.NgayTiepNhan.ToString(sfmtDate);
                //boSungChungTuDto.DeclarationDocument = new DeclarationDocument()
                //{
                //    Reference = Helpers.Format(tkmd.SoToKhai, 0),
                //    Issue = tkmd.NgayDangKy.ToString(sfmtDate),
                //    NatureOfTransaction = tkmd.MaLoaiHinh.Trim(),
                //    DeclarationOffice = tkmd.MaHaiQuan.Trim()
                //};
                boSungChungTuDto.Agents.RemoveAt(1);
            }
            #endregion Đề nghị chuyển cửa khẩu

            else if (NoiDungBoSung.GetType() == typeof(Company.KDT.SHARE.QuanLyChungTu.ChungTuKem))
            #region Chứng từ kèm
            {
                Company.KDT.SHARE.QuanLyChungTu.ChungTuKem chungtukem = (Company.KDT.SHARE.QuanLyChungTu.ChungTuKem)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.CHUNG_TU_DANG_ANH;
                boSungChungTuDto.Reference = chungtukem.GUIDSTR;

                boSungChungTuDto.AttachDocuments = new List<AttachDocumentItem>();
                AttachDocumentItem attachDocumentItem = AttachDocumentItemFrom(tkmd, chungtukem);
                boSungChungTuDto.AttachDocuments.Add(attachDocumentItem);
            }
            #endregion Chứng từ kèm

            else if (NoiDungBoSung.GetType() == typeof(GiayNopTien))
            #region Giấy nộp tiền
            {
                Company.KDT.SHARE.QuanLyChungTu.GiayNopTien giayNopTien = (GiayNopTien)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.GIAY_NOP_TIEN;
                boSungChungTuDto.Reference = giayNopTien.GuidStr;
                Receipt receipt = ReceiptFrom(giayNopTien);
                boSungChungTuDto.Receipt = receipt;
            }
            #endregion

            else if (NoiDungBoSung.GetType() == typeof(GiayKiemTra))
            {
                #region Giấy kiểm tra
                GiayKiemTra giayKT = (GiayKiemTra)NoiDungBoSung;
                boSungChungTuDto.Reference = giayKT.GuidStr;
                if (giayKT.LoaiGiay == LoaiGiayKiemTra.DANG_KY_KIEM_TRA)
                {
                    boSungChungTuDto.Issuer = AdditionalDocumentType.GIAY_DANG_KY_KT;
                    boSungChungTuDto.ExaminationRegistration = ExamRegistration(giayKT);
                }
                else if (giayKT.LoaiGiay == LoaiGiayKiemTra.KET_QUA_KIEM_TRA)
                {
                    boSungChungTuDto.Issuer = AdditionalDocumentType.GIAY_KET_QUA_KT;
                    boSungChungTuDto.ExaminationResult = ExaminationResult(giayKT);
                }
                #endregion
            }
            else if (NoiDungBoSung.GetType() == typeof(ChungThuGiamDinh))
            {
                #region Chứng thư giám định
                ChungThuGiamDinh chungthuGD = (ChungThuGiamDinh)NoiDungBoSung;
                boSungChungTuDto.Issuer = AdditionalDocumentType.CHUNG_THU_GIAM_DINH;
                boSungChungTuDto.Reference = chungthuGD.GuidStr;
                boSungChungTuDto.NatureOfTransaction = tkmd.MaLoaiHinh.Trim();
                boSungChungTuDto.CertificateOfInspection = CertificateOfInspectionFrom(chungthuGD);
                boSungChungTuDto.CustomsReference = tkmd.SoToKhai.ToString();
                boSungChungTuDto.Acceptance = tkmd.NgayTiepNhan.ToString(sfmtDate);
                #endregion
            }
            return boSungChungTuDto;
        }

        public static Company.KDT.SHARE.Components.Container_VNACCS ToDataTransferContainer(KDT_ContainerDangKy ContDK, KDT_VNACC_ToKhaiMauDich TKMD, string TenDoanhNghiep, bool KVGS)
        {
            Company.KDT.SHARE.Components.Container_VNACCS Cont = new Company.KDT.SHARE.Components.Container_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = ContDK.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDate),
                IssueLocation = string.Empty,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = ContDK.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            Cont.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = ContDK.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });

            if (KVGS)
            {
                Cont.Issuer = DeclarationIssuer.Container_KVGS;
                Cont.CustomsReference = TKMD.SoToKhai.ToString();
                Cont.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
                Cont.DeclarationOffice = TKMD.CoQuanHaiQuan.Trim();
                Cont.NatureOfTransaction = TKMD.MaLoaiHinh;
            }
            else
            {
                Cont.Issuer = DeclarationIssuer.Container;
                Cont.DeclarationOffice = ContDK.MaHQ.Trim();
                Cont.CustomsReference = string.Empty;
                Cont.Acceptance = string.Empty;

                ////Phiph
                //Cont.DeclarationDocument = new DeclarationDocument()
                //{
                //    Reference = TKMD.SoToKhai.ToString(),
                //    Issue = TKMD.NgayDangKy.ToString(sfmtDate),
                //    NatureOfTransaction = TKMD.MaLoaiHinh,
                //    DeclarationOffice = TKMD.CoQuanHaiQuan
                //};
                //TransportEquipments transportEquipments = new TransportEquipments()
                //{
                //    TransportEquipment = new List<TransportEquipment>()
                //};
                //foreach (KDT_ContainerBS item in ContDK.ListCont)
                //{
                //    transportEquipments.TransportEquipment.Add(new TransportEquipment
                //    {
                //        BillOfLading = item.SoVanDon,
                //        Container = item.SoContainer,
                //        Seal = item.SoSeal,
                //        Content = item.GhiChu
                //    });
                //}
                //Cont.TransportEquipments = transportEquipments;
                ////Phiph

            }
            //minhnd 21/03/2015
            Cont.DeclarationDocument = new Company.KDT.SHARE.Components.Messages.SXXK.Container_DeclarationDocument()
            {
                Reference = TKMD.SoToKhai.ToString(),
                Issue = TKMD.NgayDangKy.ToString(sfmtDate),
                NatureOfTransaction = TKMD.MaLoaiHinh,
                DeclarationOffice = TKMD.CoQuanHaiQuan
            };
            TransportEquipments transportEquipments = new TransportEquipments()
            {
                TransportEquipment = new List<TransportEquipment>()
            };
            foreach (KDT_ContainerBS item in ContDK.ListCont)
            {
                transportEquipments.TransportEquipment.Add(new TransportEquipment
                {
                    BillOfLading = item.SoVanDon,
                    Container = item.SoContainer,
                    Seal = item.SoSeal,
                    Content = item.GhiChu
                });
            }
            Cont.TransportEquipments = transportEquipments;
            //minhnd 21/03/2015
            return Cont;
        }

        #endregion


        public static CertificateOfOrigins_VNACCS ToDataTransferCertificateOfOrigin(KDT_VNACCS_CertificateOfOrigin certificateOfOrigin, KDT_VNACCS_CertificateOfOrigin_Detail certificateOfOriginDetail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            CertificateOfOrigins_VNACCS certificateOfOrigins_VNACCS = new CertificateOfOrigins_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = certificateOfOrigin.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = certificateOfOrigin.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            certificateOfOrigins_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = certificateOfOrigin.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                certificateOfOrigins_VNACCS.Function = DeclarationFunction.SUA;
                certificateOfOrigins_VNACCS.CustomsReference = certificateOfOrigin.SoTN.ToString();
                certificateOfOrigins_VNACCS.Acceptance = certificateOfOrigin.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                certificateOfOrigins_VNACCS.Function = DeclarationFunction.HUY;
                certificateOfOrigins_VNACCS.CustomsReference = certificateOfOrigin.SoTN.ToString();
                certificateOfOrigins_VNACCS.Acceptance = certificateOfOrigin.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                certificateOfOrigins_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                certificateOfOrigins_VNACCS.CustomsReference = string.Empty;
                certificateOfOrigins_VNACCS.Acceptance = string.Empty;
            }
            certificateOfOrigins_VNACCS.Issuer = DeclarationIssuer.CertificateOfOrigin;
            certificateOfOrigins_VNACCS.DeclarationOffice = certificateOfOrigin.MaHQ.Trim();
            //certificateOfOrigins_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
            //certificateOfOrigins_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);

            if (FormType == "TKMD")
            {
                certificateOfOrigins_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                certificateOfOrigins_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }
            CertificateOfOriginsNew certificateOfOrigins = new CertificateOfOriginsNew()
            {
                CertificateOfOriginNew = new List<CertificateOfOriginNew>()
            };
            foreach (KDT_VNACCS_CertificateOfOrigin_Detail item in certificateOfOrigin.CertificateOfOriginCollection)
            {
                certificateOfOrigins.CertificateOfOriginNew.Add(new CertificateOfOriginNew
                {
                    Reference = item.SoCO,
                    Type = item.LoaiCO,
                    Issuer = item.ToChucCapCO,
                    Issue = item.NgayCapCO.ToString(sfmtDate),
                    IssueLocation = item.NuocCapCO,
                    Representative = item.NguoiCapCO,
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                    {
                        FileName = certificateOfOrigin.FileName,
                        Content = certificateOfOrigin.Content
                    }
                });
            }
            certificateOfOrigins_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument()
            {
                Content = certificateOfOrigin.GhiChuKhac
            };
            certificateOfOrigins_VNACCS.CertificateOfOriginsNew = certificateOfOrigins;
            return certificateOfOrigins_VNACCS;
        }
        public static CapSoDinhDanh_VNACCS ToDataTransferCapSoDinhDanh(KDT_VNACCS_CapSoDinhDanh capSoDinhDanh, string TenDoanhNghiep)
        {
            CapSoDinhDanh_VNACCS CapSoDinhDanh_VNACCS = new CapSoDinhDanh_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = capSoDinhDanh.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = capSoDinhDanh.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
                RequestType = new IssueBase()
                {
                    Type = capSoDinhDanh.LoaiDoiTuong.ToString(),
                },
                TransportEquipmentType = new IssueBase()
                {
                    Type = capSoDinhDanh.LoaiTTHH.ToString(),
                },
                CustomsImporter = new NameBase()
                {
                    Identity = capSoDinhDanh.MaDoanhNghiep,
                }
            };
            // Người Khai Hải Quan
            CapSoDinhDanh_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = capSoDinhDanh.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            CapSoDinhDanh_VNACCS.Issuer = DeclarationIssuer.DinhDanh;
            CapSoDinhDanh_VNACCS.DeclarationOffice = capSoDinhDanh.MaHQ.Trim();
            CapSoDinhDanh_VNACCS.CustomsReference = capSoDinhDanh.SoTiepNhan.ToString();
            CapSoDinhDanh_VNACCS.Acceptance = capSoDinhDanh.NgayTiepNhan.ToString(sfmtDateTime);

            return CapSoDinhDanh_VNACCS;
        }

        public static Licenses_VNACCS ToDataTransferLicense(KDT_VNACCS_License license, KDT_VNACCS_License_Detail licenseDetail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            Licenses_VNACCS Licenses_VNACCS = new Licenses_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = license.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = license.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            Licenses_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = license.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                Licenses_VNACCS.Function = DeclarationFunction.SUA;
                Licenses_VNACCS.CustomsReference = license.SoTN.ToString();
                Licenses_VNACCS.Acceptance = license.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                Licenses_VNACCS.Function = DeclarationFunction.HUY;
                Licenses_VNACCS.CustomsReference = license.SoTN.ToString();
                Licenses_VNACCS.Acceptance = license.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                Licenses_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                Licenses_VNACCS.CustomsReference = license.SoTN.ToString();
                Licenses_VNACCS.Acceptance = license.NgayTN.ToString(sfmtDateTime);
            }
            Licenses_VNACCS.Issuer = DeclarationIssuer.License;
            Licenses_VNACCS.DeclarationOffice = license.MaHQ.Trim();
            //Licenses_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
            //Licenses_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
            if (FormType == "TKMD")
            {
                Licenses_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                Licenses_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }

            Licenses licenses = new Licenses()
            {
                License = new List<Company.KDT.SHARE.Components.Messages.Vouchers.License>()
            };
            foreach (KDT_VNACCS_License_Detail item in license.LicenseCollection)
            {
                licenses.License.Add(new Company.KDT.SHARE.Components.Messages.Vouchers.License
                {
                    Issuer = item.NguoiCapGP,
                    Reference = item.SoGP,
                    Issue = item.NgayCapGP.ToString(sfmtDate),
                    IssueLocation = item.NoiCapGP,
                    Category = item.LoaiGP,
                    Expire = item.NgayHetHanGP.ToString(sfmtDate),
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                    {
                        FileName = license.FileName,
                        Content = license.Content
                    }
                });
            }
            Licenses_VNACCS.AdditionalInformation = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = license.GhiChuKhac
            };

            Licenses_VNACCS.Licenses = licenses;
            return Licenses_VNACCS;
        }
        public static Contract_VNACCS ToDataTransferContract(KDT_VNACCS_ContractDocument contractDocument, KDT_VNACCS_ContractDocument_Detail contractDocumentDetail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            Contract_VNACCS Contract_VNACCS = new Contract_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = contractDocument.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = contractDocument.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            Contract_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = contractDocument.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                Contract_VNACCS.Function = DeclarationFunction.SUA;
                Contract_VNACCS.CustomsReference = contractDocument.SoTN.ToString();
                Contract_VNACCS.Acceptance = contractDocument.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                Contract_VNACCS.Function = DeclarationFunction.HUY;
                Contract_VNACCS.CustomsReference = contractDocument.SoTN.ToString();
                Contract_VNACCS.Acceptance = contractDocument.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                Contract_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                Contract_VNACCS.CustomsReference = string.Empty;
                Contract_VNACCS.Acceptance = string.Empty;
            }
            Contract_VNACCS.Issuer = DeclarationIssuer.ContractDocument;
            Contract_VNACCS.DeclarationOffice = contractDocument.MaHQ.Trim();
            //Contract_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
            //Contract_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
            if (FormType == "TKMD")
            {
                Contract_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                Contract_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }
            Contract_VNACCS.DeclarationDocument = new DeclarationDocument()
            {
                Reference = TKMD.SoToKhai.ToString(),
                Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                NatureOfTransaction = TKMD.MaLoaiHinh,
                DeclarationOffice = TKMD.CoQuanHaiQuan
            };
            ContractDocuments contractDocuments = new ContractDocuments()
            {
                ContractDocument = new List<Company.KDT.SHARE.Components.Messages.Vouchers.ContractDocument>()
            };

            foreach (KDT_VNACCS_ContractDocument_Detail item in contractDocument.ContractDocumentCollection)
            {
                contractDocuments.ContractDocument.Add(new Company.KDT.SHARE.Components.Messages.Vouchers.ContractDocument
                {
                    Reference = item.SoHopDong,
                    Issue = item.NgayHopDong.ToString(sfmtDate),
                    Expire = item.ThoiHanThanhToan.ToString(sfmtDate),
                    TotalValue = item.TongTriGia,
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                    {
                        FileName = contractDocument.FileName,
                        Content = contractDocument.Content
                    }
                });
            }
            Contract_VNACCS.AdditionalInformation = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = contractDocument.GhiChuKhac
            };

            Contract_VNACCS.ContractDocuments = contractDocuments;
            return Contract_VNACCS;
        }
        public static CommercialInvoice_VNACCS ToDataTransferCommercialInvoice(KDT_VNACCS_CommercialInvoice commercialInvoice, KDT_VNACCS_CommercialInvoice_Detail commercialInvoiceDetail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            CommercialInvoice_VNACCS CommercialInvoice_VNACCS = new CommercialInvoice_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = commercialInvoice.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = commercialInvoice.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            CommercialInvoice_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = commercialInvoice.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                CommercialInvoice_VNACCS.Function = DeclarationFunction.SUA;
                CommercialInvoice_VNACCS.CustomsReference = commercialInvoice.SoTN.ToString();
                CommercialInvoice_VNACCS.Acceptance = commercialInvoice.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                CommercialInvoice_VNACCS.Function = DeclarationFunction.HUY;
                CommercialInvoice_VNACCS.CustomsReference = commercialInvoice.SoTN.ToString();
                CommercialInvoice_VNACCS.Acceptance = commercialInvoice.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                CommercialInvoice_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                CommercialInvoice_VNACCS.CustomsReference = string.Empty;
                CommercialInvoice_VNACCS.Acceptance = string.Empty;
            }
            CommercialInvoice_VNACCS.Issuer = DeclarationIssuer.CommercialInvoice;
            CommercialInvoice_VNACCS.DeclarationOffice = commercialInvoice.MaHQ.Trim();
            //CommercialInvoice_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
            //CommercialInvoice_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
            if (FormType == "TKMD")
            {
                CommercialInvoice_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                CommercialInvoice_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }
            CommercialInvoices commercialInvoices = new CommercialInvoices()
            {
                CommercialInvoice = new List<Company.KDT.SHARE.Components.Messages.Vouchers.CommercialInvoice>()
            };
            foreach (KDT_VNACCS_CommercialInvoice_Detail item in commercialInvoice.CommercialInvoiceCollection)
            {
                commercialInvoices.CommercialInvoice.Add(new Company.KDT.SHARE.Components.Messages.Vouchers.CommercialInvoice
                {
                    Reference = item.SoHoaDonTM,
                    Issue = item.NgayPhatHanhHDTM.ToString(sfmtDate),
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                    {
                        FileName = commercialInvoice.FileName,
                        Content = commercialInvoice.Content
                    }
                });
            }
            CommercialInvoice_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = commercialInvoice.GhiChuKhac
            };

            CommercialInvoice_VNACCS.CommercialInvoices = commercialInvoices;
            return CommercialInvoice_VNACCS;
        }
        public static BillOfLading_VNACCS ToDataTransferBillOfLading(KDT_VNACCS_BillOfLading billOfLading, KDT_VNACCS_BillOfLading_Detail billOfLadingDetail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            BillOfLading_VNACCS BillOfLading_VNACCS = new BillOfLading_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = billOfLading.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = billOfLading.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            BillOfLading_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = billOfLading.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                BillOfLading_VNACCS.Function = DeclarationFunction.SUA;
                BillOfLading_VNACCS.CustomsReference = billOfLading.SoTN.ToString();
                BillOfLading_VNACCS.Acceptance = billOfLading.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                BillOfLading_VNACCS.Function = DeclarationFunction.HUY;
                BillOfLading_VNACCS.CustomsReference = billOfLading.SoTN.ToString();
                BillOfLading_VNACCS.Acceptance = billOfLading.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                BillOfLading_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                BillOfLading_VNACCS.CustomsReference = string.Empty;
                BillOfLading_VNACCS.Acceptance = string.Empty;
            }
            BillOfLading_VNACCS.Issuer = DeclarationIssuer.BillOfLading;
            BillOfLading_VNACCS.DeclarationOffice = billOfLading.MaHQ.Trim();
            //BillOfLading_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
            //BillOfLading_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
            if (FormType == "TKMD")
            {
                BillOfLading_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                BillOfLading_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }
            BillOfLadings billOfLadings = new BillOfLadings()
            {
                BillOfLading = new List<Company.KDT.SHARE.Components.BillOfLading>()
            };
            foreach (KDT_VNACCS_BillOfLading_Detail item in billOfLading.BillOfLadingCollection)
            {
                billOfLadings.BillOfLading.Add(new Company.KDT.SHARE.Components.BillOfLading
                {
                    Reference = item.SoVanDon,
                    Issue = item.NgayVanDon.ToString(sfmtDate),
                    IssueLocation = item.NuocPhatHanh,
                    TransitLocation = item.DiaDiemCTQC,
                    Category = item.LoaiVanDon.ToString(),
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                    {
                        FileName = billOfLading.FileName,
                        Content = billOfLading.Content
                    }
                });
            }
            BillOfLading_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = billOfLading.GhiChuKhac
            };
            BillOfLading_VNACCS.BillOfLadings = billOfLadings;
            return BillOfLading_VNACCS;
        }
        public static Company.KDT.SHARE.Components.Messages.Vouchers.Container_VNACCS ToDataTransferContainer(KDT_VNACCS_Container_Detail container, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            Company.KDT.SHARE.Components.Messages.Vouchers.Container_VNACCS Container_VNACCS = new Company.KDT.SHARE.Components.Messages.Vouchers.Container_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = container.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = container.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            Container_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = container.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                Container_VNACCS.Function = DeclarationFunction.SUA;
                Container_VNACCS.CustomsReference = container.SoTN.ToString();
                Container_VNACCS.Acceptance = container.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                Container_VNACCS.Function = DeclarationFunction.HUY;
                Container_VNACCS.CustomsReference = container.SoTN.ToString();
                Container_VNACCS.Acceptance = container.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                Container_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                Container_VNACCS.CustomsReference = string.Empty;
                Container_VNACCS.Acceptance = string.Empty;
            }
            Container_VNACCS.Issuer = DeclarationIssuer.Containers;
            Container_VNACCS.DeclarationOffice = container.MaHQ.Trim();
            //Container_VNACCS.CustomsReference = string.Empty;
            //Container_VNACCS.Acceptance = DateTime.Now.ToString(sfmtDateTime);

            if (FormType == "TKMD")
            {
                Container_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                Container_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }
            Container_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
            {
                Content = container.GhiChuKhac
            };
            Container_VNACCS.AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
            {
                FileName = container.FileName,
                Content = container.Content
            };
            return Container_VNACCS;
        }
        public static AdditionalDocument_VNACCS ToDataTransferAdditionalDocument(KDT_VNACCS_AdditionalDocument additionalDocument, KDT_VNACCS_AdditionalDocument_Detail additionalDocument_Detail, KDT_VNACC_ToKhaiMauDich TKMD, KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung, string TenDoanhNghiep, string Status, string FormType)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            AdditionalDocument_VNACCS AdditionalDocument_VNACCS = new AdditionalDocument_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = additionalDocument.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = additionalDocument.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            AdditionalDocument_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = additionalDocument.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                AdditionalDocument_VNACCS.Function = DeclarationFunction.SUA;
                AdditionalDocument_VNACCS.CustomsReference = additionalDocument.SoTN.ToString();
                AdditionalDocument_VNACCS.Acceptance = additionalDocument.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                AdditionalDocument_VNACCS.Function = DeclarationFunction.HUY;
                AdditionalDocument_VNACCS.CustomsReference = additionalDocument.SoTN.ToString();
                AdditionalDocument_VNACCS.Acceptance = additionalDocument.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                AdditionalDocument_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                AdditionalDocument_VNACCS.CustomsReference = string.Empty;
                AdditionalDocument_VNACCS.Acceptance = string.Empty;
            }
            AdditionalDocument_VNACCS.Issuer = DeclarationIssuer.AdditionalDocument;
            AdditionalDocument_VNACCS.DeclarationOffice = additionalDocument.MaHQ.Trim();
            if (FormType == "TKMD")
            {
                AdditionalDocument_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Issue = TKMD.NgayDangKy.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKMD.NgayDangKy.ToString(sfmtDate),
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    DeclarationOffice = TKMD.CoQuanHaiQuan
                };
            }
            else
            {
                AdditionalDocument_VNACCS.DeclarationDocument = new DeclarationDocument()
                {
                    Reference = TKBoSung.SoToKhaiBoSung.ToString(),
                    Issue = TKBoSung.NgayKhaiBao.Year == 1900 ? DateTime.Now.ToString(sfmtDate) : TKBoSung.NgayKhaiBao.ToString(sfmtDate),
                    NatureOfTransaction = TKBoSung.MaLoaiHinh,
                    DeclarationOffice = TKBoSung.CoQuanHaiQuan
                };
            }

            AdditionalDocuments additionalDocuments = new AdditionalDocuments()
            {
                AdditionalDocument = new List<AdditionalDocument>()
            };
            foreach (KDT_VNACCS_AdditionalDocument_Detail item in additionalDocument.AdditionalDocumentCollection)
            {
                additionalDocuments.AdditionalDocument.Add(new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Reference = item.SoChungTu,
                    Description = item.TenChungTu,
                    Issue = item.NgayPhatHanh.ToString(sfmtDate),
                    IssueLocation = item.NoiPhatHanh,
                    Type = additionalDocument.LoaiChungTu.ToString(),
                    AdditionalDocumentSub = new AdditionalDocument()
                    {
                        Content = additionalDocument.GhiChuKhac
                    },
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                    {
                        FileName = additionalDocument.FileName,
                        Content = additionalDocument.Content
                    }
                });
            }
            //AdditionalDocument_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
            //{
            //    Content = additionalDocument.GhiChuKhac
            //};
            AdditionalDocument_VNACCS.AdditionalDocuments = additionalDocuments;
            return AdditionalDocument_VNACCS;
        }
        public static Overtime_VNACCS ToDataTransferOvertime(KDT_VNACCS_OverTime overTime, KDT_VNACC_ToKhaiMauDich TKMD, string TenDoanhNghiep, string Status)
        {
            if (Status == "Send")
            {
                Overtime_VNACCS Overtime_VNACCS = new Overtime_VNACCS()
                {
                    Function = DeclarationFunction.KHAI_BAO,
                    Reference = overTime.GuidStr,
                    Issue = DateTime.Now.ToString(sfmtDateTime),
                    IssueLocation = string.Empty,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                    Importer = new NameBase()
                    {
                        Identity = overTime.MaDoanhNghiep,
                        Name = TenDoanhNghiep
                    },
                    Agents = new List<Agent>(),
                };
                // Người Khai Hải Quan
                Overtime_VNACCS.Agents.Add(new Agent
                {
                    Name = TenDoanhNghiep,
                    Identity = overTime.MaDoanhNghiep,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN
                });
                Overtime_VNACCS.Issuer = DeclarationIssuer.OverTime;
                Overtime_VNACCS.DeclarationOffice = overTime.MaHQ.Trim();
                //Overtime_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
                //Overtime_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
                Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Issue = overTime.NgayDangKy.ToString(sfmtDate),
                    Hour = overTime.GioLamThuTuc.ToString(),
                    Content = overTime.NoiDung,
                };
                return Overtime_VNACCS;
            }
            else if (Status == "Edit")
            {
                Overtime_VNACCS Overtime_VNACCS = new Overtime_VNACCS()
                {
                    Function = DeclarationFunction.SUA,
                    Reference = overTime.GuidStr,
                    Issue = DateTime.Now.ToString(sfmtDate),
                    IssueLocation = string.Empty,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                    Importer = new NameBase()
                    {
                        Identity = overTime.MaDoanhNghiep,
                        Name = TenDoanhNghiep
                    },
                    Agents = new List<Agent>(),
                };
                // Người Khai Hải Quan
                Overtime_VNACCS.Agents.Add(new Agent
                {
                    Name = TenDoanhNghiep,
                    Identity = overTime.MaDoanhNghiep,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN
                });
                Overtime_VNACCS.Issuer = DeclarationIssuer.OverTime;
                Overtime_VNACCS.DeclarationOffice = overTime.MaHQ.Trim();
                Overtime_VNACCS.CustomsReference = overTime.SoTN.ToString();
                Overtime_VNACCS.Acceptance = overTime.NgayTN.ToString(sfmtDateTime);

                Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Issue = overTime.NgayDangKy.ToString(sfmtDate),
                    Hour = overTime.GioLamThuTuc.ToString(),
                    Content = overTime.NoiDung,
                };
                return Overtime_VNACCS;
            }
            else
            {
                Overtime_VNACCS Overtime_VNACCS = new Overtime_VNACCS()
                {
                    Function = DeclarationFunction.HUY,
                    Reference = overTime.GuidStr,
                    Issue = DateTime.Now.ToString(sfmtDate),
                    IssueLocation = string.Empty,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                    Importer = new NameBase()
                    {
                        Identity = overTime.MaDoanhNghiep,
                        Name = TenDoanhNghiep
                    },
                    Agents = new List<Agent>(),
                };
                // Người Khai Hải Quan
                Overtime_VNACCS.Agents.Add(new Agent
                {
                    Name = TenDoanhNghiep,
                    Identity = overTime.MaDoanhNghiep,
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN
                });
                Overtime_VNACCS.Issuer = DeclarationIssuer.OverTime;
                Overtime_VNACCS.DeclarationOffice = overTime.MaHQ.Trim();
                Overtime_VNACCS.CustomsReference = overTime.SoTN.ToString();
                Overtime_VNACCS.Acceptance = overTime.NgayTN.ToString(sfmtDateTime);
                Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Content = overTime.NoiDung,
                };
                Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Issue = overTime.NgayDangKy.ToString(sfmtDate),
                    Hour = overTime.GioLamThuTuc.ToString(),
                    Content = overTime.NoiDung,
                };
                return Overtime_VNACCS;
            }
        }
        public static AdditionalDocument_VNACCS ToDataTransferAdditionalDocument(KDT_VNACCS_AdditionalDocument additionalDocument, KDT_VNACCS_AdditionalDocument_Detail additionalDocument_Detail, KDT_VNACC_ToKhaiMauDich TKMD, string TenDoanhNghiep, string Status)
        {
            bool IsEdit = true ? Status == "Edit" : Status == "Send";
            bool IsCancel = true ? Status == "Cancel" : Status == "Send";

            AdditionalDocument_VNACCS AdditionalDocument_VNACCS = new AdditionalDocument_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = additionalDocument.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = additionalDocument.MaDoanhNghiep,
                    Name = TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            AdditionalDocument_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = additionalDocument.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            if (IsEdit)
            {
                AdditionalDocument_VNACCS.Function = DeclarationFunction.SUA;
                AdditionalDocument_VNACCS.CustomsReference = additionalDocument.SoTN.ToString();
                AdditionalDocument_VNACCS.Acceptance = additionalDocument.NgayTN.ToString(sfmtDateTime);
            }
            else if (IsCancel)
            {
                AdditionalDocument_VNACCS.Function = DeclarationFunction.HUY;
                AdditionalDocument_VNACCS.CustomsReference = additionalDocument.SoTN.ToString();
                AdditionalDocument_VNACCS.Acceptance = additionalDocument.NgayTN.ToString(sfmtDateTime);
            }
            else
            {
                AdditionalDocument_VNACCS.Function = DeclarationFunction.KHAI_BAO;
                AdditionalDocument_VNACCS.CustomsReference = string.Empty;
                AdditionalDocument_VNACCS.Acceptance = string.Empty;
            }
            AdditionalDocument_VNACCS.Issuer = DeclarationIssuer.AdditionalDocument;
            AdditionalDocument_VNACCS.DeclarationOffice = additionalDocument.MaHQ.Trim();
            AdditionalDocument_VNACCS.DeclarationDocument = new DeclarationDocument()
            {
                Reference = TKMD.SoToKhai.ToString(),
                Issue = TKMD.NgayDangKy.ToString(sfmtDate),
                NatureOfTransaction = TKMD.MaLoaiHinh,
                DeclarationOffice = TKMD.CoQuanHaiQuan
            };
            AdditionalDocuments additionalDocuments = new AdditionalDocuments()
            {
                AdditionalDocument = new List<AdditionalDocument>()
            };
            foreach (KDT_VNACCS_AdditionalDocument_Detail item in additionalDocument.AdditionalDocumentCollection)
            {
                additionalDocuments.AdditionalDocument.Add(new Company.KDT.SHARE.Components.AdditionalDocument
                {
                    Reference = item.SoChungTu,
                    Description = item.TenChungTu,
                    Issue = item.NgayPhatHanh.ToString(sfmtDate),
                    IssueLocation = item.NoiPhatHanh,
                    Type = additionalDocument.LoaiChungTu.ToString(),
                    AdditionalDocumentSub = new AdditionalDocument()
                    {
                        Content = additionalDocument.GhiChuKhac
                    },
                    AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                    {
                        FileName = additionalDocument.FileName,
                        Content = additionalDocument.Content
                    }
                });
            }
            //AdditionalDocument_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
            //{
            //    Content = additionalDocument.GhiChuKhac
            //};
            AdditionalDocument_VNACCS.AdditionalDocuments = additionalDocuments;
            return AdditionalDocument_VNACCS;
        }
        //public static Overtime_VNACCS ToDataTransferOvertime(KDT_VNACCS_OverTime overTime, KDT_VNACC_ToKhaiMauDich TKMD, string TenDoanhNghiep, string Status)
        //{
        //    if (Status == "Send")
        //    {
        //        Overtime_VNACCS Overtime_VNACCS = new Overtime_VNACCS()
        //        {
        //            Function = DeclarationFunction.KHAI_BAO,
        //            Reference = overTime.GuidStr,
        //            Issue = DateTime.Now.ToString(sfmtDate),
        //            IssueLocation = string.Empty,
        //            Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
        //            Importer = new NameBase()
        //            {
        //                Identity = overTime.MaDoanhNghiep,
        //                Name = TenDoanhNghiep
        //            },
        //            Agents = new List<Agent>(),
        //        };
        //        // Người Khai Hải Quan
        //        Overtime_VNACCS.Agents.Add(new Agent
        //        {
        //            Name = TenDoanhNghiep,
        //            Identity = overTime.MaDoanhNghiep,
        //            Status = AgentsStatus.NGUOIKHAI_HAIQUAN
        //        });
        //        Overtime_VNACCS.Issuer = DeclarationIssuer.OverTime;
        //        Overtime_VNACCS.DeclarationOffice = overTime.MaHQ.Trim();
        //        Overtime_VNACCS.CustomsReference = TKMD.SoToKhai.ToString();
        //        Overtime_VNACCS.Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime);
        //        Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
        //        {
        //            Issue = overTime.NgayDangKy.ToString(sfmtDate),
        //            Hour = overTime.GioLamThuTuc.ToString(),
        //            Content = overTime.NoiDung,
        //        };
        //        return Overtime_VNACCS;
        //    }
        //    else if (Status == "Edit")
        //    {
        //        Overtime_VNACCS Overtime_VNACCS = new Overtime_VNACCS()
        //        {
        //            Function = DeclarationFunction.SUA,
        //            Reference = overTime.GuidStr,
        //            Issue = DateTime.Now.ToString(sfmtDate),
        //            IssueLocation = string.Empty,
        //            Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
        //            Importer = new NameBase()
        //            {
        //                Identity = overTime.MaDoanhNghiep,
        //                Name = TenDoanhNghiep
        //            },
        //            Agents = new List<Agent>(),
        //        };
        //        // Người Khai Hải Quan
        //        Overtime_VNACCS.Agents.Add(new Agent
        //        {
        //            Name = TenDoanhNghiep,
        //            Identity = overTime.MaDoanhNghiep,
        //            Status = AgentsStatus.NGUOIKHAI_HAIQUAN
        //        });
        //        Overtime_VNACCS.Issuer = DeclarationIssuer.OverTime;
        //        Overtime_VNACCS.DeclarationOffice = overTime.MaHQ.Trim();
        //        Overtime_VNACCS.CustomsReference = overTime.SoTN.ToString();
        //        Overtime_VNACCS.Acceptance = overTime.NgayTN.ToString(sfmtDateTime);

        //        Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
        //        {
        //            Issue = overTime.NgayDangKy.ToString(sfmtDate),
        //            Hour = overTime.GioLamThuTuc.ToString(),
        //            Content = overTime.NoiDung,
        //        };
        //        return Overtime_VNACCS;
        //    }
        //    else
        //    {
        //        Overtime_VNACCS Overtime_VNACCS = new Overtime_VNACCS()
        //        {
        //            Function = DeclarationFunction.HUY,
        //            Reference = overTime.GuidStr,
        //            Issue = DateTime.Now.ToString(sfmtDate),
        //            IssueLocation = string.Empty,
        //            Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
        //            Importer = new NameBase()
        //            {
        //                Identity = overTime.MaDoanhNghiep,
        //                Name = TenDoanhNghiep
        //            },
        //            Agents = new List<Agent>(),
        //        };
        //        // Người Khai Hải Quan
        //        Overtime_VNACCS.Agents.Add(new Agent
        //        {
        //            Name = TenDoanhNghiep,
        //            Identity = overTime.MaDoanhNghiep,
        //            Status = AgentsStatus.NGUOIKHAI_HAIQUAN
        //        });
        //        Overtime_VNACCS.Issuer = DeclarationIssuer.OverTime;
        //        Overtime_VNACCS.DeclarationOffice = overTime.MaHQ.Trim();
        //        Overtime_VNACCS.CustomsReference = overTime.SoTN.ToString();
        //        Overtime_VNACCS.Acceptance = overTime.NgayTN.ToString(sfmtDateTime);
        //        Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
        //        {
        //            Content = overTime.NoiDung,
        //        };
        //        Overtime_VNACCS.AdditionalDocument = new Company.KDT.SHARE.Components.AdditionalDocument
        //        {
        //            Issue = overTime.NgayDangKy.ToString(sfmtDate),
        //            Hour = overTime.GioLamThuTuc.ToString(),
        //            Content = overTime.NoiDung,
        //        };
        //        return Overtime_VNACCS;
        //    }
        //}


        //BÁO CÁO QUYẾT TOÁN

        //public static GoodsItem_VNACCS ToDataTransferGoodsItem(KDT_VNACCS_ContractDocument contractDocument, KDT_VNACCS_ContractDocument_Detail contractDocumentDetail, KDT_VNACC_ToKhaiMauDich TKMD, string TenDoanhNghiep)
        //{
        //    GoodsItem_VNACCS GoodsItem_VNACCS = new GoodsItem_VNACCS()
        //    {
        //        Issuer = DeclarationIssuer.GoodsItem,
        //        Reference = contractDocument.GuidStr,
        //        Issue = DateTime.Now.ToString(sfmtDate),
        //        Function = DeclarationFunction.KHAI_BAO,
        //        IssueLocation = string.Empty,
        //        CustomsReference = TKMD.SoToKhai.ToString(),
        //        Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
        //        Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime),
        //        DeclarationOffice = contractDocument.MaHQ.Trim(),
        //        FinalYear = "2016",
        //        Type = "1",
        //        Unit = "2",
        //        Importer = new NameBase()
        //        {
        //            Identity = contractDocument.MaDoanhNghiep,
        //            Name = TenDoanhNghiep ,
        //            Address  = ""
        //        },
        //        Agents = new List<Agent>(),
        //    };
        //    // Người Khai Hải Quan
        //    GoodsItem_VNACCS.Agents.Add(new Agent
        //    {
        //        Name = TenDoanhNghiep,
        //        Identity = contractDocument.MaDoanhNghiep,
        //        Status = AgentsStatus.NGUOIKHAI_HAIQUAN
        //    });
        //    GoodsItems goodsItems = new GoodsItems()
        //    {
        //        GoodsItem = new List<Company.KDT.SHARE.Components.GoodsItem>()
        //    };
        //    foreach (KDT_VNACCS_ContractDocument_Detail item in contractDocument.ContractDocumentCollection)
        //    {
        //        goodsItems.GoodsItem.Add(new Company.KDT.SHARE.Components.GoodsItem
        //        {
        //            Sequence ="",
        //            Type = "" ,
        //            Account = "" ,
        //            Description = "" ,
        //            Identification = "" ,
        //            MeasureUnit = "",
        //            QuantityBegin = 0,
        //            QuantityImport = 0,
        //            QuantityExport = 0 ,
        //            QuantityExcess = 0,
        //            Content = "",
        //            //AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
        //            //{
        //            //    FileName = contractDocument.FileName,
        //            //    Content = contractDocument.Content
        //            //}
        //        });
        //    }
        //    GoodsItem_VNACCS.AdditionalInformation = new Company.KDT.SHARE.Components.AdditionalDocument
        //    {
        //        Content = contractDocument.GhiChuKhac
        //    };

        //    GoodsItem_VNACCS.GoodsItems = goodsItems;
        //    return GoodsItem_VNACCS;
        //}

        public static ContractReference_VNACCS ToDataTransferContractReference(KDT_VNACCS_ContractDocument contractDocument, KDT_VNACCS_ContractDocument_Detail contractDocumentDetail, KDT_VNACC_ToKhaiMauDich TKMD, string TenDoanhNghiep)
        {
            ContractReference_VNACCS ContractReference_VNACCS = new ContractReference_VNACCS()
            {
                Issuer = DeclarationIssuer.GoodsItem,
                Reference = contractDocument.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDate),
                Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = string.Empty,
                CustomsReference = TKMD.SoToKhai.ToString(),
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                Acceptance = TKMD.NgayDangKy.ToString(sfmtDateTime),
                DeclarationOffice = contractDocument.MaHQ.Trim(),
                Importer = new NameBase()
                {
                    Identity = contractDocument.MaDoanhNghiep,
                    Name = TenDoanhNghiep,
                    Address = ""
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            ContractReference_VNACCS.Agents.Add(new Agent
            {
                Name = TenDoanhNghiep,
                Identity = contractDocument.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            ContractReferences contractReferences = new ContractReferences()
            {
                ContractReference = new List<Company.KDT.SHARE.Components.ContractReference>()
            };
            foreach (KDT_VNACCS_ContractDocument_Detail item in contractDocument.ContractDocumentCollection)
            {
                contractReferences.ContractReference.Add(new Company.KDT.SHARE.Components.ContractReference
                {
                    Sequence = 1,
                    Reference = "",
                    Issue = "",
                    DeclarationOffice = "",
                    Expire = "",
                    AdditionalInformation = new AdditionalDocument()
                    {
                      Content =""  
                    },
                    GoodsItems =  new GoodsItems()
                    {
                        Commodity = new Commodity()
                        {
                            Sequence = "",
                            Description = "",
                            Identification = "",
                            TariffClassification = "",
                            Content = "",
                        },
                        GoodsMeasure = new GoodsMeasure()
                        {
                            QuantityTempImport = "0",
                            QuantityReExport = "0",
                        },
                        QuantityForward = new QuantityForward()
                        {
                            Quantity = 0,
                            Reference = "",
                            Issue = "",
                            DeclarationOffice = "",
                            Expire = "",
                            //QuantityExcess = 0,
                            MeasureUnit = ""
                        },
                        //AttachedFile = new Company.KDT.SHARE.Components.AttachedFile()
                        //{
                        //    FileName = contractDocument.FileName,
                        //    Content = contractDocument.Content
                        //}
                    },
                });
            }
            ContractReference_VNACCS.ContractReferences = contractReferences;
            return ContractReference_VNACCS;
        }

        public static BillOfLadingNew_VNACCS ToDataTransferBillOfLadingNew(KDT_VNACCS_BillOfLadingNew billOfLading)
        {

            BillOfLadingNew_VNACCS BillOfLading_VNACCS = new BillOfLadingNew_VNACCS()
            {

                Function = DeclarationFunction.KHAI_BAO,
                Reference = billOfLading.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = string.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                Importer = new NameBase()
                {
                    Identity = billOfLading.MaDoanhNghiep,
                    Name = billOfLading.TenDoanhNghiep
                },
                Agents = new List<Agent>(),
            };
            // Người Khai Hải Quan
            BillOfLading_VNACCS.Agents.Add(new Agent
            {
                Name = billOfLading.TenDoanhNghiep,
                Identity = billOfLading.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });

            BillOfLading_VNACCS.Function = DeclarationFunction.KHAI_BAO;
            BillOfLading_VNACCS.CustomsReference = string.Empty;
            BillOfLading_VNACCS.Acceptance = string.Empty;
            BillOfLading_VNACCS.Issuer = DeclarationIssuer.BillOfLadingNew;
            BillOfLading_VNACCS.DeclarationOffice = billOfLading.MaHQ.Trim();

            BillOfLadingNews billOfLadings = new BillOfLadingNews()
            {
                BillOfLading = new List<BillOfLadingNew>(),
            };

            foreach (KDT_VNACCS_BillOfLadings_Detail item in billOfLading.Collection)
            {
                BranchDetails branchDetails = new BranchDetails()
                {
                    BranchDetail = new List<BranchDetail>(),
                };
                List<KDT_VNACCS_BranchDetail> collection = KDT_VNACCS_BranchDetail.SelectCollectionBy_BillOfLadings_Details_ID(item.ID);
                foreach (KDT_VNACCS_BranchDetail branchDetail in collection)
                {
                    BranchDetail_Containers transportEquipments = new BranchDetail_Containers()
                    {
                        TransportEquipment = new List<BranchDetail_Container>()
                    };
                    List<KDT_VNACCS_BranchDetail_TransportEquipment> TransportEquipmentCollction = KDT_VNACCS_BranchDetail_TransportEquipment.SelectCollectionBy_BranchDetail_ID(branchDetail.ID);
                    foreach (KDT_VNACCS_BranchDetail_TransportEquipment transportEquipment in TransportEquipmentCollction)
                    {
                        transportEquipments.TransportEquipment.Add(new BranchDetail_Container
                        {
                            Container = transportEquipment.SoContainer,
                            Seal = transportEquipment.SoSeal
                        });
                    }
                    branchDetails.BranchDetail.Add(new BranchDetail
                    {
                        Sequence = branchDetail.STT.ToString(),
                        Reference = branchDetail.SoVanDon,
                        ShipperName = branchDetail.TenNguoiGuiHang,
                        ShipperAddress = branchDetail.DiaChiNguoiGuiHang,
                        ConsigneeName = branchDetail.TenNguoiNhanHang,
                        ConsigneeAddress = branchDetail.DiaChiNguoiNhanHang,
                        NumberOfCont = branchDetail.TongSoLuongContainer.ToString(),
                        CargoPiece = branchDetail.SoLuongHang.ToString(),
                        PieceUnitCode = branchDetail.DVTSoLuong,
                        CargoWeight = branchDetail.TongTrongLuongHang.ToString().Replace(",", "."),
                        WeightUnitCode = branchDetail.DVTTrongLuong,
                        TransportEquipments = transportEquipments
                    });
                }
                billOfLadings.BillOfLading.Add(new BillOfLadingNew
                {
                    Reference = item.SoVanDonGoc,
                    Issue = item.NgayVanDonGoc.ToString(sfmtDate),
                    Issuer = item.MaNguoiPhatHanh,
                    Number = item.SoLuongVDN.ToString(),
                    Type = item.PhanLoaiTachVD.ToString(),
                    IsContainer = item.LoaiHang.ToString(),
                    BranchDetails = branchDetails
                });
            }
            BillOfLading_VNACCS.BillOfLadings = billOfLadings;
            return BillOfLading_VNACCS;
        }
        #region Khai báo Thu phí cảng HP
        public static Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Register_Information ToDataTransferRegisterInformation(T_KDT_THUPHI_DOANHNGHIEP customer, string TenDoanhNghiep)
        {
            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Register_Information Register_Information = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Register_Information()
            {
                Issuer = DeclarationIssuer.RegisterInformation,
                Reference = customer.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.KHAI_BAO,
                Company = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Company()
                {
                    Name = customer.TenDoanhNghiep,
                    Identity = customer.MaDoanhNghiep,
                    Address = customer.DiaChi,
                    Contact = customer.NguoiLienHe,
                    Email = customer.Email,
                    Phone = customer.SoDienThoai,
                },
                SignDigital = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.SignDigital()
                {
                    Serial = customer.Serial,
                    CertString = customer.CertString,
                    Subject = customer.Subject,
                    Issuer = customer.Issuer,
                    ValidFrom = customer.ValidFrom.ToString(sfmtDate),
                    ValidTo = customer.ValidTo.ToString(sfmtDate),
                }
            };
            return Register_Information;
        }

        public static Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_HangContainer ToDataTransferRegisterTK(T_KDT_THUPHI_TOKHAI TK, string TenDoanhNghiep)
        {
            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_HangContainer Register_HangContainer = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_HangContainer()
            {
                Issuer = TK.LoaiHangHoa == 100 ? DeclarationIssuer.RegisterHangContainer : DeclarationIssuer.RegisterHangRoi,
                Reference = TK.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.KHAI_BAO,
                IssueLocation = String.Empty,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = TK.SoTiepNhan.ToString(),
                Acceptance = TK.NgayTiepNhan.ToString(sfmtDateTime),
                DeclarationOffice = TK.DiemThuPhi,
                Agent = new Agent()
                {
                    Name = TK.TenDoanhNghiep,
                    Identity = TK.MaDoanhNghiep,
                },
                Importer = new NameBase()
                {
                    Name = TK.TenDoanhNghiep,
                    Identity = TK.MaDoanhNghiep,
                },
                NoticeOfPayment = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.NoticeOfPayment()
                {
                    Notice = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Notice()
                    {
                        NoticeNo = TK.SoTKNP,
                        NoticeDate = TK.NgayTKNP.ToString(sfmtDate),
                        PaymentMethod = TK.HinhThucTT,
                    },
                    CustomsReference = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.CustomsReference()
                    {
                        CustomsDeclare = TK.SoTK,
                        CustomsDeclareDate = TK.NgayTK.ToString(sfmtDate),
                        CustomsDeclareType = TK.MaLoaiHinh,
                        CustomsCode = TK.MaHQ,
                        TariffTypeCode = TK.NhomLoaiHinh,
                        TransportCode = TK.MaPTVC.ToString(),
                        DestinationCode = TK.MaDiaDiemLuuKho,
                    },
                    AdditionalInformation = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_AdditionalInformation()
                    {
                        Content = TK.GhiChu,
                    }
                }
            };

            Register_HangContainer.Issuer = TK.LoaiHangHoa == 100 ? DeclarationIssuer.RegisterHangContainer : DeclarationIssuer.RegisterHangRoi;

            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_TransportEquipments transportEquipments = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_TransportEquipments()
            {
                TransportEquipment = new List<Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_TransportEquipment>()
            };
            if (TK.LoaiHangHoa == 100)
            {
                foreach (T_KDT_THUPHI_TOKHAI_DSCONTAINER item in TK.ContainerCollection)
                {
                    transportEquipments.TransportEquipment.Add(new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_TransportEquipment
                    {
                        BillOfLading = item.SoVanDon,
                        Container = item.SoContainer,
                        Seal = item.SoSeal,
                        ContainerType = item.Loai.ToString(),
                        ContainerKind = item.TinhChat.ToString(),
                        Quantity = item.SoLuong.ToString(),
                        JournalMemo = item.GhiChu,
                    });
                }
            }
            else
            {
                foreach (T_KDT_THUPHI_TOKHAI_DSCONTAINER item in TK.ContainerCollection)
                {
                    transportEquipments.TransportEquipment.Add(new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TTTK_TransportEquipment
                    {
                        BillOfLading = item.SoVanDon,
                        GrossMass = item.TongTrongLuong,
                        MeasureUnit = item.DVT,
                        JournalMemo = item.GhiChu,
                    });
                }
            }
            Register_HangContainer.NoticeOfPayment.TransportEquipments = transportEquipments;

            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.FileAttachs fileAttachs = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.FileAttachs()
            {
                File = new List<Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.File>()
            };
            foreach (T_KDT_THUPHI_TOKHAI_FILE item in TK.FileCollection)
            {
                fileAttachs.File.Add(new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.File
                {
                    Type = item.Loai,
                    Name = item.TenFile,
                    Content = item.Content,
                });
            }
            Register_HangContainer.NoticeOfPayment.FileAttachs = fileAttachs;
            return Register_HangContainer;
        }

        public static Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_InfomationSearch ToDataTransferSearchInformation(T_KDT_THUPHI_BIENLAI BL, string TenDoanhNghiep, string LoaiHangHoa, string SoBienLai, string SoToKhai, string SoVanDon, string SoContainer)
        {
            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_InfomationSearch InfomationSearch = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.TKNP_InfomationSearch()
            {
                Issuer = DeclarationIssuer.SearchInformation,
                Reference = BL.GuidString,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.KHAI_BAO,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                CustomsReference = "",
                Acceptance = "",
                DeclarationOffice = "03CC",
                Agent = new Agent()
                {
                    Name = "Chi cục HQ CK cảng Hải Phòng KV I",
                    Identity = "03CC",
                    Status = AgentsStatus.NGUOIKHAI_HAIQUAN,
                },
                InfomationSearch = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.InfomationSearch()
                {
                    GoodItemType = LoaiHangHoa,
                    ReceiptNo = SoBienLai,
                    CustomsReference = SoToKhai,
                    BillOfLading = SoVanDon,
                    ContainerNo = SoContainer,
                },
            };
            return InfomationSearch;
        }
        #endregion
    }
}

