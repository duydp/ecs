﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.SXXK
{
    public partial class NguyenPhuLieuSuaListForm : BaseForm
    {
        public NguyenPhuLieuDangKy npldk = new NguyenPhuLieuDangKy();
        public NguyenPhuLieuSuaListForm()
        {
            InitializeComponent();
        }

        public void BindData()
        {
            dgList.DataSource = NguyenPhuLieuDangKySUA.SelectCollectionBy_IDNPLDK(this.npldk.ID);
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            NguyenPhuLieuSuaForm f = new NguyenPhuLieuSuaForm();
            f.nplDangKy = npldk;
            f.masterId = npldk.ID;
            f.npldkSUA = new NguyenPhuLieuDangKySUA();
            f.ShowDialog();
            BindData();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            NguyenPhuLieuSuaForm suaForm = new NguyenPhuLieuSuaForm();
            suaForm.npldkSUA = (NguyenPhuLieuDangKySUA)dgList.GetRow().DataRow;
            suaForm.masterIdSUA = suaForm.npldkSUA.ID;
            
            suaForm.masterId = this.npldk.ID;
            suaForm.nplDangKy = this.npldk;

            int tt = suaForm.npldkSUA.TrangThaiXuLy;

            suaForm.ShowDialog();
            if (tt != suaForm.npldkSUA.TrangThaiXuLy)
                BindData();
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
            {
                case -1:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                    break;
                case 0:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                    break;
                case 1:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                    break;
                case 2:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                    break;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            deleteNPL();
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            deleteNPL();
        }

        private void deleteNPL() {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count > 0)
            {
                if (MLMessages("Bạn có muốn xóa các lần khai báo này không?", "MSG_DEL01", "", true) == "Yes")
                {

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            if (i.GetRow().Cells["TrangThaiXuLy"].Value.ToString() == "-1")//Chưa khai báo
                            {
                                NguyenPhuLieuDangKySUA npldkSUA = (NguyenPhuLieuDangKySUA)i.GetRow().DataRow;
                                List<NguyenPhuLieuSUA> nplList = (List<NguyenPhuLieuSUA>)NguyenPhuLieuSUA.SelectCollectionBy_Master_IDSUA(npldkSUA.ID);
                                if (nplList.Count > 0)
                                {
                                    foreach (NguyenPhuLieuSUA npl in nplList)
                                        npl.Delete();
                                }
                                npldkSUA.Delete();
                            }
                            else
                            {
                                int pos = i.Position + 1;
                                MLMessages("Danh sách thứ " + pos + "  đã gửi thông tin tới hải quan.", "MSG_NPL03", "" + pos + "", false);
                            }
                        }
                    }
                    this.BindData();
                }
            }
        }

        private void NguyenPhuLieuSuaListForm_Load(object sender, EventArgs e)
        {
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            BindData();
        }
    }
}
