-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectBy_GiayPhep_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectBy_GiayPhep_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteBy_GiayPhep_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteBy_GiayPhep_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Insert]
	@GiayPhep_ID bigint,
	@TenHangCangTrungGian nvarchar(300),
	@TenCangTrungGian nvarchar(300),
	@SoLuongHangCangTrungGian numeric(8, 0),
	@DonViTinhSoLuongHangCangTrungGian varchar(3),
	@KhoiLuongHangCangTrungGian numeric(10, 3),
	@DonViTinhKhoiLuongHangCangTrungGian varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]
(
	[GiayPhep_ID],
	[TenHangCangTrungGian],
	[TenCangTrungGian],
	[SoLuongHangCangTrungGian],
	[DonViTinhSoLuongHangCangTrungGian],
	[KhoiLuongHangCangTrungGian],
	[DonViTinhKhoiLuongHangCangTrungGian],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@GiayPhep_ID,
	@TenHangCangTrungGian,
	@TenCangTrungGian,
	@SoLuongHangCangTrungGian,
	@DonViTinhSoLuongHangCangTrungGian,
	@KhoiLuongHangCangTrungGian,
	@DonViTinhKhoiLuongHangCangTrungGian,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Update]
	@ID bigint,
	@GiayPhep_ID bigint,
	@TenHangCangTrungGian nvarchar(300),
	@TenCangTrungGian nvarchar(300),
	@SoLuongHangCangTrungGian numeric(8, 0),
	@DonViTinhSoLuongHangCangTrungGian varchar(3),
	@KhoiLuongHangCangTrungGian numeric(10, 3),
	@DonViTinhKhoiLuongHangCangTrungGian varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]
SET
	[GiayPhep_ID] = @GiayPhep_ID,
	[TenHangCangTrungGian] = @TenHangCangTrungGian,
	[TenCangTrungGian] = @TenCangTrungGian,
	[SoLuongHangCangTrungGian] = @SoLuongHangCangTrungGian,
	[DonViTinhSoLuongHangCangTrungGian] = @DonViTinhSoLuongHangCangTrungGian,
	[KhoiLuongHangCangTrungGian] = @KhoiLuongHangCangTrungGian,
	[DonViTinhKhoiLuongHangCangTrungGian] = @DonViTinhKhoiLuongHangCangTrungGian,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_InsertUpdate]
	@ID bigint,
	@GiayPhep_ID bigint,
	@TenHangCangTrungGian nvarchar(300),
	@TenCangTrungGian nvarchar(300),
	@SoLuongHangCangTrungGian numeric(8, 0),
	@DonViTinhSoLuongHangCangTrungGian varchar(3),
	@KhoiLuongHangCangTrungGian numeric(10, 3),
	@DonViTinhKhoiLuongHangCangTrungGian varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian] 
		SET
			[GiayPhep_ID] = @GiayPhep_ID,
			[TenHangCangTrungGian] = @TenHangCangTrungGian,
			[TenCangTrungGian] = @TenCangTrungGian,
			[SoLuongHangCangTrungGian] = @SoLuongHangCangTrungGian,
			[DonViTinhSoLuongHangCangTrungGian] = @DonViTinhSoLuongHangCangTrungGian,
			[KhoiLuongHangCangTrungGian] = @KhoiLuongHangCangTrungGian,
			[DonViTinhKhoiLuongHangCangTrungGian] = @DonViTinhKhoiLuongHangCangTrungGian,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]
		(
			[GiayPhep_ID],
			[TenHangCangTrungGian],
			[TenCangTrungGian],
			[SoLuongHangCangTrungGian],
			[DonViTinhSoLuongHangCangTrungGian],
			[KhoiLuongHangCangTrungGian],
			[DonViTinhKhoiLuongHangCangTrungGian],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@GiayPhep_ID,
			@TenHangCangTrungGian,
			@TenCangTrungGian,
			@SoLuongHangCangTrungGian,
			@DonViTinhSoLuongHangCangTrungGian,
			@KhoiLuongHangCangTrungGian,
			@DonViTinhKhoiLuongHangCangTrungGian,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteBy_GiayPhep_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteBy_GiayPhep_ID]
	@GiayPhep_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]
WHERE
	[GiayPhep_ID] = @GiayPhep_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TenHangCangTrungGian],
	[TenCangTrungGian],
	[SoLuongHangCangTrungGian],
	[DonViTinhSoLuongHangCangTrungGian],
	[KhoiLuongHangCangTrungGian],
	[DonViTinhKhoiLuongHangCangTrungGian],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectBy_GiayPhep_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectBy_GiayPhep_ID]
	@GiayPhep_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TenHangCangTrungGian],
	[TenCangTrungGian],
	[SoLuongHangCangTrungGian],
	[DonViTinhSoLuongHangCangTrungGian],
	[KhoiLuongHangCangTrungGian],
	[DonViTinhKhoiLuongHangCangTrungGian],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]
WHERE
	[GiayPhep_ID] = @GiayPhep_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GiayPhep_ID],
	[TenHangCangTrungGian],
	[TenCangTrungGian],
	[SoLuongHangCangTrungGian],
	[DonViTinhSoLuongHangCangTrungGian],
	[KhoiLuongHangCangTrungGian],
	[DonViTinhKhoiLuongHangCangTrungGian],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TenHangCangTrungGian],
	[TenCangTrungGian],
	[SoLuongHangCangTrungGian],
	[DonViTinhSoLuongHangCangTrungGian],
	[KhoiLuongHangCangTrungGian],
	[DonViTinhKhoiLuongHangCangTrungGian],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]	

GO

