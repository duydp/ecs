﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAE8030 : BasicVNACC
    {
        public static int TongSoByte { get; set; }
        public PropertiesAttribute A01 { get; set; }
        public PropertiesAttribute A02 { get; set; }
        public PropertiesAttribute A03 { get; set; }
        public PropertiesAttribute A04 { get; set; }
        public PropertiesAttribute A05 { get; set; }
        public PropertiesAttribute A06 { get; set; }
        public PropertiesAttribute A07 { get; set; }
        public PropertiesAttribute A08 { get; set; }
        public PropertiesAttribute A09 { get; set; }
        public PropertiesAttribute A10 { get; set; }
        public PropertiesAttribute A11 { get; set; }
        public PropertiesAttribute A12 { get; set; }
        public PropertiesAttribute A13 { get; set; }
        public PropertiesAttribute A14 { get; set; }
        public PropertiesAttribute A15 { get; set; }
        public PropertiesAttribute A16 { get; set; }
        public PropertiesAttribute A17 { get; set; }
        public PropertiesAttribute A18 { get; set; }
        public PropertiesAttribute A19 { get; set; }
        public PropertiesAttribute A20 { get; set; }
        public PropertiesAttribute A21 { get; set; }
        public PropertiesAttribute A22 { get; set; }
        public PropertiesAttribute A23 { get; set; }
        public PropertiesAttribute A24 { get; set; }
        public PropertiesAttribute A25 { get; set; }
        public GroupAttribute DA1 { get; set; }
        public GroupAttribute B01 { get; set; }
        public PropertiesAttribute A32 { get; set; }
        public PropertiesAttribute A33 { get; set; }
        public PropertiesAttribute A34 { get; set; }

        public VAE8030()
        {
            A01 = new PropertiesAttribute(12, typeof(int));
            A02 = new PropertiesAttribute(14, typeof(string));
            A03 = new PropertiesAttribute(1, typeof(string));
            A04 = new PropertiesAttribute(6, typeof(string));
            A05 = new PropertiesAttribute(8, typeof(DateTime));
            A06 = new PropertiesAttribute(6, typeof(int));
            A07 = new PropertiesAttribute(8, typeof(DateTime));
            A08 = new PropertiesAttribute(6, typeof(int));
            A09 = new PropertiesAttribute(8, typeof(DateTime));
            A10 = new PropertiesAttribute(6, typeof(int));
            A11 = new PropertiesAttribute(13, typeof(string));
            A12 = new PropertiesAttribute(300, typeof(string));
            A13 = new PropertiesAttribute(300, typeof(string));
            A14 = new PropertiesAttribute(20, typeof(string));
            A15 = new PropertiesAttribute(8, typeof(DateTime));
            A16 = new PropertiesAttribute(210, typeof(string));
            A17 = new PropertiesAttribute(300, typeof(string));
            A18 = new PropertiesAttribute(300, typeof(string));
            A19 = new PropertiesAttribute(5, typeof(string));
            A20 = new PropertiesAttribute(60, typeof(string));
            A21 = new PropertiesAttribute(300, typeof(string));
            A22 = new PropertiesAttribute(8, typeof(DateTime));
            A23 = new PropertiesAttribute(20, typeof(string));
            A24 = new PropertiesAttribute(8, typeof(DateTime));
            A25 = new PropertiesAttribute(300, typeof(string));
            A32 = new PropertiesAttribute(765, typeof(string));
            A33 = new PropertiesAttribute(420, typeof(string));
            A34 = new PropertiesAttribute(420, typeof(string));
            #region DA1
            List<PropertiesAttribute> listDA1 = new List<PropertiesAttribute>();
            listDA1.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAE8030_DA1, typeof(int)));
            listDA1.Add(new PropertiesAttribute(20, 5, EnumGroupID.VAE8030_DB1, typeof(string)));
            listDA1.Add(new PropertiesAttribute(8, 5, EnumGroupID.VAE8030_DC1, typeof(DateTime)));
            listDA1.Add(new PropertiesAttribute(300, 5, EnumGroupID.VAE8030_DD1, typeof(string)));
            DA1 = new GroupAttribute("DA1", 5, listDA1);
            #endregion DA1
            #region B01
            List<PropertiesAttribute> listB01 = new List<PropertiesAttribute>();
            listB01.Add(new PropertiesAttribute(13, 15, EnumGroupID.VAE8030_B01, typeof(string)));
            listB01.Add(new PropertiesAttribute(300, 15, EnumGroupID.VAE8030_C01, typeof(string)));
            B01 = new GroupAttribute("B01", 15, listB01);
            #endregion B01
            TongSoByte = 10328;
        }

    }
    public partial class EnumGroupID
    {
        public static readonly string VAE8030_DA1 = "VAE8030_DA1";
        public static readonly string VAE8030_DB1 = "VAE8030_DB1";
        public static readonly string VAE8030_DC1 = "VAE8030_DC1";
        public static readonly string VAE8030_DD1 = "VAE8030_DD1";
        public static readonly string VAE8030_B01 = "VAE8030_B01";
        public static readonly string VAE8030_C01 = "VAE8030_C01";
    }

}
