﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3
{
     public class 
         SyncDaTaDetail
    {
         /// <summary>
        /// Đại lý khai
         /// </summary>
         [XmlElement("issuer")]
         public string Issuer { get; set; }
         
         /// <summary>
         /// Mã doanh nghiệp thuê đại lý khai
         /// </summary>
         [XmlElement("business")]
         public string Business { get; set; }

         /// <summary>
         /// Ref của tờ khai
         /// </summary>
         [XmlElement("reference")]
         public string Reference { get; set; }
         
         /// <summary>
         /// Số tiếp nhận tờ khai
         /// </summary>
         [XmlElement("customsReference")]
         public string CustomsReference { get; set; }
         /// <summary>
         /// Ngày đại lý gửi lên
         /// </summary>
         [XmlElement("dateSend")]
         public string DateSend { get; set; }

         /// <summary>
         /// Ngày đăng ký
         /// </summary>
         [XmlElement("acceptance")]
         public string Acceptance { get; set; }

         /// <summary>
         /// Số tờ khai
         /// </summary>
         [XmlElement("number")]
         public string Number { get; set; }


         /// <summary>
         /// Mã Loại Hình
         /// </summary>
         [XmlElement("natureOfTransaction")]
         public string NatureOfTransaction { get; set; }
         
         
         
         /// <summary>
         /// Mã hải quan
         /// </summary>
         [XmlElement("declarationOffice")]
         public string DeclarationOffice { get; set; }


         /// <summary>
         /// Hướng dẫn phân luồng
         /// </summary>
         [XmlElement("streamlineContent")]
         public string StreamlineContent { get; set; }

         /// <summary>
         /// Phân hệ
         /// </summary>
         [XmlElement("type")]
         public string Type { get; set; }

         /// <summary>
         /// Trạng Thái
         /// </summary>
         [XmlElement("status")]
         public string Status { get; set; }
    }
}
