using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.DuLieuChuan;
namespace Company.Interface.KDT.SXXK
{
    public partial class DanhMucSPGCReadExcelForm : BaseForm
    {

        public DanhMucSanPhamGiaCongCollection dmSPGCCollection = new DanhMucSanPhamGiaCongCollection();
        public DanhMucSanPhamGiaCong dmSPGC = new DanhMucSanPhamGiaCong();
        public DanhMucSPGCReadExcelForm()
        {
            InitializeComponent();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }
        private int ConvertCharToInt(char ch) 
        {
            return ch - 'A';
        }
        private int checkNPLExit(string ma)
        {
            for (int i = 0; i < dmSPGCCollection.Count ; i++)
            {
                if (dmSPGCCollection[i].MaSanPham.ToUpper() == ma.ToUpper()) return i;
            }
            return -1;
        }
        private bool EmptyCheck( string st)
        {
            if (st == "") return true;
            return false;
        }
        
        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if(!cvError.IsValid)return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;           
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text);
            }
            catch (Exception ex)
            {
                showMsg("MSG_0203008");
                //ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại", false);
                return;
            }
            try
            {
               ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                showMsg("MSG_EXC01", txtSheet.Text);
                //ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn ;
            int maHangCol = 0;
            char tenHangColumn;
            int tenHangCol = 0;
            char maHSColumn ;
            int maHSCol = 0;
            char dvtColumn ;
            int dvtCol = 0;
            //char SoLuongColumn ;
            //int SoLuongCol = 0;
            int j=0;
            try{
                  maHangColumn = Convert.ToChar(txtMaHangColumn.Text);
                  maHangCol = ConvertCharToInt(maHangColumn);
                  tenHangColumn = Convert.ToChar(txtTenHangColumn.Text);
                  tenHangCol = ConvertCharToInt(tenHangColumn);
                  maHSColumn = Convert.ToChar(txtMaHSColumn.Text);
                  maHSCol = ConvertCharToInt(maHSColumn);
                  dvtColumn = Convert.ToChar(txtDVTColumn.Text);
                  dvtCol = ConvertCharToInt(dvtColumn);
                  //SoLuongColumn = Convert.ToChar(txtSoLuong.Text);
                  //SoLuongCol = ConvertCharToInt(SoLuongColumn);
            }
            catch { return; }
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        
                        //NguyenPhuLieu npl = new NguyenPhuLieu();    
                        DanhMucSanPhamGiaCong dmspgc = new DanhMucSanPhamGiaCong();
                        //if (CheckNumber(Convert.ToDecimal(wsr.Cells[SoLuongCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[maHangCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[SoLuongCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[maHSCol].Value)) || CheckNameDVT(Convert.ToString(wsr.Cells[dvtCol].Value)))
                        //{
                        //    ShowMessage(" Dữ liệu tài dòng " + (wsr.Index + 1) + " sai, được bỏ qua", false);
                        //    continue;
                        //}
                           dmspgc.MaSanPham  = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                           dmspgc.MaDuKien = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                           dmspgc.TenSanPham = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                           dmspgc.DVT_ID = Convert.ToString(wsr.Cells[dvtCol].Value);
                        //   dmspgc.DVT_ID = DonViTinh_GetID(Convert.ToString(wsr.Cells[dvtCol].Value));

                           if (checkNPLExit(dmspgc.MaSanPham ) >= 0)
                            {
                               j=wsr.Index + 1;
                               if (showMsg("MSG_0203022", new String[] {dmspgc.MaSanPham, j.ToString() }, true) == "Yes")
                              // if (MLMessages("Mã loại sản phẩm  \"" + dmspgc.MaSanPham + "\" thuộc dòng " + (wsr.Index + 1) + " trong file Excel đã có trên lưới. Bạn có muốn thay thế lại nguyên phụ liệu trên lưới không?", "MSG_EXC07", j.ToString(), true) == "Yes")
                               {
                                   //this.HD.NPLCollection[checkNPLExit(npl.Ma)] = npl;
                                   dmSPGCCollection[checkNPLExit(dmspgc.MaSanPham)] = dmspgc; 
                               }

                            }
                            else
                            { 
                                //this.HD.NPLCollection.Add(npl); 
                                dmSPGCCollection.Add(dmspgc);
                            }

                        //}
                    }
                    catch
                    {
                        j = wsr.Index + 1;
                        if (showMsg("MSG_0203014", wsr.Index + 1, true) != "Yes")
                        //if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?","MSG_EXC06",j.ToString(), true) != "Yes") 
                        {
                            break;
                        }
                    }
                }
            }
            try
            {
                DanhMucSanPhamGiaCong dmspgctemp = new DanhMucSanPhamGiaCong();
                dmspgctemp.InsertUpdate(dmSPGCCollection);
            }
            catch { }

            this.Close();
        }

        private void NguyenPhuLieuReadExcelForm_Load(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;

            try
            {
                dmSPGCCollection = dmSPGC.SelectCollectionAll();
            }
            catch { }
        }
        private bool CheckNameDVT( string varcheck)
        {
            DataTable dt = Company.GC.BLL.DuLieuChuan.DonViTinh.SelectAll();
            bool ok = false;
            foreach (DataRow drcheck in dt.Rows)
            {
                if (varcheck.Equals(drcheck["Ten"].ToString()))
                {
                    ok = true;
                    break;
                }
            }
            return ok;
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }
    }
}