using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.GC.BLL.GC
{
    public partial class  BKDinhMuc : EntityBase
    {
        #region Private members.

        protected long _ID;
        protected string _TenNPL = String.Empty;
        protected string _MaNPL = String.Empty;
        protected string _DVT = String.Empty;
        protected decimal _DinhMucSD;
        protected decimal _DinhMucTH;
        protected decimal _TyLeHH;
        protected string _NguonNL = String.Empty;
        protected string _GhiChu = String.Empty;
        protected long _OldHD_ID;
        protected int _STT;
        protected string _MaSP = String.Empty;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public string TenNPL
        {
            set { this._TenNPL = value; }
            get { return this._TenNPL; }
        }
        public string MaNPL
        {
            set { this._MaNPL = value; }
            get { return this._MaNPL; }
        }
        public string DVT
        {
            set { this._DVT = value; }
            get { return this._DVT; }
        }
        public decimal DinhMucSD
        {
            set { this._DinhMucSD = value; }
            get { return this._DinhMucSD; }
        }
        public decimal DinhMucTH
        {
            set { this._DinhMucTH = value; }
            get { return this._DinhMucTH; }
        }
        public decimal TyLeHH
        {
            set { this._TyLeHH = value; }
            get { return this._TyLeHH; }
        }
        public string NguonNL
        {
            set { this._NguonNL = value; }
            get { return this._NguonNL; }
        }
        public string GhiChu
        {
            set { this._GhiChu = value; }
            get { return this._GhiChu; }
        }
        public long OldHD_ID
        {
            set { this._OldHD_ID = value; }
            get { return this._OldHD_ID; }
        }
        public int STT
        {
            set { this._STT = value; }
            get { return this._STT; }
        }
        public string MaSP
        {
            set { this._MaSP = value; }
            get { return this._MaSP; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_GC_BKDinhMuc_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) this._TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) this._DVT = reader.GetString(reader.GetOrdinal("DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSD"))) this._DinhMucSD = reader.GetDecimal(reader.GetOrdinal("DinhMucSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucTH"))) this._DinhMucTH = reader.GetDecimal(reader.GetOrdinal("DinhMucTH"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHH"))) this._TyLeHH = reader.GetDecimal(reader.GetOrdinal("TyLeHH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguonNL"))) this._NguonNL = reader.GetString(reader.GetOrdinal("NguonNL"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) this._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("OldHD_ID"))) this._OldHD_ID = reader.GetInt64(reader.GetOrdinal("OldHD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) this._STT = reader.GetInt32(reader.GetOrdinal("STT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) this._MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------



        public DataSet SelectAll()
        {
            string spName = "p_GC_BKDinhMuc_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_GC_BKDinhMuc_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_GC_BKDinhMuc_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_GC_BKDinhMuc_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public BKDinhMucCollection SelectCollectionAll()
        {
            BKDinhMucCollection collection = new BKDinhMucCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                 BKDinhMuc entity = new  BKDinhMuc();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSD"))) entity.DinhMucSD = reader.GetDecimal(reader.GetOrdinal("DinhMucSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucTH"))) entity.DinhMucTH = reader.GetDecimal(reader.GetOrdinal("DinhMucTH"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHH"))) entity.TyLeHH = reader.GetDecimal(reader.GetOrdinal("TyLeHH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguonNL"))) entity.NguonNL = reader.GetString(reader.GetOrdinal("NguonNL"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("OldHD_ID"))) entity.OldHD_ID = reader.GetInt64(reader.GetOrdinal("OldHD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt32(reader.GetOrdinal("STT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public BKDinhMucCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            BKDinhMucCollection collection = new BKDinhMucCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                 BKDinhMuc entity = new  BKDinhMuc();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSD"))) entity.DinhMucSD = reader.GetDecimal(reader.GetOrdinal("DinhMucSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucTH"))) entity.DinhMucTH = reader.GetDecimal(reader.GetOrdinal("DinhMucTH"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHH"))) entity.TyLeHH = reader.GetDecimal(reader.GetOrdinal("TyLeHH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguonNL"))) entity.NguonNL = reader.GetString(reader.GetOrdinal("NguonNL"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("OldHD_ID"))) entity.OldHD_ID = reader.GetInt64(reader.GetOrdinal("OldHD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt32(reader.GetOrdinal("STT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_BKDinhMuc_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, this._DVT);
            this.db.AddInParameter(dbCommand, "@DinhMucSD", SqlDbType.Decimal, this._DinhMucSD);
            this.db.AddInParameter(dbCommand, "@DinhMucTH", SqlDbType.Decimal, this._DinhMucTH);
            this.db.AddInParameter(dbCommand, "@TyLeHH", SqlDbType.Decimal, this._TyLeHH);
            this.db.AddInParameter(dbCommand, "@NguonNL", SqlDbType.NVarChar, this._NguonNL);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@OldHD_ID", SqlDbType.BigInt, this._OldHD_ID);
            this.db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, this._STT);
            this.db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, this._MaSP);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(BKDinhMucCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach ( BKDinhMuc item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, BKDinhMucCollection collection)
        {
            foreach ( BKDinhMuc item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_BKDinhMuc_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, this._DVT);
            this.db.AddInParameter(dbCommand, "@DinhMucSD", SqlDbType.Decimal, this._DinhMucSD);
            this.db.AddInParameter(dbCommand, "@DinhMucTH", SqlDbType.Decimal, this._DinhMucTH);
            this.db.AddInParameter(dbCommand, "@TyLeHH", SqlDbType.Decimal, this._TyLeHH);
            this.db.AddInParameter(dbCommand, "@NguonNL", SqlDbType.NVarChar, this._NguonNL);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@OldHD_ID", SqlDbType.BigInt, this._OldHD_ID);
            this.db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, this._STT);
            this.db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, this._MaSP);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(BKDinhMucCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach ( BKDinhMuc item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_BKDinhMuc_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, this._DVT);
            this.db.AddInParameter(dbCommand, "@DinhMucSD", SqlDbType.Decimal, this._DinhMucSD);
            this.db.AddInParameter(dbCommand, "@DinhMucTH", SqlDbType.Decimal, this._DinhMucTH);
            this.db.AddInParameter(dbCommand, "@TyLeHH", SqlDbType.Decimal, this._TyLeHH);
            this.db.AddInParameter(dbCommand, "@NguonNL", SqlDbType.NVarChar, this._NguonNL);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@OldHD_ID", SqlDbType.BigInt, this._OldHD_ID);
            this.db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, this._STT);
            this.db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, this._MaSP);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(BKDinhMucCollection collection, SqlTransaction transaction)
        {
            foreach ( BKDinhMuc item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_BKDinhMuc_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(BKDinhMucCollection collection, SqlTransaction transaction)
        {
            foreach ( BKDinhMuc item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(BKDinhMucCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach ( BKDinhMuc item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion
    }
}