﻿namespace Company.Interface.KDT.SXXK
{
    partial class TaiSanCoDinhEditFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaiSanCoDinhEditFrm));
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdLuu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdNhanDuLieu2 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanDuLieu");
            this.cmdKhaiSua1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiSua");
            this.cmdKetQua2 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQua");
            this.cmdThemHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdLuu = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKetQua = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQua");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdNhanDuLieu = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanDuLieu");
            this.cmdKhaiSua = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiSua");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.editBox1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.clcNgayThongBaoTL = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label6 = new System.Windows.Forms.Label();
            this.ctrMaHaiQuan = new Company.Interface.Controls.DonViHaiQuanNewControl();
            this.clcNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.clcNgayTiepNhan);
            this.grbMain.Controls.Add(this.ctrMaHaiQuan);
            this.grbMain.Controls.Add(this.clcNgayThongBaoTL);
            this.grbMain.Controls.Add(this.editBox1);
            this.grbMain.Controls.Add(this.btnDelete);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.lblTrangThai);
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Controls.Add(this.label3);
            this.grbMain.Controls.Add(this.txtSoTiepNhan);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Controls.Add(this.label6);
            this.grbMain.Controls.Add(this.label4);
            this.grbMain.Controls.Add(this.label7);
            this.grbMain.Controls.Add(this.label2);
            this.grbMain.Controls.Add(this.label1);
            this.grbMain.Location = new System.Drawing.Point(0, 28);
            this.grbMain.Size = new System.Drawing.Size(804, 424);
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang,
            this.cmdLuu,
            this.cmdKetQua,
            this.cmdKhaiBao,
            this.cmdNhanDuLieu,
            this.cmdKhaiSua});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.Id = new System.Guid("ad64d056-c799-4937-aa69-52c328e64771");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.Tag = null;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 420);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(796, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.Animation = Janus.Windows.UI.DropDownAnimation.None;
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang1,
            this.cmdLuu1,
            this.cmdKhaiBao1,
            this.cmdNhanDuLieu2,
            this.cmdKhaiSua1,
            this.cmdKetQua2});
            this.uiCommandBar1.FloatingSize = new System.Drawing.Size(221, 38);
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(804, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            this.uiCommandBar1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            // 
            // cmdThemHang1
            // 
            this.cmdThemHang1.Key = "cmdThemHang";
            this.cmdThemHang1.Name = "cmdThemHang1";
            // 
            // cmdLuu1
            // 
            this.cmdLuu1.Key = "cmdLuu";
            this.cmdLuu1.Name = "cmdLuu1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdNhanDuLieu2
            // 
            this.cmdNhanDuLieu2.Key = "cmdNhanDuLieu";
            this.cmdNhanDuLieu2.Name = "cmdNhanDuLieu2";
            // 
            // cmdKhaiSua1
            // 
            this.cmdKhaiSua1.Key = "cmdKhaiSua";
            this.cmdKhaiSua1.Name = "cmdKhaiSua1";
            // 
            // cmdKetQua2
            // 
            this.cmdKetQua2.Key = "cmdKetQua";
            this.cmdKetQua2.Name = "cmdKetQua2";
            this.cmdKetQua2.Text = "Kết quả xử lý";
            // 
            // cmdThemHang
            // 
            this.cmdThemHang.Image = ((System.Drawing.Image)(resources.GetObject("cmdThemHang.Image")));
            this.cmdThemHang.Key = "cmdThemHang";
            this.cmdThemHang.Name = "cmdThemHang";
            this.cmdThemHang.Text = "Thêm hàng";
            // 
            // cmdLuu
            // 
            this.cmdLuu.Image = ((System.Drawing.Image)(resources.GetObject("cmdLuu.Image")));
            this.cmdLuu.Key = "cmdLuu";
            this.cmdLuu.Name = "cmdLuu";
            this.cmdLuu.Text = "Lưu thông tin";
            // 
            // cmdKetQua
            // 
            this.cmdKetQua.Key = "cmdKetQua";
            this.cmdKetQua.Name = "cmdKetQua";
            this.cmdKetQua.Text = "Kết quả xử lý";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdKhaiBao.Icon")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdNhanDuLieu
            // 
            this.cmdNhanDuLieu.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdNhanDuLieu.Icon")));
            this.cmdNhanDuLieu.Key = "cmdNhanDuLieu";
            this.cmdNhanDuLieu.Name = "cmdNhanDuLieu";
            this.cmdNhanDuLieu.Text = "Nhận dữ liệu";
            // 
            // cmdKhaiSua
            // 
            this.cmdKhaiSua.Key = "cmdKhaiSua";
            this.cmdKhaiSua.Name = "cmdKhaiSua";
            this.cmdKhaiSua.Text = "Sửa bản khai";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 28);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 392);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(796, 28);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 392);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(804, 28);
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(452, 11);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(126, 13);
            this.lblTrangThai.TabIndex = 32;
            this.lblTrangThai.Text = "Chờ duyệt chính thức";
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.AutomaticSort = false;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.EditorsControlStyle.ButtonAppearance = Janus.Windows.GridEX.ButtonAppearance.Regular;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(15, 129);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(777, 262);
            this.dgList.TabIndex = 28;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(381, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 31;
            this.label3.Text = "Trạng thái";
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.BackColor = System.Drawing.Color.White;
            this.txtSoTiepNhan.Location = new System.Drawing.Point(130, 7);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(49, 21);
            this.txtSoTiepNhan.TabIndex = 30;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "Số tiếp nhận";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Hải quan tiếp nhận";
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDelete.Icon")));
            this.btnDelete.Location = new System.Drawing.Point(641, 397);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(70, 23);
            this.btnDelete.TabIndex = 35;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(717, 397);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 36;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "Ngày thông báo thanh lý";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "Ghi chú";
            // 
            // editBox1
            // 
            this.editBox1.Enabled = false;
            this.editBox1.Location = new System.Drawing.Point(66, 102);
            this.editBox1.Name = "editBox1";
            this.editBox1.Size = new System.Drawing.Size(455, 21);
            this.editBox1.TabIndex = 37;
            this.editBox1.VisualStyleManager = this.vsmMain;
            // 
            // clcNgayThongBaoTL
            // 
            // 
            // 
            // 
            this.clcNgayThongBaoTL.DropDownCalendar.Name = "";
            this.clcNgayThongBaoTL.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.clcNgayThongBaoTL.Location = new System.Drawing.Point(162, 77);
            this.clcNgayThongBaoTL.Name = "clcNgayThongBaoTL";
            this.clcNgayThongBaoTL.Size = new System.Drawing.Size(89, 21);
            this.clcNgayThongBaoTL.TabIndex = 38;
            this.clcNgayThongBaoTL.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.clcNgayThongBaoTL.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.MidnightBlue;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(788, 1);
            this.label6.TabIndex = 29;
            // 
            // ctrMaHaiQuan
            // 
            this.ctrMaHaiQuan.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaHaiQuan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrMaHaiQuan.Location = new System.Drawing.Point(130, 37);
            this.ctrMaHaiQuan.Ma = "";
            this.ctrMaHaiQuan.MaCuc = "";
            this.ctrMaHaiQuan.Name = "ctrMaHaiQuan";
            this.ctrMaHaiQuan.ReadOnly = true;
            this.ctrMaHaiQuan.Size = new System.Drawing.Size(349, 22);
            this.ctrMaHaiQuan.TabIndex = 39;
            this.ctrMaHaiQuan.Ten = "";
            this.ctrMaHaiQuan.VisualStyleManager = this.vsmMain;
            // 
            // clcNgayTiepNhan
            // 
            // 
            // 
            // 
            this.clcNgayTiepNhan.DropDownCalendar.Name = "";
            this.clcNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.clcNgayTiepNhan.Location = new System.Drawing.Point(281, 7);
            this.clcNgayTiepNhan.Name = "clcNgayTiepNhan";
            this.clcNgayTiepNhan.Size = new System.Drawing.Size(94, 21);
            this.clcNgayTiepNhan.TabIndex = 40;
            this.clcNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.clcNgayTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(188, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Ngày tiếp nhận";
            // 
            // TaiSanCoDinhEditFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 452);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "TaiSanCoDinhEditFrm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "TaiSanCoDinhEditFrm";
            this.Load += new System.EventHandler(this.TaiSanCoDinhEditFrm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.ResumeLayout(false);

        }

      

        #endregion

        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang;
        private System.Windows.Forms.Label lblTrangThai;
        private Janus.Windows.GridEX.GridEX dgList;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQua;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQua2;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu1;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.GridEX.EditControls.EditBox editBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayThongBaoTL;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiSua1;
        private Janus.Windows.UI.CommandBars.UICommand cmdNhanDuLieu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiSua;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayTiepNhan;
        private Company.Interface.Controls.DonViHaiQuanNewControl ctrMaHaiQuan;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.UI.CommandBars.UICommand cmdNhanDuLieu2;
    }
}