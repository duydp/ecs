-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@OfficeOfApplicationCode varchar(10),
	@OfficeOfApplicationName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_OGAUser]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[OfficeOfApplicationCode],
	[OfficeOfApplicationName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@OfficeOfApplicationCode,
	@OfficeOfApplicationName,
	@DestinationCode_1,
	@DestinationCode_2,
	@DestinationCode_3,
	@DestinationCode_4,
	@DestinationCode_5,
	@DestinationCode_6,
	@DestinationCode_7,
	@DestinationCode_8,
	@DestinationCode_9,
	@DestinationCode_10,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@OfficeOfApplicationCode varchar(10),
	@OfficeOfApplicationName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_OGAUser]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[OfficeOfApplicationName] = @OfficeOfApplicationName,
	[DestinationCode_1] = @DestinationCode_1,
	[DestinationCode_2] = @DestinationCode_2,
	[DestinationCode_3] = @DestinationCode_3,
	[DestinationCode_4] = @DestinationCode_4,
	[DestinationCode_5] = @DestinationCode_5,
	[DestinationCode_6] = @DestinationCode_6,
	[DestinationCode_7] = @DestinationCode_7,
	[DestinationCode_8] = @DestinationCode_8,
	[DestinationCode_9] = @DestinationCode_9,
	[DestinationCode_10] = @DestinationCode_10,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[OfficeOfApplicationCode] = @OfficeOfApplicationCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@OfficeOfApplicationCode varchar(10),
	@OfficeOfApplicationName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [OfficeOfApplicationCode] FROM [dbo].[t_VNACC_Category_OGAUser] WHERE [OfficeOfApplicationCode] = @OfficeOfApplicationCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_OGAUser] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[OfficeOfApplicationName] = @OfficeOfApplicationName,
			[DestinationCode_1] = @DestinationCode_1,
			[DestinationCode_2] = @DestinationCode_2,
			[DestinationCode_3] = @DestinationCode_3,
			[DestinationCode_4] = @DestinationCode_4,
			[DestinationCode_5] = @DestinationCode_5,
			[DestinationCode_6] = @DestinationCode_6,
			[DestinationCode_7] = @DestinationCode_7,
			[DestinationCode_8] = @DestinationCode_8,
			[DestinationCode_9] = @DestinationCode_9,
			[DestinationCode_10] = @DestinationCode_10,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[OfficeOfApplicationCode] = @OfficeOfApplicationCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_OGAUser]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[OfficeOfApplicationCode],
			[OfficeOfApplicationName],
			[DestinationCode_1],
			[DestinationCode_2],
			[DestinationCode_3],
			[DestinationCode_4],
			[DestinationCode_5],
			[DestinationCode_6],
			[DestinationCode_7],
			[DestinationCode_8],
			[DestinationCode_9],
			[DestinationCode_10],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@OfficeOfApplicationCode,
			@OfficeOfApplicationName,
			@DestinationCode_1,
			@DestinationCode_2,
			@DestinationCode_3,
			@DestinationCode_4,
			@DestinationCode_5,
			@DestinationCode_6,
			@DestinationCode_7,
			@DestinationCode_8,
			@DestinationCode_9,
			@DestinationCode_10,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Delete]
	@OfficeOfApplicationCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_OGAUser]
WHERE
	[OfficeOfApplicationCode] = @OfficeOfApplicationCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_OGAUser] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Load]
	@OfficeOfApplicationCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[OfficeOfApplicationCode],
	[OfficeOfApplicationName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_OGAUser]
WHERE
	[OfficeOfApplicationCode] = @OfficeOfApplicationCode
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[OfficeOfApplicationCode],
	[OfficeOfApplicationName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_OGAUser] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[OfficeOfApplicationCode],
	[OfficeOfApplicationName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_OGAUser]	

GO

