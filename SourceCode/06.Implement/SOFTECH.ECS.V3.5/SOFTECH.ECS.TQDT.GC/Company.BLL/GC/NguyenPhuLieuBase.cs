using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.GC.BLL.GC
{
    public partial class NguyenPhuLieu : EntityBase
    {
        #region Private members.

        protected long _HopDong_ID;
        protected string _Ma = String.Empty;
        protected string _MaMoi = String.Empty;
        protected string _Ten = String.Empty;
        protected string _MaHS = String.Empty;
        protected string _DVT_ID = String.Empty;
        protected decimal _SoLuongDangKy;
        protected decimal _SoLuongDaNhap;
        protected decimal _SoLuongDaNhapDauKy;
        protected decimal _SoLuongDaNhapNgoaiKy;
        protected decimal _SoLuongDaDung;
        protected decimal _SoLuongDaDungDauKy;
        protected decimal _SoLuongDaDungNgoaiKy;
        protected decimal _SoLuongCungUngDauKy;
        protected decimal _SoLuongCungUng;
        protected decimal _SoLuongCungUngNgoaiKy;
        protected decimal _TongNhuCau;
        protected int _STTHang;
        protected int _TrangThai;
        protected decimal _DonGia;
        protected decimal _LuongTonChuaHH;
        protected decimal _LuongSDChuaHH;
        protected decimal _LuongHH;
        protected decimal _LuongSDCoHH;
        protected decimal _SoLuongConLai;
        protected decimal _LuongTonNhuCau;
        public int STT { set; get; }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long HopDong_ID
        {
            set { this._HopDong_ID = value; }
            get { return this._HopDong_ID; }
        }
        public string Ma
        {
            set { this._Ma = value; }
            get { return this._Ma; }
        }
        public string MaMoi
        {
            set { this._MaMoi = value; }
            get { return this._MaMoi; }
        }
        public string Ten
        {
            set { this._Ten = value; }
            get { return this._Ten; }
        }
        public string MaHS
        {
            set { this._MaHS = value; }
            get { return this._MaHS; }
        }
        public string DVT_ID
        {
            set { this._DVT_ID = value; }
            get { return this._DVT_ID; }
        }
        public decimal SoLuongDangKy
        {
            set { this._SoLuongDangKy = value; }
            get { return this._SoLuongDangKy; }
        }
        public decimal SoLuongDaNhapDauKy
        {
            set { this._SoLuongDaNhapDauKy = value; }
            get { return this._SoLuongDaNhapDauKy; }
        }
        public decimal SoLuongDaNhapNgoaiKy
        {
            set { this._SoLuongDaNhapNgoaiKy = value; }
            get { return this._SoLuongDaNhapNgoaiKy; }
        }
        public decimal SoLuongDaNhap
        {
            set { this._SoLuongDaNhap = value; }
            get { return this._SoLuongDaNhap; }
        }
        public decimal SoLuongDaDung
        {
            set { this._SoLuongDaDung = value; }
            get { return this._SoLuongDaDung; }
        }
        public decimal SoLuongDaDungDauKy
        {
            set { this._SoLuongDaDungDauKy = value; }
            get { return this._SoLuongDaDungDauKy; }
        }
        public decimal SoLuongDaDungNgoaiKy
        {
            set { this._SoLuongDaDungNgoaiKy = value; }
            get { return this._SoLuongDaDungNgoaiKy; }
        }
        public decimal SoLuongCungUng
        {
            set { this._SoLuongCungUng = value; }
            get { return this._SoLuongCungUng; }
        }
        public decimal SoLuongCungUngDauKy
        {
            set { this._SoLuongCungUngDauKy = value; }
            get { return this._SoLuongCungUngDauKy; }
        }
        public decimal SoLuongCungUngNgoaiKy
        {
            set { this._SoLuongCungUngNgoaiKy = value; }
            get { return this._SoLuongCungUngNgoaiKy; }
        }
        public decimal SoLuongConLai
        {
            set { this._SoLuongConLai = value; }
            get { return this._SoLuongConLai; }
        }
        public decimal LuongTonNhuCau
        {
            set { this._LuongTonNhuCau = value; }
            get { return this._LuongTonNhuCau; }
        }
        public decimal TongNhuCau
        {
            set { this._TongNhuCau = value; }
            get { return this._TongNhuCau; }
        }
        public int STTHang
        {
            set { this._STTHang = value; }
            get { return this._STTHang; }
        }
        public int TrangThai
        {
            set { this._TrangThai = value; }
            get { return this._TrangThai; }
        }
        public decimal DonGia
        {
            set { this._DonGia = value; }
            get { return this._DonGia; }
        }
        public decimal LuongTonChuaHH
        {
            set { this._LuongTonChuaHH = value; }
            get { return this._LuongTonChuaHH; }
        }
        public decimal LuongSDChuaHH
        {
            set { this._LuongSDChuaHH = value; }
            get { return this._LuongSDChuaHH; }
        }
        public decimal LuongHH
        {
            set { this._LuongHH = value; }
            get { return this._LuongHH; }
        }
        public decimal LuongSDCoHH
        {
            set { this._LuongSDCoHH = value; }
            get { return this._LuongSDCoHH; }
        }
        //---------------------------------------------------------------------------------------------

        //public bool IsExist
        //{
        //    get
        //    {
        //        return this.Load();
        //    }
        //}

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_GC_NguyenPhuLieu_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) this._Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) this._Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) this._SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) this._SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) this._SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) this._SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) this._TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this._TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) this._DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public  bool Load_NPL()
        {
            string spName = "p_GC_NguyenPhuLieu_Load_NPL";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.VarChar, this._Ten);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) this._Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) this._Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) this._SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) this._SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) this._SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) this._SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) this._TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this._TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) this._DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public NguyenPhuLieuCollection SelectCollectionBy_HopDong_ID()
        {
            string spName = "p_GC_NguyenPhuLieu_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                NguyenPhuLieu entity = new NguyenPhuLieu();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) entity.TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_HopDong_ID()
        {
            string spName = "p_GC_NguyenPhuLieu_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string spName = "p_GC_NguyenPhuLieu_SelectAll";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_GC_NguyenPhuLieu_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_GC_NguyenPhuLieu_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_GC_NguyenPhuLieu_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 10000;
            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }
        public static IDataReader SelectReaderDynamicNew(string whereCondition, string orderByExpression)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string spName = "p_GC_NguyenPhuLieu_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 10000;
            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDynamic_FULL(string whereCondition)
        {
            string spName = "p_GC_NguyenPhuLieu_SelectDynamic_Full";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 10000;
            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            //this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public NguyenPhuLieuCollection SelectCollectionAll()
        {
            NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                NguyenPhuLieu entity = new NguyenPhuLieu();

                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) entity.TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public NguyenPhuLieuCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                NguyenPhuLieu entity = new NguyenPhuLieu();

                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) entity.TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));

                //entity._LuongTonChuaHH = entity.SoLuongDaNhap + entity.SoLuongCungUng -
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public NguyenPhuLieuCollection SelectCollectionDynamic1(long idHopDong)
        {
            Company.GC.BLL.GC.DinhMuc dm1 = new Company.GC.BLL.GC.DinhMuc();

            NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();

            IDataReader reader = this.SelectReaderDynamic("HopDong_ID = " + idHopDong, "Ma");
            while (reader.Read())
            {
                NguyenPhuLieu entity = new NguyenPhuLieu();

                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) entity.TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                decimal TLHH = dm1.GetTLHHOfHopDong(idHopDong, entity.Ma);
                entity.LuongTonChuaHH = entity.SoLuongDaNhap + entity.SoLuongCungUng * (1 - TLHH / 100) - entity.SoLuongDaDung * (1 - TLHH / 100);
                entity.LuongSDChuaHH = entity.SoLuongDaDung * (1 - TLHH / 100);
                entity.SoLuongConLai = entity.SoLuongDaNhap + entity.SoLuongCungUng - entity.SoLuongDaDung;
                entity.LuongTonNhuCau = entity.SoLuongDaNhap + entity.SoLuongCungUng - entity.TongNhuCau;
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        //Tính lại lượng NPL chưa hao hụt và có hh
        public NguyenPhuLieuCollection SelectCollectionDynamic_FULL(long idHopDong)
        {
            NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();

            IDataReader reader = this.SelectReaderDynamic_FULL(idHopDong.ToString());
            while (reader.Read())
            {
                NguyenPhuLieu entity = new NguyenPhuLieu();

                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) entity.TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NPLChuaHH"))) entity.LuongSDChuaHH = reader.GetDecimal(reader.GetOrdinal("NPLChuaHH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NPLHH"))) entity.LuongHH = reader.GetDecimal(reader.GetOrdinal("NPLHH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NPLCoHH"))) entity.LuongSDCoHH = reader.GetDecimal(reader.GetOrdinal("NPLCoHH"));
                entity.LuongTonChuaHH = entity.SoLuongDaNhap + entity.SoLuongCungUng - entity.LuongSDChuaHH;
                entity.SoLuongConLai = entity.SoLuongDaNhap + entity.SoLuongCungUng - entity.SoLuongDaDung;
                entity.LuongTonNhuCau = entity.SoLuongDaNhap + entity.SoLuongCungUng - entity.TongNhuCau;
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_NguyenPhuLieu_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, this._SoLuongDaNhap);
            this.db.AddInParameter(dbCommand, "@SoLuongDaDung", SqlDbType.Decimal, this._SoLuongDaDung);
            this.db.AddInParameter(dbCommand, "@SoLuongCungUng", SqlDbType.Decimal, this._SoLuongCungUng);
            this.db.AddInParameter(dbCommand, "@TongNhuCau", SqlDbType.Decimal, this._TongNhuCau);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, this._DonGia);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(NguyenPhuLieuCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NguyenPhuLieu item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, NguyenPhuLieuCollection collection)
        {
            foreach (NguyenPhuLieu item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_NguyenPhuLieu_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, this._SoLuongDaNhap);
            this.db.AddInParameter(dbCommand, "@SoLuongDaDung", SqlDbType.Decimal, this._SoLuongDaDung);
            this.db.AddInParameter(dbCommand, "@SoLuongCungUng", SqlDbType.Decimal, this._SoLuongCungUng);
            this.db.AddInParameter(dbCommand, "@TongNhuCau", SqlDbType.Decimal, this._TongNhuCau);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, this._DonGia);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(NguyenPhuLieuCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NguyenPhuLieu item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_NguyenPhuLieu_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, this._SoLuongDaNhap);
            this.db.AddInParameter(dbCommand, "@SoLuongDaDung", SqlDbType.Decimal, this._SoLuongDaDung);
            this.db.AddInParameter(dbCommand, "@SoLuongCungUng", SqlDbType.Decimal, this._SoLuongCungUng);
            this.db.AddInParameter(dbCommand, "@TongNhuCau", SqlDbType.Decimal, this._TongNhuCau);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, this._DonGia);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int UpdateTransactionNew(SqlTransaction transaction)
        {
            string spName = "p_GC_NguyenPhuLieu_Update_New";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@MaMoi", SqlDbType.VarChar, this._MaMoi);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, this._SoLuongDaNhap);
            this.db.AddInParameter(dbCommand, "@SoLuongDaDung", SqlDbType.Decimal, this._SoLuongDaDung);
            this.db.AddInParameter(dbCommand, "@SoLuongCungUng", SqlDbType.Decimal, this._SoLuongCungUng);
            this.db.AddInParameter(dbCommand, "@TongNhuCau", SqlDbType.Decimal, this._TongNhuCau);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, this._DonGia);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(NguyenPhuLieuCollection collection, SqlTransaction transaction)
        {
            foreach (NguyenPhuLieu item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_NguyenPhuLieu_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }
        public int DeleteTransaction(SqlTransaction transaction, long HopDong_ID, string Ma)
        {
            string spName = "p_GC_NguyenPhuLieu_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt,HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(NguyenPhuLieuCollection collection, SqlTransaction transaction)
        {
            foreach (NguyenPhuLieu item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(NguyenPhuLieuCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NguyenPhuLieu item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public int DeleteBy_HopDong_ID(long HopDong_ID)
        {
            string spName = "p_GC_NguyenPhuLieu_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand);
        }
        public int DeleteBy_HopDong_ID(SqlTransaction transaction)
        {
            string spName = "p_GC_NguyenPhuLieu_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }
        #endregion
    }
}