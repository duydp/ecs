﻿using System;
using System.Drawing;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.GC.BLL;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Net.Mail;

namespace Company.Interface
{
    public partial class SendMailChungTuForm : BaseForm
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public SendMailChungTuForm()
        {
            InitializeComponent();
            
        }
    
        private void btnAddNew_Click(object sender, EventArgs e)
        {
           
          
        }

       

        private void ChungTuForm_Load(object sender, EventArgs e)
        {
            txtSend.Text = GlobalSettings.MailDoanhNghiep;
            txtReview.Text = GlobalSettings.MailHaiQuan;
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }

        private void txtFileUpload_ButtonClick(object sender, EventArgs e)
        {
            DialogResult kq = openFileDialog1.ShowDialog();
            if (kq == DialogResult.OK)
            {
                txtFileUpload.Text = openFileDialog1.FileName;                
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (GlobalSettings.MailDoanhNghiep == "" || GlobalSettings.MailHaiQuan == "")
            {
                if (GlobalSettings.MailDoanhNghiep == "")
                    showMsg("MSG_MAL01");
                    //MLMessages("Chưa cấu hình mail của doanh nghiệp", "MSG_MAI01", "", false);
                if (GlobalSettings.MailHaiQuan == "")
                    showMsg("MSG_MAL02"); 
                    //MLMessages("Chưa cấu hình mail của hải quan tiếp nhận", "MSG_MAI02", "", false);
                return;
            }            
            {
                if (txtFileUpload.Text.Trim() == "")
                    showMsg("MSG_MAL03");
                    //MLMessages("Chưa đính kèm file để gửi","MSG_MAL03","", false);
                else
                {
                    if (!File.Exists(txtFileUpload.Text.Trim()))
                        showMsg("MSG_MAL04");
                        //MLMessages("File này không tồn tại", "MSG_MAI04", "", false);
                }
            }
            MailMessage mail = new MailMessage();
            mail.From =new MailAddress("haiquandientu@gmail.com",GlobalSettings.TEN_DON_VI,System.Text.Encoding.UTF8);
            mail.To.Add(GlobalSettings.MailHaiQuan);
            mail.Subject = "Khai báo điện tử - "+GlobalSettings.MA_DON_VI+"- Tờ khai có số tiếp nhận : "+this.TKMD.SoTiepNhan.ToString();
            mail.Body = txtBody.Text.Trim();
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = false;
            mail.Priority = MailPriority.High;
            if (txtFileUpload.Text.Trim() != "")
            {
                Attachment att = new Attachment(txtFileUpload.Text);
                mail.Attachments.Add(att);
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                uiButton1.Enabled = false; ;
                SmtpClient smtp = new SmtpClient();
                smtp.Timeout = 1000000;
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                smtp.Credentials = new System.Net.NetworkCredential("haiquandientu@gmail.com", "haiquandientudanang");
                smtp.Send(mail);
                showMsg("MSG_MAL05");
                //MLMessages("Gửi mail thành công.", "MSG_MAI05", "", false);
                this.Cursor = Cursors.Default;
                uiButton1.Enabled = true;
                this.Close();
            }
            catch(Exception ex)
            {
                this.Cursor = Cursors.Default;
                showMsg("MSG_0203064", ex.Message);
                //MLMessages("Không gửi chứng từ tới hải quan được." + ex.Message, "MSG_MAI06", ex.Message, false);
            }
                
        }

     
    }
}
