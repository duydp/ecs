﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.KDT.SHARE.QuanLyChungTu.CX;

namespace Company.KDT.SHARE.QuanLyChungTu.CX
{
    public partial class HangDuaVaoDangKy
    {

        public List<HangDuaVao> DanhSachHangDuaVao { get; set; }
        public void loadHang()
        {
            if (ID > 0)
            {
                DanhSachHangDuaVao = new List<HangDuaVao>();
                DanhSachHangDuaVao = (List<HangDuaVao>)HangDuaVao.SelectCollectionBy_Master_ID(this.ID);
            }
        }
        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert(transaction);
                    else
                        this.Update(transaction);

                    foreach (HangDuaVao nplDetail in this.DanhSachHangDuaVao)
                    {
                        if (nplDetail.ID == 0)
                        {
                            nplDetail.Master_ID = this.ID;
                            nplDetail.ID = nplDetail.Insert(transaction);
                        }
                        else
                        {
                            //NguyenPhuLieu NPL = NguyenPhuLieu.Load(nplDetail.ID);
                            //BLL.SXXK.NguyenPhuLieu nplSXXK = new Company.BLL.SXXK.NguyenPhuLieu();
                            //nplSXXK.Ma = NPL.Ma;
                            //nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                            //nplSXXK.MaHaiQuan = this.MaHaiQuan;
                            //nplSXXK.DeleteTransaction(transaction);

                            nplDetail.Master_ID = this.ID;
                            nplDetail.Update(transaction);
                        }
                    }
                    //TransgferDataToSXXK(transaction);
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
    }
}
