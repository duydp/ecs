﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using System.Data;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class KDT_VNACC_ToKhaiMauDich
    {
        List<KDT_VNACC_HangMauDich> _ListHang = new List<KDT_VNACC_HangMauDich>();
        List<KDT_VNACC_TK_GiayPhep> _ListGiayPhep = new List<KDT_VNACC_TK_GiayPhep>();
        List<KDT_VNACC_ChiThiHaiQuan> _ListChiThiHQ = new List<KDT_VNACC_ChiThiHaiQuan>();
        List<KDT_VNACC_TK_DinhKemDienTu> _ListDinhKemDT = new List<KDT_VNACC_TK_DinhKemDienTu>();
        List<KDT_VNACC_TK_KhoanDieuChinh> _ListKhoanDC = new List<KDT_VNACC_TK_KhoanDieuChinh>();
        KDT_VNACC_TK_Container _Container = new KDT_VNACC_TK_Container();
        List<KDT_VNACC_TK_TrungChuyen> _ListTrungChuyen = new List<KDT_VNACC_TK_TrungChuyen>();
        List<KDT_VNACC_TK_SoVanDon> _ListVanDon = new List<KDT_VNACC_TK_SoVanDon>();
        List<KDT_VNACC_ChungTuDinhKem> _ListChungTuDinhKem = new List<KDT_VNACC_ChungTuDinhKem>();
        List<KDT_VNACC_TK_PhanHoi_AnHan> _ListAnHan = new List<KDT_VNACC_TK_PhanHoi_AnHan>();
        List<KDT_VNACC_TK_PhanHoi_SacThue> _ListSacThue = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
        List<KDT_VNACC_TK_PhanHoi_TyGia> _ListTyGia = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
        List<KDT_VNACC_TK_Container> _ListContainer = new List<KDT_VNACC_TK_Container>();
        public List<KDT_VNACC_HangMauDich> HangCollection
        {
            set { this._ListHang = value; }
            get { return this._ListHang; }
        }
        public List<KDT_VNACC_TK_GiayPhep> GiayPhepCollection
        {
            set { this._ListGiayPhep = value; }
            get { return this._ListGiayPhep; }
        }
        public List<KDT_VNACC_ChiThiHaiQuan> ChiThiHQCollection
        {
            set { this._ListChiThiHQ = value; }
            get { return this._ListChiThiHQ; }
        }
        public List<KDT_VNACC_TK_DinhKemDienTu> DinhKemCollection
        {
            set { this._ListDinhKemDT = value; }
            get { return this._ListDinhKemDT; }
        }
        public KDT_VNACC_TK_Container ContainerTKMD
        {
            set { this._Container = value; }
            get { return this._Container; }
        }
        public List<KDT_VNACC_TK_KhoanDieuChinh> KhoanDCCollection
        {
            set { this._ListKhoanDC = value; }
            get { return this._ListKhoanDC; }
        }

        public List<KDT_VNACC_TK_TrungChuyen> TrungChuyenCollection
        {
            set { this._ListTrungChuyen = value; }
            get { return this._ListTrungChuyen; }
        }
        public List<KDT_VNACC_TK_SoVanDon> VanDonCollection
        {
            set { this._ListVanDon = value; }
            get { return this._ListVanDon; }
        }
        public List<KDT_VNACC_ChungTuDinhKem> ChungTuDinhKemCollection
        {
            set { this._ListChungTuDinhKem = value; }
            get { return this._ListChungTuDinhKem; }
        }
        public List<KDT_VNACC_TK_PhanHoi_AnHan> AnHangCollection
        {
            set { this._ListAnHan = value; }
            get { return this._ListAnHan; }
        }
        public List<KDT_VNACC_TK_PhanHoi_SacThue> SacThueCollection
        {
            set { this._ListSacThue = value; }
            get { return this._ListSacThue; }
        }
        public List<KDT_VNACC_TK_PhanHoi_TyGia> TyGiaCollection
        {
            set { this._ListTyGia = value; }
            get { return this._ListTyGia; }
        }

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        //this.TrangThaiXuLy = "0";
                        if(string.IsNullOrEmpty( this.TrangThaiXuLy))
                            this.TrangThaiXuLy = EnumTrangThaiXuLy.ChuaKhaiBao;
                        this.ID = this.Insert(transaction);
                    }
                    else
                        this.Update(transaction);

                    // luu hang to khai
                    foreach (KDT_VNACC_HangMauDich itemHang in this.HangCollection)
                    {
                        
                        if (itemHang.ID == 0)
                        {
                            itemHang.TKMD_ID = this.ID;
                            itemHang.ID = itemHang.Insert(transaction);
                            foreach (KDT_VNACC_HangMauDich_ThueThuKhac item in itemHang.ThueThuKhacCollection)
                            {
                                if (item.ID == 0)
                                {
                                    item.Master_id = itemHang.ID;
                                    item.ID = item.Insert(transaction);
                                }
                                else
                                {
                                    item.Master_id = itemHang.ID;
                                    item.Update(transaction);
                                }
                            }
                        }
                        else
                        {
                            itemHang.Update(transaction);
                            foreach (KDT_VNACC_HangMauDich_ThueThuKhac item in itemHang.ThueThuKhacCollection)
                            {
                                if (item.ID == 0)
                                {
                                    item.Master_id = itemHang.ID;
                                    item.ID = item.Insert(transaction);
                                }
                                else
                                {
                                    item.Master_id = itemHang.ID;
                                    item.Update(transaction);
                                }
                            }
                        }
                    }
                    // luu thong tin giay phep

                    foreach (KDT_VNACC_TK_GiayPhep itemGP in this.GiayPhepCollection)
                    {
                        if (itemGP.ID == 0)
                        {
                            itemGP.TKMD_ID = this.ID;
                            itemGP.ID = itemGP.Insert(transaction);
                        }
                        else
                        {
                            itemGP.InsertUpdate(transaction);
                        }
                    }
                    // dinh kem dien tu
                    foreach (KDT_VNACC_TK_DinhKemDienTu item in this.DinhKemCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.TKMD_ID = this.ID;
                            item.ID = item.Insert(transaction);
                        }
                        else
                        {
                            item.InsertUpdate(transaction);
                        }
                    }
                    //Trung chuyen
                    foreach (KDT_VNACC_TK_TrungChuyen item in this.TrungChuyenCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.TKMD_ID = this.ID;
                            item.ID = item.Insert(transaction);
                        }
                        else
                        {
                            item.InsertUpdate(transaction);
                        }
                    }
                    // van don
                    foreach (KDT_VNACC_TK_SoVanDon item in this.VanDonCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.TKMD_ID = this.ID;
                            item.ID = item.Insert(transaction);
                        }
                        else
                        {
                            item.InsertUpdate(transaction);
                        }
                    }
                    //tri gia khoan dieu chinh
                    foreach (KDT_VNACC_TK_KhoanDieuChinh item in this.KhoanDCCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.TKMD_ID = this.ID;
                            item.ID = item.Insert(transaction);
                        }
                        else
                        {
                            item.InsertUpdate(transaction);
                        }
                    }
                    //chi thi hai quan
                    KDT_VNACC_ChiThiHaiQuan.DeleteDynamic("Master_ID = " + this.ID + " AND LoaiThongTin = 'TK'");
                    foreach (KDT_VNACC_ChiThiHaiQuan item in this.ChiThiHQCollection)
                    {
                        
                        if (item.ID == 0)
                        {
                            item.Master_ID = this.ID;
                            item.LoaiThongTin = "TK";
                            item.ID = item.Insert(transaction);
                        }
                        else
                        {
                            item.LoaiThongTin = "TK";
                            item.InsertUpdate(transaction);
                        }
                    }
                    //container
                    //KDT_VNACC_TK_Container.DeleteDynamic("TKMD_ID = "+ this.ID );
                    KDT_VNACC_TK_Container Cont = new KDT_VNACC_TK_Container();
                    Cont = this.ContainerTKMD;
                    if (Cont != null)
                    {
                        if (Cont.ID == 0)
                        {
                            Cont.TKMD_ID = this.ID;
                            Cont.ID = Cont.Insert(transaction);
                            //KDT_VNACC_TK_SoContainer.DeleteDynamic("Master_id = " + Cont.ID);
                            foreach (KDT_VNACC_TK_SoContainer item in Cont.SoContainerCollection)
                            {
                                if (item.ID == 0)
                                {
                                    item.Master_id = Cont.ID;
                                    item.ID = item.Insert(transaction);
                                }
                                else
                                {
                                    item.Master_id = Cont.ID;
                                    item.Update(transaction);
                                }
                            }
                        }
                        else
                        {
                            Cont.TKMD_ID = this.ID;
                            Cont.InsertUpdate(transaction);
                            //KDT_VNACC_TK_SoContainer.DeleteDynamic("Master_id = " + Cont.ID);
                            foreach (KDT_VNACC_TK_SoContainer item in Cont.SoContainerCollection)
                            {
                                if (item.ID == 0)
                                {
                                    item.Master_id = Cont.ID;
                                    item.ID = item.Insert(transaction);
                                }
                                else
                                {
                                    item.Master_id = Cont.ID;
                                    item.Update(transaction);
                                }
                            }
                        }
                    }

                    if (this.TyGiaCollection != null && this.TyGiaCollection.Count > 0)
                    {
                        KDT_VNACC_TK_PhanHoi_TyGia.DeleteDynamic("Master_ID = " + this.ID);
                        foreach (KDT_VNACC_TK_PhanHoi_TyGia item in TyGiaCollection)
                        {
                            item.Master_ID = this.ID;
                            item.InsertUpdate(transaction);
                        }
                    }
                    if (this.SacThueCollection != null && this.SacThueCollection.Count > 0)
                    {
                        KDT_VNACC_TK_PhanHoi_SacThue.DeleteDynamic("Master_ID = " + this.ID);
                        foreach (KDT_VNACC_TK_PhanHoi_SacThue item in this.SacThueCollection)
                        {
                            item.Master_ID = this.ID;
                            item.InsertUpdate(transaction);
                        }
                    }
                    ////Sô container
                    //foreach (KDT_VNACC_TK_SoContainer item in this.SoContainerCollection)
                    //{
                    //    if (item.ID == 0)
                    //    {
                    //        item.Master_id = this.ID;
                    //        item.ID = item.Insert();
                    //    }
                    //    else
                    //    {
                    //        item.InsertUpdate();
                    //    }
                    //}
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    connection.Close();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateFull(SqlTransaction transaction)
        {
            if (this.ID == 0)
            {
                //this.TrangThaiXuLy = "0";
                if (string.IsNullOrEmpty(this.TrangThaiXuLy))
                    this.TrangThaiXuLy = EnumTrangThaiXuLy.ChuaKhaiBao;
                this.ID = this.Insert(transaction);
            }
            else
                this.Update(transaction);

            // luu hang to khai
            foreach (KDT_VNACC_HangMauDich itemHang in this.HangCollection)
            {
                if (itemHang.ID == 0)
                {
                    itemHang.TKMD_ID = this.ID;
                    itemHang.ID = itemHang.Insert();
                    foreach (KDT_VNACC_HangMauDich_ThueThuKhac item in itemHang.ThueThuKhacCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.Master_id = itemHang.ID;
                            item.ID = item.Insert(transaction);
                        }
                        else
                        {
                            item.Master_id = itemHang.ID;
                            item.Update(transaction);
                        }
                    }
                }
                else
                {
                    itemHang.Update(transaction);
                    foreach (KDT_VNACC_HangMauDich_ThueThuKhac item in itemHang.ThueThuKhacCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.Master_id = itemHang.ID;
                            item.ID = item.Insert(transaction);
                        }
                        else
                        {
                            item.Master_id = itemHang.ID;
                            item.Update(transaction);
                        }
                    }
                }
            }
            // luu thong tin giay phep

            foreach (KDT_VNACC_TK_GiayPhep itemGP in this.GiayPhepCollection)
            {
                if (itemGP.ID == 0)
                {
                    itemGP.TKMD_ID = this.ID;
                    itemGP.ID = itemGP.Insert(transaction);
                }
                else
                {
                    itemGP.InsertUpdate(transaction);
                }
            }
            // dinh kem dien tu
            foreach (KDT_VNACC_TK_DinhKemDienTu item in this.DinhKemCollection)
            {
                if (item.ID == 0)
                {
                    item.TKMD_ID = this.ID;
                    item.ID = item.Insert(transaction);
                }
                else
                {
                    item.InsertUpdate(transaction);
                }
            }
            //Trung chuyen
            foreach (KDT_VNACC_TK_TrungChuyen item in this.TrungChuyenCollection)
            {
                if (item.ID == 0)
                {
                    item.TKMD_ID = this.ID;
                    item.ID = item.Insert(transaction);
                }
                else
                {
                    item.InsertUpdate(transaction);
                }
            }
            // van don
            foreach (KDT_VNACC_TK_SoVanDon item in this.VanDonCollection)
            {
                if (item.ID == 0)
                {
                    item.TKMD_ID = this.ID;
                    item.ID = item.Insert(transaction);
                }
                else
                {
                    item.InsertUpdate(transaction);
                }
            }
            //tri gia khoan dieu chinh
            foreach (KDT_VNACC_TK_KhoanDieuChinh item in this.KhoanDCCollection)
            {
                if (item.ID == 0)
                {
                    item.TKMD_ID = this.ID;
                    item.ID = item.Insert(transaction);
                }
                else
                {
                    item.InsertUpdate(transaction);
                }
            }
            //chi thi hai quan
            KDT_VNACC_ChiThiHaiQuan.DeleteDynamic("Master_ID = " + this.ID + " AND LoaiThongTin = 'TK'");
            foreach (KDT_VNACC_ChiThiHaiQuan item in this.ChiThiHQCollection)
            {

                if (item.ID == 0)
                {
                    item.Master_ID = this.ID;
                    item.LoaiThongTin = "TK";
                    item.ID = item.Insert(transaction);
                }
                else
                {
                    item.LoaiThongTin = "TK";
                    item.InsertUpdate(transaction);
                }
            }
            //container
            KDT_VNACC_TK_Container Cont = new KDT_VNACC_TK_Container();
            Cont = this.ContainerTKMD;
            if (Cont != null)
            {
                if (Cont.ID == 0)
                {
                    Cont.TKMD_ID = this.ID;
                    Cont.ID = Cont.Insert(transaction);
                    foreach (KDT_VNACC_TK_SoContainer item in Cont.SoContainerCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.Master_id = Cont.ID;
                            item.ID = item.Insert(transaction);
                        }
                        else
                        {
                            item.Master_id = Cont.ID;
                            item.Update(transaction);
                        }
                    }
                }
                else
                {
                    Cont.TKMD_ID = this.ID;
                    Cont.InsertUpdate(transaction);
                    foreach (KDT_VNACC_TK_SoContainer item in Cont.SoContainerCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.Master_id = Cont.ID;
                            item.ID = item.Insert(transaction);
                        }
                        else
                        {
                            item.Master_id = Cont.ID;
                            item.Update(transaction);
                        }
                    }
                }
            }

            if (this.TyGiaCollection != null && this.TyGiaCollection.Count > 0)
            {
                KDT_VNACC_TK_PhanHoi_TyGia.DeleteDynamic("Master_ID = " + this.ID);
                foreach (KDT_VNACC_TK_PhanHoi_TyGia item in TyGiaCollection)
                {
                    item.Master_ID = this.ID;
                    item.InsertUpdate(transaction);
                }
            }
            if (this.SacThueCollection != null && this.SacThueCollection.Count > 0)
            {
                KDT_VNACC_TK_PhanHoi_SacThue.DeleteDynamic("Master_ID = " + this.ID);
                foreach (KDT_VNACC_TK_PhanHoi_SacThue item in this.SacThueCollection)
                {
                    item.Master_ID = this.ID;
                    item.InsertUpdate(transaction);
                }
            }
            ////Sô container
            //foreach (KDT_VNACC_TK_SoContainer item in this.SoContainerCollection)
            //{
            //    if (item.ID == 0)
            //    {
            //        item.Master_id = this.ID;
            //        item.ID = item.Insert();
            //    }
            //    else
            //    {
            //        item.InsertUpdate();
            //    }
            //}
            return true;

        }

        public bool InsertUpdateFullSynDataVNACCS(SqlTransaction transaction)
        {
            if (this.ID == 0)
            {
                //this.TrangThaiXuLy = "0";
                if (string.IsNullOrEmpty(this.TrangThaiXuLy))
                    this.TrangThaiXuLy = EnumTrangThaiXuLy.ChuaKhaiBao;
                this.ID = this.Insert(transaction);
            }
            else
                this.Update(transaction);

            // luu hang to khai
            foreach (KDT_VNACC_HangMauDich itemHang in this.HangCollection)
            {
                if (itemHang.ID == 0)
                {
                    itemHang.TKMD_ID = this.ID;
                    itemHang.ID = itemHang.InsertSynDataVNACCS();
                    foreach (KDT_VNACC_HangMauDich_ThueThuKhac item in itemHang.ThueThuKhacCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.Master_id = itemHang.ID;
                            item.ID = item.Insert(transaction);
                        }
                        else
                        {
                            item.Master_id = itemHang.ID;
                            item.Update(transaction);
                        }
                    }
                }
                else
                {
                    itemHang.UpdateSynDataVNACCS(transaction);
                    foreach (KDT_VNACC_HangMauDich_ThueThuKhac item in itemHang.ThueThuKhacCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.Master_id = itemHang.ID;
                            item.ID = item.Insert(transaction);
                        }
                        else
                        {
                            item.Master_id = itemHang.ID;
                            item.Update(transaction);
                        }
                    }
                }
            }
            // luu thong tin giay phep

            foreach (KDT_VNACC_TK_GiayPhep itemGP in this.GiayPhepCollection)
            {
                if (itemGP.ID == 0)
                {
                    itemGP.TKMD_ID = this.ID;
                    itemGP.ID = itemGP.Insert(transaction);
                }
                else
                {
                    itemGP.InsertUpdate(transaction);
                }
            }
            // dinh kem dien tu
            foreach (KDT_VNACC_TK_DinhKemDienTu item in this.DinhKemCollection)
            {
                if (item.ID == 0)
                {
                    item.TKMD_ID = this.ID;
                    item.ID = item.Insert(transaction);
                }
                else
                {
                    item.InsertUpdate(transaction);
                }
            }
            //Trung chuyen
            foreach (KDT_VNACC_TK_TrungChuyen item in this.TrungChuyenCollection)
            {
                if (item.ID == 0)
                {
                    item.TKMD_ID = this.ID;
                    item.ID = item.Insert(transaction);
                }
                else
                {
                    item.InsertUpdate(transaction);
                }
            }
            // van don
            foreach (KDT_VNACC_TK_SoVanDon item in this.VanDonCollection)
            {
                if (item.ID == 0)
                {
                    item.TKMD_ID = this.ID;
                    item.ID = item.Insert(transaction);
                }
                else
                {
                    item.InsertUpdate(transaction);
                }
            }
            //tri gia khoan dieu chinh
            foreach (KDT_VNACC_TK_KhoanDieuChinh item in this.KhoanDCCollection)
            {
                if (item.ID == 0)
                {
                    item.TKMD_ID = this.ID;
                    item.ID = item.Insert(transaction);
                }
                else
                {
                    item.InsertUpdate(transaction);
                }
            }
            //chi thi hai quan
            KDT_VNACC_ChiThiHaiQuan.DeleteDynamic("Master_ID = " + this.ID + " AND LoaiThongTin = 'TK'");
            foreach (KDT_VNACC_ChiThiHaiQuan item in this.ChiThiHQCollection)
            {

                if (item.ID == 0)
                {
                    item.Master_ID = this.ID;
                    item.LoaiThongTin = "TK";
                    item.ID = item.Insert(transaction);
                }
                else
                {
                    item.LoaiThongTin = "TK";
                    item.InsertUpdate(transaction);
                }
            }
            //container
            KDT_VNACC_TK_Container Cont = new KDT_VNACC_TK_Container();
            Cont = this.ContainerTKMD;
            if (Cont != null)
            {
                if (Cont.ID == 0)
                {
                    Cont.TKMD_ID = this.ID;
                    Cont.ID = Cont.Insert(transaction);
                    foreach (KDT_VNACC_TK_SoContainer item in Cont.SoContainerCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.Master_id = Cont.ID;
                            item.ID = item.Insert(transaction);
                        }
                        else
                        {
                            item.Master_id = Cont.ID;
                            item.Update(transaction);
                        }
                    }
                }
                else
                {
                    Cont.TKMD_ID = this.ID;
                    Cont.InsertUpdate(transaction);
                    foreach (KDT_VNACC_TK_SoContainer item in Cont.SoContainerCollection)
                    {
                        if (item.ID == 0)
                        {
                            item.Master_id = Cont.ID;
                            item.ID = item.Insert(transaction);
                        }
                        else
                        {
                            item.Master_id = Cont.ID;
                            item.Update(transaction);
                        }
                    }
                }
            }

            if (this.TyGiaCollection != null && this.TyGiaCollection.Count > 0)
            {
                KDT_VNACC_TK_PhanHoi_TyGia.DeleteDynamic("Master_ID = " + this.ID);
                foreach (KDT_VNACC_TK_PhanHoi_TyGia item in TyGiaCollection)
                {
                    item.Master_ID = this.ID;
                    item.InsertUpdate(transaction);
                }
            }
            if (this.SacThueCollection != null && this.SacThueCollection.Count > 0)
            {
                KDT_VNACC_TK_PhanHoi_SacThue.DeleteDynamic("Master_ID = " + this.ID);
                foreach (KDT_VNACC_TK_PhanHoi_SacThue item in this.SacThueCollection)
                {
                    item.Master_ID = this.ID;
                    item.InsertUpdate(transaction);
                }
            }
            ////Sô container
            //foreach (KDT_VNACC_TK_SoContainer item in this.SoContainerCollection)
            //{
            //    if (item.ID == 0)
            //    {
            //        item.Master_id = this.ID;
            //        item.ID = item.Insert();
            //    }
            //    else
            //    {
            //        item.InsertUpdate();
            //    }
            //}
            return true;

        }


        public static int DeleteDynamic(string whereCondition, SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public bool DeleteFull(long TKMD_ID)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {

                    string where_TKMD = "TKMD_ID =" + TKMD_ID;
                    string where_Master = "Master_ID =" + TKMD_ID;
                    // Xóa hàng
                    _ListHang = KDT_VNACC_HangMauDich.SelectCollectionDynamic(where_TKMD, null);
                    foreach (KDT_VNACC_HangMauDich HMD in _ListHang)
                    {
                        KDT_VNACC_HangMauDich_ThueThuKhac.DeleteDynamic("Master_ID =" + HMD.ID);
                        HMD.Delete(transaction);
                    }

                    //xóa giấy phép
                    KDT_VNACC_TK_GiayPhep.DeleteDynamic(where_TKMD);
                    //xóa đinh kem điện tử
                    KDT_VNACC_TK_DinhKemDienTu.DeleteDynamic(where_TKMD);
                    //Xóa khoản điều chỉnh
                    KDT_VNACC_TK_KhoanDieuChinh.DeleteDynamic(where_TKMD);
                    //Xóa trung chuyển
                    KDT_VNACC_TK_TrungChuyen.DeleteDynamic(where_TKMD);
                    //Xóa vận đơn
                    KDT_VNACC_TK_SoVanDon.DeleteDynamic(where_TKMD);
                    //Xóa container
                    _ListContainer = KDT_VNACC_TK_Container.SelectCollectionDynamic(where_TKMD, null);
                    foreach (KDT_VNACC_TK_Container Cont in _ListContainer)
                    {
                        KDT_VNACC_TK_SoContainer.DeleteDynamic("Master_ID =" + Cont.ID);
                        Cont.Delete(transaction);
                    }
                    //Xóa ân hạn thuế
                    KDT_VNACC_TK_PhanHoi_AnHan.DeleteDynamic(where_Master);
                    //Xóa Sắc thuế
                    KDT_VNACC_TK_PhanHoi_SacThue.DeleteDynamic(where_Master);
                    //Xóa tỷ giá
                    KDT_VNACC_TK_PhanHoi_TyGia.DeleteDynamic(where_Master);
                    //Xóa tờ khai
                    KDT_VNACC_ToKhaiMauDich.DeleteDynamic("ID=" + TKMD_ID, transaction);

                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    connection.Close();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public void LoadChungTuDinhKem()
        {
            _ListChungTuDinhKem = KDT_VNACC_ChungTuDinhKem.SelectCollectionDynamic(string.Format("TKMDID = {0}", this.ID), "");
        }

        public void  LoadFull()
        {
            try
            {

                if (this.ID > 0)
                {
                    
                    this.HangCollection = KDT_VNACC_HangMauDich.LoadHangMauDichbyID(new long[] { this.ID });
                    string where = "TKMD_ID = " + this.ID;
                    this.GiayPhepCollection = KDT_VNACC_TK_GiayPhep.SelectCollectionDynamic(where, null);
                    this.DinhKemCollection = KDT_VNACC_TK_DinhKemDienTu.SelectCollectionDynamic(where, null);
                    this.TrungChuyenCollection = KDT_VNACC_TK_TrungChuyen.SelectCollectionDynamic(where, "ID");
                    string whereChiThiHQ = string.Format("Master_ID = {0} and LoaiThongTin = 'TK'", this.ID);
                    this.ChiThiHQCollection = KDT_VNACC_ChiThiHaiQuan.SelectCollectionDynamic(whereChiThiHQ, null);
                    this.VanDonCollection = KDT_VNACC_TK_SoVanDon.SelectCollectionDynamic(where, "ID");
                    this.KhoanDCCollection = KDT_VNACC_TK_KhoanDieuChinh.SelectCollectionDynamic(where, null);
                    List<KDT_VNACC_TK_Container> listContainer = new List<KDT_VNACC_TK_Container>();
                    listContainer = KDT_VNACC_TK_Container.SelectCollectionDynamic(where +" and Madiadiem1 is not null and Madiadiem1 != '' ", "ID");
                    if(this.HangCollection != null && this.HangCollection.Count > 0)
                    {
                        foreach (KDT_VNACC_HangMauDich item in this.HangCollection)
                        {
                            item.loadThueVaThuKhac();
                        }
                    }
                    if (listContainer != null && listContainer.Count > 0)
                    {
                        this.ContainerTKMD = listContainer[0];
                        this.ContainerTKMD.SoContainerCollection = KDT_VNACC_TK_SoContainer.SelectCollectionDynamic("Master_id=" + this.ContainerTKMD.ID, null);
                      
                    }
                    this.SacThueCollection = KDT_VNACC_TK_PhanHoi_SacThue.SelectCollectionDynamic("Master_ID =" + this.ID, "ID");
                    this.TyGiaCollection = KDT_VNACC_TK_PhanHoi_TyGia.SelectCollectionDynamic("Master_ID =" + this.ID, "ID");
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        public KDT_VNACC_ToKhaiMauDich LoadToKhai(long id)
        {
            List<KDT_VNACC_ToKhaiMauDich> ListTKMD = new List<KDT_VNACC_ToKhaiMauDich>();
            ListTKMD = KDT_VNACC_ToKhaiMauDich.SelectCollectionAll();
            foreach (KDT_VNACC_ToKhaiMauDich TKMD in ListTKMD)
            {
                if (TKMD.ID == id)
                {
                    string where = "TKMD_ID=" + TKMD.ID;
                    //Hang mau dich Collection
                    List<KDT_VNACC_HangMauDich> listHang = new List<KDT_VNACC_HangMauDich>();
                    listHang = KDT_VNACC_HangMauDich.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_HangMauDich hang in listHang)
                    {
                        //List<KDT_VNACC_HangMauDich_ThueThuKhac> listThueThuKhac = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
                        //listThueThuKhac = KDT_VNACC_HangMauDich_ThueThuKhac.SelectCollectionDynamic("Master_id=" + hang.ID, "ID");
                        //foreach (KDT_VNACC_HangMauDich_ThueThuKhac thue in listThueThuKhac)
                        //{
                        //    hang.ThueThuKhacCollection.Add(thue);
                        //}
                        hang.loadThueVaThuKhac();
                        TKMD.HangCollection.Add(hang);
                    }
                    // giay phep Collection
                    List<KDT_VNACC_TK_GiayPhep> listGiayPhep = new List<KDT_VNACC_TK_GiayPhep>();
                    listGiayPhep = KDT_VNACC_TK_GiayPhep.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_GiayPhep item in listGiayPhep)
                    {
                        TKMD.GiayPhepCollection.Add(item);
                    }
                    // so dien tu dinh kem
                    List<KDT_VNACC_TK_DinhKemDienTu> listDinhKiem = new List<KDT_VNACC_TK_DinhKemDienTu>();
                    listDinhKiem = KDT_VNACC_TK_DinhKemDienTu.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_DinhKemDienTu item in listDinhKiem)
                    {
                        TKMD.DinhKemCollection.Add(item);
                    }
                    // trung chuyển collection KDT_VNACC_TK_TrungChuyen
                    List<KDT_VNACC_TK_TrungChuyen> listTC = new List<KDT_VNACC_TK_TrungChuyen>();
                    listTC = KDT_VNACC_TK_TrungChuyen.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_TrungChuyen item in listTC)
                    {
                        TKMD.TrungChuyenCollection.Add(item);
                    }
                    //Chi thi Hai quan collection
                    List<KDT_VNACC_ChiThiHaiQuan> listChiThi = new List<KDT_VNACC_ChiThiHaiQuan>();
                    string whereChiThiHQ = string.Format("Master_ID = {0} and LoaiThongTin = 'TK'", TKMD.ID);
                    listChiThi = KDT_VNACC_ChiThiHaiQuan.SelectCollectionDynamic(whereChiThiHQ, "");
                    foreach (KDT_VNACC_ChiThiHaiQuan item in listChiThi)
                    {
                        TKMD.ChiThiHQCollection.Add(item);
                    }
                    // Van don Collection
                    List<KDT_VNACC_TK_SoVanDon> listVanDon = new List<KDT_VNACC_TK_SoVanDon>();
                    listVanDon = KDT_VNACC_TK_SoVanDon.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_SoVanDon item in listVanDon)
                    {
                        TKMD.VanDonCollection.Add(item);
                    }
                    //Tri gia Khoan Dieu Chinh
                    List<KDT_VNACC_TK_KhoanDieuChinh> listDieuChinh = new List<KDT_VNACC_TK_KhoanDieuChinh>();
                    listDieuChinh = KDT_VNACC_TK_KhoanDieuChinh.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_KhoanDieuChinh item in listDieuChinh)
                    {
                        TKMD.KhoanDCCollection.Add(item);
                    }

                    List<KDT_VNACC_TK_Container> listContainer = new List<KDT_VNACC_TK_Container>();
                    listContainer = KDT_VNACC_TK_Container.SelectCollectionDynamic(where, "ID");
                    foreach (KDT_VNACC_TK_Container item in listContainer)
                    {
                        List<KDT_VNACC_TK_SoContainer> listSoCont = new List<KDT_VNACC_TK_SoContainer>();
                        listSoCont = KDT_VNACC_TK_SoContainer.SelectCollectionDynamic("Master_id=" + item.ID, "ID");
                        foreach (KDT_VNACC_TK_SoContainer soCont in listSoCont)
                        {
                            item.SoContainerCollection.Add(soCont);
                        }
                        TKMD.ContainerTKMD = item;
                    }
                    //List<KDT_VNACC_TK_SoContainer> listSoContainer = new List<KDT_VNACC_TK_SoContainer>();
                    //listSoContainer = KDT_VNACC_TK_SoContainer.SelectCollectionDynamic("Master_ID =" + TKMD.ID, "ID");
                    //foreach (KDT_VNACC_TK_SoContainer item in listSoContainer)
                    //{
                    //    TKMD.SoContainerCollection.Add(item);
                    //}
                    List<KDT_VNACC_TK_PhanHoi_SacThue> listSacThue = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
                    listSacThue = KDT_VNACC_TK_PhanHoi_SacThue.SelectCollectionDynamic("Master_ID =" + TKMD.ID, "ID");
                    foreach (KDT_VNACC_TK_PhanHoi_SacThue item in listSacThue)
                    {
                        TKMD.SacThueCollection.Add(item);
                    }
                    List<KDT_VNACC_TK_PhanHoi_TyGia> listTyGia = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
                    listTyGia = KDT_VNACC_TK_PhanHoi_TyGia.SelectCollectionDynamic("Master_ID =" + TKMD.ID, "ID");
                    foreach (KDT_VNACC_TK_PhanHoi_TyGia item in listTyGia)
                    {
                        TKMD.TyGiaCollection.Add(item);
                    }
                    return TKMD;
                }
            }
            return null;
        }
        public static KDT_VNACC_ToKhaiMauDich LoadBySoTK(string sotokhai)
        {
            string spName = "Select * from t_KDT_VNACC_ToKhaiMauDich where sotokhai =" + sotokhai;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            List<KDT_VNACC_ToKhaiMauDich> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }
        public static List<string> SelectListSoToKhai(string where)
        {
            List<string> myData = new List<string>();
            string spName = "Select SoToKhai from t_KDT_VNACC_ToKhaiMauDich where " + where;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                string a;
                a = Convert.ToString(reader.GetDecimal(0));
                myData.Add(a);
            }
            return myData;
        }

        public bool isToKhaiNhap()
        {
            if(!string.IsNullOrEmpty(this.MaLoaiHinh))
            {
                List<VNACC_Category_Common> listloaiHinh = VNACC_Category_Common.SelectCollectionDynamic("Code like '%" + this.MaLoaiHinh + "%'", null);
                if (listloaiHinh != null && listloaiHinh.Count > 0)
                {
                    if (listloaiHinh[0].ReferenceDB == "E001")
                        return true;
                }
            }
            return false;
        }

        public static long GetID(decimal SoToKhai,int NamCapPhep)
        {
            string Sql = string.Format(@"select ID FROM t_KDT_VNACC_ToKhaiMauDich WHERE SoToKhai = {0}
AND YEAR(NgayDangKy) = {1}",SoToKhai,NamCapPhep);
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(Sql);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj != null)
                return (long)obj;
            else
                return 0;
        }
        public static long GetID(decimal SoToKhai)
        {
            string Sql = string.Format(@"select ID FROM t_KDT_VNACC_ToKhaiMauDich WHERE SoToKhai = {0}", SoToKhai);
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(Sql);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj != null)
                return (long)obj;
            else
                return 0;
        }
        public static List<KDT_VNACC_ToKhaiMauDich> SelectCollectionDynamicBy(string whereCondition, string orderByExpression)
        {
            string Sql = string.Format(@"select * from t_KDT_VNACC_ToKhaiMauDich where Convert(varchar(12),SoToKhai) in(
select SoTiepNhan from t_KDT_VNACCS_MsgPhanBo where {0} order by {1}", whereCondition, orderByExpression);
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(Sql);
            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollection(reader);
        }

        public static List<KDT_VNACC_ToKhaiMauDich> SelectCollectionChuaThanhKhoan(bool isSXXK)
        {
            return SelectCollectionChuaThanhKhoan(false,isSXXK);
        }

        public static List<KDT_VNACC_ToKhaiMauDich> SelectCollectionChuaThanhKhoan(bool IsChuaThongQuan, bool isSXXK)
        {
            string Sql = string.Empty;
            if (isSXXK)
                Sql = string.Format(@"select * from t_KDT_VNACC_ToKhaiMauDich where SoToKhai > 0 and TrangThaiXuLy in (3" + (IsChuaThongQuan ? ",2" : "") + ") and  (SoToKhai not in (SELECT SoTKVNACCS FROM t_VNACCS_CapSoToKhai a INNER JOIN t_KDT_ToKhaiMauDich b ON a.SoTK = b.SoToKhai AND a.MaLoaiHinh COLLATE DATABASE_DEFAULT = Substring(b.MaLoaiHinh COLLATE DATABASE_DEFAULT ,3,3)INNER JOIN t_SXXK_ToKhaiMauDich c ON a.SoTK = c.SoToKhai AND a.MaLoaiHinh COLLATE DATABASE_DEFAULT = Substring(c.MaLoaiHinh COLLATE DATABASE_DEFAULT ,3,3)))");
            else
                {
                    Sql = string.Format(@"select * from t_KDT_VNACC_ToKhaiMauDich where SoToKhai > 0 and TrangThaiXuLy in (3" + (IsChuaThongQuan ? ",2" : "") + ") and  (SoToKhai not in (SELECT SoTKVNACCS  FROM t_VNACCS_CapSoToKhai a INNER JOIN t_KDT_ToKhaiMauDich b ON a.SoTK = b.SoToKhai AND a.MaLoaiHinh COLLATE DATABASE_DEFAULT = Substring(b.MaLoaiHinh COLLATE DATABASE_DEFAULT,3,3)) and  (SoToKhai not in (SELECT SoTKVNACCS FROM t_VNACCS_CapSoToKhai a INNER JOIN t_KDT_GC_ToKhaiChuyenTiep b ON a.SoTK = b.SoToKhai AND a.MaLoaiHinh COLLATE DATABASE_DEFAULT = Substring(b.MaLoaiHinh COLLATE DATABASE_DEFAULT,3,3))))");
                }

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(Sql);
            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollection(reader);
        }

        public static List<KDT_VNACC_ToKhaiMauDich> SelectCollectionGCChuaThanhKhoan(bool isAMAUpdate)
        {
            string Sql = string.Empty;
            if (isAMAUpdate)
            {
                Sql = string.Format(@"SELECT * FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE SoToKhai IN (SELECT SoToKhai FROM dbo.t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung WHERE TrangThaiXuLy IN (2,3)) AND Templ_1='1'");
            }
            else
            {
                Sql = string.Format(@"SELECT * FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE SoToKhai IN (SELECT SoToKhai FROM dbo.t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung WHERE TrangThaiXuLy IN (2,3)) AND Templ_1 IS NULL");
            }
            
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(Sql);
            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollection(reader);
        }

        public static List<KDT_VNACC_ToKhaiMauDich> SelectCollectionDaVaChuaThanhKhoan(bool isThongQuan)
        {
            string Sql = string.Format(@"select * from t_KDT_VNACC_ToKhaiMauDich where SoToKhai > 0 and TrangThaiXuLy in (3" + (isThongQuan ? ",2" : "") + ")");
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(Sql);
            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollection(reader);
        }
        public static List<KDT_VNACC_ToKhaiMauDich> SelectCollectionGCDaVaChuaThanhKhoan(bool isThongQuan,string namDangKy)
        {
            string Sql = string.Format(@"select * from t_KDT_VNACC_ToKhaiMauDich where SoToKhai > 0 and TrangThaiXuLy in (3" + (isThongQuan ? ",2" : "") + ") AND YEAR(NgayDangKy)=" + namDangKy + "");
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(Sql);
            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollection(reader);
        }
        public static List<KDT_VNACC_ToKhaiMauDich> SelectCollectionDaKhaiAMA()
        {
            string Sql = string.Format(@"SELECT * FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE SoToKhai <> 0 AND TrangThaiXuLy = 3 AND SoToKhai IN (SELECT SoToKhai FROM dbo.t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung)");
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(Sql);
            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollection(reader);
        }
        public static List<KDT_VNACC_ToKhaiMauDich> SelectCollectionDaDangKy(DateTime FromDate, DateTime ToDate)
        {
            string Sql = string.Format(@"select * from t_KDT_VNACC_ToKhaiMauDich where SoToKhai > 0 and TrangThaiXuLy in (2,3) AND (NgayDangKy between "+ FromDate.ToString("yyyy-MM-dd 00:00:00") + " And " + ToDate.ToString("yyyy-MM-dd 23:59:59") + "   ) ");
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(Sql);
            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollection(reader);
        }

        public static DataTable SelectCollectionAndHMD(string Where)
        {
            string Sql = string.Format(@"select * from t_KDT_VNACC_ToKhaiMauDich a inner join t_KDT_VNACC_HangMauDich b on a.ID = b.TKMD_ID 
where " + Where);
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(Sql);
            //IDataReader reader = db.ExecuteReader(dbCommand);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }



		
    }
}
