﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.SXXK.ToKhai;
using Company.Interface.Report.SXXK;
using DevExpress.XtraCharts;
using Company.Interface.Report;

namespace Company.Interface
{
    public partial class BaoCaoThongKeNuoc_Hang : Company.Interface.BaseForm
    {

        public ToKhaiMauDich tokhai = new ToKhaiMauDich();
        public HangMauDich hang = new HangMauDich();
        KimNgachXKMoiDoiTac_Nuoc report = new KimNgachXKMoiDoiTac_Nuoc();
        KimNgachXKMoiNuoc report1 = new KimNgachXKMoiNuoc();
        DataTable dtTenNuoc;
        DataTable dtGridSource;
        DataTable dtGridSource2;
        public BaoCaoThongKeNuoc_Hang()
        {
            InitializeComponent();
        }

        private void BaoCaoThongKeNuoc_Hang_Load(object sender, EventArgs e)
        {
            dtTenNuoc = tokhai.SelectTenNuocToKhaiXK().Tables[0];
            DataRow row;
            row = dtTenNuoc.NewRow();
            row["Ten"] =setText("Tất cả","All");
            row["NuocNK_ID"] = "";
            dtTenNuoc.Rows.Add(row);
            cboNuoc.DataSource = dtTenNuoc;
            cboNuoc.ValueMember = "NuocNK_ID";
            cboNuoc.DisplayMember = "Ten";            
            cboNuoc.SelectedIndex = 0;
            //thong tin
            txtDVBC.Text = Properties.Settings.Default.DonViBaoCao;
            txtNguoiDB.Text = Properties.Settings.Default.NguoiDuyetBieu;
            txtNguoiLB.Text = Properties.Settings.Default.NguoiLapBieu;
  
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {          
            string where = String.Empty;
            string nuoc= cboNuoc.SelectedItem.Value.ToString();
            dtGridSource = tokhai.SelectToKhai_HangMauDich_By(CbTuNgay.Value, CbDenNgay.Value, nuoc).Tables[0];
            dtGridSource2 = tokhai.SelectToKhai_HangMauDich_GroupByDoiTac(CbTuNgay.Value, CbDenNgay.Value, nuoc).Tables[0];
            if (ckbLuuMacdinh.Checked)
            {
                Properties.Settings.Default.NguoiLapBieu = txtNguoiLB.Text.ToUpper();
                Properties.Settings.Default.NguoiDuyetBieu = txtNguoiDB.Text.ToUpper();
                Properties.Settings.Default.DonViBaoCao = txtDVBC.Text.ToUpper();
                Properties.Settings.Default.Save();
                GlobalSettings.RefreshKey();
            }            
            if(dtGridSource.Rows.Count > 0 || dtGridSource2.Rows.Count >0){
                this.btnXemBC.Enabled = true;         
                this.btnView2.Enabled = true;
         
            }else{
                this.btnXemBC.Enabled = false ;         
                this.btnView2.Enabled = false;
                ShowMessage(setText("Không có kết quả thống kê cho trường hợp này", "No results for this case"), false);
            }            
            grdList.DataSource = dtGridSource;
            grdList2.DataSource = dtGridSource2;
  
        }

        private void btnXemBC_Click(object sender, EventArgs e)
        {
            ReportHangXKViewForm viewform1 = new ReportHangXKViewForm();
            viewform1.report = report1;
            report1.DataSource = dtGridSource;
            report1.tungay = CbTuNgay.Value;
            report1.denngay = CbDenNgay.Value;
            report1.NguoiLapBieu = txtNguoiLB.Text;
            report1.NguoiDuyetBieu = txtNguoiDB.Text;
            report1.DonViBaoCao = txtDVBC.Text;
            report1.BindData();        
            viewform1.Show();
        }


        private void btnViewChart_Click(object sender, EventArgs e)
        {
            Company.Interface.Report.ChartForm ChartFrm = new Company.Interface.Report.ChartForm();
            ChartFrm.data = dtGridSource;
            ChartFrm.Show();            
        }

        private void btnView2_Click(object sender, EventArgs e)
        {
            ReportHangXKViewForm viewform = new ReportHangXKViewForm();
            viewform.report = report;           
            report.DataSource = dtGridSource2;
            report.tungay = CbTuNgay.Value;
            report.denngay = CbDenNgay.Value;
            report.NguoiLapBieu = txtNguoiLB.Text;
            report.NguoiDuyetBieu = txtNguoiDB.Text;
            report.DonViBaoCao = txtDVBC.Text;
            report.BindData();
         //   report.ShowPreview();
            viewform.Show();
        }

        private void ckbLuuMacdinh_CheckedChanged(object sender, EventArgs e)
        {
           
        }

      

       
       

    }
}

