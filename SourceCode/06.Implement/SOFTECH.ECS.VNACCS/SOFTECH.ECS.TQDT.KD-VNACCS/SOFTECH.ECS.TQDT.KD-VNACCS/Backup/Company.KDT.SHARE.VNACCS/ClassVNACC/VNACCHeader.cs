﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public class VNACCHeader : BasicVNACC
    {
        /// <summary>
        /// (Always space)
        /// </summary>
        public PropertiesAttribute MaDieuKhienGD { set; get; }
        public PropertiesAttribute MaNghiepVu { get; set; }
        /// <summary>
        /// (Always space)
        /// </summary>
        public PropertiesAttribute VungDuTru_1 { set; get; }
        public PropertiesAttribute NguoiSuDung_Ma { get; set; }
        public PropertiesAttribute NguoiSuDung_ID { get; set; }
        public PropertiesAttribute NguoiSuDung_Pass { get; set; }
        public PropertiesAttribute TerminalID { get; set; }
        public PropertiesAttribute NguoiSuDung_DiaChi { set; get; }
        /// <summary>
        /// (Always space)
        /// </summary>
        public PropertiesAttribute VungDuTru_2 { set; get; }
        public PropertiesAttribute MessagesTag { get; set; }
        /// <summary>
        /// (Always space)
        /// </summary>
        public PropertiesAttribute VungDuTru_3 { set; get; }
        public PropertiesAttribute InputMessagesID { set; get; }
        public PropertiesAttribute IndexTag { get; set; }
        /// <summary>
        /// (Always space)
        /// </summary>
        public PropertiesAttribute VungDuTru_4 { set; get; }
        public PropertiesAttribute SystemID { get; set; }
        /// <summary>
        /// (Always space)
        /// </summary>
        public PropertiesAttribute VungDuTru_5 { set; get; }
        public PropertiesAttribute MessagesLength { set; get; }


        public VNACCHeader()
        { }
        public VNACCHeader(string NghiepVu, int LengthBody, string inputMessID)
        {
            MaNghiepVu = new PropertiesAttribute(5);
            MaNghiepVu.SetValue(NghiepVu);

            NguoiSuDung_Ma = new PropertiesAttribute(5);
            if (string.IsNullOrEmpty(GlobalVNACC.NguoiSuDung_Ma))
                throw new Exception("Bạn chưa cấu hình mã đơn vị khai báo (User-Code) !");
            NguoiSuDung_Ma.SetValue(GlobalVNACC.NguoiSuDung_Ma);

            NguoiSuDung_ID = new PropertiesAttribute(3);
            if (string.IsNullOrEmpty(GlobalVNACC.NguoiSuDung_ID))
                throw new Exception("Bạn chưa cấu hình ID người khai báo (User-ID) !");
            NguoiSuDung_ID.SetValue(GlobalVNACC.NguoiSuDung_ID);

            NguoiSuDung_Pass = new PropertiesAttribute(8);
            if(string.IsNullOrEmpty(GlobalVNACC.NguoiSuDung_Pass))
                throw new Exception("Bạn chưa cấu hình mật khẩu người khai báo (User-Password) !");
            NguoiSuDung_Pass.SetValue(GlobalVNACC.NguoiSuDung_Pass);

            TerminalID = new PropertiesAttribute(6);
            if (string.IsNullOrEmpty(GlobalVNACC.TerminalID))
                throw new Exception("Bạn chưa cấu hình ID máy khai báo (Terminal_ID)!");
            TerminalID.SetValue(GlobalVNACC.TerminalID);

            NguoiSuDung_DiaChi = new PropertiesAttribute(64);
            NguoiSuDung_DiaChi.SetValue(GlobalVNACC.NguoiSuDung_DiaChi);

            this.MessagesTag = new PropertiesAttribute(26);
            Random rd = new Random();
            this.MessagesTag.SetValue(GetMessagesTag(rd.Next(1, 10000)));

            this.VungDuTru_1 = new PropertiesAttribute(21);
            this.VungDuTru_2 = new PropertiesAttribute(104);
            this.VungDuTru_3 = new PropertiesAttribute(8);
            this.VungDuTru_4 = new PropertiesAttribute(1);
            this.VungDuTru_5 = new PropertiesAttribute(27);

            MaDieuKhienGD = new PropertiesAttribute(3);
            if (NghiepVu == "HYS")
                MaDieuKhienGD.SetValue("SS ");
            IndexTag = new PropertiesAttribute(100);

            SystemID = new PropertiesAttribute(1);
            SystemID.SetValue("1");

            this.MessagesLength = new PropertiesAttribute(6, typeof(string), 0);
            this.MessagesLength.SetValue((LengthBody + 400).ToString("000000"));

            InputMessagesID = new PropertiesAttribute(10);
            //InputMessagesID.SetValue(DateTime.Now.ToString("ddMMyyhhmm"));
            InputMessagesID.SetValue(inputMessID);
        }
        /// <summary>
        /// Load header by string response
        /// </summary>
        /// <param name="str"></param>
        public VNACCHeader(string str)
        {
            if (str.Length == 398)
            {
                MaDieuKhienGD = new PropertiesAttribute(3);
                MaDieuKhienGD.SetValue(str.Substring(0, 3));

                MaNghiepVu = new PropertiesAttribute(5);
                MaNghiepVu.SetValue(str.Substring(3, 5));

                this.VungDuTru_1 = new PropertiesAttribute(21);
                this.VungDuTru_1.SetValue(str.Substring(8, 21));

                NguoiSuDung_Ma = new PropertiesAttribute(5);
                NguoiSuDung_Ma.SetValue(str.Substring(29, 5));

                NguoiSuDung_ID = new PropertiesAttribute(3);
                NguoiSuDung_ID.SetValue(str.Substring(34, 3));

                NguoiSuDung_Pass = new PropertiesAttribute(8);
                NguoiSuDung_Pass.SetValue(str.Substring(37, 8));

                TerminalID = new PropertiesAttribute(6);
                TerminalID.SetValue(str.Substring(45, 6));

                NguoiSuDung_DiaChi = new PropertiesAttribute(64);
                NguoiSuDung_DiaChi.SetValue(str.Substring(51, 64));

                this.VungDuTru_2 = new PropertiesAttribute(104);
                this.VungDuTru_2.SetValue(str.Substring(115, 104));

                this.MessagesTag = new PropertiesAttribute(26);
                this.MessagesTag.SetValue(str.Substring(219, 26));
                this.VungDuTru_3 = new PropertiesAttribute(8);
                this.VungDuTru_3.SetValue(str.Substring(245, 8));

                InputMessagesID = new PropertiesAttribute(10);
                this.InputMessagesID.SetValue(str.Substring(253, 10));
                IndexTag = new PropertiesAttribute(100);
                this.IndexTag.SetValue(str.Substring(263, 100));

                this.VungDuTru_4 = new PropertiesAttribute(1);
                this.VungDuTru_4.SetValue(str.Substring(363, 1));

                SystemID = new PropertiesAttribute(1);
                SystemID.SetValue(str.Substring(364, 1));

                this.VungDuTru_5 = new PropertiesAttribute(27);
                this.VungDuTru_5.SetValue(str.Substring(365, 27));

                this.MessagesLength = new PropertiesAttribute(6, typeof(int), 0);
                this.MessagesLength.SetValue(str.Substring(392, 6));

            }
        }
        public string GetMessagesTag(int index)
        {
            //return "123456789";
            return this.TerminalID.Value.ToString() + DateTime.Now.ToString("yyyyMMddhhmmss") + index.ToString("000000");
        }
        
        
    }
}
