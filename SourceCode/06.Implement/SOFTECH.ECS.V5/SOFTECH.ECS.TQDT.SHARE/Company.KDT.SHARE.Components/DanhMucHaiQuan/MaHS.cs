﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Company.KDT.SHARE.Components.DanhMucHaiQuan
{
    public partial class MaHS
    {
        public static List<MaHS> ConvertDataSetToCollection(DataSet dataSet)
        {
            List<MaHS> collection = new List<MaHS>();

            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                MaHS entity = new MaHS();
                entity.Nhom = dataSet.Tables[0].Rows[i]["Nhom"].ToString();
                entity.PN1 = dataSet.Tables[0].Rows[i]["PN1"].ToString();
                entity.PN2 = dataSet.Tables[0].Rows[i]["PN2"].ToString();
                entity.Pn3 = dataSet.Tables[0].Rows[i]["Pn3"].ToString();
                entity.Mo_ta = dataSet.Tables[0].Rows[i]["Mo_ta"].ToString();
                entity.HS10SO = dataSet.Tables[0].Rows[i]["HS10SO"].ToString();
                entity.DateCreated = dataSet.Tables[0].Rows[i]["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateCreated"]) : DateTime.Today;
                entity.DateModified = dataSet.Tables[0].Rows[i]["DateModified"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateModified"]) : DateTime.Today;

                entity.MoTa_VN = dataSet.Tables[0].Rows[i]["MoTa_VN"].ToString();
                entity.DVT = dataSet.Tables[0].Rows[i]["DVT"].ToString();
                entity.MoTa_EN = dataSet.Tables[0].Rows[i]["MoTa_EN"].ToString();
                entity.MaTuongUng = dataSet.Tables[0].Rows[i]["MaTuongUng"].ToString();
                entity.ThueNKUD = Convert.ToDouble(dataSet.Tables[0].Rows[i]["ThueNKUD"].ToString());
                entity.ThueNKUDDB = Convert.ToDouble(dataSet.Tables[0].Rows[i]["ThueNKUDDB"].ToString());
                entity.ThueVAT = Convert.ToDouble(dataSet.Tables[0].Rows[i]["ThueVAT"].ToString());
                entity.ThueTTDB = Convert.ToDouble(dataSet.Tables[0].Rows[i]["ThueTTDB"].ToString());
                entity.ThueXK = Convert.ToDouble(dataSet.Tables[0].Rows[i]["ThueXK"].ToString());
                entity.ThueMT = Convert.ToDouble(dataSet.Tables[0].Rows[i]["ThueMT"].ToString());
                entity.GhiChu = dataSet.Tables[0].Rows[i]["GhiChu"].ToString();
                entity.ChinhSachMatHang = dataSet.Tables[0].Rows[i]["ChinhSachMatHang"].ToString();
                entity.QuanLyGia = dataSet.Tables[0].Rows[i]["QuanLyGia"].ToString();
                entity.ThuocNhom = dataSet.Tables[0].Rows[i]["ThuocNhom"].ToString();
                entity.GiayPhepNKKTD = dataSet.Tables[0].Rows[i]["GiayPhepNKKTD"].ToString();
                entity.GiayPhepNK = dataSet.Tables[0].Rows[i]["GiayPhepNK"].ToString();
                entity.KiemDich = dataSet.Tables[0].Rows[i]["KiemDich"].ToString();
                entity.PhanTichNguyCoGayHai = dataSet.Tables[0].Rows[i]["PhanTichNguyCoGayHai"].ToString();
                entity.VSATTP = dataSet.Tables[0].Rows[i]["VSATTP"].ToString();
                entity.HangHoaNhom2KTCL = dataSet.Tables[0].Rows[i]["HangHoaNhom2KTCL"].ToString();
                entity.CamNKQSD = dataSet.Tables[0].Rows[i]["CamNKQSD"].ToString();
                entity.CamNK = dataSet.Tables[0].Rows[i]["CamNK"].ToString();
                entity.CamXK = dataSet.Tables[0].Rows[i]["CamXK"].ToString();
                entity.HanNgachThueQuan = dataSet.Tables[0].Rows[i]["HanNgachThueQuan"].ToString();
                entity.QuanLyHoaChat = dataSet.Tables[0].Rows[i]["QuanLyHoaChat"].ToString();
                entity.HangTieuDungTT07 = dataSet.Tables[0].Rows[i]["HangTieuDungTT07"].ToString();
                entity.ChinhSachKhac = dataSet.Tables[0].Rows[i]["ChinhSachKhac"].ToString();

                collection.Add(entity);
            }

            return collection;
        }
    }
}
