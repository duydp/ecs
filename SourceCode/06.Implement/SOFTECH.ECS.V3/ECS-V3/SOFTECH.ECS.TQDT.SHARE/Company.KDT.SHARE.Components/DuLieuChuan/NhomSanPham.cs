using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.KDT.SHARE.Components.Utils;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public class NhomSanPham : BaseClass
    {
        public static DataSet SelectAll()
        {
            string sql = "select * from t_HaiQuan_DanhMucSanPhamGiaCong";
            DbCommand dbcom = db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(dbcom);
        }

        public string getDuKien(string masp)
        {
            //string sql = "select madukien from t_HaiQuan_DanhMucSanPhamGiaCong where MaSanPham=@MaSanPham";
            //DbCommand dbcom = db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbcom, "@MaSanPham", SqlDbType.VarChar);
            //object o = db.ExecuteScalar(dbcom);
            //if (o == null)
            //    return null;
            //return o.ToString();

            try
            {
                string query = string.Format("[MaSanPham] = '{0}'", masp);

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["NhomSanPham"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["madukien"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(masp, ex); }

            return string.Empty;
        }
        public static string getTenSanPham(string masp)
        {
            //string sql = "select TenSanPham from t_HaiQuan_DanhMucSanPhamGiaCong where MaSanPham=@MaSanPham";
            //DbCommand dbcom = db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbcom, "@MaSanPham", SqlDbType.VarChar, masp);
            //object o = db.ExecuteScalar(dbcom);
            //if (o == null)
            //    return null;
            //return o.ToString();

            try
            {
                string query = string.Format("[MaSanPham] = '{0}'", masp);

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["NhomSanPham"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["TenSanPham"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return string.Empty;
        }
        //ThoiLV Add extra
        public static string getTenNguyenPhuLieuDM(string manpl)
        {
            string sql = "select Ten from t_SXXK_NguyenPhuLieu where Ma=@Ma";
            DbCommand dbcom = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbcom, "@Ma", SqlDbType.VarChar, manpl);
            object o = db.ExecuteScalar(dbcom);
            if (o == null)
                return null;
            return o.ToString();
        }
        //ThoiLV Add extra

        #region Insert / Update methods.
        public bool InsertUpdate(DataTable table)
        {
            return this.InsertUpdateTransaction(table);
        }

        //---------------------------------------------------------------------------------------------
        public void InsertUpdateTransaction(string masp, string tensp, string dukien, string dvt_id, SqlTransaction transaction)
        {
            string spName = "p_t_HaiQuan_DanhMucSanPhamGiaCong_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, masp);
            db.AddInParameter(dbCommand, "@MaDuKien", SqlDbType.VarChar, dukien);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, dvt_id);
            db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, tensp);

            db.ExecuteNonQuery(dbCommand, transaction);


        }
        public bool InsertUpdateTransaction(DataTable table)
        {
            bool ret = false;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (DataRow row in table.Rows)
                    {
                        InsertUpdateTransaction(row[1].ToString(), FontConverter.TCVN2Unicode(row[2].ToString()), row[0].ToString(), row[5].ToString(), transaction);
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #endregion

        public static DataSet SelectAll(string Database)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(Database);
            string sql = "select * from sdanhmucgc";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(dbCommand);
        }

    }
}
