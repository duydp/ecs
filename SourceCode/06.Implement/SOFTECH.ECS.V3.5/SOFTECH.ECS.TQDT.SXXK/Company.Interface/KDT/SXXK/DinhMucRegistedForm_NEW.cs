using System;
using System.Data;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Collections;
using Company.Interface.KDT.SXXK;
using System.Drawing;
using Company.KDT.SHARE.Components;
using Company.Interface.SXXK.BangKeThanhLy;
using Company.BLL.KDT.SXXK;
namespace Company.Interface.SXXK
{
    public partial class DinhMucRegistedForm_NEW : BaseForm
    {

        
        public bool isBrower = false;
        
        public string maSP = "";
        string tenNPL = "";
        
        //-------------
        public SXXK_DinhMucMaHang dmmh = new SXXK_DinhMucMaHang();

        public DinhMucRegistedForm_NEW()
        {
            InitializeComponent();
        }

        public void BindData()
        {
            try
            {
                dgList.DataSource = SXXK_DinhMucMaHang.SelectDynamicDMRegisted(" MaSP <> '' ", "").Tables[0];
                dgList.Refresh();
                lblTongSP.Text = dgList.GetRows().Length.ToString();
            }
            catch (Exception ex)
            {

                //throw;
            }
            
            
        }

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
        }

        private void btnGetListFromHQ_Click(object sender, EventArgs e)
        {

        }

        //-----------------------------------------------------------------------------------------


        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record) { }
            //e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }
        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            DinhMucThanhKhoan dmtk = new DinhMucThanhKhoan();
            GridEXRow dr = dgList.CurrentRow;
            int id = int.Parse(dr.Cells["id"].Text);
            TheoDoiThanhKhoan_NPL_DM tk = new TheoDoiThanhKhoan_NPL_DM(id);
            tk.ShowDialog();

        }

        private void cmdAddNew_Click(object sender, EventArgs e)
        {

            DinhMucEditForm f = new DinhMucEditForm();
            f.OpenType = OpenFormType.Insert;
            f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            f.maSP = this.maSP;
            // f.DMCollection = this.dmCollection ;      

            f.ShowDialog(this);
            this.maSP = f.DMDetail.MaSanPHam;
            f.DMDetail.MaSanPHam = f.maSP;
            // this.dmCollection = f.DMCollection;
            // dgList.DataSource =f.DMCollection  ; 
            // dgList.Refetch();
            this.BindData();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtID.Text))
            {
                try
                {
                    dmmh = SXXK_DinhMucMaHang.Load(Int32.Parse(txtID.Text));
                    GridEXRow[] rows = dgList.GetDataRows();
                    dmmh.Invoid_HD = rows[0].Cells["Invoid_HD"].Text;
                    dmmh.NgayChungTu = Convert.ToDateTime(rows[0].Cells["NgayChungTu"].Text);
                    dmmh.Update();
                    MLMessages("Cập nhật thành công !", "MSG_SAV02", "", false);
                    btnSearch_Click(null, null);
                }
                catch
                {
                    MLMessages("Cập nhật không thành công !", "MSG_SAV01", "", false);
                }
            }
            else 
            {
                MLMessages("Bạn chưa chọn định mức sản phẩm.", "MSG_SAV01", "", false);
            }
            

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            this.Xoa();

        }
        // Deleting Method
        private void Xoa()
        {
            //int count = 0;
            //GridEXSelectedItemCollection items = dgList.SelectedItems;
            //if (items.Count <= 0) return;
            ////if (ShowMessage("Bạn có chắc chắn là xóa định mức này không ?", true) == "Yes")
            //if (MLMessages("Bạn có chắc chắn là xóa định mức này không ?", "MSG_DEL01", "", true) == "Yes")
            //{

            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            DinhMuc dinhmuc = (DinhMuc)i.GetRow().DataRow;
            //            dinhmuc.Delete();
            //            count++;
            //        }

            //    }
            //    if (count > 0)
            //    {
            //        if (GlobalSettings.NGON_NGU == "0")
            //            ShowMessage(count.ToString() + " định mức đã được xóa ", false);
            //        else
            //            ShowMessage(count.ToString() + " norm has been deleted ", false);
            //        ThongTinDinhMuc.Delete(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            //    }
            //    BindData();

            //}

        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //int count = 0;
            //GridEXSelectedItemCollection items = dgList.SelectedItems;
            //if (items.Count <= 0) return;
            //// if (ShowMessage("Bạn có chắc chắn là xóa các định mức này không ?", true) == "Yes")
            //if (MLMessages("Bạn có chắc chắn là xóa định mức này không ?", "MSG_DEL01", "", true) == "Yes")
            //{

            //    foreach (GridEXSelectedItem i in items)
            //    {
            //        if (i.RowType == RowType.Record)
            //        {
            //            DinhMuc dinhmuc = (DinhMuc)i.GetRow().DataRow;
            //            dinhmuc.Delete();
            //            count++;
            //        }

            //    }
            //    if (count > 0)
            //    {
            //        //ShowMessage(count.ToString() + " định mức đã được xóa ", false);
            //        if (GlobalSettings.NGON_NGU == "0")
            //            ShowMessage(count.ToString() + " định mức đã được xóa ", false);
            //        else
            //            ShowMessage(count.ToString() + " norm has been deleted ", false);
            //        ThongTinDinhMuc.Delete(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            //    }

            //}
            //else
            //    e.Cancel = true;
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            SelectSanPhamInDMForm f = new SelectSanPhamInDMForm();
            f.ShowDialog(this);
            if (f.spCollectionSelect.Count > 0)
            {
                Report.ReportViewDinhMucForm fDM = new Company.Interface.Report.ReportViewDinhMucForm();
                fDM.spCollection = f.spCollectionSelect;
                fDM.ShowDialog(this);
            }
        }

        private DataTable dtDM;
        private void searchNguoiKhaiBao(string userName)
        {
            //try
            //{
            //    dmCollection = new DinhMuc().GetDMFromUserName(userName);
            //    dgList.DataSource = dmCollection;
            //}
            //catch (Exception ex)
            //{
            //    ShowMessage("Có lỗi trong quá trình Tìm kiếm.\r\nChi tiết lỗi: " + ex.Message, false);
            //    return;
            //}
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string search = "";
            if (!string.IsNullOrEmpty(txtID.Text))
            {
                search = " ID = " + txtID.Text;
            }
            else
            {
                if (!string.IsNullOrEmpty(txtMaSP.Text))
                {
                    search += " MaSP like '%" + txtMaSP.Text + "%'";
                }
                if (!string.IsNullOrEmpty(txtMaNPL.Text) && search != "")
                    search += " AND MaNPL like '%" + txtMaNPL + "%'";
                else if (search == "")
                    search += " MaNPL like '%" + txtMaNPL.Text + "%'";
            }
            
            if (search == "")
                BindData();
            else
            {
                try
                {
                    dgList.DataSource = SXXK_DinhMucMaHang.SelectDynamicDMRegisted(search, "").Tables[0];
                    //dgList.Refetch();
                }
                catch (Exception ex)
                {

                    //throw;
                }
                
            }

            lblTongSP.Text = dgList.GetRows().Length.ToString();
            btnExportExcel.Enabled = true;
        }

        private void txtMaSP_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dgList_UpdatingCell(object sender, UpdatingCellEventArgs e)
        {

            //if (e.Column.Key == "DinhMucSuDung")
            //{
            //    if (Convert.ToDecimal(e.Value) > 1000000000)
            //    {
            //        //ShowMessage("Định mức sử dụng phải nhỏ hơn 1.000.000.000",false);
            //        if (GlobalSettings.NGON_NGU == "0")
            //            ShowMessage("Định mức sử dụng phải nhỏ hơn 1.000.000.000 ", false);
            //        else
            //            ShowMessage(" Used norm must be less than 1.000.000.000 ", false);
            //        e.Cancel = true;
            //    }
            //}
            //else
            //{
            //    if (Convert.ToDecimal(e.Value) > 100) e.Cancel = true;
            //}

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Xoa();
        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {
            //if (e.Row.RowType == RowType.Record)
            //{
            //    if (e.Row.Cells["TenNPL"].Text == "")
            //    {
            //        e.Row.RowStyle = new GridEXFormatStyle();
            //        e.Row.RowStyle.ForeColor = Color.Red;
            //    }
            //}
        }

        private void setDataToComboUserKB()
        {
            //DataTable dt = Company.QuanTri.User.SelectAll().Tables[0];
            //DataRow dr = dt.NewRow();
            //dr["USER_NAME"] = -1;
            //dr["HO_TEN"] = "--Tất cả--";
            //dt.Rows.InsertAt(dr, 0);
            //cbUserKB.DataSource = dt;
            //cbUserKB.DisplayMember = dt.Columns["HO_TEN"].ToString();
            //cbUserKB.ValueMember = dt.Columns["USER_NAME"].ToString();
        }

        private void cbUserKB_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cbUserKB.SelectedValue != null && cbUserKB.SelectedIndex != 0)
            //{
            //    txtMaSP.Text = "";
            //    txtMaSP.Enabled = false;
            //}
            //else
            //{
            //    txtMaSP.Text = "";
            //    txtMaSP.Enabled = true;
            //}
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DinhMucDaDangKy_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
        }

        private void chkGroup_CheckedChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    if (!chkGroup.Checked)
            //    {
            //        //Removing any group present in the table

            //        dgList.RootTable.Groups.Clear();

            //        dgList.RootTable.Columns["MaSP"].Visible = true;
            //    }
            //    else
            //    {
            //        //Grouping by columns ‘Country" and ‘City’ in the root table of a GridEX control.

            //        GridEXGroup group = new GridEXGroup();

            //        GridEXColumn column = new GridEXColumn();

            //        //Removing any group present in the table

            //        dgList.RootTable.Groups.Clear();

            //        //get the column to be grouped

            //        column = dgList.RootTable.Columns["MaSP"];

            //        //create the group

            //        group = new GridEXGroup(column, Janus.Windows.GridEX.SortOrder.Ascending);

            //        //add the group to the Groups collection of the table

            //        dgList.RootTable.Groups.Add(group);
            //    }
            //}
            //catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void DinhMucRegistedForm_NEW_Load(object sender, EventArgs e)
        {
            
            //BindData();
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void chiTietThanhKhoanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DinhMucThanhKhoan dmtk = new DinhMucThanhKhoan();
            GridEXRow dr = dgList.CurrentRow;
            int id = int.Parse(dr.Cells["id"].Text);
            TheoDoiThanhKhoan_NPL_DM tk = new TheoDoiThanhKhoan_NPL_DM(id);
            tk.ShowDialog();
        }

        private void txtID_Leave(object sender, EventArgs e)
        {
            try
            {
                int id = Int32.Parse(txtID.Text);
                cmdSave.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng trường ID","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Error);
                txtID.Text = string.Empty;
                cmdSave.Enabled = false;
            }
            
        }

        
        



    }
}