﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;

namespace Company.Interface.GC
{
    public partial class DinhMucRegistedDetailForm : BaseForm
    {
        public DinhMucCollection DMCollection = new DinhMucCollection();
        public SanPham SP;
        public DinhMucRegistedDetailForm()
        {
            InitializeComponent();
        }

        private void DinhMucRegistedDetailForm_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["NhuCau"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.DataSource = DMCollection;
            this.Text += " : "+SP.Ma;
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            decimal DinhMuc=Convert.ToDecimal(e.Row.Cells["DinhMucSuDung"].Text);
            decimal TyLeHH=Convert.ToDecimal(e.Row.Cells["TyLeHaoHut"].Text);
            decimal DinhMucChung=DinhMuc + DinhMuc*TyLeHH /100;
            e.Row.Cells["NhuCau"].Text = Convert.ToString(SP.SoLuongDangKy * DinhMucChung);

            if (e.Row.RowType == RowType.Record)
            {
                Company.GC.BLL.GC.DinhMuc DM = (Company.GC.BLL.GC.DinhMuc)e.Row.DataRow;
                if (DM.TenNPL != null && DM.TenNPL.Trim().Length == 0)
                {
                    Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
                    NPL.HopDong_ID = DM.HopDong_ID;
                    NPL.Ma = DM.MaNguyenPhuLieu;
                    NPL.Load();
                    DM.TenNPL = NPL.Ten;
                }


            }
        }
    }
}