using System;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data.SqlClient;

namespace Company.Interface.GC
{
    public partial class HangMauRegistedForm : BaseForm
    {

        public HangMau HangMauSelected = new HangMau();
        public bool isBrower = false;
        //2 hien da duyet nhung chua dieu chinh
        public int isDisplayAll = 0;//0 hien tat ca . 1 hien da duyet nhhung chua dieu chinh va bo sung .
        public bool khaiPKN06 = false;

        public HangMauRegistedForm()
        {
            InitializeComponent();
        }

        public void BindData()
        {
            if (isDisplayAll == 0)
                dgList.DataSource = HangMau.SelectCollectionDynamic("HopDong_ID=" + HangMauSelected.HopDong_ID, "");
            else if (isDisplayAll == 1)
            {
                dgList.DataSource = HangMau.SelectCollectionDynamic("HopDong_ID=" + HangMauSelected.HopDong_ID + " and (TrangThai=2 or trangthai=0)", "");             
            }
            else if (isDisplayAll == 2)
                dgList.DataSource = HangMau.SelectCollectionDynamic("HopDong_ID=" + HangMauSelected.HopDong_ID + " and trangthai=0", "");
        }

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];



            lblHint.Visible = this.CalledForm != string.Empty;
        }

        //-----------------------------------------------------------------------------------------

        private void HangMauRegistedForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            BindData();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------


        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (isBrower)
            {
                if (e.Row.RowType == RowType.Record)
                {
                    HangMauSelected = (HangMau)e.Row.DataRow;
                    this.Close();
                }
            }
            else
            {
                ;
            }
        }

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //-----------------------------------------------------------------------------------------
    }
}