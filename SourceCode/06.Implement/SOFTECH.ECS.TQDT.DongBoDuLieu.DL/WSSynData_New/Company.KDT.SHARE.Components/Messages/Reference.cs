﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Reference")]
    public class Reference
    {

        /// <summary>
        /// Phiên bản message
        /// </summary>
        [XmlElement("version")]
        public string Version { get; set; }        
        /// <summary>
        /// Định danh message Guid.NewGuid()
        /// </summary>
        [XmlElement("messageId")]
        public string MessageID { get; set; }
        [XmlElement("replyTo")]
        public string ReplyTo { get; set; }
    }
}
