﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CA_QuanLyGCN.Report;

namespace CA_QuanLyGCN
{
    public partial class CapGiayChungNhan : Form
    {
        public CapGiayChungNhan()
        {
            InitializeComponent();
        }
        public DoanhNghiep doanhnghiep { get; set; }
        public ChungNhan chungNhan { get; set; }
        private void CapGiayChungNhan_Load(object sender, EventArgs e)
        {
            if(this.doanhnghiep != null)
            {
                lblMSTDN.Text = doanhnghiep.Mst;
                lblTenDoanhNghiep.Text = doanhnghiep.Tendn;
            }

            //List<NganhNghe> nganhNgheColl = NganhNghe.ListAllNN();
            cmbNganhNgheKD.DataSource = NganhNghe.ListAllDataSet().Tables[0];
            cmbNganhNgheKD.ValueMember = "ID";
            cmbNganhNgheKD.DisplayMember = "TenNganhNghe";
            cmbNganhNgheKD.SelectedIndex = 0;

            if(chungNhan != null && chungNhan.Id > 0)
            {
                txtDiaChi.Text = chungNhan.DiaChi;
                txtTenCSKD.Text = chungNhan.TenCoSo;
                cmbNganhNgheKD.SelectedValue = chungNhan.IdNganhNghe;
                txtSoCN.Text = chungNhan.SoChungNhan;
                //ccNgay.Value = chungNhan.NgayChungNhan == null ? 
                if (!chungNhan.NgayChungNhan.HasValue)
                    ccNgay.Text = string.Empty;
                else
                    ccNgay.Value = chungNhan.NgayChungNhan.Value;
            }
            setCommandStatus();
        }
        private void setCommandStatus()
        {
            if(chungNhan != null && chungNhan.Id > 0)
            {
                btnIn.Enabled = true;
            }
            else
            {
                btnIn.Enabled = false;
                lblStatus.Text = "* Bạn phải lưu thông tin trước khi in";
            }
        }

        private void btnLuuThongTin_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtDiaChi.Text.Trim()))
                {
                    errorProvider1.SetError(txtDiaChi, "Bắt buộc nhập địa chỉ");
                    return;
                }
                if (string.IsNullOrEmpty(txtTenCSKD.Text.Trim()))
                {
                    errorProvider1.SetError(txtTenCSKD, "Tên cơ sở bắt buộc nhập");
                    return;
                }
                if (string.IsNullOrEmpty(txtSoCN.Text.Trim()))
                {
                    errorProvider1.SetError(txtTenCSKD, "Số giấy CN bắt buộc nhập");
                    return;
                }


                if (chungNhan == null) chungNhan = new ChungNhan();
                chungNhan.DoanhNghiepId = doanhnghiep.Id;
                chungNhan.IdNganhNghe = Convert.ToInt32(cmbNganhNgheKD.SelectedValue == null ? 0 : cmbNganhNgheKD.SelectedValue);
                chungNhan.TenCoSo = txtTenCSKD.Text.Trim();
                chungNhan.SoChungNhan = txtSoCN.Text.Trim();
                chungNhan.DiaChi = txtDiaChi.Text.Trim();
                chungNhan.NgayChungNhan = ccNgay.Value;
                if (chungNhan.Id > 0)
                    chungNhan.Update();
                else
                    chungNhan.Insert();
                setCommandStatus();
                MessageBox.Show("Lưu thông tin thành công");
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show("Lỗi xảy ra. Không lưu được thông tin");
            }
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            PrintFrm f = new PrintFrm();
            f.chungNhan = this.chungNhan;
            f.ShowDialog(this);

        
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            CauHinhFont f = new CauHinhFont();
            f.ShowDialog(this);
        }
    }
}
