using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.KDT.GC;
namespace Company.Interface.KDT.SXXK
{
    public partial class ThietBiReadExcelForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public ThietBiReadExcelForm()
        {
            InitializeComponent();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }
        private int ConvertCharToInt(char ch) 
        {
            return ch - 'A';
        }
        private int checkTBExit(string maTB)
        {
            for (int i = 0; i < this.HD.TBCollection.Count; i++)
            {
                if (this.HD.TBCollection[i].Ma == maTB) return i;
            }
            return -1;
        }

        private bool EmptyCheck(string st)
        {
            if (st == "") return true;
            return false;
        }
        private bool CheckNumber(decimal so)
        {
            if (so < 0) return true;
            return false;
        }
        private bool isSoLuongLimited(decimal so)
        {
            if (so < 0 || so > 10000000000000) return true;
            return false;
        }
        private bool isDonGiaLimited(double so)
        {
            if (so < 0 || so > 10000000000000) return true;
            return false;
        }
        private bool CheckNameDVT(string varcheck)
        {
            DataTable dt = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.SelectAll().Tables[0];
            bool ok = false;
            foreach (DataRow drcheck in dt.Rows)
            {
                if (varcheck.Equals(drcheck["Ten"].ToString()))
                {
                    ok = true;
                    break;
                }
            }
            return ok;
        }
        private bool CheckNuocXX(string varcheck)
        {
            DataTable dt = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.SelectAll().Tables[0];
            bool ok = false;
            foreach (DataRow drcheck in dt.Rows)
            {
                if (varcheck.Equals(drcheck["Ten"].ToString()))
                {
                    ok = true;
                    break;
                }
            }
            return ok;
           
        }
        private bool CheckNguyenTe(string varcheck)
        {
            DataTable dt = Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe.SelectAll().Tables[0];
            bool ok = false;
            foreach (DataRow drcheck in dt.Rows)
            {
                if (varcheck.Equals(drcheck["Ten"].ToString()))
                {
                    ok = true;
                    break;
                }
            }
            return ok;

        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if(!cvError.IsValid)return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;           
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text);
            }
            catch (Exception ex)
            {
                showMsg("MSG_0203008");
                //Message("MSG_EXC03", "", false);
                //ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại", false);
                return;
            }
            try
            {
               ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                showMsg("MSG_EXC01", txtSheet.Text);
                //Message("MSG_EXC01", "", false);
                //ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
                char maHangColumn ;
                int maHangCol = 0;
                char tenHangColumn ;
                int tenHangCol = 0;
                char maHSColumn;
                int maHSCol = 0;
                char dvtColumn ;
                int dvtCol = 0;
                char XuatXuColumn ;
                int XuatXuCol = 0;
                char SoLuongColumn ;
                int SoLuongCol = 0;
                char DonGiaColumn;
                int DonGiaCol =0;
                char NguyenTeColumn ;
                int NguyenTeCol = 0;
                char TinhTrangColumn;
                int TinhTrangCol = 0;
            try{
                  maHangColumn = Convert.ToChar(txtMaHangColumn.Text);
                  maHangCol = ConvertCharToInt(maHangColumn);
                  tenHangColumn = Convert.ToChar(txtTenHangColumn.Text);
                  tenHangCol = ConvertCharToInt(tenHangColumn);
                  maHSColumn = Convert.ToChar(txtMaHSColumn.Text);
                  maHSCol = ConvertCharToInt(maHSColumn);
                  dvtColumn = Convert.ToChar(txtDVTColumn.Text);
                  dvtCol = ConvertCharToInt(dvtColumn);

                  XuatXuColumn = Convert.ToChar(txtXuatXu.Text);
                  XuatXuCol = ConvertCharToInt(XuatXuColumn);
                  SoLuongColumn = Convert.ToChar(txtSoLuong.Text);
                  SoLuongCol = ConvertCharToInt(SoLuongColumn);
                  DonGiaColumn = Convert.ToChar(txtDonGia.Text);
                  DonGiaCol = ConvertCharToInt(DonGiaColumn);
                  NguyenTeColumn = Convert.ToChar(txtNguyenTe.Text);
                  NguyenTeCol = ConvertCharToInt(NguyenTeColumn);
                  TinhTrangColumn = Convert.ToChar(txtghiChu.Text);
                  TinhTrangCol = ConvertCharToInt(TinhTrangColumn);
             }
             catch { return; }
             int j = 0;
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    j = wsr.Index + 1;
                    try
                    {
                        ThietBi tb = new ThietBi();
                        //if ( EmptyCheck( Convert.ToString(wsr.Cells[TinhTrangCol].Value.ToString())) || isDonGiaLimited(Convert.ToDouble(wsr.Cells[DonGiaCol].Value)) || isSoLuongLimited(Convert.ToDecimal(wsr.Cells[SoLuongCol].Value)) || CheckNguyenTe(Convert.ToString(wsr.Cells[NguyenTeCol].Value.ToString()))||CheckNuocXX(Convert.ToString(wsr.Cells[dvtCol].Value.ToString()))|| CheckNumber(Convert.ToDecimal(wsr.Cells[SoLuongCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[maHangCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[SoLuongCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[maHSCol].Value)) || CheckNameDVT(Convert.ToString(wsr.Cells[dvtCol].Value)))
                        //{
                        //    ShowMessage(" Dữ liệu tài dòng " + (wsr.Index + 1) + " sai, được bỏ qua", false);
                        //    continue;
                        //}
                        tb.Ma = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                        tb.Ten = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                        tb.MaHS = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                        try
                        {
                            tb.DVT_ID = DonViTinh_GetID(Convert.ToString(wsr.Cells[dvtCol].Value.ToString().ToUpper()));
                        }
                        catch
                        {
                            showMsg("MSG_0203010", Convert.ToString(wsr.Cells[dvtCol].Value));
                            //MLMessages("Đơn vị tính : " + Convert.ToString(wsr.Cells[dvtCol].Value) + " không có trong hệ thống.","MSG_WRN10",Convert.ToString(wsr.Cells[dvtCol].Value), false);
                            return;
                        }
                        tb.NuocXX_ID = Convert.ToString(wsr.Cells[XuatXuCol].Value).Trim();
                        tb.SoLuongDangKy = Convert.ToDecimal(wsr.Cells[SoLuongCol].Value);
                        tb.DonGia = Convert.ToDouble(wsr.Cells[DonGiaCol].Value);
                        tb.TriGia = Convert.ToDouble(tb.SoLuongDangKy) * tb.DonGia;
                        tb.NguyenTe_ID = (Convert.ToString(wsr.Cells[NguyenTeCol].Value.ToString().ToUpper())).PadRight(3);
                        tb.TinhTrang = Convert.ToString(wsr.Cells[TinhTrangCol].Value).Trim();
                        if (checkTBExit(tb.Ma) >= 0)
                        {
                            int k = wsr.Index + 1;
                            if (showMsg("MSG_0203024", new String[] { tb.Ma, k.ToString() }, true) == "Yes")
                                //if (MLMessages("Thiết bị có mã \"" + tb.Ma + "\" thuộc dòng " + (wsr.Index + 1) + " trong file Excel đã có trên lưới. Bạn có muốn thay thế thiết bị trên lưới bằng thiết bị này?", "MSG_EXC06", j.ToString(), true) == "Yes")
                                this.HD.TBCollection[checkTBExit(tb.Ma)] = tb;

                        }
                        else this.HD.TBCollection.Add(tb);
                    }
                    catch
                    {
                        if (showMsg("MSG_0203014", wsr.Index + 1, true) != "Yes")
                        //if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", "MSG_EXC07",j.ToString(), true) != "Yes")
                        {
                            break;
                        }
                    }
                }
            }
            this.Close();
        }

        private void SanPhamReadExcelForm_Load(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }

        private void lblLinkExcel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_GC("TB_HD");
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}