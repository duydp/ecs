﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Company.Interface.KDT.GC
{
    partial class HopDongEditForm
    {
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HopDongEditForm));
            Janus.Windows.GridEX.GridEXLayout dgNhomSanPham_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgNhomSanPham_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgNguyenPhuLieu_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgNguyenPhuLieu_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgSanPham_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgSanPham_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgThietBi_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgThietBi_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgHangMau_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgHangMau_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.txtDVDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dtNguyenTe = new System.Data.DataTable();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dtNuocThueGC = new System.Data.DataTable();
            this.dtDonViHaiQuan = new System.Data.DataTable();
            this.dataColumn7 = new System.Data.DataColumn();
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdAddThietBi = new Janus.Windows.UI.CommandBars.UICommand("cmdAddThietBi");
            this.cmdAddThemPhuKien = new Janus.Windows.UI.CommandBars.UICommand("cmdAddThemPhuKien");
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmMainHDGC = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbTopBar = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.NhapHang1 = new Janus.Windows.UI.CommandBars.UICommand("NhapHang");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.NhanDuLieuHD1 = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieuHD");
            this.cmdCopyHD1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCopyHD");
            this.InPhieuTN1 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateGuidString1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdAddLoaiSanPham = new Janus.Windows.UI.CommandBars.UICommand("cmdAddLoaiSanPham");
            this.cmdAddNguyenPhuLieu = new Janus.Windows.UI.CommandBars.UICommand("cmdAddNguyenPhuLieu");
            this.cmdAddSanPham = new Janus.Windows.UI.CommandBars.UICommand("cmdAddSanPham");
            this.HuyKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("HuyKhaiBao");
            this.NhanDuLieuHD = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieuHD");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.NhapHang = new Janus.Windows.UI.CommandBars.UICommand("NhapHang");
            this.cmdAddLoaiSanPham1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAddLoaiSanPham");
            this.cmdAddNguyenPhuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAddNguyenPhuLieu");
            this.cmdAddSanPham1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAddSanPham");
            this.cmdAddThietBi1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAddThietBi");
            this.cmdAddHangMau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAddHangMau");
            this.cmdExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExcel");
            this.cmdExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdExcel");
            this.cmdNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLExcel");
            this.cmdSPExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSPExcel");
            this.cmdThietBi1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThietBi");
            this.cmdLoaiSPGCExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdLoaiSPGCExcel");
            this.cmdNPLExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLExcel");
            this.cmdSPExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdSPExcel");
            this.cmdThietBiExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdThietBi");
            this.cmdCopyHD = new Janus.Windows.UI.CommandBars.UICommand("cmdCopyHD");
            this.InPhieuTN = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdEdit = new Janus.Windows.UI.CommandBars.UICommand("cmdEdit");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateResult");
            this.cmdHistory1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHistory");
            this.cmdUpdateGuidString = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdSendEdit = new Janus.Windows.UI.CommandBars.UICommand("cmdSendEdit");
            this.cmdAddHangMau = new Janus.Windows.UI.CommandBars.UICommand("cmdAddHangMau");
            this.cmdUpdateResult = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateResult");
            this.cmdHistory = new Janus.Windows.UI.CommandBars.UICommand("cmdHistory");
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.tabHopDong = new Janus.Windows.UI.Tab.UITab();
            this.tpNhomSanPham = new Janus.Windows.UI.Tab.UITabPage();
            this.dgNhomSanPham = new Janus.Windows.GridEX.GridEX();
            this.tpNguyenPhuLieu = new Janus.Windows.UI.Tab.UITabPage();
            this.dgNguyenPhuLieu = new Janus.Windows.GridEX.GridEX();
            this.tpSanPham = new Janus.Windows.UI.Tab.UITabPage();
            this.dgSanPham = new Janus.Windows.GridEX.GridEX();
            this.tpThietBi = new Janus.Windows.UI.Tab.UITabPage();
            this.dgThietBi = new Janus.Windows.GridEX.GridEX();
            this.tpHangMau = new Janus.Windows.UI.Tab.UITabPage();
            this.dgHangMau = new Janus.Windows.GridEX.GridEX();
            this.dataColumn2 = new System.Data.DataColumn();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.nguyenTeControl1 = new Company.Interface.Controls.NguyenTeControl();
            this.nuocHControl1 = new Company.Interface.Controls.NuocHControl();
            this.donViHaiQuanControl1 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.txtTongGiaTriSP = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ccNgayKetThucHD = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtTongTriGiaTienCong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cbbIsGC = new Janus.Windows.EditControls.UIComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbPTTT = new Janus.Windows.EditControls.UIComboBox();
            this.ccNgayKyHD = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label26 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtDCDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTenDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaChiBenNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtTenBenNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMaBenNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.grpThongTinKhai = new Janus.Windows.EditControls.UIGroupBox();
            this.picHelps = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ccNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label11 = new System.Windows.Forms.Label();
            this.ds = new System.Data.DataSet();
            this.dtLoaiSanPham = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNuocThueGC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMainHDGC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTopBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabHopDong)).BeginInit();
            this.tabHopDong.SuspendLayout();
            this.tpNhomSanPham.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNhomSanPham)).BeginInit();
            this.tpNguyenPhuLieu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNguyenPhuLieu)).BeginInit();
            this.tpSanPham.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSanPham)).BeginInit();
            this.tpThietBi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgThietBi)).BeginInit();
            this.tpHangMau.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgHangMau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinKhai)).BeginInit();
            this.grpThongTinKhai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHelps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiSanPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 783), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 35);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 783);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 759);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 759);
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(203, 35);
            this.grbMain.Size = new System.Drawing.Size(1087, 783);
            // 
            // txtDVDT
            // 
            this.txtDVDT.BackColor = System.Drawing.Color.White;
            this.txtDVDT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVDT.Location = new System.Drawing.Point(68, 18);
            this.txtDVDT.MaxLength = 14;
            this.txtDVDT.Name = "txtDVDT";
            this.txtDVDT.Size = new System.Drawing.Size(341, 22);
            this.txtDVDT.TabIndex = 13;
            this.txtDVDT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDVDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDVDT.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // dataColumn11
            // 
            this.dataColumn11.AllowDBNull = false;
            this.dataColumn11.ColumnName = "nguyente_Ma";
            // 
            // dataColumn12
            // 
            this.dataColumn12.ColumnName = "nguyente_Ten";
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "nuoc_Ten";
            // 
            // dtNguyenTe
            // 
            this.dtNguyenTe.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn11,
            this.dataColumn12});
            this.dtNguyenTe.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nguyente_Ma"}, true)});
            this.dtNguyenTe.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn11};
            this.dtNguyenTe.TableName = "NguyenTe";
            // 
            // dataColumn9
            // 
            this.dataColumn9.AllowDBNull = false;
            this.dataColumn9.ColumnName = "nuoc_Ma";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "lsp_Gia";
            this.dataColumn5.DataType = typeof(decimal);
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "donvitinh_Ten";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "donvitinh_Ma";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "lsp_SoLuong";
            this.dataColumn4.DataType = typeof(decimal);
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "donvihaiquan_Ten";
            // 
            // dtNuocThueGC
            // 
            this.dtNuocThueGC.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn9,
            this.dataColumn10});
            this.dtNuocThueGC.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nuoc_Ma"}, true)});
            this.dtNuocThueGC.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn9};
            this.dtNuocThueGC.TableName = "NuocThueGC";
            // 
            // dtDonViHaiQuan
            // 
            this.dtDonViHaiQuan.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn7,
            this.dataColumn8});
            this.dtDonViHaiQuan.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "donvihaiquan_Ma"}, true)});
            this.dtDonViHaiQuan.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn7};
            this.dtDonViHaiQuan.TableName = "DonViHaiQuan";
            // 
            // dataColumn7
            // 
            this.dataColumn7.AllowDBNull = false;
            this.dataColumn7.ColumnName = "donvihaiquan_Ma";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSave.Text = "Lưu thông tin";
            this.cmdSave.ToolTipText = "Lưu thông tin (Ctrl + S)";
            // 
            // cmdSend
            // 
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Shortcut = System.Windows.Forms.Shortcut.CtrlG;
            this.cmdSend.Text = "Gửi thông tin";
            this.cmdSend.ToolTipText = "Gửi thông tin (Ctrl + G)";
            // 
            // cmdAddThietBi
            // 
            this.cmdAddThietBi.Image = ((System.Drawing.Image)(resources.GetObject("cmdAddThietBi.Image")));
            this.cmdAddThietBi.Key = "cmdAddThietBi";
            this.cmdAddThietBi.Name = "cmdAddThietBi";
            this.cmdAddThietBi.Shortcut = System.Windows.Forms.Shortcut.Ctrl4;
            this.cmdAddThietBi.Text = "Thêm Thiết Bị";
            this.cmdAddThietBi.ToolTipText = "Thêm Thiết Bị (Ctrl + 4)";
            // 
            // cmdAddThemPhuKien
            // 
            this.cmdAddThemPhuKien.Image = ((System.Drawing.Image)(resources.GetObject("cmdAddThemPhuKien.Image")));
            this.cmdAddThemPhuKien.Key = "cmdAddThemPhuKien";
            this.cmdAddThemPhuKien.Name = "cmdAddThemPhuKien";
            this.cmdAddThemPhuKien.Shortcut = System.Windows.Forms.Shortcut.Ctrl5;
            this.cmdAddThemPhuKien.Text = "Thêm phụ kiên";
            this.cmdAddThemPhuKien.ToolTipText = "Thêm phụ kiên (Ctrl + 5)";
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMainHDGC;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(638, 32);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 356);
            // 
            // cmMainHDGC
            // 
            this.cmMainHDGC.BottomRebar = this.BottomRebar1;
            this.cmMainHDGC.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbTopBar});
            this.cmMainHDGC.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdAddLoaiSanPham,
            this.cmdAddNguyenPhuLieu,
            this.cmdAddSanPham,
            this.cmdAddThietBi,
            this.cmdAddThemPhuKien,
            this.cmdSave,
            this.cmdSend,
            this.HuyKhaiBao,
            this.NhanDuLieuHD,
            this.XacNhan,
            this.NhapHang,
            this.cmdExcel,
            this.cmdLoaiSPGCExcel,
            this.cmdNPLExcel,
            this.cmdSPExcel,
            this.cmdThietBiExcel,
            this.cmdCopyHD,
            this.InPhieuTN,
            this.cmdEdit,
            this.cmdResult,
            this.cmdUpdateGuidString,
            this.cmdSendEdit,
            this.cmdAddHangMau,
            this.cmdUpdateResult,
            this.cmdHistory});
            this.cmMainHDGC.ContainerControl = this;
            this.cmMainHDGC.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMainHDGC.ImageList = this.ImageList1;
            this.cmMainHDGC.LeftRebar = this.LeftRebar1;
            this.cmMainHDGC.LockCommandBars = true;
            this.cmMainHDGC.RightRebar = this.RightRebar1;
            this.cmMainHDGC.ShowShortcutInToolTips = true;
            this.cmMainHDGC.TopRebar = this.TopRebar1;
            this.cmMainHDGC.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMainHDGC.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmMainHDGC.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMainHDGC;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 388);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(638, 0);
            // 
            // cmbTopBar
            // 
            this.cmbTopBar.CommandManager = this.cmMainHDGC;
            this.cmbTopBar.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.NhapHang1,
            this.cmdSave1,
            this.cmdSend1,
            this.NhanDuLieuHD1,
            this.cmdCopyHD1,
            this.InPhieuTN1,
            this.cmdResult1,
            this.cmdUpdateGuidString1});
            this.cmbTopBar.Key = "cmbTopBar";
            this.cmbTopBar.Location = new System.Drawing.Point(0, 0);
            this.cmbTopBar.Name = "cmbTopBar";
            this.cmbTopBar.RowIndex = 0;
            this.cmbTopBar.Size = new System.Drawing.Size(1001, 32);
            this.cmbTopBar.Text = "cmbTopBar";
            // 
            // NhapHang1
            // 
            this.NhapHang1.Key = "NhapHang";
            this.NhapHang1.Name = "NhapHang1";
            this.NhapHang1.Text = "Nhậ&p hàng";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            this.cmdSave1.Shortcut = System.Windows.Forms.Shortcut.CtrlL;
            this.cmdSave1.Text = "&Lưu thông tin";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            this.cmdSend1.Shortcut = System.Windows.Forms.Shortcut.CtrlK;
            this.cmdSend1.Text = "&Khai báo";
            // 
            // NhanDuLieuHD1
            // 
            this.NhanDuLieuHD1.Key = "NhanDuLieuHD";
            this.NhanDuLieuHD1.Name = "NhanDuLieuHD1";
            this.NhanDuLieuHD1.Shortcut = System.Windows.Forms.Shortcut.CtrlN;
            this.NhanDuLieuHD1.Text = "&Nhận dữ liệu";
            // 
            // cmdCopyHD1
            // 
            this.cmdCopyHD1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCopyHD1.Image")));
            this.cmdCopyHD1.Key = "cmdCopyHD";
            this.cmdCopyHD1.Name = "cmdCopyHD1";
            this.cmdCopyHD1.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdCopyHD1.Text = "&Sao chép HĐ";
            // 
            // InPhieuTN1
            // 
            this.InPhieuTN1.Image = ((System.Drawing.Image)(resources.GetObject("InPhieuTN1.Image")));
            this.InPhieuTN1.Key = "InPhieuTN";
            this.InPhieuTN1.Name = "InPhieuTN1";
            // 
            // cmdResult1
            // 
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            // 
            // cmdUpdateGuidString1
            // 
            this.cmdUpdateGuidString1.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString1.Name = "cmdUpdateGuidString1";
            // 
            // cmdAddLoaiSanPham
            // 
            this.cmdAddLoaiSanPham.Image = ((System.Drawing.Image)(resources.GetObject("cmdAddLoaiSanPham.Image")));
            this.cmdAddLoaiSanPham.Key = "cmdAddLoaiSanPham";
            this.cmdAddLoaiSanPham.Name = "cmdAddLoaiSanPham";
            this.cmdAddLoaiSanPham.Shortcut = System.Windows.Forms.Shortcut.Ctrl0;
            this.cmdAddLoaiSanPham.Text = "Thêm Loại SPGC";
            this.cmdAddLoaiSanPham.ToolTipText = "Thêm Loại SPGC (Ctrl + 1)";
            // 
            // cmdAddNguyenPhuLieu
            // 
            this.cmdAddNguyenPhuLieu.Image = ((System.Drawing.Image)(resources.GetObject("cmdAddNguyenPhuLieu.Image")));
            this.cmdAddNguyenPhuLieu.Key = "cmdAddNguyenPhuLieu";
            this.cmdAddNguyenPhuLieu.Name = "cmdAddNguyenPhuLieu";
            this.cmdAddNguyenPhuLieu.Shortcut = System.Windows.Forms.Shortcut.Ctrl2;
            this.cmdAddNguyenPhuLieu.Text = "Thêm Nguyên Phụ Liệu";
            this.cmdAddNguyenPhuLieu.ToolTipText = "Thêm Nguyên Phụ Liệu (Ctrl + 2)";
            // 
            // cmdAddSanPham
            // 
            this.cmdAddSanPham.Image = ((System.Drawing.Image)(resources.GetObject("cmdAddSanPham.Image")));
            this.cmdAddSanPham.Key = "cmdAddSanPham";
            this.cmdAddSanPham.Name = "cmdAddSanPham";
            this.cmdAddSanPham.Shortcut = System.Windows.Forms.Shortcut.Ctrl3;
            this.cmdAddSanPham.Text = "Thêm Sản Phẩm";
            this.cmdAddSanPham.ToolTipText = "Thêm Sản Phẩm (Ctrl + 3)";
            // 
            // HuyKhaiBao
            // 
            this.HuyKhaiBao.Image = ((System.Drawing.Image)(resources.GetObject("HuyKhaiBao.Image")));
            this.HuyKhaiBao.Key = "HuyKhaiBao";
            this.HuyKhaiBao.Name = "HuyKhaiBao";
            this.HuyKhaiBao.Text = "Khai báo hủy";
            // 
            // NhanDuLieuHD
            // 
            this.NhanDuLieuHD.Image = ((System.Drawing.Image)(resources.GetObject("NhanDuLieuHD.Image")));
            this.NhanDuLieuHD.Key = "NhanDuLieuHD";
            this.NhanDuLieuHD.Name = "NhanDuLieuHD";
            this.NhanDuLieuHD.Text = "Nhận dữ liệu";
            // 
            // XacNhan
            // 
            this.XacNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan.Icon")));
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận thông tin";
            // 
            // NhapHang
            // 
            this.NhapHang.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdAddLoaiSanPham1,
            this.cmdAddNguyenPhuLieu1,
            this.cmdAddSanPham1,
            this.cmdAddThietBi1,
            this.cmdAddHangMau1,
            this.cmdExcel1});
            this.NhapHang.Image = ((System.Drawing.Image)(resources.GetObject("NhapHang.Image")));
            this.NhapHang.Key = "NhapHang";
            this.NhapHang.Name = "NhapHang";
            this.NhapHang.Text = "Nhập hàng";
            // 
            // cmdAddLoaiSanPham1
            // 
            this.cmdAddLoaiSanPham1.Key = "cmdAddLoaiSanPham";
            this.cmdAddLoaiSanPham1.Name = "cmdAddLoaiSanPham1";
            this.cmdAddLoaiSanPham1.Shortcut = System.Windows.Forms.Shortcut.Ctrl1;
            this.cmdAddLoaiSanPham1.ToolTipText = "Thêm Loại SPGC";
            // 
            // cmdAddNguyenPhuLieu1
            // 
            this.cmdAddNguyenPhuLieu1.Key = "cmdAddNguyenPhuLieu";
            this.cmdAddNguyenPhuLieu1.Name = "cmdAddNguyenPhuLieu1";
            this.cmdAddNguyenPhuLieu1.ToolTipText = "Thêm Nguyên Phụ Liệu";
            // 
            // cmdAddSanPham1
            // 
            this.cmdAddSanPham1.Key = "cmdAddSanPham";
            this.cmdAddSanPham1.Name = "cmdAddSanPham1";
            this.cmdAddSanPham1.ToolTipText = "Thêm Sản Phẩm";
            // 
            // cmdAddThietBi1
            // 
            this.cmdAddThietBi1.Key = "cmdAddThietBi";
            this.cmdAddThietBi1.Name = "cmdAddThietBi1";
            this.cmdAddThietBi1.ToolTipText = "Thêm Thiết Bị";
            // 
            // cmdAddHangMau1
            // 
            this.cmdAddHangMau1.Key = "cmdAddHangMau";
            this.cmdAddHangMau1.Name = "cmdAddHangMau1";
            // 
            // cmdExcel1
            // 
            this.cmdExcel1.Key = "cmdExcel";
            this.cmdExcel1.Name = "cmdExcel1";
            this.cmdExcel1.Shortcut = System.Windows.Forms.Shortcut.Ctrl5;
            this.cmdExcel1.ToolTipText = "Nhập từ Excel";
            // 
            // cmdExcel
            // 
            this.cmdExcel.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdNPL1,
            this.cmdSPExcel1,
            this.cmdThietBi1});
            this.cmdExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdExcel.Image")));
            this.cmdExcel.Key = "cmdExcel";
            this.cmdExcel.Name = "cmdExcel";
            this.cmdExcel.Text = "Nhập từ Excel";
            // 
            // cmdNPL1
            // 
            this.cmdNPL1.Key = "cmdNPLExcel";
            this.cmdNPL1.Name = "cmdNPL1";
            this.cmdNPL1.Shortcut = System.Windows.Forms.Shortcut.CtrlQ;
            // 
            // cmdSPExcel1
            // 
            this.cmdSPExcel1.Key = "cmdSPExcel";
            this.cmdSPExcel1.Name = "cmdSPExcel1";
            this.cmdSPExcel1.Shortcut = System.Windows.Forms.Shortcut.CtrlR;
            // 
            // cmdThietBi1
            // 
            this.cmdThietBi1.Image = ((System.Drawing.Image)(resources.GetObject("cmdThietBi1.Image")));
            this.cmdThietBi1.Key = "cmdThietBi";
            this.cmdThietBi1.Name = "cmdThietBi1";
            this.cmdThietBi1.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            // 
            // cmdLoaiSPGCExcel
            // 
            this.cmdLoaiSPGCExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdLoaiSPGCExcel.Image")));
            this.cmdLoaiSPGCExcel.Key = "cmdLoaiSPGCExcel";
            this.cmdLoaiSPGCExcel.Name = "cmdLoaiSPGCExcel";
            this.cmdLoaiSPGCExcel.Text = "Thêm Loại SPGC";
            // 
            // cmdNPLExcel
            // 
            this.cmdNPLExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdNPLExcel.Image")));
            this.cmdNPLExcel.Key = "cmdNPLExcel";
            this.cmdNPLExcel.Name = "cmdNPLExcel";
            this.cmdNPLExcel.Text = "Thêm Nguyên Phụ Liệu";
            // 
            // cmdSPExcel
            // 
            this.cmdSPExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdSPExcel.Image")));
            this.cmdSPExcel.Key = "cmdSPExcel";
            this.cmdSPExcel.Name = "cmdSPExcel";
            this.cmdSPExcel.Text = "Thêm Sản Phẩm";
            // 
            // cmdThietBiExcel
            // 
            this.cmdThietBiExcel.Key = "cmdThietBi";
            this.cmdThietBiExcel.Name = "cmdThietBiExcel";
            this.cmdThietBiExcel.Text = "Thêm Thiết Bị";
            // 
            // cmdCopyHD
            // 
            this.cmdCopyHD.Key = "cmdCopyHD";
            this.cmdCopyHD.Name = "cmdCopyHD";
            this.cmdCopyHD.Text = "Sao chép HĐ";
            // 
            // InPhieuTN
            // 
            this.InPhieuTN.Image = ((System.Drawing.Image)(resources.GetObject("InPhieuTN.Image")));
            this.InPhieuTN.Key = "InPhieuTN";
            this.InPhieuTN.Name = "InPhieuTN";
            this.InPhieuTN.Shortcut = System.Windows.Forms.Shortcut.CtrlI;
            this.InPhieuTN.Text = "&In phiếu tiếp nhận";
            // 
            // cmdEdit
            // 
            this.cmdEdit.Image = ((System.Drawing.Image)(resources.GetObject("cmdEdit.Image")));
            this.cmdEdit.Key = "cmdEdit";
            this.cmdEdit.Name = "cmdEdit";
            this.cmdEdit.Text = "Chuyển Khai báo sửa";
            // 
            // cmdResult
            // 
            this.cmdResult.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdUpdateResult1,
            this.cmdHistory1});
            this.cmdResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult.Image")));
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "Kết quả xử lý";
            // 
            // cmdUpdateResult1
            // 
            this.cmdUpdateResult1.Key = "cmdUpdateResult";
            this.cmdUpdateResult1.Name = "cmdUpdateResult1";
            // 
            // cmdHistory1
            // 
            this.cmdHistory1.Key = "cmdHistory";
            this.cmdHistory1.Name = "cmdHistory1";
            // 
            // cmdUpdateGuidString
            // 
            this.cmdUpdateGuidString.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateGuidString.Image")));
            this.cmdUpdateGuidString.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Name = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Text = "Cập nhật chuỗi phản hồi";
            // 
            // cmdSendEdit
            // 
            this.cmdSendEdit.Image = ((System.Drawing.Image)(resources.GetObject("cmdSendEdit.Image")));
            this.cmdSendEdit.Key = "cmdSendEdit";
            this.cmdSendEdit.Name = "cmdSendEdit";
            this.cmdSendEdit.Text = "Khai báo sửa";
            // 
            // cmdAddHangMau
            // 
            this.cmdAddHangMau.Image = ((System.Drawing.Image)(resources.GetObject("cmdAddHangMau.Image")));
            this.cmdAddHangMau.Key = "cmdAddHangMau";
            this.cmdAddHangMau.Name = "cmdAddHangMau";
            this.cmdAddHangMau.Text = "Thêm Hàng mẫu";
            // 
            // cmdUpdateResult
            // 
            this.cmdUpdateResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateResult.Image")));
            this.cmdUpdateResult.Key = "cmdUpdateResult";
            this.cmdUpdateResult.Name = "cmdUpdateResult";
            this.cmdUpdateResult.Text = "Cập nhật thông tin";
            // 
            // cmdHistory
            // 
            this.cmdHistory.Image = ((System.Drawing.Image)(resources.GetObject("cmdHistory.Image")));
            this.cmdHistory.Key = "cmdHistory";
            this.cmdHistory.Name = "cmdHistory";
            this.cmdHistory.Text = "Lịch sử khai báo";
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "copy24.ico");
            this.ImageList1.Images.SetKeyName(5, "printer.png");
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMainHDGC;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 32);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 356);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbTopBar});
            this.TopRebar1.CommandManager = this.cmMainHDGC;
            this.TopRebar1.Controls.Add(this.cmbTopBar);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1293, 32);
            // 
            // tabHopDong
            // 
            this.tabHopDong.BackColor = System.Drawing.Color.Transparent;
            this.tabHopDong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabHopDong.Location = new System.Drawing.Point(0, 412);
            this.tabHopDong.Name = "tabHopDong";
            this.tabHopDong.Size = new System.Drawing.Size(1087, 371);
            this.tabHopDong.TabIndex = 1;
            this.tabHopDong.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.tpNhomSanPham,
            this.tpNguyenPhuLieu,
            this.tpSanPham,
            this.tpThietBi,
            this.tpHangMau});
            this.tabHopDong.TabsStateStyles.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.tabHopDong.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // tpNhomSanPham
            // 
            this.tpNhomSanPham.AutoScroll = true;
            this.tpNhomSanPham.Controls.Add(this.dgNhomSanPham);
            this.tpNhomSanPham.Location = new System.Drawing.Point(1, 22);
            this.tpNhomSanPham.Name = "tpNhomSanPham";
            this.tpNhomSanPham.Size = new System.Drawing.Size(1085, 348);
            this.tpNhomSanPham.TabStop = true;
            this.tpNhomSanPham.Text = "Loại Sản phẩm gia công";
            // 
            // dgNhomSanPham
            // 
            this.dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNhomSanPham.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgNhomSanPham.AlternatingColors = true;
            this.dgNhomSanPham.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgNhomSanPham.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNhomSanPham.ColumnAutoResize = true;
            dgNhomSanPham_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgNhomSanPham_DesignTimeLayout_Reference_0.Instance")));
            dgNhomSanPham_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgNhomSanPham_DesignTimeLayout_Reference_0});
            dgNhomSanPham_DesignTimeLayout.LayoutString = resources.GetString("dgNhomSanPham_DesignTimeLayout.LayoutString");
            this.dgNhomSanPham.DesignTimeLayout = dgNhomSanPham_DesignTimeLayout;
            this.dgNhomSanPham.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgNhomSanPham.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNhomSanPham.GroupByBoxVisible = false;
            this.dgNhomSanPham.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNhomSanPham.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNhomSanPham.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNhomSanPham.ImageList = this.ImageList1;
            this.dgNhomSanPham.Location = new System.Drawing.Point(0, 0);
            this.dgNhomSanPham.Name = "dgNhomSanPham";
            this.dgNhomSanPham.RecordNavigator = true;
            this.dgNhomSanPham.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgNhomSanPham.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNhomSanPham.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNhomSanPham.Size = new System.Drawing.Size(1085, 348);
            this.dgNhomSanPham.TabIndex = 0;
            this.dgNhomSanPham.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgNhomSanPham.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgNhomSanPham_RowDoubleClick);
            this.dgNhomSanPham.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgNhomSanPham_DeletingRecords);
            // 
            // tpNguyenPhuLieu
            // 
            this.tpNguyenPhuLieu.AutoScroll = true;
            this.tpNguyenPhuLieu.Controls.Add(this.dgNguyenPhuLieu);
            this.tpNguyenPhuLieu.Location = new System.Drawing.Point(1, 22);
            this.tpNguyenPhuLieu.Name = "tpNguyenPhuLieu";
            this.tpNguyenPhuLieu.Size = new System.Drawing.Size(1085, 348);
            this.tpNguyenPhuLieu.TabStop = true;
            this.tpNguyenPhuLieu.Text = "Nguyên phụ liệu";
            // 
            // dgNguyenPhuLieu
            // 
            this.dgNguyenPhuLieu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNguyenPhuLieu.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgNguyenPhuLieu.AlternatingColors = true;
            this.dgNguyenPhuLieu.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgNguyenPhuLieu.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNguyenPhuLieu.ColumnAutoResize = true;
            dgNguyenPhuLieu_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgNguyenPhuLieu_DesignTimeLayout_Reference_0.Instance")));
            dgNguyenPhuLieu_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgNguyenPhuLieu_DesignTimeLayout_Reference_0});
            dgNguyenPhuLieu_DesignTimeLayout.LayoutString = resources.GetString("dgNguyenPhuLieu_DesignTimeLayout.LayoutString");
            this.dgNguyenPhuLieu.DesignTimeLayout = dgNguyenPhuLieu_DesignTimeLayout;
            this.dgNguyenPhuLieu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgNguyenPhuLieu.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNguyenPhuLieu.GroupByBoxVisible = false;
            this.dgNguyenPhuLieu.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNguyenPhuLieu.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNguyenPhuLieu.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNguyenPhuLieu.ImageList = this.ImageList1;
            this.dgNguyenPhuLieu.Location = new System.Drawing.Point(0, 0);
            this.dgNguyenPhuLieu.Name = "dgNguyenPhuLieu";
            this.dgNguyenPhuLieu.RecordNavigator = true;
            this.dgNguyenPhuLieu.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgNguyenPhuLieu.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNguyenPhuLieu.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNguyenPhuLieu.Size = new System.Drawing.Size(1085, 348);
            this.dgNguyenPhuLieu.TabIndex = 1;
            this.dgNguyenPhuLieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgNguyenPhuLieu.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgNguyenPhuLieu_RowDoubleClick);
            this.dgNguyenPhuLieu.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgNguyenPhuLieu_DeletingRecords);
            this.dgNguyenPhuLieu.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgNguyenPhuLieu_LoadingRow);
            // 
            // tpSanPham
            // 
            this.tpSanPham.AutoScroll = true;
            this.tpSanPham.Controls.Add(this.dgSanPham);
            this.tpSanPham.Location = new System.Drawing.Point(1, 22);
            this.tpSanPham.Name = "tpSanPham";
            this.tpSanPham.Size = new System.Drawing.Size(1085, 348);
            this.tpSanPham.TabStop = true;
            this.tpSanPham.Text = "Sản phẩm";
            // 
            // dgSanPham
            // 
            this.dgSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgSanPham.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgSanPham.AlternatingColors = true;
            this.dgSanPham.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgSanPham.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgSanPham.ColumnAutoResize = true;
            dgSanPham_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgSanPham_DesignTimeLayout_Reference_0.Instance")));
            dgSanPham_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgSanPham_DesignTimeLayout_Reference_0});
            dgSanPham_DesignTimeLayout.LayoutString = resources.GetString("dgSanPham_DesignTimeLayout.LayoutString");
            this.dgSanPham.DesignTimeLayout = dgSanPham_DesignTimeLayout;
            this.dgSanPham.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgSanPham.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgSanPham.GroupByBoxVisible = false;
            this.dgSanPham.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgSanPham.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgSanPham.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgSanPham.ImageList = this.ImageList1;
            this.dgSanPham.Location = new System.Drawing.Point(0, 0);
            this.dgSanPham.Name = "dgSanPham";
            this.dgSanPham.RecordNavigator = true;
            this.dgSanPham.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgSanPham.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgSanPham.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgSanPham.Size = new System.Drawing.Size(1085, 348);
            this.dgSanPham.TabIndex = 1;
            this.dgSanPham.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgSanPham.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgSanPham_RowDoubleClick);
            this.dgSanPham.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgSanPham_DeletingRecords);
            this.dgSanPham.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgSanPham_LoadingRow);
            // 
            // tpThietBi
            // 
            this.tpThietBi.AutoScroll = true;
            this.tpThietBi.Controls.Add(this.dgThietBi);
            this.tpThietBi.Location = new System.Drawing.Point(1, 22);
            this.tpThietBi.Name = "tpThietBi";
            this.tpThietBi.Size = new System.Drawing.Size(1085, 348);
            this.tpThietBi.TabStop = true;
            this.tpThietBi.Text = "Thiết bị";
            // 
            // dgThietBi
            // 
            this.dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThietBi.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgThietBi.AlternatingColors = true;
            this.dgThietBi.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgThietBi.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgThietBi.ColumnAutoResize = true;
            dgThietBi_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgThietBi_DesignTimeLayout_Reference_0.Instance")));
            dgThietBi_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgThietBi_DesignTimeLayout_Reference_0});
            dgThietBi_DesignTimeLayout.LayoutString = resources.GetString("dgThietBi_DesignTimeLayout.LayoutString");
            this.dgThietBi.DesignTimeLayout = dgThietBi_DesignTimeLayout;
            this.dgThietBi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgThietBi.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgThietBi.GroupByBoxVisible = false;
            this.dgThietBi.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThietBi.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThietBi.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgThietBi.ImageList = this.ImageList1;
            this.dgThietBi.Location = new System.Drawing.Point(0, 0);
            this.dgThietBi.Name = "dgThietBi";
            this.dgThietBi.RecordNavigator = true;
            this.dgThietBi.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgThietBi.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThietBi.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgThietBi.Size = new System.Drawing.Size(1085, 348);
            this.dgThietBi.TabIndex = 5;
            this.dgThietBi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgThietBi.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgThietBi_RowDoubleClick);
            this.dgThietBi.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgThietBi_DeletingRecords);
            this.dgThietBi.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgThietBi_LoadingRow);
            // 
            // tpHangMau
            // 
            this.tpHangMau.AutoScroll = true;
            this.tpHangMau.Controls.Add(this.dgHangMau);
            this.tpHangMau.Location = new System.Drawing.Point(1, 22);
            this.tpHangMau.Name = "tpHangMau";
            this.tpHangMau.Size = new System.Drawing.Size(1085, 348);
            this.tpHangMau.TabStop = true;
            this.tpHangMau.Text = "Hàng mẫu";
            // 
            // dgHangMau
            // 
            this.dgHangMau.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgHangMau.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgHangMau.AlternatingColors = true;
            this.dgHangMau.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgHangMau.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgHangMau.ColumnAutoResize = true;
            dgHangMau_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgHangMau_DesignTimeLayout_Reference_0.Instance")));
            dgHangMau_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgHangMau_DesignTimeLayout_Reference_0});
            dgHangMau_DesignTimeLayout.LayoutString = resources.GetString("dgHangMau_DesignTimeLayout.LayoutString");
            this.dgHangMau.DesignTimeLayout = dgHangMau_DesignTimeLayout;
            this.dgHangMau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgHangMau.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgHangMau.GroupByBoxVisible = false;
            this.dgHangMau.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHangMau.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHangMau.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgHangMau.Hierarchical = true;
            this.dgHangMau.ImageList = this.ImageList1;
            this.dgHangMau.Location = new System.Drawing.Point(0, 0);
            this.dgHangMau.Name = "dgHangMau";
            this.dgHangMau.RecordNavigator = true;
            this.dgHangMau.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgHangMau.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgHangMau.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgHangMau.Size = new System.Drawing.Size(1085, 348);
            this.dgHangMau.TabIndex = 13;
            this.dgHangMau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgHangMau.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgHangMau_RowDoubleClick);
            this.dgHangMau.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgHangMau_DeletingRecords);
            this.dgHangMau.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgHangMau_LoadingRow);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "lsp_Ten";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.tabHopDong);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(203, 35);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1087, 783);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox2.Controls.Add(this.grpThongTinKhai);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1087, 412);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Thông tin hợp đồng gia công";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.Controls.Add(this.label22);
            this.uiGroupBox6.Controls.Add(this.txtGhiChu);
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(636, 136);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(440, 139);
            this.uiGroupBox6.TabIndex = 0;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(11, 47);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(48, 14);
            this.label22.TabIndex = 0;
            this.label22.Text = "Ghi chú";
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.BackColor = System.Drawing.Color.White;
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(68, 24);
            this.txtGhiChu.MaxLength = 2000;
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(341, 98);
            this.txtGhiChu.TabIndex = 9;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtGhiChu.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.Controls.Add(this.nguyenTeControl1);
            this.uiGroupBox3.Controls.Add(this.nuocHControl1);
            this.uiGroupBox3.Controls.Add(this.donViHaiQuanControl1);
            this.uiGroupBox3.Controls.Add(this.txtTongGiaTriSP);
            this.uiGroupBox3.Controls.Add(this.ccNgayKetThucHD);
            this.uiGroupBox3.Controls.Add(this.txtTongTriGiaTienCong);
            this.uiGroupBox3.Controls.Add(this.label33);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.label32);
            this.uiGroupBox3.Controls.Add(this.label20);
            this.uiGroupBox3.Controls.Add(this.label14);
            this.uiGroupBox3.Controls.Add(this.label21);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox3.Controls.Add(this.cbbIsGC);
            this.uiGroupBox3.Controls.Add(this.label3);
            this.uiGroupBox3.Controls.Add(this.cbPTTT);
            this.uiGroupBox3.Controls.Add(this.ccNgayKyHD);
            this.uiGroupBox3.Controls.Add(this.label26);
            this.uiGroupBox3.Controls.Add(this.label4);
            this.uiGroupBox3.Controls.Add(this.label19);
            this.uiGroupBox3.Controls.Add(this.label15);
            this.uiGroupBox3.Controls.Add(this.label16);
            this.uiGroupBox3.Controls.Add(this.label31);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(7, 13);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(623, 262);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // nguyenTeControl1
            // 
            this.nguyenTeControl1.BackColor = System.Drawing.Color.Transparent;
            this.nguyenTeControl1.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.nguyenTeControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nguyenTeControl1.Location = new System.Drawing.Point(151, 157);
            this.nguyenTeControl1.Ma = "";
            this.nguyenTeControl1.Name = "nguyenTeControl1";
            this.nguyenTeControl1.ReadOnly = false;
            this.nguyenTeControl1.Size = new System.Drawing.Size(428, 22);
            this.nguyenTeControl1.TabIndex = 6;
            this.nguyenTeControl1.VisualStyleManager = null;
            this.nguyenTeControl1.ValueChanged += new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(this.txt_TextChanged);
            // 
            // nuocHControl1
            // 
            this.nuocHControl1.BackColor = System.Drawing.Color.Transparent;
            this.nuocHControl1.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.nuocHControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nuocHControl1.Location = new System.Drawing.Point(151, 39);
            this.nuocHControl1.Ma = "";
            this.nuocHControl1.Name = "nuocHControl1";
            this.nuocHControl1.ReadOnly = false;
            this.nuocHControl1.Size = new System.Drawing.Size(428, 22);
            this.nuocHControl1.TabIndex = 1;
            this.nuocHControl1.VisualStyleManager = null;
            this.nuocHControl1.ValueChanged += new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(this.txt_TextChanged);
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(151, 11);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = false;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(428, 22);
            this.donViHaiQuanControl1.TabIndex = 0;
            this.donViHaiQuanControl1.VisualStyleManager = null;
            // 
            // txtTongGiaTriSP
            // 
            this.txtTongGiaTriSP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTongGiaTriSP.DecimalDigits = 20;
            this.txtTongGiaTriSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongGiaTriSP.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongGiaTriSP.Location = new System.Drawing.Point(151, 222);
            this.txtTongGiaTriSP.MaxLength = 25;
            this.txtTongGiaTriSP.Name = "txtTongGiaTriSP";
            this.txtTongGiaTriSP.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongGiaTriSP.Size = new System.Drawing.Size(173, 22);
            this.txtTongGiaTriSP.TabIndex = 7;
            this.txtTongGiaTriSP.Text = "0";
            this.txtTongGiaTriSP.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongGiaTriSP.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongGiaTriSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTongGiaTriSP.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // ccNgayKetThucHD
            // 
            this.ccNgayKetThucHD.BackColor = System.Drawing.Color.White;
            this.ccNgayKetThucHD.CustomFormat = "dd/MM/yyyy";
            this.ccNgayKetThucHD.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.ccNgayKetThucHD.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayKetThucHD.DropDownCalendar.Name = "";
            this.ccNgayKetThucHD.DropDownCalendar.Visible = false;
            this.ccNgayKetThucHD.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayKetThucHD.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayKetThucHD.Location = new System.Drawing.Point(452, 98);
            this.ccNgayKetThucHD.Name = "ccNgayKetThucHD";
            this.ccNgayKetThucHD.Nullable = true;
            this.ccNgayKetThucHD.NullButtonText = "Xóa";
            this.ccNgayKetThucHD.ShowNullButton = true;
            this.ccNgayKetThucHD.Size = new System.Drawing.Size(124, 22);
            this.ccNgayKetThucHD.TabIndex = 4;
            this.ccNgayKetThucHD.TodayButtonText = "Hôm nay";
            this.ccNgayKetThucHD.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayKetThucHD.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtTongTriGiaTienCong
            // 
            this.txtTongTriGiaTienCong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTongTriGiaTienCong.DecimalDigits = 20;
            this.txtTongTriGiaTienCong.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTriGiaTienCong.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTongTriGiaTienCong.Location = new System.Drawing.Point(455, 223);
            this.txtTongTriGiaTienCong.Name = "txtTongTriGiaTienCong";
            this.txtTongTriGiaTienCong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongTriGiaTienCong.Size = new System.Drawing.Size(124, 22);
            this.txtTongTriGiaTienCong.TabIndex = 8;
            this.txtTongTriGiaTienCong.Text = "0";
            this.txtTongTriGiaTienCong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongTriGiaTienCong.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongTriGiaTienCong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTongTriGiaTienCong.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Location = new System.Drawing.Point(582, 44);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(14, 14);
            this.label33.TabIndex = 0;
            this.label33.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số hợp đồng";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Location = new System.Drawing.Point(582, 16);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(14, 14);
            this.label32.TabIndex = 0;
            this.label32.Text = "*";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(12, 226);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(126, 14);
            this.label20.TabIndex = 0;
            this.label20.Text = "Tổng giá trị sản phẩm";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(582, 76);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 14);
            this.label14.TabIndex = 0;
            this.label14.Text = "*";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(327, 227);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(126, 14);
            this.label21.TabIndex = 0;
            this.label21.Text = "Tổng trị giá tiền công";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Ngày ký hợp đồng";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(145, 14);
            this.label5.TabIndex = 0;
            this.label5.Text = "Phương thức thanh toán";
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.BackColor = System.Drawing.Color.White;
            this.txtSoHopDong.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDong.Location = new System.Drawing.Point(151, 69);
            this.txtSoHopDong.MaxLength = 40;
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(428, 22);
            this.txtSoHopDong.TabIndex = 2;
            this.txtSoHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHopDong.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // cbbIsGC
            // 
            this.cbbIsGC.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbIsGC.DisplayMember = "Name";
            this.cbbIsGC.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Gia công bình thường";
            uiComboBoxItem3.Value = "0";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Gia công ngược";
            uiComboBoxItem4.Value = "1";
            this.cbbIsGC.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cbbIsGC.Location = new System.Drawing.Point(206, 127);
            this.cbbIsGC.Name = "cbbIsGC";
            this.cbbIsGC.Size = new System.Drawing.Size(269, 22);
            this.cbbIsGC.TabIndex = 5;
            this.cbbIsGC.Tag = "PhuongThucThanhToan";
            this.cbbIsGC.ValueMember = "ID";
            this.cbbIsGC.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbIsGC.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(365, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "Ngày hết hạn";
            // 
            // cbPTTT
            // 
            this.cbPTTT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTTT.DisplayMember = "Name";
            this.cbPTTT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTTT.Location = new System.Drawing.Point(206, 188);
            this.cbPTTT.Name = "cbPTTT";
            this.cbPTTT.Size = new System.Drawing.Size(118, 22);
            this.cbPTTT.TabIndex = 6;
            this.cbPTTT.Tag = "PhuongThucThanhToan";
            this.cbPTTT.ValueMember = "ID";
            this.cbPTTT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbPTTT.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // ccNgayKyHD
            // 
            this.ccNgayKyHD.BackColor = System.Drawing.Color.White;
            this.ccNgayKyHD.CustomFormat = "dd/MM/yyyy";
            this.ccNgayKyHD.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.ccNgayKyHD.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayKyHD.DropDownCalendar.Name = "";
            this.ccNgayKyHD.DropDownCalendar.Visible = false;
            this.ccNgayKyHD.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayKyHD.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayKyHD.Location = new System.Drawing.Point(151, 99);
            this.ccNgayKyHD.Name = "ccNgayKyHD";
            this.ccNgayKyHD.Nullable = true;
            this.ccNgayKyHD.NullButtonText = "Xóa";
            this.ccNgayKyHD.ShowNullButton = true;
            this.ccNgayKyHD.Size = new System.Drawing.Size(136, 22);
            this.ccNgayKyHD.TabIndex = 3;
            this.ccNgayKyHD.TodayButtonText = "Hôm nay";
            this.ccNgayKyHD.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayKyHD.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(12, 131);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(174, 14);
            this.label26.TabIndex = 0;
            this.label26.Text = "Có phải gia công ngược không";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nước thuê gia công";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(12, 161);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(126, 14);
            this.label19.TabIndex = 0;
            this.label19.Text = "Đồng tiền thanh toán";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(293, 104);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(14, 14);
            this.label15.TabIndex = 0;
            this.label15.Text = "*";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(582, 104);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(14, 14);
            this.label16.TabIndex = 0;
            this.label16.Text = "*";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Location = new System.Drawing.Point(481, 135);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(14, 14);
            this.label31.TabIndex = 0;
            this.label31.Text = "*";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "HQ";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.Controls.Add(this.label25);
            this.uiGroupBox5.Controls.Add(this.label24);
            this.uiGroupBox5.Controls.Add(this.label23);
            this.uiGroupBox5.Controls.Add(this.txtDCDT);
            this.uiGroupBox5.Controls.Add(this.txtDVDT);
            this.uiGroupBox5.Controls.Add(this.label12);
            this.uiGroupBox5.Controls.Add(this.txtTenDoiTac);
            this.uiGroupBox5.Controls.Add(this.label13);
            this.uiGroupBox5.Controls.Add(this.label18);
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(636, 281);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(440, 125);
            this.uiGroupBox5.TabIndex = 0;
            this.uiGroupBox5.Text = "Bên thuê gia công";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(415, 85);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(14, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "*";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(415, 50);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(14, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "*";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(415, 21);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(14, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "*";
            // 
            // txtDCDT
            // 
            this.txtDCDT.BackColor = System.Drawing.Color.White;
            this.txtDCDT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDCDT.Location = new System.Drawing.Point(68, 71);
            this.txtDCDT.MaxLength = 255;
            this.txtDCDT.Multiline = true;
            this.txtDCDT.Name = "txtDCDT";
            this.txtDCDT.Size = new System.Drawing.Size(341, 44);
            this.txtDCDT.TabIndex = 15;
            this.txtDCDT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDCDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDCDT.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(11, 90);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 14);
            this.label12.TabIndex = 0;
            this.label12.Text = "Địa chỉ";
            // 
            // txtTenDoiTac
            // 
            this.txtTenDoiTac.BackColor = System.Drawing.Color.White;
            this.txtTenDoiTac.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoiTac.Location = new System.Drawing.Point(68, 44);
            this.txtTenDoiTac.MaxLength = 80;
            this.txtTenDoiTac.Name = "txtTenDoiTac";
            this.txtTenDoiTac.Size = new System.Drawing.Size(341, 22);
            this.txtTenDoiTac.TabIndex = 14;
            this.txtTenDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDoiTac.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(11, 55);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 14);
            this.label13.TabIndex = 0;
            this.label13.Text = "Tên ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(11, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(22, 14);
            this.label18.TabIndex = 0;
            this.label18.Text = "Mã";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.Controls.Add(this.txtDiaChiBenNhan);
            this.uiGroupBox4.Controls.Add(this.label17);
            this.uiGroupBox4.Controls.Add(this.txtTenBenNhan);
            this.uiGroupBox4.Controls.Add(this.label10);
            this.uiGroupBox4.Controls.Add(this.txtMaBenNhan);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(7, 281);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(623, 125);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.Text = "Bên nhận gia công";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtDiaChiBenNhan
            // 
            this.txtDiaChiBenNhan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiaChiBenNhan.BackColor = System.Drawing.Color.White;
            this.txtDiaChiBenNhan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiBenNhan.Location = new System.Drawing.Point(68, 71);
            this.txtDiaChiBenNhan.MaxLength = 255;
            this.txtDiaChiBenNhan.Multiline = true;
            this.txtDiaChiBenNhan.Name = "txtDiaChiBenNhan";
            this.txtDiaChiBenNhan.Size = new System.Drawing.Size(528, 44);
            this.txtDiaChiBenNhan.TabIndex = 12;
            this.txtDiaChiBenNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiBenNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaChiBenNhan.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(12, 87);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 14);
            this.label17.TabIndex = 0;
            this.label17.Text = "Địa chỉ";
            // 
            // txtTenBenNhan
            // 
            this.txtTenBenNhan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTenBenNhan.BackColor = System.Drawing.Color.White;
            this.txtTenBenNhan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenBenNhan.Location = new System.Drawing.Point(68, 44);
            this.txtTenBenNhan.MaxLength = 80;
            this.txtTenBenNhan.Name = "txtTenBenNhan";
            this.txtTenBenNhan.Size = new System.Drawing.Size(528, 22);
            this.txtTenBenNhan.TabIndex = 11;
            this.txtTenBenNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenBenNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenBenNhan.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 52);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 14);
            this.label10.TabIndex = 0;
            this.label10.Text = "Tên ";
            // 
            // txtMaBenNhan
            // 
            this.txtMaBenNhan.BackColor = System.Drawing.Color.White;
            this.txtMaBenNhan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaBenNhan.Location = new System.Drawing.Point(68, 16);
            this.txtMaBenNhan.MaxLength = 14;
            this.txtMaBenNhan.Name = "txtMaBenNhan";
            this.txtMaBenNhan.Size = new System.Drawing.Size(407, 22);
            this.txtMaBenNhan.TabIndex = 10;
            this.txtMaBenNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaBenNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaBenNhan.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "Mã";
            // 
            // grpThongTinKhai
            // 
            this.grpThongTinKhai.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpThongTinKhai.AutoScroll = true;
            this.grpThongTinKhai.Controls.Add(this.picHelps);
            this.grpThongTinKhai.Controls.Add(this.label9);
            this.grpThongTinKhai.Controls.Add(this.label8);
            this.grpThongTinKhai.Controls.Add(this.lblTrangThai);
            this.grpThongTinKhai.Controls.Add(this.txtSoTiepNhan);
            this.grpThongTinKhai.Controls.Add(this.ccNgayTiepNhan);
            this.grpThongTinKhai.Controls.Add(this.label11);
            this.grpThongTinKhai.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpThongTinKhai.Location = new System.Drawing.Point(636, 20);
            this.grpThongTinKhai.Name = "grpThongTinKhai";
            this.grpThongTinKhai.Size = new System.Drawing.Size(440, 110);
            this.grpThongTinKhai.TabIndex = 0;
            this.grpThongTinKhai.Text = "Thông tin khai báo";
            this.grpThongTinKhai.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // picHelps
            // 
            this.picHelps.Image = ((System.Drawing.Image)(resources.GetObject("picHelps.Image")));
            this.picHelps.Location = new System.Drawing.Point(404, 9);
            this.picHelps.Name = "picHelps";
            this.picHelps.Size = new System.Drawing.Size(30, 28);
            this.picHelps.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picHelps.TabIndex = 1;
            this.picHelps.TabStop = false;
            this.picHelps.Click += new System.EventHandler(this.picHelps_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 14);
            this.label9.TabIndex = 0;
            this.label9.Text = "Số tiếp nhận";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(11, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 14);
            this.label8.TabIndex = 0;
            this.label8.Text = "Trạng thái";
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(99, 82);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(111, 14);
            this.lblTrangThai.TabIndex = 0;
            this.lblTrangThai.Text = "Chưa gửi thông tin";
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.BackColor = System.Drawing.Color.White;
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(100, 18);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(151, 22);
            this.txtSoTiepNhan.TabIndex = 0;
            this.txtSoTiepNhan.Text = "0";
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ccNgayTiepNhan
            // 
            this.ccNgayTiepNhan.BackColor = System.Drawing.Color.White;
            this.ccNgayTiepNhan.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.ccNgayTiepNhan.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.ccNgayTiepNhan.DropDownCalendar.Name = "";
            this.ccNgayTiepNhan.DropDownCalendar.Visible = false;
            this.ccNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayTiepNhan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayTiepNhan.Location = new System.Drawing.Point(100, 48);
            this.ccNgayTiepNhan.MinDate = new System.DateTime(2018, 8, 25, 0, 0, 0, 0);
            this.ccNgayTiepNhan.Name = "ccNgayTiepNhan";
            this.ccNgayTiepNhan.Nullable = true;
            this.ccNgayTiepNhan.NullButtonText = "Xóa";
            this.ccNgayTiepNhan.ReadOnly = true;
            this.ccNgayTiepNhan.ShowNullButton = true;
            this.ccNgayTiepNhan.Size = new System.Drawing.Size(153, 22);
            this.ccNgayTiepNhan.TabIndex = 0;
            this.ccNgayTiepNhan.TodayButtonText = "Hôm nay";
            this.ccNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(11, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 14);
            this.label11.TabIndex = 0;
            this.label11.Text = "Ngày tiếp nhận";
            // 
            // ds
            // 
            this.ds.DataSetName = "NewDataSet";
            this.ds.Locale = new System.Globalization.CultureInfo("en-US");
            this.ds.Tables.AddRange(new System.Data.DataTable[] {
            this.dtLoaiSanPham,
            this.dtDonViHaiQuan,
            this.dtNuocThueGC,
            this.dtNguyenTe});
            // 
            // dtLoaiSanPham
            // 
            this.dtLoaiSanPham.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6});
            this.dtLoaiSanPham.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "lsp_Ma"}, true)});
            this.dtLoaiSanPham.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn1};
            this.dtLoaiSanPham.TableName = "LoaiSanPham";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "lsp_Ma";
            // 
            // error
            // 
            this.error.ContainerControl = this;
            this.error.Icon = ((System.Drawing.Icon)(resources.GetObject("error.Icon")));
            // 
            // HopDongEditForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1293, 821);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "HopDongEditForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Hợp đồng gia công";
            this.Load += new System.EventHandler(this.HopDongEditForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HopDongEditForm_FormClosing);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNuocThueGC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMainHDGC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTopBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabHopDong)).EndInit();
            this.tabHopDong.ResumeLayout(false);
            this.tpNhomSanPham.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNhomSanPham)).EndInit();
            this.tpNguyenPhuLieu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNguyenPhuLieu)).EndInit();
            this.tpSanPham.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgSanPham)).EndInit();
            this.tpThietBi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgThietBi)).EndInit();
            this.tpHangMau.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgHangMau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpThongTinKhai)).EndInit();
            this.grpThongTinKhai.ResumeLayout(false);
            this.grpThongTinKhai.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHelps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiSanPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataTable dtNguyenTe;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMainHDGC;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar cmbTopBar;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddLoaiSanPham;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddNguyenPhuLieu;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddSanPham;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddThietBi;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddThemPhuKien;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.UI.Tab.UITab tabHopDong;
        private Janus.Windows.UI.Tab.UITabPage tpNhomSanPham;
        private Janus.Windows.GridEX.GridEX dgNhomSanPham;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.UI.Tab.UITabPage tpNguyenPhuLieu;
        private Janus.Windows.UI.Tab.UITabPage tpSanPham;
        private Janus.Windows.UI.Tab.UITabPage tpThietBi;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataTable dtNuocThueGC;
        private System.Data.DataTable dtDonViHaiQuan;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataSet ds;
        private System.Data.DataTable dtLoaiSanPham;
        private System.Data.DataColumn dataColumn1;
        private Janus.Windows.GridEX.GridEX dgThietBi;
        private Janus.Windows.GridEX.GridEX dgNguyenPhuLieu;
        private Janus.Windows.GridEX.GridEX dgSanPham;
        private System.Windows.Forms.ErrorProvider error;
        private Janus.Windows.UI.CommandBars.UICommand HuyKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand NhanDuLieuHD;
        private Janus.Windows.UI.CommandBars.UICommand XacNhan;
        private Janus.Windows.UI.CommandBars.UICommand NhapHang;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddLoaiSanPham1;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddNguyenPhuLieu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddSanPham1;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddThietBi1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.UI.CommandBars.UICommand cmdExcel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdExcel;
        private Janus.Windows.UI.CommandBars.UICommand cmdNPL1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLoaiSPGCExcel;
        private Janus.Windows.UI.CommandBars.UICommand cmdNPLExcel;
        private Janus.Windows.UI.CommandBars.UICommand cmdSPExcel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThietBi1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSPExcel;
        private Janus.Windows.UI.CommandBars.UICommand cmdThietBiExcel;
        private Janus.Windows.UI.CommandBars.UICommand cmdCopyHD;
        private Janus.Windows.UI.CommandBars.UICommand NhapHang1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand NhanDuLieuHD1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCopyHD1;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN1;
        private Janus.Windows.UI.CommandBars.UICommand cmdEdit;
        private Janus.Windows.UI.Tab.UITabPage tpHangMau;
        private Janus.Windows.GridEX.GridEX dgHangMau;
        private Janus.Windows.EditControls.UIGroupBox grpThongTinKhai;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenBenNhan;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBenNhan;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiBenNhan;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtDCDT;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoiTac;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVDT;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendEdit;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongGiaTriSP;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayKetThucHD;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTriGiaTienCong;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private Janus.Windows.EditControls.UIComboBox cbbIsGC;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIComboBox cbPTTT;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayKyHD;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label7;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl1;
        private Company.Interface.Controls.NuocHControl nuocHControl1;
        private Company.Interface.Controls.NguyenTeControl nguyenTeControl1;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddHangMau1;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddHangMau;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateResult;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHistory1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHistory;
        private System.Windows.Forms.PictureBox picHelps;

    }
}
