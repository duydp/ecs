﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC07NewForm : BaseForm
    {
        public DataSet dsThietBi = new DataSet();
        public DataSet dsTB = new DataSet();

        public DataSet TKNhapTB = new DataSet();
        public DataSet TKXuatTB = new DataSet();

        public DataSet TKNhapTBAll = new DataSet();
        public DataSet TKXuatTBAll = new DataSet();

        public DataTable dtBangThietBi = new DataTable();
        private BangKe07New BC07 = new BangKe07New();
        public HopDong HD = new HopDong();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
        int sotable;
        int soLuongTKNhap;
        int soLuongTKXuat;
        public ReportViewBC07NewForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            TKNhapTB = TKMD.GetToKhaiNhapThietBi(this.HD.ID);
            TKXuatTB = TKMD.GetToKhaiXuatThietBiAll(this.HD.ID);

            TKNhapTBAll = TKMD.GetToKhaiVaThietBiNhap(this.HD.ID);
            TKXuatTBAll = TKMD.GetToKhaiVaThietBiXuatAll(this.HD.ID);

            soLuongTKNhap = TKNhapTB.Tables[0].Rows.Count;
            soLuongTKXuat = TKXuatTB.Tables[0].Rows.Count;

            if (((soLuongTKNhap - 1) / 2 + 1) > ((soLuongTKXuat - 1) / 3 + 1))
                sotable = (soLuongTKNhap - 1) / 2 + 1;
            else sotable = (soLuongTKXuat - 1) / 3 + 1;

            for (int i = 0; i < sotable; i++)
            {
                cbPage.Items.Add("Trang " + (i + 1), i);
            }
            if(cbPage.Items.Count > 0)
                cbPage.SelectedIndex = 0;
            btnNext.Enabled = !this.BC07.Last;
            btnPrevious.Enabled = !this.BC07.First;
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BC07.First = (cbPage.SelectedIndex == 0);
            this.BC07.Last = (cbPage.SelectedIndex == (sotable - 1));
            this.BC07.HD = this.HD;
            this.BC07.toso = cbPage.SelectedIndex + 1;
            btnNext.Enabled = !this.BC07.Last;
            btnPrevious.Enabled = !this.BC07.First;

            if (dsThietBi.Tables["dtBangThietBi" + cbPage.SelectedIndex] != null)
                this.BC07.BindReport(this.dsThietBi.Tables["dtBangThietBi" + cbPage.SelectedIndex]);
            else
            {
                dtBangThietBi = new DataTable("dtBangThietBi" + cbPage.SelectedIndex);
                dtBangThietBi = CreatSchemaDataSetNewRP07(cbPage.SelectedIndex);
                this.BC07.BindReport(this.dtBangThietBi);
                dsThietBi.Tables.Add(dtBangThietBi);

            }

            printControl1.PrintingSystem = this.BC07.PrintingSystem;
            this.BC07.CreateDocument();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch 
            {
                ShowMessage("Lỗi khi in!", false);
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                ShowMessage("Lỗi khi in!", false);
            }   
        }


        public DataTable CreatSchemaDataSetNewRP07(int BangSo)
        {
            DataTable dttemp = new DataTable("dtBangNguyenPhuLieu" + BangSo.ToString());
            DataColumn[] dcCol = new DataColumn[9];

            dcCol[0] = new DataColumn("ThietBi", typeof(string));
            dcCol[0].Caption = "ThietBi";

            dcCol[1] = new DataColumn("DVT", typeof(string));
            dcCol[1].Caption = "DVT";


            int t = 2;
            for (int k = BangSo * 2; k < (BangSo + 1) * 2; k++)
            {

                if (k < soLuongTKNhap)
                {
                    string Ma = TKNhapTB.Tables[0].Rows[k].ItemArray[0].ToString();
                    string SoToKhai = TKNhapTB.Tables[0].Rows[k].ItemArray[1].ToString();
                    string MaLoaiHinh = TKNhapTB.Tables[0].Rows[k].ItemArray[2].ToString();
                    string NamDangKy = DateTime.Parse(TKNhapTB.Tables[0].Rows[k].ItemArray[3].ToString()).Year.ToString();

                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "Mã nhập " + Ma;
                    dcCol[t].DataType = typeof(decimal);
                    dcCol[t].Caption = " Tờ khai : " + SoToKhai + "\r\n Mã LH : " + MaLoaiHinh + "\r\n Năm ĐK : " + NamDangKy;
                    t++;
                }
                else
                {
                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "";
                    dcCol[t].DataType = typeof(decimal);
                    dcCol[t].Caption = "";
                    t++;
                }
            }

            for (int k = BangSo * 3; k < (BangSo + 1) * 3; k++)
            {
                if (k < soLuongTKXuat)
                {
                    string Ma = TKXuatTB.Tables[0].Rows[k].ItemArray[0].ToString();
                    string SoToKhai = TKXuatTB.Tables[0].Rows[k].ItemArray[1].ToString();
                    string MaLoaiHinh = TKXuatTB.Tables[0].Rows[k].ItemArray[2].ToString();
                    string NamDangKy = DateTime.Parse(TKXuatTB.Tables[0].Rows[k].ItemArray[3].ToString()).Year.ToString();

                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "Mã xuất " + Ma;
                    dcCol[t].DataType = typeof(decimal);
                    dcCol[t].Caption = " Tờ khai : " + SoToKhai + "\r\n Mã LH : " + MaLoaiHinh + "\r\n Năm ĐK : " + NamDangKy;
                    t++;
                }
                else
                {
                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "";
                    dcCol[t].DataType = typeof(decimal);
                    dcCol[t].Caption = "";
                    t++;
                }
            }

            dcCol[7] = new DataColumn();
            dcCol[7].ColumnName = "LuongTon";
            dcCol[7].DataType = typeof(decimal);
            dcCol[7].Caption = "LuongTon";

            dcCol[8] = new DataColumn();
            dcCol[8].ColumnName = "BienPhap";
            dcCol[8].DataType = typeof(string);
            dcCol[8].Caption = "BienPhap";

            dttemp.Columns.AddRange(dcCol);

            /* Điền số liệu */

            Company.GC.BLL.GC.ThietBiCollection TBCol = this.HD.GetTB();

            foreach (Company.GC.BLL.GC.ThietBi TB in TBCol)
            {
                DataRow drData = dttemp.NewRow();

                drData[0] = TB.Ma + " - " + TB.Ten;
                drData[1] = DonViTinh_GetName(TB.DVT_ID);

                for (int n = 2; n <= 3; n++)
                {
                    foreach (DataRow drThietBiNhap in TKNhapTBAll.Tables[0].Rows)
                    {
                        string ColName = "Mã nhập " + drThietBiNhap.ItemArray[0].ToString();
                        if (dttemp.Columns[n].ColumnName == ColName && drThietBiNhap.ItemArray[4].ToString() == TB.Ma)
                                drData[n] = drThietBiNhap.ItemArray[6];
                    }
                }

                for (int n = 4; n <= 6; n++)
                {
                    foreach (DataRow drThietBiXuat in TKXuatTBAll.Tables[0].Rows)
                    {
                        string ColName = "Mã xuất " + drThietBiXuat.ItemArray[0].ToString();
                        if (dttemp.Columns[n].ColumnName == ColName && drThietBiXuat.ItemArray[4].ToString() == TB.Ma)
                            drData[n] = drThietBiXuat.ItemArray[6];
                    }
                }

                for (int n = 2; n <= 6; n++)
                {
                    if (drData[n].ToString() != "")
                    {
                        dttemp.Rows.Add(drData);
                        break;
                    }
                }
            }

            return dttemp;
        }


        private void btnPrevious_Click(object sender, EventArgs e)
        {
            cbPage.SelectedIndex--;
            cboToKhai_SelectedIndexChanged(null, null);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            cbPage.SelectedIndex++;
            cboToKhai_SelectedIndexChanged(null, null);
        }
   
    }
}