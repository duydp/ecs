
-------------------------------------------- STORE

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_SelectAll]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[MaQuanLy],
	[TenHang],
	[ThueSuat],
	[ThueSuatTuyetDoi],
	[MaDVTTuyetDoi],
	[MaTTTuyetDoi],
	[NuocXuatXu],
	[SoLuong1],
	[DVTLuong1],
	[SoLuong2],
	[DVTLuong2],
	[TriGiaHoaDon],
	[DonGiaHoaDon],
	[MaTTDonGia],
	[DVTDonGia],
	[MaBieuThueNK],
	[MaHanNgach],
	[MaThueNKTheoLuong],
	[MaMienGiamThue],
	[SoTienGiamThue],
	[TriGiaTinhThue],
	[MaTTTriGiaTinhThue],
	[SoMucKhaiKhoanDC],
	[SoTTDongHangTKTNTX],
	[SoDMMienThue],
	[SoDongDMMienThue],
	[MaMienGiam],
	[SoTienMienGiam],
	[MaTTSoTienMienGiam],
	[MaVanBanPhapQuyKhac1],
	[MaVanBanPhapQuyKhac2],
	[MaVanBanPhapQuyKhac3],
	[MaVanBanPhapQuyKhac4],
	[MaVanBanPhapQuyKhac5],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam1],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_SelectDynamic]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[MaQuanLy],
	[TenHang],
	[ThueSuat],
	[ThueSuatTuyetDoi],
	[MaDVTTuyetDoi],
	[MaTTTuyetDoi],
	[NuocXuatXu],
	[SoLuong1],
	[DVTLuong1],
	[SoLuong2],
	[DVTLuong2],
	[TriGiaHoaDon],
	[DonGiaHoaDon],
	[MaTTDonGia],
	[DVTDonGia],
	[MaBieuThueNK],
	[MaHanNgach],
	[MaThueNKTheoLuong],
	[MaMienGiamThue],
	[SoTienGiamThue],
	[TriGiaTinhThue],
	[MaTTTriGiaTinhThue],
	[SoMucKhaiKhoanDC],
	[SoTTDongHangTKTNTX],
	[SoDMMienThue],
	[SoDongDMMienThue],
	[MaMienGiam],
	[SoTienMienGiam],
	[MaTTSoTienMienGiam],
	[MaVanBanPhapQuyKhac1],
	[MaVanBanPhapQuyKhac2],
	[MaVanBanPhapQuyKhac3],
	[MaVanBanPhapQuyKhac4],
	[MaVanBanPhapQuyKhac5],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam1],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam]
FROM [dbo].[t_KDT_VNACC_HangMauDich] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Delete]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_DeleteDynamic]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Insert]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Insert]
	@Master_id bigint,
	@MaTSThueThuKhac varchar(10),
	@MaMGThueThuKhac varchar(5),
	@SoTienGiamThueThuKhac numeric(16, 4),
	@TenKhoanMucThueVaThuKhac nvarchar(9),
	@TriGiaTinhThueVaThuKhac numeric(17, 4),
	@SoLuongTinhThueVaThuKhac numeric(14, 0),
	@MaDVTDanhThueVaThuKhac varchar(4),
	@ThueSuatThueVaThuKhac varchar(24),
	@SoTienThueVaThuKhac numeric(16, 4),
	@DieuKhoanMienGiamThueVaThuKhac varchar(60),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac]
(
	[Master_id],
	[MaTSThueThuKhac],
	[MaMGThueThuKhac],
	[SoTienGiamThueThuKhac],
	[TenKhoanMucThueVaThuKhac],
	[TriGiaTinhThueVaThuKhac],
	[SoLuongTinhThueVaThuKhac],
	[MaDVTDanhThueVaThuKhac],
	[ThueSuatThueVaThuKhac],
	[SoTienThueVaThuKhac],
	[DieuKhoanMienGiamThueVaThuKhac]
)
VALUES 
(
	@Master_id,
	@MaTSThueThuKhac,
	@MaMGThueThuKhac,
	@SoTienGiamThueThuKhac,
	@TenKhoanMucThueVaThuKhac,
	@TriGiaTinhThueVaThuKhac,
	@SoLuongTinhThueVaThuKhac,
	@MaDVTDanhThueVaThuKhac,
	@ThueSuatThueVaThuKhac,
	@SoTienThueVaThuKhac,
	@DieuKhoanMienGiamThueVaThuKhac
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_InsertUpdate]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_InsertUpdate]
	@ID bigint,
	@Master_id bigint,
	@MaTSThueThuKhac varchar(10),
	@MaMGThueThuKhac varchar(5),
	@SoTienGiamThueThuKhac numeric(16, 4),
	@TenKhoanMucThueVaThuKhac nvarchar(9),
	@TriGiaTinhThueVaThuKhac numeric(17, 4),
	@SoLuongTinhThueVaThuKhac numeric(14, 0),
	@MaDVTDanhThueVaThuKhac varchar(4),
	@ThueSuatThueVaThuKhac varchar(24),
	@SoTienThueVaThuKhac numeric(16, 4),
	@DieuKhoanMienGiamThueVaThuKhac varchar(60)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac] 
		SET
			[Master_id] = @Master_id,
			[MaTSThueThuKhac] = @MaTSThueThuKhac,
			[MaMGThueThuKhac] = @MaMGThueThuKhac,
			[SoTienGiamThueThuKhac] = @SoTienGiamThueThuKhac,
			[TenKhoanMucThueVaThuKhac] = @TenKhoanMucThueVaThuKhac,
			[TriGiaTinhThueVaThuKhac] = @TriGiaTinhThueVaThuKhac,
			[SoLuongTinhThueVaThuKhac] = @SoLuongTinhThueVaThuKhac,
			[MaDVTDanhThueVaThuKhac] = @MaDVTDanhThueVaThuKhac,
			[ThueSuatThueVaThuKhac] = @ThueSuatThueVaThuKhac,
			[SoTienThueVaThuKhac] = @SoTienThueVaThuKhac,
			[DieuKhoanMienGiamThueVaThuKhac] = @DieuKhoanMienGiamThueVaThuKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac]
		(
			[Master_id],
			[MaTSThueThuKhac],
			[MaMGThueThuKhac],
			[SoTienGiamThueThuKhac],
			[TenKhoanMucThueVaThuKhac],
			[TriGiaTinhThueVaThuKhac],
			[SoLuongTinhThueVaThuKhac],
			[MaDVTDanhThueVaThuKhac],
			[ThueSuatThueVaThuKhac],
			[SoTienThueVaThuKhac],
			[DieuKhoanMienGiamThueVaThuKhac]
		)
		VALUES 
		(
			@Master_id,
			@MaTSThueThuKhac,
			@MaMGThueThuKhac,
			@SoTienGiamThueThuKhac,
			@TenKhoanMucThueVaThuKhac,
			@TriGiaTinhThueVaThuKhac,
			@SoLuongTinhThueVaThuKhac,
			@MaDVTDanhThueVaThuKhac,
			@ThueSuatThueVaThuKhac,
			@SoTienThueVaThuKhac,
			@DieuKhoanMienGiamThueVaThuKhac
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Load]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[MaTSThueThuKhac],
	[MaMGThueThuKhac],
	[SoTienGiamThueThuKhac],
	[TenKhoanMucThueVaThuKhac],
	[TriGiaTinhThueVaThuKhac],
	[SoLuongTinhThueVaThuKhac],
	[MaDVTDanhThueVaThuKhac],
	[ThueSuatThueVaThuKhac],
	[SoTienThueVaThuKhac],
	[DieuKhoanMienGiamThueVaThuKhac]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectAll]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[MaTSThueThuKhac],
	[MaMGThueThuKhac],
	[SoTienGiamThueThuKhac],
	[TenKhoanMucThueVaThuKhac],
	[TriGiaTinhThueVaThuKhac],
	[SoLuongTinhThueVaThuKhac],
	[MaDVTDanhThueVaThuKhac],
	[ThueSuatThueVaThuKhac],
	[SoTienThueVaThuKhac],
	[DieuKhoanMienGiamThueVaThuKhac]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectDynamic]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_id],
	[MaTSThueThuKhac],
	[MaMGThueThuKhac],
	[SoTienGiamThueThuKhac],
	[TenKhoanMucThueVaThuKhac],
	[TriGiaTinhThueVaThuKhac],
	[SoLuongTinhThueVaThuKhac],
	[MaDVTDanhThueVaThuKhac],
	[ThueSuatThueVaThuKhac],
	[SoTienThueVaThuKhac],
	[DieuKhoanMienGiamThueVaThuKhac]
FROM [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Update]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_ThueThuKhac_Update]
	@ID bigint,
	@Master_id bigint,
	@MaTSThueThuKhac varchar(10),
	@MaMGThueThuKhac varchar(5),
	@SoTienGiamThueThuKhac numeric(16, 4),
	@TenKhoanMucThueVaThuKhac nvarchar(9),
	@TriGiaTinhThueVaThuKhac numeric(17, 4),
	@SoLuongTinhThueVaThuKhac numeric(14, 0),
	@MaDVTDanhThueVaThuKhac varchar(4),
	@ThueSuatThueVaThuKhac varchar(24),
	@SoTienThueVaThuKhac numeric(16, 4),
	@DieuKhoanMienGiamThueVaThuKhac varchar(60)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac]
SET
	[Master_id] = @Master_id,
	[MaTSThueThuKhac] = @MaTSThueThuKhac,
	[MaMGThueThuKhac] = @MaMGThueThuKhac,
	[SoTienGiamThueThuKhac] = @SoTienGiamThueThuKhac,
	[TenKhoanMucThueVaThuKhac] = @TenKhoanMucThueVaThuKhac,
	[TriGiaTinhThueVaThuKhac] = @TriGiaTinhThueVaThuKhac,
	[SoLuongTinhThueVaThuKhac] = @SoLuongTinhThueVaThuKhac,
	[MaDVTDanhThueVaThuKhac] = @MaDVTDanhThueVaThuKhac,
	[ThueSuatThueVaThuKhac] = @ThueSuatThueVaThuKhac,
	[SoTienThueVaThuKhac] = @SoTienThueVaThuKhac,
	[DieuKhoanMienGiamThueVaThuKhac] = @DieuKhoanMienGiamThueVaThuKhac
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_Update]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaQuanLy varchar(7),
	@TenHang nvarchar(200),
	@ThueSuat numeric(10, 0),
	@ThueSuatTuyetDoi numeric(14, 4),
	@MaDVTTuyetDoi varchar(4),
	@MaTTTuyetDoi varchar(4),
	@NuocXuatXu varchar(2),
	@SoLuong1 numeric(12, 0),
	@DVTLuong1 varchar(4),
	@SoLuong2 numeric(12, 0),
	@DVTLuong2 varchar(4),
	@TriGiaHoaDon numeric(24, 4),
	@DonGiaHoaDon numeric(13, 4),
	@MaTTDonGia varchar(4),
	@DVTDonGia varchar(3),
	@MaBieuThueNK varchar(3),
	@MaHanNgach varchar(1),
	@MaThueNKTheoLuong varchar(10),
	@MaMienGiamThue varchar(5),
	@SoTienGiamThue numeric(20, 4),
	@TriGiaTinhThue numeric(24, 4),
	@MaTTTriGiaTinhThue varchar(3),
	@SoMucKhaiKhoanDC varchar(5),
	@SoTTDongHangTKTNTX varchar(2),
	@SoDMMienThue numeric(12, 0),
	@SoDongDMMienThue varchar(3),
	@MaMienGiam varchar(5),
	@SoTienMienGiam numeric(20, 4),
	@MaTTSoTienMienGiam varchar(3),
	@MaVanBanPhapQuyKhac1 varchar(2),
	@MaVanBanPhapQuyKhac2 varchar(2),
	@MaVanBanPhapQuyKhac3 varchar(2),
	@MaVanBanPhapQuyKhac4 varchar(2),
	@MaVanBanPhapQuyKhac5 varchar(2),
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(16, 4),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(22, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(21, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam1 varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(20, 4),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(16, 4),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(16, 4),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(20, 4),
	@TienBaoHiem_KhoanTien numeric(20, 4),
	@DieuKhoanMienGiam varchar(60)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangMauDich]
SET
	[TKMD_ID] = @TKMD_ID,
	[MaSoHang] = @MaSoHang,
	[MaQuanLy] = @MaQuanLy,
	[TenHang] = @TenHang,
	[ThueSuat] = @ThueSuat,
	[ThueSuatTuyetDoi] = @ThueSuatTuyetDoi,
	[MaDVTTuyetDoi] = @MaDVTTuyetDoi,
	[MaTTTuyetDoi] = @MaTTTuyetDoi,
	[NuocXuatXu] = @NuocXuatXu,
	[SoLuong1] = @SoLuong1,
	[DVTLuong1] = @DVTLuong1,
	[SoLuong2] = @SoLuong2,
	[DVTLuong2] = @DVTLuong2,
	[TriGiaHoaDon] = @TriGiaHoaDon,
	[DonGiaHoaDon] = @DonGiaHoaDon,
	[MaTTDonGia] = @MaTTDonGia,
	[DVTDonGia] = @DVTDonGia,
	[MaBieuThueNK] = @MaBieuThueNK,
	[MaHanNgach] = @MaHanNgach,
	[MaThueNKTheoLuong] = @MaThueNKTheoLuong,
	[MaMienGiamThue] = @MaMienGiamThue,
	[SoTienGiamThue] = @SoTienGiamThue,
	[TriGiaTinhThue] = @TriGiaTinhThue,
	[MaTTTriGiaTinhThue] = @MaTTTriGiaTinhThue,
	[SoMucKhaiKhoanDC] = @SoMucKhaiKhoanDC,
	[SoTTDongHangTKTNTX] = @SoTTDongHangTKTNTX,
	[SoDMMienThue] = @SoDMMienThue,
	[SoDongDMMienThue] = @SoDongDMMienThue,
	[MaMienGiam] = @MaMienGiam,
	[SoTienMienGiam] = @SoTienMienGiam,
	[MaTTSoTienMienGiam] = @MaTTSoTienMienGiam,
	[MaVanBanPhapQuyKhac1] = @MaVanBanPhapQuyKhac1,
	[MaVanBanPhapQuyKhac2] = @MaVanBanPhapQuyKhac2,
	[MaVanBanPhapQuyKhac3] = @MaVanBanPhapQuyKhac3,
	[MaVanBanPhapQuyKhac4] = @MaVanBanPhapQuyKhac4,
	[MaVanBanPhapQuyKhac5] = @MaVanBanPhapQuyKhac5,
	[SoDong] = @SoDong,
	[MaPhanLoaiTaiXacNhanGia] = @MaPhanLoaiTaiXacNhanGia,
	[TenNoiXuatXu] = @TenNoiXuatXu,
	[SoLuongTinhThue] = @SoLuongTinhThue,
	[MaDVTDanhThue] = @MaDVTDanhThue,
	[DonGiaTinhThue] = @DonGiaTinhThue,
	[DV_SL_TrongDonGiaTinhThue] = @DV_SL_TrongDonGiaTinhThue,
	[MaTTDonGiaTinhThue] = @MaTTDonGiaTinhThue,
	[TriGiaTinhThueS] = @TriGiaTinhThueS,
	[MaTTTriGiaTinhThueS] = @MaTTTriGiaTinhThueS,
	[MaTTSoTienMienGiam1] = @MaTTSoTienMienGiam1,
	[MaPhanLoaiThueSuatThue] = @MaPhanLoaiThueSuatThue,
	[ThueSuatThue] = @ThueSuatThue,
	[PhanLoaiThueSuatThue] = @PhanLoaiThueSuatThue,
	[SoTienThue] = @SoTienThue,
	[MaTTSoTienThueXuatKhau] = @MaTTSoTienThueXuatKhau,
	[TienLePhi_DonGia] = @TienLePhi_DonGia,
	[TienBaoHiem_DonGia] = @TienBaoHiem_DonGia,
	[TienLePhi_SoLuong] = @TienLePhi_SoLuong,
	[TienLePhi_MaDVSoLuong] = @TienLePhi_MaDVSoLuong,
	[TienBaoHiem_SoLuong] = @TienBaoHiem_SoLuong,
	[TienBaoHiem_MaDVSoLuong] = @TienBaoHiem_MaDVSoLuong,
	[TienLePhi_KhoanTien] = @TienLePhi_KhoanTien,
	[TienBaoHiem_KhoanTien] = @TienBaoHiem_KhoanTien,
	[DieuKhoanMienGiam] = @DieuKhoanMienGiam
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_Delete]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HoaDon_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HoaDon]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_DeleteDynamic]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HoaDon_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HoaDon] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_Insert]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HoaDon_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_Insert]
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@PhanLoaiXuatNhap varchar(1),
	@MaDaiLyHQ varchar(5),
	@SoHoaDon varchar(35),
	@NgayLapHoaDon datetime,
	@DiaDiemLapHoaDon nvarchar(35),
	@PhuongThucThanhToan nvarchar(30),
	@PhanLoaiLienQuan varchar(6),
	@MaNguoiXNK varchar(13),
	@TenNguoiXNK nvarchar(100),
	@MaBuuChinh_NguoiXNK varchar(7),
	@DiaChi_NguoiXNK nvarchar(100),
	@SoDienThoai_NguoiXNK varchar(20),
	@NguoiLapHoaDon nvarchar(60),
	@MaNguoiGuiNhan varchar(13),
	@TenNguoiGuiNhan nvarchar(70),
	@MaBuuChinhNguoiGuiNhan varchar(9),
	@DiaChiNguoiNhanGui1 nvarchar(35),
	@DiaChiNguoiNhanGui2 nvarchar(35),
	@DiaChiNguoiNhanGui3 nvarchar(35),
	@DiaChiNguoiNhanGui4 nvarchar(35),
	@SoDienThoaiNhanGui varchar(12),
	@NuocSoTaiNhanGui varchar(2),
	@MaKyHieu nvarchar(140),
	@PhanLoaiVanChuyen varchar(4),
	@TenPTVC nvarchar(35),
	@SoHieuChuyenDi varchar(10),
	@MaDiaDiemXepHang varchar(5),
	@TenDiaDiemXepHang nvarchar(20),
	@ThoiKyXephang datetime,
	@MaDiaDiemDoHang varchar(5),
	@TenDiaDiemDoHang nvarchar(20),
	@MaDiaDiemTrungChuyen nvarchar(5),
	@TenDiaDiemTrungChuyen nvarchar(30),
	@TrongLuongGross numeric(14, 4),
	@MaDVT_TrongLuongGross varchar(3),
	@TrongLuongThuan numeric(14, 4),
	@MaDVT_TrongLuongThuan nvarchar(3),
	@TongTheTich numeric(14, 4),
	@MaDVT_TheTich nvarchar(3),
	@TongSoKienHang numeric(8, 0),
	@MaDVT_KienHang varchar(3),
	@GhiChuChuHang nvarchar(70),
	@SoPL varchar(10),
	@NganHangLC nvarchar(80),
	@TriGiaFOB numeric(24, 4),
	@MaTT_FOB varchar(3),
	@SoTienFOB numeric(17, 4),
	@MaTT_TienFOB varchar(3),
	@PhiVanChuyen numeric(22, 4),
	@MaTT_PhiVC varchar(3),
	@NoiThanhToanPhiVC nvarchar(20),
	@ChiPhiXepHang1 numeric(13, 4),
	@MaTT_ChiPhiXepHang1 nvarchar(3),
	@LoaiChiPhiXepHang1 nvarchar(20),
	@ChiPhiXepHang2 numeric(13, 4),
	@MaTT_ChiPhiXepHang2 nvarchar(3),
	@LoaiChiPhiXepHang2 nvarchar(20),
	@PhiVC_DuongBo numeric(13, 4),
	@MaTT_PhiVC_DuongBo varchar(3),
	@PhiBaoHiem numeric(22, 4),
	@MaTT_PhiBaoHiem varchar(3),
	@SoTienPhiBaoHiem numeric(13, 4),
	@MaTT_TienPhiBaoHiem varchar(3),
	@SoTienKhauTru numeric(13, 4),
	@MaTT_TienKhauTru varchar(3),
	@LoaiKhauTru nvarchar(20),
	@SoTienKhac numeric(13, 0),
	@MaTT_TienKhac varchar(3),
	@LoaiSoTienKhac nvarchar(20),
	@TongTriGiaHoaDon numeric(24, 4),
	@MaTT_TongTriGia varchar(3),
	@DieuKienGiaHoaDon varchar(20),
	@DiaDiemGiaoHang nvarchar(30),
	@GhiChuDacBiet nvarchar(100),
	@TongSoDongHang numeric(4, 0),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HoaDon]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLoaiXuatNhap],
	[MaDaiLyHQ],
	[SoHoaDon],
	[NgayLapHoaDon],
	[DiaDiemLapHoaDon],
	[PhuongThucThanhToan],
	[PhanLoaiLienQuan],
	[MaNguoiXNK],
	[TenNguoiXNK],
	[MaBuuChinh_NguoiXNK],
	[DiaChi_NguoiXNK],
	[SoDienThoai_NguoiXNK],
	[NguoiLapHoaDon],
	[MaNguoiGuiNhan],
	[TenNguoiGuiNhan],
	[MaBuuChinhNguoiGuiNhan],
	[DiaChiNguoiNhanGui1],
	[DiaChiNguoiNhanGui2],
	[DiaChiNguoiNhanGui3],
	[DiaChiNguoiNhanGui4],
	[SoDienThoaiNhanGui],
	[NuocSoTaiNhanGui],
	[MaKyHieu],
	[PhanLoaiVanChuyen],
	[TenPTVC],
	[SoHieuChuyenDi],
	[MaDiaDiemXepHang],
	[TenDiaDiemXepHang],
	[ThoiKyXephang],
	[MaDiaDiemDoHang],
	[TenDiaDiemDoHang],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[TrongLuongGross],
	[MaDVT_TrongLuongGross],
	[TrongLuongThuan],
	[MaDVT_TrongLuongThuan],
	[TongTheTich],
	[MaDVT_TheTich],
	[TongSoKienHang],
	[MaDVT_KienHang],
	[GhiChuChuHang],
	[SoPL],
	[NganHangLC],
	[TriGiaFOB],
	[MaTT_FOB],
	[SoTienFOB],
	[MaTT_TienFOB],
	[PhiVanChuyen],
	[MaTT_PhiVC],
	[NoiThanhToanPhiVC],
	[ChiPhiXepHang1],
	[MaTT_ChiPhiXepHang1],
	[LoaiChiPhiXepHang1],
	[ChiPhiXepHang2],
	[MaTT_ChiPhiXepHang2],
	[LoaiChiPhiXepHang2],
	[PhiVC_DuongBo],
	[MaTT_PhiVC_DuongBo],
	[PhiBaoHiem],
	[MaTT_PhiBaoHiem],
	[SoTienPhiBaoHiem],
	[MaTT_TienPhiBaoHiem],
	[SoTienKhauTru],
	[MaTT_TienKhauTru],
	[LoaiKhauTru],
	[SoTienKhac],
	[MaTT_TienKhac],
	[LoaiSoTienKhac],
	[TongTriGiaHoaDon],
	[MaTT_TongTriGia],
	[DieuKienGiaHoaDon],
	[DiaDiemGiaoHang],
	[GhiChuDacBiet],
	[TongSoDongHang],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@PhanLoaiXuatNhap,
	@MaDaiLyHQ,
	@SoHoaDon,
	@NgayLapHoaDon,
	@DiaDiemLapHoaDon,
	@PhuongThucThanhToan,
	@PhanLoaiLienQuan,
	@MaNguoiXNK,
	@TenNguoiXNK,
	@MaBuuChinh_NguoiXNK,
	@DiaChi_NguoiXNK,
	@SoDienThoai_NguoiXNK,
	@NguoiLapHoaDon,
	@MaNguoiGuiNhan,
	@TenNguoiGuiNhan,
	@MaBuuChinhNguoiGuiNhan,
	@DiaChiNguoiNhanGui1,
	@DiaChiNguoiNhanGui2,
	@DiaChiNguoiNhanGui3,
	@DiaChiNguoiNhanGui4,
	@SoDienThoaiNhanGui,
	@NuocSoTaiNhanGui,
	@MaKyHieu,
	@PhanLoaiVanChuyen,
	@TenPTVC,
	@SoHieuChuyenDi,
	@MaDiaDiemXepHang,
	@TenDiaDiemXepHang,
	@ThoiKyXephang,
	@MaDiaDiemDoHang,
	@TenDiaDiemDoHang,
	@MaDiaDiemTrungChuyen,
	@TenDiaDiemTrungChuyen,
	@TrongLuongGross,
	@MaDVT_TrongLuongGross,
	@TrongLuongThuan,
	@MaDVT_TrongLuongThuan,
	@TongTheTich,
	@MaDVT_TheTich,
	@TongSoKienHang,
	@MaDVT_KienHang,
	@GhiChuChuHang,
	@SoPL,
	@NganHangLC,
	@TriGiaFOB,
	@MaTT_FOB,
	@SoTienFOB,
	@MaTT_TienFOB,
	@PhiVanChuyen,
	@MaTT_PhiVC,
	@NoiThanhToanPhiVC,
	@ChiPhiXepHang1,
	@MaTT_ChiPhiXepHang1,
	@LoaiChiPhiXepHang1,
	@ChiPhiXepHang2,
	@MaTT_ChiPhiXepHang2,
	@LoaiChiPhiXepHang2,
	@PhiVC_DuongBo,
	@MaTT_PhiVC_DuongBo,
	@PhiBaoHiem,
	@MaTT_PhiBaoHiem,
	@SoTienPhiBaoHiem,
	@MaTT_TienPhiBaoHiem,
	@SoTienKhauTru,
	@MaTT_TienKhauTru,
	@LoaiKhauTru,
	@SoTienKhac,
	@MaTT_TienKhac,
	@LoaiSoTienKhac,
	@TongTriGiaHoaDon,
	@MaTT_TongTriGia,
	@DieuKienGiaHoaDon,
	@DiaDiemGiaoHang,
	@GhiChuDacBiet,
	@TongSoDongHang,
	@TrangThaiXuLy,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_InsertUpdate]    Script Date: 11/11/2013 17:34:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HoaDon_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_InsertUpdate]
	@ID bigint,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@PhanLoaiXuatNhap varchar(1),
	@MaDaiLyHQ varchar(5),
	@SoHoaDon varchar(35),
	@NgayLapHoaDon datetime,
	@DiaDiemLapHoaDon nvarchar(35),
	@PhuongThucThanhToan nvarchar(30),
	@PhanLoaiLienQuan varchar(6),
	@MaNguoiXNK varchar(13),
	@TenNguoiXNK nvarchar(100),
	@MaBuuChinh_NguoiXNK varchar(7),
	@DiaChi_NguoiXNK nvarchar(100),
	@SoDienThoai_NguoiXNK varchar(20),
	@NguoiLapHoaDon nvarchar(60),
	@MaNguoiGuiNhan varchar(13),
	@TenNguoiGuiNhan nvarchar(70),
	@MaBuuChinhNguoiGuiNhan varchar(9),
	@DiaChiNguoiNhanGui1 nvarchar(35),
	@DiaChiNguoiNhanGui2 nvarchar(35),
	@DiaChiNguoiNhanGui3 nvarchar(35),
	@DiaChiNguoiNhanGui4 nvarchar(35),
	@SoDienThoaiNhanGui varchar(12),
	@NuocSoTaiNhanGui varchar(2),
	@MaKyHieu nvarchar(140),
	@PhanLoaiVanChuyen varchar(4),
	@TenPTVC nvarchar(35),
	@SoHieuChuyenDi varchar(10),
	@MaDiaDiemXepHang varchar(5),
	@TenDiaDiemXepHang nvarchar(20),
	@ThoiKyXephang datetime,
	@MaDiaDiemDoHang varchar(5),
	@TenDiaDiemDoHang nvarchar(20),
	@MaDiaDiemTrungChuyen nvarchar(5),
	@TenDiaDiemTrungChuyen nvarchar(30),
	@TrongLuongGross numeric(14, 4),
	@MaDVT_TrongLuongGross varchar(3),
	@TrongLuongThuan numeric(14, 4),
	@MaDVT_TrongLuongThuan nvarchar(3),
	@TongTheTich numeric(14, 4),
	@MaDVT_TheTich nvarchar(3),
	@TongSoKienHang numeric(8, 0),
	@MaDVT_KienHang varchar(3),
	@GhiChuChuHang nvarchar(70),
	@SoPL varchar(10),
	@NganHangLC nvarchar(80),
	@TriGiaFOB numeric(24, 4),
	@MaTT_FOB varchar(3),
	@SoTienFOB numeric(17, 4),
	@MaTT_TienFOB varchar(3),
	@PhiVanChuyen numeric(22, 4),
	@MaTT_PhiVC varchar(3),
	@NoiThanhToanPhiVC nvarchar(20),
	@ChiPhiXepHang1 numeric(13, 4),
	@MaTT_ChiPhiXepHang1 nvarchar(3),
	@LoaiChiPhiXepHang1 nvarchar(20),
	@ChiPhiXepHang2 numeric(13, 4),
	@MaTT_ChiPhiXepHang2 nvarchar(3),
	@LoaiChiPhiXepHang2 nvarchar(20),
	@PhiVC_DuongBo numeric(13, 4),
	@MaTT_PhiVC_DuongBo varchar(3),
	@PhiBaoHiem numeric(22, 4),
	@MaTT_PhiBaoHiem varchar(3),
	@SoTienPhiBaoHiem numeric(13, 4),
	@MaTT_TienPhiBaoHiem varchar(3),
	@SoTienKhauTru numeric(13, 4),
	@MaTT_TienKhauTru varchar(3),
	@LoaiKhauTru nvarchar(20),
	@SoTienKhac numeric(13, 0),
	@MaTT_TienKhac varchar(3),
	@LoaiSoTienKhac nvarchar(20),
	@TongTriGiaHoaDon numeric(24, 4),
	@MaTT_TongTriGia varchar(3),
	@DieuKienGiaHoaDon varchar(20),
	@DiaDiemGiaoHang nvarchar(30),
	@GhiChuDacBiet nvarchar(100),
	@TongSoDongHang numeric(4, 0),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HoaDon] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HoaDon] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[PhanLoaiXuatNhap] = @PhanLoaiXuatNhap,
			[MaDaiLyHQ] = @MaDaiLyHQ,
			[SoHoaDon] = @SoHoaDon,
			[NgayLapHoaDon] = @NgayLapHoaDon,
			[DiaDiemLapHoaDon] = @DiaDiemLapHoaDon,
			[PhuongThucThanhToan] = @PhuongThucThanhToan,
			[PhanLoaiLienQuan] = @PhanLoaiLienQuan,
			[MaNguoiXNK] = @MaNguoiXNK,
			[TenNguoiXNK] = @TenNguoiXNK,
			[MaBuuChinh_NguoiXNK] = @MaBuuChinh_NguoiXNK,
			[DiaChi_NguoiXNK] = @DiaChi_NguoiXNK,
			[SoDienThoai_NguoiXNK] = @SoDienThoai_NguoiXNK,
			[NguoiLapHoaDon] = @NguoiLapHoaDon,
			[MaNguoiGuiNhan] = @MaNguoiGuiNhan,
			[TenNguoiGuiNhan] = @TenNguoiGuiNhan,
			[MaBuuChinhNguoiGuiNhan] = @MaBuuChinhNguoiGuiNhan,
			[DiaChiNguoiNhanGui1] = @DiaChiNguoiNhanGui1,
			[DiaChiNguoiNhanGui2] = @DiaChiNguoiNhanGui2,
			[DiaChiNguoiNhanGui3] = @DiaChiNguoiNhanGui3,
			[DiaChiNguoiNhanGui4] = @DiaChiNguoiNhanGui4,
			[SoDienThoaiNhanGui] = @SoDienThoaiNhanGui,
			[NuocSoTaiNhanGui] = @NuocSoTaiNhanGui,
			[MaKyHieu] = @MaKyHieu,
			[PhanLoaiVanChuyen] = @PhanLoaiVanChuyen,
			[TenPTVC] = @TenPTVC,
			[SoHieuChuyenDi] = @SoHieuChuyenDi,
			[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
			[TenDiaDiemXepHang] = @TenDiaDiemXepHang,
			[ThoiKyXephang] = @ThoiKyXephang,
			[MaDiaDiemDoHang] = @MaDiaDiemDoHang,
			[TenDiaDiemDoHang] = @TenDiaDiemDoHang,
			[MaDiaDiemTrungChuyen] = @MaDiaDiemTrungChuyen,
			[TenDiaDiemTrungChuyen] = @TenDiaDiemTrungChuyen,
			[TrongLuongGross] = @TrongLuongGross,
			[MaDVT_TrongLuongGross] = @MaDVT_TrongLuongGross,
			[TrongLuongThuan] = @TrongLuongThuan,
			[MaDVT_TrongLuongThuan] = @MaDVT_TrongLuongThuan,
			[TongTheTich] = @TongTheTich,
			[MaDVT_TheTich] = @MaDVT_TheTich,
			[TongSoKienHang] = @TongSoKienHang,
			[MaDVT_KienHang] = @MaDVT_KienHang,
			[GhiChuChuHang] = @GhiChuChuHang,
			[SoPL] = @SoPL,
			[NganHangLC] = @NganHangLC,
			[TriGiaFOB] = @TriGiaFOB,
			[MaTT_FOB] = @MaTT_FOB,
			[SoTienFOB] = @SoTienFOB,
			[MaTT_TienFOB] = @MaTT_TienFOB,
			[PhiVanChuyen] = @PhiVanChuyen,
			[MaTT_PhiVC] = @MaTT_PhiVC,
			[NoiThanhToanPhiVC] = @NoiThanhToanPhiVC,
			[ChiPhiXepHang1] = @ChiPhiXepHang1,
			[MaTT_ChiPhiXepHang1] = @MaTT_ChiPhiXepHang1,
			[LoaiChiPhiXepHang1] = @LoaiChiPhiXepHang1,
			[ChiPhiXepHang2] = @ChiPhiXepHang2,
			[MaTT_ChiPhiXepHang2] = @MaTT_ChiPhiXepHang2,
			[LoaiChiPhiXepHang2] = @LoaiChiPhiXepHang2,
			[PhiVC_DuongBo] = @PhiVC_DuongBo,
			[MaTT_PhiVC_DuongBo] = @MaTT_PhiVC_DuongBo,
			[PhiBaoHiem] = @PhiBaoHiem,
			[MaTT_PhiBaoHiem] = @MaTT_PhiBaoHiem,
			[SoTienPhiBaoHiem] = @SoTienPhiBaoHiem,
			[MaTT_TienPhiBaoHiem] = @MaTT_TienPhiBaoHiem,
			[SoTienKhauTru] = @SoTienKhauTru,
			[MaTT_TienKhauTru] = @MaTT_TienKhauTru,
			[LoaiKhauTru] = @LoaiKhauTru,
			[SoTienKhac] = @SoTienKhac,
			[MaTT_TienKhac] = @MaTT_TienKhac,
			[LoaiSoTienKhac] = @LoaiSoTienKhac,
			[TongTriGiaHoaDon] = @TongTriGiaHoaDon,
			[MaTT_TongTriGia] = @MaTT_TongTriGia,
			[DieuKienGiaHoaDon] = @DieuKienGiaHoaDon,
			[DiaDiemGiaoHang] = @DiaDiemGiaoHang,
			[GhiChuDacBiet] = @GhiChuDacBiet,
			[TongSoDongHang] = @TongSoDongHang,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HoaDon]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[PhanLoaiXuatNhap],
			[MaDaiLyHQ],
			[SoHoaDon],
			[NgayLapHoaDon],
			[DiaDiemLapHoaDon],
			[PhuongThucThanhToan],
			[PhanLoaiLienQuan],
			[MaNguoiXNK],
			[TenNguoiXNK],
			[MaBuuChinh_NguoiXNK],
			[DiaChi_NguoiXNK],
			[SoDienThoai_NguoiXNK],
			[NguoiLapHoaDon],
			[MaNguoiGuiNhan],
			[TenNguoiGuiNhan],
			[MaBuuChinhNguoiGuiNhan],
			[DiaChiNguoiNhanGui1],
			[DiaChiNguoiNhanGui2],
			[DiaChiNguoiNhanGui3],
			[DiaChiNguoiNhanGui4],
			[SoDienThoaiNhanGui],
			[NuocSoTaiNhanGui],
			[MaKyHieu],
			[PhanLoaiVanChuyen],
			[TenPTVC],
			[SoHieuChuyenDi],
			[MaDiaDiemXepHang],
			[TenDiaDiemXepHang],
			[ThoiKyXephang],
			[MaDiaDiemDoHang],
			[TenDiaDiemDoHang],
			[MaDiaDiemTrungChuyen],
			[TenDiaDiemTrungChuyen],
			[TrongLuongGross],
			[MaDVT_TrongLuongGross],
			[TrongLuongThuan],
			[MaDVT_TrongLuongThuan],
			[TongTheTich],
			[MaDVT_TheTich],
			[TongSoKienHang],
			[MaDVT_KienHang],
			[GhiChuChuHang],
			[SoPL],
			[NganHangLC],
			[TriGiaFOB],
			[MaTT_FOB],
			[SoTienFOB],
			[MaTT_TienFOB],
			[PhiVanChuyen],
			[MaTT_PhiVC],
			[NoiThanhToanPhiVC],
			[ChiPhiXepHang1],
			[MaTT_ChiPhiXepHang1],
			[LoaiChiPhiXepHang1],
			[ChiPhiXepHang2],
			[MaTT_ChiPhiXepHang2],
			[LoaiChiPhiXepHang2],
			[PhiVC_DuongBo],
			[MaTT_PhiVC_DuongBo],
			[PhiBaoHiem],
			[MaTT_PhiBaoHiem],
			[SoTienPhiBaoHiem],
			[MaTT_TienPhiBaoHiem],
			[SoTienKhauTru],
			[MaTT_TienKhauTru],
			[LoaiKhauTru],
			[SoTienKhac],
			[MaTT_TienKhac],
			[LoaiSoTienKhac],
			[TongTriGiaHoaDon],
			[MaTT_TongTriGia],
			[DieuKienGiaHoaDon],
			[DiaDiemGiaoHang],
			[GhiChuDacBiet],
			[TongSoDongHang],
			[TrangThaiXuLy],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@PhanLoaiXuatNhap,
			@MaDaiLyHQ,
			@SoHoaDon,
			@NgayLapHoaDon,
			@DiaDiemLapHoaDon,
			@PhuongThucThanhToan,
			@PhanLoaiLienQuan,
			@MaNguoiXNK,
			@TenNguoiXNK,
			@MaBuuChinh_NguoiXNK,
			@DiaChi_NguoiXNK,
			@SoDienThoai_NguoiXNK,
			@NguoiLapHoaDon,
			@MaNguoiGuiNhan,
			@TenNguoiGuiNhan,
			@MaBuuChinhNguoiGuiNhan,
			@DiaChiNguoiNhanGui1,
			@DiaChiNguoiNhanGui2,
			@DiaChiNguoiNhanGui3,
			@DiaChiNguoiNhanGui4,
			@SoDienThoaiNhanGui,
			@NuocSoTaiNhanGui,
			@MaKyHieu,
			@PhanLoaiVanChuyen,
			@TenPTVC,
			@SoHieuChuyenDi,
			@MaDiaDiemXepHang,
			@TenDiaDiemXepHang,
			@ThoiKyXephang,
			@MaDiaDiemDoHang,
			@TenDiaDiemDoHang,
			@MaDiaDiemTrungChuyen,
			@TenDiaDiemTrungChuyen,
			@TrongLuongGross,
			@MaDVT_TrongLuongGross,
			@TrongLuongThuan,
			@MaDVT_TrongLuongThuan,
			@TongTheTich,
			@MaDVT_TheTich,
			@TongSoKienHang,
			@MaDVT_KienHang,
			@GhiChuChuHang,
			@SoPL,
			@NganHangLC,
			@TriGiaFOB,
			@MaTT_FOB,
			@SoTienFOB,
			@MaTT_TienFOB,
			@PhiVanChuyen,
			@MaTT_PhiVC,
			@NoiThanhToanPhiVC,
			@ChiPhiXepHang1,
			@MaTT_ChiPhiXepHang1,
			@LoaiChiPhiXepHang1,
			@ChiPhiXepHang2,
			@MaTT_ChiPhiXepHang2,
			@LoaiChiPhiXepHang2,
			@PhiVC_DuongBo,
			@MaTT_PhiVC_DuongBo,
			@PhiBaoHiem,
			@MaTT_PhiBaoHiem,
			@SoTienPhiBaoHiem,
			@MaTT_TienPhiBaoHiem,
			@SoTienKhauTru,
			@MaTT_TienKhauTru,
			@LoaiKhauTru,
			@SoTienKhac,
			@MaTT_TienKhac,
			@LoaiSoTienKhac,
			@TongTriGiaHoaDon,
			@MaTT_TongTriGia,
			@DieuKienGiaHoaDon,
			@DiaDiemGiaoHang,
			@GhiChuDacBiet,
			@TongSoDongHang,
			@TrangThaiXuLy,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_Load]    Script Date: 11/11/2013 17:34:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HoaDon_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLoaiXuatNhap],
	[MaDaiLyHQ],
	[SoHoaDon],
	[NgayLapHoaDon],
	[DiaDiemLapHoaDon],
	[PhuongThucThanhToan],
	[PhanLoaiLienQuan],
	[MaNguoiXNK],
	[TenNguoiXNK],
	[MaBuuChinh_NguoiXNK],
	[DiaChi_NguoiXNK],
	[SoDienThoai_NguoiXNK],
	[NguoiLapHoaDon],
	[MaNguoiGuiNhan],
	[TenNguoiGuiNhan],
	[MaBuuChinhNguoiGuiNhan],
	[DiaChiNguoiNhanGui1],
	[DiaChiNguoiNhanGui2],
	[DiaChiNguoiNhanGui3],
	[DiaChiNguoiNhanGui4],
	[SoDienThoaiNhanGui],
	[NuocSoTaiNhanGui],
	[MaKyHieu],
	[PhanLoaiVanChuyen],
	[TenPTVC],
	[SoHieuChuyenDi],
	[MaDiaDiemXepHang],
	[TenDiaDiemXepHang],
	[ThoiKyXephang],
	[MaDiaDiemDoHang],
	[TenDiaDiemDoHang],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[TrongLuongGross],
	[MaDVT_TrongLuongGross],
	[TrongLuongThuan],
	[MaDVT_TrongLuongThuan],
	[TongTheTich],
	[MaDVT_TheTich],
	[TongSoKienHang],
	[MaDVT_KienHang],
	[GhiChuChuHang],
	[SoPL],
	[NganHangLC],
	[TriGiaFOB],
	[MaTT_FOB],
	[SoTienFOB],
	[MaTT_TienFOB],
	[PhiVanChuyen],
	[MaTT_PhiVC],
	[NoiThanhToanPhiVC],
	[ChiPhiXepHang1],
	[MaTT_ChiPhiXepHang1],
	[LoaiChiPhiXepHang1],
	[ChiPhiXepHang2],
	[MaTT_ChiPhiXepHang2],
	[LoaiChiPhiXepHang2],
	[PhiVC_DuongBo],
	[MaTT_PhiVC_DuongBo],
	[PhiBaoHiem],
	[MaTT_PhiBaoHiem],
	[SoTienPhiBaoHiem],
	[MaTT_TienPhiBaoHiem],
	[SoTienKhauTru],
	[MaTT_TienKhauTru],
	[LoaiKhauTru],
	[SoTienKhac],
	[MaTT_TienKhac],
	[LoaiSoTienKhac],
	[TongTriGiaHoaDon],
	[MaTT_TongTriGia],
	[DieuKienGiaHoaDon],
	[DiaDiemGiaoHang],
	[GhiChuDacBiet],
	[TongSoDongHang],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HoaDon]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_SelectAll]    Script Date: 11/11/2013 17:34:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HoaDon_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLoaiXuatNhap],
	[MaDaiLyHQ],
	[SoHoaDon],
	[NgayLapHoaDon],
	[DiaDiemLapHoaDon],
	[PhuongThucThanhToan],
	[PhanLoaiLienQuan],
	[MaNguoiXNK],
	[TenNguoiXNK],
	[MaBuuChinh_NguoiXNK],
	[DiaChi_NguoiXNK],
	[SoDienThoai_NguoiXNK],
	[NguoiLapHoaDon],
	[MaNguoiGuiNhan],
	[TenNguoiGuiNhan],
	[MaBuuChinhNguoiGuiNhan],
	[DiaChiNguoiNhanGui1],
	[DiaChiNguoiNhanGui2],
	[DiaChiNguoiNhanGui3],
	[DiaChiNguoiNhanGui4],
	[SoDienThoaiNhanGui],
	[NuocSoTaiNhanGui],
	[MaKyHieu],
	[PhanLoaiVanChuyen],
	[TenPTVC],
	[SoHieuChuyenDi],
	[MaDiaDiemXepHang],
	[TenDiaDiemXepHang],
	[ThoiKyXephang],
	[MaDiaDiemDoHang],
	[TenDiaDiemDoHang],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[TrongLuongGross],
	[MaDVT_TrongLuongGross],
	[TrongLuongThuan],
	[MaDVT_TrongLuongThuan],
	[TongTheTich],
	[MaDVT_TheTich],
	[TongSoKienHang],
	[MaDVT_KienHang],
	[GhiChuChuHang],
	[SoPL],
	[NganHangLC],
	[TriGiaFOB],
	[MaTT_FOB],
	[SoTienFOB],
	[MaTT_TienFOB],
	[PhiVanChuyen],
	[MaTT_PhiVC],
	[NoiThanhToanPhiVC],
	[ChiPhiXepHang1],
	[MaTT_ChiPhiXepHang1],
	[LoaiChiPhiXepHang1],
	[ChiPhiXepHang2],
	[MaTT_ChiPhiXepHang2],
	[LoaiChiPhiXepHang2],
	[PhiVC_DuongBo],
	[MaTT_PhiVC_DuongBo],
	[PhiBaoHiem],
	[MaTT_PhiBaoHiem],
	[SoTienPhiBaoHiem],
	[MaTT_TienPhiBaoHiem],
	[SoTienKhauTru],
	[MaTT_TienKhauTru],
	[LoaiKhauTru],
	[SoTienKhac],
	[MaTT_TienKhac],
	[LoaiSoTienKhac],
	[TongTriGiaHoaDon],
	[MaTT_TongTriGia],
	[DieuKienGiaHoaDon],
	[DiaDiemGiaoHang],
	[GhiChuDacBiet],
	[TongSoDongHang],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HoaDon]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_SelectDynamic]    Script Date: 11/11/2013 17:34:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HoaDon_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLoaiXuatNhap],
	[MaDaiLyHQ],
	[SoHoaDon],
	[NgayLapHoaDon],
	[DiaDiemLapHoaDon],
	[PhuongThucThanhToan],
	[PhanLoaiLienQuan],
	[MaNguoiXNK],
	[TenNguoiXNK],
	[MaBuuChinh_NguoiXNK],
	[DiaChi_NguoiXNK],
	[SoDienThoai_NguoiXNK],
	[NguoiLapHoaDon],
	[MaNguoiGuiNhan],
	[TenNguoiGuiNhan],
	[MaBuuChinhNguoiGuiNhan],
	[DiaChiNguoiNhanGui1],
	[DiaChiNguoiNhanGui2],
	[DiaChiNguoiNhanGui3],
	[DiaChiNguoiNhanGui4],
	[SoDienThoaiNhanGui],
	[NuocSoTaiNhanGui],
	[MaKyHieu],
	[PhanLoaiVanChuyen],
	[TenPTVC],
	[SoHieuChuyenDi],
	[MaDiaDiemXepHang],
	[TenDiaDiemXepHang],
	[ThoiKyXephang],
	[MaDiaDiemDoHang],
	[TenDiaDiemDoHang],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[TrongLuongGross],
	[MaDVT_TrongLuongGross],
	[TrongLuongThuan],
	[MaDVT_TrongLuongThuan],
	[TongTheTich],
	[MaDVT_TheTich],
	[TongSoKienHang],
	[MaDVT_KienHang],
	[GhiChuChuHang],
	[SoPL],
	[NganHangLC],
	[TriGiaFOB],
	[MaTT_FOB],
	[SoTienFOB],
	[MaTT_TienFOB],
	[PhiVanChuyen],
	[MaTT_PhiVC],
	[NoiThanhToanPhiVC],
	[ChiPhiXepHang1],
	[MaTT_ChiPhiXepHang1],
	[LoaiChiPhiXepHang1],
	[ChiPhiXepHang2],
	[MaTT_ChiPhiXepHang2],
	[LoaiChiPhiXepHang2],
	[PhiVC_DuongBo],
	[MaTT_PhiVC_DuongBo],
	[PhiBaoHiem],
	[MaTT_PhiBaoHiem],
	[SoTienPhiBaoHiem],
	[MaTT_TienPhiBaoHiem],
	[SoTienKhauTru],
	[MaTT_TienKhauTru],
	[LoaiKhauTru],
	[SoTienKhac],
	[MaTT_TienKhac],
	[LoaiSoTienKhac],
	[TongTriGiaHoaDon],
	[MaTT_TongTriGia],
	[DieuKienGiaHoaDon],
	[DiaDiemGiaoHang],
	[GhiChuDacBiet],
	[TongSoDongHang],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_HoaDon] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HoaDon_Update]    Script Date: 11/11/2013 17:34:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HoaDon_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HoaDon_Update]
	@ID bigint,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@PhanLoaiXuatNhap varchar(1),
	@MaDaiLyHQ varchar(5),
	@SoHoaDon varchar(35),
	@NgayLapHoaDon datetime,
	@DiaDiemLapHoaDon nvarchar(35),
	@PhuongThucThanhToan nvarchar(30),
	@PhanLoaiLienQuan varchar(6),
	@MaNguoiXNK varchar(13),
	@TenNguoiXNK nvarchar(100),
	@MaBuuChinh_NguoiXNK varchar(7),
	@DiaChi_NguoiXNK nvarchar(100),
	@SoDienThoai_NguoiXNK varchar(20),
	@NguoiLapHoaDon nvarchar(60),
	@MaNguoiGuiNhan varchar(13),
	@TenNguoiGuiNhan nvarchar(70),
	@MaBuuChinhNguoiGuiNhan varchar(9),
	@DiaChiNguoiNhanGui1 nvarchar(35),
	@DiaChiNguoiNhanGui2 nvarchar(35),
	@DiaChiNguoiNhanGui3 nvarchar(35),
	@DiaChiNguoiNhanGui4 nvarchar(35),
	@SoDienThoaiNhanGui varchar(12),
	@NuocSoTaiNhanGui varchar(2),
	@MaKyHieu nvarchar(140),
	@PhanLoaiVanChuyen varchar(4),
	@TenPTVC nvarchar(35),
	@SoHieuChuyenDi varchar(10),
	@MaDiaDiemXepHang varchar(5),
	@TenDiaDiemXepHang nvarchar(20),
	@ThoiKyXephang datetime,
	@MaDiaDiemDoHang varchar(5),
	@TenDiaDiemDoHang nvarchar(20),
	@MaDiaDiemTrungChuyen nvarchar(5),
	@TenDiaDiemTrungChuyen nvarchar(30),
	@TrongLuongGross numeric(14, 4),
	@MaDVT_TrongLuongGross varchar(3),
	@TrongLuongThuan numeric(14, 4),
	@MaDVT_TrongLuongThuan nvarchar(3),
	@TongTheTich numeric(14, 4),
	@MaDVT_TheTich nvarchar(3),
	@TongSoKienHang numeric(8, 0),
	@MaDVT_KienHang varchar(3),
	@GhiChuChuHang nvarchar(70),
	@SoPL varchar(10),
	@NganHangLC nvarchar(80),
	@TriGiaFOB numeric(24, 4),
	@MaTT_FOB varchar(3),
	@SoTienFOB numeric(17, 4),
	@MaTT_TienFOB varchar(3),
	@PhiVanChuyen numeric(22, 4),
	@MaTT_PhiVC varchar(3),
	@NoiThanhToanPhiVC nvarchar(20),
	@ChiPhiXepHang1 numeric(13, 4),
	@MaTT_ChiPhiXepHang1 nvarchar(3),
	@LoaiChiPhiXepHang1 nvarchar(20),
	@ChiPhiXepHang2 numeric(13, 4),
	@MaTT_ChiPhiXepHang2 nvarchar(3),
	@LoaiChiPhiXepHang2 nvarchar(20),
	@PhiVC_DuongBo numeric(13, 4),
	@MaTT_PhiVC_DuongBo varchar(3),
	@PhiBaoHiem numeric(22, 4),
	@MaTT_PhiBaoHiem varchar(3),
	@SoTienPhiBaoHiem numeric(13, 4),
	@MaTT_TienPhiBaoHiem varchar(3),
	@SoTienKhauTru numeric(13, 4),
	@MaTT_TienKhauTru varchar(3),
	@LoaiKhauTru nvarchar(20),
	@SoTienKhac numeric(13, 0),
	@MaTT_TienKhac varchar(3),
	@LoaiSoTienKhac nvarchar(20),
	@TongTriGiaHoaDon numeric(24, 4),
	@MaTT_TongTriGia varchar(3),
	@DieuKienGiaHoaDon varchar(20),
	@DiaDiemGiaoHang nvarchar(30),
	@GhiChuDacBiet nvarchar(100),
	@TongSoDongHang numeric(4, 0),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HoaDon]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[PhanLoaiXuatNhap] = @PhanLoaiXuatNhap,
	[MaDaiLyHQ] = @MaDaiLyHQ,
	[SoHoaDon] = @SoHoaDon,
	[NgayLapHoaDon] = @NgayLapHoaDon,
	[DiaDiemLapHoaDon] = @DiaDiemLapHoaDon,
	[PhuongThucThanhToan] = @PhuongThucThanhToan,
	[PhanLoaiLienQuan] = @PhanLoaiLienQuan,
	[MaNguoiXNK] = @MaNguoiXNK,
	[TenNguoiXNK] = @TenNguoiXNK,
	[MaBuuChinh_NguoiXNK] = @MaBuuChinh_NguoiXNK,
	[DiaChi_NguoiXNK] = @DiaChi_NguoiXNK,
	[SoDienThoai_NguoiXNK] = @SoDienThoai_NguoiXNK,
	[NguoiLapHoaDon] = @NguoiLapHoaDon,
	[MaNguoiGuiNhan] = @MaNguoiGuiNhan,
	[TenNguoiGuiNhan] = @TenNguoiGuiNhan,
	[MaBuuChinhNguoiGuiNhan] = @MaBuuChinhNguoiGuiNhan,
	[DiaChiNguoiNhanGui1] = @DiaChiNguoiNhanGui1,
	[DiaChiNguoiNhanGui2] = @DiaChiNguoiNhanGui2,
	[DiaChiNguoiNhanGui3] = @DiaChiNguoiNhanGui3,
	[DiaChiNguoiNhanGui4] = @DiaChiNguoiNhanGui4,
	[SoDienThoaiNhanGui] = @SoDienThoaiNhanGui,
	[NuocSoTaiNhanGui] = @NuocSoTaiNhanGui,
	[MaKyHieu] = @MaKyHieu,
	[PhanLoaiVanChuyen] = @PhanLoaiVanChuyen,
	[TenPTVC] = @TenPTVC,
	[SoHieuChuyenDi] = @SoHieuChuyenDi,
	[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
	[TenDiaDiemXepHang] = @TenDiaDiemXepHang,
	[ThoiKyXephang] = @ThoiKyXephang,
	[MaDiaDiemDoHang] = @MaDiaDiemDoHang,
	[TenDiaDiemDoHang] = @TenDiaDiemDoHang,
	[MaDiaDiemTrungChuyen] = @MaDiaDiemTrungChuyen,
	[TenDiaDiemTrungChuyen] = @TenDiaDiemTrungChuyen,
	[TrongLuongGross] = @TrongLuongGross,
	[MaDVT_TrongLuongGross] = @MaDVT_TrongLuongGross,
	[TrongLuongThuan] = @TrongLuongThuan,
	[MaDVT_TrongLuongThuan] = @MaDVT_TrongLuongThuan,
	[TongTheTich] = @TongTheTich,
	[MaDVT_TheTich] = @MaDVT_TheTich,
	[TongSoKienHang] = @TongSoKienHang,
	[MaDVT_KienHang] = @MaDVT_KienHang,
	[GhiChuChuHang] = @GhiChuChuHang,
	[SoPL] = @SoPL,
	[NganHangLC] = @NganHangLC,
	[TriGiaFOB] = @TriGiaFOB,
	[MaTT_FOB] = @MaTT_FOB,
	[SoTienFOB] = @SoTienFOB,
	[MaTT_TienFOB] = @MaTT_TienFOB,
	[PhiVanChuyen] = @PhiVanChuyen,
	[MaTT_PhiVC] = @MaTT_PhiVC,
	[NoiThanhToanPhiVC] = @NoiThanhToanPhiVC,
	[ChiPhiXepHang1] = @ChiPhiXepHang1,
	[MaTT_ChiPhiXepHang1] = @MaTT_ChiPhiXepHang1,
	[LoaiChiPhiXepHang1] = @LoaiChiPhiXepHang1,
	[ChiPhiXepHang2] = @ChiPhiXepHang2,
	[MaTT_ChiPhiXepHang2] = @MaTT_ChiPhiXepHang2,
	[LoaiChiPhiXepHang2] = @LoaiChiPhiXepHang2,
	[PhiVC_DuongBo] = @PhiVC_DuongBo,
	[MaTT_PhiVC_DuongBo] = @MaTT_PhiVC_DuongBo,
	[PhiBaoHiem] = @PhiBaoHiem,
	[MaTT_PhiBaoHiem] = @MaTT_PhiBaoHiem,
	[SoTienPhiBaoHiem] = @SoTienPhiBaoHiem,
	[MaTT_TienPhiBaoHiem] = @MaTT_TienPhiBaoHiem,
	[SoTienKhauTru] = @SoTienKhauTru,
	[MaTT_TienKhauTru] = @MaTT_TienKhauTru,
	[LoaiKhauTru] = @LoaiKhauTru,
	[SoTienKhac] = @SoTienKhac,
	[MaTT_TienKhac] = @MaTT_TienKhac,
	[LoaiSoTienKhac] = @LoaiSoTienKhac,
	[TongTriGiaHoaDon] = @TongTriGiaHoaDon,
	[MaTT_TongTriGia] = @MaTT_TongTriGia,
	[DieuKienGiaHoaDon] = @DieuKienGiaHoaDon,
	[DiaDiemGiaoHang] = @DiaDiemGiaoHang,
	[GhiChuDacBiet] = @GhiChuDacBiet,
	[TongSoDongHang] = @TongSoDongHang,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Delete]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_PhanLoaiHoaDon]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_DeleteDynamic]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_PhanLoaiHoaDon] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Insert]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Insert]
	@HoaDon_ID bigint,
	@MaPhaLoai varchar(3),
	@So varchar(35),
	@NgayThangNam datetime,
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_PhanLoaiHoaDon]
(
	[HoaDon_ID],
	[MaPhaLoai],
	[So],
	[NgayThangNam],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@HoaDon_ID,
	@MaPhaLoai,
	@So,
	@NgayThangNam,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_InsertUpdate]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_InsertUpdate]
	@ID bigint,
	@HoaDon_ID bigint,
	@MaPhaLoai varchar(3),
	@So varchar(35),
	@NgayThangNam datetime,
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_PhanLoaiHoaDon] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_PhanLoaiHoaDon] 
		SET
			[HoaDon_ID] = @HoaDon_ID,
			[MaPhaLoai] = @MaPhaLoai,
			[So] = @So,
			[NgayThangNam] = @NgayThangNam,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_PhanLoaiHoaDon]
		(
			[HoaDon_ID],
			[MaPhaLoai],
			[So],
			[NgayThangNam],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@HoaDon_ID,
			@MaPhaLoai,
			@So,
			@NgayThangNam,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Load]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HoaDon_ID],
	[MaPhaLoai],
	[So],
	[NgayThangNam],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_PhanLoaiHoaDon]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectAll]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HoaDon_ID],
	[MaPhaLoai],
	[So],
	[NgayThangNam],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_PhanLoaiHoaDon]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectDynamic]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HoaDon_ID],
	[MaPhaLoai],
	[So],
	[NgayThangNam],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_PhanLoaiHoaDon] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Update]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_PhanLoaiHoaDon_Update]
	@ID bigint,
	@HoaDon_ID bigint,
	@MaPhaLoai varchar(3),
	@So varchar(35),
	@NgayThangNam datetime,
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_PhanLoaiHoaDon]
SET
	[HoaDon_ID] = @HoaDon_ID,
	[MaPhaLoai] = @MaPhaLoai,
	[So] = @So,
	[NgayThangNam] = @NgayThangNam,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_Delete]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_Container]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_DeleteDynamic]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_Container] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_Insert]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Insert]
	@TKMD_ID bigint,
	@MaDiaDiem1 varchar(7),
	@MaDiaDiem2 varchar(7),
	@MaDiaDiem3 varchar(7),
	@MaDiaDiem4 varchar(7),
	@MaDiaDiem5 varchar(7),
	@TenDiaDiem nvarchar(70),
	@DiaChiDiaDiem nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_Container]
(
	[TKMD_ID],
	[MaDiaDiem1],
	[MaDiaDiem2],
	[MaDiaDiem3],
	[MaDiaDiem4],
	[MaDiaDiem5],
	[TenDiaDiem],
	[DiaChiDiaDiem],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@TKMD_ID,
	@MaDiaDiem1,
	@MaDiaDiem2,
	@MaDiaDiem3,
	@MaDiaDiem4,
	@MaDiaDiem5,
	@TenDiaDiem,
	@DiaChiDiaDiem,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_InsertUpdate]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@MaDiaDiem1 varchar(7),
	@MaDiaDiem2 varchar(7),
	@MaDiaDiem3 varchar(7),
	@MaDiaDiem4 varchar(7),
	@MaDiaDiem5 varchar(7),
	@TenDiaDiem nvarchar(70),
	@DiaChiDiaDiem nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_Container] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_Container] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[MaDiaDiem1] = @MaDiaDiem1,
			[MaDiaDiem2] = @MaDiaDiem2,
			[MaDiaDiem3] = @MaDiaDiem3,
			[MaDiaDiem4] = @MaDiaDiem4,
			[MaDiaDiem5] = @MaDiaDiem5,
			[TenDiaDiem] = @TenDiaDiem,
			[DiaChiDiaDiem] = @DiaChiDiaDiem,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_Container]
		(
			[TKMD_ID],
			[MaDiaDiem1],
			[MaDiaDiem2],
			[MaDiaDiem3],
			[MaDiaDiem4],
			[MaDiaDiem5],
			[TenDiaDiem],
			[DiaChiDiaDiem],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@TKMD_ID,
			@MaDiaDiem1,
			@MaDiaDiem2,
			@MaDiaDiem3,
			@MaDiaDiem4,
			@MaDiaDiem5,
			@TenDiaDiem,
			@DiaChiDiaDiem,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_Load]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaDiaDiem1],
	[MaDiaDiem2],
	[MaDiaDiem3],
	[MaDiaDiem4],
	[MaDiaDiem5],
	[TenDiaDiem],
	[DiaChiDiaDiem],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_Container]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_SelectAll]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaDiaDiem1],
	[MaDiaDiem2],
	[MaDiaDiem3],
	[MaDiaDiem4],
	[MaDiaDiem5],
	[TenDiaDiem],
	[DiaChiDiaDiem],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_Container]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_SelectDynamic]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[MaDiaDiem1],
	[MaDiaDiem2],
	[MaDiaDiem3],
	[MaDiaDiem4],
	[MaDiaDiem5],
	[TenDiaDiem],
	[DiaChiDiaDiem],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_TK_Container] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_Container_Update]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@MaDiaDiem1 varchar(7),
	@MaDiaDiem2 varchar(7),
	@MaDiaDiem3 varchar(7),
	@MaDiaDiem4 varchar(7),
	@MaDiaDiem5 varchar(7),
	@TenDiaDiem nvarchar(70),
	@DiaChiDiaDiem nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_Container]
SET
	[TKMD_ID] = @TKMD_ID,
	[MaDiaDiem1] = @MaDiaDiem1,
	[MaDiaDiem2] = @MaDiaDiem2,
	[MaDiaDiem3] = @MaDiaDiem3,
	[MaDiaDiem4] = @MaDiaDiem4,
	[MaDiaDiem5] = @MaDiaDiem5,
	[TenDiaDiem] = @TenDiaDiem,
	[DiaChiDiaDiem] = @DiaChiDiaDiem,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Delete]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_DinhKemDienTu]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_DeleteDynamic]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_DinhKemDienTu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Insert]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Insert]
	@TKMD_ID bigint,
	@SoTT int,
	@PhanLoai varchar(3),
	@SoDinhKemKhaiBaoDT numeric(12, 0),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_DinhKemDienTu]
(
	[TKMD_ID],
	[SoTT],
	[PhanLoai],
	[SoDinhKemKhaiBaoDT],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@TKMD_ID,
	@SoTT,
	@PhanLoai,
	@SoDinhKemKhaiBaoDT,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_InsertUpdate]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@PhanLoai varchar(3),
	@SoDinhKemKhaiBaoDT numeric(12, 0),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_DinhKemDienTu] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_DinhKemDienTu] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoTT] = @SoTT,
			[PhanLoai] = @PhanLoai,
			[SoDinhKemKhaiBaoDT] = @SoDinhKemKhaiBaoDT,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_DinhKemDienTu]
		(
			[TKMD_ID],
			[SoTT],
			[PhanLoai],
			[SoDinhKemKhaiBaoDT],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoTT,
			@PhanLoai,
			@SoDinhKemKhaiBaoDT,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Load]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[PhanLoai],
	[SoDinhKemKhaiBaoDT],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_DinhKemDienTu]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_SelectAll]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[PhanLoai],
	[SoDinhKemKhaiBaoDT],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_DinhKemDienTu]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_SelectDynamic]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoTT],
	[PhanLoai],
	[SoDinhKemKhaiBaoDT],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_TK_DinhKemDienTu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Update]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_DinhKemDienTu_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@PhanLoai varchar(3),
	@SoDinhKemKhaiBaoDT numeric(12, 0),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_DinhKemDienTu]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoTT] = @SoTT,
	[PhanLoai] = @PhanLoai,
	[SoDinhKemKhaiBaoDT] = @SoDinhKemKhaiBaoDT,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_Delete]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_GiayPhep_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_GiayPhep]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_DeleteDynamic]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_GiayPhep_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_GiayPhep] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_Insert]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_GiayPhep_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_Insert]
	@TKMD_ID bigint,
	@SoTT int,
	@PhanLoai varchar(4),
	@SoGiayPhep varchar(20),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_GiayPhep]
(
	[TKMD_ID],
	[SoTT],
	[PhanLoai],
	[SoGiayPhep],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@TKMD_ID,
	@SoTT,
	@PhanLoai,
	@SoGiayPhep,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_InsertUpdate]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_GiayPhep_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@PhanLoai varchar(4),
	@SoGiayPhep varchar(20),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_GiayPhep] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_GiayPhep] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoTT] = @SoTT,
			[PhanLoai] = @PhanLoai,
			[SoGiayPhep] = @SoGiayPhep,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_GiayPhep]
		(
			[TKMD_ID],
			[SoTT],
			[PhanLoai],
			[SoGiayPhep],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoTT,
			@PhanLoai,
			@SoGiayPhep,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_Load]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_GiayPhep_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[PhanLoai],
	[SoGiayPhep],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_GiayPhep]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_SelectAll]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_GiayPhep_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[PhanLoai],
	[SoGiayPhep],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_GiayPhep]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_SelectDynamic]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_GiayPhep_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoTT],
	[PhanLoai],
	[SoGiayPhep],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_TK_GiayPhep] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_GiayPhep_Update]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_GiayPhep_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_GiayPhep_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@PhanLoai varchar(4),
	@SoGiayPhep varchar(20),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_GiayPhep]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoTT] = @SoTT,
	[PhanLoai] = @PhanLoai,
	[SoGiayPhep] = @SoGiayPhep,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Delete]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_KhoanDieuChinh]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_DeleteDynamic]    Script Date: 11/11/2013 17:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_KhoanDieuChinh] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Insert]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Insert]
	@TKMD_ID bigint,
	@SoTT int,
	@MaTenDieuChinh varchar(1),
	@MaPhanLoaiDieuChinh varchar(3),
	@MaTTDieuChinhTriGia varchar(3),
	@TriGiaKhoanDieuChinh numeric(24, 4),
	@TongHeSoPhanBo numeric(24, 4),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_KhoanDieuChinh]
(
	[TKMD_ID],
	[SoTT],
	[MaTenDieuChinh],
	[MaPhanLoaiDieuChinh],
	[MaTTDieuChinhTriGia],
	[TriGiaKhoanDieuChinh],
	[TongHeSoPhanBo],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@TKMD_ID,
	@SoTT,
	@MaTenDieuChinh,
	@MaPhanLoaiDieuChinh,
	@MaTTDieuChinhTriGia,
	@TriGiaKhoanDieuChinh,
	@TongHeSoPhanBo,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_InsertUpdate]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@MaTenDieuChinh varchar(1),
	@MaPhanLoaiDieuChinh varchar(3),
	@MaTTDieuChinhTriGia varchar(3),
	@TriGiaKhoanDieuChinh numeric(24, 4),
	@TongHeSoPhanBo numeric(24, 4),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_KhoanDieuChinh] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_KhoanDieuChinh] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoTT] = @SoTT,
			[MaTenDieuChinh] = @MaTenDieuChinh,
			[MaPhanLoaiDieuChinh] = @MaPhanLoaiDieuChinh,
			[MaTTDieuChinhTriGia] = @MaTTDieuChinhTriGia,
			[TriGiaKhoanDieuChinh] = @TriGiaKhoanDieuChinh,
			[TongHeSoPhanBo] = @TongHeSoPhanBo,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_KhoanDieuChinh]
		(
			[TKMD_ID],
			[SoTT],
			[MaTenDieuChinh],
			[MaPhanLoaiDieuChinh],
			[MaTTDieuChinhTriGia],
			[TriGiaKhoanDieuChinh],
			[TongHeSoPhanBo],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoTT,
			@MaTenDieuChinh,
			@MaPhanLoaiDieuChinh,
			@MaTTDieuChinhTriGia,
			@TriGiaKhoanDieuChinh,
			@TongHeSoPhanBo,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Load]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[MaTenDieuChinh],
	[MaPhanLoaiDieuChinh],
	[MaTTDieuChinhTriGia],
	[TriGiaKhoanDieuChinh],
	[TongHeSoPhanBo],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_KhoanDieuChinh]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectAll]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[MaTenDieuChinh],
	[MaPhanLoaiDieuChinh],
	[MaTTDieuChinhTriGia],
	[TriGiaKhoanDieuChinh],
	[TongHeSoPhanBo],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_KhoanDieuChinh]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectDynamic]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoTT],
	[MaTenDieuChinh],
	[MaPhanLoaiDieuChinh],
	[MaTTDieuChinhTriGia],
	[TriGiaKhoanDieuChinh],
	[TongHeSoPhanBo],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_TK_KhoanDieuChinh] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Update]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@MaTenDieuChinh varchar(1),
	@MaPhanLoaiDieuChinh varchar(3),
	@MaTTDieuChinhTriGia varchar(3),
	@TriGiaKhoanDieuChinh numeric(24, 4),
	@TongHeSoPhanBo numeric(24, 4),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_KhoanDieuChinh]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoTT] = @SoTT,
	[MaTenDieuChinh] = @MaTenDieuChinh,
	[MaPhanLoaiDieuChinh] = @MaPhanLoaiDieuChinh,
	[MaTTDieuChinhTriGia] = @MaTTDieuChinhTriGia,
	[TriGiaKhoanDieuChinh] = @TriGiaKhoanDieuChinh,
	[TongHeSoPhanBo] = @TongHeSoPhanBo,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Delete]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_DeleteDynamic]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Insert]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Insert]
	@Master_ID bigint,
	@MaSacThueAnHan varchar(1),
	@TenSacThueAnHan nvarchar(9),
	@HanNopThueSauKhiAnHan datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan]
(
	[Master_ID],
	[MaSacThueAnHan],
	[TenSacThueAnHan],
	[HanNopThueSauKhiAnHan]
)
VALUES 
(
	@Master_ID,
	@MaSacThueAnHan,
	@TenSacThueAnHan,
	@HanNopThueSauKhiAnHan
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_InsertUpdate]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaSacThueAnHan varchar(1),
	@TenSacThueAnHan nvarchar(9),
	@HanNopThueSauKhiAnHan datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan] 
		SET
			[Master_ID] = @Master_ID,
			[MaSacThueAnHan] = @MaSacThueAnHan,
			[TenSacThueAnHan] = @TenSacThueAnHan,
			[HanNopThueSauKhiAnHan] = @HanNopThueSauKhiAnHan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan]
		(
			[Master_ID],
			[MaSacThueAnHan],
			[TenSacThueAnHan],
			[HanNopThueSauKhiAnHan]
		)
		VALUES 
		(
			@Master_ID,
			@MaSacThueAnHan,
			@TenSacThueAnHan,
			@HanNopThueSauKhiAnHan
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Load]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaSacThueAnHan],
	[TenSacThueAnHan],
	[HanNopThueSauKhiAnHan]
FROM
	[dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectAll]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaSacThueAnHan],
	[TenSacThueAnHan],
	[HanNopThueSauKhiAnHan]
FROM
	[dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectDynamic]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaSacThueAnHan],
	[TenSacThueAnHan],
	[HanNopThueSauKhiAnHan]
FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Update]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_AnHan_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaSacThueAnHan varchar(1),
	@TenSacThueAnHan nvarchar(9),
	@HanNopThueSauKhiAnHan datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan]
SET
	[Master_ID] = @Master_ID,
	[MaSacThueAnHan] = @MaSacThueAnHan,
	[TenSacThueAnHan] = @TenSacThueAnHan,
	[HanNopThueSauKhiAnHan] = @HanNopThueSauKhiAnHan
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Delete]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_DeleteDynamic]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Insert]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Insert]
	@Master_ID bigint,
	@MaSacThue varchar(1),
	@TenSacThue nvarchar(50),
	@TongTienThue numeric(15, 4),
	@SoDongTongTienThue numeric(2, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue]
(
	[Master_ID],
	[MaSacThue],
	[TenSacThue],
	[TongTienThue],
	[SoDongTongTienThue]
)
VALUES 
(
	@Master_ID,
	@MaSacThue,
	@TenSacThue,
	@TongTienThue,
	@SoDongTongTienThue
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_InsertUpdate]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaSacThue varchar(1),
	@TenSacThue nvarchar(50),
	@TongTienThue numeric(15, 4),
	@SoDongTongTienThue numeric(2, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue] 
		SET
			[Master_ID] = @Master_ID,
			[MaSacThue] = @MaSacThue,
			[TenSacThue] = @TenSacThue,
			[TongTienThue] = @TongTienThue,
			[SoDongTongTienThue] = @SoDongTongTienThue
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue]
		(
			[Master_ID],
			[MaSacThue],
			[TenSacThue],
			[TongTienThue],
			[SoDongTongTienThue]
		)
		VALUES 
		(
			@Master_ID,
			@MaSacThue,
			@TenSacThue,
			@TongTienThue,
			@SoDongTongTienThue
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Load]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaSacThue],
	[TenSacThue],
	[TongTienThue],
	[SoDongTongTienThue]
FROM
	[dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectAll]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaSacThue],
	[TenSacThue],
	[TongTienThue],
	[SoDongTongTienThue]
FROM
	[dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectDynamic]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaSacThue],
	[TenSacThue],
	[TongTienThue],
	[SoDongTongTienThue]
FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Update]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_SacThue_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaSacThue varchar(1),
	@TenSacThue nvarchar(50),
	@TongTienThue numeric(15, 4),
	@SoDongTongTienThue numeric(2, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue]
SET
	[Master_ID] = @Master_ID,
	[MaSacThue] = @MaSacThue,
	[TenSacThue] = @TenSacThue,
	[TongTienThue] = @TongTienThue,
	[SoDongTongTienThue] = @SoDongTongTienThue
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Delete]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_DeleteDynamic]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Insert]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Insert]
	@Master_ID bigint,
	@MaTTTyGiaTinhThue varchar(3),
	@TyGiaTinhThue numeric(9, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]
(
	[Master_ID],
	[MaTTTyGiaTinhThue],
	[TyGiaTinhThue]
)
VALUES 
(
	@Master_ID,
	@MaTTTyGiaTinhThue,
	@TyGiaTinhThue
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_InsertUpdate]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaTTTyGiaTinhThue varchar(3),
	@TyGiaTinhThue numeric(9, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia] 
		SET
			[Master_ID] = @Master_ID,
			[MaTTTyGiaTinhThue] = @MaTTTyGiaTinhThue,
			[TyGiaTinhThue] = @TyGiaTinhThue
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]
		(
			[Master_ID],
			[MaTTTyGiaTinhThue],
			[TyGiaTinhThue]
		)
		VALUES 
		(
			@Master_ID,
			@MaTTTyGiaTinhThue,
			@TyGiaTinhThue
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Load]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaTTTyGiaTinhThue],
	[TyGiaTinhThue]
FROM
	[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_SelectAll]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaTTTyGiaTinhThue],
	[TyGiaTinhThue]
FROM
	[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_SelectDynamic]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaTTTyGiaTinhThue],
	[TyGiaTinhThue]
FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Update]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGia_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaTTTyGiaTinhThue varchar(3),
	@TyGiaTinhThue numeric(9, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]
SET
	[Master_ID] = @Master_ID,
	[MaTTTyGiaTinhThue] = @MaTTTyGiaTinhThue,
	[TyGiaTinhThue] = @TyGiaTinhThue
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Delete]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_DeleteDynamic]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Insert]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Insert]
	@Master_ID bigint,
	@MaTTTyGiaTinhThue varchar(3),
	@TyGiaTinhThue numeric(13, 4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]
(
	[Master_ID],
	[MaTTTyGiaTinhThue],
	[TyGiaTinhThue]
)
VALUES 
(
	@Master_ID,
	@MaTTTyGiaTinhThue,
	@TyGiaTinhThue
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_InsertUpdate]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaTTTyGiaTinhThue varchar(3),
	@TyGiaTinhThue numeric(13, 4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia] 
		SET
			[Master_ID] = @Master_ID,
			[MaTTTyGiaTinhThue] = @MaTTTyGiaTinhThue,
			[TyGiaTinhThue] = @TyGiaTinhThue
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]
		(
			[Master_ID],
			[MaTTTyGiaTinhThue],
			[TyGiaTinhThue]
		)
		VALUES 
		(
			@Master_ID,
			@MaTTTyGiaTinhThue,
			@TyGiaTinhThue
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Load]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaTTTyGiaTinhThue],
	[TyGiaTinhThue]
FROM
	[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectAll]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaTTTyGiaTinhThue],
	[TyGiaTinhThue]
FROM
	[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectDynamic]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaTTTyGiaTinhThue],
	[TyGiaTinhThue]
FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Update]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaTTTyGiaTinhThue varchar(3),
	@TyGiaTinhThue numeric(13, 4)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]
SET
	[Master_ID] = @Master_ID,
	[MaTTTyGiaTinhThue] = @MaTTTyGiaTinhThue,
	[TyGiaTinhThue] = @TyGiaTinhThue
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_Delete]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_SoContainer]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_DeleteDynamic]    Script Date: 11/11/2013 17:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_SoContainer] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_Insert]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Insert]
	@Master_id bigint,
	@SoTT int,
	@SoContainer varchar(12),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_SoContainer]
(
	[Master_id],
	[SoTT],
	[SoContainer],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@Master_id,
	@SoTT,
	@SoContainer,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_InsertUpdate]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_InsertUpdate]
	@ID bigint,
	@Master_id bigint,
	@SoTT int,
	@SoContainer varchar(12),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_SoContainer] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_SoContainer] 
		SET
			[Master_id] = @Master_id,
			[SoTT] = @SoTT,
			[SoContainer] = @SoContainer,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_SoContainer]
		(
			[Master_id],
			[SoTT],
			[SoContainer],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@Master_id,
			@SoTT,
			@SoContainer,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_Load]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[SoTT],
	[SoContainer],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_SoContainer]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_SelectAll]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[SoTT],
	[SoContainer],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_SoContainer]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_SelectDynamic]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_id],
	[SoTT],
	[SoContainer],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_TK_SoContainer] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoContainer_Update]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoContainer_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoContainer_Update]
	@ID bigint,
	@Master_id bigint,
	@SoTT int,
	@SoContainer varchar(12),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_SoContainer]
SET
	[Master_id] = @Master_id,
	[SoTT] = @SoTT,
	[SoContainer] = @SoContainer,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_Delete]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_SoVanDon]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_DeleteDynamic]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_SoVanDon] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_Insert]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Insert]
	@TKMD_ID bigint,
	@SoTT int,
	@SoVanDon varchar(35),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_SoVanDon]
(
	[TKMD_ID],
	[SoTT],
	[SoVanDon],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@TKMD_ID,
	@SoTT,
	@SoVanDon,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_InsertUpdate]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@SoVanDon varchar(35),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_SoVanDon] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_SoVanDon] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoTT] = @SoTT,
			[SoVanDon] = @SoVanDon,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_SoVanDon]
		(
			[TKMD_ID],
			[SoTT],
			[SoVanDon],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoTT,
			@SoVanDon,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_Load]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[SoVanDon],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_SoVanDon]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[SoVanDon],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_SoVanDon]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoTT],
	[SoVanDon],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_TK_SoVanDon] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_SoVanDon_Update]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_SoVanDon_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_SoVanDon_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@SoVanDon varchar(35),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_SoVanDon]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoTT] = @SoTT,
	[SoVanDon] = @SoVanDon,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_Delete]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_TrungChuyen]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_DeleteDynamic]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_TrungChuyen] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_Insert]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Insert]
	@TKMD_ID bigint,
	@SoTT int,
	@MaDiaDiem varchar(7),
	@NgayDen datetime,
	@NgayKhoiHanh datetime,
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_TrungChuyen]
(
	[TKMD_ID],
	[SoTT],
	[MaDiaDiem],
	[NgayDen],
	[NgayKhoiHanh],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@TKMD_ID,
	@SoTT,
	@MaDiaDiem,
	@NgayDen,
	@NgayKhoiHanh,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_InsertUpdate]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@MaDiaDiem varchar(7),
	@NgayDen datetime,
	@NgayKhoiHanh datetime,
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_TrungChuyen] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_TrungChuyen] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoTT] = @SoTT,
			[MaDiaDiem] = @MaDiaDiem,
			[NgayDen] = @NgayDen,
			[NgayKhoiHanh] = @NgayKhoiHanh,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_TrungChuyen]
		(
			[TKMD_ID],
			[SoTT],
			[MaDiaDiem],
			[NgayDen],
			[NgayKhoiHanh],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoTT,
			@MaDiaDiem,
			@NgayDen,
			@NgayKhoiHanh,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_Load]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[MaDiaDiem],
	[NgayDen],
	[NgayKhoiHanh],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_TrungChuyen]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectAll]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[MaDiaDiem],
	[NgayDen],
	[NgayKhoiHanh],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_TrungChuyen]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectDynamic]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoTT],
	[MaDiaDiem],
	[NgayDen],
	[NgayKhoiHanh],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_TK_TrungChuyen] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_TK_TrungChuyen_Update]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_TrungChuyen_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_TrungChuyen_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@MaDiaDiem varchar(7),
	@NgayDen datetime,
	@NgayKhoiHanh datetime,
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_TrungChuyen]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoTT] = @SoTT,
	[MaDiaDiem] = @MaDiaDiem,
	[NgayDen] = @NgayDen,
	[NgayKhoiHanh] = @NgayKhoiHanh,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Delete]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_DeleteDynamic]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Insert]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Insert]
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuanNoiMoToKhai nvarchar(210),
	@SoChungTu numeric(12, 0),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@TenNganHangBaoLanh nvarchar(210),
	@MaNganHangBaoLanh varchar(11),
	@KiHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@LoaiBaoLanh nvarchar(60),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoThueKhaiBao numeric(11, 0),
	@TongSoThueAnDinh numeric(11, 0),
	@TongSoThueChenhLech numeric(12, 0),
	@MaTienTe varchar(3),
	@TyGiaQuyDoi numeric(9, 0),
	@SoNgayDuocAnHan numeric(3, 0),
	@NgayHetHieuLucTamNhapTaiXuat datetime,
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@LaiSuatPhatChamNop nvarchar(765),
	@NgayPhatHanhChungTu datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]
(
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[SoToKhai],
	[NgayDangKyToKhai],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoThueKhaiBao],
	[TongSoThueAnDinh],
	[TongSoThueChenhLech],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
)
VALUES 
(
	@TenCoQuanHaiQuan,
	@TenChiCucHaiQuanNoiMoToKhai,
	@SoChungTu,
	@SoToKhai,
	@NgayDangKyToKhai,
	@TenDonViXuatNhapKhau,
	@MaDonViXuatNhapKhau,
	@MaBuuChinh,
	@DiaChiNguoiXuatNhapKhau,
	@SoDienThoaiNguoiXuatNhapKhau,
	@TenNganHangBaoLanh,
	@MaNganHangBaoLanh,
	@KiHieuChungTuBaoLanh,
	@SoChungTuBaoLanh,
	@LoaiBaoLanh,
	@TenNganHangTraThay,
	@MaNganHangTraThay,
	@KiHieuChungTuPhatHanhHanMuc,
	@SoHieuPhatHanhHanMuc,
	@TongSoThueKhaiBao,
	@TongSoThueAnDinh,
	@TongSoThueChenhLech,
	@MaTienTe,
	@TyGiaQuyDoi,
	@SoNgayDuocAnHan,
	@NgayHetHieuLucTamNhapTaiXuat,
	@SoTaiKhoanKhoBac,
	@TenKhoBac,
	@LaiSuatPhatChamNop,
	@NgayPhatHanhChungTu
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_InsertUpdate]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_InsertUpdate]
	@ID bigint,
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuanNoiMoToKhai nvarchar(210),
	@SoChungTu numeric(12, 0),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@TenNganHangBaoLanh nvarchar(210),
	@MaNganHangBaoLanh varchar(11),
	@KiHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@LoaiBaoLanh nvarchar(60),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoThueKhaiBao numeric(11, 0),
	@TongSoThueAnDinh numeric(11, 0),
	@TongSoThueChenhLech numeric(12, 0),
	@MaTienTe varchar(3),
	@TyGiaQuyDoi numeric(9, 0),
	@SoNgayDuocAnHan numeric(3, 0),
	@NgayHetHieuLucTamNhapTaiXuat datetime,
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@LaiSuatPhatChamNop nvarchar(765),
	@NgayPhatHanhChungTu datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue] 
		SET
			[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
			[TenChiCucHaiQuanNoiMoToKhai] = @TenChiCucHaiQuanNoiMoToKhai,
			[SoChungTu] = @SoChungTu,
			[SoToKhai] = @SoToKhai,
			[NgayDangKyToKhai] = @NgayDangKyToKhai,
			[TenDonViXuatNhapKhau] = @TenDonViXuatNhapKhau,
			[MaDonViXuatNhapKhau] = @MaDonViXuatNhapKhau,
			[MaBuuChinh] = @MaBuuChinh,
			[DiaChiNguoiXuatNhapKhau] = @DiaChiNguoiXuatNhapKhau,
			[SoDienThoaiNguoiXuatNhapKhau] = @SoDienThoaiNguoiXuatNhapKhau,
			[TenNganHangBaoLanh] = @TenNganHangBaoLanh,
			[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
			[KiHieuChungTuBaoLanh] = @KiHieuChungTuBaoLanh,
			[SoChungTuBaoLanh] = @SoChungTuBaoLanh,
			[LoaiBaoLanh] = @LoaiBaoLanh,
			[TenNganHangTraThay] = @TenNganHangTraThay,
			[MaNganHangTraThay] = @MaNganHangTraThay,
			[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
			[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
			[TongSoThueKhaiBao] = @TongSoThueKhaiBao,
			[TongSoThueAnDinh] = @TongSoThueAnDinh,
			[TongSoThueChenhLech] = @TongSoThueChenhLech,
			[MaTienTe] = @MaTienTe,
			[TyGiaQuyDoi] = @TyGiaQuyDoi,
			[SoNgayDuocAnHan] = @SoNgayDuocAnHan,
			[NgayHetHieuLucTamNhapTaiXuat] = @NgayHetHieuLucTamNhapTaiXuat,
			[SoTaiKhoanKhoBac] = @SoTaiKhoanKhoBac,
			[TenKhoBac] = @TenKhoBac,
			[LaiSuatPhatChamNop] = @LaiSuatPhatChamNop,
			[NgayPhatHanhChungTu] = @NgayPhatHanhChungTu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]
		(
			[TenCoQuanHaiQuan],
			[TenChiCucHaiQuanNoiMoToKhai],
			[SoChungTu],
			[SoToKhai],
			[NgayDangKyToKhai],
			[TenDonViXuatNhapKhau],
			[MaDonViXuatNhapKhau],
			[MaBuuChinh],
			[DiaChiNguoiXuatNhapKhau],
			[SoDienThoaiNguoiXuatNhapKhau],
			[TenNganHangBaoLanh],
			[MaNganHangBaoLanh],
			[KiHieuChungTuBaoLanh],
			[SoChungTuBaoLanh],
			[LoaiBaoLanh],
			[TenNganHangTraThay],
			[MaNganHangTraThay],
			[KiHieuChungTuPhatHanhHanMuc],
			[SoHieuPhatHanhHanMuc],
			[TongSoThueKhaiBao],
			[TongSoThueAnDinh],
			[TongSoThueChenhLech],
			[MaTienTe],
			[TyGiaQuyDoi],
			[SoNgayDuocAnHan],
			[NgayHetHieuLucTamNhapTaiXuat],
			[SoTaiKhoanKhoBac],
			[TenKhoBac],
			[LaiSuatPhatChamNop],
			[NgayPhatHanhChungTu]
		)
		VALUES 
		(
			@TenCoQuanHaiQuan,
			@TenChiCucHaiQuanNoiMoToKhai,
			@SoChungTu,
			@SoToKhai,
			@NgayDangKyToKhai,
			@TenDonViXuatNhapKhau,
			@MaDonViXuatNhapKhau,
			@MaBuuChinh,
			@DiaChiNguoiXuatNhapKhau,
			@SoDienThoaiNguoiXuatNhapKhau,
			@TenNganHangBaoLanh,
			@MaNganHangBaoLanh,
			@KiHieuChungTuBaoLanh,
			@SoChungTuBaoLanh,
			@LoaiBaoLanh,
			@TenNganHangTraThay,
			@MaNganHangTraThay,
			@KiHieuChungTuPhatHanhHanMuc,
			@SoHieuPhatHanhHanMuc,
			@TongSoThueKhaiBao,
			@TongSoThueAnDinh,
			@TongSoThueChenhLech,
			@MaTienTe,
			@TyGiaQuyDoi,
			@SoNgayDuocAnHan,
			@NgayHetHieuLucTamNhapTaiXuat,
			@SoTaiKhoanKhoBac,
			@TenKhoBac,
			@LaiSuatPhatChamNop,
			@NgayPhatHanhChungTu
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Load]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[SoToKhai],
	[NgayDangKyToKhai],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoThueKhaiBao],
	[TongSoThueAnDinh],
	[TongSoThueChenhLech],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectAll]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[SoToKhai],
	[NgayDangKyToKhai],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoThueKhaiBao],
	[TongSoThueAnDinh],
	[TongSoThueChenhLech],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectDynamic]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuanNoiMoToKhai],
	[SoChungTu],
	[SoToKhai],
	[NgayDangKyToKhai],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[TenNganHangBaoLanh],
	[MaNganHangBaoLanh],
	[KiHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[LoaiBaoLanh],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoThueKhaiBao],
	[TongSoThueAnDinh],
	[TongSoThueChenhLech],
	[MaTienTe],
	[TyGiaQuyDoi],
	[SoNgayDuocAnHan],
	[NgayHetHieuLucTamNhapTaiXuat],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[LaiSuatPhatChamNop],
	[NgayPhatHanhChungTu]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Update]    Script Date: 11/11/2013 17:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThue_Update]
	@ID bigint,
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuanNoiMoToKhai nvarchar(210),
	@SoChungTu numeric(12, 0),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@TenNganHangBaoLanh nvarchar(210),
	@MaNganHangBaoLanh varchar(11),
	@KiHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@LoaiBaoLanh nvarchar(60),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoThueKhaiBao numeric(11, 0),
	@TongSoThueAnDinh numeric(11, 0),
	@TongSoThueChenhLech numeric(12, 0),
	@MaTienTe varchar(3),
	@TyGiaQuyDoi numeric(9, 0),
	@SoNgayDuocAnHan numeric(3, 0),
	@NgayHetHieuLucTamNhapTaiXuat datetime,
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@LaiSuatPhatChamNop nvarchar(765),
	@NgayPhatHanhChungTu datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]
SET
	[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
	[TenChiCucHaiQuanNoiMoToKhai] = @TenChiCucHaiQuanNoiMoToKhai,
	[SoChungTu] = @SoChungTu,
	[SoToKhai] = @SoToKhai,
	[NgayDangKyToKhai] = @NgayDangKyToKhai,
	[TenDonViXuatNhapKhau] = @TenDonViXuatNhapKhau,
	[MaDonViXuatNhapKhau] = @MaDonViXuatNhapKhau,
	[MaBuuChinh] = @MaBuuChinh,
	[DiaChiNguoiXuatNhapKhau] = @DiaChiNguoiXuatNhapKhau,
	[SoDienThoaiNguoiXuatNhapKhau] = @SoDienThoaiNguoiXuatNhapKhau,
	[TenNganHangBaoLanh] = @TenNganHangBaoLanh,
	[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
	[KiHieuChungTuBaoLanh] = @KiHieuChungTuBaoLanh,
	[SoChungTuBaoLanh] = @SoChungTuBaoLanh,
	[LoaiBaoLanh] = @LoaiBaoLanh,
	[TenNganHangTraThay] = @TenNganHangTraThay,
	[MaNganHangTraThay] = @MaNganHangTraThay,
	[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
	[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
	[TongSoThueKhaiBao] = @TongSoThueKhaiBao,
	[TongSoThueAnDinh] = @TongSoThueAnDinh,
	[TongSoThueChenhLech] = @TongSoThueChenhLech,
	[MaTienTe] = @MaTienTe,
	[TyGiaQuyDoi] = @TyGiaQuyDoi,
	[SoNgayDuocAnHan] = @SoNgayDuocAnHan,
	[NgayHetHieuLucTamNhapTaiXuat] = @NgayHetHieuLucTamNhapTaiXuat,
	[SoTaiKhoanKhoBac] = @SoTaiKhoanKhoBac,
	[TenKhoBac] = @TenKhoBac,
	[LaiSuatPhatChamNop] = @LaiSuatPhatChamNop,
	[NgayPhatHanhChungTu] = @NgayPhatHanhChungTu
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Delete]    Script Date: 11/11/2013 17:34:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteBy_AnDinhThue_ID]    Script Date: 11/11/2013 17:34:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteBy_AnDinhThue_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteBy_AnDinhThue_ID]
	@AnDinhThue_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]
WHERE
	[AnDinhThue_ID] = @AnDinhThue_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteDynamic]    Script Date: 11/11/2013 17:34:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Insert]    Script Date: 11/11/2013 17:34:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Insert]
	@AnDinhThue_ID bigint,
	@TenSacThue nvarchar(27),
	@TieuMuc numeric(4, 0),
	@SoThueKhaiBao numeric(11, 0),
	@SoThueAnDinh numeric(11, 0),
	@SoThueChenhLech numeric(12, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]
(
	[AnDinhThue_ID],
	[TenSacThue],
	[TieuMuc],
	[SoThueKhaiBao],
	[SoThueAnDinh],
	[SoThueChenhLech]
)
VALUES 
(
	@AnDinhThue_ID,
	@TenSacThue,
	@TieuMuc,
	@SoThueKhaiBao,
	@SoThueAnDinh,
	@SoThueChenhLech
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_InsertUpdate]    Script Date: 11/11/2013 17:34:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_InsertUpdate]
	@ID bigint,
	@AnDinhThue_ID bigint,
	@TenSacThue nvarchar(27),
	@TieuMuc numeric(4, 0),
	@SoThueKhaiBao numeric(11, 0),
	@SoThueAnDinh numeric(11, 0),
	@SoThueChenhLech numeric(12, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue] 
		SET
			[AnDinhThue_ID] = @AnDinhThue_ID,
			[TenSacThue] = @TenSacThue,
			[TieuMuc] = @TieuMuc,
			[SoThueKhaiBao] = @SoThueKhaiBao,
			[SoThueAnDinh] = @SoThueAnDinh,
			[SoThueChenhLech] = @SoThueChenhLech
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]
		(
			[AnDinhThue_ID],
			[TenSacThue],
			[TieuMuc],
			[SoThueKhaiBao],
			[SoThueAnDinh],
			[SoThueChenhLech]
		)
		VALUES 
		(
			@AnDinhThue_ID,
			@TenSacThue,
			@TieuMuc,
			@SoThueKhaiBao,
			@SoThueAnDinh,
			@SoThueChenhLech
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Load]    Script Date: 11/11/2013 17:34:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[AnDinhThue_ID],
	[TenSacThue],
	[TieuMuc],
	[SoThueKhaiBao],
	[SoThueAnDinh],
	[SoThueChenhLech]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectAll]    Script Date: 11/11/2013 17:34:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[AnDinhThue_ID],
	[TenSacThue],
	[TieuMuc],
	[SoThueKhaiBao],
	[SoThueAnDinh],
	[SoThueChenhLech]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectBy_AnDinhThue_ID]    Script Date: 11/11/2013 17:34:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectBy_AnDinhThue_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectBy_AnDinhThue_ID]
	@AnDinhThue_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[AnDinhThue_ID],
	[TenSacThue],
	[TieuMuc],
	[SoThueKhaiBao],
	[SoThueAnDinh],
	[SoThueChenhLech]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]
WHERE
	[AnDinhThue_ID] = @AnDinhThue_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectDynamic]    Script Date: 11/11/2013 17:34:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[AnDinhThue_ID],
	[TenSacThue],
	[TieuMuc],
	[SoThueKhaiBao],
	[SoThueAnDinh],
	[SoThueChenhLech]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Update]    Script Date: 11/11/2013 17:34:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Update]
	@ID bigint,
	@AnDinhThue_ID bigint,
	@TenSacThue nvarchar(27),
	@TieuMuc numeric(4, 0),
	@SoThueKhaiBao numeric(11, 0),
	@SoThueAnDinh numeric(11, 0),
	@SoThueChenhLech numeric(12, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]
SET
	[AnDinhThue_ID] = @AnDinhThue_ID,
	[TenSacThue] = @TenSacThue,
	[TieuMuc] = @TieuMuc,
	[SoThueKhaiBao] = @SoThueKhaiBao,
	[SoThueAnDinh] = @SoThueAnDinh,
	[SoThueChenhLech] = @SoThueChenhLech
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_Delete]    Script Date: 11/11/2013 17:34:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_DeleteDynamic]    Script Date: 11/11/2013 17:34:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_Insert]    Script Date: 11/11/2013 17:34:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_Insert]
	@SoToKhai numeric(12, 0),
	@PhanLoaiBaoCaoSuaDoi varchar(1),
	@SoToKhaiDauTien numeric(12, 0),
	@SoNhanhToKhai numeric(2, 0),
	@TongSoTKChiaNho numeric(2, 0),
	@SoToKhaiTNTX numeric(12, 0),
	@MaLoaiHinh varchar(3),
	@MaPhanLoaiHH varchar(1),
	@MaPhuongThucVT varchar(1),
	@PhanLoaiToChuc varchar(1),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHS varchar(2),
	@ThoiHanTaiNhapTaiXuat datetime,
	@NgayDangKy datetime,
	@MaDonVi varchar(13),
	@TenDonVi nvarchar(100),
	@MaBuuChinhDonVi varchar(7),
	@DiaChiDonVi nvarchar(100),
	@SoDienThoaiDonVi varchar(20),
	@MaUyThac varchar(13),
	@TenUyThac nvarchar(100),
	@MaDoiTac varchar(13),
	@TenDoiTac nvarchar(100),
	@MaBuuChinhDoiTac varchar(7),
	@DiaChiDoiTac1 nvarchar(35),
	@DiaChiDoiTac2 nvarchar(35),
	@DiaChiDoiTac3 nvarchar(35),
	@DiaChiDoiTac4 nvarchar(35),
	@MaNuocDoiTac varchar(2),
	@NguoiUyThacXK nvarchar(70),
	@MaDaiLyHQ varchar(5),
	@SoLuong numeric(8, 0),
	@MaDVTSoLuong varchar(3),
	@TrongLuong numeric(14, 4),
	@MaDVTTrongLuong varchar(3),
	@MaDDLuuKho varchar(7),
	@SoHieuKyHieu varchar(140),
	@MaPTVC varchar(9),
	@TenPTVC nvarchar(35),
	@NgayHangDen datetime,
	@MaDiaDiemDoHang varchar(6),
	@TenDiaDiemDohang nvarchar(35),
	@MaDiaDiemXepHang varchar(6),
	@TenDiaDiemXepHang nvarchar(35),
	@SoLuongCont numeric(3, 0),
	@MaKetQuaKiemTra varchar(1),
	@MaVanbanPhapQuy1 varchar(2),
	@MaVanbanPhapQuy2 varchar(2),
	@MaVanbanPhapQuy3 varchar(2),
	@MaVanbanPhapQuy4 varchar(2),
	@MaVanbanPhapQuy5 varchar(2),
	@PhanLoaiHD varchar(1),
	@SoTiepNhanHD numeric(12, 0),
	@SoHoaDon varchar(35),
	@NgayPhatHanhHD datetime,
	@PhuongThucTT varchar(7),
	@PhanLoaiGiaHD varchar(1),
	@MaDieuKienGiaHD varchar(3),
	@MaTTHoaDon varchar(3),
	@TongTriGiaHD numeric(24, 4),
	@MaPhanLoaiTriGia varchar(1),
	@SoTiepNhanTKTriGia numeric(9, 0),
	@MaTTHieuChinhTriGia varchar(3),
	@GiaHieuChinhTriGia numeric(24, 4),
	@MaPhanLoaiPhiVC varchar(1),
	@MaTTPhiVC varchar(3),
	@PhiVanChuyen numeric(22, 4),
	@MaPhanLoaiPhiBH varchar(1),
	@MaTTPhiBH varchar(3),
	@PhiBaoHiem numeric(22, 4),
	@SoDangKyBH varchar(6),
	@ChiTietKhaiTriGia nvarchar(280),
	@TriGiaTinhThue numeric(24, 4),
	@PhanLoaiKhongQDVND varchar(1),
	@MaTTTriGiaTinhThue varchar(3),
	@TongHeSoPhanBoTG numeric(24, 4),
	@MaLyDoDeNghiBP varchar(1),
	@NguoiNopThue varchar(1),
	@MaNHTraThueThay varchar(11),
	@NamPhatHanhHM numeric(4, 0),
	@KyHieuCTHanMuc varchar(10),
	@SoCTHanMuc varchar(10),
	@MaXDThoiHanNopThue varchar(1),
	@MaNHBaoLanh varchar(11),
	@NamPhatHanhBL numeric(4, 0),
	@KyHieuCTBaoLanh varchar(10),
	@SoCTBaoLanh varchar(10),
	@NgayNhapKhoDau datetime,
	@NgayKhoiHanhVC datetime,
	@DiaDiemDichVC varchar(7),
	@NgayDen datetime,
	@GhiChu nvarchar(100),
	@SoQuanLyNoiBoDN varchar(20),
	@MaPhanLoaiKiemTra varchar(3),
	@MaSoThueDaiDien varchar(4),
	@TenCoQuanHaiQuan varchar(10),
	@NgayThayDoiDangKy datetime,
	@BieuThiTruongHopHetHan varchar(1),
	@TenDaiLyHaiQuan varchar(50),
	@MaNhanVienHaiQuan varchar(5),
	@TenDDLuuKho varchar(20),
	@MaPhanLoaiTongGiaCoBan varchar(1),
	@PhanLoaiCongThucChuan varchar(1),
	@MaPhanLoaiDieuChinhTriGia varchar(2),
	@PhuongPhapDieuChinhTriGia varchar(22),
	@TongTienThuePhaiNop numeric(15, 4),
	@SoTienBaoLanh numeric(15, 4),
	@TenTruongDonViHaiQuan nvarchar(36),
	@NgayCapPhep datetime,
	@PhanLoaiThamTraSauThongQuan varchar(2),
	@NgayPheDuyetBP datetime,
	@NgayHoanThanhKiemTraBP datetime,
	@SoNgayDoiCapPhepNhapKhau numeric(2, 0),
	@TieuDe varchar(27),
	@MaSacThueAnHan_VAT varchar(1),
	@TenSacThueAnHan_VAT nvarchar(9),
	@HanNopThueSauKhiAnHan_VAT datetime,
	@PhanLoaiNopThue varchar(1),
	@TongSoTienThueXuatKhau numeric(15, 4),
	@MaTTTongTienThueXuatKhau varchar(3),
	@TongSoTienLePhi numeric(15, 4),
	@MaTTCuaSoTienBaoLanh varchar(3),
	@SoQuanLyNguoiSuDung varchar(5),
	@NgayHoanThanhKiemTra datetime,
	@TongSoTrangCuaToKhai numeric(2, 0),
	@TongSoDongHangCuaToKhai numeric(2, 0),
	@NgayKhaiBaoNopThue datetime,
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich]
(
	[SoToKhai],
	[PhanLoaiBaoCaoSuaDoi],
	[SoToKhaiDauTien],
	[SoNhanhToKhai],
	[TongSoTKChiaNho],
	[SoToKhaiTNTX],
	[MaLoaiHinh],
	[MaPhanLoaiHH],
	[MaPhuongThucVT],
	[PhanLoaiToChuc],
	[CoQuanHaiQuan],
	[NhomXuLyHS],
	[ThoiHanTaiNhapTaiXuat],
	[NgayDangKy],
	[MaDonVi],
	[TenDonVi],
	[MaBuuChinhDonVi],
	[DiaChiDonVi],
	[SoDienThoaiDonVi],
	[MaUyThac],
	[TenUyThac],
	[MaDoiTac],
	[TenDoiTac],
	[MaBuuChinhDoiTac],
	[DiaChiDoiTac1],
	[DiaChiDoiTac2],
	[DiaChiDoiTac3],
	[DiaChiDoiTac4],
	[MaNuocDoiTac],
	[NguoiUyThacXK],
	[MaDaiLyHQ],
	[SoLuong],
	[MaDVTSoLuong],
	[TrongLuong],
	[MaDVTTrongLuong],
	[MaDDLuuKho],
	[SoHieuKyHieu],
	[MaPTVC],
	[TenPTVC],
	[NgayHangDen],
	[MaDiaDiemDoHang],
	[TenDiaDiemDohang],
	[MaDiaDiemXepHang],
	[TenDiaDiemXepHang],
	[SoLuongCont],
	[MaKetQuaKiemTra],
	[MaVanbanPhapQuy1],
	[MaVanbanPhapQuy2],
	[MaVanbanPhapQuy3],
	[MaVanbanPhapQuy4],
	[MaVanbanPhapQuy5],
	[PhanLoaiHD],
	[SoTiepNhanHD],
	[SoHoaDon],
	[NgayPhatHanhHD],
	[PhuongThucTT],
	[PhanLoaiGiaHD],
	[MaDieuKienGiaHD],
	[MaTTHoaDon],
	[TongTriGiaHD],
	[MaPhanLoaiTriGia],
	[SoTiepNhanTKTriGia],
	[MaTTHieuChinhTriGia],
	[GiaHieuChinhTriGia],
	[MaPhanLoaiPhiVC],
	[MaTTPhiVC],
	[PhiVanChuyen],
	[MaPhanLoaiPhiBH],
	[MaTTPhiBH],
	[PhiBaoHiem],
	[SoDangKyBH],
	[ChiTietKhaiTriGia],
	[TriGiaTinhThue],
	[PhanLoaiKhongQDVND],
	[MaTTTriGiaTinhThue],
	[TongHeSoPhanBoTG],
	[MaLyDoDeNghiBP],
	[NguoiNopThue],
	[MaNHTraThueThay],
	[NamPhatHanhHM],
	[KyHieuCTHanMuc],
	[SoCTHanMuc],
	[MaXDThoiHanNopThue],
	[MaNHBaoLanh],
	[NamPhatHanhBL],
	[KyHieuCTBaoLanh],
	[SoCTBaoLanh],
	[NgayNhapKhoDau],
	[NgayKhoiHanhVC],
	[DiaDiemDichVC],
	[NgayDen],
	[GhiChu],
	[SoQuanLyNoiBoDN],
	[MaPhanLoaiKiemTra],
	[MaSoThueDaiDien],
	[TenCoQuanHaiQuan],
	[NgayThayDoiDangKy],
	[BieuThiTruongHopHetHan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[TenDDLuuKho],
	[MaPhanLoaiTongGiaCoBan],
	[PhanLoaiCongThucChuan],
	[MaPhanLoaiDieuChinhTriGia],
	[PhuongPhapDieuChinhTriGia],
	[TongTienThuePhaiNop],
	[SoTienBaoLanh],
	[TenTruongDonViHaiQuan],
	[NgayCapPhep],
	[PhanLoaiThamTraSauThongQuan],
	[NgayPheDuyetBP],
	[NgayHoanThanhKiemTraBP],
	[SoNgayDoiCapPhepNhapKhau],
	[TieuDe],
	[MaSacThueAnHan_VAT],
	[TenSacThueAnHan_VAT],
	[HanNopThueSauKhiAnHan_VAT],
	[PhanLoaiNopThue],
	[TongSoTienThueXuatKhau],
	[MaTTTongTienThueXuatKhau],
	[TongSoTienLePhi],
	[MaTTCuaSoTienBaoLanh],
	[SoQuanLyNguoiSuDung],
	[NgayHoanThanhKiemTra],
	[TongSoTrangCuaToKhai],
	[TongSoDongHangCuaToKhai],
	[NgayKhaiBaoNopThue],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@SoToKhai,
	@PhanLoaiBaoCaoSuaDoi,
	@SoToKhaiDauTien,
	@SoNhanhToKhai,
	@TongSoTKChiaNho,
	@SoToKhaiTNTX,
	@MaLoaiHinh,
	@MaPhanLoaiHH,
	@MaPhuongThucVT,
	@PhanLoaiToChuc,
	@CoQuanHaiQuan,
	@NhomXuLyHS,
	@ThoiHanTaiNhapTaiXuat,
	@NgayDangKy,
	@MaDonVi,
	@TenDonVi,
	@MaBuuChinhDonVi,
	@DiaChiDonVi,
	@SoDienThoaiDonVi,
	@MaUyThac,
	@TenUyThac,
	@MaDoiTac,
	@TenDoiTac,
	@MaBuuChinhDoiTac,
	@DiaChiDoiTac1,
	@DiaChiDoiTac2,
	@DiaChiDoiTac3,
	@DiaChiDoiTac4,
	@MaNuocDoiTac,
	@NguoiUyThacXK,
	@MaDaiLyHQ,
	@SoLuong,
	@MaDVTSoLuong,
	@TrongLuong,
	@MaDVTTrongLuong,
	@MaDDLuuKho,
	@SoHieuKyHieu,
	@MaPTVC,
	@TenPTVC,
	@NgayHangDen,
	@MaDiaDiemDoHang,
	@TenDiaDiemDohang,
	@MaDiaDiemXepHang,
	@TenDiaDiemXepHang,
	@SoLuongCont,
	@MaKetQuaKiemTra,
	@MaVanbanPhapQuy1,
	@MaVanbanPhapQuy2,
	@MaVanbanPhapQuy3,
	@MaVanbanPhapQuy4,
	@MaVanbanPhapQuy5,
	@PhanLoaiHD,
	@SoTiepNhanHD,
	@SoHoaDon,
	@NgayPhatHanhHD,
	@PhuongThucTT,
	@PhanLoaiGiaHD,
	@MaDieuKienGiaHD,
	@MaTTHoaDon,
	@TongTriGiaHD,
	@MaPhanLoaiTriGia,
	@SoTiepNhanTKTriGia,
	@MaTTHieuChinhTriGia,
	@GiaHieuChinhTriGia,
	@MaPhanLoaiPhiVC,
	@MaTTPhiVC,
	@PhiVanChuyen,
	@MaPhanLoaiPhiBH,
	@MaTTPhiBH,
	@PhiBaoHiem,
	@SoDangKyBH,
	@ChiTietKhaiTriGia,
	@TriGiaTinhThue,
	@PhanLoaiKhongQDVND,
	@MaTTTriGiaTinhThue,
	@TongHeSoPhanBoTG,
	@MaLyDoDeNghiBP,
	@NguoiNopThue,
	@MaNHTraThueThay,
	@NamPhatHanhHM,
	@KyHieuCTHanMuc,
	@SoCTHanMuc,
	@MaXDThoiHanNopThue,
	@MaNHBaoLanh,
	@NamPhatHanhBL,
	@KyHieuCTBaoLanh,
	@SoCTBaoLanh,
	@NgayNhapKhoDau,
	@NgayKhoiHanhVC,
	@DiaDiemDichVC,
	@NgayDen,
	@GhiChu,
	@SoQuanLyNoiBoDN,
	@MaPhanLoaiKiemTra,
	@MaSoThueDaiDien,
	@TenCoQuanHaiQuan,
	@NgayThayDoiDangKy,
	@BieuThiTruongHopHetHan,
	@TenDaiLyHaiQuan,
	@MaNhanVienHaiQuan,
	@TenDDLuuKho,
	@MaPhanLoaiTongGiaCoBan,
	@PhanLoaiCongThucChuan,
	@MaPhanLoaiDieuChinhTriGia,
	@PhuongPhapDieuChinhTriGia,
	@TongTienThuePhaiNop,
	@SoTienBaoLanh,
	@TenTruongDonViHaiQuan,
	@NgayCapPhep,
	@PhanLoaiThamTraSauThongQuan,
	@NgayPheDuyetBP,
	@NgayHoanThanhKiemTraBP,
	@SoNgayDoiCapPhepNhapKhau,
	@TieuDe,
	@MaSacThueAnHan_VAT,
	@TenSacThueAnHan_VAT,
	@HanNopThueSauKhiAnHan_VAT,
	@PhanLoaiNopThue,
	@TongSoTienThueXuatKhau,
	@MaTTTongTienThueXuatKhau,
	@TongSoTienLePhi,
	@MaTTCuaSoTienBaoLanh,
	@SoQuanLyNguoiSuDung,
	@NgayHoanThanhKiemTra,
	@TongSoTrangCuaToKhai,
	@TongSoDongHangCuaToKhai,
	@NgayKhaiBaoNopThue,
	@TrangThaiXuLy,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_InsertUpdate]    Script Date: 11/11/2013 17:34:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_InsertUpdate]
	@ID bigint,
	@SoToKhai numeric(12, 0),
	@PhanLoaiBaoCaoSuaDoi varchar(1),
	@SoToKhaiDauTien numeric(12, 0),
	@SoNhanhToKhai numeric(2, 0),
	@TongSoTKChiaNho numeric(2, 0),
	@SoToKhaiTNTX numeric(12, 0),
	@MaLoaiHinh varchar(3),
	@MaPhanLoaiHH varchar(1),
	@MaPhuongThucVT varchar(1),
	@PhanLoaiToChuc varchar(1),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHS varchar(2),
	@ThoiHanTaiNhapTaiXuat datetime,
	@NgayDangKy datetime,
	@MaDonVi varchar(13),
	@TenDonVi nvarchar(100),
	@MaBuuChinhDonVi varchar(7),
	@DiaChiDonVi nvarchar(100),
	@SoDienThoaiDonVi varchar(20),
	@MaUyThac varchar(13),
	@TenUyThac nvarchar(100),
	@MaDoiTac varchar(13),
	@TenDoiTac nvarchar(100),
	@MaBuuChinhDoiTac varchar(7),
	@DiaChiDoiTac1 nvarchar(35),
	@DiaChiDoiTac2 nvarchar(35),
	@DiaChiDoiTac3 nvarchar(35),
	@DiaChiDoiTac4 nvarchar(35),
	@MaNuocDoiTac varchar(2),
	@NguoiUyThacXK nvarchar(70),
	@MaDaiLyHQ varchar(5),
	@SoLuong numeric(8, 0),
	@MaDVTSoLuong varchar(3),
	@TrongLuong numeric(14, 4),
	@MaDVTTrongLuong varchar(3),
	@MaDDLuuKho varchar(7),
	@SoHieuKyHieu varchar(140),
	@MaPTVC varchar(9),
	@TenPTVC nvarchar(35),
	@NgayHangDen datetime,
	@MaDiaDiemDoHang varchar(6),
	@TenDiaDiemDohang nvarchar(35),
	@MaDiaDiemXepHang varchar(6),
	@TenDiaDiemXepHang nvarchar(35),
	@SoLuongCont numeric(3, 0),
	@MaKetQuaKiemTra varchar(1),
	@MaVanbanPhapQuy1 varchar(2),
	@MaVanbanPhapQuy2 varchar(2),
	@MaVanbanPhapQuy3 varchar(2),
	@MaVanbanPhapQuy4 varchar(2),
	@MaVanbanPhapQuy5 varchar(2),
	@PhanLoaiHD varchar(1),
	@SoTiepNhanHD numeric(12, 0),
	@SoHoaDon varchar(35),
	@NgayPhatHanhHD datetime,
	@PhuongThucTT varchar(7),
	@PhanLoaiGiaHD varchar(1),
	@MaDieuKienGiaHD varchar(3),
	@MaTTHoaDon varchar(3),
	@TongTriGiaHD numeric(24, 4),
	@MaPhanLoaiTriGia varchar(1),
	@SoTiepNhanTKTriGia numeric(9, 0),
	@MaTTHieuChinhTriGia varchar(3),
	@GiaHieuChinhTriGia numeric(24, 4),
	@MaPhanLoaiPhiVC varchar(1),
	@MaTTPhiVC varchar(3),
	@PhiVanChuyen numeric(22, 4),
	@MaPhanLoaiPhiBH varchar(1),
	@MaTTPhiBH varchar(3),
	@PhiBaoHiem numeric(22, 4),
	@SoDangKyBH varchar(6),
	@ChiTietKhaiTriGia nvarchar(280),
	@TriGiaTinhThue numeric(24, 4),
	@PhanLoaiKhongQDVND varchar(1),
	@MaTTTriGiaTinhThue varchar(3),
	@TongHeSoPhanBoTG numeric(24, 4),
	@MaLyDoDeNghiBP varchar(1),
	@NguoiNopThue varchar(1),
	@MaNHTraThueThay varchar(11),
	@NamPhatHanhHM numeric(4, 0),
	@KyHieuCTHanMuc varchar(10),
	@SoCTHanMuc varchar(10),
	@MaXDThoiHanNopThue varchar(1),
	@MaNHBaoLanh varchar(11),
	@NamPhatHanhBL numeric(4, 0),
	@KyHieuCTBaoLanh varchar(10),
	@SoCTBaoLanh varchar(10),
	@NgayNhapKhoDau datetime,
	@NgayKhoiHanhVC datetime,
	@DiaDiemDichVC varchar(7),
	@NgayDen datetime,
	@GhiChu nvarchar(100),
	@SoQuanLyNoiBoDN varchar(20),
	@MaPhanLoaiKiemTra varchar(3),
	@MaSoThueDaiDien varchar(4),
	@TenCoQuanHaiQuan varchar(10),
	@NgayThayDoiDangKy datetime,
	@BieuThiTruongHopHetHan varchar(1),
	@TenDaiLyHaiQuan varchar(50),
	@MaNhanVienHaiQuan varchar(5),
	@TenDDLuuKho varchar(20),
	@MaPhanLoaiTongGiaCoBan varchar(1),
	@PhanLoaiCongThucChuan varchar(1),
	@MaPhanLoaiDieuChinhTriGia varchar(2),
	@PhuongPhapDieuChinhTriGia varchar(22),
	@TongTienThuePhaiNop numeric(15, 4),
	@SoTienBaoLanh numeric(15, 4),
	@TenTruongDonViHaiQuan nvarchar(36),
	@NgayCapPhep datetime,
	@PhanLoaiThamTraSauThongQuan varchar(2),
	@NgayPheDuyetBP datetime,
	@NgayHoanThanhKiemTraBP datetime,
	@SoNgayDoiCapPhepNhapKhau numeric(2, 0),
	@TieuDe varchar(27),
	@MaSacThueAnHan_VAT varchar(1),
	@TenSacThueAnHan_VAT nvarchar(9),
	@HanNopThueSauKhiAnHan_VAT datetime,
	@PhanLoaiNopThue varchar(1),
	@TongSoTienThueXuatKhau numeric(15, 4),
	@MaTTTongTienThueXuatKhau varchar(3),
	@TongSoTienLePhi numeric(15, 4),
	@MaTTCuaSoTienBaoLanh varchar(3),
	@SoQuanLyNguoiSuDung varchar(5),
	@NgayHoanThanhKiemTra datetime,
	@TongSoTrangCuaToKhai numeric(2, 0),
	@TongSoDongHangCuaToKhai numeric(2, 0),
	@NgayKhaiBaoNopThue datetime,
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich] 
		SET
			[SoToKhai] = @SoToKhai,
			[PhanLoaiBaoCaoSuaDoi] = @PhanLoaiBaoCaoSuaDoi,
			[SoToKhaiDauTien] = @SoToKhaiDauTien,
			[SoNhanhToKhai] = @SoNhanhToKhai,
			[TongSoTKChiaNho] = @TongSoTKChiaNho,
			[SoToKhaiTNTX] = @SoToKhaiTNTX,
			[MaLoaiHinh] = @MaLoaiHinh,
			[MaPhanLoaiHH] = @MaPhanLoaiHH,
			[MaPhuongThucVT] = @MaPhuongThucVT,
			[PhanLoaiToChuc] = @PhanLoaiToChuc,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[NhomXuLyHS] = @NhomXuLyHS,
			[ThoiHanTaiNhapTaiXuat] = @ThoiHanTaiNhapTaiXuat,
			[NgayDangKy] = @NgayDangKy,
			[MaDonVi] = @MaDonVi,
			[TenDonVi] = @TenDonVi,
			[MaBuuChinhDonVi] = @MaBuuChinhDonVi,
			[DiaChiDonVi] = @DiaChiDonVi,
			[SoDienThoaiDonVi] = @SoDienThoaiDonVi,
			[MaUyThac] = @MaUyThac,
			[TenUyThac] = @TenUyThac,
			[MaDoiTac] = @MaDoiTac,
			[TenDoiTac] = @TenDoiTac,
			[MaBuuChinhDoiTac] = @MaBuuChinhDoiTac,
			[DiaChiDoiTac1] = @DiaChiDoiTac1,
			[DiaChiDoiTac2] = @DiaChiDoiTac2,
			[DiaChiDoiTac3] = @DiaChiDoiTac3,
			[DiaChiDoiTac4] = @DiaChiDoiTac4,
			[MaNuocDoiTac] = @MaNuocDoiTac,
			[NguoiUyThacXK] = @NguoiUyThacXK,
			[MaDaiLyHQ] = @MaDaiLyHQ,
			[SoLuong] = @SoLuong,
			[MaDVTSoLuong] = @MaDVTSoLuong,
			[TrongLuong] = @TrongLuong,
			[MaDVTTrongLuong] = @MaDVTTrongLuong,
			[MaDDLuuKho] = @MaDDLuuKho,
			[SoHieuKyHieu] = @SoHieuKyHieu,
			[MaPTVC] = @MaPTVC,
			[TenPTVC] = @TenPTVC,
			[NgayHangDen] = @NgayHangDen,
			[MaDiaDiemDoHang] = @MaDiaDiemDoHang,
			[TenDiaDiemDohang] = @TenDiaDiemDohang,
			[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
			[TenDiaDiemXepHang] = @TenDiaDiemXepHang,
			[SoLuongCont] = @SoLuongCont,
			[MaKetQuaKiemTra] = @MaKetQuaKiemTra,
			[MaVanbanPhapQuy1] = @MaVanbanPhapQuy1,
			[MaVanbanPhapQuy2] = @MaVanbanPhapQuy2,
			[MaVanbanPhapQuy3] = @MaVanbanPhapQuy3,
			[MaVanbanPhapQuy4] = @MaVanbanPhapQuy4,
			[MaVanbanPhapQuy5] = @MaVanbanPhapQuy5,
			[PhanLoaiHD] = @PhanLoaiHD,
			[SoTiepNhanHD] = @SoTiepNhanHD,
			[SoHoaDon] = @SoHoaDon,
			[NgayPhatHanhHD] = @NgayPhatHanhHD,
			[PhuongThucTT] = @PhuongThucTT,
			[PhanLoaiGiaHD] = @PhanLoaiGiaHD,
			[MaDieuKienGiaHD] = @MaDieuKienGiaHD,
			[MaTTHoaDon] = @MaTTHoaDon,
			[TongTriGiaHD] = @TongTriGiaHD,
			[MaPhanLoaiTriGia] = @MaPhanLoaiTriGia,
			[SoTiepNhanTKTriGia] = @SoTiepNhanTKTriGia,
			[MaTTHieuChinhTriGia] = @MaTTHieuChinhTriGia,
			[GiaHieuChinhTriGia] = @GiaHieuChinhTriGia,
			[MaPhanLoaiPhiVC] = @MaPhanLoaiPhiVC,
			[MaTTPhiVC] = @MaTTPhiVC,
			[PhiVanChuyen] = @PhiVanChuyen,
			[MaPhanLoaiPhiBH] = @MaPhanLoaiPhiBH,
			[MaTTPhiBH] = @MaTTPhiBH,
			[PhiBaoHiem] = @PhiBaoHiem,
			[SoDangKyBH] = @SoDangKyBH,
			[ChiTietKhaiTriGia] = @ChiTietKhaiTriGia,
			[TriGiaTinhThue] = @TriGiaTinhThue,
			[PhanLoaiKhongQDVND] = @PhanLoaiKhongQDVND,
			[MaTTTriGiaTinhThue] = @MaTTTriGiaTinhThue,
			[TongHeSoPhanBoTG] = @TongHeSoPhanBoTG,
			[MaLyDoDeNghiBP] = @MaLyDoDeNghiBP,
			[NguoiNopThue] = @NguoiNopThue,
			[MaNHTraThueThay] = @MaNHTraThueThay,
			[NamPhatHanhHM] = @NamPhatHanhHM,
			[KyHieuCTHanMuc] = @KyHieuCTHanMuc,
			[SoCTHanMuc] = @SoCTHanMuc,
			[MaXDThoiHanNopThue] = @MaXDThoiHanNopThue,
			[MaNHBaoLanh] = @MaNHBaoLanh,
			[NamPhatHanhBL] = @NamPhatHanhBL,
			[KyHieuCTBaoLanh] = @KyHieuCTBaoLanh,
			[SoCTBaoLanh] = @SoCTBaoLanh,
			[NgayNhapKhoDau] = @NgayNhapKhoDau,
			[NgayKhoiHanhVC] = @NgayKhoiHanhVC,
			[DiaDiemDichVC] = @DiaDiemDichVC,
			[NgayDen] = @NgayDen,
			[GhiChu] = @GhiChu,
			[SoQuanLyNoiBoDN] = @SoQuanLyNoiBoDN,
			[MaPhanLoaiKiemTra] = @MaPhanLoaiKiemTra,
			[MaSoThueDaiDien] = @MaSoThueDaiDien,
			[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
			[NgayThayDoiDangKy] = @NgayThayDoiDangKy,
			[BieuThiTruongHopHetHan] = @BieuThiTruongHopHetHan,
			[TenDaiLyHaiQuan] = @TenDaiLyHaiQuan,
			[MaNhanVienHaiQuan] = @MaNhanVienHaiQuan,
			[TenDDLuuKho] = @TenDDLuuKho,
			[MaPhanLoaiTongGiaCoBan] = @MaPhanLoaiTongGiaCoBan,
			[PhanLoaiCongThucChuan] = @PhanLoaiCongThucChuan,
			[MaPhanLoaiDieuChinhTriGia] = @MaPhanLoaiDieuChinhTriGia,
			[PhuongPhapDieuChinhTriGia] = @PhuongPhapDieuChinhTriGia,
			[TongTienThuePhaiNop] = @TongTienThuePhaiNop,
			[SoTienBaoLanh] = @SoTienBaoLanh,
			[TenTruongDonViHaiQuan] = @TenTruongDonViHaiQuan,
			[NgayCapPhep] = @NgayCapPhep,
			[PhanLoaiThamTraSauThongQuan] = @PhanLoaiThamTraSauThongQuan,
			[NgayPheDuyetBP] = @NgayPheDuyetBP,
			[NgayHoanThanhKiemTraBP] = @NgayHoanThanhKiemTraBP,
			[SoNgayDoiCapPhepNhapKhau] = @SoNgayDoiCapPhepNhapKhau,
			[TieuDe] = @TieuDe,
			[MaSacThueAnHan_VAT] = @MaSacThueAnHan_VAT,
			[TenSacThueAnHan_VAT] = @TenSacThueAnHan_VAT,
			[HanNopThueSauKhiAnHan_VAT] = @HanNopThueSauKhiAnHan_VAT,
			[PhanLoaiNopThue] = @PhanLoaiNopThue,
			[TongSoTienThueXuatKhau] = @TongSoTienThueXuatKhau,
			[MaTTTongTienThueXuatKhau] = @MaTTTongTienThueXuatKhau,
			[TongSoTienLePhi] = @TongSoTienLePhi,
			[MaTTCuaSoTienBaoLanh] = @MaTTCuaSoTienBaoLanh,
			[SoQuanLyNguoiSuDung] = @SoQuanLyNguoiSuDung,
			[NgayHoanThanhKiemTra] = @NgayHoanThanhKiemTra,
			[TongSoTrangCuaToKhai] = @TongSoTrangCuaToKhai,
			[TongSoDongHangCuaToKhai] = @TongSoDongHangCuaToKhai,
			[NgayKhaiBaoNopThue] = @NgayKhaiBaoNopThue,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich]
		(
			[SoToKhai],
			[PhanLoaiBaoCaoSuaDoi],
			[SoToKhaiDauTien],
			[SoNhanhToKhai],
			[TongSoTKChiaNho],
			[SoToKhaiTNTX],
			[MaLoaiHinh],
			[MaPhanLoaiHH],
			[MaPhuongThucVT],
			[PhanLoaiToChuc],
			[CoQuanHaiQuan],
			[NhomXuLyHS],
			[ThoiHanTaiNhapTaiXuat],
			[NgayDangKy],
			[MaDonVi],
			[TenDonVi],
			[MaBuuChinhDonVi],
			[DiaChiDonVi],
			[SoDienThoaiDonVi],
			[MaUyThac],
			[TenUyThac],
			[MaDoiTac],
			[TenDoiTac],
			[MaBuuChinhDoiTac],
			[DiaChiDoiTac1],
			[DiaChiDoiTac2],
			[DiaChiDoiTac3],
			[DiaChiDoiTac4],
			[MaNuocDoiTac],
			[NguoiUyThacXK],
			[MaDaiLyHQ],
			[SoLuong],
			[MaDVTSoLuong],
			[TrongLuong],
			[MaDVTTrongLuong],
			[MaDDLuuKho],
			[SoHieuKyHieu],
			[MaPTVC],
			[TenPTVC],
			[NgayHangDen],
			[MaDiaDiemDoHang],
			[TenDiaDiemDohang],
			[MaDiaDiemXepHang],
			[TenDiaDiemXepHang],
			[SoLuongCont],
			[MaKetQuaKiemTra],
			[MaVanbanPhapQuy1],
			[MaVanbanPhapQuy2],
			[MaVanbanPhapQuy3],
			[MaVanbanPhapQuy4],
			[MaVanbanPhapQuy5],
			[PhanLoaiHD],
			[SoTiepNhanHD],
			[SoHoaDon],
			[NgayPhatHanhHD],
			[PhuongThucTT],
			[PhanLoaiGiaHD],
			[MaDieuKienGiaHD],
			[MaTTHoaDon],
			[TongTriGiaHD],
			[MaPhanLoaiTriGia],
			[SoTiepNhanTKTriGia],
			[MaTTHieuChinhTriGia],
			[GiaHieuChinhTriGia],
			[MaPhanLoaiPhiVC],
			[MaTTPhiVC],
			[PhiVanChuyen],
			[MaPhanLoaiPhiBH],
			[MaTTPhiBH],
			[PhiBaoHiem],
			[SoDangKyBH],
			[ChiTietKhaiTriGia],
			[TriGiaTinhThue],
			[PhanLoaiKhongQDVND],
			[MaTTTriGiaTinhThue],
			[TongHeSoPhanBoTG],
			[MaLyDoDeNghiBP],
			[NguoiNopThue],
			[MaNHTraThueThay],
			[NamPhatHanhHM],
			[KyHieuCTHanMuc],
			[SoCTHanMuc],
			[MaXDThoiHanNopThue],
			[MaNHBaoLanh],
			[NamPhatHanhBL],
			[KyHieuCTBaoLanh],
			[SoCTBaoLanh],
			[NgayNhapKhoDau],
			[NgayKhoiHanhVC],
			[DiaDiemDichVC],
			[NgayDen],
			[GhiChu],
			[SoQuanLyNoiBoDN],
			[MaPhanLoaiKiemTra],
			[MaSoThueDaiDien],
			[TenCoQuanHaiQuan],
			[NgayThayDoiDangKy],
			[BieuThiTruongHopHetHan],
			[TenDaiLyHaiQuan],
			[MaNhanVienHaiQuan],
			[TenDDLuuKho],
			[MaPhanLoaiTongGiaCoBan],
			[PhanLoaiCongThucChuan],
			[MaPhanLoaiDieuChinhTriGia],
			[PhuongPhapDieuChinhTriGia],
			[TongTienThuePhaiNop],
			[SoTienBaoLanh],
			[TenTruongDonViHaiQuan],
			[NgayCapPhep],
			[PhanLoaiThamTraSauThongQuan],
			[NgayPheDuyetBP],
			[NgayHoanThanhKiemTraBP],
			[SoNgayDoiCapPhepNhapKhau],
			[TieuDe],
			[MaSacThueAnHan_VAT],
			[TenSacThueAnHan_VAT],
			[HanNopThueSauKhiAnHan_VAT],
			[PhanLoaiNopThue],
			[TongSoTienThueXuatKhau],
			[MaTTTongTienThueXuatKhau],
			[TongSoTienLePhi],
			[MaTTCuaSoTienBaoLanh],
			[SoQuanLyNguoiSuDung],
			[NgayHoanThanhKiemTra],
			[TongSoTrangCuaToKhai],
			[TongSoDongHangCuaToKhai],
			[NgayKhaiBaoNopThue],
			[TrangThaiXuLy],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@SoToKhai,
			@PhanLoaiBaoCaoSuaDoi,
			@SoToKhaiDauTien,
			@SoNhanhToKhai,
			@TongSoTKChiaNho,
			@SoToKhaiTNTX,
			@MaLoaiHinh,
			@MaPhanLoaiHH,
			@MaPhuongThucVT,
			@PhanLoaiToChuc,
			@CoQuanHaiQuan,
			@NhomXuLyHS,
			@ThoiHanTaiNhapTaiXuat,
			@NgayDangKy,
			@MaDonVi,
			@TenDonVi,
			@MaBuuChinhDonVi,
			@DiaChiDonVi,
			@SoDienThoaiDonVi,
			@MaUyThac,
			@TenUyThac,
			@MaDoiTac,
			@TenDoiTac,
			@MaBuuChinhDoiTac,
			@DiaChiDoiTac1,
			@DiaChiDoiTac2,
			@DiaChiDoiTac3,
			@DiaChiDoiTac4,
			@MaNuocDoiTac,
			@NguoiUyThacXK,
			@MaDaiLyHQ,
			@SoLuong,
			@MaDVTSoLuong,
			@TrongLuong,
			@MaDVTTrongLuong,
			@MaDDLuuKho,
			@SoHieuKyHieu,
			@MaPTVC,
			@TenPTVC,
			@NgayHangDen,
			@MaDiaDiemDoHang,
			@TenDiaDiemDohang,
			@MaDiaDiemXepHang,
			@TenDiaDiemXepHang,
			@SoLuongCont,
			@MaKetQuaKiemTra,
			@MaVanbanPhapQuy1,
			@MaVanbanPhapQuy2,
			@MaVanbanPhapQuy3,
			@MaVanbanPhapQuy4,
			@MaVanbanPhapQuy5,
			@PhanLoaiHD,
			@SoTiepNhanHD,
			@SoHoaDon,
			@NgayPhatHanhHD,
			@PhuongThucTT,
			@PhanLoaiGiaHD,
			@MaDieuKienGiaHD,
			@MaTTHoaDon,
			@TongTriGiaHD,
			@MaPhanLoaiTriGia,
			@SoTiepNhanTKTriGia,
			@MaTTHieuChinhTriGia,
			@GiaHieuChinhTriGia,
			@MaPhanLoaiPhiVC,
			@MaTTPhiVC,
			@PhiVanChuyen,
			@MaPhanLoaiPhiBH,
			@MaTTPhiBH,
			@PhiBaoHiem,
			@SoDangKyBH,
			@ChiTietKhaiTriGia,
			@TriGiaTinhThue,
			@PhanLoaiKhongQDVND,
			@MaTTTriGiaTinhThue,
			@TongHeSoPhanBoTG,
			@MaLyDoDeNghiBP,
			@NguoiNopThue,
			@MaNHTraThueThay,
			@NamPhatHanhHM,
			@KyHieuCTHanMuc,
			@SoCTHanMuc,
			@MaXDThoiHanNopThue,
			@MaNHBaoLanh,
			@NamPhatHanhBL,
			@KyHieuCTBaoLanh,
			@SoCTBaoLanh,
			@NgayNhapKhoDau,
			@NgayKhoiHanhVC,
			@DiaDiemDichVC,
			@NgayDen,
			@GhiChu,
			@SoQuanLyNoiBoDN,
			@MaPhanLoaiKiemTra,
			@MaSoThueDaiDien,
			@TenCoQuanHaiQuan,
			@NgayThayDoiDangKy,
			@BieuThiTruongHopHetHan,
			@TenDaiLyHaiQuan,
			@MaNhanVienHaiQuan,
			@TenDDLuuKho,
			@MaPhanLoaiTongGiaCoBan,
			@PhanLoaiCongThucChuan,
			@MaPhanLoaiDieuChinhTriGia,
			@PhuongPhapDieuChinhTriGia,
			@TongTienThuePhaiNop,
			@SoTienBaoLanh,
			@TenTruongDonViHaiQuan,
			@NgayCapPhep,
			@PhanLoaiThamTraSauThongQuan,
			@NgayPheDuyetBP,
			@NgayHoanThanhKiemTraBP,
			@SoNgayDoiCapPhepNhapKhau,
			@TieuDe,
			@MaSacThueAnHan_VAT,
			@TenSacThueAnHan_VAT,
			@HanNopThueSauKhiAnHan_VAT,
			@PhanLoaiNopThue,
			@TongSoTienThueXuatKhau,
			@MaTTTongTienThueXuatKhau,
			@TongSoTienLePhi,
			@MaTTCuaSoTienBaoLanh,
			@SoQuanLyNguoiSuDung,
			@NgayHoanThanhKiemTra,
			@TongSoTrangCuaToKhai,
			@TongSoDongHangCuaToKhai,
			@NgayKhaiBaoNopThue,
			@TrangThaiXuLy,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Delete]    Script Date: 11/11/2013 17:34:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_DeleteDynamic]    Script Date: 11/11/2013 17:34:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Insert]    Script Date: 11/11/2013 17:34:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Insert]
	@TKMD_ID bigint,
	@SoToKhaiBoSung numeric(12, 0),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@NhomXuLyHoSoID int,
	@PhanLoaiXuatNhapKhau varchar(1),
	@SoToKhai numeric(12, 0),
	@MaLoaiHinh varchar(3),
	@NgayKhaiBao datetime,
	@NgayCapPhep datetime,
	@ThoiHanTaiNhapTaiXuat datetime,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiKhai nvarchar(300),
	@SoDienThoaiNguoiKhai varchar(20),
	@MaLyDoKhaiBoSung varchar(1),
	@MaTienTeTienThue varchar(3),
	@MaNganHangTraThueThay varchar(110),
	@NamPhatHanhHanMuc numeric(4, 0),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoChungTuPhatHanhHanMuc varchar(10),
	@MaXacDinhThoiHanNopThue varchar(1),
	@MaNganHangBaoLanh varchar(11),
	@NamPhatHanhBaoLanh numeric(4, 0),
	@KyHieuPhatHanhChungTuBaoLanh varchar(10),
	@SoHieuPhatHanhChungTuBaoLanh varchar(10),
	@MaTienTeTruocKhiKhaiBoSung varchar(3),
	@TyGiaHoiDoaiTruocKhiKhaiBoSung numeric(9, 0),
	@MaTienTeSauKhiKhaiBoSung varchar(3),
	@TyGiaHoiDoaiSauKhiKhaiBoSung numeric(9, 0),
	@SoQuanLyTrongNoiBoDoanhNghiep nvarchar(20),
	@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung nvarchar(300),
	@GhiChuNoiDungLienQuanSauKhiKhaiBoSung nvarchar(300),
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@PhanLuong nvarchar(300),
	@HuongDan nvarchar(1000),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@SoThongBao numeric(12, 0),
	@NgayHoanThanhKiemTra datetime,
	@GioHoanThanhKiemTra datetime,
	@NgayDangKyKhaiBoSung datetime,
	@GioDangKyKhaiBoSung datetime,
	@DauHieuBaoQua60Ngay varchar(1),
	@MaHetThoiHan varchar(1),
	@MaDaiLyHaiQuan varchar(5),
	@TenDaiLyHaiQuan nvarchar(50),
	@MaNhanVienHaiQuan varchar(5),
	@PhanLoaiNopThue varchar(1),
	@NgayHieuLucChungTu datetime,
	@SoNgayNopThue numeric(3, 0),
	@SoNgayNopThueDanhChoVATHangHoaDacBiet numeric(3, 0),
	@HienThiTongSoTienTangGiamThueXuatNhapKhau varchar(1),
	@TongSoTienTangGiamThueXuatNhapKhau numeric(11, 0),
	@MaTienTeTongSoTienTangGiamThueXuatNhapKhau varchar(3),
	@TongSoTrangToKhaiBoSung numeric(2, 0),
	@TongSoDongHangToKhaiBoSung numeric(2, 0),
	@LyDo nvarchar(600),
	@TenNguoiPhuTrach nvarchar(108),
	@TenTruongDonViHaiQuan nvarchar(108),
	@NgayDangKyDuLieu datetime,
	@GioDangKyDuLieu datetime,
	@TrangThaiXuLy int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung]
(
	[TKMD_ID],
	[SoToKhaiBoSung],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[NhomXuLyHoSoID],
	[PhanLoaiXuatNhapKhau],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayKhaiBao],
	[NgayCapPhep],
	[ThoiHanTaiNhapTaiXuat],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinh],
	[DiaChiNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[MaLyDoKhaiBoSung],
	[MaTienTeTienThue],
	[MaNganHangTraThueThay],
	[NamPhatHanhHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoChungTuPhatHanhHanMuc],
	[MaXacDinhThoiHanNopThue],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuPhatHanhChungTuBaoLanh],
	[SoHieuPhatHanhChungTuBaoLanh],
	[MaTienTeTruocKhiKhaiBoSung],
	[TyGiaHoiDoaiTruocKhiKhaiBoSung],
	[MaTienTeSauKhiKhaiBoSung],
	[TyGiaHoiDoaiSauKhiKhaiBoSung],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung],
	[GhiChuNoiDungLienQuanSauKhiKhaiBoSung],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoThongBao],
	[NgayHoanThanhKiemTra],
	[GioHoanThanhKiemTra],
	[NgayDangKyKhaiBoSung],
	[GioDangKyKhaiBoSung],
	[DauHieuBaoQua60Ngay],
	[MaHetThoiHan],
	[MaDaiLyHaiQuan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[PhanLoaiNopThue],
	[NgayHieuLucChungTu],
	[SoNgayNopThue],
	[SoNgayNopThueDanhChoVATHangHoaDacBiet],
	[HienThiTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTienTangGiamThueXuatNhapKhau],
	[MaTienTeTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTrangToKhaiBoSung],
	[TongSoDongHangToKhaiBoSung],
	[LyDo],
	[TenNguoiPhuTrach],
	[TenTruongDonViHaiQuan],
	[NgayDangKyDuLieu],
	[GioDangKyDuLieu],
	[TrangThaiXuLy]
)
VALUES 
(
	@TKMD_ID,
	@SoToKhaiBoSung,
	@CoQuanHaiQuan,
	@NhomXuLyHoSo,
	@NhomXuLyHoSoID,
	@PhanLoaiXuatNhapKhau,
	@SoToKhai,
	@MaLoaiHinh,
	@NgayKhaiBao,
	@NgayCapPhep,
	@ThoiHanTaiNhapTaiXuat,
	@MaNguoiKhai,
	@TenNguoiKhai,
	@MaBuuChinh,
	@DiaChiNguoiKhai,
	@SoDienThoaiNguoiKhai,
	@MaLyDoKhaiBoSung,
	@MaTienTeTienThue,
	@MaNganHangTraThueThay,
	@NamPhatHanhHanMuc,
	@KiHieuChungTuPhatHanhHanMuc,
	@SoChungTuPhatHanhHanMuc,
	@MaXacDinhThoiHanNopThue,
	@MaNganHangBaoLanh,
	@NamPhatHanhBaoLanh,
	@KyHieuPhatHanhChungTuBaoLanh,
	@SoHieuPhatHanhChungTuBaoLanh,
	@MaTienTeTruocKhiKhaiBoSung,
	@TyGiaHoiDoaiTruocKhiKhaiBoSung,
	@MaTienTeSauKhiKhaiBoSung,
	@TyGiaHoiDoaiSauKhiKhaiBoSung,
	@SoQuanLyTrongNoiBoDoanhNghiep,
	@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung,
	@GhiChuNoiDungLienQuanSauKhiKhaiBoSung,
	@SoTiepNhan,
	@NgayTiepNhan,
	@PhanLuong,
	@HuongDan,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@SoThongBao,
	@NgayHoanThanhKiemTra,
	@GioHoanThanhKiemTra,
	@NgayDangKyKhaiBoSung,
	@GioDangKyKhaiBoSung,
	@DauHieuBaoQua60Ngay,
	@MaHetThoiHan,
	@MaDaiLyHaiQuan,
	@TenDaiLyHaiQuan,
	@MaNhanVienHaiQuan,
	@PhanLoaiNopThue,
	@NgayHieuLucChungTu,
	@SoNgayNopThue,
	@SoNgayNopThueDanhChoVATHangHoaDacBiet,
	@HienThiTongSoTienTangGiamThueXuatNhapKhau,
	@TongSoTienTangGiamThueXuatNhapKhau,
	@MaTienTeTongSoTienTangGiamThueXuatNhapKhau,
	@TongSoTrangToKhaiBoSung,
	@TongSoDongHangToKhaiBoSung,
	@LyDo,
	@TenNguoiPhuTrach,
	@TenTruongDonViHaiQuan,
	@NgayDangKyDuLieu,
	@GioDangKyDuLieu,
	@TrangThaiXuLy
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_InsertUpdate]    Script Date: 11/11/2013 17:34:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoToKhaiBoSung numeric(12, 0),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@NhomXuLyHoSoID int,
	@PhanLoaiXuatNhapKhau varchar(1),
	@SoToKhai numeric(12, 0),
	@MaLoaiHinh varchar(3),
	@NgayKhaiBao datetime,
	@NgayCapPhep datetime,
	@ThoiHanTaiNhapTaiXuat datetime,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiKhai nvarchar(300),
	@SoDienThoaiNguoiKhai varchar(20),
	@MaLyDoKhaiBoSung varchar(1),
	@MaTienTeTienThue varchar(3),
	@MaNganHangTraThueThay varchar(110),
	@NamPhatHanhHanMuc numeric(4, 0),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoChungTuPhatHanhHanMuc varchar(10),
	@MaXacDinhThoiHanNopThue varchar(1),
	@MaNganHangBaoLanh varchar(11),
	@NamPhatHanhBaoLanh numeric(4, 0),
	@KyHieuPhatHanhChungTuBaoLanh varchar(10),
	@SoHieuPhatHanhChungTuBaoLanh varchar(10),
	@MaTienTeTruocKhiKhaiBoSung varchar(3),
	@TyGiaHoiDoaiTruocKhiKhaiBoSung numeric(9, 0),
	@MaTienTeSauKhiKhaiBoSung varchar(3),
	@TyGiaHoiDoaiSauKhiKhaiBoSung numeric(9, 0),
	@SoQuanLyTrongNoiBoDoanhNghiep nvarchar(20),
	@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung nvarchar(300),
	@GhiChuNoiDungLienQuanSauKhiKhaiBoSung nvarchar(300),
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@PhanLuong nvarchar(300),
	@HuongDan nvarchar(1000),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@SoThongBao numeric(12, 0),
	@NgayHoanThanhKiemTra datetime,
	@GioHoanThanhKiemTra datetime,
	@NgayDangKyKhaiBoSung datetime,
	@GioDangKyKhaiBoSung datetime,
	@DauHieuBaoQua60Ngay varchar(1),
	@MaHetThoiHan varchar(1),
	@MaDaiLyHaiQuan varchar(5),
	@TenDaiLyHaiQuan nvarchar(50),
	@MaNhanVienHaiQuan varchar(5),
	@PhanLoaiNopThue varchar(1),
	@NgayHieuLucChungTu datetime,
	@SoNgayNopThue numeric(3, 0),
	@SoNgayNopThueDanhChoVATHangHoaDacBiet numeric(3, 0),
	@HienThiTongSoTienTangGiamThueXuatNhapKhau varchar(1),
	@TongSoTienTangGiamThueXuatNhapKhau numeric(11, 0),
	@MaTienTeTongSoTienTangGiamThueXuatNhapKhau varchar(3),
	@TongSoTrangToKhaiBoSung numeric(2, 0),
	@TongSoDongHangToKhaiBoSung numeric(2, 0),
	@LyDo nvarchar(600),
	@TenNguoiPhuTrach nvarchar(108),
	@TenTruongDonViHaiQuan nvarchar(108),
	@NgayDangKyDuLieu datetime,
	@GioDangKyDuLieu datetime,
	@TrangThaiXuLy int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoToKhaiBoSung] = @SoToKhaiBoSung,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[NhomXuLyHoSo] = @NhomXuLyHoSo,
			[NhomXuLyHoSoID] = @NhomXuLyHoSoID,
			[PhanLoaiXuatNhapKhau] = @PhanLoaiXuatNhapKhau,
			[SoToKhai] = @SoToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayKhaiBao] = @NgayKhaiBao,
			[NgayCapPhep] = @NgayCapPhep,
			[ThoiHanTaiNhapTaiXuat] = @ThoiHanTaiNhapTaiXuat,
			[MaNguoiKhai] = @MaNguoiKhai,
			[TenNguoiKhai] = @TenNguoiKhai,
			[MaBuuChinh] = @MaBuuChinh,
			[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
			[SoDienThoaiNguoiKhai] = @SoDienThoaiNguoiKhai,
			[MaLyDoKhaiBoSung] = @MaLyDoKhaiBoSung,
			[MaTienTeTienThue] = @MaTienTeTienThue,
			[MaNganHangTraThueThay] = @MaNganHangTraThueThay,
			[NamPhatHanhHanMuc] = @NamPhatHanhHanMuc,
			[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
			[SoChungTuPhatHanhHanMuc] = @SoChungTuPhatHanhHanMuc,
			[MaXacDinhThoiHanNopThue] = @MaXacDinhThoiHanNopThue,
			[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
			[NamPhatHanhBaoLanh] = @NamPhatHanhBaoLanh,
			[KyHieuPhatHanhChungTuBaoLanh] = @KyHieuPhatHanhChungTuBaoLanh,
			[SoHieuPhatHanhChungTuBaoLanh] = @SoHieuPhatHanhChungTuBaoLanh,
			[MaTienTeTruocKhiKhaiBoSung] = @MaTienTeTruocKhiKhaiBoSung,
			[TyGiaHoiDoaiTruocKhiKhaiBoSung] = @TyGiaHoiDoaiTruocKhiKhaiBoSung,
			[MaTienTeSauKhiKhaiBoSung] = @MaTienTeSauKhiKhaiBoSung,
			[TyGiaHoiDoaiSauKhiKhaiBoSung] = @TyGiaHoiDoaiSauKhiKhaiBoSung,
			[SoQuanLyTrongNoiBoDoanhNghiep] = @SoQuanLyTrongNoiBoDoanhNghiep,
			[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung] = @GhiChuNoiDungLienQuanTruocKhiKhaiBoSung,
			[GhiChuNoiDungLienQuanSauKhiKhaiBoSung] = @GhiChuNoiDungLienQuanSauKhiKhaiBoSung,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[PhanLuong] = @PhanLuong,
			[HuongDan] = @HuongDan,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[SoThongBao] = @SoThongBao,
			[NgayHoanThanhKiemTra] = @NgayHoanThanhKiemTra,
			[GioHoanThanhKiemTra] = @GioHoanThanhKiemTra,
			[NgayDangKyKhaiBoSung] = @NgayDangKyKhaiBoSung,
			[GioDangKyKhaiBoSung] = @GioDangKyKhaiBoSung,
			[DauHieuBaoQua60Ngay] = @DauHieuBaoQua60Ngay,
			[MaHetThoiHan] = @MaHetThoiHan,
			[MaDaiLyHaiQuan] = @MaDaiLyHaiQuan,
			[TenDaiLyHaiQuan] = @TenDaiLyHaiQuan,
			[MaNhanVienHaiQuan] = @MaNhanVienHaiQuan,
			[PhanLoaiNopThue] = @PhanLoaiNopThue,
			[NgayHieuLucChungTu] = @NgayHieuLucChungTu,
			[SoNgayNopThue] = @SoNgayNopThue,
			[SoNgayNopThueDanhChoVATHangHoaDacBiet] = @SoNgayNopThueDanhChoVATHangHoaDacBiet,
			[HienThiTongSoTienTangGiamThueXuatNhapKhau] = @HienThiTongSoTienTangGiamThueXuatNhapKhau,
			[TongSoTienTangGiamThueXuatNhapKhau] = @TongSoTienTangGiamThueXuatNhapKhau,
			[MaTienTeTongSoTienTangGiamThueXuatNhapKhau] = @MaTienTeTongSoTienTangGiamThueXuatNhapKhau,
			[TongSoTrangToKhaiBoSung] = @TongSoTrangToKhaiBoSung,
			[TongSoDongHangToKhaiBoSung] = @TongSoDongHangToKhaiBoSung,
			[LyDo] = @LyDo,
			[TenNguoiPhuTrach] = @TenNguoiPhuTrach,
			[TenTruongDonViHaiQuan] = @TenTruongDonViHaiQuan,
			[NgayDangKyDuLieu] = @NgayDangKyDuLieu,
			[GioDangKyDuLieu] = @GioDangKyDuLieu,
			[TrangThaiXuLy] = @TrangThaiXuLy
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung]
		(
			[TKMD_ID],
			[SoToKhaiBoSung],
			[CoQuanHaiQuan],
			[NhomXuLyHoSo],
			[NhomXuLyHoSoID],
			[PhanLoaiXuatNhapKhau],
			[SoToKhai],
			[MaLoaiHinh],
			[NgayKhaiBao],
			[NgayCapPhep],
			[ThoiHanTaiNhapTaiXuat],
			[MaNguoiKhai],
			[TenNguoiKhai],
			[MaBuuChinh],
			[DiaChiNguoiKhai],
			[SoDienThoaiNguoiKhai],
			[MaLyDoKhaiBoSung],
			[MaTienTeTienThue],
			[MaNganHangTraThueThay],
			[NamPhatHanhHanMuc],
			[KiHieuChungTuPhatHanhHanMuc],
			[SoChungTuPhatHanhHanMuc],
			[MaXacDinhThoiHanNopThue],
			[MaNganHangBaoLanh],
			[NamPhatHanhBaoLanh],
			[KyHieuPhatHanhChungTuBaoLanh],
			[SoHieuPhatHanhChungTuBaoLanh],
			[MaTienTeTruocKhiKhaiBoSung],
			[TyGiaHoiDoaiTruocKhiKhaiBoSung],
			[MaTienTeSauKhiKhaiBoSung],
			[TyGiaHoiDoaiSauKhiKhaiBoSung],
			[SoQuanLyTrongNoiBoDoanhNghiep],
			[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung],
			[GhiChuNoiDungLienQuanSauKhiKhaiBoSung],
			[SoTiepNhan],
			[NgayTiepNhan],
			[PhanLuong],
			[HuongDan],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[SoThongBao],
			[NgayHoanThanhKiemTra],
			[GioHoanThanhKiemTra],
			[NgayDangKyKhaiBoSung],
			[GioDangKyKhaiBoSung],
			[DauHieuBaoQua60Ngay],
			[MaHetThoiHan],
			[MaDaiLyHaiQuan],
			[TenDaiLyHaiQuan],
			[MaNhanVienHaiQuan],
			[PhanLoaiNopThue],
			[NgayHieuLucChungTu],
			[SoNgayNopThue],
			[SoNgayNopThueDanhChoVATHangHoaDacBiet],
			[HienThiTongSoTienTangGiamThueXuatNhapKhau],
			[TongSoTienTangGiamThueXuatNhapKhau],
			[MaTienTeTongSoTienTangGiamThueXuatNhapKhau],
			[TongSoTrangToKhaiBoSung],
			[TongSoDongHangToKhaiBoSung],
			[LyDo],
			[TenNguoiPhuTrach],
			[TenTruongDonViHaiQuan],
			[NgayDangKyDuLieu],
			[GioDangKyDuLieu],
			[TrangThaiXuLy]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoToKhaiBoSung,
			@CoQuanHaiQuan,
			@NhomXuLyHoSo,
			@NhomXuLyHoSoID,
			@PhanLoaiXuatNhapKhau,
			@SoToKhai,
			@MaLoaiHinh,
			@NgayKhaiBao,
			@NgayCapPhep,
			@ThoiHanTaiNhapTaiXuat,
			@MaNguoiKhai,
			@TenNguoiKhai,
			@MaBuuChinh,
			@DiaChiNguoiKhai,
			@SoDienThoaiNguoiKhai,
			@MaLyDoKhaiBoSung,
			@MaTienTeTienThue,
			@MaNganHangTraThueThay,
			@NamPhatHanhHanMuc,
			@KiHieuChungTuPhatHanhHanMuc,
			@SoChungTuPhatHanhHanMuc,
			@MaXacDinhThoiHanNopThue,
			@MaNganHangBaoLanh,
			@NamPhatHanhBaoLanh,
			@KyHieuPhatHanhChungTuBaoLanh,
			@SoHieuPhatHanhChungTuBaoLanh,
			@MaTienTeTruocKhiKhaiBoSung,
			@TyGiaHoiDoaiTruocKhiKhaiBoSung,
			@MaTienTeSauKhiKhaiBoSung,
			@TyGiaHoiDoaiSauKhiKhaiBoSung,
			@SoQuanLyTrongNoiBoDoanhNghiep,
			@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung,
			@GhiChuNoiDungLienQuanSauKhiKhaiBoSung,
			@SoTiepNhan,
			@NgayTiepNhan,
			@PhanLuong,
			@HuongDan,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@SoThongBao,
			@NgayHoanThanhKiemTra,
			@GioHoanThanhKiemTra,
			@NgayDangKyKhaiBoSung,
			@GioDangKyKhaiBoSung,
			@DauHieuBaoQua60Ngay,
			@MaHetThoiHan,
			@MaDaiLyHaiQuan,
			@TenDaiLyHaiQuan,
			@MaNhanVienHaiQuan,
			@PhanLoaiNopThue,
			@NgayHieuLucChungTu,
			@SoNgayNopThue,
			@SoNgayNopThueDanhChoVATHangHoaDacBiet,
			@HienThiTongSoTienTangGiamThueXuatNhapKhau,
			@TongSoTienTangGiamThueXuatNhapKhau,
			@MaTienTeTongSoTienTangGiamThueXuatNhapKhau,
			@TongSoTrangToKhaiBoSung,
			@TongSoDongHangToKhaiBoSung,
			@LyDo,
			@TenNguoiPhuTrach,
			@TenTruongDonViHaiQuan,
			@NgayDangKyDuLieu,
			@GioDangKyDuLieu,
			@TrangThaiXuLy
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Load]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoToKhaiBoSung],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[NhomXuLyHoSoID],
	[PhanLoaiXuatNhapKhau],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayKhaiBao],
	[NgayCapPhep],
	[ThoiHanTaiNhapTaiXuat],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinh],
	[DiaChiNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[MaLyDoKhaiBoSung],
	[MaTienTeTienThue],
	[MaNganHangTraThueThay],
	[NamPhatHanhHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoChungTuPhatHanhHanMuc],
	[MaXacDinhThoiHanNopThue],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuPhatHanhChungTuBaoLanh],
	[SoHieuPhatHanhChungTuBaoLanh],
	[MaTienTeTruocKhiKhaiBoSung],
	[TyGiaHoiDoaiTruocKhiKhaiBoSung],
	[MaTienTeSauKhiKhaiBoSung],
	[TyGiaHoiDoaiSauKhiKhaiBoSung],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung],
	[GhiChuNoiDungLienQuanSauKhiKhaiBoSung],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoThongBao],
	[NgayHoanThanhKiemTra],
	[GioHoanThanhKiemTra],
	[NgayDangKyKhaiBoSung],
	[GioDangKyKhaiBoSung],
	[DauHieuBaoQua60Ngay],
	[MaHetThoiHan],
	[MaDaiLyHaiQuan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[PhanLoaiNopThue],
	[NgayHieuLucChungTu],
	[SoNgayNopThue],
	[SoNgayNopThueDanhChoVATHangHoaDacBiet],
	[HienThiTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTienTangGiamThueXuatNhapKhau],
	[MaTienTeTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTrangToKhaiBoSung],
	[TongSoDongHangToKhaiBoSung],
	[LyDo],
	[TenNguoiPhuTrach],
	[TenTruongDonViHaiQuan],
	[NgayDangKyDuLieu],
	[GioDangKyDuLieu],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectAll]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoToKhaiBoSung],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[NhomXuLyHoSoID],
	[PhanLoaiXuatNhapKhau],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayKhaiBao],
	[NgayCapPhep],
	[ThoiHanTaiNhapTaiXuat],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinh],
	[DiaChiNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[MaLyDoKhaiBoSung],
	[MaTienTeTienThue],
	[MaNganHangTraThueThay],
	[NamPhatHanhHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoChungTuPhatHanhHanMuc],
	[MaXacDinhThoiHanNopThue],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuPhatHanhChungTuBaoLanh],
	[SoHieuPhatHanhChungTuBaoLanh],
	[MaTienTeTruocKhiKhaiBoSung],
	[TyGiaHoiDoaiTruocKhiKhaiBoSung],
	[MaTienTeSauKhiKhaiBoSung],
	[TyGiaHoiDoaiSauKhiKhaiBoSung],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung],
	[GhiChuNoiDungLienQuanSauKhiKhaiBoSung],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoThongBao],
	[NgayHoanThanhKiemTra],
	[GioHoanThanhKiemTra],
	[NgayDangKyKhaiBoSung],
	[GioDangKyKhaiBoSung],
	[DauHieuBaoQua60Ngay],
	[MaHetThoiHan],
	[MaDaiLyHaiQuan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[PhanLoaiNopThue],
	[NgayHieuLucChungTu],
	[SoNgayNopThue],
	[SoNgayNopThueDanhChoVATHangHoaDacBiet],
	[HienThiTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTienTangGiamThueXuatNhapKhau],
	[MaTienTeTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTrangToKhaiBoSung],
	[TongSoDongHangToKhaiBoSung],
	[LyDo],
	[TenNguoiPhuTrach],
	[TenTruongDonViHaiQuan],
	[NgayDangKyDuLieu],
	[GioDangKyDuLieu],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamic]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoToKhaiBoSung],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[NhomXuLyHoSoID],
	[PhanLoaiXuatNhapKhau],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayKhaiBao],
	[NgayCapPhep],
	[ThoiHanTaiNhapTaiXuat],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinh],
	[DiaChiNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[MaLyDoKhaiBoSung],
	[MaTienTeTienThue],
	[MaNganHangTraThueThay],
	[NamPhatHanhHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoChungTuPhatHanhHanMuc],
	[MaXacDinhThoiHanNopThue],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuPhatHanhChungTuBaoLanh],
	[SoHieuPhatHanhChungTuBaoLanh],
	[MaTienTeTruocKhiKhaiBoSung],
	[TyGiaHoiDoaiTruocKhiKhaiBoSung],
	[MaTienTeSauKhiKhaiBoSung],
	[TyGiaHoiDoaiSauKhiKhaiBoSung],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung],
	[GhiChuNoiDungLienQuanSauKhiKhaiBoSung],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoThongBao],
	[NgayHoanThanhKiemTra],
	[GioHoanThanhKiemTra],
	[NgayDangKyKhaiBoSung],
	[GioDangKyKhaiBoSung],
	[DauHieuBaoQua60Ngay],
	[MaHetThoiHan],
	[MaDaiLyHaiQuan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[PhanLoaiNopThue],
	[NgayHieuLucChungTu],
	[SoNgayNopThue],
	[SoNgayNopThueDanhChoVATHangHoaDacBiet],
	[HienThiTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTienTangGiamThueXuatNhapKhau],
	[MaTienTeTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTrangToKhaiBoSung],
	[TongSoDongHangToKhaiBoSung],
	[LyDo],
	[TenNguoiPhuTrach],
	[TenTruongDonViHaiQuan],
	[NgayDangKyDuLieu],
	[GioDangKyDuLieu],
	[TrangThaiXuLy]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Delete]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_DeleteDynamic]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Insert]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Insert]
	@TKMDBoSung_ID bigint,
	@SoDong varchar(2),
	@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac varchar(1),
	@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac nvarchar(9),
	@HienThiTongSoTienTangGiamThuKhac varchar(1),
	@TongSoTienTangGiamThuKhac numeric(11, 0),
	@MaTienTeTongSoTienTangGiamThuKhac varchar(3),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac]
(
	[TKMDBoSung_ID],
	[SoDong],
	[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[HienThiTongSoTienTangGiamThuKhac],
	[TongSoTienTangGiamThuKhac],
	[MaTienTeTongSoTienTangGiamThuKhac]
)
VALUES 
(
	@TKMDBoSung_ID,
	@SoDong,
	@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
	@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
	@HienThiTongSoTienTangGiamThuKhac,
	@TongSoTienTangGiamThuKhac,
	@MaTienTeTongSoTienTangGiamThuKhac
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_InsertUpdate]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_InsertUpdate]
	@ID bigint,
	@TKMDBoSung_ID bigint,
	@SoDong varchar(2),
	@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac varchar(1),
	@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac nvarchar(9),
	@HienThiTongSoTienTangGiamThuKhac varchar(1),
	@TongSoTienTangGiamThuKhac numeric(11, 0),
	@MaTienTeTongSoTienTangGiamThuKhac varchar(3)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac] 
		SET
			[TKMDBoSung_ID] = @TKMDBoSung_ID,
			[SoDong] = @SoDong,
			[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac] = @MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
			[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac] = @TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
			[HienThiTongSoTienTangGiamThuKhac] = @HienThiTongSoTienTangGiamThuKhac,
			[TongSoTienTangGiamThuKhac] = @TongSoTienTangGiamThuKhac,
			[MaTienTeTongSoTienTangGiamThuKhac] = @MaTienTeTongSoTienTangGiamThuKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac]
		(
			[TKMDBoSung_ID],
			[SoDong],
			[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
			[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
			[HienThiTongSoTienTangGiamThuKhac],
			[TongSoTienTangGiamThuKhac],
			[MaTienTeTongSoTienTangGiamThuKhac]
		)
		VALUES 
		(
			@TKMDBoSung_ID,
			@SoDong,
			@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
			@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
			@HienThiTongSoTienTangGiamThuKhac,
			@TongSoTienTangGiamThuKhac,
			@MaTienTeTongSoTienTangGiamThuKhac
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Load]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[HienThiTongSoTienTangGiamThuKhac],
	[TongSoTienTangGiamThuKhac],
	[MaTienTeTongSoTienTangGiamThuKhac]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectAll]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[HienThiTongSoTienTangGiamThuKhac],
	[TongSoTienTangGiamThuKhac],
	[MaTienTeTongSoTienTangGiamThuKhac]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectDynamic]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[HienThiTongSoTienTangGiamThuKhac],
	[TongSoTienTangGiamThuKhac],
	[MaTienTeTongSoTienTangGiamThuKhac]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Update]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Update]
	@ID bigint,
	@TKMDBoSung_ID bigint,
	@SoDong varchar(2),
	@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac varchar(1),
	@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac nvarchar(9),
	@HienThiTongSoTienTangGiamThuKhac varchar(1),
	@TongSoTienTangGiamThuKhac numeric(11, 0),
	@MaTienTeTongSoTienTangGiamThuKhac varchar(3)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac]
SET
	[TKMDBoSung_ID] = @TKMDBoSung_ID,
	[SoDong] = @SoDong,
	[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac] = @MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
	[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac] = @TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
	[HienThiTongSoTienTangGiamThuKhac] = @HienThiTongSoTienTangGiamThuKhac,
	[TongSoTienTangGiamThuKhac] = @TongSoTienTangGiamThuKhac,
	[MaTienTeTongSoTienTangGiamThuKhac] = @MaTienTeTongSoTienTangGiamThuKhac
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Update]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoToKhaiBoSung numeric(12, 0),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@NhomXuLyHoSoID int,
	@PhanLoaiXuatNhapKhau varchar(1),
	@SoToKhai numeric(12, 0),
	@MaLoaiHinh varchar(3),
	@NgayKhaiBao datetime,
	@NgayCapPhep datetime,
	@ThoiHanTaiNhapTaiXuat datetime,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiKhai nvarchar(300),
	@SoDienThoaiNguoiKhai varchar(20),
	@MaLyDoKhaiBoSung varchar(1),
	@MaTienTeTienThue varchar(3),
	@MaNganHangTraThueThay varchar(110),
	@NamPhatHanhHanMuc numeric(4, 0),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoChungTuPhatHanhHanMuc varchar(10),
	@MaXacDinhThoiHanNopThue varchar(1),
	@MaNganHangBaoLanh varchar(11),
	@NamPhatHanhBaoLanh numeric(4, 0),
	@KyHieuPhatHanhChungTuBaoLanh varchar(10),
	@SoHieuPhatHanhChungTuBaoLanh varchar(10),
	@MaTienTeTruocKhiKhaiBoSung varchar(3),
	@TyGiaHoiDoaiTruocKhiKhaiBoSung numeric(9, 0),
	@MaTienTeSauKhiKhaiBoSung varchar(3),
	@TyGiaHoiDoaiSauKhiKhaiBoSung numeric(9, 0),
	@SoQuanLyTrongNoiBoDoanhNghiep nvarchar(20),
	@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung nvarchar(300),
	@GhiChuNoiDungLienQuanSauKhiKhaiBoSung nvarchar(300),
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@PhanLuong nvarchar(300),
	@HuongDan nvarchar(1000),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@SoThongBao numeric(12, 0),
	@NgayHoanThanhKiemTra datetime,
	@GioHoanThanhKiemTra datetime,
	@NgayDangKyKhaiBoSung datetime,
	@GioDangKyKhaiBoSung datetime,
	@DauHieuBaoQua60Ngay varchar(1),
	@MaHetThoiHan varchar(1),
	@MaDaiLyHaiQuan varchar(5),
	@TenDaiLyHaiQuan nvarchar(50),
	@MaNhanVienHaiQuan varchar(5),
	@PhanLoaiNopThue varchar(1),
	@NgayHieuLucChungTu datetime,
	@SoNgayNopThue numeric(3, 0),
	@SoNgayNopThueDanhChoVATHangHoaDacBiet numeric(3, 0),
	@HienThiTongSoTienTangGiamThueXuatNhapKhau varchar(1),
	@TongSoTienTangGiamThueXuatNhapKhau numeric(11, 0),
	@MaTienTeTongSoTienTangGiamThueXuatNhapKhau varchar(3),
	@TongSoTrangToKhaiBoSung numeric(2, 0),
	@TongSoDongHangToKhaiBoSung numeric(2, 0),
	@LyDo nvarchar(600),
	@TenNguoiPhuTrach nvarchar(108),
	@TenTruongDonViHaiQuan nvarchar(108),
	@NgayDangKyDuLieu datetime,
	@GioDangKyDuLieu datetime,
	@TrangThaiXuLy int
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoToKhaiBoSung] = @SoToKhaiBoSung,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[NhomXuLyHoSo] = @NhomXuLyHoSo,
	[NhomXuLyHoSoID] = @NhomXuLyHoSoID,
	[PhanLoaiXuatNhapKhau] = @PhanLoaiXuatNhapKhau,
	[SoToKhai] = @SoToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayKhaiBao] = @NgayKhaiBao,
	[NgayCapPhep] = @NgayCapPhep,
	[ThoiHanTaiNhapTaiXuat] = @ThoiHanTaiNhapTaiXuat,
	[MaNguoiKhai] = @MaNguoiKhai,
	[TenNguoiKhai] = @TenNguoiKhai,
	[MaBuuChinh] = @MaBuuChinh,
	[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
	[SoDienThoaiNguoiKhai] = @SoDienThoaiNguoiKhai,
	[MaLyDoKhaiBoSung] = @MaLyDoKhaiBoSung,
	[MaTienTeTienThue] = @MaTienTeTienThue,
	[MaNganHangTraThueThay] = @MaNganHangTraThueThay,
	[NamPhatHanhHanMuc] = @NamPhatHanhHanMuc,
	[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
	[SoChungTuPhatHanhHanMuc] = @SoChungTuPhatHanhHanMuc,
	[MaXacDinhThoiHanNopThue] = @MaXacDinhThoiHanNopThue,
	[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
	[NamPhatHanhBaoLanh] = @NamPhatHanhBaoLanh,
	[KyHieuPhatHanhChungTuBaoLanh] = @KyHieuPhatHanhChungTuBaoLanh,
	[SoHieuPhatHanhChungTuBaoLanh] = @SoHieuPhatHanhChungTuBaoLanh,
	[MaTienTeTruocKhiKhaiBoSung] = @MaTienTeTruocKhiKhaiBoSung,
	[TyGiaHoiDoaiTruocKhiKhaiBoSung] = @TyGiaHoiDoaiTruocKhiKhaiBoSung,
	[MaTienTeSauKhiKhaiBoSung] = @MaTienTeSauKhiKhaiBoSung,
	[TyGiaHoiDoaiSauKhiKhaiBoSung] = @TyGiaHoiDoaiSauKhiKhaiBoSung,
	[SoQuanLyTrongNoiBoDoanhNghiep] = @SoQuanLyTrongNoiBoDoanhNghiep,
	[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung] = @GhiChuNoiDungLienQuanTruocKhiKhaiBoSung,
	[GhiChuNoiDungLienQuanSauKhiKhaiBoSung] = @GhiChuNoiDungLienQuanSauKhiKhaiBoSung,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[PhanLuong] = @PhanLuong,
	[HuongDan] = @HuongDan,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[SoThongBao] = @SoThongBao,
	[NgayHoanThanhKiemTra] = @NgayHoanThanhKiemTra,
	[GioHoanThanhKiemTra] = @GioHoanThanhKiemTra,
	[NgayDangKyKhaiBoSung] = @NgayDangKyKhaiBoSung,
	[GioDangKyKhaiBoSung] = @GioDangKyKhaiBoSung,
	[DauHieuBaoQua60Ngay] = @DauHieuBaoQua60Ngay,
	[MaHetThoiHan] = @MaHetThoiHan,
	[MaDaiLyHaiQuan] = @MaDaiLyHaiQuan,
	[TenDaiLyHaiQuan] = @TenDaiLyHaiQuan,
	[MaNhanVienHaiQuan] = @MaNhanVienHaiQuan,
	[PhanLoaiNopThue] = @PhanLoaiNopThue,
	[NgayHieuLucChungTu] = @NgayHieuLucChungTu,
	[SoNgayNopThue] = @SoNgayNopThue,
	[SoNgayNopThueDanhChoVATHangHoaDacBiet] = @SoNgayNopThueDanhChoVATHangHoaDacBiet,
	[HienThiTongSoTienTangGiamThueXuatNhapKhau] = @HienThiTongSoTienTangGiamThueXuatNhapKhau,
	[TongSoTienTangGiamThueXuatNhapKhau] = @TongSoTienTangGiamThueXuatNhapKhau,
	[MaTienTeTongSoTienTangGiamThueXuatNhapKhau] = @MaTienTeTongSoTienTangGiamThueXuatNhapKhau,
	[TongSoTrangToKhaiBoSung] = @TongSoTrangToKhaiBoSung,
	[TongSoDongHangToKhaiBoSung] = @TongSoDongHangToKhaiBoSung,
	[LyDo] = @LyDo,
	[TenNguoiPhuTrach] = @TenNguoiPhuTrach,
	[TenTruongDonViHaiQuan] = @TenTruongDonViHaiQuan,
	[NgayDangKyDuLieu] = @NgayDangKyDuLieu,
	[GioDangKyDuLieu] = @GioDangKyDuLieu,
	[TrangThaiXuLy] = @TrangThaiXuLy
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Delete]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_DeleteDynamic]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Insert]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Insert]
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuan nvarchar(210),
	@SoChungTu numeric(12, 0),
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaPhanLoaiToKhai varchar(3),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoPhiPhaiNop numeric(12, 0),
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@NgayPhatHanhChungTu datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]
(
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuan],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoPhiPhaiNop],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[NgayPhatHanhChungTu]
)
VALUES 
(
	@TenCoQuanHaiQuan,
	@TenChiCucHaiQuan,
	@SoChungTu,
	@TenDonViXuatNhapKhau,
	@MaDonViXuatNhapKhau,
	@MaBuuChinh,
	@DiaChiNguoiXuatNhapKhau,
	@SoDienThoaiNguoiXuatNhapKhau,
	@SoToKhai,
	@NgayDangKyToKhai,
	@MaPhanLoaiToKhai,
	@TenNganHangTraThay,
	@MaNganHangTraThay,
	@KiHieuChungTuPhatHanhHanMuc,
	@SoHieuPhatHanhHanMuc,
	@TongSoPhiPhaiNop,
	@SoTaiKhoanKhoBac,
	@TenKhoBac,
	@NgayPhatHanhChungTu
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_InsertUpdate]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_InsertUpdate]
	@ID bigint,
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuan nvarchar(210),
	@SoChungTu numeric(12, 0),
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaPhanLoaiToKhai varchar(3),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoPhiPhaiNop numeric(12, 0),
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@NgayPhatHanhChungTu datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu] 
		SET
			[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
			[TenChiCucHaiQuan] = @TenChiCucHaiQuan,
			[SoChungTu] = @SoChungTu,
			[TenDonViXuatNhapKhau] = @TenDonViXuatNhapKhau,
			[MaDonViXuatNhapKhau] = @MaDonViXuatNhapKhau,
			[MaBuuChinh] = @MaBuuChinh,
			[DiaChiNguoiXuatNhapKhau] = @DiaChiNguoiXuatNhapKhau,
			[SoDienThoaiNguoiXuatNhapKhau] = @SoDienThoaiNguoiXuatNhapKhau,
			[SoToKhai] = @SoToKhai,
			[NgayDangKyToKhai] = @NgayDangKyToKhai,
			[MaPhanLoaiToKhai] = @MaPhanLoaiToKhai,
			[TenNganHangTraThay] = @TenNganHangTraThay,
			[MaNganHangTraThay] = @MaNganHangTraThay,
			[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
			[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
			[TongSoPhiPhaiNop] = @TongSoPhiPhaiNop,
			[SoTaiKhoanKhoBac] = @SoTaiKhoanKhoBac,
			[TenKhoBac] = @TenKhoBac,
			[NgayPhatHanhChungTu] = @NgayPhatHanhChungTu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]
		(
			[TenCoQuanHaiQuan],
			[TenChiCucHaiQuan],
			[SoChungTu],
			[TenDonViXuatNhapKhau],
			[MaDonViXuatNhapKhau],
			[MaBuuChinh],
			[DiaChiNguoiXuatNhapKhau],
			[SoDienThoaiNguoiXuatNhapKhau],
			[SoToKhai],
			[NgayDangKyToKhai],
			[MaPhanLoaiToKhai],
			[TenNganHangTraThay],
			[MaNganHangTraThay],
			[KiHieuChungTuPhatHanhHanMuc],
			[SoHieuPhatHanhHanMuc],
			[TongSoPhiPhaiNop],
			[SoTaiKhoanKhoBac],
			[TenKhoBac],
			[NgayPhatHanhChungTu]
		)
		VALUES 
		(
			@TenCoQuanHaiQuan,
			@TenChiCucHaiQuan,
			@SoChungTu,
			@TenDonViXuatNhapKhau,
			@MaDonViXuatNhapKhau,
			@MaBuuChinh,
			@DiaChiNguoiXuatNhapKhau,
			@SoDienThoaiNguoiXuatNhapKhau,
			@SoToKhai,
			@NgayDangKyToKhai,
			@MaPhanLoaiToKhai,
			@TenNganHangTraThay,
			@MaNganHangTraThay,
			@KiHieuChungTuPhatHanhHanMuc,
			@SoHieuPhatHanhHanMuc,
			@TongSoPhiPhaiNop,
			@SoTaiKhoanKhoBac,
			@TenKhoBac,
			@NgayPhatHanhChungTu
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Load]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuan],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoPhiPhaiNop],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[NgayPhatHanhChungTu]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectAll]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuan],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoPhiPhaiNop],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[NgayPhatHanhChungTu]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectDynamic]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuan],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoPhiPhaiNop],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[NgayPhatHanhChungTu]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Update]    Script Date: 11/11/2013 17:34:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Update]
	@ID bigint,
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuan nvarchar(210),
	@SoChungTu numeric(12, 0),
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaPhanLoaiToKhai varchar(3),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoPhiPhaiNop numeric(12, 0),
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@NgayPhatHanhChungTu datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]
SET
	[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
	[TenChiCucHaiQuan] = @TenChiCucHaiQuan,
	[SoChungTu] = @SoChungTu,
	[TenDonViXuatNhapKhau] = @TenDonViXuatNhapKhau,
	[MaDonViXuatNhapKhau] = @MaDonViXuatNhapKhau,
	[MaBuuChinh] = @MaBuuChinh,
	[DiaChiNguoiXuatNhapKhau] = @DiaChiNguoiXuatNhapKhau,
	[SoDienThoaiNguoiXuatNhapKhau] = @SoDienThoaiNguoiXuatNhapKhau,
	[SoToKhai] = @SoToKhai,
	[NgayDangKyToKhai] = @NgayDangKyToKhai,
	[MaPhanLoaiToKhai] = @MaPhanLoaiToKhai,
	[TenNganHangTraThay] = @TenNganHangTraThay,
	[MaNganHangTraThay] = @MaNganHangTraThay,
	[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
	[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
	[TongSoPhiPhaiNop] = @TongSoPhiPhaiNop,
	[SoTaiKhoanKhoBac] = @SoTaiKhoanKhoBac,
	[TenKhoBac] = @TenKhoBac,
	[NgayPhatHanhChungTu] = @NgayPhatHanhChungTu
WHERE
	[ID] = @ID


GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '11.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('11.1', GETDATE(), N'Cap nhat store (VNACCS)')
END	

