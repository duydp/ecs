﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using System.Text;
using System.Security.Cryptography;

namespace Company.Interface
{
    public partial class WSDBDLForm : BaseForm
    {
        public bool IsReady = false;
        public static bool IsSuccess = false;
        public string USERNAME_DONGBO = "";
        public string PASSWOR_DONGBO = "";
        public bool IsSave = false;
        public WSDBDLForm()
        {
            InitializeComponent();
        }

        private void WSForm_Load(object sender, EventArgs e)
        {
            txtMaDoanhNghiep.Text = "";
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {

            
            if (!string.IsNullOrEmpty(txtMaDoanhNghiep.Text) && !string.IsNullOrEmpty(txtMatKhau.Text))
            {
                if (ckLuu.Checked)
                {
                    IsSave = true;
                    GlobalSettings.USERNAME_DONGBO = txtMaDoanhNghiep.Text;
                    GlobalSettings.PASSWOR_DONGBO = DongBoDuLieu_New.ServiceDongBoDuLieu.GetMD5Value(txtMatKhau.Text);
                }
                else
                {
                    IsSave = false;
                    USERNAME_DONGBO = txtMaDoanhNghiep.Text;
                    PASSWOR_DONGBO = DongBoDuLieu_New.ServiceDongBoDuLieu.GetMD5Value(txtMatKhau.Text);
                }
                IsReady = true;
                this.Close();
            }
            else
            {
                ShowMessage("Chưa nhập [Tên đăng nhâp] hoặc [Mật khẩu] ", false);
                return;
            }
        }
        private void WSForm2_FormClosing(object sender, FormClosingEventArgs e)
        {
            //IsReady = false;
        }

        private void btnAction_Click(object sender, EventArgs e)
        {
            IsReady = false;
        }

    }
}