using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public class DonViHaiQuan : BaseClass
    {
        public static DataSet SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_DonViHaiQuan ORDER BY Ten";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }

        public static string GetName(string id)
        {
            //DataSet ds = SelectDynamic(string.Format("ID = '{0}'", id.PadRight(6)), "");
            //if (ds.Tables[0].Rows.Count > 0)
            //{
            //    return ds.Tables[0].Rows[0]["Ten"].ToString();
            //}
            //return string.Empty;

            try
            {
                string query = string.Format("[ID] = '{0}'", id.PadRight(6));

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["DonViHaiQuan"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["Ten"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(id, ex); }

            return string.Empty;
        }

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string query = "SELECT * FROM t_HaiQuan_DonViHaiQuan";
            if (whereCondition.Length > 0)
            {
                query = "SELECT * FROM t_HaiQuan_DonViHaiQuan WHERE " + whereCondition;
            }

            if (orderByExpression.Length > 0)
            {
                query += " ORDER BY " + orderByExpression;
            }

            SqlCommand dbCommand = (SqlCommand) db.GetSqlStringCommand(query);

            return db.ExecuteDataSet(dbCommand);
        }
        public static void Update(DataSet ds)
        {
            string insert = "Insert INTO t_HaiQuan_DonViHaiQuan VALUES(@Id,@Ten,@DateCreated,@DateModified)";
            string update = "UPDATE t_HaiQuan_DonViHaiQuan SET Ten = @Ten, DateModified = @DateModified WHERE Id=@Id";
            string delete = "DELETE FROM t_HaiQuan_DonViHaiQuan WHERE ID = @Id";

            DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);

            DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
    }
}