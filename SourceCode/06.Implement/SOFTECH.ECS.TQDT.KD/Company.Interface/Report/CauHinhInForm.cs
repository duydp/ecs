﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.KDT;

namespace Company.Interface.Report
{
    public partial class CauHinhInForm : Company.Interface.BaseForm
    {
        public CauHinhInForm()
        {
            InitializeComponent();
        }

        private void CauHinhInForm_Load(object sender, EventArgs e)
        {
            try
            {
                GlobalSettings.KhoiTao_GiaTriMacDinh();
                txtDinhMuc.Value = GlobalSettings.SoThapPhan.DinhMuc;
                txtLuongNPL.Value = GlobalSettings.SoThapPhan.LuongNPL;
                txtLuongSP.Value = GlobalSettings.SoThapPhan.LuongSP;

                try
                {
                    txtSoLuongHMD.Value = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SoTPSoLuongHMD");
                    txtTrongLuongTK.Value = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SoTPTrongLuongTK");
                    txtFontTenHang.Value = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang");
                }
                catch
                {
                    ShowMessage("Lỗi đọc thông tin cấu hình số thập phân.", false);
                    txtSoLuongHMD.Value = 0;
                    txtTrongLuongTK.Value = 0;
                    txtFontTenHang.Value = 0;
                }

                string delay = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TimeDelay");
                numTimeDelay.Value = Convert.ToDecimal(delay);

                //TODO: Hungtq updated 01/03/2012.
                cbHienThiHoaDonTKX.SelectedValue = GlobalSettings.HienThiHoaDonTKX;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                HeThongPhongKhaiCollection htpkCollection = new HeThongPhongKhaiCollection();
                GlobalSettings.SoThapPhan.DinhMuc = (int)txtDinhMuc.Value;
                HeThongPhongKhai htpk = new HeThongPhongKhai();
                htpk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                htpk.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                htpk.PassWord = "";
                htpk.Role = 0;
                htpk.Key_Config = "DinhMuc";
                htpk.Value_Config = txtDinhMuc.Text;
                htpkCollection.Add(htpk);

                GlobalSettings.SoThapPhan.LuongNPL = (int)txtLuongNPL.Value;
                htpk = new HeThongPhongKhai();
                htpk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                htpk.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                htpk.PassWord = "";
                htpk.Role = 0;
                htpk.Key_Config = "LuongNPL";
                htpk.Value_Config = txtLuongNPL.Text;
                htpkCollection.Add(htpk);

                GlobalSettings.SoThapPhan.LuongSP = (int)txtLuongSP.Value;
                htpk = new HeThongPhongKhai();
                htpk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                htpk.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                htpk.PassWord = "";
                htpk.Role = 0;
                htpk.Key_Config = "LuongSP";
                htpk.Value_Config = txtLuongSP.Text;
                htpkCollection.Add(htpk);

                try
                {
                    GlobalSettings.SoThapPhan.SoLuongHMD = (int)txtSoLuongHMD.Value;
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SoTPSoLuongHMD", txtSoLuongHMD.Value);
                    htpk = new HeThongPhongKhai();
                    htpk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    htpk.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                    htpk.PassWord = "";
                    htpk.Role = 0;
                    htpk.Key_Config = "SoLuongTK";
                    htpk.Value_Config = txtSoLuongHMD.Text;
                    htpkCollection.Add(htpk);

                    //DATLMQ bổ sung cấu hình số thập phân trọng lượng ở ngoài tờ khai 14/03/2011
                    GlobalSettings.SoThapPhan.TrongLuongHangTK = (int)txtTrongLuongTK.Value;
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SoTPTrongLuongTK", txtTrongLuongTK.Value);
                    htpk = new HeThongPhongKhai();
                    htpk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    htpk.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                    htpk.PassWord = "";
                    htpk.Role = 0;
                    htpk.Key_Config = "TrongLuongTK";
                    htpk.Value_Config = txtTrongLuongTK.Text;
                    htpkCollection.Add(htpk);

                    //DATLMQ bổ sung Cấu hình font tên hàng hóa 18/05/2011
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FontTenHang", txtFontTenHang.Text);

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TimeDelay", numTimeDelay.Value.ToString());

                    //TODO: Hungtq updated 01/03/2012.
                    GlobalSettings.HienThiHoaDonTKX = cbHienThiHoaDonTKX.SelectedValue.ToString();
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("HienThiHoaDonTKX", cbHienThiHoaDonTKX.SelectedValue.ToString());
                }
                catch
                {
                    ShowMessage("Lỗi: Thiếu một số key trong file config", false);
                    return;
                }

                try
                {
                    htpk.InsertUpdate(htpkCollection);
                    ShowMessage("Lưu thông tin cấu hình thành công.", false);
                }
                catch
                {
                    ShowMessage("Có lỗi khi lưu thông tin cấu hình.", false);
                    return;
                }

                this.Close();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

    }
}

