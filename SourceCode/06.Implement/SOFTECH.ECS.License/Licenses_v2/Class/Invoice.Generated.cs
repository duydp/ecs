using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Activation.Server
{
    public partial class Invoice
    {
        #region Private members.

        protected string _ID = string.Empty;
        protected string _CustomerID = string.Empty;
        protected string _EmployeeID = string.Empty;
        protected DateTime _CreateDate;
        protected string _Notes = string.Empty;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public string ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }

        public string CustomerID
        {
            set { this._CustomerID = value; }
            get { return this._CustomerID; }
        }

        public string EmployeeID
        {
            set { this._EmployeeID = value; }
            get { return this._EmployeeID; }
        }

        public DateTime CreateDate
        {
            set { this._CreateDate = value; }
            get { return this._CreateDate; }
        }

        public string Notes
        {
            set { this._Notes = value; }
            get { return this._Notes; }
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static Invoice Load(string iD)
        {
            const string spName = "[dbo].[p_LICENSE_Invoice_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, iD);
            Invoice entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new Invoice();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetString(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerID"))) entity.CustomerID = reader.GetString(reader.GetOrdinal("CustomerID"));
                if (!reader.IsDBNull(reader.GetOrdinal("EmployeeID"))) entity.EmployeeID = reader.GetString(reader.GetOrdinal("EmployeeID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreateDate"))) entity.CreateDate = reader.GetDateTime(reader.GetOrdinal("CreateDate"));
                if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_CustomerID(string customerID)
        {
            const string spName = "[dbo].[p_LICENSE_Invoice_SelectBy_CustomerID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@CustomerID", SqlDbType.VarChar, customerID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static DataSet SelectBy_EmployeeID(string employeeID)
        {
            const string spName = "[dbo].[p_LICENSE_Invoice_SelectBy_EmployeeID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@EmployeeID", SqlDbType.VarChar, employeeID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LICENSE_Invoice_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_LICENSE_Invoice_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LICENSE_Invoice_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_LICENSE_Invoice_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_CustomerID(string customerID)
        {
            const string spName = "p_LICENSE_Invoice_SelectBy_CustomerID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@CustomerID", SqlDbType.VarChar, customerID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static IDataReader SelectReaderBy_EmployeeID(string employeeID)
        {
            const string spName = "p_LICENSE_Invoice_SelectBy_EmployeeID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@EmployeeID", SqlDbType.VarChar, employeeID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static string InsertInvoice(string customerID, string employeeID, DateTime createDate, string notes)
        {
            Invoice entity = new Invoice();
            entity.CustomerID = customerID;
            entity.EmployeeID = employeeID;
            entity.CreateDate = createDate;
            entity.Notes = notes;
            return entity.Insert();
        }

        public string Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public string Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_Invoice_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
            db.AddInParameter(dbCommand, "@CustomerID", SqlDbType.VarChar, CustomerID);
            db.AddInParameter(dbCommand, "@EmployeeID", SqlDbType.VarChar, EmployeeID);
            db.AddInParameter(dbCommand, "@CreateDate", SqlDbType.DateTime, CreateDate);
            db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (string)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                this._ID = (string)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateInvoice(string iD, string customerID, string employeeID, DateTime createDate, string notes)
        {
            Invoice entity = new Invoice();
            entity.CustomerID = customerID;
            entity.EmployeeID = employeeID;
            entity.CreateDate = createDate;
            entity.Notes = notes;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_LICENSE_Invoice_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
            db.AddInParameter(dbCommand, "@CustomerID", SqlDbType.VarChar, CustomerID);
            db.AddInParameter(dbCommand, "@EmployeeID", SqlDbType.VarChar, EmployeeID);
            db.AddInParameter(dbCommand, "@CreateDate", SqlDbType.DateTime, CreateDate);
            db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateInvoice(string iD, string customerID, string employeeID, DateTime createDate, string notes)
        {
            Invoice entity = new Invoice();
            entity.ID = iD;
            entity.CustomerID = customerID;
            entity.EmployeeID = employeeID;
            entity.CreateDate = createDate;
            entity.Notes = notes;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_Invoice_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
            db.AddInParameter(dbCommand, "@CustomerID", SqlDbType.VarChar, CustomerID);
            db.AddInParameter(dbCommand, "@EmployeeID", SqlDbType.VarChar, EmployeeID);
            db.AddInParameter(dbCommand, "@CreateDate", SqlDbType.DateTime, CreateDate);
            db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteInvoice(string iD)
        {
            Invoice entity = new Invoice();
            entity.ID = iD;
            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_Invoice_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_CustomerID(string customerID)
        {
            string spName = "[dbo].[p_LICENSE_Invoice_DeleteBy_CustomerID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@CustomerID", SqlDbType.VarChar, customerID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_EmployeeID(string employeeID)
        {
            string spName = "[dbo].[p_LICENSE_Invoice_DeleteBy_EmployeeID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@EmployeeID", SqlDbType.VarChar, employeeID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        #endregion
    }
}