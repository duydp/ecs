using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.Report.VNACCS
{
    public partial class QuyetDinhThongQuanHangHoaXuatKhau_2 : DevExpress.XtraReports.UI.XtraReport
    {
        public QuyetDinhThongQuanHangHoaXuatKhau_2()
        {
            InitializeComponent();
        }

        public void BindReport(VAE1LF0 vae1lf)
        {
            //lblSoTrang.Text = "2/" + vae1lf.K69.GetValue().ToString();
            int HangHoa = vae1lf.HangMD.Count;
            if (HangHoa <= 2)
            {
                lblSoTrang.Text = "2/" + System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblSoTrang.Text = "2/" + System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 2, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblSotokhai.Text = vae1lf.ECN.GetValue().ToString();
            lblSotokhaidautien.Text = vae1lf.FIC.GetValue().ToString();
            lblSonhanhtokhaichianho.Text = vae1lf.BNO.GetValue().ToString();
            lblTongsotokhaichianho.Text = vae1lf.DNO.GetValue().ToString();
            lblSotokhaitamnhaptaixuat.Text = vae1lf.TDN.GetValue().ToString();
            lblMaphanloaikiemtra.Text = vae1lf.K07.GetValue().ToString();
            lblMaloaihinh.Text = vae1lf.ECB.GetValue().ToString();
            lblMaphanloaihanghoa.Text = vae1lf.CCC.GetValue().ToString();
            lblMahieuphuongthucvanchuyen.Text = vae1lf.MTC.GetValue().ToString();
            lblMasothuedaidien.Text = vae1lf.K01.GetValue().ToString();
            lblTencoquanhaiquantiepnhantokhai.Text = vae1lf.K08.GetValue().ToString();
            lblNhomxulyhoso.Text = vae1lf.CHB.GetValue().ToString();
            if
            (Convert.ToDateTime(vae1lf.K10.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaydangky.Text = Convert.ToDateTime(vae1lf.K10.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaydangky.Text = "";
            }

            
            if (vae1lf.AD1.GetValue().ToString() != "" && vae1lf.AD1.GetValue().ToString() != "0")
            {
                lblGiodangky.Text = vae1lf.AD1.GetValue().ToString().Substring(0, 2) + ":" + vae1lf.AD1.GetValue().ToString().Substring(2, 2) + ":" + vae1lf.AD1.GetValue().ToString().Substring(4, 2);
            }
            else
            {
                lblGiodangky.Text = "";
            }


            if
            (Convert.ToDateTime(vae1lf.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaythaydoidangky.Text = Convert.ToDateTime(vae1lf.AD2.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaythaydoidangky.Text = "";
            }

            if (vae1lf.AD3.GetValue().ToString() != "" && vae1lf.AD3.GetValue().ToString() != "0")
            {
                lblGiothaydoidangky.Text = vae1lf.AD3.GetValue().ToString().Substring(0, 2) + ":" + vae1lf.AD3.GetValue().ToString().Substring(2, 2) + ":" + vae1lf.AD3.GetValue().ToString().Substring(4, 2);
            }
            else
            {
                lblGiothaydoidangky.Text = "";
            }


            if
            (Convert.ToDateTime(vae1lf.RID.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoihantainhapxuat.Text = Convert.ToDateTime(vae1lf.RID.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoihantainhapxuat.Text = "";
            }

            lblBieuthitruonghophethan.Text = vae1lf.AAA.GetValue().ToString();
            //lblNgaydukienden.Text = Convert.ToDateTime(vae1lf.ADT.GetValue()).ToString("dd/MM/yyyy");

            lblTendiachixephang.Text = vae1lf.VN.GetValue().ToString();
            lblDiachixephang.Text = vae1lf.K72.GetValue().ToString();

            lblPhanloai.Text = vae1lf.CCM.GetValue().ToString();

            for (int i = 0; i < vae1lf.VC_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaxephang1.Text = vae1lf.VC_.listAttribute[0].GetValueCollection(i).ToString();
                        break;
                    case 1:
                        lblMaxephang2.Text = vae1lf.VC_.listAttribute[0].GetValueCollection(i).ToString();
                        break;
                    case 2:
                        lblMaxephang3.Text = vae1lf.VC_.listAttribute[0].GetValueCollection(i).ToString();
                        break;
                    case 3:
                        lblMaxephang4.Text = vae1lf.VC_.listAttribute[0].GetValueCollection(i).ToString();

                        break;
                    case 4:
                        lblMaxephang5.Text = vae1lf.VC_.listAttribute[0].GetValueCollection(i).ToString();
                        break;

                }

            }

            BindReportContainer(vae1lf);
            BindReportChiThiHQ(vae1lf);

        }

        public void BindReportChiThiHQ(VAE1LF0 vae1lf_custome)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Date", typeof(string));
                dt.Columns.Add("Ten", typeof(string));
                dt.Columns.Add("NoiDung", typeof(string));
                dt.Columns.Add("STT", typeof(string));
                int STT = 0;

                for (int i = 0; i < vae1lf_custome.D__.listAttribute[0].ListValue.Count; i++)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    if
                      (Convert.ToDateTime(vae1lf_custome.D__.listAttribute[0].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                    {
                        dr["Date"] = Convert.ToDateTime(vae1lf_custome.D__.listAttribute[0].GetValueCollection(i)).ToString("dd/MM/yyyy");
                    }


                    dr["Ten"] = vae1lf_custome.D__.listAttribute[1].GetValueCollection(i).ToString();
                    dr["STT"] = STT.ToString();
                    dr["NoiDung"] = vae1lf_custome.D__.listAttribute[2].GetValueCollection(i).ToString();
                    dt.Rows.Add(dr);
                }

                while (dt.Rows.Count < 10)
                {

                    STT++;
                    DataRow dr = dt.NewRow();
                    dr["Date"] = "";
                    dr["Ten"] = "";
                    dr["NoiDung"] = "";
                    dr["STT"] = STT.ToString();
                    dt.Rows.Add(dr);
                }
                DetailReport1.DataSource = dt;
                lblNgaychithihaiquan1.DataBindings.Add("Text", DetailReport1.DataSource, "Date");
                lblTenchithihaiquan1.DataBindings.Add("Text", DetailReport1.DataSource, "Ten");
                lblNoidungchithihaiquan1.DataBindings.Add("Text", DetailReport1.DataSource, "NoiDung");
                lblSTTChiThiHQ.DataBindings.Add("Text", DetailReport1.DataSource, "STT");
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        public void BindReportContainer(VAE1LF0 vae1lf_Cont)
        {

            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("STT1", typeof(string));
                dt.Columns.Add("SoCont1", typeof(string));
                dt.Columns.Add("STT2", typeof(string));
                dt.Columns.Add("SoCont2", typeof(string));
                dt.Columns.Add("STT3", typeof(string));
                dt.Columns.Add("SoCont3", typeof(string));
                dt.Columns.Add("STT4", typeof(string));
                dt.Columns.Add("SoCont4", typeof(string));
                dt.Columns.Add("STT5", typeof(string));
                dt.Columns.Add("SoCont5", typeof(string));
                for (int i = 1; i < 11; i++)
                {
                    DataRow dr = dt.NewRow();
                    int r = 1;
                    for (int j = (i - 1) * 5; j < i * 5; j++)
                    {

                        dr["STT" + r] = (j + 1).ToString();
                        dr["SoCont" + r] = vae1lf_Cont.C__.listAttribute[0].GetValueCollection(j).ToString();
                        r = r + 1;

                    }
                    dt.Rows.Add(dr);

                }
               
                DetailReport.DataSource = dt;


                lblSTT1.DataBindings.Add("Text", DetailReport.DataSource, "STT1");
                lblSocontainer1.DataBindings.Add("Text", DetailReport.DataSource, "SoCont1");
                lblSTT2.DataBindings.Add("Text", DetailReport.DataSource, "STT2");
                lblSocontainer2.DataBindings.Add("Text", DetailReport.DataSource, "SoCont2");
                lblSTT3.DataBindings.Add("Text", DetailReport.DataSource, "STT3");
                lblSocontainer3.DataBindings.Add("Text", DetailReport.DataSource, "SoCont3");
                lblSTT4.DataBindings.Add("Text", DetailReport.DataSource, "STT4");
                lblSocontainer4.DataBindings.Add("Text", DetailReport.DataSource, "SoCont4");
                lblSTT5.DataBindings.Add("Text", DetailReport.DataSource, "STT5");
                lblSocontainer5.DataBindings.Add("Text", DetailReport.DataSource, "SoCont5");




            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }




        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0")
                lbl.Text = "";
        }

    }
}
