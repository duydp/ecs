﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{

    public class ErrorInformation
    {
        /// <summary>
        /// Thông tin lỗi định dạng Xml từ hải quan trả về
        /// </summary>        
        [XmlElement("CHECKNODENOTEXIST")]
        public ErrorDescription CheckNoDeNotExist { get; set; }
        /// <summary>
        /// Thông tin bắt buộc phải có
        /// </summary>
        [XmlElement("REQUIRED")]
        public ErrorDescription ReQuired { get; set; }
    }
}
