﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Company.Controls.CustomValidation;
using Janus.Windows.CalendarCombo;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Dock;
using CuaKhauControl=Company.Interface.Controls.CuaKhauControl;
using DonViHaiQuanControl=Company.Interface.Controls.DonViHaiQuanControl;
using LoaiHinhMauDichControl01=Company.Interface.Controls.LoaiHinhMauDichVControl;
using NguyenTeControl=Company.Interface.Controls.NguyenTeControl;
using NuocControl=Company.Interface.Controls.NuocControl;

namespace Company.Interface.SXXK
{
    partial class ToKhaiMauDichDetailForm
    {
        private Panel pnlToKhaiMauDich;
        private UICheckBox chkDiaDiemXepHang;
        private UICheckBox chkVanTaiDon;
        private UICheckBox chkHoaDonThuongMai;
        private UICheckBox chkGiayPhep;
        private UICheckBox chkHopDong;
        private NumericEditBox txtTrongLuong;
        private NumericEditBox txtSoKien;
        private NumericEditBox txtSoContainer40;
        private NumericEditBox txtSoContainer20;
        private EditBox txtTenChuHang;
        private Label label29;
        private Label label31;
        private Label label32;
        private Label label21;
        private Label label22;
        private UIGroupBox grbNguyenTe;
        private Label label9;
        private Label label30;
        private UIGroupBox grbNuocXK;
        private UIGroupBox uiGroupBox6;
        private EditBox txtTenDaiLy;
        private EditBox txtMaDaiLy;
        private Label label7;
        private Label label8;
        private UIGroupBox uiGroupBox5;
        private EditBox txtMaDonViUyThac;
        private Label label5;
        private Label label6;
        private EditBox txtTenDonViUyThac;
        private UIGroupBox grbNguoiXK;
        private EditBox txtTenDonViDoiTac;
        private CalendarCombo ccNgayHHHopDong;
        private EditBox txtSoHopDong;
        private Label label13;
        private Label label14;
        private Label label34;
        private CalendarCombo ccNgayHopDong;
        private UIGroupBox uiGroupBox18;
        private UIGroupBox uiGroupBox16;
        private UIGroupBox grbDiaDiemDoHang;
        private EditBox txtDiaDiemXepHang;
        private CalendarCombo ccNgayVanTaiDon;
        private EditBox txtSoVanTaiDon;
        private Label label19;
        private Label label20;
        private UIGroupBox uiGroupBox11;
        private UIComboBox cbPTVT;
        private Label label17;
        private Label lblSoHieuPTVT;
        private EditBox txtSoHieuPTVT;
        private Label lblNgayDenPTVT;
        private EditBox txtSoHoaDonThuongMai;
        private Label label15;
        private Label label16;
        private CalendarCombo ccNgayHHGiayPhep;
        private EditBox txtSoGiayPhep;
        private Label label11;
        private Label label12;
        private Label label33;
        private CalendarCombo ccNgayGiayPhep;
        private UIGroupBox grbHoaDonThuongMai;
        private UIGroupBox grbHopDong;
        private UIGroupBox grbDiaDiemXepHang;
        private UIGroupBox grbVanTaiDon;
        private UIGroupBox gbGiayPhep;
        private NumericEditBox txtTyGiaTinhThue;
        private NumericEditBox txtTyGiaUSD;
        private DataSet ds;
        private DataTable dtHangMauDich;
        private DataColumn dataColumn1;
        private DataColumn dataColumn2;
        private DataColumn dataColumn3;
        private DataColumn dataColumn4;
        private DataColumn dataColumn5;
        private DataColumn dataColumn6;
        private DataColumn dataColumn7;
        private DataColumn dataColumn8;
        private DataColumn dataColumn9;
        private DataTable dtLoaiHinhMauDich;
        private DataColumn dataColumn10;
        private DataColumn dataColumn11;
        private DataTable dtPTTT;
        private DataColumn dataColumn12;
        private DataColumn dataColumn13;
        private DataTable dtCuaKhau;
        private DataColumn dataColumn14;
        private DataColumn dataColumn15;
        private DataColumn dataColumn16;
        private DataTable dtNguyenTe;
        private DataColumn dataColumn17;
        private DataColumn dataColumn18;
        private DataTable dtCompanyNuoc;
        private DataColumn dataColumn19;
        private DataColumn dataColumn20;
        private ErrorProvider epError;
        private ContainerValidator cvError;
        private ListValidationSummary lvsError;
        private RequiredFieldValidator rfvSoHieuPTVT;
        private Label label1;
        private Label label2;
        private NumericEditBox txtLePhiHQ;
        private NumericEditBox txtPhiVanChuyen;
        private NumericEditBox txtPhiBaoHiem;
        private Label label4;
        private UIGroupBox uiGroupBox2;
        private DataTable dtDonViHaiQuan;
        private DataColumn dataColumn21;
        private DataColumn dataColumn22;
        private Label label3;
        private UIGroupBox uiGroupBox1;
        private UIPanelManager uiPanelManager1;
        private UIPanelGroup uiPanel0;
        private UIPanel uiPanel1;
        private UIPanelInnerContainer uiPanel1Container;
        private Label label23;
        private NumericEditBox txtSoLuongPLTK;
        private UIGroupBox grbNguoiNK;
        private EditBox txtTenDonVi;
        private EditBox txtMaDonVi;
        private CalendarCombo ccNgayDen;
        private CalendarCombo ccNgayHDTM;
        private RequiredFieldValidator rfvNgayDen;
        private RequiredFieldValidator rfvNguoiXuatKhau;
        private RangeValidator rvTyGiaTT;
        private RangeValidator rvTyGiaUSD;
        private IContainer components;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiMauDichDetailForm));
            this.pnlToKhaiMauDich = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.txtTrongLuongTinh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtPhiKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtchungtu = new Janus.Windows.EditControls.UIGroupBox();
            this.txtChungTuTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrLoaiHinhMauDich = new Company.Interface.Controls.LoaiHinhMauDichVControl();
            this.grpLoaiHangHoa = new Janus.Windows.EditControls.UIGroupBox();
            this.radTB = new Janus.Windows.EditControls.UIRadioButton();
            this.radNPL = new Janus.Windows.EditControls.UIRadioButton();
            this.radSP = new Janus.Windows.EditControls.UIRadioButton();
            this.chkDiaDiemXepHang = new Janus.Windows.EditControls.UICheckBox();
            this.grbNguoiNK = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhiBaoHiem = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.grbDiaDiemXepHang = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaDiemXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPhiVanChuyen = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLePhiHQ = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkHopDong = new Janus.Windows.EditControls.UICheckBox();
            this.chkVanTaiDon = new Janus.Windows.EditControls.UICheckBox();
            this.chkHoaDonThuongMai = new Janus.Windows.EditControls.UICheckBox();
            this.chkGiayPhep = new Janus.Windows.EditControls.UICheckBox();
            this.txtTrongLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoKien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoContainer40 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoContainer20 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTenChuHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.grbNguyenTe = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNguyenTe = new Company.Interface.Controls.NguyenTeControl();
            this.txtTyGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtTyGiaUSD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.grbNuocXK = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNuocXuatKhau = new Company.Interface.Controls.NuocControl();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDaiLy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDaiLy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grbNguoiXK = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDonViDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grbHopDong = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayHHHopDong = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.ccNgayHopDong = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.uiGroupBox18 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbPTTT = new Janus.Windows.EditControls.UIComboBox();
            this.uiGroupBox16 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbDKGH = new Janus.Windows.EditControls.UIComboBox();
            this.grbDiaDiemDoHang = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrCuaKhau = new Company.Interface.Controls.CuaKhauControl();
            this.grbVanTaiDon = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayVanTaiDon = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoVanTaiDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayDen = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.cbPTVT = new Janus.Windows.EditControls.UIComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lblSoHieuPTVT = new System.Windows.Forms.Label();
            this.txtSoHieuPTVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblNgayDenPTVT = new System.Windows.Forms.Label();
            this.grbHoaDonThuongMai = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayHDTM = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoHoaDonThuongMai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.gbGiayPhep = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayHHGiayPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.ccNgayGiayPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ds = new System.Data.DataSet();
            this.dtHangMauDich = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dtLoaiHinhMauDich = new System.Data.DataTable();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dtPTTT = new System.Data.DataTable();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dtCuaKhau = new System.Data.DataTable();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dtNguyenTe = new System.Data.DataTable();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dtCompanyNuoc = new System.Data.DataTable();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dtDonViHaiQuan = new System.Data.DataTable();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvSoHieuPTVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.cboDateNgayHT = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label23 = new System.Windows.Forms.Label();
            this.cbNgayThucNX = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSotk = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.cbNgayDangky = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label18 = new System.Windows.Forms.Label();
            this.lblSoTiepNhan = new System.Windows.Forms.Label();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.ctrDonViHaiQuan = new Company.Interface.Controls.DonViHaiQuanControl();
            this.txtSoLuongPLTK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.rfvNgayDen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNguoiXuatKhau = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rvTyGiaTT = new Company.Controls.CustomValidation.RangeValidator();
            this.rvTyGiaUSD = new Company.Controls.CustomValidation.RangeValidator();
            this.ilLarge = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.ThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("ThemHang");
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdInCV1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInCV");
            this.cmdPhanBo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanBo");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.ThemHang = new Janus.Windows.UI.CommandBars.UICommand("ThemHang");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdPhanBo = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanBo");
            this.cmdInCV = new Janus.Windows.UI.CommandBars.UICommand("cmdInCV");
            this.cmdCVXNK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCVXNK");
            this.cmdGiayDeNghi1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayDeNghi");
            this.cmdCamKet1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCamKet");
            this.cmdKiemTraHo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKiemTraHo");
            this.cmdChuyenCK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChuyenCK");
            this.cmdCVXNK = new Janus.Windows.UI.CommandBars.UICommand("cmdCVXNK");
            this.cmdGiayDeNghi = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayDeNghi");
            this.cmdCamKet = new Janus.Windows.UI.CommandBars.UICommand("cmdCamKet");
            this.cmdKiemTraHo = new Janus.Windows.UI.CommandBars.UICommand("cmdKiemTraHo");
            this.cmdChuyenCK = new Janus.Windows.UI.CommandBars.UICommand("cmdChuyenCK");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.pnlToKhaiMauDich.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtchungtu)).BeginInit();
            this.txtchungtu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpLoaiHangHoa)).BeginInit();
            this.grpLoaiHangHoa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiNK)).BeginInit();
            this.grbNguoiNK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemXepHang)).BeginInit();
            this.grbDiaDiemXepHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguyenTe)).BeginInit();
            this.grbNguyenTe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNuocXK)).BeginInit();
            this.grbNuocXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiXK)).BeginInit();
            this.grbNguoiXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDong)).BeginInit();
            this.grbHopDong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).BeginInit();
            this.uiGroupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).BeginInit();
            this.uiGroupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemDoHang)).BeginInit();
            this.grbDiaDiemDoHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbVanTaiDon)).BeginInit();
            this.grbVanTaiDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHoaDonThuongMai)).BeginInit();
            this.grbHoaDonThuongMai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbGiayPhep)).BeginInit();
            this.gbGiayPhep.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHangMauDich)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiHinhMauDich)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPTTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCuaKhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompanyNuoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieuPTVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayDen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiXuatKhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaUSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(3, 35);
            this.grbMain.Size = new System.Drawing.Size(1138, 680);
            // 
            // pnlToKhaiMauDich
            // 
            this.pnlToKhaiMauDich.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlToKhaiMauDich.AutoScroll = true;
            this.pnlToKhaiMauDich.AutoScrollMargin = new System.Drawing.Size(4, 4);
            this.pnlToKhaiMauDich.BackColor = System.Drawing.Color.Transparent;
            this.pnlToKhaiMauDich.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlToKhaiMauDich.Controls.Add(this.label25);
            this.pnlToKhaiMauDich.Controls.Add(this.txtTrongLuongTinh);
            this.pnlToKhaiMauDich.Controls.Add(this.txtPhiKhac);
            this.pnlToKhaiMauDich.Controls.Add(this.label24);
            this.pnlToKhaiMauDich.Controls.Add(this.txtchungtu);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox7);
            this.pnlToKhaiMauDich.Controls.Add(this.grpLoaiHangHoa);
            this.pnlToKhaiMauDich.Controls.Add(this.chkDiaDiemXepHang);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNguoiNK);
            this.pnlToKhaiMauDich.Controls.Add(this.txtPhiBaoHiem);
            this.pnlToKhaiMauDich.Controls.Add(this.grbDiaDiemXepHang);
            this.pnlToKhaiMauDich.Controls.Add(this.label4);
            this.pnlToKhaiMauDich.Controls.Add(this.txtPhiVanChuyen);
            this.pnlToKhaiMauDich.Controls.Add(this.label2);
            this.pnlToKhaiMauDich.Controls.Add(this.txtLePhiHQ);
            this.pnlToKhaiMauDich.Controls.Add(this.label1);
            this.pnlToKhaiMauDich.Controls.Add(this.chkHopDong);
            this.pnlToKhaiMauDich.Controls.Add(this.chkVanTaiDon);
            this.pnlToKhaiMauDich.Controls.Add(this.chkHoaDonThuongMai);
            this.pnlToKhaiMauDich.Controls.Add(this.chkGiayPhep);
            this.pnlToKhaiMauDich.Controls.Add(this.txtTrongLuong);
            this.pnlToKhaiMauDich.Controls.Add(this.txtSoKien);
            this.pnlToKhaiMauDich.Controls.Add(this.txtSoContainer40);
            this.pnlToKhaiMauDich.Controls.Add(this.txtSoContainer20);
            this.pnlToKhaiMauDich.Controls.Add(this.txtTenChuHang);
            this.pnlToKhaiMauDich.Controls.Add(this.label29);
            this.pnlToKhaiMauDich.Controls.Add(this.label31);
            this.pnlToKhaiMauDich.Controls.Add(this.label32);
            this.pnlToKhaiMauDich.Controls.Add(this.label21);
            this.pnlToKhaiMauDich.Controls.Add(this.label22);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNguyenTe);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNuocXK);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox6);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox5);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNguoiXK);
            this.pnlToKhaiMauDich.Controls.Add(this.grbHopDong);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox18);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox16);
            this.pnlToKhaiMauDich.Controls.Add(this.grbDiaDiemDoHang);
            this.pnlToKhaiMauDich.Controls.Add(this.grbVanTaiDon);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox11);
            this.pnlToKhaiMauDich.Controls.Add(this.grbHoaDonThuongMai);
            this.pnlToKhaiMauDich.Controls.Add(this.gbGiayPhep);
            this.pnlToKhaiMauDich.Location = new System.Drawing.Point(-1, 83);
            this.pnlToKhaiMauDich.Name = "pnlToKhaiMauDich";
            this.pnlToKhaiMauDich.Size = new System.Drawing.Size(1138, 597);
            this.pnlToKhaiMauDich.TabIndex = 6;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(769, 452);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(86, 13);
            this.label25.TabIndex = 45;
            this.label25.Text = "Trọng lượng tịnh";
            // 
            // txtTrongLuongTinh
            // 
            this.txtTrongLuongTinh.DecimalDigits = 2;
            this.txtTrongLuongTinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongLuongTinh.Location = new System.Drawing.Point(773, 468);
            this.txtTrongLuongTinh.Name = "txtTrongLuongTinh";
            this.txtTrongLuongTinh.Size = new System.Drawing.Size(95, 21);
            this.txtTrongLuongTinh.TabIndex = 31;
            this.txtTrongLuongTinh.Text = "0.00";
            this.txtTrongLuongTinh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTrongLuongTinh.Value = 0;
            this.txtTrongLuongTinh.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTrongLuongTinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTrongLuongTinh.VisualStyleManager = this.vsmMain;
            // 
            // txtPhiKhac
            // 
            this.txtPhiKhac.DecimalDigits = 2;
            this.txtPhiKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhiKhac.Location = new System.Drawing.Point(666, 516);
            this.txtPhiKhac.Name = "txtPhiKhac";
            this.txtPhiKhac.Size = new System.Drawing.Size(96, 21);
            this.txtPhiKhac.TabIndex = 30;
            this.txtPhiKhac.Text = "0.00";
            this.txtPhiKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtPhiKhac.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtPhiKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtPhiKhac.VisualStyleManager = this.vsmMain;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(666, 500);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(46, 13);
            this.label24.TabIndex = 40;
            this.label24.Text = "Phí khác";
            // 
            // txtchungtu
            // 
            this.txtchungtu.Controls.Add(this.txtChungTuTK);
            this.txtchungtu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtchungtu.Location = new System.Drawing.Point(856, 332);
            this.txtchungtu.Name = "txtchungtu";
            this.txtchungtu.Size = new System.Drawing.Size(271, 104);
            this.txtchungtu.TabIndex = 17;
            this.txtchungtu.Text = "Chứng từ kèm theo";
            this.txtchungtu.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtChungTuTK
            // 
            this.txtChungTuTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChungTuTK.Location = new System.Drawing.Point(8, 24);
            this.txtChungTuTK.Multiline = true;
            this.txtChungTuTK.Name = "txtChungTuTK";
            this.txtChungTuTK.ReadOnly = true;
            this.txtChungTuTK.Size = new System.Drawing.Size(246, 71);
            this.txtChungTuTK.TabIndex = 0;
            this.txtChungTuTK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtChungTuTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtChungTuTK.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.Controls.Add(this.ctrLoaiHinhMauDich);
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.Location = new System.Drawing.Point(310, 8);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(249, 104);
            this.uiGroupBox7.TabIndex = 1;
            this.uiGroupBox7.Text = "Loại hình mậu dịch";
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ctrLoaiHinhMauDich
            // 
            this.ctrLoaiHinhMauDich.BackColor = System.Drawing.Color.Transparent;
            this.ctrLoaiHinhMauDich.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrLoaiHinhMauDich.Location = new System.Drawing.Point(7, 24);
            this.ctrLoaiHinhMauDich.Ma = "";
            this.ctrLoaiHinhMauDich.Name = "ctrLoaiHinhMauDich";
            this.ctrLoaiHinhMauDich.Nhom = "";
            this.ctrLoaiHinhMauDich.ReadOnly = false;
            this.ctrLoaiHinhMauDich.Size = new System.Drawing.Size(230, 50);
            this.ctrLoaiHinhMauDich.TabIndex = 0;
            this.ctrLoaiHinhMauDich.VisualStyleManager = this.vsmMain;
            // 
            // grpLoaiHangHoa
            // 
            this.grpLoaiHangHoa.AutoScroll = true;
            this.grpLoaiHangHoa.BorderColor = System.Drawing.Color.Transparent;
            this.grpLoaiHangHoa.Controls.Add(this.radTB);
            this.grpLoaiHangHoa.Controls.Add(this.radNPL);
            this.grpLoaiHangHoa.Controls.Add(this.radSP);
            this.grpLoaiHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpLoaiHangHoa.Location = new System.Drawing.Point(12, 541);
            this.grpLoaiHangHoa.Name = "grpLoaiHangHoa";
            this.grpLoaiHangHoa.Size = new System.Drawing.Size(508, 33);
            this.grpLoaiHangHoa.TabIndex = 32;
            this.grpLoaiHangHoa.VisualStyleManager = this.vsmMain;
            // 
            // radTB
            // 
            this.radTB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTB.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.radTB.Location = new System.Drawing.Point(260, 9);
            this.radTB.Name = "radTB";
            this.radTB.Size = new System.Drawing.Size(175, 23);
            this.radTB.TabIndex = 2;
            this.radTB.Text = "Thiết bị";
            this.radTB.VisualStyleManager = this.vsmMain;
            // 
            // radNPL
            // 
            this.radNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radNPL.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.radNPL.Location = new System.Drawing.Point(118, 9);
            this.radNPL.Name = "radNPL";
            this.radNPL.Size = new System.Drawing.Size(124, 23);
            this.radNPL.TabIndex = 1;
            this.radNPL.Text = "Nguyên phụ liệu";
            this.radNPL.VisualStyleManager = this.vsmMain;
            // 
            // radSP
            // 
            this.radSP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.radSP.Checked = true;
            this.radSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radSP.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.radSP.Location = new System.Drawing.Point(23, 10);
            this.radSP.Name = "radSP";
            this.radSP.Size = new System.Drawing.Size(104, 23);
            this.radSP.TabIndex = 0;
            this.radSP.TabStop = true;
            this.radSP.Text = "Sản phẩm";
            this.radSP.VisualStyleManager = this.vsmMain;
            // 
            // chkDiaDiemXepHang
            // 
            this.chkDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDiaDiemXepHang.Location = new System.Drawing.Point(957, 238);
            this.chkDiaDiemXepHang.Name = "chkDiaDiemXepHang";
            this.chkDiaDiemXepHang.Size = new System.Drawing.Size(17, 18);
            this.chkDiaDiemXepHang.TabIndex = 36;
            this.chkDiaDiemXepHang.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.chkDiaDiemXepHang.VisualStyleManager = this.vsmMain;
            this.chkDiaDiemXepHang.CheckedChanged += new System.EventHandler(this.chkDiaDiemXepHang_CheckedChanged);
            // 
            // grbNguoiNK
            // 
            this.grbNguoiNK.Controls.Add(this.txtTenDonVi);
            this.grbNguoiNK.Controls.Add(this.txtMaDonVi);
            this.grbNguoiNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguoiNK.Location = new System.Drawing.Point(7, 8);
            this.grbNguoiNK.Name = "grbNguoiNK";
            this.grbNguoiNK.Size = new System.Drawing.Size(297, 104);
            this.grbNguoiNK.TabIndex = 0;
            this.grbNguoiNK.Text = "Người nhập khẩu";
            this.grbNguoiNK.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtTenDonVi
            // 
            this.txtTenDonVi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTenDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonVi.Location = new System.Drawing.Point(8, 48);
            this.txtTenDonVi.Multiline = true;
            this.txtTenDonVi.Name = "txtTenDonVi";
            this.txtTenDonVi.ReadOnly = true;
            this.txtTenDonVi.Size = new System.Drawing.Size(275, 48);
            this.txtTenDonVi.TabIndex = 1;
            this.txtTenDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDonVi.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDonVi
            // 
            this.txtMaDonVi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtMaDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonVi.Location = new System.Drawing.Point(8, 20);
            this.txtMaDonVi.Name = "txtMaDonVi";
            this.txtMaDonVi.ReadOnly = true;
            this.txtMaDonVi.Size = new System.Drawing.Size(275, 21);
            this.txtMaDonVi.TabIndex = 0;
            this.txtMaDonVi.Text = "0400100457";
            this.txtMaDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaDonVi.VisualStyleManager = this.vsmMain;
            // 
            // txtPhiBaoHiem
            // 
            this.txtPhiBaoHiem.DecimalDigits = 2;
            this.txtPhiBaoHiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhiBaoHiem.Location = new System.Drawing.Point(424, 514);
            this.txtPhiBaoHiem.Name = "txtPhiBaoHiem";
            this.txtPhiBaoHiem.Size = new System.Drawing.Size(96, 21);
            this.txtPhiBaoHiem.TabIndex = 26;
            this.txtPhiBaoHiem.Text = "0.00";
            this.txtPhiBaoHiem.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtPhiBaoHiem.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtPhiBaoHiem.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtPhiBaoHiem.VisualStyleManager = this.vsmMain;
            // 
            // grbDiaDiemXepHang
            // 
            this.grbDiaDiemXepHang.Controls.Add(this.txtDiaDiemXepHang);
            this.grbDiaDiemXepHang.Enabled = false;
            this.grbDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDiaDiemXepHang.Location = new System.Drawing.Point(852, 240);
            this.grbDiaDiemXepHang.Name = "grbDiaDiemXepHang";
            this.grbDiaDiemXepHang.Size = new System.Drawing.Size(271, 80);
            this.grbDiaDiemXepHang.TabIndex = 12;
            this.grbDiaDiemXepHang.Text = "Địa điểm xếp hàng";
            this.grbDiaDiemXepHang.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtDiaDiemXepHang
            // 
            this.txtDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiemXepHang.Location = new System.Drawing.Point(8, 24);
            this.txtDiaDiemXepHang.Multiline = true;
            this.txtDiaDiemXepHang.Name = "txtDiaDiemXepHang";
            this.txtDiaDiemXepHang.Size = new System.Drawing.Size(250, 48);
            this.txtDiaDiemXepHang.TabIndex = 0;
            this.txtDiaDiemXepHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaDiemXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaDiemXepHang.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(424, 498);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Phí bảo hiểm";
            // 
            // txtPhiVanChuyen
            // 
            this.txtPhiVanChuyen.DecimalDigits = 2;
            this.txtPhiVanChuyen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhiVanChuyen.Location = new System.Drawing.Point(564, 516);
            this.txtPhiVanChuyen.Name = "txtPhiVanChuyen";
            this.txtPhiVanChuyen.Size = new System.Drawing.Size(92, 21);
            this.txtPhiVanChuyen.TabIndex = 28;
            this.txtPhiVanChuyen.Text = "0.00";
            this.txtPhiVanChuyen.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtPhiVanChuyen.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtPhiVanChuyen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtPhiVanChuyen.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(564, 500);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "Phí vận chuyển";
            // 
            // txtLePhiHQ
            // 
            this.txtLePhiHQ.DecimalDigits = 2;
            this.txtLePhiHQ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLePhiHQ.Location = new System.Drawing.Point(317, 514);
            this.txtLePhiHQ.Name = "txtLePhiHQ";
            this.txtLePhiHQ.Size = new System.Drawing.Size(96, 21);
            this.txtLePhiHQ.TabIndex = 23;
            this.txtLePhiHQ.Text = "0.00";
            this.txtLePhiHQ.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtLePhiHQ.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtLePhiHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtLePhiHQ.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(317, 498);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Lệ phí HQ";
            // 
            // chkHopDong
            // 
            this.chkHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHopDong.Location = new System.Drawing.Point(913, 5);
            this.chkHopDong.Name = "chkHopDong";
            this.chkHopDong.Size = new System.Drawing.Size(17, 18);
            this.chkHopDong.TabIndex = 36;
            this.chkHopDong.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.chkHopDong.VisualStyleManager = this.vsmMain;
            this.chkHopDong.CheckedChanged += new System.EventHandler(this.chkHopDong_CheckedChanged);
            // 
            // chkVanTaiDon
            // 
            this.chkVanTaiDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkVanTaiDon.Location = new System.Drawing.Point(921, 120);
            this.chkVanTaiDon.Name = "chkVanTaiDon";
            this.chkVanTaiDon.Size = new System.Drawing.Size(17, 18);
            this.chkVanTaiDon.TabIndex = 36;
            this.chkVanTaiDon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.chkVanTaiDon.VisualStyleManager = this.vsmMain;
            this.chkVanTaiDon.CheckedChanged += new System.EventHandler(this.chkVanTaiDon_CheckedChanged);
            // 
            // chkHoaDonThuongMai
            // 
            this.chkHoaDonThuongMai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkHoaDonThuongMai.Location = new System.Drawing.Point(678, 121);
            this.chkHoaDonThuongMai.Name = "chkHoaDonThuongMai";
            this.chkHoaDonThuongMai.Size = new System.Drawing.Size(17, 18);
            this.chkHoaDonThuongMai.TabIndex = 36;
            this.chkHoaDonThuongMai.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.chkHoaDonThuongMai.VisualStyleManager = this.vsmMain;
            this.chkHoaDonThuongMai.CheckedChanged += new System.EventHandler(this.chkHoaDonThuongMai_CheckedChanged);
            // 
            // chkGiayPhep
            // 
            this.chkGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkGiayPhep.Location = new System.Drawing.Point(628, 5);
            this.chkGiayPhep.Name = "chkGiayPhep";
            this.chkGiayPhep.Size = new System.Drawing.Size(17, 18);
            this.chkGiayPhep.TabIndex = 36;
            this.chkGiayPhep.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.chkGiayPhep.VisualStyleManager = this.vsmMain;
            this.chkGiayPhep.CheckedChanged += new System.EventHandler(this.chkGiayPhep_CheckedChanged);
            // 
            // txtTrongLuong
            // 
            this.txtTrongLuong.DecimalDigits = 2;
            this.txtTrongLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongLuong.Location = new System.Drawing.Point(666, 468);
            this.txtTrongLuong.Name = "txtTrongLuong";
            this.txtTrongLuong.Size = new System.Drawing.Size(96, 21);
            this.txtTrongLuong.TabIndex = 29;
            this.txtTrongLuong.Text = "0.00";
            this.txtTrongLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTrongLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtTrongLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTrongLuong.VisualStyleManager = this.vsmMain;
            // 
            // txtSoKien
            // 
            this.txtSoKien.DecimalDigits = 0;
            this.txtSoKien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoKien.Location = new System.Drawing.Point(564, 468);
            this.txtSoKien.Name = "txtSoKien";
            this.txtSoKien.Size = new System.Drawing.Size(92, 21);
            this.txtSoKien.TabIndex = 27;
            this.txtSoKien.Text = "0";
            this.txtSoKien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoKien.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoKien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoKien.VisualStyleManager = this.vsmMain;
            // 
            // txtSoContainer40
            // 
            this.txtSoContainer40.DecimalDigits = 2;
            this.txtSoContainer40.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoContainer40.Location = new System.Drawing.Point(424, 466);
            this.txtSoContainer40.Name = "txtSoContainer40";
            this.txtSoContainer40.Size = new System.Drawing.Size(96, 21);
            this.txtSoContainer40.TabIndex = 24;
            this.txtSoContainer40.Text = "0.00";
            this.txtSoContainer40.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoContainer40.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtSoContainer40.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoContainer40.VisualStyleManager = this.vsmMain;
            // 
            // txtSoContainer20
            // 
            this.txtSoContainer20.DecimalDigits = 2;
            this.txtSoContainer20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoContainer20.Location = new System.Drawing.Point(317, 466);
            this.txtSoContainer20.Name = "txtSoContainer20";
            this.txtSoContainer20.Size = new System.Drawing.Size(96, 21);
            this.txtSoContainer20.TabIndex = 21;
            this.txtSoContainer20.Text = "0.00";
            this.txtSoContainer20.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoContainer20.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtSoContainer20.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoContainer20.VisualStyleManager = this.vsmMain;
            // 
            // txtTenChuHang
            // 
            this.txtTenChuHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenChuHang.Location = new System.Drawing.Point(12, 468);
            this.txtTenChuHang.Multiline = true;
            this.txtTenChuHang.Name = "txtTenChuHang";
            this.txtTenChuHang.Size = new System.Drawing.Size(278, 70);
            this.txtTenChuHang.TabIndex = 19;
            this.txtTenChuHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenChuHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenChuHang.VisualStyleManager = this.vsmMain;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(12, 452);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(72, 13);
            this.label29.TabIndex = 18;
            this.label29.Text = "Tên chủ hàng";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(317, 450);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(82, 13);
            this.label31.TabIndex = 20;
            this.label31.Text = "Số container 20";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(424, 450);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(82, 13);
            this.label32.TabIndex = 24;
            this.label32.Text = "Số container 40";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(564, 452);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 13);
            this.label21.TabIndex = 26;
            this.label21.Text = "Số kiện hàng";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(666, 452);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(88, 13);
            this.label22.TabIndex = 28;
            this.label22.Text = "Trọng lượng (Kg)";
            // 
            // grbNguyenTe
            // 
            this.grbNguyenTe.Controls.Add(this.ctrNguyenTe);
            this.grbNguyenTe.Controls.Add(this.txtTyGiaTinhThue);
            this.grbNguyenTe.Controls.Add(this.label9);
            this.grbNguyenTe.Controls.Add(this.label30);
            this.grbNguyenTe.Controls.Add(this.txtTyGiaUSD);
            this.grbNguyenTe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguyenTe.Location = new System.Drawing.Point(565, 332);
            this.grbNguyenTe.Name = "grbNguyenTe";
            this.grbNguyenTe.Size = new System.Drawing.Size(281, 104);
            this.grbNguyenTe.TabIndex = 16;
            this.grbNguyenTe.Text = "Đồng tiền thanh toán";
            this.grbNguyenTe.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ctrNguyenTe
            // 
            this.ctrNguyenTe.BackColor = System.Drawing.Color.Transparent;
            this.ctrNguyenTe.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.ctrNguyenTe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNguyenTe.Location = new System.Drawing.Point(8, 20);
            this.ctrNguyenTe.Ma = "USD";
            this.ctrNguyenTe.Name = "ctrNguyenTe";
            this.ctrNguyenTe.ReadOnly = false;
            this.ctrNguyenTe.Size = new System.Drawing.Size(267, 22);
            this.ctrNguyenTe.TabIndex = 0;
            this.ctrNguyenTe.VisualStyleManager = this.vsmMain;
            // 
            // txtTyGiaTinhThue
            // 
            this.txtTyGiaTinhThue.DecimalDigits = 3;
            this.txtTyGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGiaTinhThue.Location = new System.Drawing.Point(80, 47);
            this.txtTyGiaTinhThue.Name = "txtTyGiaTinhThue";
            this.txtTyGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyGiaTinhThue.Size = new System.Drawing.Size(81, 21);
            this.txtTyGiaTinhThue.TabIndex = 1;
            this.txtTyGiaTinhThue.Text = "0.000";
            this.txtTyGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTyGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTyGiaTinhThue.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Tỷ giá USD";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(8, 55);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(51, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "Tỷ giá TT";
            // 
            // txtTyGiaUSD
            // 
            this.txtTyGiaUSD.DecimalDigits = 0;
            this.txtTyGiaUSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGiaUSD.Location = new System.Drawing.Point(80, 74);
            this.txtTyGiaUSD.Name = "txtTyGiaUSD";
            this.txtTyGiaUSD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyGiaUSD.Size = new System.Drawing.Size(81, 21);
            this.txtTyGiaUSD.TabIndex = 2;
            this.txtTyGiaUSD.Text = "0";
            this.txtTyGiaUSD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyGiaUSD.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTyGiaUSD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTyGiaUSD.VisualStyleManager = this.vsmMain;
            // 
            // grbNuocXK
            // 
            this.grbNuocXK.Controls.Add(this.ctrNuocXuatKhau);
            this.grbNuocXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNuocXK.Location = new System.Drawing.Point(310, 240);
            this.grbNuocXK.Name = "grbNuocXK";
            this.grbNuocXK.Size = new System.Drawing.Size(249, 80);
            this.grbNuocXK.TabIndex = 10;
            this.grbNuocXK.Text = "Nước xuất khẩu";
            this.grbNuocXK.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ctrNuocXuatKhau
            // 
            this.ctrNuocXuatKhau.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocXuatKhau.ErrorMessage = "\"Nước xuất khẩu\" bắt buộc phải chọn.";
            this.ctrNuocXuatKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocXuatKhau.Location = new System.Drawing.Point(7, 24);
            this.ctrNuocXuatKhau.Ma = "JP";
            this.ctrNuocXuatKhau.Name = "ctrNuocXuatKhau";
            this.ctrNuocXuatKhau.ReadOnly = false;
            this.ctrNuocXuatKhau.Size = new System.Drawing.Size(230, 50);
            this.ctrNuocXuatKhau.TabIndex = 0;
            this.ctrNuocXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Controls.Add(this.txtTenDaiLy);
            this.uiGroupBox6.Controls.Add(this.txtMaDaiLy);
            this.uiGroupBox6.Controls.Add(this.label7);
            this.uiGroupBox6.Controls.Add(this.label8);
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(7, 332);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(297, 104);
            this.uiGroupBox6.TabIndex = 13;
            this.uiGroupBox6.Text = "Đại lý làm TTHQ";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtTenDaiLy
            // 
            this.txtTenDaiLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDaiLy.Location = new System.Drawing.Point(40, 47);
            this.txtTenDaiLy.Multiline = true;
            this.txtTenDaiLy.Name = "txtTenDaiLy";
            this.txtTenDaiLy.Size = new System.Drawing.Size(243, 48);
            this.txtTenDaiLy.TabIndex = 1;
            this.txtTenDaiLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDaiLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDaiLy.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDaiLy
            // 
            this.txtMaDaiLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDaiLy.Location = new System.Drawing.Point(40, 20);
            this.txtMaDaiLy.Name = "txtMaDaiLy";
            this.txtMaDaiLy.Size = new System.Drawing.Size(243, 21);
            this.txtMaDaiLy.TabIndex = 0;
            this.txtMaDaiLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDaiLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaDaiLy.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Mã";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Tên";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.txtMaDonViUyThac);
            this.uiGroupBox5.Controls.Add(this.label5);
            this.uiGroupBox5.Controls.Add(this.label6);
            this.uiGroupBox5.Controls.Add(this.txtTenDonViUyThac);
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(7, 240);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(297, 80);
            this.uiGroupBox5.TabIndex = 9;
            this.uiGroupBox5.Text = "Người ủy thác";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtMaDonViUyThac
            // 
            this.txtMaDonViUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonViUyThac.Location = new System.Drawing.Point(40, 20);
            this.txtMaDonViUyThac.Name = "txtMaDonViUyThac";
            this.txtMaDonViUyThac.Size = new System.Drawing.Size(243, 21);
            this.txtMaDonViUyThac.TabIndex = 0;
            this.txtMaDonViUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonViUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaDonViUyThac.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Mã";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Tên";
            // 
            // txtTenDonViUyThac
            // 
            this.txtTenDonViUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonViUyThac.Location = new System.Drawing.Point(40, 47);
            this.txtTenDonViUyThac.Name = "txtTenDonViUyThac";
            this.txtTenDonViUyThac.Size = new System.Drawing.Size(243, 21);
            this.txtTenDonViUyThac.TabIndex = 1;
            this.txtTenDonViUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonViUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDonViUyThac.VisualStyleManager = this.vsmMain;
            // 
            // grbNguoiXK
            // 
            this.grbNguoiXK.Controls.Add(this.txtTenDonViDoiTac);
            this.grbNguoiXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguoiXK.Location = new System.Drawing.Point(7, 124);
            this.grbNguoiXK.Name = "grbNguoiXK";
            this.grbNguoiXK.Size = new System.Drawing.Size(297, 104);
            this.grbNguoiXK.TabIndex = 5;
            this.grbNguoiXK.Text = "Người xuất khẩu";
            this.grbNguoiXK.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtTenDonViDoiTac
            // 
            this.txtTenDonViDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonViDoiTac.Location = new System.Drawing.Point(8, 20);
            this.txtTenDonViDoiTac.Multiline = true;
            this.txtTenDonViDoiTac.Name = "txtTenDonViDoiTac";
            this.txtTenDonViDoiTac.Size = new System.Drawing.Size(275, 78);
            this.txtTenDonViDoiTac.TabIndex = 0;
            this.txtTenDonViDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonViDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDonViDoiTac.VisualStyleManager = this.vsmMain;
            // 
            // grbHopDong
            // 
            this.grbHopDong.Controls.Add(this.ccNgayHHHopDong);
            this.grbHopDong.Controls.Add(this.txtSoHopDong);
            this.grbHopDong.Controls.Add(this.label13);
            this.grbHopDong.Controls.Add(this.label14);
            this.grbHopDong.Controls.Add(this.label34);
            this.grbHopDong.Controls.Add(this.ccNgayHopDong);
            this.grbHopDong.Enabled = false;
            this.grbHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbHopDong.Location = new System.Drawing.Point(852, 8);
            this.grbHopDong.Name = "grbHopDong";
            this.grbHopDong.Size = new System.Drawing.Size(271, 104);
            this.grbHopDong.TabIndex = 4;
            this.grbHopDong.Text = "Hợp đồng";
            this.grbHopDong.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ccNgayHHHopDong
            // 
            // 
            // 
            // 
            this.ccNgayHHHopDong.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHHHopDong.DropDownCalendar.Name = "";
            this.ccNgayHHHopDong.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHHHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHHHopDong.Location = new System.Drawing.Point(64, 77);
            this.ccNgayHHHopDong.Name = "ccNgayHHHopDong";
            this.ccNgayHHHopDong.Nullable = true;
            this.ccNgayHHHopDong.NullButtonText = "Xóa";
            this.ccNgayHHHopDong.ShowNullButton = true;
            this.ccNgayHHHopDong.Size = new System.Drawing.Size(96, 21);
            this.ccNgayHHHopDong.TabIndex = 2;
            this.ccNgayHHHopDong.TodayButtonText = "Hôm nay";
            this.ccNgayHHHopDong.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHHHopDong.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDong.Location = new System.Drawing.Point(64, 20);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(194, 21);
            this.txtSoHopDong.TabIndex = 0;
            this.txtSoHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHopDong.VisualStyleManager = this.vsmMain;
            this.txtSoHopDong.ButtonClick += new System.EventHandler(this.txtSoHopDong_ButtonClick);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(4, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Số";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(4, 53);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Ngày";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(4, 82);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(49, 13);
            this.label34.TabIndex = 4;
            this.label34.Text = "Ngày HH";
            // 
            // ccNgayHopDong
            // 
            // 
            // 
            // 
            this.ccNgayHopDong.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHopDong.DropDownCalendar.Name = "";
            this.ccNgayHopDong.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHopDong.Location = new System.Drawing.Point(64, 48);
            this.ccNgayHopDong.Name = "ccNgayHopDong";
            this.ccNgayHopDong.Nullable = true;
            this.ccNgayHopDong.NullButtonText = "Xóa";
            this.ccNgayHopDong.ShowNullButton = true;
            this.ccNgayHopDong.Size = new System.Drawing.Size(96, 21);
            this.ccNgayHopDong.TabIndex = 1;
            this.ccNgayHopDong.TodayButtonText = "Hôm nay";
            this.ccNgayHopDong.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHopDong.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox18
            // 
            this.uiGroupBox18.Controls.Add(this.cbPTTT);
            this.uiGroupBox18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox18.Location = new System.Drawing.Point(310, 387);
            this.uiGroupBox18.Name = "uiGroupBox18";
            this.uiGroupBox18.Size = new System.Drawing.Size(249, 49);
            this.uiGroupBox18.TabIndex = 15;
            this.uiGroupBox18.Text = "Phương thức thanh toán";
            this.uiGroupBox18.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // cbPTTT
            // 
            this.cbPTTT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTTT.DisplayMember = "Name";
            this.cbPTTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTTT.Location = new System.Drawing.Point(7, 19);
            this.cbPTTT.Name = "cbPTTT";
            this.cbPTTT.Size = new System.Drawing.Size(230, 21);
            this.cbPTTT.TabIndex = 0;
            this.cbPTTT.ValueMember = "ID";
            this.cbPTTT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbPTTT.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox16
            // 
            this.uiGroupBox16.Controls.Add(this.cbDKGH);
            this.uiGroupBox16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox16.Location = new System.Drawing.Point(310, 332);
            this.uiGroupBox16.Name = "uiGroupBox16";
            this.uiGroupBox16.Size = new System.Drawing.Size(249, 48);
            this.uiGroupBox16.TabIndex = 14;
            this.uiGroupBox16.Text = "Điều kiện giao hàng";
            this.uiGroupBox16.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // cbDKGH
            // 
            this.cbDKGH.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDKGH.DisplayMember = "ID";
            this.cbDKGH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDKGH.Location = new System.Drawing.Point(7, 19);
            this.cbDKGH.Name = "cbDKGH";
            this.cbDKGH.Size = new System.Drawing.Size(230, 21);
            this.cbDKGH.TabIndex = 0;
            this.cbDKGH.ValueMember = "ID";
            this.cbDKGH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbDKGH.VisualStyleManager = this.vsmMain;
            // 
            // grbDiaDiemDoHang
            // 
            this.grbDiaDiemDoHang.Controls.Add(this.ctrCuaKhau);
            this.grbDiaDiemDoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDiaDiemDoHang.Location = new System.Drawing.Point(565, 240);
            this.grbDiaDiemDoHang.Name = "grbDiaDiemDoHang";
            this.grbDiaDiemDoHang.Size = new System.Drawing.Size(281, 80);
            this.grbDiaDiemDoHang.TabIndex = 11;
            this.grbDiaDiemDoHang.Text = "Địa điểm dỡ hàng";
            this.grbDiaDiemDoHang.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ctrCuaKhau
            // 
            this.ctrCuaKhau.BackColor = System.Drawing.Color.Transparent;
            this.ctrCuaKhau.ErrorMessage = "\"Địa điểm dỡ hàng\" bắt buộc phải chọn.";
            this.ctrCuaKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrCuaKhau.Location = new System.Drawing.Point(8, 24);
            this.ctrCuaKhau.Ma = "A006";
            this.ctrCuaKhau.Name = "ctrCuaKhau";
            this.ctrCuaKhau.ReadOnly = false;
            this.ctrCuaKhau.Size = new System.Drawing.Size(267, 50);
            this.ctrCuaKhau.TabIndex = 0;
            this.ctrCuaKhau.VisualStyleManager = this.vsmMain;
            // 
            // grbVanTaiDon
            // 
            this.grbVanTaiDon.Controls.Add(this.ccNgayVanTaiDon);
            this.grbVanTaiDon.Controls.Add(this.txtSoVanTaiDon);
            this.grbVanTaiDon.Controls.Add(this.label19);
            this.grbVanTaiDon.Controls.Add(this.label20);
            this.grbVanTaiDon.Enabled = false;
            this.grbVanTaiDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbVanTaiDon.Location = new System.Drawing.Point(852, 124);
            this.grbVanTaiDon.Name = "grbVanTaiDon";
            this.grbVanTaiDon.Size = new System.Drawing.Size(271, 104);
            this.grbVanTaiDon.TabIndex = 8;
            this.grbVanTaiDon.Text = "Vận tải đơn";
            this.grbVanTaiDon.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ccNgayVanTaiDon
            // 
            // 
            // 
            // 
            this.ccNgayVanTaiDon.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayVanTaiDon.DropDownCalendar.Name = "";
            this.ccNgayVanTaiDon.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayVanTaiDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayVanTaiDon.Location = new System.Drawing.Point(64, 47);
            this.ccNgayVanTaiDon.Name = "ccNgayVanTaiDon";
            this.ccNgayVanTaiDon.Nullable = true;
            this.ccNgayVanTaiDon.NullButtonText = "Xóa";
            this.ccNgayVanTaiDon.ShowNullButton = true;
            this.ccNgayVanTaiDon.Size = new System.Drawing.Size(96, 21);
            this.ccNgayVanTaiDon.TabIndex = 1;
            this.ccNgayVanTaiDon.TodayButtonText = "Hôm nay";
            this.ccNgayVanTaiDon.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayVanTaiDon.VisualStyleManager = this.vsmMain;
            // 
            // txtSoVanTaiDon
            // 
            this.txtSoVanTaiDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanTaiDon.Location = new System.Drawing.Point(64, 20);
            this.txtSoVanTaiDon.Name = "txtSoVanTaiDon";
            this.txtSoVanTaiDon.Size = new System.Drawing.Size(194, 21);
            this.txtSoVanTaiDon.TabIndex = 0;
            this.txtSoVanTaiDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanTaiDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoVanTaiDon.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(8, 28);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Số";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(8, 55);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Ngày";
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.Controls.Add(this.ccNgayDen);
            this.uiGroupBox11.Controls.Add(this.cbPTVT);
            this.uiGroupBox11.Controls.Add(this.label17);
            this.uiGroupBox11.Controls.Add(this.lblSoHieuPTVT);
            this.uiGroupBox11.Controls.Add(this.txtSoHieuPTVT);
            this.uiGroupBox11.Controls.Add(this.lblNgayDenPTVT);
            this.uiGroupBox11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox11.Location = new System.Drawing.Point(310, 124);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(249, 104);
            this.uiGroupBox11.TabIndex = 6;
            this.uiGroupBox11.Text = "Phương tiện vận tải";
            this.uiGroupBox11.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ccNgayDen
            // 
            // 
            // 
            // 
            this.ccNgayDen.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayDen.DropDownCalendar.Name = "";
            this.ccNgayDen.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayDen.Location = new System.Drawing.Point(65, 75);
            this.ccNgayDen.Name = "ccNgayDen";
            this.ccNgayDen.NullButtonText = "Xóa";
            this.ccNgayDen.Size = new System.Drawing.Size(96, 21);
            this.ccNgayDen.TabIndex = 2;
            this.ccNgayDen.TodayButtonText = "Hôm nay";
            this.ccNgayDen.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDen.VisualStyleManager = this.vsmMain;
            // 
            // cbPTVT
            // 
            this.cbPTVT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTVT.DisplayMember = "Ten";
            this.cbPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTVT.Location = new System.Drawing.Point(65, 20);
            this.cbPTVT.Name = "cbPTVT";
            this.cbPTVT.Size = new System.Drawing.Size(172, 21);
            this.cbPTVT.TabIndex = 0;
            this.cbPTVT.ValueMember = "ID";
            this.cbPTVT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbPTVT.VisualStyleManager = this.vsmMain;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(2, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "PTVT";
            // 
            // lblSoHieuPTVT
            // 
            this.lblSoHieuPTVT.AutoSize = true;
            this.lblSoHieuPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHieuPTVT.Location = new System.Drawing.Point(2, 56);
            this.lblSoHieuPTVT.Name = "lblSoHieuPTVT";
            this.lblSoHieuPTVT.Size = new System.Drawing.Size(42, 13);
            this.lblSoHieuPTVT.TabIndex = 2;
            this.lblSoHieuPTVT.Text = "Số hiệu";
            // 
            // txtSoHieuPTVT
            // 
            this.txtSoHieuPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHieuPTVT.Location = new System.Drawing.Point(65, 47);
            this.txtSoHieuPTVT.Name = "txtSoHieuPTVT";
            this.txtSoHieuPTVT.Size = new System.Drawing.Size(172, 21);
            this.txtSoHieuPTVT.TabIndex = 1;
            this.txtSoHieuPTVT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHieuPTVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHieuPTVT.VisualStyleManager = this.vsmMain;
            // 
            // lblNgayDenPTVT
            // 
            this.lblNgayDenPTVT.AutoSize = true;
            this.lblNgayDenPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDenPTVT.Location = new System.Drawing.Point(2, 83);
            this.lblNgayDenPTVT.Name = "lblNgayDenPTVT";
            this.lblNgayDenPTVT.Size = new System.Drawing.Size(53, 13);
            this.lblNgayDenPTVT.TabIndex = 4;
            this.lblNgayDenPTVT.Text = "Ngày đến";
            // 
            // grbHoaDonThuongMai
            // 
            this.grbHoaDonThuongMai.Controls.Add(this.ccNgayHDTM);
            this.grbHoaDonThuongMai.Controls.Add(this.txtSoHoaDonThuongMai);
            this.grbHoaDonThuongMai.Controls.Add(this.label15);
            this.grbHoaDonThuongMai.Controls.Add(this.label16);
            this.grbHoaDonThuongMai.Enabled = false;
            this.grbHoaDonThuongMai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbHoaDonThuongMai.Location = new System.Drawing.Point(565, 124);
            this.grbHoaDonThuongMai.Name = "grbHoaDonThuongMai";
            this.grbHoaDonThuongMai.Size = new System.Drawing.Size(281, 104);
            this.grbHoaDonThuongMai.TabIndex = 7;
            this.grbHoaDonThuongMai.Text = "Hóa đơn thương mại";
            this.grbHoaDonThuongMai.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ccNgayHDTM
            // 
            // 
            // 
            // 
            this.ccNgayHDTM.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHDTM.DropDownCalendar.Name = "";
            this.ccNgayHDTM.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHDTM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHDTM.Location = new System.Drawing.Point(63, 48);
            this.ccNgayHDTM.Name = "ccNgayHDTM";
            this.ccNgayHDTM.Nullable = true;
            this.ccNgayHDTM.NullButtonText = "Xóa";
            this.ccNgayHDTM.ShowNullButton = true;
            this.ccNgayHDTM.Size = new System.Drawing.Size(96, 21);
            this.ccNgayHDTM.TabIndex = 1;
            this.ccNgayHDTM.TodayButtonText = "Hôm nay";
            this.ccNgayHDTM.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHDTM.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHoaDonThuongMai
            // 
            this.txtSoHoaDonThuongMai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHoaDonThuongMai.Location = new System.Drawing.Point(63, 20);
            this.txtSoHoaDonThuongMai.Name = "txtSoHoaDonThuongMai";
            this.txtSoHoaDonThuongMai.Size = new System.Drawing.Size(212, 21);
            this.txtSoHoaDonThuongMai.TabIndex = 0;
            this.txtSoHoaDonThuongMai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHoaDonThuongMai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHoaDonThuongMai.VisualStyleManager = this.vsmMain;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(8, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Số";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(8, 56);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Ngày";
            // 
            // gbGiayPhep
            // 
            this.gbGiayPhep.Controls.Add(this.ccNgayHHGiayPhep);
            this.gbGiayPhep.Controls.Add(this.txtSoGiayPhep);
            this.gbGiayPhep.Controls.Add(this.label11);
            this.gbGiayPhep.Controls.Add(this.label12);
            this.gbGiayPhep.Controls.Add(this.label33);
            this.gbGiayPhep.Controls.Add(this.ccNgayGiayPhep);
            this.gbGiayPhep.Enabled = false;
            this.gbGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbGiayPhep.Location = new System.Drawing.Point(565, 8);
            this.gbGiayPhep.Name = "gbGiayPhep";
            this.gbGiayPhep.Size = new System.Drawing.Size(281, 104);
            this.gbGiayPhep.TabIndex = 2;
            this.gbGiayPhep.Text = "Giấy phép";
            this.gbGiayPhep.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // ccNgayHHGiayPhep
            // 
            // 
            // 
            // 
            this.ccNgayHHGiayPhep.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHHGiayPhep.DropDownCalendar.Name = "";
            this.ccNgayHHGiayPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHHGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHHGiayPhep.Location = new System.Drawing.Point(64, 77);
            this.ccNgayHHGiayPhep.Name = "ccNgayHHGiayPhep";
            this.ccNgayHHGiayPhep.Nullable = true;
            this.ccNgayHHGiayPhep.NullButtonText = "Xóa";
            this.ccNgayHHGiayPhep.ShowNullButton = true;
            this.ccNgayHHGiayPhep.Size = new System.Drawing.Size(96, 21);
            this.ccNgayHHGiayPhep.TabIndex = 2;
            this.ccNgayHHGiayPhep.TodayButtonText = "Hôm nay";
            this.ccNgayHHGiayPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHHGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // txtSoGiayPhep
            // 
            this.txtSoGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoGiayPhep.Location = new System.Drawing.Point(64, 20);
            this.txtSoGiayPhep.Name = "txtSoGiayPhep";
            this.txtSoGiayPhep.Size = new System.Drawing.Size(211, 21);
            this.txtSoGiayPhep.TabIndex = 0;
            this.txtSoGiayPhep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(2, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Số";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(0, 55);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Ngày";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(2, 84);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(49, 13);
            this.label33.TabIndex = 4;
            this.label33.Text = "Ngày HH";
            // 
            // ccNgayGiayPhep
            // 
            // 
            // 
            // 
            this.ccNgayGiayPhep.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayGiayPhep.DropDownCalendar.Name = "";
            this.ccNgayGiayPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayGiayPhep.Location = new System.Drawing.Point(64, 48);
            this.ccNgayGiayPhep.Name = "ccNgayGiayPhep";
            this.ccNgayGiayPhep.Nullable = true;
            this.ccNgayGiayPhep.NullButtonText = "Xóa";
            this.ccNgayGiayPhep.ShowNullButton = true;
            this.ccNgayGiayPhep.Size = new System.Drawing.Size(96, 21);
            this.ccNgayGiayPhep.TabIndex = 1;
            this.ccNgayGiayPhep.TodayButtonText = "Hôm nay";
            this.ccNgayGiayPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // ds
            // 
            this.ds.DataSetName = "NewDataSet";
            this.ds.Locale = new System.Globalization.CultureInfo("en-US");
            this.ds.Tables.AddRange(new System.Data.DataTable[] {
            this.dtHangMauDich,
            this.dtLoaiHinhMauDich,
            this.dtPTTT,
            this.dtCuaKhau,
            this.dtNguyenTe,
            this.dtCompanyNuoc,
            this.dtDonViHaiQuan});
            // 
            // dtHangMauDich
            // 
            this.dtHangMauDich.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9});
            this.dtHangMauDich.TableName = "HangMauDich";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "hmd_SoThuTuHang";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "hmd_MaHS";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "hmd_MaPhu";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "hmd_TenHang";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "nuoc_Ten";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "donvitinh_Ten";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "hmd_Luong";
            this.dataColumn7.DataType = typeof(decimal);
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "hmd_DonGiaKB";
            this.dataColumn8.DataType = typeof(decimal);
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "hmd_TriGiaKB";
            this.dataColumn9.DataType = typeof(decimal);
            // 
            // dtLoaiHinhMauDich
            // 
            this.dtLoaiHinhMauDich.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn10,
            this.dataColumn11});
            this.dtLoaiHinhMauDich.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "loaihinh_Ma"}, true)});
            this.dtLoaiHinhMauDich.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn10};
            this.dtLoaiHinhMauDich.TableName = "LoaiHinhMauDich";
            // 
            // dataColumn10
            // 
            this.dataColumn10.AllowDBNull = false;
            this.dataColumn10.ColumnName = "loaihinh_Ma";
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "loaihinh_Ten";
            // 
            // dtPTTT
            // 
            this.dtPTTT.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn12,
            this.dataColumn13});
            this.dtPTTT.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "pttt_Ma"}, true)});
            this.dtPTTT.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn12};
            this.dtPTTT.TableName = "PhuongThucThanhToan";
            // 
            // dataColumn12
            // 
            this.dataColumn12.AllowDBNull = false;
            this.dataColumn12.ColumnName = "pttt_Ma";
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnName = "pttt_GhiChu";
            // 
            // dtCuaKhau
            // 
            this.dtCuaKhau.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn16});
            this.dtCuaKhau.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "cuakhau_Ma"}, true)});
            this.dtCuaKhau.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn14};
            this.dtCuaKhau.TableName = "CuaKhau";
            // 
            // dataColumn14
            // 
            this.dataColumn14.AllowDBNull = false;
            this.dataColumn14.ColumnName = "cuakhau_Ma";
            // 
            // dataColumn15
            // 
            this.dataColumn15.ColumnName = "cuakhau_Ten";
            // 
            // dataColumn16
            // 
            this.dataColumn16.ColumnName = "cuc_ma";
            // 
            // dtNguyenTe
            // 
            this.dtNguyenTe.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn17,
            this.dataColumn18});
            this.dtNguyenTe.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nguyente_Ma"}, true)});
            this.dtNguyenTe.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn17};
            this.dtNguyenTe.TableName = "NguyenTe";
            // 
            // dataColumn17
            // 
            this.dataColumn17.AllowDBNull = false;
            this.dataColumn17.ColumnName = "nguyente_Ma";
            // 
            // dataColumn18
            // 
            this.dataColumn18.ColumnName = "nguyente_Ten";
            // 
            // dtCompanyNuoc
            // 
            this.dtCompanyNuoc.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn19,
            this.dataColumn20});
            this.dtCompanyNuoc.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nuoc_Ma"}, true)});
            this.dtCompanyNuoc.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn19};
            this.dtCompanyNuoc.TableName = "CompanyNuoc";
            // 
            // dataColumn19
            // 
            this.dataColumn19.AllowDBNull = false;
            this.dataColumn19.ColumnName = "nuoc_Ma";
            // 
            // dataColumn20
            // 
            this.dataColumn20.ColumnName = "nuoc_Ten";
            // 
            // dtDonViHaiQuan
            // 
            this.dtDonViHaiQuan.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn21,
            this.dataColumn22});
            this.dtDonViHaiQuan.TableName = "DonViHaiQuan";
            // 
            // dataColumn21
            // 
            this.dataColumn21.ColumnName = "donvihaiquan_Ma";
            // 
            // dataColumn22
            // 
            this.dataColumn22.ColumnName = "donvihaiquan_Ten";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            this.epError.Icon = ((System.Drawing.Icon)(resources.GetObject("epError.Icon")));
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvSoHieuPTVT
            // 
            this.rfvSoHieuPTVT.ControlToValidate = this.txtSoHieuPTVT;
            this.rfvSoHieuPTVT.ErrorMessage = "\"Số hiệu phương tiện vận tải\" không được để trống.";
            this.rfvSoHieuPTVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoHieuPTVT.Icon")));
            this.rfvSoHieuPTVT.Tag = "rfvSoHieuPTVT";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(754, 139);
            this.uiGroupBox2.TabIndex = 258;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Chi cục Hải Quan";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.MistyRose;
            this.uiGroupBox1.Controls.Add(this.cboDateNgayHT);
            this.uiGroupBox1.Controls.Add(this.label23);
            this.uiGroupBox1.Controls.Add(this.cbNgayThucNX);
            this.uiGroupBox1.Controls.Add(this.txtSotk);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.label27);
            this.uiGroupBox1.Controls.Add(this.cbNgayDangky);
            this.uiGroupBox1.Controls.Add(this.label18);
            this.uiGroupBox1.Controls.Add(this.lblSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.lblTrangThai);
            this.uiGroupBox1.Controls.Add(this.label26);
            this.uiGroupBox1.Controls.Add(this.ctrDonViHaiQuan);
            this.uiGroupBox1.Controls.Add(this.pnlToKhaiMauDich);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongPLTK);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 35);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1138, 680);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // cboDateNgayHT
            // 
            this.cboDateNgayHT.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.cboDateNgayHT.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.cboDateNgayHT.DropDownCalendar.Name = "";
            this.cboDateNgayHT.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.cboDateNgayHT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDateNgayHT.Location = new System.Drawing.Point(547, 51);
            this.cboDateNgayHT.Name = "cboDateNgayHT";
            this.cboDateNgayHT.Nullable = true;
            this.cboDateNgayHT.NullButtonText = "Xóa";
            this.cboDateNgayHT.ShowNullButton = true;
            this.cboDateNgayHT.Size = new System.Drawing.Size(114, 21);
            this.cboDateNgayHT.TabIndex = 4;
            this.cboDateNgayHT.TodayButtonText = "Hôm nay";
            this.cboDateNgayHT.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(668, 55);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "Số lượng PLTK";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbNgayThucNX
            // 
            this.cbNgayThucNX.DateFormat = Janus.Windows.CalendarCombo.DateFormat.DateTime;
            // 
            // 
            // 
            this.cbNgayThucNX.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.cbNgayThucNX.DropDownCalendar.Name = "";
            this.cbNgayThucNX.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.cbNgayThucNX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNgayThucNX.Location = new System.Drawing.Point(329, 51);
            this.cbNgayThucNX.Name = "cbNgayThucNX";
            this.cbNgayThucNX.Nullable = true;
            this.cbNgayThucNX.NullButtonText = "Xóa";
            this.cbNgayThucNX.ShowNullButton = true;
            this.cbNgayThucNX.Size = new System.Drawing.Size(115, 21);
            this.cbNgayThucNX.TabIndex = 3;
            this.cbNgayThucNX.TodayButtonText = "Hôm nay";
            this.cbNgayThucNX.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.cbNgayThucNX.VisualStyleManager = this.vsmMain;
            // 
            // txtSotk
            // 
            this.txtSotk.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSotk.Location = new System.Drawing.Point(486, 16);
            this.txtSotk.Name = "txtSotk";
            this.txtSotk.ReadOnly = true;
            this.txtSotk.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSotk.Size = new System.Drawing.Size(61, 21);
            this.txtSotk.TabIndex = 1;
            this.txtSotk.Text = "0";
            this.txtSotk.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSotk.Value = ((ulong)(0ul));
            this.txtSotk.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSotk.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSotk.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(426, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Số tờ khai";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(446, 53);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(90, 13);
            this.label27.TabIndex = 20;
            this.label27.Text = "Ngày hoàn thành";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbNgayDangky
            // 
            // 
            // 
            // 
            this.cbNgayDangky.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.cbNgayDangky.DropDownCalendar.Name = "";
            this.cbNgayDangky.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.cbNgayDangky.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNgayDangky.Location = new System.Drawing.Point(109, 51);
            this.cbNgayDangky.Name = "cbNgayDangky";
            this.cbNgayDangky.Nullable = true;
            this.cbNgayDangky.NullButtonText = "Xóa";
            this.cbNgayDangky.ShowNullButton = true;
            this.cbNgayDangky.Size = new System.Drawing.Size(82, 21);
            this.cbNgayDangky.TabIndex = 2;
            this.cbNgayDangky.TodayButtonText = "Hôm nay";
            this.cbNgayDangky.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.cbNgayDangky.VisualStyleManager = this.vsmMain;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(14, 55);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 18;
            this.label18.Text = "Ngày đăng ký";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSoTiepNhan
            // 
            this.lblSoTiepNhan.AutoSize = true;
            this.lblSoTiepNhan.BackColor = System.Drawing.Color.Transparent;
            this.lblSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoTiepNhan.Location = new System.Drawing.Point(199, 55);
            this.lblSoTiepNhan.Name = "lblSoTiepNhan";
            this.lblSoTiepNhan.Size = new System.Drawing.Size(113, 13);
            this.lblSoTiepNhan.TabIndex = 16;
            this.lblSoTiepNhan.Text = "Ngày thực nhập /xuất";
            this.lblSoTiepNhan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(698, 20);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(151, 13);
            this.lblTrangThai.TabIndex = 15;
            this.lblTrangThai.Text = "Thanh khoản một phần";
            this.lblTrangThai.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(553, 20);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(126, 13);
            this.label26.TabIndex = 14;
            this.label26.Text = "Trạng thái thanh khoản :";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ctrDonViHaiQuan
            // 
            this.ctrDonViHaiQuan.AutoSize = true;
            this.ctrDonViHaiQuan.BackColor = System.Drawing.Color.Transparent;
            this.ctrDonViHaiQuan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrDonViHaiQuan.Location = new System.Drawing.Point(109, 14);
            this.ctrDonViHaiQuan.Ma = "C34C";
            this.ctrDonViHaiQuan.MaCuc = "";
            this.ctrDonViHaiQuan.Name = "ctrDonViHaiQuan";
            this.ctrDonViHaiQuan.ReadOnly = false;
            this.ctrDonViHaiQuan.Size = new System.Drawing.Size(292, 24);
            this.ctrDonViHaiQuan.TabIndex = 0;
            this.ctrDonViHaiQuan.VisualStyleManager = this.vsmMain;
            this.ctrDonViHaiQuan.ValueChanged += new Company.Interface.Controls.DonViHaiQuanControl.ValueChangedEventHandler(this.ctrDonViHaiQuan_ValueChanged);
            // 
            // txtSoLuongPLTK
            // 
            this.txtSoLuongPLTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongPLTK.Location = new System.Drawing.Point(749, 51);
            this.txtSoLuongPLTK.Name = "txtSoLuongPLTK";
            this.txtSoLuongPLTK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongPLTK.Size = new System.Drawing.Size(77, 21);
            this.txtSoLuongPLTK.TabIndex = 5;
            this.txtSoLuongPLTK.Text = "0";
            this.txtSoLuongPLTK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongPLTK.Value = ((byte)(0));
            this.txtSoLuongPLTK.ValueType = Janus.Windows.GridEX.NumericEditValueType.Byte;
            this.txtSoLuongPLTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongPLTK.VisualStyleManager = this.vsmMain;
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(241)))), ((int)(((byte)(228)))));
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.uiPanelManager1.DefaultPanelSettings.CloseButtonVisible = false;
            this.uiPanel0.Id = new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2");
            this.uiPanel0.StaticGroup = true;
            this.uiPanel1.Id = new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8");
            this.uiPanel0.Panels.Add(this.uiPanel1);
            this.uiPanelManager1.Panels.Add(this.uiPanel0);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), Janus.Windows.UI.Dock.PanelGroupStyle.HorizontalTiles, Janus.Windows.UI.Dock.PanelDockStyle.Bottom, true, new System.Drawing.Size(754, 210), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8"), new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), 174, true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), Janus.Windows.UI.Dock.PanelGroupStyle.HorizontalTiles, true, new System.Drawing.Point(22, 29), new System.Drawing.Size(56, 56), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.AutoHide = true;
            this.uiPanel0.CaptionFormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.uiPanel0.CloseButtonVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel0.FloatingLocation = new System.Drawing.Point(22, 29);
            this.uiPanel0.FloatingSize = new System.Drawing.Size(56, 56);
            this.uiPanel0.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiPanel0.Location = new System.Drawing.Point(3, 187);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.Size = new System.Drawing.Size(754, 210);
            this.uiPanel0.TabIndex = 1;
            this.uiPanel0.Text = "Thông tin hàng hóa";
            // 
            // uiPanel1
            // 
            this.uiPanel1.CaptionVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(0, 26);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(754, 184);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Panel 1";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.dgList);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 1);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(752, 182);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(752, 182);
            this.dgList.TabIndex = 0;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // rfvNgayDen
            // 
            this.rfvNgayDen.ControlToValidate = this.ccNgayDen;
            this.rfvNgayDen.ErrorMessage = "\"Ngày đến\" không được bỏ trống.";
            this.rfvNgayDen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayDen.Icon")));
            this.rfvNgayDen.Tag = "rfvNgayDen";
            // 
            // rfvNguoiXuatKhau
            // 
            this.rfvNguoiXuatKhau.ControlToValidate = this.txtTenDonViDoiTac;
            this.rfvNguoiXuatKhau.ErrorMessage = "\"Người xuất khẩu\" không được để trống.";
            this.rfvNguoiXuatKhau.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNguoiXuatKhau.Icon")));
            this.rfvNguoiXuatKhau.Tag = "rfvNguoiXuatKhau";
            // 
            // rvTyGiaTT
            // 
            this.rvTyGiaTT.ControlToValidate = this.txtTyGiaTinhThue;
            this.rvTyGiaTT.ErrorMessage = "\"Tỷ giá tính thuế\" không hợp lệ.";
            this.rvTyGiaTT.Icon = ((System.Drawing.Icon)(resources.GetObject("rvTyGiaTT.Icon")));
            this.rvTyGiaTT.MaximumValue = "50000";
            this.rvTyGiaTT.MinimumValue = "1";
            this.rvTyGiaTT.Tag = "rvTyGiaTT";
            this.rvTyGiaTT.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rvTyGiaUSD
            // 
            this.rvTyGiaUSD.ControlToValidate = this.txtTyGiaUSD;
            this.rvTyGiaUSD.ErrorMessage = "\"Tỷ giá USD\" không hợp lệ.";
            this.rvTyGiaUSD.Icon = ((System.Drawing.Icon)(resources.GetObject("rvTyGiaUSD.Icon")));
            this.rvTyGiaUSD.MaximumValue = "50000";
            this.rvTyGiaUSD.MinimumValue = "1";
            this.rvTyGiaUSD.Tag = "rvTyGiaUSD";
            this.rvTyGiaUSD.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // ilLarge
            // 
            this.ilLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilLarge.ImageStream")));
            this.ilLarge.TransparentColor = System.Drawing.Color.Transparent;
            this.ilLarge.Images.SetKeyName(0, "");
            this.ilLarge.Images.SetKeyName(1, "");
            this.ilLarge.Images.SetKeyName(2, "");
            this.ilLarge.Images.SetKeyName(3, "");
            this.ilLarge.Images.SetKeyName(4, "Add.gif");
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "Add.gif");
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave,
            this.ThemHang,
            this.cmdPrint,
            this.cmdPhanBo,
            this.cmdInCV,
            this.cmdCVXNK,
            this.cmdGiayDeNghi,
            this.cmdCamKet,
            this.cmdKiemTraHo,
            this.cmdChuyenCK});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick_1);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ThemHang1,
            this.cmdPrint1,
            this.cmdSave1,
            this.cmdInCV1,
            this.cmdPhanBo1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(560, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // ThemHang1
            // 
            this.ThemHang1.Key = "ThemHang";
            this.ThemHang1.Name = "ThemHang1";
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            this.cmdSave1.Text = "Lưu thông tin tờ khai";
            // 
            // cmdInCV1
            // 
            this.cmdInCV1.Key = "cmdInCV";
            this.cmdInCV1.Name = "cmdInCV1";
            // 
            // cmdPhanBo1
            // 
            this.cmdPhanBo1.Key = "cmdPhanBo";
            this.cmdPhanBo1.Name = "cmdPhanBo1";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "Lưu thông tin";
            // 
            // ThemHang
            // 
            this.ThemHang.Image = ((System.Drawing.Image)(resources.GetObject("ThemHang.Image")));
            this.ThemHang.Key = "ThemHang";
            this.ThemHang.Name = "ThemHang";
            this.ThemHang.Text = "Thêm hàng";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrint.Image")));
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Text = "In tờ khai";
            // 
            // cmdPhanBo
            // 
            this.cmdPhanBo.Image = ((System.Drawing.Image)(resources.GetObject("cmdPhanBo.Image")));
            this.cmdPhanBo.Key = "cmdPhanBo";
            this.cmdPhanBo.Name = "cmdPhanBo";
            this.cmdPhanBo.Text = "Xóa phân bổ";
            // 
            // cmdInCV
            // 
            this.cmdInCV.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCVXNK1,
            this.cmdGiayDeNghi1,
            this.cmdCamKet1,
            this.cmdKiemTraHo1,
            this.cmdChuyenCK1});
            this.cmdInCV.Image = ((System.Drawing.Image)(resources.GetObject("cmdInCV.Image")));
            this.cmdInCV.Key = "cmdInCV";
            this.cmdInCV.Name = "cmdInCV";
            this.cmdInCV.Shortcut = System.Windows.Forms.Shortcut.CtrlShift1;
            this.cmdInCV.Text = "In Công Văn";
            // 
            // cmdCVXNK1
            // 
            this.cmdCVXNK1.Key = "cmdCVXNK";
            this.cmdCVXNK1.Name = "cmdCVXNK1";
            // 
            // cmdGiayDeNghi1
            // 
            this.cmdGiayDeNghi1.Key = "cmdGiayDeNghi";
            this.cmdGiayDeNghi1.Name = "cmdGiayDeNghi1";
            // 
            // cmdCamKet1
            // 
            this.cmdCamKet1.Key = "cmdCamKet";
            this.cmdCamKet1.Name = "cmdCamKet1";
            // 
            // cmdKiemTraHo1
            // 
            this.cmdKiemTraHo1.Key = "cmdKiemTraHo";
            this.cmdKiemTraHo1.Name = "cmdKiemTraHo1";
            // 
            // cmdChuyenCK1
            // 
            this.cmdChuyenCK1.Key = "cmdChuyenCK";
            this.cmdChuyenCK1.Name = "cmdChuyenCK1";
            // 
            // cmdCVXNK
            // 
            this.cmdCVXNK.Image = ((System.Drawing.Image)(resources.GetObject("cmdCVXNK.Image")));
            this.cmdCVXNK.Key = "cmdCVXNK";
            this.cmdCVXNK.Name = "cmdCVXNK";
            this.cmdCVXNK.Shortcut = System.Windows.Forms.Shortcut.CtrlShift2;
            this.cmdCVXNK.Text = "Xuất nhập khẩu";
            // 
            // cmdGiayDeNghi
            // 
            this.cmdGiayDeNghi.Image = ((System.Drawing.Image)(resources.GetObject("cmdGiayDeNghi.Image")));
            this.cmdGiayDeNghi.Key = "cmdGiayDeNghi";
            this.cmdGiayDeNghi.Name = "cmdGiayDeNghi";
            this.cmdGiayDeNghi.Shortcut = System.Windows.Forms.Shortcut.CtrlShift3;
            this.cmdGiayDeNghi.Text = "Giấy đề nghị nhập NPL";
            // 
            // cmdCamKet
            // 
            this.cmdCamKet.Image = ((System.Drawing.Image)(resources.GetObject("cmdCamKet.Image")));
            this.cmdCamKet.Key = "cmdCamKet";
            this.cmdCamKet.Name = "cmdCamKet";
            this.cmdCamKet.Shortcut = System.Windows.Forms.Shortcut.CtrlShift4;
            this.cmdCamKet.Text = "Cam kết nhập NPL";
            // 
            // cmdKiemTraHo
            // 
            this.cmdKiemTraHo.Image = ((System.Drawing.Image)(resources.GetObject("cmdKiemTraHo.Image")));
            this.cmdKiemTraHo.Key = "cmdKiemTraHo";
            this.cmdKiemTraHo.Name = "cmdKiemTraHo";
            this.cmdKiemTraHo.Shortcut = System.Windows.Forms.Shortcut.CtrlShift5;
            this.cmdKiemTraHo.Text = "Kiểm tra hộ";
            // 
            // cmdChuyenCK
            // 
            this.cmdChuyenCK.Image = ((System.Drawing.Image)(resources.GetObject("cmdChuyenCK.Image")));
            this.cmdChuyenCK.Key = "cmdChuyenCK";
            this.cmdChuyenCK.Name = "cmdChuyenCK";
            this.cmdChuyenCK.Shortcut = System.Windows.Forms.Shortcut.CtrlShift6;
            this.cmdChuyenCK.Text = "Chuyển cửa khẩu";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1144, 32);
            // 
            // ToKhaiMauDichDetailForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(1144, 736);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ToKhaiMauDichDetailForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tờ khai nhập khẩu";
            this.Load += new System.EventHandler(this.ToKhaiNhapKhauSXXK_Form_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.pnlToKhaiMauDich.ResumeLayout(false);
            this.pnlToKhaiMauDich.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtchungtu)).EndInit();
            this.txtchungtu.ResumeLayout(false);
            this.txtchungtu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grpLoaiHangHoa)).EndInit();
            this.grpLoaiHangHoa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiNK)).EndInit();
            this.grbNguoiNK.ResumeLayout(false);
            this.grbNguoiNK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemXepHang)).EndInit();
            this.grbDiaDiemXepHang.ResumeLayout(false);
            this.grbDiaDiemXepHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguyenTe)).EndInit();
            this.grbNguyenTe.ResumeLayout(false);
            this.grbNguyenTe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNuocXK)).EndInit();
            this.grbNuocXK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiXK)).EndInit();
            this.grbNguoiXK.ResumeLayout(false);
            this.grbNguoiXK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDong)).EndInit();
            this.grbHopDong.ResumeLayout(false);
            this.grbHopDong.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).EndInit();
            this.uiGroupBox18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).EndInit();
            this.uiGroupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemDoHang)).EndInit();
            this.grbDiaDiemDoHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbVanTaiDon)).EndInit();
            this.grbVanTaiDon.ResumeLayout(false);
            this.grbVanTaiDon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHoaDonThuongMai)).EndInit();
            this.grbHoaDonThuongMai.ResumeLayout(false);
            this.grbHoaDonThuongMai.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbGiayPhep)).EndInit();
            this.gbGiayPhep.ResumeLayout(false);
            this.gbGiayPhep.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHangMauDich)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiHinhMauDich)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPTTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCuaKhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompanyNuoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieuPTVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayDen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiXuatKhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaUSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion		        

        private NguyenTeControl ctrNguyenTe;
        private NuocControl ctrNuocXuatKhau;
        private UIComboBox cbPTTT;
        private UIComboBox cbDKGH;
        private CuaKhauControl ctrCuaKhau;
        private ImageList ImageList1;
        internal ImageList ilLarge;
        private ImageList imageList2;
        private GridEX dgList;
        private UIGroupBox grpLoaiHangHoa;
        private UIRadioButton radNPL;
        private UIRadioButton radSP;
        private UIRadioButton radTB;
        private NumericEditBox txtSotk;
        private Label label10;
        private UIGroupBox uiGroupBox7;
        private Company.Interface.Controls.LoaiHinhMauDichVControl ctrLoaiHinhMauDich;
        private UIGroupBox txtchungtu;
        private EditBox txtChungTuTK;
        private UIRebar TopRebar1;
        private UICommandBar uiCommandBar1;
        private UICommandManager cmMain;
        private UIRebar BottomRebar1;
        private UICommand cmdSave;
        private UIRebar LeftRebar1;
        private UIRebar RightRebar1;
        private UICommand ThemHang;
        private UICommand ThemHang1;
        private NumericEditBox txtPhiKhac;
        private Label label24;
        private Label label25;
        private NumericEditBox txtTrongLuongTinh;
        private UICommand cmdPrint;
        private UICommand cmdPrint1;
        private DonViHaiQuanControl ctrDonViHaiQuan;
        private UICommand cmdSave1;
        private CalendarCombo cbNgayDangky;
        private Label label18;
        private CalendarCombo cbNgayThucNX;
        private Label lblSoTiepNhan;
        private Label lblTrangThai;
        private Label label26;
        private UICommand cmdPhanBo;
        private UICommand cmdPhanBo1;
        private UICommand cmdInCV;
        private UICommand cmdCVXNK;
        private UICommand cmdCVXNK1;
        private UICommand cmdGiayDeNghi1;
        private UICommand cmdCamKet1;
        private UICommand cmdKiemTraHo1;
        private UICommand cmdChuyenCK1;
        private UICommand cmdGiayDeNghi;
        private UICommand cmdCamKet;
        private UICommand cmdKiemTraHo;
        private UICommand cmdChuyenCK;
        private UICommand cmdInCV1;
        private CalendarCombo cboDateNgayHT;
        private Label label27;
    }
}
