﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [Serializable]
    [XmlRoot("Body")]
    public class MessageBody<T>
    {
        /// <summary>
        /// Nội dung
        /// </summary>
        [XmlElement("Content")]
        public MessageContent<T> MessageContent { get; set; }

        /// <summary>
        /// Chữ ký số
        /// </summary>
        [XmlElement("Signature")]
        public MessageSignature MessageSignature { get; set; }

    }
}
