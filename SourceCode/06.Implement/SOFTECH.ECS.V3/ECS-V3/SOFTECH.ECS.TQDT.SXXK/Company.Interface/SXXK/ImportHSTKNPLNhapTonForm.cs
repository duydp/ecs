using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using Company.BLL.SXXK.ThanhKhoan;
namespace Company.Interface.SXXK
{
    public partial class ImportHSTKNPLNhapTonForm : BaseForm
    {
        public NPLNhapTonCollection NPLNTCollection = new NPLNhapTonCollection();
        public ImportHSTKNPLNhapTonForm()
        {
            InitializeComponent();
        }

        private int ConvertCharToInt(string ch)
        {
            return ch[0] - 'A';
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                btnSave.Enabled = false;
                new NPLNhapTon().InsertUpdate(this.NPLNTCollection);
                MLMessages("Nhập thành công.", "MSG_PUB02", "", false);
               // ShowMessage("Import thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
            finally
            {
                btnSave.Enabled = true;
                this.Cursor = Cursors.Default;
                this.Close();
            }
            
        }

        private void ImportTTDMForm_Load(object sender, EventArgs e)
        {
            //GlobalSettings.KhoiTao_GiaTriMacDinh();
            //txtSoToKhai.Text = GlobalSettings.ThongTinDinhMuc.MaSP;
            //txtMaLoaiHinh.Text = GlobalSettings.ThongTinDinhMuc.SoDinhMuc;
            //txtNamDangKy.Text = GlobalSettings.ThongTinDinhMuc.NgayDangKy;
            //txtMaHaiQuan.Text = GlobalSettings.ThongTinDinhMuc.NgayApDung;
            //this.dgList.DataSource = this.TTDMReadCollection;
            openFileDialog1.InitialDirectory = Application.StartupPath;

        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                //error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                }
                else
                {
                    error.SetError(txtRow, "Begin row must be greater than zero ");
                }
                error.SetIconPadding(txtRow, 8);
                return;

            }
            this.Cursor = Cursors.WaitCursor;
            uiButton1.Enabled = false;
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text,true );
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
                this.Cursor = Cursors.Default;
                uiButton1.Enabled = true;
                return;
            }
            try
            {
                ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
               // ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"", "MSG_EXC01", txtSheet.Text, false);
                this.Cursor = Cursors.Default;
                uiButton1.Enabled = true;
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {

                    try
                    {

                        NPLNhapTon npl = new NPLNhapTon();
                        npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        npl.SoToKhai = Convert.ToInt32(wsr.Cells[ConvertCharToInt(txtSoToKhai.Text)].Value);
                        npl.MaLoaiHinh = Convert.ToString(wsr.Cells[ConvertCharToInt(txtMaLoaiHinh.Text)].Value);
                        npl.NamDangKy = Convert.ToInt16(wsr.Cells[ConvertCharToInt(txtNamDangKy.Text)].Value);
                        npl.MaHaiQuan = Convert.ToString(wsr.Cells[ConvertCharToInt(txtMaHaiQuan.Text)].Value);
                        npl.MaNguyenPhuLieu = Convert.ToString(wsr.Cells[ConvertCharToInt(txtMaNPL.Text)].Value);
                        npl.Luong = Convert.ToDecimal(wsr.Cells[ConvertCharToInt(txtLuongNhap.Text)].Value);
                        npl.Ton = Convert.ToDecimal(wsr.Cells[ConvertCharToInt(txtLuongTon.Text)].Value);
                        this.NPLNTCollection.Add(npl);
                    }
                    catch
                    {
                       // if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                        if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\n Bạn có muốn tiếp tục không?", "MSG_EXC06", "", true) != "Yes")
                        {
                            this.Cursor = Cursors.Default;
                            uiButton1.Enabled = true;
                            return;
                        }
                    }
                }
            }
            dgList.DataSource = this.NPLNTCollection;
            dgList.Refetch();
            this.Cursor = Cursors.Default;
            uiButton1.Enabled = true;
        }

        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog(this);
            txtFilePath.Text = openFileDialog1.FileName;
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }


    }
}