﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using DongBoDuLieu.BLL.TKMD_V3;
using System.IO;

namespace DongBo.InsertUpdateTKMD_TQDT
{
      class InsertUpdateTQDT
    {
          public event EventHandler<InsertUpdateEventArgs> InsertUpdateEventArgs;
          private void OnInsertUpdate(InsertUpdateEventArgs e)
        {
            if (InsertUpdateEventArgs != null)
            {
                InsertUpdateEventArgs(this, e);
            }
        }
          public void InsertUpdate(string connectionString)
          {
              int TongSoTKDuocCapNhat = 0;
              int TongSoVanDonCapNhat = 0;
              int TongSoHoaDonCapNhat = 0;
              int TongSoHopDongCapNhat = 0;
              int TongSoCOCapNhat = 0;
              int TongSoGiayPhepCapNhat = 0;
              int TongSoChuyenCuaKhauCapNhat = 0;
              if (GlobalSettings.dsTKMDHQ.Tables.Count > 0)
              {
                  if (GlobalSettings.dsTKMDHQ.Tables["ToKhaiMauDich"].Rows.Count > 0)
                  {
                      SqlDatabase db = new SqlDatabase(connectionString);
                       using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                       {
                           connection.Open();
                           using (SqlTransaction transaction = connection.BeginTransaction())
                           {
                               try
                               {
                                   int total = GlobalSettings.dsTKMDHQ.Tables["ToKhaiMauDich"].Rows.Count;
                                   int pos = 0;
                                   foreach (DataRow drTKMD in GlobalSettings.dsTKMDHQ.Tables["ToKhaiMauDich"].Rows)
                                   {
                                       pos++;
                                       
                                       string MaLoaiHinh = drTKMD["Ma_LH"].ToString();
                                       int SoToKhai = System.Convert.ToInt32(drTKMD["SoTKChinhThuc"]);
                                       int NamDK = System.Convert.ToInt32(drTKMD["NamDK"]);
                                       //if (SoToKhai == 301 && MaLoaiHinh == "XSX01")
                                       //{

                                       //}
                                       //Trạng thái
                                       OnInsertUpdate(new InsertUpdateEventArgs(string.Format("Tờ khai: {0}/{1}/{2} - Ngày đăng ký: {3} ", SoToKhai, MaLoaiHinh, NamDK, drTKMD["Ngay_DK"].ToString()), (int)((pos* 100)/total), TongSoTKDuocCapNhat));

                                       string condition = string.Format("Ma_LH = '{0}' AND SoTK = {1} AND NamDK = {2}",MaLoaiHinh,SoToKhai,NamDK);
                                       long IDTKMD = DongBoDuLieu.BLL.TKMD_V3.TQDT_ToKhaiMauDich.InsertUpdateTKMD_SXXK(drTKMD, transaction, db);
                                       if (IDTKMD > 0)
                                       {
                                           TongSoTKDuocCapNhat++;
                                           #region Vận đơn
                                           if (GlobalSettings.dsTKMDHQ.Tables["VanDon"] != null)
                                           {
                                               long idVanDon = 0;
                                               DataRow[] listVanDon = GlobalSettings.dsTKMDHQ.Tables["VanDon"].Select(condition);
                                               if (listVanDon != null && listVanDon.Length > 0)
                                               {
                                                   DataRow drVanDon = listVanDon[0];
                                                   DataRow[] listContainer;
                                                   if (GlobalSettings.dsTKMDHQ.Tables["VanDonCT"] != null)
                                                   {
                                                       listContainer = GlobalSettings.dsTKMDHQ.Tables["VanDonCT"].Select("Id_CT = " + System.Convert.ToInt32(drVanDon["Id_CT"]));
                                                       idVanDon = ChungTuVanDon.InsertUpdatevd_SXXK(IDTKMD, drVanDon, listContainer, transaction, db);
                                                   }
                                                   else
                                                       idVanDon = ChungTuVanDon.InsertUpdatevd_SXXK(IDTKMD, drVanDon, transaction, db);
                                                   if (idVanDon > 0) TongSoVanDonCapNhat++;
                                               }
                                           }
                                           #endregion

                                           #region Hóa đơn thương mại
                                           if (GlobalSettings.dsTKMDHQ.Tables["HoaDon"] != null)
                                           {
                                               DataRow[] listHoaDon = GlobalSettings.dsTKMDHQ.Tables["HoaDon"].Select(condition);
                                               if (listHoaDon != null && listHoaDon.Length > 0)
                                               {
                                                   DataRow drHoaDon = listHoaDon[0];
                                                   if(HoaDonTM.InsertUpdateHoaDonTM(IDTKMD, drHoaDon, transaction, db)>0)
                                                       TongSoHoaDonCapNhat++;
                                               }
                                           }
#endregion

                                           #region Hợp Đồng Thương mại
                                           if (GlobalSettings.dsTKMDHQ.Tables["HopDong"] != null)
                                           {
                                               DataRow[] listHopDong = GlobalSettings.dsTKMDHQ.Tables["HopDong"].Select(condition);
                                               if (listHopDong != null && listHopDong.Length > 0)
                                               {
                                                   DataRow drHopDong = listHopDong[0];
                                                   if (HopDongTM.InsertUpdateHDTM(IDTKMD, drHopDong, transaction, db) > 0)
                                                       TongSoHopDongCapNhat++;
                                               }
                                           }
                                           #endregion

                                           #region Đề nghị chuyển cửa khẩu
                                           if (GlobalSettings.dsTKMDHQ.Tables["ChuyenCK"] != null)
                                           {
                                               DataRow[] listChuyenCK = GlobalSettings.dsTKMDHQ.Tables["ChuyenCK"].Select(condition);
                                               if (listChuyenCK != null && listChuyenCK.Length > 0)
                                               {
                                                   DataRow drChuyenCK = listChuyenCK[0];
                                                   if (ChuyenCK.InsertUpdateCCK(IDTKMD, drChuyenCK, transaction, db) > 0)
                                                       TongSoChuyenCuaKhauCapNhat++;
                                               }
                                           }
                                           #endregion
                                       }
                                       else
                                       {

                                       }
                                   }
                                   
                                   transaction.Commit();
                                   OnInsertUpdate(new InsertUpdateEventArgs("Hoàn thành cập nhật",100,TongSoTKDuocCapNhat));
                               }
                               catch (System.Exception ex)
                               {
                                   transaction.Rollback();
                                   OnInsertUpdate(new InsertUpdateEventArgs(ex, TongSoTKDuocCapNhat));
                               }
                               finally
                               {
                                   connection.Close();
                               }
                           }
                       }
                  }
                  else
                      OnInsertUpdate(new InsertUpdateEventArgs(new Exception("Chưa lấy thông tin tờ khai mậu dịch"), 0));
              }
              else
                  OnInsertUpdate(new InsertUpdateEventArgs(new Exception("Chưa lấy dữ liệu - hoặc dữ liệu lỗi"), 0));
          
          }

          /// <summary>
          /// Bổ sung các tờ khai mậu dịch từ table t_SXXK_ToKhaiMauDich vào table t_KDT_ToKhaiMauDich  Và  hàng mậu dịch từ t_sxxk_HangMauDich vào t_KDT_HangMauDich
          /// </summary>
          public bool InsertTKMDFromSXXK(string connectionString, string Path)
          {
             
                   SqlDatabase db = new SqlDatabase(connectionString);
                   using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                   {
                       connection.Open();
                       using (SqlTransaction transaction = connection.BeginTransaction())
                       {
                           try
                           {
                               OnInsertUpdate(new InsertUpdateEventArgs("Đang thực hiện bổ sung tờ khai từ 'Đã đăng ký' -> 'Theo dõi'", 0, 0));
                               #region Bổ sung tờ khai mậu dịch
                               FileInfo fileTKMD = new FileInfo(Path + "\\InsertTKMDFromSXXK.sql");
                               string scriptTKMD = fileTKMD.OpenText().ReadToEnd();
                               SqlCommand dbcommand1 = (SqlCommand)db.GetSqlStringCommand(scriptTKMD);
                               db.ExecuteNonQuery(dbcommand1, transaction);
                               #endregion

                               #region Bổ sung hàng mậu dịch
                               FileInfo fileHMD = new FileInfo(Path + "\\inserHMDFromSXXK.sql");
                               string scriptHMD = fileHMD.OpenText().ReadToEnd();
                               SqlCommand dbcommand2 = (SqlCommand)db.GetSqlStringCommand(scriptHMD);
                               db.ExecuteNonQuery(dbcommand2, transaction);
                               #endregion


                               transaction.Commit();
                               return true;
                           }
                           catch (System.Exception ex)
                           {
                               transaction.Rollback();
                               OnInsertUpdate(new InsertUpdateEventArgs(ex, 0));
                               return false;
                           }
                           finally
                           {
                               connection.Close();
                           }
                       }
                   }
          }
    }
}
