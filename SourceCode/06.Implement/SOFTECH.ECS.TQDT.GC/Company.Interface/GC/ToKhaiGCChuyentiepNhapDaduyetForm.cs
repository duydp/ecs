﻿using System;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.DuLieuChuan;
using System.Drawing;
using Company.Interface.KDT.GC;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;

using System.IO;
using Company.GC.BLL;
using Company.GC.BLL.KDT;
using Company.GC.BLL.GC;
//using Company.Interface.Reports;

namespace Company.Interface.GC
{    
    public partial class ToKhaiGCChuyentiepNhapDaduyetForm : BaseForm
    {
        public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep();
        public Company.GC.BLL.KDT.GC.HopDong HDGC = new Company.GC.BLL.KDT.GC.HopDong();
        public Company.GC.BLL.KDT.GC.HopDong HDGCDoiXung = new Company.GC.BLL.KDT.GC.HopDong();
        public string NhomLoaiHinh = string.Empty;
        public bool boolFlag = false ;
        public bool boolFlagEdit = false;
        private string xmlCurrent = "";

        public ToKhaiGCChuyentiepNhapDaduyetForm()
        {
            InitializeComponent();            
        }


        private void KhoiTaoGiaoDien()
        {

            txtMaDonVinhan.Text = GlobalSettings.MA_DON_VI;
            txtTenDonVinhan.Text = GlobalSettings.TEN_DON_VI;
            if (this.OpenType == OpenFormType.Edit)
            {
                this.NhomLoaiHinh = this.TKCT.MaLoaiHinh.Trim().Substring(this.TKCT.MaLoaiHinh.Trim().Length - 1, 1);
            }             
            if (this.NhomLoaiHinh == "N")
            {
                this.Text = setText("Tờ khai gia công chuyển tiếp nhập","Import Transitional Outsourcing declaration ");
                txtSoHopDongNhan.Text = this.HDGC.SoHopDong;
                ccNgayHHHopDongNhan.Value  = this.HDGC.NgayHetHan;
                ccNgayHopDongNhan.Value = this.HDGC.NgayDangKy;
            }
            else 
            {
                this.Text = setText("Tờ khai gia công chuyển tiếp xuất", "Export Transitional Outsourcing declaration ");
                grbNguoiGiao.Text = setText("Người nhận hàng","Receiver");
                grbNguoiNhan.Text = setText("Người giao hàng", "Deliverer");
                grbHopDongGiao.Text = setText("Hợp đồng nhận","Receiving contract");
                grbHopDongNhan.Text = setText("Hợp đồng giao", "Delivery contract ");
                uiNguoiChiDinhDV.Text = setText("Người chỉ định giao hàng","Assigner");
                lblNguoiChiDinhKH.Text = setText("Người chỉ định nhận hàng", "Assigner");
                uiKhachHang.Text = setText("Bên nhận hàng","Receiving party");
                uiDV.Text = setText("Bên giao hàng", "Delivery party");
                lblHDKH.Text = setText("Số HD nhận","Receiving contract no");
                lblNgayHDKH.Text = setText("Ngày HD nhận", "receiving contract date");
                lblSoHDDV.Text = setText("Số HD giao", "Delivery contract no");
                lblNgayHDDV.Text = setText("Ngày HD giao", "Delivery contract date");
                lblNguoiChiDinhKH.Text = setText("Người chỉ định nhận hàng", "Assigner");
                txtSoHopDongNhan.Text = this.HDGC.SoHopDong;
                ccNgayHHHopDongNhan.Value = this.HDGC.NgayHetHan;
                ccNgayHopDongNhan.Value = this.HDGC.NgayDangKy;
            }
            cbLoaiHinh.Nhom = NhomLoaiHinh;
            if (this.OpenType == OpenFormType.Insert)
            {
                cbLoaiHinh.cbTen.SelectedIndex = 0;
            }   
           // this.cbLoaiHinh.ValueChanged -= new Company.Interface.Controls.LoaiHinhChuyenTiepControl.ValueChangedEventHandler(this.cbLoaiHinh_SelectedIndexChanged);
        }
        private void txtSoHopDongNhan_ButtonClick(object sender, EventArgs e)
        {
            HopDongManageForm f = new HopDongManageForm();            
            f.IsBrowseForm=true;
            f.IsDaDuyet = true;
            f.ShowDialog();      
            if (f.HopDongSelected.SoHopDong != "")
            {
                if (this.TKCT.HCTCollection.Count > 0)
                {
                    if (showMsg("MSG_SAV05", true) == "Yes")
                    //if (ShowMessage("Chương trình sẽ xóa tất cả các hàng đã nhập của hợp đồng trước đó.Bạn có muốn thay đổi hợp đồng không ?", true) == "Yes")
                    {
                        string LoaiHangHoa="N";
                        if(TKCT.MaLoaiHinh.IndexOf("SP")>0)
                            LoaiHangHoa="S";
                        else if(TKCT.MaLoaiHinh.IndexOf("TB")>0)
                            LoaiHangHoa="T";
                        foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                        {
                            if (HCT.ID > 0)
                                HCT.Delete(TKCT.IDHopDong, LoaiHangHoa, TKCT.MaLoaiHinh);
                        }
                        TKCT.HCTCollection.Clear();
                    }
                    else
                        return;
                }
                this.HDGC = f.HopDongSelected;
                txtSoHopDongNhan.Text = f.HopDongSelected.SoHopDong;
                ccNgayHopDongNhan.Value = f.HopDongSelected.NgayKy;
                ccNgayHHHopDongNhan.Value = f.HopDongSelected.NgayHetHan;
                this.TKCT.IDHopDong = this.HDGC.ID;
               
            }
        }
        private void loadTKCTData()
        {
            switch (this.TKCT.TrangThaiXuLy)
            {
                case -1:
                    lbltrangthai.Text = setText("Chưa gửi đến Hải quan","Not declared yet");
                    break;
                case 0:
                    lbltrangthai.Text = setText("Chờ duyệt", "Waiting for approval");
                    break;
                case 1:
                    lbltrangthai.Text = setText("Đã duyệt", "Approved");
                    break;
            }
            txtsotiepnhan.Value = this.TKCT.SoTiepNhan;        
            txtToKhaiSo.Value = this.TKCT.SoToKhai;
            txtCanbodangky.Text =  this.TKCT.CanBoDangKy;
            ccNgayDangKy.Value = this.TKCT.NgayDangKy;            
            cbLoaiHinh.Ma = this.TKCT.MaLoaiHinh.Trim();

            txtMaDonVinhan.Text = this.TKCT.MaDoanhNghiep;
            txtTenDonVinhan.Text = GlobalSettings.TEN_DON_VI;
            txtSoHopDongNhan.Text = this.TKCT.SoHopDongDV;
            ccNgayHopDongNhan.Value = this.TKCT.NgayHDDV;
            ccNgayHHHopDongNhan.Value = this.TKCT.NgayHetHanHDDV;
            txtNguoiChiDinhDV.Text = this.TKCT.NguoiChiDinhDV;
            txtDiaDiemGiaohang.Text = this.TKCT.DiaDiemXepHang;

            txtMaDVGiao.Text = this.TKCT.MaKhachHang;
            txtTenDVGiao.Text = this.TKCT.TenKH;
            txtSoHongDonggiao.Text = this.TKCT.SoHDKH;
            ccNgayHopDongGiao.Value = this.TKCT.NgayHDKH;
            ccNgayHHHopDongGiao.Value = this.TKCT.NgayHetHanHDDV;
            txtNguoiChiDinhKH.Text = this.TKCT.NguoiChiDinhKH;
            donViHaiQuanControl2.Ma = this.TKCT.MaHaiQuanKH;

            txtMaDonViUyThac.Text = this.TKCT.MaDaiLy;
            //txtTenDonViUyThac.Text = this.TKCT.TenDaiLy;          
            txtTyGiaTinhThue.Value = this.TKCT.TyGiaVND;
            txtTyGiaUSD.Value = this.TKCT.TyGiaUSD;
            txtLePhiHQ.Value = this.TKCT.LePhiHQ;
            txtSoBienLai.Text = this.TKCT.SoBienLai;
            ccNgayBienLai.Value = this.TKCT.NgayBienLai;
            txtChungTu.Text = this.TKCT.ChungTu;            

            nguyenTeControl1.Ma = this.TKCT.NguyenTe_ID;
            this.TKCT.LoadHCTCollection();
            dgList.DataSource = this.TKCT.HCTCollection;
           // donViHaiQuanControl1.ReadOnly = true;
            //ccNgayDangKy.ReadOnly = true;
            //cbLoaiHinh.ReadOnly = true;
            this.cbLoaiHinh.ValueChanged += new Company.Interface.Controls.LoaiHinhChuyenTiepControl.ValueChangedEventHandler(this.cbLoaiHinh_SelectedIndexChanged);
            HDGC = new HopDong();
            HDGC.ID = TKCT.IDHopDong;
        }
        private void ToKhaiGCChuyenTiepNhapForm_Load(object sender, EventArgs e)
        {
            KhoiTaoGiaoDien();
            if(TKCT.ID>0)
                this.loadTKCTData();
            donViHaiQuanControl2.ReadOnly = false;
            setCommandStatus();
            dgList.DataSource = TKCT.HCTCollection;
            TKCT.IDHopDong = HDGC.ID;
            lbltrangthai.Text = setText("Đã duyệt", "Approved");
            string MaLoaiHinh = cbLoaiHinh.Ma.Trim();
            switch (MaLoaiHinh.Substring(2, 2))
            {
                case "PL":
                    dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    break;
                case "SP":
                    if (MaLoaiHinh.EndsWith("X"))
                    {
                        dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
                    }
                    else
                    {
                        dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    }
                    break;
                case "TB":
                    dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    break;
            }
        }

        private void ViewData()
        {
            cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True ;
            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            txtToKhaiSo.ReadOnly = true;
            txtCanbodangky.ReadOnly = true;
            ccNgayDangKy.ReadOnly = true;
            txtMaDonVinhan.ReadOnly = true;
            txtTenDonVinhan.ReadOnly = true;
            txtSoHopDongNhan.ReadOnly = true;
           // txtSoHopDongNhan.ButtonStyle = EditButtonStyle.NoButton;
            ccNgayHopDongNhan.ReadOnly = true;
            ccNgayHHHopDongNhan.ReadOnly = true;
            txtMaDVGiao.ReadOnly = true;
            txtTenDVGiao.ReadOnly = true;
            txtSoHongDonggiao.ReadOnly = true;
            ccNgayHopDongGiao.ReadOnly = true;
            ccNgayHHHopDongGiao.ReadOnly = true;
            txtNguoiChiDinhKH.ReadOnly = true;
            txtMaDonViUyThac.ReadOnly = true;            
            cbLoaiHinh.ReadOnly = true;
            txtTyGiaTinhThue.ReadOnly = true;
            txtTyGiaUSD.ReadOnly = true;
            txtLePhiHQ.ReadOnly = true;
            txtSoBienLai.ReadOnly = true;
            ccNgayBienLai.ReadOnly = true;
            txtChungTu.ReadOnly = true;
            txtDiaDiemGiaohang.ReadOnly = true;
            nguyenTeControl1.ReadOnly = true;
            txtNguoiChiDinhDV.ReadOnly = true;
            txtNguoiChiDinhKH.ReadOnly = true;
        }

        private void cbLoaiHinh_SelectedIndexChanged(object sender, EventArgs e)
        {                        
            {                
                this.TKCT.HCTCollection.Clear();
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }

        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdThemHang":
                    this.Add();
                    break;
                case "cmdPrint":
                    this.Print();
                    break;
                case "cmdSend":
                    this.Send();
                    break;
                case "XacNhan":
                    this.LaySoTiepNhanDT();
                    break;
                case "NhanDuLieu":
                    this.NhanDuLieuTK();
                    break;
                case "Huy":
                    this.HuyKhaiBao();
                    break;
            }
        }
        private void NhanDuLieuTK()
        {
            this.Cursor = Cursors.Default;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "TKCT";
            sendXML.master_id = TKCT.ID;
            if (sendXML.Load())
            {
                //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                showMsg("MSG_WRN05");
                return;
            }
            WSForm wsForm = new WSForm();
            string password = "";
            {
                try
                {
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();

                    this.Cursor = Cursors.WaitCursor;
                    xmlCurrent = this.TKCT.WSRequest(password);
                    this.Cursor = Cursors.Default;
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = TKCT.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 2;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                    LayPhanHoi(password);

                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    //if (loaiWS == "1")
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                                //{
                                //    HangDoi hd = new HangDoi();
                                //    hd.ID = TKMD.ID;
                                //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                //    hd.TrangThai = TKMD.TrangThaiXuLy;
                                //    hd.ChucNang = ChucNang.KHAI_BAO;
                                //    hd.PassWord = password;
                                //    MainForm.AddToQueueForm(hd);
                                //    MainForm.ShowQueueForm();
                                //}
                                showMsg("MSG_WRN12");
                                //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                                return;
                            }
                            else
                            {
                                showMsg("MSG_2702016", msg[0]);
                                //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            }
                        }
                        else
                        {
                            if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                                showMsg("MSG_WRN13",ex.Message);
                                //ShowMessage("Xảy ra lỗi không xác định.", false);
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                                showMsg("MSG_2702004", ex.Message);
                                //ShowMessage(ex.Message, false);
                            }
                            setCommandStatus();
                        }
                        #endregion FPTService
                    }

                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi khai báo danh sách Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();

                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }

            }
        }
        private void HuyKhaiBao()
        {          
            this.Cursor = Cursors.Default;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "TKCT";
            sendXML.master_id = TKCT.ID;
            if (sendXML.Load())
            {
                //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                showMsg("MSG_WRN05");
                return;
            }
            WSForm wsForm = new WSForm();
            string password = "";           
            {               
                try
                {
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();

                    this.Cursor = Cursors.WaitCursor;
                    xmlCurrent = this.TKCT.WSCancel(password);
                    this.Cursor = Cursors.Default;
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = TKCT.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 3;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                    LayPhanHoi(password);

                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    //if (loaiWS == "1")
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                                //{
                                //    HangDoi hd = new HangDoi();
                                //    hd.ID = TKMD.ID;
                                //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                //    hd.TrangThai = TKMD.TrangThaiXuLy;
                                //    hd.ChucNang = ChucNang.KHAI_BAO;
                                //    hd.PassWord = password;
                                //    MainForm.AddToQueueForm(hd);
                                //    MainForm.ShowQueueForm();
                                //}
                                showMsg("MSG_WRN12");
                                //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                                return;
                            }
                            else
                            {
                                showMsg("MSG_2702016", msg[0]);
                                //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            }
                        }
                        else
                        {
                            if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                                showMsg("MSG_WRN13",ex.Message);
                                //ShowMessage("Xảy ra lỗi không xác định.", false);
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                                showMsg("MSG_2702004", ex.Message);
                                //ShowMessage(ex.Message, false);
                            }
                            setCommandStatus();
                        }
                        #endregion FPTService
                    }

                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi khai báo danh sách Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();

                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }

            }
        }
        private void Send()
        {
           
            this.Cursor = Cursors.Default;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "TKCT";
            sendXML.master_id = TKCT.ID;
            if (sendXML.Load())
            {
                //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                showMsg("MSG_SEN01"); 
                return;
            }
            WSForm wsForm = new WSForm();
            string password = "";
            cvError.Validate();
            if (cvError.IsValid)
            {
                if (this.TKCT.HCTCollection.Count == 0)
                {
                    showMsg("MSG_SEN08");
                    //ShowMessage("Chưa nhập thông tin hàng của tờ khai.", false);
                    return;
                }
                
                if (this.TKCT.ID == 0)
                {
                    this.TKCT.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    this.TKCT.SoTiepNhan = 0;
                    this.TKCT.NgayTiepNhan = new DateTime(1900, 01, 01);
                }           
                try
                {
                                        
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();
            
                    this.Cursor = Cursors.WaitCursor;
                    xmlCurrent = this.TKCT.WSSendNhap(password);
                    this.Cursor = Cursors.Default;
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = TKCT.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 1;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                    LayPhanHoi(password);    
                    
                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    //if (loaiWS == "1")
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                                //{
                                //    HangDoi hd = new HangDoi();
                                //    hd.ID = TKMD.ID;
                                //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                //    hd.TrangThai = TKMD.TrangThaiXuLy;
                                //    hd.ChucNang = ChucNang.KHAI_BAO;
                                //    hd.PassWord = password;
                                //    MainForm.AddToQueueForm(hd);
                                //    MainForm.ShowQueueForm();
                                //}
                                showMsg("MSG_WRN12");
                                //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                                return;
                            }
                            else
                            {
                                showMsg("MSG_2702016", msg[0]);
                                //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            }
                        }
                        else
                        {
                            if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                                showMsg("MSG_WRN13",ex.Message);
                                //ShowMessage("Xảy ra lỗi không xác định.", false);
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                                showMsg("MSG_2702004", ex.Message);
                                //ShowMessage(ex.Message, false);
                            }
                            setCommandStatus();
                        }
                        #endregion FPTService
                    }

                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi khai báo danh sách Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();

                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
                
            }
        }
        private void LayPhanHoi(string pass)
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            try
            {
                sendXML.LoaiHS = "TKCT";
                sendXML.master_id = TKCT.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;              
                xmlCurrent = TKCT.LayPhanHoi(pass, sendXML.msg);               
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    if (showMsg("MSG_STN02", true) == "Yes")
                    //if (ShowMessage("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", true) == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(pass);
                    }
                    else
                    {
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cbLoaiHinh.ReadOnly = true;
                        XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;                       
                    }
                    return;
                }
                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", TKCT.SoTiepNhan);
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + TKCT.SoTiepNhan, false);
                    txtsotiepnhan.Text = this.TKCT.SoTiepNhan.ToString();
                    lbltrangthai.Text = setText("Chờ duyệt", "Waiting for approval");                    
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702024");
                    //ShowMessage("Hủy khai báo thành công", false);
                    txtsotiepnhan.Text = "";
                    lbltrangthai.Text = setText("Chưa khai báo", "Not declared yet");
                }
                else if (sendXML.func == 2)
                {
                    if (TKCT.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        showMsg("MSG_SEN11", TKCT.SoToKhai);
                        //this.ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKCT.SoToKhai.ToString(), false);
                        lbltrangthai.Text = setText("Đã duyệt", "Approved");
                        txtToKhaiSo.Text = TKCT.SoToKhai.ToString();
                    }
                    else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        showMsg("MSG_SEN04");
                        //this.ShowMessage("Hải quan chưa duyệt danh sách này", false);
                    }
                    else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        showMsg("MSG_SEN05");
                        //this.ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", false);
                        lbltrangthai.Text = setText("Không phê duyệt", "Not approved");
                    }
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13",ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận  danh sách Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void Print()
        {
            if (this.TKCT.HCTCollection.Count == 0) return;
            if (this.TKCT.ID == 0)
            {
                showMsg("MSG_PRI01");
                //MLMessages("Bạn hãy lưu trước khi in.","MSG_PRI01","", false);
                return;
            }
            Report.ReportViewTKCTForm f = new Company.Interface.Report.ReportViewTKCTForm();
            f.TKCT = this.TKCT;
            f.ShowDialog();
        }
        private void Add()
        {
            if (txtSoHopDongNhan.Text.Trim() == "")
            {
                epError.SetError(txtSoHopDongNhan, setText("Số hợp đồng nhận không được rỗng.","This VietNam must be filled"));
                epError.SetIconPadding(txtSoHopDongNhan, -8);
                return;
            }
            if (txtSoHongDonggiao.Text.Trim() == "")
            {
                epError.SetError(txtSoHongDonggiao, setText("Số hợp đồng giao không được rỗng.", "This VietNam must be filled"));
                epError.SetIconPadding(txtSoHongDonggiao, -8);
                return;
            }            
            HangChuyenTiepRegisterForm f = new HangChuyenTiepRegisterForm();            
            if (this.HDGC.SoHopDong == "")
            {
                this.HDGC.SoHopDong = txtSoHopDongNhan.Text;
                this.HDGC.NgayKy = ccNgayHopDongNhan.Value;
                this.HDGC.MaDoanhNghiep = txtMaDonVinhan.Text;                
                //this.HDGC.MaHaiQuan = GlobalSetti1ngs.MA_HAI_QUAN;
            }
            f.HD = this.HDGC;
            f.MaLoaiHinh = cbLoaiHinh.Ma;
            f.MaNguyenTe = nguyenTeControl1.Ma;
            f.TKCT = this.TKCT;
            f.ShowDialog();            
            dgList.DataSource = this.TKCT.HCTCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void TaoToKhaiDoiXung()
        {
            ToKhaiChuyenTiep tkCopy = new ToKhaiChuyenTiep();
            tkCopy.SoTiepNhan = 0;
            tkCopy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            tkCopy.NgayTiepNhan = new DateTime(1900, 01, 01);
            tkCopy.MaHaiQuanTiepNhan = TKCT.MaHaiQuanTiepNhan;
            tkCopy.SoToKhai = Convert.ToInt32(txtToKhaiSo.Value);
            tkCopy.CanBoDangKy = txtCanbodangky.Text;
            tkCopy.NgayDangKy = ccNgayDangKy.Value;
            tkCopy.MaLoaiHinh = cbLoaiHinh.Ma.Substring(0,cbLoaiHinh.Ma.Length-1);
            if (TKCT.MaLoaiHinh.EndsWith("N"))
                tkCopy.MaLoaiHinh += "X";
            else 
                tkCopy.MaLoaiHinh += "N";
            tkCopy.MaDoanhNghiep = this.TKCT.MaKhachHang;
            tkCopy.SoHopDongDV = this.TKCT.SoHDKH;
            tkCopy.NgayHDDV = TKCT.NgayHDKH;
            tkCopy.NgayHetHanHDDV = TKCT.NgayHetHanHDKH;
            tkCopy.NguoiChiDinhDV = TKCT.NguoiChiDinhKH;
            tkCopy.DiaDiemXepHang = TKCT.DiaDiemXepHang;

            tkCopy.MaKhachHang = TKCT.MaDoanhNghiep;
            tkCopy.TenKH = GlobalSettings.TEN_DON_VI;
            tkCopy.SoHDKH = TKCT.SoHopDongDV;
            tkCopy.NgayHDKH = TKCT.NgayHDDV;
            tkCopy.NgayHetHanHDKH = TKCT.NgayHetHanHDDV;
            tkCopy.NguoiChiDinhKH = TKCT.NguoiChiDinhDV;
            tkCopy.MaHaiQuanKH = donViHaiQuanControl2.Ma;

            tkCopy.MaDaiLy = txtMaDonViUyThac.Text;
            tkCopy.TyGiaVND = (decimal)txtTyGiaTinhThue.Value;
            tkCopy.TyGiaUSD = (decimal)txtTyGiaUSD.Value;
            tkCopy.LePhiHQ = (decimal)txtLePhiHQ.Value;
            tkCopy.SoBienLai = txtSoBienLai.Text;
            tkCopy.NgayBienLai = ccNgayBienLai.Value;
            tkCopy.ChungTu = txtChungTu.Text;
            tkCopy.NguyenTe_ID = nguyenTeControl1.Ma;
            tkCopy.IDHopDong = HDGCDoiXung.ID;
            foreach (HangChuyenTiep hang in TKCT.HCTCollection)
            {
                HangChuyenTiep hangCopy = new HangChuyenTiep();
                hangCopy.DonGia = hang.DonGia;
                hangCopy.ID_DVT = hang.ID_DVT;
                hangCopy.ID_NuocXX = hang.ID_NuocXX;
                hangCopy.MaHang = hang.MaHang;
                hangCopy.MaHS = hang.MaHS;
                hangCopy.SoLuong = hang.SoLuong;
                hangCopy.SoThuTuHang = hang.SoThuTuHang;
                hangCopy.TenHang = hang.TenHang;
                hangCopy.TriGia = hang.TriGia;
                tkCopy.HCTCollection.Add(hangCopy);
            }
            try
            {
                tkCopy.InsertUpdateFullCopy(TKCT);
                showMsg("MSG_COPY01", tkCopy.ID);
                //ShowMessage("Tạo tờ khai đối xứng thành công. Tờ khai đối xứng có ID="+tkCopy.ID.ToString(), false);    
            }
            catch (Exception ex)
            {
                showMsg("MSG_COPY02", ex.Message);
                //ShowMessage("Lỗi khi tạo tờ khai đối xứng : " + ex.Message, false);    
            }
        }

        private void CapNhatToKhaiDoiXung()
        {
            ToKhaiChuyenTiep tkCopy = new ToKhaiChuyenTiep();
            tkCopy.ID = TKCT.ID_Relation;
            if (!tkCopy.Load())
            {
                if(showMsg("MSG_COPY03", true) == "Yes")
                //if (ShowMessage("Tờ khai đối xứng đã bị xóa. Bạn có muốn tạo tờ khai đối xứng không ? ", true) == "Yes")
                {
                    TaoToKhaiDoiXung();
                }
                return;
            }
            tkCopy.LoadHCTCollection();
            tkCopy.SoTiepNhan = 0;
            tkCopy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            tkCopy.NgayTiepNhan = new DateTime(1900, 01, 01);
            tkCopy.MaHaiQuanTiepNhan = TKCT.MaHaiQuanTiepNhan;
            tkCopy.SoToKhai = Convert.ToInt32(txtToKhaiSo.Value);
            tkCopy.CanBoDangKy = txtCanbodangky.Text;
            tkCopy.NgayDangKy = ccNgayDangKy.Value;
            tkCopy.MaLoaiHinh = cbLoaiHinh.Ma.Substring(0, cbLoaiHinh.Ma.Length - 1);
            if (TKCT.MaLoaiHinh.EndsWith("N"))
                tkCopy.MaLoaiHinh += "X";
            else
                tkCopy.MaLoaiHinh += "N";
            tkCopy.MaDoanhNghiep = this.TKCT.MaKhachHang;
            tkCopy.SoHopDongDV = this.TKCT.SoHDKH;
            tkCopy.NgayHDDV = TKCT.NgayHDKH;
            tkCopy.NgayHetHanHDDV = TKCT.NgayHetHanHDKH;
            tkCopy.NguoiChiDinhDV = TKCT.NguoiChiDinhKH;
            tkCopy.DiaDiemXepHang = TKCT.DiaDiemXepHang;

            tkCopy.MaKhachHang = TKCT.MaDoanhNghiep;
            tkCopy.TenKH = GlobalSettings.TEN_DON_VI;
            tkCopy.SoHDKH = TKCT.SoHopDongDV;
            tkCopy.NgayHDKH = TKCT.NgayHDDV;
            tkCopy.NgayHetHanHDKH = TKCT.NgayHetHanHDDV;
            tkCopy.NguoiChiDinhKH = TKCT.NguoiChiDinhDV;
            tkCopy.MaHaiQuanKH = donViHaiQuanControl2.Ma;

            tkCopy.MaDaiLy = txtMaDonViUyThac.Text;
            tkCopy.TyGiaVND = (decimal)txtTyGiaTinhThue.Value;
            tkCopy.TyGiaUSD = (decimal)txtTyGiaUSD.Value;
            tkCopy.LePhiHQ = (decimal)txtLePhiHQ.Value;
            tkCopy.SoBienLai = txtSoBienLai.Text;
            tkCopy.NgayBienLai = ccNgayBienLai.Value;
            tkCopy.ChungTu = txtChungTu.Text;
            tkCopy.NguyenTe_ID = nguyenTeControl1.Ma;
            tkCopy.IDHopDong = HDGCDoiXung.ID;
            tkCopy.HCTCollection.Clear();
            foreach (HangChuyenTiep hang in TKCT.HCTCollection)
            {
                HangChuyenTiep hangCopy = new HangChuyenTiep();
                hangCopy.DonGia = hang.DonGia;
                hangCopy.ID_DVT = hang.ID_DVT;
                hangCopy.ID_NuocXX = hang.ID_NuocXX;
                hangCopy.MaHang = hang.MaHang;
                hangCopy.MaHS = hang.MaHS;
                hangCopy.SoLuong = hang.SoLuong;
                hangCopy.SoThuTuHang = hang.SoThuTuHang;
                hangCopy.TenHang = hang.TenHang;
                hangCopy.TriGia = hang.TriGia;
                tkCopy.HCTCollection.Add(hangCopy);
            }
            try
            {
                tkCopy.CapNhatFullCopy();
                //ShowMessage("Cập nhật tờ khai đối xứng thành công.", false);
                showMsg("MSG_2702025");
            }
            catch (Exception ex)
            {
                showMsg("MSG_COPY04", ex.Message);
                //ShowMessage("Lỗi khi cập nhật tờ khai đối xứng : "+ex.Message, false);                
            }
           
        }

        private void XoaToKhaiDoiXung()
        {
            try
            {
                ToKhaiChuyenTiep tkCopy = new ToKhaiChuyenTiep();
                tkCopy.ID = TKCT.ID_Relation;
                tkCopy.XoaFullCopy(TKCT);
                showMsg("MSG_DEL02");
                //ShowMessage("Xóa tờ khai đối xứng thành công.", false);  
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702026", ex.Message);
                //ShowMessage("Lỗi khi xóa tờ khai đối xứng : "+ex.Message, false);                
            }
        }

        private void Save()
        {
            if (TKCT.ID > 0)
            {
                if (TKCT.MaLoaiHinh.Contains("X"))
                {
                    if (PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(TKCT.ID))
                    {
                        ShowMessage("Tờ khai chuyển tiếp có id = " + TKCT.ID + " này đã được phân bổ nên không thể xóa được.", false);
                        return;
                    }
                }
                else
                {
                    if (PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap((int)TKCT.SoToKhai, TKCT.MaLoaiHinh, TKCT.MaHaiQuanTiepNhan, (short)TKCT.NgayDangKy.Year,TKCT.IDHopDong))
                    {
                        ShowMessage("Tờ khai chuyển tiếp có id= " + TKCT.ID + " đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                        return;
                    }
                }
            }
            string temp = "";
            cvError.Validate();
            if (cvError.IsValid)
            {
                if (this.txtMaDVGiao.Text.Trim() == this.txtMaDonVinhan.Text.Trim())
                {
                    HopDong hd = new HopDong();
                    if (!hd.checkSoHopDongExit(txtSoHongDonggiao.Text.Trim(), HDGC.MaHaiQuan, GlobalSettings.MA_DON_VI))
                    {
                        if (showMsg("MSG_COPY05", true) == "Yes")
                        //if (ShowMessage("Doanh nghiệp không có số hợp đồng này. Bạn có muốn tiếp tục không?", true) != "Yes")
                            return;
                    }
                }
                if (this.TKCT.HCTCollection.Count == 0)
                {
                    showMsg("MSG_SEN08");
                    //ShowMessage("Chưa nhập thông tin hàng của tờ khai", false);
                    return;
                }
                HDGC.Load();
                this.TKCT.SoTiepNhan = Convert.ToInt64(txtsotiepnhan.Value);              
                this.TKCT.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET ;
                       
                
                
                this.TKCT.NgayTiepNhan = new DateTime(1900, 01, 01);
                this.TKCT.MaHaiQuanTiepNhan = HDGC.MaHaiQuan;
                //this.TKCT.MaHaiQuanTiepNhan = GlobalSettings.MA_HAI_QUAN;
                this.TKCT.SoToKhai = Convert.ToInt32(txtToKhaiSo.Value);
                this.TKCT.CanBoDangKy = txtCanbodangky.Text;
                this.TKCT.NgayDangKy = ccNgayDangKy.Value;
                this.TKCT.MaLoaiHinh = cbLoaiHinh.Ma;

                this.TKCT.MaDoanhNghiep = txtMaDonVinhan.Text;
                this.TKCT.SoHopDongDV = txtSoHopDongNhan.Text;
                this.TKCT.NgayHDDV = ccNgayHopDongNhan.Value;
                this.TKCT.NgayHetHanHDDV = ccNgayHHHopDongNhan.Value;
                this.TKCT.NguoiChiDinhDV = txtNguoiChiDinhDV.Text;
                this.TKCT.DiaDiemXepHang = txtDiaDiemGiaohang.Text;

                this.TKCT.MaKhachHang = txtMaDVGiao.Text;
                this.TKCT.TenKH = txtTenDVGiao.Text;
                this.TKCT.SoHDKH = txtSoHongDonggiao.Text;
                this.TKCT.NgayHDKH = ccNgayHopDongGiao.Value;
                this.TKCT.NgayHetHanHDKH = ccNgayHHHopDongGiao.Value;
                this.TKCT.NguoiChiDinhKH = txtNguoiChiDinhKH.Text;
                this.TKCT.MaHaiQuanKH = donViHaiQuanControl2.Ma;

                this.TKCT.MaDaiLy = txtMaDonViUyThac.Text;
                this.TKCT.TyGiaVND = (decimal)txtTyGiaTinhThue.Value;
                this.TKCT.TyGiaUSD = (decimal)txtTyGiaUSD.Value;
                this.TKCT.LePhiHQ = (decimal)txtLePhiHQ.Value;
                this.TKCT.SoBienLai = txtSoBienLai.Text;
                this.TKCT.NgayBienLai = ccNgayBienLai.Value;
                this.TKCT.ChungTu = txtChungTu.Text;
                this.TKCT.NguyenTe_ID = nguyenTeControl1.Ma;
                long idTK = TKCT.ID;
                this.TKCT.IDHopDong = HDGC.ID;
                this.TKCT.InsertUpdateFull();
                if (TKCT.MaLoaiHinh.Contains("PHPLN") || TKCT.MaLoaiHinh.Contains("PHSPN") || TKCT.MaLoaiHinh.Contains("NGC18") || TKCT.MaLoaiHinh.Contains("NGC19"))
                    NPLNhapTonThucTe.UpdateNguyenPhuLieuTonThucTeByTKCT(this.TKCT);
                //this.TKCT.LoadHCTCollection();
                //dgList.DataSource = this.TKCT.HCTCollection;
                //dgList.Refetch();
                showMsg("MSG_SAV02");
                //ShowMessage("Ghi thành công", false);
                
                
            }
        }
        private void LaySoTiepNhanDT()
        {
            WSForm wsForm = new WSForm();
            string password = "";   
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            try
            {
                sendXML.LoaiHS = "TKCT";
                sendXML.master_id = TKCT.ID;
                if (!sendXML.Load())
                {
                    showMsg("MSG_STN01");
                    //ShowMessage("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                    return;
                }                             
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                {
                    xmlCurrent = TKCT.LayPhanHoi(password, sendXML.msg);                 
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    if(showMsg("MSG_STN02", true) == "Yes")
                    //if (ShowMessage("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", true) == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(password);
                    }
                    else
                    {
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cbLoaiHinh.ReadOnly = true;
                        XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;                        
                    }
                    return;
                }
                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", TKCT.SoTiepNhan);
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + TKCT.SoTiepNhan, false);
                    txtsotiepnhan.Text = this.TKCT.SoTiepNhan.ToString();
                    lbltrangthai.Text = setText("Chờ duyệt", "Wait for approval");                   
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702024");
                    //ShowMessage("Hủy khai báo thành công", false);
                    txtsotiepnhan.Text = "";
                    lbltrangthai.Text = setText("Chưa khai báo", "Not declared yet");
                }
                else if (sendXML.func == 2)
                {
                    if (TKCT.SoToKhai == TrangThaiXuLy.DA_DUYET)
                    {
                        showMsg("MSG_SEN11", TKCT.SoToKhai);
                        //this.ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKCT.SoToKhai.ToString(), false);
                        lbltrangthai.Text = setText("Đã duyệt", "Approved");
                        txtToKhaiSo.Text = TKCT.SoToKhai.ToString();
                    }
                    else if (TKCT.SoToKhai == TrangThaiXuLy.CHO_DUYET)
                    {
                        showMsg("MSG_SEN04");
                        //this.ShowMessage("Hải quan chưa duyệt danh sách này", false);
                    }                   
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13",ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void setCommandStatus()
        {
            //if (TKCT.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            //{
            //    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            //    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cbLoaiHinh.ReadOnly = true;
            //    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    lbltrangthai.Text = "Đã duyệt";
            //}
            //else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            //{
            //    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            //    cbLoaiHinh.ReadOnly =false;
            //    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    lbltrangthai.Text = "Chưa khai báo";
            //}
            //else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            //{
            //    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            //    cbLoaiHinh.ReadOnly = true;
            //    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    lbltrangthai.Text = "Chờ duyệt";
            //}
            //if(boolFlag ==true )
            //    {
                    lbltrangthai.Text = setText("Đã duyệt", "Approved");
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                    //cbLoaiHinh.ReadOnly = true;
                    //NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True ;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //}
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["ID_NuocXX"].Text = this.Nuoc_GetName(e.Row.Cells["ID_NuocXX"].Text.ToString());
            e.Row.Cells["ID_DVT"].Text = this.DonViTinh_GetName(e.Row.Cells["ID_DVT"].Text.ToString());
        }
  
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType== RowType.Record)
            {
                HangChuyenTiepRegisterEditForm f = new HangChuyenTiepRegisterEditForm();
                f.HCT= (HangChuyenTiep)e.Row.DataRow;
                f.TKCT = this.TKCT;        
                if (this.HDGC.SoHopDong == "")
                {
                    this.HDGC.SoHopDong = TKCT.SoHopDongDV;
                    this.HDGC.MaDoanhNghiep = TKCT.MaDoanhNghiep;
                    this.HDGC.MaHaiQuan = TKCT.MaHaiQuanTiepNhan;
                    this.HDGC.NgayKy = TKCT.NgayHDDV;
                }
                f.HD = this.HDGC;
                f.MaLoaiHinh = cbLoaiHinh.Ma;
                f.MaHaiQuan = HDGC.MaHaiQuan;
                f.MaNguyenTe = nguyenTeControl1.Ma;
                f.ShowDialog();
                try
                {
                    dgList.DataSource = this.TKCT.HCTCollection;
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
            
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (TKCT.ID > 0)
            {
                if (TKCT.MaLoaiHinh.Contains("X"))
                {
                    if (PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(TKCT.ID))
                    {
                        MLMessages("Tờ khai chuyển tiếp có id = " + TKCT.ID + " này đã được phân bổ nên không thể xóa được.", "MSG_0203094", TKCT.ID.ToString(), false);
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    if (PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap((int)TKCT.SoToKhai, TKCT.MaLoaiHinh, TKCT.MaHaiQuanTiepNhan, (short)TKCT.NgayDangKy.Year,TKCT.IDHopDong))
                    {
                        MLMessages("Tờ khai chuyển tiếp có id= " + TKCT.ID + " đã được phân bổ nên không chỉnh sửa dữ liệu được.","MSG_0203095",TKCT.ID.ToString(), false);
                        e.Cancel = true;
                        return;
                    }
                }
            }
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {               
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        string LoaiHangHoa = "N";
                        if (TKCT.MaLoaiHinh.IndexOf("SP") > 0)
                            LoaiHangHoa = "S";
                        else if (TKCT.MaLoaiHinh.IndexOf("TB") > 0)
                            LoaiHangHoa = "T";
                        HangChuyenTiep hct = (HangChuyenTiep)i.GetRow().DataRow;
                        if (hct.ID > 0)
                        {
                            hct.Delete(TKCT.IDHopDong, LoaiHangHoa, TKCT.MaLoaiHinh);
                            if(!TKCT.MaLoaiHinh.EndsWith("X") && LoaiHangHoa=="N")
                            {
                                Company.GC.BLL.GC.NPLNhapTonThucTe npl = Company.GC.BLL.GC.NPLNhapTonThucTe.Load((int)TKCT.SoToKhai,TKCT.MaLoaiHinh, (short)TKCT.NgayDangKy.Year, TKCT.MaHaiQuanTiepNhan, hct.MaHang);
                                npl.Delete();
                            }
                        }
                    }
                }
                //long idTK = TKCT.ID;                
                //string st = "Nhập";
                //if (TKCT.MaLoaiHinh.EndsWith("X"))
                //    st = "Xuất";
                //if (TKCT.MaDoanhNghiep.Trim() == TKCT.MaKhachHang.Trim())
                //{
                //    string msg = "";
                //    if (TKCT.ID_Relation > 0)
                //    {
                //        msg = "Bạn có muốn cập nhật tờ khai " + st + "  đối xứng không ?";
                //        if (ShowMessage(msg, true) == "Yes")
                //        {
                //            CapNhatToKhaiDoiXung();
                //        }
                //    }
                //    else
                //    {
                //        msg = "Bạn có muốn tạo tờ khai " + st + " đối xứng không ?";
                //        if (ShowMessage(msg, true) == "Yes")
                //        {
                //            XoaToKhaiDoiXung();
                //        }
                //    }
                //}               
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void txtSoHongDonggiao_ButtonClick(object sender, EventArgs e)
        {
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.IsDaDuyet = true;
            f.ShowDialog();
            if (f.HopDongSelected.SoHopDong != "")
            {              
                txtSoHongDonggiao.Text = f.HopDongSelected.SoHopDong;
                ccNgayHopDongGiao.Value = f.HopDongSelected.NgayKy;
                ccNgayHHHopDongGiao.Value = f.HopDongSelected.NgayHetHan;
                HDGCDoiXung.ID = f.HopDongSelected.ID;
                txtMaDVGiao.Text = f.HopDongSelected.MaDoanhNghiep;
                txtTenDVGiao.Text = GlobalSettings.TEN_DON_VI;
            }
        }

        private void txtSoHongDonggiao_Leave(object sender, EventArgs e)
        {
          
        }

        private void ToKhaiGCChuyenTiepNhapForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }
    }
}