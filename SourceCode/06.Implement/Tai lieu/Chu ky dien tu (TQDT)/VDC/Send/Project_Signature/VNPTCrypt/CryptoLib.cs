using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.IO;

using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Ocsp;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Ocsp;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.Utilities.Collections;

namespace VNPTCrypt
{
    public enum HashType
    {
        SHA128 = 20,
        SHA256 = 32,
        SHA384 = 48,
        SHA512 = 64,
    }

    public class CryptoLib
    {
        public static bool VerifySign(byte[] CertData, byte[] OriginalData, byte[] Signeddata)
        {            
            X509Certificate2 _Cert = new X509Certificate2(CertData);
            SHA1Managed _Hash = new SHA1Managed();
            byte[] hashData = _Hash.ComputeHash(OriginalData);
            RSACryptoServiceProvider rsaCSP = (RSACryptoServiceProvider)_Cert.PublicKey.Key;
            bool VerifySign = rsaCSP.VerifyHash(hashData, CryptoConfig.MapNameToOID("SHA1"), Signeddata);             
            if (VerifySign)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static byte[] HASHSHA(byte[] OriginalData, HashType _HashType)
        {
            if (OriginalData == null)
            {
                return null;
            }
            if (_HashType == HashType.SHA128)
            {
                SHA1Managed hash = new SHA1Managed();
                return hash.ComputeHash(OriginalData);
            }
            if (_HashType == HashType.SHA256)
            {
                SHA256Managed hash = new SHA256Managed();
                return hash.ComputeHash(OriginalData);
            }
            if (_HashType == HashType.SHA384)
            {
                SHA384Managed hash = new SHA384Managed();
                return hash.ComputeHash(OriginalData);
            }
            if (_HashType == HashType.SHA512)
            {
                SHA512Managed hash = new SHA512Managed();
                return hash.ComputeHash(OriginalData);
            }
            return null;
        }
    }
}
