﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class T_KDT_THUPHI_BIENLAI : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MauBienLai { set; get; }
		public string SoSerial { set; get; }
		public string QuyenBienLai { set; get; }
		public string SoBienLai { set; get; }
		public DateTime NgayBienLai { set; get; }
		public string TrangThaiBienLai { set; get; }
		public decimal TongTien { set; get; }
		public string MaDoanhNghiepNP { set; get; }
		public string TenDoanhNghiepNP { set; get; }
		public string DiaChiDoanhNghiepNP { set; get; }
		public string MST { set; get; }
		public string TenNguoiNop { set; get; }
		public string SoToKhai { set; get; }
		public string NgayToKhai { set; get; }
		public string MaLoaiHinh { set; get; }
		public string NhomLoaiHinh { set; get; }
		public int MaPTVC { set; get; }
		public string MaDDLK { set; get; }
		public int ChoPhepLayHang { set; get; }
		public string DienGiai { set; get; }
		public string GuidString { set; get; }
        public List<T_KDT_THUPHI_BIENLAI_PHI> PhiCollection = new List<T_KDT_THUPHI_BIENLAI_PHI>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KDT_THUPHI_BIENLAI> ConvertToCollection(IDataReader reader)
		{
			List<T_KDT_THUPHI_BIENLAI> collection = new List<T_KDT_THUPHI_BIENLAI>();
			while (reader.Read())
			{
				T_KDT_THUPHI_BIENLAI entity = new T_KDT_THUPHI_BIENLAI();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MauBienLai"))) entity.MauBienLai = reader.GetString(reader.GetOrdinal("MauBienLai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoSerial"))) entity.SoSerial = reader.GetString(reader.GetOrdinal("SoSerial"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuyenBienLai"))) entity.QuyenBienLai = reader.GetString(reader.GetOrdinal("QuyenBienLai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiBienLai"))) entity.TrangThaiBienLai = reader.GetString(reader.GetOrdinal("TrangThaiBienLai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTien"))) entity.TongTien = reader.GetDecimal(reader.GetOrdinal("TongTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiepNP"))) entity.MaDoanhNghiepNP = reader.GetString(reader.GetOrdinal("MaDoanhNghiepNP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiepNP"))) entity.TenDoanhNghiepNP = reader.GetString(reader.GetOrdinal("TenDoanhNghiepNP"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoanhNghiepNP"))) entity.DiaChiDoanhNghiepNP = reader.GetString(reader.GetOrdinal("DiaChiDoanhNghiepNP"));
				if (!reader.IsDBNull(reader.GetOrdinal("MST"))) entity.MST = reader.GetString(reader.GetOrdinal("MST"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNop"))) entity.TenNguoiNop = reader.GetString(reader.GetOrdinal("TenNguoiNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetString(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayToKhai"))) entity.NgayToKhai = reader.GetString(reader.GetOrdinal("NgayToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhomLoaiHinh"))) entity.NhomLoaiHinh = reader.GetString(reader.GetOrdinal("NhomLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPTVC"))) entity.MaPTVC = reader.GetInt32(reader.GetOrdinal("MaPTVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDDLK"))) entity.MaDDLK = reader.GetString(reader.GetOrdinal("MaDDLK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChoPhepLayHang"))) entity.ChoPhepLayHang = reader.GetInt32(reader.GetOrdinal("ChoPhepLayHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("DienGiai"))) entity.DienGiai = reader.GetString(reader.GetOrdinal("DienGiai"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidString"))) entity.GuidString = reader.GetString(reader.GetOrdinal("GuidString"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KDT_THUPHI_BIENLAI> collection, long id)
        {
            foreach (T_KDT_THUPHI_BIENLAI item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KDT_THUPHI_BIENLAI VALUES(@MauBienLai, @SoSerial, @QuyenBienLai, @SoBienLai, @NgayBienLai, @TrangThaiBienLai, @TongTien, @MaDoanhNghiepNP, @TenDoanhNghiepNP, @DiaChiDoanhNghiepNP, @MST, @TenNguoiNop, @SoToKhai, @NgayToKhai, @MaLoaiHinh, @NhomLoaiHinh, @MaPTVC, @MaDDLK, @ChoPhepLayHang, @DienGiai, @GuidString)";
            string update = "UPDATE T_KDT_THUPHI_BIENLAI SET MauBienLai = @MauBienLai, SoSerial = @SoSerial, QuyenBienLai = @QuyenBienLai, SoBienLai = @SoBienLai, NgayBienLai = @NgayBienLai, TrangThaiBienLai = @TrangThaiBienLai, TongTien = @TongTien, MaDoanhNghiepNP = @MaDoanhNghiepNP, TenDoanhNghiepNP = @TenDoanhNghiepNP, DiaChiDoanhNghiepNP = @DiaChiDoanhNghiepNP, MST = @MST, TenNguoiNop = @TenNguoiNop, SoToKhai = @SoToKhai, NgayToKhai = @NgayToKhai, MaLoaiHinh = @MaLoaiHinh, NhomLoaiHinh = @NhomLoaiHinh, MaPTVC = @MaPTVC, MaDDLK = @MaDDLK, ChoPhepLayHang = @ChoPhepLayHang, DienGiai = @DienGiai, GuidString = @GuidString WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_THUPHI_BIENLAI WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MauBienLai", SqlDbType.NVarChar, "MauBienLai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSerial", SqlDbType.NVarChar, "SoSerial", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuyenBienLai", SqlDbType.NVarChar, "QuyenBienLai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoBienLai", SqlDbType.NVarChar, "SoBienLai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBienLai", SqlDbType.DateTime, "NgayBienLai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiBienLai", SqlDbType.NVarChar, "TrangThaiBienLai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTien", SqlDbType.Decimal, "TongTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiepNP", SqlDbType.NVarChar, "MaDoanhNghiepNP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiepNP", SqlDbType.NVarChar, "TenDoanhNghiepNP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoanhNghiepNP", SqlDbType.NVarChar, "DiaChiDoanhNghiepNP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MST", SqlDbType.NVarChar, "MST", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiNop", SqlDbType.NVarChar, "TenNguoiNop", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.NVarChar, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayToKhai", SqlDbType.NVarChar, "NgayToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.NVarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomLoaiHinh", SqlDbType.NVarChar, "NhomLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPTVC", SqlDbType.Int, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDDLK", SqlDbType.NVarChar, "MaDDLK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChoPhepLayHang", SqlDbType.Int, "ChoPhepLayHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DienGiai", SqlDbType.NVarChar, "DienGiai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidString", SqlDbType.VarChar, "GuidString", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MauBienLai", SqlDbType.NVarChar, "MauBienLai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSerial", SqlDbType.NVarChar, "SoSerial", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuyenBienLai", SqlDbType.NVarChar, "QuyenBienLai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoBienLai", SqlDbType.NVarChar, "SoBienLai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBienLai", SqlDbType.DateTime, "NgayBienLai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiBienLai", SqlDbType.NVarChar, "TrangThaiBienLai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTien", SqlDbType.Decimal, "TongTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiepNP", SqlDbType.NVarChar, "MaDoanhNghiepNP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiepNP", SqlDbType.NVarChar, "TenDoanhNghiepNP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoanhNghiepNP", SqlDbType.NVarChar, "DiaChiDoanhNghiepNP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MST", SqlDbType.NVarChar, "MST", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiNop", SqlDbType.NVarChar, "TenNguoiNop", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.NVarChar, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayToKhai", SqlDbType.NVarChar, "NgayToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.NVarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomLoaiHinh", SqlDbType.NVarChar, "NhomLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPTVC", SqlDbType.Int, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDDLK", SqlDbType.NVarChar, "MaDDLK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChoPhepLayHang", SqlDbType.Int, "ChoPhepLayHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DienGiai", SqlDbType.NVarChar, "DienGiai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidString", SqlDbType.VarChar, "GuidString", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KDT_THUPHI_BIENLAI VALUES(@MauBienLai, @SoSerial, @QuyenBienLai, @SoBienLai, @NgayBienLai, @TrangThaiBienLai, @TongTien, @MaDoanhNghiepNP, @TenDoanhNghiepNP, @DiaChiDoanhNghiepNP, @MST, @TenNguoiNop, @SoToKhai, @NgayToKhai, @MaLoaiHinh, @NhomLoaiHinh, @MaPTVC, @MaDDLK, @ChoPhepLayHang, @DienGiai, @GuidString)";
            string update = "UPDATE T_KDT_THUPHI_BIENLAI SET MauBienLai = @MauBienLai, SoSerial = @SoSerial, QuyenBienLai = @QuyenBienLai, SoBienLai = @SoBienLai, NgayBienLai = @NgayBienLai, TrangThaiBienLai = @TrangThaiBienLai, TongTien = @TongTien, MaDoanhNghiepNP = @MaDoanhNghiepNP, TenDoanhNghiepNP = @TenDoanhNghiepNP, DiaChiDoanhNghiepNP = @DiaChiDoanhNghiepNP, MST = @MST, TenNguoiNop = @TenNguoiNop, SoToKhai = @SoToKhai, NgayToKhai = @NgayToKhai, MaLoaiHinh = @MaLoaiHinh, NhomLoaiHinh = @NhomLoaiHinh, MaPTVC = @MaPTVC, MaDDLK = @MaDDLK, ChoPhepLayHang = @ChoPhepLayHang, DienGiai = @DienGiai, GuidString = @GuidString WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_THUPHI_BIENLAI WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MauBienLai", SqlDbType.NVarChar, "MauBienLai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSerial", SqlDbType.NVarChar, "SoSerial", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuyenBienLai", SqlDbType.NVarChar, "QuyenBienLai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoBienLai", SqlDbType.NVarChar, "SoBienLai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBienLai", SqlDbType.DateTime, "NgayBienLai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiBienLai", SqlDbType.NVarChar, "TrangThaiBienLai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTien", SqlDbType.Decimal, "TongTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiepNP", SqlDbType.NVarChar, "MaDoanhNghiepNP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiepNP", SqlDbType.NVarChar, "TenDoanhNghiepNP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoanhNghiepNP", SqlDbType.NVarChar, "DiaChiDoanhNghiepNP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MST", SqlDbType.NVarChar, "MST", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiNop", SqlDbType.NVarChar, "TenNguoiNop", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.NVarChar, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayToKhai", SqlDbType.NVarChar, "NgayToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.NVarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomLoaiHinh", SqlDbType.NVarChar, "NhomLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPTVC", SqlDbType.Int, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDDLK", SqlDbType.NVarChar, "MaDDLK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChoPhepLayHang", SqlDbType.Int, "ChoPhepLayHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DienGiai", SqlDbType.NVarChar, "DienGiai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidString", SqlDbType.VarChar, "GuidString", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MauBienLai", SqlDbType.NVarChar, "MauBienLai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSerial", SqlDbType.NVarChar, "SoSerial", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuyenBienLai", SqlDbType.NVarChar, "QuyenBienLai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoBienLai", SqlDbType.NVarChar, "SoBienLai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBienLai", SqlDbType.DateTime, "NgayBienLai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiBienLai", SqlDbType.NVarChar, "TrangThaiBienLai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTien", SqlDbType.Decimal, "TongTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiepNP", SqlDbType.NVarChar, "MaDoanhNghiepNP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiepNP", SqlDbType.NVarChar, "TenDoanhNghiepNP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoanhNghiepNP", SqlDbType.NVarChar, "DiaChiDoanhNghiepNP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MST", SqlDbType.NVarChar, "MST", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiNop", SqlDbType.NVarChar, "TenNguoiNop", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.NVarChar, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayToKhai", SqlDbType.NVarChar, "NgayToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.NVarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomLoaiHinh", SqlDbType.NVarChar, "NhomLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPTVC", SqlDbType.Int, "MaPTVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDDLK", SqlDbType.NVarChar, "MaDDLK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChoPhepLayHang", SqlDbType.Int, "ChoPhepLayHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DienGiai", SqlDbType.NVarChar, "DienGiai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidString", SqlDbType.VarChar, "GuidString", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KDT_THUPHI_BIENLAI Load(long id)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KDT_THUPHI_BIENLAI> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KDT_THUPHI_BIENLAI> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KDT_THUPHI_BIENLAI> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KDT_THUPHI_BIENLAI(string mauBienLai, string soSerial, string quyenBienLai, string soBienLai, DateTime ngayBienLai, string trangThaiBienLai, decimal tongTien, string maDoanhNghiepNP, string tenDoanhNghiepNP, string diaChiDoanhNghiepNP, string mST, string tenNguoiNop, string soToKhai, string ngayToKhai, string maLoaiHinh, string nhomLoaiHinh, int maPTVC, string maDDLK, int choPhepLayHang, string dienGiai, string guidString)
		{
			T_KDT_THUPHI_BIENLAI entity = new T_KDT_THUPHI_BIENLAI();	
			entity.MauBienLai = mauBienLai;
			entity.SoSerial = soSerial;
			entity.QuyenBienLai = quyenBienLai;
			entity.SoBienLai = soBienLai;
			entity.NgayBienLai = ngayBienLai;
			entity.TrangThaiBienLai = trangThaiBienLai;
			entity.TongTien = tongTien;
			entity.MaDoanhNghiepNP = maDoanhNghiepNP;
			entity.TenDoanhNghiepNP = tenDoanhNghiepNP;
			entity.DiaChiDoanhNghiepNP = diaChiDoanhNghiepNP;
			entity.MST = mST;
			entity.TenNguoiNop = tenNguoiNop;
			entity.SoToKhai = soToKhai;
			entity.NgayToKhai = ngayToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NhomLoaiHinh = nhomLoaiHinh;
			entity.MaPTVC = maPTVC;
			entity.MaDDLK = maDDLK;
			entity.ChoPhepLayHang = choPhepLayHang;
			entity.DienGiai = dienGiai;
			entity.GuidString = guidString;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MauBienLai", SqlDbType.NVarChar, MauBienLai);
			db.AddInParameter(dbCommand, "@SoSerial", SqlDbType.NVarChar, SoSerial);
			db.AddInParameter(dbCommand, "@QuyenBienLai", SqlDbType.NVarChar, QuyenBienLai);
			db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NVarChar, SoBienLai);
			db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai.Year <= 1753 ? DBNull.Value : (object) NgayBienLai);
			db.AddInParameter(dbCommand, "@TrangThaiBienLai", SqlDbType.NVarChar, TrangThaiBienLai);
			db.AddInParameter(dbCommand, "@TongTien", SqlDbType.Decimal, TongTien);
			db.AddInParameter(dbCommand, "@MaDoanhNghiepNP", SqlDbType.NVarChar, MaDoanhNghiepNP);
			db.AddInParameter(dbCommand, "@TenDoanhNghiepNP", SqlDbType.NVarChar, TenDoanhNghiepNP);
			db.AddInParameter(dbCommand, "@DiaChiDoanhNghiepNP", SqlDbType.NVarChar, DiaChiDoanhNghiepNP);
			db.AddInParameter(dbCommand, "@MST", SqlDbType.NVarChar, MST);
			db.AddInParameter(dbCommand, "@TenNguoiNop", SqlDbType.NVarChar, TenNguoiNop);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.NVarChar, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayToKhai", SqlDbType.NVarChar, NgayToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NhomLoaiHinh", SqlDbType.NVarChar, NhomLoaiHinh);
			db.AddInParameter(dbCommand, "@MaPTVC", SqlDbType.Int, MaPTVC);
			db.AddInParameter(dbCommand, "@MaDDLK", SqlDbType.NVarChar, MaDDLK);
			db.AddInParameter(dbCommand, "@ChoPhepLayHang", SqlDbType.Int, ChoPhepLayHang);
			db.AddInParameter(dbCommand, "@DienGiai", SqlDbType.NVarChar, DienGiai);
			db.AddInParameter(dbCommand, "@GuidString", SqlDbType.VarChar, GuidString);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KDT_THUPHI_BIENLAI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_BIENLAI item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KDT_THUPHI_BIENLAI(long id, string mauBienLai, string soSerial, string quyenBienLai, string soBienLai, DateTime ngayBienLai, string trangThaiBienLai, decimal tongTien, string maDoanhNghiepNP, string tenDoanhNghiepNP, string diaChiDoanhNghiepNP, string mST, string tenNguoiNop, string soToKhai, string ngayToKhai, string maLoaiHinh, string nhomLoaiHinh, int maPTVC, string maDDLK, int choPhepLayHang, string dienGiai, string guidString)
		{
			T_KDT_THUPHI_BIENLAI entity = new T_KDT_THUPHI_BIENLAI();			
			entity.ID = id;
			entity.MauBienLai = mauBienLai;
			entity.SoSerial = soSerial;
			entity.QuyenBienLai = quyenBienLai;
			entity.SoBienLai = soBienLai;
			entity.NgayBienLai = ngayBienLai;
			entity.TrangThaiBienLai = trangThaiBienLai;
			entity.TongTien = tongTien;
			entity.MaDoanhNghiepNP = maDoanhNghiepNP;
			entity.TenDoanhNghiepNP = tenDoanhNghiepNP;
			entity.DiaChiDoanhNghiepNP = diaChiDoanhNghiepNP;
			entity.MST = mST;
			entity.TenNguoiNop = tenNguoiNop;
			entity.SoToKhai = soToKhai;
			entity.NgayToKhai = ngayToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NhomLoaiHinh = nhomLoaiHinh;
			entity.MaPTVC = maPTVC;
			entity.MaDDLK = maDDLK;
			entity.ChoPhepLayHang = choPhepLayHang;
			entity.DienGiai = dienGiai;
			entity.GuidString = guidString;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KDT_THUPHI_BIENLAI_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MauBienLai", SqlDbType.NVarChar, MauBienLai);
			db.AddInParameter(dbCommand, "@SoSerial", SqlDbType.NVarChar, SoSerial);
			db.AddInParameter(dbCommand, "@QuyenBienLai", SqlDbType.NVarChar, QuyenBienLai);
			db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NVarChar, SoBienLai);
			db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai.Year <= 1753 ? DBNull.Value : (object) NgayBienLai);
			db.AddInParameter(dbCommand, "@TrangThaiBienLai", SqlDbType.NVarChar, TrangThaiBienLai);
			db.AddInParameter(dbCommand, "@TongTien", SqlDbType.Decimal, TongTien);
			db.AddInParameter(dbCommand, "@MaDoanhNghiepNP", SqlDbType.NVarChar, MaDoanhNghiepNP);
			db.AddInParameter(dbCommand, "@TenDoanhNghiepNP", SqlDbType.NVarChar, TenDoanhNghiepNP);
			db.AddInParameter(dbCommand, "@DiaChiDoanhNghiepNP", SqlDbType.NVarChar, DiaChiDoanhNghiepNP);
			db.AddInParameter(dbCommand, "@MST", SqlDbType.NVarChar, MST);
			db.AddInParameter(dbCommand, "@TenNguoiNop", SqlDbType.NVarChar, TenNguoiNop);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.NVarChar, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayToKhai", SqlDbType.NVarChar, NgayToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NhomLoaiHinh", SqlDbType.NVarChar, NhomLoaiHinh);
			db.AddInParameter(dbCommand, "@MaPTVC", SqlDbType.Int, MaPTVC);
			db.AddInParameter(dbCommand, "@MaDDLK", SqlDbType.NVarChar, MaDDLK);
			db.AddInParameter(dbCommand, "@ChoPhepLayHang", SqlDbType.Int, ChoPhepLayHang);
			db.AddInParameter(dbCommand, "@DienGiai", SqlDbType.NVarChar, DienGiai);
			db.AddInParameter(dbCommand, "@GuidString", SqlDbType.VarChar, GuidString);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KDT_THUPHI_BIENLAI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_BIENLAI item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KDT_THUPHI_BIENLAI(long id, string mauBienLai, string soSerial, string quyenBienLai, string soBienLai, DateTime ngayBienLai, string trangThaiBienLai, decimal tongTien, string maDoanhNghiepNP, string tenDoanhNghiepNP, string diaChiDoanhNghiepNP, string mST, string tenNguoiNop, string soToKhai, string ngayToKhai, string maLoaiHinh, string nhomLoaiHinh, int maPTVC, string maDDLK, int choPhepLayHang, string dienGiai, string guidString)
		{
			T_KDT_THUPHI_BIENLAI entity = new T_KDT_THUPHI_BIENLAI();			
			entity.ID = id;
			entity.MauBienLai = mauBienLai;
			entity.SoSerial = soSerial;
			entity.QuyenBienLai = quyenBienLai;
			entity.SoBienLai = soBienLai;
			entity.NgayBienLai = ngayBienLai;
			entity.TrangThaiBienLai = trangThaiBienLai;
			entity.TongTien = tongTien;
			entity.MaDoanhNghiepNP = maDoanhNghiepNP;
			entity.TenDoanhNghiepNP = tenDoanhNghiepNP;
			entity.DiaChiDoanhNghiepNP = diaChiDoanhNghiepNP;
			entity.MST = mST;
			entity.TenNguoiNop = tenNguoiNop;
			entity.SoToKhai = soToKhai;
			entity.NgayToKhai = ngayToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NhomLoaiHinh = nhomLoaiHinh;
			entity.MaPTVC = maPTVC;
			entity.MaDDLK = maDDLK;
			entity.ChoPhepLayHang = choPhepLayHang;
			entity.DienGiai = dienGiai;
			entity.GuidString = guidString;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MauBienLai", SqlDbType.NVarChar, MauBienLai);
			db.AddInParameter(dbCommand, "@SoSerial", SqlDbType.NVarChar, SoSerial);
			db.AddInParameter(dbCommand, "@QuyenBienLai", SqlDbType.NVarChar, QuyenBienLai);
			db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NVarChar, SoBienLai);
			db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai.Year <= 1753 ? DBNull.Value : (object) NgayBienLai);
			db.AddInParameter(dbCommand, "@TrangThaiBienLai", SqlDbType.NVarChar, TrangThaiBienLai);
			db.AddInParameter(dbCommand, "@TongTien", SqlDbType.Decimal, TongTien);
			db.AddInParameter(dbCommand, "@MaDoanhNghiepNP", SqlDbType.NVarChar, MaDoanhNghiepNP);
			db.AddInParameter(dbCommand, "@TenDoanhNghiepNP", SqlDbType.NVarChar, TenDoanhNghiepNP);
			db.AddInParameter(dbCommand, "@DiaChiDoanhNghiepNP", SqlDbType.NVarChar, DiaChiDoanhNghiepNP);
			db.AddInParameter(dbCommand, "@MST", SqlDbType.NVarChar, MST);
			db.AddInParameter(dbCommand, "@TenNguoiNop", SqlDbType.NVarChar, TenNguoiNop);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.NVarChar, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayToKhai", SqlDbType.NVarChar, NgayToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NVarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NhomLoaiHinh", SqlDbType.NVarChar, NhomLoaiHinh);
			db.AddInParameter(dbCommand, "@MaPTVC", SqlDbType.Int, MaPTVC);
			db.AddInParameter(dbCommand, "@MaDDLK", SqlDbType.NVarChar, MaDDLK);
			db.AddInParameter(dbCommand, "@ChoPhepLayHang", SqlDbType.Int, ChoPhepLayHang);
			db.AddInParameter(dbCommand, "@DienGiai", SqlDbType.NVarChar, DienGiai);
			db.AddInParameter(dbCommand, "@GuidString", SqlDbType.VarChar, GuidString);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KDT_THUPHI_BIENLAI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_BIENLAI item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KDT_THUPHI_BIENLAI(long id)
		{
			T_KDT_THUPHI_BIENLAI entity = new T_KDT_THUPHI_BIENLAI();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KDT_THUPHI_BIENLAI_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KDT_THUPHI_BIENLAI> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_THUPHI_BIENLAI item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert(transaction);
                    }
                    else
                    {
                        this.Update(transaction);
                    }

                    foreach (T_KDT_THUPHI_BIENLAI_PHI item in PhiCollection)
                    {
                        item.BL_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
	}	
}