﻿using System;
using Company.KD.BLL;
using Company.KD.BLL.KDT.SXXK;
using Company.KD.BLL.Utils;
using Company.KD.BLL.DuLieuChuan;
using System.Data;
using Company.QuanTri;
using System.Text;
using System.Security.Cryptography;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;

namespace Company.Interface.QuanTri
{
    public partial class NguoiDungEditForm : BaseForm
    {
        public User user = new User();
        public NguoiDungEditForm()
        {
            InitializeComponent();
        }
               

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                cvError.Validate();
                if (!cvError.IsValid)
                    return;
                if (user.ID == 0)
                {
                    if (txtMatKhau.Text.Length == 0)
                    {
                        epError.SetError(txtMatKhau,setText( "Mật khẩu bắt buộc phải nhập","This field must be enter"));
                        epError.SetIconPadding(txtMatKhau, -8);
                        return;
                    }
                    if (editBox1.Text.Length == 0)
                    {
                        epError.SetError(editBox1, setText("Mật khẩu nhập lại bắt buộc phải nhập", "This field must be enter"));
                        epError.SetIconPadding(editBox1, -8);
                        return;
                    }                   
                }
                else
                {
                    if (txtMatKhau.Text.Trim().Length > 0)
                    {
                        if (editBox1.Text.Trim().ToUpper() != txtMatKhau.Text.Trim().ToUpper())
                        {
                            epError.SetError(editBox1,setText( "Mật khẩu không giống nhau"," This value must be the same the first value"));
                            epError.SetIconPadding(editBox1, -8);
                            return;
                        }
                    }
                }
                if (user.CheckUserName(txtUser.Text.Trim()))
                {
                    MLMessages("Tên người dùng này đã có.Bạn hãy nhập tên khác","MSG_SAV10","", false);
                    return;
                }
                user.USER_NAME = txtUser.Text.Trim();
                user.HO_TEN = txtHoTen.Text.Trim();
                user.MO_TA = txtMoTa.Text.Trim();
                if (user.ID == 0)
                    user.PASSWORD = EncryptPassword(txtMatKhau.Text.Trim());
                else
                {
                    if(txtMatKhau.Text.Trim().Length>0)
                        user.PASSWORD = EncryptPassword(txtMatKhau.Text.Trim());
                }
                user.RoleList.Clear();
                if (!user.isAdmin)
                {                    
                    foreach (GridEXRow row in dgList.GetCheckedRows())
                    {
                        GROUPS group = (GROUPS)row.DataRow;
                        user.RoleList.Add(group.MA_NHOM);
                    }
                }
                user.InsertUpdateFull();                
                this.Close();
            }
            catch(Exception ex)
            {
                ShowMessage("Lỗi : "+ex.Message, false);
            }
        }
        public string EncryptPassword(string password)
        {
            UnicodeEncoding encoding = new UnicodeEncoding();
            byte[] hashBytes = encoding.GetBytes(password);

            // compute SHA-1 hash.
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] bytePassword = sha1.ComputeHash(hashBytes);
            string cryptPassword = Convert.ToBase64String(bytePassword);
            return cryptPassword;
        }

        private void NguoiDungEditForm_Load(object sender, EventArgs e)
        {
            txtHoTen.Text = user.HO_TEN;
            txtMoTa.Text = user.MO_TA;
            txtUser.Text = user.USER_NAME;
            user.LoadRoleList();
            GROUPS g = new GROUPS();
            GROUPSCollection collection = g.SelectCollectionAll();
            for (int i = 0; i < user.RoleList.Count; ++i)
            {
                foreach (GROUPS row in collection)
                {
                    if (row.MA_NHOM.ToString() ==user.RoleList[i].ToString())
                    {
                        row.Check = true;
                        break;
                    }
                }
            }
            dgList.DataSource = collection;           
            if (user.ID > 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.UpdateUser)))
                {
                    btnUpdate.Visible = false;
                }
            }
            else
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.CreateUser)))
                {
                    btnUpdate.Visible = false;
                }
            }
            if (user.isAdmin)
            {
                txtUser.ReadOnly = false;
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["CheckData"].Value.ToString() == "True")
            {
                e.Row.CheckState = RowCheckState.Checked;
            }
            else
                e.Row.CheckState = RowCheckState.Unchecked;
        }
      
    }
}