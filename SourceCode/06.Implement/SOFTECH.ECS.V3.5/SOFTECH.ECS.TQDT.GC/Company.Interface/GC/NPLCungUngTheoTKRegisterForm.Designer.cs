﻿namespace Company.Interface.KDT.GC
{
    partial class NPLCungUngTheoTKRegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NPLCungUngTheoTKRegisterForm));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNgayHetHanHD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNgayKyHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grbToKhai = new Janus.Windows.EditControls.UIGroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ctrHQTiepNhanTK = new Company.Interface.Controls.DonViHaiQuanNewControl();
            this.txtLoaiHinh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamDangKyTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSoTiepNhanTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cmMainNPLCungUng = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar2 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdAddSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAddSP");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdAddSP = new Janus.Windows.UI.CommandBars.UICommand("cmdAddSP");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.label8 = new System.Windows.Forms.Label();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.compareValidator1 = new Company.Controls.CustomValidation.CompareValidator();
            this.rfvSoTiepNhanTK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbToKhai)).BeginInit();
            this.grbToKhai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMainNPLCungUng)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compareValidator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoTiepNhanTK)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.label8);
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Controls.Add(this.grbToKhai);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Location = new System.Drawing.Point(0, 32);
            this.grbMain.Size = new System.Drawing.Size(719, 418);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.txtNgayHetHanHD);
            this.uiGroupBox2.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox2.Controls.Add(this.txtNgayKyHopDong);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 3);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(713, 55);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Hợp Đồng";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số hợp đồng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(478, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ngày hết hạn";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(278, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ngày ký";
            // 
            // txtNgayHetHanHD
            // 
            this.txtNgayHetHanHD.BackColor = System.Drawing.Color.White;
            this.txtNgayHetHanHD.Location = new System.Drawing.Point(575, 24);
            this.txtNgayHetHanHD.Name = "txtNgayHetHanHD";
            this.txtNgayHetHanHD.ReadOnly = true;
            this.txtNgayHetHanHD.Size = new System.Drawing.Size(101, 21);
            this.txtNgayHetHanHD.TabIndex = 5;
            this.txtNgayHetHanHD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNgayHetHanHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.BackColor = System.Drawing.Color.White;
            this.txtSoHopDong.Location = new System.Drawing.Point(122, 24);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.ReadOnly = true;
            this.txtSoHopDong.Size = new System.Drawing.Size(112, 21);
            this.txtSoHopDong.TabIndex = 5;
            this.txtSoHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtNgayKyHopDong
            // 
            this.txtNgayKyHopDong.BackColor = System.Drawing.Color.White;
            this.txtNgayKyHopDong.Location = new System.Drawing.Point(336, 24);
            this.txtNgayKyHopDong.Name = "txtNgayKyHopDong";
            this.txtNgayKyHopDong.ReadOnly = true;
            this.txtNgayKyHopDong.Size = new System.Drawing.Size(101, 21);
            this.txtNgayKyHopDong.TabIndex = 5;
            this.txtNgayKyHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNgayKyHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // grbToKhai
            // 
            this.grbToKhai.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grbToKhai.BackColor = System.Drawing.Color.Transparent;
            this.grbToKhai.Controls.Add(this.label11);
            this.grbToKhai.Controls.Add(this.ctrHQTiepNhanTK);
            this.grbToKhai.Controls.Add(this.txtLoaiHinh);
            this.grbToKhai.Controls.Add(this.txtNamDangKyTK);
            this.grbToKhai.Controls.Add(this.label7);
            this.grbToKhai.Controls.Add(this.label4);
            this.grbToKhai.Controls.Add(this.label5);
            this.grbToKhai.Controls.Add(this.txtSoTiepNhanTK);
            this.grbToKhai.Controls.Add(this.label6);
            this.grbToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbToKhai.Location = new System.Drawing.Point(3, 64);
            this.grbToKhai.Name = "grbToKhai";
            this.grbToKhai.Size = new System.Drawing.Size(713, 114);
            this.grbToKhai.TabIndex = 1;
            this.grbToKhai.Text = "Tờ khai xuất GC";
            this.grbToKhai.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbToKhai.VisualStyleManager = this.vsmMain;
            this.grbToKhai.Click += new System.EventHandler(this.grbToKhai_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(237, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "* ";
            // 
            // ctrHQTiepNhanTK
            // 
            this.ctrHQTiepNhanTK.BackColor = System.Drawing.Color.Transparent;
            this.ctrHQTiepNhanTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrHQTiepNhanTK.Location = new System.Drawing.Point(363, 21);
            this.ctrHQTiepNhanTK.Ma = "";
            this.ctrHQTiepNhanTK.MaCuc = "";
            this.ctrHQTiepNhanTK.Name = "ctrHQTiepNhanTK";
            this.ctrHQTiepNhanTK.ReadOnly = true;
            this.ctrHQTiepNhanTK.Size = new System.Drawing.Size(313, 22);
            this.ctrHQTiepNhanTK.TabIndex = 10;
            this.ctrHQTiepNhanTK.Ten = "";
            this.ctrHQTiepNhanTK.VisualStyleManager = null;
            // 
            // txtLoaiHinh
            // 
            this.txtLoaiHinh.BackColor = System.Drawing.Color.White;
            this.txtLoaiHinh.Location = new System.Drawing.Point(363, 58);
            this.txtLoaiHinh.Name = "txtLoaiHinh";
            this.txtLoaiHinh.ReadOnly = true;
            this.txtLoaiHinh.Size = new System.Drawing.Size(232, 21);
            this.txtLoaiHinh.TabIndex = 7;
            this.txtLoaiHinh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLoaiHinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtLoaiHinh.VisualStyleManager = this.vsmMain;
            // 
            // txtNamDangKyTK
            // 
            this.txtNamDangKyTK.BackColor = System.Drawing.Color.White;
            this.txtNamDangKyTK.Location = new System.Drawing.Point(121, 58);
            this.txtNamDangKyTK.Name = "txtNamDangKyTK";
            this.txtNamDangKyTK.ReadOnly = true;
            this.txtNamDangKyTK.Size = new System.Drawing.Size(68, 21);
            this.txtNamDangKyTK.TabIndex = 5;
            this.txtNamDangKyTK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNamDangKyTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtNamDangKyTK.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(278, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "HQ tiếp nhận";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(278, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Loại hình";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(30, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Năm đăng ký";
            // 
            // txtSoTiepNhanTK
            // 
            this.txtSoTiepNhanTK.BackColor = System.Drawing.Color.White;
            this.txtSoTiepNhanTK.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoTiepNhanTK.Location = new System.Drawing.Point(122, 21);
            this.txtSoTiepNhanTK.Name = "txtSoTiepNhanTK";
            this.txtSoTiepNhanTK.ReadOnly = true;
            this.txtSoTiepNhanTK.Size = new System.Drawing.Size(112, 21);
            this.txtSoTiepNhanTK.TabIndex = 1;
            this.txtSoTiepNhanTK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhanTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoTiepNhanTK.VisualStyleManager = this.vsmMain;
            this.txtSoTiepNhanTK.ButtonClick += new System.EventHandler(this.txtSoTiepNhanTK_ButtonClick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(30, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Số tiếp nhận";
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(3, 198);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(713, 217);
            this.dgList.TabIndex = 3;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.dgList_DeletingRecord);
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ImageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // cmMainNPLCungUng
            // 
            this.cmMainNPLCungUng.BottomRebar = this.BottomRebar1;
            this.cmMainNPLCungUng.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar2});
            this.cmMainNPLCungUng.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave,
            this.cmdAddSP});
            this.cmMainNPLCungUng.ContainerControl = this;
            this.cmMainNPLCungUng.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMainNPLCungUng.LeftRebar = this.LeftRebar1;
            this.cmMainNPLCungUng.RightRebar = this.RightRebar1;
            this.cmMainNPLCungUng.ShowShortcutInToolTips = true;
            this.cmMainNPLCungUng.Tag = null;
            this.cmMainNPLCungUng.TopRebar = this.TopRebar1;
            this.cmMainNPLCungUng.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMainNPLCungUng.VisualStyleManager = this.vsmMain;
            this.cmMainNPLCungUng.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMainNPLCungUng_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMainNPLCungUng;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar2
            // 
            this.uiCommandBar2.CommandManager = this.cmMainNPLCungUng;
            this.uiCommandBar2.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdAddSP1,
            this.cmdSave1,
            this.Separator1});
            this.uiCommandBar2.Key = "CommandBar1";
            this.uiCommandBar2.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar2.Name = "uiCommandBar2";
            this.uiCommandBar2.RowIndex = 0;
            this.uiCommandBar2.Size = new System.Drawing.Size(237, 32);
            this.uiCommandBar2.Text = "CommandBar1";
            // 
            // cmdAddSP1
            // 
            this.cmdAddSP1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAddSP1.Icon")));
            this.cmdAddSP1.Key = "cmdAddSP";
            this.cmdAddSP1.Name = "cmdAddSP1";
            this.cmdAddSP1.Shortcut = System.Windows.Forms.Shortcut.CtrlM;
            this.cmdAddSP1.Text = "&Thêm mới SP";
            this.cmdAddSP1.ToolTipText = "Thêm mới SP";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSave1.Icon")));
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            this.cmdSave1.Shortcut = System.Windows.Forms.Shortcut.CtrlL;
            this.cmdSave1.Text = "&Lưu thông tin";
            this.cmdSave1.ToolTipText = "Lưu thông tin";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdSave
            // 
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "Lưu";
            // 
            // cmdAddSP
            // 
            this.cmdAddSP.Key = "cmdAddSP";
            this.cmdAddSP.Name = "cmdAddSP";
            this.cmdAddSP.Text = "Thêm mới SP";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMainNPLCungUng;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMainNPLCungUng;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar2});
            this.TopRebar1.CommandManager = this.cmMainNPLCungUng;
            this.TopRebar1.Controls.Add(this.uiCommandBar2);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(719, 32);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = null;
            this.uiCommandBar1.Key = "";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.Size = new System.Drawing.Size(28, 26);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(0, 181);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(215, 14);
            this.label8.TabIndex = 2;
            this.label8.Text = "Danh sách sản phẩm tự cung ứng";
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // compareValidator1
            // 
            this.compareValidator1.ControlToValidate = this.txtSoTiepNhanTK;
            this.compareValidator1.ErrorMessage = "\"Số tiếp nhận tờ khai\" không hợp lệ.";
            this.compareValidator1.Icon = ((System.Drawing.Icon)(resources.GetObject("compareValidator1.Icon")));
            this.compareValidator1.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.compareValidator1.Tag = "compareValidator1";
            this.compareValidator1.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.compareValidator1.ValueToCompare = "0";
            // 
            // rfvSoTiepNhanTK
            // 
            this.rfvSoTiepNhanTK.ControlToValidate = this.txtSoTiepNhanTK;
            this.rfvSoTiepNhanTK.ErrorMessage = "\"Số tiếp nhận\" không được để trống.";
            this.rfvSoTiepNhanTK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoTiepNhanTK.Icon")));
            this.rfvSoTiepNhanTK.Tag = "rfvSoTiepNhanTK";
            // 
            // NPLCungUngTheoTKRegisterForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(719, 450);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "NPLCungUngTheoTKRegisterForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Khai báo NPL cung ứng theo tờ khai xuất";
            this.Load += new System.EventHandler(this.NPLCungUngTheoTKSendForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NPLCungUngTheoTKSendForm_FormClosing);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbToKhai)).EndInit();
            this.grbToKhai.ResumeLayout(false);
            this.grbToKhai.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMainNPLCungUng)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compareValidator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoTiepNhanTK)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox grbToKhai;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhanTK;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtNamDangKyTK;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.GridEX dgList;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMainNPLCungUng;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar2;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddSP1;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddSP;
        private Janus.Windows.UI.CommandBars.UICommand Separator1;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiHinh;
        private Company.Interface.Controls.DonViHaiQuanNewControl ctrHQTiepNhanTK;
        private System.Windows.Forms.Label label11;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.CompareValidator compareValidator1;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoTiepNhanTK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayHetHanHD;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayKyHopDong;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
    }
}