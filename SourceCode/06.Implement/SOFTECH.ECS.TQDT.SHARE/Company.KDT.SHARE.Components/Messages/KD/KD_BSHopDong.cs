﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
   public class KD_BSHopDong:DeclarationBase
    {
       [XmlElement("DeclarationDocument")]
       public DeclarationBase DeclarationDocument { get; set; }
       [XmlArray("ContractDocuments")]
       [XmlArrayItem("ContractDocument")]
       public List<ContractDocument> ContractDocuments { get; set; }
    }
}
