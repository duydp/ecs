using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Company.KDT.SHARE.Components.Feedback.Components;

namespace Company.Interface.Feedback
{
    public partial class LinhVucUC : DevExpress.XtraEditors.XtraUserControl
    {
        private DataTable dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["LinhVuc"];
        public LinhVucUC()
        {
            InitializeComponent();
        }
        private void LoadData()
        {
            try
            {
                if (dt == null)
                    dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["LinhVuc"];

                if (dt != null)
                {
                    TrlLinhvuc.DataSource = dt;
                    //TrlLinhvuc.DataMember = "Ten";
                    //TrlLinhvuc.PreviewFieldName = "Ten";
                    TrlLinhvuc.KeyFieldName = "Ma";
                    TrlLinhvuc.ParentFieldName = "ParentID";
                    
                    //DataView dv = dt.Copy().DefaultView;
                    //dv.RowFilter = "HienThi = 1";
                    //dv.Sort = "ThuTu asc";

                    //dgList.DataSource = dv;

                   // CheckDefault();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public void ReLoad()
        {
            try
            {
                Company.KDT.SHARE.Components.Feedback.Components.FeedbackGlobalSettings.LamMoiDanhMucLinhVuc();

                dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["LinhVuc"];

                LoadData();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void LinhVucUC_Load(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}
