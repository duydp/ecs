using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class BanKhaiThongTinHoaDon_2 : DevExpress.XtraReports.UI.XtraReport
    {
        public int spl;
        public BanKhaiThongTinHoaDon_2()
        {
            InitializeComponent();
        }
        public void BindReport(VAL0870_IVA Val087)
        {
            try
            {
                if (Val087.HangHoaTrongHoaDon != null && Val087.HangHoaTrongHoaDon.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("SoDong", typeof(string));
                    dt.Columns.Add("MaHangHoa", typeof(string));
                    dt.Columns.Add("MaSoHangHoa", typeof(string));
                    dt.Columns.Add("MoTaHangHoa", typeof(string));
                    dt.Columns.Add("MaNuocXuatXu", typeof(string));
                    dt.Columns.Add("TenNuocXuatXu", typeof(string));
                    dt.Columns.Add("SoKienHang", typeof(string));
                    dt.Columns.Add("SoLuong1", typeof(string));
                    dt.Columns.Add("MaDVTSoLuong1", typeof(string));
                    dt.Columns.Add("SoLuong2", typeof(string));
                    dt.Columns.Add("MaDVTSoLuong2", typeof(string));
                    dt.Columns.Add("DonGiaHoaDon", typeof(string));
                    dt.Columns.Add("MTTDonGiaHoaDon", typeof(string));
                    dt.Columns.Add("DonViDonGiaHoaDon", typeof(string));
                    dt.Columns.Add("TriGiaHoaDon", typeof(string));
                    dt.Columns.Add("MTTGiaTien", typeof(string));
                    dt.Columns.Add("LoaiKhauTru_Hang", typeof(string));
                    dt.Columns.Add("SoTienKhauTru_Hang", typeof(string));
                    dt.Columns.Add("MTTSoTienKhauTru_Hang", typeof(string));

                    int begin = spl * 4 - 2;
                    int end = spl * 4 + 2;
                    for (int i = begin ; i < end; i++)
                    {
                        if (i <= Val087.HangHoaTrongHoaDon.Count)
                        {
                            DataRow dr = dt.NewRow();
                            dr["SoDong"] = Val087.HangHoaTrongHoaDon[i].RNO.Value.ToString().Trim();
                            dr["MaHangHoa"] = Val087.HangHoaTrongHoaDon[i].SNO.Value.ToString().Trim();
                            dr["MaSoHangHoa"] = Val087.HangHoaTrongHoaDon[i].CMD.Value.ToString().Trim();
                            dr["MoTaHangHoa"] = Val087.HangHoaTrongHoaDon[i].CMN.Value.ToString().Trim();
                            dr["MaNuocXuatXu"] = Val087.HangHoaTrongHoaDon[i].ORC.Value.ToString().Trim();
                            dr["TenNuocXuatXu"] = Val087.HangHoaTrongHoaDon[i].ORN.Value.ToString().Trim();
                            dr["SoKienHang"] = Val087.HangHoaTrongHoaDon[i].KBN.Value.ToString().Trim();
                            dr["SoLuong1"] = Val087.HangHoaTrongHoaDon[i].QN1.Value.ToString().Trim();
                            dr["MaDVTSoLuong1"] = Val087.HangHoaTrongHoaDon[i].QT1.Value.ToString().Trim();
                            dr["SoLuong2"] = Val087.HangHoaTrongHoaDon[i].QN2.Value.ToString().Trim();
                            dr["MaDVTSoLuong2"] = Val087.HangHoaTrongHoaDon[i].QT2.Value.ToString().Trim();
                            dr["DonGiaHoaDon"] = Val087.HangHoaTrongHoaDon[i].TNK.Value.ToString().Trim();
                            dr["MTTDonGiaHoaDon"] = Val087.HangHoaTrongHoaDon[i].TNC.Value.ToString().Trim();
                            dr["DonViDonGiaHoaDon"] = Val087.HangHoaTrongHoaDon[i].TSC.Value.ToString().Trim();
                            dr["TriGiaHoaDon"] = Val087.HangHoaTrongHoaDon[i].KKT.Value.ToString().Trim();
                            dr["MTTGiaTien"] = Val087.HangHoaTrongHoaDon[i].KKC.Value.ToString().Trim();
                            dr["LoaiKhauTru_Hang"] = Val087.HangHoaTrongHoaDon[i].NSR.Value.ToString().Trim();
                            dr["SoTienKhauTru_Hang"] = Val087.HangHoaTrongHoaDon[i].NBG.Value.ToString().Trim();
                            dr["MTTSoTienKhauTru_Hang"] = Val087.HangHoaTrongHoaDon[i].NBC.Value.ToString().Trim();
                            dt.Rows.Add(dr);
                        }
                    }
                    BindReportHang(dt);

                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
           
           
        }
        private void BindReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSoDong.DataBindings.Add("Text", DetailReport.DataSource, "SoDong"); 
            lblMaHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MaHangHoa");
            lblMaSoHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MaSoHangHoa");
            lblMoTaHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MoTaHangHoa");
            lblMaNuocXuatXu.DataBindings.Add("Text", DetailReport.DataSource, "MaNuocXuatXu");
            lblTenNuocXuatXu.DataBindings.Add("Text", DetailReport.DataSource, "TenNuocXuatXu");
            lblSoKienHang.DataBindings.Add("Text", DetailReport.DataSource, "SoKienHang");
            lblSoLuong1.DataBindings.Add("Text", DetailReport.DataSource, "SoLuong1");
            lblMaDVTSoLuong1.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTSoLuong1");
            lblSoLuong2.DataBindings.Add("Text", DetailReport.DataSource, "SoLuong2");
            lblMaDVTSoLuong2.DataBindings.Add("Text", DetailReport.DataSource, "MaDVTSoLuong2");
            lblDonGiaHoaDon.DataBindings.Add("Text", DetailReport.DataSource, "DonGiaHoaDon");
            lblMTTDonGiaHoaDon.DataBindings.Add("Text", DetailReport.DataSource, "MTTDonGiaHoaDon");
            lblDonViDonGiaHoaDon.DataBindings.Add("Text", DetailReport.DataSource, "DonViDonGiaHoaDon");
            lblTriGiaHoaDon.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaHoaDon");
            lblMTTGiaTien.DataBindings.Add("Text", DetailReport.DataSource, "MTTGiaTien");
            lblLoaiKhauTru_Hang.DataBindings.Add("Text", DetailReport.DataSource, "LoaiKhauTru_Hang");
            lblSoTienKhauTru_Hang.DataBindings.Add("Text", DetailReport.DataSource, "SoTienKhauTru_Hang");
            lblMTTSoTienKhauTru_Hang.DataBindings.Add("Text", DetailReport.DataSource, "MTTSoTienKhauTru_Hang");

        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0")
                lbl.Text = "";
        }
        private void cell_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            if (cell.Text.Trim() == "0")
                cell.Text = "";
        }
    }
}
