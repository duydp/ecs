using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Windows.Forms;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class QuyetDinhChapThuanKhaiSuaDoiBoSung_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public QuyetDinhChapThuanKhaiSuaDoiBoSung_1()
        {
            InitializeComponent();
        }
        public void BindReport(VAL8000 val)
        {
            lblSoThongBao.Text = val.ADD.GetValue().ToString();
            lblNgayHoanThanhKT.Text = Convert.ToDateTime(val.AD2.GetValue()).ToString("dd/MM/yyyy");
            lblGioHoanThanhKT.Text = val.AD3.GetValue().ToString();
            lblSoToKhaiBoSung.Text = val.A03.GetValue().ToString();
            lblNgayDKKhaiBoSung.Text = Convert.ToDateTime(val.A02.GetValue()).ToString("dd/MM/yyyy");
            lblGioDKKhaiBoSung.Text = val.AD1.GetValue().ToString();
            lblCoQuanNhan.Text = val.A00.GetValue().ToString();
            lblNhomXuLyHoSo.Text = val.A01.GetValue().ToString();
            lblPhanLoaiXNK.Text = val.ADG.GetValue().ToString();
            lblSoToKhaiXNK.Text = val.A27.GetValue().ToString();
            lblMaLoaiHinh.Text = val.ADQ.GetValue().ToString();
            lblNgayToKhaiXNK.Text = Convert.ToDateTime(val.A29.GetValue()).ToString("dd/MM/yyyy");
            lblDauHieuBaoQua.Text = val.ADE.GetValue().ToString();
            lblMaHetThoiHan.Text = val.CTO.GetValue().ToString();
            lblNgayCapPhep.Text = Convert.ToDateTime(val.A30.GetValue()).ToString("dd/MM/yyyy");
            lblThoiHanTaiNhapXuat.Text = Convert.ToDateTime(val.ADR.GetValue()).ToString("dd/MM/yyyy");
            lblMaNguoiKhai.Text = val.A04.GetValue().ToString();
            lblTenNguoiKhai.Text = val.A05.GetValue().ToString();
            lblMaBuuChinh.Text = val.A06.GetValue().ToString();
            lblDiaChi.Text = val.A07.GetValue().ToString();
            lblSoDT.Text = val.A11.GetValue().ToString();
            lblMaDaiLyHQ.Text = val.A12.GetValue().ToString();
            lblTenDaiLyHQ.Text = val.A13.GetValue().ToString();
            lblMaNhanVienHQ.Text = val.A14.GetValue().ToString();
            lblMaLyDoKhaiBS.Text = val.A15.GetValue().ToString();
            lblPhanLoaiNopThue.Text = val.A16.GetValue().ToString();
            lblMaXacDinhThoiHanNop.Text = val.ADL.GetValue().ToString();
            lblNgayHetHieuLuc.Text = Convert.ToDateTime(val.ADS.GetValue()).ToString("dd/MM/yyyy");
            lblSoNgayAnHan.Text = val.ADT.GetValue().ToString();
            lblSoNgayAnHanVAT.Text = val.ADU.GetValue().ToString();
            lblSoQuanLyNoiBo.Text = val.A20.GetValue().ToString();
            lblMaTienTeTruocKhaiBao.Text = val.ADH.GetValue().ToString();
            lblTyGiaHoiDoaiTruocKhaiBao.Text = val.ADI.GetValue().ToString();
            lblGhiChuTruocKhaiBao.Text = val.ADJ.GetValue().ToString();
            lblMaTienTeSauKhaiBao.Text = val.ADK.GetValue().ToString();
            lblTyGiaHoiDoaiSauKhaiBao.Text = val.A21.GetValue().ToString();
            lblGhiChuSauKhaiBao.Text = val.A22.GetValue().ToString();
            lblBieuThiSoTienTangGiamThueXNK.Text = val.ADB.GetValue().ToString();
            lblTongSoTienTangGiamThueXNK.Text = val.A23.GetValue().ToString();
            lblMaTienTeThueXNK.Text = val.ADN.GetValue().ToString();

            BindReportTax(val);
            lblTongSoTrang.Text = val.A24.GetValue().ToString();
            lblTongSoDongHang.Text = val.A25.GetValue().ToString();
            lblLyDo.Text = val.ADO.GetValue().ToString();
            lblTenNguoiPhuTrach.Text = val.ADF.GetValue().ToString();
            lblTenTruongDv.Text = val.ADP.GetValue().ToString();


        }
        public void BindReportTax(VAL8000 valtax)
        {

            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("MaKhoanMucTiepNhan", typeof(string));
                dt.Columns.Add("TenKhoanMucTiepNhan", typeof(string));
                dt.Columns.Add("BieuThiSoTien", typeof(string));
                dt.Columns.Add("TongSoTien", typeof(string));
                dt.Columns.Add("MaTienTeThueVaThuKhac", typeof(string));
                for (int i = 0; i < valtax.KA1.listAttribute[0].ListValue.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["MaKhoanMucTiepNhan"] = valtax.KA1.listAttribute[0].GetValueCollection(i).ToString();
                    dr["TenKhoanMucTiepNhan"] = valtax.KA1.listAttribute[1].GetValueCollection(i).ToString();
                    dr["BieuThiSoTien"] = valtax.KA1.listAttribute[2].GetValueCollection(i).ToString();
                    dr["TongSoTien"] = valtax.KA1.listAttribute[3].GetValueCollection(i).ToString();
                    dr["MaTienTeThueVaThuKhac"] = valtax.KA1.listAttribute[4].GetValueCollection(i).ToString();
                    dt.Rows.Add(dr);
                }

                while (dt.Rows.Count < 5)
                {


                    DataRow dr = dt.NewRow();
                    dr["MaKhoanMucTiepNhan"] = "";
                    dr["TenKhoanMucTiepNhan"] = "";
                    dr["BieuThiSoTien"] = "";
                    dr["TongSoTien"] = "";
                    dr["MaTienTeThueVaThuKhac"] = "";

                    dt.Rows.Add(dr);
                }
                DetailReport.DataSource = dt;
                lblMaKhoanMucTiepNhan.DataBindings.Add("Text", DetailReport.DataSource, "MaKhoanMucTiepNhan");
                lblTenKhoanMucTiepNhan.DataBindings.Add("Text", DetailReport.DataSource, "TenKhoanMucTiepNhan");
                lblBieuThiSoTienTangGiamThueVaThuKhac.DataBindings.Add("Text", DetailReport.DataSource, "BieuThiSoTien");
                lblTongSoTienTangGiamThueVaThuKhac.DataBindings.Add("Text", DetailReport.DataSource, "TongSoTien");
                lblMaTienTeThueVaThuKhac.DataBindings.Add("Text", DetailReport.DataSource, "MaTienTeThueVaThuKhac");

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
          
        }

    }
}
