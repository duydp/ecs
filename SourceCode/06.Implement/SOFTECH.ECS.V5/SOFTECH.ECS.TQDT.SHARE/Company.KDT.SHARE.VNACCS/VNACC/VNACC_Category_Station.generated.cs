using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_Station : ICloneable
	{
		#region Properties.
		
		public string ResultCode { set; get; }
		public int PGNumber { set; get; }
		public string TableID { set; get; }
		public string ProcessClassification { set; get; }
		public int CreatorClassification { set; get; }
		public string NumberOfKeyItems { set; get; }
		public string StationCode { set; get; }
		public string StationName { set; get; }
		public string CountryCode { set; get; }
		public int NecessityIndicationOfInputName { set; get; }
		public string CustomsOfficeCode { set; get; }
		public string CustomsOfficeCodeRM { set; get; }
		public string UserCodeOfImmigrationBureau { set; get; }
		public int ManifestForImmigration { set; get; }
		public int ArrivalDepartureForImmigration { set; get; }
		public int PassengerForImmigration { set; get; }
		public int CrewForImmigration { set; get; }
		public string UserCodeOfQuarantineStation { set; get; }
		public int ManifestForQuarantine { set; get; }
		public int ArrivalDepartureForQuarantine { set; get; }
		public int PassengerForQuarantine { set; get; }
		public int CrewForQuarantine { set; get; }
		public string UserCodeOfRailwayStationAuthority { set; get; }
		public int ManifestForRailwayStationAuthority { set; get; }
		public int ArrivalDepartureForRailwayStationAuthority { set; get; }
		public int PassengerForRailwayStationAuthority { set; get; }
		public int CrewForRailwayStationAuthority { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<VNACC_Category_Station> ConvertToCollection(IDataReader reader)
		{
			List<VNACC_Category_Station> collection = new List<VNACC_Category_Station>();
			while (reader.Read())
			{
				VNACC_Category_Station entity = new VNACC_Category_Station();
				if (!reader.IsDBNull(reader.GetOrdinal("ResultCode"))) entity.ResultCode = reader.GetString(reader.GetOrdinal("ResultCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("PGNumber"))) entity.PGNumber = reader.GetInt32(reader.GetOrdinal("PGNumber"));
				if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ProcessClassification"))) entity.ProcessClassification = reader.GetString(reader.GetOrdinal("ProcessClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatorClassification"))) entity.CreatorClassification = reader.GetInt32(reader.GetOrdinal("CreatorClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("NumberOfKeyItems"))) entity.NumberOfKeyItems = reader.GetString(reader.GetOrdinal("NumberOfKeyItems"));
				if (!reader.IsDBNull(reader.GetOrdinal("StationCode"))) entity.StationCode = reader.GetString(reader.GetOrdinal("StationCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("StationName"))) entity.StationName = reader.GetString(reader.GetOrdinal("StationName"));
				if (!reader.IsDBNull(reader.GetOrdinal("CountryCode"))) entity.CountryCode = reader.GetString(reader.GetOrdinal("CountryCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("NecessityIndicationOfInputName"))) entity.NecessityIndicationOfInputName = reader.GetInt32(reader.GetOrdinal("NecessityIndicationOfInputName"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomsOfficeCode"))) entity.CustomsOfficeCode = reader.GetString(reader.GetOrdinal("CustomsOfficeCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomsOfficeCodeRM"))) entity.CustomsOfficeCodeRM = reader.GetString(reader.GetOrdinal("CustomsOfficeCodeRM"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserCodeOfImmigrationBureau"))) entity.UserCodeOfImmigrationBureau = reader.GetString(reader.GetOrdinal("UserCodeOfImmigrationBureau"));
				if (!reader.IsDBNull(reader.GetOrdinal("ManifestForImmigration"))) entity.ManifestForImmigration = reader.GetInt32(reader.GetOrdinal("ManifestForImmigration"));
				if (!reader.IsDBNull(reader.GetOrdinal("ArrivalDepartureForImmigration"))) entity.ArrivalDepartureForImmigration = reader.GetInt32(reader.GetOrdinal("ArrivalDepartureForImmigration"));
				if (!reader.IsDBNull(reader.GetOrdinal("PassengerForImmigration"))) entity.PassengerForImmigration = reader.GetInt32(reader.GetOrdinal("PassengerForImmigration"));
				if (!reader.IsDBNull(reader.GetOrdinal("CrewForImmigration"))) entity.CrewForImmigration = reader.GetInt32(reader.GetOrdinal("CrewForImmigration"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserCodeOfQuarantineStation"))) entity.UserCodeOfQuarantineStation = reader.GetString(reader.GetOrdinal("UserCodeOfQuarantineStation"));
				if (!reader.IsDBNull(reader.GetOrdinal("ManifestForQuarantine"))) entity.ManifestForQuarantine = reader.GetInt32(reader.GetOrdinal("ManifestForQuarantine"));
				if (!reader.IsDBNull(reader.GetOrdinal("ArrivalDepartureForQuarantine"))) entity.ArrivalDepartureForQuarantine = reader.GetInt32(reader.GetOrdinal("ArrivalDepartureForQuarantine"));
				if (!reader.IsDBNull(reader.GetOrdinal("PassengerForQuarantine"))) entity.PassengerForQuarantine = reader.GetInt32(reader.GetOrdinal("PassengerForQuarantine"));
				if (!reader.IsDBNull(reader.GetOrdinal("CrewForQuarantine"))) entity.CrewForQuarantine = reader.GetInt32(reader.GetOrdinal("CrewForQuarantine"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserCodeOfRailwayStationAuthority"))) entity.UserCodeOfRailwayStationAuthority = reader.GetString(reader.GetOrdinal("UserCodeOfRailwayStationAuthority"));
				if (!reader.IsDBNull(reader.GetOrdinal("ManifestForRailwayStationAuthority"))) entity.ManifestForRailwayStationAuthority = reader.GetInt32(reader.GetOrdinal("ManifestForRailwayStationAuthority"));
				if (!reader.IsDBNull(reader.GetOrdinal("ArrivalDepartureForRailwayStationAuthority"))) entity.ArrivalDepartureForRailwayStationAuthority = reader.GetInt32(reader.GetOrdinal("ArrivalDepartureForRailwayStationAuthority"));
				if (!reader.IsDBNull(reader.GetOrdinal("PassengerForRailwayStationAuthority"))) entity.PassengerForRailwayStationAuthority = reader.GetInt32(reader.GetOrdinal("PassengerForRailwayStationAuthority"));
				if (!reader.IsDBNull(reader.GetOrdinal("CrewForRailwayStationAuthority"))) entity.CrewForRailwayStationAuthority = reader.GetInt32(reader.GetOrdinal("CrewForRailwayStationAuthority"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
        public static List<VNACC_Category_Station> ConvertDataSetToCollection(DataSet dataSet)
        {
            List<VNACC_Category_Station> collection = new List<VNACC_Category_Station>();
            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                VNACC_Category_Station entity = new VNACC_Category_Station();
                entity.ResultCode = dataSet.Tables[0].Rows[i]["ResultCode"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["ResultCode"].ToString() : String.Empty;
                entity.PGNumber = dataSet.Tables[0].Rows[i]["PGNumber"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["PGNumber"].ToString()) : 0;
                entity.TableID = dataSet.Tables[0].Rows[i]["TableID"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["TableID"].ToString() : "A601A";
                entity.ProcessClassification = dataSet.Tables[0].Rows[i]["ProcessClassification"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["ProcessClassification"].ToString() : String.Empty;
                entity.CreatorClassification = dataSet.Tables[0].Rows[i]["CreatorClassification"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["CreatorClassification"].ToString()) : 0;
                entity.NumberOfKeyItems = dataSet.Tables[0].Rows[i]["NumberOfKeyItems"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["NumberOfKeyItems"].ToString() : String.Empty;
                entity.StationCode = dataSet.Tables[0].Rows[i]["StationCode"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["StationCode"].ToString() : String.Empty;
                entity.StationName = dataSet.Tables[0].Rows[i]["StationName"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["StationName"].ToString() : String.Empty;
                entity.CountryCode = dataSet.Tables[0].Rows[i]["CountryCode"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["CountryCode"].ToString() : String.Empty;
                entity.NecessityIndicationOfInputName = dataSet.Tables[0].Rows[i]["NecessityIndicationOfInputName"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["NecessityIndicationOfInputName"].ToString()) : 0;
                entity.CustomsOfficeCode = dataSet.Tables[0].Rows[i]["CustomsOfficeCode"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["CustomsOfficeCode"].ToString() : String.Empty;
                entity.CustomsOfficeCodeRM = dataSet.Tables[0].Rows[i]["CustomsOfficeCodeRM"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["CustomsOfficeCodeRM"].ToString() : String.Empty;
                entity.UserCodeOfImmigrationBureau = dataSet.Tables[0].Rows[i]["UserCodeOfImmigrationBureau"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["UserCodeOfImmigrationBureau"].ToString() : String.Empty;
                entity.ManifestForImmigration = dataSet.Tables[0].Rows[i]["ManifestForImmigration"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["ManifestForImmigration"].ToString()) : 0;
                entity.ArrivalDepartureForImmigration = dataSet.Tables[0].Rows[i]["ArrivalDepartureForImmigration"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["ArrivalDepartureForImmigration"].ToString()) : 0;
                entity.PassengerForImmigration = dataSet.Tables[0].Rows[i]["PassengerForImmigration"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["PassengerForImmigration"].ToString()) : 0;
                entity.CrewForImmigration = dataSet.Tables[0].Rows[i]["CrewForImmigration"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["CrewForImmigration"].ToString()) : 0;
                entity.UserCodeOfQuarantineStation = dataSet.Tables[0].Rows[i]["UserCodeOfQuarantineStation"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["UserCodeOfQuarantineStation"].ToString() : String.Empty;
                entity.ManifestForQuarantine = dataSet.Tables[0].Rows[i]["ManifestForQuarantine"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["ManifestForQuarantine"].ToString()) : 0;
                entity.ArrivalDepartureForQuarantine = dataSet.Tables[0].Rows[i]["ArrivalDepartureForQuarantine"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["ArrivalDepartureForQuarantine"].ToString()) : 0;
                entity.PassengerForQuarantine = dataSet.Tables[0].Rows[i]["PassengerForQuarantine"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["PassengerForQuarantine"].ToString()) : 0;
                entity.CrewForQuarantine = dataSet.Tables[0].Rows[i]["CrewForQuarantine"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["CrewForQuarantine"].ToString()) : 0;
                entity.UserCodeOfRailwayStationAuthority = dataSet.Tables[0].Rows[i]["UserCodeOfRailwayStationAuthority"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["UserCodeOfRailwayStationAuthority"].ToString() : String.Empty;
                entity.ManifestForRailwayStationAuthority = dataSet.Tables[0].Rows[i]["ManifestForRailwayStationAuthority"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["ManifestForRailwayStationAuthority"].ToString()) : 0;
                entity.ArrivalDepartureForRailwayStationAuthority = dataSet.Tables[0].Rows[i]["ArrivalDepartureForRailwayStationAuthority"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["ArrivalDepartureForRailwayStationAuthority"].ToString()) : 0;
                entity.PassengerForRailwayStationAuthority = dataSet.Tables[0].Rows[i]["PassengerForRailwayStationAuthority"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["PassengerForRailwayStationAuthority"].ToString()) : 0;
                entity.CrewForRailwayStationAuthority = dataSet.Tables[0].Rows[i]["CrewForRailwayStationAuthority"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["CrewForRailwayStationAuthority"].ToString()) : 0;
                entity.Notes = dataSet.Tables[0].Rows[i]["Notes"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["Notes"].ToString() : String.Empty;
                entity.InputMessageID = dataSet.Tables[0].Rows[i]["InputMessageID"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["InputMessageID"].ToString() : String.Empty;
                entity.MessageTag = dataSet.Tables[0].Rows[i]["MessageTag"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["MessageTag"].ToString() : String.Empty;
                entity.IndexTag = dataSet.Tables[0].Rows[i]["IndexTag"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["IndexTag"].ToString() : String.Empty;

                collection.Add(entity);
            }
            return collection;
        }
		public static bool Find(List<VNACC_Category_Station> collection, string stationCode)
        {
            foreach (VNACC_Category_Station item in collection)
            {
                if (item.StationCode == stationCode)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_VNACC_Category_Stations VALUES(@ResultCode, @PGNumber, @TableID, @ProcessClassification, @CreatorClassification, @NumberOfKeyItems, @StationCode, @StationName, @CountryCode, @NecessityIndicationOfInputName, @CustomsOfficeCode, @CustomsOfficeCodeRM, @UserCodeOfImmigrationBureau, @ManifestForImmigration, @ArrivalDepartureForImmigration, @PassengerForImmigration, @CrewForImmigration, @UserCodeOfQuarantineStation, @ManifestForQuarantine, @ArrivalDepartureForQuarantine, @PassengerForQuarantine, @CrewForQuarantine, @UserCodeOfRailwayStationAuthority, @ManifestForRailwayStationAuthority, @ArrivalDepartureForRailwayStationAuthority, @PassengerForRailwayStationAuthority, @CrewForRailwayStationAuthority, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_Stations SET ResultCode = @ResultCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, CreatorClassification = @CreatorClassification, NumberOfKeyItems = @NumberOfKeyItems, StationName = @StationName, CountryCode = @CountryCode, NecessityIndicationOfInputName = @NecessityIndicationOfInputName, CustomsOfficeCode = @CustomsOfficeCode, CustomsOfficeCodeRM = @CustomsOfficeCodeRM, UserCodeOfImmigrationBureau = @UserCodeOfImmigrationBureau, ManifestForImmigration = @ManifestForImmigration, ArrivalDepartureForImmigration = @ArrivalDepartureForImmigration, PassengerForImmigration = @PassengerForImmigration, CrewForImmigration = @CrewForImmigration, UserCodeOfQuarantineStation = @UserCodeOfQuarantineStation, ManifestForQuarantine = @ManifestForQuarantine, ArrivalDepartureForQuarantine = @ArrivalDepartureForQuarantine, PassengerForQuarantine = @PassengerForQuarantine, CrewForQuarantine = @CrewForQuarantine, UserCodeOfRailwayStationAuthority = @UserCodeOfRailwayStationAuthority, ManifestForRailwayStationAuthority = @ManifestForRailwayStationAuthority, ArrivalDepartureForRailwayStationAuthority = @ArrivalDepartureForRailwayStationAuthority, PassengerForRailwayStationAuthority = @PassengerForRailwayStationAuthority, CrewForRailwayStationAuthority = @CrewForRailwayStationAuthority, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE StationCode = @StationCode";
            string delete = "DELETE FROM t_VNACC_Category_Stations WHERE StationCode = @StationCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StationCode", SqlDbType.VarChar, "StationCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StationName", SqlDbType.NVarChar, "StationName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CountryCode", SqlDbType.VarChar, "CountryCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NecessityIndicationOfInputName", SqlDbType.Int, "NecessityIndicationOfInputName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomsOfficeCode", SqlDbType.VarChar, "CustomsOfficeCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomsOfficeCodeRM", SqlDbType.VarChar, "CustomsOfficeCodeRM", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserCodeOfImmigrationBureau", SqlDbType.VarChar, "UserCodeOfImmigrationBureau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ManifestForImmigration", SqlDbType.Int, "ManifestForImmigration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ArrivalDepartureForImmigration", SqlDbType.Int, "ArrivalDepartureForImmigration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PassengerForImmigration", SqlDbType.Int, "PassengerForImmigration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CrewForImmigration", SqlDbType.Int, "CrewForImmigration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserCodeOfQuarantineStation", SqlDbType.VarChar, "UserCodeOfQuarantineStation", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ManifestForQuarantine", SqlDbType.Int, "ManifestForQuarantine", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ArrivalDepartureForQuarantine", SqlDbType.Int, "ArrivalDepartureForQuarantine", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PassengerForQuarantine", SqlDbType.Int, "PassengerForQuarantine", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CrewForQuarantine", SqlDbType.Int, "CrewForQuarantine", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserCodeOfRailwayStationAuthority", SqlDbType.VarChar, "UserCodeOfRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ManifestForRailwayStationAuthority", SqlDbType.Int, "ManifestForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ArrivalDepartureForRailwayStationAuthority", SqlDbType.Int, "ArrivalDepartureForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PassengerForRailwayStationAuthority", SqlDbType.Int, "PassengerForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CrewForRailwayStationAuthority", SqlDbType.Int, "CrewForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StationCode", SqlDbType.VarChar, "StationCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StationName", SqlDbType.NVarChar, "StationName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CountryCode", SqlDbType.VarChar, "CountryCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NecessityIndicationOfInputName", SqlDbType.Int, "NecessityIndicationOfInputName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomsOfficeCode", SqlDbType.VarChar, "CustomsOfficeCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomsOfficeCodeRM", SqlDbType.VarChar, "CustomsOfficeCodeRM", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserCodeOfImmigrationBureau", SqlDbType.VarChar, "UserCodeOfImmigrationBureau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ManifestForImmigration", SqlDbType.Int, "ManifestForImmigration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ArrivalDepartureForImmigration", SqlDbType.Int, "ArrivalDepartureForImmigration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PassengerForImmigration", SqlDbType.Int, "PassengerForImmigration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CrewForImmigration", SqlDbType.Int, "CrewForImmigration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserCodeOfQuarantineStation", SqlDbType.VarChar, "UserCodeOfQuarantineStation", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ManifestForQuarantine", SqlDbType.Int, "ManifestForQuarantine", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ArrivalDepartureForQuarantine", SqlDbType.Int, "ArrivalDepartureForQuarantine", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PassengerForQuarantine", SqlDbType.Int, "PassengerForQuarantine", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CrewForQuarantine", SqlDbType.Int, "CrewForQuarantine", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserCodeOfRailwayStationAuthority", SqlDbType.VarChar, "UserCodeOfRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ManifestForRailwayStationAuthority", SqlDbType.Int, "ManifestForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ArrivalDepartureForRailwayStationAuthority", SqlDbType.Int, "ArrivalDepartureForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PassengerForRailwayStationAuthority", SqlDbType.Int, "PassengerForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CrewForRailwayStationAuthority", SqlDbType.Int, "CrewForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@StationCode", SqlDbType.VarChar, "StationCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_VNACC_Category_Stations VALUES(@ResultCode, @PGNumber, @TableID, @ProcessClassification, @CreatorClassification, @NumberOfKeyItems, @StationCode, @StationName, @CountryCode, @NecessityIndicationOfInputName, @CustomsOfficeCode, @CustomsOfficeCodeRM, @UserCodeOfImmigrationBureau, @ManifestForImmigration, @ArrivalDepartureForImmigration, @PassengerForImmigration, @CrewForImmigration, @UserCodeOfQuarantineStation, @ManifestForQuarantine, @ArrivalDepartureForQuarantine, @PassengerForQuarantine, @CrewForQuarantine, @UserCodeOfRailwayStationAuthority, @ManifestForRailwayStationAuthority, @ArrivalDepartureForRailwayStationAuthority, @PassengerForRailwayStationAuthority, @CrewForRailwayStationAuthority, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_Stations SET ResultCode = @ResultCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, CreatorClassification = @CreatorClassification, NumberOfKeyItems = @NumberOfKeyItems, StationName = @StationName, CountryCode = @CountryCode, NecessityIndicationOfInputName = @NecessityIndicationOfInputName, CustomsOfficeCode = @CustomsOfficeCode, CustomsOfficeCodeRM = @CustomsOfficeCodeRM, UserCodeOfImmigrationBureau = @UserCodeOfImmigrationBureau, ManifestForImmigration = @ManifestForImmigration, ArrivalDepartureForImmigration = @ArrivalDepartureForImmigration, PassengerForImmigration = @PassengerForImmigration, CrewForImmigration = @CrewForImmigration, UserCodeOfQuarantineStation = @UserCodeOfQuarantineStation, ManifestForQuarantine = @ManifestForQuarantine, ArrivalDepartureForQuarantine = @ArrivalDepartureForQuarantine, PassengerForQuarantine = @PassengerForQuarantine, CrewForQuarantine = @CrewForQuarantine, UserCodeOfRailwayStationAuthority = @UserCodeOfRailwayStationAuthority, ManifestForRailwayStationAuthority = @ManifestForRailwayStationAuthority, ArrivalDepartureForRailwayStationAuthority = @ArrivalDepartureForRailwayStationAuthority, PassengerForRailwayStationAuthority = @PassengerForRailwayStationAuthority, CrewForRailwayStationAuthority = @CrewForRailwayStationAuthority, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE StationCode = @StationCode";
            string delete = "DELETE FROM t_VNACC_Category_Stations WHERE StationCode = @StationCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StationCode", SqlDbType.VarChar, "StationCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StationName", SqlDbType.NVarChar, "StationName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CountryCode", SqlDbType.VarChar, "CountryCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NecessityIndicationOfInputName", SqlDbType.Int, "NecessityIndicationOfInputName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomsOfficeCode", SqlDbType.VarChar, "CustomsOfficeCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomsOfficeCodeRM", SqlDbType.VarChar, "CustomsOfficeCodeRM", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserCodeOfImmigrationBureau", SqlDbType.VarChar, "UserCodeOfImmigrationBureau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ManifestForImmigration", SqlDbType.Int, "ManifestForImmigration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ArrivalDepartureForImmigration", SqlDbType.Int, "ArrivalDepartureForImmigration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PassengerForImmigration", SqlDbType.Int, "PassengerForImmigration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CrewForImmigration", SqlDbType.Int, "CrewForImmigration", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserCodeOfQuarantineStation", SqlDbType.VarChar, "UserCodeOfQuarantineStation", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ManifestForQuarantine", SqlDbType.Int, "ManifestForQuarantine", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ArrivalDepartureForQuarantine", SqlDbType.Int, "ArrivalDepartureForQuarantine", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PassengerForQuarantine", SqlDbType.Int, "PassengerForQuarantine", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CrewForQuarantine", SqlDbType.Int, "CrewForQuarantine", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserCodeOfRailwayStationAuthority", SqlDbType.VarChar, "UserCodeOfRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ManifestForRailwayStationAuthority", SqlDbType.Int, "ManifestForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ArrivalDepartureForRailwayStationAuthority", SqlDbType.Int, "ArrivalDepartureForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PassengerForRailwayStationAuthority", SqlDbType.Int, "PassengerForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CrewForRailwayStationAuthority", SqlDbType.Int, "CrewForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StationCode", SqlDbType.VarChar, "StationCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StationName", SqlDbType.NVarChar, "StationName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CountryCode", SqlDbType.VarChar, "CountryCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NecessityIndicationOfInputName", SqlDbType.Int, "NecessityIndicationOfInputName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomsOfficeCode", SqlDbType.VarChar, "CustomsOfficeCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomsOfficeCodeRM", SqlDbType.VarChar, "CustomsOfficeCodeRM", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserCodeOfImmigrationBureau", SqlDbType.VarChar, "UserCodeOfImmigrationBureau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ManifestForImmigration", SqlDbType.Int, "ManifestForImmigration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ArrivalDepartureForImmigration", SqlDbType.Int, "ArrivalDepartureForImmigration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PassengerForImmigration", SqlDbType.Int, "PassengerForImmigration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CrewForImmigration", SqlDbType.Int, "CrewForImmigration", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserCodeOfQuarantineStation", SqlDbType.VarChar, "UserCodeOfQuarantineStation", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ManifestForQuarantine", SqlDbType.Int, "ManifestForQuarantine", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ArrivalDepartureForQuarantine", SqlDbType.Int, "ArrivalDepartureForQuarantine", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PassengerForQuarantine", SqlDbType.Int, "PassengerForQuarantine", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CrewForQuarantine", SqlDbType.Int, "CrewForQuarantine", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserCodeOfRailwayStationAuthority", SqlDbType.VarChar, "UserCodeOfRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ManifestForRailwayStationAuthority", SqlDbType.Int, "ManifestForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ArrivalDepartureForRailwayStationAuthority", SqlDbType.Int, "ArrivalDepartureForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PassengerForRailwayStationAuthority", SqlDbType.Int, "PassengerForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CrewForRailwayStationAuthority", SqlDbType.Int, "CrewForRailwayStationAuthority", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@StationCode", SqlDbType.VarChar, "StationCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static VNACC_Category_Station Load(string stationCode)
		{
			const string spName = "[dbo].[p_VNACC_Category_Station_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@StationCode", SqlDbType.VarChar, stationCode);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<VNACC_Category_Station> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<VNACC_Category_Station> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<VNACC_Category_Station> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_Station_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_Station_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_Station_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_Station_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertVNACC_Category_Station(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string stationName, string countryCode, int necessityIndicationOfInputName, string customsOfficeCode, string customsOfficeCodeRM, string userCodeOfImmigrationBureau, int manifestForImmigration, int arrivalDepartureForImmigration, int passengerForImmigration, int crewForImmigration, string userCodeOfQuarantineStation, int manifestForQuarantine, int arrivalDepartureForQuarantine, int passengerForQuarantine, int crewForQuarantine, string userCodeOfRailwayStationAuthority, int manifestForRailwayStationAuthority, int arrivalDepartureForRailwayStationAuthority, int passengerForRailwayStationAuthority, int crewForRailwayStationAuthority, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_Station entity = new VNACC_Category_Station();	
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.StationName = stationName;
			entity.CountryCode = countryCode;
			entity.NecessityIndicationOfInputName = necessityIndicationOfInputName;
			entity.CustomsOfficeCode = customsOfficeCode;
			entity.CustomsOfficeCodeRM = customsOfficeCodeRM;
			entity.UserCodeOfImmigrationBureau = userCodeOfImmigrationBureau;
			entity.ManifestForImmigration = manifestForImmigration;
			entity.ArrivalDepartureForImmigration = arrivalDepartureForImmigration;
			entity.PassengerForImmigration = passengerForImmigration;
			entity.CrewForImmigration = crewForImmigration;
			entity.UserCodeOfQuarantineStation = userCodeOfQuarantineStation;
			entity.ManifestForQuarantine = manifestForQuarantine;
			entity.ArrivalDepartureForQuarantine = arrivalDepartureForQuarantine;
			entity.PassengerForQuarantine = passengerForQuarantine;
			entity.CrewForQuarantine = crewForQuarantine;
			entity.UserCodeOfRailwayStationAuthority = userCodeOfRailwayStationAuthority;
			entity.ManifestForRailwayStationAuthority = manifestForRailwayStationAuthority;
			entity.ArrivalDepartureForRailwayStationAuthority = arrivalDepartureForRailwayStationAuthority;
			entity.PassengerForRailwayStationAuthority = passengerForRailwayStationAuthority;
			entity.CrewForRailwayStationAuthority = crewForRailwayStationAuthority;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_VNACC_Category_Station_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@StationCode", SqlDbType.VarChar, StationCode);
			db.AddInParameter(dbCommand, "@StationName", SqlDbType.NVarChar, StationName);
			db.AddInParameter(dbCommand, "@CountryCode", SqlDbType.VarChar, CountryCode);
			db.AddInParameter(dbCommand, "@NecessityIndicationOfInputName", SqlDbType.Int, NecessityIndicationOfInputName);
			db.AddInParameter(dbCommand, "@CustomsOfficeCode", SqlDbType.VarChar, CustomsOfficeCode);
			db.AddInParameter(dbCommand, "@CustomsOfficeCodeRM", SqlDbType.VarChar, CustomsOfficeCodeRM);
			db.AddInParameter(dbCommand, "@UserCodeOfImmigrationBureau", SqlDbType.VarChar, UserCodeOfImmigrationBureau);
			db.AddInParameter(dbCommand, "@ManifestForImmigration", SqlDbType.Int, ManifestForImmigration);
			db.AddInParameter(dbCommand, "@ArrivalDepartureForImmigration", SqlDbType.Int, ArrivalDepartureForImmigration);
			db.AddInParameter(dbCommand, "@PassengerForImmigration", SqlDbType.Int, PassengerForImmigration);
			db.AddInParameter(dbCommand, "@CrewForImmigration", SqlDbType.Int, CrewForImmigration);
			db.AddInParameter(dbCommand, "@UserCodeOfQuarantineStation", SqlDbType.VarChar, UserCodeOfQuarantineStation);
			db.AddInParameter(dbCommand, "@ManifestForQuarantine", SqlDbType.Int, ManifestForQuarantine);
			db.AddInParameter(dbCommand, "@ArrivalDepartureForQuarantine", SqlDbType.Int, ArrivalDepartureForQuarantine);
			db.AddInParameter(dbCommand, "@PassengerForQuarantine", SqlDbType.Int, PassengerForQuarantine);
			db.AddInParameter(dbCommand, "@CrewForQuarantine", SqlDbType.Int, CrewForQuarantine);
			db.AddInParameter(dbCommand, "@UserCodeOfRailwayStationAuthority", SqlDbType.VarChar, UserCodeOfRailwayStationAuthority);
			db.AddInParameter(dbCommand, "@ManifestForRailwayStationAuthority", SqlDbType.Int, ManifestForRailwayStationAuthority);
			db.AddInParameter(dbCommand, "@ArrivalDepartureForRailwayStationAuthority", SqlDbType.Int, ArrivalDepartureForRailwayStationAuthority);
			db.AddInParameter(dbCommand, "@PassengerForRailwayStationAuthority", SqlDbType.Int, PassengerForRailwayStationAuthority);
			db.AddInParameter(dbCommand, "@CrewForRailwayStationAuthority", SqlDbType.Int, CrewForRailwayStationAuthority);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<VNACC_Category_Station> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Station item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateVNACC_Category_Station(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string stationCode, string stationName, string countryCode, int necessityIndicationOfInputName, string customsOfficeCode, string customsOfficeCodeRM, string userCodeOfImmigrationBureau, int manifestForImmigration, int arrivalDepartureForImmigration, int passengerForImmigration, int crewForImmigration, string userCodeOfQuarantineStation, int manifestForQuarantine, int arrivalDepartureForQuarantine, int passengerForQuarantine, int crewForQuarantine, string userCodeOfRailwayStationAuthority, int manifestForRailwayStationAuthority, int arrivalDepartureForRailwayStationAuthority, int passengerForRailwayStationAuthority, int crewForRailwayStationAuthority, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_Station entity = new VNACC_Category_Station();			
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.StationCode = stationCode;
			entity.StationName = stationName;
			entity.CountryCode = countryCode;
			entity.NecessityIndicationOfInputName = necessityIndicationOfInputName;
			entity.CustomsOfficeCode = customsOfficeCode;
			entity.CustomsOfficeCodeRM = customsOfficeCodeRM;
			entity.UserCodeOfImmigrationBureau = userCodeOfImmigrationBureau;
			entity.ManifestForImmigration = manifestForImmigration;
			entity.ArrivalDepartureForImmigration = arrivalDepartureForImmigration;
			entity.PassengerForImmigration = passengerForImmigration;
			entity.CrewForImmigration = crewForImmigration;
			entity.UserCodeOfQuarantineStation = userCodeOfQuarantineStation;
			entity.ManifestForQuarantine = manifestForQuarantine;
			entity.ArrivalDepartureForQuarantine = arrivalDepartureForQuarantine;
			entity.PassengerForQuarantine = passengerForQuarantine;
			entity.CrewForQuarantine = crewForQuarantine;
			entity.UserCodeOfRailwayStationAuthority = userCodeOfRailwayStationAuthority;
			entity.ManifestForRailwayStationAuthority = manifestForRailwayStationAuthority;
			entity.ArrivalDepartureForRailwayStationAuthority = arrivalDepartureForRailwayStationAuthority;
			entity.PassengerForRailwayStationAuthority = passengerForRailwayStationAuthority;
			entity.CrewForRailwayStationAuthority = crewForRailwayStationAuthority;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_VNACC_Category_Station_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@StationCode", SqlDbType.VarChar, StationCode);
			db.AddInParameter(dbCommand, "@StationName", SqlDbType.NVarChar, StationName);
			db.AddInParameter(dbCommand, "@CountryCode", SqlDbType.VarChar, CountryCode);
			db.AddInParameter(dbCommand, "@NecessityIndicationOfInputName", SqlDbType.Int, NecessityIndicationOfInputName);
			db.AddInParameter(dbCommand, "@CustomsOfficeCode", SqlDbType.VarChar, CustomsOfficeCode);
			db.AddInParameter(dbCommand, "@CustomsOfficeCodeRM", SqlDbType.VarChar, CustomsOfficeCodeRM);
			db.AddInParameter(dbCommand, "@UserCodeOfImmigrationBureau", SqlDbType.VarChar, UserCodeOfImmigrationBureau);
			db.AddInParameter(dbCommand, "@ManifestForImmigration", SqlDbType.Int, ManifestForImmigration);
			db.AddInParameter(dbCommand, "@ArrivalDepartureForImmigration", SqlDbType.Int, ArrivalDepartureForImmigration);
			db.AddInParameter(dbCommand, "@PassengerForImmigration", SqlDbType.Int, PassengerForImmigration);
			db.AddInParameter(dbCommand, "@CrewForImmigration", SqlDbType.Int, CrewForImmigration);
			db.AddInParameter(dbCommand, "@UserCodeOfQuarantineStation", SqlDbType.VarChar, UserCodeOfQuarantineStation);
			db.AddInParameter(dbCommand, "@ManifestForQuarantine", SqlDbType.Int, ManifestForQuarantine);
			db.AddInParameter(dbCommand, "@ArrivalDepartureForQuarantine", SqlDbType.Int, ArrivalDepartureForQuarantine);
			db.AddInParameter(dbCommand, "@PassengerForQuarantine", SqlDbType.Int, PassengerForQuarantine);
			db.AddInParameter(dbCommand, "@CrewForQuarantine", SqlDbType.Int, CrewForQuarantine);
			db.AddInParameter(dbCommand, "@UserCodeOfRailwayStationAuthority", SqlDbType.VarChar, UserCodeOfRailwayStationAuthority);
			db.AddInParameter(dbCommand, "@ManifestForRailwayStationAuthority", SqlDbType.Int, ManifestForRailwayStationAuthority);
			db.AddInParameter(dbCommand, "@ArrivalDepartureForRailwayStationAuthority", SqlDbType.Int, ArrivalDepartureForRailwayStationAuthority);
			db.AddInParameter(dbCommand, "@PassengerForRailwayStationAuthority", SqlDbType.Int, PassengerForRailwayStationAuthority);
			db.AddInParameter(dbCommand, "@CrewForRailwayStationAuthority", SqlDbType.Int, CrewForRailwayStationAuthority);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<VNACC_Category_Station> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Station item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateVNACC_Category_Station(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string stationCode, string stationName, string countryCode, int necessityIndicationOfInputName, string customsOfficeCode, string customsOfficeCodeRM, string userCodeOfImmigrationBureau, int manifestForImmigration, int arrivalDepartureForImmigration, int passengerForImmigration, int crewForImmigration, string userCodeOfQuarantineStation, int manifestForQuarantine, int arrivalDepartureForQuarantine, int passengerForQuarantine, int crewForQuarantine, string userCodeOfRailwayStationAuthority, int manifestForRailwayStationAuthority, int arrivalDepartureForRailwayStationAuthority, int passengerForRailwayStationAuthority, int crewForRailwayStationAuthority, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_Station entity = new VNACC_Category_Station();			
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.StationCode = stationCode;
			entity.StationName = stationName;
			entity.CountryCode = countryCode;
			entity.NecessityIndicationOfInputName = necessityIndicationOfInputName;
			entity.CustomsOfficeCode = customsOfficeCode;
			entity.CustomsOfficeCodeRM = customsOfficeCodeRM;
			entity.UserCodeOfImmigrationBureau = userCodeOfImmigrationBureau;
			entity.ManifestForImmigration = manifestForImmigration;
			entity.ArrivalDepartureForImmigration = arrivalDepartureForImmigration;
			entity.PassengerForImmigration = passengerForImmigration;
			entity.CrewForImmigration = crewForImmigration;
			entity.UserCodeOfQuarantineStation = userCodeOfQuarantineStation;
			entity.ManifestForQuarantine = manifestForQuarantine;
			entity.ArrivalDepartureForQuarantine = arrivalDepartureForQuarantine;
			entity.PassengerForQuarantine = passengerForQuarantine;
			entity.CrewForQuarantine = crewForQuarantine;
			entity.UserCodeOfRailwayStationAuthority = userCodeOfRailwayStationAuthority;
			entity.ManifestForRailwayStationAuthority = manifestForRailwayStationAuthority;
			entity.ArrivalDepartureForRailwayStationAuthority = arrivalDepartureForRailwayStationAuthority;
			entity.PassengerForRailwayStationAuthority = passengerForRailwayStationAuthority;
			entity.CrewForRailwayStationAuthority = crewForRailwayStationAuthority;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_Station_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@StationCode", SqlDbType.VarChar, StationCode);
			db.AddInParameter(dbCommand, "@StationName", SqlDbType.NVarChar, StationName);
			db.AddInParameter(dbCommand, "@CountryCode", SqlDbType.VarChar, CountryCode);
			db.AddInParameter(dbCommand, "@NecessityIndicationOfInputName", SqlDbType.Int, NecessityIndicationOfInputName);
			db.AddInParameter(dbCommand, "@CustomsOfficeCode", SqlDbType.VarChar, CustomsOfficeCode);
			db.AddInParameter(dbCommand, "@CustomsOfficeCodeRM", SqlDbType.VarChar, CustomsOfficeCodeRM);
			db.AddInParameter(dbCommand, "@UserCodeOfImmigrationBureau", SqlDbType.VarChar, UserCodeOfImmigrationBureau);
			db.AddInParameter(dbCommand, "@ManifestForImmigration", SqlDbType.Int, ManifestForImmigration);
			db.AddInParameter(dbCommand, "@ArrivalDepartureForImmigration", SqlDbType.Int, ArrivalDepartureForImmigration);
			db.AddInParameter(dbCommand, "@PassengerForImmigration", SqlDbType.Int, PassengerForImmigration);
			db.AddInParameter(dbCommand, "@CrewForImmigration", SqlDbType.Int, CrewForImmigration);
			db.AddInParameter(dbCommand, "@UserCodeOfQuarantineStation", SqlDbType.VarChar, UserCodeOfQuarantineStation);
			db.AddInParameter(dbCommand, "@ManifestForQuarantine", SqlDbType.Int, ManifestForQuarantine);
			db.AddInParameter(dbCommand, "@ArrivalDepartureForQuarantine", SqlDbType.Int, ArrivalDepartureForQuarantine);
			db.AddInParameter(dbCommand, "@PassengerForQuarantine", SqlDbType.Int, PassengerForQuarantine);
			db.AddInParameter(dbCommand, "@CrewForQuarantine", SqlDbType.Int, CrewForQuarantine);
			db.AddInParameter(dbCommand, "@UserCodeOfRailwayStationAuthority", SqlDbType.VarChar, UserCodeOfRailwayStationAuthority);
			db.AddInParameter(dbCommand, "@ManifestForRailwayStationAuthority", SqlDbType.Int, ManifestForRailwayStationAuthority);
			db.AddInParameter(dbCommand, "@ArrivalDepartureForRailwayStationAuthority", SqlDbType.Int, ArrivalDepartureForRailwayStationAuthority);
			db.AddInParameter(dbCommand, "@PassengerForRailwayStationAuthority", SqlDbType.Int, PassengerForRailwayStationAuthority);
			db.AddInParameter(dbCommand, "@CrewForRailwayStationAuthority", SqlDbType.Int, CrewForRailwayStationAuthority);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<VNACC_Category_Station> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Station item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteVNACC_Category_Station(string stationCode)
		{
			VNACC_Category_Station entity = new VNACC_Category_Station();
			entity.StationCode = stationCode;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_Station_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@StationCode", SqlDbType.VarChar, StationCode);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_VNACC_Category_Station_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<VNACC_Category_Station> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Station item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}