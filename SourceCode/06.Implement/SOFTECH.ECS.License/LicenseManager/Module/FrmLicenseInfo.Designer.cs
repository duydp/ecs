namespace Licenses_v2.Module
{
    partial class FrmLicenseInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLicenseInfo));
            this.panelTop = new DevExpress.XtraEditors.PanelControl();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.lblCaption = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.txtLicTerm = new DevExpress.XtraEditors.SpinEdit();
            this.txtLicNumberClient = new DevExpress.XtraEditors.SpinEdit();
            this.txtLicDesc = new DevExpress.XtraEditors.MemoEdit();
            this.txtLicName = new DevExpress.XtraEditors.TextEdit();
            this.txtLicID = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.groupLicenseInfo = new DevExpress.XtraEditors.GroupControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).BeginInit();
            this.panelTop.SuspendLayout();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicTerm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicNumberClient.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupLicenseInfo)).BeginInit();
            this.groupLicenseInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelTop.Controls.Add(this.pnlTop);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(363, 67);
            this.panelTop.TabIndex = 2;
            // 
            // pnlTop
            // 
            this.pnlTop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlTop.BackgroundImage")));
            this.pnlTop.Controls.Add(this.lblCaption);
            this.pnlTop.Controls.Add(this.pictureBox1);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(363, 67);
            this.pnlTop.TabIndex = 8;
            // 
            // lblCaption
            // 
            this.lblCaption.Appearance.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaption.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblCaption.Appearance.Options.UseFont = true;
            this.lblCaption.Appearance.Options.UseForeColor = true;
            this.lblCaption.Location = new System.Drawing.Point(72, 9);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(242, 31);
            this.lblCaption.TabIndex = 2;
            this.lblCaption.Text = "Thông tin bản quyền";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(69, 67);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(211, 322);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(72, 22);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(145, 322);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(60, 22);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(289, 322);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(64, 22);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.Location = new System.Drawing.Point(82, 322);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(59, 22);
            this.btnEdit.TabIndex = 9;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // txtLicTerm
            // 
            this.txtLicTerm.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtLicTerm.Location = new System.Drawing.Point(87, 173);
            this.txtLicTerm.Name = "txtLicTerm";
            this.txtLicTerm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtLicTerm.Properties.MaxValue = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.txtLicTerm.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtLicTerm.Size = new System.Drawing.Size(52, 20);
            this.txtLicTerm.TabIndex = 8;
            // 
            // txtLicNumberClient
            // 
            this.txtLicNumberClient.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtLicNumberClient.Location = new System.Drawing.Point(88, 140);
            this.txtLicNumberClient.Name = "txtLicNumberClient";
            this.txtLicNumberClient.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txtLicNumberClient.Properties.MaxValue = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.txtLicNumberClient.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtLicNumberClient.Size = new System.Drawing.Size(53, 20);
            this.txtLicNumberClient.TabIndex = 7;
            // 
            // txtLicDesc
            // 
            this.txtLicDesc.Location = new System.Drawing.Point(88, 63);
            this.txtLicDesc.Name = "txtLicDesc";
            this.txtLicDesc.Size = new System.Drawing.Size(243, 62);
            this.txtLicDesc.TabIndex = 6;
            // 
            // txtLicName
            // 
            this.txtLicName.Location = new System.Drawing.Point(87, 30);
            this.txtLicName.Name = "txtLicName";
            this.txtLicName.Size = new System.Drawing.Size(244, 20);
            this.txtLicName.TabIndex = 5;
            // 
            // txtLicID
            // 
            this.txtLicID.Location = new System.Drawing.Point(98, 83);
            this.txtLicID.Name = "txtLicID";
            this.txtLicID.Properties.ReadOnly = true;
            this.txtLicID.Size = new System.Drawing.Size(104, 20);
            this.txtLicID.TabIndex = 14;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(20, 86);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(72, 13);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Mã bản quyền:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(5, 33);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(76, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Tên bản quyền:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(50, 65);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(31, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Mô tả:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(42, 143);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(39, 13);
            this.labelControl4.TabIndex = 4;
            this.labelControl4.Text = "Số máy:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(36, 176);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(45, 13);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Thời hạn:";
            // 
            // groupLicenseInfo
            // 
            this.groupLicenseInfo.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupLicenseInfo.AppearanceCaption.Options.UseFont = true;
            this.groupLicenseInfo.Controls.Add(this.labelControl2);
            this.groupLicenseInfo.Controls.Add(this.labelControl7);
            this.groupLicenseInfo.Controls.Add(this.labelControl6);
            this.groupLicenseInfo.Controls.Add(this.labelControl5);
            this.groupLicenseInfo.Controls.Add(this.labelControl3);
            this.groupLicenseInfo.Controls.Add(this.labelControl4);
            this.groupLicenseInfo.Controls.Add(this.txtLicDesc);
            this.groupLicenseInfo.Controls.Add(this.txtLicNumberClient);
            this.groupLicenseInfo.Controls.Add(this.txtLicName);
            this.groupLicenseInfo.Controls.Add(this.txtLicTerm);
            this.groupLicenseInfo.Location = new System.Drawing.Point(11, 109);
            this.groupLicenseInfo.Name = "groupLicenseInfo";
            this.groupLicenseInfo.Size = new System.Drawing.Size(342, 207);
            this.groupLicenseInfo.TabIndex = 5;
            this.groupLicenseInfo.Text = "Thông tin bản quyền";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseForeColor = true;
            this.labelControl7.Location = new System.Drawing.Point(147, 143);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(29, 13);
            this.labelControl7.TabIndex = 4;
            this.labelControl7.Text = "(Máy)";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.labelControl6.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            this.labelControl6.Location = new System.Drawing.Point(145, 176);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(39, 13);
            this.labelControl6.TabIndex = 4;
            this.labelControl6.Text = "(Tháng)";
            // 
            // FrmLicenseInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 349);
            this.Controls.Add(this.groupLicenseInfo);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtLicID);
            this.Controls.Add(this.btnClose);
            this.Name = "FrmLicenseInfo";
            this.Text = "Thông tin bản quyền";
            this.Load += new System.EventHandler(this.FrmLicenseInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicTerm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicNumberClient.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLicID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupLicenseInfo)).EndInit();
            this.groupLicenseInfo.ResumeLayout(false);
            this.groupLicenseInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelTop;
        private DevExpress.XtraEditors.SpinEdit txtLicTerm;
        private DevExpress.XtraEditors.SpinEdit txtLicNumberClient;
        private DevExpress.XtraEditors.MemoEdit txtLicDesc;
        private DevExpress.XtraEditors.TextEdit txtLicName;
        private DevExpress.XtraEditors.TextEdit txtLicID;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private System.Windows.Forms.Panel pnlTop;
        private DevExpress.XtraEditors.LabelControl lblCaption;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.GroupControl groupLicenseInfo;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
    }
}