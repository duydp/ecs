﻿namespace Company.Interface.GC
{
    partial class ThongKeNPLTonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgListTotal_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThongKeNPLTonForm));
            Janus.Windows.GridEX.GridEXLayout dgListDetail_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnProcess = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.dtpDateFrom = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtpDateTo = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label10 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.tbNPLTon = new System.Windows.Forms.TabControl();
            this.tpToTal = new System.Windows.Forms.TabPage();
            this.dgListTotal = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.tpDetail = new System.Windows.Forms.TabPage();
            this.dgListDetail = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuExport = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.tbNPLTon.SuspendLayout();
            this.tpToTal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.tpDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDetail)).BeginInit();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Size = new System.Drawing.Size(1221, 424);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.btnProcess);
            this.uiGroupBox4.Controls.Add(this.label11);
            this.uiGroupBox4.Controls.Add(this.dtpDateFrom);
            this.uiGroupBox4.Controls.Add(this.dtpDateTo);
            this.uiGroupBox4.Controls.Add(this.label10);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1221, 53);
            this.uiGroupBox4.TabIndex = 1;
            this.uiGroupBox4.Text = "Thông tin chung";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // btnProcess
            // 
            this.btnProcess.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcess.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnProcess.ForeColor = System.Drawing.Color.White;
            this.btnProcess.Location = new System.Drawing.Point(390, 21);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(135, 23);
            this.btnProcess.TabIndex = 9;
            this.btnProcess.Text = "XỬ LÝ SỐ LIỆU";
            this.btnProcess.UseVisualStyleBackColor = false;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(30, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "TỪ NGÀY  :";
            // 
            // dtpDateFrom
            // 
            this.dtpDateFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpDateFrom.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dtpDateFrom.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.dtpDateFrom.DropDownCalendar.Name = "";
            this.dtpDateFrom.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.dtpDateFrom.EditStyle = Janus.Windows.CalendarCombo.EditStyle.Free;
            this.dtpDateFrom.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDateFrom.Location = new System.Drawing.Point(101, 21);
            this.dtpDateFrom.Name = "dtpDateFrom";
            this.dtpDateFrom.Nullable = true;
            this.dtpDateFrom.NullButtonText = "Xóa";
            this.dtpDateFrom.ShowNullButton = true;
            this.dtpDateFrom.Size = new System.Drawing.Size(95, 21);
            this.dtpDateFrom.TabIndex = 4;
            this.dtpDateFrom.TodayButtonText = "Hôm nay";
            this.dtpDateFrom.Value = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
            this.dtpDateFrom.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            // 
            // dtpDateTo
            // 
            this.dtpDateTo.CustomFormat = "dd/MM/yyyy";
            this.dtpDateTo.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dtpDateTo.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.dtpDateTo.DropDownCalendar.Name = "";
            this.dtpDateTo.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.dtpDateTo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDateTo.Location = new System.Drawing.Point(277, 21);
            this.dtpDateTo.Name = "dtpDateTo";
            this.dtpDateTo.NullButtonText = "Xóa";
            this.dtpDateTo.ShowNullButton = true;
            this.dtpDateTo.Size = new System.Drawing.Size(95, 21);
            this.dtpDateTo.TabIndex = 8;
            this.dtpDateTo.TodayButtonText = "Hôm nay";
            this.dtpDateTo.Value = new System.DateTime(2015, 12, 31, 0, 0, 0, 0);
            this.dtpDateTo.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(206, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "ĐẾN NGÀY :";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.tbNPLTon);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 53);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1221, 371);
            this.uiGroupBox1.TabIndex = 2;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // tbNPLTon
            // 
            this.tbNPLTon.Controls.Add(this.tpToTal);
            this.tbNPLTon.Controls.Add(this.tpDetail);
            this.tbNPLTon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbNPLTon.Location = new System.Drawing.Point(3, 8);
            this.tbNPLTon.Name = "tbNPLTon";
            this.tbNPLTon.SelectedIndex = 0;
            this.tbNPLTon.Size = new System.Drawing.Size(1215, 360);
            this.tbNPLTon.TabIndex = 0;
            this.tbNPLTon.Selected += new System.Windows.Forms.TabControlEventHandler(this.tbNPLTon_Selected);
            // 
            // tpToTal
            // 
            this.tpToTal.Controls.Add(this.dgListTotal);
            this.tpToTal.Location = new System.Drawing.Point(4, 22);
            this.tpToTal.Margin = new System.Windows.Forms.Padding(0);
            this.tpToTal.Name = "tpToTal";
            this.tpToTal.Size = new System.Drawing.Size(1207, 334);
            this.tpToTal.TabIndex = 0;
            this.tpToTal.Text = "TỔNG HỢP SỐ LIỆU CỦA CÁC HỢP ĐỒNG";
            this.tpToTal.UseVisualStyleBackColor = true;
            // 
            // dgListTotal
            // 
            this.dgListTotal.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTotal.AlternatingColors = true;
            this.dgListTotal.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTotal.ColumnAutoResize = true;
            this.dgListTotal.ContextMenuStrip = this.contextMenuStrip1;
            dgListTotal_DesignTimeLayout.LayoutString = resources.GetString("dgListTotal_DesignTimeLayout.LayoutString");
            this.dgListTotal.DesignTimeLayout = dgListTotal_DesignTimeLayout;
            this.dgListTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTotal.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTotal.FrozenColumns = 3;
            this.dgListTotal.GroupByBoxVisible = false;
            this.dgListTotal.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTotal.Location = new System.Drawing.Point(0, 0);
            this.dgListTotal.Margin = new System.Windows.Forms.Padding(0);
            this.dgListTotal.Name = "dgListTotal";
            this.dgListTotal.RecordNavigator = true;
            this.dgListTotal.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTotal.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTotal.Size = new System.Drawing.Size(1207, 334);
            this.dgListTotal.TabIndex = 9;
            this.dgListTotal.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuExportExcel});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(141, 26);
            // 
            // mnuExportExcel
            // 
            this.mnuExportExcel.Name = "mnuExportExcel";
            this.mnuExportExcel.Size = new System.Drawing.Size(140, 22);
            this.mnuExportExcel.Text = "XUẤT EXCEL";
            this.mnuExportExcel.Click += new System.EventHandler(this.mnuExportExcel_Click);
            // 
            // tpDetail
            // 
            this.tpDetail.Controls.Add(this.dgListDetail);
            this.tpDetail.Location = new System.Drawing.Point(4, 22);
            this.tpDetail.Margin = new System.Windows.Forms.Padding(0);
            this.tpDetail.Name = "tpDetail";
            this.tpDetail.Size = new System.Drawing.Size(1207, 334);
            this.tpDetail.TabIndex = 1;
            this.tpDetail.Text = "TỔNG HỢP SỐ LIỆU CHI TIẾT CỦA CÁC HỢP ĐỒNG";
            this.tpDetail.UseVisualStyleBackColor = true;
            // 
            // dgListDetail
            // 
            this.dgListDetail.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDetail.AlternatingColors = true;
            this.dgListDetail.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListDetail.ColumnAutoResize = true;
            this.dgListDetail.ContextMenuStrip = this.contextMenuStrip2;
            dgListDetail_DesignTimeLayout.LayoutString = resources.GetString("dgListDetail_DesignTimeLayout.LayoutString");
            this.dgListDetail.DesignTimeLayout = dgListDetail_DesignTimeLayout;
            this.dgListDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDetail.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDetail.FrozenColumns = 3;
            this.dgListDetail.GroupByBoxVisible = false;
            this.dgListDetail.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDetail.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDetail.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDetail.Location = new System.Drawing.Point(0, 0);
            this.dgListDetail.Margin = new System.Windows.Forms.Padding(0);
            this.dgListDetail.Name = "dgListDetail";
            this.dgListDetail.RecordNavigator = true;
            this.dgListDetail.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDetail.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDetail.Size = new System.Drawing.Size(1207, 334);
            this.dgListDetail.TabIndex = 9;
            this.dgListDetail.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuExport});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(141, 26);
            // 
            // mnuExport
            // 
            this.mnuExport.Name = "mnuExport";
            this.mnuExport.Size = new System.Drawing.Size(140, 22);
            this.mnuExport.Text = "XUẤT EXCEL";
            this.mnuExport.Click += new System.EventHandler(this.mnuExport_Click);
            // 
            // ThongKeNPLTonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1221, 424);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ThongKeNPLTonForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "THỐNG KÊ NGUYÊN PHỤ LIỆU TỒN";
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.tbNPLTon.ResumeLayout(false);
            this.tpToTal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.tpDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDetail)).EndInit();
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpDateFrom;
        private Janus.Windows.CalendarCombo.CalendarCombo dtpDateTo;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.TabControl tbNPLTon;
        private System.Windows.Forms.TabPage tpToTal;
        private System.Windows.Forms.TabPage tpDetail;
        private Janus.Windows.GridEX.GridEX dgListTotal;
        private Janus.Windows.GridEX.GridEX dgListDetail;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuExportExcel;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem mnuExport;
    }
}