-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Insert]
	@TKMDBoSung_ID bigint,
	@SoDong varchar(2),
	@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac varchar(1),
	@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac nvarchar(9),
	@HienThiTongSoTienTangGiamThuKhac varchar(1),
	@TongSoTienTangGiamThuKhac numeric(11, 0),
	@MaTienTeTongSoTienTangGiamThuKhac varchar(3),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac]
(
	[TKMDBoSung_ID],
	[SoDong],
	[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[HienThiTongSoTienTangGiamThuKhac],
	[TongSoTienTangGiamThuKhac],
	[MaTienTeTongSoTienTangGiamThuKhac]
)
VALUES 
(
	@TKMDBoSung_ID,
	@SoDong,
	@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
	@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
	@HienThiTongSoTienTangGiamThuKhac,
	@TongSoTienTangGiamThuKhac,
	@MaTienTeTongSoTienTangGiamThuKhac
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Update]
	@ID bigint,
	@TKMDBoSung_ID bigint,
	@SoDong varchar(2),
	@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac varchar(1),
	@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac nvarchar(9),
	@HienThiTongSoTienTangGiamThuKhac varchar(1),
	@TongSoTienTangGiamThuKhac numeric(11, 0),
	@MaTienTeTongSoTienTangGiamThuKhac varchar(3)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac]
SET
	[TKMDBoSung_ID] = @TKMDBoSung_ID,
	[SoDong] = @SoDong,
	[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac] = @MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
	[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac] = @TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
	[HienThiTongSoTienTangGiamThuKhac] = @HienThiTongSoTienTangGiamThuKhac,
	[TongSoTienTangGiamThuKhac] = @TongSoTienTangGiamThuKhac,
	[MaTienTeTongSoTienTangGiamThuKhac] = @MaTienTeTongSoTienTangGiamThuKhac
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_InsertUpdate]
	@ID bigint,
	@TKMDBoSung_ID bigint,
	@SoDong varchar(2),
	@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac varchar(1),
	@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac nvarchar(9),
	@HienThiTongSoTienTangGiamThuKhac varchar(1),
	@TongSoTienTangGiamThuKhac numeric(11, 0),
	@MaTienTeTongSoTienTangGiamThuKhac varchar(3)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac] 
		SET
			[TKMDBoSung_ID] = @TKMDBoSung_ID,
			[SoDong] = @SoDong,
			[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac] = @MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
			[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac] = @TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
			[HienThiTongSoTienTangGiamThuKhac] = @HienThiTongSoTienTangGiamThuKhac,
			[TongSoTienTangGiamThuKhac] = @TongSoTienTangGiamThuKhac,
			[MaTienTeTongSoTienTangGiamThuKhac] = @MaTienTeTongSoTienTangGiamThuKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac]
		(
			[TKMDBoSung_ID],
			[SoDong],
			[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
			[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
			[HienThiTongSoTienTangGiamThuKhac],
			[TongSoTienTangGiamThuKhac],
			[MaTienTeTongSoTienTangGiamThuKhac]
		)
		VALUES 
		(
			@TKMDBoSung_ID,
			@SoDong,
			@MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
			@TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac,
			@HienThiTongSoTienTangGiamThuKhac,
			@TongSoTienTangGiamThuKhac,
			@MaTienTeTongSoTienTangGiamThuKhac
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[HienThiTongSoTienTangGiamThuKhac],
	[TongSoTienTangGiamThuKhac],
	[MaTienTeTongSoTienTangGiamThuKhac]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[HienThiTongSoTienTangGiamThuKhac],
	[TongSoTienTangGiamThuKhac],
	[MaTienTeTongSoTienTangGiamThuKhac]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac],
	[HienThiTongSoTienTangGiamThuKhac],
	[TongSoTienTangGiamThuKhac],
	[MaTienTeTongSoTienTangGiamThuKhac]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac]	

GO

