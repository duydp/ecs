﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.VNACCS.WareHouse
{
    public partial class VNACC_WareHouseManagementForm : BaseForm
    {
        public VNACC_WareHouseManagementForm()
        {
            InitializeComponent();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                try
                {
                    this.Cursor = Cursors.WaitCursor;

                    object dr = dgList.GetRow().DataRow;
                    if (dgList.GetRows().Length < 0) return;
                    if (cbbLoaiPhieu.SelectedValue.ToString()== "1")
                    {
                        T_KDT_VNACCS_WarehouseImport warehouseImport = new T_KDT_VNACCS_WarehouseImport();
                        warehouseImport = (T_KDT_VNACCS_WarehouseImport)dgList.CurrentRow.DataRow;
                        VNACC_WareHouseImportForm f = new VNACC_WareHouseImportForm();
                        f.wareHouseImport = warehouseImport;
                        f.ShowDialog(this);                        
                    }
                    else
                    {
                        T_KDT_VNACCS_WarehouseExport warehouseExport = new T_KDT_VNACCS_WarehouseExport();
                        warehouseExport = (T_KDT_VNACCS_WarehouseExport)dgList.CurrentRow.DataRow;
                        VNACC_WareHouseExportForm f = new VNACC_WareHouseExportForm();
                        f.wareHouseExport = warehouseExport;
                        f.ShowDialog(this);  
                    }
                    this.Cursor = Cursors.Default;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    if (e.Row.Cells["TrangThaiXuLy"].Value.ToString() == TrangThaiXuLy.CHO_DUYET.ToString())
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                    else if (e.Row.Cells["TrangThaiXuLy"].Value.ToString() == TrangThaiXuLy.CHUA_KHAI_BAO.ToString())
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                    else if (e.Row.Cells["TrangThaiXuLy"].Value.ToString() == TrangThaiXuLy.DA_DUYET.ToString())
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                    else if (e.Row.Cells["TrangThaiXuLy"].Value.ToString() == TrangThaiXuLy.KHONG_PHE_DUYET.ToString())
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                    else if (e.Row.Cells["TrangThaiXuLy"].Value.ToString() == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY.ToString())
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ hủy";
                    else if (e.Row.Cells["TrangThaiXuLy"].Value.ToString() == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY.ToString())
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
                    else if (e.Row.Cells["TrangThaiXuLy"].Value.ToString() == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET.ToString())
                        e.Row.Cells["TrangThaiXuLy"].Text = "Sửa";
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            try
            {
                btnXoa_Click(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private string GetSearchWhere()
        {
            string where = " 1=1 ";
            if (!String.IsNullOrEmpty(txtSoTN.Text.Trim()))
                where += " AND SoTN = " + txtSoTN.Text.Trim();
            if (!String.IsNullOrEmpty(txtSoTKCT.Text.Trim()))
                where += " AND SoTKChungTu  LIKE '%" + txtSoTKCT.Text.Trim() + "%'";
            if (!String.IsNullOrEmpty(txtMaKho.Text.Trim()))
                where += "  AND MaKho LIKE '%" + txtMaKho.Text.Trim() + "%'";
            if (!String.IsNullOrEmpty(txtNamTN.Text.Trim()))
                where += " AND YEAR(NgayTN) = " + txtNamTN.Text.Trim();
            if (String.IsNullOrEmpty(cbStatus.SelectedValue.ToString()))
                where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue.ToString();
            //if (dtpNgayBDBC.Value.Year > 1900)
            //    where += " AND NgayBatDauBC = '" + dtpNgayBDBC.Value.ToString("dd-MM-yyyy 00:00:00") + "'";
            //if (dtpNgayBDBC.Value.Year > 1900)
            //    where += " AND NgayKetthucBC = '" + dtpNgayKTBC.Value.ToString("dd-MM-yyyy 23:59:59") + "'";
            return where;
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbbLoaiPhieu.SelectedValue.ToString()=="1")
                {
                    dgList.Refetch();
                    dgList.DataSource = T_KDT_VNACCS_WarehouseImport.SelectCollectionDynamic(GetSearchWhere(), null);
                    dgList.Refresh();
                }
                else if (cbbLoaiPhieu.SelectedValue.ToString() == "2")
                {
                    dgList.Refetch();
                    dgList.DataSource = T_KDT_VNACCS_WarehouseExport.SelectCollectionDynamic(GetSearchWhere(), null);
                    dgList.Refresh();
                }
                else
                {
                    dgList.Refetch();
                    dgList.DataSource = T_KDT_VNACCS_WarehouseImport.SelectCollectionDynamic(GetSearchWhere(), null);
                    dgList.Refresh();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    this.Cursor = Cursors.WaitCursor;

                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    List<T_KDT_VNACCS_WarehouseImport> ImportColl = new List<T_KDT_VNACCS_WarehouseImport>();
                    List<T_KDT_VNACCS_WarehouseExport> ExporColl = new List<T_KDT_VNACCS_WarehouseExport>();
                    if (dgList.GetRows().Length < 0) return;
                    if (items.Count <= 0) return;
                    if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
                    {
                        foreach (GridEXSelectedItem i in items)
                        {
                            if (i.RowType == RowType.Record)
                            {
                                if (cbbLoaiPhieu.SelectedValue.ToString() == "1")
                                {
                                    ImportColl.Add((T_KDT_VNACCS_WarehouseImport)i.GetRow().DataRow);
                                }
                                else if (cbbLoaiPhieu.SelectedValue.ToString() == "2")
                                {
                                    ExporColl.Add((T_KDT_VNACCS_WarehouseExport)i.GetRow().DataRow);
                                }
                                else
                                {

                                }
                            }
                        }
                        foreach (T_KDT_VNACCS_WarehouseImport item in ImportColl)
                        {
                            if (item.ID > 0)
                                item.DeleteFull();
                        }
                        foreach (T_KDT_VNACCS_WarehouseExport item in ExporColl)
                        {
                            if (item.ID > 0)
                                item.DeleteFull();
                        }
                        btnSearch_Click(null,null);
                    }
                    this.Cursor = Cursors.Default;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    this.Cursor = Cursors.Default;
                    //
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void VNACC_WareHouseManagementForm_Load(object sender, EventArgs e)
        {
            try
            {
                txtNamTN.Text = DateTime.Now.Year.ToString();
                dtpNgayBDBC.Value = DateTime.Now;
                dtpNgayKTBC.Value = DateTime.Now;
                cbStatus.SelectedValue = TrangThaiXuLy.CHUA_KHAI_BAO;
                cbbLoaiPhieu.SelectedIndex = 0;
                btnSearch_Click(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdChuyenTT":
                    ChuyenTT();
                    break;
                case "cmdPrint":
                    Print(); 
                    break;
                case "cmdDelete":
                    Delete(); 
                    break;
                case "cmdExportExcel":
                    ExportExcel(); 
                    break;
            }            
        }
        private void ChuyenTT()
        {
            try
            {
                if (dgList.SelectedItems.Count > 0)
                {
                    List<T_KDT_VNACCS_WarehouseImport> warehouseImportCollection = new List<T_KDT_VNACCS_WarehouseImport>();
                    List<T_KDT_VNACCS_WarehouseExport> warehouseExportCollection = new List<T_KDT_VNACCS_WarehouseExport>();
                    foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                    {
                        if (cbbLoaiPhieu.SelectedValue.ToString() == "1")
                        {
                            warehouseImportCollection.Add((T_KDT_VNACCS_WarehouseImport)grItem.GetRow().DataRow);
                        }
                        else if (cbbLoaiPhieu.SelectedValue.ToString() == "2")
                        {
                            warehouseExportCollection.Add((T_KDT_VNACCS_WarehouseExport)grItem.GetRow().DataRow);
                        }
                    }
                    for (int i = 0; i < warehouseImportCollection.Count; i++)
                    {
                        warehouseImportCollection[i].TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                        warehouseExportCollection[i].Update();
                    }
                    for (int i = 0; i < warehouseExportCollection.Count; i++)
                    {
                        warehouseExportCollection[i].TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                        warehouseExportCollection[i].Update();
                    }
                    ShowMessage(" Chuyển trạng thái thành công ",false);
                    this.btnSearch_Click(null, null);
                }
                else
                {
                    ShowMessage("Có lỗi trong quá trình xử lý !",false);
                    //showMsg("MSG_2702040", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage(" Chuyển trạng thái không thành công \n\r " + ex, false);
            }

        }
        private void Print()
        {
            try
            {
                if (!(dgList.GetRows().Length > 0 && dgList.GetRow().RowType == RowType.Record)) return;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
                phieuTNForm.TenPhieu = "PHIẾU NHẬP XUẤT KHO";
#if GC_V4
                Company.Interface.Report.GC.PhieuTN phieuTNAll = new Company.Interface.Report.GC.PhieuTN();
#elif SXXK_V4
                Company.Interface.Report.SXXK.PhieuTN phieuTNAll = new Company.Interface.Report.SXXK.PhieuTN();
#endif
                string[,] arrPhieuTN = new string[items.Count, 2];
                int j = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        if (cbbLoaiPhieu.SelectedValue.ToString() == "1")
                        {
                            T_KDT_VNACCS_WarehouseImport warehouseImport = (T_KDT_VNACCS_WarehouseImport)i.GetRow().DataRow;
                            arrPhieuTN[j, 0] = warehouseImport.SoTN.ToString();
                            arrPhieuTN[j, 1] = warehouseImport.NgayTN.ToString("dd/MM/yyyy");
                            j++;
                            break;
                        }
                        else if (cbbLoaiPhieu.SelectedValue.ToString() == "2")
                        {
                            T_KDT_VNACCS_WarehouseExport warehouseExport = (T_KDT_VNACCS_WarehouseExport)i.GetRow().DataRow;
                            arrPhieuTN[j, 0] = warehouseExport.SoTN.ToString();
                            arrPhieuTN[j, 1] = warehouseExport.NgayTN.ToString("dd/MM/yyyy");
                            j++;
                            break;
                        }
                    }
                }
                phieuTNForm.phieuTN = arrPhieuTN;
                phieuTNForm.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }
        public void ExportExcel(GridEX gridList)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH PHIẾU XUẤT NHẬP KHO _" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void ExportExcel()
        {
            ExportExcel(dgList);
        }

        private void Delete()
        {
            btnXoa_Click(null, null);
        }

        private void uiContextMenu1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdChuyenTT":
                    ChuyenTT();
                    break;
                case "cmdPrint":
                    Print();
                    break;
                case "cmdDelete":
                    Delete();
                    break;
                case "cmdExportExcel":
                    ExportExcel();
                    break;
                case "cmdUpdateStatus":
                    UpdateStatus();
                    break;
            } 
        }

        private void UpdateStatus()
        {
            try
            {
                WSForm2 login = new WSForm2();
                login.ShowDialog(this);
                if (WSForm2.IsSuccess == true)
                {
                    if (dgList.SelectedItems.Count > 0)
                    {
                        if (cbbLoaiPhieu.SelectedValue.ToString() == "1")
                        {
                            List<T_KDT_VNACCS_WarehouseImport> ImportCollection = new List<T_KDT_VNACCS_WarehouseImport>();
                            foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                            {
                                ImportCollection.Add((T_KDT_VNACCS_WarehouseImport)grItem.GetRow().DataRow);
                            }
                            for (int i = 0; i < ImportCollection.Count; i++)
                            {
                                Company.Interface.KDT.GC.UpdateStatusForm f = new Company.Interface.KDT.GC.UpdateStatusForm();
                                f.WarehouseImport = ImportCollection[i];
                                f.formType = "PNK";
                                f.ShowDialog(this);
                            }
                            this.btnSearch_Click(null, null);
                        }
                        else
                        {
                            List<T_KDT_VNACCS_WarehouseExport> ExportCollection = new List<T_KDT_VNACCS_WarehouseExport>();
                            foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                            {
                                ExportCollection.Add((T_KDT_VNACCS_WarehouseExport)grItem.GetRow().DataRow);
                            }
                            for (int i = 0; i < ExportCollection.Count; i++)
                            {
                                Company.Interface.KDT.GC.UpdateStatusForm f = new Company.Interface.KDT.GC.UpdateStatusForm();
                                f.WarehouseExport = ExportCollection[i];
                                f.formType = "PXK";
                                f.ShowDialog(this);
                            }
                            this.btnSearch_Click(null, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
    }
}
