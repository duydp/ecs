﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.WSSyncData;

namespace Company.BLL.VNACCS
{
    public class DataVNACCSSync
    {
        public MsgPhanBo msgPB { get; set; }
        public MsgLog msgLog { get; set; }
        public string exception { get; set; }
        public string SoToKhai;
        public string TrangThaiXuLy;
        public DateTime NgayDangKy;
        public DataVNACCSSync() { }
        public DataVNACCSSync(string SoToKhai, bool isDaiLy, DateTime ngayDangKy)
        {
            try
            {
                GetDataLog(SoToKhai);
                this.SoToKhai = SoToKhai;
                this.NgayDangKy = ngayDangKy;
                exception = null;
            }
            catch (System.Exception ex)
            {
                //Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        public DataVNACCSSync(string SoToKhai, bool isDaiLy, DateTime ngayDangKy,string trangThaiXuLy)
        {
            try
            {
                GetDataLogCTQ(SoToKhai);
                this.SoToKhai = SoToKhai;
                this.NgayDangKy = ngayDangKy;
                this.TrangThaiXuLy = trangThaiXuLy;
                exception = null;
            }
            catch (System.Exception ex)
            {
                //Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }
        private void GetDataLog(string SoToKhai)
        {
            try
            {
                MsgPhanBo phanbo = MsgPhanBo.GetMessThongQuan(SoToKhai);
                if (phanbo != null)
                {
                    MsgLog log = MsgLog.Load(phanbo.Master_ID);
                    if (log != null)
                    {
                        this.msgPB = phanbo;
                        this.msgLog = log;
                        this.msgLog.ID = this.msgPB.ID = 0;
                    }
                    else
                        throw new Exception("Không tìm thấy log messages thông quan của tờ khai " + SoToKhai);
                }
                else
                    throw new Exception("Không tìm thấy log messages thông quan của tờ khai " + SoToKhai);
            }
            catch (System.Exception ex)
            {
                throw new Exception("Lỗi tìm kiếm Msg Thông quan tờ khai " + SoToKhai + ".(" + ex.Message + ")");
            }

        }
        private void GetDataLogCTQ(string SoToKhai)
        {
            try
            {
                MsgPhanBo phanbo = MsgPhanBo.GetMessChuaThongQuan(SoToKhai);
                if (phanbo != null)
                {
                    MsgLog log = MsgLog.Load(phanbo.Master_ID);
                    if (log != null)
                    {
                        this.msgPB = phanbo;
                        this.msgLog = log;
                        this.msgLog.ID = this.msgPB.ID = 0;
                    }
                    else
                        throw new Exception("Không tìm thấy log messages thông quan của tờ khai " + SoToKhai);
                }
                else
                    throw new Exception("Không tìm thấy log messages thông quan của tờ khai " + SoToKhai);
            }
            catch (System.Exception ex)
            {
                throw new Exception("Lỗi tìm kiếm Msg Thông quan tờ khai " + SoToKhai + ".(" + ex.Message + ")");
            }

        }
        private void Load_NPL_SP_DM_DN(string SoToKhai)
        {
            try
            {

                List<DongBoDuLieu_TrackVNACCS> listtrack = DongBoDuLieu_TrackVNACCS.SelectCollectionDynamic("SoToKhai like '%" + SoToKhai + "%'", "NgayDongBo desc");
                if (listtrack != null && listtrack.Count > 0)
                {
                    DongBoDuLieu_TrackVNACCS track = listtrack[0];
                }
            }
            catch (System.Exception ex)
            {
               // Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;
            }
        }

        public bool insertDataFullToServer(string MaDoanhNghiep,string UserName, int daily)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                DongBoDuLieu_TrackVNACCS track;
                List<DongBoDuLieu_TrackVNACCS> listtrack = DongBoDuLieu_TrackVNACCS.SelectCollectionDynamic("SoToKhai like '%" + this.SoToKhai + "%'", "NgayDongBo desc");
                if (listtrack != null && listtrack.Count > 0)
                {
                    track = listtrack[0];
                }
                else
                    track = new DongBoDuLieu_TrackVNACCS();

                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    track.SoToKhai = SoToKhai;
                    track.NgayDongBo = DateTime.Now;
                    track.NgayDangKy = NgayDangKy;
                    track.TrangThaiDongBoLen = 1;
                    track.TrangThaiDongBoXuong = 0;
                    track.MaDoanhNghiep = MaDoanhNghiep;
                    track.UserName = UserName;
                    track.IDUserDaiLy = daily;

                    MsgPhanBo phanbo = MsgPhanBo.GetMessThongQuan(SoToKhai);
                    MsgLog log = null;
                    if (phanbo != null)
                    {
                        phanbo.CreatedTime = this.msgPB.CreatedTime;
                        phanbo.GhiChu = this.msgPB.GhiChu;
                        phanbo.IndexTag = this.msgPB.IndexTag;
                        phanbo.MaNghiepVu = this.msgPB.MaNghiepVu;
                        phanbo.MessagesInputID = this.msgPB.MessagesInputID;
                        phanbo.messagesTag = this.msgPB.messagesTag;
                        phanbo.RTPTag = this.msgPB.RTPTag;
                        phanbo.SoTiepNhan = this.msgPB.SoTiepNhan;
                        phanbo.TerminalID = this.msgPB.TerminalID;
                        phanbo.GhiChu = this.msgPB.GhiChu;
                        phanbo.TrangThai = this.msgPB.TrangThai;

                        if (phanbo.Master_ID > 0)
                        {
                            log = MsgLog.Load(phanbo.Master_ID);
                            if (log != null)
                            {
                                log.CreatedTime = this.msgLog.CreatedTime;
                                log.InputMessagesID = this.msgLog.InputMessagesID;
                                log.MaNghiepVu = this.msgLog.MaNghiepVu;
                                log.MessagesTag = this.msgLog.MessagesTag;
                                log.NoiDungThongBao = this.msgLog.NoiDungThongBao;
                                log.TieuDeThongBao = this.msgLog.TieuDeThongBao;
                                log.IndexTag = this.msgLog.IndexTag;
                                log.Log_Messages = this.msgLog.Log_Messages;
                            }
                            log = this.msgLog;
                        }
                        else
                            log = this.msgLog;

                    }
                    else
                    {
                        phanbo = this.msgPB;
                        log = this.msgLog;
                    }

                    if (log.ID == 0)
                        phanbo.Master_ID = log.Insert(transaction);
                    else
                    {
                        log.InsertUpdate(transaction);
                        phanbo.Master_ID = log.ID;
                    }
                    phanbo.InsertUpdate(transaction);
                    track.InsertUpdate(transaction);
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ret = false;
                    connection.Close();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }


        #region dự phòng
        //private bool insertDataFullToClient()
        //{
        //    bool ret;
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction();
        //        try
        //        {
        //            new NguyenPhuLieu().InsertUpdate(this.NplCollection, transaction);
        //            new SanPham().InsertUpdate(this.SPCollection, transaction);
        //            new DinhMuc().InsertUpdate(this.DMCollection, transaction);

        //            new Company.KDT.SHARE.VNACCS.LogMessages.MsgLog().ProcessMSG(this.msgLog.Log_Messages, true, transaction);
        //            transaction.Commit();
        //            ret = true;

        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.LocalLogger.Instance().WriteMessage(ex);
        //            transaction.Rollback();
        //            ret = false;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }

        //    }
        //    return ret;
        //}
        #endregion  dự phòng
    }
}
