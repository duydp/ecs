﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class DinhMucThanhKhoan : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public int LanThanhLy { set; get; }
		public string MaSP { set; get; }
		public string MaNPL { set; get; }
		public decimal SoTKN_VNACCS { set; get; }
		public string Invoid_HD { set; get; }
		public DateTime NgayChungTu { set; get; }
		public decimal SoLuongThanhKhoan { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<DinhMucThanhKhoan> ConvertToCollection(IDataReader reader)
		{
			List<DinhMucThanhKhoan> collection = new List<DinhMucThanhKhoan>();
			while (reader.Read())
			{
				DinhMucThanhKhoan entity = new DinhMucThanhKhoan();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKN_VNACCS"))) entity.SoTKN_VNACCS = reader.GetDecimal(reader.GetOrdinal("SoTKN_VNACCS"));
				if (!reader.IsDBNull(reader.GetOrdinal("Invoid_HD"))) entity.Invoid_HD = reader.GetString(reader.GetOrdinal("Invoid_HD"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayChungTu"))) entity.NgayChungTu = reader.GetDateTime(reader.GetOrdinal("NgayChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongThanhKhoan"))) entity.SoLuongThanhKhoan = reader.GetDecimal(reader.GetOrdinal("SoLuongThanhKhoan"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_SXXK_DinhMucThanhKhoan VALUES(@ID, @LanThanhLy, @MaSP, @MaNPL, @SoTKN_VNACCS, @Invoid_HD, @NgayChungTu, @SoLuongThanhKhoan)";
            string update = "UPDATE t_KDT_SXXK_DinhMucThanhKhoan SET ID = @ID, LanThanhLy = @LanThanhLy, MaSP = @MaSP, MaNPL = @MaNPL, SoTKN_VNACCS = @SoTKN_VNACCS, Invoid_HD = @Invoid_HD, NgayChungTu = @NgayChungTu, SoLuongThanhKhoan = @SoLuongThanhKhoan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_SXXK_DinhMucThanhKhoan WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LanThanhLy", SqlDbType.Int, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKN_VNACCS", SqlDbType.Decimal, "SoTKN_VNACCS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Invoid_HD", SqlDbType.VarChar, "Invoid_HD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongThanhKhoan", SqlDbType.Decimal, "SoLuongThanhKhoan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LanThanhLy", SqlDbType.Int, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKN_VNACCS", SqlDbType.Decimal, "SoTKN_VNACCS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Invoid_HD", SqlDbType.VarChar, "Invoid_HD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongThanhKhoan", SqlDbType.Decimal, "SoLuongThanhKhoan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@LanThanhLy", SqlDbType.Int, "LanThanhLy", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_SXXK_DinhMucThanhKhoan VALUES(@ID, @LanThanhLy, @MaSP, @MaNPL, @SoTKN_VNACCS, @Invoid_HD, @NgayChungTu, @SoLuongThanhKhoan)";
            string update = "UPDATE t_KDT_SXXK_DinhMucThanhKhoan SET ID = @ID, LanThanhLy = @LanThanhLy, MaSP = @MaSP, MaNPL = @MaNPL, SoTKN_VNACCS = @SoTKN_VNACCS, Invoid_HD = @Invoid_HD, NgayChungTu = @NgayChungTu, SoLuongThanhKhoan = @SoLuongThanhKhoan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_SXXK_DinhMucThanhKhoan WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LanThanhLy", SqlDbType.Int, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKN_VNACCS", SqlDbType.Decimal, "SoTKN_VNACCS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Invoid_HD", SqlDbType.VarChar, "Invoid_HD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongThanhKhoan", SqlDbType.Decimal, "SoLuongThanhKhoan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LanThanhLy", SqlDbType.Int, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSP", SqlDbType.VarChar, "MaSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKN_VNACCS", SqlDbType.Decimal, "SoTKN_VNACCS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Invoid_HD", SqlDbType.VarChar, "Invoid_HD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongThanhKhoan", SqlDbType.Decimal, "SoLuongThanhKhoan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@LanThanhLy", SqlDbType.Int, "LanThanhLy", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static DinhMucThanhKhoan Load(long id, int lanThanhLy)
		{
			const string spName = "[dbo].[p_DinhMucThanhKhoan_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, lanThanhLy);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<DinhMucThanhKhoan> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_DinhMucThanhKhoan_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_DinhMucThanhKhoan_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_DinhMucThanhKhoan_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_DinhMucThanhKhoan_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertDinhMucThanhKhoan(long iD, int lanThanhLy, string maSP, string maNPL, decimal soTKN_VNACCS, string invoid_HD, DateTime ngayChungTu, decimal soLuongThanhKhoan)
		{
			DinhMucThanhKhoan entity = new DinhMucThanhKhoan();	
			entity.ID = iD;
			entity.LanThanhLy = lanThanhLy;
			entity.MaSP = maSP;
			entity.MaNPL = maNPL;
			entity.SoTKN_VNACCS = soTKN_VNACCS;
			entity.Invoid_HD = invoid_HD;
			entity.NgayChungTu = ngayChungTu;
			entity.SoLuongThanhKhoan = soLuongThanhKhoan;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_DinhMucThanhKhoan_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);
			db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			db.AddInParameter(dbCommand, "@SoTKN_VNACCS", SqlDbType.Decimal, SoTKN_VNACCS);
			db.AddInParameter(dbCommand, "@Invoid_HD", SqlDbType.VarChar, Invoid_HD);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@SoLuongThanhKhoan", SqlDbType.Decimal, SoLuongThanhKhoan);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateDinhMucThanhKhoan(long id, int lanThanhLy, string maSP, string maNPL, decimal soTKN_VNACCS, string invoid_HD, DateTime ngayChungTu, decimal soLuongThanhKhoan)
		{
			DinhMucThanhKhoan entity = new DinhMucThanhKhoan();			
			entity.ID = id;
			entity.LanThanhLy = lanThanhLy;
			entity.MaSP = maSP;
			entity.MaNPL = maNPL;
			entity.SoTKN_VNACCS = soTKN_VNACCS;
			entity.Invoid_HD = invoid_HD;
			entity.NgayChungTu = ngayChungTu;
			entity.SoLuongThanhKhoan = soLuongThanhKhoan;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_DinhMucThanhKhoan_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);
			db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			db.AddInParameter(dbCommand, "@SoTKN_VNACCS", SqlDbType.Decimal, SoTKN_VNACCS);
			db.AddInParameter(dbCommand, "@Invoid_HD", SqlDbType.VarChar, Invoid_HD);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@SoLuongThanhKhoan", SqlDbType.Decimal, SoLuongThanhKhoan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateDinhMucThanhKhoan(long id, int lanThanhLy, string maSP, string maNPL, decimal soTKN_VNACCS, string invoid_HD, DateTime ngayChungTu, decimal soLuongThanhKhoan)
		{
			DinhMucThanhKhoan entity = new DinhMucThanhKhoan();			
			entity.ID = id;
			entity.LanThanhLy = lanThanhLy;
			entity.MaSP = maSP;
			entity.MaNPL = maNPL;
			entity.SoTKN_VNACCS = soTKN_VNACCS;
			entity.Invoid_HD = invoid_HD;
			entity.NgayChungTu = ngayChungTu;
			entity.SoLuongThanhKhoan = soLuongThanhKhoan;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_DinhMucThanhKhoan_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);
			db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			db.AddInParameter(dbCommand, "@SoTKN_VNACCS", SqlDbType.Decimal, SoTKN_VNACCS);
			db.AddInParameter(dbCommand, "@Invoid_HD", SqlDbType.VarChar, Invoid_HD);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@SoLuongThanhKhoan", SqlDbType.Decimal, SoLuongThanhKhoan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteDinhMucThanhKhoan(long id, int lanThanhLy)
		{
			DinhMucThanhKhoan entity = new DinhMucThanhKhoan();
			entity.ID = id;
			entity.LanThanhLy = lanThanhLy;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_DinhMucThanhKhoan_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_DinhMucThanhKhoan_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		#endregion
        #region Transacsion
        //add by minhnd
        public static int DeleteDynamicTransaction(SqlTransaction transaction, string whereCondition)
        {
            string spName = "p_DinhMucThanhKhoan_DeleteDynamic";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.VarChar, whereCondition);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        public static void InsertTransaction(SqlTransaction transaction, List<DinhMucThanhKhoan> collection)
        {
            try
            {
                foreach (DinhMucThanhKhoan item in collection)
                {
                    item.Insert(transaction);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        //add by minhnd
        #endregion

        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}