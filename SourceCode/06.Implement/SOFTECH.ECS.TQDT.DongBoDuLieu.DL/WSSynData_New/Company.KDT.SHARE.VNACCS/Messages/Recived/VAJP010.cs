﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAJP010 : BasicVNACC
    {

        public List<VAJP010_HANG> HangHoa { get; set; }
        public void LoadVAJP010(string strResult)
        {
            try
            {
                this.GetObject<VAJP010>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAJP010, false, VAJP010.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAJP010.TongSoByte);
                while (true)
                {
                    VAJP010_HANG ivaHang = new VAJP010_HANG();
                    ivaHang.GetObject<VAJP010_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAJP010_HANG", false, VAJP010_HANG.TongSoByte);
                    if (this.HangHoa == null) this.HangHoa = new List<VAJP010_HANG>();
                    this.HangHoa.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAJP010_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAJP010_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAJP010_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
