﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace KeyServer
{
    public partial class Customer
    {


        public static List<Customer> LoadFromKeys(string productID)
        {
            const string spName = "[dbo].[p_LM_Customer_SelectFromKey]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ProductID", SqlDbType.NVarChar, productID);
            Customer entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            List<Customer> collection = new List<Customer>();
            while (reader.Read())
            {
                entity = new Customer();
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerCode"))) entity.CustomerCode = reader.GetString(reader.GetOrdinal("CustomerCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerName"))) entity.CustomerName = reader.GetString(reader.GetOrdinal("CustomerName"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }		

            
    }
}
