﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Smo;
using System.Data;

namespace Company.KDT.SHARE.Components
{
    public class SQL
    {
        //public static Microsoft.SqlServer.Management.Smo.Server server;
        static SqlConnection sqlConnection = new SqlConnection();
        /// <summary>
        /// Initializes the field 'server'
        /// </summary>
        public static void InitializeServer()
        {
            // To Connect to our SQL Server - we Can use the Connection from the System.Data.SqlClient Namespace.
            SqlConnection sqlConnection = new SqlConnection(@"Integrated Security=SSPI; Data Source=(local)\ECSEXPRESS");

            //build a "serverConnection" with the information of the "sqlConnection"
            //Microsoft.SqlServer.Management.Common.ServerConnection serverConnection = new Microsoft.SqlServer.Management.Common.ServerConnection(sqlConnection);

            //The "serverConnection is used in the ctor of the Server.
            //server = new Server(serverConnection);
        }

        /// <summary>
        /// Initializes the field 'server'
        /// </summary>
        public static void InitializeServer(string sqlConnectionString)
        {
            // To Connect to our SQL Server - we Can use the Connection from the System.Data.SqlClient Namespace.
            sqlConnection = new SqlConnection(sqlConnectionString);

            //build a "serverConnection" with the information of the "sqlConnection"
            //Microsoft.SqlServer.Management.Common.ServerConnection serverConnection = new Microsoft.SqlServer.Management.Common.ServerConnection(sqlConnection);

            //The "serverConnection is used in the ctor of the Server.
            //server = new Server(serverConnection);
        }

        public static List<string> ListDatabases()
        {
            List<string> listData = new List<string>();
            DataTable dtSource = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            string query = "SELECT name FROM sys.sysdatabases";
            adapter.SelectCommand = new SqlCommand(query, sqlConnection);
            adapter.Fill(dtSource);
            foreach (DataRow dr in dtSource.Rows)
            {
                listData.Add(dr[0].ToString());
            }
            return listData;
        }

        //private BackupDevice backupDevice;

        //public BackupDevice BackupDevice
        //{
        //    get { return backupDevice; }
        //    set { backupDevice = value; }
        //}
    }
}
