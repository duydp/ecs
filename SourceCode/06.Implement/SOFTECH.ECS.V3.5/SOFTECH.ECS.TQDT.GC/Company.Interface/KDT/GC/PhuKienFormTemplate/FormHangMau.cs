﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using System.Data;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using System.Linq;
using Company.KDT.SHARE.QuanLyChungTu;
namespace Company.Interface.KDT.GC
{
    public partial class FormHangMau : BaseFormHaveGuidPanel
    {
        public HopDong HD = new HopDong();
        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new PhuKienDangKy();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
        public string MaLoaiPK = string.Empty;
        public bool boolFlag;
        public String Caption;
        public bool IsChange;
        public FormHangMau()
        {
            InitializeComponent();
            this.Load += new EventHandler(FormHangMau_Load);
            txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = LoaiPK.HPKCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || HD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || HD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }

        private void FormHangMau_Load(object sender, EventArgs e)
        {
            txtSoLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;

            bool isSua = MaLoaiPK == "505";
            dgList.RootTable.Columns["ThongTinCu"].Visible = txtMaCu.ReadOnly =
            lblCu.Visible = txtMaCu.Visible = lblTenCu.Visible = isSua;

            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";


            cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

            Caption = this.Text;
            if (this.OpenType == OpenFormType.View)
            {
                btnAdd.Enabled = false;
                btnXoa.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False; ;
            }
            else
            {

                btnAdd.Enabled = true;
                btnXoa.Enabled = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True; ;
            }
            if (LoaiPK.HPKCollection.Count == 0) LoaiPK.LoadCollection();
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == MaLoaiPK)
                {
                    LoaiPK = pk;
                    break;
                }
            }
            if (boolFlag == true)
            {
                btnAdd.Enabled = true;
            }
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            LoaiPK.MaPhuKien = MaLoaiPK;
            if (LoaiPK.HPKCollection.Count == 0)
                LoaiPK.LoadCollection();
            if (MaLoaiPK == "105")
            {
                txtMa.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            }
            BindData();

        }
        private void SetData()
        {
            try
            {
                txtMa.TextChanged -= new EventHandler(txt_TextChanged);
                txtMa.Text = HangPK.MaHang.Trim();
                txtMa.TextChanged += new EventHandler(txt_TextChanged);

                txtTen.TextChanged -= new EventHandler(txt_TextChanged);
                txtTen.Text = HangPK.TenHang.Trim();
                txtTen.TextChanged += new EventHandler(txt_TextChanged);

                txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaHS.Text = HangPK.MaHS;
                txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

                cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
                cbDonViTinh.SelectedValue = HangPK.DVT_ID;
                cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

                txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong.Value = HangPK.SoLuong;
                txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

                txtMaCu.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaCu.Text = HangPK.ThongTinCu.Trim();
                txtMaCu.TextChanged += new EventHandler(txt_TextChanged);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void reset()
        {
            txtMa.TextChanged -= new EventHandler(txt_TextChanged);
            txtMa.Text = "";
            txtMa.TextChanged += new EventHandler(txt_TextChanged);

            txtTen.TextChanged -= new EventHandler(txt_TextChanged);
            txtTen.Text = "";
            txtTen.TextChanged += new EventHandler(txt_TextChanged);

            txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaHS.Text = "";
            txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

            txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoLuong.Text = "";
            txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

            error.SetError(txtMaHS, null);
            error.SetError(txtMa, null);
            error.SetError(txtTen, null);
            BindData();
            HangPK = new HangPhuKien();


        }
        private void setError()
        {
            error.Clear();
            error.SetError(txtTen, null);
            error.SetError(txtMa, null);
            error.SetError(txtMaHS, null);

        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!ValidateForm(false))
                return;
            txtMa.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;
            {
                checkExitsAndSTTHang();
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                if (MaLoaiPK == "805")
                {
                    isValid &= ValidateControl.ValidateSpecialChar(txtMa, errorProvider, "Mã hàng mẫu", isOnlyWarning);
                }
                else if (MaLoaiPK == "505")
                {
                    isValid &= ValidateControl.ValidateSpecialChar(txtMa, errorProvider, "Mã hàng mẫu", isOnlyWarning);
                    isValid &= ValidateControl.ValidateSpecialChar(txtMaCu, errorProvider, "Mã hàng mẫu", isOnlyWarning);
                }
                else
                {
                    isValid &= ValidateControl.ValidateSpecialChar(txtMaCu, errorProvider, "Mã hàng mẫu", isOnlyWarning);
                }
                //Cursor = Cursors.WaitCursor;

            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            return isValid;
        }
        private void checkExitsAndSTTHang()
        {
            if (string.IsNullOrEmpty(MaLoaiPK))
            {
                ShowMessage("Mã loại phụ kiện chưa thiết lập", false);
                DialogResult = DialogResult.Cancel;
            }
            LoaiPK.HPKCollection.Remove(HangPK);
            foreach (HangPhuKien p in LoaiPK.HPKCollection)
            {
                if (p.MaHang.Trim() == txtMa.Text)
                {
                    //ShowMessage("Đã có loại sản phẩm này trong danh sách.", false);
                    showMsg("Mã này đã được thêm vào rồi");
                    if (HangPK.MaHang.Trim() != "")
                        LoaiPK.HPKCollection.Add(HangPK);
                    txtMa.Focus();
                    return;
                }
            }
            HangPK.MaHang = txtMa.Text.Trim();
            HangPK.TenHang = txtTen.Text.Trim();
            HangPK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            HangPK.MaHS = txtMaHS.Text.Trim();
            HangPK.ThongTinCu = txtMaCu.Text.Trim();
            HangPK.SoLuong = Convert.ToDecimal(txtSoLuong.Value);
            LoaiPK.HPKCollection.Add(HangPK);
            LoaiPK.Master_ID = pkdk.ID;
            LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
            if (boolFlag == true)
            {
                LoaiPK.InsertUpdateBoSungHM(pkdk.HopDong_ID);
                boolFlag = false;
            }
            else
            {
                LoaiPK.InsertUpdateBoSungChiTietLoaiPhuKien(pkdk.HopDong_ID);
            }

            Company.GC.BLL.KDT.GC.LoaiPhuKien lpk = pkdk.PKCollection.ToArray().SingleOrDefault(l => l.MaPhuKien == MaLoaiPK);
            if (lpk == null)
                pkdk.PKCollection.Add(LoaiPK);
            reset();
            this.setErro();
            this.SetChange(true);
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {

            HangPK = (Company.GC.BLL.KDT.GC.HangPhuKien)e.Row.DataRow;
            if (HangPK != null)
            {
                SetData();
                error.Clear();
            }
        }



        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
        }

        private void ctrNguyenTe_Load(object sender, EventArgs e)
        {

        }

        private void dgHangMau_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {

            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem row in items)
                    {
                        Company.GC.BLL.KDT.GC.HangMau tbDelete = (Company.GC.BLL.KDT.GC.HangMau)row.GetRow().DataRow;
                        tbDelete.Delete();
                        this.SetChange(false);
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        private void setErro()
        {
            error.Clear();
            error.SetError(txtMa, null);
            error.SetError(txtTen, null);
            error.SetError(txtMaHS, null);

        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            HangPhuKienCollection hpkColl = new HangPhuKienCollection();
            if (LoaiPK.HPKCollection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;

            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa thông tin đã chọn không ?", true) == "Yes")
            {
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (pkDelete.ID > 0)
                        {
                            pkDelete.Delete(LoaiPK.MaPhuKien, pkdk.HopDong_ID);
                        }
                        hpkColl.Add(pkDelete);
                    }
                }
                foreach (Company.GC.BLL.KDT.GC.HangPhuKien hpkk in hpkColl)
                {
                    try
                    {
                        LoaiPK.HPKCollection.Remove(hpkk);
                    }
                    catch { }

                }
                this.reset();
                this.setErro();
                this.SetChange(false);
            }
        }
        private void btnChon_Click(object sender, EventArgs e)
        {
            FormChonHangMau frm = new FormChonHangMau();
            frm.HD = HD;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (frm.HPKCollection.Count > 0)
                    {
                        if (LoaiPK.HPKCollection.Count == 0)
                        {
                            foreach (HangPhuKien hangPK in frm.HPKCollection)
                            {
                                hangPK.Master_ID = LoaiPK.ID;
                                LoaiPK.HPKCollection.Add(hangPK);
                            }
                        }
                        else
                        {
                            bool IsExits = false;
                            foreach (HangPhuKien hangPK in frm.HPKCollection)
                            {
                                IsExits = false;
                                foreach (HangPhuKien item in LoaiPK.HPKCollection)
                                {
                                    if (hangPK.MaHang == item.MaHang)
                                    {
                                        IsExits = true;
                                        break;
                                    }
                                }
                                if (!IsExits)
                                {
                                    hangPK.Master_ID = LoaiPK.ID;
                                    LoaiPK.HPKCollection.Add(hangPK);
                                }
                            }
                        }
                        this.SetChange(true);
                    }
                    //foreach (HangPhuKien hangPK in frm.HPKCollection)
                    //{
                    //    hangPK.Master_ID = LoaiPK.ID;
                    //    LoaiPK.HPKCollection.Add(hangPK);
                    //}
                    LoaiPK.Master_ID = pkdk.ID;
                    LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                    if (boolFlag == true)
                    {
                        LoaiPK.InsertUpdateBoSungDaDangKy(pkdk.HopDong_ID);
                        boolFlag = false;
                    }
                    else
                    {
                        LoaiPK.InsertUpdateBoSungChiTietLoaiPhuKien(pkdk.HopDong_ID);
                    }
                    Company.GC.BLL.KDT.GC.LoaiPhuKien lpk = pkdk.PKCollection.ToArray().SingleOrDefault(l => l.MaPhuKien == MaLoaiPK);
                    if (lpk == null)
                        pkdk.PKCollection.Add(LoaiPK);
                    BindData();
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                reset();
                this.setErro();
                this.SetChange(true);
            }
        }
        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            HangPK = (Company.GC.BLL.KDT.GC.HangPhuKien)i.GetRow().DataRow;
                            if (HangPK != null)
                            {
                                SetData();
                                error.Clear();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH HÀNG MẪU KHAI BÁO CỦA PHỤ KIỆN.xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaHM.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}