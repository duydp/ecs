namespace LicenseCode.Module
{
    partial class FrmCustomerInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCustomerInfo));
            this.panelTop = new DevExpress.XtraEditors.PanelControl();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.lblCaption = new DevExpress.XtraEditors.LabelControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupCustAddress = new DevExpress.XtraEditors.GroupControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtCustFax = new DevExpress.XtraEditors.TextEdit();
            this.txtCustNotes = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtCustPhone = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtCustAddress = new DevExpress.XtraEditors.MemoEdit();
            this.txtCustCity = new DevExpress.XtraEditors.TextEdit();
            this.txtCustEmail = new DevExpress.XtraEditors.TextEdit();
            this.txtCustPostalCode = new DevExpress.XtraEditors.TextEdit();
            this.txtCustProvince = new DevExpress.XtraEditors.TextEdit();
            this.groupCustInfo = new DevExpress.XtraEditors.GroupControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txtMaDN = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtCustName = new DevExpress.XtraEditors.TextEdit();
            this.txtCustFirstName = new DevExpress.XtraEditors.TextEdit();
            this.txtCustLastName = new DevExpress.XtraEditors.TextEdit();
            this.txtCustTitle = new DevExpress.XtraEditors.TextEdit();
            this.txtCustDepartment = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.txtCustCode = new DevExpress.XtraEditors.TextEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.ProductId = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).BeginInit();
            this.panelTop.SuspendLayout();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupCustAddress)).BeginInit();
            this.groupCustAddress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustPostalCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustProvince.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupCustInfo)).BeginInit();
            this.groupCustInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaDN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustTitle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelTop.Controls.Add(this.pnlTop);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(842, 67);
            this.panelTop.TabIndex = 0;
            // 
            // pnlTop
            // 
            this.pnlTop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlTop.BackgroundImage")));
            this.pnlTop.Controls.Add(this.lblCaption);
            this.pnlTop.Controls.Add(this.pictureBox1);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(842, 67);
            this.pnlTop.TabIndex = 10;
            // 
            // lblCaption
            // 
            this.lblCaption.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaption.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblCaption.Appearance.Options.UseFont = true;
            this.lblCaption.Appearance.Options.UseForeColor = true;
            this.lblCaption.Location = new System.Drawing.Point(72, 8);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(221, 23);
            this.lblCaption.TabIndex = 2;
            this.lblCaption.Text = "THÔNG TIN KHÁCH HÀNG";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(69, 67);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.gridControl1);
            this.panelControl1.Controls.Add(this.groupCustAddress);
            this.panelControl1.Controls.Add(this.groupCustInfo);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.btnEdit);
            this.panelControl1.Controls.Add(this.btnClose);
            this.panelControl1.Controls.Add(this.labelControl18);
            this.panelControl1.Controls.Add(this.btnSave);
            this.panelControl1.Controls.Add(this.txtCustCode);
            this.panelControl1.Controls.Add(this.btnCancel);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 67);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(842, 522);
            this.panelControl1.TabIndex = 1;
            // 
            // groupCustAddress
            // 
            this.groupCustAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupCustAddress.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupCustAddress.AppearanceCaption.Options.UseFont = true;
            this.groupCustAddress.Controls.Add(this.labelControl10);
            this.groupCustAddress.Controls.Add(this.labelControl9);
            this.groupCustAddress.Controls.Add(this.labelControl8);
            this.groupCustAddress.Controls.Add(this.labelControl13);
            this.groupCustAddress.Controls.Add(this.labelControl14);
            this.groupCustAddress.Controls.Add(this.txtCustFax);
            this.groupCustAddress.Controls.Add(this.txtCustNotes);
            this.groupCustAddress.Controls.Add(this.labelControl12);
            this.groupCustAddress.Controls.Add(this.labelControl11);
            this.groupCustAddress.Controls.Add(this.txtCustPhone);
            this.groupCustAddress.Controls.Add(this.labelControl7);
            this.groupCustAddress.Controls.Add(this.txtCustAddress);
            this.groupCustAddress.Controls.Add(this.txtCustCity);
            this.groupCustAddress.Controls.Add(this.txtCustEmail);
            this.groupCustAddress.Controls.Add(this.txtCustPostalCode);
            this.groupCustAddress.Controls.Add(this.txtCustProvince);
            this.groupCustAddress.Location = new System.Drawing.Point(11, 186);
            this.groupCustAddress.Name = "groupCustAddress";
            this.groupCustAddress.Size = new System.Drawing.Size(819, 187);
            this.groupCustAddress.TabIndex = 25;
            this.groupCustAddress.Text = "Địa chỉ liên hệ khách hàng";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(298, 94);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(95, 13);
            this.labelControl10.TabIndex = 1;
            this.labelControl10.Text = "Thư điện tử (Email):";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(328, 62);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(65, 13);
            this.labelControl9.TabIndex = 1;
            this.labelControl9.Text = "Quận/Huyện:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(314, 36);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(79, 13);
            this.labelControl8.TabIndex = 1;
            this.labelControl8.Text = "Tỉnh/Thành phố:";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(39, 145);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(37, 13);
            this.labelControl13.TabIndex = 1;
            this.labelControl13.Text = "Số Fax:";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(319, 119);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(74, 13);
            this.labelControl14.TabIndex = 1;
            this.labelControl14.Text = "Thông tin khác:";
            // 
            // txtCustFax
            // 
            this.txtCustFax.Location = new System.Drawing.Point(82, 142);
            this.txtCustFax.Name = "txtCustFax";
            this.txtCustFax.Properties.Mask.EditMask = "((\\d{3,4}-\\d{4})|(\\d{4,5}-\\d{3}-\\d{3}))";
            this.txtCustFax.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCustFax.Size = new System.Drawing.Size(144, 20);
            this.txtCustFax.TabIndex = 3;
            // 
            // txtCustNotes
            // 
            this.txtCustNotes.Location = new System.Drawing.Point(399, 116);
            this.txtCustNotes.Name = "txtCustNotes";
            this.txtCustNotes.Size = new System.Drawing.Size(215, 49);
            this.txtCustNotes.TabIndex = 7;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(10, 119);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(66, 13);
            this.labelControl12.TabIndex = 1;
            this.labelControl12.Text = "Số điện thoại:";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(31, 93);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(45, 13);
            this.labelControl11.TabIndex = 1;
            this.labelControl11.Text = "Mã vùng:";
            // 
            // txtCustPhone
            // 
            this.txtCustPhone.Location = new System.Drawing.Point(82, 116);
            this.txtCustPhone.Name = "txtCustPhone";
            this.txtCustPhone.Properties.Mask.EditMask = "((\\d{3,4}-\\d{4})|(\\d{4,5}-\\d{3}-\\d{3}))";
            this.txtCustPhone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCustPhone.Size = new System.Drawing.Size(144, 20);
            this.txtCustPhone.TabIndex = 2;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(39, 36);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(36, 13);
            this.labelControl7.TabIndex = 1;
            this.labelControl7.Text = "Địa chỉ:";
            // 
            // txtCustAddress
            // 
            this.txtCustAddress.Location = new System.Drawing.Point(82, 33);
            this.txtCustAddress.Name = "txtCustAddress";
            this.txtCustAddress.Size = new System.Drawing.Size(201, 51);
            this.txtCustAddress.TabIndex = 0;
            // 
            // txtCustCity
            // 
            this.txtCustCity.Location = new System.Drawing.Point(399, 33);
            this.txtCustCity.Name = "txtCustCity";
            this.txtCustCity.Size = new System.Drawing.Size(138, 20);
            this.txtCustCity.TabIndex = 4;
            // 
            // txtCustEmail
            // 
            this.txtCustEmail.Location = new System.Drawing.Point(399, 91);
            this.txtCustEmail.Name = "txtCustEmail";
            this.txtCustEmail.Properties.Mask.EditMask = "([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)" +
                "+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)";
            this.txtCustEmail.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCustEmail.Size = new System.Drawing.Size(215, 20);
            this.txtCustEmail.TabIndex = 6;
            // 
            // txtCustPostalCode
            // 
            this.txtCustPostalCode.Location = new System.Drawing.Point(82, 90);
            this.txtCustPostalCode.Name = "txtCustPostalCode";
            this.txtCustPostalCode.Properties.Mask.EditMask = "(\\d{3}|\\d{4})";
            this.txtCustPostalCode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCustPostalCode.Size = new System.Drawing.Size(71, 20);
            this.txtCustPostalCode.TabIndex = 1;
            // 
            // txtCustProvince
            // 
            this.txtCustProvince.Location = new System.Drawing.Point(399, 59);
            this.txtCustProvince.Name = "txtCustProvince";
            this.txtCustProvince.Size = new System.Drawing.Size(138, 20);
            this.txtCustProvince.TabIndex = 5;
            // 
            // groupCustInfo
            // 
            this.groupCustInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupCustInfo.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupCustInfo.AppearanceCaption.Options.UseFont = true;
            this.groupCustInfo.Controls.Add(this.labelControl17);
            this.groupCustInfo.Controls.Add(this.txtMaDN);
            this.groupCustInfo.Controls.Add(this.labelControl16);
            this.groupCustInfo.Controls.Add(this.labelControl15);
            this.groupCustInfo.Controls.Add(this.labelControl6);
            this.groupCustInfo.Controls.Add(this.labelControl4);
            this.groupCustInfo.Controls.Add(this.labelControl5);
            this.groupCustInfo.Controls.Add(this.labelControl3);
            this.groupCustInfo.Controls.Add(this.labelControl2);
            this.groupCustInfo.Controls.Add(this.txtCustName);
            this.groupCustInfo.Controls.Add(this.txtCustFirstName);
            this.groupCustInfo.Controls.Add(this.txtCustLastName);
            this.groupCustInfo.Controls.Add(this.txtCustTitle);
            this.groupCustInfo.Controls.Add(this.txtCustDepartment);
            this.groupCustInfo.Location = new System.Drawing.Point(12, 32);
            this.groupCustInfo.Name = "groupCustInfo";
            this.groupCustInfo.Size = new System.Drawing.Size(819, 148);
            this.groupCustInfo.TabIndex = 1;
            this.groupCustInfo.Text = "Thông tin khách hàng";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(397, 36);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(86, 13);
            this.labelControl17.TabIndex = 9;
            this.labelControl17.Text = "Mã doanh nghiệp:";
            // 
            // txtMaDN
            // 
            this.txtMaDN.Location = new System.Drawing.Point(486, 33);
            this.txtMaDN.Name = "txtMaDN";
            this.txtMaDN.Size = new System.Drawing.Size(140, 20);
            this.txtMaDN.TabIndex = 1;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl16.Appearance.Options.UseFont = true;
            this.labelControl16.Appearance.Options.UseForeColor = true;
            this.labelControl16.Location = new System.Drawing.Point(343, 98);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(121, 13);
            this.labelControl16.TabIndex = 6;
            this.labelControl16.Text = "Họ và tên người đại diện.";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic);
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl15.Appearance.Options.UseFont = true;
            this.labelControl15.Appearance.Options.UseForeColor = true;
            this.labelControl15.Location = new System.Drawing.Point(314, 56);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(148, 13);
            this.labelControl15.TabIndex = 6;
            this.labelControl15.Text = "Tên đại diện hoặc tên Công ty.";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(260, 123);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(56, 13);
            this.labelControl6.TabIndex = 1;
            this.labelControl6.Text = "Chức danh:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(301, 78);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(17, 13);
            this.labelControl4.TabIndex = 1;
            this.labelControl4.Text = "Họ:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(49, 123);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(43, 13);
            this.labelControl5.TabIndex = 1;
            this.labelControl5.Text = "Bộ phận:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(72, 78);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(22, 13);
            this.labelControl3.TabIndex = 1;
            this.labelControl3.Text = "Tên:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(14, 36);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(80, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Tên khách hàng:";
            // 
            // txtCustName
            // 
            this.txtCustName.Location = new System.Drawing.Point(100, 33);
            this.txtCustName.Name = "txtCustName";
            this.txtCustName.Size = new System.Drawing.Size(292, 20);
            this.txtCustName.TabIndex = 0;
            // 
            // txtCustFirstName
            // 
            this.txtCustFirstName.Location = new System.Drawing.Point(100, 75);
            this.txtCustFirstName.Name = "txtCustFirstName";
            this.txtCustFirstName.Size = new System.Drawing.Size(126, 20);
            this.txtCustFirstName.TabIndex = 2;
            // 
            // txtCustLastName
            // 
            this.txtCustLastName.Location = new System.Drawing.Point(324, 75);
            this.txtCustLastName.Name = "txtCustLastName";
            this.txtCustLastName.Size = new System.Drawing.Size(141, 20);
            this.txtCustLastName.TabIndex = 3;
            // 
            // txtCustTitle
            // 
            this.txtCustTitle.Location = new System.Drawing.Point(323, 120);
            this.txtCustTitle.Name = "txtCustTitle";
            this.txtCustTitle.Size = new System.Drawing.Size(141, 20);
            this.txtCustTitle.TabIndex = 5;
            // 
            // txtCustDepartment
            // 
            this.txtCustDepartment.Location = new System.Drawing.Point(99, 120);
            this.txtCustDepartment.Name = "txtCustDepartment";
            this.txtCustDepartment.Size = new System.Drawing.Size(126, 20);
            this.txtCustDepartment.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(30, 9);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(76, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Mã khách hàng:";
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.Location = new System.Drawing.Point(550, 379);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(59, 22);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(757, 379);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(72, 22);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(615, 379);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(57, 22);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtCustCode
            // 
            this.txtCustCode.Location = new System.Drawing.Point(112, 6);
            this.txtCustCode.Name = "txtCustCode";
            this.txtCustCode.Properties.MaxLength = 10;
            this.txtCustCode.Properties.ReadOnly = true;
            this.txtCustCode.Size = new System.Drawing.Size(101, 20);
            this.txtCustCode.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(678, 379);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(73, 22);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Hủy bỏ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gridControl1.Location = new System.Drawing.Point(0, 407);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(842, 115);
            this.gridControl1.TabIndex = 26;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ProductId});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.Options.UseFont = true;
            this.labelControl18.Location = new System.Drawing.Point(11, 382);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(186, 19);
            this.labelControl18.TabIndex = 1;
            this.labelControl18.Text = "Danh sách key đã cấp: ";
            // 
            // ProductId
            // 
            this.ProductId.Caption = "Sản phẩm";
            this.ProductId.FieldName = "ProductId";
            this.ProductId.Name = "ProductId";
            this.ProductId.Visible = true;
            this.ProductId.VisibleIndex = 0;
            // 
            // FrmCustomerInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 589);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelTop);
            this.Name = "FrmCustomerInfo";
            this.Text = "Thêm mới khách hàng";
            this.Load += new System.EventHandler(this.FrmCustomerInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelTop)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupCustAddress)).EndInit();
            this.groupCustAddress.ResumeLayout(false);
            this.groupCustAddress.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustPostalCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustProvince.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupCustInfo)).EndInit();
            this.groupCustInfo.ResumeLayout(false);
            this.groupCustInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaDN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustTitle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelTop;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.TextEdit txtCustTitle;
        private DevExpress.XtraEditors.TextEdit txtCustDepartment;
        private DevExpress.XtraEditors.TextEdit txtCustLastName;
        private DevExpress.XtraEditors.TextEdit txtCustFirstName;
        private DevExpress.XtraEditors.TextEdit txtCustName;
        private DevExpress.XtraEditors.MemoEdit txtCustNotes;
        private DevExpress.XtraEditors.TextEdit txtCustEmail;
        private DevExpress.XtraEditors.TextEdit txtCustFax;
        private DevExpress.XtraEditors.TextEdit txtCustPhone;
        private DevExpress.XtraEditors.TextEdit txtCustPostalCode;
        private DevExpress.XtraEditors.TextEdit txtCustProvince;
        private DevExpress.XtraEditors.TextEdit txtCustCity;
        private DevExpress.XtraEditors.MemoEdit txtCustAddress;
        private DevExpress.XtraEditors.TextEdit txtCustCode;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private System.Windows.Forms.Panel pnlTop;
        private DevExpress.XtraEditors.LabelControl lblCaption;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.GroupControl groupCustInfo;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.GroupControl groupCustAddress;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtMaDN;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraGrid.Columns.GridColumn ProductId;
    }
}