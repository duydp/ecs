using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_Nation : ICloneable
	{
		#region Properties.
		
		public string ResultCode { set; get; }
		public int PGNumber { set; get; }
		public string TableID { set; get; }
		public string ProcessClassification { set; get; }
		public int CreatorClassification { set; get; }
		public string NumberOfKeyItems { set; get; }
		public string NationCode { set; get; }
		public string GenerationManagementIndication { set; get; }
		public DateTime DateOfMaintenanceUpdated { set; get; }
		public string CountryShortName { set; get; }
		public DateTime ApplicationStartDate { set; get; }
		public int PlaceOfOriginIsMFNTaxRatesApplicationCountry { set; get; }
		public string ImportTaxClassificationCodeForFTA1 { set; get; }
		public string ImportTaxClassificationCodeForFTA2 { set; get; }
		public string ImportTaxClassificationCodeForFTA3 { set; get; }
		public string ImportTaxClassificationCodeForFTA4 { set; get; }
		public string ImportTaxClassificationCodeForFTA5 { set; get; }
		public string ImportTaxClassificationCodeForFTA6 { set; get; }
		public string ImportTaxClassificationCodeForFTA7 { set; get; }
		public string ImportTaxClassificationCodeForFTA8 { set; get; }
		public string ImportTaxClassificationCodeForFTA9 { set; get; }
		public string ImportTaxClassificationCodeForFTA10 { set; get; }
		public string ImportTaxClassificationCodeForFTA11 { set; get; }
		public string ImportTaxClassificationCodeForFTA12 { set; get; }
		public string ImportTaxClassificationCodeForFTA13 { set; get; }
		public string ImportTaxClassificationCodeForFTA14 { set; get; }
		public string ImportTaxClassificationCodeForFTA15 { set; get; }
		public string ImportTaxClassificationCodeForFTA16 { set; get; }
		public string ImportTaxClassificationCodeForFTA17 { set; get; }
		public string ImportTaxClassificationCodeForFTA18 { set; get; }
		public string ImportTaxClassificationCodeForFTA19 { set; get; }
		public string ImportTaxClassificationCodeForFTA20 { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<VNACC_Category_Nation> ConvertToCollection(IDataReader reader)
		{
			List<VNACC_Category_Nation> collection = new List<VNACC_Category_Nation>();
			while (reader.Read())
			{
				VNACC_Category_Nation entity = new VNACC_Category_Nation();
				if (!reader.IsDBNull(reader.GetOrdinal("ResultCode"))) entity.ResultCode = reader.GetString(reader.GetOrdinal("ResultCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("PGNumber"))) entity.PGNumber = reader.GetInt32(reader.GetOrdinal("PGNumber"));
				if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ProcessClassification"))) entity.ProcessClassification = reader.GetString(reader.GetOrdinal("ProcessClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatorClassification"))) entity.CreatorClassification = reader.GetInt32(reader.GetOrdinal("CreatorClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("NumberOfKeyItems"))) entity.NumberOfKeyItems = reader.GetString(reader.GetOrdinal("NumberOfKeyItems"));
				if (!reader.IsDBNull(reader.GetOrdinal("NationCode"))) entity.NationCode = reader.GetString(reader.GetOrdinal("NationCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("GenerationManagementIndication"))) entity.GenerationManagementIndication = reader.GetString(reader.GetOrdinal("GenerationManagementIndication"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateOfMaintenanceUpdated"))) entity.DateOfMaintenanceUpdated = reader.GetDateTime(reader.GetOrdinal("DateOfMaintenanceUpdated"));
				if (!reader.IsDBNull(reader.GetOrdinal("CountryShortName"))) entity.CountryShortName = reader.GetString(reader.GetOrdinal("CountryShortName"));
				if (!reader.IsDBNull(reader.GetOrdinal("ApplicationStartDate"))) entity.ApplicationStartDate = reader.GetDateTime(reader.GetOrdinal("ApplicationStartDate"));
				if (!reader.IsDBNull(reader.GetOrdinal("PlaceOfOriginIsMFNTaxRatesApplicationCountry"))) entity.PlaceOfOriginIsMFNTaxRatesApplicationCountry = reader.GetInt32(reader.GetOrdinal("PlaceOfOriginIsMFNTaxRatesApplicationCountry"));
#if GC_V4
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA1"))) entity.ImportTaxClassificationCodeForFTA1 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA1"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA2"))) entity.ImportTaxClassificationCodeForFTA2 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA2"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA3"))) entity.ImportTaxClassificationCodeForFTA3 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA3"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA4"))) entity.ImportTaxClassificationCodeForFTA4 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA4"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA5"))) entity.ImportTaxClassificationCodeForFTA5 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA5"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA6"))) entity.ImportTaxClassificationCodeForFTA6 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA6"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA7"))) entity.ImportTaxClassificationCodeForFTA7 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA7"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA8"))) entity.ImportTaxClassificationCodeForFTA8 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA8"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA9"))) entity.ImportTaxClassificationCodeForFTA9 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA9"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA10"))) entity.ImportTaxClassificationCodeForFTA10 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA10"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA11"))) entity.ImportTaxClassificationCodeForFTA11 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA11"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA12"))) entity.ImportTaxClassificationCodeForFTA12 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA12"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA13"))) entity.ImportTaxClassificationCodeForFTA13 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA13"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA14"))) entity.ImportTaxClassificationCodeForFTA14 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA14"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA15"))) entity.ImportTaxClassificationCodeForFTA15 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA15"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA16"))) entity.ImportTaxClassificationCodeForFTA16 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA16"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA17"))) entity.ImportTaxClassificationCodeForFTA17 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA17"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA18"))) entity.ImportTaxClassificationCodeForFTA18 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA18"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA19"))) entity.ImportTaxClassificationCodeForFTA19 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA19"));
				if (!reader.IsDBNull(reader.GetOrdinal("ImportTaxClassificationCodeForFTA20"))) entity.ImportTaxClassificationCodeForFTA20 = reader.GetString(reader.GetOrdinal("ImportTaxClassificationCodeForFTA20"));
#elif SXXK_V4
#endif
                if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
        public static List<VNACC_Category_Nation> ConvertDataSetToCollection(DataSet dataSet)
        {
            List<VNACC_Category_Nation> collection = new List<VNACC_Category_Nation>();
            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                VNACC_Category_Nation entity = new VNACC_Category_Nation();
                entity.ResultCode = dataSet.Tables[0].Rows[i]["ResultCode"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["ResultCode"].ToString() : String.Empty;
                entity.PGNumber = dataSet.Tables[0].Rows[i]["PGNumber"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["PGNumber"].ToString()) : 0;
                entity.TableID = dataSet.Tables[0].Rows[i]["TableID"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["TableID"].ToString() : "A015A";
                entity.ProcessClassification = dataSet.Tables[0].Rows[i]["ProcessClassification"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["ProcessClassification"].ToString() : String.Empty;
                entity.CreatorClassification = dataSet.Tables[0].Rows[i]["CreatorClassification"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["CreatorClassification"].ToString()) : 0;
                entity.NumberOfKeyItems = dataSet.Tables[0].Rows[i]["NumberOfKeyItems"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["NumberOfKeyItems"].ToString() : String.Empty;
                entity.NationCode = dataSet.Tables[0].Rows[i]["NationCode"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["NationCode"].ToString() : String.Empty;
                entity.GenerationManagementIndication = dataSet.Tables[0].Rows[i]["GenerationManagementIndication"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["GenerationManagementIndication"].ToString() : String.Empty;
                entity.DateOfMaintenanceUpdated = dataSet.Tables[0].Rows[i]["DateOfMaintenanceUpdated"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateOfMaintenanceUpdated"].ToString()) : DateTime.Now;
                entity.CountryShortName = dataSet.Tables[0].Rows[i]["CountryShortName"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["CountryShortName"].ToString() : String.Empty;
                entity.ApplicationStartDate = dataSet.Tables[0].Rows[i]["ApplicationStartDate"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["ApplicationStartDate"].ToString()) : DateTime.Now;
                entity.PlaceOfOriginIsMFNTaxRatesApplicationCountry = dataSet.Tables[0].Rows[i]["PlaceOfOriginIsMFNTaxRatesApplicationCountry"] != DBNull.Value ? Convert.ToInt32(dataSet.Tables[0].Rows[i]["PlaceOfOriginIsMFNTaxRatesApplicationCountry"].ToString()) : 0;
                entity.Notes = dataSet.Tables[0].Rows[i]["Notes"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["Notes"].ToString() : String.Empty;
                entity.InputMessageID = dataSet.Tables[0].Rows[i]["InputMessageID"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["InputMessageID"].ToString() : String.Empty;
                entity.MessageTag = dataSet.Tables[0].Rows[i]["MessageTag"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["MessageTag"].ToString() : String.Empty;
                entity.IndexTag = dataSet.Tables[0].Rows[i]["IndexTag"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["IndexTag"].ToString() : String.Empty;
                collection.Add(entity);
            }
            return collection;
        }
		public static bool Find(List<VNACC_Category_Nation> collection, string nationCode)
        {
            foreach (VNACC_Category_Nation item in collection)
            {
                if (item.NationCode == nationCode)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_VNACC_Category_Nation VALUES(@ResultCode, @PGNumber, @TableID, @ProcessClassification, @CreatorClassification, @NumberOfKeyItems, @NationCode, @GenerationManagementIndication, @DateOfMaintenanceUpdated, @CountryShortName, @ApplicationStartDate, @PlaceOfOriginIsMFNTaxRatesApplicationCountry, @ImportTaxClassificationCodeForFTA1, @ImportTaxClassificationCodeForFTA2, @ImportTaxClassificationCodeForFTA3, @ImportTaxClassificationCodeForFTA4, @ImportTaxClassificationCodeForFTA5, @ImportTaxClassificationCodeForFTA6, @ImportTaxClassificationCodeForFTA7, @ImportTaxClassificationCodeForFTA8, @ImportTaxClassificationCodeForFTA9, @ImportTaxClassificationCodeForFTA10, @ImportTaxClassificationCodeForFTA11, @ImportTaxClassificationCodeForFTA12, @ImportTaxClassificationCodeForFTA13, @ImportTaxClassificationCodeForFTA14, @ImportTaxClassificationCodeForFTA15, @ImportTaxClassificationCodeForFTA16, @ImportTaxClassificationCodeForFTA17, @ImportTaxClassificationCodeForFTA18, @ImportTaxClassificationCodeForFTA19, @ImportTaxClassificationCodeForFTA20, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_Nation SET ResultCode = @ResultCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, CreatorClassification = @CreatorClassification, NumberOfKeyItems = @NumberOfKeyItems, GenerationManagementIndication = @GenerationManagementIndication, DateOfMaintenanceUpdated = @DateOfMaintenanceUpdated, CountryShortName = @CountryShortName, ApplicationStartDate = @ApplicationStartDate, PlaceOfOriginIsMFNTaxRatesApplicationCountry = @PlaceOfOriginIsMFNTaxRatesApplicationCountry, ImportTaxClassificationCodeForFTA1 = @ImportTaxClassificationCodeForFTA1, ImportTaxClassificationCodeForFTA2 = @ImportTaxClassificationCodeForFTA2, ImportTaxClassificationCodeForFTA3 = @ImportTaxClassificationCodeForFTA3, ImportTaxClassificationCodeForFTA4 = @ImportTaxClassificationCodeForFTA4, ImportTaxClassificationCodeForFTA5 = @ImportTaxClassificationCodeForFTA5, ImportTaxClassificationCodeForFTA6 = @ImportTaxClassificationCodeForFTA6, ImportTaxClassificationCodeForFTA7 = @ImportTaxClassificationCodeForFTA7, ImportTaxClassificationCodeForFTA8 = @ImportTaxClassificationCodeForFTA8, ImportTaxClassificationCodeForFTA9 = @ImportTaxClassificationCodeForFTA9, ImportTaxClassificationCodeForFTA10 = @ImportTaxClassificationCodeForFTA10, ImportTaxClassificationCodeForFTA11 = @ImportTaxClassificationCodeForFTA11, ImportTaxClassificationCodeForFTA12 = @ImportTaxClassificationCodeForFTA12, ImportTaxClassificationCodeForFTA13 = @ImportTaxClassificationCodeForFTA13, ImportTaxClassificationCodeForFTA14 = @ImportTaxClassificationCodeForFTA14, ImportTaxClassificationCodeForFTA15 = @ImportTaxClassificationCodeForFTA15, ImportTaxClassificationCodeForFTA16 = @ImportTaxClassificationCodeForFTA16, ImportTaxClassificationCodeForFTA17 = @ImportTaxClassificationCodeForFTA17, ImportTaxClassificationCodeForFTA18 = @ImportTaxClassificationCodeForFTA18, ImportTaxClassificationCodeForFTA19 = @ImportTaxClassificationCodeForFTA19, ImportTaxClassificationCodeForFTA20 = @ImportTaxClassificationCodeForFTA20, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE NationCode = @NationCode";
            string delete = "DELETE FROM t_VNACC_Category_Nation WHERE NationCode = @NationCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NationCode", SqlDbType.VarChar, "NationCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GenerationManagementIndication", SqlDbType.VarChar, "GenerationManagementIndication", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateOfMaintenanceUpdated", SqlDbType.DateTime, "DateOfMaintenanceUpdated", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CountryShortName", SqlDbType.VarChar, "CountryShortName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ApplicationStartDate", SqlDbType.DateTime, "ApplicationStartDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PlaceOfOriginIsMFNTaxRatesApplicationCountry", SqlDbType.Int, "PlaceOfOriginIsMFNTaxRatesApplicationCountry", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA1", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA2", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA3", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA4", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA5", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA6", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA7", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA8", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA9", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA10", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA11", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA11", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA12", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA12", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA13", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA13", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA14", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA14", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA15", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA15", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA16", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA16", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA17", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA17", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA18", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA18", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA19", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA19", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA20", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA20", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NationCode", SqlDbType.VarChar, "NationCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GenerationManagementIndication", SqlDbType.VarChar, "GenerationManagementIndication", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateOfMaintenanceUpdated", SqlDbType.DateTime, "DateOfMaintenanceUpdated", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CountryShortName", SqlDbType.VarChar, "CountryShortName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ApplicationStartDate", SqlDbType.DateTime, "ApplicationStartDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PlaceOfOriginIsMFNTaxRatesApplicationCountry", SqlDbType.Int, "PlaceOfOriginIsMFNTaxRatesApplicationCountry", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA1", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA2", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA3", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA4", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA5", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA6", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA7", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA8", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA9", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA10", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA11", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA11", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA12", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA12", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA13", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA13", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA14", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA14", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA15", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA15", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA16", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA16", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA17", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA17", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA18", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA18", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA19", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA19", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA20", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA20", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@NationCode", SqlDbType.VarChar, "NationCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_VNACC_Category_Nation VALUES(@ResultCode, @PGNumber, @TableID, @ProcessClassification, @CreatorClassification, @NumberOfKeyItems, @NationCode, @GenerationManagementIndication, @DateOfMaintenanceUpdated, @CountryShortName, @ApplicationStartDate, @PlaceOfOriginIsMFNTaxRatesApplicationCountry, @ImportTaxClassificationCodeForFTA1, @ImportTaxClassificationCodeForFTA2, @ImportTaxClassificationCodeForFTA3, @ImportTaxClassificationCodeForFTA4, @ImportTaxClassificationCodeForFTA5, @ImportTaxClassificationCodeForFTA6, @ImportTaxClassificationCodeForFTA7, @ImportTaxClassificationCodeForFTA8, @ImportTaxClassificationCodeForFTA9, @ImportTaxClassificationCodeForFTA10, @ImportTaxClassificationCodeForFTA11, @ImportTaxClassificationCodeForFTA12, @ImportTaxClassificationCodeForFTA13, @ImportTaxClassificationCodeForFTA14, @ImportTaxClassificationCodeForFTA15, @ImportTaxClassificationCodeForFTA16, @ImportTaxClassificationCodeForFTA17, @ImportTaxClassificationCodeForFTA18, @ImportTaxClassificationCodeForFTA19, @ImportTaxClassificationCodeForFTA20, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_Nation SET ResultCode = @ResultCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, CreatorClassification = @CreatorClassification, NumberOfKeyItems = @NumberOfKeyItems, GenerationManagementIndication = @GenerationManagementIndication, DateOfMaintenanceUpdated = @DateOfMaintenanceUpdated, CountryShortName = @CountryShortName, ApplicationStartDate = @ApplicationStartDate, PlaceOfOriginIsMFNTaxRatesApplicationCountry = @PlaceOfOriginIsMFNTaxRatesApplicationCountry, ImportTaxClassificationCodeForFTA1 = @ImportTaxClassificationCodeForFTA1, ImportTaxClassificationCodeForFTA2 = @ImportTaxClassificationCodeForFTA2, ImportTaxClassificationCodeForFTA3 = @ImportTaxClassificationCodeForFTA3, ImportTaxClassificationCodeForFTA4 = @ImportTaxClassificationCodeForFTA4, ImportTaxClassificationCodeForFTA5 = @ImportTaxClassificationCodeForFTA5, ImportTaxClassificationCodeForFTA6 = @ImportTaxClassificationCodeForFTA6, ImportTaxClassificationCodeForFTA7 = @ImportTaxClassificationCodeForFTA7, ImportTaxClassificationCodeForFTA8 = @ImportTaxClassificationCodeForFTA8, ImportTaxClassificationCodeForFTA9 = @ImportTaxClassificationCodeForFTA9, ImportTaxClassificationCodeForFTA10 = @ImportTaxClassificationCodeForFTA10, ImportTaxClassificationCodeForFTA11 = @ImportTaxClassificationCodeForFTA11, ImportTaxClassificationCodeForFTA12 = @ImportTaxClassificationCodeForFTA12, ImportTaxClassificationCodeForFTA13 = @ImportTaxClassificationCodeForFTA13, ImportTaxClassificationCodeForFTA14 = @ImportTaxClassificationCodeForFTA14, ImportTaxClassificationCodeForFTA15 = @ImportTaxClassificationCodeForFTA15, ImportTaxClassificationCodeForFTA16 = @ImportTaxClassificationCodeForFTA16, ImportTaxClassificationCodeForFTA17 = @ImportTaxClassificationCodeForFTA17, ImportTaxClassificationCodeForFTA18 = @ImportTaxClassificationCodeForFTA18, ImportTaxClassificationCodeForFTA19 = @ImportTaxClassificationCodeForFTA19, ImportTaxClassificationCodeForFTA20 = @ImportTaxClassificationCodeForFTA20, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE NationCode = @NationCode";
            string delete = "DELETE FROM t_VNACC_Category_Nation WHERE NationCode = @NationCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NationCode", SqlDbType.VarChar, "NationCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GenerationManagementIndication", SqlDbType.VarChar, "GenerationManagementIndication", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateOfMaintenanceUpdated", SqlDbType.DateTime, "DateOfMaintenanceUpdated", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CountryShortName", SqlDbType.VarChar, "CountryShortName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ApplicationStartDate", SqlDbType.DateTime, "ApplicationStartDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PlaceOfOriginIsMFNTaxRatesApplicationCountry", SqlDbType.Int, "PlaceOfOriginIsMFNTaxRatesApplicationCountry", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA1", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA2", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA3", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA4", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA5", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA6", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA7", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA8", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA9", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA10", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA11", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA11", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA12", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA12", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA13", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA13", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA14", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA14", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA15", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA15", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA16", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA16", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA17", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA17", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA18", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA18", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA19", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA19", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ImportTaxClassificationCodeForFTA20", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA20", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NationCode", SqlDbType.VarChar, "NationCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GenerationManagementIndication", SqlDbType.VarChar, "GenerationManagementIndication", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateOfMaintenanceUpdated", SqlDbType.DateTime, "DateOfMaintenanceUpdated", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CountryShortName", SqlDbType.VarChar, "CountryShortName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ApplicationStartDate", SqlDbType.DateTime, "ApplicationStartDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PlaceOfOriginIsMFNTaxRatesApplicationCountry", SqlDbType.Int, "PlaceOfOriginIsMFNTaxRatesApplicationCountry", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA1", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA2", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA3", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA4", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA5", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA6", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA7", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA8", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA9", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA10", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA11", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA11", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA12", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA12", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA13", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA13", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA14", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA14", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA15", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA15", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA16", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA16", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA17", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA17", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA18", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA18", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA19", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA19", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ImportTaxClassificationCodeForFTA20", SqlDbType.VarChar, "ImportTaxClassificationCodeForFTA20", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@NationCode", SqlDbType.VarChar, "NationCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static VNACC_Category_Nation Load(string nationCode)
		{
			const string spName = "[dbo].[p_VNACC_Category_Nation_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@NationCode", SqlDbType.VarChar, nationCode);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<VNACC_Category_Nation> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<VNACC_Category_Nation> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<VNACC_Category_Nation> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_Nation_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_Nation_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_Nation_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_Nation_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertVNACC_Category_Nation(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string generationManagementIndication, DateTime dateOfMaintenanceUpdated, string countryShortName, DateTime applicationStartDate, int placeOfOriginIsMFNTaxRatesApplicationCountry, string importTaxClassificationCodeForFTA1, string importTaxClassificationCodeForFTA2, string importTaxClassificationCodeForFTA3, string importTaxClassificationCodeForFTA4, string importTaxClassificationCodeForFTA5, string importTaxClassificationCodeForFTA6, string importTaxClassificationCodeForFTA7, string importTaxClassificationCodeForFTA8, string importTaxClassificationCodeForFTA9, string importTaxClassificationCodeForFTA10, string importTaxClassificationCodeForFTA11, string importTaxClassificationCodeForFTA12, string importTaxClassificationCodeForFTA13, string importTaxClassificationCodeForFTA14, string importTaxClassificationCodeForFTA15, string importTaxClassificationCodeForFTA16, string importTaxClassificationCodeForFTA17, string importTaxClassificationCodeForFTA18, string importTaxClassificationCodeForFTA19, string importTaxClassificationCodeForFTA20, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_Nation entity = new VNACC_Category_Nation();	
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.GenerationManagementIndication = generationManagementIndication;
			entity.DateOfMaintenanceUpdated = dateOfMaintenanceUpdated;
			entity.CountryShortName = countryShortName;
			entity.ApplicationStartDate = applicationStartDate;
			entity.PlaceOfOriginIsMFNTaxRatesApplicationCountry = placeOfOriginIsMFNTaxRatesApplicationCountry;
			entity.ImportTaxClassificationCodeForFTA1 = importTaxClassificationCodeForFTA1;
			entity.ImportTaxClassificationCodeForFTA2 = importTaxClassificationCodeForFTA2;
			entity.ImportTaxClassificationCodeForFTA3 = importTaxClassificationCodeForFTA3;
			entity.ImportTaxClassificationCodeForFTA4 = importTaxClassificationCodeForFTA4;
			entity.ImportTaxClassificationCodeForFTA5 = importTaxClassificationCodeForFTA5;
			entity.ImportTaxClassificationCodeForFTA6 = importTaxClassificationCodeForFTA6;
			entity.ImportTaxClassificationCodeForFTA7 = importTaxClassificationCodeForFTA7;
			entity.ImportTaxClassificationCodeForFTA8 = importTaxClassificationCodeForFTA8;
			entity.ImportTaxClassificationCodeForFTA9 = importTaxClassificationCodeForFTA9;
			entity.ImportTaxClassificationCodeForFTA10 = importTaxClassificationCodeForFTA10;
			entity.ImportTaxClassificationCodeForFTA11 = importTaxClassificationCodeForFTA11;
			entity.ImportTaxClassificationCodeForFTA12 = importTaxClassificationCodeForFTA12;
			entity.ImportTaxClassificationCodeForFTA13 = importTaxClassificationCodeForFTA13;
			entity.ImportTaxClassificationCodeForFTA14 = importTaxClassificationCodeForFTA14;
			entity.ImportTaxClassificationCodeForFTA15 = importTaxClassificationCodeForFTA15;
			entity.ImportTaxClassificationCodeForFTA16 = importTaxClassificationCodeForFTA16;
			entity.ImportTaxClassificationCodeForFTA17 = importTaxClassificationCodeForFTA17;
			entity.ImportTaxClassificationCodeForFTA18 = importTaxClassificationCodeForFTA18;
			entity.ImportTaxClassificationCodeForFTA19 = importTaxClassificationCodeForFTA19;
			entity.ImportTaxClassificationCodeForFTA20 = importTaxClassificationCodeForFTA20;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_VNACC_Category_Nation_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@NationCode", SqlDbType.VarChar, NationCode);
			db.AddInParameter(dbCommand, "@GenerationManagementIndication", SqlDbType.VarChar, GenerationManagementIndication);
			db.AddInParameter(dbCommand, "@DateOfMaintenanceUpdated", SqlDbType.DateTime, DateOfMaintenanceUpdated.Year <= 1753 ? DBNull.Value : (object) DateOfMaintenanceUpdated);
			db.AddInParameter(dbCommand, "@CountryShortName", SqlDbType.VarChar, CountryShortName);
			db.AddInParameter(dbCommand, "@ApplicationStartDate", SqlDbType.DateTime, ApplicationStartDate.Year <= 1753 ? DBNull.Value : (object) ApplicationStartDate);
			db.AddInParameter(dbCommand, "@PlaceOfOriginIsMFNTaxRatesApplicationCountry", SqlDbType.Int, PlaceOfOriginIsMFNTaxRatesApplicationCountry);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA1", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA1);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA2", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA2);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA3", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA3);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA4", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA4);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA5", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA5);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA6", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA6);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA7", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA7);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA8", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA8);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA9", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA9);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA10", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA10);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA11", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA11);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA12", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA12);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA13", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA13);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA14", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA14);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA15", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA15);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA16", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA16);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA17", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA17);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA18", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA18);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA19", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA19);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA20", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA20);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<VNACC_Category_Nation> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Nation item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateVNACC_Category_Nation(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string nationCode, string generationManagementIndication, DateTime dateOfMaintenanceUpdated, string countryShortName, DateTime applicationStartDate, int placeOfOriginIsMFNTaxRatesApplicationCountry, string importTaxClassificationCodeForFTA1, string importTaxClassificationCodeForFTA2, string importTaxClassificationCodeForFTA3, string importTaxClassificationCodeForFTA4, string importTaxClassificationCodeForFTA5, string importTaxClassificationCodeForFTA6, string importTaxClassificationCodeForFTA7, string importTaxClassificationCodeForFTA8, string importTaxClassificationCodeForFTA9, string importTaxClassificationCodeForFTA10, string importTaxClassificationCodeForFTA11, string importTaxClassificationCodeForFTA12, string importTaxClassificationCodeForFTA13, string importTaxClassificationCodeForFTA14, string importTaxClassificationCodeForFTA15, string importTaxClassificationCodeForFTA16, string importTaxClassificationCodeForFTA17, string importTaxClassificationCodeForFTA18, string importTaxClassificationCodeForFTA19, string importTaxClassificationCodeForFTA20, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_Nation entity = new VNACC_Category_Nation();			
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.NationCode = nationCode;
			entity.GenerationManagementIndication = generationManagementIndication;
			entity.DateOfMaintenanceUpdated = dateOfMaintenanceUpdated;
			entity.CountryShortName = countryShortName;
			entity.ApplicationStartDate = applicationStartDate;
			entity.PlaceOfOriginIsMFNTaxRatesApplicationCountry = placeOfOriginIsMFNTaxRatesApplicationCountry;
			entity.ImportTaxClassificationCodeForFTA1 = importTaxClassificationCodeForFTA1;
			entity.ImportTaxClassificationCodeForFTA2 = importTaxClassificationCodeForFTA2;
			entity.ImportTaxClassificationCodeForFTA3 = importTaxClassificationCodeForFTA3;
			entity.ImportTaxClassificationCodeForFTA4 = importTaxClassificationCodeForFTA4;
			entity.ImportTaxClassificationCodeForFTA5 = importTaxClassificationCodeForFTA5;
			entity.ImportTaxClassificationCodeForFTA6 = importTaxClassificationCodeForFTA6;
			entity.ImportTaxClassificationCodeForFTA7 = importTaxClassificationCodeForFTA7;
			entity.ImportTaxClassificationCodeForFTA8 = importTaxClassificationCodeForFTA8;
			entity.ImportTaxClassificationCodeForFTA9 = importTaxClassificationCodeForFTA9;
			entity.ImportTaxClassificationCodeForFTA10 = importTaxClassificationCodeForFTA10;
			entity.ImportTaxClassificationCodeForFTA11 = importTaxClassificationCodeForFTA11;
			entity.ImportTaxClassificationCodeForFTA12 = importTaxClassificationCodeForFTA12;
			entity.ImportTaxClassificationCodeForFTA13 = importTaxClassificationCodeForFTA13;
			entity.ImportTaxClassificationCodeForFTA14 = importTaxClassificationCodeForFTA14;
			entity.ImportTaxClassificationCodeForFTA15 = importTaxClassificationCodeForFTA15;
			entity.ImportTaxClassificationCodeForFTA16 = importTaxClassificationCodeForFTA16;
			entity.ImportTaxClassificationCodeForFTA17 = importTaxClassificationCodeForFTA17;
			entity.ImportTaxClassificationCodeForFTA18 = importTaxClassificationCodeForFTA18;
			entity.ImportTaxClassificationCodeForFTA19 = importTaxClassificationCodeForFTA19;
			entity.ImportTaxClassificationCodeForFTA20 = importTaxClassificationCodeForFTA20;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_VNACC_Category_Nation_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@NationCode", SqlDbType.VarChar, NationCode);
			db.AddInParameter(dbCommand, "@GenerationManagementIndication", SqlDbType.VarChar, GenerationManagementIndication);
			db.AddInParameter(dbCommand, "@DateOfMaintenanceUpdated", SqlDbType.DateTime, DateOfMaintenanceUpdated.Year <= 1753 ? DBNull.Value : (object) DateOfMaintenanceUpdated);
			db.AddInParameter(dbCommand, "@CountryShortName", SqlDbType.VarChar, CountryShortName);
			db.AddInParameter(dbCommand, "@ApplicationStartDate", SqlDbType.DateTime, ApplicationStartDate.Year <= 1753 ? DBNull.Value : (object) ApplicationStartDate);
			db.AddInParameter(dbCommand, "@PlaceOfOriginIsMFNTaxRatesApplicationCountry", SqlDbType.Int, PlaceOfOriginIsMFNTaxRatesApplicationCountry);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA1", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA1);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA2", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA2);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA3", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA3);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA4", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA4);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA5", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA5);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA6", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA6);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA7", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA7);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA8", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA8);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA9", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA9);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA10", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA10);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA11", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA11);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA12", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA12);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA13", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA13);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA14", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA14);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA15", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA15);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA16", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA16);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA17", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA17);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA18", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA18);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA19", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA19);
            db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA20", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA20);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<VNACC_Category_Nation> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Nation item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateVNACC_Category_Nation(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string nationCode, string generationManagementIndication, DateTime dateOfMaintenanceUpdated, string countryShortName, DateTime applicationStartDate, int placeOfOriginIsMFNTaxRatesApplicationCountry, string importTaxClassificationCodeForFTA1, string importTaxClassificationCodeForFTA2, string importTaxClassificationCodeForFTA3, string importTaxClassificationCodeForFTA4, string importTaxClassificationCodeForFTA5, string importTaxClassificationCodeForFTA6, string importTaxClassificationCodeForFTA7, string importTaxClassificationCodeForFTA8, string importTaxClassificationCodeForFTA9, string importTaxClassificationCodeForFTA10, string importTaxClassificationCodeForFTA11, string importTaxClassificationCodeForFTA12, string importTaxClassificationCodeForFTA13, string importTaxClassificationCodeForFTA14, string importTaxClassificationCodeForFTA15, string importTaxClassificationCodeForFTA16, string importTaxClassificationCodeForFTA17, string importTaxClassificationCodeForFTA18, string importTaxClassificationCodeForFTA19, string importTaxClassificationCodeForFTA20, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_Nation entity = new VNACC_Category_Nation();			
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.NationCode = nationCode;
			entity.GenerationManagementIndication = generationManagementIndication;
			entity.DateOfMaintenanceUpdated = dateOfMaintenanceUpdated;
			entity.CountryShortName = countryShortName;
			entity.ApplicationStartDate = applicationStartDate;
			entity.PlaceOfOriginIsMFNTaxRatesApplicationCountry = placeOfOriginIsMFNTaxRatesApplicationCountry;
			entity.ImportTaxClassificationCodeForFTA1 = importTaxClassificationCodeForFTA1;
			entity.ImportTaxClassificationCodeForFTA2 = importTaxClassificationCodeForFTA2;
			entity.ImportTaxClassificationCodeForFTA3 = importTaxClassificationCodeForFTA3;
			entity.ImportTaxClassificationCodeForFTA4 = importTaxClassificationCodeForFTA4;
			entity.ImportTaxClassificationCodeForFTA5 = importTaxClassificationCodeForFTA5;
			entity.ImportTaxClassificationCodeForFTA6 = importTaxClassificationCodeForFTA6;
			entity.ImportTaxClassificationCodeForFTA7 = importTaxClassificationCodeForFTA7;
			entity.ImportTaxClassificationCodeForFTA8 = importTaxClassificationCodeForFTA8;
			entity.ImportTaxClassificationCodeForFTA9 = importTaxClassificationCodeForFTA9;
			entity.ImportTaxClassificationCodeForFTA10 = importTaxClassificationCodeForFTA10;
			entity.ImportTaxClassificationCodeForFTA11 = importTaxClassificationCodeForFTA11;
			entity.ImportTaxClassificationCodeForFTA12 = importTaxClassificationCodeForFTA12;
			entity.ImportTaxClassificationCodeForFTA13 = importTaxClassificationCodeForFTA13;
			entity.ImportTaxClassificationCodeForFTA14 = importTaxClassificationCodeForFTA14;
			entity.ImportTaxClassificationCodeForFTA15 = importTaxClassificationCodeForFTA15;
			entity.ImportTaxClassificationCodeForFTA16 = importTaxClassificationCodeForFTA16;
			entity.ImportTaxClassificationCodeForFTA17 = importTaxClassificationCodeForFTA17;
			entity.ImportTaxClassificationCodeForFTA18 = importTaxClassificationCodeForFTA18;
			entity.ImportTaxClassificationCodeForFTA19 = importTaxClassificationCodeForFTA19;
			entity.ImportTaxClassificationCodeForFTA20 = importTaxClassificationCodeForFTA20;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_Nation_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@NationCode", SqlDbType.VarChar, NationCode);
			db.AddInParameter(dbCommand, "@GenerationManagementIndication", SqlDbType.VarChar, GenerationManagementIndication);
			db.AddInParameter(dbCommand, "@DateOfMaintenanceUpdated", SqlDbType.DateTime, DateOfMaintenanceUpdated.Year <= 1753 ? DBNull.Value : (object) DateOfMaintenanceUpdated);
			db.AddInParameter(dbCommand, "@CountryShortName", SqlDbType.VarChar, CountryShortName);
			db.AddInParameter(dbCommand, "@ApplicationStartDate", SqlDbType.DateTime, ApplicationStartDate.Year <= 1753 ? DBNull.Value : (object) ApplicationStartDate);
			db.AddInParameter(dbCommand, "@PlaceOfOriginIsMFNTaxRatesApplicationCountry", SqlDbType.Int, PlaceOfOriginIsMFNTaxRatesApplicationCountry);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA1", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA1);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA2", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA2);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA3", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA3);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA4", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA4);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA5", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA5);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA6", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA6);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA7", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA7);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA8", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA8);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA9", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA9);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA10", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA10);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA11", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA11);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA12", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA12);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA13", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA13);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA14", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA14);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA15", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA15);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA16", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA16);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA17", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA17);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA18", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA18);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA19", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA19);
			db.AddInParameter(dbCommand, "@ImportTaxClassificationCodeForFTA20", SqlDbType.VarChar, ImportTaxClassificationCodeForFTA20);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<VNACC_Category_Nation> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Nation item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteVNACC_Category_Nation(string nationCode)
		{
			VNACC_Category_Nation entity = new VNACC_Category_Nation();
			entity.NationCode = nationCode;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_Nation_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@NationCode", SqlDbType.VarChar, NationCode);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_VNACC_Category_Nation_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<VNACC_Category_Nation> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_Nation item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}