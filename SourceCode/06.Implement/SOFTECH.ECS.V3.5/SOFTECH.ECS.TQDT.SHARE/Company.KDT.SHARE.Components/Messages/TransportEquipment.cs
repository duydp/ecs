﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
  public class TransportEquipment
    {
        [XmlElement("characteristic")]
        public string Characteristic { get; set; }
        [XmlElement("fullness")]
        public string Fullness { get; set; }

      /// <summary>
      /// Số container
      /// </summary>
        [XmlElement("container")]
        public string Container { get; set; }

        /// <summary>
        /// Số vận đơn
        /// </summary>
        [XmlElement("billOfLading")]
        public string BillOfLading { get; set; }

      /// <summary>
      /// số seal
      /// </summary>
      [XmlElement("seal")]
        public string Seal { get; set; }

      /// <summary>
      /// Số seal HQ
      /// </summary>
      [XmlElement("customsSeal")]
        public string CustomsSeal { get; set; }

      /// <summary>
      /// Loại container
      /// </summary>
      [XmlElement("type")]
      public string Type { get; set; }

      /// <summary>
      /// code 64bit
      /// </summary>
        [XmlElement("codeContent")]
        public string CodeContent { get; set; }

        /// <summary>
        /// Số hiệu container
        /// </summary>
        [XmlElement("EquipmentIdentification")]
        public EquipmentIdentification EquipmentIdentifications { get; set; }
        /// <summary>
        /// Trọng lượng hàng hóa - không tính thiết bị của nhà vận tải
        /// </summary>
        [XmlElement("grossMass")]
        public string GrossMass { get; set; }
        /// <summary>
        /// Trọng lượng tịnh của hàng hóa - không tính thiết bị của nhà vận tải
        /// </summary>
        [XmlElement("netMass")]
        public string NetMass { get; set; }

        /// <summary>
        /// Địa điểm đóng hàng
        /// </summary>
        [XmlElement("packagingLocation")]
        public string PackagingLocation { get; set; }

        /// <summary>
        /// Số lượng kiện trong container
        /// </summary>
        [XmlElement("quantity")]
        public string Quantity { get; set; }

        /// <summary>
        /// Tính chất cont
        /// </summary>
        [XmlElement("property")]
        public string Property { get; set; }

      //Cont cho hàng rời
        /// <summary>
        /// Số lượng hàng
        /// </summary>
        [XmlElement("cargoPiece")]
        public string CargoPiece{ get; set; }

        //Cont cho hàng rời
        /// <summary>
        /// DVT số lượng hàng
        /// </summary>
        [XmlElement("pieceUnitCode")]
        public string PieceUnitCode { get; set; }
        /// <summary>
        /// DVT tổng lượng hàng
        /// </summary>
        [XmlElement("cargoWeight")]
        public string CargoWeight { get; set; }
        /// <summary>
        /// DVT tổng trọng lượng hàng
        /// </summary>
        [XmlElement("weightUnitCode")]
        public string WeightUnitCode{ get; set; }
        #region V5



        ///// <summary>
        ///// Số vận đơn
        ///// </summary>
        //[XmlElement("container")]
        //public string Container { get; set; }



        /// <summary>
        /// Số vận đơn
        /// </summary>
        [XmlElement("content")]
        public string Content { get; set; }
        #endregion

    }
}
