﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using Company.KDT.SHARE.VNACCS;
using System.Data.Common;
using Company.KDT.SHARE.Components;
using System.Threading;

namespace Company.Interface
{
    public partial class PerformanceDatabaseForm : Company.Interface.BaseForm
    {
        protected SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        public DataTable dtDatabase = new DataTable();
        public DataTable dtServer = new DataTable();
        public PerformanceDatabaseForm()
        {
            InitializeComponent();
        }

        private void PerformanceDatabaseForm_Load(object sender, EventArgs e)
        {
            dtDatabase = GetDatabaseInformation();
            //GetServerInstances();
            dtServer = GetSQLServerInformation();
            SetInformation();
            BindDatabase();
        }
        public DataTable GetDatabaseInformation()
        {
            try
            {
                string WhereCondition = "sys.databases.name='" + GlobalSettings.DATABASE_NAME + "'";
                string spName = "p_Database_Information";
                SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
                this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, WhereCondition);
                return this.db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public DataTable GetSQLServerInformation()
        {
            try
            {
                string spName = "p_Server_Information";
                SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
                return this.db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public DataTable DeleteDatabaseLog()
        {
            try
            {
                string spName = "p_Delete_LogDatabase";
                SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
                this.db.AddInParameter(dbCommand, "@DatabaseName", SqlDbType.NVarChar, GlobalSettings.DATABASE_NAME);
                this.db.AddInParameter(dbCommand, "@DatabaseLog", SqlDbType.NVarChar, dtDatabase.Rows[1]["LOGICAL_NAME"]);
                return this.db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public void CreateIndex(int Index)
        {
            string Sql = "";
            switch (Index)
            {
                case 1:
                    Sql = "BEGIN " +
    " IF EXISTS ( SELECT  * " +
                " FROM    sys.indexes " +
                " WHERE   name = 'IDX_KDT_VNACC_ToKhaiMauDich' " +
                        " AND object_id = OBJECT_ID('t_KDT_VNACC_ToKhaiMauDich') ) " +
        " BEGIN " +
            " DROP INDEX IDX_KDT_VNACC_ToKhaiMauDich ON dbo.t_KDT_VNACC_ToKhaiMauDich; " +
        " END; " +
    " ELSE " +
        " BEGIN " +
            " CREATE NONCLUSTERED INDEX IDX_KDT_VNACC_ToKhaiMauDich " +
            " ON [dbo].[t_KDT_VNACC_ToKhaiMauDich] ([MaDonVi],[TrangThaiXuLy],[CoQuanHaiQuan]) " +
            " INCLUDE ([ID],[SoToKhai],[SoToKhaiDauTien],[MaLoaiHinh],[NgayDangKy],[TenDoiTac],[MaKetQuaKiemTra],[SoHoaDon],[GhiChu],[SoQuanLyNoiBoDN],[HopDong_ID],[HopDong_So]); " +
        " END; " +
" END;";
                    break;
                case 2:
                    Sql = "BEGIN " +
   " IF EXISTS ( SELECT  * " +
                " FROM    sys.indexes " +
                " WHERE   name = 'IX_KDT_VNACC_ToKhaiMauDich' " +
                        " AND object_id = OBJECT_ID('t_KDT_VNACC_ToKhaiMauDich') ) " +
        " BEGIN " +
            " DROP INDEX IX_KDT_VNACC_ToKhaiMauDich ON dbo.t_KDT_VNACC_ToKhaiMauDich; " +
        " END; " +
    " ELSE " +
        " BEGIN " +
            " CREATE NONCLUSTERED INDEX IX_KDT_VNACC_ToKhaiMauDich " +
            " ON dbo.t_KDT_VNACC_ToKhaiMauDich ([ID], " +
            " [SoToKhai], " +
            " [SoToKhaiDauTien], " +
            " [MaLoaiHinh], " +
            " [CoQuanHaiQuan], " +
            " [NgayDangKy], " +
            " [MaKetQuaKiemTra], " +
            " [SoHoaDon], " +
            " [TrangThaiXuLy] " +
            " ); " +
        " END; " +
 " END;";
                    break;
#if GC_V4
                case 3:
                    Sql = "BEGIN " +
   " IF EXISTS ( SELECT  * " +
               " FROM    sys.indexes " +
               " WHERE   name = 'IDX_t_KDT_GC_HopDong' " +
                       " AND object_id = OBJECT_ID('t_KDT_GC_HopDong') ) " +
       " BEGIN " +
           " DROP INDEX IDX_t_KDT_GC_HopDong ON dbo.t_KDT_GC_HopDong; " +
       " END; " +
   " ELSE " +
       " BEGIN " +
           " CREATE INDEX IDX_t_KDT_GC_HopDong " +
           " ON dbo.t_KDT_GC_HopDong  " +
           " ( " +
           " ID, " +
           " MaDoanhNghiep, " +
           " SoTiepNhan, " +
           " SoHopDong, " +
           " NgayKy, " +
           " NgayTiepNhan, " +
           " TrangThaiXuLy, " +
           " TrangThaiThanhKhoan " +
           " ); " +
       " END;  " +
" END;";
                    break;

                case 4:
                    Sql = "  BEGIN " +
    " IF EXISTS ( SELECT  * " +
                " FROM    sys.indexes " +
                " WHERE   name = 'IDX_t_KDT_GC_DinhMucDangKy' " +
                        " AND object_id = OBJECT_ID('t_KDT_GC_DinhMucDangKy') ) " +
        " BEGIN " +
            " DROP INDEX IDX_t_KDT_GC_DinhMucDangKy ON dbo.t_KDT_GC_DinhMucDangKy; " +
        " END; " +
    " ELSE " +
        " BEGIN " +
            " CREATE INDEX IDX_t_KDT_GC_DinhMucDangKy " +
            " ON dbo.t_KDT_GC_DinhMucDangKy " +
            " ( " +
            " ID, " +
            " MaHaiQuan, " +
            " MaDoanhNghiep, " +
            " ID_HopDong, " +
            " NgayTiepNhan, " +
            " SoTiepNhan, " +
            " TrangThaiXuLy " +
            " ); " +
        " END;  " +
  " END;";
                    break;
                case 5:
                    Sql = "    BEGIN" +
        " IF EXISTS ( SELECT  *" +
                    " FROM    sys.indexes" +
                    " WHERE   name = 'IDX_t_KDT_GC_DinhMuc'" +
                            " AND object_id = OBJECT_ID('t_KDT_GC_DinhMuc') )" +
            " BEGIN" +
                " DROP INDEX IDX_t_KDT_GC_DinhMuc ON dbo.t_KDT_GC_DinhMuc;" +
            " END;" +
        " ELSE" +
            " BEGIN" +
                " CREATE INDEX IDX_t_KDT_GC_DinhMuc" +
                " ON dbo.t_KDT_GC_DinhMuc" +
                " (" +
                " ID," +
                " Master_ID," +
                " MaNguyenPhuLieu," +
                " MaSanPham" +
                " );" +
            " END; " +
    " END;";
                    break;
                case 6:
                    Sql = "        BEGIN" +
            " IF EXISTS ( SELECT  *" +
                        " FROM    sys.indexes" +
                        " WHERE   name = 'IDX_t_KDT_GC_DinhMucThucTeDangKy'" +
                                " AND object_id = OBJECT_ID('t_KDT_GC_DinhMucThucTeDangKy') )" +
                " BEGIN" +
                    " DROP INDEX IDX_t_KDT_GC_DinhMucThucTeDangKy ON dbo.t_KDT_GC_DinhMucThucTeDangKy;" +
                " END;" +
            " ELSE" +
                " BEGIN" +
                    " CREATE INDEX IDX_t_KDT_GC_DinhMucThucTeDangKy" +
                    " ON dbo.t_KDT_GC_DinhMucThucTeDangKy" +
                    " (" +
                    " ID," +
                    " SoTiepNhan," +
                    " NgayTiepNhan," +
                    " TrangThaiXuLy," +
                    " HopDong_ID," +
                    " LenhSanXuat_ID" +
                    " );" +
                " END; " +
        " END;";
                    break;
                case 7:
                    Sql = "BEGIN" +
    " IF EXISTS ( SELECT  *" +
                " FROM    sys.indexes" +
                " WHERE   name = 'IDX_t_KDT_GC_PhuKienDangKy'" +
                        " AND object_id = OBJECT_ID('t_KDT_GC_PhuKienDangKy') )" +
        " BEGIN" +
            " DROP INDEX IDX_t_KDT_GC_PhuKienDangKy ON dbo.t_KDT_GC_PhuKienDangKy;" +
        " END;" +
    " ELSE" +
        " BEGIN" +
            " CREATE NONCLUSTERED INDEX IDX_t_KDT_GC_PhuKienDangKy" +
            " ON dbo.t_KDT_GC_PhuKienDangKy" +
            " (" +
            " ID," +
            " MaDoanhNghiep," +
            " SoTiepNhan," +
            " SoPhuKien," +
            " HopDong_ID," +
            " NgayTiepNhan," +
            " TrangThaiXuLy" +
            " );" +
        " END;" +
" END;";
                    break;
                case 8:
                    Sql = "BEGIN " +
    " IF EXISTS ( SELECT  * " +
                " FROM    sys.indexes " +
                " WHERE   name = 'IDX_t_KDT_ToKhaiMauDich' " +
                        " AND object_id = OBJECT_ID('t_KDT_ToKhaiMauDich') ) " +
        " BEGIN " +
            " DROP INDEX IDX_t_KDT_ToKhaiMauDich ON dbo.t_KDT_ToKhaiMauDich; " +
        " END; " +
    " ELSE " +
        " BEGIN " +
            " CREATE NONCLUSTERED INDEX IDX_t_KDT_ToKhaiMauDich " +
            " ON dbo.t_KDT_ToKhaiMauDich " +
            " ( " +
            " ID, " +
            " MaHaiQuan, " +
            " MaDoanhNghiep, " +
            " TrangThaiXuLy, " +
            " NgayTiepNhan, " +
            " SoTiepNhan, " +
            " SoToKhai, " +
            " IDHopDong, " +
            " ActionStatus, " +
            " PhanLuong, " +
            " LoaiVanDon " +
            " ); " +
        " END; " +
" END;";
                    break;
                case 9:
                    Sql = "BEGIN " +
    " IF EXISTS ( SELECT  * " +
                " FROM    sys.indexes " +
                " WHERE   name = 'IDX_t_KDT_GC_ToKhaiChuyenTiep' " +
                        " AND object_id = OBJECT_ID('t_KDT_GC_ToKhaiChuyenTiep') ) " +
        " BEGIN " +
            " DROP INDEX IDX_t_KDT_GC_ToKhaiChuyenTiep ON dbo.t_KDT_GC_ToKhaiChuyenTiep; " +
        " END; " +
    " ELSE " +
        " BEGIN " +
            " CREATE NONCLUSTERED INDEX IDX_t_KDT_GC_ToKhaiChuyenTiep " +
            " ON dbo.t_KDT_GC_ToKhaiChuyenTiep  " +
            " ( " +
            " ID, " +
            " MaHaiQuanTiepNhan, " +
            " MaDoanhNghiep, " +
            " ActionStatus, " +
            " TrangThaiXuLy, " +
            " PhanLuong, " +
            " NgayTiepNhan, " +
            " SoTiepNhan, " +
            " Huongdan_PL " +
            " ); " +
        " END; " +
" END;";
                    break;
                case 10:
                    Sql = "BEGIN " +
    " IF EXISTS ( SELECT  * " +
                " FROM    sys.indexes " +
                " WHERE   name = 'IDX_T_GC_DinhMuc' " +
                        " AND object_id = OBJECT_ID('t_GC_DinhMuc') ) " +
        " BEGIN " +
            " DROP INDEX IDX_T_GC_DinhMuc ON dbo.t_GC_DinhMuc; " +
        " END; " +
    " ELSE " +
        " BEGIN " +
            " CREATE NONCLUSTERED INDEX IDX_T_GC_DinhMuc " +
            " ON dbo.t_GC_DinhMuc " +
            " ( " +
            " MaSanPham, " +
            " HopDong_ID, " +
            " LenhSanXuat_ID " +
            " ); " +
        " END; " +
" END;";
                    break;
#elif SXXK_V4
                case 3:
                    Sql = "  BEGIN " +
    " IF EXISTS ( SELECT  * " +
                " FROM    sys.indexes " +
                " WHERE   name = 'IDX_t_KDT_SXXK_NguyenPhuLieuDangKy' " +
                        " AND object_id = OBJECT_ID('t_KDT_SXXK_NguyenPhuLieuDangKy') ) " +
        " BEGIN " +
            " DROP INDEX IDX_t_KDT_SXXK_NguyenPhuLieuDangKy ON dbo.t_KDT_SXXK_NguyenPhuLieuDangKy; " +
        " END; " +
    " ELSE " +
        " BEGIN " +
            " CREATE INDEX IDX_t_KDT_SXXK_NguyenPhuLieuDangKy " +
            " ON dbo.t_KDT_SXXK_NguyenPhuLieuDangKy " +
            " ( " +
            " ID, " +
            " MaHaiQuan, " +
            " MaDoanhNghiep, " +
            " NgayTiepNhan, " +
            " SoTiepNhan, " +
            " TrangThaiXuLy " +
            " ); " +
        " END;  " +
  " END;";
                    break;
                case 4:
                    Sql = "  BEGIN " +
    " IF EXISTS ( SELECT  * " +
                " FROM    sys.indexes " +
                " WHERE   name = 'IDX_t_KDT_SXXK_SanPhamDangKy' " +
                        " AND object_id = OBJECT_ID('t_KDT_SXXK_SanPhamDangKy') ) " +
        " BEGIN " +
            " DROP INDEX IDX_t_KDT_SXXK_SanPhamDangKy ON dbo.t_KDT_SXXK_SanPhamDangKy; " +
        " END; " +
    " ELSE " +
        " BEGIN " +
            " CREATE INDEX IDX_t_KDT_SXXK_SanPhamDangKy " +
            " ON dbo.t_KDT_SXXK_SanPhamDangKy " +
            " ( " +
            " ID, " +
            " MaHaiQuan, " +
            " MaDoanhNghiep, " +
            " NgayTiepNhan, " +
            " SoTiepNhan, " +
            " TrangThaiXuLy " +
            " ); " +
        " END;  " +
  " END;";
                    break;
                case 5:
                    Sql = "    BEGIN " +
    " IF EXISTS ( SELECT  * " +
                " FROM    sys.indexes " +
                " WHERE   name = 'IDX_t_KDT_SXXK_DinhMucDangKy' " +
                        " AND object_id = OBJECT_ID('t_KDT_SXXK_DinhMucDangKy') ) " +
        " BEGIN " +
            " DROP INDEX IDX_t_KDT_SXXK_DinhMucDangKy ON dbo.t_KDT_SXXK_DinhMucDangKy; " +
        " END; " +
    " ELSE " +
        " BEGIN " +
            " CREATE INDEX IDX_t_KDT_SXXK_DinhMucDangKy " +
            " ON dbo.t_KDT_SXXK_DinhMucDangKy " +
            " ( " +
            " ID, " +
            " MaHaiQuan, " +
            " MaDoanhNghiep, " +
            " NgayTiepNhan, " +
            " SoTiepNhan, " +
            " TrangThaiXuLy " +
            " ); " +
        " END;  " +
  " END;";
                    break;
                case 6:
                    Sql = "    BEGIN " +
    " IF EXISTS ( SELECT  * " +
                " FROM    sys.indexes " +
                " WHERE   name = 'IDX_t_KDT_SXXK_DinhMucDangKy' " +
                        " AND object_id = OBJECT_ID('t_KDT_SXXK_DinhMucDangKy') ) " +
        " BEGIN " +
            " DROP INDEX IDX_t_KDT_SXXK_DinhMucDangKy ON dbo.t_KDT_SXXK_DinhMucDangKy; " +
        " END; " +
    " ELSE " +
        " BEGIN " +
            " CREATE INDEX IDX_t_KDT_SXXK_DinhMucDangKy " +
            " ON dbo.t_KDT_SXXK_DinhMucDangKy " +
            " ( " +
            " ID, " +
            " MaHaiQuan, " +
            " MaDoanhNghiep, " +
            " NgayTiepNhan, " +
            " SoTiepNhan, " +
            " TrangThaiXuLy " +			
            " ); " +
        " END; " +
  " END;";
                    break;
                case 7:
                    Sql = "      BEGIN " +
    " IF EXISTS ( SELECT  * " +
                " FROM    sys.indexes " +
                " WHERE   name = 'IDX_t_SXXK_NguyenPhuLieu' " +
                        " AND object_id = OBJECT_ID('t_SXXK_NguyenPhuLieu') ) " +
        " BEGIN " +
            " DROP INDEX IDX_t_SXXK_NguyenPhuLieu ON dbo.t_SXXK_NguyenPhuLieu; " +
        " END; " +
    " ELSE " +
        " BEGIN " +
            " CREATE INDEX IDX_t_SXXK_NguyenPhuLieu " +
            " ON dbo.t_SXXK_NguyenPhuLieu " +
            " ( " +
            " MaHaiQuan, " +
            " MaDoanhNghiep, " +
            " Ma, " +
            " Ten, " +
            " MaHS, " +
			" DVT_ID " +			
            " ); " +
        " END; " +
  " END;";
                    break;
                case 8:
                    Sql = "      BEGIN " +
    " IF EXISTS ( SELECT  * " +
                " FROM    sys.indexes " +
                " WHERE   name = 'IDX_t_SXXK_SanPham' " +
                        " AND object_id = OBJECT_ID('t_SXXK_SanPham') ) " +
        " BEGIN " +
            " DROP INDEX IDX_t_SXXK_SanPham ON dbo.t_SXXK_SanPham; " +
        " END; " +
    " ELSE " +
        " BEGIN " +
            " CREATE INDEX IDX_t_SXXK_SanPham " +
            " ON dbo.t_SXXK_SanPham " +
            " ( " +
            " MaHaiQuan, " +
            " MaDoanhNghiep, " +
            " Ma, " +
            " Ten, " +
            " MaHS, " +
			" DVT_ID " +		
            " ); " +
        " END;  " +
  " END;";
                    break;
                case 9:
                    Sql = "BEGIN " +
    " IF EXISTS ( SELECT  * " +
                " FROM    sys.indexes " +
                " WHERE   name = 'IDX_t_SXXK_DinhMuc' " +
                        " AND object_id = OBJECT_ID('t_SXXK_DinhMuc') ) " +
        " BEGIN " +
            " DROP INDEX IDX_t_SXXK_DinhMuc ON dbo.t_SXXK_DinhMuc; " +
        " END; " +
    " ELSE " +
        " BEGIN " +
            " CREATE INDEX IDX_t_SXXK_DinhMuc " +
            " ON dbo.t_SXXK_DinhMuc " +
            " ( " +
            " MaHaiQuan, " +
            " MaDoanhNghiep, " +
            " MaNguyenPhuLieu, " +
            " MaSanPham " +		
            " ); " +
        " END; " +
" END;";
                    break;
#endif
                default:
                    break;
            }
            if (!String.IsNullOrEmpty(Sql))
            {
                DbCommand command = db.GetSqlStringCommand(Sql);
                db.ExecuteNonQuery(command);
            }
        }
        private void BindDatabase()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = dtDatabase;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetServerInstances()
        {
            try
            {
                SqlDataSourceEnumerator instance = SqlDataSourceEnumerator.Instance;
                System.Data.DataTable table = instance.GetDataSources();
                foreach (DataRow item in table.Rows)
                {
                    string serverName = item[0].ToString();
                    serverName += "\\";
                    serverName += item[1].ToString();
                    if (serverName == GlobalSettings.SERVER_NAME)
                    {
                        string version = item[2].ToString();
                        lblSQLVersion.Text = version;
                        switch (version)
                        {
                            case "9.0.1399.06":
                                lblSQLName.Text = "Microsoft SQL Server 2005 RTM";
                                break;
                            case "9.0.2047":
                                lblSQLName.Text = "Microsoft SQL Server 2005 SP1";
                                break;
                            case "9.0.3042":
                                lblSQLName.Text = "Microsoft SQL Server 2005 SP2";
                                break;
                            case "9.0.4035":
                                lblSQLName.Text = "Microsoft SQL Server 2005 SP3";
                                break;
                            case "9.0.5000":
                                lblSQLName.Text = "Microsoft SQL Server 2005 SP4";
                                break;
                            case "10.0.1600.22":
                                lblSQLName.Text = "Microsoft SQL Server 2008 RTM";
                                break;
                            case "10.0.2531.0":
                                lblSQLName.Text = "Microsoft SQL Server 2008 SP1";
                                break;
                            case "10.1.2531.0":
                                lblSQLName.Text = "Microsoft SQL Server 2008 SP1";
                                break;
                            case "10.0.4000.0":
                                lblSQLName.Text = "Microsoft SQL Server 2008 SP2";
                                break;
                            case "10.2.4000.0":
                                lblSQLName.Text = "Microsoft SQL Server 2008 SP2";
                                break;
                            case "10.0.5500.0":
                                lblSQLName.Text = "Microsoft SQL Server 2008 SP3";
                                break;
                            case "10.3.5500.0":
                                lblSQLName.Text = "Microsoft SQL Server 2008 SP3";
                                break;
                            case "10.0.6000.29":
                                lblSQLName.Text = "Microsoft SQL Server 2008 SP4";
                                break;
                            case "10.4.6000.29":
                                lblSQLName.Text = "Microsoft SQL Server 2008 SP4";
                                break;
                            case "10.50.1600.1":
                                lblSQLName.Text = "Microsoft SQL Server 2008 R2 RTM";
                                break;
                            case "10.50.2500.0":
                                lblSQLName.Text = "Microsoft SQL Server 2008 R2 SP1";
                                break;
                            case "10.51.2500.0":
                                lblSQLName.Text = "Microsoft SQL Server 2008 R2 SP1";
                                break;
                            case "10.50.4000.0":
                                lblSQLName.Text = "Microsoft SQL Server 2008 R2 SP2";
                                break;
                            case "10.52.4000.0":
                                lblSQLName.Text = "Microsoft SQL Server 2008 R2 SP2";
                                break;
                            case "10.50.6000.34":
                                lblSQLName.Text = "Microsoft SQL Server 2008 R2 SP3";
                                break;
                            case "10.53.6000.34":
                                lblSQLName.Text = "Microsoft SQL Server 2008 R2 SP3";
                                break;
                            case "11.0.2100.60":
                                lblSQLName.Text = "Microsoft SQL Server 2012 RTM";
                                break;
                            case "11.0.3000.0":
                                lblSQLName.Text = "Microsoft SQL Server 2012 SP1";
                                break;
                            case "11.1.3000.0":
                                lblSQLName.Text = "Microsoft SQL Server 2012 SP1";
                                break;
                            case "11.0.5058.0":
                                lblSQLName.Text = "Microsoft SQL Server 2012 SP2";
                                break;
                            case "11.2.5058.0":
                                lblSQLName.Text = "Microsoft SQL Server 2012 SP2";
                                break;
                            case "11.0.6020.0":
                                lblSQLName.Text = "Microsoft SQL Server 2012 SP3";
                                break;
                            case "11.3.6020.0":
                                lblSQLName.Text = "Microsoft SQL Server 2012 SP3";
                                break;
                            case "11.0.7001.0":
                                lblSQLName.Text = "Microsoft SQL Server 2012 SP4";
                                break;
                            case "11.4.7001.0":
                                lblSQLName.Text = "Microsoft SQL Server 2012 SP4";
                                break;
                            case "12.0.2000.8":
                                lblSQLName.Text = "Microsoft SQL Server 2014 RTM";
                                break;
                            case "12.0.4100.1":
                                lblSQLName.Text = "Microsoft SQL Server 2014 SP1";
                                break;
                            case "12.1.4100.1":
                                lblSQLName.Text = "Microsoft SQL Server 2014 SP1";
                                break;
                            case "12.0.5000.0":
                                lblSQLName.Text = "Microsoft SQL Server 2014 SP2";
                                break;
                            case "12.2.5000.0":
                                lblSQLName.Text = "Microsoft SQL Server 2014 SP2";
                                break;
                            case "12.0.6024.0":
                                lblSQLName.Text = "Microsoft SQL Server 2014 SP3";
                                break;
                            case "12.3.6024.0":
                                lblSQLName.Text = "Microsoft SQL Server 2014 SP3";
                                break;
                            case "13.0.1601.5":
                                lblSQLName.Text = "Microsoft SQL Server 2016 RTM";
                                break;
                            case "13.0.4001.0":
                                lblSQLName.Text = "Microsoft SQL Server 2016 SP1";
                                break;
                            case "13.1.4001.0":
                                lblSQLName.Text = "Microsoft SQL Server 2016 SP1";
                                break;
                            case "13.0.5026.0":
                                lblSQLName.Text = "Microsoft SQL Server 2016 SP2";
                                break;
                            case "13.2.5026.0":
                                lblSQLName.Text = "Microsoft SQL Server 2016 SP2";
                                break;
                            case "14.0.1000.169":
                                lblSQLName.Text = "Microsoft SQL Server 2017 RTM";
                                break;
                            case "14.0.3356.20":
                                lblSQLName.Text = "Microsoft SQL Server 2017 September 2020";
                                break;
                            case "15.0.2000.5":
                                lblSQLName.Text = "Microsoft SQL Server 2019 RTM";
                                break;
                            case "15.0.4073.23":
                                lblSQLName.Text = "Microsoft SQL Server 2019 October 2020";
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void SetInformation()
        {
            try
            {
                //GetServerInstances();
                if (dtServer.Rows.Count > 0)
                {
                    for (int i = 0; i < dtServer.Rows.Count; i++)
                    {
                        lblSQLName.Text = dtServer.Rows[i]["sversion_name"].ToString() + dtServer.Rows[i]["edition"].ToString();
                        lblSQLVersion.Text = dtServer.Rows[i]["product_version"].ToString();
                        lblSQLCapacity.Text = dtServer.Rows[i]["max_db_size_in_gb"].ToString() == "-1" ? "Không giới hạn dung lượng" : dtServer.Rows[i]["max_db_size_in_gb"].ToString() + " GB";
                    }
                }
                lblServerName.Text = GlobalSettings.SERVER_NAME.ToString();

                lblDatabaseName.Text = GlobalSettings.DATABASE_NAME;
                lblUserName.Text = GlobalSettings.USER;

                if (dtDatabase.Rows.Count > 0)
                {
                    int DatabaseSize = 0;
                    for (int i = 0; i < dtDatabase.Rows.Count; i++)
                    {
                        DatabaseSize += Convert.ToInt32(dtDatabase.Rows[i]["SIZEMB"].ToString());
                        lblDateCreated.Text = dtDatabase.Rows[i]["CREATE_DATE"].ToString();
                        lblDatabaseSize.Text = DatabaseSize.ToString() + " MB";
                        lblRecoveryMode.Text = dtDatabase.Rows[i]["RECOVERY_MODEL_DESC"].ToString();
                        lblStatus.Text = dtDatabase.Rows[i]["STATE_DESC"].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (ShowMessageTQDT("ĐỂ ĐẢM BẢO TÍNH TOÀN VẸN DỮ LIỆU . DOANH NGHIỆP NÊN SAO LƯU CƠ SỞ DỮ LIỆU TRƯỚC KHI THỰC HIỆN TỐI ƯU HOÁ CSDL . DOANH NGHIỆP CÓ MUỐN SAO LƯU CSDL KHÔNG ? NHẤN YES ĐỂ SAO LƯU NHẤN NO ĐỂ BỎ QUA NẾU ĐÃ SAO LƯU", true) == "Yes")
            {
                BackUpAndReStoreForm f = new BackUpAndReStoreForm();
                f.isBackUp = true;
                f.ShowDialog(this);
            }
            ThreadPool.QueueUserWorkItem(DoWork);
        }
        private void SetProcessBar(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { uiProgressBar1.Value = value; }));
            }
            else
            {
                uiProgressBar1.Value = value;
            }
        }
        private bool IsCompress(string CompressData)
        {
            try
            {
                String DecompressedData = "";
                DecompressedData = Compression.DeCompress(CompressData);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void DoWork(object obj)
        {
            try
            {
                SetProcessBar(0);
                int k = 0;
                if (chkCompressFile.Checked)
                {
                    if (InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            btnProcess.Enabled = false;

                        }));
                    }
                    else
                    {
                        btnProcess.Enabled = true;
                    }
                    List<KDT_VNACCS_CertificateOfOrigin> COCollection = new List<KDT_VNACCS_CertificateOfOrigin>();
                    COCollection = KDT_VNACCS_CertificateOfOrigin.SelectCollectionAll();
                    SetProcessBar(0);
                    foreach (KDT_VNACCS_CertificateOfOrigin item in COCollection)
                    {
                        if (!IsCompress(item.Content))
                        {
                            item.Content = Compression.Compress(item.Content);
                            item.Update();
                            SetProcessBar((k * 100 / COCollection.Count));
                            k++;
                        }
                    }
                    //
                    SetProcessBar(0);
                    k = 0;
                    List<KDT_VNACCS_BillOfLading> BLCollection = new List<KDT_VNACCS_BillOfLading>();
                    BLCollection = KDT_VNACCS_BillOfLading.SelectCollectionAll();

                    foreach (KDT_VNACCS_BillOfLading item in BLCollection)
                    {
                        if (!IsCompress(item.Content))
                        {
                            item.Content = Compression.Compress(item.Content);
                            item.Update();
                            SetProcessBar((k * 100 / BLCollection.Count));
                            k++;
                        }
                    }
                    //
                    SetProcessBar(0);
                    k = 0;
                    List<KDT_VNACCS_CommercialInvoice> CICollection = new List<KDT_VNACCS_CommercialInvoice>();
                    CICollection = KDT_VNACCS_CommercialInvoice.SelectCollectionAll();
                    foreach (KDT_VNACCS_CommercialInvoice item in CICollection)
                    {
                        if (!IsCompress(item.Content))
                        {
                            item.Content = Compression.Compress(item.Content);
                            item.Update();
                            SetProcessBar((k * 100 / CICollection.Count));
                            k++;
                        }
                    }
                    //
                    SetProcessBar(0);
                    k = 0;
                    List<KDT_VNACCS_Container_Detail> CTCollection = new List<KDT_VNACCS_Container_Detail>();
                    CTCollection = KDT_VNACCS_Container_Detail.SelectCollectionAll();
                    foreach (KDT_VNACCS_Container_Detail item in CTCollection)
                    {
                        if (!IsCompress(item.Content))
                        {
                            item.Content = Compression.Compress(item.Content);
                            item.Update();
                            SetProcessBar((k * 100 / CTCollection.Count));
                            k++;
                        }
                    }
                    //
                    SetProcessBar(0);
                    k = 0;
                    List<KDT_VNACCS_ContractDocument> CTDCollection = new List<KDT_VNACCS_ContractDocument>();
                    CTDCollection = KDT_VNACCS_ContractDocument.SelectCollectionAll();
                    foreach (KDT_VNACCS_ContractDocument item in CTDCollection)
                    {
                        if (!IsCompress(item.Content))
                        {
                            item.Content = Compression.Compress(item.Content);
                            item.Update();
                            SetProcessBar((k * 100 / CTDCollection.Count));
                            k++;
                        }
                    }
                    //
                    SetProcessBar(0);
                    k = 0;
                    List<KDT_VNACCS_License> LCCollection = new List<KDT_VNACCS_License>();
                    LCCollection = KDT_VNACCS_License.SelectCollectionAll();
                    foreach (KDT_VNACCS_License item in LCCollection)
                    {
                        if (!IsCompress(item.Content))
                        {
                            item.Content = Compression.Compress(item.Content);
                            item.Update();
                            SetProcessBar((k * 100 / LCCollection.Count));
                            k++;
                        }
                    }
                    //
                    SetProcessBar(0);
                    k = 0;
                    List<KDT_VNACCS_AdditionalDocument> ATDCollection = new List<KDT_VNACCS_AdditionalDocument>();
                    ATDCollection = KDT_VNACCS_AdditionalDocument.SelectCollectionAll();

                    foreach (KDT_VNACCS_AdditionalDocument item in ATDCollection)
                    {
                        if (!IsCompress(item.Content))
                        {
                            item.Content = Compression.Compress(item.Content);
                            item.Update();
                            SetProcessBar((k * 100 / ATDCollection.Count));
                            k++;
                        }
                    }
                }
                SetProcessBar(0);
                if (chkDeleteMsgLog.Checked)
                {
                    IList<Company.KDT.SHARE.VNACCS.LogMessages.MsgLog> MsgLogCollection = new List<Company.KDT.SHARE.VNACCS.LogMessages.MsgLog>();
                    MsgLogCollection = Company.KDT.SHARE.VNACCS.LogMessages.MsgLog.SelectMinimumCollectionDynamic(" MaNghiepVu IN ('IDA','IDA Return','IDA01','IDA01 Retu','EDA','EDA Return','EDA01','EDA01 Retu')", "");
                    foreach (Company.KDT.SHARE.VNACCS.LogMessages.MsgLog item in MsgLogCollection)
                    {
                        item.Delete();
                        SetProcessBar((k * 100 / MsgLogCollection.Count));
                        k++;
                    }
                }
                SetProcessBar(0);
                if (chkIndexTable.Checked)
                {
                    if (InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            btnProcess.Enabled = false;

                        }));
                    }
                    else
                    {
                        btnProcess.Enabled = true;
                    }
                    for (int i = 1; i <= 10; i++)
                    {
                        CreateIndex(i);
                        SetProcessBar((i * 100 / 10));
                    }
                }
                SetProcessBar(0);
                if (chkPerformance.Checked)
                {
                    if (InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            btnProcess.Enabled = false;

                        }));
                    }
                    else
                    {
                        btnProcess.Enabled = true;
                    }
                    DataTable dtDeleteLog = new DataTable();
                    dtDeleteLog = DeleteDatabaseLog();
                }
                ShowMessage("Tối ưu hoá cơ sở dữ liệu thành công !", false);
                btnProcess.Enabled = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                btnProcess.Enabled = true;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
