-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_DBDL_KetQuaXuLy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_DBDL_KetQuaXuLy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_DBDL_KetQuaXuLy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_DBDL_KetQuaXuLy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_DBDL_KetQuaXuLy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_DBDL_KetQuaXuLy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_DBDL_KetQuaXuLy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_DBDL_KetQuaXuLy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Insert]
	@ID bigint,
	@SoToKhai bigint,
	@MaDoanhNghiep nvarchar(15),
	@NgayDongBo datetime,
	@TrangThai int,
	@GhiChu nvarchar(max),
	@GUIDSTR nvarchar(36)
AS
INSERT INTO [dbo].[t_KDT_DBDL_KetQuaXuLy]
(
	[ID],
	[SoToKhai],
	[MaDoanhNghiep],
	[NgayDongBo],
	[TrangThai],
	[GhiChu],
	[GUIDSTR]
)
VALUES
(
	@ID,
	@SoToKhai,
	@MaDoanhNghiep,
	@NgayDongBo,
	@TrangThai,
	@GhiChu,
	@GUIDSTR
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Update]
	@ID bigint,
	@SoToKhai bigint,
	@MaDoanhNghiep nvarchar(15),
	@NgayDongBo datetime,
	@TrangThai int,
	@GhiChu nvarchar(max),
	@GUIDSTR nvarchar(36)
AS

UPDATE
	[dbo].[t_KDT_DBDL_KetQuaXuLy]
SET
	[SoToKhai] = @SoToKhai,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[NgayDongBo] = @NgayDongBo,
	[TrangThai] = @TrangThai,
	[GhiChu] = @GhiChu,
	[GUIDSTR] = @GUIDSTR
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_InsertUpdate]
	@ID bigint,
	@SoToKhai bigint,
	@MaDoanhNghiep nvarchar(15),
	@NgayDongBo datetime,
	@TrangThai int,
	@GhiChu nvarchar(max),
	@GUIDSTR nvarchar(36)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_DBDL_KetQuaXuLy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_DBDL_KetQuaXuLy] 
		SET
			[SoToKhai] = @SoToKhai,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[NgayDongBo] = @NgayDongBo,
			[TrangThai] = @TrangThai,
			[GhiChu] = @GhiChu,
			[GUIDSTR] = @GUIDSTR
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_DBDL_KetQuaXuLy]
	(
			[ID],
			[SoToKhai],
			[MaDoanhNghiep],
			[NgayDongBo],
			[TrangThai],
			[GhiChu],
			[GUIDSTR]
	)
	VALUES
	(
			@ID,
			@SoToKhai,
			@MaDoanhNghiep,
			@NgayDongBo,
			@TrangThai,
			@GhiChu,
			@GUIDSTR
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_DBDL_KetQuaXuLy]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_DBDL_KetQuaXuLy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[MaDoanhNghiep],
	[NgayDongBo],
	[TrangThai],
	[GhiChu],
	[GUIDSTR]
FROM
	[dbo].[t_KDT_DBDL_KetQuaXuLy]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoToKhai],
	[MaDoanhNghiep],
	[NgayDongBo],
	[TrangThai],
	[GhiChu],
	[GUIDSTR]
FROM [dbo].[t_KDT_DBDL_KetQuaXuLy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[MaDoanhNghiep],
	[NgayDongBo],
	[TrangThai],
	[GhiChu],
	[GUIDSTR]
FROM
	[dbo].[t_KDT_DBDL_KetQuaXuLy]	

GO

