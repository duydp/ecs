﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.GC;
using System.IO;
using System.Diagnostics;
using Janus.Windows.GridEX;

namespace Company.Interface.GC
{
    public partial class NPLSuDungTrongTKXuatForm : BaseForm
    {
        public long IDHopDong = 0;
        public string MaNPL = "";
        public NPLSuDungTrongTKXuatForm()
        {
            InitializeComponent();
        }

        private void NPLXuatForm_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["LuongNPL"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["SoLuong"].TotalFormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

            //DataSet dsNPLQD = NguyenPhuLieu.GetNPLQuyDoiTheoToKhaiXuat(IDHopDong, MaNPL);
            DataSet dsNPLQD = NguyenPhuLieu.GetNPLQuyDoiTheoToKhaiXuatByLSX(IDHopDong, MaNPL);
            if (dsNPLQD.Tables[0].Rows.Count == 0)
            {
                dgList.DataSource = NguyenPhuLieu.GetNPLQuyDoiTheoToKhaiXuatTemp(IDHopDong, MaNPL).Tables[0]; 
            }
            else
            {
                dgList.DataSource = dsNPLQD.Tables[0]; 
            }
            btnExcel.Text = setText("Xuất Excel","Export to Excel");
            
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.FileName = "Danh sách Nguyên phụ liệu của Tờ khai xuất có Mã _" + MaNPL + "_" + DateTime.Now.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
            sf.Filter = "Excel File | *.xls ";

            if (sf.ShowDialog(this) == DialogResult.OK)
            {
                Janus.Windows.GridEX.Export.GridEXExporter grdExport = new Janus.Windows.GridEX.Export.GridEXExporter();
                grdExport.GridEX = dgList;

                try
                {
                    System.IO.Stream str = sf.OpenFile();
                    grdExport.Export(str);
                    str.Close();
                    if (ShowMessage("Bạn có muốn mở File này không", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sf.FileName);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi trong quá trình mở File" + ex.Message, false);
                }
            }
            else
            {
                ShowMessage("Xuất Excel không thành công", false);
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    KDT_LenhSanXuat lenhhXS = new KDT_LenhSanXuat();
                    lenhhXS = KDT_LenhSanXuat.Load(Convert.ToInt64(e.Row.Cells["LenhSanXuat_ID"].Value));
                    if (lenhhXS != null)
                        e.Row.Cells["LenhSanXuat_ID"].Text = lenhhXS.SoLenhSanXuat;

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
       
    }
}