﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components.Messages.Send;
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
#endif
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.Maper;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.Interface.VNACCS;
using Infragistics.Excel;

namespace Company.Interface
{
    public partial class VNACC_ChungTuDinhKemForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ChungTuDinhKem ChungTuKem = new KDT_VNACC_ChungTuDinhKem();
        public List<KDT_VNACC_ChungTuDinhKem_ChiTiet> ListCTDK = new List<KDT_VNACC_ChungTuDinhKem_ChiTiet>();
        public bool isAddNew = true;
        private string msgInfor = string.Empty;
        string filebase64 = "";
        long filesize = 0;
        public ELoaiThongTin LoaiChungTu = ELoaiThongTin.MSB;

        public VNACC_ChungTuDinhKemForm(ELoaiThongTin loaiChungTu)
        {
            InitializeComponent();
            ChungTuKem.TrangThaiXuLy = "0";
            LoaiChungTu = loaiChungTu;

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EOthers.MSB.ToString());
        }

        /// <summary>
        /// Tinh tong dung luong
        /// </summary>
        /// <param name="FileInBytes"></param>
        /// <returns></returns>
        private string CalculateFileSize(decimal FileInBytes)
        {
            string strSize = "00";
            if (FileInBytes < 1024)
                strSize = FileInBytes + " B";//Byte
            else if (FileInBytes > 1024 & FileInBytes < 1048576)
                strSize = Math.Round((FileInBytes / 1024), 2) + " KB";//Kilobyte
            else if (FileInBytes > 1048576 & FileInBytes < 107341824)
                strSize = Math.Round((FileInBytes / 1024) / 1024, 2) + " MB";//Megabyte
            else if (FileInBytes > 107341824 & FileInBytes < 1099511627776)
                strSize = Math.Round(((FileInBytes / 1024) / 1024) / 1024, 2) + " GB";//Gigabyte
            else
                strSize = Math.Round((((FileInBytes / 1024) / 1024) / 1024) / 1024, 2) + " TB";//Terabyte
            return strSize;
        }

        /// <summary>
        /// Lay tong dung luong cac file dinh kem
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private void HienThiTongDungLuong(List<KDT_VNACC_ChungTuDinhKem_ChiTiet> list)
        {
            long size = 0;

            for (int i = 0; i < list.Count; i++)
            {
                size += Convert.ToInt64(list[i].FileSize);
            }

            //hien thi tong dung luong file            
            lblTongDungLuong.Text = CalculateFileSize(size);
            lblLuuY.Text = CalculateFileSize(GlobalSettings.FileSize);
        }

        private void BindData()
        {
            dgList.DataSource = ListCTDK;
            try { dgList.Refetch(); }
            catch { }

            //Cap nhat thong tin tong dung luong file.
            HienThiTongDungLuong(ListCTDK);
        }

        private void VNACC_ChungTuDinhKemForm_Load(object sender, EventArgs e)
        {
            try
            {
                setCommandStatus();
                Cursor = Cursors.WaitCursor;
                ucCoQuanHaiQuan.Leave += new EventHandler(ctrCoQuanHaiQuan_Leave);
                SetIDControl();
                // Hai quan
                string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                ucCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS; //Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(MaHaiQuan, "MaHQ"); //MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
                ucCoQuanHaiQuan_HYS.Code = GlobalSettings.MA_HAI_QUAN_VNACCS; //Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(MaHaiQuan, "MaHQ"); //MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
                uiTabPageMSB.Text = uiTabPageHYS.Text = "Thông tin chung";
                if (LoaiChungTu == ELoaiThongTin.MSB)
                {
                    uiTabPageMSB.TabVisible = true;
                    uiTabPageHYS.TabVisible = false;
                }
                else if (LoaiChungTu == ELoaiThongTin.HYS)
                {
                    uiTabPageMSB.TabVisible = false;
                    uiTabPageHYS.TabVisible = true;
                }

                if (ChungTuKem.ID > 0)
                {
                    SetChungTuKem();

                    ListCTDK = KDT_VNACC_ChungTuDinhKem_ChiTiet.SelectCollectionBy_ChungTuKemID(ChungTuKem.ID);

                    BindData();
                }
                else
                {
                    //Cap nhat thong tin tong dung luong file.
                    HienThiTongDungLuong(ListCTDK);
                }

                if (ChungTuKem.SoTiepNhan > 0 && ChungTuKem.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET.ToString())
                {
                    txtSoTiepNhan.Text = ChungTuKem.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Text = ChungTuKem.NgayTiepNhan.ToShortDateString();
                }
                else if (ChungTuKem.SoTiepNhan > 0 && ChungTuKem.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET.ToString())
                {
                    txtSoTiepNhan.Text = ChungTuKem.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Text = ChungTuKem.NgayTiepNhan.ToShortDateString();
                }

                txtTieuDe.Focus();

                // Thiết lập trạng thái các nút trên form.
                // HUNGTQ, Update 07/06/2010.
                //SetButtonStateCHUNGTUKEM(ChungTuKem, ChungTuKem);

                SetMaxLengthControl();
                ctrCoQuanHaiQuan_Leave(null, null);
                ValidateForm(true);

                SetAutoRemoveUnicodeAndUpperCaseControl();
                ctrNhomXuLyHS.ShowColumnName = true;
                ctrNhomXuLyHS.ShowColumnCode = true;
                ucNhomXuLyHS_HYS.ShowColumnName = true;
                ucNhomXuLyHS_HYS.ShowColumnCode = true;
                TuDongCapNhatThongTin();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetChungTuKem()
        {
            if (LoaiChungTu == ELoaiThongTin.MSB)
            {
                GetChungTuKem_MSB();
            }
            else if (LoaiChungTu == ELoaiThongTin.HYS)
            {
                GetChungTuKem_HYS();
            }
        }

        private void SetChungTuKem()
        {
            if (LoaiChungTu == ELoaiThongTin.MSB)
            {
                SetChungTuKem_MSB();
            }
            else if (LoaiChungTu == ELoaiThongTin.HYS)
            {
                SetChungTuKem_HYS();
            }
        }

        private void GetChungTuKem_MSB()
        {
            ChungTuKem.LoaiChungTu = LoaiChungTu.ToString();
            //Cap nhat lai danh sach file dinh kem chi tiet
            ChungTuKem.ChungTuDinhKemChiTietCollection = ListCTDK;

            ChungTuKem.TieuDe = txtTieuDe.Text.Trim();
            ChungTuKem.GhiChu = txtGhiChu.Text;
            ChungTuKem.CoQuanHaiQuan = ucCoQuanHaiQuan.Code;
            ChungTuKem.NhomXuLyHoSo = ctrNhomXuLyHS.Code == "" ? "00" : ctrNhomXuLyHS.Code;
            //ChungTuKem.NhomXuLyHoSoID = ctrNhomXuLyHS.Code==""? 00:Convert.ToInt32(ctrNhomXuLyHS.Code);
            ChungTuKem.SoToKhai = Convert.ToInt64(txtSoToKhai.Value);
            ChungTuKem.PhanLoaiThuTucKhaiBao = "A01";
        }

        private void SetChungTuKem_MSB()
        {
            txtGhiChu.Text = ChungTuKem.GhiChu;
            txtTieuDe.Text = ChungTuKem.TieuDe;
            ucCoQuanHaiQuan.Code = ChungTuKem.CoQuanHaiQuan;
            ctrNhomXuLyHS.Code = ChungTuKem.NhomXuLyHoSo.ToString();
            txtSoToKhai.Value = ChungTuKem.SoToKhai;

            ChungTuKem.LoadListChungTuDinhKem_ChiTiet();
            ListCTDK = ChungTuKem.ChungTuDinhKemChiTietCollection;
        }

        private void GetChungTuKem_HYS()
        {
            ChungTuKem.LoaiChungTu = LoaiChungTu.ToString();
            //Cap nhat lai danh sach file dinh kem chi tiet
            ChungTuKem.ChungTuDinhKemChiTietCollection = ListCTDK;

            ChungTuKem.SoToKhai = Convert.ToInt64(txtSoToKhai_HYS.Value);
            ChungTuKem.PhanLoaiThuTucKhaiBao = ucPhanLoaiThuTucKhaiBao.Code;
            ChungTuKem.TenThuTucKhaiBao = ucPhanLoaiThuTucKhaiBao.Name_VN;
            ChungTuKem.CoQuanHaiQuan = ucCoQuanHaiQuan_HYS.Code;
            ChungTuKem.NhomXuLyHoSo = ucNhomXuLyHS_HYS.Code;
            //ChungTuKem.NhomXuLyHoSoID = Convert.ToInt32(ctrNhomXuLyHS.Code);
            //ChungTuKem.NgayKhaiBao = dtNgayKhaiBao.Value;
            ChungTuKem.TrangThaiKhaiBao = ucTrangThaiKhaiBao.Code;
            ChungTuKem.NgaySuaCuoiCung = dtNgaySuaCuoiCung.Value;
            ChungTuKem.TenNguoiKhaiBao = txtTenNguoiKhaiBao.Text;
            ChungTuKem.DiaChiNguoiKhaiBao = txtDiaChiNguoiKhaiBao.Text;
            ChungTuKem.SoDienThoaiNguoiKhaiBao = txtSoDienThoaiNguoiKhaiBao.Text;
            ChungTuKem.SoDeLayTepDinhKem = Convert.ToInt32(txtSoDeLayTepDinhKem.Value);
            ChungTuKem.SoQuanLyTrongNoiBoDoanhNghiep = txtSoQuanLyTrongNoiBoDoanhNghiep.Text;
            ChungTuKem.GhiChu = txtGhiChu_HYS.Text;
            ChungTuKem.NgayHoanThanhKiemTraHoSo = dtNgayHoanThanhKiemTraHoSo.Value;
            ChungTuKem.GhiChuHaiQuan = txtGhiChuHaiQuan.Text;
        }

        private void SetChungTuKem_HYS()
        {
            txtSoTiepNhan.Text = ChungTuKem.SoTiepNhan.ToString();
            dtNgayKhaiBao.Value = ChungTuKem.NgayKhaiBao;
            ccNgayTiepNhan.Value = ChungTuKem.NgayTiepNhan;
            txtSoToKhai_HYS.Value = ChungTuKem.SoToKhai;
            ucPhanLoaiThuTucKhaiBao.Code = ChungTuKem.PhanLoaiThuTucKhaiBao;
            ucPhanLoaiThuTucKhaiBao.Name_VN = ChungTuKem.TenThuTucKhaiBao;
            ucCoQuanHaiQuan_HYS.Code = ChungTuKem.CoQuanHaiQuan;
            ucNhomXuLyHS_HYS.Code = ChungTuKem.NhomXuLyHoSo;
            dtNgayKhaiBao.Value = ChungTuKem.NgayKhaiBao;
            ucTrangThaiKhaiBao.Code = ChungTuKem.TrangThaiKhaiBao;
            dtNgaySuaCuoiCung.Value = ChungTuKem.NgaySuaCuoiCung;
            txtTenNguoiKhaiBao.Text = ChungTuKem.TenNguoiKhaiBao;
            txtDiaChiNguoiKhaiBao.Text = ChungTuKem.DiaChiNguoiKhaiBao;
            txtSoDienThoaiNguoiKhaiBao.Text = ChungTuKem.SoDienThoaiNguoiKhaiBao;
            txtSoDeLayTepDinhKem.Value = ChungTuKem.SoDeLayTepDinhKem.ToString();
            txtSoQuanLyTrongNoiBoDoanhNghiep.Text = ChungTuKem.SoQuanLyTrongNoiBoDoanhNghiep;
            txtGhiChu_HYS.Text = ChungTuKem.GhiChu;
            dtNgayHoanThanhKiemTraHoSo.Value = ChungTuKem.NgayHoanThanhKiemTraHoSo;
            txtGhiChuHaiQuan.Text = ChungTuKem.GhiChuHaiQuan;

            ChungTuKem.LoadListChungTuDinhKem_ChiTiet();
            ListCTDK = ChungTuKem.ChungTuDinhKemChiTietCollection;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<KDT_VNACC_ChungTuDinhKem_ChiTiet> HangGiayPhepDetailCollection = new List<KDT_VNACC_ChungTuDinhKem_ChiTiet>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        KDT_VNACC_ChungTuDinhKem_ChiTiet hgpDetail = new KDT_VNACC_ChungTuDinhKem_ChiTiet();
                        hgpDetail = (KDT_VNACC_ChungTuDinhKem_ChiTiet)i.GetRow().DataRow;

                        if (hgpDetail == null) continue;

                        //Detele from DB.
                        if (hgpDetail.ID > 0)
                            hgpDetail.Delete();

                        //Remove out Collction
                        ListCTDK.Remove(hgpDetail);
                    }
                }
            }

            BindData();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (ListCTDK == null || ListCTDK.Count == 0)
                {
                    ShowMessage("Bạn chưa thêm tệp tin chứng từ đính kèm", false);
                    uiTabPageFile.Selected = true;
                    return;
                }

                GetChungTuKem();

                isAddNew = ChungTuKem.ID == 0;
                ChungTuKem.InsertUpdateFull();

                BindData();

                ShowMessage("Lưu thành công.", false);
                setCommandStatus();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            openFileDialog1.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.bmp;*.tiff)|*.jpg;*.jpeg;*.gif;*.bmp;*.tiff|"
                                    + "Text files (*.txt;*.xml;*xsl)|*.txt;*.xml;*xsl|"
                                    + "Application File (*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;)|*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;";

            openFileDialog1.Multiselect = true;

            try
            {
                if (openFileDialog1.ShowDialog(this) != DialogResult.Cancel)
                {
                    long size = 0;

                    for (int k = 0; k < openFileDialog1.FileNames.Length; k++)
                    {
                        System.IO.FileInfo fin = new System.IO.FileInfo(openFileDialog1.FileNames[k]);

                        if (fin.Extension.ToUpper() != ".jpg".ToUpper()
                            && fin.Extension.ToUpper() != ".jpeg".ToUpper()
                            && fin.Extension.ToUpper() != ".gif".ToUpper()
                            && fin.Extension.ToUpper() != ".tiff".ToUpper()
                            && fin.Extension.ToUpper() != ".txt".ToUpper()
                            && fin.Extension.ToUpper() != ".xml".ToUpper()
                            && fin.Extension.ToUpper() != ".xsl".ToUpper()
                            && fin.Extension.ToUpper() != ".csv".ToUpper()
                            && fin.Extension.ToUpper() != ".doc".ToUpper()
                            && fin.Extension.ToUpper() != ".mdb".ToUpper()
                            && fin.Extension.ToUpper() != ".pdf".ToUpper()
                            && fin.Extension.ToUpper() != ".ppt".ToUpper()
                            && fin.Extension.ToUpper() != ".xls".ToUpper())
                        {
                            ShowMessage("Tệp tin " + fin.Name + "Không đúng định dạng tiếp nhận của Hải Quan", false);
                        }
                        else
                        {
                            System.IO.FileStream fs = new System.IO.FileStream(openFileDialog1.FileNames[k], System.IO.FileMode.Open, System.IO.FileAccess.Read);

                            //Cap nhat tong dung luong file.
                            size = 0;

                            for (int i = 0; i < ListCTDK.Count; i++)
                            {
                                size += Convert.ToInt64(ListCTDK[i].FileSize);
                            }

                            //+ them dung luong file moi chuan bi them vao danh sach
                            size += fs.Length;

                            //Kiem tra dung luong file
                            if (size > GlobalSettings.FileSize)
                            {
                                this.ShowMessage(string.Format("Tổng dung lượng cho phép {0}\r\nDung lượng bạn nhập {1} đã vượt mức cho phép.", CalculateFileSize(GlobalSettings.FileSize), CalculateFileSize(size)), false);
                                return;
                            }

                            byte[] data = new byte[fs.Length];
                            fs.Read(data, 0, data.Length);
                            filebase64 = System.Convert.ToBase64String(data);
                            filesize = fs.Length;

                            /*
                             * truoc khi them moi file dinh kem vao danh sach, phai kiem tra tong dung luong co hop len khong?.
                             * Neu > dung luong choh phep -> Hien thi thong bao va khong them vao danh sach.
                             */
                            KDT_VNACC_ChungTuDinhKem_ChiTiet ctctiet = new KDT_VNACC_ChungTuDinhKem_ChiTiet();
                            ctctiet.ChungTuKemID = ChungTuKem.ID;
                            ctctiet.FileName = fin.Name;
                            ctctiet.FileSize = filesize;
                            ctctiet.NoiDung = data;

                            ListCTDK.Add(ctctiet);
                            dgList.DataSource = ListCTDK;
                            try
                            {
                                dgList.Refetch();
                            }
                            catch { dgList.Refresh(); }

                            lblTongDungLuong.Text = CalculateFileSize(size);
                            if (LoaiChungTu == ELoaiThongTin.HYS)
                            {
                                if (ucPhanLoaiThuTucKhaiBao.Code=="A02")
                                {
                                    ReadExcelContainer(fin.FullName,"Sheet1");
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void ReadExcelContainer(string FilePath , string SheetName)
        {
            int beginRow = 2;
            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(FilePath, true);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return;
            }
            try
            {
                ws = wb.Worksheets[SheetName];
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char SoVanDonColumn = Convert.ToChar("A");
            int SoVanDonCol = ConvertCharToInt(SoVanDonColumn);

            char SoContainerColumn = Convert.ToChar("B");
            int SoContainerCol = ConvertCharToInt(SoContainerColumn);

            char SoSealColumn = Convert.ToChar("C");
            int SoSealCol = ConvertCharToInt(SoSealColumn);

            char GhiChuColumn = Convert.ToChar("D");
            int GhiChuCol = ConvertCharToInt(GhiChuColumn);

            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        KDT_VNACC_ContainerDinhKem containerBS = new KDT_VNACC_ContainerDinhKem();
                        containerBS.SoVanDon = Convert.ToString(wsr.Cells[SoVanDonCol].Value);
                        if (containerBS.SoVanDon.ToString().Length == 0)
                        {
                            return;
                        }
                        containerBS.SoContainer = Convert.ToString(wsr.Cells[SoContainerCol].Value).Trim();
                        if (containerBS.SoContainer.Trim().Length == 0)
                        {
                            return;
                        }
                        containerBS.SoSeal = Convert.ToString(wsr.Cells[SoSealCol].Value).Trim();
                        if (containerBS.SoSeal.Trim().Length == 0)
                        {
                            return;
                        }
                        containerBS.GhiChu = Convert.ToString(wsr.Cells[GhiChuCol].Value).Trim();
                        ChungTuKem.ContainerCollection.Add(containerBS);
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
            }
        }
        private void btnXemFile_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                //kiem tra thu da co muc temp?. Neu chua co -> tao 1 thu muc temp de chua cac file tam thoi.
                string path = Application.StartupPath + "\\Temp";

                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }

                //Giai ma basecode64 -> file & luu tam vao thu muc Temp moi tao.
                if (dgList.GetRow() == null)
                {
                    ShowMessage("Chưa chọn file để xem", false);
                    return;
                }
                KDT_VNACC_ChungTuDinhKem_ChiTiet fileData = (KDT_VNACC_ChungTuDinhKem_ChiTiet)dgList.GetRow().DataRow;
                string fileName = path + "\\" + fileData.FileName;

                //Ghi file
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }

                System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                fs.Write(fileData.NoiDung, 0, fileData.NoiDung.Length);
                fs.Close();

                System.Diagnostics.Process.Start(fileName);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXoaChungTu_Click(object sender, EventArgs e)
        {
            if (ChungTuKem.ID > 0)
            {
                if (ShowMessage("Bạn có muốn xóa chứng từ này không?", true) == "Yes")
                {
                    //Xoa chung tu chi tiet
                    ChungTuKem.LoadListChungTuDinhKem_ChiTiet();

                    for (int i = 0; i < ChungTuKem.ChungTuDinhKemChiTietCollection.Count; i++)
                    {
                        ChungTuKem.ChungTuDinhKemChiTietCollection[i].Delete();
                    }

                    //Xoa chung tu trong DB
                    ChungTuKem.Delete();
                }
            }

            //Dong form sau khi xoa chung tu.
            this.Close();
        }

        private void ucCoQuanHaiQuan_EditValueChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    Cursor = Cursors.WaitCursor;

            //    ctrNhomXuLyHS.WhereCondition = string.Format("CustomsCode like '{0}%'", ucCoQuanHaiQuan.Code);
            //    ctrNhomXuLyHS.ReLoadData();
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //    Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            //}
            //finally { Cursor = Cursors.Default; }
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                ucCoQuanHaiQuan.TagCode = "CHN"; //Cơ quan hải quan
                ctrNhomXuLyHS.TagName = "CHB"; //Nhóm xử lý hồ sơ 
                txtTieuDe.Tag = "SUB"; //Tiêu đề
                txtSoToKhai.Tag = "ICN"; //Số tờ khai
                txtGhiChu.Tag = "TUS"; //Ghi chú

                ucCoQuanHaiQuan_HYS.TagCode = "CH"; //Cơ quan hải quan
                ucNhomXuLyHS_HYS.TagName = "CHB"; //Nhóm xử lý hồ sơ 
                ucPhanLoaiThuTucKhaiBao.TagCode = "TSB"; //Phân loại thủ tục khai báo
                txtSoDienThoaiNguoiKhaiBao.Tag = "STL"; //Số điện thoại người khai báo
                txtSoQuanLyTrongNoiBoDoanhNghiep.Tag = "REF"; //Số quản lý trong nội bộ doanh nghiệp
                txtGhiChu_HYS.Tag = "KIJ"; //Ghi chú

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                if (LoaiChungTu == ELoaiThongTin.MSB)
                {
                    ucCoQuanHaiQuan.SetValidate = !isOnlyWarning; ucCoQuanHaiQuan.IsOnlyWarning = isOnlyWarning;
                    isValid &= ucCoQuanHaiQuan.IsValidate; //"Cơ quan hải quan");
                    ctrNhomXuLyHS.SetValidate = !isOnlyWarning; ctrNhomXuLyHS.IsOnlyWarning = isOnlyWarning;
                    isValid &= ctrNhomXuLyHS.IsValidate; //"Nhóm xử lý hồ sơ ");
                }
                else if (LoaiChungTu == ELoaiThongTin.HYS)
                {
                    ucCoQuanHaiQuan_HYS.SetValidate = !isOnlyWarning; ucCoQuanHaiQuan_HYS.IsOnlyWarning = isOnlyWarning;
                    isValid &= ucCoQuanHaiQuan_HYS.IsValidate; //"Cơ quan hải quan");
                    ucPhanLoaiThuTucKhaiBao.SetValidate = !isOnlyWarning; ucPhanLoaiThuTucKhaiBao.IsOnlyWarning = isOnlyWarning;
                    isValid &= ucPhanLoaiThuTucKhaiBao.IsValidate; //"Phân loại thủ tục khai báo");
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                //ucCoQuanHaiQuan.MaxLength = 6;
                //ctrNhomXuLyHS.MaxLength = 2;
                txtTieuDe.MaxLength = 210;
                txtSoToKhai.MaxLength = 15;
                txtGhiChu.MaxLength = 996;

                //ucCoQuanHaiQuan_HYS.MaxLength = 6;
                //ucNhomXuLyHS_HYS.MaxLength = 2;
                //ucPhanLoaiThuTucKhaiBao.MaxLength = 3;
                txtSoDienThoaiNguoiKhaiBao.MaxLength = 20;
                txtSoQuanLyTrongNoiBoDoanhNghiep.MaxLength = 20;
                txtGhiChu_HYS.MaxLength = 996;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            if (LoaiChungTu == ELoaiThongTin.MSB)
            {
                ucCoQuanHaiQuan.TextChanged += new EventHandler(SetTextChanged_Handler); //Số tờ khai đầu tiên
                //ctrNhomXuLyHS.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã loại hình            
                txtSoToKhai.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã hiệu phương thức vận chuyển

                ucCoQuanHaiQuan.IsUpperCase = true; //Số tờ khai đầu tiên
                //ctrNhomXuLyHS.IsUpperCase = true; //Mã loại hình            
                txtSoToKhai.CharacterCasing = CharacterCasing.Upper; //Mã hiệu phương thức vận chuyển
            }
            else if (LoaiChungTu == ELoaiThongTin.MSB)
            {
                ucCoQuanHaiQuan_HYS.TextChanged += new EventHandler(SetTextChanged_Handler); //Nhóm xử lý hồ sơ
                //ucNhomXuLyHS_HYS.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã người nhập khẩu
                ucPhanLoaiThuTucKhaiBao.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã bưu chính
                txtSoDienThoaiNguoiKhaiBao.TextChanged += new EventHandler(SetTextChanged_Handler); //Số điện thoại người nhập khẩu
                txtSoQuanLyTrongNoiBoDoanhNghiep.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã người ủy thác nhập khẩu

                // ucCoQuanHaiQuan_HYS.IsUpperCase = true; //Nhóm xử lý hồ sơ
                // ucNhomXuLyHS_HYS.IsUpperCase = true; //Mã người nhập khẩu
                ucPhanLoaiThuTucKhaiBao.IsUpperCase = true; //Mã bưu chính
                txtSoDienThoaiNguoiKhaiBao.CharacterCasing = CharacterCasing.Upper; //Số điện thoại người nhập khẩu
                txtSoQuanLyTrongNoiBoDoanhNghiep.CharacterCasing = CharacterCasing.Upper; //Mã người ủy thác nhập khẩu
            }
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdKhaiBao":
                    SendVnaccsIDC(false);
                    break;
                case "cmdPhanHoi":
                    ShowKetQuaTraVe();
                    break;
                case "cmdSuaChungTu":
                    ChungTuKem.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoSua;
                    btnGhi_Click(null, null);
                    break;
            }
        }
        private void ShowKetQuaTraVe()
        {

            SendmsgVNACCFrm f = new SendmsgVNACCFrm(null);
            f.isSend = false;
            f.isRep = true;
            f.inputMSGID = ChungTuKem.InputMessageID;
            f.ShowDialog(this);
        }
        #region Send VNACCS

        /// <summary>
        /// Khai báo thông tin
        /// </summary>
        /// <param name="KhaiBaoSua"></param>
        private void SendVnaccsIDC(bool KhaiBaoSua)
        {
            try
            {
                if (ChungTuKem.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }

                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến Hải quan? ", true) == "Yes")
                {
                    ChungTuKem.InputMessageID = HelperVNACCS.NewInputMSGID(); //Tạo mới GUID ID
                    ChungTuKem.InsertUpdateFull(); //Lưu thông tin GUID ID vừa tạo

                    Company.KDT.SHARE.VNACCS.ClassVNACC.MessagesSend msgEdi;
                    string msg;
                    if (this.LoaiChungTu == ELoaiThongTin.HYS)
                    {
                        HYS hys = VNACCMaperFromObject.HYSMapper(ChungTuKem);
                        msgEdi = Company.KDT.SHARE.VNACCS.ClassVNACC.MessagesSend.LoadHYS(hys, ChungTuKem.InputMessageID);
                        //msgEdi.Body = hys.BuildEdiMessages(new StringBuilder(), GlobalVNACC.PathConfig);

                        msg = hys.BuilHYSMsg(new StringBuilder(), GlobalVNACC.PathConfig, ChungTuKem.InputMessageID).ToString();
                        //MSB saa = VNACCMaperFromObject.MSBMapper(ChungTuKem); //Set Mapper
                        //if (saa == null)
                        //{
                        //    this.ShowMessage("Lỗi khi tạo messages !", false);
                        //    return;
                        //}
                        //msg = MessagesSend.Load<MSB>(saa, ChungTuKem.InputMessageID);

                        MsgLog.SaveMessages(msg, "HYS", ChungTuKem.ID, EnumThongBao.SendMess, "Gửi thông tin", ChungTuKem.MessageTag, ChungTuKem.InputMessageID, ChungTuKem.IndexTag); //Lưu thông tin trước khai báo
                    }
                    else
                    {
                        MSB msb = VNACCMaperFromObject.MSBMapper(ChungTuKem);
                        msgEdi = Company.KDT.SHARE.VNACCS.ClassVNACC.MessagesSend.LoadMSB(msb, ChungTuKem.InputMessageID);
                        //msgEdi.Body = hys.BuildEdiMessages(new StringBuilder(), GlobalVNACC.PathConfig);

                        msg = msb.BuilMSBMsg(new StringBuilder(), GlobalVNACC.PathConfig, ChungTuKem.InputMessageID).ToString();
                        MsgLog.SaveMessages(msg, "MSB", ChungTuKem.ID, EnumThongBao.SendMess, "Gửi thông tin", ChungTuKem.MessageTag, ChungTuKem.InputMessageID, ChungTuKem.IndexTag); //Lưu thông tin trước khai báo
                    }
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msgEdi, msg);
                    f.isSend = true; //Có khai báo
                    f.isRep = this.LoaiChungTu == ELoaiThongTin.HYS; //Có nhận phản hồi
                    f.inputMSGID = ChungTuKem.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result) //Có kêt quả trả về
                    {

                        ShowMessage(f.msgFeedBack, false);
                        TuDongCapNhatThongTin();
                    }
                    else if (f.DialogResult == DialogResult.Cancel)
                    {
                        ShowMessage(f.msgFeedBack, false);
                    }
                    else if (f.DialogResult == DialogResult.Yes)
                    {
                        string ketqua = "Khai báo thông tin thành công";

                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                        {
                            try
                            {
                                //decimal soTiepNhan = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
                                //ketqua = ketqua + Environment.NewLine;
                                //ketqua += "Số chứng từ: " + soTiepNhan;

                                //ChungTuKem.SoTiepNhan = soTiepNhan;
                                ChungTuKem.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoChinhThuc;
                                ChungTuKem.InsertUpdateFull();
                            }
                            catch (System.Exception ex)
                            {
                                ketqua += Environment.NewLine + "Lỗi cập nhật: " + Environment.NewLine + ex.Message;
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                            TuDongCapNhatThongTin();
                            ShowMessage(ketqua, false);
                        }
                    }
                    //ShowMessage(f.msgFeedBack, false);
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        #endregion

        #region Cập nhật thông tin
        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            CapNhatThongTin(msgResult, string.Empty);
        }
        private void CapNhatThongTin(ReturnMessages msgResult, string MaNV)
        {
            ProcessMessages.GetDataResult_ChungTuDinhKem(msgResult, MaNV, ChungTuKem);
            SetChungTuKem_HYS();
            SetChungTuKem_MSB();
            ChungTuKem.InsertUpdateFull();
        }

        private void TuDongCapNhatThongTin()
        {
            if (ChungTuKem != null && ChungTuKem.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("MessagesInputID = '{1}'", ChungTuKem.SoTiepNhan.ToString(), ChungTuKem.InputMessageID), null);
                foreach (MsgPhanBo msgPb in listPB)

                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }
            }
        }

        #endregion
        private void ctrCoQuanHaiQuan_Leave(object sender, EventArgs e)
        {
            if (this.LoaiChungTu == ELoaiThongTin.MSB)
            {
                ctrNhomXuLyHS.CustomsCode = ucCoQuanHaiQuan.Code;
                ctrNhomXuLyHS.ReLoadData();
                if (ChungTuKem != null && !string.IsNullOrEmpty(ChungTuKem.NhomXuLyHoSo))
                    ctrNhomXuLyHS.Code = ChungTuKem.NhomXuLyHoSo;
            }
            else if (this.LoaiChungTu == ELoaiThongTin.HYS)
            {
                ucNhomXuLyHS_HYS.CustomsCode = ucCoQuanHaiQuan_HYS.Code;
                ucNhomXuLyHS_HYS.ReLoadData();
                if (ChungTuKem != null && !string.IsNullOrEmpty(ChungTuKem.NhomXuLyHoSo))
                    ucNhomXuLyHS_HYS.Code = ChungTuKem.NhomXuLyHoSo;
            }
        }

        private void setCommandStatus()
        {
            //cmdPhanHoi.Visible = cmdKhaiBao1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            if (ChungTuKem.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao || ChungTuKem.TrangThaiXuLy == null)
            {
                btnGhi.Enabled = btnXoa.Enabled = btnAddNew.Enabled = btnXoaChungTu.Enabled = true;
                cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdPhanHoi.Enabled = cmdSuaChungTu.Enabled = cmdSuaChungTu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //cmdKhaiBaoSua.Visible = cmdIDC.Visible = cmdIDE.Visible = cmdIDA01.Visible = cmdIDD.Visible = Janus.Windows.UI.InheritableBoolean.False;
                //cmdIDA.Visible = cmdIDB.Visible = Janus.Windows.UI.InheritableBoolean.True;

            }
            else if (ChungTuKem.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc)
            {
                btnGhi.Enabled = btnXoa.Enabled = btnAddNew.Enabled = btnXoaChungTu.Enabled = false;
                cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSuaChungTu.Enabled = cmdSuaChungTu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

            }
            else if (ChungTuKem.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
            {
                cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaChungTu.Enabled = cmdSuaChungTu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnGhi.Enabled = btnXoa.Enabled = btnAddNew.Enabled = btnXoaChungTu.Enabled = false;
            }
            else if (ChungTuKem.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoSua || ChungTuKem.TrangThaiXuLy == EnumTrangThaiXuLy.TuChoi)
            {
                cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSuaChungTu.Enabled = cmdSuaChungTu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnGhi.Enabled = btnXoa.Enabled = btnAddNew.Enabled = btnXoaChungTu.Enabled = true;
            }
            
            //             if (string.IsNullOrEmpty(ChungTuKem.InputMessageID))
            //                 cmdKetQuaTraVe.Visible = Janus.Windows.UI.InheritableBoolean.False;
            //             else
            //                 cmdKetQuaTraVe.Visible = Janus.Windows.UI.InheritableBoolean.True;

        }

        private void lblLinkExcelTemplate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_DSContainerDinhKem();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            VNACC_KetQuaXuLy f = new VNACC_KetQuaXuLy(ChungTuKem.ID);
            //f.master_id = TKMD.ID;
            f.ShowDialog(this);
        }


    }
}

