﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace KeyServer
{
    public partial class Key
    {
        public static string GenKey(string Productid, string SearialNum, int days, int soLuong, bool trial, string createdBy)
        {

            try
            {
                string productID = Productid;
                string sfmtKeyNumber = SearialNum;
                int startID = 0;
                List<Key> keyItems = Key.SelectCollectionDynamic("ProductId='" + productID + "' AND SerialNumber like '" + SearialNum + "%'", "SerialNumber Desc");
                if (keyItems.Count > 0)
                {
                    string maxKey = keyItems[0].SerialNumber;
                    startID = int.Parse(maxKey.Substring(7, maxKey.Length - 7));
                }
                DateTime dateTrials = new DateTime(1900, 1, 1); //DateTime.Now.Date.AddDays(soNgay);
                List<Key> keys = new List<Key>();
                string status = trial ? "0" : "1";
                for (int i = 0; i < soLuong; i++)
                {

                    Key k = new Key()
                    {
                        Id = Guid.NewGuid(),
                        ModuleID = Guid.NewGuid(),
                        SerialNumber = string.Format("{0}{1}{2:00000}", sfmtKeyNumber, DateTime.Now.Year, ++startID),
                        ProductId = productID,
                        ActivedKey = Guid.NewGuid().ToString().ToUpper(),
                        Days = days,
                        Status = status,
                        ActivedDate = new DateTime(1900, 1, 1),
                        CreatedDate = DateTime.Now,
                        ExpiredDate = dateTrials,
                        LastCheckStatus = new DateTime(1900, 1, 1),
                        CreatedBy = createdBy,
                    };
                    keys.Add(k);
                }
                Key.InsertCollection(keys);
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static string UpdateNotices(string text, string product, bool trial, bool all)
        {
            try
            {
                string query = string.Empty;
                if (all)
                    query = string.Format("ProductId = '{0}'", product);
                else
                    query = string.Format("ProductId = '{0}' AND Status = '{1}'", product, trial ? "0" : "1");

                List<Key> keys = Key.SelectCollectionDynamic(query, null);
                if (keys == null || keys.Count == 0)
                    return "Loại key không tồn tại";
                foreach (Key key in keys)
                {
                    key.Notice = text;
                    key.CustomerField10 = null;
                    if (key.LastCheckStatus.Year < 1900)
                        key.LastCheckStatus = new DateTime(1900, 1, 1);
                    if (key.RequestedDate.Year < 1900)
                        key.RequestedDate = new DateTime(1900, 1, 1);
                    key.Update();
                }
                return null;
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }
        public static List<Key> GetKey(string productID, string RequestBy, int soNgay, int soluong, string typeKey)
        {
            return GetKey(productID, RequestBy, soNgay, soluong, typeKey, string.Empty, string.Empty, string.Empty);
        }

        public static List<Key> GetKey(string productID, string RequestBy, int soNgay, int soluong, string typeKey, string CustomerCode, string CustomerName, string CustomerDes)
        {
            string querry = string.Format(@"ProductId = '{0}' AND RequestedBy is NULL AND Days = {1} AND year(ActivedDate) = 1900 AND Status = '{2}'", productID, soNgay.ToString(), typeKey);
            List<Key> listkey = Key.SelectCollectionDynamic(querry, "CreatedDate");
            List<Key> listGet = new List<Key>();
            if (listkey == null || listkey.Count == 0)
                return null;
            int SoluongGet;
            if (listkey.Count < soluong)
                SoluongGet = listkey.Count;
            else
                SoluongGet = soluong;
            for (int i = 0; i < SoluongGet; i++)
            {
                Key key = new Key();
                key = listkey[i];
                key.RequestedBy = RequestBy;
                key.RequestedDate = DateTime.Now;
                if (typeKey == "1")
                {
                    key.CustomerName = CustomerName;
                    key.CustomerCode = CustomerCode;
                    key.CustomerDescription = CustomerDes;
                }
                key.Update();
                listGet.Add(key);
            }
            return listGet;
        }

        public static List<Key> SeachKey(string TypeKey, bool? status, DateTime FromDay, DateTime ToDay, string ProductID, string RequestBy)
        {
            List<Key> listKey = new List<Key>();
            if (FromDay.Year == 1900)
                FromDay = new DateTime(2000, 1, 1);
            string typekey = string.IsNullOrEmpty(TypeKey) ? "Status <> '-1'" : "Status = '" + TypeKey + "'";
            string active = "(1=1)";
            if (status != null)
                active = status.Value ? "Year(ActivedDate) > 1900" : "Year(ActivedDate) = 1900";
            
            string product = string.IsNullOrEmpty(ProductID) ? string.Empty : string.Format("AND ProductId = '{0}'", ProductID);
            string request = string.IsNullOrEmpty(RequestBy) ? string.Empty : string.Format("AND RequestedBy = '{0}'", RequestBy);
            string where = string.Format("{0} AND {1} AND RequestedDate > '{2}' AND RequestedDate < '{3}' {4} {5}",
                typekey, active, FromDay.ToString("yyyy-MM-dd"), ToDay.ToString("yyyy-MM-dd"), product, request);
            listKey = Key.SelectCollectionDynamic(where, "RequestedDate");
            return listKey;
        }
        public static List<Key> SeachAllKey(int permissionID, DateTime FromDay, DateTime ToDay)
        {
            List<Key> collection = new List<Key>();
            const string spName = "[dbo].[p_LM_Key_SeachAllbyPermission]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@PermissionID", SqlDbType.BigInt, permissionID);
            db.AddInParameter(dbCommand, "@Fromday", SqlDbType.DateTime, FromDay);
            db.AddInParameter(dbCommand, "@ToDay", SqlDbType.DateTime, ToDay);
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                Key entity = new Key();

                if (!reader.IsDBNull(reader.GetOrdinal("Id"))) entity.Id = reader.GetGuid(reader.GetOrdinal("Id"));
                if (!reader.IsDBNull(reader.GetOrdinal("ModuleID"))) entity.ModuleID = reader.GetGuid(reader.GetOrdinal("ModuleID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SerialNumber"))) entity.SerialNumber = reader.GetString(reader.GetOrdinal("SerialNumber"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActivedKey"))) entity.ActivedKey = reader.GetString(reader.GetOrdinal("ActivedKey"));
                if (!reader.IsDBNull(reader.GetOrdinal("ProductId"))) entity.ProductId = reader.GetString(reader.GetOrdinal("ProductId"));
                if (!reader.IsDBNull(reader.GetOrdinal("Days"))) entity.Days = reader.GetInt32(reader.GetOrdinal("Days"));
                if (!reader.IsDBNull(reader.GetOrdinal("RecordCount"))) entity.RecordCount = reader.GetInt32(reader.GetOrdinal("RecordCount"));
                if (!reader.IsDBNull(reader.GetOrdinal("Status"))) entity.Status = reader.GetString(reader.GetOrdinal("Status"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerCode"))) entity.CustomerCode = reader.GetString(reader.GetOrdinal("CustomerCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerName"))) entity.CustomerName = reader.GetString(reader.GetOrdinal("CustomerName"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerAddress"))) entity.CustomerAddress = reader.GetString(reader.GetOrdinal("CustomerAddress"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerPhone"))) entity.CustomerPhone = reader.GetString(reader.GetOrdinal("CustomerPhone"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerEmail"))) entity.CustomerEmail = reader.GetString(reader.GetOrdinal("CustomerEmail"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerDescription"))) entity.CustomerDescription = reader.GetString(reader.GetOrdinal("CustomerDescription"));
                if (!reader.IsDBNull(reader.GetOrdinal("MachineCode"))) entity.MachineCode = reader.GetString(reader.GetOrdinal("MachineCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActivedDate"))) entity.ActivedDate = reader.GetDateTime(reader.GetOrdinal("ActivedDate"));
                if (!reader.IsDBNull(reader.GetOrdinal("ExpiredDate"))) entity.ExpiredDate = reader.GetDateTime(reader.GetOrdinal("ExpiredDate"));
                if (!reader.IsDBNull(reader.GetOrdinal("RequestedBy"))) entity.RequestedBy = reader.GetString(reader.GetOrdinal("RequestedBy"));
                if (!reader.IsDBNull(reader.GetOrdinal("RequestedDate"))) entity.RequestedDate = reader.GetDateTime(reader.GetOrdinal("RequestedDate"));
                if (!reader.IsDBNull(reader.GetOrdinal("LastCheckStatus"))) entity.LastCheckStatus = reader.GetDateTime(reader.GetOrdinal("LastCheckStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("Command"))) entity.Command = reader.GetString(reader.GetOrdinal("Command"));
                if (!reader.IsDBNull(reader.GetOrdinal("Notice"))) entity.Notice = reader.GetString(reader.GetOrdinal("Notice"));
                if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField1"))) entity.CustomerField1 = reader.GetString(reader.GetOrdinal("CustomerField1"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField2"))) entity.CustomerField2 = reader.GetString(reader.GetOrdinal("CustomerField2"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField3"))) entity.CustomerField3 = reader.GetString(reader.GetOrdinal("CustomerField3"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField4"))) entity.CustomerField4 = reader.GetString(reader.GetOrdinal("CustomerField4"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField5"))) entity.CustomerField5 = reader.GetString(reader.GetOrdinal("CustomerField5"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField6"))) entity.CustomerField6 = reader.GetString(reader.GetOrdinal("CustomerField6"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField7"))) entity.CustomerField7 = reader.GetString(reader.GetOrdinal("CustomerField7"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField8"))) entity.CustomerField8 = reader.GetString(reader.GetOrdinal("CustomerField8"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField9"))) entity.CustomerField9 = reader.GetString(reader.GetOrdinal("CustomerField9"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField10"))) entity.CustomerField10 = reader.GetString(reader.GetOrdinal("CustomerField10"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreatedDate"))) entity.CreatedDate = reader.GetDateTime(reader.GetOrdinal("CreatedDate"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreatedBy"))) entity.CreatedBy = reader.GetString(reader.GetOrdinal("CreatedBy"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;			

        }
        public static List<Key> SeachByCustomer(string CustomerCode, string WhereCondition)
        {
            List<Key> collection = new List<Key>();
            const string spName = "[dbo].[p_LM_Key_SelectByCustomer]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, CustomerCode);
            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, WhereCondition);
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                Key entity = new Key();
                if (!reader.IsDBNull(reader.GetOrdinal("Id"))) entity.Id = reader.GetGuid(reader.GetOrdinal("Id"));
                if (!reader.IsDBNull(reader.GetOrdinal("ModuleID"))) entity.ModuleID = reader.GetGuid(reader.GetOrdinal("ModuleID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SerialNumber"))) entity.SerialNumber = reader.GetString(reader.GetOrdinal("SerialNumber"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActivedKey"))) entity.ActivedKey = reader.GetString(reader.GetOrdinal("ActivedKey"));
                if (!reader.IsDBNull(reader.GetOrdinal("ProductId"))) entity.ProductId = reader.GetString(reader.GetOrdinal("ProductId"));
                if (!reader.IsDBNull(reader.GetOrdinal("Days"))) entity.Days = reader.GetInt32(reader.GetOrdinal("Days"));
                if (!reader.IsDBNull(reader.GetOrdinal("RecordCount"))) entity.RecordCount = reader.GetInt32(reader.GetOrdinal("RecordCount"));
                if (!reader.IsDBNull(reader.GetOrdinal("Status"))) entity.Status = reader.GetString(reader.GetOrdinal("Status"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerCode"))) entity.CustomerCode = reader.GetString(reader.GetOrdinal("CustomerCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerName"))) entity.CustomerName = reader.GetString(reader.GetOrdinal("CustomerName"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerAddress"))) entity.CustomerAddress = reader.GetString(reader.GetOrdinal("CustomerAddress"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerPhone"))) entity.CustomerPhone = reader.GetString(reader.GetOrdinal("CustomerPhone"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerEmail"))) entity.CustomerEmail = reader.GetString(reader.GetOrdinal("CustomerEmail"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerDescription"))) entity.CustomerDescription = reader.GetString(reader.GetOrdinal("CustomerDescription"));
                if (!reader.IsDBNull(reader.GetOrdinal("MachineCode"))) entity.MachineCode = reader.GetString(reader.GetOrdinal("MachineCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActivedDate"))) entity.ActivedDate = reader.GetDateTime(reader.GetOrdinal("ActivedDate"));
                if (!reader.IsDBNull(reader.GetOrdinal("ExpiredDate"))) entity.ExpiredDate = reader.GetDateTime(reader.GetOrdinal("ExpiredDate"));
                if (!reader.IsDBNull(reader.GetOrdinal("RequestedBy"))) entity.RequestedBy = reader.GetString(reader.GetOrdinal("RequestedBy"));
                if (!reader.IsDBNull(reader.GetOrdinal("RequestedDate"))) entity.RequestedDate = reader.GetDateTime(reader.GetOrdinal("RequestedDate"));
                if (!reader.IsDBNull(reader.GetOrdinal("LastCheckStatus"))) entity.LastCheckStatus = reader.GetDateTime(reader.GetOrdinal("LastCheckStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("Command"))) entity.Command = reader.GetString(reader.GetOrdinal("Command"));
                if (!reader.IsDBNull(reader.GetOrdinal("Notice"))) entity.Notice = reader.GetString(reader.GetOrdinal("Notice"));
                if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField1"))) entity.CustomerField1 = reader.GetString(reader.GetOrdinal("CustomerField1"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField2"))) entity.CustomerField2 = reader.GetString(reader.GetOrdinal("CustomerField2"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField3"))) entity.CustomerField3 = reader.GetString(reader.GetOrdinal("CustomerField3"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField4"))) entity.CustomerField4 = reader.GetString(reader.GetOrdinal("CustomerField4"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField5"))) entity.CustomerField5 = reader.GetString(reader.GetOrdinal("CustomerField5"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField6"))) entity.CustomerField6 = reader.GetString(reader.GetOrdinal("CustomerField6"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField7"))) entity.CustomerField7 = reader.GetString(reader.GetOrdinal("CustomerField7"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField8"))) entity.CustomerField8 = reader.GetString(reader.GetOrdinal("CustomerField8"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField9"))) entity.CustomerField9 = reader.GetString(reader.GetOrdinal("CustomerField9"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField10"))) entity.CustomerField10 = reader.GetString(reader.GetOrdinal("CustomerField10"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreatedDate"))) entity.CreatedDate = reader.GetDateTime(reader.GetOrdinal("CreatedDate"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreatedBy"))) entity.CreatedBy = reader.GetString(reader.GetOrdinal("CreatedBy"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;			

        }

        public static string ProposalKeyLicense(string productID, string RequestBy, int soNgay, int soluong, string CustomerCode, string CustomerName, string CustomerDescription)
        {
            string querry = string.Format(@"ProductId = '{0}' AND RequestedBy is NULL AND Days = {1} AND year(ActivedDate) = 1900 AND Status = '1'", productID, soNgay.ToString());
            List<Key> listkey = Key.SelectCollectionDynamic(querry, "CreatedDate");
            if (listkey == null || listkey.Count == 0)
                return "Không tìm thấy key phù hợp";
            if (listkey.Count < soluong)
                return "Số lượng key không đủ";
            for (int i = 0; i < soluong; i++)
            {
                Key key = new Key();
                key = listkey[i];
                key.RequestedBy = RequestBy;
                key.RequestedDate = DateTime.Now;
                key.CustomerCode = CustomerCode;
                key.CustomerDescription = CustomerDescription;
                key.CustomerName = CustomerName;
                key.Status = TypeKey.ChoXacNhan;
                key.Update();
            }
            return string.Empty;
        }
        public static Key checkKey(string activecode)
        {
            Key key = new Key();
            try
            {
                string querry = string.Format(@"ActivedKey = '{0}'", activecode);
                List<Key> listkey = Key.SelectCollectionDynamic(querry, null);
                if (listkey == null || listkey.Count == 0)
                    return null;
                key = listkey[0];
                return key;
            }
            catch
            {
                return null;
            }
        }
        public static List<Key> SelectProduct()
        {
            string sql = @"SELECT DISTINCT Days, Status, ProductId,SUBSTRING(SerialNumber, 0, 4) As SerialNumber
                            FROM         [SOFTECH.LICENSES].dbo.t_LM_Keys
                            ORDER BY ProductId,SerialNumber, Status, Days";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            List<Key> collection = new List<Key>();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                Key entity = new Key();
                if (!reader.IsDBNull(reader.GetOrdinal("ProductId"))) entity.ProductId = reader.GetString(reader.GetOrdinal("ProductId"));
                if (!reader.IsDBNull(reader.GetOrdinal("Days"))) entity.Days = reader.GetInt32(reader.GetOrdinal("Days"));
                if (!reader.IsDBNull(reader.GetOrdinal("Status"))) entity.Status = reader.GetString(reader.GetOrdinal("Status"));
                if (!reader.IsDBNull(reader.GetOrdinal("SerialNumber"))) entity.SerialNumber = reader.GetString(reader.GetOrdinal("SerialNumber"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }


        public static string ApproveKeyLicense(string SerialNumber, bool Approve, string UserName)
        {
            List<Key> ListkeyApprove = Key.SelectCollectionDynamic("SerialNumber = '" + SerialNumber + "'", null);
            if (ListkeyApprove == null || ListkeyApprove.Count != 1)
                return "Key không hợp lệ";
            Key ApproveKey = ListkeyApprove[0];
            if (Approve)
            {
                ApproveKey.Notes = ", " +UserName + " Approve " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                ApproveKey.Status = TypeKey.License;
                ApproveKey.Update();
            }
            else
            {
                ApproveKey.RequestedBy = null;
                ApproveKey.RequestedDate = DateTime.Parse("1900-01-01 00:00:00.000");
                ApproveKey.Status = TypeKey.License;
                ApproveKey.CustomerName = string.Empty;
                ApproveKey.CustomerCode = string.Empty;
                ApproveKey.CustomerDescription = string.Empty;
                ApproveKey.Update();
            }
            return string.Empty;
        }

        public static List<Key> SelectCollectionByCustomer(string CustomerCode)
        {
            List<Key> collection = new List<Key>();

            const string spName = "[dbo].[p_LM_Key_LoadByCustomer]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, CustomerCode);
            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                Key entity = new Key();

                if (!reader.IsDBNull(reader.GetOrdinal("Id"))) entity.Id = reader.GetGuid(reader.GetOrdinal("Id"));
                if (!reader.IsDBNull(reader.GetOrdinal("ModuleID"))) entity.ModuleID = reader.GetGuid(reader.GetOrdinal("ModuleID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SerialNumber"))) entity.SerialNumber = reader.GetString(reader.GetOrdinal("SerialNumber"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActivedKey"))) entity.ActivedKey = reader.GetString(reader.GetOrdinal("ActivedKey"));
                if (!reader.IsDBNull(reader.GetOrdinal("ProductId"))) entity.ProductId = reader.GetString(reader.GetOrdinal("ProductId"));
                if (!reader.IsDBNull(reader.GetOrdinal("Days"))) entity.Days = reader.GetInt32(reader.GetOrdinal("Days"));
                if (!reader.IsDBNull(reader.GetOrdinal("RecordCount"))) entity.RecordCount = reader.GetInt32(reader.GetOrdinal("RecordCount"));
                if (!reader.IsDBNull(reader.GetOrdinal("Status"))) entity.Status = reader.GetString(reader.GetOrdinal("Status"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerCode"))) entity.CustomerCode = reader.GetString(reader.GetOrdinal("CustomerCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerName"))) entity.CustomerName = reader.GetString(reader.GetOrdinal("CustomerName"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerAddress"))) entity.CustomerAddress = reader.GetString(reader.GetOrdinal("CustomerAddress"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerPhone"))) entity.CustomerPhone = reader.GetString(reader.GetOrdinal("CustomerPhone"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerEmail"))) entity.CustomerEmail = reader.GetString(reader.GetOrdinal("CustomerEmail"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerDescription"))) entity.CustomerDescription = reader.GetString(reader.GetOrdinal("CustomerDescription"));
                if (!reader.IsDBNull(reader.GetOrdinal("MachineCode"))) entity.MachineCode = reader.GetString(reader.GetOrdinal("MachineCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActivedDate"))) entity.ActivedDate = reader.GetDateTime(reader.GetOrdinal("ActivedDate"));
                if (!reader.IsDBNull(reader.GetOrdinal("ExpiredDate"))) entity.ExpiredDate = reader.GetDateTime(reader.GetOrdinal("ExpiredDate"));
                if (!reader.IsDBNull(reader.GetOrdinal("RequestedBy"))) entity.RequestedBy = reader.GetString(reader.GetOrdinal("RequestedBy"));
                if (!reader.IsDBNull(reader.GetOrdinal("RequestedDate"))) entity.RequestedDate = reader.GetDateTime(reader.GetOrdinal("RequestedDate"));
                if (!reader.IsDBNull(reader.GetOrdinal("LastCheckStatus"))) entity.LastCheckStatus = reader.GetDateTime(reader.GetOrdinal("LastCheckStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("Command"))) entity.Command = reader.GetString(reader.GetOrdinal("Command"));
                if (!reader.IsDBNull(reader.GetOrdinal("Notice"))) entity.Notice = reader.GetString(reader.GetOrdinal("Notice"));
                if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField1"))) entity.CustomerField1 = reader.GetString(reader.GetOrdinal("CustomerField1"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField2"))) entity.CustomerField2 = reader.GetString(reader.GetOrdinal("CustomerField2"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField3"))) entity.CustomerField3 = reader.GetString(reader.GetOrdinal("CustomerField3"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField4"))) entity.CustomerField4 = reader.GetString(reader.GetOrdinal("CustomerField4"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField5"))) entity.CustomerField5 = reader.GetString(reader.GetOrdinal("CustomerField5"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField6"))) entity.CustomerField6 = reader.GetString(reader.GetOrdinal("CustomerField6"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField7"))) entity.CustomerField7 = reader.GetString(reader.GetOrdinal("CustomerField7"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField8"))) entity.CustomerField8 = reader.GetString(reader.GetOrdinal("CustomerField8"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField9"))) entity.CustomerField9 = reader.GetString(reader.GetOrdinal("CustomerField9"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomerField10"))) entity.CustomerField10 = reader.GetString(reader.GetOrdinal("CustomerField10"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreatedDate"))) entity.CreatedDate = reader.GetDateTime(reader.GetOrdinal("CreatedDate"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreatedBy"))) entity.CreatedBy = reader.GetString(reader.GetOrdinal("CreatedBy"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static string LockKey(string serialNumber, string LyDoLock, string userName)
        {
            List<Key> _listKey = Key.SelectCollectionDynamic("SerialNumber = '" + serialNumber + "'", null);
            if (_listKey != null && _listKey.Count > 0)
            {
                Key keyLock = _listKey[0];
                keyLock.Status = TypeKey.Lock;
                keyLock.Notes = keyLock.Notes + ", ("+DateTime.Now+")" + userName + " LOCK key - Lý do: " + LyDoLock;
                keyLock.Update();
                return string.Empty;
            }
            else
                return "Không tồn tại key";
        }
    }
}
