------------------------------------------MÃ ÁP DỤNG MỨC THUẾ TUYỆT ĐỐI TRONG VNACCS -------------------------------------
DELETE FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A402'
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A402' AND Code='1')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A402' ,'1' ,N'Xe ô tô chở người từ 9 chỗ ngồi trở xuống (kể cả lái xe) có dung tích xi lanh dưới 1.000cc (5000 USD/PCE)')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='1' ,Name_VN = N'Xe ô tô chở người từ 9 chỗ ngồi trở xuống (kể cả lái xe) có dung tích xi lanh dưới 1.000cc (5000 USD/PCE)' WHERE ReferenceDB='A402' AND Code='1'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A402' AND Code='2')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A402' ,'2' ,N'Xe ô tô chở người từ 9 chỗ ngồi trở xuống (kể cả lái xe) có dung tích xi lanh từ 1.000cc đến dưới 1.500cc (10000 USD/PCE)')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='2' ,Name_VN = N'Xe ô tô chở người từ 9 chỗ ngồi trở xuống (kể cả lái xe) có dung tích xi lanh từ 1.000cc đến dưới 1.500cc (10000 USD/PCE)' WHERE ReferenceDB='A402' AND Code='2'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A402' AND Code='3')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A402' ,'3' ,N'Xe ô tô chở người từ 10 đến 15 chỗ ngồi (kể cả lái xe) có dung tích xi lanh từ 2.000cc trở xuống (9500 USD/PCE)')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='3' ,Name_VN = N'Xe ô tô chở người từ 10 đến 15 chỗ ngồi (kể cả lái xe) có dung tích xi lanh từ 2.000cc trở xuống (9500 USD/PCE)' WHERE ReferenceDB='A402' AND Code='3'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A402' AND Code='4')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A402' ,'4' ,N'Xe ô tô chở người từ 10 đến 15 chỗ ngồi (kể cả lái xe) có dung tích xi lanh trên 2.000cc đến 3.000cc (13000 USD/PCE)')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='4' ,Name_VN = N'Xe ô tô chở người từ 10 đến 15 chỗ ngồi (kể cả lái xe) có dung tích xi lanh trên 2.000cc đến 3.000cc (13000 USD/PCE)' WHERE ReferenceDB='A402' AND Code='4'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A402' AND Code='5')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A402' ,'5' ,N'Xe ô tô chở người từ 10 đến 15 chỗ ngồi (kể cả lái xe) có dung tích xi lanh trên 3.000cc (17000 USD/PCE)')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='5' ,Name_VN = N'Xe ô tô chở người từ 10 đến 15 chỗ ngồi (kể cả lái xe) có dung tích xi lanh trên 3.000cc (17000 USD/PCE)' WHERE ReferenceDB='A402' AND Code='5'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A402' AND Code='6')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A402' ,'6' ,N'Xe ô tô chở người từ 9 chỗ ngồi trở xuống (kể cả lái xe) có dung tích xi lanh từ 1.500cc đến dưới 2.500cc (5000 USD/PCE)')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='6' ,Name_VN = N'Xe ô tô chở người từ 9 chỗ ngồi trở xuống (kể cả lái xe) có dung tích xi lanh từ 1.500cc đến dưới 2.500cc (5000 USD/PCE)' WHERE ReferenceDB='A402' AND Code='6'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A402' AND Code='7')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A402' ,'7' ,N'Xe ô tô chở người từ 9 chỗ ngồi trở xuống (kể cả lái xe) có dung tích xi lanh từ 2.500cc trở lên (15000 USD/PCE)')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='7' ,Name_VN = N'Xe ô tô chở người từ 9 chỗ ngồi trở xuống (kể cả lái xe) có dung tích xi lanh từ 2.500cc trở lên (15000 USD/PCE)' WHERE ReferenceDB='A402' AND Code='7'
END

END

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '21.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('21.0',GETDATE(), N'BẢNG MÃ ÁP DỤNG MỨC THUẾ TUYỆT ĐỐI TRONG VNACCS')
END