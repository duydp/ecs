﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using Company.BLL;
using System.Text;
using System.Security.Cryptography;

namespace DongBoDuLieu
{
    public partial class WSForm : BaseForm
    {
        public bool IsReady = false;
        public WSForm()
        {
            InitializeComponent();
        }

        private void WSForm_Load(object sender, EventArgs e)
        {
            txtMaDoanhNghiep.Text = "";
        }

        private void btnConnect_Click(object sender, EventArgs e)
        { 
            if (!ccError.IsValid)
                return;
            IsReady = true;
            //txtMatKhau.Text = GetMD5Value(txtMaDoanhNghiep.Text.Trim() + "+" + txtMatKhau.Text.Trim());
            txtMatKhau.Text = GetMD5Value(txtMatKhau.Text.Trim());
            if (ckLuu.Checked)
            {
                MainForm.password = txtMatKhau.Text;
                MainForm.user = txtMaDoanhNghiep.Text;
            }
            this.Close();
            
        }
        public string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();


        }
      
    }
}