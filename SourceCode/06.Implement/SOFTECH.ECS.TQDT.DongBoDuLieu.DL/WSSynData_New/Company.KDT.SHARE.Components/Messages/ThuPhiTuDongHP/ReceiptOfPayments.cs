﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
   public class ReceiptOfPayments
    {
        /// <summary>
        ///  Danh sách Khoản mục phí.
        /// </summary>
       [XmlElement("ReceiptOfPayment")]
       public List<ReceiptOfPayment> ReceiptOfPayment { get; set; }
    }
}
