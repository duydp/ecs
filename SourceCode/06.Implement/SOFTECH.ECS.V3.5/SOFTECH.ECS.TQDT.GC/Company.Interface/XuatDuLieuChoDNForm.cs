﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using System.IO;
using System.Xml.Serialization;

namespace Company.Interface
{
    public partial class XuatDuLieuChoDNForm : BaseForm
    {
        XmlSerializer serializer;
        List<HopDong>  HopDongCol;
        HopDong HD;
        Company.GC.BLL.GC.DinhMucCollection DinhMucCol;
        Company.GC.BLL.GC.DinhMuc DM;
        PhuKienDangKyCollection PhuKienCol;
        PhuKienDangKy PK;
        Company.GC.BLL.KDT.ToKhaiMauDichCollection ToKhaiMauDichCol;
        Company.GC.BLL.KDT.ToKhaiMauDich TKMD;
        List<ToKhaiChuyenTiep> ToKhaiChuyenTiepCol;
        ToKhaiChuyenTiep TKCT;
        Company.GC.BLL.KDT.GC.BKCungUngDangKyCollection BKCUCol;
        Company.GC.BLL.KDT.GC.BKCungUngDangKy BKCU;

        public string ThuMucLuu;
        public string DieuKienHopDong = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND TrangThaiXuLy = 1";
        public string DieuKienDinhMuc = "HopDong_ID in (";
        public string DieuKienPhuKien = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND TrangThaiXuLy = 1 AND HopDong_ID in (";
        public string DieuKienTKMD = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND TrangThaiXuLy = 1 AND IDHopDong in (";
        public string DieuKienTKGCCT = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND TrangThaiXuLy = 1 AND IDHopDong in (";
        public string DieuKienBKCU = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND TrangThaiXuLy = 1 ";

        

        public XuatDuLieuChoDNForm()
        {
            InitializeComponent();
        }

        private void XuatDuLieuChoDNForm_Load(object sender, EventArgs e)
        {
            ThuMucLuu = Application.StartupPath;
            txtThuMucLuu.Text = ThuMucLuu;
            HD = new HopDong();
            DM = new Company.GC.BLL.GC.DinhMuc();
            PK = new PhuKienDangKy();
            TKMD = new Company.GC.BLL.KDT.ToKhaiMauDich();
            TKCT = new ToKhaiChuyenTiep();
            BKCU = new BKCungUngDangKy();
            ToKhaiChuyenTiepCol = new List<ToKhaiChuyenTiep>();
            ToKhaiMauDichCol = new Company.GC.BLL.KDT.ToKhaiMauDichCollection();
        }

        private void chkDefault_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkDefault.Checked)
            {
                btnChon.Enabled = true;
                txtThuMucLuu.Enabled = true;
            }
            else
            {
                ThuMucLuu = Application.StartupPath;
                txtThuMucLuu.Text = ThuMucLuu;
                btnChon.Enabled = false;
                txtThuMucLuu.Enabled = false;
            }
        }


        private void btnXuat_Click(object sender, EventArgs e)
        {
            HopDongCol = HopDong.SelectCollectionDynamic(DieuKienHopDong, "ID");
            ChonHopDongForm HDform = new ChonHopDongForm();
            HDform.HDCol = HopDongCol;
            HDform.ShowDialog();
            HopDongCol.Clear();
            HopDongCol = HDform.HDColRet;

            if (HopDongCol.Count == 0)
            {
                ShowMessage("Không có hợp đồng nào được chọn !",false);
                return;
            }

            string Folder = ThuMucLuu + "\\" + GlobalSettings.MA_DON_VI;
            if (!System.IO.Directory.Exists(Folder))
                System.IO.Directory.CreateDirectory(Folder);

            /* XUẤT HỢP ĐỒNG */
            FileStream fsHD = new FileStream(Folder + "\\HopDong.XML", FileMode.Create);
            serializer = new XmlSerializer(typeof(List<HopDong> ));
            foreach (HopDong HDLoad in HopDongCol)
                HDLoad.LoadCollection();
            serializer.Serialize(fsHD, HopDongCol);
            fsHD.Close();

            string msg = "Xuất thành công :";
            int sttHD = 0;
            int sttTKMD = 0;
            int sttTKGCCT = 0;
            foreach (HopDong HDTemp in HopDongCol)
            {
                sttHD++;
                if (sttHD == HopDongCol.Count)
                {
                    DieuKienDinhMuc += HDTemp.ID.ToString() + ") ";
                    DieuKienPhuKien += HDTemp.ID.ToString() + ") ";
                    DieuKienTKMD += HDTemp.ID.ToString() + ") ";
                    DieuKienTKGCCT += HDTemp.ID.ToString() + ") ";
                }
                else
                {
                    DieuKienDinhMuc += HDTemp.ID.ToString() + ", ";
                    DieuKienPhuKien += HDTemp.ID.ToString() + ", ";
                    DieuKienTKMD += HDTemp.ID.ToString() + ", ";
                    DieuKienTKGCCT += HDTemp.ID.ToString() + ", ";
                }
            }

            /* XUẤT ĐỊNH MỨC, PHỤ KIỆN, TỜ KHAI MẬU DỊCH, TỜ KHAI GCCT, BẢNG KÊ CUNG ỨNG */
            try
            {
                if (sttHD > 0)
                {
                    msg += "\n\t " + sttHD + " hợp đồng";
                    /* ĐỊNH MỨC */
                    if (chkDinhMuc.Checked)
                    {
                        DinhMucCol = DM.SelectCollectionDynamic(DieuKienDinhMuc, "HopDong_ID");

                        if (DinhMucCol.Count > 0)
                        {
                            serializer = new XmlSerializer(typeof(Company.GC.BLL.GC.DinhMucCollection));
                            FileStream fsDM = new FileStream(Folder + "\\DinhMuc.XML", FileMode.Create);
                            serializer.Serialize(fsDM, DinhMucCol);
                            fsDM.Close();

                            msg += "\n\t " + DinhMucCol.Count + " định mức";
                        }
                        else msg += "\n\t không có định mức cần xuất";
                    }


                    /* PHỤ KIỆN */
                    if (chkPhuKien.Checked)
                    {
                        PhuKienCol = PK.SelectCollectionDynamic(DieuKienPhuKien, "HopDong_ID");
                        foreach (PhuKienDangKy PKLoad in PhuKienCol)
                        {
                            PKLoad.LoadCollection();
                            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK in PKLoad.PKCollection)
                            {
                                LoaiPK.LoadCollection();
                            }
                        }

                        if (PhuKienCol.Count > 0)
                        {
                            serializer = new XmlSerializer(typeof(PhuKienDangKyCollection));
                            FileStream fsPK = new FileStream(Folder + "\\PhuKien.XML", FileMode.Create);
                            serializer.Serialize(fsPK, PhuKienCol);
                            fsPK.Close();

                            msg += "\n\t " + PhuKienCol.Count + " phụ kiện";
                        }
                        else msg += "\n\t không có phụ kiện cần xuất";
                    }


                    /* TỜ KHAI MẬU DỊCH */
                    if (chkTKMD.Checked)
                    {
                        ToKhaiMauDichCol = TKMD.SelectCollectionDynamic(DieuKienTKMD, "IDHopDong");
                        if (ToKhaiMauDichCol.Count > 0)
                            DieuKienBKCU += " AND ((TKMD_ID in (";
                        foreach (Company.GC.BLL.KDT.ToKhaiMauDich TKMDLoad in ToKhaiMauDichCol)
                        {
                            sttTKMD++;
                            TKMDLoad.LoadHMDCollection();
                            if (sttTKMD == ToKhaiMauDichCol.Count)
                            {
                                DieuKienBKCU += TKMDLoad.ID + ")";
                            }
                            else
                                DieuKienBKCU += TKMDLoad.ID + ", ";
                        }

                        if (sttTKMD > 0)
                        {
                            serializer = new XmlSerializer(typeof(Company.GC.BLL.KDT.ToKhaiMauDichCollection));
                            FileStream fsTKMD = new FileStream(Folder + "\\ToKhaiMauDich.XML", FileMode.Create);
                            serializer.Serialize(fsTKMD, ToKhaiMauDichCol);
                            fsTKMD.Close();

                            msg += "\n\t " + sttTKMD + " tờ khai mậu dịch";
                        }
                        else msg += "\n\t không có TKMD cần xuất";
                    }


                    /* TỜ KHAI CHUYỂN TIẾP */
                    if (chkTKGCCT.Checked)
                    {
                        ToKhaiChuyenTiepCol = ToKhaiChuyenTiep.SelectCollectionDynamic(DieuKienTKGCCT, "IDHopDong");
                        if (ToKhaiMauDichCol.Count > 0)
                        {
                            if (ToKhaiChuyenTiepCol.Count > 0)
                                DieuKienBKCU += ") OR (TKCT_ID in (";
                            else DieuKienBKCU += "))";
                        }
                        else
                        {
                            if (ToKhaiChuyenTiepCol.Count > 0)
                                DieuKienBKCU += " AND (TKCT_ID in (";
                        }

                        foreach (ToKhaiChuyenTiep TKGCCTLoad in ToKhaiChuyenTiepCol)
                        {
                            sttTKGCCT++;
                            TKGCCTLoad.LoadHCTCollection();
                            if (sttTKGCCT == ToKhaiChuyenTiepCol.Count)
                            {
                                if (ToKhaiMauDichCol.Count > 0)
                                    DieuKienBKCU += TKGCCTLoad.ID + ")))";
                                else DieuKienBKCU += TKGCCTLoad.ID + "))";
                            }
                            else
                                DieuKienBKCU += TKGCCTLoad.ID + ", ";
                        }

                        if (sttTKGCCT > 0)
                        {
                            serializer = new XmlSerializer(typeof(List<ToKhaiChuyenTiep>));
                            FileStream fsTKGCCT = new FileStream(Folder + "\\ToKhaiGiaCongChuyenTiep.XML", FileMode.Create);
                            serializer.Serialize(fsTKGCCT, ToKhaiChuyenTiepCol);
                            fsTKGCCT.Close();

                            msg += "\n\t " + sttTKGCCT + " tờ khai gia công chuyển tiếp";
                        }
                        else msg += "\n\t không có TKCT cần xuất";
                    }
                    else
                        if (ToKhaiMauDichCol.Count > 0)
                            DieuKienBKCU += "))";


                    /* BẢNG KÊ CUNG ỨNG */
                    if (chkBKCU.Checked && (chkTKGCCT.Checked || chkTKMD.Checked))
                    {
                        if (ToKhaiChuyenTiepCol.Count > 0 || ToKhaiMauDichCol.Count > 0)
                        {
                            BKCUCol = BKCU.XuatBKCungUngDangKy(DieuKienBKCU);
                            foreach (BKCungUngDangKy BKCULoad in BKCUCol)
                            {
                                BKCULoad.LoadSanPhamCungUngCollection();
                                foreach (SanPhanCungUng SPCULoad in BKCULoad.SanPhamCungUngCollection)
                                {
                                    SPCULoad.LoadNPLCungUngCollection();
                                }
                            }

                            if (BKCUCol.Count > 0)
                            {
                                serializer = new XmlSerializer(typeof(BKCungUngDangKyCollection));
                                FileStream fsBKCU = new FileStream(Folder + "\\BangKeCungUng.XML", FileMode.Create);
                                serializer.Serialize(fsBKCU, BKCUCol);
                                fsBKCU.Close();

                                msg += "\n\t " + BKCUCol.Count + " bảng kê cung ứng";
                            }
                            else msg += "\n\t không có BKCU cần xuất";
                        }
                    }

                    ShowMessage(msg, false);
                }
                else ShowMessage("Không có dữ liệu nào được xuất", false);
            }
            catch
            {
                ShowMessage("Có lỗi hệ thống trong khi xuất ra file.\nVui lòng đóng cửa sổ này và thực hiện lại !",false);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnChon_Click(object sender, EventArgs e)
        {
            if (FolderBrowser.ShowDialog() == DialogResult.OK)
                ThuMucLuu = FolderBrowser.SelectedPath;
            this.txtThuMucLuu.Text = ThuMucLuu;
        }
       
        private void chkXuatTheoHopDong_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkTKMD_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkTKMD.Checked || !chkTKGCCT.Checked)
                chkBKCU.Checked = false;
            else chkBKCU.Checked = true;
        }
    }
}