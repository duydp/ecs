﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using Company.Interface.GC;
using SOFTECH.ECS.TQDT.GC.KDT.GC;
namespace Company.Interface.KDT.GC
{
    public partial class CungUngSelectTKForm : BaseForm
    {
        public KDT_GC_CungUngDangKy CungUngDangky = new KDT_GC_CungUngDangKy();
        public KDT_GC_CungUng CungUng = new KDT_GC_CungUng();
        public long IDHopDong;

        public HopDong HD = new HopDong();
        public CungUngSelectTKForm()
        {
            InitializeComponent();
        }

      
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void CungUngManagerForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            DataSet ds = KDT_GC_CungUng.SelectTKXTheoHD(IDHopDong,CungUng.MaNPL);
            dgList.DataSource = ds.Tables[0];
            dgList.Refetch();
        }
       

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            // if (e.Row.RowType == RowType.Record)
            //{
            //    string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
            //     switch(trangthai)
            //     {
            //         case"-1":
            //             e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
            //             break;
            //         case "0":
            //             e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
            //             break;
            //         case "1":
            //             e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
            //             break;
            //         case "21":
            //             e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
            //             break;
            //     }
            // }
        }

        private void btnChon_Click(object sender, EventArgs e)
        {
            KDT_GC_CungUng_Detail CU_Detail = new KDT_GC_CungUng_Detail();
             Janus.Windows.GridEX.GridEXRow[] listChecked = dgList.GetCheckedRows();
             for (int i = 0; i < listChecked.Length; i++)
             {
                 CU_Detail = new KDT_GC_CungUng_Detail();
                 Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                 bool trungTK = false;
                 foreach (KDT_GC_CungUng_Detail cungungTK in CungUng.CungUngDetail_List)
                 {

                     if (cungungTK.SoToKhaiVNACCS.Trim() == item.Cells["SoToKhaiVNACCS"].Text.Trim() && cungungTK.MaLoaiHinh.Trim() == item.Cells["MaLoaiHinh"].Text.Trim())
                         {
                             if (cungungTK.MaSanPham.Trim() == item.Cells["MaSanPham"].Text.Trim())
                             {
                                 trungTK = true;
                             }
                         }
                    
                     
                 }
                 if (trungTK)
                     continue;
                 CU_Detail.SoToKhaiXuat = item.Cells["SoToKhai"].Text;
                 CU_Detail.SoToKhaiVNACCS = item.Cells["SoToKhaiVNACCS"].Text;
                 CU_Detail.MaLoaiHinh = item.Cells["MaLoaiHinh"].Text;
                 CU_Detail.MaHaiQuan = item.Cells["MaHaiQuan"].Text;
                 CU_Detail.NgayDangKy =Convert.ToDateTime(item.Cells["NgayDangKy"].Text);
                 CU_Detail.MaSanPham = item.Cells["MaSanPham"].Text;
                 if (!string.IsNullOrEmpty(CU_Detail.MaSanPham.Trim()))
                 {
                     Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                     sp.Ma = CU_Detail.MaSanPham;
                     sp.HopDong_ID = IDHopDong;
                     sp.Load();
                     CU_Detail.TenSanPham = sp.Ten;
                     CU_Detail.DVT_ID = VNACCS_Mapper.GetCodeVNACC(sp.DVT_ID.Trim());
                 }
                 CU_Detail.SoLuong = Convert.ToDecimal(item.Cells["SoLuong"].Text);
                 KDT_GC_CungUng_ChungTu chungtu = new KDT_GC_CungUng_ChungTu();
                 Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                 dm.HopDong_ID = IDHopDong;
                 dm.MaSanPham = CU_Detail.MaSanPham;
                 dm.MaNguyenPhuLieu = CungUng.MaNPL;
                 dm.Load();
                 chungtu.SoLuong =  CU_Detail.SoLuong * (dm.DinhMucSuDung + (dm.DinhMucSuDung * dm.TyLeHaoHut / 100));
                 chungtu.SoChungTu = "";
                 chungtu.LoaiChungTu =1;
                 CU_Detail.CungUngCT_List.Add(chungtu);
                 CungUng.CungUngDetail_List.Add(CU_Detail);
             }
             this.Close();
        }

     
        
    }
}
