﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class SXXK_MaterialSupply
    {
        [XmlElement("DeclarationDocument")]
        public List<DeclarationDocument> DeclarationDocument { get; set; }

    }
}
