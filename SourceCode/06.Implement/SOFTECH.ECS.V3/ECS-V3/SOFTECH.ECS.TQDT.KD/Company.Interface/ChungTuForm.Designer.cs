﻿using System.ComponentModel;
using System.Windows.Forms;
using Company.Interface.Controls;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;

namespace Company.Interface
{
    partial class ChungTuForm
    {
        private UIGroupBox uiGroupBox1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label27;
        private UIGroupBox uiGroupBox2;
        private UIButton btnAddNew;
        private ToolTip toolTip1;
        private ErrorProvider epError;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChungTuForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiComboBox1 = new Janus.Windows.EditControls.UIComboBox();
            this.txtFileUpload = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoBanPhu = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoBanChinh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAddNew = new Janus.Windows.EditControls.UIButton();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Size = new System.Drawing.Size(348, 179);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.btnAddNew);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(348, 179);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnClose.Location = new System.Drawing.Point(261, 151);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 182;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.uiComboBox1);
            this.uiGroupBox2.Controls.Add(this.txtFileUpload);
            this.uiGroupBox2.Controls.Add(this.txtSoBanPhu);
            this.uiGroupBox2.Controls.Add(this.txtSoBanChinh);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 8);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(324, 137);
            this.uiGroupBox2.TabIndex = 173;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // uiComboBox1
            // 
            this.uiComboBox1.Location = new System.Drawing.Point(97, 23);
            this.uiComboBox1.Name = "uiComboBox1";
            this.uiComboBox1.Size = new System.Drawing.Size(208, 21);
            this.uiComboBox1.TabIndex = 177;
            this.uiComboBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtFileUpload
            // 
            this.txtFileUpload.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtFileUpload.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFileUpload.Location = new System.Drawing.Point(97, 104);
            this.txtFileUpload.Name = "txtFileUpload";
            this.txtFileUpload.ReadOnly = true;
            this.txtFileUpload.Size = new System.Drawing.Size(208, 21);
            this.txtFileUpload.TabIndex = 176;
            this.txtFileUpload.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtFileUpload.VisualStyleManager = this.vsmMain;
            this.txtFileUpload.ButtonClick += new System.EventHandler(this.txtFileUpload_ButtonClick);
            // 
            // txtSoBanPhu
            // 
            this.txtSoBanPhu.DecimalDigits = 0;
            this.txtSoBanPhu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoBanPhu.Location = new System.Drawing.Point(97, 77);
            this.txtSoBanPhu.Name = "txtSoBanPhu";
            this.txtSoBanPhu.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoBanPhu.Size = new System.Drawing.Size(208, 21);
            this.txtSoBanPhu.TabIndex = 175;
            this.txtSoBanPhu.Text = "0";
            this.txtSoBanPhu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoBanPhu.Value = ((uint)(0u));
            this.txtSoBanPhu.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt32;
            this.txtSoBanPhu.VisualStyleManager = this.vsmMain;
            // 
            // txtSoBanChinh
            // 
            this.txtSoBanChinh.DecimalDigits = 0;
            this.txtSoBanChinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoBanChinh.Location = new System.Drawing.Point(97, 50);
            this.txtSoBanChinh.Name = "txtSoBanChinh";
            this.txtSoBanChinh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoBanChinh.Size = new System.Drawing.Size(208, 21);
            this.txtSoBanChinh.TabIndex = 174;
            this.txtSoBanChinh.Text = "0";
            this.txtSoBanChinh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoBanChinh.Value = ((uint)(0u));
            this.txtSoBanChinh.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt32;
            this.txtSoBanChinh.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 169;
            this.label4.Text = "Số bản sao";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 171;
            this.label2.Text = "Số bản chính";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(8, 28);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(72, 13);
            this.label27.TabIndex = 159;
            this.label27.Text = "Tên chứng từ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 172;
            this.label3.Text = "File đính kèm";
            // 
            // btnAddNew
            // 
            this.btnAddNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNew.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNew.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAddNew.Icon")));
            this.btnAddNew.Location = new System.Drawing.Point(175, 151);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(75, 23);
            this.btnAddNew.TabIndex = 0;
            this.btnAddNew.Text = "Ghi";
            this.btnAddNew.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnAddNew.VisualStyleManager = this.vsmMain;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.lvsError.SetErrorCaption(this.cvError, "Thông báo");
            this.lvsError.SetErrorMessage(this.cvError, "Có một số lỗi sau:");
            this.cvError.HostingForm = this;
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.uiComboBox1;
            this.rfvMa.ErrorMessage = "\"Tên chứng từ\" không được để trống.";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // ChungTuForm
            // 
            this.AcceptButton = this.btnAddNew;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(348, 179);
            this.ControlBox = false;
            this.Controls.Add(this.uiGroupBox1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChungTuForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thông tin chứng từ";
            this.Load += new System.EventHandler(this.ChungTuForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion        

        private ImageList ImageList1;
        private UIButton btnClose;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private NumericEditBox txtSoBanPhu;
        private NumericEditBox txtSoBanChinh;
        private EditBox txtFileUpload;
        private OpenFileDialog openFileDialog1;
        private UIComboBox uiComboBox1;
    }
}
