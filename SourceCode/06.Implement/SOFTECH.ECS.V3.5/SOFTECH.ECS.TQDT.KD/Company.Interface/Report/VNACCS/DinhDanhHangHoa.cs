using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.Report.VNACCS
{
    public partial class DinhDanhHangHoa : DevExpress.XtraReports.UI.XtraReport
    {
        public KDT_VNACCS_CapSoDinhDanh capSoDinhDanh;
        public DinhDanhHangHoa()
        {
            InitializeComponent();
        }

        private void GeneralQrCode()
        {
            try
            {
                Zen.Barcode.CodeQrBarcodeDraw qrCode = Zen.Barcode.BarcodeDrawFactory.CodeQr;
                picQrCode.Image = qrCode.Draw(capSoDinhDanh.SoDinhDanh, 50);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        public void BindReport()
        {
            try
            {
                GeneralQrCode();
                lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
                lblMaDoanhNghiep.Text = capSoDinhDanh.MaDoanhNghiep;
                string loaiHangHoa = capSoDinhDanh.LoaiTTHH.ToString();
                switch (loaiHangHoa)
                {
                    case "1":
                        lblLoaiHangHoa.Text = "Nhập khẩu";
                        break;
                    case "2":
                        lblLoaiHangHoa.Text = "Xuất khẩu";
                        break;
                    case "3":
                        lblLoaiHangHoa.Text = "Hàng nội địa";
                        break;
                    default:
                        break;
                }
                //lblLoaiHangHoa.Text = capSoDinhDanh.LoaiTTHH.ToString();
                string loaiDoiTuong = capSoDinhDanh.LoaiDoiTuong.ToString();
                switch (loaiDoiTuong)
                {
                    case "1":
                        lblDoiTuong.Text = "Doanh nghiệp xuất nhập khẩu";
                        break;
                    case "2":
                        lblDoiTuong.Text = "Doanh nghiệp kho, bãi, cảng";
                        break;
                    default:
                        break;
                }
                //lblDoiTuong.Text = capSoDinhDanh.LoaiDoiTuong.ToString();
                lblSoDinhDanh.Text = capSoDinhDanh.SoDinhDanh.ToString();
                lblNgayCap.Text = capSoDinhDanh.NgayCap.ToString("yyyy-MM-dd HH:mm:ss");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
        }
    }
}
