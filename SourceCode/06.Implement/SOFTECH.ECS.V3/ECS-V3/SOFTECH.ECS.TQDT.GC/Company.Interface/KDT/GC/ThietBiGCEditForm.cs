﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;

namespace Company.Interface.KDT.GC
{
    public partial class ThietBiGCEditForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public ThietBi tbDetail = new ThietBi();
        public bool isBrower = false;
        public ThietBiGCEditForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void ThietBiGCEditForm_Load(object sender, EventArgs e)
        {
            txtSoLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            txtDonGia.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            txtTriGia.DecimalDigits = GlobalSettings.TriGiaNT;

            if (!isBrower)
            {
                txtMa.Focus();
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                cbDonViTinh.DataSource = this._DonViTinh;
                cbDonViTinh.DisplayMember = "Ten";
                cbDonViTinh.ValueMember = "ID";
                if (!string.IsNullOrEmpty(tbDetail.Ma))
                {
                    txtMa.Text = tbDetail.Ma.Trim();
                    txtTen.Text = tbDetail.Ten.Trim();
                    txtMaHS.Text = tbDetail.MaHS.Trim();
                    txtGhiChu.Text = tbDetail.GhiChu;
                    cbDonViTinh.SelectedValue = tbDetail.DVT_ID;
                    if (tbDetail.NuocXX_ID.Trim().Length != 0)
                    ctrNuoc.Ma = tbDetail.NuocXX_ID;
                    txtSoLuong.Text = (tbDetail.SoLuongDangKy.ToString());
                    txtDonGia.Text = tbDetail.DonGia.ToString();
                    txtTriGia.Text = tbDetail.TriGia.ToString();
                     ctrNguyenTe.Ma = tbDetail.NguyenTe_ID;
                }
                else
                {
                     ctrNuoc.Ma = GlobalSettings.NUOC;
                    cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
                    ctrNguyenTe.Ma = GlobalSettings.NGUYEN_TE_MAC_DINH;
                }                
                chTinhTrang.Checked = tbDetail.TinhTrang == "1"; ;
                
                if (this.OpenType == OpenFormType.View)
                {
                    btnAdd.Enabled = false;
                    btnXoa.Enabled = false;
                    dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                }
                
            }
            else
            {
                uiGroupBox2.Visible = false;
                HD.LoadCollection();
                dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                this.Width = dgThietBi.Width;
                this.Height = dgThietBi.Height;
            }
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            dgThietBi.DataSource = HD.TBCollection;

        }
        private void reset()
        {
            txtMa.Text = "";
            txtTen.Text = "";
            txtMaHS.Text = "";
            txtSoLuong.Text = "";
            txtDonGia.Text = "0";
            txtTriGia.Text = "0";
            chTinhTrang.Checked = false;
            error.SetError(txtMaHS, null);
            error.SetError(txtMa, null);
            error.SetError(txtTen, null);


            try
            {
                dgThietBi.Refetch();
            }
            catch
            {
                dgThietBi.Refresh();
            }
            tbDetail = new ThietBi();


        }
        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                error.SetIconPadding(txtMaHS, -8);
                error.SetError(txtMaHS, setText("Mã số HS không hợp lệ.", "This value is invalid"));
                txtMaHS.Focus();
            }
            else
            {
                error.SetError(txtMaHS, null);
            }
        }
        private void setErro()
        {
            error.Clear();
            error.SetError(txtTen, null);
            error.SetError(txtMa, null);
            error.SetError(txtMaHS, null);

        }
        private void btnAdd_Click(object sender, EventArgs e)
        {

            txtTriGia_Leave(null, null);
            txtMa.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;
            {
                //if (!MaHS.Validate(txtMaHS.Text, 10))
                //{
                //    error.SetIconPadding(txtMaHS, -8);
                //    error.SetError(txtMaHS, setText("Mã số HS không hợp lệ.", "This value is invalid"));
                //    txtMaHS.Focus();
                //    return;
                //}

                checkExitsThietBiAndSTTHang();
            }
        }
        private void checkExitsThietBiAndSTTHang()
        {
            HD.TBCollection.Remove(tbDetail);

            foreach (ThietBi tb in HD.TBCollection)
            {
                if (tb.Ma.Trim() == txtMa.Text.Trim())
                {
                    showMsg("MSG_0203003");
                    //string st = MLMessages("Đã có mã thiết bị này trong danh sách?","MSG_WRN06","", false);                                       
                    if (tbDetail.Ma != "")
                        HD.TBCollection.Add(tbDetail);
                    return;
                }
            }
            tbDetail.Ma = txtMa.Text.Trim();
            tbDetail.Ten = txtTen.Text.Trim();
            tbDetail.MaHS = txtMaHS.Text.Trim();
            tbDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            tbDetail.NuocXX_ID = ctrNuoc.Ma;
            tbDetail.SoLuongDangKy = Convert.ToDecimal(txtSoLuong.Text);
            tbDetail.DonGia = Convert.ToDouble(txtDonGia.Text);
            tbDetail.TriGia = Convert.ToDouble(txtTriGia.Text);
            tbDetail.NguyenTe_ID = ctrNguyenTe.Ma;
            tbDetail.TinhTrang = chTinhTrang.Checked ? "1" : "0";
            tbDetail.GhiChu = txtGhiChu.Text;
            tbDetail.HopDong_ID = HD.ID;
            HD.TBCollection.Add(tbDetail);
            reset();
            this.setErro();
        }
        private void dgThietBi_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {

            tbDetail = (ThietBi)e.Row.DataRow;
            tbDetail.HopDong_ID = HD.ID;
            if (!isBrower)
            {
                txtMa.Text = tbDetail.Ma;
                txtTen.Text = tbDetail.Ten;
                txtMaHS.Text = tbDetail.MaHS;
                cbDonViTinh.SelectedValue = tbDetail.DVT_ID;
                ctrNuoc.Ma = tbDetail.NuocXX_ID;
                txtSoLuong.Text = tbDetail.SoLuongDangKy.ToString();
                txtDonGia.Text = tbDetail.DonGia.ToString();
                txtTriGia.Text = tbDetail.TriGia.ToString();
                ctrNguyenTe.Ma = tbDetail.NguyenTe_ID;
                chTinhTrang.Checked = tbDetail.TinhTrang == "1"; ;
                txtGhiChu.Text = tbDetail.GhiChu;
            }
            else
            {
                this.Close();
            }
        }



        private void dgThietBi_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            e.Row.Cells["TenNguyenTe"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe_ID"].Value).Trim();
            e.Row.Cells["XuatXu"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value).Trim();
        }

        private void ctrNguyenTe_Load(object sender, EventArgs e)
        {

        }

        private void txtTriGia_Leave(object sender, EventArgs e)
        {
            decimal soluong = Convert.ToDecimal(txtSoLuong.Text);
            decimal dongia = Convert.ToDecimal(txtDonGia.Text);
            decimal trigia = soluong * dongia;
            txtTriGia.Text = trigia.ToString();
        }

        private void dgThietBi_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {

            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgThietBi.SelectedItems;
                    foreach (GridEXSelectedItem row in items)
                    {
                        Company.GC.BLL.KDT.GC.ThietBi tbDelete = (Company.GC.BLL.KDT.GC.ThietBi)row.GetRow().DataRow;
                        tbDelete.Delete();

                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        //0112
        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<ThietBi> tbColl = new List<ThietBi>();
            if (HD.TBCollection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgThietBi.SelectedItems;
            if (items.Count <= 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            {
                foreach (GridEXSelectedItem row in items)
                {
                    Company.GC.BLL.KDT.GC.ThietBi tbDelete = (Company.GC.BLL.KDT.GC.ThietBi)row.GetRow().DataRow;
                    if (HD.ID > 0)
                    {
                        try { tbDelete.Delete(); }
                        catch { }
                    }

                    tbColl.Add(tbDelete);
                }
                foreach (Company.GC.BLL.KDT.GC.ThietBi tbt in tbColl)
                {
                    HD.TBCollection.Remove(tbt);
                }
                dgThietBi.DataSource = HD.TBCollection;
                this.setErro();
                try { dgThietBi.Refetch(); }
                catch { dgThietBi.Refresh(); }

            }

        }
    }
}