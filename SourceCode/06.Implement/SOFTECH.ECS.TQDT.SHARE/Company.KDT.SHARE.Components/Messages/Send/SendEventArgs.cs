﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Messages.Send
{
    public class SendEventArgs : EventArgs
    {
        public Exception Error { get; private set; }
        public TimeSpan TotalTime { get; private set; }
        public string FeedBackMessage { get; set; }

        public SendEventArgs(string feedBackMessage, TimeSpan totalTime, Exception ex)
        {
            this.FeedBackMessage = feedBackMessage;
            this.TotalTime = totalTime;
            this.Error = ex;
        }
    }
}
