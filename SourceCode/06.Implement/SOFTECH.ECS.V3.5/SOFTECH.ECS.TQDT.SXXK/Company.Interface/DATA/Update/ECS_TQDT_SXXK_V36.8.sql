

UPDATE dbo.t_KDT_VNACC_TK_SoVanDon SET SoVanDonChu = SoVanDon WHERE SoTT >=1
UPDATE dbo.t_KDT_VNACC_TK_SoVanDon SET SoVanDon='' WHERE SoVanDonChu IS NOT NULL

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'LoaiChungTu'
          AND Object_ID = Object_ID(N'schemaName.t_KDT_VNACCS_AdditionalDocument'))
BEGIN
	ALTER TABLE t_KDT_VNACCS_AdditionalDocument
	ADD LoaiChungTu INT NULL
END

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(200),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max),
	@LoaiChungTu int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_AdditionalDocument]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr],
	[LoaiChungTu]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@MaDoanhNghiep,
	@TKMD_ID,
	@GhiChuKhac,
	@FileName,
	@Content,
	@GuidStr,
	@LoaiChungTu
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(200),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max),
	@LoaiChungTu int
AS

UPDATE
	[dbo].[t_KDT_VNACCS_AdditionalDocument]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[GhiChuKhac] = @GhiChuKhac,
	[FileName] = @FileName,
	[Content] = @Content,
	[GuidStr] = @GuidStr,
	[LoaiChungTu] = @LoaiChungTu
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TKMD_ID bigint,
	@GhiChuKhac nvarchar(200),
	@FileName nvarchar(255),
	@Content nvarchar(max),
	@GuidStr varchar(max),
	@LoaiChungTu int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_AdditionalDocument] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_AdditionalDocument] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[GhiChuKhac] = @GhiChuKhac,
			[FileName] = @FileName,
			[Content] = @Content,
			[GuidStr] = @GuidStr,
			[LoaiChungTu] = @LoaiChungTu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_AdditionalDocument]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[MaDoanhNghiep],
			[TKMD_ID],
			[GhiChuKhac],
			[FileName],
			[Content],
			[GuidStr],
			[LoaiChungTu]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@MaDoanhNghiep,
			@TKMD_ID,
			@GhiChuKhac,
			@FileName,
			@Content,
			@GuidStr,
			@LoaiChungTu
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_AdditionalDocument]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_AdditionalDocument] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr],
	[LoaiChungTu]
FROM
	[dbo].[t_KDT_VNACCS_AdditionalDocument]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr],
	[LoaiChungTu]
FROM [dbo].[t_KDT_VNACCS_AdditionalDocument] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_SelectAll]












AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GhiChuKhac],
	[FileName],
	[Content],
	[GuidStr],
	[LoaiChungTu]
FROM
	[dbo].[t_KDT_VNACCS_AdditionalDocument]	

GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '36.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('36.8',GETDATE(), N'CẬP NHẬT MÃ ĐỊNH DANH HÀNG HÓA TT MỚI')
END