﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class NoiDungChinhSuaTKDetailForm : BaseForm
    {
        public ToKhaiMauDich TKMD;
        public NoiDungDieuChinhTKDetail noiDungDieuChinhTKDetail = new NoiDungDieuChinhTKDetail();
        public List<NoiDungDieuChinhTKDetail> ListNoiDungDieuChinhTKDetail = new List<NoiDungDieuChinhTKDetail>();
        public List<NoiDungDieuChinhTK> ListNoiDungDieuChinhTK = new List<NoiDungDieuChinhTK>();
        public NoiDungDieuChinhTK noiDungDieuChinhTK = new NoiDungDieuChinhTK();
        public bool isKhaiBoSung = false;
        public static int i;
        public bool isAddNew = true;
        public NoiDungChinhSuaTKDetailForm()
        {
            InitializeComponent();
        }

        private void BindData()
        {
            ListNoiDungDieuChinhTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(noiDungDieuChinhTK.ID);
            dgList.DataSource = ListNoiDungDieuChinhTKDetail;

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void NoiDungChinhSuaTKDetailForm_Load(object sender, EventArgs e)
        {
            BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            NoiDungChinhSuaTKForm ndDieuChinhTKForm = new NoiDungChinhSuaTKForm();
            ndDieuChinhTKForm.BindData();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (TKMD.ID <= 0)
            {
                ShowMessage("Vui lòng nhập tờ khai trước", false);
                return;
            }

            if (ListNoiDungDieuChinhTKDetail.Count == 0)
            {
                ShowMessage("Chưa nhập vào nội dung điều chỉnh", false);
                btnAddNew.Focus();
                return;
            }

            try
            {
                if (noiDungDieuChinhTK.ID == 0)
                {
                    noiDungDieuChinhTK.TKMD_ID = this.TKMD.ID;
                    noiDungDieuChinhTK.SoTK = this.TKMD.SoToKhai;
                    noiDungDieuChinhTK.NgayDK = this.TKMD.NgayDangKy;
                    noiDungDieuChinhTK.MaLoaiHinh = this.TKMD.MaLoaiHinh;
                    noiDungDieuChinhTK.NgaySua = DateTime.Now;
                    noiDungDieuChinhTK.SoDieuChinh = noiDungDieuChinhTK.SoDieuChinh;
                    noiDungDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                    noiDungDieuChinhTK.InsertUpdate();
                    
                    foreach (NoiDungDieuChinhTKDetail nddetail in ListNoiDungDieuChinhTKDetail)
                    {
                        nddetail.Id_DieuChinh = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                    }
                    noiDungDieuChinhTK.InsertUpdateFull(ListNoiDungDieuChinhTKDetail);
                    if (isAddNew == true)
                    {
                        noiDungDieuChinhTK.ID = NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                        TKMD.NoiDungDieuChinhTKCollection.Add(noiDungDieuChinhTK);
                    }
                    BindData();
                    ShowMessage("Lưu thành công", false);
                }
                else
                {
                    foreach (NoiDungDieuChinhTKDetail nddetail in ListNoiDungDieuChinhTKDetail)
                    {
                        nddetail.Id_DieuChinh = noiDungDieuChinhTK.ID;
                    }
                    noiDungDieuChinhTK.InsertUpdateExistIDDieuChinh(ListNoiDungDieuChinhTKDetail, noiDungDieuChinhTK.ID);
                    BindData();
                    ShowMessage("Lưu thành công", false);
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
                return;
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            
        }

        private void NoiDungChinhSuaTKDetailForm_Load_1(object sender, EventArgs e)
        {
            txtChiCucHQMoTK.Text = GlobalSettings.TEN_HAI_QUAN;
            txtChiCucHQMoTK.ReadOnly = true;
            txtSoTK.Text = TKMD.SoToKhai.ToString();
            txtSoTK.ReadOnly = true;
            txtNgayMoTK.Text = TKMD.NgayTiepNhan.ToShortDateString();
            txtNgayMoTK.ReadOnly = true;
            txtSoDieuChinh.Text = noiDungDieuChinhTK.SoDieuChinh.ToString();
            txtSoDieuChinh.ReadOnly = true;

            //i = NoiDungDieuChinhTK.SelectMaxSoDieuChinh(TKMD.ID);
            if (noiDungDieuChinhTK.ID > 0)
            {
                ListNoiDungDieuChinhTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(noiDungDieuChinhTK.ID);
                if (noiDungDieuChinhTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET || noiDungDieuChinhTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                {
                    btnAddNew.Enabled = false;
                    btnDelete.Enabled = false;
                    btnGhi.Enabled = false;
                }
            }
            BindData();
        }

        private void btnAddNew_Click_1(object sender, EventArgs e)
        {
            NoiDungDieuChinhTKDetail nddcDetail = new NoiDungDieuChinhTKDetail();
            if (txtNoiDungChinhTK.Text.Equals(""))
            {
                ShowMessage("Nội dung chính tờ khai không được để trống", false);
                return;
            }
            else if (txtNoiDungTKSua.Text.Equals(""))
            {
                ShowMessage("Nội dung sửa đổi bổ sung không được để trống", false);
                return;
            }
            else
            {
                nddcDetail.NoiDungTKChinh = txtNoiDungChinhTK.Text;
                nddcDetail.NoiDungTKSua = txtNoiDungTKSua.Text;
                ListNoiDungDieuChinhTKDetail.Add(nddcDetail);
                dgList.DataSource = ListNoiDungDieuChinhTKDetail;
                txtNoiDungChinhTK.Text = "";
                txtNoiDungTKSua.Text = "";
                try
                {
                    dgList.Refetch();
                }
                catch (Exception ex)
                {
                    dgList.Refresh();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            List<NoiDungDieuChinhTKDetail> NoiDungDieuChinhTKDetailCollection = new List<NoiDungDieuChinhTKDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa nội dung này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NoiDungDieuChinhTKDetail nddctkDetail = new NoiDungDieuChinhTKDetail();
                        nddctkDetail = (NoiDungDieuChinhTKDetail)i.GetRow().DataRow;
                        if (nddctkDetail == null) continue;

                        if (nddctkDetail.ID > 0)
                            nddctkDetail.Delete();

                        ListNoiDungDieuChinhTKDetail.Remove(nddctkDetail);
                    }
                }
            }
            BindData();
        }
    }
}

