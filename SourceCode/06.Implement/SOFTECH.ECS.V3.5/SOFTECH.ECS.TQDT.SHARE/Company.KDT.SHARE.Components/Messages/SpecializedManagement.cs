﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class SpecializedManagement
    {

        [XmlElement("type")]
        public string Type { set; get; }

        [XmlElement("identification")]
        public string Identification { set; get; }

        [XmlElement("quantity")]
        public string Quantity { set; get; }

        [XmlElement("measureUnit")]
        public string MeasureUnit { set; get; }

        [XmlElement("unitPrice")]
        public string UnitPrice { set; get; }

    }
}
