using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using System.Xml.Serialization;
using Company.KDT.SHARE.VNACCS;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using Company.Interface.VNACCS;
namespace Company.Interface.GC
{
    public partial class PhieuNhapKhoForm : BaseForm
    {
        public KDT_GC_PhieuNhapKho PNK = new KDT_GC_PhieuNhapKho();
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        public List<KDT_VNACC_HangMauDich> ListHang = new List<KDT_VNACC_HangMauDich>();
        public ToKhaiMauDich TKMD_V4 = new ToKhaiMauDich();
        public ToKhaiChuyenTiep TKCT_V4 = new ToKhaiChuyenTiep();
        public List<HangMauDich> ListHang_V4 = new List<HangMauDich>();
        public KDT_GC_PhieuNhapKho_Hang hang = new KDT_GC_PhieuNhapKho_Hang();
        DataTable dt = new DataTable();
        public HopDong HopDong = new HopDong();
        public bool isV4 = false;
        public bool isV4_TKCT = false;

        public PhieuNhapKhoForm()
        {
            InitializeComponent();
        }
        private void GetPhieuNhapKho()
        {
            PNK.NgayNhapKho = clcNgayNhapKho.Value;
            PNK.SoPhieu = Convert.ToDecimal(txtSoPhieu.Text);
            PNK.KhachHang = txtKhachHang.Text;
            PNK.NhapTaiKho = txtNhapTaiKho.Text;
            PNK.NgoaiTe = ctrNgoaiTe.Code;
            PNK.TyGia = Convert.ToDecimal(txtTyGia.Text);
            PNK.LyDo = txtLyDo.Text;
            PNK.NhaCungCap = txtNhaCungCap.Text;
            PNK.SoHopDong = txtSoHopDong.Text;
            PNK.ToKhai = Convert.ToDecimal(txtSoToKhai.Text);
            PNK.SoInvoice = txtSoInvoice.Text;
            PNK.NgayInvoice = Convert.ToDateTime(clcNgayInvoice.Value);
            PNK.SoBL = txtSoBL.Text;
            PNK.SoKien = Convert.ToDecimal(txtSoKien.Text);
            PNK.TongVAT = Convert.ToDecimal(txtTongVAT.Text);
            //GetHang();
        }
        private void SetPhieuNhapKho()
        {
            errorProvider.Clear();
            clcNgayNhapKho.Value = Convert.ToDateTime(PNK.NgayNhapKho);
            txtSoPhieu.Text = Convert.ToString(PNK.SoPhieu);
            txtKhachHang.Text = PNK.KhachHang;
            txtNhapTaiKho.Text = PNK.NhapTaiKho;
            txtSoToKhai.Text = Convert.ToString(PNK.ToKhai);
            txtSoHopDong.Text = Convert.ToString(PNK.SoHopDong);
            ctrNgoaiTe.Code = PNK.NgoaiTe;
            txtTyGia.Text = Convert.ToString(PNK.TyGia);
            txtLyDo.Text = PNK.LyDo;
            txtNhaCungCap.Text = PNK.NhaCungCap;
            txtSoInvoice.Text = PNK.SoInvoice;
            clcNgayInvoice.Value = Convert.ToDateTime(PNK.NgayInvoice);
            txtSoBL.Text = PNK.SoBL;
            txtSoKien.Text = Convert.ToString(PNK.SoKien);
            txtTongVAT.Text = Convert.ToString(PNK.TongVAT);
        }
        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void PhieuNhapKhoForm_Load(object sender, EventArgs e)
        {
            if (isV4)
            {
                if (TKMD_V4.SoToKhai != 0)
                {
                    //this.SetIDControl();
                    if (PNK == null || PNK.ID == 0)
                    {
                        PNK = new KDT_GC_PhieuNhapKho();
                        txtKhachHang.Text = TKMD_V4.TenDonViDoiTac;
                        HopDong hd = HopDong.Load(TKMD_V4.IDHopDong);
                        txtSoHopDong.Text = hd != null ? hd.SoHopDong : string.Empty;
                        txtSoToKhai.Text = Convert.ToString(TKMD_V4.SoToKhai);
                        txtSoInvoice.Text = TKMD_V4.SoHoaDonThuongMai;
                        txtSoBL.Text = TKMD_V4.LoaiVanDon;
                        clcNgayInvoice.Value = TKMD_V4.NgayHoaDonThuongMai;
                        txtSoKien.Text = Convert.ToString(TKMD_V4.SoLuong);
                        ctrNgoaiTe.Code = TKMD_V4.NguyenTe_ID;
                        txtTyGia.Text = TKMD_V4.TyGiaTinhThue.ToString();//Convert.ToString(Company.KDT.SHARE.Components.Globals.GetNguyenTe(ctrNgoaiTe.Code));
                        //set ngày nhập kho = ngày đăng ký trừ 3 ngày theo VINATEX
                        if (TKMD_V4.NgayDangKy.AddDays(+5).DayOfWeek == DayOfWeek.Sunday)
                            clcNgayNhapKho.Value = TKMD_V4.NgayDangKy.AddDays(+6);
                        else
                            clcNgayNhapKho.Value = TKMD_V4.NgayDangKy.AddDays(+5);

                        TKMD_V4.HMDCollection = (List<HangMauDich>)HangMauDich.SelectCollectionBy_TKMD_ID(TKMD_V4.ID);
                        foreach (HangMauDich HMD in TKMD_V4.HMDCollection)
                        {
                            KDT_GC_PhieuNhapKho_Hang hang = new KDT_GC_PhieuNhapKho_Hang();
                            hang.Ma2 = HMD.MaPhu;
                            hang.TenHang = HMD.TenHang;
                            //hang.MaSoHang = HMD.MaSoHang;
                            hang.SoLuong1 = HMD.SoLuong;
                            hang.DVTLuong1 = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(HMD.DVT_ID);
                            try
                            {
                                hang.MaTTDonGia = TKMD_V4.NguyenTe_ID;
                            }
                            catch (Exception)
                            {
                                
                                //throw;
                            }
                            
                            //hang.DonGiaHoaDon = HMD.DonGiaHoaDon;
                            //hang.TriGiaHoaDon = HMD.TriGiaHoaDon;
                            decimal tygia = Convert.ToDecimal(txtTyGia.Text);
                            decimal soluong = Convert.ToDecimal(hang.SoLuong1);
                            if (hang.MaTTDonGia == "VND")
                            {
                                hang.DonGiaHoaDon = HMD.DonGiaKB;
                                hang.TriGiaHoaDon = HMD.TriGiaKB;
                                hang.DonGiaNgoaiTe = hang.DonGiaHoaDon / tygia;
                                hang.ThanhTienNgoaiTe = hang.DonGiaNgoaiTe * soluong;
                            }
                            else
                            {
                                hang.DonGiaNgoaiTe = HMD.DonGiaKB;
                                hang.ThanhTienNgoaiTe = HMD.TriGiaKB;
                                hang.DonGiaHoaDon = hang.DonGiaNgoaiTe * tygia;
                                hang.TriGiaHoaDon = hang.DonGiaHoaDon * soluong;
                            }
                            PNK.HangNhapCollection.Add(hang);
                        }
                        dgList.DataSource = PNK.HangNhapCollection;
                        dgList.Refetch();
                        txtSoPhieu.Text = (KDT_GC_PhieuNhapKho.GetSoPhieuMax(TKMD_V4.IDHopDong) + 1).ToString();
                    }
                    else
                    {
                        SetPhieuNhapKho();
                        dgList.DataSource = PNK.HangNhapCollection;
                    }
                    if (PNK.HangNhapCollection.Count > 0)
                    {
                        btnIn.Enabled = true;
                    }
                }
                else
                {
                    if (PNK == null || PNK.ID == 0)
                    {
                        PNK = new KDT_GC_PhieuNhapKho();
                        txtKhachHang.Text = TKMD_V4.TenDonViDoiTac;
                        txtSoHopDong.Text = HopDong != null ? HopDong.SoHopDong : string.Empty;
                        txtSoToKhai.Text = Convert.ToString(TKMD_V4.SoToKhai);
                        txtSoInvoice.Text = "0";//TKMD.SoHoaDon;
                        clcNgayInvoice.Value = DateTime.Now.Date;//TKMD.NgayPhatHanhHD;
                        txtSoKien.Text = "0";//Convert.ToString(TKMD.SoLuong);
                        ctrNgoaiTe.Code = "";//TKMD.MaTTHoaDon;
                        txtTyGia.Text = TKMD_V4.TyGiaTinhThue.ToString();//Convert.ToString(Company.KDT.SHARE.Components.Globals.GetNguyenTe(ctrNgoaiTe.Code));
                        //set ngày nhập kho = ngày đăng ký trừ 3 ngày theo VINATEX
                        clcNgayNhapKho.Value = DateTime.Now;
                        dgList.Refetch();
                        txtSoPhieu.Text = "0";//(KDT_GC_PhieuNhapKho.GetSoPhieuMax(TKMD.HopDong_ID) + 1).ToString();
                    }
                    else
                    {
                        SetPhieuNhapKho();
                        dgList.DataSource = PNK.HangNhapCollection;
                    }
                    if (PNK.HangNhapCollection.Count > 0)
                    {
                        btnIn.Enabled = true;
                    }
                }
            }
            else if (isV4_TKCT)
            {
                if (TKCT_V4.SoToKhai != 0)
                {
                    //this.SetIDControl();
                    if (PNK == null || PNK.ID == 0)
                    {
                        PNK = new KDT_GC_PhieuNhapKho();
                        txtKhachHang.Text = TKCT_V4.TenDonViDoiTac;
                        HopDong hd = HopDong.Load(TKCT_V4.IDHopDong);
                        txtSoHopDong.Text = hd != null ? hd.SoHopDong : string.Empty;
                        txtSoToKhai.Text = Convert.ToString(TKCT_V4.SoToKhai);
                        txtSoInvoice.Text =null ;//TKCT_V4.SoHoaDonThuongMai;
                        txtSoBL.Text =TKCT_V4.SoHDKH;
                        clcNgayInvoice.Value = TKCT_V4.NgayHDKH;
                        txtSoKien.Text = Convert.ToString(TKCT_V4.SoKien);
                        ctrNgoaiTe.Code = TKCT_V4.NguyenTe_ID;
                        txtTyGia.Text = TKCT_V4.TyGiaVND.ToString();//Convert.ToString(Company.KDT.SHARE.Components.Globals.GetNguyenTe(ctrNgoaiTe.Code));
                        //set ngày nhập kho = ngày đăng ký trừ 3 ngày theo VINATEX
                        if (TKCT_V4.NgayDangKy.AddDays(+5).DayOfWeek == DayOfWeek.Sunday)
                            clcNgayNhapKho.Value = TKCT_V4.NgayDangKy.AddDays(+6);
                        else
                            clcNgayNhapKho.Value = TKCT_V4.NgayDangKy.AddDays(+5);

                        TKCT_V4.HCTCollection = (List<HangChuyenTiep>)HangChuyenTiep.SelectCollectionBy_Master_ID(TKCT_V4.ID);
                        foreach (HangChuyenTiep HCT in TKCT_V4.HCTCollection)
                        {
                            KDT_GC_PhieuNhapKho_Hang hang = new KDT_GC_PhieuNhapKho_Hang();
                            hang.Ma2 = HCT.MaHang;
                            hang.TenHang = HCT.TenHang;
                            //hang.MaSoHang = HMD.MaSoHang;
                            hang.SoLuong1 = HCT.SoLuong;
                            hang.DVTLuong1 = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(HCT.ID_DVT);
                            try
                            {
                                hang.MaTTDonGia = TKCT_V4.NguyenTe_ID;
                            }
                            catch (Exception)
                            {

                                //throw;
                            }

                            //hang.DonGiaHoaDon = HMD.DonGiaHoaDon;
                            //hang.TriGiaHoaDon = HMD.TriGiaHoaDon;
                            decimal tygia = Convert.ToDecimal(txtTyGia.Text);
                            decimal soluong = Convert.ToDecimal(hang.SoLuong1);
                            if (hang.MaTTDonGia == "VND")
                            {
                                hang.DonGiaHoaDon = HCT.DonGia;
                                hang.TriGiaHoaDon = HCT.TriGia;
                                hang.DonGiaNgoaiTe = hang.DonGiaHoaDon / tygia;
                                hang.ThanhTienNgoaiTe = hang.DonGiaNgoaiTe * soluong;
                            }
                            else
                            {
                                hang.DonGiaNgoaiTe = HCT.DonGia;
                                hang.ThanhTienNgoaiTe = HCT.TriGia;
                                hang.DonGiaHoaDon = hang.DonGiaNgoaiTe * tygia;
                                hang.TriGiaHoaDon = hang.DonGiaHoaDon * soluong;
                            }
                            PNK.HangNhapCollection.Add(hang);
                        }
                        dgList.DataSource = PNK.HangNhapCollection;
                        dgList.Refetch();
                        txtSoPhieu.Text = (KDT_GC_PhieuNhapKho.GetSoPhieuMax(TKCT_V4.IDHopDong) + 1).ToString();
                    }
                    else
                    {
                        SetPhieuNhapKho();
                        dgList.DataSource = PNK.HangNhapCollection;
                    }
                    if (PNK.HangNhapCollection.Count > 0)
                    {
                        btnIn.Enabled = true;
                    }
                }
                else
                {
                    if (PNK == null || PNK.ID == 0)
                    {
                        PNK = new KDT_GC_PhieuNhapKho();
                        txtKhachHang.Text = TKMD_V4.TenDonViDoiTac;
                        txtSoHopDong.Text = HopDong != null ? HopDong.SoHopDong : string.Empty;
                        txtSoToKhai.Text = Convert.ToString(TKMD_V4.SoToKhai);
                        txtSoInvoice.Text = "0";//TKMD.SoHoaDon;
                        clcNgayInvoice.Value = DateTime.Now.Date;//TKMD.NgayPhatHanhHD;
                        txtSoKien.Text = "0";//Convert.ToString(TKMD.SoLuong);
                        ctrNgoaiTe.Code = "";//TKMD.MaTTHoaDon;
                        txtTyGia.Text = TKMD_V4.TyGiaTinhThue.ToString();//Convert.ToString(Company.KDT.SHARE.Components.Globals.GetNguyenTe(ctrNgoaiTe.Code));
                        //set ngày nhập kho = ngày đăng ký trừ 3 ngày theo VINATEX
                        clcNgayNhapKho.Value = DateTime.Now;
                        dgList.Refetch();
                        txtSoPhieu.Text = "0";//(KDT_GC_PhieuNhapKho.GetSoPhieuMax(TKMD.HopDong_ID) + 1).ToString();
                    }
                    else
                    {
                        SetPhieuNhapKho();
                        dgList.DataSource = PNK.HangNhapCollection;
                    }
                    if (PNK.HangNhapCollection.Count > 0)
                    {
                        btnIn.Enabled = true;
                    }
                }                
            }
            else
            {
                if (TKMD.SoToKhai != 0)
                {
                    //this.SetIDControl();
                    if (PNK == null || PNK.ID == 0)
                    {
                        PNK = new KDT_GC_PhieuNhapKho();
                        txtKhachHang.Text = TKMD.TenDoiTac;
                        HopDong hd = HopDong.Load(TKMD.HopDong_ID);
                        txtSoHopDong.Text = hd != null ? hd.SoHopDong : string.Empty;
                        txtSoToKhai.Text = Convert.ToString(TKMD.SoToKhai);
                        try
                        {
                            KDT_VNACC_TK_SoVanDon vd = KDT_VNACC_TK_SoVanDon.SelectCollectionAll().Find(delegate(KDT_VNACC_TK_SoVanDon t)
                            {
                                return t.TKMD_ID == TKMD.ID;
                            });
                            txtSoBL.Text = vd.SoVanDon;
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            //throw;
                        }
                        
                        txtSoInvoice.Text = TKMD.SoHoaDon;
                        clcNgayInvoice.Value = TKMD.NgayPhatHanhHD;
                        txtSoKien.Text = Convert.ToString(TKMD.SoLuong);
                        
                        KDT_VNACC_TK_PhanHoi_TyGia tg = KDT_VNACC_TK_PhanHoi_TyGia.SelectCollectionAll().Find(delegate(KDT_VNACC_TK_PhanHoi_TyGia t)
                        {
                            return t.Master_ID == TKMD.ID;
                        });
                        ctrNgoaiTe.Code = tg.MaTTTyGiaTinhThue;
                        txtTyGia.Text = tg.TyGiaTinhThue.ToString();//Convert.ToString(Company.KDT.SHARE.Components.Globals.GetNguyenTe(ctrNgoaiTe.Code));
                        //set ngày nhập kho = ngày đăng ký trừ 3 ngày theo VINATEX
                        if (TKMD.NgayDangKy.AddDays(+5).DayOfWeek == DayOfWeek.Sunday)
                            clcNgayNhapKho.Value = TKMD.NgayDangKy.AddDays(+6);
                        else
                            clcNgayNhapKho.Value = TKMD.NgayDangKy.AddDays(+5);

                        foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                        {
                            KDT_GC_PhieuNhapKho_Hang hang = new KDT_GC_PhieuNhapKho_Hang();
                            hang.Ma2 = HMD.MaHangHoa;
                            hang.TenHang = HMD.TenHang;
                            //hang.MaSoHang = HMD.MaSoHang;
                            hang.SoLuong1 = HMD.SoLuong1;
                            hang.DVTLuong1 = HMD.DVTLuong1;
                            hang.MaTTDonGia = HMD.MaTTDonGia;
                            //hang.DonGiaHoaDon = HMD.DonGiaHoaDon;
                            //hang.TriGiaHoaDon = HMD.TriGiaHoaDon;
                            decimal tygia = Convert.ToDecimal(txtTyGia.Text);
                            decimal soluong = Convert.ToDecimal(hang.SoLuong1);
                            if (hang.MaTTDonGia == "VND")
                            {
                                hang.DonGiaHoaDon = HMD.DonGiaHoaDon;
                                hang.TriGiaHoaDon = HMD.TriGiaHoaDon;
                                hang.DonGiaNgoaiTe = HMD.DonGiaTinhThue;
                                hang.ThanhTienNgoaiTe = HMD.TriGiaTinhThue;
                            }
                            else
                            {
                                hang.DonGiaNgoaiTe = HMD.DonGiaHoaDon;
                                hang.ThanhTienNgoaiTe = HMD.TriGiaHoaDon;
                                hang.DonGiaHoaDon = HMD.DonGiaTinhThue;
                                hang.TriGiaHoaDon = HMD.TriGiaTinhThueS;
                            }
                            PNK.HangNhapCollection.Add(hang);
                        }
                        dgList.DataSource = PNK.HangNhapCollection;
                        dgList.Refetch();
                        txtSoPhieu.Text = (KDT_GC_PhieuNhapKho.GetSoPhieuMax(TKMD.HopDong_ID) + 1).ToString();
                    }
                    else
                    {
                        SetPhieuNhapKho();
                        dgList.DataSource = PNK.HangNhapCollection;
                    }
                    if (PNK.HangNhapCollection.Count > 0)
                    {
                        btnIn.Enabled = true;
                    }
                }
                else
                {
                    if (PNK == null || PNK.ID == 0)
                    {
                        PNK = new KDT_GC_PhieuNhapKho();
                        txtKhachHang.Text = TKMD.TenDoiTac;
                        txtSoHopDong.Text = HopDong != null ? HopDong.SoHopDong : string.Empty;
                        txtSoToKhai.Text = Convert.ToString(TKMD.SoToKhai);
                        txtSoInvoice.Text = "0";//TKMD.SoHoaDon;
                        clcNgayInvoice.Value = DateTime.Now;//TKMD.NgayPhatHanhHD;
                        txtSoKien.Text = "0";//Convert.ToString(TKMD.SoLuong);
                        ctrNgoaiTe.Code = "";//TKMD.MaTTHoaDon;
                        txtTyGia.Text = "";// Convert.ToString(Company.KDT.SHARE.Components.Globals.GetNguyenTe(ctrNgoaiTe.Code));
                        //set ngày nhập kho = ngày đăng ký trừ 3 ngày theo VINATEX
                        clcNgayNhapKho.Value = DateTime.Now;



                        dgList.Refetch();
                        txtSoPhieu.Text = "0";//(KDT_GC_PhieuNhapKho.GetSoPhieuMax(TKMD.HopDong_ID) + 1).ToString();
                    }
                    else
                    {
                        SetPhieuNhapKho();
                        dgList.DataSource = PNK.HangNhapCollection;
                    }
                    if (PNK.HangNhapCollection.Count > 0)
                    {
                        btnIn.Enabled = true;
                    }
                }
            }
            
            
        }
        private void LoadGrid()
        {
            //dgList.DataSource = TKMD.HangCollection;
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                GetPhieuNhapKho();
                if (PNK.HangNhapCollection.Count > 0)
                {
                    if (isV4)
                    {
                        if (PNK.TKMD_ID == 0)
                            PNK.TKMD_ID = TKMD_V4.ID;
                    }
                    else 
                    {
                        if (PNK.TKMD_ID == 0)
                            PNK.TKMD_ID = TKMD.ID;
                    }
                        
                    PNK.InsertUpdateFul();
                    ShowMessage("Lưu thành công", false);
                    dgList.DataSource = PNK.HangNhapCollection;
                    SetPhieuNhapKho();
                    dgList.Refetch();
                    btnIn.Enabled = true;
                }
                else
                {
                    ShowMessage("Chưa nhập thông tin hàng", false);
                }
                
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            this.Cursor = Cursors.Default;
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            Company.Interface.Report.GC.PhieuNhapKho pnkrp = new Company.Interface.Report.GC.PhieuNhapKho();
            pnkrp.BindReport(PNK);
            pnkrp.ShowRibbonPreview();
        }
        public long IDTKMD;
        private void txtSoThuTuDongHangTrenToKhaiGoc_Click(object sender, EventArgs e)
        {
            try
            {
                VNACC_ListHangHMD listForm = new VNACC_ListHangHMD(VNACC_ListHangHMD.SelectTion.SingleSelect, new long[] { this.TKMD.ID });
                listForm.ShowDialog();
                if (listForm.DialogResult == DialogResult.OK)
                {
                    if (listForm.HangMD != null)
                    {
                        KDT_VNACC_HangMauDich hmdTNTX = listForm.HangMD;
                        //ctrMaSoHang.Code = hmdTNTX.MaSoHang;
                        txtMoTaHH.Text = hmdTNTX.TenHang;
                        txtSoLuongChungTu.Text = hmdTNTX.SoLuong1.ToString();
                        txtSoLuongThucNhan.Text = hmdTNTX.SoLuong1.ToString();
                        ctrDVTLuong1.Code = hmdTNTX.DVTLuong1;
                        txtDonGia.Text = hmdTNTX.DonGiaHoaDon.ToString();
                        txtThanhTien.Text = hmdTNTX.TriGiaHoaDon.ToString();
                        //txtSoThuTuDongHangTrenToKhaiGoc.Text = hmdTNTX.SoDong;
                        ctrMaTienTe.Code = hmdTNTX.MaTTDonGia;
                        txtMa2.Text = hmdTNTX.MaHangHoa;
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
        private void GetHang()
        {
            hang.MaSoHang = txtMSVT.Text;
            hang.Ma2 = txtMa2.Text;
            hang.TenHang = txtMoTaHH.Text;
            hang.Mau = txtMauSac.Text;
            hang.Size = txtSize.Text;
            hang.SoLuong1 = Convert.ToDecimal(txtSoLuongChungTu.Text);
            hang.SoLuong1 = Convert.ToDecimal(txtSoLuongThucNhan.Text);
            hang.DVTLuong1 = ctrDVTLuong1.Code;
            hang.MaTTDonGia = ctrMaTienTe.Code;
            decimal tygia = Convert.ToDecimal(txtTyGia.Text);
            //decimal dongiaVND = 0;
            //decimal dongiaNT = 0;
            decimal soluong = Convert.ToDecimal(txtSoLuongThucNhan.Text);
            if (ctrMaTienTe.Code == "VND")
            {
                hang.DonGiaHoaDon = Convert.ToDecimal(txtDonGia.Text);
                hang.TriGiaHoaDon = Convert.ToDecimal(txtThanhTien.Text);
            }
            else
            {
                hang.DonGiaNgoaiTe = Convert.ToDecimal(txtDonGia.Text);
                hang.ThanhTienNgoaiTe = Convert.ToDecimal(txtThanhTien.Text);
            }
            hang.GhiChu = txtGhiChu.Text;
        }
        private void SetHang()
        {
            errorProvider.Clear();
            txtMSVT.Text = hang.MaSoHang;
            txtMa2.Text = hang.Ma2;
            txtMoTaHH.Text = hang.TenHang;
            txtMauSac.Text = hang.Mau;
            txtSize.Text = hang.Size;
            txtSoLuongChungTu.Text = Convert.ToString(hang.SoLuong1);
            txtSoLuongThucNhan.Text = Convert.ToString(hang.SoLuong1);
            ctrDVTLuong1.Code = hang.DVTLuong1;
            ctrMaTienTe.Code = hang.MaTTDonGia;
            if (ctrMaTienTe.Code == "VND")
            {
                txtDonGia.Text = Convert.ToString(hang.DonGiaHoaDon);
                txtThanhTien.Text = Convert.ToString(hang.TriGiaHoaDon);
            }
            else
            {
                txtDonGia.Text = Convert.ToString(hang.DonGiaNgoaiTe);
                txtThanhTien.Text = Convert.ToString(hang.ThanhTienNgoaiTe);
            }
            txtGhiChu.Text = hang.GhiChu;
        }
        bool isAddNew = true;
        private void btnThem_Click(object sender, EventArgs e)
        {
            try
            {
                GetHang();
                if (isAddNew)                   
                    PNK.HangNhapCollection.Add(hang);
                dgList.DataSource = PNK.HangNhapCollection;
                dgList.Refetch();
                hang = new KDT_GC_PhieuNhapKho_Hang();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi khi lưu hàng: " + ex, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                hang = (KDT_GC_PhieuNhapKho_Hang)dgList.CurrentRow.DataRow;
                SetHang();
                isAddNew = false;

                dgList.DataSource = PNK.HangNhapCollection;
                try
                {
                    dgList.Refetch();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<KDT_GC_PhieuNhapKho_Hang> ItemColl = new List<KDT_GC_PhieuNhapKho_Hang>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_GC_PhieuNhapKho_Hang)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_GC_PhieuNhapKho_Hang item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        PNK.HangNhapCollection.Remove(item);
                        
                    }
                    
                    dgList.DataSource = PNK.HangNhapCollection;
                    try { dgList.Refetch(); }
                    catch { dgList.Refresh(); }

                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

    }
}