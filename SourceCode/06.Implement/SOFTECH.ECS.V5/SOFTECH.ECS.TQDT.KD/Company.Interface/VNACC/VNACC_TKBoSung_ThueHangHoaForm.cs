﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS.Maper;

namespace Company.Interface
{
    public partial class VNACC_TKBoSung_ThueHangHoaForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung();

        public VNACC_TKBoSung_ThueHangHoaForm()
        {
            InitializeComponent();

            base.SetHandler(this);

            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.AMA.ToString());
        }

        private void VNACC_GiayPhepForm_Load(object sender, EventArgs e)
        {
            SetIDControl();

            SetMaxLengthControl();

            if (TKBoSung.ID != 0)
            {
                SetGiayPhep();
            }

            ValidateControl(true);

            SetAutoRemoveUnicodeAndUpperCaseControl();
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThemHang":
                    this.ShowThemHang();
                    break;
                case "cmdLuu":
                    this.SaveGiayPhep();
                    break;
                case "cmdChiThiHaiQuan":
                    ShowChiThiHaiQuan();
                    break;

                case "cmdKhaiBao":
                    SendVnaccsIDC(false);
                    break;
            }
        }

        private void ShowChiThiHaiQuan()
        {
            VNACC_ChiThiHaiQuanForm f = new VNACC_ChiThiHaiQuanForm();
            f.Master_ID = TKBoSung.ID;
            f.LoaiThongTin = ELoaiThongTin.TK_KhaiBoSung;
            f.ShowDialog();
        }

        private void ShowThemHang()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                VNACC_TKBoSung_ThueHangHoa_HangForm f = new VNACC_TKBoSung_ThueHangHoa_HangForm();
                f.TKBoSung = TKBoSung;
                f.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void SaveGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (!ValidateControl(false))
                    return;

                GetGiayPhep();

                TKBoSung.InsertUpdateFull();

                Helper.Controls.MessageBoxControlV.ShowMessage(Company.KDT.SHARE.Components.ThongBao.APPLICATION_SAVE_DATA_SUCCESS_0Param, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                errorProvider.Clear();

                TKBoSung.SoToKhaiBoSung = Convert.ToInt32(txtSoToKhaiBoSung.Value);
                TKBoSung.CoQuanHaiQuan = ucCoQuanHaiQuan.Code;
                TKBoSung.NhomXuLyHoSo = ucNhomXuLyHoSo.Code;
                TKBoSung.PhanLoaiXuatNhapKhau = txtPhanLoaiXuatNhapKhau.Text;
                TKBoSung.SoToKhai = Convert.ToInt32(txtSoToKhai.Value);
                TKBoSung.MaLoaiHinh = ucMaLoaiHinh.Code;
                TKBoSung.NgayKhaiBao = dtNgayKhaiBao.Value;
                TKBoSung.NgayCapPhep = dtNgayCapPhep.Value;
                TKBoSung.ThoiHanTaiNhapTaiXuat = dtThoiHanTaiNhapTaiXuat.Value;
                TKBoSung.MaNguoiKhai = txtMaNguoiKhai.Text;
                TKBoSung.TenNguoiKhai = txtTenNguoiKhai.Text;
                TKBoSung.MaBuuChinh = txtMaBuuChinhNguoiKhai.Text;
                TKBoSung.DiaChiNguoiKhai = txtDiaChiNguoiKhai.Text;
                TKBoSung.SoDienThoaiNguoiKhai = txtSoDienThoaiNguoiKhai.Text;
                TKBoSung.MaLyDoKhaiBoSung = ucMaLyDoKhaiBoSung.Code;
                TKBoSung.MaTienTeTienThue = ucMaTienTeTienThue.Code;
                TKBoSung.MaNganHangTraThueThay = txtMaNganHangTraThueThay.Text;
                TKBoSung.NamPhatHanhHanMuc = Convert.ToInt32(txtNamPhatHanhHanMuc.Value);
                TKBoSung.KiHieuChungTuPhatHanhHanMuc = txtKiHieuChungTuPhatHanhHanMuc.Text;
                TKBoSung.SoChungTuPhatHanhHanMuc = txtSoChungPhatHanhHanMuc.Text;
                TKBoSung.MaXacDinhThoiHanNopThue = txtMaXacDinhThoiHanNopThue.Text;
                TKBoSung.MaNganHangBaoLanh = txtMaNganHangBaoLanh.Text;
                TKBoSung.NamPhatHanhBaoLanh = Convert.ToInt32(txtNamPhatHanh.Value);
                TKBoSung.KyHieuPhatHanhChungTuBaoLanh = txtKyHieuChungTuBaoLanh.Text;
                TKBoSung.SoHieuPhatHanhChungTuBaoLanh = txtSoChungTuBaoLanh.Text;
                TKBoSung.MaTienTeTruocKhiKhaiBoSung = ucMaTienTeTruocKhiKhaiBoSung.Code;
                TKBoSung.TyGiaHoiDoaiTruocKhiKhaiBoSung = Convert.ToDecimal(txtTyGiaHoiDoaiTruocKhiKhaiBoSung.Value);
                TKBoSung.MaTienTeSauKhiKhaiBoSung = ucMaTienTeSauKhiKhaiBoSung.Code;
                TKBoSung.TyGiaHoiDoaiSauKhiKhaiBoSung = Convert.ToDecimal(txtTyGiaHoiDoaiSauKhiKhaiBoSung.Value);
                TKBoSung.SoQuanLyTrongNoiBoDoanhNghiep = txtSoQuanLyTrongNoiBoDoanhNghiep.Text;
                TKBoSung.GhiChuNoiDungLienQuanTruocKhiKhaiBoSung = txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.Text;
                TKBoSung.GhiChuNoiDungLienQuanSauKhiKhaiBoSung = txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void SetGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                errorProvider.Clear();

                txtSoToKhaiBoSung.Value = TKBoSung.SoToKhaiBoSung;
                ucCoQuanHaiQuan.Code = TKBoSung.CoQuanHaiQuan;
                ucNhomXuLyHoSo.Code = TKBoSung.NhomXuLyHoSo;
                txtPhanLoaiXuatNhapKhau.Text = TKBoSung.PhanLoaiXuatNhapKhau;
                txtSoToKhai.Value = TKBoSung.SoToKhai;
                ucMaLoaiHinh.Code = TKBoSung.MaLoaiHinh;
                dtNgayKhaiBao.Value = TKBoSung.NgayKhaiBao;
                dtNgayCapPhep.Value = TKBoSung.NgayCapPhep;
                dtNgayCapPhep.Text = TKBoSung.NgayCapPhep.ToString();
                dtThoiHanTaiNhapTaiXuat.Value = TKBoSung.ThoiHanTaiNhapTaiXuat;
                dtThoiHanTaiNhapTaiXuat.Text = TKBoSung.NgayCapPhep.ToString();
                txtMaNguoiKhai.Text = TKBoSung.MaNguoiKhai;
                txtTenNguoiKhai.Text = TKBoSung.TenNguoiKhai;
                txtMaBuuChinhNguoiKhai.Text = TKBoSung.MaBuuChinh;
                txtDiaChiNguoiKhai.Text = TKBoSung.DiaChiNguoiKhai;
                txtSoDienThoaiNguoiKhai.Text = TKBoSung.SoDienThoaiNguoiKhai;
                ucMaLyDoKhaiBoSung.Code = TKBoSung.MaLyDoKhaiBoSung;
                ucMaTienTeTienThue.Code = TKBoSung.MaTienTeTienThue;
                txtMaNganHangTraThueThay.Text = TKBoSung.MaNganHangTraThueThay;
                txtNamPhatHanhHanMuc.Value = TKBoSung.NamPhatHanhHanMuc;
                txtKiHieuChungTuPhatHanhHanMuc.Text = TKBoSung.KiHieuChungTuPhatHanhHanMuc;
                txtSoChungPhatHanhHanMuc.Text = TKBoSung.SoChungTuPhatHanhHanMuc;
                txtMaXacDinhThoiHanNopThue.Text = TKBoSung.MaXacDinhThoiHanNopThue;
                txtMaNganHangBaoLanh.Text = TKBoSung.MaNganHangBaoLanh;
                txtNamPhatHanh.Value = TKBoSung.NamPhatHanhBaoLanh;
                txtKyHieuChungTuBaoLanh.Text = TKBoSung.KyHieuPhatHanhChungTuBaoLanh;
                txtSoChungTuBaoLanh.Text = TKBoSung.SoHieuPhatHanhChungTuBaoLanh;
                ucMaTienTeTruocKhiKhaiBoSung.Code = TKBoSung.MaTienTeTruocKhiKhaiBoSung;
                txtTyGiaHoiDoaiTruocKhiKhaiBoSung.Value = TKBoSung.TyGiaHoiDoaiTruocKhiKhaiBoSung;
                ucMaTienTeSauKhiKhaiBoSung.Code = TKBoSung.MaTienTeSauKhiKhaiBoSung;
                txtTyGiaHoiDoaiSauKhiKhaiBoSung.Value = TKBoSung.TyGiaHoiDoaiSauKhiKhaiBoSung;
                txtSoQuanLyTrongNoiBoDoanhNghiep.Text = TKBoSung.SoQuanLyTrongNoiBoDoanhNghiep;
                txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.Text = TKBoSung.GhiChuNoiDungLienQuanTruocKhiKhaiBoSung;
                txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.Text = TKBoSung.GhiChuNoiDungLienQuanSauKhiKhaiBoSung;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void ucMaQuocGia_EditValueChanged(object sender, EventArgs e)
        {
            //ucCuaKhauXuatNhap1.CountryCode = ucMaQuocGia.Code;
            //ucCuaKhauXuatNhap1.Code = "";
            //ucCuaKhauXuatNhap1.ReLoadData();
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtSoToKhaiBoSung.Tag = "SYN"; //Số tờ khai bổ sung
                ucCoQuanHaiQuan.TagName = "CHS"; //Cơ quan Hải quan
                ucNhomXuLyHoSo.TagName = "CHB"; //Nhóm xử lý hồ sơ
                txtPhanLoaiXuatNhapKhau.Tag = "YNK"; //Phân loại xuất nhập khẩu
                txtSoToKhaiBoSung.Tag = "ICN"; //Số tờ khai
                ucMaLoaiHinh.TagName = "ICB"; //Mã loại hình
                dtNgayKhaiBao.Tag = "IDD"; //Ngày khai báo xuất nhập khẩu
                dtNgayCapPhep.Tag = "IPD"; //Ngày cấp phép xuất nhập khẩu
                dtThoiHanTaiNhapTaiXuat.Tag = "RED"; //Thời hạn tái nhập/ tái xuất
                txtMaNguoiKhai.Tag = "IMC"; //Mã người khai
                txtTenNguoiKhai.Tag = "IMN"; //Tên người khai
                txtMaBuuChinhNguoiKhai.Tag = "IMY"; //Mã bưu chính
                txtDiaChiNguoiKhai.Tag = "IMA"; //Địa chỉ của người khai
                txtSoDienThoaiNguoiKhai.Tag = "IMT"; //Số điện thoại của người khai
                ucMaLyDoKhaiBoSung.TagName = "REC"; //Mã lý do khai bổ sung
                ucMaTienTeTienThue.TagName = "TTC"; //Mã tiền tệ của tiền thuế
                txtMaNganHangTraThueThay.Tag = "BRC"; //Mã ngân hàng trả thuế thay
                txtNamPhatHanhHanMuc.Tag = "BYA"; //Năm phát hành hạn mức
                txtKiHieuChungTuPhatHanhHanMuc.Tag = "BCM"; //Kí hiệu chứng từ phát hành hạn mức
                txtSoChungPhatHanhHanMuc.Tag = "BCN"; //Số chứng từ phát hành hạn mức
                txtMaXacDinhThoiHanNopThue.Tag = "ENC"; //Mã xác định thời hạn nộp thuế
                txtMaNganHangBaoLanh.Tag = "SBC"; //Mã ngân hàng bảo lãnh
                txtNamPhatHanh.Tag = "RYA"; //Năm phát hành bảo lãnh
                txtKyHieuChungTuBaoLanh.Tag = "SCM"; //Ký hiệu phát hành chứng từ bảo lãnh
                txtSoChungTuBaoLanh.Tag = "SCN"; //Số hiệu phát hành chứng từ bảo lãnh
                ucMaTienTeTruocKhiKhaiBoSung.TagName = "CDB"; //Mã tiền tệ trước khi khai bổ sung
                txtTyGiaHoiDoaiTruocKhiKhaiBoSung.Tag = "RTB"; //Tỷ giá hối đoái trước khi khai bổ sung
                ucMaTienTeSauKhiKhaiBoSung.TagName = "CDA"; //Mã tiền tệ sau khi khai bổ sung
                txtTyGiaHoiDoaiSauKhiKhaiBoSung.Tag = "RTA"; //Tỷ giá hối đoái sau khi khai bổ sung
                txtSoQuanLyTrongNoiBoDoanhNghiep.Tag = "REF"; //Số quản lý trong nội bộ doanh nghiệp
                txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.Tag = "NTB"; //Ghi chú nội dung liên quan trước khi khai bổ sung
                txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.Tag = "NTA"; //Ghi chú nội dung liên quan sau khi khai bổ sung
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateControl(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                ucCoQuanHaiQuan.SetValidate = true;
                isValid &= ucCoQuanHaiQuan.IsValidate; //"Cơ quan Hải quan");
                ucNhomXuLyHoSo.SetValidate = true;
                isValid &= ucNhomXuLyHoSo.IsValidate; //"Nhóm xử lý hồ sơ");
                isValid &= Globals.ValidateNull(txtPhanLoaiXuatNhapKhau, errorProvider, "Phân loại xuất nhập khẩu", isOnlyWarning);
                isValid &= Globals.ValidateNull(txtSoToKhai, errorProvider, "Số tờ khai", isOnlyWarning);
                ucMaLoaiHinh.SetValidate = true;
                isValid &= ucMaLoaiHinh.IsValidate; //"Mã loại hình");
                isValid &= Globals.ValidateNull(dtNgayKhaiBao, errorProvider, "Ngày khai báo xuất nhập khẩu", isOnlyWarning);
                isValid &= Globals.ValidateNull(dtNgayCapPhep, errorProvider, "Ngày cấp phép xuất nhập khẩu", isOnlyWarning);
                isValid &= Globals.ValidateNull(txtMaNguoiKhai, errorProvider, "Mã người khai", isOnlyWarning);
                ucMaLyDoKhaiBoSung.SetValidate = true;
                isValid &= ucMaLyDoKhaiBoSung.IsValidate; //"Mã lý do khai bổ sung");
                ucMaTienTeTienThue.SetValidate = true;
                isValid &= ucMaTienTeTienThue.IsValidate; //"Mã tiền tệ của tiền thuế");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtSoToKhaiBoSung.MaxLength = 12;
                //ucCoQuanHaiQuan.MaxLength = 6;
                //ucNhomXuLyHoSo.MaxLength = 2;
                txtPhanLoaiXuatNhapKhau.MaxLength = 1;
                txtSoToKhaiBoSung.MaxLength = 12;
                //ucMaLoaiHinh.MaxLength = 3;
                //dtNgayKhaiBao.MaxLength = 8;
                //dtNgayCapPhep.MaxLength = 8;
                //dtThoiHanTaiNhapTaiXuat.MaxLength = 8;
                txtMaNguoiKhai.MaxLength = 13;
                txtTenNguoiKhai.MaxLength = 300;
                txtMaBuuChinhNguoiKhai.MaxLength = 7;
                txtDiaChiNguoiKhai.MaxLength = 300;
                txtSoDienThoaiNguoiKhai.MaxLength = 20;
                //ucMaLyDoKhaiBoSung.MaxLength = 1;
                //ucMaTienTeTienThue.MaxLength = 3;
                txtMaNganHangTraThueThay.MaxLength = 11;
                txtNamPhatHanhHanMuc.MaxLength = 4;
                txtKiHieuChungTuPhatHanhHanMuc.MaxLength = 10;
                txtSoChungTuBaoLanh.MaxLength = 10;
                txtMaXacDinhThoiHanNopThue.MaxLength = 1;
                txtMaNganHangBaoLanh.MaxLength = 11;
                txtNamPhatHanh.MaxLength = 4;
                txtKyHieuChungTuBaoLanh.MaxLength = 10;
                txtSoChungTuBaoLanh.MaxLength = 10;
                //ucMaTienTeTruocKhiKhaiBoSung.MaxLength = 3;
                txtTyGiaHoiDoaiTruocKhiKhaiBoSung.MaxLength = 9;
                //ucMaTienTeSauKhiKhaiBoSung.MaxLength = 3;
                txtTyGiaHoiDoaiSauKhiKhaiBoSung.MaxLength = 9;
                txtSoQuanLyTrongNoiBoDoanhNghiep.MaxLength = 20;
                txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.MaxLength = 300;
                txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.MaxLength = 300;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCoQuanHaiQuan_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                ucNhomXuLyHoSo.WhereCondition = string.Format("CustomsCode like '{0}%'", ucCoQuanHaiQuan.Code);
                ucNhomXuLyHoSo.ReLoadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void txtPhanLoaiXuatNhapKhau_TextChanged(object sender, EventArgs e)
        {
            try
            {
                ucMaLoaiHinh.CategoryType = txtPhanLoaiXuatNhapKhau.Text.Trim() == "I" ? ECategory.E001 : ECategory.E002;
                ucMaLoaiHinh.ReLoadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        #region Send VNACCS

        /// <summary>
        /// Khai báo thông tin
        /// </summary>
        /// <param name="KhaiBaoSua"></param>
        private void SendVnaccsIDC(bool KhaiBaoSua)
        {
            try
            {
                if (TKBoSung.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }

                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến Hải quan? ", true) == "Yes")
                {
                    TKBoSung.InputMessageID = HelperVNACCS.NewInputMSGID(); //Tạo mới GUID ID
                    TKBoSung.InsertUpdateFull(); //Lưu thông tin GUID ID vừa tạo

                    MessagesSend msg = null; //Form khai báo
                    //AMA seaObj = VNACCMaperFromObject.AMAMapper(TKBoSung); //Set Mapper
                    //if (seaObj == null)
                    //{
                    //    this.ShowMessage("Lỗi khi tạo messages !", false);
                    //    return;
                    //}
                    //msg = MessagesSend.Load<SEA>(seaObj, TKBoSung.InputMessageID);

                    MsgLog.SaveMessages(msg, TKBoSung.ID, EnumThongBao.SendMess, ""); //Lưu thông tin trước khai báo
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true; //Có khai báo
                    f.isRep = true; //Có nhận phản hồi
                    f.inputMSGID = TKBoSung.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result) //Có kêt quả trả về
                    {
                        string ketqua = "Khai báo thông tin thành công";

                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                        {
                            try
                            {
                                decimal soTiepNhan = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
                                ketqua = ketqua + Environment.NewLine;
                                ketqua += "Số tờ khai sửa: " + soTiepNhan;

                                TKBoSung.SoToKhaiBoSung = soTiepNhan;
                            }
                            catch (System.Exception ex)
                            {
                                ketqua += Environment.NewLine + "Lỗi cập nhật số tờ khai: " + Environment.NewLine + ex.Message;
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }

                        TuDongCapNhatThongTin();
                    }
                    else if (f.DialogResult == DialogResult.Cancel)
                    {
                        ShowMessage(f.msgFeedBack, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        #endregion

        #region Cập nhật thông tin

        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            ProcessMessages.GetDataResult_TKBoSung(msgResult, "", TKBoSung);
            TKBoSung.InsertUpdateFull();
            SetGiayPhep();
            //setCommandStatus();
        }

        private void TuDongCapNhatThongTin()
        {
            if (TKBoSung != null && TKBoSung.SoToKhai > 0 && TKBoSung.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan = '{0}' And MessagesInputID = '{1}'", TKBoSung.SoToKhaiBoSung.ToString(), TKBoSung.InputMessageID), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }
            }
        }

        #endregion

        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            txtSoToKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucCoQuanHaiQuan.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucNhomXuLyHoSo.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtPhanLoaiXuatNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoToKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucMaLoaiHinh.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtNgayKhaiBao.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtNgayCapPhep.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtThoiHanTaiNhapTaiXuat.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTenNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaBuuChinh.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtDiaChiNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoDienThoaiNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucMaLyDoKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaTienTeTienThue.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaNganHangTraThueThay.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtNamPhatHanhHanMuc.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtKiHieuChungTuPhatHanhHanMuc.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoChungPhatHanhHanMuc.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaXacDinhThoiHanNopThue.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaNganHangBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtNamPhatHanhBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtKyHieuChungTuBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoChungTuBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaTienTeTruocKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTyGiaHoiDoaiTruocKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaTienTeSauKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTyGiaHoiDoaiSauKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoQuanLyTrongNoiBoDoanhNghiep.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);

            txtSoToKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            ucCoQuanHaiQuan.IsUpperCase = true;
            ucNhomXuLyHoSo.IsUpperCase = true;
            txtPhanLoaiXuatNhapKhau.CharacterCasing = CharacterCasing.Upper;
            txtSoToKhai.CharacterCasing = CharacterCasing.Upper;
            ucMaLoaiHinh.IsUpperCase = true;
            //txtNgayKhaiBao.CharacterCasing = CharacterCasing.Upper;
            //txtNgayCapPhep.CharacterCasing = CharacterCasing.Upper;
            //txtThoiHanTaiNhapTaiXuat.CharacterCasing = CharacterCasing.Upper;
            txtMaNguoiKhai.CharacterCasing = CharacterCasing.Upper;
            txtTenNguoiKhai.CharacterCasing = CharacterCasing.Upper;
            txtMaBuuChinhNguoiKhai.CharacterCasing = CharacterCasing.Upper;
            txtDiaChiNguoiKhai.CharacterCasing = CharacterCasing.Upper;
            txtSoDienThoaiNguoiKhai.CharacterCasing = CharacterCasing.Upper;
            ucMaLyDoKhaiBoSung.IsUpperCase = true;
            ucMaTienTeTienThue.IsUpperCase = true;
            txtMaNganHangTraThueThay.CharacterCasing = CharacterCasing.Upper;
            txtNamPhatHanhHanMuc.CharacterCasing = CharacterCasing.Upper;
            txtKiHieuChungTuPhatHanhHanMuc.CharacterCasing = CharacterCasing.Upper;
            txtSoChungPhatHanhHanMuc.CharacterCasing = CharacterCasing.Upper;
            txtMaXacDinhThoiHanNopThue.CharacterCasing = CharacterCasing.Upper;
            txtMaNganHangBaoLanh.CharacterCasing = CharacterCasing.Upper;
            //txtNamPhatHanhBaoLanh.CharacterCasing = CharacterCasing.Upper;
            txtKyHieuChungTuBaoLanh.CharacterCasing = CharacterCasing.Upper;
            txtSoChungTuBaoLanh.CharacterCasing = CharacterCasing.Upper;
            ucMaTienTeTruocKhiKhaiBoSung.IsUpperCase = true;
            txtTyGiaHoiDoaiTruocKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            ucMaTienTeSauKhiKhaiBoSung.IsUpperCase = true;
            txtTyGiaHoiDoaiSauKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            txtSoQuanLyTrongNoiBoDoanhNghiep.CharacterCasing = CharacterCasing.Upper;
            txtGhiChuNoiDungLienQuanTruocKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            txtGhiChuNoiDungLienQuanSauKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;

        }

    }
}
