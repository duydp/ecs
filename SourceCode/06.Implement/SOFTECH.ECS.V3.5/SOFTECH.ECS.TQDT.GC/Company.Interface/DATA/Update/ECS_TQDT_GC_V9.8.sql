/****** Object:  StoredProcedure [p_KDT_GC_DinhMucDangKy_SelectDynamic_SP_NPL]    Script Date: 05/28/2013 09:28:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [p_KDT_GC_DinhMucDangKy_SelectDynamic_SP_NPL]
-- Database: ECS_GCTQ
-- Author: Ngo Thanh Tung
-- Time created: Friday, March 05, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_SelectDynamic_SP_NPL]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON 
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT  DinhMuc_DK.[ID]
      ,DinhMuc_DK.[SoTiepNhan]
      ,DinhMuc_DK.[TrangThaiXuLy]
      ,DinhMuc_DK.[NgayTiepNhan]
      ,DinhMuc_DK.[ID_HopDong]
      ,DinhMuc_DK.[MaHaiQuan]
      ,DinhMuc_DK.[MaDoanhNghiep]
      ,DinhMuc_DK.[GUIDSTR]
      ,DinhMuc_DK.[DeXuatKhac]
      ,DinhMuc_DK.[LyDoSua]
      ,DinhMuc_DK.[ActionStatus]
      ,DinhMuc_DK.[GuidReference]
      ,DinhMuc_DK.[NamTN]
      ,DinhMuc_DK.[HUONGDAN]
      ,DinhMuc_DK.[PhanLuong]
      ,DinhMuc_DK.[Huongdan_PL]
  FROM [dbo].[t_KDT_GC_DinhMucDangKy] DinhMuc_DK
  INNER JOIN [dbo].[t_KDT_GC_DinhMuc] DinhMuc
  ON DinhMuc_DK.ID = DinhMuc.Master_ID
  INNER JOIN t_kdt_gc_hopdong HopDong
  on DinhMuc_DK.ID_hopDong= HopDong.id
   ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL
GO

UPDATE t_haiquan_version set [version]='9.8', notes = N'Cap nhat store tim kiem DM'

