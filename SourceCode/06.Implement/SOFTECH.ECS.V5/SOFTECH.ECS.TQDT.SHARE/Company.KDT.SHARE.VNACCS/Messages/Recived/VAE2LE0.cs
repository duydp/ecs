﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAE2LE0 : BasicVNACC
    {
        public List<VAE2LE0_HANG> HangMD { get; set; }
        public void LoadVAE2LE0(string strResult)
        {
            try
            {
                this.GetObject<VAE2LE0>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAE2LE0, false, VAE2LE0.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAE2LE0.TongSoByte);
                while (true)
                {
                    VAE2LE0_HANG vad1agHang = new VAE2LE0_HANG();
                    vad1agHang.GetObject<VAE2LE0_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAE2LE0_HANG", false, VAE2LE0_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAE2LE0_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAE2LE0_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAE2LE0_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAE2LE0_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }

    }
}
