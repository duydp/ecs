﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System;

namespace SaoChepChuyenDaDuyetMaHQKhac
{
    public partial class BaseForm : Form
    {
        public string MaDoanhNghiep;
        public string MaHaiQuan;

        private IContainer components = null;
        ResourceManager resource = null;

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);
        }

        public BaseForm()
        {
            InitializeComponent();
        }

    }
}