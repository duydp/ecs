﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.DanhMucChuan
{
    public partial class NguyenTeForm : BaseForm
    {
        HtmlDocument docHTML = null;
        DataSet ds = new DataSet();
        WebBrowser wbManin = new WebBrowser();

        public NguyenTeForm()
        {
            InitializeComponent();
            wbManin.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wbManin_DocumentCompleted);
        }

        private void NguyenTeForm_Load(object sender, EventArgs e)
        {
            LoadData();
            //wbManin.Navigate("http://www.sbv.gov.vn/vn/CdeQLNH/tygiaXNK.jsp");
        }

        private void LoadData()
        {
            ds = NguyenTe.SelectAll();
            dgList.DataSource = ds;
            dgList.DataMember = ds.Tables[0].TableName;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region GetChanges

                //Updated by Hungtq, 10/08/2012.
                if (ds.Tables[0].GetChanges() != null)
                {
                    DataRow[] rows;

                    foreach (DataRow dr in ds.Tables[0].GetChanges().Rows)
                    {
                        if (dr.RowState == DataRowState.Added)
                        {
                            rows = ds.Tables[0].Select(" ID= '" + dr["ID"].ToString().Trim() + "'");

                            for (int i = 0; i < rows.Length; i++)
                            {
                                rows[i]["DateCreated"] = System.DateTime.Now;
                                rows[i]["DateModified"] = System.DateTime.Now;
                            }
                        }
                        else if (dr.RowState == DataRowState.Modified)
                        {
                            rows = ds.Tables[0].Select(" ID= '" + dr["ID"].ToString().Trim() + "'");

                            for (int i = 0; i < rows.Length; i++)
                            {
                                rows[i]["DateModified"] = System.DateTime.Now;
                            }
                        }
                    }
                }

                #endregion

                NguyenTe.Update(ds);
                ShowMessage(setText("Cập nhật thành công.", "Save successfully"), false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            if (ShowMessage(setText("Bạn có muốn xóa các cửa khẩu này không?", "Do you want to delete ?"), true) != "Yes") e.Cancel = true;
        }
        private bool CheckID(string Id)
        {
            if (ds.Tables[0].Select(" ID= '" + Id.Trim() + "'").Length == 0) return false;
            else return true;
        }
        private void dgList_UpdatingCell(object sender, Janus.Windows.GridEX.UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "ID")
            {
                string s = e.Value.ToString();
                if (CheckID(s))
                {
                    ShowMessage(setText("Mã nguyên tệ này đã có.", "This code is exist"), false);
                    e.Cancel = true;
                }
                if (s.Trim().Length == 0)
                {
                    ShowMessage(setText("Mã nguyên tệ không được rỗng.", "Original Currency Code must be filled"), false);
                    e.Cancel = true;
                }
            }
        }

        private void btnGetCurrency_Click(object sender, EventArgs e)
        {
            //if (docHTML == null)
            //{
            //    docHTML = wbManin.Document;
            //}
            //HtmlElement itemTyGia = null;
            //foreach (HtmlElement item in docHTML.GetElementsByTagName("table"))
            //{
            //    if (item.GetAttribute("id") == "AutoNumber6")
            //    {
            //        itemTyGia = item;
            //        break;
            //    }
            //}
            //if (itemTyGia == null)
            //    return;
            //int i = 1;
            //XmlDocument doc = new XmlDocument();
            //XmlElement root = doc.CreateElement("Root");
            //doc.AppendChild(root);
            //foreach (HtmlElement itemTD in itemTyGia.GetElementsByTagName("TR"))
            //{
            //    if (i == 1)
            //    {
            //        i++;
            //        continue;
            //    }
            //    HtmlElementCollection collection = itemTD.GetElementsByTagName("TD");
            //    for (int j = 1; j < collection.Count; ++j)
            //    {
            //        XmlElement itemNT = doc.CreateElement("NgoaiTe");
            //        XmlElement itemMaNT = doc.CreateElement("MaNgoaiTe");
            //        itemMaNT.InnerText = collection[0].InnerText;

            //        XmlElement itemTenNT = doc.CreateElement("TenNgoaiTe");
            //        itemTenNT.InnerText = collection[1].InnerText;

            //        XmlElement itemTyNT = doc.CreateElement("TyGiaNgoaiTe");
            //        itemTyNT.InnerText = collection[2].InnerText;

            //        itemNT.AppendChild(itemMaNT);
            //        itemNT.AppendChild(itemTenNT);
            //        itemNT.AppendChild(itemTyNT);
            //        root.AppendChild(itemNT);
            //    }
            //}
            //doc.Save("TyGia");

            this.Cursor = Cursors.WaitCursor;

            Company.KDT.SHARE.Components.Globals.GetTyGia();

            this.Cursor = Cursors.Default;
        }

        private void wbManin_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            //docHTML = wbManin.Document;

            //HtmlElement itemTyGia = null;
            //foreach (HtmlElement item in docHTML.GetElementsByTagName("table"))
            //{
            //    if (item.GetAttribute("id") == "AutoNumber6")
            //    {
            //        itemTyGia = item;
            //        break;
            //    }
            //}
            //if (itemTyGia == null)
            //    return;
            //int i = 1;
            //XmlDocument doc = new XmlDocument();
            //XmlElement root = doc.CreateElement("Root");
            //doc.AppendChild(root);
            //foreach (HtmlElement itemTD in itemTyGia.GetElementsByTagName("TR"))
            //{
            //    if (i == 1)
            //    {
            //        i++;
            //        continue;
            //    }
            //    HtmlElementCollection collection = itemTD.GetElementsByTagName("TD");
            //    for (int j = 1; j < collection.Count; ++j)
            //    {
            //        XmlElement itemNT = doc.CreateElement("NgoaiTe");
            //        XmlElement itemMaNT = doc.CreateElement("MaNgoaiTe");
            //        itemMaNT.InnerText = collection[0].InnerText;

            //        XmlElement itemTenNT = doc.CreateElement("TenNgoaiTe");
            //        itemTenNT.InnerText = collection[1].InnerText;

            //        XmlElement itemTyNT = doc.CreateElement("TyGiaNgoaiTe");
            //        itemTyNT.InnerText = collection[2].InnerText;

            //        itemNT.AppendChild(itemMaNT);
            //        itemNT.AppendChild(itemTenNT);
            //        itemNT.AppendChild(itemTyNT);
            //        root.AppendChild(itemNT);
            //    }
            //}
            //doc.Save("TyGia");
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}