﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_HoaDon 
	{
        List<KDT_VNACC_HangHoaDon> _listHangHoaDon = new List<KDT_VNACC_HangHoaDon>();
        List<KDT_VNACC_PhanLoaiHoaDon> _listPhanLoaiHoaDon = new List<KDT_VNACC_PhanLoaiHoaDon>();
        public List<KDT_VNACC_HangHoaDon> HangCollection
        {
            set { this._listHangHoaDon = value; }
            get { return this._listHangHoaDon; }
        }
        public List<KDT_VNACC_PhanLoaiHoaDon> PhanLoaiCollection
        {
            set { this._listPhanLoaiHoaDon = value; }
            get { return this._listPhanLoaiHoaDon; }
        }

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.TrangThaiXuLy = "0";
                        this.ID = this.Insert(transaction);
                    }
                    else
                        this.Update(transaction);


                    foreach (KDT_VNACC_HangHoaDon itemHang in this.HangCollection)
                    {
                        if (itemHang.ID == 0)
                        {
                            itemHang.HoaDon_ID = this.ID;
                            itemHang.ID = itemHang.Insert(transaction);
                        }
                        else
                        {
                            itemHang.Update(transaction);
                        }
                    }
                    foreach (KDT_VNACC_PhanLoaiHoaDon itemPhanLoai in this.PhanLoaiCollection)
                    {
                        if (itemPhanLoai.ID == 0)
                        {
                            itemPhanLoai.HoaDon_ID = this.ID;
                            itemPhanLoai.ID = itemPhanLoai.Insert(transaction);
                        }
                        else
                        {
                            itemPhanLoai.Update(transaction);
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    connection.Close();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdatePhanHoiFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert(transaction);
                    else
                        this.Update(transaction);

                    //KDT_VNACC_HangHoaDon.DeleteDynamic("HoaDon_ID = " + this.ID);
                    foreach (KDT_VNACC_HangHoaDon itemHang in this.HangCollection)
                    {
                        if (itemHang.ID == 0)
                        {
                        itemHang.ID = 0;
                            itemHang.HoaDon_ID = this.ID;
                            itemHang.ID = itemHang.Insert(transaction);
                        }
                        else
                        {
                            itemHang.Update(transaction);
                        }
                    }
                    //KDT_VNACC_PhanLoaiHoaDon.DeleteDynamic("HoaDon_ID = " + this.ID);
                    foreach (KDT_VNACC_PhanLoaiHoaDon itemPhanLoai in this.PhanLoaiCollection)
                    {
                        if (itemPhanLoai.ID == 0)
                        {
                        itemPhanLoai.ID = 0;
                        itemPhanLoai.HoaDon_ID = this.ID;
                        itemPhanLoai.ID = itemPhanLoai.Insert(transaction);
                        }
                        else
                        {
                            itemPhanLoai.Update(transaction);
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    connection.Close();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public static int DeleteDynamic(string whereCondition, SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_VNACC_HoaDon_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        public bool DeleteFull(long id)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string where = "HoaDon_ID ="+id;
                    //Xoa hang hoa don
                    KDT_VNACC_HangHoaDon.DeleteDynamic(where);
                    //Xoa phan loai hoa don
                    KDT_VNACC_PhanLoaiHoaDon.DeleteDynamic(where);
                    //Xoa hoa don
                    KDT_VNACC_HoaDon.DeleteDynamic("ID=" + id, transaction);

                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    connection.Close();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

	}	
}