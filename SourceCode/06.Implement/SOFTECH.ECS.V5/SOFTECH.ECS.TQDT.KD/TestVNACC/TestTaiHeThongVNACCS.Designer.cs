﻿namespace TestVNACC
{
    partial class TestTaiHeThongVNACCS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout gridBanTam_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestTaiHeThongVNACCS));
            Janus.Windows.GridEX.GridEXLayout gridBanChinhThuc_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.label1 = new System.Windows.Forms.Label();
            this.numericEditBox1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gridBanTam = new Janus.Windows.GridEX.GridEX();
            this.label3 = new System.Windows.Forms.Label();
            this.gridBanChinhThuc = new Janus.Windows.GridEX.GridEX();
            this.label4 = new System.Windows.Forms.Label();
            this.btnExportXMLTam = new Janus.Windows.EditControls.UIButton();
            this.btnExportXMLChinhThuc = new Janus.Windows.EditControls.UIButton();
            this.editBox1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gridBanTam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBanChinhThuc)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số lần thực hiện";
            // 
            // numericEditBox1
            // 
            this.numericEditBox1.DecimalDigits = 0;
            this.numericEditBox1.Location = new System.Drawing.Point(102, 26);
            this.numericEditBox1.Name = "numericEditBox1";
            this.numericEditBox1.Size = new System.Drawing.Size(116, 20);
            this.numericEditBox1.TabIndex = 1;
            this.numericEditBox1.Text = "0";
            this.numericEditBox1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // uiButton1
            // 
            this.uiButton1.Location = new System.Drawing.Point(249, 24);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(75, 23);
            this.uiButton1.TabIndex = 2;
            this.uiButton1.Text = "Thực hiện";
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(12, 110);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(334, 503);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Messages khai báo";
            // 
            // gridBanTam
            // 
            gridBanTam_DesignTimeLayout.LayoutString = resources.GetString("gridBanTam_DesignTimeLayout.LayoutString");
            this.gridBanTam.DesignTimeLayout = gridBanTam_DesignTimeLayout;
            this.gridBanTam.Location = new System.Drawing.Point(352, 84);
            this.gridBanTam.Name = "gridBanTam";
            this.gridBanTam.Size = new System.Drawing.Size(313, 529);
            this.gridBanTam.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(349, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Danh sách tờ khai tạm";
            // 
            // gridBanChinhThuc
            // 
            gridBanChinhThuc_DesignTimeLayout.LayoutString = resources.GetString("gridBanChinhThuc_DesignTimeLayout.LayoutString");
            this.gridBanChinhThuc.DesignTimeLayout = gridBanChinhThuc_DesignTimeLayout;
            this.gridBanChinhThuc.Location = new System.Drawing.Point(671, 84);
            this.gridBanChinhThuc.Name = "gridBanChinhThuc";
            this.gridBanChinhThuc.Size = new System.Drawing.Size(299, 529);
            this.gridBanChinhThuc.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(668, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Danh sách tờ khai chính thức";
            // 
            // btnExportXMLTam
            // 
            this.btnExportXMLTam.Location = new System.Drawing.Point(576, 619);
            this.btnExportXMLTam.Name = "btnExportXMLTam";
            this.btnExportXMLTam.Size = new System.Drawing.Size(75, 23);
            this.btnExportXMLTam.TabIndex = 2;
            this.btnExportXMLTam.Text = "Xuất XML";
            this.btnExportXMLTam.Click += new System.EventHandler(this.btnExportXMLTam_Click);
            // 
            // btnExportXMLChinhThuc
            // 
            this.btnExportXMLChinhThuc.Location = new System.Drawing.Point(895, 619);
            this.btnExportXMLChinhThuc.Name = "btnExportXMLChinhThuc";
            this.btnExportXMLChinhThuc.Size = new System.Drawing.Size(75, 23);
            this.btnExportXMLChinhThuc.TabIndex = 2;
            this.btnExportXMLChinhThuc.Text = "Xuất XML";
            // 
            // editBox1
            // 
            this.editBox1.Location = new System.Drawing.Point(12, 84);
            this.editBox1.Name = "editBox1";
            this.editBox1.Size = new System.Drawing.Size(334, 20);
            this.editBox1.TabIndex = 5;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(349, 29);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(55, 13);
            this.lblStatus.TabIndex = 0;
            this.lblStatus.Text = "Trạng thái";
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // TestTaiHeThongVNACCS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 652);
            this.Controls.Add(this.editBox1);
            this.Controls.Add(this.gridBanChinhThuc);
            this.Controls.Add(this.gridBanTam);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.btnExportXMLChinhThuc);
            this.Controls.Add(this.btnExportXMLTam);
            this.Controls.Add(this.uiButton1);
            this.Controls.Add(this.numericEditBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.label1);
            this.Name = "TestTaiHeThongVNACCS";
            this.Text = "TestTaiHeThongVNACCS";
            ((System.ComponentModel.ISupportInitialize)(this.gridBanTam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBanChinhThuc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox1;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.GridEX gridBanTam;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.GridEX gridBanChinhThuc;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIButton btnExportXMLTam;
        private Janus.Windows.EditControls.UIButton btnExportXMLChinhThuc;
        private Janus.Windows.GridEX.EditControls.EditBox editBox1;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Timer timer1;
    }
}