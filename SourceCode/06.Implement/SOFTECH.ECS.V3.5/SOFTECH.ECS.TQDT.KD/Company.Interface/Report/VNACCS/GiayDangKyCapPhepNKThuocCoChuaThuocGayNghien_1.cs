using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_1()
        {
            InitializeComponent();
        }
        public void BindingReport(VAGP110 vagp)
        {
            lblMaNguoiKhai.Text = vagp.SBC.GetValue().ToString().ToUpper();
            lblTenNguoiKhai.Text = vagp.SBN.GetValue().ToString().ToUpper();
            lblSoDonXinCapPhep.Text = vagp.ONO.GetValue().ToString().ToUpper();
            lblChucNangChungTu.Text = vagp.FNC.GetValue().ToString().ToUpper();
            lblLoaiGP.Text = vagp.PRT.GetValue().ToString().ToUpper();
            //lblNgayKhaiBao.Text = vagp.DOS.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vagp.DOS.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKhaiBao.Text = Convert.ToDateTime(vagp.DOS.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKhaiBao.Text = "";
            }
            lblMaDonViCapPhep.Text = vagp.OAC.GetValue().ToString().ToUpper();
            lblTenDonViCapPhep.Text = vagp.OAN.GetValue().ToString().ToUpper();
            lblMaDoanhNghiepNK.Text = vagp.IMC.GetValue().ToString().ToUpper();
            lblTenDoanhNghiepNK.Text = vagp.IMN.GetValue().ToString().ToUpper();
            lblMaBuuChinhDNNK.Text = vagp.IPC.GetValue().ToString().ToUpper();
            lblSoNhaDNNK.Text = vagp.IAD.GetValue().ToString().ToUpper();
            lblMaQuocGiaDNNK.Text = vagp.ICC.GetValue().ToString().ToUpper();
            lblSoDienThoaiDNNK.Text = vagp.IPN.GetValue().ToString().ToUpper();
            lblSoFaxDNNK.Text = vagp.IFX.GetValue().ToString().ToUpper();
            lblEmailDNNK.Text = vagp.IEM.GetValue().ToString().ToUpper();
            lblSoGPLuuHanhNuocSoTai.Text = vagp.FSC.GetValue().ToString().ToUpper();
            lblSoGPThucHanhTotSXThuoc.Text = vagp.GCN.GetValue().ToString().ToUpper();
            lblMaCuaKhauNhapDuKien.Text = vagp.EPC.GetValue().ToString().ToUpper();
            lblTenCuaKhauNhapDuKien.Text = vagp.EPN.GetValue().ToString().ToUpper();
            lblHoSoLienQuan.Text = vagp.AD.GetValue().ToString().ToUpper();
            lblGhiChu.Text = vagp.RMA.GetValue().ToString().ToUpper();
            //lblTonKhoDenNgay.Text = vagp.SUD.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vagp.SUD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblTonKhoDenNgay.Text = Convert.ToDateTime(vagp.SUD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblTonKhoDenNgay.Text = "";
            }
            lblGiamDocDN.Text = vagp.PIC.GetValue().ToString().ToUpper();

            int HangHoa = vagp.HangHoa.Count;
            if (HangHoa <= 1)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(2).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 1, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            

        }



    }
}
