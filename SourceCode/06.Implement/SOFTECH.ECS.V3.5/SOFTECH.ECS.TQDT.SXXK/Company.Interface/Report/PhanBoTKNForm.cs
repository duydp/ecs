﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace Company.Interface.Report
{
   
    public partial class PhanBoTKNForm : BaseForm
    {
        private string MaHaiQuan;
        PhanBoTKNReport report = new PhanBoTKNReport();
        public PhanBoTKNForm()
        {
            InitializeComponent();
        }

        private void PhanBoTKNForm_Load(object sender, EventArgs e)
        {
            if (GlobalSettings.GiaoDien.Id == "Office2003")
                this.defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            
        }

        private void txtToKhaiNhap_ButtonClick(object sender, EventArgs e)
        {
            ToKhaiNhapDaPhanBoForm f = new ToKhaiNhapDaPhanBoForm();
            f.ShowDialog(this);
            txtToKhaiNhap.Text = f.TKMD.SoToKhai + "";
            txtLoaiHinhNhap.Text = f.TKMD.MaLoaiHinh;
            txtNamDKNhap.Text = f.TKMD.NamDangKy + "";
            this.MaHaiQuan = f.TKMD.MaHaiQuan;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (cvError.IsValid)
            {
                this.report = new PhanBoTKNReport();
                report.SoToKhai = Convert.ToInt32(txtToKhaiNhap.Text);
                report.NamDangKy = Convert.ToInt16(txtNamDKNhap.Text);
                report.MaHaiQuan = this.MaHaiQuan.Trim();
                report.MaLoaiHinh = txtLoaiHinhNhap.Text;
                report.BindReport();
                printControl1.PrintingSystem = this.report.PrintingSystem;
                this.report.CreateDocument();

            }
        }

    


    }
}