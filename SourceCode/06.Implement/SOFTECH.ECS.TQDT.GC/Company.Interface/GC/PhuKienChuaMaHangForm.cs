using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.GC
{
    public partial class PhuKienChuaMaHangForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public PhuKienChuaMaHangForm()
        {
            InitializeComponent();
        }
        public PhuKienDangKyCollection PKDKCollection = new PhuKienDangKyCollection();
        private void PhuKienChuaMaHangForm_Load(object sender, EventArgs e)
        {
            dgPhuKien.DataSource = this.PKDKCollection;
        }
        private void dgPhuKien_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            PhuKienDangKy PKDK = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
            PhuKienGCDetailForm pkForm = new PhuKienGCDetailForm();
            pkForm.HD = this.HD;
            pkForm.PKDK = PKDK;
            pkForm.ShowDialog();
        }
    }
}

