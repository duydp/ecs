using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using System;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using System.ServiceModel;
using System.Threading;
using System.Collections.Generic;
using SignRemote;
namespace Company.Interface
{
    public class SendMessageForm : XtraForm
    {

        private Font _boldFont;
        private string _caption;
        private Font _font;
        private PictureBox _pic;
        private string _title;
        public string Caption
        {

            get
            {
                return _caption;
            }
            set
            {
                _caption = value;
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(Refresh));
                }
            }
        }



        public override bool AllowFormSkin
        {
            get
            {
                return false;
            }
        }

        public SendMessageForm()
            : this(string.Empty)
        {

        }

        public SendMessageForm(string caption)
            : this(caption, string.Empty)
        {
        }

        public SendMessageForm(string caption, string title)
            : this(caption, title, new Size(260, 50), null)
        {
        }

        public SendMessageForm(string caption, Size size)
            : this(caption, string.Empty, size, null)
        {
        }

        public SendMessageForm(string caption, string title, Size size)
            : this(caption, title, size, null)
        {
        }

        public SendMessageForm(string caption, string title, Size size, Form parent)
        {
            this._caption = string.Empty;
            this._title = string.Empty;

            this._caption = caption;
            this._title = title == string.Empty ? "Đang thực hiện. Vui lòng chờ giây lát." : title;
            _boldFont = new Font("Arial", 9.0F, FontStyle.Bold);
            _font = new Font("Arial", 9.0F);
            _pic = new PictureBox();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            ControlBox = false;
            ClientSize = size;
            if (parent == null)
            {
                StartPosition = FormStartPosition.CenterScreen;
            }
            else
            {
                StartPosition = FormStartPosition.Manual;
                Left = parent.Left + ((parent.Width - Width) / 2);
                Top = parent.Top + ((parent.Height - Height) / 2);
            }
            ShowInTaskbar = false;
            TopMost = false;
            Paint += new PaintEventHandler(WaitDialogPaint);
            _pic.Size = new Size(16, 16);
            Size size1 = ClientSize;
            _pic.Location = new Point(8, (size1.Height / 2) - 16);

            _pic.Image = Globals.ResourceWait;
            if (_pic.Image == null && System.IO.File.Exists(@"Image/Wait.gif"))
                _pic.Image = Image.FromFile(@"Image/Wait.gif");

            Controls.Add(_pic);
            Refresh();
        }
        public string GetCaption()
        {
            return Caption;
        }

        public void SetCaption(string newCaption)
        {
            Caption = newCaption;
        }

        private void WaitDialogPaint(object sender, PaintEventArgs e)
        {
            Rectangle rectangle = e.ClipRectangle;
            rectangle.Inflate(-1, -1);
            if (_boldFont == null)
                _boldFont = new Font("Arial", 9.0F, FontStyle.Bold);
            if (_font == null)
                _font = new Font("Arial", 9.0F);
            GraphicsCache graphicsCache = new GraphicsCache(e);
            using (StringFormat stringFormat = new StringFormat())
            {

                Brush brush = graphicsCache.GetSolidBrush(LookAndFeelHelper.GetSystemColor(LookAndFeel, SystemColors.WindowText));
                StringAlignment stringAlignment = StringAlignment.Center;
                stringFormat.LineAlignment = StringAlignment.Center;
                stringFormat.Alignment = stringAlignment;
                stringFormat.Trimming = StringTrimming.EllipsisCharacter;
                if (LookAndFeel.ActiveLookAndFeel.ActiveStyle == ActiveLookAndFeelStyle.Skin)
                    ObjectPainter.DrawObject(graphicsCache, new SkinTextBorderPainter(LookAndFeel), new BorderObjectInfoArgs(null, rectangle, null));
                else
                    ControlPaint.DrawBorder3D(e.Graphics, rectangle, Border3DStyle.RaisedInner);
                rectangle.X += 30;
                rectangle.Width -= 30;
                rectangle.Height /= 3;
                rectangle.Y += rectangle.Height / 2;
                e.Graphics.DrawString(_title, _boldFont, brush, rectangle, stringFormat);
                rectangle.Y += rectangle.Height;
                e.Graphics.DrawString(_caption, _font, brush, rectangle, stringFormat);
                graphicsCache.Dispose();
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            _pic.Image = null;
            _boldFont = null;
            _font = null;
            base.OnClosing(e);
        }

        #region Process Send Message


        private string _messageSend = string.Empty;
        private string _password = GlobalSettings.PassWordDT;
        private ObjectSend _objectSend;
        /// <summary>
        /// Gửi thông tin mặc định gỡ bỏ thuộc tính namespace
        /// </summary>
        /// <param name="objContent"></param>
        /// <returns></returns>
        public bool DoSend(ObjectSend objSend)
        {
            return DoSend(objSend, true);
        }
        /// <summary>
        /// Gửi thông tin 
        /// </summary>
        /// <param name="objContent"></param>
        /// <param name="removeNameSpace"></param>
        /// <returns></returns>
        public bool DoSend(ObjectSend objSend, bool removeNameSpace)
        {
            return DoSend(objSend, true, removeNameSpace);
        }

        private bool _removeDeclartion;
        private bool _removeNameSpace;
        public bool DoSend(ObjectSend objSend, bool removeDeclartion, bool removeNameSpace)
        {

            if (objSend == null)
                throw new ArgumentNullException("Không có nội dung gửi đi");

            _objectSend = objSend;
            _removeDeclartion = removeDeclartion;
            _removeNameSpace = removeNameSpace;

            //Nếu cho phép sử dụng chữ ký số và
            //Không ghi nhớ mật khẩu và
            //Không ghi nhớ cho lần sau
            //Và không kết nối tới USB TOKEN
            //Thì quay ra. KHÔNG KÝ
            //LANNT
            if (Company.KDT.SHARE.Components.Globals.IsSignature
                && !Company.KDT.SHARE.Components.Globals.IsRememberSign
                && !Company.KDT.SHARE.Components.Globals.IsRememberSignOnline
                && new FrmLoginUSB().ShowDialog() != DialogResult.OK)
            {
                return false;
            }
            if (!GlobalSettings.IsRemember)
            {
                WSForm wsForm = new WSForm();
                wsForm.ShowDialog();
                if (!wsForm.IsReady) return false;
                _password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
            }
            ThreadPool.QueueUserWorkItem(DoWork);
            return ShowDialog() == DialogResult.OK;
        }
        public event EventHandler<SendEventArgs> Send;
        private IHaiQuanContract Service { get; set; }
        private TimeSpan _usedTime = new TimeSpan();
        private DateTime _lastStartTime;
        public TimeSpan TotalUsedTime
        {
            get
            {
                return _usedTime.Add(DateTime.Now - _lastStartTime);
            }
        }
        private int CountRequest(decimal len)
        {
            int timeRequest = 0;
            Random rnd1 = new Random();

            if (len < 1024)
                timeRequest = rnd1.Next(3, 7);
            else if (len > 1024 & len < 1048576)
                timeRequest = rnd1.Next(5, 9);
            else if (len > 1048576 & len < 107341824)
                timeRequest = rnd1.Next(10, 20);
            else timeRequest = rnd1.Next(30, 60);
            return timeRequest;
        }
        private MessageSignature GetSignatureRemote(string content)
        {

            ISignMessage signRemote = WebService.SignMessage();
            string guidStr = Guid.NewGuid().ToString();
            MessageSend msgSend = new MessageSend()
           {
               From = Company.KDT.SHARE.Components.Globals.UserNameSignRemote,
               Password = Company.KDT.SHARE.Components.Globals.PasswordSignRemote,
               SendStatus = SendStatusInfo.Send,
               Message = new SignRemote.Message()
               {
                   Content = content,
                   GuidStr = guidStr
               },
               To = GlobalSettings.MA_DON_VI.Trim()
           };

            string msg = Helpers.Serializer(msgSend, true, true);
            SetCaption("Gửi ký thông tin");
            string msgRet = signRemote.ClientSend(msg);
            msgSend = Helpers.Deserialize<MessageSend>(msgRet);
            if (msgSend.SendStatus == SendStatusInfo.Error)
            {
                throw new Exception("Thông báo từ hệ thống: " + msgSend.Warrning);
            }
            MessageSend msgFeedBack = new MessageSend()
            {
                From = Company.KDT.SHARE.Components.Globals.UserNameSignRemote,
                Password = Company.KDT.SHARE.Components.Globals.PasswordSignRemote,
                SendStatus = SendStatusInfo.Request,
                Message = new SignRemote.Message()
                {
                    GuidStr = guidStr
                },
                To = GlobalSettings.MA_DON_VI.Trim()
            };
            msg = Helpers.Serializer(msgFeedBack, true, true);

            bool isWait = true;
            int countSend = 1;
            int countRequest = CountRequest(msg.Length);
            while (isWait && countSend < 5)
            {
                int count = countRequest;
                string title = this._title;
                this._title = string.Format("Thử lấy phản hồi ký số lần {0}", countSend);
                while (count > 0)
                {
                    SetCaption(string.Format("Chờ lấy thông tin {0} giây", count));
                    Thread.Sleep(1000);
                    count--;
                }
                this._title = title;
                msgRet = signRemote.ClientSend(msg);
                msgSend = Helpers.Deserialize<MessageSend>(msgRet);
                if (msgSend.SendStatus == SendStatusInfo.Error)
                {
                    throw new Exception("Thông báo từ hệ thống: " + msgSend.Warrning);
                }
                else if (msgSend.SendStatus == SendStatusInfo.Sign)
                {
                    return new MessageSignature()
                    {
                        Data = msgSend.Message.SignData,
                        FileCert = msgSend.Message.SignCert
                    };

                }
                else if (msgSend.SendStatus == SendStatusInfo.Wait)
                    countSend++;
            }
            throw new Exception(string.Format("ID={0}. Ký số từ xa thất bại\nMáy chủ đã không phản hồi thông tin trong thời gian giao dịch", guidStr));
        }
        private MessageSignature GetSignatureOnLan(string content)
        {
            string guidStr = Guid.NewGuid().ToString();
            string cnnString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { GlobalSettings.SERVER_NAME, Company.KDT.SHARE.Components.Globals.DataSignLan, GlobalSettings.USER, GlobalSettings.PASS });
            INBOX.InsertSignContent(cnnString, guidStr, content);
            int count = 0;
            int timeWait = 15;
            while (count < timeWait)
            {

                OUTBOX outbox = OUTBOX.GetContentSignLocal(cnnString, guidStr);
                if (outbox != null)
                {
                    return new MessageSignature()
                    {
                        Data = outbox.MSG_SIGN_DATA,
                        FileCert = outbox.MSG_SIGN_CERT
                    };
                }
                else
                {
                    count++;
                    SetCaption(string.Format("Chờ lấy thông tin {0} giây", count));
                    Thread.Sleep(1000);
                }
            }
            throw new Exception("Đã gửi yêu cầu ký để khai báo\r\nTuy nhiên máy chủ đã không ký thông tin yêu cầu\r\nVui lòng xem thiết lập chữ ký số trên máy chủ");

        }
        private void DoWork(object obj)
        {
            DialogResult result = DialogResult.Cancel;
            string sfmtFeedback = string.Empty;
            try
            {

                #region Build message Send
                SetCaption("Đóng gói dữ liệu");
                Thread.Sleep(300);
                MessageSend<string> msgSend = Helpers.Envelope<string>(_objectSend.From, _objectSend.To, _objectSend.Subject);
                string content = string.Empty;
                if (_objectSend.Subject.Function == DeclarationFunction.HOI_TRANG_THAI)
                {
                    string subType = _objectSend.Subject.Type;
                    _objectSend.Subject.Type = null;
                    content = Helpers.Serializer(_objectSend.Subject, System.Text.Encoding.UTF8, true, true);
                    _objectSend.Subject.Type = subType;
                }
                else
                    content = Helpers.Serializer(_objectSend.Content, _removeDeclartion, _removeNameSpace);

                MessageSignature signature = null;
                if (Company.KDT.SHARE.Components.Globals.IsSignature)
                {
                    SetCaption("Ký thông tin");
                    Thread.Sleep(300);
                    signature = Helpers.GetSignature(content);
                    SetCaption("Mã hóa thông tin");
                    content = Helpers.ConvertToBase64(content);
                }
                else if (Company.KDT.SHARE.Components.Globals.IsSignRemote)
                {
                    SetCaption("Mã hóa thông tin");
                    content = Helpers.ConvertToBase64(content);
                    SetCaption("Ký thông tin từ xa");
                    Thread.Sleep(300);
                    signature = GetSignatureRemote(content);
                }
                else if (Company.KDT.SHARE.Components.Globals.IsSignOnLan)
                {
                    if (string.IsNullOrEmpty(Company.KDT.SHARE.Components.Globals.DataSignLan))
                        throw new Exception("Loại ký số trong mạng nội bộ chưa xác nhận cơ sở dữ liệu\r\nVui lòng vào cấu hình chữ ký số để thiết lập");
                    SetCaption("Ký dữ liệu nội bộ");
                    content = Helpers.ConvertToBase64(content);
                    signature = GetSignatureOnLan(content);
                    Thread.Sleep(300);

                }
                else
                {
                    SetCaption("Mã hóa thông tin");
                    content = Helpers.ConvertToBase64(content);
                }
                msgSend.MessageBody.MessageContent.Text = content;
                if (signature != null)
                {
                    msgSend.MessageBody.MessageSignature = signature;
                }
                SetCaption("Đồng bộ thông tin");
                Thread.Sleep(300);
                if (_objectSend.Subject.Function == DeclarationFunction.HOI_TRANG_THAI)
                {
                    this._messageSend = Helpers.Serializer(msgSend, true, true);
                }
                else
                    this._messageSend = Helpers.Serializer(msgSend);
                #endregion


                Service = WebService.HQService(GlobalSettings.IsDaiLy,GlobalSettings.MA_DON_VI);

                if (_objectSend.Subject.Function == DeclarationFunction.HOI_TRANG_THAI)
                {
                    int count = Company.KDT.SHARE.Components.Globals.Delay;
                    while (count > 0)
                    {
                        SetCaption(string.Format("Còn lại {0} giây", count));
                        Thread.Sleep(1000);
                        count--;
                    }
                }
                SetCaption("Đang truyền dữ liệu lên Hải quan");
                this._lastStartTime = DateTime.Now;
                //Helpers.DoAction<Company.KDT.SHARE.Components.CisWS.CISServiceSoapClient>(Service, client =>
                //{
                sfmtFeedback = Service.Send(_messageSend, GlobalSettings.UserId, _password);
                //});
#if DEBUG
                //System.Xml.XmlDocument xmlTest = new System.Xml.XmlDocument();
                //xmlTest.Load("c:\\TK_Sua_6113_XSX01.xml");
                //sfmtFeedback = xmlTest.InnerXml;
#endif
                SetCaption("Truyền dữ liệu thành công");
                this.OnSend(new SendEventArgs(sfmtFeedback, this.TotalUsedTime, null));
                result = DialogResult.OK;
            }
            catch (Exception ex)
            {
                this._title = "Thực hiện thất bại";
                SetCaption("Truyền dữ liệu thất bại");
                string filename = string.Empty;
                string sfmt = string.Empty;
                if (ex.Message.Contains("Could not connect"))
                {

                    if (!GlobalSettings.IsDaiLy)
                        sfmt = string.Format("Địa chỉ:{0}\r\nKhông kết nối đến hệ thống Hải quan", WebService.LoadConfigure("WS_URL"));
                    else
                        sfmt = string.Format("Địa chỉ:{0}\r\nKhông kết nối đến hệ thống Hải quan", GlobalSettings.DiaChiWS);


                }
                else if (ex.Message.Contains("SendTimeout"))
                {
                    sfmt = string.Format("Đường truyền mạng internet bị ngắt kết nối khi đang truyền dữ liệu\r\n" +
                                               "Thời gian giữ kết nối internet là {0} giây\r\n", Company.KDT.SHARE.Components.Globals.TimeConnect);
                }
                else if (ex.Message.Contains("There was no endpoint listening at"))
                {
                    sfmt = string.Format("Địa chỉ:\r\n{0}\r\nĐịa chỉ không hợp lệ vì địa chỉ trên không phải là loại địa chỉ khai báo\r\nVui lòng thiết lập lại địa chỉ khai báo", WebService.LoadConfigure("WS_URL"));
                }
                else if (ex.Message.Contains("Maximum request length exceeded"))
                {

                    sfmt = string.Format("Dung lượng bạn gửi đi là {0} đã vượt mức cho phép của hệ thống Hải quan\r\n" +
                                        "Vui lòng giảm dung lượng(nếu có tệp tin đính kèm) đến mức tối thiểu cho phù hợp với hệ thống Hải quan", CalculateFileSize((decimal)_messageSend.Length));
                }
                else if (ex.Message.Contains("connection was closed"))
                {
                    sfmt = string.Format("Hệ thống đã đóng kết nối truy cập");
                }

                filename = Company.KDT.SHARE.Components.Globals.Message2File(_messageSend);
                if (!string.IsNullOrEmpty(sfmtFeedback))
                    filename = Company.KDT.SHARE.Components.Globals.Message2File(sfmtFeedback);

                if (!string.IsNullOrEmpty(sfmt))
                    ex = new Exception(sfmt);
                OnSend(new SendEventArgs(
                    filename, this.TotalUsedTime, ex));
            }
            finally
            {
                this.DialogResult = result;
            }
        }
        private string CalculateFileSize(decimal FileInBytes)
        {
            string strSize = "00";
            if (FileInBytes < 1024)
                strSize = FileInBytes + " B";//Byte
            else if (FileInBytes > 1024 & FileInBytes < 1048576)
                strSize = Math.Round((FileInBytes / 1024), 2) + " KB";//Kilobyte
            else if (FileInBytes > 1048576 & FileInBytes < 107341824)
                strSize = Math.Round((FileInBytes / 1024) / 1024, 2) + " MB";//Megabyte
            else if (FileInBytes > 107341824 & FileInBytes < 1099511627776)
                strSize = Math.Round(((FileInBytes / 1024) / 1024) / 1024, 2) + " GB";//Gigabyte
            else
                strSize = Math.Round((((FileInBytes / 1024) / 1024) / 1024) / 1024, 2) + " TB";//Terabyte
            return strSize;
        }
        public string Message
        {
            get
            {
                return this._messageSend;
            }
        }
        private void OnSend(SendEventArgs e)
        {
            if (Send != null)
            {
                Send(this, e);
            }
        }
        #endregion Process Send Message

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // SendMessageForm
            // 
            this.ClientSize = new System.Drawing.Size(284, 265);
            this.LookAndFeel.SkinName = "Office 2007 Blue";
            this.Name = "SendMessageForm";
            this.ResumeLayout(false);

        }

    }
    public enum CommandStatus
    {
        Send = 0,
        FeedBack,
        Cancel
    }
    interface ICommand
    {
        void Send();
    }
    class Command : ICommand
    {
        private SendMessageForm _sendForm = null;
        private string _message = string.Empty;
        public Command(SendMessageForm sendForm, string message)
        {
            _sendForm = sendForm;
            _message = message;
        }
        public void Send()
        {
            // _sendForm.DoSend(_message);
        }
    }
    public class SendCommand
    {
        public SendMessageForm SendMessageForm { get; set; }
        private Dictionary<CommandStatus, ICommand> _commands = new Dictionary<CommandStatus, ICommand>();
        public void Add(CommandStatus status, string message)
        {
            Remove(status);
            ICommand cmd = new Command(SendMessageForm, message);
            _commands.Add(status, cmd);
        }
        public void Clear()
        {
            _commands.Clear();
        }
        public void Remove(CommandStatus status)
        {
            _commands.Remove(status);
        }
        public void Send(CommandStatus status)
        {
            ICommand cmd = _commands[status];
            cmd.Send();
        }
    }

}

