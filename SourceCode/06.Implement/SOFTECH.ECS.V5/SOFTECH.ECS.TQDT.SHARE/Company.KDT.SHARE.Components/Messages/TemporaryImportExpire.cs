﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class TemporaryImportExpire
    {

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("statement")]
        public string Statement { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("content")]
        public string Content { get; set; }


    }
}
