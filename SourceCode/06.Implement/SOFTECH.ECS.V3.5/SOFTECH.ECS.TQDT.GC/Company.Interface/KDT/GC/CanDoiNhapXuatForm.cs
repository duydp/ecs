//using Infragistics.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.GC.BLL.KDT;
using Company.GC.BLL.GC;

namespace Company.Interface.KDT.GC
{
    public partial class CanDoiNhapXuatForm : BaseForm
    {
        public DataTable tableCanDoi;
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        Company.GC.BLL.KDT.GC.BKCungUngDangKy BKCU = new Company.GC.BLL.KDT.GC.BKCungUngDangKy();
        public long HopDongID;
        public CanDoiNhapXuatForm()
        {
            InitializeComponent();
        }

        private void CanDoiNhapXuatForm_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["DinhMucChung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["LuongTonDau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["LuongTonCuoi"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["LuongSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

            saveFileDialog1.InitialDirectory = Application.StartupPath;           
            this.dgList.DataSource = this.tableCanDoi;
            BKCungUng.Visible = Janus.Windows.UI.InheritableBoolean.False;
            BKCungUng1.Visible = Janus.Windows.UI.InheritableBoolean.False;
        }
       
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "BKCungUng":
                    this.BKCungUngForm();
                    break;
                case "XuatExcel":
                    ExportExcel();
                    break;              
            }
            
        }
        private void ExportExcel()
        {
            Workbook wb = new Workbook();
            Worksheet ws = wb.Worksheets.Add("CanDoiNPLXuatNhapTon");
            ws.Columns[0].Width = 5000;
            ws.Columns[1].Width = 7000;
            ws.Columns[4].Width = 7000;
            ws.Columns[3].Width = 7000;
            WorksheetRowCollection wsrc = ws.Rows;
            WorksheetRow wsr0 = ws.Rows[0];
            wsr0.Cells[0].Value = "Mã NPL";
            wsr0.Cells[1].Value = "Tên NPL";
            wsr0.Cells[2].Value = "Lượng NPL Tồn Đầu";
            wsr0.Cells[3].Value = "Lượng NPL Sử Dụng";
            wsr0.Cells[4].Value = "Lượng NPl Tồn Cuối";


            for (int i = 0; i < tableCanDoi.Rows.Count; i++)
            {
                WorksheetRow wsr = ws.Rows[i + 1];
                wsr.Cells[0].Value = tableCanDoi.Rows[i]["MaNPL"].ToString();
                NguyenPhuLieu NPL = new NguyenPhuLieu();
                NPL.Ma = tableCanDoi.Rows[i]["MaNPL"].ToString();
                NPL.HopDong_ID = TKMD.IDHopDong;
                NPL.Load();
                wsr.Cells[1].Value = NPL.Ten;
                wsr.Cells[2].Value = Convert.ToDecimal(tableCanDoi.Rows[i]["LuongTonDau"]);
                wsr.Cells[3].Value = Convert.ToDecimal(tableCanDoi.Rows[i]["LuongSuDung"]);
                wsr.Cells[4].Value = Convert.ToDecimal(tableCanDoi.Rows[i]["LuongTonCuoi"]);
            }
            DialogResult rs = saveFileDialog1.ShowDialog();
            if (rs == DialogResult.OK)
            {
                string fileName = saveFileDialog1.FileName;
                wb.Save(fileName);
            }
        }
        private Company.GC.BLL.KDT.GC.SanPhanCungUng CheckExitSP(string maSP)
        {
            foreach (Company.GC.BLL.KDT.GC.SanPhanCungUng spCungUng in BKCU.SanPhamCungUngCollection)
            {
                if (spCungUng.MaSanPham.Trim().ToUpper() == maSP.ToUpper().Trim())
                    return spCungUng;
            }
            return null;
        }

        private bool AddNguyenPhuLieuCungUngToSP(string maSP,Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUng NPLCungUng)
        {
            foreach (Company.GC.BLL.KDT.GC.SanPhanCungUng spCungUng in BKCU.SanPhamCungUngCollection)
            {
                if (spCungUng.MaSanPham.Trim().ToUpper() == maSP.ToUpper().Trim())
                {
                    if(spCungUng.NPLCungUngCollection==null)
                        spCungUng.NPLCungUngCollection=new Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUngCollection();
                    spCungUng.NPLCungUngCollection.Add(NPLCungUng);
                    return true;
                }
            }
            return false;
        }
        private void KiemTraBangKeCungUngExit()
        {
            for (int i = 0; i < BKCU.SanPhamCungUngCollection.Count;++i)
            {
                for (int j = 0; j < BKCU.SanPhamCungUngCollection[i].NPLCungUngCollection.Count; ++j)
                {
                    long id = BKCU.SanPhamCungUngCollection[i].NPLCungUngCollection[j].checkExitNPLCungUngTKMD(BKCU.SanPhamCungUngCollection[i].MaSanPham.Trim(), BKCU.SanPhamCungUngCollection[i].NPLCungUngCollection[j].MaNguyenPhuLieu, TKMD.ID, BKCU.ID);
                    if (id > 0)
                    {
                        string[] arr = new string[3];
                        arr[0] = BKCU.SanPhamCungUngCollection[i].NPLCungUngCollection[j].MaNguyenPhuLieu;
                        arr[1] = BKCU.SanPhamCungUngCollection[i].MaSanPham;
                        arr[2] = id.ToString();
                        if (showMsg("MSG_2702032", arr, true) == "Yes")
                        //if (ShowMessage("Nguyên phụ liệu cung ứng có mã " + BKCU.SanPhamCungUngCollection[i].NPLCungUngCollection[j].MaNguyenPhuLieu + " cho sản phẩm " + BKCU.SanPhamCungUngCollection[i].MaSanPham + " đã được khai báo trong danh sách cung ứng có ID=" + id.ToString() + " nên sẽ được bỏ qua.\n Bạn có muốn tiếp tục không?", true) != "Yes")
                        {
                            return;
                        }
                        else
                        {
                            BKCU.SanPhamCungUngCollection[i].NPLCungUngCollection.Remove(BKCU.SanPhamCungUngCollection[i].NPLCungUngCollection[j]);                            
                        }
                    }
                }
            }
        }
        private void BKCungUngForm()
        {
            if (TKMD.TrangThaiXuLy == -1)
            {
                showMsg("MSG_240227");
                //ShowMessage("Phải khai báo tờ khai xuất trước khi tạo bảng kê cung ứng.", false);
                return;
            }
            DataRow[] rowCollection = tableCanDoi.Select("LuongSuDung>LuongTonDau");
            BKCU.TKMD_ID = TKMD.ID;
            BKCU.SanPhamCungUngCollection = new Company.GC.BLL.KDT.GC.SanPhanCungUngCollection();
            if (rowCollection != null && rowCollection.Length > 0)
            {
                //dua cac san pham bi am vo bang ke cung ung
                foreach (DataRow row in rowCollection)
                {
                    string MaSP = row["MaSP"].ToString();
                    decimal TonDau = Convert.ToDecimal(row["LuongTonDau"]);
                    decimal LuongSuDung = Convert.ToDecimal(row["LuongSuDung"]);
                    decimal DinhMucChung = Convert.ToDecimal(row["DinhMucChung"]);
                    Company.GC.BLL.KDT.GC.SanPhanCungUng spCungUng = CheckExitSP(MaSP);
                    if (spCungUng==null)
                    {
                        spCungUng = new Company.GC.BLL.KDT.GC.SanPhanCungUng();            
                        if (TonDau > 0)
                            spCungUng.LuongCUSanPham = (LuongSuDung - TonDau) / DinhMucChung;
                        else
                            spCungUng.LuongCUSanPham = LuongSuDung / DinhMucChung;
                        if (BKCU.SanPhamCungUngCollection == null)
                            BKCU.SanPhamCungUngCollection = new Company.GC.BLL.KDT.GC.SanPhanCungUngCollection();
                        spCungUng.MaSanPham = MaSP;
                        spCungUng.NPLCungUngCollection = new Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUngCollection();
                        BKCU.SanPhamCungUngCollection.Add(spCungUng);
                    }
                    else
                    {
                        decimal LuongCUSP = 0;
                        if (TonDau > 0)
                            LuongCUSP = (LuongSuDung - TonDau) / DinhMucChung;
                        else
                            LuongCUSP = LuongSuDung / DinhMucChung;
                        if (spCungUng.LuongCUSanPham < LuongCUSP)
                            spCungUng.LuongCUSanPham = LuongCUSP;
                    }
                    string MaNPL = row["MaNPL"].ToString();
                    Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUng nplCungUng = new Company.GC.BLL.KDT.GC.NguyenPhuLieuCungUng();
                    DinhMuc DMSanPham=new DinhMuc();
                    DMSanPham.MaNguyenPhuLieu=MaNPL;
                    DMSanPham.MaSanPham=MaSP;
                    DMSanPham.HopDong_ID=TKMD.IDHopDong;
                    if(!DMSanPham.Load())
                    {
                        showMsg("MSG_2702033", new string[] { MaSP, MaNPL });
                        //ShowMessage("Sản phẩm : "+MaSP+" không có định mức với Nguyên phụ liệu :"+MaNPL,false);
                        return;
                    }
                    nplCungUng.DinhMucCungUng =Convert.ToDouble(DMSanPham.DinhMucSuDung);
                    nplCungUng.TyLeHH = Convert.ToDouble(DMSanPham.TyLeHaoHut);
                    nplCungUng.HinhThuCungUng = "Mua VN";
                    if (TonDau > 0)
                        nplCungUng.LuongCung = (LuongSuDung - TonDau);
                    else
                        nplCungUng.LuongCung = LuongSuDung;
                    nplCungUng.MaNguyenPhuLieu = MaNPL;                    
                    AddNguyenPhuLieuCungUngToSP(MaSP, nplCungUng);
                    
                }
                KiemTraBangKeCungUngExit();
                NPLCungUngTheoTKSendForm f = new NPLCungUngTheoTKSendForm();
                f.BKCU = BKCU;
                f.TKMD = TKMD;
                f.MdiParent = this.ParentForm;
               
                f.Show();
                f.Activate();
                this.Close();
                f.Focus();
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            NguyenPhuLieu NPL = new NguyenPhuLieu();
            NPL.Ma = e.Row.Cells["MaNPL"].Text;
            NPL.HopDong_ID = TKMD.IDHopDong == 0 ? HopDongID : TKMD.IDHopDong;
            NPL.Load();
            e.Row.Cells["TenNPL"].Text = NPL.Ten;
        }
    }
}

