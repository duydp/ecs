Alter table t_KDT_SXXK_PhuKienDetail
alter column DVTCu varchar(10)

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_PhuKienDetail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Insert]
	@Master_ID bigint,
	@NgayTKHeThong datetime,
	@NgayTKSuaDoi datetime,
	@SoHopDongCu varchar(80),
	@SoHopDongMoi varchar(80),
	@NgayHopDongCu datetime,
	@NgayHopDongMoi datetime,
	@STTHang int,
	@MaHangCu varchar(50),
	@LoaiHangCu varchar(2),
	@SoLuongCu numeric(18, 4),
	@DVTCu varchar(10),
	@MaHQDangKyCu varchar(6),
	@MaHangMoi varchar(50),
	@LoaiHangMoi varchar(2),
	@SoLuongMoi numeric(18, 4),
	@DVTMoi varchar(10),
	@MaHQDangKyMoi varchar(6),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_PhuKienDetail]
(
	[Master_ID],
	[NgayTKHeThong],
	[NgayTKSuaDoi],
	[SoHopDongCu],
	[SoHopDongMoi],
	[NgayHopDongCu],
	[NgayHopDongMoi],
	[STTHang],
	[MaHangCu],
	[LoaiHangCu],
	[SoLuongCu],
	[DVTCu],
	[MaHQDangKyCu],
	[MaHangMoi],
	[LoaiHangMoi],
	[SoLuongMoi],
	[DVTMoi],
	[MaHQDangKyMoi]
)
VALUES 
(
	@Master_ID,
	@NgayTKHeThong,
	@NgayTKSuaDoi,
	@SoHopDongCu,
	@SoHopDongMoi,
	@NgayHopDongCu,
	@NgayHopDongMoi,
	@STTHang,
	@MaHangCu,
	@LoaiHangCu,
	@SoLuongCu,
	@DVTCu,
	@MaHQDangKyCu,
	@MaHangMoi,
	@LoaiHangMoi,
	@SoLuongMoi,
	@DVTMoi,
	@MaHQDangKyMoi
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Update]
	@ID bigint,
	@Master_ID bigint,
	@NgayTKHeThong datetime,
	@NgayTKSuaDoi datetime,
	@SoHopDongCu varchar(80),
	@SoHopDongMoi varchar(80),
	@NgayHopDongCu datetime,
	@NgayHopDongMoi datetime,
	@STTHang int,
	@MaHangCu varchar(50),
	@LoaiHangCu varchar(2),
	@SoLuongCu numeric(18, 4),
	@DVTCu varchar(10),
	@MaHQDangKyCu varchar(6),
	@MaHangMoi varchar(50),
	@LoaiHangMoi varchar(2),
	@SoLuongMoi numeric(18, 4),
	@DVTMoi varchar(10),
	@MaHQDangKyMoi varchar(6)
AS

UPDATE
	[dbo].[t_KDT_SXXK_PhuKienDetail]
SET
	[Master_ID] = @Master_ID,
	[NgayTKHeThong] = @NgayTKHeThong,
	[NgayTKSuaDoi] = @NgayTKSuaDoi,
	[SoHopDongCu] = @SoHopDongCu,
	[SoHopDongMoi] = @SoHopDongMoi,
	[NgayHopDongCu] = @NgayHopDongCu,
	[NgayHopDongMoi] = @NgayHopDongMoi,
	[STTHang] = @STTHang,
	[MaHangCu] = @MaHangCu,
	[LoaiHangCu] = @LoaiHangCu,
	[SoLuongCu] = @SoLuongCu,
	[DVTCu] = @DVTCu,
	[MaHQDangKyCu] = @MaHQDangKyCu,
	[MaHangMoi] = @MaHangMoi,
	[LoaiHangMoi] = @LoaiHangMoi,
	[SoLuongMoi] = @SoLuongMoi,
	[DVTMoi] = @DVTMoi,
	[MaHQDangKyMoi] = @MaHQDangKyMoi
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@NgayTKHeThong datetime,
	@NgayTKSuaDoi datetime,
	@SoHopDongCu varchar(80),
	@SoHopDongMoi varchar(80),
	@NgayHopDongCu datetime,
	@NgayHopDongMoi datetime,
	@STTHang int,
	@MaHangCu varchar(50),
	@LoaiHangCu varchar(2),
	@SoLuongCu numeric(18, 4),
	@DVTCu varchar(10),
	@MaHQDangKyCu varchar(6),
	@MaHangMoi varchar(50),
	@LoaiHangMoi varchar(2),
	@SoLuongMoi numeric(18, 4),
	@DVTMoi varchar(10),
	@MaHQDangKyMoi varchar(6)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_PhuKienDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_PhuKienDetail] 
		SET
			[Master_ID] = @Master_ID,
			[NgayTKHeThong] = @NgayTKHeThong,
			[NgayTKSuaDoi] = @NgayTKSuaDoi,
			[SoHopDongCu] = @SoHopDongCu,
			[SoHopDongMoi] = @SoHopDongMoi,
			[NgayHopDongCu] = @NgayHopDongCu,
			[NgayHopDongMoi] = @NgayHopDongMoi,
			[STTHang] = @STTHang,
			[MaHangCu] = @MaHangCu,
			[LoaiHangCu] = @LoaiHangCu,
			[SoLuongCu] = @SoLuongCu,
			[DVTCu] = @DVTCu,
			[MaHQDangKyCu] = @MaHQDangKyCu,
			[MaHangMoi] = @MaHangMoi,
			[LoaiHangMoi] = @LoaiHangMoi,
			[SoLuongMoi] = @SoLuongMoi,
			[DVTMoi] = @DVTMoi,
			[MaHQDangKyMoi] = @MaHQDangKyMoi
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_PhuKienDetail]
		(
			[Master_ID],
			[NgayTKHeThong],
			[NgayTKSuaDoi],
			[SoHopDongCu],
			[SoHopDongMoi],
			[NgayHopDongCu],
			[NgayHopDongMoi],
			[STTHang],
			[MaHangCu],
			[LoaiHangCu],
			[SoLuongCu],
			[DVTCu],
			[MaHQDangKyCu],
			[MaHangMoi],
			[LoaiHangMoi],
			[SoLuongMoi],
			[DVTMoi],
			[MaHQDangKyMoi]
		)
		VALUES 
		(
			@Master_ID,
			@NgayTKHeThong,
			@NgayTKSuaDoi,
			@SoHopDongCu,
			@SoHopDongMoi,
			@NgayHopDongCu,
			@NgayHopDongMoi,
			@STTHang,
			@MaHangCu,
			@LoaiHangCu,
			@SoLuongCu,
			@DVTCu,
			@MaHQDangKyCu,
			@MaHangMoi,
			@LoaiHangMoi,
			@SoLuongMoi,
			@DVTMoi,
			@MaHQDangKyMoi
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_PhuKienDetail]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_PhuKienDetail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[NgayTKHeThong],
	[NgayTKSuaDoi],
	[SoHopDongCu],
	[SoHopDongMoi],
	[NgayHopDongCu],
	[NgayHopDongMoi],
	[STTHang],
	[MaHangCu],
	[LoaiHangCu],
	[SoLuongCu],
	[DVTCu],
	[MaHQDangKyCu],
	[MaHangMoi],
	[LoaiHangMoi],
	[SoLuongMoi],
	[DVTMoi],
	[MaHQDangKyMoi]
FROM
	[dbo].[t_KDT_SXXK_PhuKienDetail]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[NgayTKHeThong],
	[NgayTKSuaDoi],
	[SoHopDongCu],
	[SoHopDongMoi],
	[NgayHopDongCu],
	[NgayHopDongMoi],
	[STTHang],
	[MaHangCu],
	[LoaiHangCu],
	[SoLuongCu],
	[DVTCu],
	[MaHQDangKyCu],
	[MaHangMoi],
	[LoaiHangMoi],
	[SoLuongMoi],
	[DVTMoi],
	[MaHQDangKyMoi]
FROM [dbo].[t_KDT_SXXK_PhuKienDetail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_PhuKienDetail_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Friday, July 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_PhuKienDetail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[NgayTKHeThong],
	[NgayTKSuaDoi],
	[SoHopDongCu],
	[SoHopDongMoi],
	[NgayHopDongCu],
	[NgayHopDongMoi],
	[STTHang],
	[MaHangCu],
	[LoaiHangCu],
	[SoLuongCu],
	[DVTCu],
	[MaHQDangKyCu],
	[MaHangMoi],
	[LoaiHangMoi],
	[SoLuongMoi],
	[DVTMoi],
	[MaHQDangKyMoi]
FROM
	[dbo].[t_KDT_SXXK_PhuKienDetail]	

GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '18.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('18.8',GETDATE(), N' Cập nhật table và store phu kien detail')
END	



