﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Threading;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;
using System.Collections.Generic;
namespace Company.GC.BLL.KDT.GC
{
    public partial class PhuKienDangKy
    {
        public LoaiPhuKienCollection PKCollection = new LoaiPhuKienCollection();
        public string SoHopDong = "";
        public long checkExitsSoPhuKien(string soPhuKien)
        {
            string spName = "select id from  t_KDT_GC_PhuKienDangKy where sophukien=@sophukien and HopDong_ID=@HopDong_ID and id<>@ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            db.AddInParameter(dbCommand, "@sophukien", SqlDbType.VarChar, soPhuKien);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this.HopDong_ID);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this.ID);
            object o = db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(o);
        }

        public bool checkExitsSoPK(string soPhuKien, string MaHQ, string MaDN)
        {
            string spName = "select id from  t_KDT_GC_PhuKienDangKy where sophukien=@sophukien and MaHaiQuan=@MaHQ and MaDoanhNghiep = @MaDN";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            db.AddInParameter(dbCommand, "@sophukien", SqlDbType.VarChar, soPhuKien);
            db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.VarChar, MaHQ);
            db.AddInParameter(dbCommand, "@MaDN", SqlDbType.VarChar, MaDN);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return false;
            else return true;
        }

        public bool Load(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_PhuKienDangKy_LoadBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@NgayPhuKien", SqlDbType.DateTime, this._NgayPhuKien);
            this.db.AddInParameter(dbCommand, "@SoPhuKien", SqlDbType.VarChar, this._SoPhuKien);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);

            IDataReader reader = null;

            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayPhuKien"))) this._NgayPhuKien = reader.GetDateTime(reader.GetOrdinal("NgayPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiDuyet"))) this._NguoiDuyet = reader.GetString(reader.GetOrdinal("NguoiDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("VanBanChoPhep"))) this._VanBanChoPhep = reader.GetString(reader.GetOrdinal("VanBanChoPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) this._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoPhuKien"))) this._SoPhuKien = reader.GetString(reader.GetOrdinal("SoPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) this._GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) this._DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) this._LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) this._ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) this._GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) this._NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) this._HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("HuongdanPL"))) this._HuongdanPL = reader.GetString(reader.GetOrdinal("HuongdanPL"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool LoadKTX(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_PhuKienDangKy_LoadByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@NgayPhuKien", SqlDbType.DateTime, this._NgayPhuKien);
            this.db.AddInParameter(dbCommand, "@SoPhuKien", SqlDbType.VarChar, this._SoPhuKien);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);

            IDataReader reader = null;

            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayPhuKien"))) this._NgayPhuKien = reader.GetDateTime(reader.GetOrdinal("NgayPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiDuyet"))) this._NguoiDuyet = reader.GetString(reader.GetOrdinal("NguoiDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("VanBanChoPhep"))) this._VanBanChoPhep = reader.GetString(reader.GetOrdinal("VanBanChoPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) this._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoPhuKien"))) this._SoPhuKien = reader.GetString(reader.GetOrdinal("SoPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public static long GetIDPhuKienByHopDongAndSoPhuKien(string soPhuKien, long IdHopDong)
        {
            string spName = "select id from  t_KDT_GC_PhuKienDangKy where sophukien=@sophukien and HopDong_ID=@HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            db.AddInParameter(dbCommand, "@sophukien", SqlDbType.VarChar, soPhuKien);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IdHopDong);
            object o = db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(o);
        }
        public static long GetIDPhuKienByHopDongAndSoPhuKien(string soPhuKien, long IdHopDong, string name)
        {
            string spName = "select id from  t_KDT_GC_PhuKienDangKy where sophukien=@sophukien and HopDong_ID=@HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(name);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            db.AddInParameter(dbCommand, "@sophukien", SqlDbType.VarChar, soPhuKien);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IdHopDong);
            object o = db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(o);
        }
        public void LoadCollection()
        {
            LoaiPhuKien loaiPK = new LoaiPhuKien();
            loaiPK.Master_ID = this.ID;
            PKCollection = loaiPK.SelectCollectionBy_Master_ID();
        }
        public void LoadCollection(string database)
        {
            LoaiPhuKien loaiPK = new LoaiPhuKien();
            loaiPK.SetDabaseMoi(database);
            loaiPK.Master_ID = this.ID;
            PKCollection = loaiPK.SelectCollectionBy_Master_ID();
        }
        public void LoadCollection(SqlTransaction transaction, string database)
        {
            LoaiPhuKien loaiPK = new LoaiPhuKien();
            loaiPK.Master_ID = this.ID;
            PKCollection = loaiPK.SelectCollectionBy_Master_ID(transaction, database);
        }
        public bool InsertUpdateDongBoDuLieu(PhuKienDangKyCollection collection, HopDong HD)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    PhuKienDangKy pkdk = new PhuKienDangKy();
                    pkdk.HopDong_ID = HD.ID;
                    pkdk.DeleteBy_HopDong_ID(transaction);
                    foreach (PhuKienDangKy item in collection)
                    {
                        item.HopDong_ID = HD.ID;
                        if (item.ID == 0)
                            item.InsertTransaction(transaction);
                        else
                            item.UpdateTransaction(transaction);
                        foreach (LoaiPhuKien pk in item.PKCollection)
                        {
                            int stt = 1;
                            pk.Master_ID = item.ID;
                            if (pk.ID > 0)
                                pk.UpdateTransaction(transaction);
                            else
                                pk.InsertTransaction(transaction);
                            foreach (HangPhuKien hang in pk.HPKCollection)
                            {
                                hang.STTHang = stt++;
                                hang.Master_ID = pk.ID;
                                if (hang.ID == 0)
                                    hang.InsertTransaction(transaction);
                                else
                                    hang.UpdateTransaction(transaction);
                            }
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdateDongBoDuLieu(PhuKienDangKyCollection collection, HopDong HD, string dbName)
        {
            SetDabaseMoi(dbName);

            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    //PhuKienDangKy pkdk = new PhuKienDangKy();
                    //pkdk.HopDong_ID = HD.ID;
                    //pkdk.DeleteBy_HopDong_ID(transaction, dbName);

                    foreach (PhuKienDangKy item in collection)
                    {
                        item.HopDong_ID = HD.ID;
                        item.InsertUpdate(transaction, dbName);

                        PhuKienDangKy itemTemp = new PhuKienDangKy();
                        itemTemp.SoPhuKien = item.SoPhuKien;
                        itemTemp.NgayPhuKien = item.NgayPhuKien;
                        itemTemp.HopDong_ID = item.HopDong_ID;
                        itemTemp.MaHaiQuan = item.MaHaiQuan;
                        itemTemp.MaDoanhNghiep = item.MaDoanhNghiep;
                        itemTemp.Load(transaction, dbName);

                        foreach (LoaiPhuKien pk in item.PKCollection)
                        {
                            int stt = 1;
                            pk.Master_ID = itemTemp.ID;
                            pk.InsertUpdateTransaction(transaction, dbName);

                            LoaiPhuKien pkTemp = new LoaiPhuKien();
                            SetDabaseMoi(dbName);
                            pkTemp.MaPhuKien = pk.MaPhuKien;
                            pkTemp.NoiDung = pk.NoiDung;
                            pkTemp.Master_ID = itemTemp.ID;
                            pkTemp.Load(transaction, dbName);

                            foreach (HangPhuKien hang in pk.HPKCollection)
                            {
                                hang.STTHang = stt++;
                                hang.Master_ID = pkTemp.ID;
                                hang.InsertUpdateTransaction(transaction, dbName);
                            }
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdateDongBoDuLieuKTX(PhuKienDangKyCollection collection, HopDong HD)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    PhuKienDangKy pkdk = new PhuKienDangKy();
                    pkdk.HopDong_ID = HD.ID;
                    pkdk.DeleteBy_HopDong_ID(transaction);
                    foreach (PhuKienDangKy item in collection)
                    {
                        item.HopDong_ID = HD.ID;
                        if (item.ID == 0)
                            item.InsertTransactionKTX(transaction);
                        else
                            item.UpdateTransactionKTX(transaction);
                        foreach (LoaiPhuKien pk in item.PKCollection)
                        {
                            int stt = 1;
                            pk.Master_ID = item.ID;
                            if (pk.ID > 0)
                                pk.UpdateTransaction(transaction);
                            else
                                pk.InsertTransaction(transaction);
                            foreach (HangPhuKien hang in pk.HPKCollection)
                            {
                                hang.STTHang = stt++;
                                hang.Master_ID = pk.ID;
                                if (hang.ID == 0)
                                    hang.InsertTransaction(transaction);
                                else
                                    hang.UpdateTransaction(transaction);
                            }
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdateDongBoDuLieuKTX(PhuKienDangKyCollection collection, HopDong HD, string dbName)
        {
            SetDabaseMoi(dbName);

            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    //PhuKienDangKy pkdk = new PhuKienDangKy();
                    //pkdk.HopDong_ID = HD.ID;
                    //pkdk.DeleteBy_HopDong_ID(transaction, dbName);

                    foreach (PhuKienDangKy item in collection)
                    {
                        item.HopDong_ID = HD.ID;
                        item.InsertUpdateKTX(transaction, dbName);

                        PhuKienDangKy itemTemp = new PhuKienDangKy();
                        itemTemp.SoPhuKien = item.SoPhuKien;
                        itemTemp.NgayPhuKien = item.NgayPhuKien;
                        itemTemp.HopDong_ID = item.HopDong_ID;
                        itemTemp.MaHaiQuan = item.MaHaiQuan;
                        itemTemp.MaDoanhNghiep = item.MaDoanhNghiep;
                        itemTemp.LoadKTX(transaction, dbName);

                        foreach (LoaiPhuKien pk in item.PKCollection)
                        {
                            int stt = 1;
                            pk.Master_ID = itemTemp.ID;
                            pk.InsertUpdateTransaction(transaction, dbName);

                            LoaiPhuKien pkTemp = new LoaiPhuKien();
                            SetDabaseMoi(dbName);
                            pkTemp.MaPhuKien = pk.MaPhuKien;
                            pkTemp.NoiDung = pk.NoiDung;
                            pkTemp.Master_ID = itemTemp.ID;
                            pkTemp.Load(transaction, dbName);

                            foreach (HangPhuKien hang in pk.HPKCollection)
                            {
                                hang.STTHang = stt++;
                                hang.Master_ID = pkTemp.ID;
                                hang.InsertUpdateTransaction(transaction, dbName);
                            }
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        //---------------------------------------------------------------------------------------------

        public long InsertTransactionTQDT(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_PhuKienDangKy_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@NgayPhuKien", SqlDbType.DateTime, this._NgayPhuKien);
            this.db.AddInParameter(dbCommand, "@NguoiDuyet", SqlDbType.NVarChar, this._NguoiDuyet);
            this.db.AddInParameter(dbCommand, "@VanBanChoPhep", SqlDbType.NVarChar, this._VanBanChoPhep);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@SoPhuKien", SqlDbType.VarChar, this._SoPhuKien);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@HuongdanPL", SqlDbType.NVarChar, this._HuongdanPL);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        public long InsertTransactionKTX(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_PhuKienDangKy_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@NgayPhuKien", SqlDbType.DateTime, this._NgayPhuKien);
            this.db.AddInParameter(dbCommand, "@NguoiDuyet", SqlDbType.NVarChar, this._NguoiDuyet);
            this.db.AddInParameter(dbCommand, "@VanBanChoPhep", SqlDbType.NVarChar, this._VanBanChoPhep);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@SoPhuKien", SqlDbType.VarChar, this._SoPhuKien);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        public long InsertTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_PhuKienDangKy_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@NgayPhuKien", SqlDbType.DateTime, this._NgayPhuKien);
            this.db.AddInParameter(dbCommand, "@NguoiDuyet", SqlDbType.NVarChar, this._NguoiDuyet);
            this.db.AddInParameter(dbCommand, "@VanBanChoPhep", SqlDbType.NVarChar, this._VanBanChoPhep);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@SoPhuKien", SqlDbType.VarChar, this._SoPhuKien);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransactionTQDT(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_PhuKienDangKy_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@NgayPhuKien", SqlDbType.DateTime, this._NgayPhuKien);
            this.db.AddInParameter(dbCommand, "@NguoiDuyet", SqlDbType.NVarChar, this._NguoiDuyet);
            this.db.AddInParameter(dbCommand, "@VanBanChoPhep", SqlDbType.NVarChar, this._VanBanChoPhep);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@SoPhuKien", SqlDbType.VarChar, this._SoPhuKien);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@HuongdanPL", SqlDbType.NVarChar, this._HuongdanPL);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int UpdateTransactionKTX(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_PhuKienDangKy_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@NgayPhuKien", SqlDbType.DateTime, this._NgayPhuKien);
            this.db.AddInParameter(dbCommand, "@NguoiDuyet", SqlDbType.NVarChar, this._NguoiDuyet);
            this.db.AddInParameter(dbCommand, "@VanBanChoPhep", SqlDbType.NVarChar, this._VanBanChoPhep);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@SoPhuKien", SqlDbType.VarChar, this._SoPhuKien);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int UpdateTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_PhuKienDangKy_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@NgayPhuKien", SqlDbType.DateTime, this._NgayPhuKien);
            this.db.AddInParameter(dbCommand, "@NguoiDuyet", SqlDbType.NVarChar, this._NguoiDuyet);
            this.db.AddInParameter(dbCommand, "@VanBanChoPhep", SqlDbType.NVarChar, this._VanBanChoPhep);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@SoPhuKien", SqlDbType.VarChar, this._SoPhuKien);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdate(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_PhuKienDangKy_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@NgayPhuKien", SqlDbType.DateTime, this._NgayPhuKien);
            this.db.AddInParameter(dbCommand, "@NguoiDuyet", SqlDbType.NVarChar, this._NguoiDuyet);
            this.db.AddInParameter(dbCommand, "@VanBanChoPhep", SqlDbType.NVarChar, this._VanBanChoPhep);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@SoPhuKien", SqlDbType.VarChar, this._SoPhuKien);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, this._GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, this._DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, this._LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, this._ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, this._GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, this._NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, this._HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@HuongdanPL", SqlDbType.NVarChar, this._HuongdanPL);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateKTX(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_PhuKienDangKy_InsertUpdateByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@NgayPhuKien", SqlDbType.DateTime, this._NgayPhuKien);
            this.db.AddInParameter(dbCommand, "@NguoiDuyet", SqlDbType.NVarChar, this._NguoiDuyet);
            this.db.AddInParameter(dbCommand, "@VanBanChoPhep", SqlDbType.NVarChar, this._VanBanChoPhep);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@SoPhuKien", SqlDbType.VarChar, this._SoPhuKien);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int DeleteBy_MaDoanhNghiep(SqlTransaction transaction)
        {
            string spName = "delete t_KDT_GC_PhuKienDangKy where maDoanhNghiep='" + this.MaDoanhNghiep + "'";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);


            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }

        public bool InsertUpdateDongBoDuLieu(PhuKienDangKyCollection collection, string MaDoanhNghiep)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    PhuKienDangKy pkdk = new PhuKienDangKy();
                    pkdk.MaDoanhNghiep = MaDoanhNghiep;
                    pkdk.DeleteBy_MaDoanhNghiep(transaction);
                    foreach (PhuKienDangKy item in collection)
                    {
                        item.InsertTransaction(transaction);
                        foreach (LoaiPhuKien pk in item.PKCollection)
                        {
                            int stt = 1;
                            pk.Master_ID = item.ID;
                            pk.InsertTransaction(transaction);

                            foreach (HangPhuKien hang in pk.HPKCollection)
                            {
                                hang.STTHang = stt++;
                                hang.Master_ID = pk.ID;
                                hang.InsertTransaction(transaction);
                            }
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateDongBoDuLieuMoi(PhuKienDangKyCollection collection, HopDong HD)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;

                    foreach (PhuKienDangKy item in collection)
                    {
                        item.HopDong_ID = HD.ID;
                        if (item.ID == 0)
                            item.InsertTransaction(transaction);
                        else
                            item.UpdateTransaction(transaction);
                        foreach (LoaiPhuKien pk in item.PKCollection)
                        {
                            int stt = 1;
                            pk.Master_ID = item.ID;
                            if (pk.ID > 0)
                                pk.UpdateTransaction(transaction);
                            else
                                pk.InsertTransaction(transaction);
                            foreach (HangPhuKien hang in pk.HPKCollection)
                            {
                                hang.STTHang = stt++;
                                hang.Master_ID = pk.ID;
                                if (hang.ID == 0)
                                    hang.InsertTransaction(transaction);
                                else
                                    hang.UpdateTransaction(transaction);
                            }
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateDongBoDuLieuMoiKTX(PhuKienDangKyCollection collection, HopDong HD)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;

                    foreach (PhuKienDangKy item in collection)
                    {
                        item.HopDong_ID = HD.ID;
                        if (item.ID == 0)
                            item.InsertTransactionKTX(transaction);
                        else
                            item.UpdateTransactionKTX(transaction);
                        foreach (LoaiPhuKien pk in item.PKCollection)
                        {
                            int stt = 1;
                            pk.Master_ID = item.ID;
                            if (pk.ID > 0)
                                pk.UpdateTransaction(transaction);
                            else
                                pk.InsertTransaction(transaction);
                            foreach (HangPhuKien hang in pk.HPKCollection)
                            {
                                hang.STTHang = stt++;
                                hang.Master_ID = pk.ID;
                                if (hang.ID == 0)
                                    hang.InsertTransaction(transaction);
                                else
                                    hang.UpdateTransaction(transaction);
                            }
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateDongBoDuLieuMoiKTX(PhuKienDangKyCollection collection, HopDong HD, string databaseName)
        {
            SetDabaseMoi(databaseName);

            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;

                    foreach (PhuKienDangKy item in collection)
                    {
                        item.HopDong_ID = HD.ID;
                        if (item.ID == 0)
                            item.InsertTransactionKTX(transaction, databaseName);
                        else
                            item.UpdateTransactionKTX(transaction, databaseName);

                        foreach (LoaiPhuKien pk in item.PKCollection)
                        {
                            int stt = 1;
                            pk.Master_ID = item.ID;
                            if (pk.ID > 0)
                                pk.UpdateTransaction(transaction, databaseName);
                            else
                                pk.InsertTransaction(transaction, databaseName);

                            foreach (HangPhuKien hang in pk.HPKCollection)
                            {
                                hang.STTHang = stt++;
                                hang.Master_ID = pk.ID;
                                if (hang.ID == 0)
                                    hang.InsertTransaction(transaction, databaseName);
                                else
                                    hang.UpdateTransaction(transaction, databaseName);
                            }
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateDongBoDuLieuMoi(PhuKienDangKyCollection collection)
        {
            bool ret;
            PhuKienDangKyCollection pkdkTMP = new PhuKienDangKyCollection();
            foreach (PhuKienDangKy pkdkt in collection)
            {
                if (Company.GC.BLL.KDT.GC.PhuKienDangKy.GetIDPhuKienByHopDongAndSoPhuKien(pkdkt.SoPhuKien, pkdkt.HopDong_ID) > 0)
                    continue;
                pkdkTMP.Add(pkdkt);
            }
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;

                    foreach (PhuKienDangKy item in pkdkTMP)
                    {
                        if (Company.GC.BLL.KDT.GC.PhuKienDangKy.GetIDPhuKienByHopDongAndSoPhuKien(item.SoPhuKien, item.HopDong_ID) > 0)
                            continue;
                        item.InsertTransaction(transaction);
                        foreach (LoaiPhuKien pk in item.PKCollection)
                        {
                            int stt = 1;
                            pk.Master_ID = item.ID;
                            if (pk.ID > 0)
                                pk.UpdateTransaction(transaction);
                            else
                                pk.InsertTransaction(transaction);
                            foreach (HangPhuKien hang in pk.HPKCollection)
                            {
                                hang.STTHang = stt++;
                                hang.Master_ID = pk.ID;
                                if (hang.ID == 0)
                                    hang.InsertTransaction(transaction);
                                else
                                    hang.UpdateTransaction(transaction);
                            }
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }


        public void InsertUpDatePhuKienXuLyDuLieu(SqlTransaction transaction)
        {
            foreach (LoaiPhuKien pk in PKCollection)
            {
                pk.UpdateTransaction(transaction);
                //cap nhat vao he thong xu lys chung
                // Update by KhanhHN - thay đổi mã phụ kiện - 07/05/2012
                //Gia han hop dong
                if (pk.MaPhuKien.Trim() == "201" && this.TrangThaiXuLy == 1)
                {
                    HopDong HD = new HopDong();
                    HD.ID = this.HopDong_ID;
                    HD.Load(transaction);
                    DateTimeFormatInfo dtF = new CultureInfo("vi-VN").DateTimeFormat;
                    HD.NgayGiaHan = Convert.ToDateTime(pk.ThongTinMoi, dtF);
                    HD.Update(transaction);
                }
                //Hủy HĐ  
                else if (pk.MaPhuKien.Trim() == "101" && this.TrangThaiXuLy == 1)
                {
                    HopDong HD = new HopDong();
                    HD.ID = this.HopDong_ID;
                    HD.Load(transaction);
                    HD.TrangThaiXuLy = -2;//-2 la huy hop dong
                    HD.Update(transaction);
                }
                //Mở phụ kiện độc lập
                else if (pk.MaPhuKien.Trim() == "H11" && this.TrangThaiXuLy == 1)
                {

                }

                foreach (HangPhuKien hang in pk.HPKCollection)
                {
                    //Bổ sung danh mục NPL
                    if (pk.MaPhuKien.Trim() == "803")
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl.DVT_ID = hang.DVT_ID;
                        npl.HopDong_ID = this.HopDong_ID;
                        npl.Ten = hang.TenHang;
                        npl.SoLuongDangKy = hang.SoLuong;
                        npl.Ma = hang.MaHang;
                        npl.MaHS = hang.MaHS;
                        if (this.TrangThaiXuLy == 1)
                            npl.TrangThai = 0;
                        else
                            npl.TrangThai = 2;
                        npl.InsertUpdateTransaction(transaction);
                    }


                    ////Điều chỉnh tổng nhu cầu NPL  
                    //else if (pk.MaPhuKien.Trim() == "N05")
                    //{
                    //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    //    npl.Ma = hang.MaHang;
                    //    npl.HopDong_ID = this.HopDong_ID;
                    //    npl.Load(transaction);
                    //    npl.SoLuongDangKy = hang.SoLuong;
                    //    //try
                    //    //{
                    //    //    decimal SoLuongcu = Convert.ToDecimal(hang.ThongTinCu);
                    //    //    npl.SoLuongDangKy += SoLuongcu;
                    //    //}
                    //    //catch { }
                    //    if (this.TrangThaiXuLy == 1)
                    //        npl.TrangThai = 0;
                    //    else
                    //        npl.TrangThai = 5;
                    //    npl.UpdateTransaction(transaction);
                    //}


                    //Thay đổi NPL
                    else if (pk.MaPhuKien.Trim() == "503")
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl.Ma = hang.MaHang;
                        npl.HopDong_ID = this.HopDong_ID;
                        npl.Load(transaction);
                        npl.DVT_ID = hang.DVT_ID;
                        npl.Ten = hang.TenHang;
                        npl.Ma = hang.MaHang;
                        npl.MaHS = hang.MaHS;
                        npl.SoLuongDangKy = hang.SoLuong;
                        if (this.TrangThaiXuLy == 1)
                            npl.TrangThai = 0;
                        else
                            npl.TrangThai = 3;
                        npl.UpdateTransaction(transaction);
                    }
                    //Mua NPL tại thị trường VN
                    else if (pk.MaPhuKien.Trim() == "N11")
                    {
                        //Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        //npl.Ma = hang.MaHang;
                        //npl.HopDong_ID = this.HopDong_ID;
                        //npl.Load(transaction);
                        //npl.SoLuongCungUng += hang.SoLuong;
                        //npl.TrangThai = 0;
                        //npl.UpdateTransaction(transaction);
                    }
                    // Sửa sản phẩm
                    else if (pk.MaPhuKien.Trim() == "502")
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.Ma = hang.MaHang;
                        sp.HopDong_ID = this.HopDong_ID;
                        sp.Load(transaction);
                        sp.DVT_ID = hang.DVT_ID;
                        sp.Ten = hang.TenHang;
                        sp.Ma = hang.MaHang;
                        sp.MaHS = hang.MaHS;
                        sp.SoLuongDangKy = hang.SoLuong;
                        if (this.TrangThaiXuLy == 1)
                            sp.TrangThai = 0;
                        else
                            sp.TrangThai = 3;
                        sp.UpdateTransaction(transaction);
                    }
                    // Sửa thiết bị
                    else if (pk.MaPhuKien.Trim() == "504")
                    {
                        Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                        tb.Ma = hang.MaHang;
                        tb.HopDong_ID = this.HopDong_ID;
                        tb.Load(transaction);
                        tb.DVT_ID = hang.DVT_ID;
                        tb.Ten = hang.TenHang;
                        tb.Ma = hang.MaHang;
                        tb.MaHS = hang.MaHS;
                        tb.SoLuongDangKy = hang.SoLuong;
                        if (this.TrangThaiXuLy == 1)
                            tb.TrangThai = 0;
                        else
                            tb.TrangThai = 3;
                        tb.UpdateTransaction(transaction);
                    }
                    // sửa hàng mẫu
                    else if (pk.MaPhuKien.Trim() == "505")
                    {
                        Company.GC.BLL.GC.HangMau hm = new Company.GC.BLL.GC.HangMau();
                        hm.Ma = hang.MaHang;
                        hm.HopDong_ID = this.HopDong_ID;
                        hm = Company.GC.BLL.GC.HangMau.Load(hm.HopDong_ID, hm.Ma);
                        hm.DVT_ID = hang.DVT_ID;
                        hm.Ten = hang.TenHang;
                        hm.Ma = hang.MaHang;
                        hm.MaHS = hang.MaHS;
                        hm.SoLuongDangKy = hang.SoLuong;
                        hm.Update(transaction);
                    }
                    //Điều chỉnh mã SP  
                    //else if (pk.MaPhuKien.Trim() == "S10")
                    //{
                    //    //Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                    //    //sp.Ma = hang.ThongTinCu;
                    //    //sp.HopDong_ID = this.HopDong_ID;
                    //    //sp.Load(transaction);
                    //    //if (this.TrangThaiXuLy == 1)
                    //    //    sp.TrangThai = 0;
                    //    //sp.Ma = hang.MaHang;
                    //    //sp.UpdateMaSPTransaction(transaction, hang.ThongTinCu);
                    //    ////cap nhat lai dinh muc neu co
                    //    //Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                    //    //dm.HopDong_ID = this.HopDong_ID;
                    //    //dm.MaSanPham = sp.Ma;
                    //    //dm.UpdateMaDinhMucTransaction(transaction, hang.ThongTinCu);
                    //}
                    //Chi tiết mã SP  
                    //else if (pk.MaPhuKien.Trim() == "S13")
                    //{
                    //    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                    //    sp.Ma = hang.MaHang;
                    //    sp.HopDong_ID = this.HopDong_ID;
                    //    sp.DVT_ID = hang.DVT_ID;
                    //    sp.MaHS = hang.MaHS;
                    //    sp.NhomSanPham_ID = hang.NhomSP;
                    //    sp.SoLuongDangKy = hang.SoLuong;
                    //    sp.Ten = hang.TenHang;
                    //    if (this.TrangThaiXuLy == 1)
                    //        sp.TrangThai = 0;
                    //    else
                    //        sp.TrangThai = 2;
                    //    sp.InsertUpdateTransaction(transaction);
                    //}

                    //Bổ sung SP cho Hợp đồng
                    else if (pk.MaPhuKien.Trim() == "802")
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.Ma = hang.MaHang;
                        sp.HopDong_ID = this.HopDong_ID;
                        sp.DVT_ID = hang.DVT_ID;
                        sp.Ten = hang.TenHang;
                        sp.Ma = hang.MaHang;
                        sp.MaHS = hang.MaHS;
                        sp.SoLuongDangKy = hang.SoLuong;
                        sp.NhomSanPham_ID = hang.NhomSP;
                        sp.DonGia = (decimal)hang.DonGia;
                        sp.InsertUpdateTransaction(transaction);
                    }
                    //Bổ sung Thiết bị cho Hợp đồng
                    else if (pk.MaPhuKien.Trim() == "804")
                    {
                        Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                        tb.Ma = hang.MaHang;
                        tb.HopDong_ID = this.HopDong_ID;
                        tb.DVT_ID = hang.DVT_ID;
                        tb.Ten = hang.TenHang;
                        tb.Ma = hang.MaHang;
                        tb.MaHS = hang.MaHS;
                        tb.SoLuongDangKy = hang.SoLuong;
                        tb.DonGia = hang.DonGia;
                        tb.InsertUpdateTransaction(transaction);
                    }
                    // Bổ sung hàng mẫu cho Hợp đồng
                    else if (pk.MaPhuKien.Trim() == "805")
                    {
                        Company.GC.BLL.GC.HangMau hm = new Company.GC.BLL.GC.HangMau();
                        hm.Ma = hang.MaHang;
                        hm.HopDong_ID = this.HopDong_ID;
                        hm.DVT_ID = hang.DVT_ID;
                        hm.Ten = hang.TenHang;
                        hm.Ma = hang.MaHang;
                        hm.MaHS = hang.MaHS;
                        hm.SoLuongDangKy = hang.SoLuong;
                        hm.DonGia = (decimal) hang.DonGia;
                        hm.InsertUpdate(transaction);
                    }
                  

                    //Điều chỉnh tăng/ giảm thiết bị tạm nhập
                    //else if (pk.MaPhuKien.Trim() == "T01")
                    //{
                    //    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                    //    tb.Ma = hang.MaHang;
                    //    tb.HopDong_ID = this.HopDong_ID;
                    //    tb.Load(transaction);
                    //    if (this.TrangThaiXuLy == 1)
                    //        tb.TrangThai = 0;
                    //    else
                    //        tb.TrangThai = 5;
                    //    tb.SoLuongDangKy = hang.SoLuong;
                    //    tb.UpdateTransaction(transaction);
                    //}
                    ////Bổ sung danh mục TB tạm nhập
                    //else if (pk.MaPhuKien.Trim() == "T07")
                    //{
                    //    BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                    //    entity.HopDong_ID = this.HopDong_ID;
                    //    entity.Ma = hang.MaHang;
                    //    entity.DVT_ID = hang.DVT_ID;
                    //    entity.MaHS = hang.MaHS;
                    //    entity.DonGia = hang.DonGia;
                    //    entity.SoLuongDangKy = hang.SoLuong;
                    //    entity.Ten = hang.TenHang;
                    //    entity.NguyenTe_ID = hang.NguyenTe_ID;
                    //    entity.NuocXX_ID = hang.NuocXX_ID;
                    //    entity.TinhTrang = hang.TinhTrang;
                    //    entity.TriGia = hang.TriGia;
                    //    if (this.TrangThaiXuLy == 1)
                    //        entity.TrangThai = 0;
                    //    else
                    //        entity.TrangThai = 2;
                    //    entity.InsertUpdateTransaction(transaction);
                    //}
                    // Hủy đăng ký nguyên phụ liệu
                    if (pk.MaPhuKien.Trim() == "103")
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl.Ma = hang.MaHang;
                        npl.HopDong_ID = this.HopDong_ID;
                        npl.Load(transaction);
                        if (this.TrangThaiXuLy == 1)
                            {
                            npl.TrangThai = 0;
                            npl.Delete();
                            }
                        else
                            npl.TrangThai = 2;
                        
                    }
                    // Hủy đăng ký sản phẩm 
                    if (pk.MaPhuKien.Trim() == "102")
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.Ma = hang.MaHang;
                        sp.HopDong_ID = this.HopDong_ID;
                        sp.Load(transaction);
                        if (this.TrangThaiXuLy == 1)
                        {
                            sp.TrangThai = 0;
                            sp.Delete();
                        }
                        else
                            sp.TrangThai = 2;

                    }
                    // Hủy đăng ký Thiết bị
                    if (pk.MaPhuKien.Trim() == "104")
                    {   
                        Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                        tb.Ma = hang.MaHang;
                        tb.HopDong_ID = this.HopDong_ID;
                        tb.Load(transaction);
                        if (this.TrangThaiXuLy == 1)
                        {
                            tb.TrangThai = 0;
                            tb.Delete();
                        }
                        else
                            tb.TrangThai = 2;

                    }
                    // Hủy đăng ký hàng mẫu

                    if (pk.MaPhuKien.Trim() == "105")
                    {
                        Company.GC.BLL.GC.HangMau hm = new Company.GC.BLL.GC.HangMau();
                        hm.Ma = hang.MaHang;
                        hm.HopDong_ID = this.HopDong_ID;
                        if (this.TrangThaiXuLy == 1)
                        {
                            hm.TrangThai = 0;
                            hm.Delete();
                        }
                        else
                            hm.TrangThai = 2;

                    }
                    
                }
            }
        }

        public void InsertUpDatePhuKienDongBoDuLieu(string Database)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(Database);
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                long id = this.ID;
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);
                    foreach (LoaiPhuKien pk in PKCollection)
                    {
                        if (id == 0)
                            pk.ID = 0;
                        int stt = 1;
                        pk.Master_ID = this.ID;
                        if (pk.ID > 0)
                            pk.UpdateTransaction(transaction);
                        else
                            pk.InsertTransaction(transaction);
                        foreach (HangPhuKien hang in pk.HPKCollection)
                        {
                            if (id == 0)
                                hang.ID = 0;
                            hang.STTHang = stt++;
                            hang.Master_ID = pk.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public void InsertUpDatePhuKienDongBoDuLieu()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                long id = this.ID;
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);
                    foreach (LoaiPhuKien pk in PKCollection)
                    {
                        if (id == 0)
                            pk.ID = 0;
                        int stt = 1;
                        pk.Master_ID = this.ID;
                        if (pk.ID > 0)
                            pk.UpdateTransaction(transaction);
                        else
                            pk.InsertTransaction(transaction);
                        foreach (HangPhuKien hang in pk.HPKCollection)
                        {
                            if (id == 0)
                                hang.ID = 0;
                            hang.STTHang = stt++;
                            hang.Master_ID = pk.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public void InsertPhuKienDongBoDuLieu()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.ID = this.InsertTransaction(transaction);
                    foreach (LoaiPhuKien pk in PKCollection)
                    {
                        int stt = 1;
                        pk.Master_ID = this.ID;
                        long pkID = pk.InsertTransaction(transaction);
                        foreach (HangPhuKien hang in pk.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = pkID;
                            hang.InsertTransaction(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
        public void InsertUpDateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                long id = this.ID;
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);
                    foreach (LoaiPhuKien pk in PKCollection)
                    {
                        int stt = 1;
                        pk.Master_ID = this.ID;
                        if (pk.ID > 0)
                            pk.UpdateTransaction(transaction);
                        else
                            pk.InsertTransaction(transaction);
                        //cap nhat vao he thong xu lys chung
                        //#region Gia han hop dong
                        //if (pk.MaPhuKien == "H06")
                        //{
                        //    HopDong HD = new HopDong();
                        //    HD.ID = this.HopDong_ID;
                        //    HD.Load(transaction);
                        //    DateTimeFormatInfo dtF = new CultureInfo("vi-VN").DateTimeFormat;
                        //    HD.NgayGiaHan = Convert.ToDateTime(pk.ThongTinMoi, dtF);
                        //    HD.UpdateTransaction(transaction);
                        //}
                        //else if (pk.MaPhuKien == "H10")
                        //{
                        //    HopDong HD = new HopDong();
                        //    HD.ID = this.HopDong_ID;
                        //    HD.Load(transaction);
                        //    HD.TrangThaiXuLy = -2;//-2 la huy hop dong
                        //    HD.UpdateTransaction(transaction);
                        //}
                        //else if (pk.MaPhuKien == "H11")
                        //{
                        //    ;
                        //}
                        //#endregion Gia han hop dong
                        foreach (HangPhuKien hang in pk.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = pk.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);
                            if (pk.MaPhuKien == "N01")
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                npl.DVT_ID = hang.DVT_ID;
                                npl.HopDong_ID = this.HopDong_ID;
                                npl.Ten = hang.TenHang;
                                npl.SoLuongDangKy = hang.SoLuong;
                                npl.Ma = hang.MaHang;
                                npl.MaHS = hang.MaHS;
                                npl.TrangThai = 2;
                                npl.InsertUpdateTransaction(transaction);
                            }
                            //else if (pk.MaPhuKien == "N05")
                            //{
                            //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            //    npl.Ma = hang.MaHang;
                            //    npl.HopDong_ID = this.HopDong_ID;
                            //    npl.Load(transaction);
                            //    npl.SoLuongDangKy = hang.SoLuong;
                            //    npl.UpdateTransaction(transaction);
                            //}
                            //else if (pk.MaPhuKien == "N06")
                            //{
                            //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            //    npl.Ma = hang.MaHang;
                            //    npl.HopDong_ID = this.HopDong_ID;
                            //    npl.Load(transaction);
                            //    npl.DVT_ID = hang.DVT_ID;
                            //    npl.UpdateTransaction(transaction);
                            //}
                            //else if (pk.MaPhuKien == "N11")
                            //{
                            //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            //    npl.Ma = hang.MaHang;
                            //    npl.HopDong_ID = this.HopDong_ID;
                            //    npl.Load(transaction);
                            //    npl.SoLuongCungUng += hang.SoLuong;
                            //    npl.UpdateTransaction(transaction);
                            //}
                            //else if (pk.MaPhuKien == "S06")
                            //{
                            //    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            //    sp.Ma = hang.MaHang;
                            //    sp.HopDong_ID = this.HopDong_ID;
                            //    sp.Load(transaction);
                            //    sp.DVT_ID = hang.DVT_ID;
                            //    sp.UpdateTransaction(transaction);
                            //}
                            //else if (pk.MaPhuKien == "S10")
                            //{
                            //    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            //    sp.Ma = hang.ThongTinCu;
                            //    sp.HopDong_ID = this.HopDong_ID;
                            //    sp.Load(transaction);
                            //    sp.Ma = hang.MaHang;
                            //    sp.UpdateMaSPTransaction(transaction, hang.ThongTinCu);
                            //    //cap nhat lai dinh muc neu co
                            //    Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                            //    dm.HopDong_ID = this.HopDong_ID;
                            //    dm.MaSanPham = sp.Ma;
                            //    dm.UpdateMaDinhMucTransaction(transaction, hang.ThongTinCu);
                            //}
                            else if (pk.MaPhuKien == "S13")
                            {
                                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                                sp.Ma = hang.MaHang;
                                sp.HopDong_ID = this.HopDong_ID;
                                sp.DVT_ID = hang.DVT_ID;
                                sp.MaHS = hang.MaHS;
                                sp.NhomSanPham_ID = hang.NhomSP;
                                sp.SoLuongDangKy = hang.SoLuong;
                                sp.TrangThai = 2;
                                sp.Ten = hang.TenHang;
                                sp.InsertUpdateTransaction(transaction);
                            }
                            else if (pk.MaPhuKien == "S15")
                            {
                                Company.GC.BLL.GC.NhomSanPham LoaiSP = new Company.GC.BLL.GC.NhomSanPham();
                                LoaiSP.MaSanPham = hang.MaHang;
                                LoaiSP.HopDong_ID = this.HopDong_ID;
                                LoaiSP.TrangThai = 2;
                                LoaiSP.TenSanPham = hang.TenHang;
                                LoaiSP.SoLuong = hang.SoLuong;
                                LoaiSP.GiaGiaCong = Convert.ToDecimal(hang.DonGia);
                                LoaiSP.InsertUpdateTransaction(transaction);
                            }
                            //else if (pk.MaPhuKien == "T01")
                            //{
                            //    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                            //    tb.Ma = hang.MaHang;
                            //    tb.HopDong_ID = this.HopDong_ID;
                            //    tb.TrangThai = 2;
                            //    tb.Load(transaction);
                            //    tb.SoLuongDangKy += hang.SoLuong;
                            //    tb.UpdateTransaction(transaction);
                            //}
                            else if (pk.MaPhuKien == "T07")
                            {
                                BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                                entity.HopDong_ID = this.HopDong_ID;
                                entity.Ma = hang.MaHang;
                                entity.DVT_ID = hang.DVT_ID;
                                entity.MaHS = hang.MaHS;
                                entity.DonGia = hang.DonGia;
                                entity.SoLuongDangKy = hang.SoLuong;
                                entity.Ten = hang.TenHang;
                                entity.NguyenTe_ID = hang.NguyenTe_ID;
                                entity.NuocXX_ID = hang.NuocXX_ID;
                                entity.TinhTrang = hang.TinhTrang;
                                entity.TriGia = hang.TriGia;
                                entity.TrangThai = 2;
                                entity.InsertUpdateTransaction(transaction);
                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void InsertUpDateFullSLXNK()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                long id = this.ID;
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.InsertTransaction(transaction);
                    else
                        this.UpdateTransaction(transaction);
                    foreach (LoaiPhuKien pk in PKCollection)
                    {
                        int stt = 1;
                        pk.Master_ID = this.ID;
                        if (pk.ID > 0)
                            pk.UpdateTransaction(transaction);
                        else
                            pk.InsertTransaction(transaction);
                        //cap nhat vao he thong xu lys chung
                        //#region Gia han hop dong
                        //if (pk.MaPhuKien == "H06")
                        //{
                        //    HopDong HD = new HopDong();
                        //    HD.ID = this.HopDong_ID;
                        //    HD.Load(transaction);
                        //    DateTimeFormatInfo dtF = new CultureInfo("vi-VN").DateTimeFormat;
                        //    HD.NgayGiaHan = Convert.ToDateTime(pk.ThongTinMoi, dtF);
                        //    HD.UpdateTransaction(transaction);
                        //}
                        //else if (pk.MaPhuKien == "H10")
                        //{
                        //    HopDong HD = new HopDong();
                        //    HD.ID = this.HopDong_ID;
                        //    HD.Load(transaction);
                        //    HD.TrangThaiXuLy = -2;//-2 la huy hop dong
                        //    HD.UpdateTransaction(transaction);
                        //}
                        //else if (pk.MaPhuKien == "H11")
                        //{
                        //    ;
                        //}
                        //#endregion Gia han hop dong
                        foreach (HangPhuKien hang in pk.HPKCollection)
                        {
                            hang.STTHang = stt++;
                            hang.Master_ID = pk.ID;
                            if (hang.ID == 0)
                                hang.InsertTransaction(transaction);
                            else
                                hang.UpdateTransaction(transaction);
                            //if (pk.MaPhuKien == "N01")
                            //{
                            //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            //    npl.DVT_ID = hang.DVT_ID;
                            //    npl.HopDong_ID = this.HopDong_ID;
                            //    npl.Ten = hang.TenHang;
                            //    npl.SoLuongDangKy = hang.SoLuong;
                            //    npl.Ma = hang.MaHang;
                            //    npl.MaHS = hang.MaHS;
                            //    npl.TrangThai = 2;
                            //    npl.InsertUpdateTransaction(transaction);
                            //}
                            if (pk.MaPhuKien == "N05")
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                npl.Ma = hang.MaHang;
                                npl.HopDong_ID = this.HopDong_ID;
                                npl.Load(transaction);
                                npl.SoLuongDangKy += hang.SoLuong;
                                npl.TrangThai = 0;
                                npl.UpdateTransaction(transaction);
                            }
                            //else if (pk.MaPhuKien == "N06")
                            //{
                            //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            //    npl.Ma = hang.MaHang;
                            //    npl.HopDong_ID = this.HopDong_ID;
                            //    npl.Load(transaction);
                            //    npl.DVT_ID = hang.DVT_ID;
                            //    npl.UpdateTransaction(transaction);
                            //}
                            //else if (pk.MaPhuKien == "N11")
                            //{
                            //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            //    npl.Ma = hang.MaHang;
                            //    npl.HopDong_ID = this.HopDong_ID;
                            //    npl.Load(transaction);
                            //    npl.SoLuongCungUng += hang.SoLuong;
                            //    npl.UpdateTransaction(transaction);
                            //}
                            //else if (pk.MaPhuKien == "S06")
                            //{
                            //    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            //    sp.Ma = hang.MaHang;
                            //    sp.HopDong_ID = this.HopDong_ID;
                            //    sp.Load(transaction);
                            //    sp.DVT_ID = hang.DVT_ID;
                            //    sp.UpdateTransaction(transaction);
                            //}
                            //else if (pk.MaPhuKien == "S10")
                            //{
                            //    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            //    sp.Ma = hang.ThongTinCu;
                            //    sp.HopDong_ID = this.HopDong_ID;
                            //    sp.Load(transaction);
                            //    sp.Ma = hang.MaHang;
                            //    sp.UpdateMaSPTransaction(transaction, hang.ThongTinCu);
                            //    //cap nhat lai dinh muc neu co
                            //    Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                            //    dm.HopDong_ID = this.HopDong_ID;
                            //    dm.MaSanPham = sp.Ma;
                            //    dm.UpdateMaDinhMucTransaction(transaction, hang.ThongTinCu);
                            //}
                            //else if (pk.MaPhuKien == "S13")
                            //{
                            //    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            //    sp.Ma = hang.MaHang;
                            //    sp.HopDong_ID = this.HopDong_ID;
                            //    sp.DVT_ID = hang.DVT_ID;
                            //    sp.MaHS = hang.MaHS;
                            //    sp.NhomSanPham_ID = hang.NhomSP;
                            //    sp.SoLuongDangKy = hang.SoLuong;
                            //    sp.TrangThai = 2;
                            //    sp.Ten = hang.TenHang;
                            //    sp.InsertUpdateTransaction(transaction);
                            //}
                            //else if (pk.MaPhuKien == "S15")
                            //{
                            //    Company.GC.BLL.GC.NhomSanPham LoaiSP = new Company.GC.BLL.GC.NhomSanPham();
                            //    LoaiSP.MaSanPham = hang.MaHang;
                            //    LoaiSP.HopDong_ID = this.HopDong_ID;
                            //    LoaiSP.TrangThai = 2;
                            //    LoaiSP.TenSanPham = hang.TenHang;
                            //    LoaiSP.SoLuong = hang.SoLuong;
                            //    LoaiSP.GiaGiaCong = Convert.ToDecimal(hang.DonGia);
                            //    LoaiSP.InsertUpdateTransaction(transaction);
                            //}
                            else if (pk.MaPhuKien == "T01")
                            {
                                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                                tb.Ma = hang.MaHang;
                                tb.HopDong_ID = this.HopDong_ID;
                                tb.Load(transaction);
                                tb.SoLuongDangKy = hang.SoLuong;
                                tb.TrangThai = 0;
                                tb.UpdateTransaction(transaction);
                            }
                            //else if (pk.MaPhuKien == "T07")
                            //{
                            //    BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                            //    entity.HopDong_ID = this.HopDong_ID;
                            //    entity.Ma = hang.MaHang;
                            //    entity.DVT_ID = hang.DVT_ID;
                            //    entity.MaHS = hang.MaHS;
                            //    entity.DonGia = hang.DonGia;
                            //    entity.SoLuongDangKy = hang.SoLuong;
                            //    entity.Ten = hang.TenHang;
                            //    entity.NguyenTe_ID = hang.NguyenTe_ID;
                            //    entity.NuocXX_ID = hang.NuocXX_ID;
                            //    entity.TinhTrang = hang.TinhTrang;
                            //    entity.TriGia = hang.TriGia;
                            //    entity.TrangThai = 2;
                            //    entity.InsertUpdateTransaction(transaction);
                            //}
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public void InsertUpDateFull(SqlTransaction transaction)
        {
            if (this.ID == 0)
                this.InsertTransaction(transaction);
            else
                this.UpdateTransaction(transaction);
            foreach (LoaiPhuKien pk in PKCollection)
            {
                int stt = 1;
                pk.Master_ID = this.ID;
                if (pk.ID > 0)
                    pk.UpdateTransaction(transaction);
                else
                    pk.InsertTransaction(transaction);
                foreach (HangPhuKien hang in pk.HPKCollection)
                {
                    hang.STTHang = stt++;
                    hang.Master_ID = pk.ID;
                    if (hang.ID == 0)
                        hang.InsertTransaction(transaction);
                    else
                        hang.UpdateTransaction(transaction);
                }
            }
        }

        public long checkPhuKienCungLoai(string MaPhuKien, long idPKDK, long HopDong_ID)
        {
            string sql = "select id from t_View_PhuKienDangKy " +
                        "where MaPhuKien=@MaPhuKien and trangthaixuly<>1 and id<>@idPKDK and HopDong_ID=@HopDong_ID";

            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@MaPhuKien", SqlDbType.Char, MaPhuKien);
            this.db.AddInParameter(dbCommand, "@idPKDK", SqlDbType.BigInt, idPKDK);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);

            long id = Convert.ToInt64(db.ExecuteScalar(dbCommand));
            return id;
        }
        public void Delete()
        {
            this.LoadCollection();
            foreach (LoaiPhuKien lpk in PKCollection)
            {
                if (lpk.HPKCollection.Count == 0)
                    lpk.LoadCollection();
            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                long id = this.ID;
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (LoaiPhuKien pk in PKCollection)
                    {
                        pk.Delete(this.HopDong_ID, transaction);
                    }
                    this.DeleteTransaction(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }


        public void Delete(SqlTransaction transaction)
        {
            foreach (LoaiPhuKien pk in PKCollection)
            {
                pk.Delete(this.HopDong_ID, transaction);
            }
            this.DeleteTransaction(transaction);
        }

        public static DataSet getDanhSachPhuKienMuaVN(long ID_HopDong)
        {
            string sql = "select id,MaHang,SoLuong from t_View_DanhSachPhuKienMuaVN where HopDong_id=" + ID_HopDong;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(dbCommand);
        }
        public static DataSet getDanhSachPhuKienMuaVN(long ID_HopDong, SqlTransaction transaction, string dbName)
        {
            string sql = "select id,MaHang,SoLuong from t_View_DanhSachPhuKienMuaVN where HopDong_id=" + ID_HopDong;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(dbName);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            if (transaction != null)
                return db.ExecuteDataSet(dbCommand, transaction);
            else
                return db.ExecuteDataSet(dbCommand);
        }

        #region WS
        //private void UpdateThongTinSauKhiDuyet()
        //{
        //    this.LoadCollection();
        //    foreach (LoaiPhuKien lpk in PKCollection)
        //    {
        //        lpk.LoadCollection();
        //    }
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
        //        try
        //        {
        //            this.TrangThaiXuLy = 1;
        //            foreach (LoaiPhuKien LoaiPK in PKCollection)
        //            {
        //                if (LoaiPK.MaPhuKien.Trim() == "N01")
        //                    LoaiPK.InsertUpdateBoSungNPL(this.HopDong_ID, transaction);
        //                else if (LoaiPK.MaPhuKien.Trim() == "S15")
        //                    LoaiPK.InsertUpdateBoSungNhomSamPhamGiaCong(this.HopDong_ID, transaction);
        //                else if (LoaiPK.MaPhuKien.Trim() == "T07")
        //                    LoaiPK.InsertUpdateBoSungTB(this.HopDong_ID, transaction);
        //                else if (LoaiPK.MaPhuKien.Trim() == "S13")
        //                    LoaiPK.InsertUpdateBoSungSP(this.HopDong_ID, transaction);
        //                else if (LoaiPK.MaPhuKien.Trim() == "H06")
        //                    LoaiPK.InsertUpdateGiaHanHD(this.HopDong_ID, transaction);
        //                else if (LoaiPK.MaPhuKien.Trim() == "H10")
        //                    LoaiPK.InsertUpdateHuyHD(this.HopDong_ID, transaction);
        //                else if (LoaiPK.MaPhuKien.Trim() == "S06")
        //                    LoaiPK.InsertUpdateDieuChinhDVTSP(this.HopDong_ID, transaction);
        //                else if (LoaiPK.MaPhuKien.Trim() == "S10")
        //                    LoaiPK.InsertUpdateDieuChinhMaSP(this.HopDong_ID, transaction);
        //                else if (LoaiPK.MaPhuKien.Trim() == "N05")
        //                    LoaiPK.InsertUpdateDieuChinhSoLuongNPL(this.HopDong_ID, transaction);
        //                else if (LoaiPK.MaPhuKien.Trim() == "N06")
        //                    LoaiPK.InsertUpdateDieuChinhDVTNPL(this.HopDong_ID, transaction);
        //                else if (LoaiPK.MaPhuKien.Trim() == "N11")
        //                    LoaiPK.InsertUpdateMuaVN(this.HopDong_ID, transaction);
        //                else if (LoaiPK.MaPhuKien.Trim() == "T01")
        //                    LoaiPK.InsertUpdateDieuChinhSoLuongThietBi(this.HopDong_ID, transaction);                        
        //            }
        //            this.UpdateTransaction(transaction);
        //            transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}
        private string ConfigPhongBi(int type, int function)
        {
            HopDong HD = new HopDong();
            HD.ID = this.HopDong_ID;
            HD = HopDong.Load(HD.ID);
            
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\B03GiaCong\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HD.MaHaiQuan));
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = HD.MaHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            ////DATLMQ bổ sung kiểm tra GUIDSTR cho Phụ kiện 10/01/2011
            //if (this.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET || this.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            //    nodeReference.InnerText = this.GUIDSTR.ToUpper();
            //else
            //{
            //    this.GUIDSTR = (System.Guid.NewGuid().ToString());
            //    nodeReference.InnerText = this.GUIDSTR.ToUpper();
            //}
            nodeReference.InnerText = GUIDSTR;
            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString().ToUpper());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = HD.MaDoanhNghiep.Trim();
            nodeFrom.ChildNodes[0].InnerText = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI"));
            this.Update();
            return doc.InnerXml;
        }

        private string ConfigPhongBi_LayPhanHoi(int type, int function)
        {
            HopDong HD = new HopDong();
            HD.ID = this.HopDong_ID;
            HD = HopDong.Load(HD.ID);
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\B03GiaCong\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HD.MaHaiQuan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = HD.MaHaiQuan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            //nodeReference.InnerText = (System.Guid.NewGuid().ToString());
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = HD.MaDoanhNghiep;
            nodeFrom.ChildNodes[0].InnerText = HD.MaDoanhNghiep;

            return doc.InnerXml;
        }

        public string WSCancel(string pass)
        {
            HopDong HD = new HopDong();
            HD.ID = this.HopDong_ID;
            HD = HopDong.Load(HD.ID);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi((int)MessgaseType.PhuKien, (int)MessageFunctions.HuyKhaiBao));
            XmlDocument docHopDong = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docHopDong.Load(path + "\\B03GiaCong\\HuyKhaiBaoPhuKien.XML");

            XmlNode NodeSoHD = docHopDong.SelectSingleNode(@"Root/So_HD");
            NodeSoHD.InnerText = HD.SoHopDong;
            XmlNode NodeMaHaiQuan = docHopDong.SelectSingleNode(@"Root/Ma_HQHD");
            NodeMaHaiQuan.InnerText = HD.MaHaiQuan;
            XmlNode NodeMaDoanhNghiep = docHopDong.SelectSingleNode(@"Root/DVGC");
            NodeMaDoanhNghiep.InnerText = HD.MaDoanhNghiep;
            XmlNode NodeNgayKy = docHopDong.SelectSingleNode(@"Root/Ngay_Ky");
            NodeNgayKy.InnerText = HD.NgayKy.ToString("MM/dd/yyyy");
            XmlNode NodeSoPhuKien = docHopDong.SelectSingleNode(@"Root/So_PK");
            NodeSoPhuKien.InnerText = this.SoPhuKien;
            XmlNode NodeNgayPhuKien = docHopDong.SelectSingleNode(@"Root/Ngay_PK");
            NodeNgayPhuKien.InnerText = this.NgayPhuKien.ToString("MM/dd/yyyy");

            XmlNode root = doc.ImportNode(docHopDong.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);


            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            string msgError = string.Empty;
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);

                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHuyNguyenPhuLieu);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        return doc.InnerXml;
                    }
                }
                else
                {
                    msgError = "Hải quan thông báo: " + FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                    throw new Exception(msgError);
                }

            }
            catch (Exception ex)
            {
                if (msgError != string.Empty) throw ex;
                if (!string.IsNullOrEmpty(kq)) Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            return "";
        }

        public string WSDownLoad(string pass)
        {
            HopDong HD = new HopDong();
            HD.ID = this.HopDong_ID;
            
            HD = HopDong.Load(HD.ID);
            XmlDocument doc = new XmlDocument();
            //doc.LoadXml(ConfigPhongBi(MessgaseType.PhuKien, MessageFunctions.HoiTrangThai));
            doc.LoadXml(ConfigPhongBi_LayPhanHoi((int)MessgaseType.PhuKien, (int)MessageFunctions.HoiTrangThai));
            //XmlDocument docHopDong = new XmlDocument();
            //string path = EntityBase.GetPathProram();
            //docHopDong.Load(path+"\\B03GiaCong\\LayPhanHoiPhuKien.XML");

            //XmlNode NodeSoHD = docHopDong.SelectSingleNode(@"Root/So_HD");
            //NodeSoHD.InnerText = HD.SoHopDong;
            //XmlNode NodeMaHaiQuan = docHopDong.SelectSingleNode(@"Root/Ma_HQHD");
            //NodeMaHaiQuan.InnerText = HD.MaHaiQuan;
            //XmlNode NodeMaDoanhNghiep = docHopDong.SelectSingleNode(@"Root/DVGC");
            //NodeMaDoanhNghiep.InnerText = HD.MaDoanhNghiep;
            //XmlNode NodeNgayKy = docHopDong.SelectSingleNode(@"Root/Ngay_Ky");
            //NodeNgayKy.InnerText = HD.NgayKy.ToString("MM/dd/yyyy");
            //XmlNode NodeSoPhuKien = docHopDong.SelectSingleNode(@"Root/So_PK");
            //NodeSoPhuKien.InnerText = this.SoPhuKien;
            //XmlNode NodeNgayPhuKien = docHopDong.SelectSingleNode(@"Root/Ngay_PK");
            //NodeNgayPhuKien.InnerText = this.NgayPhuKien.ToString("MM/dd/yyyy");

            //XmlNode root = doc.ImportNode(docHopDong.SelectSingleNode("Root"), true);
            //XmlNode Content = doc.GetElementsByTagName("Content")[0];
            //Content.AppendChild(root);

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + @"\B03GiaCong\LayPhanHoiDaDuyet.xml");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this._MaHaiQuan));

            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);


            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }

        public string WSSend(string pass)
        {
            NumberFormatInfo infoNumber = new CultureInfo("en-US").NumberFormat;
            HopDong HD = new HopDong();
            HD.ID = this.HopDong_ID;
            HD= HopDong.Load(HD.ID);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi((int)MessgaseType.PhuKien, (int)MessageFunctions.KhaiBao));
            XmlDocument docHopDong = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docHopDong.Load(path + "\\B03GiaCong\\KhaiBaoPhuKien.xml");

            XmlNode NodeSoHD = docHopDong.SelectSingleNode(@"Root/PhuKien/So_HD");
            NodeSoHD.InnerText = HD.SoHopDong;
            XmlNode NodeMaHaiQuan = docHopDong.SelectSingleNode(@"Root/PhuKien/Ma_HQHD");
            NodeMaHaiQuan.InnerText = HD.MaHaiQuan;
            XmlNode NodeMaDoanhNghiep = docHopDong.SelectSingleNode(@"Root/PhuKien/DVGC");
            NodeMaDoanhNghiep.InnerText = HD.MaDoanhNghiep;
            XmlNode NodeNgayKy = docHopDong.SelectSingleNode(@"Root/PhuKien/Ngay_Ky");
            NodeNgayKy.InnerText = HD.NgayKy.ToString("MM/dd/yyyy");
            XmlNode NodePhuKien = docHopDong.SelectSingleNode(@"Root/PhuKien/So_PK");
            NodePhuKien.InnerText = this.SoPhuKien;
            XmlNode NodeNgayPhuKien = docHopDong.SelectSingleNode(@"Root/PhuKien/Ngay_PK");
            NodeNgayPhuKien.InnerText = this.NgayPhuKien.ToString("MM/dd/yyyy");
            XmlNode NodeVBCP = docHopDong.SelectSingleNode(@"Root/PhuKien/VBCP");
            NodeVBCP.InnerText = FontConverter.Unicode2TCVN(this.VanBanChoPhep);
            XmlNode NodeGhiChu = docHopDong.SelectSingleNode(@"Root/PhuKien/GhiChu");
            NodeGhiChu.InnerText = FontConverter.Unicode2TCVN(this.GhiChu);
            XmlNode NodeNguoiDuyet = docHopDong.SelectSingleNode(@"Root/PhuKien/Nguoi_Duyet");
            if (this.NguoiDuyet != "")
                NodeNguoiDuyet.InnerText = FontConverter.Unicode2TCVN(this.NguoiDuyet);
            else
                NodeNguoiDuyet.InnerText = "x";
            XmlNode NodeSoTN = docHopDong.SelectSingleNode(@"Root/PhuKien/So_TN");
            NodeSoTN.InnerText = "0";
            CultureInfo culture = new CultureInfo("vi-VN");
            NumberFormatInfo f = new NumberFormatInfo();
            if (Thread.CurrentThread.CurrentCulture.Equals(culture))
            {
                f.NumberDecimalSeparator = ".";
                f.NumberGroupSeparator = ",";
            }
            //loai san pham gia cong

            XmlNode NodeNoiDungs = docHopDong.SelectSingleNode(@"Root/NoiDungs");
            XmlNode NodeHangPKs = docHopDong.SelectSingleNode(@"Root/DHangPKHDs");
            XmlNode NodeLoaiSPs = docHopDong.SelectSingleNode(@"Root/DLOAISPSGCs");
            XmlNode NodeSPs = docHopDong.SelectSingleNode(@"Root/DSPGCs");
            XmlNode NodeNPLs = docHopDong.SelectSingleNode(@"Root/DNPLHDs");
            XmlNode NodeTBs = docHopDong.SelectSingleNode(@"Root/DThietBis");
            LoaiPhuKien LoaiPKMuaVN = null;
            if (this.PKCollection != null || this.PKCollection.Count == 0)
                this.LoadCollection();
            foreach (LoaiPhuKien LoaiPK in this.PKCollection)
            {
                XmlNode nodeNoiDung = docHopDong.CreateElement("NoiDung");
                XmlNode nodeMaPhuKien = docHopDong.CreateElement("Ma_PK");
                nodeMaPhuKien.InnerText = LoaiPK.MaPhuKien.Trim();
                XmlNode nodeTenPhuKien = docHopDong.CreateElement("Noi_Dung");
                nodeTenPhuKien.InnerText = FontConverter.Unicode2TCVN(LoaiPK.NoiDung); ;
                XmlNode nodeThongTinMoi = docHopDong.CreateElement("NewInfo");
                if (LoaiPK.MaPhuKien.Trim() == "H06")
                {
                    DateTimeFormatInfo dtF = new CultureInfo("vi-VN").DateTimeFormat;
                    if (culture != Thread.CurrentThread.CurrentCulture)
                        dtF = Thread.CurrentThread.CurrentCulture.DateTimeFormat;
                    nodeThongTinMoi.InnerText = Convert.ToDateTime(LoaiPK.ThongTinMoi, dtF).ToString("MM/dd/yyyy");
                }
                else
                    nodeThongTinMoi.InnerText = LoaiPK.ThongTinMoi;
                XmlNode nodeThongTinCuPK = docHopDong.CreateElement("OldInfo");
                nodeThongTinCuPK.InnerText = "";
                nodeNoiDung.AppendChild(nodeMaPhuKien);
                nodeNoiDung.AppendChild(nodeTenPhuKien);
                nodeNoiDung.AppendChild(nodeThongTinMoi);
                nodeNoiDung.AppendChild(nodeThongTinCuPK);

                NodeNoiDungs.AppendChild(nodeNoiDung);

                #region lay thong tin hang phu kien
                if (LoaiPK.HPKCollection == null || LoaiPK.HPKCollection.Count == 0)
                    LoaiPK.LoadCollection();
                foreach (HangPhuKien pk in LoaiPK.HPKCollection)
                {
                    XmlNode nodeHang = docHopDong.CreateElement("DHangPKHD");
                    XmlNode nodeSoPhuKien = docHopDong.CreateElement("So_PK");
                    nodeSoPhuKien.InnerText = this.SoPhuKien;
                    XmlNode nodeMaPK = docHopDong.CreateElement("Ma_PK");
                    nodeMaPK.InnerText = LoaiPK.MaPhuKien.Trim();
                    XmlNode nodeMaHang = docHopDong.CreateElement("P_Code");
                    string st = "";
                    if (LoaiPK.MaPhuKien.Trim() == "N11")
                        LoaiPKMuaVN = LoaiPK;
                    if (LoaiPK.MaPhuKien.StartsWith("N"))
                        st = "N";
                    else if (LoaiPK.MaPhuKien.StartsWith("S") && LoaiPK.MaPhuKien.Trim() != "S15")
                        st = "S";
                    else if (LoaiPK.MaPhuKien.StartsWith("T"))
                        st = "T";
                    nodeMaHang.InnerText = st + pk.MaHang;
                    XmlNode nodeMaHS = docHopDong.CreateElement("HS_Code");
                    nodeMaHS.InnerText = pk.MaHS;
                    XmlNode nodeTenHang = (docHopDong.CreateElement("Ten_SP"));
                    nodeTenHang.InnerText = FontConverter.Unicode2TCVN(pk.TenHang);
                    XmlNode nodeSoLuong = docHopDong.CreateElement("So_Luong");
                    if (LoaiPK.MaPhuKien.Trim() == "N05" || LoaiPK.MaPhuKien.Trim() == "S09")
                    {
                        decimal LuongBoSung = pk.SoLuong - Convert.ToDecimal(pk.ThongTinCu);
                        nodeSoLuong.InnerText = LuongBoSung.ToString(f);
                    }
                    else if (LoaiPK.MaPhuKien.Trim() == "N11")
                    {
                        decimal LuongMuaVN = pk.SoLuong + Convert.ToDecimal(pk.ThongTinCu, infoNumber);
                        nodeSoLuong.InnerText = LuongMuaVN.ToString(f);
                    }
                    else if (LoaiPK.MaPhuKien.Trim() == "T01")
                    {
                        decimal LuongBoSung = pk.SoLuong - Convert.ToDecimal(pk.ThongTinCu);
                        nodeSoLuong.InnerText = LuongBoSung.ToString(f);
                    }
                    else
                        nodeSoLuong.InnerText = pk.SoLuong.ToString(f);
                    XmlNode nodeDVT_ID = docHopDong.CreateElement("Ma_DVT");
                    nodeDVT_ID.InnerText = pk.DVT_ID;
                    XmlNode nodeMaSP = docHopDong.CreateElement("Ma_SP");
                    if (LoaiPK.MaPhuKien.Trim() != "S13")
                        nodeMaSP.InnerText = nodeMaHang.InnerText;
                    else
                        nodeMaSP.InnerText = pk.NhomSP;
                    XmlNode nodeNuocXX = docHopDong.CreateElement("Xuat_Xu");
                    //if(LoaiPK.MaPhuKien.Trim()!="S15")                    
                    if (LoaiPK.MaPhuKien.Trim().StartsWith("S"))
                        nodeNuocXX.InnerText = "VN";
                    else
                        nodeNuocXX.InnerText = pk.NuocXX_ID;
                    //XmlNode nodeNguyenTe = docHopDong.CreateElement("NgTe");
                    ////if(LoaiPK.MaPhuKien.Trim()!="S15")
                    //nodeNguyenTe.InnerText = pk.NguyenTe_ID;
                    //else
                    //   nodeNuocXX.InnerText = "VN";
                    XmlNode nodeDonGia = docHopDong.CreateElement("DonGia");
                    if (pk.DonGia > 0)
                        nodeDonGia.InnerText = pk.DonGia.ToString(f);
                    else
                        nodeDonGia.InnerText = "";
                    XmlNode nodeThongTinCu = docHopDong.CreateElement("OldInfo");
                    if (LoaiPK.MaPhuKien.Trim() == "N11")
                        nodeThongTinCu.InnerText = pk.SoLuong.ToString(f);
                    else if (LoaiPK.MaPhuKien.Trim() == "T07")
                        nodeThongTinCu.InnerText = pk.SoLuong.ToString(f);
                    //DATLM comment 22/03/2011
                    //else
                    //    nodeThongTinCu.InnerText = (pk.ThongTinCu);
                    XmlNode nodeTinhTrang = docHopDong.CreateElement("TinhTrang");
                    nodeTinhTrang.InnerText = pk.TinhTrang;
                    XmlNode nodeTriGia = docHopDong.CreateElement("TriGia");
                    if (pk.TriGia > 0)
                        nodeTriGia.InnerText = pk.TriGia.ToString(f);
                    else
                        nodeTriGia.InnerText = "";
                    nodeHang.AppendChild(nodeSoPhuKien);
                    nodeHang.AppendChild(nodeMaPK);
                    nodeHang.AppendChild(nodeMaHang);
                    nodeHang.AppendChild(nodeMaHS);
                    nodeHang.AppendChild(nodeTenHang);
                    nodeHang.AppendChild(nodeSoLuong);
                    nodeHang.AppendChild(nodeDVT_ID);
                    nodeHang.AppendChild(nodeMaSP);
                    nodeHang.AppendChild(nodeNuocXX);
                    nodeHang.AppendChild(nodeDonGia);
                    nodeHang.AppendChild(nodeThongTinCu);
                    nodeHang.AppendChild(nodeTriGia);
                    nodeHang.AppendChild(nodeTinhTrang);
                    NodeHangPKs.AppendChild(nodeHang);

                    #region Nhóm sản phẩm
                    if (LoaiPK.MaPhuKien.Trim() == "S15")
                    {
                        XmlAttribute att = docHopDong.CreateAttribute("insert");
                        att.Value = "yes";
                        XmlNode NodeLoaiSPGC = docHopDong.CreateElement("DLOAISPSGC");
                        NodeLoaiSPGC.Attributes.Append(att);
                        XmlNode NodeMaSanPhamGC = docHopDong.CreateElement("Ma_SPGC");
                        NodeMaSanPhamGC.InnerText = pk.MaHang;
                        XmlNode NodeSoLuongGC = docHopDong.CreateElement("So_Luong");
                        NodeSoLuongGC.InnerText = pk.SoLuong.ToString(f);
                        XmlNode NodeGiaGC = docHopDong.CreateElement("Gia_GC");
                        NodeGiaGC.InnerText = pk.DonGia.ToString(f);
                        NodeLoaiSPGC.AppendChild(NodeMaSanPhamGC);
                        NodeLoaiSPGC.AppendChild(NodeSoLuongGC);
                        NodeLoaiSPGC.AppendChild(NodeGiaGC);
                        NodeLoaiSPs.AppendChild(NodeLoaiSPGC);
                    }
                    #endregion Nhóm sản phẩm

                    #region Sản phẩm
                    if (LoaiPK.MaPhuKien.StartsWith("S") && LoaiPK.MaPhuKien.Trim() != "S15")
                    {
                        XmlNode NodeSPGC = docHopDong.CreateElement("DSPGC");
                        XmlAttribute att = docHopDong.CreateAttribute("insert");
                        if (LoaiPK.MaPhuKien.Trim() == "S13")
                            att.Value = "yes";
                        else
                            att.Value = "no";
                        NodeSPGC.Attributes.Append(att);
                        XmlNode nodeMaPKSP = docHopDong.CreateElement("Ma_PK");
                        nodeMaPKSP.InnerText = LoaiPK.MaPhuKien;
                        XmlNode NodeMaSP = docHopDong.CreateElement("P_Code");
                        NodeMaSP.InnerText = "S" + pk.MaHang;

                        XmlNode NodeTenSP = docHopDong.CreateElement("Ten_SP");
                        NodeTenSP.InnerText = FontConverter.Unicode2TCVN(pk.TenHang);

                        XmlNode NodeMaHS = docHopDong.CreateElement("HS_Code");
                        NodeMaHS.InnerText = pk.MaHS;

                        XmlNode NodeSoLuongDK = docHopDong.CreateElement("SL_DK");
                        NodeSoLuongDK.InnerText = pk.SoLuong.ToString(f);

                        XmlNode NodeSoLuongDC = docHopDong.CreateElement("SL_DC");
                        NodeSoLuongDC.InnerText = "0";

                        XmlNode NodeDVT = docHopDong.CreateElement("Ma_DVT");
                        NodeDVT.InnerText = pk.DVT_ID;

                        XmlNode NodeVBDC = docHopDong.CreateElement("VB_DC");
                        NodeVBDC.InnerText = "";

                        XmlNode NodeNhomSP = docHopDong.CreateElement("Nhom_SP");
                        NodeNhomSP.InnerText = pk.NhomSP;

                        XmlNode NodeMaCu = docHopDong.CreateElement("P_CodeOld");
                        if (LoaiPK.MaPhuKien.Trim() == "S06" || LoaiPK.MaPhuKien.Trim() == "S09")
                        {
                            NodeMaCu.InnerText = "S" + pk.MaHang;
                        }
                        else if (LoaiPK.MaPhuKien.Trim() == "S10")
                        {
                            NodeMaCu.InnerText = "S" + pk.ThongTinCu;
                        }
                        else
                            NodeMaCu.InnerText = pk.ThongTinCu;

                        NodeSPGC.AppendChild(nodeMaPKSP);
                        NodeSPGC.AppendChild(NodeMaSP);
                        NodeSPGC.AppendChild(NodeTenSP);
                        NodeSPGC.AppendChild(NodeMaHS);
                        NodeSPGC.AppendChild(NodeSoLuongDK);
                        NodeSPGC.AppendChild(NodeDVT);
                        NodeSPGC.AppendChild(NodeNhomSP);
                        NodeSPGC.AppendChild(NodeSoLuongDC);
                        NodeSPGC.AppendChild(NodeVBDC);
                        NodeSPGC.AppendChild(NodeMaCu);
                        NodeSPs.AppendChild(NodeSPGC);



                        if (LoaiPK.MaPhuKien.Trim() == "S09")
                        {
                            XmlNode nodeX = NodeSPGC.Clone();
                            nodeX.Attributes["insert"].Value = "x";
                            nodeX.RemoveChild(nodeX.SelectSingleNode("P_CodeOld"));

                            Company.GC.BLL.GC.SanPham spDaDuyet = new Company.GC.BLL.GC.SanPham();
                            spDaDuyet.HopDong_ID = HD.ID;
                            spDaDuyet.Ma = pk.MaHang;
                            spDaDuyet.Load();
                            nodeX.SelectSingleNode("Nhom_SP").InnerText = spDaDuyet.NhomSanPham_ID;
                            nodeX.SelectSingleNode("SL_DK").InnerText = Convert.ToDecimal(pk.ThongTinCu).ToString(f);
                            NodeSPs.AppendChild(nodeX);
                        }
                    }
                    #endregion sản phẩm

                    #region nguyên phụ liệu
                    if (LoaiPK.MaPhuKien.StartsWith("N"))
                    {
                        XmlNode NodeNPL = docHopDong.CreateElement("DNPLHD");
                        XmlAttribute att = docHopDong.CreateAttribute("insert");
                        if (LoaiPK.MaPhuKien.Trim() == "N01")
                            att.Value = "yes";
                        else if (LoaiPK.MaPhuKien.Trim() == "N05")
                            att.Value = "x";
                        else
                            att.Value = "no";
                        NodeNPL.Attributes.Append(att);
                        XmlNode nodeMaPKNPL = docHopDong.CreateElement("Ma_PK");
                        nodeMaPKNPL.InnerText = LoaiPK.MaPhuKien.Trim();
                        XmlNode NodeMaNPL = docHopDong.CreateElement("P_Code");
                        NodeMaNPL.InnerText = "N" + pk.MaHang;

                        XmlNode NodeTenNPL = docHopDong.CreateElement("Ten_NPL");
                        NodeTenNPL.InnerText = FontConverter.Unicode2TCVN(pk.TenHang);

                        XmlNode NodeMaHS = docHopDong.CreateElement("HS_Code");
                        NodeMaHS.InnerText = pk.MaHS;

                        XmlNode NodeSoLuongDK = docHopDong.CreateElement("SL_DK");
                        NodeSoLuongDK.InnerText = pk.SoLuong.ToString(f);

                        XmlNode NodeSoLuongDC = docHopDong.CreateElement("SL_DC");
                        NodeSoLuongDC.InnerText = "0";

                        XmlNode NodeDVT = docHopDong.CreateElement("Ma_DVT");
                        NodeDVT.InnerText = pk.DVT_ID;

                        XmlNode NodeVBDC = docHopDong.CreateElement("VB_DC");
                        NodeVBDC.InnerText = "";

                        XmlNode NodeMuaVN = docHopDong.CreateElement("Mua_VN");
                        NodeMuaVN.InnerText = "0";

                        XmlNode NodeDonGiaNPL = docHopDong.CreateElement("DonGia");
                        NodeDonGiaNPL.InnerText = pk.DonGia.ToString(f);
                        if (LoaiPK.MaPhuKien.Trim() == "N06")
                        {
                            if (!pk.TinhTrang.Equals(""))
                                NodeMuaVN.InnerText = pk.TinhTrang;
                            NodeDonGiaNPL.InnerText = pk.DonGia.ToString(f);
                        }
                        NodeNPL.AppendChild(nodeMaPKNPL);
                        NodeNPL.AppendChild(NodeMaNPL);
                        NodeNPL.AppendChild(NodeTenNPL);
                        NodeNPL.AppendChild(NodeMaHS);
                        NodeNPL.AppendChild(NodeSoLuongDK);
                        NodeNPL.AppendChild(NodeDVT);
                        NodeNPL.AppendChild(NodeMuaVN);
                        NodeNPL.AppendChild(NodeSoLuongDC);
                        NodeNPL.AppendChild(NodeVBDC);
                        NodeNPL.AppendChild(NodeDonGiaNPL);
                        NodeNPLs.AppendChild(NodeNPL);
                    }
                    #endregion nguyên phụ liệu

                    #region thiết bị
                    if (LoaiPK.MaPhuKien.StartsWith("T"))
                    {
                        XmlNode NodeTB = docHopDong.CreateElement("DThietBi");
                        XmlAttribute att = docHopDong.CreateAttribute("insert");
                        if (LoaiPK.MaPhuKien.Trim() == "T07")
                            att.Value = "yes";
                        else
                            att.Value = "no";//x chi dung cho thiet bi
                        NodeTB.Attributes.Append(att);
                        XmlNode nodeMaTB = docHopDong.CreateElement("Ma_PK");
                        nodeMaTB.InnerText = LoaiPK.MaPhuKien;
                        XmlNode NodeMa = docHopDong.CreateElement("P_Code");
                        NodeMa.InnerText = "T" + pk.MaHang;

                        XmlNode NodeTen = docHopDong.CreateElement("Ten_TB");
                        NodeTen.InnerText = FontConverter.Unicode2TCVN(pk.TenHang);

                        XmlNode NodeMaHS = docHopDong.CreateElement("HS_Code");
                        NodeMaHS.InnerText = pk.MaHS;

                        XmlNode NodeSoLuongDK = docHopDong.CreateElement("SL_DK");
                        NodeSoLuongDK.InnerText = pk.SoLuong.ToString(f);

                        XmlNode NodeNuocXX = docHopDong.CreateElement("Xuat_Xu");
                        NodeNuocXX.InnerText = pk.NuocXX_ID;

                        XmlNode NodeDVT = docHopDong.CreateElement("Ma_DVT");
                        NodeDVT.InnerText = pk.DVT_ID;

                        XmlNode NodeDonGia = docHopDong.CreateElement("DonGia");
                        NodeDonGia.InnerText = pk.DonGia.ToString(f);

                        XmlNode NodeTriGia = docHopDong.CreateElement("TriGia");
                        NodeTriGia.InnerText = pk.TriGia.ToString(f);

                        XmlNode NodeNguyenTe = docHopDong.CreateElement("NGTe");
                        NodeNguyenTe.InnerText = pk.NguyenTe_ID;

                        XmlNode NodeTinhTrang = docHopDong.CreateElement("GhiChu");
                        NodeTinhTrang.InnerText = FontConverter.Unicode2TCVN(pk.TinhTrang);

                        NodeTB.AppendChild(nodeMaTB);
                        NodeTB.AppendChild(NodeMa);
                        NodeTB.AppendChild(NodeTen);
                        NodeTB.AppendChild(NodeMaHS);
                        NodeTB.AppendChild(NodeSoLuongDK);
                        NodeTB.AppendChild(NodeDVT);
                        NodeTB.AppendChild(NodeNuocXX);
                        NodeTB.AppendChild(NodeDonGia);
                        NodeTB.AppendChild(NodeTriGia);
                        NodeTB.AppendChild(NodeNguyenTe);
                        NodeTB.AppendChild(NodeTinhTrang);

                        NodeTBs.AppendChild(NodeTB);
                    }
                    #endregion Thiết bị
                }
                #endregion lay thong tin hang phu kien
            }
            if (LoaiPKMuaVN != null)
            {
                foreach (HangPhuKien hangPK in LoaiPKMuaVN.HPKCollection)
                {
                    BLL.GC.NguyenPhuLieu npl = new BLL.GC.NguyenPhuLieu();
                    npl.Ma = hangPK.MaHang;
                    npl.HopDong_ID = this._HopDong_ID;
                    npl.Load();
                    foreach (XmlNode nodeNPL in NodeNPLs.ChildNodes)
                    {

                        decimal LuongMuaVN = Convert.ToDecimal(hangPK.ThongTinCu, infoNumber) + hangPK.SoLuong;
                        if ("N" + hangPK.MaHang.Trim() == nodeNPL.SelectSingleNode("P_Code").InnerText.Trim())
                        {
                            if (nodeNPL.SelectSingleNode("Ma_PK").InnerText.Trim() == "N11" || nodeNPL.Attributes["insert"].Value.Trim() == "yes")
                            {
                                nodeNPL.SelectSingleNode("Mua_VN").InnerText = LuongMuaVN.ToString(f);
                                nodeNPL.SelectSingleNode("SL_DK").InnerText = npl.SoLuongDangKy.ToString(f);
                            }
                        }
                        else
                        {
                            continue;
                            //XmlNode NodeNPLMuaVN = docHopDong.CreateElement("DNPLHD");
                            //XmlAttribute att = docHopDong.CreateAttribute("insert");
                            //att.Value = "no";
                            //NodeNPLMuaVN.Attributes.Append(att);
                            //XmlNode nodeMaPKNPL = docHopDong.CreateElement("Ma_PK");
                            //nodeMaPKNPL.InnerText = LoaiPKMuaVN.MaPhuKien;
                            //XmlNode NodeMaNPL = docHopDong.CreateElement("P_Code");
                            //NodeMaNPL.InnerText = "N" + hangPK.MaHang;

                            //XmlNode NodeTenNPL = docHopDong.CreateElement("Ten_NPL");
                            //NodeTenNPL.InnerText = hangPK.TenHang;

                            //XmlNode NodeMaHS = docHopDong.CreateElement("HS_Code");
                            //NodeMaHS.InnerText = hangPK.MaHS;

                            //XmlNode NodeSoLuongDK = docHopDong.CreateElement("SL_DK");
                            //NodeSoLuongDK.InnerText = npl.SoLuongDangKy.ToString(f);

                            //XmlNode NodeSoLuongDC = docHopDong.CreateElement("SL_DC");
                            //NodeSoLuongDC.InnerText = "0";

                            //XmlNode NodeDVT = docHopDong.CreateElement("Ma_DVT");
                            //NodeDVT.InnerText = hangPK.DVT_ID;

                            //XmlNode NodeVBDC = docHopDong.CreateElement("VB_DC");
                            //NodeVBDC.InnerText = "";

                            //XmlNode NodeMuaVN = docHopDong.CreateElement("Mua_VN");
                            //NodeMuaVN.InnerText = LuongMuaVN.ToString(f);

                            //NodeNPLMuaVN.AppendChild(nodeMaPKNPL);
                            //NodeNPLMuaVN.AppendChild(NodeMaNPL);
                            //NodeNPLMuaVN.AppendChild(NodeTenNPL);
                            //NodeNPLMuaVN.AppendChild(NodeMaHS);
                            //NodeNPLMuaVN.AppendChild(NodeSoLuongDK);
                            //NodeNPLMuaVN.AppendChild(NodeDVT);
                            //NodeNPLMuaVN.AppendChild(NodeMuaVN);
                            //NodeNPLMuaVN.AppendChild(NodeSoLuongDC);
                            //NodeNPLMuaVN.AppendChild(NodeVBDC);

                            //NodeNPLs.AppendChild(NodeNPLMuaVN);                            
                        }

                    }
                }
            }



            XmlNode root = doc.ImportNode(docHopDong.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);


            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            string msgError = string.Empty;
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
                doc.InnerXml.XmlSaveMessage(ID, MessageTitle.KhaiBaoNguyenPhuLieu);
                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                        return doc.InnerXml;
                }
                else
                {
                    msgError = FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                {
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                    throw ex;
                }
                else
                    if (!string.IsNullOrEmpty(kq)) Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            return "";
        }
        public string LayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);
            doc.GetElementsByTagName("function")[0].InnerText = "5";
            string kq = "";
            int i = 0;
            XmlNode Result = null;
            for (i = 1; i <= 1; ++i)
            {
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
                docNPL = new XmlDocument();
                docNPL.LoadXml(kq);
                Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                if (Result.Attributes["Err"].Value == "no")
                {
                    if (Result.Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                }
                else
                {
                    throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
                }
            }

            if (i > 1)
                return doc.InnerXml;
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
            }
            bool ok = false;
            if (function == (int)MessageFunctions.KhaiBao)
            {
                this.SoTiepNhan = Convert.ToInt64(Result.Attributes["SoTN"].Value);
                this.NgayTiepNhan = this.NgayTiepNhan = DateTime.Today;
                this.TrangThaiXuLy = 0;
                ok = true;
            }
            else if (function == (int)MessageFunctions.HuyKhaiBao)
            {
                this.SoTiepNhan = 0;
                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                this.TrangThaiXuLy = -1;
                ok = true;
            }
            else if (function == (int)MessageFunctions.HoiTrangThai)
            {
                if (Result.Attributes["TrangThai"].Value == "yes")
                {
                    try
                    {
                        XmlNode nodephanluong = docNPL.SelectSingleNode("Envelope/Body/Content/Root/PHAN_LUONG");
                        if (nodephanluong != null)
                        {
                            this.PhanLuong = docNPL.SelectSingleNode("Envelope/Body/Content/Root/PHAN_LUONG").Attributes["MALUONG"].Value.Trim();
                            this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root/PHAN_LUONG").Attributes["HUONGDAN"].Value);
                            this.ActionStatus = 1;
                            this.TrangThaiXuLy = 1;
                            this.Update();
                        }
                    }
                    catch { }


                    ok = false;

                    try
                    {
                        XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                        if (nodeMess != null)
                        {
                            return nodeMess.OuterXml;
                        }
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                        if (nodeMess != null)
                        {
                            return nodeMess.OuterXml;
                        }
                    }
                    catch { }
                }

            }
            if (ok)
                this.Update();
            return "";

        }

        public void updateTrangThaiDuLieu()
        {
            if (this.PKCollection == null || this.PKCollection.Count == 0)
                this.LoadCollection();
            foreach (LoaiPhuKien LoaiPK in this.PKCollection)
            {
                if (LoaiPK.HPKCollection.Count == 0)
                {
                    LoaiPK.LoadCollection();
                }
            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                long id = this.ID;
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (LoaiPhuKien LoaiPK in this.PKCollection)
                    {
                        if (LoaiPK.MaPhuKien.Trim() == "H06")
                        {
                            HopDong HD = new HopDong();
                            HD.ID = this.HopDong_ID;
                            HD.Load(transaction);
                            HD.NgayGiaHan = Convert.ToDateTime(LoaiPK.ThongTinMoi);
                            HD.Update(transaction);
                        }
                        else if (LoaiPK.MaPhuKien.Trim() == "H10")
                        {
                            HopDong HD = new HopDong();
                            HD.ID = HopDong_ID;
                            HD.Load(transaction);
                            HD.TrangThaiXuLy = -2;//Hủy hợp đồng
                            HD.Update(transaction);
                        }
                        else if (LoaiPK.MaPhuKien.Trim() == "H11")
                        {
                            ;
                        }
                        foreach (HangPhuKien hang in LoaiPK.HPKCollection)
                        {
                            if (LoaiPK.MaPhuKien.Trim() == "N01")
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                npl.HopDong_ID = HopDong_ID;
                                npl.Ma = hang.MaHang;
                                npl.Load(transaction);
                                npl.TrangThai = 0;
                                npl.UpdateTransaction(transaction);
                            }
                            else if (LoaiPK.MaPhuKien.Trim() == "N05")//chua
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                npl.Ma = hang.MaHang;
                                npl.HopDong_ID = HopDong_ID;
                                npl.Load(transaction);
                                npl.TrangThai = 0;
                                npl.UpdateTransaction(transaction);
                            }
                            else if (LoaiPK.MaPhuKien.Trim() == "N06")
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                npl.Ma = hang.MaHang;
                                npl.HopDong_ID = HopDong_ID;
                                npl.Load(transaction);
                                npl.TrangThai = 0;
                                npl.UpdateTransaction(transaction);
                            }
                            else if (LoaiPK.MaPhuKien.Trim() == "N11")
                            {
                                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                                npl.Ma = hang.MaHang;
                                npl.HopDong_ID = HopDong_ID;
                                npl.Load(transaction);
                                npl.TrangThai = 0;
                                npl.UpdateTransaction(transaction);
                            }
                            else if (LoaiPK.MaPhuKien.Trim() == "S06" || LoaiPK.MaPhuKien.Trim() == "S09")
                            {
                                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                                sp.Ma = hang.MaHang;
                                sp.HopDong_ID = HopDong_ID;
                                sp.Load(transaction);
                                sp.TrangThai = 0;
                                sp.UpdateTransaction(transaction);
                            }

                            else if (LoaiPK.MaPhuKien.Trim() == "S10")
                            {
                                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                                sp.Ma = hang.MaHang;
                                sp.HopDong_ID = HopDong_ID;
                                sp.Load(transaction);
                                sp.TrangThai = 0;
                                sp.UpdateTransaction(transaction);
                            }
                            else if (LoaiPK.MaPhuKien.Trim() == "S13")
                            {
                                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                                sp.Ma = hang.MaHang;
                                sp.HopDong_ID = HopDong_ID;
                                sp.Load(transaction);
                                sp.TrangThai = 0;
                                sp.UpdateTransaction(transaction);
                            }
                            else if (LoaiPK.MaPhuKien.Trim() == "S15")
                            {
                                Company.GC.BLL.GC.NhomSanPham LoaiSP = new Company.GC.BLL.GC.NhomSanPham();
                                LoaiSP.MaSanPham = hang.MaHang;
                                LoaiSP.HopDong_ID = HopDong_ID;
                                LoaiSP.Load(transaction);
                                LoaiSP.TrangThai = 0;
                                LoaiSP.UpdateTransaction(transaction);
                            }
                            else if (LoaiPK.MaPhuKien.Trim() == "T01")
                            {
                                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                                tb.Ma = hang.MaHang;
                                tb.HopDong_ID = HopDong_ID;
                                tb.Load(transaction);
                                tb.TrangThai = 0;
                                tb.UpdateTransaction(transaction);
                            }
                            else if (LoaiPK.MaPhuKien.Trim() == "T07")
                            {
                                BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                                entity.HopDong_ID = HopDong_ID;
                                entity.Ma = hang.MaHang;
                                entity.Load(transaction);
                                entity.TrangThai = 0;
                                entity.UpdateTransaction(transaction);
                            }
                        }
                        this.TrangThaiXuLy = 1;
                        this.UpdateTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }


        public bool LoadBySoPhuKien()
        {
            string sql = "select * from t_KDT_GC_PhuKienDangKy where SoPhuKien=@SoPhuKien and HopDong_ID=@HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@SoPhuKien", SqlDbType.VarChar, this.SoPhuKien);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this.HopDong_ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayPhuKien"))) this._NgayPhuKien = reader.GetDateTime(reader.GetOrdinal("NgayPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiDuyet"))) this._NguoiDuyet = reader.GetString(reader.GetOrdinal("NguoiDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("VanBanChoPhep"))) this._VanBanChoPhep = reader.GetString(reader.GetOrdinal("VanBanChoPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) this._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoPhuKien"))) this._SoPhuKien = reader.GetString(reader.GetOrdinal("SoPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        #endregion

        #region DongBoDuLieuPhongKhai

        public static void DongBoDuLieuKhaiDTByIDPhuKienDK(PhuKienDangKy pkdk, HopDong HD, string nameConnectKDT)
        {
            NumberFormatInfo infoNumber = new CultureInfo("vi-VN").NumberFormat;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            //nguyen phu lieu
            string sql = "select * from DPKHDGC where  DVGC=@DVGC and Ngay_Ky=@Ngay_Ky and Ma_HQHD=@Ma_HQHD and So_HD=@So_HD and So_TN=@So_TN";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, HD.MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, HD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@Ngay_Ky", SqlDbType.DateTime, HD.NgayKy);
            db.AddInParameter(dbCommand, "@So_HD", SqlDbType.VarChar, HD.SoHopDong);
            db.AddInParameter(dbCommand, "@So_TN", SqlDbType.Decimal, pkdk.SoTiepNhan);

            DataSet dsPK = db.ExecuteDataSet(dbCommand);
            pkdk.PKCollection = new LoaiPhuKienCollection();
            foreach (DataRow row in dsPK.Tables[0].Rows)
            {
                LoaiPhuKien LoaiPK = new LoaiPhuKien();
                LoaiPK.MaPhuKien = row["Ma_PK"].ToString();
                LoaiPK.NoiDung = FontConverter.TCVN2Unicode(row["Noi_Dung"].ToString());
                LoaiPK.ThongTinCu = row["Old_Info"].ToString();
                if (LoaiPK.MaPhuKien != "H06")
                    LoaiPK.ThongTinMoi = row["New_Info"].ToString();
                else
                {
                    DateTimeFormatInfo infoDate = new CultureInfo("en-US").DateTimeFormat;
                    LoaiPK.ThongTinMoi = Convert.ToDateTime(row["New_Info"].ToString(), infoDate).ToString("dd/MM/yyyy");
                }
                sql = "select * from DHangPKHD where  DVGC=@DVGC and Ngay_Ky=@Ngay_Ky and Ma_HQHD=@Ma_HQHD and So_HD=@So_HD and So_PK=@So_PK and Ma_PK=@Ma_PK";
                dbCommand.Parameters.Clear();
                dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
                db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, HD.MaDoanhNghiep);
                db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, HD.MaHaiQuan);
                db.AddInParameter(dbCommand, "@Ngay_Ky", SqlDbType.DateTime, HD.NgayKy);
                db.AddInParameter(dbCommand, "@So_HD", SqlDbType.VarChar, HD.SoHopDong);
                db.AddInParameter(dbCommand, "@Ma_PK", SqlDbType.VarChar, LoaiPK.MaPhuKien);
                db.AddInParameter(dbCommand, "@So_PK", SqlDbType.VarChar, pkdk.SoPhuKien);
                DataSet dsHangPK = db.ExecuteDataSet(dbCommand);
                LoaiPK.HPKCollection = new HangPhuKienCollection();
                foreach (DataRow rowHang in dsHangPK.Tables[0].Rows)
                {
                    HangPhuKien hangPK = new HangPhuKien();
                    if (rowHang["DonGia"].ToString() != "")
                        hangPK.DonGia = Convert.ToDouble(rowHang["DonGia"]);
                    hangPK.DVT_ID = rowHang["Ma_DVT"].ToString();
                    if (LoaiPK.MaPhuKien != "S15")
                        hangPK.MaHang = rowHang["P_Code"].ToString().Substring(1).Trim();
                    else
                        hangPK.MaHang = rowHang["P_Code"].ToString().Trim();
                    hangPK.MaHS = rowHang["HS_Code"].ToString().Trim();
                    hangPK.NguyenTe_ID = rowHang["NGTe"].ToString();
                    hangPK.NhomSP = rowHang["Ma_SP"].ToString();
                    hangPK.NuocXX_ID = rowHang["Xuat_xu"].ToString();
                    hangPK.ThongTinCu = rowHang["Old_Info"].ToString();
                    if (rowHang["So_Luong"].ToString() != "")
                    {

                        try
                        {
                            if (LoaiPK.MaPhuKien.Trim() == "N05" || LoaiPK.MaPhuKien.Trim() == "S09")
                                hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]) + Convert.ToDecimal(hangPK.ThongTinCu);
                            else if (LoaiPK.MaPhuKien.Trim() == "N11")
                                hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]) - Convert.ToDecimal(hangPK.ThongTinCu);
                            else if (LoaiPK.MaPhuKien.Trim() == "T01")
                                hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]) + Convert.ToDecimal(hangPK.ThongTinCu);
                            else
                                hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                        }
                        catch { }
                    }


                    hangPK.TenHang = FontConverter.TCVN2Unicode(rowHang["Ten_SP"].ToString());
                    hangPK.TinhTrang = FontConverter.TCVN2Unicode(rowHang["TinhTrang"].ToString());
                    if (rowHang["TriGia"].ToString() != "")
                        hangPK.TriGia = Convert.ToDouble(rowHang["TriGia"]);
                    LoaiPK.HPKCollection.Add(hangPK);
                }
                pkdk.PKCollection.Add(LoaiPK);

                try
                {

                    pkdk.InsertUpDateFull();
                }
                catch { }

            }


        }


        public static void DongBoDuLieuKhaiDT(string MaDoanhNghiep, string MaHaiQuan, string nameConnectKDT)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sqlDelete = "delete t_KDT_GC_PhuKienDangKy where MaDoanhNghiep=@DVGC and TrangThaiXuLy!=-1 and MaHaiQuan=@Ma_HQHD";
            SqlCommand dbCommandDelete = (SqlCommand)db.GetSqlStringCommand(sqlDelete);
            db.AddInParameter(dbCommandDelete, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommandDelete, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);

            db.ExecuteNonQuery(dbCommandDelete);
            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select distinct So_HD,Ma_HQHD,DVGC,Ngay_Ky,So_PK,Ngay_PK,So_TN,Ngay_TN,Nguoi_Duyet,VBCP,GhiChu  from DPKHDGC  where DVGC=@DVGC and TrangThai<>2 and Ma_HQHD=@Ma_HQHD";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                try
                {
                    string sohopdong = row["So_HD"].ToString().Trim();
                    PhuKienDangKy pkdk = new PhuKienDangKy();
                    pkdk.GhiChu = FontConverter.TCVN2Unicode(row["GhiChu"].ToString());
                    pkdk.MaDoanhNghiep = MaDoanhNghiep;
                    pkdk.MaHaiQuan = MaHaiQuan;
                    pkdk.NgayPhuKien = Convert.ToDateTime(row["Ngay_PK"]);
                    pkdk.NgayTiepNhan = Convert.ToDateTime(row["Ngay_TN"]);
                    pkdk.NguoiDuyet = FontConverter.TCVN2Unicode(row["Nguoi_Duyet"].ToString());
                    pkdk.SoPhuKien = row["So_PK"].ToString();
                    pkdk.SoTiepNhan = Convert.ToInt64(row["So_TN"].ToString());
                    pkdk.TrangThaiXuLy = 0;
                    pkdk.VanBanChoPhep = FontConverter.TCVN2Unicode(row["VBCP"].ToString().Trim());
                    HopDong hd = new HopDong();
                    hd.ID = (new HopDong()).GetIDHopDongExit(sohopdong, MaHaiQuan, MaDoanhNghiep, Convert.ToDateTime(row["Ngay_Ky"]));
                    hd = HopDong.Load(hd.ID);
                    pkdk.HopDong_ID = hd.ID;
                    pkdk.TrangThaiXuLy = 0;
                    if (pkdk.HopDong_ID > 0)
                        DongBoDuLieuKhaiDTByIDPhuKienDK(pkdk, hd, nameConnectKDT);
                }
                catch
                {
                    ;
                }
            }

        }

        #endregion DongBoDuLieuPhongKhai


        #region DongBoDuLieuPhongKhai


        public static void DongBoDuLieuSLNXK(string MaDoanhNghiep, string MaHaiQuan, string nameConnectKDT)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sqlDelete = "delete t_KDT_GC_PhuKienDangKy where MaDoanhNghiep=@DVGC and TrangThaiXuLy=1 and MaHaiQuan=@Ma_HQHD";
            SqlCommand dbCommandDelete = (SqlCommand)db.GetSqlStringCommand(sqlDelete);
            db.AddInParameter(dbCommandDelete, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommandDelete, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);

            db.ExecuteNonQuery(dbCommandDelete);
            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select distinct So_HD,Ma_HQHD,DVGC,Ngay_Ky,So_PK,Ngay_PK,So_TN,Ngay_TN,Nguoi_Duyet,VBCP,GhiChu  from DPKHDGC  where DVGC=@DVGC  and Ma_HQHD=@Ma_HQHD";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string sohopdong = row["So_HD"].ToString();
                PhuKienDangKy pkdk = new PhuKienDangKy();
                pkdk.GhiChu = row["GhiChu"].ToString();
                pkdk.MaDoanhNghiep = MaDoanhNghiep;
                pkdk.MaHaiQuan = MaHaiQuan;
                pkdk.NgayPhuKien = Convert.ToDateTime(row["Ngay_PK"]);
                pkdk.NgayTiepNhan = Convert.ToDateTime(row["Ngay_TN"]);
                pkdk.NguoiDuyet = row["Nguoi_Duyet"].ToString();
                pkdk.SoPhuKien = row["So_PK"].ToString();
                pkdk.SoTiepNhan = Convert.ToInt64(row["So_TN"].ToString());
                pkdk.TrangThaiXuLy = 0;
                pkdk.VanBanChoPhep = row["VBCP"].ToString();
                HopDong hd = new HopDong();
                hd.ID = (new HopDong()).GetIDHopDongExit(sohopdong, MaHaiQuan, MaDoanhNghiep, Convert.ToDateTime(row["Ngay_Ky"]));
                hd = HopDong.Load(hd.ID);                
                pkdk.HopDong_ID = hd.ID;
                pkdk.TrangThaiXuLy = 1;
                if (pkdk.HopDong_ID > 0)
                    DongBoDuLieuSLXNKByIDPhuKienDK(pkdk, hd, nameConnectKDT);
            }

        }
        public static void DongBoDuLieuSLXNKByIDPhuKienDK(PhuKienDangKy pkdk, HopDong HD, string nameConnectKDT)
        {
            NumberFormatInfo infoNumber = new CultureInfo("vi-VN").NumberFormat;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            //nguyen phu lieu
            string sql = "select * from DPKHDGC where  DVGC=@DVGC and Ngay_Ky=@Ngay_Ky and Ma_HQHD=@Ma_HQHD and So_HD=@So_HD and So_TN=@So_TN";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, HD.MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, HD.MaHaiQuan);
            db.AddInParameter(dbCommand, "@Ngay_Ky", SqlDbType.DateTime, HD.NgayKy);
            db.AddInParameter(dbCommand, "@So_HD", SqlDbType.VarChar, HD.SoHopDong);
            db.AddInParameter(dbCommand, "@So_TN", SqlDbType.Decimal, pkdk.SoTiepNhan);

            DataSet dsPK = db.ExecuteDataSet(dbCommand);
            pkdk.PKCollection = new LoaiPhuKienCollection();
            foreach (DataRow row in dsPK.Tables[0].Rows)
            {
                LoaiPhuKien LoaiPK = new LoaiPhuKien();
                LoaiPK.MaPhuKien = row["Ma_PK"].ToString();
                LoaiPK.NoiDung = FontConverter.TCVN2Unicode(row["Noi_Dung"].ToString());
                LoaiPK.ThongTinCu = row["Old_Info"].ToString();
                if (LoaiPK.MaPhuKien != "H06")
                    LoaiPK.ThongTinMoi = row["New_Info"].ToString();
                else
                {
                    DateTimeFormatInfo infoDate = new CultureInfo("en-US").DateTimeFormat;
                    LoaiPK.ThongTinMoi = Convert.ToDateTime(row["New_Info"].ToString(), infoDate).ToString("dd/MM/yyyy");
                }
                sql = "select * from DHangPKHD where  DVGC=@DVGC and Ngay_Ky=@Ngay_Ky and Ma_HQHD=@Ma_HQHD and So_HD=@So_HD and So_PK=@So_PK and Ma_PK=@Ma_PK";
                dbCommand.Parameters.Clear();
                dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
                db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, HD.MaDoanhNghiep);
                db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, HD.MaHaiQuan);
                db.AddInParameter(dbCommand, "@Ngay_Ky", SqlDbType.DateTime, HD.NgayKy);
                db.AddInParameter(dbCommand, "@So_HD", SqlDbType.VarChar, HD.SoHopDong);
                db.AddInParameter(dbCommand, "@Ma_PK", SqlDbType.VarChar, LoaiPK.MaPhuKien);
                db.AddInParameter(dbCommand, "@So_PK", SqlDbType.VarChar, pkdk.SoPhuKien);
                DataSet dsHangPK = db.ExecuteDataSet(dbCommand);
                LoaiPK.HPKCollection = new HangPhuKienCollection();
                foreach (DataRow rowHang in dsHangPK.Tables[0].Rows)
                {
                    HangPhuKien hangPK = new HangPhuKien();
                    if (rowHang["DonGia"].ToString() != "")
                        hangPK.DonGia = Convert.ToDouble(rowHang["DonGia"]);
                    hangPK.DVT_ID = rowHang["Ma_DVT"].ToString();
                    if (LoaiPK.MaPhuKien != "S15")
                        hangPK.MaHang = rowHang["P_Code"].ToString().Substring(1).Trim();
                    else
                        hangPK.MaHang = rowHang["P_Code"].ToString().Trim();
                    hangPK.MaHS = rowHang["HS_Code"].ToString().Trim();
                    hangPK.NguyenTe_ID = rowHang["NGTe"].ToString();
                    hangPK.NhomSP = rowHang["Ma_SP"].ToString();
                    hangPK.NuocXX_ID = rowHang["Xuat_xu"].ToString();
                    hangPK.ThongTinCu = rowHang["Old_Info"].ToString();
                    if (rowHang["So_Luong"].ToString() != "")
                    {

                        try
                        {
                            hangPK.SoLuong = Convert.ToDecimal(rowHang["So_Luong"]);
                        }
                        catch { }
                    }


                    hangPK.TenHang = FontConverter.TCVN2Unicode(rowHang["Ten_SP"].ToString());
                    hangPK.TinhTrang = FontConverter.TCVN2Unicode(rowHang["TinhTrang"].ToString());
                    if (rowHang["TriGia"].ToString() != "")
                        hangPK.TriGia = Convert.ToDouble(rowHang["TriGia"]);
                    LoaiPK.HPKCollection.Add(hangPK);
                }
                pkdk.PKCollection.Add(LoaiPK);

                try
                {

                    pkdk.InsertUpDateFullSLXNK();
                }
                catch { }

            }


        }

        #endregion DongBoDuLieuPhongKhai

        #region TQDT:
        public string TQDTWSLayPhanHoi(string pass)
        {
            HopDong HD = new HopDong();
            HD.ID = this.HopDong_ID;
            
            HD = HopDong.Load(HD.ID);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ConfigPhongBi((int)MessgaseType.PhuKien, (int)MessageFunctions.HoiTrangThai));
            XmlDocument docHopDong = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docHopDong.Load(path + "\\B03GiaCong\\LayPhanHoiPhuKien.XML");

            XmlNode NodeSoHD = docHopDong.SelectSingleNode(@"Root/So_HD");
            NodeSoHD.InnerText = HD.SoHopDong;
            XmlNode NodeMaHaiQuan = docHopDong.SelectSingleNode(@"Root/Ma_HQHD");
            NodeMaHaiQuan.InnerText = HD.MaHaiQuan;
            XmlNode NodeMaDoanhNghiep = docHopDong.SelectSingleNode(@"Root/DVGC");
            NodeMaDoanhNghiep.InnerText = HD.MaDoanhNghiep;
            XmlNode NodeNgayKy = docHopDong.SelectSingleNode(@"Root/Ngay_Ky");
            NodeNgayKy.InnerText = HD.NgayKy.ToString("MM/dd/yyyy");
            XmlNode NodeSoPhuKien = docHopDong.SelectSingleNode(@"Root/So_PK");
            NodeSoPhuKien.InnerText = this.SoPhuKien;
            XmlNode NodeNgayPhuKien = docHopDong.SelectSingleNode(@"Root/Ngay_PK");
            NodeNgayPhuKien.InnerText = this.NgayPhuKien.ToString("MM/dd/yyyy");



            XmlNode root = doc.ImportNode(docHopDong.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";
        }
        public string TQDTLayPhanHoi(string pass, string xml)
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            //int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);
            //doc.GetElementsByTagName("function")[0].InnerText = "5";
            doc.GetElementsByTagName("function")[0].InnerText = MessageFunctions.LayPhanHoi.ToString();
            doc.GetElementsByTagName("type")[0].InnerText = MessgaseType.ThongTin.ToString();

            int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);

            string kq = "";
            int i = 0;
            XmlNode Result = null;
            for (i = 1; i <= 1; ++i)
            {
                string msgError = string.Empty;
                try
                {

                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);

                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);
                    Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                    if (Result.Attributes["Err"].Value == "no")
                    {
                        if (Result.Attributes["TrangThai"].Value == "yes")
                        {
                            break;
                        }
                    }
                    else
                    {
                        msgError =FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + "");
                        throw new Exception(msgError);
                    }
                }
                catch (Exception ex)
                {
                    if (msgError != string.Empty)
                    {
                        kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                        throw ex;
                    }else
                    if (!string.IsNullOrEmpty(kq)) Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }

            if (i > 1)
                return doc.InnerXml;
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
            }
            bool ok = false;
            /*
            if (function == MessageFunctions.KhaiBao)
            {
                this.SoTiepNhan = Convert.ToInt64(Result.Attributes["SoTN"].Value);
                this.NgayTiepNhan = this.NgayTiepNhan = DateTime.Today;
                this.TrangThaiXuLy = 0;
                ok = true;
            }
            else if (function == MessageFunctions.HuyKhaiBao)
            {
                this.SoTiepNhan = 0;
                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                this.TrangThaiXuLy = -1;
                ok = true;
            }
            //DATLMQ update nhận dữ liệu theo chuẩn mới
            else 
             */
            if (function == (int)MessageFunctions.LayPhanHoi)
            {
                #region old code
                //if (docNPL.SelectSingleNode("Envelope/Body/Content/Root/Status").InnerText == "2")
                //{
                //    updateTrangThaiDuLieu();
                //    ok = false;
                //    try
                //    {
                //        XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                //        if (nodeMess != null)
                //        {
                //            return nodeMess.OuterXml;
                //        }
                //    }
                //    catch { }
                //}
                //else
                //{
                //    try
                //    {
                //        XmlNode nodeMess = docNPL.SelectSingleNode("Envelope/Body/Content/Root/Megs");
                //        if (nodeMess != null)
                //        {
                //            return nodeMess.OuterXml;
                //        }
                //    }
                //    catch { }
                //}
                #endregion
                string errorSt = "";
                try
                {
                    if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("TuChoi") != null)
                    {
                        if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TuChoi"].Value == "yes")
                        {
                            //nodeTuChoi.InnerText;
                            this.HUONGDAN = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);

                            Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                            kqxl.ItemID = this.ID;
                            kqxl.ReferenceID = new Guid(this.GUIDSTR);
                            kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_PhuKien;
                            kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan;
                            kqxl.NoiDung = FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                            kqxl.Ngay = DateTime.Now;
                            kqxl.Insert();

                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            this.SoTiepNhan = 0;
                            this.NgayTiepNhan = new DateTime(1990, 01, 01);
                            this.Update();
                            kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQKhongDuyetNguyenPhuLieu, kqxl.NoiDung);
                        }
                        //DATLMQ bổ sung nhận phụ kiện đã duyệt 09/12/2010
                        else
                        {
                            XmlNode nodeDuLieu = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                            this.SoTiepNhan = Convert.ToInt64(nodeDuLieu.Attributes["SOTN"].Value);
                            this.NamDK = Convert.ToInt16(nodeDuLieu.Attributes["NAMTN"].Value);
                            if (nodeDuLieu.Attributes["NGAYTN"] != null && nodeDuLieu.Attributes["NGAYTN"].Value != "")
                                this.NgayTiepNhan = Convert.ToDateTime(nodeDuLieu.Attributes["NGAYTN"].Value);
                            this.ActionStatus = 1;
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                            this.Update();

                            Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                            kqxl.ItemID = this.ID;
                            kqxl.ReferenceID = new Guid(this.GUIDSTR);
                            kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_HopDong;
                            kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_PhuKienDuocDuyet;
                            if (nodeDuLieu.Attributes["NGAYTN"] != null && nodeDuLieu.Attributes["NGAYTN"].Value != "")
                                kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNăm tiếp nhận: {1}\r\nNgày tiếp nhận: {2}", this.SoTiepNhan, this.NamDK, this.NgayTiepNhan.ToShortDateString());
                            kqxl.Ngay = DateTime.Now;
                            kqxl.Insert();
                            kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoHQDuyetNguyennPhuLieu, kqxl.NoiDung);
                        }
                    }
                    else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root") != null
                        && docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "LOI")
                    {
                        XmlNode nodeMota = docNPL.SelectSingleNode("Envelope/Body/Content/Root/DULIEU/TT_LOI/MO_TA");
                        XmlNode nodeMucLoi = docNPL.SelectSingleNode("Envelope/Body/Content/Root/DULIEU/TT_LOI/MA_LOI");
                        string stMucLoi = nodeMucLoi.Attributes["MUC_LOI"].Value;

                        if (stMucLoi == "XML_LEVEL")
                            errorSt = "Lỗi cấu trúc dữ liệu XML mà chương trình gửi đi";
                        else if (stMucLoi == "DATA_LEVEL")
                            errorSt = "Lỗi do nội dung dữ liệu không hợp lệ";
                        else if (stMucLoi == "SERVICE_LEVEL")
                            errorSt = "Lỗi do Web service trả về ";
                        else if (stMucLoi == "DOTNET_LEVEL")
                            errorSt = "Lỗi do hệ thống của hải quan ";

                        Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                        kqxl.ItemID = this.ID;
                        kqxl.ReferenceID = new Guid(this.GUIDSTR);
                        kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_PhuKien;
                        kqxl.LoaiThongDiep = errorSt;
                        kqxl.NoiDung = docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText;
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();

                        errorSt = errorSt + " : " + nodeMota.InnerText + "|" + stMucLoi;

                        throw new Exception(errorSt);
                    }
                    /*Kiem tra PHAN LUONG*/
                    else if (docNPL.SelectSingleNode("Envelope/Body/Content/Root").Attributes.GetNamedItem("SOTN") != null)
                    {
                        #region Lấy số tiếp nhận của NPL & Thong tin phan luong

                        XmlNode nodeRoot = docNPL.SelectSingleNode("Envelope/Body/Content/Root");

                        if (nodeRoot.SelectSingleNode("PHAN_LUONG") == null) //Neu message chua phan luong -> cap nhat SO TN.
                        {
                            this.SoTiepNhan = Convert.ToInt64(nodeRoot.Attributes["SOTN"].Value);
                            this.NgayTiepNhan = DateTime.Today;
                            this.NamDK = Convert.ToInt16(nodeRoot.Attributes["NAMTN"].Value);

                            this.ActionStatus = 0;
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;

                            Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                            kqxl.ItemID = this.ID;
                            kqxl.ReferenceID = new Guid(this.GUIDSTR);
                            kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_HopDong;
                            kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong;
                            kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                            kqxl.Ngay = DateTime.Now;
                            kqxl.Insert();
                            kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoLayPhanHoiNguyenPhuLieu, kqxl.NoiDung);
                        }

                        /*Lay thong tin phan luong*/
                        if (nodeRoot.SelectSingleNode("PHAN_LUONG") != null)
                        {
                            XmlNode nodePhanLuong = nodeRoot.SelectSingleNode("PHAN_LUONG");

                            this.PhanLuong = nodePhanLuong.Attributes["MALUONG"].Value;
                            this.HUONGDAN = FontConverter.TCVN2Unicode(nodePhanLuong.Attributes["HUONGDAN"].Value);

                            this.ActionStatus = 1;
                            this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;

                            Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                            kqxl.ItemID = this.ID;
                            kqxl.ReferenceID = new Guid(this.GUIDSTR);
                            kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_PhuKien;
                            kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_PhuKienDuocDuyet;

                            string tenluong = "Xanh";

                            if (this.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                                tenluong = "Vàng";
                            else if (this.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                                tenluong = "Đỏ";

                            kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp: {1}\r\nHải quan: {2}\r\nPhân luồng: {3}\r\nHướng dẫn: {4}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), this.MaHaiQuan.Trim(), tenluong, this.HUONGDAN);
                            kqxl.Ngay = DateTime.Now;
                            kqxl.Insert();
                        }

                        this.Update();

                        #endregion Lấy số tiếp nhận của danh sách NPL
                    }
                    //DATLMQ bổ sung nhận dữ liệu Hủy khai báo 10/01/2011
                    else if (docNPL.SelectSingleNode("Envelope/Header/Subject/function").InnerText == "2")
                    {
                        this.SoTiepNhan = 0;
                        this.NgayTiepNhan = new DateTime(1900, 1, 1);
                        this.TrangThaiXuLy = -1;
                        ok = true;
                        kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoChapNhanHuyNguyenPhuLieu);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                    throw ex;
                }

            }
            if (ok)
                this.Update();
            return "";

        }

        #endregion

        //---------------------------------------------------------------------------------------------

        public PhuKienDangKyCollection SelectCollectionBy_HopDong_ID(string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_PhuKienDangKy_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            PhuKienDangKyCollection collection = new PhuKienDangKyCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                PhuKienDangKy entity = new PhuKienDangKy();
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayPhuKien"))) entity.NgayPhuKien = reader.GetDateTime(reader.GetOrdinal("NgayPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiDuyet"))) entity.NguoiDuyet = reader.GetString(reader.GetOrdinal("NguoiDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("VanBanChoPhep"))) entity.VanBanChoPhep = reader.GetString(reader.GetOrdinal("VanBanChoPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoPhuKien"))) entity.SoPhuKien = reader.GetString(reader.GetOrdinal("SoPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                //if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                //if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                //if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                //if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                //if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                //if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                //if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                //if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                //if (!reader.IsDBNull(reader.GetOrdinal("HuongdanPL"))) entity.HuongdanPL = reader.GetString(reader.GetOrdinal("HuongdanPL"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public PhuKienDangKyCollection SelectCollectionBy_HopDong_ID_KTX()
        {
            string spName = "p_KDT_GC_PhuKienDangKy_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            PhuKienDangKyCollection collection = new PhuKienDangKyCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                PhuKienDangKy entity = new PhuKienDangKy();
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayPhuKien"))) entity.NgayPhuKien = reader.GetDateTime(reader.GetOrdinal("NgayPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiDuyet"))) entity.NguoiDuyet = reader.GetString(reader.GetOrdinal("NguoiDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("VanBanChoPhep"))) entity.VanBanChoPhep = reader.GetString(reader.GetOrdinal("VanBanChoPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoPhuKien"))) entity.SoPhuKien = reader.GetString(reader.GetOrdinal("SoPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public PhuKienDangKyCollection SelectCollectionBy_HopDong_ID_KTX(string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_PhuKienDangKy_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            PhuKienDangKyCollection collection = new PhuKienDangKyCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                PhuKienDangKy entity = new PhuKienDangKy();
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayPhuKien"))) entity.NgayPhuKien = reader.GetDateTime(reader.GetOrdinal("NgayPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiDuyet"))) entity.NguoiDuyet = reader.GetString(reader.GetOrdinal("NguoiDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("VanBanChoPhep"))) entity.VanBanChoPhep = reader.GetString(reader.GetOrdinal("VanBanChoPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoPhuKien"))) entity.SoPhuKien = reader.GetString(reader.GetOrdinal("SoPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public PhuKienDangKyCollection SelectCollectionBy_HopDong_ID_KTX_KhongLayChuakhaiBao(string dbName)
        {
            SetDabaseMoi(dbName);

            PhuKienDangKyCollection collection = new PhuKienDangKyCollection();

            IDataReader reader = this.SelectReaderDynamic("HopDong_ID = " + this._HopDong_ID + " and TrangThaiXuLy != -1", "", null, dbName);
            while (reader.Read())
            {
                PhuKienDangKy entity = new PhuKienDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayPhuKien"))) entity.NgayPhuKien = reader.GetDateTime(reader.GetOrdinal("NgayPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiDuyet"))) entity.NguoiDuyet = reader.GetString(reader.GetOrdinal("NguoiDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("VanBanChoPhep"))) entity.VanBanChoPhep = reader.GetString(reader.GetOrdinal("VanBanChoPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoPhuKien"))) entity.SoPhuKien = reader.GetString(reader.GetOrdinal("SoPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression, string databaseName)
        {
            string spName = "p_KDT_GC_PhuKienDangKy_SelectDynamic";

            //Updated by HUNGTQ, 06/05/2011.
            SetDabaseMoi(databaseName);

            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        public int DeleteBy_HopDong_ID(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_PhuKienDangKy_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression, SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_PhuKienDangKy_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            if (transaction != null)
                return this.db.ExecuteReader(dbCommand, transaction);
            else
                return this.db.ExecuteReader(dbCommand);
        }

        public PhuKienDangKyCollection SelectCollectionDynamic(string whereCondition, string orderByExpression, SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            PhuKienDangKyCollection collection = new PhuKienDangKyCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression, transaction, dbName);
            while (reader.Read())
            {
                PhuKienDangKy entity = new PhuKienDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayPhuKien"))) entity.NgayPhuKien = reader.GetDateTime(reader.GetOrdinal("NgayPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiDuyet"))) entity.NguoiDuyet = reader.GetString(reader.GetOrdinal("NguoiDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("VanBanChoPhep"))) entity.VanBanChoPhep = reader.GetString(reader.GetOrdinal("VanBanChoPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoPhuKien"))) entity.SoPhuKien = reader.GetString(reader.GetOrdinal("SoPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("HuongdanPL"))) entity.HuongdanPL = reader.GetString(reader.GetOrdinal("HuongdanPL"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public PhuKienDangKyCollection SelectCollectionDynamicKTX(string whereCondition, string orderByExpression, SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            PhuKienDangKyCollection collection = new PhuKienDangKyCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression, transaction, dbName);
            while (reader.Read())
            {
                PhuKienDangKy entity = new PhuKienDangKy();

                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayPhuKien"))) entity.NgayPhuKien = reader.GetDateTime(reader.GetOrdinal("NgayPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiDuyet"))) entity.NguoiDuyet = reader.GetString(reader.GetOrdinal("NguoiDuyet"));
                if (!reader.IsDBNull(reader.GetOrdinal("VanBanChoPhep"))) entity.VanBanChoPhep = reader.GetString(reader.GetOrdinal("VanBanChoPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoPhuKien"))) entity.SoPhuKien = reader.GetString(reader.GetOrdinal("SoPhuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        #region Đồng bộ dữ liệu V3
        public string InsertFullFromISyncDaTa()
        {
            string error = string.Empty;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    List<HopDong> hdco = HopDong.SelectCollectionDynamic("SoHopDong = '" + this.SoHopDong.Trim()+"'", null);
                    if (hdco == null || hdco.Count == 0)
                    {
                        transaction.Rollback();
                        this.ID = 0;
                        connection.Close();
                        return "Không tồn tại hợp đồng của tờ khai";
                    }
                    else
                    {
                        this.HopDong_ID = hdco[0].ID;
                    }
                    this.ID = 0;
                    this.ID = this.InsertTransaction(transaction);
                   foreach (LoaiPhuKien item in this.PKCollection)
                   {
                       item.ID = 0;
                       item.Master_ID = this.ID;
                       item.ID = item.InsertTransaction(transaction);
                       foreach (HangPhuKien hangpk in item.HPKCollection)
                       {
                           hangpk.ID = 0;
                           hangpk.Master_ID = item.ID;
                           hangpk.InsertTransaction(transaction);
                       }
                   }
                   transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    error = ex.Message;
                }
                finally
                {
                    connection.Close();
                }
                return error;
            }

        }
        #endregion
    }
}
