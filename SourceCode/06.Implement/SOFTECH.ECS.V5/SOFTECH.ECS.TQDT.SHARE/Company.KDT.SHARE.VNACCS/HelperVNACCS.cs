﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
using Company.KDT.SHARE.Components;
using System.Net.Mail;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using System.Collections;
using System.Xml;
using GetPKCS7;
using System.Net;
using System.Net.Security;
using Company.KDT.SHARE.Components.Messages.SmartCA;
using System.Threading;
using System.Web.Script.Serialization;

namespace Company.KDT.SHARE.VNACCS
{
    public class HelperVNACCS
    {

        public static DataTable GeneratorTableValidate()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Name", typeof(string)); // Tên của chỉ tiêu
            dt.Columns.Add("Length", typeof(int)); // Độ dài chỉ tiêu
            dt.Columns.Add("Index", typeof(int)); // Thứ tự chỉ tiêu
            dt.Columns.Add("Required", typeof(bool)); // Có bắt buộc hay không
            dt.Columns.Add("Type", typeof(string)); // Nghiệp vụ
            dt.Columns.Add("Repetition", typeof(int));// Số lần lặp
            dt.Columns.Add("GroupRepetition", typeof(string)); // Nhóm lặp
            return dt;
        }
        public static void AddRowValidate(DataTable dt, string Name, int Length, int index, bool required, string type, int Repetition, string GroupRepetition)
        {
            if (dt == null || dt.Columns.Count == 0)
            {
                dt = GeneratorTableValidate();
            }
            DataRow[] listdr = dt.Select("Name = '" + Name + "'");
            if (listdr.Length > 0)
            {
                dt.Rows.Remove(listdr[0]);
            }
            DataRow dr = dt.NewRow();
            dr["Name"] = Name;
            dr["Length"] = Length;
            dr["Index"] = index;
            dr["Required"] = required;
            dr["Type"] = type;
            dr["Repetition"] = Repetition;
            dr["GroupRepetition"] = GroupRepetition;
            dt.Rows.Add(dr);
        }

        public static DataTable LoadDataValidate(string FileName)
        {
            try
            {
                string _fileName = Application.StartupPath + "\\Validate\\" + FileName;
                FileInfo xmlValidate = new FileInfo(_fileName);
                if (!xmlValidate.Exists)
                    return null;
                DataSet ds = new DataSet();
                ds.ReadXml(_fileName);
                if (ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                    return null;
                return ds.Tables[0];
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public static void SaveDataValidate(string FileName, DataTable dt)
        {
            try
            {
                string _fileName = Application.StartupPath + "\\Validate\\" + FileName;
                if (dt != null)
                    dt.WriteXml(_fileName, XmlWriteMode.WriteSchema);
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        public static string FormatNumber(object value, int digit, int decimalFormat, bool isRequired)
        {
            try
            {
                string ret = string.Empty;
                System.Globalization.CultureInfo cltInfo = new System.Globalization.CultureInfo("en-us");
                decimal round;
                string sFormat;
                if (value == null || string.IsNullOrEmpty(value.ToString().Trim()))
                {
                    value = "0";
                    sFormat = string.Empty;
                }
                else
                {
                    //round = Math.Round(System.Convert.ToDecimal(value), decimalFormat);
                    round = System.Convert.ToDecimal(value);
                    if (round == 0 && !isRequired) sFormat = string.Empty;
                    else
                        sFormat = round.ToString("0.#######", cltInfo.NumberFormat);
                }
                if (sFormat.Length < digit)
                {

                    sFormat = sFormat.PadLeft(digit);
                }

                return sFormat;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return value.ToString();
            }
        }
        public static string FormatString(object value, int digit)
        {
            if (value == null) value = "";
            string temp = string.Empty;
            //byte[] bytevalue = UTF8Encoding.UTF8.GetBytes(value.ToString());
            //if (bytevalue.Length > digit)
            //{
            //    MemoryStream memory = new MemoryStream();
            //    memory.Write(bytevalue, 0, digit);
            //    return UTF8Encoding.UTF8.GetString(memory.ToArray());
            //}
            //else
            //{ 
            //    MemoryStream memory = new MemoryStream();
            //    memory.Write(bytevalue, 0, bytevalue.Length);
            //    for (int i = 0; i < digit - bytevalue.Length; i++)
            //    {
            //        memory.Write(UTF8Encoding.UTF8.GetBytes(" "), 0, 1);
            //    } 
            //    return UTF8Encoding.UTF8.GetString(memory.ToArray());
            //}
            if (value == null || string.IsNullOrEmpty(value.ToString()))
                return temp.PadRight(digit);
            temp = value.ToString().TrimEnd().Replace("\r", string.Empty).Replace("\n", string.Empty);

            int lengStr = UTF8Encoding.UTF8.GetBytes(temp).Length;
            if (lengStr > digit)
            {
                string ex = temp + ": Có độ dài quá lớn";
                Logger.LocalLogger.Instance().WriteMessage(ex, new Exception());
                throw new Exception(ex);
            }
            else if (lengStr < digit)
            {
                int numberDigit = lengStr - temp.Length;
                return temp.PadRight(digit - numberDigit);
            }
            else
                return temp;
        }
        public static string FormatDateTime(object date)
        {
            if (date == null)
                date = "";
            string temp = date.ToString();
            if (string.IsNullOrEmpty(date.ToString()))
                temp = temp.PadRight(8);
            else
            {
                DateTime tempdate = Convert.ToDateTime(date);
                if (tempdate.Year < 2000)
                    temp = string.Empty.PadRight(8);
                else
                    temp = tempdate.ToString("ddMMyyyy");
            }
            return temp;
        }
        public static DateTime AddHoursToDateTime(DateTime date, string house)
        {
            if (string.IsNullOrEmpty(house) || string.IsNullOrEmpty(house.Trim()))
                return date;
            else
            {
                if (house.Length == 6)
                {
                    int HH = System.Convert.ToInt16(house.Substring(0, 2));
                    int MM = System.Convert.ToInt16(house.Substring(2, 2));
                    int SS = System.Convert.ToInt16(house.Substring(4, 2));
                    return new DateTime(date.Year, date.Month, date.Day, HH, MM, SS);
                }
                else
                    return date;
            }
        }
        #region Code OLD
        //public static string GetMIMEMessages(string content1, ref string boundary, bool signedNotpass, X509Certificate2 X509cert)
        //{
        //    //X509Certificate2 X509cert = Cryptography.GetStoreX509Certificate2(Globals.GetX509CertificatedName);
        //    //MailMessage mail = new MailMessage();
        //    //mail.Headers.Add("Host", "xxx.xxx.xxx.xxx:xxxx");
        //    //mail.Headers.Add("Cache-Control", "no-store,no-cache");
        //    //mail.Headers.Add("Pragma", "no-cache");
        //    //mail.Headers.Add("Content-Length", "46706");
        //    //mail.Headers.Add("Expect", "100-continue");
        //    //mail.Headers.Add("Connection", "Close");
        //    //mail.Body = strbody;
        //    //byte[] data = Encoding.UTF8.GetBytes(content1);
        //    //ContentInfo content = new ContentInfo(data);
        //    //SignedCms signedCms = new SignedCms(content, true);
        //    //CspParameters csp = new Company.KDT.SHARE.Components.RSACryptographyHelper(X509cert).GetCspParameters(Globals.PasswordSign);


        //    //Compute hash using SHA1

        //    //SHA1Managed sha1 = new SHA1Managed();
        //    //byte[] dataHash = sha1.ComputeHash(data);

        //    //ContentInfo ci = new ContentInfo(dataHash);
        //    //SignedCms cms = new SignedCms(ci);
        //    //CmsSigner signer = new CmsSigner(csp);
        //    //signer.IncludeOption = X509IncludeOption.EndCertOnly;

        //    //X509Chain chain = new X509Chain();
        //    //chain.ChainPolicy.RevocationMode = X509RevocationMode.NoCheck;
        //    //chain.Build(X509cert);

        //    //if (chain != null)
        //    //{
        //    //    signer.IncludeOption = X509IncludeOption.None;
        //    //    X509ChainElementEnumerator enumerator = chain.ChainElements.GetEnumerator();
        //    //    while (enumerator.MoveNext())
        //    //    {
        //    //        X509ChainElement current = enumerator.Current;
        //    //        signer.Certificates.Add(current.Certificate);
        //    //    }
        //    //}

        //    //signer.DigestAlgorithm = new Oid("SHA1");
        //    //cms.ComputeSignature(signer, false);
        //    //byte[] signedData = cms.Encode();




        //    #region ký số bắt buộc dùng mật khẩu
        //    //CmsSigner signer = new CmsSigner(csp);
        //    //signedCms.Certificates.Add(X509cert);
        //    //signer.SignerIdentifierType = SubjectIdentifierType.IssuerAndSerialNumber;
        //    //signer.IncludeOption = X509IncludeOption.WholeChain;
        //    //signer.DigestAlgorithm = new Oid(System.Security.Cryptography.CryptoConfig.MapNameToOID(new Company.KDT.SHARE.Components.RSACryptographyHelper(X509cert).GetHashAlgorithmName()));
        //    //signedCms.ComputeSignature(signer, false);
        //    //byte[] signedbytes = signedCms.Encode();

        //    //MIME mime = new MIME(signedbytes, "xxxx.xxxx.xxxx.xxxx:xxxx", "45678");
        //    //mime.ContentEdi = new StringBuilder().Append(content1);
        //    #endregion

        //    #region Ký số V3
        //    // Company.KDT.SHARE.Components.RSACryptographyHelper digitalsign = new Company.KDT.SHARE.Components.RSACryptographyHelper(X509cert);

        //    //RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider(csp);
        //    //string strAlgorithmOID = System.Security.Cryptography.CryptoConfig.MapNameToOID(digitalsign.GetHashAlgorithmName());
        //    //byte[] signedData = cryptoServiceProvider.SignData(data, strAlgorithmOID);
        //    ////MIME mime = new MIME(bArr, "xxxx.xxxx.xxxx.xxxx:xxxx", "45678");
        //    MIME mime = new MIME(signedNotpass ? signEdiNotPass(content1, X509cert) : signEdi(content1, X509cert), "xxxx.xxxx.xxxx.xxxx:xxxx", "45678");
        //    boundary = mime.boundary;
        //    mime.ContentEdi = new StringBuilder().Append(content1);

        //    #endregion
        //    boundary = mime.boundary;
        //    return mime.GetMessages().ToString();
        //}
        #endregion

        public static MIME GetMIME(string content1, X509Certificate2 X509cert)
        {
            try
            {
                MIME mime = new MIME(signEdi(content1, X509cert), "xxxx.xxxx.xxxx.xxxx:xxxx", "45678");
                mime.ContentEdi = new StringBuilder().Append(content1);
                return mime;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
        public static MIME GetMIME(string content, string strSign)
        {
            try
            {
                //System.IO.FileInfo fin;
                //System.IO.FileStream fs;
                //string filebase64 = "";
                //long filesize = 0;
                //long size;
                //byte[] data;
                //fs = new System.IO.FileStream(@"C:\Users\dangp\Downloads\091b00c2-ac8e-482a-8545-ac8eecbccefd.txt", System.IO.FileMode.Open, System.IO.FileAccess.Read);
                //size = 0;
                //size = fs.Length;
                //data = new byte[fs.Length];
                //fs.Read(data, 0, data.Length);
                //filebase64 = System.Convert.ToBase64String(data);
                //fs.Flush();
                //fs.Close();
                //strSign = filebase64;
                //byte[] unsignData;
                //unsignData = File.ReadAllBytes(@"C:\Users\dangp\Downloads\d3bafd02-dcaf-4ec3-bc01-2e427f999a25.txt");
                //string signedData = System.Convert.ToBase64String(unsignData);
                //strSign = signedData;
                //strSign = "MIAGCSqGSIb3DQEHAqCAMIACAQExDzANBglghkgBZQMEAgEFADCABgkqhkiG9w0BBwEAAKCAMIIExjCCA66gAwIBAgIQVAEBATnrXfoJvqGqEBfQajANBgkqhkiG9w0BAQsFADBeMRUwEwYDVQQDDAxWTlBUIFNNQVJUQ0ExIzAhBgNVBAsMGlZOUFQtU01BUlRDQSBUcnVzdCBOZXR3b3JrMRMwEQYDVQQKDApWTlBUIEdyb3VwMQswCQYDVQQGEwJWTjAeFw0yMTA5MTMxNTIyMDBaFw0yMjA5MTQwMzIyMDBaMIGEMQswCQYDVQQGEwJWTjESMBAGA1UECAwJSMOAIE7hu5hJMQ8wDQYDVQQHDAZRdeG6rW4xFDASBgNVBAwMC05ow6JuIHZpw6puMRowGAYDVQQDDBFOZ8O0IFF1YW5nIMSQ4bqhdDEeMBwGCgmSJomT8ixkAQEMDkNNTkQ6MTYyOTUyNTMwMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1zMAkF5iya1dWFhuK5xCjgMspehrG+Fe4BlXFrhdUxknNS2eraFb0t7XSq/CVv4+ZfPU+Ce7WYEYExCK2EMa4+La7d+MINyDsa7ttgDzVHnPHqOBAE+0/PwXTuMUapKT1NQ8XEicbFYEFxZkK+YKFQ4ejy8qQ072rAozbvhJdJPzbyjomQa58LFfT3xjiQAxLH8Hx7tKM1hFHq1KR9bwEmGJEhFBd+3fx3/10UL+rqPSY/mspQoT631gZNj6W8GDXBzWQKNOouerskkF2Lx9R5lx7xY/UfW05GGdLeA9SmLOHypA6A5phQ5ZCJq0OSnqt7bTwuPPhxIt+4Q2nmo48QIDAQABo4IBVzCCAVMwQgYIKwYBBQUHAQEENjA0MDIGCCsGAQUFBzAChiZodHRwOi8vcHViLnZucHQtY2Eudm4vY2VydHMvdm5wdGNhLmNlcjAdBgNVHQ4EFgQUgCWxryazRImf/UeeMhGDEoT1BNIwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBTLWGBhPr3TDeMCsYSdMaZ01btzijBsBgNVHSAEZTBjMGEGDisGAQQBge0DAQEDAQMBME8wJgYIKwYBBQUHAgIwGh4YAFMASQBEAC0AUwAxAC4AMAAtADAANgBtMCUGCCsGAQUFBwIBFhlodHRwOi8vcHViLnZucHQtY2Eudm4vcnBhMA4GA1UdDwEB/wQEAwIE8DAfBgNVHSUEGDAWBggrBgEFBQcDBAYKKwYBBAGCNwoDDDAgBgNVHREEGTAXgRVuZ29xdWFuZ2RhdEBnbWFpbC5jb20wDQYJKoZIhvcNAQELBQADggEBAF7rpAZVElh8pXjZVobm4wcxCVEfTwtWfF9k3ka5diMDDp6SeAGJQRe09lC/4ODF7J8IAz6sjO/aeN1cvgXpKhX9dEiI0ANq7qSxBqugN1YVgdT8m5rNwvDrz/UlH074nic0fV7kjM5NR6MoFbnh8Q9KduvB7Ad2z5JQI1cFb77DHO1tWEs/fOu3/h6U8xXYhMwuB+3+tMkSdrpN9dqPCvcsVNE75o3Cr6f0429XNi2IlRimHYXPtG5XyRYboKbQ7wGOlTw7wdQEdFgKu5CFm6U6r8xX2ZgFbnu3lATWoWiRzc0RWLjG/5UzYfsjC7WE0yfu2wRui5xxIXvcUGPbZwQAADGCAfwwggH4AgEBMHIwXjEVMBMGA1UEAwwMVk5QVCBTTUFSVENBMSMwIQYDVQQLDBpWTlBULVNNQVJUQ0EgVHJ1c3QgTmV0d29yazETMBEGA1UECgwKVk5QVCBHcm91cDELMAkGA1UEBhMCVk4CEFQBAQE56136Cb6hqhAX0GowDQYJYIZIAWUDBAIBBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0yMTEyMjIxMDI0MjdaMCMGCSqGSIb3DQEJBDEWBBRzLcUMGG/MF1wGAMeOlch6+2zaozANBgkqhkiG9w0BAQEFAASCAQDJbUSFB64A5RjsHJGsY9IqnIOxFIkE/gEaMfoccba7nAjqwDJeq38EitQJ5/AUGuCErc2X1ZWfobyEf2H0YFslvYzRqFQ3R/ZuiOJDhI53GGubQeiboP08eea1Ohqu+P79UsZPhpA8RhppzhohBdvzAiu2g/wkin4+JPgKa5moC5FE19Rt7ViF603f8jxu8GO4kWYDRF030+jpNnlMnOxBnGmGgrzlZGoD9+v/0s7ar3sgd0qgdDDTVQL6w+j6+YY8KVu+iSlOpQsaeIh9kDMd4cdWx2RES3ayaWdnvlJHrPFiIJ9CzLIqWTvtTw7LAVRD3K3YVtY8CuZuj59c1wj0AAAAAAAA";

                //byte[] data1 = Encoding.UTF8.GetBytes(strSign);
                //strSign = Convert.ToBase64String(data1);
                MIME mime = new MIME(strSign, "xxxx.xxxx.xxxx.xxxx:xxxx", "45678");
                mime.ContentEdi = new StringBuilder().Append(content);
                return mime;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
        public static string GetMIMEMessagesByEmail(string content1)
        {
            X509Certificate2 X509cert = Cryptography.GetStoreX509Certificate2(Globals.GetX509CertificatedName);
            MailMessage mail = new MailMessage();
            mail.Headers.Add("Host", "xxx.xxx.xxx.xxx:xxxx");
            mail.Headers.Add("Cache-Control", "no-store,no-cache");
            mail.Headers.Add("Pragma", "no-cache");
            mail.Headers.Add("Content-Length", "46706");
            mail.Headers.Add("Expect", "100-continue");
            mail.Headers.Add("Connection", "Close");
            mail.Body = content1;
            byte[] data = Encoding.UTF8.GetBytes(content1);
            ContentInfo content = new ContentInfo(data);
            SignedCms signedCms = new SignedCms(content, true);
            CspParameters csp = new Company.KDT.SHARE.Components.RSACryptographyHelper(X509cert).GetCspParameters(Globals.PasswordSign);

            #region //Compute hash using SHA1


            //SHA1Managed sha1 = new SHA1Managed();
            //byte[] dataHash = sha1.ComputeHash(data);

            //ContentInfo ci = new ContentInfo(dataHash);
            //SignedCms cms = new SignedCms(ci);
            //CmsSigner signer = new CmsSigner(csp);
            //signer.IncludeOption = X509IncludeOption.EndCertOnly;

            //X509Chain chain = new X509Chain();
            //chain.ChainPolicy.RevocationMode = X509RevocationMode.NoCheck;
            //chain.Build(X509cert);

            //if (chain != null)
            //{
            //    signer.IncludeOption = X509IncludeOption.None;
            //    X509ChainElementEnumerator enumerator = chain.ChainElements.GetEnumerator();
            //    while (enumerator.MoveNext())
            //    {
            //        X509ChainElement current = enumerator.Current;
            //        signer.Certificates.Add(current.Certificate);
            //    }
            //}

            //signer.DigestAlgorithm = new Oid("SHA1");
            //cms.ComputeSignature(signer,false);
            //byte[] signedData = cms.Encode();

            #endregion


            #region ký số bắt buộc dùng mật khẩu
            //CmsSigner signer = new CmsSigner(csp);
            //signedCms.Certificates.Add(X509cert);
            ////signer.SignerIdentifierType = SubjectIdentifierType.IssuerAndSerialNumber;
            //signer.IncludeOption = X509IncludeOption.WholeChain;
            //signer.DigestAlgorithm = new Oid(System.Security.Cryptography.CryptoConfig.MapNameToOID(new Company.KDT.SHARE.Components.RSACryptographyHelper(X509cert).GetHashAlgorithmName()));
            //signedCms.ComputeSignature(signer, false);
            //byte[] signedbytes = signedCms.Encode();
            //MemoryStream ms2 = new MemoryStream(signedbytes);
            //AlternateView av2 = new AlternateView(ms2, "application/pkcs7-mime; smime-type=signed-data;name=smime.p7m");
            //mail.AlternateViews.Add(av2);
            #endregion

            #region Ký số V3
            Company.KDT.SHARE.Components.RSACryptographyHelper digitalsign = new Company.KDT.SHARE.Components.RSACryptographyHelper(X509cert);

            RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider(csp);
            string strAlgorithmOID = System.Security.Cryptography.CryptoConfig.MapNameToOID(digitalsign.GetHashAlgorithmName());
            byte[] bArr = cryptoServiceProvider.SignData(data, strAlgorithmOID);
            MemoryStream ms = new MemoryStream(bArr);
            AlternateView av = new AlternateView(ms, "application/pkcs7-mime; smime-type=signed-data;name=smime.p7m");
            mail.AlternateViews.Add(av);
            #endregion
            mail.To.Add("ToToTo@ToTo.ToTo");
            mail.From = new System.Net.Mail.MailAddress("from@from.from");

            MemoryStream memory = ConvertMailMessageToMemoryStream(mail);

            return Encoding.ASCII.GetString(memory.ToArray());
        }

        private static MemoryStream ConvertMailMessageToMemoryStream(MailMessage message)
        {
            BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            Assembly assembly = typeof(SmtpClient).Assembly;
            MemoryStream stream = new MemoryStream();
            Type mailWriterType = assembly.GetType("System.Net.Mail.MailWriter");
            ConstructorInfo mailWriterContructor = mailWriterType.GetConstructor(flags, null, new[] { typeof(Stream) }, null);
            object mailWriter = mailWriterContructor.Invoke(new object[] { stream });
            MethodInfo sendMethod = typeof(MailMessage).GetMethod("Send", flags);
            sendMethod.Invoke(message, flags, null, new[] { mailWriter, true }, null);
            MethodInfo closeMethod = mailWriter.GetType().GetMethod("Close", flags);
            closeMethod.Invoke(mailWriter, flags, null, new object[] { }, null);
            return stream;
        }
        public static StringBuilder BuildEdiMessages<T>(object obj, string InputMsgID)
        {
            return BuildEdiMessages<T>(obj, string.Empty, InputMsgID);
        }
        public static StringBuilder BuildEdiMessages<T>(object obj, string MaNghiepVu, string InputMSGid)
        {
            StringBuilder strBody = GetStrBody<T>(obj, MaNghiepVu);
            if (strBody != null)
            {
                StringBuilder strHeader = new StringBuilder();
                VNACCHeader header = new VNACCHeader(string.IsNullOrEmpty(MaNghiepVu) ? GetMaNghiepVu<T>() : MaNghiepVu, strBody.Length, InputMSGid);
                strHeader = header.BuildEdiMessages<VNACCHeader>(strHeader, false, GlobalVNACC.PathConfig, GetMaNghiepVu<VNACCHeader>());
                if (strHeader != null && strHeader.Length > 0)
                {
                    strHeader.AppendLine();
                    strHeader.Append(strBody);
                    return strHeader;
                }
            }
            return new StringBuilder();
        }

        public static StringBuilder BuildEdiMessages(MessagesSend obj)
        {

            if (obj.Body != null && !string.IsNullOrEmpty(obj.Body.ToString()))
            {
                StringBuilder strHeader = new StringBuilder();
                strHeader = obj.Header.BuildEdiMessages<VNACCHeader>(strHeader, false, GlobalVNACC.PathConfig, GetMaNghiepVu<VNACCHeader>());
                if (strHeader != null && strHeader.Length > 0)
                {
                    strHeader.AppendLine();
                    strHeader.Append(obj.Body);
                    return strHeader;
                }
            }
            return new StringBuilder();
        }

        public static string GetMaNghiepVu<T>()
        {
            string MaNghiepVu = string.Empty;
            MaNghiepVu = typeof(T).ToString();
            string[] temp = MaNghiepVu.Split('.');
            MaNghiepVu = temp[temp.Length - 1];
            return MaNghiepVu;
        }
        public static StringBuilder GetStrBody<T>(object obj, string MaNghiepVuSua)
        {
            try
            {
                if (typeof(T) == typeof(IVA))
                {
                    IVA iva = obj as IVA;
                    return iva.BuilEdiMessagesIVA(new StringBuilder());
                }
                else if (typeof(T) == typeof(IDA))
                {
                    IDA ida = obj as IDA;
                    if (MaNghiepVuSua == EnumNghiepVu.IDA01)
                        return ida.BuilEdiMessagesIDA01(new StringBuilder());
                    return ida.BuilEdiMessagesIDA(new StringBuilder());
                }
                else if (typeof(T) == typeof(EDA))
                {
                    EDA eda = obj as EDA;
                    if (MaNghiepVuSua == EnumNghiepVu.EDA01)
                        return eda.BuilEdiMessagesEDA01(new StringBuilder());
                    return eda.BuilEdiMessagesEDA(new StringBuilder());
                }
                else if (typeof(T) == typeof(MIC))
                {
                    MIC mic = obj as MIC;
                    if (MaNghiepVuSua == EnumNghiepVu.MIE)
                        return mic.BuidMessagesEdiMIE(new StringBuilder());
                    return mic.BuildEdiMessages<MIC>(new StringBuilder(), true, GlobalVNACC.PathConfig, EnumNghiepVu.MIC);
                }
                else if (typeof(T) == typeof(MEC))
                {
                    MEC mec = obj as MEC;
                    if (MaNghiepVuSua == EnumNghiepVu.MEE)
                        return mec.BuidMessagesEdiMEE(new StringBuilder());
                    return mec.BuildEdiMessages<MEC>(new StringBuilder(), true, GlobalVNACC.PathConfig, EnumNghiepVu.MEC);
                }
                else if (typeof(T) == typeof(SAA))
                {
                    SAA saa = obj as SAA;
                    return saa.BuilEdiMessagesSAA(new StringBuilder());
                }
                else if (typeof(T) == typeof(SEA))
                {
                    SEA saa = obj as SEA;
                    return saa.BuilEdiMessagesSEA(new StringBuilder());
                }
                else if (typeof(T) == typeof(SFA))
                {
                    SFA saa = obj as SFA;
                    return saa.BuilEdiMessagesSFA(new StringBuilder());
                }
                else if (typeof(T) == typeof(SMA))
                {
                    SMA saa = obj as SMA;
                    return saa.BuilEdiMessagesSMA(new StringBuilder());
                }
                else if (typeof(T) == typeof(OLA))
                {
                    OLA ola = obj as OLA;
                    return ola.BuilEdiMessagesOLA(new StringBuilder());
                }
                else if (typeof(T) == typeof(AMA))
                {
                    AMA ama = obj as AMA;
                    return ama.BuilEdiMessagesSFA(new StringBuilder());
                }
                else if (typeof(T) == typeof(TEA))
                {
                    TEA tea = obj as TEA;
                    return tea.BuilEdiMessagesTEA(new StringBuilder());
                }
                else if (typeof(T) == typeof(TIA))
                {
                    TIA tia = obj as TIA;
                    return tia.BuilEdiMessagesTIA(new StringBuilder());
                }
                else if (typeof(T) == typeof(COT))
                {
                    COT cot = obj as COT;
                    return cot.BuilEdiMessagesOLA(new StringBuilder());
                }
                else
                {
                    BasicVNACC bVnacc = obj as BasicVNACC;
                    return bVnacc.BuildEdiMessages<T>(new StringBuilder(), true, GlobalVNACC.PathConfig, GetMaNghiepVu<T>());

                }

            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;

            }
        }
        public static void SaveFileEdi(string path, StringBuilder messages, string name)
        {
            try
            {
                string pathedi = path + "\\" + name + DateTime.Now.ToString("yyyyMMddhhmmss") + ".txt";
                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);
                if (!System.IO.File.Exists(pathedi))
                {
                    TextWriter tw = new StreamWriter(pathedi, true);
                    tw.Write(messages);
                    tw.Close();
                }
                else if (System.IO.File.Exists(pathedi))
                {
                    TextWriter tw = new StreamWriter(pathedi, true);
                    tw.WriteLine("The next line!");
                    tw.Close();
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        public static void SaveFileEdi(string path, StringBuilder messages)
        {
            try
            {
                string pathedi = path;
                //if (!System.IO.Directory.Exists(path))
                //    System.IO.Directory.CreateDirectory(path);
                if (!System.IO.File.Exists(pathedi))
                {
                    //File.Create(pathedi,500000,FileOptions.RandomAccess);

                    TextWriter tw = new StreamWriter(pathedi, true);
                    tw.Write(messages);
                    tw.Close();
                }
                else if (System.IO.File.Exists(pathedi))
                {
                    TextWriter tw = new StreamWriter(pathedi, true);
                    tw.WriteLine("The next line!");
                    tw.Close();
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        //public const string APP_ID = "4094-637750986327728798.apps.smartcaapi.com";
        //public const string APP_SECRET = "ZDJkZmZkZWU-NDY0YS00YmRm";
        //public const string SERVICE_GET_TOKENURL = "https://rmgateway.vnptit.vn/auth/token";
        //public const string DEFAULT_SERVICE = "https://gateway.vnpt-ca.vn/signservice/v4/api_gateway";
        public static bool CheckSmartCA(string customerEmail, string customerPass, string refresh_token)
        {
            try
            {
                var access_token = HelperVNACCS.GetAccessToken(customerEmail, customerPass, refresh_token);
                if (access_token != null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static string GetAccessToken(string Email, string Password, string RefreshToken)
        {
            try
            {
                RefreshToken = "";
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)768 | (SecurityProtocolType)3072;
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateRemoteCertificate);
                var accessToken = new AccessTokenSmartCA
                {
                    grant_type = "password",
                    username = Email,
                    password = Password,
                    client_id = Company.KDT.SHARE.Components.Globals.APP_ID,
                    client_secret = Company.KDT.SHARE.Components.Globals.APP_SECRET
                };
                var javaScriptSerializer = new JavaScriptSerializer();
                var parameter = javaScriptSerializer.Serialize(accessToken);

                //var parameter = JsonConvert.SerializeObject(accessToken);

                System.Net.WebClient client = new System.Net.WebClient();
                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                string myParameters = String.Format("grant_type={0}&username={1}&password={2}&client_id={3}&client_secret={4}", accessToken.grant_type, accessToken.username, accessToken.password, accessToken.client_id, accessToken.client_secret);
                var response = client.UploadString(new Uri(Company.KDT.SHARE.Components.Globals.SERVICE_GET_TOKENURL), myParameters);

                if (response != null)
                {
                    var responseAccessToken = javaScriptSerializer.Deserialize<ResponseAccessToken>(response);
                    //var responseAccessToken = JsonConvert.DeserializeObject<ResponseAccessToken>(response);
                    RefreshToken = responseAccessToken.Refresh_Token;

                    Company.KDT.SHARE.Components.Globals.ACCESS_TOKEN = responseAccessToken.Access_Token;
                    Company.KDT.SHARE.Components.Globals.REFRESH_TOKEN = responseAccessToken.Refresh_Token;
                    DateTime date = DateTime.Now;
                    date = date.AddSeconds(Convert.ToInt32(responseAccessToken.Expires_in));
                    Company.KDT.SHARE.Components.Globals.EXPIRES_TOKEN = date;

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ACCESS_TOKEN", responseAccessToken.Access_Token);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("REFRESH_TOKEN", responseAccessToken.Refresh_Token);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("EXPIRES_TOKEN", date.ToString("yyyy-MM-dd HH:mm:ss"));

                    return responseAccessToken.Access_Token;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public static ResponseMessage Query(RequestMessage requestMessage, string accessToken)
        {
            try
            {
                // Verify service certificate. 
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)768 | (SecurityProtocolType)3072;
                ServicePointManager.DefaultConnectionLimit = 100;
                ServicePointManager.MaxServicePointIdleTime = 5000;
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateRemoteCertificate);
                var javaScriptSerializer = new JavaScriptSerializer();
                var parameter = javaScriptSerializer.Serialize(requestMessage);

                //var parameter = JsonConvert.SerializeObject(requestMessage);
                System.Net.WebClient client = new System.Net.WebClient();
                client.UseDefaultCredentials = true;
                client.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                client.Headers.Add("Authorization", String.Format("Bearer {0}", accessToken));
                var response = client.UploadString(new Uri(Company.KDT.SHARE.Components.Globals.DEFAULT_SERVICE), parameter);
                if (response != null)
                {
                    return javaScriptSerializer.Deserialize<ResponseMessage>(response);
                    //return JsonConvert.DeserializeObject<ResponseMessage>(response);
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }

        public static String Query(object Request, string ServiceUri, string AccessToken)
        {
            try
            {
                // Verify service certificate. 
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)768 | (SecurityProtocolType)3072;
                ServicePointManager.DefaultConnectionLimit = 100;
                ServicePointManager.MaxServicePointIdleTime = 5000;
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(ValidateRemoteCertificate);
                var javaScriptSerializer = new JavaScriptSerializer();
                var parameter = javaScriptSerializer.Serialize(Request);

                //var parameter = JsonConvert.SerializeObject(Request);
                System.Net.WebClient client = new System.Net.WebClient();
                client.UseDefaultCredentials = true;
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                client.Headers.Add("Authorization", String.Format("Bearer {0}", AccessToken));

                var response = client.UploadString(new Uri(ServiceUri), parameter);
                if (response != null)
                {
                    return response;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public static String GetCredentialSmartCA(String AccessToken, String ServiceUri)
        {
            try
            {
                var response = Query(new RequestCredentialSmartCA(), ServiceUri, AccessToken);

                if (response != null)
                {
                    var javaScriptSerializer = new JavaScriptSerializer();
                    CredentialSmartCAResponse credentials = javaScriptSerializer.Deserialize<CredentialSmartCAResponse>(response);

                    //CredentialSmartCAResponse credentials = JsonConvert.DeserializeObject<CredentialSmartCAResponse>(response);
                    return credentials.Content[0];
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public static String GetAccoutSmartCACert(String AccessToken, String ServiceUri, string CredentialId)
        {
            try
            {
                var response = Query(new RequestCertificateSmartCA
                {
                    credentialId = CredentialId,
                    certificates = "chain",
                    certInfo = true,
                    authInfo = true
                }, ServiceUri, AccessToken);
                if (response != null)
                {
                    var javaScriptSerializer = new JavaScriptSerializer();
                    CertificateSmartCAResponse req = javaScriptSerializer.Deserialize<CertificateSmartCAResponse>(response);

                    //CertificateSmartCAResponse req = JsonConvert.DeserializeObject<CertificateSmartCAResponse>(response);
                    String certBase64 = req.Cert.Certificates[0];
                    return certBase64.Replace("\r\n", "");
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public static TranInfoSmartCAResp GetTranInfo(string AccessToken, String ServiceUri, String TranId)
        {
            try
            {
                var response = Query(new ContenSignHash
                {
                    tranId = TranId
                }, ServiceUri, AccessToken);

                if (response != null)
                {
                    var javaScriptSerializer = new JavaScriptSerializer();
                    TranInfoSmartCAResp resp = javaScriptSerializer.Deserialize<TranInfoSmartCAResp>(response);
                    //TranInfoSmartCAResp resp = JsonConvert.DeserializeObject<TranInfoSmartCAResp>(response);
                    return resp;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public static string SignHash(string AccessToken, String ServiceUri, string Data, string CredentialId)
        {
            try
            {

                var SignHashSmartCA = new SignHashSmartCA
                {
                    credentialId = CredentialId,
                    refTranId = Guid.NewGuid().ToString(),
                    notifyUrl = "https://softech.vn/",
                    description = "Softech",
                    datas = new List<DataSignHash>()
                };
                var DataSignHash = new DataSignHash
                {
                    name = "CMS",
                    hash = Data
                };
                SignHashSmartCA.datas.Add(DataSignHash);

                var response = Query(SignHashSmartCA, ServiceUri, AccessToken);
                if (response != null)
                {
                    var javaScriptSerializer = new JavaScriptSerializer();
                    SignHashSmartCAResponse signHashSmartCAResponse = javaScriptSerializer.Deserialize<SignHashSmartCAResponse>(response);

                    //SignHashSmartCAResponse signHashSmartCAResponse = JsonConvert.DeserializeObject<SignHashSmartCAResponse>(response);
                    if (signHashSmartCAResponse.Code == 0)
                    {
                        return signHashSmartCAResponse.Content.tranId;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private static string Sign(string _unsignBase64, string fileExtension, string contentType, string certId, string access_token)
        {
            try
            {
                var response = HelperVNACCS.Query(new RequestMessage
                {
                    RequestID = Guid.NewGuid().ToString(),
                    ServiceID = "SignServer",
                    FunctionName = "Sign",
                    Parameter = new SignParameter
                    {
                        CertID = certId,
                        Type = fileExtension,
                        ContentType = contentType,
                        DataBase64 = _unsignBase64
                    }
                }, access_token);
                if (response != null)
                {
                    var javaScriptSerializer = new JavaScriptSerializer();
                    var str = javaScriptSerializer.Serialize(response.Content);
                    SignResponse signResponse = javaScriptSerializer.Deserialize<SignResponse>(str);

                    //var str = JsonConvert.SerializeObject(response.Content);
                    //SignResponse signResponse = JsonConvert.DeserializeObject<SignResponse>(str);
                    if (signResponse != null)
                    {
                        return signResponse.SignedData;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public static string SignOffice(string certId, string access_token, string FileName, string Data)
        {
            try
            {
                // Sign data using service api
                var fileExtension = "docx";
                var fileContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                var signedBase64 = Sign(Data, fileExtension, fileContentType, certId, access_token);
                if (!string.IsNullOrEmpty(signedBase64))
                {
                    return signedBase64;
                    File.WriteAllBytes(Application.StartupPath + "\\" + FileName + "_Signed.docx", Convert.FromBase64String(signedBase64));
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public static string SignXml(string certId, string access_token, string FileName, string Data)
        {
            try
            {
                // Sign data using service api
                var fileExtension = "xml";
                var fileContentType = "text/xml";
                var signedBase64 = Sign(Data, fileExtension, fileContentType, certId, access_token);
                if (!string.IsNullOrEmpty(signedBase64))
                {
                    return signedBase64;
                    File.WriteAllBytes(Application.StartupPath + "\\" + FileName + "_Signed.xml", Convert.FromBase64String(signedBase64));
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public static string SignPdf(string certId, string access_token, string FileName, string Data)
        {
            try
            {
                // Sign data using service api
                var fileExtension = "pdf";
                var fileContentType = "application/pdf";
                var signedBase64 = Sign(Data, fileExtension, fileContentType, certId, access_token);
                if (!string.IsNullOrEmpty(signedBase64))
                {
                    return signedBase64;
                    File.WriteAllBytes(Application.StartupPath + "\\" + FileName + "_Signed.pdf", Convert.FromBase64String(signedBase64));
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }

        }
        public static string Sign(string AccessToken, String ServiceUri, string Data, string CredentialId)
        {
            try
            {
                string RefTranId = Guid.NewGuid().ToString();
                var SignSmartCA = new SignSmartCA
                {
                    credentialId = CredentialId,
                    refTranId = RefTranId,
                    notifyUrl = "https://softech.vn/",
                    description = "XML",
                    datas = new List<DataSign>()
                };

                byte[] bytes = Encoding.UTF8.GetBytes(Data);
                String dataBase64 = Convert.ToBase64String(bytes);

                File.WriteAllText(Application.StartupPath + "\\" + RefTranId + ".xml", Data);

                //Byte[] bytes = File.ReadAllBytes(Application.StartupPath + "\\" + RefTranId + ".xml");

                var DataSign = new DataSign
                {
                    name = "" + RefTranId + ".xml",
                    dataBase64 = dataBase64
                };
                SignSmartCA.datas.Add(DataSign);

                var response = Query(SignSmartCA, ServiceUri, AccessToken);
                if (response != null)
                {
                    var javaScriptSerializer = new JavaScriptSerializer();
                    SignHashSmartCAResponse signHashSmartCAResponse = javaScriptSerializer.Deserialize<SignHashSmartCAResponse>(response);
                    //SignHashSmartCAResponse signHashSmartCAResponse = JsonConvert.DeserializeObject<SignHashSmartCAResponse>(response);
                    if (signHashSmartCAResponse.Code == 0)
                    {
                        return signHashSmartCAResponse.Content.tranId;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public static string SignDocument(string AccessToken, String ServiceUri, string Data, string CredentialId, string FileName, String Content)
        {
            try
            {
                var SignSmartCA = new SignSmartCA
                {
                    credentialId = CredentialId,
                    refTranId = FileName,
                    notifyUrl = "https://softech.vn/",
                    description = FileName,
                    datas = new List<DataSign>()
                };

                //byte[] bytes = Encoding.UTF8.GetBytes(Data);
                //String dataBase64 = Convert.ToBase64String(bytes);

                File.WriteAllText(Application.StartupPath + "\\" + FileName + "", Content);

                //Byte[] bytes = File.ReadAllBytes(Application.StartupPath + "\\" + RefTranId + ".xml");

                var DataSign = new DataSign
                {
                    name = FileName,
                    dataBase64 = Content
                };
                SignSmartCA.datas.Add(DataSign);

                var response = Query(SignSmartCA, ServiceUri, AccessToken);
                if (response != null)
                {
                    var javaScriptSerializer = new JavaScriptSerializer();
                    SignHashSmartCAResponse signHashSmartCAResponse = javaScriptSerializer.Deserialize<SignHashSmartCAResponse>(response);

                    //SignHashSmartCAResponse signHashSmartCAResponse = JsonConvert.DeserializeObject<SignHashSmartCAResponse>(response);
                    if (signHashSmartCAResponse.Code == 0)
                    {
                        return signHashSmartCAResponse.Content.tranId;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public static bool ValidateRemoteCertificate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors policyErrors)
        {
            bool result = true;
            return result;
        }
        public static string signEdi(string dataToSign, X509Certificate2 cert)
        {
            try
            {
                // var store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
                // store.Open(OpenFlags.ReadOnly);
                if (cert != null)
                {
                    Helpers.GetSignature("Test");
                    if (GetNameSigner(cert.IssuerName.Name) == "VNPT Certification Authority")
                    {

                        byte[] data = Encoding.UTF8.GetBytes(dataToSign);
                        //var smartCASigned = "MIIHswYJKoZIhvcNAQcCoIIHpDCCB6ACAQExDzANBglghkgBZQMEAgEFADALBgkqhkiG9w0BBwGgggVxMIIFbTCCBFWgAwIBAgIQVAEBAc4aUp0pLIr1CvA3eTANBgkqhkiG9w0BAQsFADBcMQswCQYDVQQGEwJWTjEzMDEGA1UECgwqVklFVE5BTSBQT1NUUyBBTkQgVEVMRUNPTU1VTklDQVRJT05TIEdST1VQMRgwFgYDVQQDDA9WTlBUIFNtYXJ0Q0EgUlMwHhcNMjIwMTE5MDk0NjAwWhcNMjMwMTE5MjE0NjAwWjCBtTELMAkGA1UEBhMCVk4xFDASBgNVBAgMC8SQw4AgTuG6tE5HMUAwPgYDVQQHDDczOCBZw6puIELDoWkgUGjGsOG7nW5nIEjhuqNpIENow6J1IEkgUXXhuq1uIEjhuqNpIENow6J1MSowKAYDVQQDDCFUZXN0IEPDlE5HIFRZIEPhu5QgUEjhuqZOIFNPRlRFQ0gxIjAgBgoJkiaJk/IsZAEBDBJNU1Q6MDQwMDM5MjI2My0yOTgwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDA2CyQpYTEnOJbMx+MgZ/hQNBtlU9YtF0lT9tlwevbBiyj83kXz0gv3d0DFjzU6rQ4RSVYanwmvFZwlO5KE4Kz1iNEwzsDlAOnRG4XK9JEvCjL+KPe49yb1FQGV/5YN1tntZLR5xhkuHRSiaCLiFpx+CDmPG9+T3t2wgZPML9DDX7ro411leI6eJUp1DU/w6L9BUzuErIRSrtgU4QsPyPu0HDKhaNbk2qZggNsI1AvPlsRyeWiE3MYp0iFALov/rvnV2ydyqF+NrEkc640S+zcKR2i1Np1FMfGQHqHoRoDdwiKNnsj8ViU5rZK7nfHfV964z0j0fRpP7HPx4PPAjbHAgMBAAGjggHPMIIByzCBgAYIKwYBBQUHAQEEdDByMDoGCCsGAQUFBzAChi5odHRwOi8vcHViLnZucHQtY2Eudm4vY2VydHMvdm5wdGNhLXNtYXJ0Y2EuY2VyMDQGCCsGAQUFBzABhihodHRwOi8vb2NzcC1zbWFydGNhLnZucHQtY2Eudm4vcmVzcG9uZGVyMB0GA1UdDgQWBBQXXfvTAx7bzdbvAC1UjtXYInwbtDAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFF/vxOs69iL3ctLaGTwpJVm7oP0vMGgGA1UdIARhMF8wXQYOKwYBBAGB7QMBAQMBAQEwSzAiBggrBgEFBQcCAjAWHhQATwBJAEQALQBTAFQALQAyAC4AMDAlBggrBgEFBQcCARYZaHR0cDovL3B1Yi52bnB0LWNhLnZuL3JwYTBBBgNVHR8EOjA4MDagNKAyhjBodHRwOi8vY3JsLXNtYXJ0Y2Eudm5wdC1jYS52bi92bnB0Y2Etc21hcnRjYS5jcmwwDgYDVR0PAQH/BAQDAgTwMCAGA1UdJQQZMBcGCisGAQQBgjcKAwwGCSqGSIb3LwEBBTAZBgNVHREEEjAQgQ5uZ29kYXRAdm5wdC52bjANBgkqhkiG9w0BAQsFAAOCAQEAeZ2D8yEQ9s5DQSqgj8Xva6fC1upFay7rOWd3QECHTnUxWVfbGmqdYRxLeuGv7y7L22T7plob8zwR3bxMf01A6KyPs8UqKV0B8d4DH/mXLlkSNUqx3ewtggDwvf3cBQ/C4shkBGJBsoxLhev7PDo1ykD/nYkPOZh2K1j9yj1ckugBkZol7Q6JUW2vPcq734cVUom/ztf0uftbVYuOImbbDkNe/L80T7/PxUU9Zz0EFTD3KTBg/hThktDrW1STExmXLnNFaa0hgwWPkT/M8PbMZGehwyde7Z5RIue5HGOoQfn4itCmi50OWiswBYEYGwnzDN7XCbtQFcrtNOtypKcGFDGCAgYwggICAgEBMHAwXDELMAkGA1UEBhMCVk4xMzAxBgNVBAoMKlZJRVROQU0gUE9TVFMgQU5EIFRFTEVDT01NVU5JQ0FUSU9OUyBHUk9VUDEYMBYGA1UEAwwPVk5QVCBTbWFydENBIFJTAhBUAQEBzhpSnSksivUK8Dd5MA0GCWCGSAFlAwQCAQUAoGkwGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMjIwMTE5MTAxMzQ4WjAvBgkqhkiG9w0BCQQxIgQgr/elqMrVfqVkoKRtDFsZlda6pAU+haOkF4+raK3dllkwDQYJKoZIhvcNAQEBBQAEggEAcUvepQgS1ub9PGW27dC9oW7SwH/oepuQeRIZjInqWi7h6A3o8suTgMbrT3O2SgdPocV+PNUnYKfZ5EtHCquIXVs/Xs4a9eIsbQ/Q6OU4s9/AdUPiKewGDlTK+BdwlPzaVcbykF1likxFBLoy5jpHVWm8M+Wk1czTmC/aUntZ66m4dFtMEpr16iYTO7m14JUuw8QQrHWE25m0OapXtPhQjefjNGxsnb862Aj5PjvxZ1kumEqqUVzuThB69QgemPsFa8UkGlLviTsif8BK2M8TJu3HmNhC/WAsmsC/SIK+hLGAU9GPkyPXeWp40rxBjfMo+s8DNEbjAlcacfqBSd33zA==";
                        //var VNPTSigned = "MIIIfwYJKoZIhvcNAQcCoIIIcDCCCGwCAQExDzANBglghkgBZQMEAgEFADALBgkqhkiG9w0BBwGgggYwMIIGLDCCBBSgAwIBAgIQVAEBAUjA8QLp4XHaBUP5sjANBgkqhkiG9w0BAQUFADBpMQswCQYDVQQGEwJWTjETMBEGA1UEChMKVk5QVCBHcm91cDEeMBwGA1UECxMVVk5QVC1DQSBUcnVzdCBOZXR3b3JrMSUwIwYDVQQDExxWTlBUIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTIxMDUxODA0MjgwMFoXDTIyMDUxODE2MjgwMFowbTELMAkGA1UEBhMCVk4xFjAUBgNVBAgMDULDjE5IIMSQ4buKTkgxDzANBgNVBAcMBlF14bqtbjEUMBIGA1UEAwwLxJDhuqF0IFRlc3QxHzAdBgoJkiaJk/IsZAEBDA9DTU5EOjExMjMxMjMyMjIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCfR/vFK2ANkCczV+Rj2lHPfRcQWJ9vRrHwm9/XfRjUCOD40/Yo+oKEBSuPYsuaJFNbnVLvH2X4fy+0tEPFoYNrg3efuEf+90m+y0V4WUvy4LYlYroUe78pM0DmvRPJgJOwyC40qvs023mhRR/e3lT19vAmSUYNCICNv3ddUEuL3WyZhdKVGXYII5hijj/8ZCqqOPnYolSG/hLWeeCJJl+jnBpfqWQaSj50qWXFYhyIm887/7X19nlEr5n3PAqTu/CDI7kjcPvoBZ7l+m65q7S1MVqNLujLGnt1FI+jQoxB+ErjnhtRST9DBeAGG3a7ApGnk94trvtWGd0jaOLETp4XAgMBAAGjggHKMIIBxjBwBggrBgEFBQcBAQRkMGIwMgYIKwYBBQUHMAKGJmh0dHA6Ly9wdWIudm5wdC1jYS52bi9jZXJ0cy92bnB0Y2EuY2VyMCwGCCsGAQUFBzABhiBodHRwOi8vb2NzcC52bnB0LWNhLnZuL3Jlc3BvbmRlcjAdBgNVHQ4EFgQUUXzsdZBrQctLhsvlMs4h9ATdh5swDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBQGacDV1QKKFY1Gfel84mgKVaxqrzBsBgNVHSAEZTBjMGEGDisGAQQBge0DAQEDAQMCME8wJgYIKwYBBQUHAgIwGh4YAFMASQBEAC0AUAAyAC4AMAAtADUAMQBtMCUGCCsGAQUFBwIBFhlodHRwOi8vcHViLnZucHQtY2Eudm4vcnBhMDEGA1UdHwQqMCgwJqAkoCKGIGh0dHA6Ly9jcmwudm5wdC1jYS52bi92bnB0Y2EuY3JsMA4GA1UdDwEB/wQEAwIE8DA0BgNVHSUELTArBggrBgEFBQcDAgYIKwYBBQUHAwQGCisGAQQBgjcKAwwGCSqGSIb3LwEBBTAdBgNVHREEFjAUgRJodXluYy45NEBnbWFpbC5jb20wDQYJKoZIhvcNAQEFBQADggIBAHowDIN7XiUH35Aq5SyjCt77qqoS2qzCe4j5dBj8ShZyyOCRijpHjOsxc0Bm7huSQSSZxS+37gsnOF+mqO55LZ0IW55z6+J/vSkCpgihNM19C5h0eHl841QbVwc7KLKFOVhkCoDWl4do048ukE+CiHMYz52LnPxbQLu//IayL1tG+loyQqAH/ZisYVEEzndBHHhln+oO9mr5PYhD8G0rwI2NCnIa6UpKZwojJBPpH5iU4Mg0Rgd3gAp5NSfRycaWITJmZ26MAjBaJtzgrBGaAzBt69bbuXanLYpDTN7NANzwS8tbRF0BIh9dzSrtAUx3BzE/CDh2PzoF0SmnzjwTgJ8jnwhraGyy5dx66YXlOtS6LtyWX/RjmMc7BgsZW6XK8ckTIIRaak1TcKSAYSYyMNQkHR/a5McEEONGsLAAZjL6XRD09Fe+M9LNEcNg7v+KNpe92xrw+uyHZ6uFt0GeMtSWRfDPV12FCjgFskn0eEzbN3IlkLtrOPYws1an8MWp3cVTB47dGA7frR4LyZiqcYWf5dAHUXt/BFaRc2HbP9G2XJFL0cwdpC8tmGuSK0gVNpdrW+J8fynKqm0tKVwSV/Avd2R2uP1yUbCiQfd41keEvhhZ6o14PZMozKHQfWIhuh/ssZkBeO933oCs+MZJYNORhtjGC/0bojKeTjiw6P6IMYICEzCCAg8CAQEwfTBpMQswCQYDVQQGEwJWTjETMBEGA1UEChMKVk5QVCBHcm91cDEeMBwGA1UECxMVVk5QVC1DQSBUcnVzdCBOZXR3b3JrMSUwIwYDVQQDExxWTlBUIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AhBUAQEBSMDxAunhcdoFQ/myMA0GCWCGSAFlAwQCAQUAoGkwGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMjIwMTE5MTAxMDMxWjAvBgkqhkiG9w0BCQQxIgQgr/elqMrVfqVkoKRtDFsZlda6pAU+haOkF4+raK3dllkwDQYJKoZIhvcNAQEBBQAEggEAS1dsPLXwtBnBHlKNWpbprToVWZNyv3sC6q27/FoGTyOp+HoHsVYPFKa/f25t7zKFDC2BZ4VxELtmKrx4dfKMia5wqLFkK2AXP2CEbAHtEAuwxJWQrQlykUpqXMna0nWdU11HGG02g77XLNH6rc5v79Ofs+mCq87TIcIe1B1RBv4CZwEhkeNE7OpWke1/9kUgC5xuq108C2QiFZSoVlvfKa0Mh9dxDo0QIT+pgmQLsAwqTTPLlRiPRQuC2DDWewSAj4Hz8/lLjOKFelcvV2+FP1PPWsLs4tnlxLV8jaUT7c8t2eCFgSNF6+bgTdR9KpXHZW3CCEwIfWNzl3B5dchTUw==";
                        //return VNPTSigned;
                        //var VNPTSigned = Convert.ToBase64String(GetPKCS7Signed.CreatePKCS7(data, cert, Globals.PasswordSign));
                        return Convert.ToBase64String(GetPKCS7Signed.CreatePKCS7(data, cert, Globals.PasswordSign));
                    }
                    else if (GetNameSigner(cert.IssuerName.Name).Contains("Viettel-CA"))
                    {
                        var data = Encoding.UTF8.GetBytes(dataToSign);
                        ContentInfo contentInfo = new ContentInfo(data);
                        SignedCms signedCms = new SignedCms(contentInfo, false);
                        CmsSigner cmsSigner = new CmsSigner(cert);
                        cmsSigner.IncludeOption = X509IncludeOption.EndCertOnly;
                        cmsSigner.SignedAttributes.Add(new Pkcs9SigningTime());
                        signedCms.ComputeSignature(cmsSigner,false);
                        byte[] encoded = signedCms.Encode();
                        return Convert.ToBase64String(encoded);
                    }
                    else
                    {
                        var data = Encoding.UTF8.GetBytes(dataToSign);
                        var digestOid = new Oid("1.2.840.113549.1.7.2"); //pkcs7 signed
                        var content = new ContentInfo(digestOid, data);
                        var signedCms = new SignedCms(SubjectIdentifierType.IssuerAndSerialNumber, content, true); //detached=true
                        var singer = new CmsSigner(cert) { DigestAlgorithm = new Oid("1.3.14.3.2.26") };
                        signedCms.ComputeSignature(singer, false);
                        var signEnv = signedCms.Encode();
                        return Convert.ToBase64String(signEnv);
                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw e;

            }
            return null;
        }
        public static byte[] signEdiNotPass(string dataToSign, X509Certificate2 cert)
        {

            if (cert != null)
            {
                CspParameters csp = new Company.KDT.SHARE.Components.RSACryptographyHelper(cert).GetCspParameters(Globals.PasswordSign);
                Company.KDT.SHARE.Components.RSACryptographyHelper digitalsign = new Company.KDT.SHARE.Components.RSACryptographyHelper(cert);
                var data = Encoding.UTF8.GetBytes(dataToSign);
                //var hash = sha1.ComputeHash(data);
                RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider(csp);

                string strAlgorithmOID = System.Security.Cryptography.CryptoConfig.MapNameToOID("SHA1");
                byte[] signedData = cryptoServiceProvider.SignData(data, strAlgorithmOID);
                return signedData;
                //var digestOid = new Oid("1.2.840.113549.1.7.2"); //pkcs7 signed

                //var content = new ContentInfo(digestOid, data);
                //try
                //{
                //    var signedCms = new SignedCms(SubjectIdentifierType.IssuerAndSerialNumber, content, true); //detached=true
                //    var singer = new CmsSigner(SubjectIdentifierType.IssuerAndSerialNumber, cert) { DigestAlgorithm = new Oid("1.3.14.3.2.26") };
                //    signedCms.ComputeSignature(singer, false);
                //    var signEnv = signedCms.Encode();
                //    return signEnv;
                //}
                //catch (Exception e)
                //{
                //    MessageBox.Show(e.Message);
                //}

            }
            return null;
        }
        /// <summary>
        /// Lấy chuỗi từ số byte cho  trước
        /// </summary>
        /// <param name="str">Chuỗi nhập vào</param>
        /// <param name="SoByte">Số lượng byte muốn cắt</param>
        /// <returns>Chuỗi mới bắt đầu từ offset 0 của chuỗi cũ đến offset thứ "SoByte" - 1</returns>
        public static string GetStringByBytes(string str, int SoByte)
        {
            string strResult = string.Empty;
            byte[] temp1 = UTF8Encoding.UTF8.GetBytes(str);
            byte[] temp2 = new byte[SoByte];
            if (temp1.Length > temp2.Length)
            {
                Buffer.BlockCopy(temp1, 0, temp2, 0, SoByte);
                strResult = UTF8Encoding.UTF8.GetString(temp2);
            }
            else
                strResult = UTF8Encoding.UTF8.GetString(temp1);

            return strResult;
        }
        /// <summary>
        /// Cắt chuỗi theo theo số bytes
        /// </summary>
        /// <param name="str">Chuỗi đầu vào</param>
        /// <param name="SoByte">Số lượng byte muốn cắt</param>
        /// <returns>Trả về là chuỗi mới đã được cắt "soByte" đầu tiên từ chuỗi cũ </returns>
        public static string SubStringByBytes(string str, int SoByte)
        {
            //SoByte = SoByte;
            string strResult = string.Empty;
            byte[] temp1 = UTF8Encoding.UTF8.GetBytes(str);
            byte[] temp2 = new byte[temp1.Length - SoByte];

            Buffer.BlockCopy(temp1, SoByte, temp2, 0, temp1.Length - SoByte);
            strResult = UTF8Encoding.UTF8.GetString(temp2);

            return strResult;
        }

        public static T UpdateObject<T>(object DoiTuongCanUpdate, object DoiTuongChuaDuLieuUpdate, bool UpdateIDandMasterID)
        {
            if (DoiTuongCanUpdate == null) return (T)DoiTuongChuaDuLieuUpdate;
            else if (DoiTuongChuaDuLieuUpdate == null) return (T)DoiTuongCanUpdate;
            else
            {
                PropertyInfo[] listProperties = typeof(T).GetProperties();
                foreach (PropertyInfo property in listProperties)
                {
                    bool isUpdate = true;
                    object value = property.GetValue(DoiTuongChuaDuLieuUpdate, null);
                    if (property.PropertyType.Namespace != "System.Collections.Generic")
                    {
                        if (value != null)
                        {
                            //if (property.GetType() == typeof(string))
                            //{
                            //    if (!string.IsNullOrEmpty(value.ToString().Trim())) isUpdate = true;
                            //}
                            //else
                            //    isUpdate = true;
                            if (property.Name.ToUpper() == "ID" || property.Name.ToUpper() == "MASTERID"
                                || property.Name.ToUpper() == "TKMDID" || property.Name.ToUpper() == "MASTER_ID"
                                || property.Name.ToUpper() == "TKMD_ID" || property.Name.ToUpper() == "ID_TKMD" || property.Name.ToUpper() == "ID_MASTER"
                                || property.Name.ToUpper() == "IDTKMD" || property.Name.ToUpper() == "IDMASTER" || property.Name.ToUpper() == "GIAYPHEP_ID")
                            {
                                /* if (Convert.ToInt64(value) == 0)*/
                                isUpdate = UpdateIDandMasterID;
                            }
                        }
                    }
                    if (isUpdate)
                    {
                        property.SetValue(DoiTuongCanUpdate, value, null);
                    }
                    if (property.PropertyType.Namespace == "System.Collections.Generic")
                    {
                        Type typelist = property.PropertyType.GetGenericArguments()[0];
                        object valueAfterUpdate = property.GetValue(DoiTuongCanUpdate, null);
                        IList listValue = valueAfterUpdate as IList;
                        if (listValue != null)
                            foreach (var item in listValue)
                            {
                                UpdateIdOffList(item, typelist);
                            }
                        //for (var i = 0; i < length; i++)
                        //{

                        //}
                    }
                }
                return (T)DoiTuongCanUpdate;
            }
        }
        public static object UpdateIdOffList(object value, Type typeGeneric)
        {
            PropertyInfo[] listProperties = typeGeneric.GetProperties();
            foreach (PropertyInfo property in listProperties)
            {
                //object value = property.GetValue(value, null);
                if (property.Name.ToUpper() == "ID" || property.Name.ToUpper() == "MASTERID"
                               || property.Name.ToUpper() == "TKMDID" || property.Name.ToUpper() == "MASTER_ID"
                               || property.Name.ToUpper() == "TKMD_ID" || property.Name.ToUpper() == "ID_TKMD" || property.Name.ToUpper() == "ID_MASTER"
                               || property.Name.ToUpper() == "IDTKMD" || property.Name.ToUpper() == "IDMASTER")
                {
                    property.SetValue(value, 0, null);
                }
                if (property.PropertyType.Namespace == "System.Collections.Generic")
                {
                    Type typelist = property.PropertyType.GetGenericArguments()[0];
                    object valueAfterUpdate = property.GetValue(value, null);
                    IList listValue = valueAfterUpdate as IList;
                    if (listValue != null)
                        foreach (var item in listValue)
                        {
                            UpdateIdOffList(item, typelist);
                        }

                }
            }
            return value;
        }
        public static List<T> UpdateList<T>(List<T> DoiTuongCanUpdate, List<T> DoiTuongChuaDuLieuUpdate)
        {
            return UpdateList<T>(DoiTuongCanUpdate, DoiTuongChuaDuLieuUpdate, false);
        }
        public static List<T> UpdateList<T>(List<T> DoiTuongCanUpdate, List<T> DoiTuongChuaDuLieuUpdate, bool UpdateID)
        {
            if (DoiTuongChuaDuLieuUpdate == null || DoiTuongChuaDuLieuUpdate.Count == 0)
                DoiTuongCanUpdate = DoiTuongChuaDuLieuUpdate;
            else if (DoiTuongCanUpdate.Count > DoiTuongChuaDuLieuUpdate.Count)
                DoiTuongCanUpdate.RemoveRange(DoiTuongChuaDuLieuUpdate.Count - 1, DoiTuongCanUpdate.Count - DoiTuongChuaDuLieuUpdate.Count);
            for (int i = 0; i < DoiTuongChuaDuLieuUpdate.Count; i++)
            {
                if (DoiTuongCanUpdate == null) DoiTuongCanUpdate = new List<T>();
                if (i >= DoiTuongCanUpdate.Count)
                    DoiTuongCanUpdate.Add(DoiTuongChuaDuLieuUpdate[i]);
                else
                {
                    DoiTuongCanUpdate[i] = UpdateObject<T>(DoiTuongCanUpdate[i], DoiTuongChuaDuLieuUpdate[i], UpdateID);
                }
            }
            return DoiTuongCanUpdate;
        }
        public static IList<T> UpdateIList<T>(IList<T> DoiTuongCanUpdate, IList<T> DoiTuongChuaDuLieuUpdate)
        {
            return UpdateIList<T>(DoiTuongCanUpdate, DoiTuongChuaDuLieuUpdate, false);
        }
        public static IList<T> UpdateIList<T>(IList<T> DoiTuongCanUpdate, IList<T> DoiTuongChuaDuLieuUpdate, bool UpdateID)
        {
            for (int i = 0; i < DoiTuongChuaDuLieuUpdate.Count; i++)
            {
                //if (DoiTuongCanUpdate == null) DoiTuongCanUpdate = new List<T>();
                if (i >= DoiTuongCanUpdate.Count)
                    DoiTuongCanUpdate.Add(DoiTuongChuaDuLieuUpdate[i]);
                else
                {
                    DoiTuongCanUpdate[i] = UpdateObject<T>(DoiTuongCanUpdate[i], DoiTuongChuaDuLieuUpdate[i], UpdateID);
                }
            }
            return DoiTuongCanUpdate;
        }

        #region GUIDE and ERROR GUIDE

        public static string GuidePath = AppDomain.CurrentDomain.BaseDirectory + "Help\\guide";
        public static string GuideErrorPath = AppDomain.CurrentDomain.BaseDirectory + "Help\\gym_err";

        /// <summary>
        /// Doc file thong tin XML Guide theo Code cua Nghiep vu khai bao
        /// </summary>
        /// <param name="guideCode"></param>
        public static XmlDocument ReadGuideFile(string guideCode)
        {
            XmlDocument docGuide = new XmlDocument();
            try
            {
                string guideFile = GuidePath + string.Format("\\{0}_guide.xml", guideCode);

                if (System.IO.File.Exists(guideFile))
                    docGuide.Load(guideFile);
                else
                    docGuide = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                docGuide = null;
            }

            return docGuide;
        }

        /// <summary>
        /// Tim lay thong tin guide theo ID control trong danh sach
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetGuide(XmlDocument docGuide, string id)
        {
            return GetGuideString(docGuide, "id", id);
        }

        public static string GetGuideName(XmlDocument docGuide, string val)
        {
            XmlNode node = GetGuideNode(docGuide, "id", val);

            return node != null ? node.Attributes["name"].InnerXml : "";
        }

        private static string GetGuideString(XmlDocument docGuide, string key, string id)
        {
            if (docGuide != null)
            {
                int length = id.Length;
                StringBuilder builder = new StringBuilder(id);
                while (length > 0)
                {
                    XmlNode node = docGuide.DocumentElement.SelectSingleNode("item[@" + key + "='" + builder.ToString() + "']");
                    if (node != null)
                    {
                        return node.InnerText;
                    }
                    builder[--length] = '_';
                }
            }
            return "";
        }

        private static XmlNode GetGuideNode(XmlDocument docGuide, string key, string id)
        {
            if (docGuide != null)
            {
                int length = id.Length;
                StringBuilder builder = new StringBuilder(id);
                while (length > 0)
                {
                    XmlNode node = docGuide.DocumentElement.SelectSingleNode("item[@" + key + "='" + builder.ToString() + "']");
                    if (node != null)
                    {
                        return node;
                    }
                    builder[--length] = '_';
                }
            }
            return null;
        }

        /// <summary>
        /// Doc file thong tin XML Guide theo Code cua Nghiep vu khai bao
        /// </summary>
        /// <param name="guideCode"></param>
        public static XmlDocument ReadGuideErrorFile(string guideCode)
        {
            XmlDocument docGuideError = new XmlDocument();
            try
            {
                string guideFile = Company.KDT.SHARE.VNACCS.HelperVNACCS.GuideErrorPath + string.Format("\\{0}_err.xml", guideCode);

                if (System.IO.File.Exists(guideFile))
                    docGuideError.Load(guideFile);
                else
                    docGuideError = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                docGuideError = null;
            }

            return docGuideError;
        }

        /// <summary>
        /// Tim lay thong tin guide theo ID control trong danh sach
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetGuideError(XmlDocument docGuideError, string id)
        {
            XmlNode node = GetGuideErrorNode(docGuideError, "code", id);

            return node != null ? node.SelectSingleNode("description").InnerXml : "";
        }

        public static string GetGuideErrorDescription(XmlDocument docGuideError, string id)
        {
            XmlNode node = GetGuideErrorNode(docGuideError, "code", id);

            return node != null ? node.SelectSingleNode("description").InnerXml : "";
        }

        public static string GetGuideErrorDisposition(XmlDocument docGuideError, string id)
        {
            XmlNode node = GetGuideErrorNode(docGuideError, "code", id);

            return node != null ? node.SelectSingleNode("disposition").InnerXml : "";
        }

        private static string GetGuideErrorString(XmlDocument docGuideError, string key, string id)
        {
            if (docGuideError != null)
            {
                int length = id.Length;
                StringBuilder builder = new StringBuilder(id);
                while (length > 0)
                {
                    XmlNode node = docGuideError.DocumentElement.SelectSingleNode("response[@" + key + "='" + builder.ToString().Trim() + "']");
                    if (node != null)
                    {
                        return node.InnerText;
                    }
                    builder[--length] = '_';
                }
            }
            return "";
        }

        private static XmlNode GetGuideErrorNode(XmlDocument docGuideError, string key, string id)
        {
            if (docGuideError != null)
            {
                int length = id.Length;
                StringBuilder builder = new StringBuilder(id);
                while (length > 0)
                {
                    XmlNode node = docGuideError.DocumentElement.SelectSingleNode("response[@" + key + "='" + builder.ToString().Trim() + "']");
                    if (node != null)
                    {
                        return node;
                    }
                    builder[--length] = '_';
                }
            }
            return null;
        }

        #endregion
        private static string GetNameSigner(string signName)
        {
            string[] infors = signName.Split(',');
            string name = string.Empty;
            string mst = string.Empty;
            foreach (string info in infors)
            {
                int index = info.IndexOf("CN=");
                if (index > -1)
                {
                    name = info.Substring(index + 3);
                }
                index = info.IndexOf("OID");

                if (index > -1)
                {
                    index = info.IndexOf("=");
                    if (index > 0)
                        mst = info.Substring(index + 1);
                }
            }
            if (!string.IsNullOrEmpty(mst)) return string.Format("{0} - [{1}]", name, mst);
            else return string.Format("{0}", name);
        }

        public static string NewInputMSGID()
        {
            //return "123456789";
            return DateTime.Now.ToString("ddMMyyhhmm");
        }
    }
}
