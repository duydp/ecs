﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAL0870_IVA : BasicVNACC
    {
      //  public static int TongSoByte { get { return 3070; } }
        //public VAL0870_IVA()
        //{
        //}
        //public VAL0870_IVA(string strResult)
        //{
        //    try
        //    {
        //        this.GetObject<VAL0870_IVA>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAL0870_IVA, false,VAL0870_IVA.TongSoByte);
        //        strResult = HelperVNACCS.SubStringByBytes(strResult, VAL0870_IVA.TongSoByte);
        //        while (true)
        //        {
        //            IVA_HANGHOA ivaHang = new IVA_HANGHOA();
        //            ivaHang.GetObject<IVA_HANGHOA>(strResult, true, GlobalVNACC.PathConfig, "IVA_HANGHOA", false, IVA_HANGHOA.TotalByte);
        //            if (this.HangHoaTrongHoaDon == null) this.HangHoaTrongHoaDon = new List<IVA_HANGHOA>();
        //            this.HangHoaTrongHoaDon.Add(ivaHang);
        //            if (UTF8Encoding.UTF8.GetBytes(strResult).Length > IVA_HANGHOA.TotalByte)
        //            {
        //                if (UTF8Encoding.UTF8.GetBytes(strResult).Length - IVA_HANGHOA.TotalByte > 4)
        //                {
        //                    strResult = HelperVNACCS.SubStringByBytes(strResult, IVA_HANGHOA.TotalByte);
        //                    continue;
        //                }
        //            }
        //            break;
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        throw ex;
        //    }
          

            
        //}
        public List<VAL0870_HANGHOA> HangHoaTrongHoaDon { get; set; }

        public StringBuilder BuilEdiMessagesIVA(StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<VAL0870_IVA>(StrBuild, true, GlobalVNACC.PathConfig, "VAL0870_IVA");
            foreach (VAL0870_HANGHOA item in HangHoaTrongHoaDon)
            {
                item.BuildEdiMessages<VAL0870_HANGHOA>(StrBuild, true, GlobalVNACC.PathConfig, "VAL0870_HANGHOA");

            }
            return str;
        }
        public void LoadVAL0870_IVA(string strResult)
        {
            try
            {
                this.GetObject<VAL0870_IVA>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAL0870, false, VAL0870_IVA.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAL0870_IVA.TongSoByte);
                while (true)
                {
                    VAL0870_HANGHOA ivaHang = new VAL0870_HANGHOA();
                    ivaHang.GetObject<VAL0870_HANGHOA>(strResult, true, GlobalVNACC.PathConfig, "VAL0870_HANGHOA", false, VAL0870_HANGHOA.TotalByte);
                    if (this.HangHoaTrongHoaDon == null) this.HangHoaTrongHoaDon = new List<VAL0870_HANGHOA>();
                    this.HangHoaTrongHoaDon.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAL0870_HANGHOA.TotalByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAL0870_HANGHOA.TotalByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAL0870_HANGHOA.TotalByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
