-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteBy_TKMDBoSung_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteBy_TKMDBoSung_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Insert]
	@TKMDBoSung_ID bigint,
	@SoDong varchar(2),
	@SoThuTuDongHangTrenToKhaiGoc varchar(2),
	@MoTaHangHoaTruocKhiKhaiBoSung nvarchar(600),
	@MoTaHangHoaSauKhiKhaiBoSung nvarchar(600),
	@MaSoHangHoaTruocKhiKhaiBoSung varchar(12),
	@MaSoHangHoaSauKhiKhaiBoSung varchar(12),
	@MaNuocXuatXuTruocKhiKhaiBoSung varchar(2),
	@MaNuocXuatXuSauKhiKhaiBoSung varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSung numeric(21, 6),
	@TriGiaTinhThueSauKhiKhaiBoSung numeric(21, 6),
	@SoLuongTinhThueTruocKhiKhaiBoSung numeric(21, 6),
	@SoLuongTinhThueSauKhiKhaiBoSung numeric(21, 6),
	@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung varchar(4),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung varchar(4),
	@ThueSuatTruocKhiKhaiBoSung varchar(30),
	@ThueSuatSauKhiKhaiBoSung varchar(30),
	@SoTienThueTruocKhiKhaiBoSung varchar(16),
	@SoTienThueSauKhiKhaiBoSung varchar(16),
	@HienThiMienThueTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueXuatNhapKhau varchar(1),
	@SoTienTangGiamThue numeric(16, 6),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
(
	[TKMDBoSung_ID],
	[SoDong],
	[SoThuTuDongHangTrenToKhaiGoc],
	[MoTaHangHoaTruocKhiKhaiBoSung],
	[MoTaHangHoaSauKhiKhaiBoSung],
	[MaSoHangHoaTruocKhiKhaiBoSung],
	[MaSoHangHoaSauKhiKhaiBoSung],
	[MaNuocXuatXuTruocKhiKhaiBoSung],
	[MaNuocXuatXuSauKhiKhaiBoSung],
	[TriGiaTinhThueTruocKhiKhaiBoSung],
	[TriGiaTinhThueSauKhiKhaiBoSung],
	[SoLuongTinhThueTruocKhiKhaiBoSung],
	[SoLuongTinhThueSauKhiKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
	[ThueSuatTruocKhiKhaiBoSung],
	[ThueSuatSauKhiKhaiBoSung],
	[SoTienThueTruocKhiKhaiBoSung],
	[SoTienThueSauKhiKhaiBoSung],
	[HienThiMienThueTruocKhiKhaiBoSung],
	[HienThiMienThueSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueXuatNhapKhau],
	[SoTienTangGiamThue]
)
VALUES 
(
	@TKMDBoSung_ID,
	@SoDong,
	@SoThuTuDongHangTrenToKhaiGoc,
	@MoTaHangHoaTruocKhiKhaiBoSung,
	@MoTaHangHoaSauKhiKhaiBoSung,
	@MaSoHangHoaTruocKhiKhaiBoSung,
	@MaSoHangHoaSauKhiKhaiBoSung,
	@MaNuocXuatXuTruocKhiKhaiBoSung,
	@MaNuocXuatXuSauKhiKhaiBoSung,
	@TriGiaTinhThueTruocKhiKhaiBoSung,
	@TriGiaTinhThueSauKhiKhaiBoSung,
	@SoLuongTinhThueTruocKhiKhaiBoSung,
	@SoLuongTinhThueSauKhiKhaiBoSung,
	@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung,
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung,
	@ThueSuatTruocKhiKhaiBoSung,
	@ThueSuatSauKhiKhaiBoSung,
	@SoTienThueTruocKhiKhaiBoSung,
	@SoTienThueSauKhiKhaiBoSung,
	@HienThiMienThueTruocKhiKhaiBoSung,
	@HienThiMienThueSauKhiKhaiBoSung,
	@HienThiSoTienTangGiamThueXuatNhapKhau,
	@SoTienTangGiamThue
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Update]
	@ID bigint,
	@TKMDBoSung_ID bigint,
	@SoDong varchar(2),
	@SoThuTuDongHangTrenToKhaiGoc varchar(2),
	@MoTaHangHoaTruocKhiKhaiBoSung nvarchar(600),
	@MoTaHangHoaSauKhiKhaiBoSung nvarchar(600),
	@MaSoHangHoaTruocKhiKhaiBoSung varchar(12),
	@MaSoHangHoaSauKhiKhaiBoSung varchar(12),
	@MaNuocXuatXuTruocKhiKhaiBoSung varchar(2),
	@MaNuocXuatXuSauKhiKhaiBoSung varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSung numeric(21, 6),
	@TriGiaTinhThueSauKhiKhaiBoSung numeric(21, 6),
	@SoLuongTinhThueTruocKhiKhaiBoSung numeric(21, 6),
	@SoLuongTinhThueSauKhiKhaiBoSung numeric(21, 6),
	@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung varchar(4),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung varchar(4),
	@ThueSuatTruocKhiKhaiBoSung varchar(30),
	@ThueSuatSauKhiKhaiBoSung varchar(30),
	@SoTienThueTruocKhiKhaiBoSung varchar(16),
	@SoTienThueSauKhiKhaiBoSung varchar(16),
	@HienThiMienThueTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueXuatNhapKhau varchar(1),
	@SoTienTangGiamThue numeric(16, 6)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
SET
	[TKMDBoSung_ID] = @TKMDBoSung_ID,
	[SoDong] = @SoDong,
	[SoThuTuDongHangTrenToKhaiGoc] = @SoThuTuDongHangTrenToKhaiGoc,
	[MoTaHangHoaTruocKhiKhaiBoSung] = @MoTaHangHoaTruocKhiKhaiBoSung,
	[MoTaHangHoaSauKhiKhaiBoSung] = @MoTaHangHoaSauKhiKhaiBoSung,
	[MaSoHangHoaTruocKhiKhaiBoSung] = @MaSoHangHoaTruocKhiKhaiBoSung,
	[MaSoHangHoaSauKhiKhaiBoSung] = @MaSoHangHoaSauKhiKhaiBoSung,
	[MaNuocXuatXuTruocKhiKhaiBoSung] = @MaNuocXuatXuTruocKhiKhaiBoSung,
	[MaNuocXuatXuSauKhiKhaiBoSung] = @MaNuocXuatXuSauKhiKhaiBoSung,
	[TriGiaTinhThueTruocKhiKhaiBoSung] = @TriGiaTinhThueTruocKhiKhaiBoSung,
	[TriGiaTinhThueSauKhiKhaiBoSung] = @TriGiaTinhThueSauKhiKhaiBoSung,
	[SoLuongTinhThueTruocKhiKhaiBoSung] = @SoLuongTinhThueTruocKhiKhaiBoSung,
	[SoLuongTinhThueSauKhiKhaiBoSung] = @SoLuongTinhThueSauKhiKhaiBoSung,
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung] = @MaDonViTinhSoLuongTinhThueTruocKhaiBoSung,
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung] = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung,
	[ThueSuatTruocKhiKhaiBoSung] = @ThueSuatTruocKhiKhaiBoSung,
	[ThueSuatSauKhiKhaiBoSung] = @ThueSuatSauKhiKhaiBoSung,
	[SoTienThueTruocKhiKhaiBoSung] = @SoTienThueTruocKhiKhaiBoSung,
	[SoTienThueSauKhiKhaiBoSung] = @SoTienThueSauKhiKhaiBoSung,
	[HienThiMienThueTruocKhiKhaiBoSung] = @HienThiMienThueTruocKhiKhaiBoSung,
	[HienThiMienThueSauKhiKhaiBoSung] = @HienThiMienThueSauKhiKhaiBoSung,
	[HienThiSoTienTangGiamThueXuatNhapKhau] = @HienThiSoTienTangGiamThueXuatNhapKhau,
	[SoTienTangGiamThue] = @SoTienTangGiamThue
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_InsertUpdate]
	@ID bigint,
	@TKMDBoSung_ID bigint,
	@SoDong varchar(2),
	@SoThuTuDongHangTrenToKhaiGoc varchar(2),
	@MoTaHangHoaTruocKhiKhaiBoSung nvarchar(600),
	@MoTaHangHoaSauKhiKhaiBoSung nvarchar(600),
	@MaSoHangHoaTruocKhiKhaiBoSung varchar(12),
	@MaSoHangHoaSauKhiKhaiBoSung varchar(12),
	@MaNuocXuatXuTruocKhiKhaiBoSung varchar(2),
	@MaNuocXuatXuSauKhiKhaiBoSung varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSung numeric(21, 6),
	@TriGiaTinhThueSauKhiKhaiBoSung numeric(21, 6),
	@SoLuongTinhThueTruocKhiKhaiBoSung numeric(21, 6),
	@SoLuongTinhThueSauKhiKhaiBoSung numeric(21, 6),
	@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung varchar(4),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung varchar(4),
	@ThueSuatTruocKhiKhaiBoSung varchar(30),
	@ThueSuatSauKhiKhaiBoSung varchar(30),
	@SoTienThueTruocKhiKhaiBoSung varchar(16),
	@SoTienThueSauKhiKhaiBoSung varchar(16),
	@HienThiMienThueTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueXuatNhapKhau varchar(1),
	@SoTienTangGiamThue numeric(16, 6)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] 
		SET
			[TKMDBoSung_ID] = @TKMDBoSung_ID,
			[SoDong] = @SoDong,
			[SoThuTuDongHangTrenToKhaiGoc] = @SoThuTuDongHangTrenToKhaiGoc,
			[MoTaHangHoaTruocKhiKhaiBoSung] = @MoTaHangHoaTruocKhiKhaiBoSung,
			[MoTaHangHoaSauKhiKhaiBoSung] = @MoTaHangHoaSauKhiKhaiBoSung,
			[MaSoHangHoaTruocKhiKhaiBoSung] = @MaSoHangHoaTruocKhiKhaiBoSung,
			[MaSoHangHoaSauKhiKhaiBoSung] = @MaSoHangHoaSauKhiKhaiBoSung,
			[MaNuocXuatXuTruocKhiKhaiBoSung] = @MaNuocXuatXuTruocKhiKhaiBoSung,
			[MaNuocXuatXuSauKhiKhaiBoSung] = @MaNuocXuatXuSauKhiKhaiBoSung,
			[TriGiaTinhThueTruocKhiKhaiBoSung] = @TriGiaTinhThueTruocKhiKhaiBoSung,
			[TriGiaTinhThueSauKhiKhaiBoSung] = @TriGiaTinhThueSauKhiKhaiBoSung,
			[SoLuongTinhThueTruocKhiKhaiBoSung] = @SoLuongTinhThueTruocKhiKhaiBoSung,
			[SoLuongTinhThueSauKhiKhaiBoSung] = @SoLuongTinhThueSauKhiKhaiBoSung,
			[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung] = @MaDonViTinhSoLuongTinhThueTruocKhaiBoSung,
			[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung] = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung,
			[ThueSuatTruocKhiKhaiBoSung] = @ThueSuatTruocKhiKhaiBoSung,
			[ThueSuatSauKhiKhaiBoSung] = @ThueSuatSauKhiKhaiBoSung,
			[SoTienThueTruocKhiKhaiBoSung] = @SoTienThueTruocKhiKhaiBoSung,
			[SoTienThueSauKhiKhaiBoSung] = @SoTienThueSauKhiKhaiBoSung,
			[HienThiMienThueTruocKhiKhaiBoSung] = @HienThiMienThueTruocKhiKhaiBoSung,
			[HienThiMienThueSauKhiKhaiBoSung] = @HienThiMienThueSauKhiKhaiBoSung,
			[HienThiSoTienTangGiamThueXuatNhapKhau] = @HienThiSoTienTangGiamThueXuatNhapKhau,
			[SoTienTangGiamThue] = @SoTienTangGiamThue
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
		(
			[TKMDBoSung_ID],
			[SoDong],
			[SoThuTuDongHangTrenToKhaiGoc],
			[MoTaHangHoaTruocKhiKhaiBoSung],
			[MoTaHangHoaSauKhiKhaiBoSung],
			[MaSoHangHoaTruocKhiKhaiBoSung],
			[MaSoHangHoaSauKhiKhaiBoSung],
			[MaNuocXuatXuTruocKhiKhaiBoSung],
			[MaNuocXuatXuSauKhiKhaiBoSung],
			[TriGiaTinhThueTruocKhiKhaiBoSung],
			[TriGiaTinhThueSauKhiKhaiBoSung],
			[SoLuongTinhThueTruocKhiKhaiBoSung],
			[SoLuongTinhThueSauKhiKhaiBoSung],
			[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
			[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
			[ThueSuatTruocKhiKhaiBoSung],
			[ThueSuatSauKhiKhaiBoSung],
			[SoTienThueTruocKhiKhaiBoSung],
			[SoTienThueSauKhiKhaiBoSung],
			[HienThiMienThueTruocKhiKhaiBoSung],
			[HienThiMienThueSauKhiKhaiBoSung],
			[HienThiSoTienTangGiamThueXuatNhapKhau],
			[SoTienTangGiamThue]
		)
		VALUES 
		(
			@TKMDBoSung_ID,
			@SoDong,
			@SoThuTuDongHangTrenToKhaiGoc,
			@MoTaHangHoaTruocKhiKhaiBoSung,
			@MoTaHangHoaSauKhiKhaiBoSung,
			@MaSoHangHoaTruocKhiKhaiBoSung,
			@MaSoHangHoaSauKhiKhaiBoSung,
			@MaNuocXuatXuTruocKhiKhaiBoSung,
			@MaNuocXuatXuSauKhiKhaiBoSung,
			@TriGiaTinhThueTruocKhiKhaiBoSung,
			@TriGiaTinhThueSauKhiKhaiBoSung,
			@SoLuongTinhThueTruocKhiKhaiBoSung,
			@SoLuongTinhThueSauKhiKhaiBoSung,
			@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung,
			@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung,
			@ThueSuatTruocKhiKhaiBoSung,
			@ThueSuatSauKhiKhaiBoSung,
			@SoTienThueTruocKhiKhaiBoSung,
			@SoTienThueSauKhiKhaiBoSung,
			@HienThiMienThueTruocKhiKhaiBoSung,
			@HienThiMienThueSauKhiKhaiBoSung,
			@HienThiSoTienTangGiamThueXuatNhapKhau,
			@SoTienTangGiamThue
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteBy_TKMDBoSung_ID]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteBy_TKMDBoSung_ID]
	@TKMDBoSung_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
WHERE
	[TKMDBoSung_ID] = @TKMDBoSung_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[SoThuTuDongHangTrenToKhaiGoc],
	[MoTaHangHoaTruocKhiKhaiBoSung],
	[MoTaHangHoaSauKhiKhaiBoSung],
	[MaSoHangHoaTruocKhiKhaiBoSung],
	[MaSoHangHoaSauKhiKhaiBoSung],
	[MaNuocXuatXuTruocKhiKhaiBoSung],
	[MaNuocXuatXuSauKhiKhaiBoSung],
	[TriGiaTinhThueTruocKhiKhaiBoSung],
	[TriGiaTinhThueSauKhiKhaiBoSung],
	[SoLuongTinhThueTruocKhiKhaiBoSung],
	[SoLuongTinhThueSauKhiKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
	[ThueSuatTruocKhiKhaiBoSung],
	[ThueSuatSauKhiKhaiBoSung],
	[SoTienThueTruocKhiKhaiBoSung],
	[SoTienThueSauKhiKhaiBoSung],
	[HienThiMienThueTruocKhiKhaiBoSung],
	[HienThiMienThueSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueXuatNhapKhau],
	[SoTienTangGiamThue]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID]
	@TKMDBoSung_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[SoThuTuDongHangTrenToKhaiGoc],
	[MoTaHangHoaTruocKhiKhaiBoSung],
	[MoTaHangHoaSauKhiKhaiBoSung],
	[MaSoHangHoaTruocKhiKhaiBoSung],
	[MaSoHangHoaSauKhiKhaiBoSung],
	[MaNuocXuatXuTruocKhiKhaiBoSung],
	[MaNuocXuatXuSauKhiKhaiBoSung],
	[TriGiaTinhThueTruocKhiKhaiBoSung],
	[TriGiaTinhThueSauKhiKhaiBoSung],
	[SoLuongTinhThueTruocKhiKhaiBoSung],
	[SoLuongTinhThueSauKhiKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
	[ThueSuatTruocKhiKhaiBoSung],
	[ThueSuatSauKhiKhaiBoSung],
	[SoTienThueTruocKhiKhaiBoSung],
	[SoTienThueSauKhiKhaiBoSung],
	[HienThiMienThueTruocKhiKhaiBoSung],
	[HienThiMienThueSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueXuatNhapKhau],
	[SoTienTangGiamThue]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
WHERE
	[TKMDBoSung_ID] = @TKMDBoSung_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[SoThuTuDongHangTrenToKhaiGoc],
	[MoTaHangHoaTruocKhiKhaiBoSung],
	[MoTaHangHoaSauKhiKhaiBoSung],
	[MaSoHangHoaTruocKhiKhaiBoSung],
	[MaSoHangHoaSauKhiKhaiBoSung],
	[MaNuocXuatXuTruocKhiKhaiBoSung],
	[MaNuocXuatXuSauKhiKhaiBoSung],
	[TriGiaTinhThueTruocKhiKhaiBoSung],
	[TriGiaTinhThueSauKhiKhaiBoSung],
	[SoLuongTinhThueTruocKhiKhaiBoSung],
	[SoLuongTinhThueSauKhiKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
	[ThueSuatTruocKhiKhaiBoSung],
	[ThueSuatSauKhiKhaiBoSung],
	[SoTienThueTruocKhiKhaiBoSung],
	[SoTienThueSauKhiKhaiBoSung],
	[HienThiMienThueTruocKhiKhaiBoSung],
	[HienThiMienThueSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueXuatNhapKhau],
	[SoTienTangGiamThue]
FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[SoThuTuDongHangTrenToKhaiGoc],
	[MoTaHangHoaTruocKhiKhaiBoSung],
	[MoTaHangHoaSauKhiKhaiBoSung],
	[MaSoHangHoaTruocKhiKhaiBoSung],
	[MaSoHangHoaSauKhiKhaiBoSung],
	[MaNuocXuatXuTruocKhiKhaiBoSung],
	[MaNuocXuatXuSauKhiKhaiBoSung],
	[TriGiaTinhThueTruocKhiKhaiBoSung],
	[TriGiaTinhThueSauKhiKhaiBoSung],
	[SoLuongTinhThueTruocKhiKhaiBoSung],
	[SoLuongTinhThueSauKhiKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
	[ThueSuatTruocKhiKhaiBoSung],
	[ThueSuatSauKhiKhaiBoSung],
	[SoTienThueTruocKhiKhaiBoSung],
	[SoTienThueSauKhiKhaiBoSung],
	[HienThiMienThueTruocKhiKhaiBoSung],
	[HienThiMienThueSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueXuatNhapKhau],
	[SoTienTangGiamThue]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]	

GO

