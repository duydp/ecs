using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAD1AC0 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute ICN { get; set; }
public PropertiesAttribute JKN { get; set; }
public PropertiesAttribute FIC { get; set; }
public PropertiesAttribute BNO { get; set; }
public PropertiesAttribute DNO { get; set; }
public PropertiesAttribute TDN { get; set; }
public PropertiesAttribute A06 { get; set; }
public PropertiesAttribute ICB { get; set; }
public PropertiesAttribute CCC { get; set; }
public PropertiesAttribute MTC { get; set; }
public PropertiesAttribute SKB { get; set; }
public PropertiesAttribute A00 { get; set; }
public PropertiesAttribute A07 { get; set; }
public PropertiesAttribute CHB { get; set; }
public PropertiesAttribute A09 { get; set; }
public PropertiesAttribute AD1 { get; set; }
public PropertiesAttribute AD2 { get; set; }
public PropertiesAttribute AD3 { get; set; }
public PropertiesAttribute RED { get; set; }
public PropertiesAttribute AAA { get; set; }
public PropertiesAttribute IMC { get; set; }
public PropertiesAttribute IMN { get; set; }
public PropertiesAttribute IMY { get; set; }
public PropertiesAttribute IMA { get; set; }
public PropertiesAttribute IMT { get; set; }
public PropertiesAttribute NMC { get; set; }
public PropertiesAttribute NMN { get; set; }
public PropertiesAttribute EPC { get; set; }
public PropertiesAttribute EPN { get; set; }
public PropertiesAttribute EPY { get; set; }
public PropertiesAttribute EPA { get; set; }
public PropertiesAttribute EP2 { get; set; }
public PropertiesAttribute EP3 { get; set; }
public PropertiesAttribute EP4 { get; set; }
public PropertiesAttribute EPO { get; set; }
public PropertiesAttribute ENM { get; set; }
public PropertiesAttribute A37 { get; set; }
public PropertiesAttribute A38 { get; set; }
public PropertiesAttribute A39 { get; set; }
public GroupAttribute BL_ { get; set; }
public PropertiesAttribute NO { get; set; }
public PropertiesAttribute NOT { get; set; }
public PropertiesAttribute GW { get; set; }
public PropertiesAttribute GWT { get; set; }
public PropertiesAttribute COC { get; set; }
public PropertiesAttribute ST { get; set; }
public PropertiesAttribute A51 { get; set; }
public PropertiesAttribute DST { get; set; }
public PropertiesAttribute DSN { get; set; }
public PropertiesAttribute PSC { get; set; }
public PropertiesAttribute PSN { get; set; }
public PropertiesAttribute VSC { get; set; }
public PropertiesAttribute VSN { get; set; }
public PropertiesAttribute ARR { get; set; }
public PropertiesAttribute MRK { get; set; }
public PropertiesAttribute ISD { get; set; }
public GroupAttribute OL_ { get; set; }
public PropertiesAttribute IV1 { get; set; }
public PropertiesAttribute IV3 { get; set; }
public PropertiesAttribute IV2 { get; set; }
public PropertiesAttribute IVD { get; set; }
public PropertiesAttribute IVP { get; set; }
public PropertiesAttribute IP1 { get; set; }
public PropertiesAttribute IP2 { get; set; }
public PropertiesAttribute IP3 { get; set; }
public PropertiesAttribute IP4 { get; set; }
public PropertiesAttribute A86 { get; set; }
public PropertiesAttribute TP { get; set; }
public PropertiesAttribute A97 { get; set; }
public PropertiesAttribute N4 { get; set; }
public GroupAttribute SS_ { get; set; }
public PropertiesAttribute VD1 { get; set; }
public PropertiesAttribute VD2 { get; set; }
public PropertiesAttribute A93 { get; set; }
public PropertiesAttribute A94 { get; set; }
public PropertiesAttribute A95 { get; set; }
public PropertiesAttribute VCC { get; set; }
public PropertiesAttribute VPC { get; set; }
public PropertiesAttribute FR1 { get; set; }
public PropertiesAttribute FR2 { get; set; }
public PropertiesAttribute FR3 { get; set; }
public PropertiesAttribute IN1 { get; set; }
public PropertiesAttribute IN2 { get; set; }
public PropertiesAttribute IN3 { get; set; }
public PropertiesAttribute IN4 { get; set; }
public GroupAttribute VR_ { get; set; }
public PropertiesAttribute VLD { get; set; }
public GroupAttribute KF1 { get; set; }
public PropertiesAttribute B02 { get; set; }
public PropertiesAttribute B03 { get; set; }
public GroupAttribute KJ1 { get; set; }
public PropertiesAttribute ENC { get; set; }
public PropertiesAttribute TPM { get; set; }
public PropertiesAttribute BP { get; set; }
public PropertiesAttribute B08 { get; set; }
public PropertiesAttribute B12 { get; set; }
public PropertiesAttribute B13 { get; set; }
public GroupAttribute EA_ { get; set; }
public PropertiesAttribute NT2 { get; set; }
public PropertiesAttribute REF { get; set; }
public PropertiesAttribute B16 { get; set; }
public PropertiesAttribute CCM { get; set; }
public GroupAttribute D__ { get; set; }
public PropertiesAttribute ADY { get; set; }
public PropertiesAttribute ADZ { get; set; }
public PropertiesAttribute B23 { get; set; }
public GroupAttribute KN1 { get; set; }
public PropertiesAttribute DPD { get; set; }
public GroupAttribute ST_ { get; set; }
public PropertiesAttribute ARP { get; set; }
public PropertiesAttribute ADT { get; set; }

public VAD1AC0()
        {
ICN = new PropertiesAttribute(12, typeof(int));
JKN = new PropertiesAttribute(1, typeof(string));
FIC = new PropertiesAttribute(12, typeof(string));
BNO = new PropertiesAttribute(2, typeof(int));
DNO = new PropertiesAttribute(2, typeof(int));
TDN = new PropertiesAttribute(12, typeof(int));
A06 = new PropertiesAttribute(3, typeof(string));
ICB = new PropertiesAttribute(3, typeof(string));
CCC = new PropertiesAttribute(1, typeof(string));
MTC = new PropertiesAttribute(1, typeof(string));
SKB = new PropertiesAttribute(1, typeof(string));
A00 = new PropertiesAttribute(4, typeof(string));
A07 = new PropertiesAttribute(10, typeof(string));
CHB = new PropertiesAttribute(2, typeof(string));
A09 = new PropertiesAttribute(8, typeof(DateTime));
AD1 = new PropertiesAttribute(6, typeof(string));
AD2 = new PropertiesAttribute(8, typeof(DateTime));
AD3 = new PropertiesAttribute(6, typeof(string));
RED = new PropertiesAttribute(8, typeof(DateTime));
AAA = new PropertiesAttribute(1, typeof(string));
IMC = new PropertiesAttribute(13, typeof(string));
IMN = new PropertiesAttribute(300, typeof(string));
IMY = new PropertiesAttribute(7, typeof(string));
IMA = new PropertiesAttribute(300, typeof(string));
IMT = new PropertiesAttribute(20, typeof(string));
NMC = new PropertiesAttribute(13, typeof(string));
NMN = new PropertiesAttribute(300, typeof(string));
EPC = new PropertiesAttribute(13, typeof(string));
EPN = new PropertiesAttribute(70, typeof(string));
EPY = new PropertiesAttribute(9, typeof(string));
EPA = new PropertiesAttribute(35, typeof(string));
EP2 = new PropertiesAttribute(35, typeof(string));
EP3 = new PropertiesAttribute(35, typeof(string));
EP4 = new PropertiesAttribute(35, typeof(string));
EPO = new PropertiesAttribute(2, typeof(string));
ENM = new PropertiesAttribute(70, typeof(string));
A37 = new PropertiesAttribute(5, typeof(string));
A38 = new PropertiesAttribute(50, typeof(string));
A39 = new PropertiesAttribute(5, typeof(string));
NO = new PropertiesAttribute(8, typeof(int));
NOT = new PropertiesAttribute(3, typeof(string));
GW = new PropertiesAttribute(10, typeof(int));
GWT = new PropertiesAttribute(3, typeof(string));
COC = new PropertiesAttribute(3, typeof(int));
ST = new PropertiesAttribute(7, typeof(string));
A51 = new PropertiesAttribute(20, typeof(string));
DST = new PropertiesAttribute(6, typeof(string));
DSN = new PropertiesAttribute(35, typeof(string));
PSC = new PropertiesAttribute(5, typeof(string));
PSN = new PropertiesAttribute(35, typeof(string));
VSC = new PropertiesAttribute(9, typeof(string));
VSN = new PropertiesAttribute(35, typeof(string));
ARR = new PropertiesAttribute(8, typeof(DateTime));
MRK = new PropertiesAttribute(140, typeof(string));
ISD = new PropertiesAttribute(8, typeof(DateTime));
IV1 = new PropertiesAttribute(1, typeof(string));
IV3 = new PropertiesAttribute(35, typeof(string));
IV2 = new PropertiesAttribute(12, typeof(int));
IVD = new PropertiesAttribute(8, typeof(DateTime));
IVP = new PropertiesAttribute(7, typeof(string));
IP1 = new PropertiesAttribute(1, typeof(string));
IP2 = new PropertiesAttribute(3, typeof(string));
IP3 = new PropertiesAttribute(3, typeof(string));
IP4 = new PropertiesAttribute(20, typeof(int));
A86 = new PropertiesAttribute(19, typeof(int));
TP = new PropertiesAttribute(20, typeof(int));
A97 = new PropertiesAttribute(1, typeof(string));
N4 = new PropertiesAttribute(1, typeof(string));
VD1 = new PropertiesAttribute(1, typeof(string));
VD2 = new PropertiesAttribute(9, typeof(int));
A93 = new PropertiesAttribute(1, typeof(string));
A94 = new PropertiesAttribute(2, typeof(string));
A95 = new PropertiesAttribute(22, typeof(string));
VCC = new PropertiesAttribute(3, typeof(string));
VPC = new PropertiesAttribute(20, typeof(int));
FR1 = new PropertiesAttribute(1, typeof(string));
FR2 = new PropertiesAttribute(3, typeof(string));
FR3 = new PropertiesAttribute(18, typeof(int));
IN1 = new PropertiesAttribute(1, typeof(string));
IN2 = new PropertiesAttribute(3, typeof(string));
IN3 = new PropertiesAttribute(16, typeof(int));
IN4 = new PropertiesAttribute(6, typeof(string));
VLD = new PropertiesAttribute(840, typeof(string));
B02 = new PropertiesAttribute(11, typeof(int));
B03 = new PropertiesAttribute(11, typeof(int));
ENC = new PropertiesAttribute(1, typeof(string));
TPM = new PropertiesAttribute(1, typeof(string));
BP = new PropertiesAttribute(1, typeof(string));
B08 = new PropertiesAttribute(1, typeof(string));
B12 = new PropertiesAttribute(2, typeof(int));
B13 = new PropertiesAttribute(2, typeof(int));
NT2 = new PropertiesAttribute(300, typeof(string));
REF = new PropertiesAttribute(20, typeof(string));
B16 = new PropertiesAttribute(5, typeof(string));
CCM = new PropertiesAttribute(1, typeof(string));
ADY = new PropertiesAttribute(8, typeof(DateTime));
ADZ = new PropertiesAttribute(6, typeof(string));
B23 = new PropertiesAttribute(81, typeof(string));
DPD = new PropertiesAttribute(8, typeof(DateTime));
ARP = new PropertiesAttribute(7, typeof(string));
ADT = new PropertiesAttribute(8, typeof(DateTime));
#region BL_
List<PropertiesAttribute> listBL_ = new List<PropertiesAttribute>();
listBL_.Add(new PropertiesAttribute(35, 5, EnumGroupID.VAD1AC0_BL_, typeof(string)));
BL_ = new GroupAttribute("BL_", 5, listBL_);
#endregion BL_
#region OL_
List<PropertiesAttribute> listOL_ = new List<PropertiesAttribute>();
listOL_.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAD1AC0_OL_, typeof(string)));
OL_ = new GroupAttribute("OL_", 5, listOL_);
#endregion OL_
#region SS_
List<PropertiesAttribute> listSS_ = new List<PropertiesAttribute>();
listSS_.Add(new PropertiesAttribute(4, 5, EnumGroupID.VAD1AC0_SS_, typeof(string)));
listSS_.Add(new PropertiesAttribute(20, 5, EnumGroupID.VAD1AC0_SN_, typeof(string)));
SS_ = new GroupAttribute("SS_", 5, listSS_);
#endregion SS_
#region VR_
List<PropertiesAttribute> listVR_ = new List<PropertiesAttribute>();
listVR_.Add(new PropertiesAttribute(1, 5, EnumGroupID.VAD1AC0_VR_, typeof(string)));
listVR_.Add(new PropertiesAttribute(3, 5, EnumGroupID.VAD1AC0_VL_, typeof(string)));
listVR_.Add(new PropertiesAttribute(3, 5, EnumGroupID.VAD1AC0_VC_, typeof(string)));
listVR_.Add(new PropertiesAttribute(20, 5, EnumGroupID.VAD1AC0_VP_, typeof(int)));
listVR_.Add(new PropertiesAttribute(20, 5, EnumGroupID.VAD1AC0_VT_, typeof(int)));
VR_ = new GroupAttribute("VR_", 5, listVR_);
#endregion VR_
#region KF1
List<PropertiesAttribute> listKF1 = new List<PropertiesAttribute>();
listKF1.Add(new PropertiesAttribute(1, 6, EnumGroupID.VAD1AC0_KF1, typeof(string)));
listKF1.Add(new PropertiesAttribute(27, 6, EnumGroupID.VAD1AC0_KG1, typeof(string)));
listKF1.Add(new PropertiesAttribute(11, 6, EnumGroupID.VAD1AC0_KH1, typeof(int)));
listKF1.Add(new PropertiesAttribute(2, 6, EnumGroupID.VAD1AC0_Kl1, typeof(int)));
KF1 = new GroupAttribute("KF1", 6, listKF1);
#endregion KF1
#region KJ1
List<PropertiesAttribute> listKJ1 = new List<PropertiesAttribute>();
listKJ1.Add(new PropertiesAttribute(3, 3, EnumGroupID.VAD1AC0_KJ1, typeof(string)));
listKJ1.Add(new PropertiesAttribute(9, 3, EnumGroupID.VAD1AC0_KK1, typeof(int)));
KJ1 = new GroupAttribute("KJ1", 3, listKJ1);
#endregion KJ1
#region EA_
List<PropertiesAttribute> listEA_ = new List<PropertiesAttribute>();
listEA_.Add(new PropertiesAttribute(3, 3, EnumGroupID.VAD1AC0_EA_, typeof(string)));
listEA_.Add(new PropertiesAttribute(12, 3, EnumGroupID.VAD1AC0_EB, typeof(int)));
EA_ = new GroupAttribute("EA_", 3, listEA_);
#endregion EA_
#region D__
List<PropertiesAttribute> listD__ = new List<PropertiesAttribute>();
listD__.Add(new PropertiesAttribute(8, 10, EnumGroupID.VAD1AC0_D__, typeof(DateTime)));
listD__.Add(new PropertiesAttribute(120, 10, EnumGroupID.VAD1AC0_T__, typeof(string)));
listD__.Add(new PropertiesAttribute(420, 10, EnumGroupID.VAD1AC0_I__, typeof(string)));
D__ = new GroupAttribute("D__", 10, listD__);
#endregion D__
#region KN1
List<PropertiesAttribute> listKN1 = new List<PropertiesAttribute>();
listKN1.Add(new PropertiesAttribute(1, 6, EnumGroupID.VAD1AC0_KN1, typeof(string)));
listKN1.Add(new PropertiesAttribute(27, 6, EnumGroupID.VAD1AC0_KO1, typeof(string)));
listKN1.Add(new PropertiesAttribute(8, 6, EnumGroupID.VAD1AC0_KP1, typeof(DateTime)));
KN1 = new GroupAttribute("KN1", 6, listKN1);
#endregion KN1
#region ST_
List<PropertiesAttribute> listST_ = new List<PropertiesAttribute>();
listST_.Add(new PropertiesAttribute(7, 3, EnumGroupID.VAD1AC0_ST_, typeof(string)));
listST_.Add(new PropertiesAttribute(8, 3, EnumGroupID.VAD1AC0_AD_, typeof(DateTime)));
listST_.Add(new PropertiesAttribute(8, 3, EnumGroupID.VAD1AC0_SD_, typeof(DateTime)));
ST_ = new GroupAttribute("ST_", 3, listST_);
#endregion ST_
TongSoByte = 10416;
}

}public partial class EnumGroupID
    {
public static readonly string VAD1AC0_BL_ = "VAD1AC0_BL_";
public static readonly string VAD1AC0_OL_ = "VAD1AC0_OL_";
public static readonly string VAD1AC0_SS_ = "VAD1AC0_SS_";
public static readonly string VAD1AC0_SN_ = "VAD1AC0_SN_";
public static readonly string VAD1AC0_VR_ = "VAD1AC0_VR_";
public static readonly string VAD1AC0_VL_ = "VAD1AC0_VL_";
public static readonly string VAD1AC0_VC_ = "VAD1AC0_VC_";
public static readonly string VAD1AC0_VP_ = "VAD1AC0_VP_";
public static readonly string VAD1AC0_VT_ = "VAD1AC0_VT_";
public static readonly string VAD1AC0_KF1 = "VAD1AC0_KF1";
public static readonly string VAD1AC0_KG1 = "VAD1AC0_KG1";
public static readonly string VAD1AC0_KH1 = "VAD1AC0_KH1";
public static readonly string VAD1AC0_Kl1 = "VAD1AC0_Kl1";
public static readonly string VAD1AC0_KJ1 = "VAD1AC0_KJ1";
public static readonly string VAD1AC0_KK1 = "VAD1AC0_KK1";
public static readonly string VAD1AC0_EA_ = "VAD1AC0_EA_";
public static readonly string VAD1AC0_EB = "VAD1AC0_EB";
public static readonly string VAD1AC0_D__ = "VAD1AC0_D__";
public static readonly string VAD1AC0_T__ = "VAD1AC0_T__";
public static readonly string VAD1AC0_I__ = "VAD1AC0_I__";
public static readonly string VAD1AC0_KN1 = "VAD1AC0_KN1";
public static readonly string VAD1AC0_KO1 = "VAD1AC0_KO1";
public static readonly string VAD1AC0_KP1 = "VAD1AC0_KP1";
public static readonly string VAD1AC0_ST_ = "VAD1AC0_ST_";
public static readonly string VAD1AC0_AD_ = "VAD1AC0_AD_";
public static readonly string VAD1AC0_SD_ = "VAD1AC0_SD_";
}

}
