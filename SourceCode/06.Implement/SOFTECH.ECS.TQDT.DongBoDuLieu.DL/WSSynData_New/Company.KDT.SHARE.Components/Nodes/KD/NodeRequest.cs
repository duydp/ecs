﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Nodes.KD
{
    public class NodeRequest
    {
        /// <summary>
        /// Tiêu đề
        /// </summary>
        public const string Header = "Header";
        /// <summary>
        /// loại thủ tục áp dụng
        /// </summary>
        public const string procedureType = "procedureType";
        /// <summary>
        /// message
        /// </summary>
        public const string Reference = "Reference";
        /// <summary>
        /// phiên bản message || Phiên bản phần mềm
        /// </summary>
        public const string version = "version";
        /// <summary>
        /// định danh message
        /// </summary>
        public const string messageId = "messageId";
        /// <summary>
        /// Ứng dụng gửi phía doanh nghiệp
        /// </summary>
        public const string SendApplication = "SendApplication";
        /// <summary>
        /// Tên phần mềm || Tên người gửi || Tên người nhận || Tên đơn vị VAN || 
        /// </summary>
        public const string name = "name";
        /// <summary>
        /// Tên công ty
        /// </summary>
        public const string companyName = "companyName";
        /// <summary>
        /// Mã công ty
        /// </summary>
        public const string companyIdentity = "companyIdentity";
        /// <summary>
        /// Ngày giờ biên soạn message
        /// </summary>
        public const string createMessageIssue = "createMessageIssue";
        /// <summary>
        /// Chữ ký xác thực phần mềm
        /// </summary>
        public const string Signature = "Signature";
        /// <summary>
        /// Nội dung chữ ký số
        /// </summary>
        public const string data = "data";
        /// <summary>
        /// Nội dung file cert || Chứa nội dung chứng thư
        /// </summary>
        public const string fileCert = "fileCert";
        /// <summary>
        /// Người gửi
        /// </summary>
        public const string From = "From";
        /// <summary>
        /// Mã người gửi || Mã người nhận || Mã đơn vị VAN
        /// </summary>
        public const string identity = "identity";
        /// <summary>
        /// Người nhận
        /// </summary>
        public const string To = "To";
        /// <summary>
        /// Đơn vị vận
        /// </summary>
        public const string VAN = "VAN";
        /// <summary>
        /// Message
        /// </summary>
        public const string Subject = "Subject";
        /// <summary>
        /// Loại message
        /// </summary>
        public const string type = "type";
        /// <summary>
        /// chức năng message
        /// </summary>
        public const string function = "function";
        /// <summary>
        /// Số tham chiếu
        /// </summary>
        public const string reference = "reference";
        /// <summary>
        /// Thân
        /// </summary>
        public const string Body = "Body";
        /// <summary>
        /// Nội dung thông tin khai báo
        /// </summary>
        public const string Content = "Content";						

    }
}
