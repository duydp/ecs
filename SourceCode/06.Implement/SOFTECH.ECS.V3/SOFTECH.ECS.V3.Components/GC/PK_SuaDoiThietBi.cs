﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.GC;
using System.Linq;

namespace SOFTECH.ECS.V3.Components.GC
{
    public partial class PK_SuaDoiThietBi
    {
        /// <summary>
        /// ConfigEquipment
        /// </summary>
        /// <param name="Equipment">XElement</param>
        /// <param name="TB">ThietBi</param>
        /// <returns>XElement</returns>
        public static XElement ConfigEquipment(XElement Equipment, ThietBi TB)
        {
            if (Equipment != null && Equipment.Elements().Count() > 0 && TB != null)
            {
                DonViTinh DVT = DonViTinh.Load(TB.DVT_ID);
                Nuoc nuoc = Nuoc.Load(TB.NuocXX_ID);
                NguyenTe NT = NguyenTe.Load(TB.NguyenTe_ID);
                foreach (XElement element in Equipment.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodePhuKien.preIdentification.ToUpper()))
                        element.SetValue("");
                    if (element.Name.ToString().ToUpper().Equals(NodePhuKien.Commodity.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                        {
                            if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.description.ToUpper()))
                                elementChild.SetValue(TB.Ten);
                            if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.identification.ToUpper()))
                                elementChild.SetValue(TB.Ma);
                            if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.tariffClassification.ToUpper()))
                                elementChild.SetValue(TB.MaHS);
                        }
                    }
                    if (DVT != null)
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.GoodsMeasure.ToUpper()))
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.quantity.ToUpper()))
                                    elementChild.SetValue(TB.SoLuongDangKy);
                                if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.measureUnit.ToUpper()))
                                    elementChild.SetValue(DVT.Ten);
                            }
                        }
                    }
                    if (nuoc != null)
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.Origin.ToUpper()))
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.originCountry.ToUpper()))
                                    elementChild.SetValue(nuoc.Ten);
                            }
                        }
                    }
                    if (NT != null)
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.CurrencyExchange.ToUpper()))
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.currencyType.ToUpper()))
                                    elementChild.SetValue(NT.Ten);
                            }
                        }
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodePhuKien.CustomsValue.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                        {
                            if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.unitPrice.ToUpper()))
                                elementChild.SetValue(TB.TriGia);
                        }
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodePhuKien.status.ToUpper()))
                    {
                        element.SetValue("");
                    }
                }
            }
            return Equipment;
        }
        /// <summary>
        /// ConfigPK_SuaDoiThietBi
        /// </summary> 
        /// <returns>string</returns>
        public static string ConfigPK_SuaDoiThietBi(PhuKienDangKy PKDK, int issuer, int function, int status)
        {
            try
            {
                XDocument docXML = XDocument.Load("../../../SOFTECH.ECS.V3.Components/XML/GC/PK_SuaDoiThietBi.xml");
                HopDong HD = HopDong.Load(PKDK.HopDong_ID);
                if (HD != null && docXML != null)
                {
                    IList<ThietBi> lTB = ThietBi.SelectCollectionBy_HopDong_ID(HD.ID);
                    XElement Root = docXML.Root;
                    PhuKienChung.ConfigPhuKien(Root, PKDK, issuer, function, status);

                    XElement AdditionalInformation = Root.Element(NodePhuKien.AdditionalInformation);
                    XElement Content = AdditionalInformation.Element(NodePhuKien.Content);
                    XElement Equipment = Content.Element(NodePhuKien.Equipment);
                    if (Root != null && Root.Elements().Count() > 0)
                    {
                        foreach (XElement element in Root.Elements())
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.AdditionalInformation.ToUpper()))
                            {
                                AdditionalInformation = element;
                                foreach (XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.Content.ToUpper()))
                                    {
                                        Content = elementChild;
                                        foreach (XElement Child in elementChild.Elements())
                                        {
                                            if (Child.Name.ToString().ToUpper().Equals(NodePhuKien.Material.ToUpper()))
                                                Equipment = Child;
                                        }
                                    }
                                }
                            }
                        }
                        if (lTB != null)
                        {
                            if (lTB.Count == 1)
                            {
                                foreach (XElement element in Equipment.Elements())
                                {
                                    ConfigEquipment(Equipment, lTB[0]);
                                }
                            }
                            else if (lTB.Count > 1)
                            {
                                XElement TempEquipment = new XElement(Equipment);
                                ConfigEquipment(Equipment, lTB[0]);
                                // Add new Equipment
                                for (int i = 1; i < lTB.Count; i++)
                                {
                                    XElement NewEquipment = new XElement(TempEquipment);
                                    Content.Add(ConfigEquipment(NewEquipment, lTB[i]));
                                }
                            }
                        }
                        docXML.Save(@"D:\NewPK_SuaDoiThietBi.xml");
                        return docXML.ToString();
                    }
                }
                return "";
            }
            catch
            {
                return "";
            }
        }
    }
}
