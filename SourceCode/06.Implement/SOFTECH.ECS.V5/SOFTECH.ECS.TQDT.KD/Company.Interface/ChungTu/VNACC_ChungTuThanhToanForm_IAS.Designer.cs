﻿namespace Company.Interface
{
    partial class VNACC_ChungTuThanhToanForm_IAS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grdHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_ChungTuThanhToanForm_IAS));
            this.txtMaNganHangCungCapBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ucLoaiHinhBaoLanh = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTenNganHangCungCapBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNamPhatHanh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtKiHieuChungTuPhatHanhBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.dtNgayHetHieuLuc = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtNgayBatDauHieuLuc = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtNgayDuDinhKhaiBao = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label22 = new System.Windows.Forms.Label();
            this.txtSoDuBaoLanh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienDangKyBaoLanh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoVanDon_4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoVanDon_2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoVanDon_5 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoVanDon_3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoVanDon_1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDonViDaiDienSuDungBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDonViSuDungBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtCoQuanHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtCoBaoVohieu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ucMaTienTe = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtMaLoaiHinh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoChungTuPhatHanhBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTenDonViDaiDienSuDungBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDonViSuDungBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenCucHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.grdHang = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtCoQuanhaiQuanChiTiet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenCucHaiQuanChiTiet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label25 = new System.Windows.Forms.Label();
            this.dtThoiGianTaoDuLieu = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtNgayDangKyToKhai = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtNgayTaoDuLieu = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoToKhaiChiTiet = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoThuTuGiaoDich = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienTangLen = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienTruLui = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txtMaLoaiHinhChiTiet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdLayPhanHoi1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLayPhanHoi");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdLayPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdLayPhanHoi");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 539), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 31);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 539);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 515);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 515);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Location = new System.Drawing.Point(203, 31);
            this.grbMain.Size = new System.Drawing.Size(706, 539);
            // 
            // txtMaNganHangCungCapBaoLanh
            // 
            this.txtMaNganHangCungCapBaoLanh.Location = new System.Drawing.Point(121, 58);
            this.txtMaNganHangCungCapBaoLanh.Name = "txtMaNganHangCungCapBaoLanh";
            this.txtMaNganHangCungCapBaoLanh.Size = new System.Drawing.Size(100, 21);
            this.txtMaNganHangCungCapBaoLanh.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(16, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Loại hình bảo lãnh";
            // 
            // ucLoaiHinhBaoLanh
            // 
            this.ucLoaiHinhBaoLanh.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucLoaiHinhBaoLanh.Appearance.Options.UseBackColor = true;
            this.ucLoaiHinhBaoLanh.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E032;
            this.ucLoaiHinhBaoLanh.Code = "";
            this.ucLoaiHinhBaoLanh.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucLoaiHinhBaoLanh.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucLoaiHinhBaoLanh.IsValidate = true;
            this.ucLoaiHinhBaoLanh.Location = new System.Drawing.Point(121, 10);
            this.ucLoaiHinhBaoLanh.Name = "ucLoaiHinhBaoLanh";
            this.ucLoaiHinhBaoLanh.Name_VN = "";
            this.ucLoaiHinhBaoLanh.ShowColumnCode = true;
            this.ucLoaiHinhBaoLanh.ShowColumnName = true;
            this.ucLoaiHinhBaoLanh.Size = new System.Drawing.Size(100, 26);
            this.ucLoaiHinhBaoLanh.TabIndex = 0;
            this.ucLoaiHinhBaoLanh.TagName = "";
            this.ucLoaiHinhBaoLanh.WhereCondition = "";
            this.ucLoaiHinhBaoLanh.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(16, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ngân hàng cung cấp bảo lãnh";
            // 
            // txtTenNganHangCungCapBaoLanh
            // 
            this.txtTenNganHangCungCapBaoLanh.Location = new System.Drawing.Point(227, 58);
            this.txtTenNganHangCungCapBaoLanh.Name = "txtTenNganHangCungCapBaoLanh";
            this.txtTenNganHangCungCapBaoLanh.Size = new System.Drawing.Size(458, 21);
            this.txtTenNganHangCungCapBaoLanh.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(16, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Năm phát hành";
            // 
            // txtNamPhatHanh
            // 
            this.txtNamPhatHanh.DecimalDigits = 0;
            this.txtNamPhatHanh.Location = new System.Drawing.Point(121, 85);
            this.txtNamPhatHanh.MaxLength = 4;
            this.txtNamPhatHanh.Name = "txtNamPhatHanh";
            this.txtNamPhatHanh.Size = new System.Drawing.Size(100, 21);
            this.txtNamPhatHanh.TabIndex = 3;
            this.txtNamPhatHanh.Text = "0";
            this.txtNamPhatHanh.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // txtKiHieuChungTuPhatHanhBaoLanh
            // 
            this.txtKiHieuChungTuPhatHanhBaoLanh.Location = new System.Drawing.Point(366, 85);
            this.txtKiHieuChungTuPhatHanhBaoLanh.Name = "txtKiHieuChungTuPhatHanhBaoLanh";
            this.txtKiHieuChungTuPhatHanhBaoLanh.Size = new System.Drawing.Size(100, 21);
            this.txtKiHieuChungTuPhatHanhBaoLanh.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(227, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(133, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Ký hiệu chứng từ bảo lãnh";
            // 
            // uiTab1
            // 
            this.uiTab1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(706, 503);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2007;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.AutoScroll = true;
            this.uiTabPage1.Controls.Add(this.dtNgayHetHieuLuc);
            this.uiTabPage1.Controls.Add(this.dtNgayBatDauHieuLuc);
            this.uiTabPage1.Controls.Add(this.dtNgayDuDinhKhaiBao);
            this.uiTabPage1.Controls.Add(this.label22);
            this.uiTabPage1.Controls.Add(this.label1);
            this.uiTabPage1.Controls.Add(this.txtSoDuBaoLanh);
            this.uiTabPage1.Controls.Add(this.txtSoTienDangKyBaoLanh);
            this.uiTabPage1.Controls.Add(this.txtSoToKhai);
            this.uiTabPage1.Controls.Add(this.txtNamPhatHanh);
            this.uiTabPage1.Controls.Add(this.txtSoVanDon_4);
            this.uiTabPage1.Controls.Add(this.txtSoVanDon_2);
            this.uiTabPage1.Controls.Add(this.txtSoHoaDon);
            this.uiTabPage1.Controls.Add(this.txtSoVanDon_5);
            this.uiTabPage1.Controls.Add(this.txtSoVanDon_3);
            this.uiTabPage1.Controls.Add(this.txtSoVanDon_1);
            this.uiTabPage1.Controls.Add(this.txtMaDonViDaiDienSuDungBaoLanh);
            this.uiTabPage1.Controls.Add(this.txtMaDonViSuDungBaoLanh);
            this.uiTabPage1.Controls.Add(this.txtCoQuanHaiQuan);
            this.uiTabPage1.Controls.Add(this.txtCoBaoVohieu);
            this.uiTabPage1.Controls.Add(this.txtMaNganHangCungCapBaoLanh);
            this.uiTabPage1.Controls.Add(this.ucMaTienTe);
            this.uiTabPage1.Controls.Add(this.ucLoaiHinhBaoLanh);
            this.uiTabPage1.Controls.Add(this.txtMaLoaiHinh);
            this.uiTabPage1.Controls.Add(this.txtSoChungTuPhatHanhBaoLanh);
            this.uiTabPage1.Controls.Add(this.txtKiHieuChungTuPhatHanhBaoLanh);
            this.uiTabPage1.Controls.Add(this.label5);
            this.uiTabPage1.Controls.Add(this.label8);
            this.uiTabPage1.Controls.Add(this.label7);
            this.uiTabPage1.Controls.Add(this.txtTenDonViDaiDienSuDungBaoLanh);
            this.uiTabPage1.Controls.Add(this.label4);
            this.uiTabPage1.Controls.Add(this.txtTenDonViSuDungBaoLanh);
            this.uiTabPage1.Controls.Add(this.label24);
            this.uiTabPage1.Controls.Add(this.label6);
            this.uiTabPage1.Controls.Add(this.txtTenCucHaiQuan);
            this.uiTabPage1.Controls.Add(this.txtTenNganHangCungCapBaoLanh);
            this.uiTabPage1.Controls.Add(this.label14);
            this.uiTabPage1.Controls.Add(this.label3);
            this.uiTabPage1.Controls.Add(this.label15);
            this.uiTabPage1.Controls.Add(this.label12);
            this.uiTabPage1.Controls.Add(this.label13);
            this.uiTabPage1.Controls.Add(this.label20);
            this.uiTabPage1.Controls.Add(this.label10);
            this.uiTabPage1.Controls.Add(this.label19);
            this.uiTabPage1.Controls.Add(this.label18);
            this.uiTabPage1.Controls.Add(this.label11);
            this.uiTabPage1.Controls.Add(this.label23);
            this.uiTabPage1.Controls.Add(this.label17);
            this.uiTabPage1.Controls.Add(this.label21);
            this.uiTabPage1.Controls.Add(this.label16);
            this.uiTabPage1.Controls.Add(this.label9);
            this.uiTabPage1.Controls.Add(this.label2);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(704, 481);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Thông tin chung";
            // 
            // dtNgayHetHieuLuc
            // 
            // 
            // 
            // 
            this.dtNgayHetHieuLuc.DropDownCalendar.Name = "";
            this.dtNgayHetHieuLuc.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayHetHieuLuc.Location = new System.Drawing.Point(585, 420);
            this.dtNgayHetHieuLuc.Name = "dtNgayHetHieuLuc";
            this.dtNgayHetHieuLuc.Size = new System.Drawing.Size(100, 21);
            this.dtNgayHetHieuLuc.TabIndex = 23;
            this.dtNgayHetHieuLuc.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtNgayBatDauHieuLuc
            // 
            // 
            // 
            // 
            this.dtNgayBatDauHieuLuc.DropDownCalendar.Name = "";
            this.dtNgayBatDauHieuLuc.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayBatDauHieuLuc.Location = new System.Drawing.Point(260, 420);
            this.dtNgayBatDauHieuLuc.Name = "dtNgayBatDauHieuLuc";
            this.dtNgayBatDauHieuLuc.Size = new System.Drawing.Size(100, 21);
            this.dtNgayBatDauHieuLuc.TabIndex = 22;
            this.dtNgayBatDauHieuLuc.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtNgayDuDinhKhaiBao
            // 
            // 
            // 
            // 
            this.dtNgayDuDinhKhaiBao.DropDownCalendar.Name = "";
            this.dtNgayDuDinhKhaiBao.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayDuDinhKhaiBao.Location = new System.Drawing.Point(366, 112);
            this.dtNgayDuDinhKhaiBao.Name = "dtNgayDuDinhKhaiBao";
            this.dtNgayDuDinhKhaiBao.Size = new System.Drawing.Size(100, 21);
            this.dtNgayDuDinhKhaiBao.TabIndex = 7;
            this.dtNgayDuDinhKhaiBao.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayDuDinhKhaiBao.VisualStyleManager = this.vsmMain;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Location = new System.Drawing.Point(16, 478);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 13);
            this.label22.TabIndex = 1;
            this.label22.Text = "Mã tiền tệ";
            // 
            // txtSoDuBaoLanh
            // 
            this.txtSoDuBaoLanh.DecimalDigits = 0;
            this.txtSoDuBaoLanh.Location = new System.Drawing.Point(585, 447);
            this.txtSoDuBaoLanh.MaxLength = 13;
            this.txtSoDuBaoLanh.Name = "txtSoDuBaoLanh";
            this.txtSoDuBaoLanh.Size = new System.Drawing.Size(100, 21);
            this.txtSoDuBaoLanh.TabIndex = 25;
            this.txtSoDuBaoLanh.Text = "0";
            this.txtSoDuBaoLanh.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // txtSoTienDangKyBaoLanh
            // 
            this.txtSoTienDangKyBaoLanh.DecimalDigits = 0;
            this.txtSoTienDangKyBaoLanh.Location = new System.Drawing.Point(227, 447);
            this.txtSoTienDangKyBaoLanh.MaxLength = 13;
            this.txtSoTienDangKyBaoLanh.Name = "txtSoTienDangKyBaoLanh";
            this.txtSoTienDangKyBaoLanh.Size = new System.Drawing.Size(133, 21);
            this.txtSoTienDangKyBaoLanh.TabIndex = 24;
            this.txtSoTienDangKyBaoLanh.Text = "0";
            this.txtSoTienDangKyBaoLanh.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.DecimalDigits = 0;
            this.txtSoToKhai.Location = new System.Drawing.Point(121, 112);
            this.txtSoToKhai.MaxLength = 4;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(100, 21);
            this.txtSoToKhai.TabIndex = 6;
            this.txtSoToKhai.Text = "0";
            this.txtSoToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // txtSoVanDon_4
            // 
            this.txtSoVanDon_4.Location = new System.Drawing.Point(446, 246);
            this.txtSoVanDon_4.Name = "txtSoVanDon_4";
            this.txtSoVanDon_4.Size = new System.Drawing.Size(239, 21);
            this.txtSoVanDon_4.TabIndex = 15;
            // 
            // txtSoVanDon_2
            // 
            this.txtSoVanDon_2.Location = new System.Drawing.Point(446, 219);
            this.txtSoVanDon_2.Name = "txtSoVanDon_2";
            this.txtSoVanDon_2.Size = new System.Drawing.Size(239, 21);
            this.txtSoVanDon_2.TabIndex = 13;
            // 
            // txtSoHoaDon
            // 
            this.txtSoHoaDon.Location = new System.Drawing.Point(121, 300);
            this.txtSoHoaDon.Name = "txtSoHoaDon";
            this.txtSoHoaDon.Size = new System.Drawing.Size(239, 21);
            this.txtSoHoaDon.TabIndex = 17;
            // 
            // txtSoVanDon_5
            // 
            this.txtSoVanDon_5.Location = new System.Drawing.Point(121, 273);
            this.txtSoVanDon_5.Name = "txtSoVanDon_5";
            this.txtSoVanDon_5.Size = new System.Drawing.Size(239, 21);
            this.txtSoVanDon_5.TabIndex = 16;
            // 
            // txtSoVanDon_3
            // 
            this.txtSoVanDon_3.Location = new System.Drawing.Point(121, 246);
            this.txtSoVanDon_3.Name = "txtSoVanDon_3";
            this.txtSoVanDon_3.Size = new System.Drawing.Size(239, 21);
            this.txtSoVanDon_3.TabIndex = 14;
            // 
            // txtSoVanDon_1
            // 
            this.txtSoVanDon_1.Location = new System.Drawing.Point(121, 219);
            this.txtSoVanDon_1.Name = "txtSoVanDon_1";
            this.txtSoVanDon_1.Size = new System.Drawing.Size(239, 21);
            this.txtSoVanDon_1.TabIndex = 12;
            // 
            // txtMaDonViDaiDienSuDungBaoLanh
            // 
            this.txtMaDonViDaiDienSuDungBaoLanh.Location = new System.Drawing.Point(121, 393);
            this.txtMaDonViDaiDienSuDungBaoLanh.Name = "txtMaDonViDaiDienSuDungBaoLanh";
            this.txtMaDonViDaiDienSuDungBaoLanh.Size = new System.Drawing.Size(100, 21);
            this.txtMaDonViDaiDienSuDungBaoLanh.TabIndex = 20;
            // 
            // txtMaDonViSuDungBaoLanh
            // 
            this.txtMaDonViSuDungBaoLanh.Location = new System.Drawing.Point(121, 350);
            this.txtMaDonViSuDungBaoLanh.Name = "txtMaDonViSuDungBaoLanh";
            this.txtMaDonViSuDungBaoLanh.Size = new System.Drawing.Size(100, 21);
            this.txtMaDonViSuDungBaoLanh.TabIndex = 18;
            // 
            // txtCoQuanHaiQuan
            // 
            this.txtCoQuanHaiQuan.Location = new System.Drawing.Point(121, 167);
            this.txtCoQuanHaiQuan.Name = "txtCoQuanHaiQuan";
            this.txtCoQuanHaiQuan.Size = new System.Drawing.Size(100, 21);
            this.txtCoQuanHaiQuan.TabIndex = 10;
            // 
            // txtCoBaoVohieu
            // 
            this.txtCoBaoVohieu.Location = new System.Drawing.Point(588, 139);
            this.txtCoBaoVohieu.Name = "txtCoBaoVohieu";
            this.txtCoBaoVohieu.Size = new System.Drawing.Size(44, 21);
            this.txtCoBaoVohieu.TabIndex = 9;
            // 
            // ucMaTienTe
            // 
            this.ucMaTienTe.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaTienTe.Appearance.Options.UseBackColor = true;
            this.ucMaTienTe.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ucMaTienTe.Code = "";
            this.ucMaTienTe.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaTienTe.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaTienTe.IsValidate = true;
            this.ucMaTienTe.Location = new System.Drawing.Point(227, 471);
            this.ucMaTienTe.Name = "ucMaTienTe";
            this.ucMaTienTe.Name_VN = "";
            this.ucMaTienTe.ShowColumnCode = true;
            this.ucMaTienTe.ShowColumnName = false;
            this.ucMaTienTe.Size = new System.Drawing.Size(133, 26);
            this.ucMaTienTe.TabIndex = 26;
            this.ucMaTienTe.TagName = "";
            this.ucMaTienTe.WhereCondition = "";
            this.ucMaTienTe.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtMaLoaiHinh
            // 
            this.txtMaLoaiHinh.Location = new System.Drawing.Point(588, 112);
            this.txtMaLoaiHinh.Name = "txtMaLoaiHinh";
            this.txtMaLoaiHinh.Size = new System.Drawing.Size(97, 21);
            this.txtMaLoaiHinh.TabIndex = 8;
            // 
            // txtSoChungTuPhatHanhBaoLanh
            // 
            this.txtSoChungTuPhatHanhBaoLanh.Location = new System.Drawing.Point(588, 85);
            this.txtSoChungTuPhatHanhBaoLanh.Name = "txtSoChungTuPhatHanhBaoLanh";
            this.txtSoChungTuPhatHanhBaoLanh.Size = new System.Drawing.Size(97, 21);
            this.txtSoChungTuPhatHanhBaoLanh.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(472, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Số chứng từ bảo lãnh";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(472, 117);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Mã loại hình";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(227, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Ngày dự định khai báo";
            // 
            // txtTenDonViDaiDienSuDungBaoLanh
            // 
            this.txtTenDonViDaiDienSuDungBaoLanh.Location = new System.Drawing.Point(227, 393);
            this.txtTenDonViDaiDienSuDungBaoLanh.Name = "txtTenDonViDaiDienSuDungBaoLanh";
            this.txtTenDonViDaiDienSuDungBaoLanh.Size = new System.Drawing.Size(458, 21);
            this.txtTenDonViDaiDienSuDungBaoLanh.TabIndex = 21;
            // 
            // txtTenDonViSuDungBaoLanh
            // 
            this.txtTenDonViSuDungBaoLanh.Location = new System.Drawing.Point(227, 350);
            this.txtTenDonViSuDungBaoLanh.Name = "txtTenDonViSuDungBaoLanh";
            this.txtTenDonViSuDungBaoLanh.Size = new System.Drawing.Size(458, 21);
            this.txtTenDonViSuDungBaoLanh.TabIndex = 19;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(472, 143);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(79, 13);
            this.label24.TabIndex = 1;
            this.label24.Text = "Cờ báo vô hiệu";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(16, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Số tờ khai";
            // 
            // txtTenCucHaiQuan
            // 
            this.txtTenCucHaiQuan.Location = new System.Drawing.Point(227, 167);
            this.txtTenCucHaiQuan.Name = "txtTenCucHaiQuan";
            this.txtTenCucHaiQuan.Size = new System.Drawing.Size(458, 21);
            this.txtTenCucHaiQuan.TabIndex = 11;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(423, 250);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "4.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(98, 277);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "5.";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(423, 223);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "2.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(98, 250);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "3.";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Location = new System.Drawing.Point(380, 425);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(199, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Ngày hết hiệu lực bảo lãnh (cho ân hạn)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(16, 203);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(165, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Số vận đơn (Số B/L, số AWB, ...)";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Location = new System.Drawing.Point(16, 425);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(220, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "Ngày bắt đầu hiệu lực bảo lãnh (cho ân hạn)";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Location = new System.Drawing.Point(16, 377);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(164, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Đơn vị đại diện sử dụng bảo lãnh";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(98, 223);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "1.";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Location = new System.Drawing.Point(380, 451);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(79, 13);
            this.label23.TabIndex = 1;
            this.label23.Text = "Số dư bảo lãnh";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(16, 334);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(124, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Đơn vị sử dụng bảo lãnh";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(16, 451);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(125, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Số tiền đăng ký bảo lãnh";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(16, 304);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Số hóa đơn";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(16, 151);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Cơ quan Hải quan";
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.grdHang);
            this.uiTabPage2.Controls.Add(this.uiGroupBox1);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(704, 481);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Thông tin chi tiết";
            // 
            // grdHang
            // 
            this.grdHang.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdHang.CardColumnHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.ColumnAutoResize = true;
            grdHang_DesignTimeLayout.LayoutString = resources.GetString("grdHang_DesignTimeLayout.LayoutString");
            this.grdHang.DesignTimeLayout = grdHang_DesignTimeLayout;
            this.grdHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grdHang.GroupByBoxVisible = false;
            this.grdHang.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.Location = new System.Drawing.Point(0, 120);
            this.grdHang.Name = "grdHang";
            this.grdHang.RecordNavigator = true;
            this.grdHang.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grdHang.RowHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdHang.RowHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdHang.Size = new System.Drawing.Size(704, 361);
            this.grdHang.TabIndex = 2;
            this.grdHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grdHang.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtCoQuanhaiQuanChiTiet);
            this.uiGroupBox1.Controls.Add(this.txtTenCucHaiQuanChiTiet);
            this.uiGroupBox1.Controls.Add(this.label25);
            this.uiGroupBox1.Controls.Add(this.dtThoiGianTaoDuLieu);
            this.uiGroupBox1.Controls.Add(this.dtNgayDangKyToKhai);
            this.uiGroupBox1.Controls.Add(this.dtNgayTaoDuLieu);
            this.uiGroupBox1.Controls.Add(this.txtSoToKhaiChiTiet);
            this.uiGroupBox1.Controls.Add(this.txtSoThuTuGiaoDich);
            this.uiGroupBox1.Controls.Add(this.txtSoTienTangLen);
            this.uiGroupBox1.Controls.Add(this.txtSoTienTruLui);
            this.uiGroupBox1.Controls.Add(this.label31);
            this.uiGroupBox1.Controls.Add(this.label29);
            this.uiGroupBox1.Controls.Add(this.label33);
            this.uiGroupBox1.Controls.Add(this.label32);
            this.uiGroupBox1.Controls.Add(this.label30);
            this.uiGroupBox1.Controls.Add(this.label28);
            this.uiGroupBox1.Controls.Add(this.txtMaLoaiHinhChiTiet);
            this.uiGroupBox1.Controls.Add(this.label27);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(704, 120);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtCoQuanhaiQuanChiTiet
            // 
            this.txtCoQuanhaiQuanChiTiet.Location = new System.Drawing.Point(114, 92);
            this.txtCoQuanhaiQuanChiTiet.Name = "txtCoQuanhaiQuanChiTiet";
            this.txtCoQuanhaiQuanChiTiet.Size = new System.Drawing.Size(113, 21);
            this.txtCoQuanhaiQuanChiTiet.TabIndex = 8;
            // 
            // txtTenCucHaiQuanChiTiet
            // 
            this.txtTenCucHaiQuanChiTiet.Location = new System.Drawing.Point(233, 92);
            this.txtTenCucHaiQuanChiTiet.Name = "txtTenCucHaiQuanChiTiet";
            this.txtTenCucHaiQuanChiTiet.Size = new System.Drawing.Size(445, 21);
            this.txtTenCucHaiQuanChiTiet.TabIndex = 9;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Location = new System.Drawing.Point(10, 96);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(92, 13);
            this.label25.TabIndex = 12;
            this.label25.Text = "Cơ quan Hải quan";
            // 
            // dtThoiGianTaoDuLieu
            // 
            this.dtThoiGianTaoDuLieu.CustomFormat = "hh:mm:ss";
            this.dtThoiGianTaoDuLieu.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dtThoiGianTaoDuLieu.DropDownCalendar.Name = "";
            this.dtThoiGianTaoDuLieu.Location = new System.Drawing.Point(503, 11);
            this.dtThoiGianTaoDuLieu.Name = "dtThoiGianTaoDuLieu";
            this.dtThoiGianTaoDuLieu.Size = new System.Drawing.Size(74, 21);
            this.dtThoiGianTaoDuLieu.TabIndex = 2;
            // 
            // dtNgayDangKyToKhai
            // 
            // 
            // 
            // 
            this.dtNgayDangKyToKhai.DropDownCalendar.Name = "";
            this.dtNgayDangKyToKhai.Location = new System.Drawing.Point(384, 65);
            this.dtNgayDangKyToKhai.Name = "dtNgayDangKyToKhai";
            this.dtNgayDangKyToKhai.Size = new System.Drawing.Size(113, 21);
            this.dtNgayDangKyToKhai.TabIndex = 6;
            // 
            // dtNgayTaoDuLieu
            // 
            // 
            // 
            // 
            this.dtNgayTaoDuLieu.DropDownCalendar.Name = "";
            this.dtNgayTaoDuLieu.Location = new System.Drawing.Point(384, 11);
            this.dtNgayTaoDuLieu.Name = "dtNgayTaoDuLieu";
            this.dtNgayTaoDuLieu.Size = new System.Drawing.Size(113, 21);
            this.dtNgayTaoDuLieu.TabIndex = 1;
            // 
            // txtSoToKhaiChiTiet
            // 
            this.txtSoToKhaiChiTiet.DecimalDigits = 0;
            this.txtSoToKhaiChiTiet.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoToKhaiChiTiet.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhaiChiTiet.Location = new System.Drawing.Point(114, 65);
            this.txtSoToKhaiChiTiet.MaxLength = 4;
            this.txtSoToKhaiChiTiet.Name = "txtSoToKhaiChiTiet";
            this.txtSoToKhaiChiTiet.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoToKhaiChiTiet.Size = new System.Drawing.Size(113, 21);
            this.txtSoToKhaiChiTiet.TabIndex = 5;
            this.txtSoToKhaiChiTiet.Text = "0";
            this.txtSoToKhaiChiTiet.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhaiChiTiet.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoToKhaiChiTiet.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoThuTuGiaoDich
            // 
            this.txtSoThuTuGiaoDich.DecimalDigits = 0;
            this.txtSoThuTuGiaoDich.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoThuTuGiaoDich.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoThuTuGiaoDich.Location = new System.Drawing.Point(114, 11);
            this.txtSoThuTuGiaoDich.MaxLength = 4;
            this.txtSoThuTuGiaoDich.Name = "txtSoThuTuGiaoDich";
            this.txtSoThuTuGiaoDich.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoThuTuGiaoDich.Size = new System.Drawing.Size(52, 21);
            this.txtSoThuTuGiaoDich.TabIndex = 0;
            this.txtSoThuTuGiaoDich.Text = "0";
            this.txtSoThuTuGiaoDich.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoThuTuGiaoDich.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoThuTuGiaoDich.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTienTangLen
            // 
            this.txtSoTienTangLen.DecimalDigits = 3;
            this.txtSoTienTangLen.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienTangLen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienTangLen.Location = new System.Drawing.Point(384, 38);
            this.txtSoTienTangLen.MaxLength = 13;
            this.txtSoTienTangLen.Name = "txtSoTienTangLen";
            this.txtSoTienTangLen.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienTangLen.Size = new System.Drawing.Size(113, 21);
            this.txtSoTienTangLen.TabIndex = 4;
            this.txtSoTienTangLen.Text = "0.000";
            this.txtSoTienTangLen.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienTangLen.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtSoTienTangLen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTienTruLui
            // 
            this.txtSoTienTruLui.DecimalDigits = 3;
            this.txtSoTienTruLui.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienTruLui.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienTruLui.Location = new System.Drawing.Point(114, 38);
            this.txtSoTienTruLui.MaxLength = 13;
            this.txtSoTienTruLui.Name = "txtSoTienTruLui";
            this.txtSoTienTruLui.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienTruLui.Size = new System.Drawing.Size(113, 21);
            this.txtSoTienTruLui.TabIndex = 3;
            this.txtSoTienTruLui.Text = "0.000";
            this.txtSoTienTruLui.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienTruLui.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtSoTienTruLui.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(244, 43);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(134, 13);
            this.label31.TabIndex = 5;
            this.label31.Text = "Số tiền tăng lên/ khôi phục";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(245, 16);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(86, 13);
            this.label29.TabIndex = 5;
            this.label29.Text = "Ngày tạo dữ liệu";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(244, 70);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(108, 13);
            this.label33.TabIndex = 5;
            this.label33.Text = "Ngày đăng ký tờ khai";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(10, 70);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(54, 13);
            this.label32.TabIndex = 5;
            this.label32.Text = "Số tờ khai";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(10, 43);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(71, 13);
            this.label30.TabIndex = 5;
            this.label30.Text = "Số tiền trừ lùi";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(10, 16);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(98, 13);
            this.label28.TabIndex = 5;
            this.label28.Text = "Số thứ tự giao dịch";
            // 
            // txtMaLoaiHinhChiTiet
            // 
            this.txtMaLoaiHinhChiTiet.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaLoaiHinhChiTiet.Location = new System.Drawing.Point(572, 62);
            this.txtMaLoaiHinhChiTiet.MaxLength = 255;
            this.txtMaLoaiHinhChiTiet.Name = "txtMaLoaiHinhChiTiet";
            this.txtMaLoaiHinhChiTiet.Size = new System.Drawing.Size(106, 21);
            this.txtMaLoaiHinhChiTiet.TabIndex = 7;
            this.txtMaLoaiHinhChiTiet.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaLoaiHinhChiTiet.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaLoaiHinhChiTiet.VisualStyleManager = this.vsmMain;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(503, 67);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(63, 13);
            this.label27.TabIndex = 8;
            this.label27.Text = "Mã loại hình";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(617, 509);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 27;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdKhaiBao,
            this.cmdLayPhanHoi});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("64392c4c-08e0-49a5-ab35-32992c7e6756");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.Tag = null;
            this.cmbMain.TopRebar = this.TopRebar1;
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 573);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(912, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmbMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdKhaiBao1,
            this.cmdLayPhanHoi1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(193, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdLayPhanHoi1
            // 
            this.cmdLayPhanHoi1.Key = "cmdLayPhanHoi";
            this.cmdLayPhanHoi1.Name = "cmdLayPhanHoi1";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Image = ((System.Drawing.Image)(resources.GetObject("cmdKhaiBao.Image")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdLayPhanHoi
            // 
            this.cmdLayPhanHoi.Image = ((System.Drawing.Image)(resources.GetObject("cmdLayPhanHoi.Image")));
            this.cmdLayPhanHoi.Key = "cmdLayPhanHoi";
            this.cmdLayPhanHoi.Name = "cmdLayPhanHoi";
            this.cmdLayPhanHoi.Text = "Lấy phản hồi";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 28);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 545);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(912, 28);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 545);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmbMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(912, 28);
            // 
            // VNACC_ChungTuThanhToanForm_IAS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 573);
            this.Controls.Add(this.LeftRebar1);
            this.Controls.Add(this.RightRebar1);
            this.Controls.Add(this.TopRebar1);
            this.Controls.Add(this.BottomRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_ChungTuThanhToanForm_IAS";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Chứng từ thanh toán (IAS: Reference of security infomation)";
            this.Controls.SetChildIndex(this.BottomRebar1, 0);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.RightRebar1, 0);
            this.Controls.SetChildIndex(this.LeftRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            this.uiTabPage1.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucLoaiHinhBaoLanh;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNganHangCungCapBaoLanh;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNganHangCungCapBaoLanh;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamPhatHanh;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoChungTuPhatHanhBaoLanh;
        private Janus.Windows.GridEX.EditControls.EditBox txtKiHieuChungTuPhatHanhBaoLanh;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayDuDinhKhaiBao;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai;
        private Janus.Windows.GridEX.EditControls.EditBox txtCoQuanHaiQuan;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaLoaiHinh;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCucHaiQuan;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon_4;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon_2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHoaDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon_5;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon_3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon_1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDonViDaiDienSuDungBaoLanh;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDonViSuDungBaoLanh;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDonViDaiDienSuDungBaoLanh;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDonViSuDungBaoLanh;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayHetHieuLuc;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayBatDauHieuLuc;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienDangKyBaoLanh;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaTienTe;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoDuBaoLanh;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.GridEX.EditControls.EditBox txtCoBaoVohieu;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaLoaiHinhChiTiet;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.CalendarCombo.CalendarCombo dtThoiGianTaoDuLieu;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayTaoDuLieu;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoThuTuGiaoDich;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienTruLui;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayDangKyToKhai;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhaiChiTiet;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienTangLen;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private Janus.Windows.GridEX.EditControls.EditBox txtCoQuanhaiQuanChiTiet;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCucHaiQuanChiTiet;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.GridEX.GridEX grdHang;
        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLayPhanHoi1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLayPhanHoi;
    }
}