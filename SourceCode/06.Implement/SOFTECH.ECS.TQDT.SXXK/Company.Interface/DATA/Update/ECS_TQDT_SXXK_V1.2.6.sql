/*
Run this script on:

        ecsteam.ECS_TQDT_SXXK_VERSION    -  This database will be modified

to synchronize it with:

        ecsteam.ECS_TQDT_SXXK

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 11/07/2011 12:35:56 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[t_CTTT_ChiPhiKhac]'
GO
CREATE TABLE [dbo].[t_CTTT_ChiPhiKhac]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[IDCT] [int] NOT NULL,
[MaChiPhi] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TriGia] [numeric] (18, 15) NOT NULL,
[MaToanTu] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GhiChu] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_CTTT_ChiPhiKhac] on [dbo].[t_CTTT_ChiPhiKhac]'
GO
ALTER TABLE [dbo].[t_CTTT_ChiPhiKhac] ADD CONSTRAINT [PK_t_CTTT_ChiPhiKhac] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChiPhiKhac_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_Insert]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Insert]
	@IDCT int,
	@MaChiPhi varchar(10),
	@TriGia numeric(18, 15),
	@MaToanTu varchar(50),
	@GhiChu nvarchar(150),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_CTTT_ChiPhiKhac]
(
	[IDCT],
	[MaChiPhi],
	[TriGia],
	[MaToanTu],
	[GhiChu]
)
VALUES 
(
	@IDCT,
	@MaChiPhi,
	@TriGia,
	@MaToanTu,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChiPhiKhac_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_Update]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Update]
	@ID int,
	@IDCT int,
	@MaChiPhi varchar(10),
	@TriGia numeric(18, 15),
	@MaToanTu varchar(50),
	@GhiChu nvarchar(150)
AS

UPDATE
	[dbo].[t_CTTT_ChiPhiKhac]
SET
	[IDCT] = @IDCT,
	[MaChiPhi] = @MaChiPhi,
	[TriGia] = @TriGia,
	[MaToanTu] = @MaToanTu,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChiPhiKhac_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_InsertUpdate]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_InsertUpdate]
	@ID int,
	@IDCT int,
	@MaChiPhi varchar(10),
	@TriGia numeric(18, 15),
	@MaToanTu varchar(50),
	@GhiChu nvarchar(150)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_CTTT_ChiPhiKhac] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_CTTT_ChiPhiKhac] 
		SET
			[IDCT] = @IDCT,
			[MaChiPhi] = @MaChiPhi,
			[TriGia] = @TriGia,
			[MaToanTu] = @MaToanTu,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_CTTT_ChiPhiKhac]
		(
			[IDCT],
			[MaChiPhi],
			[TriGia],
			[MaToanTu],
			[GhiChu]
		)
		VALUES 
		(
			@IDCT,
			@MaChiPhi,
			@TriGia,
			@MaToanTu,
			@GhiChu
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChiPhiKhac_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_Delete]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_CTTT_ChiPhiKhac]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChiPhiKhac_DeleteBy_IDCT]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_DeleteBy_IDCT]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_DeleteBy_IDCT]
	@IDCT int
AS

DELETE FROM [dbo].[t_CTTT_ChiPhiKhac]
WHERE
	[IDCT] = @IDCT

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChiPhiKhac_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_Load]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDCT],
	[MaChiPhi],
	[TriGia],
	[MaToanTu],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChiPhiKhac]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChiPhiKhac_SelectBy_IDCT]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_SelectBy_IDCT]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_SelectBy_IDCT]
	@IDCT int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDCT],
	[MaChiPhi],
	[TriGia],
	[MaToanTu],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChiPhiKhac]
WHERE
	[IDCT] = @IDCT

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChiPhiKhac_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_SelectAll]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDCT],
	[MaChiPhi],
	[TriGia],
	[MaToanTu],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChiPhiKhac]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_CTTT_ChungTu]'
GO
CREATE TABLE [dbo].[t_CTTT_ChungTu]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[LanThanhLy] [int] NOT NULL,
[SoChungTu] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayChungTu] [datetime] NOT NULL,
[HinhThucThanhToan] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TongTriGia] [numeric] (18, 5) NOT NULL,
[ConLai] [numeric] (18, 5) NOT NULL,
[GhiChu] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_CTTT_ChungTu] on [dbo].[t_CTTT_ChungTu]'
GO
ALTER TABLE [dbo].[t_CTTT_ChungTu] ADD CONSTRAINT [PK_t_CTTT_ChungTu] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTu_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_Insert]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_Insert]
	@LanThanhLy int,
	@SoChungTu nvarchar(150),
	@NgayChungTu datetime,
	@HinhThucThanhToan varchar(10),
	@TongTriGia numeric(18, 5),
	@ConLai numeric(18, 5),
	@GhiChu nvarchar(150),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_CTTT_ChungTu]
(
	[LanThanhLy],
	[SoChungTu],
	[NgayChungTu],
	[HinhThucThanhToan],
	[TongTriGia],
	[ConLai],
	[GhiChu]
)
VALUES 
(
	@LanThanhLy,
	@SoChungTu,
	@NgayChungTu,
	@HinhThucThanhToan,
	@TongTriGia,
	@ConLai,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTu_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_Update]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_Update]
	@ID int,
	@LanThanhLy int,
	@SoChungTu nvarchar(150),
	@NgayChungTu datetime,
	@HinhThucThanhToan varchar(10),
	@TongTriGia numeric(18, 5),
	@ConLai numeric(18, 5),
	@GhiChu nvarchar(150)
AS

UPDATE
	[dbo].[t_CTTT_ChungTu]
SET
	[LanThanhLy] = @LanThanhLy,
	[SoChungTu] = @SoChungTu,
	[NgayChungTu] = @NgayChungTu,
	[HinhThucThanhToan] = @HinhThucThanhToan,
	[TongTriGia] = @TongTriGia,
	[ConLai] = @ConLai,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTu_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_InsertUpdate]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_InsertUpdate]
	@ID int,
	@LanThanhLy int,
	@SoChungTu nvarchar(150),
	@NgayChungTu datetime,
	@HinhThucThanhToan varchar(10),
	@TongTriGia numeric(18, 5),
	@ConLai numeric(18, 5),
	@GhiChu nvarchar(150)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_CTTT_ChungTu] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_CTTT_ChungTu] 
		SET
			[LanThanhLy] = @LanThanhLy,
			[SoChungTu] = @SoChungTu,
			[NgayChungTu] = @NgayChungTu,
			[HinhThucThanhToan] = @HinhThucThanhToan,
			[TongTriGia] = @TongTriGia,
			[ConLai] = @ConLai,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_CTTT_ChungTu]
		(
			[LanThanhLy],
			[SoChungTu],
			[NgayChungTu],
			[HinhThucThanhToan],
			[TongTriGia],
			[ConLai],
			[GhiChu]
		)
		VALUES 
		(
			@LanThanhLy,
			@SoChungTu,
			@NgayChungTu,
			@HinhThucThanhToan,
			@TongTriGia,
			@ConLai,
			@GhiChu
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTu_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_Delete]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_CTTT_ChungTu]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTu_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_Load]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LanThanhLy],
	[SoChungTu],
	[NgayChungTu],
	[HinhThucThanhToan],
	[TongTriGia],
	[ConLai],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChungTu]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTu_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_SelectAll]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LanThanhLy],
	[SoChungTu],
	[NgayChungTu],
	[HinhThucThanhToan],
	[TongTriGia],
	[ConLai],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChungTu]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_CTTT_ChungTuChiTiet]'
GO
CREATE TABLE [dbo].[t_CTTT_ChungTuChiTiet]
(
[IDChiTiet] [int] NOT NULL IDENTITY(1, 1),
[IDCT] [int] NOT NULL,
[SoToKhai] [int] NOT NULL,
[MaLoaiHinh] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayDangKy] [datetime] NOT NULL,
[TriGia] [numeric] (18, 5) NOT NULL,
[GhiChu] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_CTTT_ChungTuChiTiet] on [dbo].[t_CTTT_ChungTuChiTiet]'
GO
ALTER TABLE [dbo].[t_CTTT_ChungTuChiTiet] ADD CONSTRAINT [PK_t_CTTT_ChungTuChiTiet] PRIMARY KEY CLUSTERED  ([IDChiTiet])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTuChiTiet_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_Insert]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_Insert]
	@IDCT int,
	@SoToKhai int,
	@MaLoaiHinh varchar(5),
	@NgayDangKy datetime,
	@TriGia numeric(18, 5),
	@GhiChu nvarchar(150),
	@IDChiTiet int OUTPUT
AS

INSERT INTO [dbo].[t_CTTT_ChungTuChiTiet]
(
	[IDCT],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[TriGia],
	[GhiChu]
)
VALUES 
(
	@IDCT,
	@SoToKhai,
	@MaLoaiHinh,
	@NgayDangKy,
	@TriGia,
	@GhiChu
)

SET @IDChiTiet = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTuChiTiet_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_Update]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_Update]
	@IDChiTiet int,
	@IDCT int,
	@SoToKhai int,
	@MaLoaiHinh varchar(5),
	@NgayDangKy datetime,
	@TriGia numeric(18, 5),
	@GhiChu nvarchar(150)
AS

UPDATE
	[dbo].[t_CTTT_ChungTuChiTiet]
SET
	[IDCT] = @IDCT,
	[SoToKhai] = @SoToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[TriGia] = @TriGia,
	[GhiChu] = @GhiChu
WHERE
	[IDChiTiet] = @IDChiTiet

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTuChiTiet_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_InsertUpdate]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_InsertUpdate]
	@IDChiTiet int,
	@IDCT int,
	@SoToKhai int,
	@MaLoaiHinh varchar(5),
	@NgayDangKy datetime,
	@TriGia numeric(18, 5),
	@GhiChu nvarchar(150)
AS
IF EXISTS(SELECT [IDChiTiet] FROM [dbo].[t_CTTT_ChungTuChiTiet] WHERE [IDChiTiet] = @IDChiTiet)
	BEGIN
		UPDATE
			[dbo].[t_CTTT_ChungTuChiTiet] 
		SET
			[IDCT] = @IDCT,
			[SoToKhai] = @SoToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[TriGia] = @TriGia,
			[GhiChu] = @GhiChu
		WHERE
			[IDChiTiet] = @IDChiTiet
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_CTTT_ChungTuChiTiet]
		(
			[IDCT],
			[SoToKhai],
			[MaLoaiHinh],
			[NgayDangKy],
			[TriGia],
			[GhiChu]
		)
		VALUES 
		(
			@IDCT,
			@SoToKhai,
			@MaLoaiHinh,
			@NgayDangKy,
			@TriGia,
			@GhiChu
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTuChiTiet_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_Delete]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_Delete]
	@IDChiTiet int
AS

DELETE FROM 
	[dbo].[t_CTTT_ChungTuChiTiet]
WHERE
	[IDChiTiet] = @IDChiTiet

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTuChiTiet_DeleteBy_IDCT]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_DeleteBy_IDCT]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_DeleteBy_IDCT]
	@IDCT int
AS

DELETE FROM [dbo].[t_CTTT_ChungTuChiTiet]
WHERE
	[IDCT] = @IDCT

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTuChiTiet_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_Load]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_Load]
	@IDChiTiet int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[IDChiTiet],
	[IDCT],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[TriGia],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChungTuChiTiet]
WHERE
	[IDChiTiet] = @IDChiTiet
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTuChiTiet_SelectBy_IDCT]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_SelectBy_IDCT]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_SelectBy_IDCT]
	@IDCT int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[IDChiTiet],
	[IDCT],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[TriGia],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChungTuChiTiet]
WHERE
	[IDCT] = @IDCT

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTuChiTiet_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_SelectAll]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[IDChiTiet],
	[IDCT],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[TriGia],
	[GhiChu]
FROM
	[dbo].[t_CTTT_ChungTuChiTiet]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_Insert]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_Insert]
	@TKMD_ID bigint,
	@TKMD_Ref nvarchar(150),
	@SoQuyetDinh nvarchar(50),
	@NgayQuyetDinh datetime,
	@NgayHetHan datetime,
	@TaiKhoanKhoBac varchar(50),
	@TenKhoBac nvarchar(150),
	@GhiChu nvarchar(250),
	@ID int OUTPUT
AS
INSERT INTO [dbo].[t_KDT_AnDinhThue]
(
	[TKMD_ID],
	[TKMD_Ref],
	[SoQuyetDinh],
	[NgayQuyetDinh],
	[NgayHetHan],
	[TaiKhoanKhoBac],
	[TenKhoBac],
	[GhiChu]
)
VALUES 
(
	@TKMD_ID,
	@TKMD_Ref,
	@SoQuyetDinh,
	@NgayQuyetDinh,
	@NgayHetHan,
	@TaiKhoanKhoBac,
	@TenKhoBac,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_Update]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_Update]
	@ID int,
	@TKMD_ID bigint,
	@TKMD_Ref nvarchar(150),
	@SoQuyetDinh nvarchar(50),
	@NgayQuyetDinh datetime,
	@NgayHetHan datetime,
	@TaiKhoanKhoBac varchar(50),
	@TenKhoBac nvarchar(150),
	@GhiChu nvarchar(250)
AS

UPDATE
	[dbo].[t_KDT_AnDinhThue]
SET
	[TKMD_ID] = @TKMD_ID,
	[TKMD_Ref] = @TKMD_Ref,
	[SoQuyetDinh] = @SoQuyetDinh,
	[NgayQuyetDinh] = @NgayQuyetDinh,
	[NgayHetHan] = @NgayHetHan,
	[TaiKhoanKhoBac] = @TaiKhoanKhoBac,
	[TenKhoBac] = @TenKhoBac,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_InsertUpdate]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_InsertUpdate]
	@ID int,
	@TKMD_ID bigint,
	@TKMD_Ref nvarchar(150),
	@SoQuyetDinh nvarchar(50),
	@NgayQuyetDinh datetime,
	@NgayHetHan datetime,
	@TaiKhoanKhoBac varchar(50),
	@TenKhoBac nvarchar(150),
	@GhiChu nvarchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_AnDinhThue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_AnDinhThue] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[TKMD_Ref] = @TKMD_Ref,
			[SoQuyetDinh] = @SoQuyetDinh,
			[NgayQuyetDinh] = @NgayQuyetDinh,
			[NgayHetHan] = @NgayHetHan,
			[TaiKhoanKhoBac] = @TaiKhoanKhoBac,
			[TenKhoBac] = @TenKhoBac,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_AnDinhThue]
		(
			[TKMD_ID],
			[TKMD_Ref],
			[SoQuyetDinh],
			[NgayQuyetDinh],
			[NgayHetHan],
			[TaiKhoanKhoBac],
			[TenKhoBac],
			[GhiChu]
		)
		VALUES 
		(
			@TKMD_ID,
			@TKMD_Ref,
			@SoQuyetDinh,
			@NgayQuyetDinh,
			@NgayHetHan,
			@TaiKhoanKhoBac,
			@TenKhoBac,
			@GhiChu
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_Delete]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_KDT_AnDinhThue]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_DeleteBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_DeleteBy_TKMD_ID]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_AnDinhThue]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_Load]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[TKMD_Ref],
	[SoQuyetDinh],
	[NgayQuyetDinh],
	[NgayHetHan],
	[TaiKhoanKhoBac],
	[TenKhoBac],
	[GhiChu]
FROM
	[dbo].[t_KDT_AnDinhThue]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_SelectBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_SelectBy_TKMD_ID]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[TKMD_Ref],
	[SoQuyetDinh],
	[NgayQuyetDinh],
	[NgayHetHan],
	[TaiKhoanKhoBac],
	[TenKhoBac],
	[GhiChu]
FROM
	[dbo].[t_KDT_AnDinhThue]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_SelectAll]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[TKMD_Ref],
	[SoQuyetDinh],
	[NgayQuyetDinh],
	[NgayHetHan],
	[TaiKhoanKhoBac],
	[TenKhoBac],
	[GhiChu]
FROM
	[dbo].[t_KDT_AnDinhThue]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_Insert]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_Insert]
	@IDAnDinhThue int,
	@SacThue char(10),
	@TienThue numeric(18, 4),
	@Chuong varchar(10),
	@Loai int,
	@Khoan int,
	@Muc int,
	@TieuMuc int,
	@GhiChu nvarchar(150),
	@ID int OUTPUT
AS
INSERT INTO [dbo].[t_KDT_AnDinhThue_ChiTiet]
(
	[IDAnDinhThue],
	[SacThue],
	[TienThue],
	[Chuong],
	[Loai],
	[Khoan],
	[Muc],
	[TieuMuc],
	[GhiChu]
)
VALUES 
(
	@IDAnDinhThue,
	@SacThue,
	@TienThue,
	@Chuong,
	@Loai,
	@Khoan,
	@Muc,
	@TieuMuc,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_Update]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_Update]
	@ID int,
	@IDAnDinhThue int,
	@SacThue char(10),
	@TienThue numeric(18, 4),
	@Chuong varchar(10),
	@Loai int,
	@Khoan int,
	@Muc int,
	@TieuMuc int,
	@GhiChu nvarchar(150)
AS

UPDATE
	[dbo].[t_KDT_AnDinhThue_ChiTiet]
SET
	[IDAnDinhThue] = @IDAnDinhThue,
	[SacThue] = @SacThue,
	[TienThue] = @TienThue,
	[Chuong] = @Chuong,
	[Loai] = @Loai,
	[Khoan] = @Khoan,
	[Muc] = @Muc,
	[TieuMuc] = @TieuMuc,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_InsertUpdate]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_InsertUpdate]
	@ID int,
	@IDAnDinhThue int,
	@SacThue char(10),
	@TienThue numeric(18, 4),
	@Chuong varchar(10),
	@Loai int,
	@Khoan int,
	@Muc int,
	@TieuMuc int,
	@GhiChu nvarchar(150)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_AnDinhThue_ChiTiet] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_AnDinhThue_ChiTiet] 
		SET
			[IDAnDinhThue] = @IDAnDinhThue,
			[SacThue] = @SacThue,
			[TienThue] = @TienThue,
			[Chuong] = @Chuong,
			[Loai] = @Loai,
			[Khoan] = @Khoan,
			[Muc] = @Muc,
			[TieuMuc] = @TieuMuc,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_AnDinhThue_ChiTiet]
		(
			[IDAnDinhThue],
			[SacThue],
			[TienThue],
			[Chuong],
			[Loai],
			[Khoan],
			[Muc],
			[TieuMuc],
			[GhiChu]
		)
		VALUES 
		(
			@IDAnDinhThue,
			@SacThue,
			@TienThue,
			@Chuong,
			@Loai,
			@Khoan,
			@Muc,
			@TieuMuc,
			@GhiChu
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_Delete]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_KDT_AnDinhThue_ChiTiet]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_DeleteBy_IDAnDinhThue]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_DeleteBy_IDAnDinhThue]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_DeleteBy_IDAnDinhThue]
	@IDAnDinhThue int
AS

DELETE FROM [dbo].[t_KDT_AnDinhThue_ChiTiet]
WHERE
	[IDAnDinhThue] = @IDAnDinhThue

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_Load]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDAnDinhThue],
	[SacThue],
	[TienThue],
	[Chuong],
	[Loai],
	[Khoan],
	[Muc],
	[TieuMuc],
	[GhiChu]
FROM
	[dbo].[t_KDT_AnDinhThue_ChiTiet]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectBy_IDAnDinhThue]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectBy_IDAnDinhThue]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectBy_IDAnDinhThue]
	@IDAnDinhThue int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDAnDinhThue],
	[SacThue],
	[TienThue],
	[Chuong],
	[Loai],
	[Khoan],
	[Muc],
	[TieuMuc],
	[GhiChu]
FROM
	[dbo].[t_KDT_AnDinhThue_ChiTiet]
WHERE
	[IDAnDinhThue] = @IDAnDinhThue

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectAll]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDAnDinhThue],
	[SacThue],
	[TienThue],
	[Chuong],
	[Loai],
	[Khoan],
	[Muc],
	[TieuMuc],
	[GhiChu]
FROM
	[dbo].[t_KDT_AnDinhThue_ChiTiet]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_InsertUpdateBy]'
GO
   
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_InsertUpdate]  
-- Database: ECS_TQ_SXXK_CTTT  
-- Author: Ngo Thanh Tung  
-- Time created: Sunday, November 06, 2011  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_InsertUpdateBy]  
 @ID int,  
 @IDAnDinhThue int,  
 @SacThue char(10),  
 @TienThue numeric(18, 4),  
 @Chuong varchar(10),  
 @Loai int,  
 @Khoan int,  
 @Muc int,  
 @TieuMuc int,  
 @GhiChu nvarchar(150)  
AS  
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_AnDinhThue_ChiTiet] WHERE [IDAnDinhThue] = @IDAnDinhThue AND SacThue = @SacThue)  
 BEGIN  
  UPDATE  
   [dbo].[t_KDT_AnDinhThue_ChiTiet]   
  SET  
   [IDAnDinhThue] = @IDAnDinhThue,  
   [SacThue] = @SacThue,  
   [TienThue] = @TienThue,  
   [Chuong] = @Chuong,  
   [Loai] = @Loai,  
   [Khoan] = @Khoan,  
   [Muc] = @Muc,  
   [TieuMuc] = @TieuMuc,  
   [GhiChu] = @GhiChu  
  WHERE  
   [IDAnDinhThue] = @IDAnDinhThue AND SacThue = @SacThue
 END  
ELSE  
 BEGIN  
    
  INSERT INTO [dbo].[t_KDT_AnDinhThue_ChiTiet]  
  (  
   [IDAnDinhThue],  
   [SacThue],  
   [TienThue],  
   [Chuong],  
   [Loai],  
   [Khoan],  
   [Muc],  
   [TieuMuc],  
   [GhiChu]  
  )  
  VALUES   
  (  
   @IDAnDinhThue,  
   @SacThue,  
   @TienThue,  
   @Chuong,  
   @Loai,  
   @Khoan,  
   @Muc,  
   @TieuMuc,  
   @GhiChu  
  )    
 END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]'
GO
CREATE TABLE [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]
(
[ID] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_LoaiPhiChungTuThanhToan] on [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]'
GO
ALTER TABLE [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan] ADD CONSTRAINT [PK_t_HaiQuan_LoaiPhiChungTuThanhToan] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChiPhiKhac_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_DeleteDynamic]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_CTTT_ChiPhiKhac] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChiPhiKhac_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_SelectDynamic]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChiPhiKhac_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[IDCT],
	[MaChiPhi],
	[TriGia],
	[MaToanTu],
	[GhiChu]
FROM [dbo].[t_CTTT_ChiPhiKhac] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTu_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_DeleteDynamic]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_CTTT_ChungTu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTu_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTu_SelectDynamic]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LanThanhLy],
	[SoChungTu],
	[NgayChungTu],
	[HinhThucThanhToan],
	[TongTriGia],
	[ConLai],
	[GhiChu]
FROM [dbo].[t_CTTT_ChungTu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTuChiTiet_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_DeleteDynamic]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_CTTT_ChungTuChiTiet] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_ChungTuChiTiet_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChungTuChiTiet_SelectDynamic]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_CTTT_ChungTuChiTiet_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[IDChiTiet],
	[IDCT],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[TriGia],
	[GhiChu]
FROM [dbo].[t_CTTT_ChungTuChiTiet] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_DeleteDynamic]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_AnDinhThue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_SelectDynamic]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[TKMD_Ref],
	[SoQuyetDinh],
	[NgayQuyetDinh],
	[NgayHetHan],
	[TaiKhoanKhoBac],
	[TenKhoBac],
	[GhiChu]
FROM [dbo].[t_KDT_AnDinhThue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_DeleteDynamic]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_AnDinhThue_ChiTiet] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectDynamic]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[IDAnDinhThue],
	[SacThue],
	[TienThue],
	[Chuong],
	[Loai],
	[Khoan],
	[Muc],
	[TieuMuc],
	[GhiChu]
FROM [dbo].[t_KDT_AnDinhThue_ChiTiet] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_CTTT_ChiPhiKhac]'
GO
ALTER TABLE [dbo].[t_CTTT_ChiPhiKhac] ADD
CONSTRAINT [FK_t_CTTT_ChiPhiKhac_t_CTTT_ChungTu] FOREIGN KEY ([IDCT]) REFERENCES [dbo].[t_CTTT_ChungTu] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_CTTT_ChungTuChiTiet]'
GO
ALTER TABLE [dbo].[t_CTTT_ChungTuChiTiet] ADD
CONSTRAINT [FK_t_CTTT_ChungTuChiTiet_t_CTTT_ChungTu] FOREIGN KEY ([IDCT]) REFERENCES [dbo].[t_CTTT_ChungTu] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

UPDATE    dbo.t_HaiQuan_Version SET [Version] ='1.2.6', Date = getdate()