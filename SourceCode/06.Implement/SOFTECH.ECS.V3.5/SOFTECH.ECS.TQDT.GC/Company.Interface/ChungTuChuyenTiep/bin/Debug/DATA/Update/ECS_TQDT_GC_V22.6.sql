﻿-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_NPLQuyetToan_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_NPLQuyetToan_Insert]

IF OBJECT_ID(N'[dbo].[p_NPLQuyetToan_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_NPLQuyetToan_Update]

IF OBJECT_ID(N'[dbo].[p_NPLQuyetToan_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_NPLQuyetToan_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_NPLQuyetToan_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_NPLQuyetToan_Delete]

IF OBJECT_ID(N'[dbo].[p_NPLQuyetToan_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_NPLQuyetToan_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_NPLQuyetToan_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_NPLQuyetToan_Load]

IF OBJECT_ID(N'[dbo].[p_NPLQuyetToan_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_NPLQuyetToan_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_NPLQuyetToan_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_NPLQuyetToan_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_Insert]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 3, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_Insert]
	@HopDong_ID int,
	@SoHopDong varchar(50),
	@SoToKhai numeric(18, 0),
	@MaLoaiHinh varchar(5),
	@NgayDangKy datetime,
	@MaHang varchar(50),
	@TenHang nvarchar(150),
	@SoLuong numeric(24, 4),
	@MaNguyenPhuLieu varchar(50),
	@DinhMucSuDung numeric(24, 6),
	@TyLeHaoHut numeric(24, 6),
	@LuongNPLChuaHH numeric(24, 6),
	@LuongNPLCoHH numeric(24, 6),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_NPLQuyetToan]
(
	[HopDong_ID],
	[SoHopDong],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHang],
	[TenHang],
	[SoLuong],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[LuongNPLChuaHH],
	[LuongNPLCoHH]
)
VALUES 
(
	@HopDong_ID,
	@SoHopDong,
	@SoToKhai,
	@MaLoaiHinh,
	@NgayDangKy,
	@MaHang,
	@TenHang,
	@SoLuong,
	@MaNguyenPhuLieu,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@LuongNPLChuaHH,
	@LuongNPLCoHH
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_Update]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 3, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_Update]
	@ID bigint,
	@HopDong_ID int,
	@SoHopDong varchar(50),
	@SoToKhai numeric(18, 0),
	@MaLoaiHinh varchar(5),
	@NgayDangKy datetime,
	@MaHang varchar(50),
	@TenHang nvarchar(150),
	@SoLuong numeric(24, 4),
	@MaNguyenPhuLieu varchar(50),
	@DinhMucSuDung numeric(24, 6),
	@TyLeHaoHut numeric(24, 6),
	@LuongNPLChuaHH numeric(24, 6),
	@LuongNPLCoHH numeric(24, 6)
AS

UPDATE
	[dbo].[t_KDT_GC_NPLQuyetToan]
SET
	[HopDong_ID] = @HopDong_ID,
	[SoHopDong] = @SoHopDong,
	[SoToKhai] = @SoToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[MaHang] = @MaHang,
	[TenHang] = @TenHang,
	[SoLuong] = @SoLuong,
	[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[LuongNPLChuaHH] = @LuongNPLChuaHH,
	[LuongNPLCoHH] = @LuongNPLCoHH
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 3, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_InsertUpdate]
	@ID bigint,
	@HopDong_ID int,
	@SoHopDong varchar(50),
	@SoToKhai numeric(18, 0),
	@MaLoaiHinh varchar(5),
	@NgayDangKy datetime,
	@MaHang varchar(50),
	@TenHang nvarchar(150),
	@SoLuong numeric(24, 4),
	@MaNguyenPhuLieu varchar(50),
	@DinhMucSuDung numeric(24, 6),
	@TyLeHaoHut numeric(24, 6),
	@LuongNPLChuaHH numeric(24, 6),
	@LuongNPLCoHH numeric(24, 6)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_NPLQuyetToan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_NPLQuyetToan] 
		SET
			[HopDong_ID] = @HopDong_ID,
			[SoHopDong] = @SoHopDong,
			[SoToKhai] = @SoToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[MaHang] = @MaHang,
			[TenHang] = @TenHang,
			[SoLuong] = @SoLuong,
			[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[LuongNPLChuaHH] = @LuongNPLChuaHH,
			[LuongNPLCoHH] = @LuongNPLCoHH
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_NPLQuyetToan]
		(
			[HopDong_ID],
			[SoHopDong],
			[SoToKhai],
			[MaLoaiHinh],
			[NgayDangKy],
			[MaHang],
			[TenHang],
			[SoLuong],
			[MaNguyenPhuLieu],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[LuongNPLChuaHH],
			[LuongNPLCoHH]
		)
		VALUES 
		(
			@HopDong_ID,
			@SoHopDong,
			@SoToKhai,
			@MaLoaiHinh,
			@NgayDangKy,
			@MaHang,
			@TenHang,
			@SoLuong,
			@MaNguyenPhuLieu,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@LuongNPLChuaHH,
			@LuongNPLCoHH
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 3, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_NPLQuyetToan]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 3, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_NPLQuyetToan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_Load]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 3, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[SoHopDong],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHang],
	[TenHang],
	[SoLuong],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[LuongNPLChuaHH],
	[LuongNPLCoHH]
FROM
	[dbo].[t_KDT_GC_NPLQuyetToan]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 3, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HopDong_ID],
	[SoHopDong],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHang],
	[TenHang],
	[SoLuong],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[LuongNPLChuaHH],
	[LuongNPLCoHH]
FROM [dbo].[t_KDT_GC_NPLQuyetToan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLQuyetToan_SelectAll]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 3, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLQuyetToan_SelectAll]


AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[SoHopDong],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHang],
	[TenHang],
	[SoLuong],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[LuongNPLChuaHH],
	[LuongNPLCoHH]
FROM
	[dbo].[t_KDT_GC_NPLQuyetToan]	

GO


GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '22.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('22.6',GETDATE(), N'Fix lại NPLQuyetToan')
END	