-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_Insert]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_Update]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_Delete]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_Load]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_SelectAll]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_DeleteBy_HopDong_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_Insert]
	@HopDong_ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@STTHang int,
	@NPL_TuCungUng numeric(18, 6),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255),
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@GuidString nvarchar(50),
	@LenhSanXuat_ID bigint
AS
INSERT INTO [dbo].[t_GC_DinhMuc]
(
	[HopDong_ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[STTHang],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[LenhSanXuat_ID]
)
VALUES
(
	@HopDong_ID,
	@MaSanPham,
	@MaNguyenPhuLieu,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@STTHang,
	@NPL_TuCungUng,
	@TenSanPham,
	@TenNPL,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThaiXuLy,
	@GuidString,
	@LenhSanXuat_ID
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_Update]
	@HopDong_ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@STTHang int,
	@NPL_TuCungUng numeric(18, 6),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255),
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@GuidString nvarchar(50),
	@LenhSanXuat_ID bigint
AS

UPDATE
	[dbo].[t_GC_DinhMuc]
SET
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[STTHang] = @STTHang,
	[NPL_TuCungUng] = @NPL_TuCungUng,
	[TenSanPham] = @TenSanPham,
	[TenNPL] = @TenNPL,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[GuidString] = @GuidString
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
	AND [LenhSanXuat_ID] = @LenhSanXuat_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_InsertUpdate]
	@HopDong_ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@STTHang int,
	@NPL_TuCungUng numeric(18, 6),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255),
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@GuidString nvarchar(50),
	@LenhSanXuat_ID bigint
AS
IF EXISTS(SELECT [HopDong_ID], [MaSanPham], [MaNguyenPhuLieu], [LenhSanXuat_ID] FROM [dbo].[t_GC_DinhMuc] WHERE [HopDong_ID] = @HopDong_ID AND [MaSanPham] = @MaSanPham AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu AND [LenhSanXuat_ID] = @LenhSanXuat_ID)
	BEGIN
		UPDATE
			[dbo].[t_GC_DinhMuc] 
		SET
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[STTHang] = @STTHang,
			[NPL_TuCungUng] = @NPL_TuCungUng,
			[TenSanPham] = @TenSanPham,
			[TenNPL] = @TenNPL,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[GuidString] = @GuidString
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [MaSanPham] = @MaSanPham
			AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
			AND [LenhSanXuat_ID] = @LenhSanXuat_ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_GC_DinhMuc]
	(
			[HopDong_ID],
			[MaSanPham],
			[MaNguyenPhuLieu],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[STTHang],
			[NPL_TuCungUng],
			[TenSanPham],
			[TenNPL],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThaiXuLy],
			[GuidString],
			[LenhSanXuat_ID]
	)
	VALUES
	(
			@HopDong_ID,
			@MaSanPham,
			@MaNguyenPhuLieu,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@STTHang,
			@NPL_TuCungUng,
			@TenSanPham,
			@TenNPL,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThaiXuLy,
			@GuidString,
			@LenhSanXuat_ID
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_Delete]
	@HopDong_ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@LenhSanXuat_ID bigint
AS

DELETE FROM 
	[dbo].[t_GC_DinhMuc]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
	AND [LenhSanXuat_ID] = @LenhSanXuat_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_GC_DinhMuc]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_GC_DinhMuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_Load]
	@HopDong_ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@LenhSanXuat_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[STTHang],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[LenhSanXuat_ID]
FROM
	[dbo].[t_GC_DinhMuc]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
	AND [LenhSanXuat_ID] = @LenhSanXuat_ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[STTHang],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[LenhSanXuat_ID]
FROM
	[dbo].[t_GC_DinhMuc]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[HopDong_ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[STTHang],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[LenhSanXuat_ID]
FROM [dbo].[t_GC_DinhMuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[STTHang],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[LenhSanXuat_ID]
FROM
	[dbo].[t_GC_DinhMuc]	

GO
IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_Update_LenhSanXuat]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_Update_LenhSanXuat]
GO
CREATE PROCEDURE [dbo].[p_GC_DinhMuc_Update_LenhSanXuat]    
 @HopDong_ID bigint,    
 @MaSanPham varchar(50),    
 @MaNguyenPhuLieu nvarchar(50),    
 @DinhMucSuDung numeric(18, 8),    
 @TyLeHaoHut numeric(18, 8),    
 @STTHang int,    
 @NPL_TuCungUng numeric(18, 6),    
 @TenSanPham nvarchar(255),    
 @TenNPL nvarchar(255),    
 @SoTiepNhan numeric(18, 0),    
 @NgayTiepNhan datetime,    
 @TrangThaiXuLy int,    
 @GuidString nvarchar(50),    
 @LenhSanXuat_ID bigint    
AS    
    
UPDATE    
 [dbo].[t_GC_DinhMuc]    
SET    
 [DinhMucSuDung] = @DinhMucSuDung,    
 [TyLeHaoHut] = @TyLeHaoHut,    
 [STTHang] = @STTHang,    
 [NPL_TuCungUng] = @NPL_TuCungUng,    
 [TenSanPham] = @TenSanPham,    
 [TenNPL] = @TenNPL,    
 [SoTiepNhan] = @SoTiepNhan,    
 [NgayTiepNhan] = @NgayTiepNhan,    
 [TrangThaiXuLy] = @TrangThaiXuLy,    
 [GuidString] = @GuidString ,  
 [LenhSanXuat_ID] = @LenhSanXuat_ID    
WHERE    
 [HopDong_ID] = @HopDong_ID    
 AND [MaSanPham] = @MaSanPham    
 AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu  

 GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '41.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('41.0',GETDATE(), N'CẬP NHẬT PROCDEDURE LỆNH SẢN XUẤT VÀ ĐỊNH MỨC LOG')
END