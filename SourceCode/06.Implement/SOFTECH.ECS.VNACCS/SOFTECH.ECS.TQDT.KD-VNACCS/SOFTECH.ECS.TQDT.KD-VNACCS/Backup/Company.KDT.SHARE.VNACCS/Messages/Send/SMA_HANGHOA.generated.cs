using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class SMA_HANGHOA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute MDC { get; set; }
public PropertiesAttribute HSC { get; set; }
public PropertiesAttribute AET { get; set; }
public PropertiesAttribute QST { get; set; }
public PropertiesAttribute RGN { get; set; }
public PropertiesAttribute EOM { get; set; }
public PropertiesAttribute QT { get; set; }
public PropertiesAttribute QTU { get; set; }
public PropertiesAttribute QEA { get; set; }
public PropertiesAttribute EFE { get; set; }
public PropertiesAttribute TW { get; set; }
public PropertiesAttribute TWU { get; set; }
public PropertiesAttribute IMP { get; set; }
public PropertiesAttribute SWP { get; set; }
public PropertiesAttribute ESP { get; set; }
public PropertiesAttribute EFN { get; set; }
public PropertiesAttribute EXC { get; set; }
public PropertiesAttribute EFA { get; set; }
public PropertiesAttribute EFS { get; set; }
public PropertiesAttribute EFD { get; set; }
public PropertiesAttribute EFC { get; set; }
public PropertiesAttribute ECC { get; set; }
public PropertiesAttribute MFN { get; set; }
public PropertiesAttribute MPC { get; set; }
public PropertiesAttribute MFA { get; set; }
public PropertiesAttribute MFS { get; set; }
public PropertiesAttribute MFD { get; set; }
public PropertiesAttribute MFC { get; set; }
public PropertiesAttribute MCC { get; set; }
public PropertiesAttribute DBN { get; set; }
public PropertiesAttribute DPC { get; set; }
public PropertiesAttribute DAB { get; set; }
public PropertiesAttribute DAS { get; set; }
public PropertiesAttribute DAD { get; set; }
public PropertiesAttribute DAN { get; set; }
public PropertiesAttribute DCC { get; set; }
public PropertiesAttribute TPN { get; set; }
public PropertiesAttribute PPC { get; set; }
public PropertiesAttribute TPB { get; set; }
public PropertiesAttribute TPS { get; set; }
public PropertiesAttribute TPD { get; set; }
public PropertiesAttribute TPC { get; set; }
public PropertiesAttribute TCC { get; set; }
public PropertiesAttribute QS { get; set; }
public PropertiesAttribute QSU { get; set; }
public PropertiesAttribute QI { get; set; }
public PropertiesAttribute TQ { get; set; }
public PropertiesAttribute TE { get; set; }
public PropertiesAttribute ISD { get; set; }
public PropertiesAttribute DMD { get; set; }
public PropertiesAttribute REM { get; set; }

public SMA_HANGHOA()
        {
MDC = new PropertiesAttribute(768, typeof(string));
HSC = new PropertiesAttribute(12, typeof(string));
AET = new PropertiesAttribute(35, typeof(string));
QST = new PropertiesAttribute(35, typeof(string));
RGN = new PropertiesAttribute(17, typeof(string));
EOM = new PropertiesAttribute(8, typeof(DateTime));
QT = new PropertiesAttribute(8, typeof(int));
QTU = new PropertiesAttribute(3, typeof(string));
QEA = new PropertiesAttribute(70, typeof(string));
EFE = new PropertiesAttribute(50, typeof(string));
TW = new PropertiesAttribute(10, typeof(int));
TWU = new PropertiesAttribute(3, typeof(string));
IMP = new PropertiesAttribute(19, typeof(int));
SWP = new PropertiesAttribute(19, typeof(int));
ESP = new PropertiesAttribute(19, typeof(int));
EFN = new PropertiesAttribute(70, typeof(string));
EXC = new PropertiesAttribute(9, typeof(string));
EFA = new PropertiesAttribute(35, typeof(string));
EFS = new PropertiesAttribute(35, typeof(string));
EFD = new PropertiesAttribute(35, typeof(string));
EFC = new PropertiesAttribute(35, typeof(string));
ECC = new PropertiesAttribute(2, typeof(string));
MFN = new PropertiesAttribute(70, typeof(string));
MPC = new PropertiesAttribute(9, typeof(string));
MFA = new PropertiesAttribute(35, typeof(string));
MFS = new PropertiesAttribute(35, typeof(string));
MFD = new PropertiesAttribute(35, typeof(string));
MFC = new PropertiesAttribute(35, typeof(string));
MCC = new PropertiesAttribute(2, typeof(string));
DBN = new PropertiesAttribute(70, typeof(string));
DPC = new PropertiesAttribute(9, typeof(string));
DAB = new PropertiesAttribute(35, typeof(string));
DAS = new PropertiesAttribute(35, typeof(string));
DAD = new PropertiesAttribute(35, typeof(string));
DAN = new PropertiesAttribute(35, typeof(string));
DCC = new PropertiesAttribute(2, typeof(string));
TPN = new PropertiesAttribute(70, typeof(string));
PPC = new PropertiesAttribute(9, typeof(string));
TPB = new PropertiesAttribute(35, typeof(string));
TPS = new PropertiesAttribute(35, typeof(string));
TPD = new PropertiesAttribute(35, typeof(string));
TPC = new PropertiesAttribute(35, typeof(string));
TCC = new PropertiesAttribute(2, typeof(string));
QS = new PropertiesAttribute(8, typeof(int));
QSU = new PropertiesAttribute(3, typeof(string));
QI = new PropertiesAttribute(8, typeof(int));
TQ = new PropertiesAttribute(9, typeof(int));
TE = new PropertiesAttribute(9, typeof(int));
ISD = new PropertiesAttribute(8, typeof(int));
DMD = new PropertiesAttribute(8, typeof(int));
REM = new PropertiesAttribute(105, typeof(string));
TongSoByte = 2220;
}

}public partial class EnumGroupID
    {
}

}
