/*
Run this script on:

192.168.72.100\ecsexpress.ECS_TQDT_KD_V4_UPGRADE    -  This database will be modified

to synchronize it with:

192.168.72.100\ecsexpress.ECS_TQDT_KD_V5

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 8.1.0 from Red Gate Software Ltd at 11/21/2013 2:46:56 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

DELETE FROM [dbo].[t_VNACC_Category_Common] WHERE ReferenceDB IN ('A528', 'A519', 'A520', 'A521', 'A522')

-- Add 312 rows to [dbo].[t_VNACC_Category_Common]
SET IDENTITY_INSERT [dbo].[t_VNACC_Category_Common] ON
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (348, 'A528', 'AE01', N'AE01', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (349, 'A528', 'AF01', N'AF01', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (350, 'A528', 'AH01', N'AH01', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (351, 'A528', 'AK01', N'AK01', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (352, 'A528', 'AL01', N'AL01', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (353, 'A528', 'AM01', N'AM01', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (354, 'A528', 'AM02', N'AM02', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (355, 'A528', 'AT01', N'AT01', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (356, 'A528', 'EH01', N'EH01', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (357, 'A528', 'EH02', N'EH02', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (358, 'A528', 'HD01', N'HD01', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (359, 'A528', 'HD02', N'HD02', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (360, 'A528', 'TBDB', N'TBDB', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (398, 'A519', 'AA', N'AA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (399, 'A519', 'AB', N'AB', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (400, 'A519', 'AD', N'AD', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (401, 'A519', 'AE', N'AE', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (402, 'A519', 'AF', N'AF', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (403, 'A519', 'AG', N'AG', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (404, 'A519', 'AH', N'AH', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (405, 'A519', 'AJ', N'AJ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (406, 'A519', 'AK', N'AK', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (407, 'A519', 'AL', N'AL', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (408, 'A519', 'AM', N'AM', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (409, 'A519', 'AN', N'AN', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (410, 'A519', 'AP', N'AP', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (411, 'A519', 'AQ', N'AQ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (412, 'A519', 'AS', N'AS', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (413, 'A519', 'AT', N'AT', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (414, 'A519', 'AU', N'AU', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (415, 'A519', 'AV', N'AV', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (416, 'A519', 'AW', N'AW', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (417, 'A519', 'AX', N'AX', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (418, 'A519', 'AZ', N'AZ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (419, 'A519', 'BB', N'BB', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (420, 'A519', 'BC', N'BC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (421, 'A519', 'BD', N'BD', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (422, 'A519', 'BE', N'BE', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (423, 'A519', 'BF', N'BF', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (424, 'A519', 'BG', N'BG', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (425, 'A519', 'BH', N'BH', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (426, 'A519', 'DA', N'DA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (427, 'A519', 'DB', N'DB', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (428, 'A519', 'DC', N'DC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (429, 'A519', 'DD', N'DD', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (430, 'A519', 'DE', N'DE', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (431, 'A519', 'DF', N'DF', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (432, 'A519', 'DG', N'DG', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (433, 'A519', 'DH', N'DH', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (434, 'A519', 'DK', N'DK', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (435, 'A519', 'DL', N'DL', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (436, 'A519', 'DM', N'DM', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (437, 'A519', 'DN', N'DN', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (438, 'A519', 'DP', N'DP', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (439, 'A519', 'DQ', N'DQ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (440, 'A519', 'DR', N'DR', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (441, 'A519', 'DS', N'DS', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (442, 'A519', 'DT', N'DT', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (443, 'A519', 'DU', N'DU', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (444, 'A519', 'DX', N'DX', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (445, 'A519', 'DY', N'DY', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (446, 'A519', 'DZ', N'DZ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (447, 'A519', 'EA', N'EA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (448, 'A519', 'EC', N'EC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (449, 'A519', 'ED', N'ED', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (450, 'A519', 'EE', N'EE', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (451, 'A519', 'EG', N'EG', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (452, 'A519', 'EH', N'EH', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (453, 'A519', 'EJ', N'EJ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (454, 'A519', 'EK', N'EK', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (455, 'A519', 'EL', N'EL', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (456, 'A519', 'EM', N'EM', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (457, 'A519', 'ER', N'ER', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (458, 'A519', 'ES', N'ES', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (459, 'A519', 'ET', N'ET', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (460, 'A519', 'EU', N'EU', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (461, 'A519', 'EV', N'EV', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (462, 'A519', 'EW', N'EW', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (463, 'A519', 'EX', N'EX', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (464, 'A519', 'EY', N'EY', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (465, 'A519', 'EZ', N'EZ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (466, 'A519', 'FA', N'FA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (467, 'A519', 'FB', N'FB', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (468, 'A519', 'FC', N'FC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (469, 'A519', 'HA', N'HA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (470, 'A519', 'HB', N'HB', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (471, 'A519', 'HC', N'HC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (472, 'A519', 'HD', N'HD', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (473, 'A519', 'HE', N'HE', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (474, 'A519', 'HG', N'HG', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (475, 'A519', 'HH', N'HH', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (476, 'A519', 'HI', N'HI', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (477, 'A519', 'HK', N'HK', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (478, 'A519', 'HL', N'HL', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (479, 'A519', 'HM', N'HM', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (480, 'A519', 'HN', N'HN', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (481, 'A519', 'HP', N'HP', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (482, 'A519', 'KA', N'KA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (483, 'A519', 'KB', N'KB', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (484, 'A519', 'LA', N'LA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (485, 'A519', 'LB', N'LB', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (486, 'A519', 'LC', N'LC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (487, 'A519', 'LD', N'LD', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (488, 'A519', 'MA', N'MA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (489, 'A519', 'MB', N'MB', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (490, 'A519', 'NA', N'NA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (491, 'A519', 'NB', N'NB', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (492, 'A519', 'NC', N'NC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (493, 'A519', 'ND', N'ND', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (494, 'A519', 'NE', N'NE', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (495, 'A519', 'PA', N'PA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (496, 'A519', 'PB', N'PB', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (497, 'A519', 'QA', N'QA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (498, 'A519', 'RA', N'RA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (499, 'A519', 'SA', N'SA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (500, 'A519', 'SB', N'SB', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (501, 'A519', 'SC', N'SC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (502, 'A519', 'SD', N'SD', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (503, 'A519', 'TA', N'TA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (504, 'A519', 'UA', N'UA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (505, 'A519', 'UB', N'UB', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (506, 'A519', 'VA', N'VA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (507, 'A519', 'WA', N'WA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (508, 'A519', 'WB', N'WB', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (509, 'A519', 'WC', N'WC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (510, 'A519', 'WD', N'WD', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (511, 'A519', 'WE', N'WE', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (512, 'A519', 'WF', N'WF', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (513, 'A519', 'WG', N'WG', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (514, 'A519', 'WH', N'WH', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (515, 'A519', 'WK', N'WK', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (516, 'A519', 'WL', N'WL', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (517, 'A519', 'WM', N'WM', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (518, 'A519', 'WN', N'WN', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (519, 'A519', 'WP', N'WP', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (520, 'A519', 'WQ', N'WQ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (521, 'A519', 'WR', N'WR', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (522, 'A519', 'WS', N'WS', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (523, 'A519', 'XA', N'XA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (524, 'A519', 'XB', N'XB', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (525, 'A519', 'XC', N'XC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (526, 'A519', 'XD', N'XD', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (527, 'A519', 'XE', N'XE', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (528, 'A519', 'XF', N'XF', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (529, 'A520', 'NG', N'HANG HOA DUOC GIAM THUE NHAP KHAU', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (530, 'A520', 'XG', N'HANG HOA DUOC GIAM THUE XUAT KHAU', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (531, 'A520', 'XN011', N'HANG TNTX, TXTN THAM DU HOI CHO TRIEN LAM', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (532, 'A520', 'XN012', N'HANG TNTX, TXTN PHUC VU CONG VIEC TRONG THOI HAN NHAT DINH', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (533, 'A520', 'XN020', N'TAI SAN DI CHUYEN', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (534, 'A520', 'XN030', N'HANG CUA DOI TUONG DUOC UU DAI, MIEN TRU NGOAI GIAO ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (535, 'A520', 'XN050', N'HANG GUI QUA DICH VU CHUYEN PHAT NHANH', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (536, 'A520', 'XN061', N'HANG NK TAO TSCD CUA DU AN DAU TU VAO LINH VUC DUOC UU DAI', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (537, 'A520', 'XN062', N'HANG NK TAO TSCD CUA DU AN DAU TU VAO DIA BAN DUOC UU DAI', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (538, 'A520', 'XN063', N'HANG NK TAO TSCD CUA DU AN DAU TU BANG NGUON VON ODA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (539, 'A520', 'XN070', N'GIONG CAY TRONG, VAT NUOI CHO DU AN NONG, LAM, NGU NGHIEP', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (540, 'A520', 'XN081', N' HANG NK CHO DU AN DAU TU MO RONG THUOC  LINH VUC UU DAI ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (541, 'A520', 'XN082', N' HANG NK CHO DU AN DAU TU MO RONG THUOC  DIA BAN UU DAI ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (542, 'A520', 'XN083', N'HANG NK TAO TSCD CUA DU AN ODA MO RONG', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (543, 'A520', 'XN084', N'HANG NK CHO DU AN NONG, LAM, NGU NGHIEP MO RONG', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (544, 'A520', 'XN090', N'HANG NK DUOC MIEN THUE LAN DAU', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (545, 'A520', 'XN100', N'HANG NK PHUC VU HOAT DONG DAU KHI', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (546, 'A520', 'XN111', N'HANG NK CUA CO SO DONG TAU', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (547, 'A520', 'XN112', N'SAN PHAM TAU BIEN XK CUA CO SO DONG TAU', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (548, 'A520', 'XN120', N'HANG NK CHO HOAT DONG SAN XUAT  PHAN MEM', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (549, 'A520', 'XN130', N'HANG NK CHO HOAT DONG NCKH VA PHAT TRIEN CONG NGHE', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (550, 'A520', 'XN140', N'HANG NK DE SAN XUAT CUA DU AN DAU TU DUOC MIEN THUE 05 NAM', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (551, 'A520', 'XN150', N'HANG NK DUOC SAN XUAT, GIA CONG, TAI CHE,LAP RAP TAI KHU PTQ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (552, 'A520', 'XN160', N'HANG TNTX DE THUC HIEN DU AN ODA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (553, 'A520', 'XN170', N'HANG NK DE SAN XUAT CUA DU AN DAU TU THEO QD 33/2009/QD-TTG', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (554, 'A520', 'XN180', N'HANG NK DE BAN TAI CUA HANG MIEN THUE', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (555, 'A520', 'XN500', N'HANG MIEN THUE THEO K20, D12, ND 87/2010/ND-CP', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (556, 'A520', 'XN600', N'VAT LIEU XAY DUNG DUA VAO KHU PTQ THEO TT 11/2012/TT-BTC ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (557, 'A520', 'XN710', N'HANG NONG SAN NK DO VN HO TRO DAU TU, TRONG TAI CAMPUCHIA ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (558, 'A520', 'XN720', N'HANG NK DE SX, LAP RAP XE BUYT THEO TT 85/2012/TT-BTC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (559, 'A520', 'XN900', N'HANG NK, XK KHAC ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (560, 'A520', 'XN910', N'HANG NK, XK KHAC ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (561, 'A520', 'XNG81', N'HANG NK DE GIA CONG CHO NUOC NGOAI (DOI TUONG MIEN THUE NK)', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (562, 'A520', 'XNG82', N'SP GIA CONG XUAT TRA NUOC NGOAI (DOI TUONG MIEN THUE XK)', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (563, 'A520', 'XNG83', N'HANG XK DE GIA CONG CHO VIET NAM (DOI TUONG MIEN THUE XK)', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (564, 'A520', 'XNG84', N'SP GIA CONG NHAP TRA VIET NAM (DOI TUONG MIEN THUE NK)', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (565, 'A520', 'XNK10', N'HANG VAN CHUYEN QUA CANH, CHUYEN KHAU', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (566, 'A520', 'XNK20', N'HANG VIEN TRO NHAN DAO, VIEN TRO KHONG HOAN LAI', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (567, 'A520', 'XNK31', N'HANG TU KHU PTQ XK RA NUOC NGOAI', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (568, 'A520', 'XNK32', N'HANG NK TU NUOC NGOAI VAO KHU PTQ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (569, 'A520', 'XNK33', N'HANG TU KHU PTQ NAY SANG KHU PTQ KHAC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (570, 'A520', 'XNK40', N'HANG XK LA PHAN DAU KHI THUOC THUE TAI NGUYEN CUA NN', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (571, 'A520', 'XNK90', N'HANG HOA KHAC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (572, 'A521', 'NG', N'HANG HOA DUOC GIAM THUE NHAP KHAU', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (573, 'A521', 'XG', N'HANG HOA DUOC GIAM THUE XUAT KHAU', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (574, 'A521', 'XN011', N'HANG TNTX, TXTN THAM DU HOI CHO TRIEN LAM', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (575, 'A521', 'XN012', N'HANG TNTX, TXTN PHUC VU CONG VIEC TRONG THOI HAN NHAT DINH', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (576, 'A521', 'XN020', N'TAI SAN DI CHUYEN', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (577, 'A521', 'XN030', N'HANG CUA DOI TUONG DUOC UU DAI, MIEN TRU NGOAI GIAO ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (578, 'A521', 'XN050', N'HANG GUI QUA DICH VU CHUYEN PHAT NHANH', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (579, 'A521', 'XN061', N'HANG NK TAO TSCD CUA DU AN DAU TU VAO LINH VUC DUOC UU DAI', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (580, 'A521', 'XN062', N'HANG NK TAO TSCD CUA DU AN DAU TU VAO DIA BAN DUOC UU DAI', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (581, 'A521', 'XN063', N'HANG NK TAO TSCD CUA DU AN DAU TU BANG NGUON VON ODA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (582, 'A521', 'XN070', N'GIONG CAY TRONG, VAT NUOI CHO DU AN NONG, LAM, NGU NGHIEP', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (583, 'A521', 'XN081', N' HANG NK CHO DU AN DAU TU MO RONG THUOC  LINH VUC UU DAI ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (584, 'A521', 'XN082', N' HANG NK CHO DU AN DAU TU MO RONG THUOC  DIA BAN UU DAI ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (585, 'A521', 'XN083', N'HANG NK TAO TSCD CUA DU AN ODA MO RONG', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (586, 'A521', 'XN084', N'HANG NK CHO DU AN NONG, LAM, NGU NGHIEP MO RONG', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (587, 'A521', 'XN090', N'HANG NK DUOC MIEN THUE LAN DAU', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (588, 'A521', 'XN100', N'HANG NK PHUC VU HOAT DONG DAU KHI', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (589, 'A521', 'XN111', N'HANG NK CUA CO SO DONG TAU', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (590, 'A521', 'XN112', N'SAN PHAM TAU BIEN XK CUA CO SO DONG TAU', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (591, 'A521', 'XN120', N'HANG NK CHO HOAT DONG SAN XUAT  PHAN MEM', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (592, 'A521', 'XN130', N'HANG NK CHO HOAT DONG NCKH VA PHAT TRIEN CONG NGHE', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (593, 'A521', 'XN140', N'HANG NK DE SAN XUAT CUA DU AN DAU TU DUOC MIEN THUE 05 NAM', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (594, 'A521', 'XN150', N'HANG NK DUOC SAN XUAT, GIA CONG, TAI CHE,LAP RAP TAI KHU PTQ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (595, 'A521', 'XN160', N'HANG TNTX DE THUC HIEN DU AN ODA', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (596, 'A521', 'XN170', N'HANG NK DE SAN XUAT CUA DU AN DAU TU THEO QD 33/2009/QD-TTG', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (597, 'A521', 'XN180', N'HANG NK DE BAN TAI CUA HANG MIEN THUE', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (598, 'A521', 'XN500', N'HANG MIEN THUE THEO K20, D12, ND 87/2010/ND-CP', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (599, 'A521', 'XN600', N'VAT LIEU XAY DUNG DUA VAO KHU PTQ THEO TT 11/2012/TT-BTC ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (600, 'A521', 'XN710', N'HANG NONG SAN NK DO VN HO TRO DAU TU, TRONG TAI CAMPUCHIA ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (601, 'A521', 'XN720', N'HANG NK DE SX, LAP RAP XE BUYT THEO TT 85/2012/TT-BTC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (602, 'A521', 'XN900', N'HANG NK, XK KHAC ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (603, 'A521', 'XN910', N'HANG NK, XK KHAC ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (604, 'A521', 'XNG81', N'HANG NK DE GIA CONG CHO NUOC NGOAI (DOI TUONG MIEN THUE NK)', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (605, 'A521', 'XNG82', N'SP GIA CONG XUAT TRA NUOC NGOAI (DOI TUONG MIEN THUE XK)', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (606, 'A521', 'XNG83', N'HANG XK DE GIA CONG CHO VIET NAM (DOI TUONG MIEN THUE XK)', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (607, 'A521', 'XNG84', N'SP GIA CONG NHAP TRA VIET NAM (DOI TUONG MIEN THUE NK)', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (608, 'A521', 'XNK10', N'HANG VAN CHUYEN QUA CANH, CHUYEN KHAU ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (609, 'A521', 'XNK20', N'HANG VIEN TRO NHAN DAO, VIEN TRO KHONG HOAN LAI ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (610, 'A521', 'XNK31', N'HANG TU KHU PTQ XK RA NUOC NGOAI ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (611, 'A521', 'XNK32', N'HANG NK TU NUOC NGOAI VAO KHU PTQ', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (612, 'A521', 'XNK33', N'HANG TU KHU PTQ NAY SANG KHU PTQ KHAC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (613, 'A521', 'XNK40', N'HANG XK LA PHAN DAU KHI THUOC THUE TAI NGUYEN CUA NN', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (614, 'A521', 'XNK90', N'HANG HOA KHAC', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (615, 'A522', 'B', N'B', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (616, 'A522', 'B001', N'B001', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (617, 'A522', 'B002', N'B002', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (618, 'A522', 'B003', N'B003', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (619, 'A522', 'B004', N'B004', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (620, 'A522', 'C', N'C', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (621, 'A522', 'D', N'D', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (622, 'A522', 'E', N'E', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (623, 'A522', 'G', N'G', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (624, 'A522', 'M', N'M', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (625, 'A522', 'MB010', N'MB010', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (626, 'A522', 'MB020', N'MB020', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (627, 'A522', 'MB030', N'MB030', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (628, 'A522', 'MB040', N'MB040', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (629, 'A522', 'MB050', N'MB050', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (630, 'A522', 'MB060', N'MB060', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (631, 'A522', 'MB070', N'MB070', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (632, 'A522', 'MB110', N'MB110', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (633, 'A522', 'MB120', N'MB120', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (634, 'A522', 'MB130', N'MB130', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (635, 'A522', 'MB140', N'MB140', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (636, 'A522', 'MB200', N'MB200', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (637, 'A522', 'MB300', N'MB300', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (638, 'A522', 'MB400', N'MB400', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (639, 'A522', 'MB500', N'MB500', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (640, 'A522', 'MB600', N'MB600', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (641, 'A522', 'MB700', N'MB700', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (642, 'A522', 'P', N'P', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (643, 'A522', 'T', N'T', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (644, 'A522', 'TB010', N'TB010', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (645, 'A522', 'TB020', N'TB020', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (646, 'A522', 'TB030', N'TB030', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (647, 'A522', 'TB040', N'TB040', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (648, 'A522', 'TB050', N'TB050', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (649, 'A522', 'TB060', N'TB060', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (650, 'A522', 'TB070', N'TB070', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (651, 'A522', 'TB080', N'TB080', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (652, 'A522', 'TB090', N'TB090', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (653, 'A522', 'TB100', N'TB100', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (654, 'A522', 'TB110', N'TB110', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (655, 'A522', 'TB120', N'TB120', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (656, 'A522', 'TB130', N'TB130', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (657, 'A522', 'TB140', N'TB140', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (658, 'A522', 'TB150', N'TB150', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (659, 'A522', 'TB160', N'TB160', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (660, 'A522', 'TB170', N'TB170', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (661, 'A522', 'TB180', N'TB180', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (662, 'A522', 'TB190', N'TB190', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (663, 'A522', 'TB200', N'TB200', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (664, 'A522', 'TB210', N'TB210', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (665, 'A522', 'TB220', N'TB220', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (666, 'A522', 'TB230', N'TB230', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (667, 'A522', 'TB240', N'TB240', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (668, 'A522', 'TB250', N'TB250', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (669, 'A522', 'TB260', N'TB260', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (670, 'A522', 'TB270', N'TB270', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (671, 'A522', 'TB280', N'TB280', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (672, 'A522', 'TB290', N'TB290', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (673, 'A522', 'TB300', N'TB300', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (674, 'A522', 'TB310', N'TB310', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (675, 'A522', 'TB320', N'TB320', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (676, 'A522', 'TB330', N'TB330', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (677, 'A522', 'V', N'V', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (678, 'A522', 'VB015', N'VB015', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (679, 'A522', 'VB025', N'VB025', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (680, 'A522', 'VB035', N'VB035', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (681, 'A522', 'VB045', N'VB045', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (682, 'A522', 'VB055', N'VB055', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (683, 'A522', 'VB065', N'VB065', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (684, 'A522', 'VB075', N'VB075', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (685, 'A522', 'VB085', N'VB085', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (686, 'A522', 'VB095', N'VB095', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (687, 'A522', 'VB105', N'VB105', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (688, 'A522', 'VB115', N'VB115', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (689, 'A522', 'VB125', N'VB125', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (690, 'A522', 'VB135', N'VB135', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (691, 'A522', 'VB145', N'VB145', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (692, 'A522', 'VB155', N'VB155', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (693, 'A522', 'VB165', N'VB165', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (694, 'A522', 'VB175', N'VB175', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (695, 'A522', 'VB185', N'VB185', '', NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[t_VNACC_Category_Common] ([ID], [ReferenceDB], [Code], [Name_VN], [Name_EN], [Notes], [InputMessageID], [MessageTag], [IndexTag]) VALUES (696, 'A522', 'VB901', N'VB901', '', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[t_VNACC_Category_Common] OFF
COMMIT TRANSACTION
GO

DELETE [dbo].[t_VNACC_Category_Common]
WHERE ReferenceDB = 'E013'

GO

INSERT INTO t_VNACC_Category_Common
(
	-- ID -- this column value is auto-generated
	ReferenceDB,
	Code,
	Name_VN,
	Name_EN,
	Notes,
	InputMessageID,
	MessageTag,
	IndexTag
)
VALUES
(
	'E013',
	'A',
	N'Bảo hiểm cá nhân',
	'Individual Insurance',
	'',
	'',
	'',
	''
)
GO

INSERT INTO t_VNACC_Category_Common
(
	-- ID -- this column value is auto-generated
	ReferenceDB,
	Code,
	Name_VN,
	Name_EN,
	Notes,
	InputMessageID,
	MessageTag,
	IndexTag
)
VALUES
(
	'E013',
	'B',
	N'Bảo hiểm toàn diện',
	'Comprehensive insurance',
	'',
	'',
	'',
	''
)
GO
INSERT INTO t_VNACC_Category_Common
(
	-- ID -- this column value is auto-generated
	ReferenceDB,
	Code,
	Name_VN,
	Name_EN,
	Notes,
	InputMessageID,
	MessageTag,
	IndexTag
)
VALUES
(
	'E013',
	'D',
	N'Không có bảo hiểm',
	'Uninsured',
	'',
	'',
	'',
	''
)

GO
-- Reseed identity on [dbo].[t_VNACC_Category_Common]
DBCC CHECKIDENT('[dbo].[t_VNACC_Category_Common]', RESEED)
GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '12.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('12.9', GETDATE(), N'cap nhat danh muc VNACCS')
END	

