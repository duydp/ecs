﻿namespace Company.Interface.Report.SXXK
{
    partial class ToKhaiGCCTA4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiGCCTA4));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTrongLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhiBaoHiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTriGiaNT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChiCucHQ2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCucHQ2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel91 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable58 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow75 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable57 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow74 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable19 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChiCucHQ1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCucHQ1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel90 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable18 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel94 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBanSao12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel92 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBanChinh13 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBanChinh12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBanChinh11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBanSao11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBanSao13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBanSao16 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBanSao15 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBanSao14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel97 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBanChinh15 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBanChinh14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel89 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel88 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel87 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel85 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel83 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel82 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel81 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaygiaohang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaynhanhang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoiChiDinhNhanHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDailyLTT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDaiLyTTHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDaiLyTTHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNguoiChiDinhGiaoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiemGiaoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNguoiNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDoiTac = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrNhanMMTB = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrNhanNLD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrNhanSPGCCT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHHHDNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHDNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblsoHDNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHHPKHDNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel70 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNguoiGiao = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDoanhNghiep1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrGiaoMMTB = new DevExpress.XtraReports.UI.XRLabel();
            this.xrGiaoNLD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrGiaoSPGCCT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHDGiao = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHHHDGiao = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHDGiao = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHHPKHDGiao = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHD = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoPKHDGiao = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBanLuuHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTiepNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoBanChinh3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHHHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHHGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4,
            this.xrTable3,
            this.xrTable12,
            this.xrTable11,
            this.xrTable58,
            this.xrTable57,
            this.xrTable13,
            this.xrTable19,
            this.xrTable17,
            this.xrTable18,
            this.xrTable2,
            this.xrTable16,
            this.xrTable14,
            this.xrTable9,
            this.xrTable7,
            this.xrTable5,
            this.lblBanLuuHaiQuan,
            this.xrLabel2,
            this.xrLabel1,
            this.xrLabel23,
            this.lblSoTiepNhan,
            this.xrTable1});
            this.Detail.Height = 2268;
            this.Detail.Name = "Detail";
            this.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.Location = new System.Drawing.Point(25, 57);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.ParentStyleUsing.UseBorders = false;
            this.xrTable4.ParentStyleUsing.UseFont = false;
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable4.Size = new System.Drawing.Size(767, 58);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Size = new System.Drawing.Size(767, 58);
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.CanGrow = false;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell1.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell1.ParentStyleUsing.UseBorders = false;
            this.xrTableCell1.ParentStyleUsing.UseFont = false;
            this.xrTableCell1.Size = new System.Drawing.Size(767, 58);
            this.xrTableCell1.Text = "A- PHẦN KÊ KHAI CỦA NGƯỜI GIAO HÀNG, NGƯỜI NHẬN HÀNG";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable3.Location = new System.Drawing.Point(25, 1042);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.ParentStyleUsing.UseBorders = false;
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable3.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTrongLuong,
            this.lblPhiBaoHiem,
            this.xrTableCell3,
            this.lblTongTriGiaNT});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Size = new System.Drawing.Size(767, 30);
            // 
            // lblTrongLuong
            // 
            this.lblTrongLuong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTrongLuong.CanGrow = false;
            this.lblTrongLuong.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTrongLuong.Location = new System.Drawing.Point(0, 0);
            this.lblTrongLuong.Name = "lblTrongLuong";
            this.lblTrongLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.lblTrongLuong.ParentStyleUsing.UseBorders = false;
            this.lblTrongLuong.ParentStyleUsing.UseFont = false;
            this.lblTrongLuong.Size = new System.Drawing.Size(367, 30);
            this.lblTrongLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblPhiBaoHiem
            // 
            this.lblPhiBaoHiem.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblPhiBaoHiem.Location = new System.Drawing.Point(367, 0);
            this.lblPhiBaoHiem.Name = "lblPhiBaoHiem";
            this.lblPhiBaoHiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.lblPhiBaoHiem.ParentStyleUsing.UseFont = false;
            this.lblPhiBaoHiem.Size = new System.Drawing.Size(200, 30);
            this.lblPhiBaoHiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.CanGrow = false;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell3.Location = new System.Drawing.Point(567, 0);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 5, 0, 0, 100F);
            this.xrTableCell3.ParentStyleUsing.UseFont = false;
            this.xrTableCell3.Size = new System.Drawing.Size(104, 30);
            this.xrTableCell3.Text = "Cộng:     ";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblTongTriGiaNT
            // 
            this.lblTongTriGiaNT.BackColor = System.Drawing.Color.White;
            this.lblTongTriGiaNT.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTriGiaNT.CanGrow = false;
            this.lblTongTriGiaNT.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblTongTriGiaNT.Location = new System.Drawing.Point(671, 0);
            this.lblTongTriGiaNT.Name = "lblTongTriGiaNT";
            this.lblTongTriGiaNT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaNT.ParentStyleUsing.UseBackColor = false;
            this.lblTongTriGiaNT.ParentStyleUsing.UseBorders = false;
            this.lblTongTriGiaNT.ParentStyleUsing.UseFont = false;
            this.lblTongTriGiaNT.Size = new System.Drawing.Size(96, 30);
            this.lblTongTriGiaNT.Tag = "Tổng trị giá NT";
            this.lblTongTriGiaNT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongTriGiaNT.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // xrTable12
            // 
            this.xrTable12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable12.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable12.Location = new System.Drawing.Point(25, 1760);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.ParentStyleUsing.UseBorders = false;
            this.xrTable12.ParentStyleUsing.UseFont = false;
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32});
            this.xrTable12.Size = new System.Drawing.Size(767, 45);
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell135});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Size = new System.Drawing.Size(767, 45);
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell135.CanGrow = false;
            this.xrTableCell135.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell135.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell135.Multiline = true;
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell135.ParentStyleUsing.UseBorders = false;
            this.xrTableCell135.ParentStyleUsing.UseFont = false;
            this.xrTableCell135.Size = new System.Drawing.Size(767, 45);
            this.xrTableCell135.Text = " C- PHẦN DÀNH CHO HẢI QUAN LÀM THỦ TỤC NHẬN HÀNG";
            this.xrTableCell135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable11
            // 
            this.xrTable11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable11.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable11.Location = new System.Drawing.Point(25, 1802);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.ParentStyleUsing.UseBorders = false;
            this.xrTable11.ParentStyleUsing.UseFont = false;
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
            this.xrTable11.Size = new System.Drawing.Size(767, 93);
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell131,
            this.xrTableCell132,
            this.xrTableCell134});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Size = new System.Drawing.Size(767, 93);
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell131.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblChiCucHQ2,
            this.lblCucHQ2,
            this.xrLabel27,
            this.xrLabel25,
            this.xrLabel24});
            this.xrTableCell131.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell131.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell131.Multiline = true;
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell131.ParentStyleUsing.UseBorders = false;
            this.xrTableCell131.ParentStyleUsing.UseFont = false;
            this.xrTableCell131.Size = new System.Drawing.Size(258, 93);
            this.xrTableCell131.Text = " ";
            // 
            // lblChiCucHQ2
            // 
            this.lblChiCucHQ2.BorderWidth = 0;
            this.lblChiCucHQ2.Location = new System.Drawing.Point(95, 56);
            this.lblChiCucHQ2.Name = "lblChiCucHQ2";
            this.lblChiCucHQ2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHQ2.ParentStyleUsing.UseBorderWidth = false;
            this.lblChiCucHQ2.Size = new System.Drawing.Size(150, 20);
            this.lblChiCucHQ2.Text = " ";
            this.lblChiCucHQ2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblCucHQ2
            // 
            this.lblCucHQ2.BorderWidth = 0;
            this.lblCucHQ2.Location = new System.Drawing.Point(77, 29);
            this.lblCucHQ2.Name = "lblCucHQ2";
            this.lblCucHQ2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCucHQ2.ParentStyleUsing.UseBorderWidth = false;
            this.lblCucHQ2.Size = new System.Drawing.Size(150, 20);
            this.lblCucHQ2.Text = " ";
            this.lblCucHQ2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.BorderWidth = 0;
            this.xrLabel27.Location = new System.Drawing.Point(8, 57);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel27.Size = new System.Drawing.Size(242, 20);
            this.xrLabel27.Text = "Chi cục hải quan...................................................";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel25
            // 
            this.xrLabel25.BorderWidth = 0;
            this.xrLabel25.Location = new System.Drawing.Point(8, 31);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel25.Size = new System.Drawing.Size(242, 20);
            this.xrLabel25.Text = "Cục hải quan...................................................";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.BorderWidth = 0;
            this.xrLabel24.Location = new System.Drawing.Point(8, 6);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel24.Size = new System.Drawing.Size(109, 20);
            this.xrLabel24.Text = "Tổng cục hải quan";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel6,
            this.xrLabel5});
            this.xrTableCell132.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell132.Location = new System.Drawing.Point(258, 0);
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell132.ParentStyleUsing.UseFont = false;
            this.xrTableCell132.Size = new System.Drawing.Size(318, 93);
            this.xrTableCell132.Text = " ";
            // 
            // xrLabel6
            // 
            this.xrLabel6.BorderWidth = 0;
            this.xrLabel6.Location = new System.Drawing.Point(8, 50);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel6.Size = new System.Drawing.Size(299, 20);
            this.xrLabel6.Text = "Ngày đăng ký....................................................................." +
                ".";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.BorderWidth = 0;
            this.xrLabel5.Location = new System.Drawing.Point(8, 17);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel5.Size = new System.Drawing.Size(300, 20);
            this.xrLabel5.Text = "Tờ khai số ............./N/.................../.................................." +
                "....";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel91});
            this.xrTableCell134.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell134.Location = new System.Drawing.Point(576, 0);
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell134.ParentStyleUsing.UseFont = false;
            this.xrTableCell134.Size = new System.Drawing.Size(191, 93);
            this.xrTableCell134.Text = " ";
            // 
            // xrLabel91
            // 
            this.xrLabel91.BorderWidth = 0;
            this.xrLabel91.Location = new System.Drawing.Point(7, 8);
            this.xrLabel91.Name = "xrLabel91";
            this.xrLabel91.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel91.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel91.Size = new System.Drawing.Size(166, 59);
            this.xrLabel91.Text = "Cán bộ đăng ký( ký đóng dấu số hiệu công chức)";
            // 
            // xrTable58
            // 
            this.xrTable58.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable58.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable58.Location = new System.Drawing.Point(25, 1977);
            this.xrTable58.Name = "xrTable58";
            this.xrTable58.ParentStyleUsing.UseBorders = false;
            this.xrTable58.ParentStyleUsing.UseFont = false;
            this.xrTable58.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow75});
            this.xrTable58.Size = new System.Drawing.Size(767, 202);
            // 
            // xrTableRow75
            // 
            this.xrTableRow75.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell216,
            this.xrTableCell218});
            this.xrTableRow75.Name = "xrTableRow75";
            this.xrTableRow75.Size = new System.Drawing.Size(767, 202);
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell216.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell216.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell216.Multiline = true;
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell216.ParentStyleUsing.UseBorders = false;
            this.xrTableCell216.ParentStyleUsing.UseFont = false;
            this.xrTableCell216.Size = new System.Drawing.Size(350, 202);
            this.xrTableCell216.Text = "27. Ghi chép khác của Hải quan";
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell218.Location = new System.Drawing.Point(350, 0);
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell218.ParentStyleUsing.UseFont = false;
            this.xrTableCell218.Size = new System.Drawing.Size(417, 202);
            this.xrTableCell218.Text = "28. Xác nhận đã làm thủ tục hải quan (Ký, đóng dấu số hiệu công chức).";
            // 
            // xrTable57
            // 
            this.xrTable57.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable57.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable57.Location = new System.Drawing.Point(25, 1894);
            this.xrTable57.Name = "xrTable57";
            this.xrTable57.ParentStyleUsing.UseBorders = false;
            this.xrTable57.ParentStyleUsing.UseFont = false;
            this.xrTable57.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow74});
            this.xrTable57.Size = new System.Drawing.Size(767, 83);
            // 
            // xrTableRow74
            // 
            this.xrTableRow74.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell215});
            this.xrTableRow74.Name = "xrTableRow74";
            this.xrTableRow74.Size = new System.Drawing.Size(767, 83);
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell215.CanGrow = false;
            this.xrTableCell215.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell215.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell215.Multiline = true;
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell215.ParentStyleUsing.UseBorders = false;
            this.xrTableCell215.ParentStyleUsing.UseFont = false;
            this.xrTableCell215.Size = new System.Drawing.Size(767, 83);
            this.xrTableCell215.Text = resources.GetString("xrTableCell215.Text");
            // 
            // xrTable13
            // 
            this.xrTable13.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable13.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable13.Location = new System.Drawing.Point(25, 1433);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.ParentStyleUsing.UseBorders = false;
            this.xrTable13.ParentStyleUsing.UseFont = false;
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33});
            this.xrTable13.Size = new System.Drawing.Size(767, 45);
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell136});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Size = new System.Drawing.Size(767, 45);
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell136.CanGrow = false;
            this.xrTableCell136.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell136.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell136.Multiline = true;
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell136.ParentStyleUsing.UseBorders = false;
            this.xrTableCell136.ParentStyleUsing.UseFont = false;
            this.xrTableCell136.Size = new System.Drawing.Size(767, 45);
            this.xrTableCell136.Text = " B- PHẦN DÀNH CHO HẢI QUAN LÀM THỦ TỤC GIAO HÀNG";
            this.xrTableCell136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable19
            // 
            this.xrTable19.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable19.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable19.Location = new System.Drawing.Point(25, 1570);
            this.xrTable19.Name = "xrTable19";
            this.xrTable19.ParentStyleUsing.UseBorders = false;
            this.xrTable19.ParentStyleUsing.UseFont = false;
            this.xrTable19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow38});
            this.xrTable19.Size = new System.Drawing.Size(767, 83);
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell146});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Size = new System.Drawing.Size(767, 83);
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell146.CanGrow = false;
            this.xrTableCell146.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell146.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell146.Multiline = true;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell146.ParentStyleUsing.UseBorders = false;
            this.xrTableCell146.ParentStyleUsing.UseFont = false;
            this.xrTableCell146.Size = new System.Drawing.Size(767, 83);
            this.xrTableCell146.Text = resources.GetString("xrTableCell146.Text");
            // 
            // xrTable17
            // 
            this.xrTable17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable17.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable17.Location = new System.Drawing.Point(25, 1477);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.ParentStyleUsing.UseBorders = false;
            this.xrTable17.ParentStyleUsing.UseFont = false;
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable17.Size = new System.Drawing.Size(767, 93);
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell137,
            this.xrTableCell142,
            this.xrTableCell143});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Size = new System.Drawing.Size(767, 93);
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell137.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblChiCucHQ1,
            this.lblCucHQ1,
            this.xrLabel39,
            this.xrLabel40,
            this.xrLabel42});
            this.xrTableCell137.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell137.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell137.Multiline = true;
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell137.ParentStyleUsing.UseBorders = false;
            this.xrTableCell137.ParentStyleUsing.UseFont = false;
            this.xrTableCell137.Size = new System.Drawing.Size(258, 93);
            this.xrTableCell137.Text = " ";
            // 
            // lblChiCucHQ1
            // 
            this.lblChiCucHQ1.BorderWidth = 0;
            this.lblChiCucHQ1.Location = new System.Drawing.Point(100, 62);
            this.lblChiCucHQ1.Name = "lblChiCucHQ1";
            this.lblChiCucHQ1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHQ1.ParentStyleUsing.UseBorderWidth = false;
            this.lblChiCucHQ1.Size = new System.Drawing.Size(150, 20);
            this.lblChiCucHQ1.Text = " ";
            this.lblChiCucHQ1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblCucHQ1
            // 
            this.lblCucHQ1.BorderWidth = 0;
            this.lblCucHQ1.Location = new System.Drawing.Point(81, 31);
            this.lblCucHQ1.Name = "lblCucHQ1";
            this.lblCucHQ1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCucHQ1.ParentStyleUsing.UseBorderWidth = false;
            this.lblCucHQ1.Size = new System.Drawing.Size(142, 20);
            this.lblCucHQ1.Text = " ";
            this.lblCucHQ1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel39
            // 
            this.xrLabel39.BorderWidth = 0;
            this.xrLabel39.Location = new System.Drawing.Point(8, 61);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel39.Size = new System.Drawing.Size(242, 19);
            this.xrLabel39.Text = "Tổng cục hải quan...................................";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel40
            // 
            this.xrLabel40.BorderWidth = 0;
            this.xrLabel40.Location = new System.Drawing.Point(8, 33);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel40.Size = new System.Drawing.Size(242, 20);
            this.xrLabel40.Text = "Cục hải quan...........................................";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel42
            // 
            this.xrLabel42.BorderWidth = 0;
            this.xrLabel42.Location = new System.Drawing.Point(8, 8);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel42.Size = new System.Drawing.Size(142, 20);
            this.xrLabel42.Text = "Tổng cục hải quan";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel44,
            this.xrLabel43});
            this.xrTableCell142.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell142.Location = new System.Drawing.Point(258, 0);
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell142.ParentStyleUsing.UseFont = false;
            this.xrTableCell142.Size = new System.Drawing.Size(318, 93);
            this.xrTableCell142.Text = " ";
            // 
            // xrLabel44
            // 
            this.xrLabel44.BorderWidth = 0;
            this.xrLabel44.Location = new System.Drawing.Point(9, 50);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel44.Size = new System.Drawing.Size(299, 20);
            this.xrLabel44.Text = "Ngày đăng ký....................................................................." +
                ".";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel43
            // 
            this.xrLabel43.BorderWidth = 0;
            this.xrLabel43.Location = new System.Drawing.Point(8, 15);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel43.Size = new System.Drawing.Size(300, 20);
            this.xrLabel43.Text = "Tờ khai số ............./G/................../..................................." +
                "...";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel90});
            this.xrTableCell143.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell143.Location = new System.Drawing.Point(576, 0);
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell143.ParentStyleUsing.UseFont = false;
            this.xrTableCell143.Size = new System.Drawing.Size(191, 93);
            this.xrTableCell143.Text = " ";
            // 
            // xrLabel90
            // 
            this.xrLabel90.BorderWidth = 0;
            this.xrLabel90.Location = new System.Drawing.Point(7, 6);
            this.xrLabel90.Name = "xrLabel90";
            this.xrLabel90.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel90.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel90.Size = new System.Drawing.Size(175, 59);
            this.xrLabel90.Text = "Cán bộ đăng ký( ký đóng dấu số hiệu công chức)";
            // 
            // xrTable18
            // 
            this.xrTable18.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable18.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable18.Location = new System.Drawing.Point(25, 1653);
            this.xrTable18.Name = "xrTable18";
            this.xrTable18.ParentStyleUsing.UseBorders = false;
            this.xrTable18.ParentStyleUsing.UseFont = false;
            this.xrTable18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow37});
            this.xrTable18.Size = new System.Drawing.Size(767, 109);
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell144,
            this.xrTableCell145});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Size = new System.Drawing.Size(767, 109);
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell144.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell144.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell144.Multiline = true;
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell144.ParentStyleUsing.UseBorders = false;
            this.xrTableCell144.ParentStyleUsing.UseFont = false;
            this.xrTableCell144.Size = new System.Drawing.Size(325, 109);
            this.xrTableCell144.Text = "24. Ghi chép khác của Hải quan :";
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell145.Location = new System.Drawing.Point(325, 0);
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell145.ParentStyleUsing.UseFont = false;
            this.xrTableCell145.Size = new System.Drawing.Size(442, 109);
            this.xrTableCell145.Text = "25. Xác nhận đã làm thủ tục hải quan (Ký, đóng dấu số hiệu công chức).";
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable2.Location = new System.Drawing.Point(25, 1142);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.ParentStyleUsing.UseBorders = false;
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34,
            this.xrTableRow35});
            this.xrTable2.Size = new System.Drawing.Size(767, 291);
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell138,
            this.xrTableCell139});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Size = new System.Drawing.Size(767, 125);
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell138.CanGrow = false;
            this.xrTableCell138.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel94,
            this.lblBanSao12,
            this.xrLabel92,
            this.lblBanChinh13,
            this.lblBanChinh12,
            this.lblBanChinh11,
            this.xrLabel50,
            this.lblBanSao11,
            this.xrLabel51,
            this.xrLabel21,
            this.xrLabel20,
            this.lblBanSao13,
            this.xrLabel46,
            this.xrLabel45,
            this.xrLabel22,
            this.xrLabel18,
            this.xrLabel16,
            this.xrLabel15});
            this.xrTableCell138.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell138.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell138.ParentStyleUsing.UseBorders = false;
            this.xrTableCell138.ParentStyleUsing.UseFont = false;
            this.xrTableCell138.Size = new System.Drawing.Size(400, 125);
            this.xrTableCell138.Text = " ";
            this.xrTableCell138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel94
            // 
            this.xrLabel94.BorderWidth = 0;
            this.xrLabel94.Location = new System.Drawing.Point(283, 83);
            this.xrLabel94.Name = "xrLabel94";
            this.xrLabel94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel94.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel94.Size = new System.Drawing.Size(75, 16);
            this.xrLabel94.Text = " ";
            this.xrLabel94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblBanSao12
            // 
            this.lblBanSao12.BorderWidth = 0;
            this.lblBanSao12.Location = new System.Drawing.Point(283, 58);
            this.lblBanSao12.Name = "lblBanSao12";
            this.lblBanSao12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanSao12.ParentStyleUsing.UseBorderWidth = false;
            this.lblBanSao12.Size = new System.Drawing.Size(75, 16);
            this.lblBanSao12.Text = " ";
            this.lblBanSao12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel92
            // 
            this.xrLabel92.BorderWidth = 0;
            this.xrLabel92.Location = new System.Drawing.Point(283, 34);
            this.xrLabel92.Name = "xrLabel92";
            this.xrLabel92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel92.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel92.Size = new System.Drawing.Size(75, 16);
            this.xrLabel92.Text = " ";
            this.xrLabel92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblBanChinh13
            // 
            this.lblBanChinh13.BorderWidth = 0;
            this.lblBanChinh13.Location = new System.Drawing.Point(175, 84);
            this.lblBanChinh13.Name = "lblBanChinh13";
            this.lblBanChinh13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanChinh13.ParentStyleUsing.UseBorderWidth = false;
            this.lblBanChinh13.Size = new System.Drawing.Size(75, 16);
            this.lblBanChinh13.Text = " ";
            this.lblBanChinh13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblBanChinh12
            // 
            this.lblBanChinh12.BorderWidth = 0;
            this.lblBanChinh12.Location = new System.Drawing.Point(175, 59);
            this.lblBanChinh12.Name = "lblBanChinh12";
            this.lblBanChinh12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanChinh12.ParentStyleUsing.UseBorderWidth = false;
            this.lblBanChinh12.Size = new System.Drawing.Size(75, 16);
            this.lblBanChinh12.Text = " ";
            this.lblBanChinh12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblBanChinh11
            // 
            this.lblBanChinh11.BorderWidth = 0;
            this.lblBanChinh11.Location = new System.Drawing.Point(175, 33);
            this.lblBanChinh11.Name = "lblBanChinh11";
            this.lblBanChinh11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanChinh11.ParentStyleUsing.UseBorderWidth = false;
            this.lblBanChinh11.Size = new System.Drawing.Size(75, 16);
            this.lblBanChinh11.Text = " ";
            this.lblBanChinh11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel50
            // 
            this.xrLabel50.BorderWidth = 0;
            this.xrLabel50.Location = new System.Drawing.Point(0, 33);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel50.Size = new System.Drawing.Size(142, 20);
            this.xrLabel50.Text = "- Chỉ định giao hàng";
            this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblBanSao11
            // 
            this.lblBanSao11.BorderWidth = 0;
            this.lblBanSao11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblBanSao11.Location = new System.Drawing.Point(283, 33);
            this.lblBanSao11.Name = "lblBanSao11";
            this.lblBanSao11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanSao11.ParentStyleUsing.UseBorderWidth = false;
            this.lblBanSao11.ParentStyleUsing.UseFont = false;
            this.lblBanSao11.Size = new System.Drawing.Size(84, 26);
            this.lblBanSao11.Text = ".................. ";
            this.lblBanSao11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel51
            // 
            this.xrLabel51.BorderWidth = 0;
            this.xrLabel51.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel51.Location = new System.Drawing.Point(167, 33);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel51.ParentStyleUsing.UseFont = false;
            this.xrLabel51.Size = new System.Drawing.Size(100, 26);
            this.xrLabel51.Text = "................. ";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel21
            // 
            this.xrLabel21.BorderWidth = 0;
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel21.Location = new System.Drawing.Point(25, 83);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel21.ParentStyleUsing.UseFont = false;
            this.xrLabel21.Size = new System.Drawing.Size(142, 25);
            this.xrLabel21.Text = "............................ ";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.BorderWidth = 0;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel20.Location = new System.Drawing.Point(25, 58);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel20.ParentStyleUsing.UseFont = false;
            this.xrLabel20.Size = new System.Drawing.Size(142, 26);
            this.xrLabel20.Text = ".......................... ";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblBanSao13
            // 
            this.lblBanSao13.BorderWidth = 0;
            this.lblBanSao13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblBanSao13.Location = new System.Drawing.Point(283, 83);
            this.lblBanSao13.Name = "lblBanSao13";
            this.lblBanSao13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanSao13.ParentStyleUsing.UseBorderWidth = false;
            this.lblBanSao13.ParentStyleUsing.UseFont = false;
            this.lblBanSao13.Size = new System.Drawing.Size(84, 25);
            this.lblBanSao13.Text = "..................";
            this.lblBanSao13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel46
            // 
            this.xrLabel46.BorderWidth = 0;
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel46.Location = new System.Drawing.Point(283, 58);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel46.ParentStyleUsing.UseFont = false;
            this.xrLabel46.Size = new System.Drawing.Size(84, 25);
            this.xrLabel46.Text = ".................. ";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel45
            // 
            this.xrLabel45.BorderWidth = 0;
            this.xrLabel45.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel45.Location = new System.Drawing.Point(167, 58);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel45.ParentStyleUsing.UseFont = false;
            this.xrLabel45.Size = new System.Drawing.Size(100, 26);
            this.xrLabel45.Text = "................. ";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel22
            // 
            this.xrLabel22.BorderWidth = 0;
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel22.Location = new System.Drawing.Point(167, 83);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel22.ParentStyleUsing.UseFont = false;
            this.xrLabel22.Size = new System.Drawing.Size(100, 25);
            this.xrLabel22.Text = "................";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel18
            // 
            this.xrLabel18.BorderWidth = 0;
            this.xrLabel18.Location = new System.Drawing.Point(175, 8);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel18.Size = new System.Drawing.Size(75, 20);
            this.xrLabel18.Text = "Bản chính";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.BorderWidth = 0;
            this.xrLabel16.Location = new System.Drawing.Point(283, 8);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel16.Size = new System.Drawing.Size(75, 20);
            this.xrLabel16.Text = "Bản sao";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel15
            // 
            this.xrLabel15.BorderWidth = 0;
            this.xrLabel15.Location = new System.Drawing.Point(8, 8);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel15.Size = new System.Drawing.Size(142, 20);
            this.xrLabel15.Text = "19. Chứng từ kèm theo :";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell139.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblBanSao16,
            this.lblBanSao15,
            this.lblBanSao14,
            this.xrLabel97,
            this.lblBanChinh15,
            this.lblBanChinh14,
            this.xrLabel89,
            this.xrLabel88,
            this.xrLabel87,
            this.xrLabel86,
            this.xrLabel85,
            this.xrLabel84,
            this.xrLabel83,
            this.xrLabel82,
            this.xrLabel81,
            this.xrLabel80,
            this.xrLabel79,
            this.xrLabel78});
            this.xrTableCell139.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell139.Location = new System.Drawing.Point(400, 0);
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.xrTableCell139.ParentStyleUsing.UseBorders = false;
            this.xrTableCell139.ParentStyleUsing.UseFont = false;
            this.xrTableCell139.Size = new System.Drawing.Size(367, 125);
            this.xrTableCell139.Text = " ";
            this.xrTableCell139.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblBanSao16
            // 
            this.lblBanSao16.BorderWidth = 0;
            this.lblBanSao16.Location = new System.Drawing.Point(275, 83);
            this.lblBanSao16.Name = "lblBanSao16";
            this.lblBanSao16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanSao16.ParentStyleUsing.UseBorderWidth = false;
            this.lblBanSao16.Size = new System.Drawing.Size(75, 16);
            this.lblBanSao16.Text = " ";
            this.lblBanSao16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblBanSao15
            // 
            this.lblBanSao15.BorderWidth = 0;
            this.lblBanSao15.Location = new System.Drawing.Point(275, 58);
            this.lblBanSao15.Name = "lblBanSao15";
            this.lblBanSao15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanSao15.ParentStyleUsing.UseBorderWidth = false;
            this.lblBanSao15.Size = new System.Drawing.Size(75, 16);
            this.lblBanSao15.Text = " ";
            this.lblBanSao15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblBanSao14
            // 
            this.lblBanSao14.BorderWidth = 0;
            this.lblBanSao14.Location = new System.Drawing.Point(275, 33);
            this.lblBanSao14.Name = "lblBanSao14";
            this.lblBanSao14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanSao14.ParentStyleUsing.UseBorderWidth = false;
            this.lblBanSao14.Size = new System.Drawing.Size(75, 16);
            this.lblBanSao14.Text = " ";
            this.lblBanSao14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel97
            // 
            this.xrLabel97.BorderWidth = 0;
            this.xrLabel97.Location = new System.Drawing.Point(175, 80);
            this.xrLabel97.Name = "xrLabel97";
            this.xrLabel97.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel97.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel97.Size = new System.Drawing.Size(75, 16);
            this.xrLabel97.Text = " ";
            this.xrLabel97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblBanChinh15
            // 
            this.lblBanChinh15.BorderWidth = 0;
            this.lblBanChinh15.Location = new System.Drawing.Point(175, 58);
            this.lblBanChinh15.Name = "lblBanChinh15";
            this.lblBanChinh15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanChinh15.ParentStyleUsing.UseBorderWidth = false;
            this.lblBanChinh15.Size = new System.Drawing.Size(75, 16);
            this.lblBanChinh15.Text = " ";
            this.lblBanChinh15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblBanChinh14
            // 
            this.lblBanChinh14.BorderWidth = 0;
            this.lblBanChinh14.Location = new System.Drawing.Point(175, 33);
            this.lblBanChinh14.Name = "lblBanChinh14";
            this.lblBanChinh14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanChinh14.ParentStyleUsing.UseBorderWidth = false;
            this.lblBanChinh14.Size = new System.Drawing.Size(75, 16);
            this.lblBanChinh14.Text = " ";
            this.lblBanChinh14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel89
            // 
            this.xrLabel89.BorderWidth = 0;
            this.xrLabel89.Location = new System.Drawing.Point(0, 33);
            this.xrLabel89.Name = "xrLabel89";
            this.xrLabel89.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel89.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel89.Size = new System.Drawing.Size(142, 20);
            this.xrLabel89.Text = "- Chỉ định nhận hàng";
            this.xrLabel89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel88
            // 
            this.xrLabel88.BorderWidth = 0;
            this.xrLabel88.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel88.Location = new System.Drawing.Point(17, 83);
            this.xrLabel88.Name = "xrLabel88";
            this.xrLabel88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel88.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel88.ParentStyleUsing.UseFont = false;
            this.xrLabel88.Size = new System.Drawing.Size(142, 25);
            this.xrLabel88.Text = "......................... ";
            this.xrLabel88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel87
            // 
            this.xrLabel87.BorderWidth = 0;
            this.xrLabel87.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel87.Location = new System.Drawing.Point(17, 67);
            this.xrLabel87.Name = "xrLabel87";
            this.xrLabel87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel87.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel87.ParentStyleUsing.UseFont = false;
            this.xrLabel87.Size = new System.Drawing.Size(142, 17);
            this.xrLabel87.Text = "......................... ";
            this.xrLabel87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel86
            // 
            this.xrLabel86.BorderWidth = 0;
            this.xrLabel86.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel86.Location = new System.Drawing.Point(275, 67);
            this.xrLabel86.Name = "xrLabel86";
            this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel86.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel86.ParentStyleUsing.UseFont = false;
            this.xrLabel86.Size = new System.Drawing.Size(84, 17);
            this.xrLabel86.Text = "..............  ";
            this.xrLabel86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel85
            // 
            this.xrLabel85.BorderWidth = 0;
            this.xrLabel85.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel85.Location = new System.Drawing.Point(275, 83);
            this.xrLabel85.Name = "xrLabel85";
            this.xrLabel85.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel85.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel85.ParentStyleUsing.UseFont = false;
            this.xrLabel85.Size = new System.Drawing.Size(83, 25);
            this.xrLabel85.Text = "..............";
            this.xrLabel85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel84
            // 
            this.xrLabel84.BorderWidth = 0;
            this.xrLabel84.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel84.Location = new System.Drawing.Point(167, 83);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel84.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel84.ParentStyleUsing.UseFont = false;
            this.xrLabel84.Size = new System.Drawing.Size(100, 25);
            this.xrLabel84.Text = "................";
            this.xrLabel84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel83
            // 
            this.xrLabel83.BorderWidth = 0;
            this.xrLabel83.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel83.Location = new System.Drawing.Point(167, 66);
            this.xrLabel83.Name = "xrLabel83";
            this.xrLabel83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel83.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel83.ParentStyleUsing.UseFont = false;
            this.xrLabel83.Size = new System.Drawing.Size(100, 18);
            this.xrLabel83.Text = "................. ";
            this.xrLabel83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel82
            // 
            this.xrLabel82.BorderWidth = 0;
            this.xrLabel82.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel82.Location = new System.Drawing.Point(167, 33);
            this.xrLabel82.Name = "xrLabel82";
            this.xrLabel82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel82.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel82.ParentStyleUsing.UseFont = false;
            this.xrLabel82.Size = new System.Drawing.Size(100, 26);
            this.xrLabel82.Text = "................. ";
            this.xrLabel82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel81
            // 
            this.xrLabel81.BorderWidth = 0;
            this.xrLabel81.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel81.Location = new System.Drawing.Point(275, 33);
            this.xrLabel81.Name = "xrLabel81";
            this.xrLabel81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel81.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel81.ParentStyleUsing.UseFont = false;
            this.xrLabel81.Size = new System.Drawing.Size(84, 26);
            this.xrLabel81.Text = "..............  ";
            this.xrLabel81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel80
            // 
            this.xrLabel80.BorderWidth = 0;
            this.xrLabel80.Location = new System.Drawing.Point(270, 8);
            this.xrLabel80.Name = "xrLabel80";
            this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel80.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel80.Size = new System.Drawing.Size(75, 20);
            this.xrLabel80.Text = "Bản sao";
            this.xrLabel80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel79
            // 
            this.xrLabel79.BorderWidth = 0;
            this.xrLabel79.Location = new System.Drawing.Point(173, 8);
            this.xrLabel79.Name = "xrLabel79";
            this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel79.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel79.Size = new System.Drawing.Size(75, 20);
            this.xrLabel79.Text = "Bản chính";
            this.xrLabel79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel78
            // 
            this.xrLabel78.BorderWidth = 0;
            this.xrLabel78.Location = new System.Drawing.Point(4, 8);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel78.Size = new System.Drawing.Size(142, 20);
            this.xrLabel78.Text = "20. Chứng từ kèm theo :";
            this.xrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell140,
            this.xrTableCell141});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Size = new System.Drawing.Size(767, 166);
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell140.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel74,
            this.lblNgaygiaohang,
            this.xrLabel35});
            this.xrTableCell140.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell140.ParentStyleUsing.UseBorders = false;
            this.xrTableCell140.Size = new System.Drawing.Size(400, 166);
            this.xrTableCell140.Text = " ";
            // 
            // xrLabel74
            // 
            this.xrLabel74.BorderWidth = 0;
            this.xrLabel74.Location = new System.Drawing.Point(142, 140);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel74.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel74.Size = new System.Drawing.Size(192, 20);
            this.xrLabel74.Text = "(ký tên, đóng đấu, ghi rõ họ tên)";
            this.xrLabel74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgaygiaohang
            // 
            this.lblNgaygiaohang.BorderWidth = 0;
            this.lblNgaygiaohang.Location = new System.Drawing.Point(108, 58);
            this.lblNgaygiaohang.Name = "lblNgaygiaohang";
            this.lblNgaygiaohang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaygiaohang.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgaygiaohang.Size = new System.Drawing.Size(267, 20);
            this.lblNgaygiaohang.Text = "Ngày        tháng         năm  2009";
            this.lblNgaygiaohang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel35
            // 
            this.xrLabel35.BorderWidth = 0;
            this.xrLabel35.Location = new System.Drawing.Point(8, 8);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel35.Size = new System.Drawing.Size(367, 50);
            this.xrLabel35.Text = "21. Ngưởi giao hàng : Cam kết đã giao đúng, đủ các sản phẩm kê khai trên tờ khai " +
                "này và chịu trách nhiệm nội dung khai trên tờ khai này.";
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell141.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel77,
            this.lblNgaynhanhang,
            this.xrLabel75});
            this.xrTableCell141.Location = new System.Drawing.Point(400, 0);
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell141.ParentStyleUsing.UseBorders = false;
            this.xrTableCell141.Size = new System.Drawing.Size(367, 166);
            this.xrTableCell141.Text = " ";
            // 
            // xrLabel77
            // 
            this.xrLabel77.BorderWidth = 0;
            this.xrLabel77.Location = new System.Drawing.Point(133, 142);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel77.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel77.Size = new System.Drawing.Size(192, 20);
            this.xrLabel77.Text = "(ký tên, đóng đấu, ghi rõ họ tên)";
            this.xrLabel77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgaynhanhang
            // 
            this.lblNgaynhanhang.BorderWidth = 0;
            this.lblNgaynhanhang.Location = new System.Drawing.Point(100, 58);
            this.lblNgaynhanhang.Name = "lblNgaynhanhang";
            this.lblNgaynhanhang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaynhanhang.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgaynhanhang.Size = new System.Drawing.Size(267, 20);
            this.lblNgaynhanhang.Text = "Ngày        tháng         năm  2009";
            this.lblNgaynhanhang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel75
            // 
            this.xrLabel75.BorderWidth = 0;
            this.xrLabel75.Location = new System.Drawing.Point(0, 8);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel75.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel75.Size = new System.Drawing.Size(367, 50);
            this.xrLabel75.Text = "22. Ngưởi nhận hàng : Cam kết đã nhận đúng, đủ các sản phẩm kê khai trên tờ khai " +
                "này và chịu trách nhiệm nội dung khai trên tờ khai này.";
            // 
            // xrTable16
            // 
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable16.Location = new System.Drawing.Point(25, 526);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.ParentStyleUsing.UseBorders = false;
            this.xrTable16.ParentStyleUsing.UseFont = false;
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.xrTable16.Size = new System.Drawing.Size(767, 41);
            this.xrTable16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell88,
            this.xrTableCell80,
            this.xrTableCell89,
            this.xrTableCell81,
            this.xrTableCell83,
            this.xrTableCell90,
            this.xrTableCell92});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Size = new System.Drawing.Size(767, 41);
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell88.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell88.ParentStyleUsing.UseFont = false;
            this.xrTableCell88.Size = new System.Drawing.Size(25, 41);
            this.xrTableCell88.Text = "SỐ TT";
            this.xrTableCell88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell80.Location = new System.Drawing.Point(25, 0);
            this.xrTableCell80.Multiline = true;
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell80.ParentStyleUsing.UseFont = false;
            this.xrTableCell80.Size = new System.Drawing.Size(283, 41);
            this.xrTableCell80.Text = "13. TÊN HÀNG \r\n QUY CÁCH PHẨM CHẤT";
            this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell89.Location = new System.Drawing.Point(308, 0);
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell89.ParentStyleUsing.UseFont = false;
            this.xrTableCell89.Size = new System.Drawing.Size(84, 41);
            this.xrTableCell89.Text = "14. MÃ SỐ HÀNG HÓA";
            this.xrTableCell89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell81.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell81.ParentStyleUsing.UseFont = false;
            this.xrTableCell81.Size = new System.Drawing.Size(83, 41);
            this.xrTableCell81.Text = "15.ĐƠN VỊ TÍNH";
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell83.Location = new System.Drawing.Point(475, 0);
            this.xrTableCell83.Multiline = true;
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell83.ParentStyleUsing.UseFont = false;
            this.xrTableCell83.Size = new System.Drawing.Size(92, 41);
            this.xrTableCell83.Text = "20.LƯỢNG\r\n      ";
            this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell90.Location = new System.Drawing.Point(567, 0);
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell90.ParentStyleUsing.UseFont = false;
            this.xrTableCell90.Size = new System.Drawing.Size(105, 41);
            this.xrTableCell90.Text = "22. ĐƠN GIÁ NGUYÊN TỆ";
            this.xrTableCell90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell92.Location = new System.Drawing.Point(672, 0);
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell92.ParentStyleUsing.UseFont = false;
            this.xrTableCell92.Size = new System.Drawing.Size(95, 41);
            this.xrTableCell92.Text = "23. TRỊ GIÁ NGUYÊN TỆ ";
            this.xrTableCell92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable14
            // 
            this.xrTable14.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable14.Location = new System.Drawing.Point(25, 418);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.ParentStyleUsing.UseBorders = false;
            this.xrTable14.ParentStyleUsing.UseFont = false;
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18});
            this.xrTable14.Size = new System.Drawing.Size(767, 108);
            this.xrTable14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell65,
            this.xrTableCell79});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Size = new System.Drawing.Size(767, 108);
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell65.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel37,
            this.lblNguoiChiDinhNhanHang});
            this.xrTableCell65.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell65.ParentStyleUsing.UseBorders = false;
            this.xrTableCell65.Size = new System.Drawing.Size(392, 108);
            this.xrTableCell65.Text = " ";
            // 
            // xrLabel37
            // 
            this.xrLabel37.BorderWidth = 0;
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel37.Location = new System.Drawing.Point(8, 8);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel37.ParentStyleUsing.UseFont = false;
            this.xrLabel37.Size = new System.Drawing.Size(184, 25);
            this.xrLabel37.Text = "4. Người chỉ định nhận hàng";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNguoiChiDinhNhanHang
            // 
            this.lblNguoiChiDinhNhanHang.BorderWidth = 0;
            this.lblNguoiChiDinhNhanHang.CanGrow = false;
            this.lblNguoiChiDinhNhanHang.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblNguoiChiDinhNhanHang.Location = new System.Drawing.Point(17, 40);
            this.lblNguoiChiDinhNhanHang.Name = "lblNguoiChiDinhNhanHang";
            this.lblNguoiChiDinhNhanHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblNguoiChiDinhNhanHang.ParentStyleUsing.UseBorderWidth = false;
            this.lblNguoiChiDinhNhanHang.ParentStyleUsing.UseFont = false;
            this.lblNguoiChiDinhNhanHang.Size = new System.Drawing.Size(366, 59);
            this.lblNguoiChiDinhNhanHang.Text = " ";
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell79.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMaDailyLTT,
            this.lblTenDaiLyTTHQ,
            this.xrLabel41,
            this.xrTable15});
            this.xrTableCell79.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell79.ParentStyleUsing.UseBorders = false;
            this.xrTableCell79.Size = new System.Drawing.Size(375, 108);
            this.xrTableCell79.Text = " ";
            this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDailyLTT
            // 
            this.lblMaDailyLTT.BorderWidth = 0;
            this.lblMaDailyLTT.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDailyLTT.Location = new System.Drawing.Point(121, 3);
            this.lblMaDailyLTT.Name = "lblMaDailyLTT";
            this.lblMaDailyLTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDailyLTT.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaDailyLTT.ParentStyleUsing.UseFont = false;
            this.lblMaDailyLTT.Size = new System.Drawing.Size(248, 17);
            this.lblMaDailyLTT.Text = " ";
            this.lblMaDailyLTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTenDaiLyTTHQ
            // 
            this.lblTenDaiLyTTHQ.BorderWidth = 0;
            this.lblTenDaiLyTTHQ.CanGrow = false;
            this.lblTenDaiLyTTHQ.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblTenDaiLyTTHQ.Location = new System.Drawing.Point(25, 42);
            this.lblTenDaiLyTTHQ.Name = "lblTenDaiLyTTHQ";
            this.lblTenDaiLyTTHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDaiLyTTHQ.ParentStyleUsing.UseBorderWidth = false;
            this.lblTenDaiLyTTHQ.ParentStyleUsing.UseFont = false;
            this.lblTenDaiLyTTHQ.Size = new System.Drawing.Size(341, 50);
            // 
            // xrLabel41
            // 
            this.xrLabel41.BorderWidth = 0;
            this.xrLabel41.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel41.Location = new System.Drawing.Point(8, 6);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel41.ParentStyleUsing.UseFont = false;
            this.xrLabel41.Size = new System.Drawing.Size(100, 41);
            this.xrLabel41.Text = "12. Đại lý làm thủ tục";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable15
            // 
            this.xrTable15.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable15.Location = new System.Drawing.Point(117, 0);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.ParentStyleUsing.UseBorders = false;
            this.xrTable15.ParentStyleUsing.UseFont = false;
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19});
            this.xrTable15.Size = new System.Drawing.Size(260, 25);
            this.xrTable15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell70,
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell78});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Size = new System.Drawing.Size(260, 25);
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell66.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell66.ParentStyleUsing.UseBorders = false;
            this.xrTableCell66.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell66.Text = " ";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell67.Location = new System.Drawing.Point(20, 0);
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell67.ParentStyleUsing.UseBorders = false;
            this.xrTableCell67.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell67.Text = " ";
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell68.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMaDaiLyTTHQ});
            this.xrTableCell68.Location = new System.Drawing.Point(40, 0);
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell68.ParentStyleUsing.UseBorders = false;
            this.xrTableCell68.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell68.Text = " ";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDaiLyTTHQ
            // 
            this.lblMaDaiLyTTHQ.BorderWidth = 0;
            this.lblMaDaiLyTTHQ.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaDaiLyTTHQ.Location = new System.Drawing.Point(0, 0);
            this.lblMaDaiLyTTHQ.Name = "lblMaDaiLyTTHQ";
            this.lblMaDaiLyTTHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblMaDaiLyTTHQ.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaDaiLyTTHQ.ParentStyleUsing.UseFont = false;
            this.lblMaDaiLyTTHQ.Size = new System.Drawing.Size(260, 24);
            this.lblMaDaiLyTTHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell69.Location = new System.Drawing.Point(60, 0);
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell69.ParentStyleUsing.UseBorders = false;
            this.xrTableCell69.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell69.Text = " ";
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell70.Location = new System.Drawing.Point(80, 0);
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell70.ParentStyleUsing.UseBorders = false;
            this.xrTableCell70.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell70.Text = " ";
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell71.Location = new System.Drawing.Point(100, 0);
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell71.ParentStyleUsing.UseBorders = false;
            this.xrTableCell71.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell71.Text = " ";
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell72.Location = new System.Drawing.Point(120, 0);
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell72.ParentStyleUsing.UseBorders = false;
            this.xrTableCell72.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell72.Text = " ";
            this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell73.Location = new System.Drawing.Point(140, 0);
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell73.ParentStyleUsing.UseBorders = false;
            this.xrTableCell73.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell73.Text = " ";
            this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell74.Location = new System.Drawing.Point(160, 0);
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell74.ParentStyleUsing.UseBorders = false;
            this.xrTableCell74.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell74.Text = " ";
            this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell75.Location = new System.Drawing.Point(180, 0);
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell75.ParentStyleUsing.UseBorders = false;
            this.xrTableCell75.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell75.Text = " ";
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell76.Location = new System.Drawing.Point(200, 0);
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell76.ParentStyleUsing.UseBorders = false;
            this.xrTableCell76.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell76.Text = " ";
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell77.Location = new System.Drawing.Point(220, 0);
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell77.ParentStyleUsing.UseBorders = false;
            this.xrTableCell77.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell77.Text = " ";
            this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell78.Location = new System.Drawing.Point(240, 0);
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell78.ParentStyleUsing.UseBorders = false;
            this.xrTableCell78.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell78.Text = " ";
            this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable9
            // 
            this.xrTable9.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable9.Location = new System.Drawing.Point(25, 310);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.ParentStyleUsing.UseBorders = false;
            this.xrTable9.ParentStyleUsing.UseFont = false;
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable9.Size = new System.Drawing.Size(767, 108);
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell52});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Size = new System.Drawing.Size(767, 108);
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell38.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNguoiChiDinhGiaoHang,
            this.xrLabel36});
            this.xrTableCell38.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell38.ParentStyleUsing.UseBorders = false;
            this.xrTableCell38.Size = new System.Drawing.Size(392, 108);
            this.xrTableCell38.Text = "xrTableCell24";
            // 
            // lblNguoiChiDinhGiaoHang
            // 
            this.lblNguoiChiDinhGiaoHang.BorderWidth = 0;
            this.lblNguoiChiDinhGiaoHang.CanGrow = false;
            this.lblNguoiChiDinhGiaoHang.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblNguoiChiDinhGiaoHang.Location = new System.Drawing.Point(17, 31);
            this.lblNguoiChiDinhGiaoHang.Name = "lblNguoiChiDinhGiaoHang";
            this.lblNguoiChiDinhGiaoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblNguoiChiDinhGiaoHang.ParentStyleUsing.UseBorderWidth = false;
            this.lblNguoiChiDinhGiaoHang.ParentStyleUsing.UseFont = false;
            this.lblNguoiChiDinhGiaoHang.Size = new System.Drawing.Size(366, 66);
            // 
            // xrLabel36
            // 
            this.xrLabel36.BorderWidth = 0;
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.Location = new System.Drawing.Point(8, 2);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel36.ParentStyleUsing.UseFont = false;
            this.xrLabel36.Size = new System.Drawing.Size(217, 26);
            this.xrLabel36.Text = "3. Người chỉ định giao hàng";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell52.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblDiaDiemGiaoHang,
            this.xrLabel4});
            this.xrTableCell52.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell52.ParentStyleUsing.UseBorders = false;
            this.xrTableCell52.Size = new System.Drawing.Size(375, 108);
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblDiaDiemGiaoHang
            // 
            this.lblDiaDiemGiaoHang.BorderWidth = 0;
            this.lblDiaDiemGiaoHang.CanGrow = false;
            this.lblDiaDiemGiaoHang.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblDiaDiemGiaoHang.Location = new System.Drawing.Point(17, 32);
            this.lblDiaDiemGiaoHang.Name = "lblDiaDiemGiaoHang";
            this.lblDiaDiemGiaoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaDiemGiaoHang.ParentStyleUsing.UseBorderWidth = false;
            this.lblDiaDiemGiaoHang.ParentStyleUsing.UseFont = false;
            this.lblDiaDiemGiaoHang.Size = new System.Drawing.Size(350, 66);
            // 
            // xrLabel4
            // 
            this.xrLabel4.BorderWidth = 0;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.Location = new System.Drawing.Point(8, 2);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel4.ParentStyleUsing.UseFont = false;
            this.xrLabel4.Size = new System.Drawing.Size(150, 26);
            this.xrLabel4.Text = "11. Địa điểm giao hàng";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable7
            // 
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable7.Location = new System.Drawing.Point(25, 210);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.ParentStyleUsing.UseBorders = false;
            this.xrTable7.ParentStyleUsing.UseFont = false;
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable7.Size = new System.Drawing.Size(767, 100);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell23});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Size = new System.Drawing.Size(767, 100);
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNguoiNhan,
            this.xrTable8,
            this.lblTenDoiTac,
            this.xrLabel31});
            this.xrTableCell24.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell24.ParentStyleUsing.UseBorders = false;
            this.xrTableCell24.Size = new System.Drawing.Size(392, 100);
            this.xrTableCell24.Text = "xrTableCell24";
            // 
            // lblNguoiNhan
            // 
            this.lblNguoiNhan.BorderWidth = 0;
            this.lblNguoiNhan.Font = new System.Drawing.Font("Times New Roman", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguoiNhan.Location = new System.Drawing.Point(139, 1);
            this.lblNguoiNhan.Name = "lblNguoiNhan";
            this.lblNguoiNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNguoiNhan.ParentStyleUsing.UseBorderWidth = false;
            this.lblNguoiNhan.ParentStyleUsing.UseFont = false;
            this.lblNguoiNhan.Size = new System.Drawing.Size(250, 19);
            this.lblNguoiNhan.Text = "0400101242";
            this.lblNguoiNhan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable8
            // 
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable8.Location = new System.Drawing.Point(129, 0);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.ParentStyleUsing.UseBorders = false;
            this.xrTable8.ParentStyleUsing.UseFont = false;
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable8.Size = new System.Drawing.Size(260, 25);
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36,
            this.xrTableCell37});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Size = new System.Drawing.Size(260, 25);
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell25.ParentStyleUsing.UseBorders = false;
            this.xrTableCell25.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell26.Location = new System.Drawing.Point(20, 0);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell26.ParentStyleUsing.UseBorders = false;
            this.xrTableCell26.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell27.Location = new System.Drawing.Point(40, 0);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell27.ParentStyleUsing.UseBorders = false;
            this.xrTableCell27.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell28.Location = new System.Drawing.Point(60, 0);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell28.ParentStyleUsing.UseBorders = false;
            this.xrTableCell28.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell29.Location = new System.Drawing.Point(80, 0);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell29.ParentStyleUsing.UseBorders = false;
            this.xrTableCell29.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell29.Text = " ";
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell30.Location = new System.Drawing.Point(100, 0);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell30.ParentStyleUsing.UseBorders = false;
            this.xrTableCell30.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell30.Text = " ";
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell31.Location = new System.Drawing.Point(120, 0);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell31.ParentStyleUsing.UseBorders = false;
            this.xrTableCell31.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell31.Text = " ";
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell32.Location = new System.Drawing.Point(140, 0);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell32.ParentStyleUsing.UseBorders = false;
            this.xrTableCell32.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell32.Text = " ";
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell33.Location = new System.Drawing.Point(160, 0);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell33.ParentStyleUsing.UseBorders = false;
            this.xrTableCell33.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell33.Text = " ";
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Location = new System.Drawing.Point(180, 0);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell34.ParentStyleUsing.UseBorders = false;
            this.xrTableCell34.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell34.Text = " ";
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell35.Location = new System.Drawing.Point(200, 0);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell35.ParentStyleUsing.UseBorders = false;
            this.xrTableCell35.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell35.Text = " ";
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell36.Location = new System.Drawing.Point(220, 0);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell36.ParentStyleUsing.UseBorders = false;
            this.xrTableCell36.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell36.Text = " ";
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell37.Location = new System.Drawing.Point(240, 0);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell37.ParentStyleUsing.UseBorders = false;
            this.xrTableCell37.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell37.Text = " ";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblTenDoiTac
            // 
            this.lblTenDoiTac.BorderColor = System.Drawing.Color.Black;
            this.lblTenDoiTac.BorderWidth = 0;
            this.lblTenDoiTac.CanGrow = false;
            this.lblTenDoiTac.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenDoiTac.Location = new System.Drawing.Point(8, 40);
            this.lblTenDoiTac.Multiline = true;
            this.lblTenDoiTac.Name = "lblTenDoiTac";
            this.lblTenDoiTac.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenDoiTac.ParentStyleUsing.UseBorderColor = false;
            this.lblTenDoiTac.ParentStyleUsing.UseBorderWidth = false;
            this.lblTenDoiTac.ParentStyleUsing.UseFont = false;
            this.lblTenDoiTac.Size = new System.Drawing.Size(375, 51);
            // 
            // xrLabel31
            // 
            this.xrLabel31.BorderWidth = 0;
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.Location = new System.Drawing.Point(14, 4);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel31.ParentStyleUsing.UseFont = false;
            this.xrLabel31.Size = new System.Drawing.Size(100, 25);
            this.xrLabel31.Text = "2. Người nhận";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel10,
            this.xrLabel9,
            this.xrNhanMMTB,
            this.xrLabel7,
            this.xrNhanNLD,
            this.xrNhanSPGCCT,
            this.xrLabel3});
            this.xrTableCell6.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.ParentStyleUsing.UseBorders = false;
            this.xrTableCell6.Size = new System.Drawing.Size(116, 100);
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel10
            // 
            this.xrLabel10.BorderWidth = 0;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.Location = new System.Drawing.Point(8, 0);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel10.ParentStyleUsing.UseFont = false;
            this.xrLabel10.Size = new System.Drawing.Size(92, 17);
            this.xrLabel10.Text = "8. Loại hình:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.BorderWidth = 0;
            this.xrLabel9.CanGrow = false;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.Location = new System.Drawing.Point(25, 74);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel9.ParentStyleUsing.UseFont = false;
            this.xrLabel9.Size = new System.Drawing.Size(84, 11);
            this.xrLabel9.Text = "Nhận MM,TB";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrNhanMMTB
            // 
            this.xrNhanMMTB.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrNhanMMTB.CanGrow = false;
            this.xrNhanMMTB.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrNhanMMTB.Location = new System.Drawing.Point(8, 73);
            this.xrNhanMMTB.Name = "xrNhanMMTB";
            this.xrNhanMMTB.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrNhanMMTB.ParentStyleUsing.UseBorders = false;
            this.xrNhanMMTB.ParentStyleUsing.UseFont = false;
            this.xrNhanMMTB.Size = new System.Drawing.Size(13, 13);
            this.xrNhanMMTB.Text = " ";
            this.xrNhanMMTB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.BorderWidth = 0;
            this.xrLabel7.CanGrow = false;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.Location = new System.Drawing.Point(25, 50);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel7.ParentStyleUsing.UseFont = false;
            this.xrLabel7.Size = new System.Drawing.Size(75, 11);
            this.xrLabel7.Text = "Nhận NLD";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrNhanNLD
            // 
            this.xrNhanNLD.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrNhanNLD.CanGrow = false;
            this.xrNhanNLD.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrNhanNLD.Location = new System.Drawing.Point(8, 50);
            this.xrNhanNLD.Name = "xrNhanNLD";
            this.xrNhanNLD.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrNhanNLD.ParentStyleUsing.UseBorders = false;
            this.xrNhanNLD.ParentStyleUsing.UseFont = false;
            this.xrNhanNLD.Size = new System.Drawing.Size(13, 13);
            this.xrNhanNLD.Text = " ";
            this.xrNhanNLD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrNhanSPGCCT
            // 
            this.xrNhanSPGCCT.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrNhanSPGCCT.CanGrow = false;
            this.xrNhanSPGCCT.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrNhanSPGCCT.Location = new System.Drawing.Point(8, 25);
            this.xrNhanSPGCCT.Name = "xrNhanSPGCCT";
            this.xrNhanSPGCCT.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrNhanSPGCCT.ParentStyleUsing.UseBorders = false;
            this.xrNhanSPGCCT.ParentStyleUsing.UseBorderWidth = false;
            this.xrNhanSPGCCT.ParentStyleUsing.UseFont = false;
            this.xrNhanSPGCCT.Size = new System.Drawing.Size(13, 13);
            this.xrNhanSPGCCT.Text = " ";
            this.xrNhanSPGCCT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.BorderWidth = 0;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.Location = new System.Drawing.Point(25, 25);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel3.ParentStyleUsing.UseFont = false;
            this.xrLabel3.Size = new System.Drawing.Size(83, 16);
            this.xrLabel3.Text = "Nhận SPGCCT";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNgayHHHDNhan,
            this.lblNgayHDNhan,
            this.lblsoHDNhan,
            this.xrLabel32});
            this.xrTableCell7.Location = new System.Drawing.Point(508, 0);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell7.ParentStyleUsing.UseBorders = false;
            this.xrTableCell7.Size = new System.Drawing.Size(142, 100);
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayHHHDNhan
            // 
            this.lblNgayHHHDNhan.BorderWidth = 0;
            this.lblNgayHHHDNhan.CanGrow = false;
            this.lblNgayHHHDNhan.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHHHDNhan.Location = new System.Drawing.Point(12, 77);
            this.lblNgayHHHDNhan.Name = "lblNgayHHHDNhan";
            this.lblNgayHHHDNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHHHDNhan.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayHHHDNhan.ParentStyleUsing.UseFont = false;
            this.lblNgayHHHDNhan.Size = new System.Drawing.Size(125, 16);
            this.lblNgayHHHDNhan.Text = "Ngày hết hạn: ";
            // 
            // lblNgayHDNhan
            // 
            this.lblNgayHDNhan.BorderWidth = 0;
            this.lblNgayHDNhan.CanGrow = false;
            this.lblNgayHDNhan.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHDNhan.Location = new System.Drawing.Point(12, 54);
            this.lblNgayHDNhan.Name = "lblNgayHDNhan";
            this.lblNgayHDNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHDNhan.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayHDNhan.ParentStyleUsing.UseFont = false;
            this.lblNgayHDNhan.Size = new System.Drawing.Size(108, 17);
            this.lblNgayHDNhan.Text = "Ngày: ";
            // 
            // lblsoHDNhan
            // 
            this.lblsoHDNhan.BorderWidth = 0;
            this.lblsoHDNhan.CanGrow = false;
            this.lblsoHDNhan.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsoHDNhan.Location = new System.Drawing.Point(12, 25);
            this.lblsoHDNhan.Name = "lblsoHDNhan";
            this.lblsoHDNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblsoHDNhan.ParentStyleUsing.UseBorderWidth = false;
            this.lblsoHDNhan.ParentStyleUsing.UseFont = false;
            this.lblsoHDNhan.Size = new System.Drawing.Size(116, 25);
            this.lblsoHDNhan.Text = "Số  : ";
            // 
            // xrLabel32
            // 
            this.xrLabel32.BorderWidth = 0;
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.Location = new System.Drawing.Point(12, 0);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel32.ParentStyleUsing.UseFont = false;
            this.xrLabel32.Size = new System.Drawing.Size(113, 25);
            this.xrLabel32.Text = "9. HĐGC nhận :";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNgayHHPKHDNhan,
            this.xrLabel71,
            this.xrLabel70,
            this.xrLabel69});
            this.xrTableCell23.Location = new System.Drawing.Point(650, 0);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell23.ParentStyleUsing.UseBorders = false;
            this.xrTableCell23.Size = new System.Drawing.Size(117, 100);
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblNgayHHPKHDNhan
            // 
            this.lblNgayHHPKHDNhan.BorderWidth = 0;
            this.lblNgayHHPKHDNhan.CanGrow = false;
            this.lblNgayHHPKHDNhan.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHHPKHDNhan.Location = new System.Drawing.Point(8, 69);
            this.lblNgayHHPKHDNhan.Name = "lblNgayHHPKHDNhan";
            this.lblNgayHHPKHDNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHHPKHDNhan.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayHHPKHDNhan.ParentStyleUsing.UseFont = false;
            this.lblNgayHHPKHDNhan.Size = new System.Drawing.Size(92, 16);
            this.lblNgayHHPKHDNhan.Text = "Ngày hết hạn:";
            // 
            // xrLabel71
            // 
            this.xrLabel71.BorderWidth = 0;
            this.xrLabel71.CanGrow = false;
            this.xrLabel71.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel71.Location = new System.Drawing.Point(8, 50);
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel71.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel71.ParentStyleUsing.UseFont = false;
            this.xrLabel71.Size = new System.Drawing.Size(92, 15);
            this.xrLabel71.Text = "Ngày:";
            // 
            // xrLabel70
            // 
            this.xrLabel70.BorderWidth = 0;
            this.xrLabel70.CanGrow = false;
            this.xrLabel70.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel70.Location = new System.Drawing.Point(8, 25);
            this.xrLabel70.Name = "xrLabel70";
            this.xrLabel70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel70.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel70.ParentStyleUsing.UseFont = false;
            this.xrLabel70.Size = new System.Drawing.Size(100, 25);
            this.xrLabel70.Text = "Số  :";
            // 
            // xrLabel69
            // 
            this.xrLabel69.BorderWidth = 0;
            this.xrLabel69.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel69.Location = new System.Drawing.Point(7, 0);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel69.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel69.ParentStyleUsing.UseFont = false;
            this.xrLabel69.Size = new System.Drawing.Size(100, 17);
            this.xrLabel69.Text = "10. PKHĐGC nhận :";
            this.xrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5.Location = new System.Drawing.Point(25, 115);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.ParentStyleUsing.UseBorders = false;
            this.xrTable5.ParentStyleUsing.UseFont = false;
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable5.Size = new System.Drawing.Size(767, 95);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell4,
            this.xrTableCell11});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Size = new System.Drawing.Size(767, 95);
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNguoiGiao,
            this.xrTable6,
            this.lblTenDoanhNghiep1,
            this.xrLabel11});
            this.xrTableCell8.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell8.ParentStyleUsing.UseBorders = false;
            this.xrTableCell8.Size = new System.Drawing.Size(392, 95);
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNguoiGiao
            // 
            this.lblNguoiGiao.BorderWidth = 0;
            this.lblNguoiGiao.Font = new System.Drawing.Font("Times New Roman", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguoiGiao.Location = new System.Drawing.Point(140, 4);
            this.lblNguoiGiao.Name = "lblNguoiGiao";
            this.lblNguoiGiao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNguoiGiao.ParentStyleUsing.UseBorderWidth = false;
            this.lblNguoiGiao.ParentStyleUsing.UseFont = false;
            this.lblNguoiGiao.Size = new System.Drawing.Size(250, 17);
            this.lblNguoiGiao.Text = "0400101242";
            this.lblNguoiGiao.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable6
            // 
            this.xrTable6.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable6.Location = new System.Drawing.Point(130, 0);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.ParentStyleUsing.UseBorders = false;
            this.xrTable6.ParentStyleUsing.UseFont = false;
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable6.Size = new System.Drawing.Size(260, 25);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell12,
            this.xrTableCell17,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell14,
            this.xrTableCell18,
            this.xrTableCell20,
            this.xrTableCell19,
            this.xrTableCell22,
            this.xrTableCell21,
            this.xrTableCell5,
            this.xrTableCell13});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Size = new System.Drawing.Size(260, 25);
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell10.ParentStyleUsing.UseBorders = false;
            this.xrTableCell10.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell12.Location = new System.Drawing.Point(20, 0);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell12.ParentStyleUsing.UseBorders = false;
            this.xrTableCell12.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell17.Location = new System.Drawing.Point(40, 0);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell17.ParentStyleUsing.UseBorders = false;
            this.xrTableCell17.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Location = new System.Drawing.Point(60, 0);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell15.ParentStyleUsing.UseBorders = false;
            this.xrTableCell15.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.Location = new System.Drawing.Point(80, 0);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell16.ParentStyleUsing.UseBorders = false;
            this.xrTableCell16.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell14.Location = new System.Drawing.Point(100, 0);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell14.ParentStyleUsing.UseBorders = false;
            this.xrTableCell14.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell18.Location = new System.Drawing.Point(120, 0);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell18.ParentStyleUsing.UseBorders = false;
            this.xrTableCell18.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell20.Location = new System.Drawing.Point(140, 0);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell20.ParentStyleUsing.UseBorders = false;
            this.xrTableCell20.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell19.Location = new System.Drawing.Point(160, 0);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell19.ParentStyleUsing.UseBorders = false;
            this.xrTableCell19.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell22.Location = new System.Drawing.Point(180, 0);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell22.ParentStyleUsing.UseBorders = false;
            this.xrTableCell22.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell21.Location = new System.Drawing.Point(200, 0);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell21.ParentStyleUsing.UseBorders = false;
            this.xrTableCell21.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Location = new System.Drawing.Point(220, 0);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.ParentStyleUsing.UseBorders = false;
            this.xrTableCell5.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Location = new System.Drawing.Point(240, 0);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell13.ParentStyleUsing.UseBorders = false;
            this.xrTableCell13.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblTenDoanhNghiep1
            // 
            this.lblTenDoanhNghiep1.BorderWidth = 0;
            this.lblTenDoanhNghiep1.CanGrow = false;
            this.lblTenDoanhNghiep1.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenDoanhNghiep1.Location = new System.Drawing.Point(8, 33);
            this.lblTenDoanhNghiep1.Multiline = true;
            this.lblTenDoanhNghiep1.Name = "lblTenDoanhNghiep1";
            this.lblTenDoanhNghiep1.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenDoanhNghiep1.ParentStyleUsing.UseBorderWidth = false;
            this.lblTenDoanhNghiep1.ParentStyleUsing.UseFont = false;
            this.lblTenDoanhNghiep1.Size = new System.Drawing.Size(375, 58);
            // 
            // xrLabel11
            // 
            this.xrLabel11.BorderWidth = 0;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.Location = new System.Drawing.Point(9, 2);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel11.ParentStyleUsing.UseFont = false;
            this.xrLabel11.Size = new System.Drawing.Size(102, 25);
            this.xrLabel11.Text = "1. Người giao";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrGiaoMMTB,
            this.xrGiaoNLD,
            this.xrGiaoSPGCCT,
            this.xrLabel19,
            this.xrLabel17,
            this.xrLabel14,
            this.xrLabel13});
            this.xrTableCell9.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell9.ParentStyleUsing.UseBorders = false;
            this.xrTableCell9.Size = new System.Drawing.Size(116, 95);
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrGiaoMMTB
            // 
            this.xrGiaoMMTB.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrGiaoMMTB.CanGrow = false;
            this.xrGiaoMMTB.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrGiaoMMTB.Location = new System.Drawing.Point(5, 68);
            this.xrGiaoMMTB.Name = "xrGiaoMMTB";
            this.xrGiaoMMTB.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrGiaoMMTB.ParentStyleUsing.UseBorders = false;
            this.xrGiaoMMTB.ParentStyleUsing.UseFont = false;
            this.xrGiaoMMTB.Size = new System.Drawing.Size(13, 13);
            this.xrGiaoMMTB.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrGiaoNLD
            // 
            this.xrGiaoNLD.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrGiaoNLD.CanGrow = false;
            this.xrGiaoNLD.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrGiaoNLD.Location = new System.Drawing.Point(5, 45);
            this.xrGiaoNLD.Name = "xrGiaoNLD";
            this.xrGiaoNLD.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrGiaoNLD.ParentStyleUsing.UseBorders = false;
            this.xrGiaoNLD.ParentStyleUsing.UseFont = false;
            this.xrGiaoNLD.Size = new System.Drawing.Size(13, 13);
            this.xrGiaoNLD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrGiaoSPGCCT
            // 
            this.xrGiaoSPGCCT.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrGiaoSPGCCT.CanGrow = false;
            this.xrGiaoSPGCCT.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrGiaoSPGCCT.Location = new System.Drawing.Point(5, 24);
            this.xrGiaoSPGCCT.Name = "xrGiaoSPGCCT";
            this.xrGiaoSPGCCT.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrGiaoSPGCCT.ParentStyleUsing.UseBorders = false;
            this.xrGiaoSPGCCT.ParentStyleUsing.UseBorderWidth = false;
            this.xrGiaoSPGCCT.ParentStyleUsing.UseFont = false;
            this.xrGiaoSPGCCT.Size = new System.Drawing.Size(13, 13);
            this.xrGiaoSPGCCT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel19
            // 
            this.xrLabel19.BorderWidth = 0;
            this.xrLabel19.CanGrow = false;
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.Location = new System.Drawing.Point(23, 69);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel19.ParentStyleUsing.UseFont = false;
            this.xrLabel19.Size = new System.Drawing.Size(75, 11);
            this.xrLabel19.Text = "Giao MM,TB";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.BorderWidth = 0;
            this.xrLabel17.CanGrow = false;
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.Location = new System.Drawing.Point(25, 46);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel17.ParentStyleUsing.UseFont = false;
            this.xrLabel17.Size = new System.Drawing.Size(75, 11);
            this.xrLabel17.Text = "Giao NLD";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.BorderWidth = 0;
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.Location = new System.Drawing.Point(25, 22);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel14.ParentStyleUsing.UseFont = false;
            this.xrLabel14.Size = new System.Drawing.Size(83, 16);
            this.xrLabel14.Text = "Giao SPGCCT";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.BorderWidth = 0;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.Location = new System.Drawing.Point(5, 0);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel13.ParentStyleUsing.UseFont = false;
            this.xrLabel13.Size = new System.Drawing.Size(102, 25);
            this.xrLabel13.Text = "5. Loại hình:";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblSoHDGiao,
            this.lblNgayHHHDGiao,
            this.xrLabel28,
            this.lblNgayHDGiao});
            this.xrTableCell4.Location = new System.Drawing.Point(508, 0);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell4.ParentStyleUsing.UseBorders = false;
            this.xrTableCell4.Size = new System.Drawing.Size(142, 95);
            // 
            // lblSoHDGiao
            // 
            this.lblSoHDGiao.BorderWidth = 0;
            this.lblSoHDGiao.CanGrow = false;
            this.lblSoHDGiao.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHDGiao.Location = new System.Drawing.Point(8, 25);
            this.lblSoHDGiao.Name = "lblSoHDGiao";
            this.lblSoHDGiao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoHDGiao.ParentStyleUsing.UseBorderWidth = false;
            this.lblSoHDGiao.ParentStyleUsing.UseFont = false;
            this.lblSoHDGiao.Size = new System.Drawing.Size(125, 25);
            this.lblSoHDGiao.Text = "Số  : ";
            // 
            // lblNgayHHHDGiao
            // 
            this.lblNgayHHHDGiao.BorderWidth = 0;
            this.lblNgayHHHDGiao.CanGrow = false;
            this.lblNgayHHHDGiao.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHHHDGiao.Location = new System.Drawing.Point(8, 72);
            this.lblNgayHHHDGiao.Name = "lblNgayHHHDGiao";
            this.lblNgayHHHDGiao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHHHDGiao.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayHHHDGiao.ParentStyleUsing.UseFont = false;
            this.lblNgayHHHDGiao.Size = new System.Drawing.Size(116, 17);
            this.lblNgayHHHDGiao.Text = "Ngày hết hạn: ";
            // 
            // xrLabel28
            // 
            this.xrLabel28.BorderWidth = 0;
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.Location = new System.Drawing.Point(5, 0);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel28.ParentStyleUsing.UseFont = false;
            this.xrLabel28.Size = new System.Drawing.Size(113, 25);
            this.xrLabel28.Text = "6.HĐGC giao :";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNgayHDGiao
            // 
            this.lblNgayHDGiao.BorderWidth = 0;
            this.lblNgayHDGiao.CanGrow = false;
            this.lblNgayHDGiao.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHDGiao.Location = new System.Drawing.Point(8, 51);
            this.lblNgayHDGiao.Name = "lblNgayHDGiao";
            this.lblNgayHDGiao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHDGiao.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayHDGiao.ParentStyleUsing.UseFont = false;
            this.lblNgayHDGiao.Size = new System.Drawing.Size(108, 17);
            this.lblNgayHDGiao.Text = "Ngày: ";
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNgayHHPKHDGiao,
            this.lblNgayHD,
            this.lblSoPKHDGiao,
            this.xrLabel30});
            this.xrTableCell11.Location = new System.Drawing.Point(650, 0);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell11.ParentStyleUsing.UseBorders = false;
            this.xrTableCell11.Size = new System.Drawing.Size(117, 95);
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblNgayHHPKHDGiao
            // 
            this.lblNgayHHPKHDGiao.BorderWidth = 0;
            this.lblNgayHHPKHDGiao.CanGrow = false;
            this.lblNgayHHPKHDGiao.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHHPKHDGiao.Location = new System.Drawing.Point(8, 69);
            this.lblNgayHHPKHDGiao.Name = "lblNgayHHPKHDGiao";
            this.lblNgayHHPKHDGiao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHHPKHDGiao.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayHHPKHDGiao.ParentStyleUsing.UseFont = false;
            this.lblNgayHHPKHDGiao.Size = new System.Drawing.Size(92, 16);
            this.lblNgayHHPKHDGiao.Text = "Ngày hết hạn:";
            // 
            // lblNgayHD
            // 
            this.lblNgayHD.BorderWidth = 0;
            this.lblNgayHD.CanGrow = false;
            this.lblNgayHD.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHD.Location = new System.Drawing.Point(8, 51);
            this.lblNgayHD.Name = "lblNgayHD";
            this.lblNgayHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHD.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayHD.ParentStyleUsing.UseFont = false;
            this.lblNgayHD.Size = new System.Drawing.Size(92, 15);
            this.lblNgayHD.Text = "Ngày:";
            // 
            // lblSoPKHDGiao
            // 
            this.lblSoPKHDGiao.BorderWidth = 0;
            this.lblSoPKHDGiao.CanGrow = false;
            this.lblSoPKHDGiao.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoPKHDGiao.Location = new System.Drawing.Point(8, 27);
            this.lblSoPKHDGiao.Name = "lblSoPKHDGiao";
            this.lblSoPKHDGiao.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoPKHDGiao.ParentStyleUsing.UseBorderWidth = false;
            this.lblSoPKHDGiao.ParentStyleUsing.UseFont = false;
            this.lblSoPKHDGiao.Size = new System.Drawing.Size(94, 20);
            this.lblSoPKHDGiao.Text = "Số  :";
            // 
            // xrLabel30
            // 
            this.xrLabel30.BorderWidth = 0;
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.Location = new System.Drawing.Point(8, 2);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel30.ParentStyleUsing.UseFont = false;
            this.xrLabel30.Size = new System.Drawing.Size(87, 25);
            this.xrLabel30.Text = "7. PKHĐGC giao :";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblBanLuuHaiQuan
            // 
            this.lblBanLuuHaiQuan.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBanLuuHaiQuan.Location = new System.Drawing.Point(316, 33);
            this.lblBanLuuHaiQuan.Name = "lblBanLuuHaiQuan";
            this.lblBanLuuHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanLuuHaiQuan.ParentStyleUsing.UseFont = false;
            this.lblBanLuuHaiQuan.Size = new System.Drawing.Size(216, 17);
            this.lblBanLuuHaiQuan.Text = "Bản lưu người khai Hải quan";
            this.lblBanLuuHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.Location = new System.Drawing.Point(700, 33);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.ParentStyleUsing.UseFont = false;
            this.xrLabel2.Size = new System.Drawing.Size(92, 17);
            this.xrLabel2.Text = "HQ/2008-GCCT";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.Location = new System.Drawing.Point(233, 8);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.ParentStyleUsing.UseFont = false;
            this.xrLabel1.Size = new System.Drawing.Size(403, 25);
            this.xrLabel1.Text = "TỜ KHAI HÀNG GIA CÔNG CHUYỂN TIẾP";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.Location = new System.Drawing.Point(25, 8);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.ParentStyleUsing.UseFont = false;
            this.xrLabel23.Size = new System.Drawing.Size(175, 25);
            this.xrLabel23.Text = "HẢI QUAN VIỆT NAM";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoTiepNhan
            // 
            this.lblSoTiepNhan.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblSoTiepNhan.Location = new System.Drawing.Point(650, 8);
            this.lblSoTiepNhan.Name = "lblSoTiepNhan";
            this.lblSoTiepNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTiepNhan.ParentStyleUsing.UseFont = false;
            this.lblSoTiepNhan.Size = new System.Drawing.Size(142, 25);
            // 
            // xrTable1
            // 
            this.xrTable1.BorderColor = System.Drawing.Color.Black;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable1.Location = new System.Drawing.Point(25, 567);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.ParentStyleUsing.UseBackColor = false;
            this.xrTable1.ParentStyleUsing.UseBorderColor = false;
            this.xrTable1.ParentStyleUsing.UseBorders = false;
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow10,
            this.xrTableRow12,
            this.xrTableRow11,
            this.xrTableRow15,
            this.xrTableRow21,
            this.xrTableRow17,
            this.xrTableRow25,
            this.xrTableRow24,
            this.xrTableRow23,
            this.xrTableRow16,
            this.xrTableRow22,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow30,
            this.xrTableRow29,
            this.xrTableRow28});
            this.xrTable1.Size = new System.Drawing.Size(767, 475);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.TenHang1,
            this.MaHS1,
            this.DVT1,
            this.Luong1,
            this.DonGiaNT1,
            this.TriGiaNT1});
            this.xrTableRow1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.ParentStyleUsing.UseFont = false;
            this.xrTableRow1.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell93.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrTableCell93.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell93.ParentStyleUsing.UseBorders = false;
            this.xrTableCell93.ParentStyleUsing.UseFont = false;
            this.xrTableCell93.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell93.Text = "1";
            this.xrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang1
            // 
            this.TenHang1.BackColor = System.Drawing.Color.White;
            this.TenHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang1.CanGrow = false;
            this.TenHang1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TenHang1.Location = new System.Drawing.Point(25, 0);
            this.TenHang1.Name = "TenHang1";
            this.TenHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.TenHang1.ParentStyleUsing.UseBackColor = false;
            this.TenHang1.ParentStyleUsing.UseBorders = false;
            this.TenHang1.ParentStyleUsing.UseFont = false;
            this.TenHang1.Size = new System.Drawing.Size(283, 25);
            this.TenHang1.Tag = "Tên hàng 1";
            this.TenHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang1.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS1
            // 
            this.MaHS1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.MaHS1.Location = new System.Drawing.Point(308, 0);
            this.MaHS1.Name = "MaHS1";
            this.MaHS1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS1.ParentStyleUsing.UseBorders = false;
            this.MaHS1.ParentStyleUsing.UseFont = false;
            this.MaHS1.Size = new System.Drawing.Size(84, 25);
            this.MaHS1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT1
            // 
            this.DVT1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DVT1.Location = new System.Drawing.Point(392, 0);
            this.DVT1.Name = "DVT1";
            this.DVT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT1.ParentStyleUsing.UseBorders = false;
            this.DVT1.ParentStyleUsing.UseFont = false;
            this.DVT1.Size = new System.Drawing.Size(83, 25);
            this.DVT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong1
            // 
            this.Luong1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.Luong1.Location = new System.Drawing.Point(475, 0);
            this.Luong1.Name = "Luong1";
            this.Luong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong1.ParentStyleUsing.UseBorders = false;
            this.Luong1.ParentStyleUsing.UseFont = false;
            this.Luong1.Size = new System.Drawing.Size(92, 25);
            this.Luong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT1
            // 
            this.DonGiaNT1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DonGiaNT1.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT1.Name = "DonGiaNT1";
            this.DonGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT1.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT1.ParentStyleUsing.UseFont = false;
            this.DonGiaNT1.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT1
            // 
            this.TriGiaNT1.BackColor = System.Drawing.Color.White;
            this.TriGiaNT1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaNT1.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT1.Name = "TriGiaNT1";
            this.TriGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT1.ParentStyleUsing.UseBackColor = false;
            this.TriGiaNT1.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT1.ParentStyleUsing.UseFont = false;
            this.TriGiaNT1.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT1.Tag = "Trị giá NT 1";
            this.TriGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT1.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell94,
            this.TenHang2,
            this.MaHS2,
            this.DVT2,
            this.Luong2,
            this.DonGiaNT2,
            this.TriGiaNT2});
            this.xrTableRow2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.ParentStyleUsing.UseFont = false;
            this.xrTableRow2.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell94.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrTableCell94.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell94.ParentStyleUsing.UseBorders = false;
            this.xrTableCell94.ParentStyleUsing.UseFont = false;
            this.xrTableCell94.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell94.Text = "2";
            this.xrTableCell94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang2
            // 
            this.TenHang2.BackColor = System.Drawing.Color.White;
            this.TenHang2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang2.CanGrow = false;
            this.TenHang2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TenHang2.Location = new System.Drawing.Point(25, 0);
            this.TenHang2.Name = "TenHang2";
            this.TenHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.TenHang2.ParentStyleUsing.UseBackColor = false;
            this.TenHang2.ParentStyleUsing.UseBorders = false;
            this.TenHang2.ParentStyleUsing.UseFont = false;
            this.TenHang2.Size = new System.Drawing.Size(283, 25);
            this.TenHang2.Tag = "Tên hàng 2";
            this.TenHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang2.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS2
            // 
            this.MaHS2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.MaHS2.Location = new System.Drawing.Point(308, 0);
            this.MaHS2.Name = "MaHS2";
            this.MaHS2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS2.ParentStyleUsing.UseBorders = false;
            this.MaHS2.ParentStyleUsing.UseFont = false;
            this.MaHS2.Size = new System.Drawing.Size(84, 25);
            this.MaHS2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT2
            // 
            this.DVT2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DVT2.Location = new System.Drawing.Point(392, 0);
            this.DVT2.Name = "DVT2";
            this.DVT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT2.ParentStyleUsing.UseBorders = false;
            this.DVT2.ParentStyleUsing.UseFont = false;
            this.DVT2.Size = new System.Drawing.Size(83, 25);
            this.DVT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong2
            // 
            this.Luong2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.Luong2.Location = new System.Drawing.Point(475, 0);
            this.Luong2.Name = "Luong2";
            this.Luong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong2.ParentStyleUsing.UseBorders = false;
            this.Luong2.ParentStyleUsing.UseFont = false;
            this.Luong2.Size = new System.Drawing.Size(92, 25);
            this.Luong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT2
            // 
            this.DonGiaNT2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DonGiaNT2.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT2.Name = "DonGiaNT2";
            this.DonGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT2.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT2.ParentStyleUsing.UseFont = false;
            this.DonGiaNT2.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT2
            // 
            this.TriGiaNT2.BackColor = System.Drawing.Color.White;
            this.TriGiaNT2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaNT2.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT2.Name = "TriGiaNT2";
            this.TriGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT2.ParentStyleUsing.UseBackColor = false;
            this.TriGiaNT2.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT2.ParentStyleUsing.UseFont = false;
            this.TriGiaNT2.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT2.Tag = "Trị giá NT 2";
            this.TriGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT2.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell95,
            this.TenHang3,
            this.MaHS3,
            this.DVT3,
            this.Luong3,
            this.DonGiaNT3,
            this.TriGiaNT3});
            this.xrTableRow3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.ParentStyleUsing.UseFont = false;
            this.xrTableRow3.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell95.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrTableCell95.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell95.ParentStyleUsing.UseBorders = false;
            this.xrTableCell95.ParentStyleUsing.UseFont = false;
            this.xrTableCell95.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell95.Text = "3";
            this.xrTableCell95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang3
            // 
            this.TenHang3.BackColor = System.Drawing.Color.White;
            this.TenHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang3.CanGrow = false;
            this.TenHang3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TenHang3.Location = new System.Drawing.Point(25, 0);
            this.TenHang3.Name = "TenHang3";
            this.TenHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.TenHang3.ParentStyleUsing.UseBackColor = false;
            this.TenHang3.ParentStyleUsing.UseBorders = false;
            this.TenHang3.ParentStyleUsing.UseFont = false;
            this.TenHang3.Size = new System.Drawing.Size(283, 25);
            this.TenHang3.Tag = "Tên hàng 3";
            this.TenHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang3.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS3
            // 
            this.MaHS3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.MaHS3.Location = new System.Drawing.Point(308, 0);
            this.MaHS3.Name = "MaHS3";
            this.MaHS3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS3.ParentStyleUsing.UseBorders = false;
            this.MaHS3.ParentStyleUsing.UseFont = false;
            this.MaHS3.Size = new System.Drawing.Size(84, 25);
            this.MaHS3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT3
            // 
            this.DVT3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DVT3.Location = new System.Drawing.Point(392, 0);
            this.DVT3.Name = "DVT3";
            this.DVT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT3.ParentStyleUsing.UseBorders = false;
            this.DVT3.ParentStyleUsing.UseFont = false;
            this.DVT3.Size = new System.Drawing.Size(83, 25);
            this.DVT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong3
            // 
            this.Luong3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.Luong3.Location = new System.Drawing.Point(475, 0);
            this.Luong3.Name = "Luong3";
            this.Luong3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong3.ParentStyleUsing.UseBorders = false;
            this.Luong3.ParentStyleUsing.UseFont = false;
            this.Luong3.Size = new System.Drawing.Size(92, 25);
            this.Luong3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT3
            // 
            this.DonGiaNT3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DonGiaNT3.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT3.Name = "DonGiaNT3";
            this.DonGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT3.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT3.ParentStyleUsing.UseFont = false;
            this.DonGiaNT3.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT3
            // 
            this.TriGiaNT3.BackColor = System.Drawing.Color.White;
            this.TriGiaNT3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaNT3.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT3.Name = "TriGiaNT3";
            this.TriGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT3.ParentStyleUsing.UseBackColor = false;
            this.TriGiaNT3.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT3.ParentStyleUsing.UseFont = false;
            this.TriGiaNT3.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT3.Tag = "Trị giá NT 3";
            this.TriGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT3.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell53,
            this.TenHang4,
            this.MaHS4,
            this.DVT4,
            this.Luong4,
            this.DonGiaNT4,
            this.TriGiaNT4});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell53.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell53.ParentStyleUsing.UseBorders = false;
            this.xrTableCell53.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell53.Text = " 4";
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang4
            // 
            this.TenHang4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang4.Location = new System.Drawing.Point(25, 0);
            this.TenHang4.Name = "TenHang4";
            this.TenHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang4.ParentStyleUsing.UseBorders = false;
            this.TenHang4.Size = new System.Drawing.Size(283, 25);
            this.TenHang4.Text = " ";
            // 
            // MaHS4
            // 
            this.MaHS4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS4.Location = new System.Drawing.Point(308, 0);
            this.MaHS4.Name = "MaHS4";
            this.MaHS4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS4.ParentStyleUsing.UseBorders = false;
            this.MaHS4.Size = new System.Drawing.Size(84, 25);
            this.MaHS4.Text = " ";
            this.MaHS4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT4
            // 
            this.DVT4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT4.Location = new System.Drawing.Point(392, 0);
            this.DVT4.Name = "DVT4";
            this.DVT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT4.ParentStyleUsing.UseBorders = false;
            this.DVT4.Size = new System.Drawing.Size(83, 25);
            this.DVT4.Text = " ";
            this.DVT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong4
            // 
            this.Luong4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong4.Location = new System.Drawing.Point(475, 0);
            this.Luong4.Name = "Luong4";
            this.Luong4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong4.ParentStyleUsing.UseBorders = false;
            this.Luong4.Size = new System.Drawing.Size(92, 25);
            this.Luong4.Text = " ";
            this.Luong4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT4
            // 
            this.DonGiaNT4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT4.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT4.Name = "DonGiaNT4";
            this.DonGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT4.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT4.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT4.Text = " ";
            this.DonGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT4
            // 
            this.TriGiaNT4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT4.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT4.Name = "TriGiaNT4";
            this.TriGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT4.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT4.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT4.Text = " ";
            this.TriGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.TenHang5,
            this.MaHS5,
            this.DVT5,
            this.Luong5,
            this.DonGiaNT5,
            this.TriGiaNT5});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell85.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell85.ParentStyleUsing.UseBorders = false;
            this.xrTableCell85.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell85.Text = " 5";
            this.xrTableCell85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang5
            // 
            this.TenHang5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang5.Location = new System.Drawing.Point(25, 0);
            this.TenHang5.Name = "TenHang5";
            this.TenHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang5.ParentStyleUsing.UseBorders = false;
            this.TenHang5.Size = new System.Drawing.Size(283, 25);
            this.TenHang5.Text = " ";
            // 
            // MaHS5
            // 
            this.MaHS5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS5.Location = new System.Drawing.Point(308, 0);
            this.MaHS5.Name = "MaHS5";
            this.MaHS5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS5.ParentStyleUsing.UseBorders = false;
            this.MaHS5.Size = new System.Drawing.Size(84, 25);
            this.MaHS5.Text = " ";
            this.MaHS5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT5
            // 
            this.DVT5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT5.Location = new System.Drawing.Point(392, 0);
            this.DVT5.Name = "DVT5";
            this.DVT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT5.ParentStyleUsing.UseBorders = false;
            this.DVT5.Size = new System.Drawing.Size(83, 25);
            this.DVT5.Text = " ";
            this.DVT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong5
            // 
            this.Luong5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong5.Location = new System.Drawing.Point(475, 0);
            this.Luong5.Name = "Luong5";
            this.Luong5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong5.ParentStyleUsing.UseBorders = false;
            this.Luong5.Size = new System.Drawing.Size(92, 25);
            this.Luong5.Text = " ";
            this.Luong5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT5
            // 
            this.DonGiaNT5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT5.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT5.Name = "DonGiaNT5";
            this.DonGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT5.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT5.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT5.Text = " ";
            this.DonGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT5
            // 
            this.TriGiaNT5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT5.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT5.Name = "TriGiaNT5";
            this.TriGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT5.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT5.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT5.Text = " ";
            this.TriGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell60,
            this.TenHang6,
            this.MaHS6,
            this.DVT6,
            this.Luong6,
            this.DonGiaNT6,
            this.TriGiaNT6});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell60.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell60.ParentStyleUsing.UseBorders = false;
            this.xrTableCell60.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell60.Text = " 6";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang6
            // 
            this.TenHang6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang6.Location = new System.Drawing.Point(25, 0);
            this.TenHang6.Name = "TenHang6";
            this.TenHang6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang6.ParentStyleUsing.UseBorders = false;
            this.TenHang6.Size = new System.Drawing.Size(283, 25);
            this.TenHang6.Text = " ";
            // 
            // MaHS6
            // 
            this.MaHS6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS6.Location = new System.Drawing.Point(308, 0);
            this.MaHS6.Name = "MaHS6";
            this.MaHS6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS6.ParentStyleUsing.UseBorders = false;
            this.MaHS6.Size = new System.Drawing.Size(84, 25);
            this.MaHS6.Text = " ";
            this.MaHS6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT6
            // 
            this.DVT6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT6.Location = new System.Drawing.Point(392, 0);
            this.DVT6.Name = "DVT6";
            this.DVT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT6.ParentStyleUsing.UseBorders = false;
            this.DVT6.Size = new System.Drawing.Size(83, 25);
            this.DVT6.Text = " ";
            this.DVT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong6
            // 
            this.Luong6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong6.Location = new System.Drawing.Point(475, 0);
            this.Luong6.Name = "Luong6";
            this.Luong6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong6.ParentStyleUsing.UseBorders = false;
            this.Luong6.Size = new System.Drawing.Size(92, 25);
            this.Luong6.Text = " ";
            this.Luong6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT6
            // 
            this.DonGiaNT6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT6.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT6.Name = "DonGiaNT6";
            this.DonGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT6.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT6.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT6.Text = " ";
            this.DonGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT6
            // 
            this.TriGiaNT6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT6.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT6.Name = "TriGiaNT6";
            this.TriGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT6.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT6.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT6.Text = " ";
            this.TriGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell99,
            this.TenHang7,
            this.MaHS7,
            this.DVT7,
            this.Luong7,
            this.DonGiaNT7,
            this.TriGiaNT7});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell99.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell99.ParentStyleUsing.UseBorders = false;
            this.xrTableCell99.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell99.Text = " 7";
            this.xrTableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang7
            // 
            this.TenHang7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang7.Location = new System.Drawing.Point(25, 0);
            this.TenHang7.Name = "TenHang7";
            this.TenHang7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang7.ParentStyleUsing.UseBorders = false;
            this.TenHang7.Size = new System.Drawing.Size(283, 25);
            this.TenHang7.Text = " ";
            // 
            // MaHS7
            // 
            this.MaHS7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS7.Location = new System.Drawing.Point(308, 0);
            this.MaHS7.Name = "MaHS7";
            this.MaHS7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS7.ParentStyleUsing.UseBorders = false;
            this.MaHS7.Size = new System.Drawing.Size(84, 25);
            this.MaHS7.Text = " ";
            this.MaHS7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT7
            // 
            this.DVT7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT7.Location = new System.Drawing.Point(392, 0);
            this.DVT7.Name = "DVT7";
            this.DVT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT7.ParentStyleUsing.UseBorders = false;
            this.DVT7.Size = new System.Drawing.Size(83, 25);
            this.DVT7.Text = " ";
            this.DVT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong7
            // 
            this.Luong7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong7.Location = new System.Drawing.Point(475, 0);
            this.Luong7.Name = "Luong7";
            this.Luong7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong7.ParentStyleUsing.UseBorders = false;
            this.Luong7.Size = new System.Drawing.Size(92, 25);
            this.Luong7.Text = " ";
            this.Luong7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT7
            // 
            this.DonGiaNT7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT7.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT7.Name = "DonGiaNT7";
            this.DonGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT7.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT7.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT7.Text = " ";
            this.DonGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT7
            // 
            this.TriGiaNT7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT7.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT7.Name = "TriGiaNT7";
            this.TriGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT7.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT7.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT7.Text = " ";
            this.TriGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell120,
            this.TenHang8,
            this.MaHS8,
            this.DVT8,
            this.Luong8,
            this.DonGiaNT8,
            this.TriGiaNT8});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell120.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell120.ParentStyleUsing.UseBorders = false;
            this.xrTableCell120.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell120.Text = " 8";
            this.xrTableCell120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang8
            // 
            this.TenHang8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang8.Location = new System.Drawing.Point(25, 0);
            this.TenHang8.Name = "TenHang8";
            this.TenHang8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang8.ParentStyleUsing.UseBorders = false;
            this.TenHang8.Size = new System.Drawing.Size(283, 25);
            this.TenHang8.Text = " ";
            // 
            // MaHS8
            // 
            this.MaHS8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS8.Location = new System.Drawing.Point(308, 0);
            this.MaHS8.Name = "MaHS8";
            this.MaHS8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS8.ParentStyleUsing.UseBorders = false;
            this.MaHS8.Size = new System.Drawing.Size(84, 25);
            this.MaHS8.Text = " ";
            this.MaHS8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT8
            // 
            this.DVT8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT8.Location = new System.Drawing.Point(392, 0);
            this.DVT8.Name = "DVT8";
            this.DVT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT8.ParentStyleUsing.UseBorders = false;
            this.DVT8.Size = new System.Drawing.Size(83, 25);
            this.DVT8.Text = " ";
            this.DVT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong8
            // 
            this.Luong8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong8.Location = new System.Drawing.Point(475, 0);
            this.Luong8.Name = "Luong8";
            this.Luong8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong8.ParentStyleUsing.UseBorders = false;
            this.Luong8.Size = new System.Drawing.Size(92, 25);
            this.Luong8.Text = " ";
            this.Luong8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT8
            // 
            this.DonGiaNT8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT8.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT8.Name = "DonGiaNT8";
            this.DonGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT8.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT8.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT8.Text = " ";
            this.DonGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT8
            // 
            this.TriGiaNT8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT8.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT8.Name = "TriGiaNT8";
            this.TriGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT8.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT8.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT8.Text = " ";
            this.TriGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell113,
            this.TenHang9,
            this.MaHS9,
            this.DVT9,
            this.Luong9,
            this.DonGiaNT9,
            this.TriGiaNT9});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell113.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell113.ParentStyleUsing.UseBorders = false;
            this.xrTableCell113.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell113.Text = " 9";
            this.xrTableCell113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang9
            // 
            this.TenHang9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang9.Location = new System.Drawing.Point(25, 0);
            this.TenHang9.Name = "TenHang9";
            this.TenHang9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang9.ParentStyleUsing.UseBorders = false;
            this.TenHang9.Size = new System.Drawing.Size(283, 25);
            this.TenHang9.Text = " ";
            // 
            // MaHS9
            // 
            this.MaHS9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS9.Location = new System.Drawing.Point(308, 0);
            this.MaHS9.Name = "MaHS9";
            this.MaHS9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS9.ParentStyleUsing.UseBorders = false;
            this.MaHS9.Size = new System.Drawing.Size(84, 25);
            this.MaHS9.Text = " ";
            this.MaHS9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT9
            // 
            this.DVT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT9.Location = new System.Drawing.Point(392, 0);
            this.DVT9.Name = "DVT9";
            this.DVT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT9.ParentStyleUsing.UseBorders = false;
            this.DVT9.Size = new System.Drawing.Size(83, 25);
            this.DVT9.Text = " ";
            this.DVT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong9
            // 
            this.Luong9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong9.Location = new System.Drawing.Point(475, 0);
            this.Luong9.Name = "Luong9";
            this.Luong9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong9.ParentStyleUsing.UseBorders = false;
            this.Luong9.Size = new System.Drawing.Size(92, 25);
            this.Luong9.Text = " ";
            this.Luong9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT9
            // 
            this.DonGiaNT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT9.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT9.Name = "DonGiaNT9";
            this.DonGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT9.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT9.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT9.Text = " ";
            this.DonGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT9
            // 
            this.TriGiaNT9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT9.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT9.Name = "TriGiaNT9";
            this.TriGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT9.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT9.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT9.Text = " ";
            this.TriGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell226,
            this.TenHang10,
            this.MaHS10,
            this.DVT10,
            this.Luong10,
            this.DonGiaNT10,
            this.TriGiaNT10});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell226.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell226.ParentStyleUsing.UseBorders = false;
            this.xrTableCell226.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell226.Text = " 10";
            this.xrTableCell226.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang10
            // 
            this.TenHang10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang10.Location = new System.Drawing.Point(25, 0);
            this.TenHang10.Name = "TenHang10";
            this.TenHang10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang10.ParentStyleUsing.UseBorders = false;
            this.TenHang10.Size = new System.Drawing.Size(283, 25);
            this.TenHang10.Text = " ";
            // 
            // MaHS10
            // 
            this.MaHS10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS10.Location = new System.Drawing.Point(308, 0);
            this.MaHS10.Name = "MaHS10";
            this.MaHS10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS10.ParentStyleUsing.UseBorders = false;
            this.MaHS10.Size = new System.Drawing.Size(84, 25);
            this.MaHS10.Text = " ";
            this.MaHS10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT10
            // 
            this.DVT10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT10.Location = new System.Drawing.Point(392, 0);
            this.DVT10.Name = "DVT10";
            this.DVT10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT10.ParentStyleUsing.UseBorders = false;
            this.DVT10.Size = new System.Drawing.Size(83, 25);
            this.DVT10.Text = " ";
            this.DVT10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong10
            // 
            this.Luong10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong10.Location = new System.Drawing.Point(475, 0);
            this.Luong10.Name = "Luong10";
            this.Luong10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong10.ParentStyleUsing.UseBorders = false;
            this.Luong10.Size = new System.Drawing.Size(92, 25);
            this.Luong10.Text = " ";
            this.Luong10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT10
            // 
            this.DonGiaNT10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT10.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT10.Name = "DonGiaNT10";
            this.DonGiaNT10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT10.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT10.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT10.Text = " ";
            this.DonGiaNT10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT10
            // 
            this.TriGiaNT10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT10.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT10.Name = "TriGiaNT10";
            this.TriGiaNT10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT10.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT10.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT10.Text = " ";
            this.TriGiaNT10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell219,
            this.TenHang11,
            this.MaHS11,
            this.DVT11,
            this.Luong11,
            this.DonGiaNT11,
            this.TriGiaNT11});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell219.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell219.ParentStyleUsing.UseBorders = false;
            this.xrTableCell219.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell219.Text = " 11";
            this.xrTableCell219.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang11
            // 
            this.TenHang11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang11.Location = new System.Drawing.Point(25, 0);
            this.TenHang11.Name = "TenHang11";
            this.TenHang11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang11.ParentStyleUsing.UseBorders = false;
            this.TenHang11.Size = new System.Drawing.Size(283, 25);
            this.TenHang11.Text = " ";
            // 
            // MaHS11
            // 
            this.MaHS11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS11.Location = new System.Drawing.Point(308, 0);
            this.MaHS11.Name = "MaHS11";
            this.MaHS11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS11.ParentStyleUsing.UseBorders = false;
            this.MaHS11.Size = new System.Drawing.Size(84, 25);
            this.MaHS11.Text = " ";
            this.MaHS11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT11
            // 
            this.DVT11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT11.Location = new System.Drawing.Point(392, 0);
            this.DVT11.Name = "DVT11";
            this.DVT11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT11.ParentStyleUsing.UseBorders = false;
            this.DVT11.Size = new System.Drawing.Size(83, 25);
            this.DVT11.Text = " ";
            this.DVT11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong11
            // 
            this.Luong11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong11.Location = new System.Drawing.Point(475, 0);
            this.Luong11.Name = "Luong11";
            this.Luong11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong11.ParentStyleUsing.UseBorders = false;
            this.Luong11.Size = new System.Drawing.Size(92, 25);
            this.Luong11.Text = " ";
            this.Luong11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT11
            // 
            this.DonGiaNT11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT11.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT11.Name = "DonGiaNT11";
            this.DonGiaNT11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT11.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT11.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT11.Text = " ";
            this.DonGiaNT11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT11
            // 
            this.TriGiaNT11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT11.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT11.Name = "TriGiaNT11";
            this.TriGiaNT11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT11.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT11.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT11.Text = " ";
            this.TriGiaNT11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell153,
            this.TenHang12,
            this.MaHS12,
            this.DVT12,
            this.Luong12,
            this.DonGiaNT12,
            this.TriGiaNT12});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell153.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell153.ParentStyleUsing.UseBorders = false;
            this.xrTableCell153.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell153.Text = " 12";
            this.xrTableCell153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang12
            // 
            this.TenHang12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang12.Location = new System.Drawing.Point(25, 0);
            this.TenHang12.Name = "TenHang12";
            this.TenHang12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang12.ParentStyleUsing.UseBorders = false;
            this.TenHang12.Size = new System.Drawing.Size(283, 25);
            this.TenHang12.Text = " ";
            // 
            // MaHS12
            // 
            this.MaHS12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS12.Location = new System.Drawing.Point(308, 0);
            this.MaHS12.Name = "MaHS12";
            this.MaHS12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS12.ParentStyleUsing.UseBorders = false;
            this.MaHS12.Size = new System.Drawing.Size(84, 25);
            this.MaHS12.Text = " ";
            this.MaHS12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT12
            // 
            this.DVT12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT12.Location = new System.Drawing.Point(392, 0);
            this.DVT12.Name = "DVT12";
            this.DVT12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT12.ParentStyleUsing.UseBorders = false;
            this.DVT12.Size = new System.Drawing.Size(83, 25);
            this.DVT12.Text = " ";
            this.DVT12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong12
            // 
            this.Luong12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong12.Location = new System.Drawing.Point(475, 0);
            this.Luong12.Name = "Luong12";
            this.Luong12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong12.ParentStyleUsing.UseBorders = false;
            this.Luong12.Size = new System.Drawing.Size(92, 25);
            this.Luong12.Text = " ";
            this.Luong12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT12
            // 
            this.DonGiaNT12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT12.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT12.Name = "DonGiaNT12";
            this.DonGiaNT12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT12.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT12.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT12.Text = " ";
            this.DonGiaNT12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT12
            // 
            this.TriGiaNT12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT12.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT12.Name = "TriGiaNT12";
            this.TriGiaNT12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT12.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT12.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT12.Text = " ";
            this.TriGiaNT12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell106,
            this.TenHang13,
            this.MaHS13,
            this.DVT13,
            this.Luong13,
            this.DonGiaNT13,
            this.TriGiaNT13});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell106.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell106.ParentStyleUsing.UseBorders = false;
            this.xrTableCell106.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell106.Text = " 13";
            this.xrTableCell106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang13
            // 
            this.TenHang13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang13.Location = new System.Drawing.Point(25, 0);
            this.TenHang13.Name = "TenHang13";
            this.TenHang13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang13.ParentStyleUsing.UseBorders = false;
            this.TenHang13.Size = new System.Drawing.Size(283, 25);
            this.TenHang13.Text = " ";
            // 
            // MaHS13
            // 
            this.MaHS13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS13.Location = new System.Drawing.Point(308, 0);
            this.MaHS13.Name = "MaHS13";
            this.MaHS13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS13.ParentStyleUsing.UseBorders = false;
            this.MaHS13.Size = new System.Drawing.Size(84, 25);
            this.MaHS13.Text = " ";
            this.MaHS13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT13
            // 
            this.DVT13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT13.Location = new System.Drawing.Point(392, 0);
            this.DVT13.Name = "DVT13";
            this.DVT13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT13.ParentStyleUsing.UseBorders = false;
            this.DVT13.Size = new System.Drawing.Size(83, 25);
            this.DVT13.Text = " ";
            this.DVT13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong13
            // 
            this.Luong13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong13.Location = new System.Drawing.Point(475, 0);
            this.Luong13.Name = "Luong13";
            this.Luong13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong13.ParentStyleUsing.UseBorders = false;
            this.Luong13.Size = new System.Drawing.Size(92, 25);
            this.Luong13.Text = " ";
            this.Luong13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT13
            // 
            this.DonGiaNT13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT13.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT13.Name = "DonGiaNT13";
            this.DonGiaNT13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT13.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT13.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT13.Text = " ";
            this.DonGiaNT13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT13
            // 
            this.TriGiaNT13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT13.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT13.Name = "TriGiaNT13";
            this.TriGiaNT13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT13.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT13.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT13.Text = " ";
            this.TriGiaNT13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell127,
            this.TenHang14,
            this.MaHS14,
            this.DVT14,
            this.Luong14,
            this.DonGiaNT14,
            this.TriGiaNT14});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell127.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell127.ParentStyleUsing.UseBorders = false;
            this.xrTableCell127.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell127.Text = " 14";
            this.xrTableCell127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang14
            // 
            this.TenHang14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang14.Location = new System.Drawing.Point(25, 0);
            this.TenHang14.Name = "TenHang14";
            this.TenHang14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang14.ParentStyleUsing.UseBorders = false;
            this.TenHang14.Size = new System.Drawing.Size(283, 25);
            this.TenHang14.Text = " ";
            // 
            // MaHS14
            // 
            this.MaHS14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS14.Location = new System.Drawing.Point(308, 0);
            this.MaHS14.Name = "MaHS14";
            this.MaHS14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS14.ParentStyleUsing.UseBorders = false;
            this.MaHS14.Size = new System.Drawing.Size(84, 25);
            this.MaHS14.Text = " ";
            this.MaHS14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT14
            // 
            this.DVT14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT14.Location = new System.Drawing.Point(392, 0);
            this.DVT14.Name = "DVT14";
            this.DVT14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT14.ParentStyleUsing.UseBorders = false;
            this.DVT14.Size = new System.Drawing.Size(83, 25);
            this.DVT14.Text = " ";
            this.DVT14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong14
            // 
            this.Luong14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong14.Location = new System.Drawing.Point(475, 0);
            this.Luong14.Name = "Luong14";
            this.Luong14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong14.ParentStyleUsing.UseBorders = false;
            this.Luong14.Size = new System.Drawing.Size(92, 25);
            this.Luong14.Text = " ";
            this.Luong14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT14
            // 
            this.DonGiaNT14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT14.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT14.Name = "DonGiaNT14";
            this.DonGiaNT14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT14.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT14.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT14.Text = " ";
            this.DonGiaNT14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT14
            // 
            this.TriGiaNT14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT14.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT14.Name = "TriGiaNT14";
            this.TriGiaNT14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT14.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT14.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT14.Text = " ";
            this.TriGiaNT14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell233,
            this.TenHang15,
            this.MaHS15,
            this.DVT15,
            this.Luong15,
            this.DonGiaNT15,
            this.TriGiaNT15});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell233.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell233.ParentStyleUsing.UseBorders = false;
            this.xrTableCell233.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell233.Text = " 15";
            this.xrTableCell233.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang15
            // 
            this.TenHang15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang15.Location = new System.Drawing.Point(25, 0);
            this.TenHang15.Name = "TenHang15";
            this.TenHang15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang15.ParentStyleUsing.UseBorders = false;
            this.TenHang15.Size = new System.Drawing.Size(283, 25);
            this.TenHang15.Text = " ";
            // 
            // MaHS15
            // 
            this.MaHS15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS15.Location = new System.Drawing.Point(308, 0);
            this.MaHS15.Name = "MaHS15";
            this.MaHS15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS15.ParentStyleUsing.UseBorders = false;
            this.MaHS15.Size = new System.Drawing.Size(84, 25);
            this.MaHS15.Text = " ";
            this.MaHS15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT15
            // 
            this.DVT15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT15.Location = new System.Drawing.Point(392, 0);
            this.DVT15.Name = "DVT15";
            this.DVT15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT15.ParentStyleUsing.UseBorders = false;
            this.DVT15.Size = new System.Drawing.Size(83, 25);
            this.DVT15.Text = " ";
            this.DVT15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong15
            // 
            this.Luong15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong15.Location = new System.Drawing.Point(475, 0);
            this.Luong15.Name = "Luong15";
            this.Luong15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong15.ParentStyleUsing.UseBorders = false;
            this.Luong15.Size = new System.Drawing.Size(92, 25);
            this.Luong15.Text = " ";
            this.Luong15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT15
            // 
            this.DonGiaNT15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT15.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT15.Name = "DonGiaNT15";
            this.DonGiaNT15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT15.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT15.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT15.Text = " ";
            this.DonGiaNT15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT15
            // 
            this.TriGiaNT15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT15.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT15.Name = "TriGiaNT15";
            this.TriGiaNT15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT15.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT15.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT15.Text = " ";
            this.TriGiaNT15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell240,
            this.TenHang16,
            this.MaHS16,
            this.DVT16,
            this.Luong16,
            this.DonGiaNT16,
            this.TriGiaNT16});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell240.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell240.ParentStyleUsing.UseBorders = false;
            this.xrTableCell240.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell240.Text = " 16";
            this.xrTableCell240.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang16
            // 
            this.TenHang16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang16.Location = new System.Drawing.Point(25, 0);
            this.TenHang16.Name = "TenHang16";
            this.TenHang16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang16.ParentStyleUsing.UseBorders = false;
            this.TenHang16.Size = new System.Drawing.Size(283, 25);
            this.TenHang16.Text = " ";
            // 
            // MaHS16
            // 
            this.MaHS16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS16.Location = new System.Drawing.Point(308, 0);
            this.MaHS16.Name = "MaHS16";
            this.MaHS16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS16.ParentStyleUsing.UseBorders = false;
            this.MaHS16.Size = new System.Drawing.Size(84, 25);
            this.MaHS16.Text = " ";
            this.MaHS16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT16
            // 
            this.DVT16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT16.Location = new System.Drawing.Point(392, 0);
            this.DVT16.Name = "DVT16";
            this.DVT16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT16.ParentStyleUsing.UseBorders = false;
            this.DVT16.Size = new System.Drawing.Size(83, 25);
            this.DVT16.Text = " ";
            this.DVT16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong16
            // 
            this.Luong16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong16.Location = new System.Drawing.Point(475, 0);
            this.Luong16.Name = "Luong16";
            this.Luong16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong16.ParentStyleUsing.UseBorders = false;
            this.Luong16.Size = new System.Drawing.Size(92, 25);
            this.Luong16.Text = " ";
            this.Luong16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT16
            // 
            this.DonGiaNT16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT16.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT16.Name = "DonGiaNT16";
            this.DonGiaNT16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT16.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT16.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT16.Text = " ";
            this.DonGiaNT16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT16
            // 
            this.TriGiaNT16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT16.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT16.Name = "TriGiaNT16";
            this.TriGiaNT16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT16.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT16.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT16.Text = " ";
            this.TriGiaNT16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell261,
            this.TenHang17,
            this.MaHS17,
            this.DVT17,
            this.Luong17,
            this.DonGiaNT17,
            this.TriGiaNT17});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell261.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell261.ParentStyleUsing.UseBorders = false;
            this.xrTableCell261.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell261.Text = " 17";
            this.xrTableCell261.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang17
            // 
            this.TenHang17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang17.Location = new System.Drawing.Point(25, 0);
            this.TenHang17.Name = "TenHang17";
            this.TenHang17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang17.ParentStyleUsing.UseBorders = false;
            this.TenHang17.Size = new System.Drawing.Size(283, 25);
            this.TenHang17.Text = " ";
            // 
            // MaHS17
            // 
            this.MaHS17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS17.Location = new System.Drawing.Point(308, 0);
            this.MaHS17.Name = "MaHS17";
            this.MaHS17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS17.ParentStyleUsing.UseBorders = false;
            this.MaHS17.Size = new System.Drawing.Size(84, 25);
            this.MaHS17.Text = " ";
            this.MaHS17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT17
            // 
            this.DVT17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT17.Location = new System.Drawing.Point(392, 0);
            this.DVT17.Name = "DVT17";
            this.DVT17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT17.ParentStyleUsing.UseBorders = false;
            this.DVT17.Size = new System.Drawing.Size(83, 25);
            this.DVT17.Text = " ";
            this.DVT17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong17
            // 
            this.Luong17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong17.Location = new System.Drawing.Point(475, 0);
            this.Luong17.Name = "Luong17";
            this.Luong17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong17.ParentStyleUsing.UseBorders = false;
            this.Luong17.Size = new System.Drawing.Size(92, 25);
            this.Luong17.Text = " ";
            this.Luong17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT17
            // 
            this.DonGiaNT17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT17.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT17.Name = "DonGiaNT17";
            this.DonGiaNT17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT17.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT17.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT17.Text = " ";
            this.DonGiaNT17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT17
            // 
            this.TriGiaNT17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT17.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT17.Name = "TriGiaNT17";
            this.TriGiaNT17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT17.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT17.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT17.Text = " ";
            this.TriGiaNT17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell254,
            this.TenHang18,
            this.MaHS18,
            this.DVT18,
            this.Luong18,
            this.DonGiaNT18,
            this.TriGiaNT18});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell254.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell254.ParentStyleUsing.UseBorders = false;
            this.xrTableCell254.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell254.Text = " 18";
            this.xrTableCell254.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang18
            // 
            this.TenHang18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang18.Location = new System.Drawing.Point(25, 0);
            this.TenHang18.Name = "TenHang18";
            this.TenHang18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang18.ParentStyleUsing.UseBorders = false;
            this.TenHang18.Size = new System.Drawing.Size(283, 25);
            this.TenHang18.Text = " ";
            // 
            // MaHS18
            // 
            this.MaHS18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS18.Location = new System.Drawing.Point(308, 0);
            this.MaHS18.Name = "MaHS18";
            this.MaHS18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS18.ParentStyleUsing.UseBorders = false;
            this.MaHS18.Size = new System.Drawing.Size(84, 25);
            this.MaHS18.Text = " ";
            this.MaHS18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT18
            // 
            this.DVT18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT18.Location = new System.Drawing.Point(392, 0);
            this.DVT18.Name = "DVT18";
            this.DVT18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT18.ParentStyleUsing.UseBorders = false;
            this.DVT18.Size = new System.Drawing.Size(83, 25);
            this.DVT18.Text = " ";
            this.DVT18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong18
            // 
            this.Luong18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong18.Location = new System.Drawing.Point(475, 0);
            this.Luong18.Name = "Luong18";
            this.Luong18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong18.ParentStyleUsing.UseBorders = false;
            this.Luong18.Size = new System.Drawing.Size(92, 25);
            this.Luong18.Text = " ";
            this.Luong18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT18
            // 
            this.DonGiaNT18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT18.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT18.Name = "DonGiaNT18";
            this.DonGiaNT18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT18.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT18.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT18.Text = " ";
            this.DonGiaNT18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT18
            // 
            this.TriGiaNT18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT18.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT18.Name = "TriGiaNT18";
            this.TriGiaNT18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT18.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT18.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT18.Text = " ";
            this.TriGiaNT18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell247,
            this.TenHang19,
            this.MaHS19,
            this.DVT19,
            this.Luong19,
            this.DonGiaNT19,
            this.TriGiaNT19});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell247.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell247.ParentStyleUsing.UseBorders = false;
            this.xrTableCell247.Size = new System.Drawing.Size(25, 25);
            this.xrTableCell247.Text = "19";
            this.xrTableCell247.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang19
            // 
            this.TenHang19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TenHang19.Location = new System.Drawing.Point(25, 0);
            this.TenHang19.Name = "TenHang19";
            this.TenHang19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang19.ParentStyleUsing.UseBorders = false;
            this.TenHang19.Size = new System.Drawing.Size(283, 25);
            this.TenHang19.Text = " ";
            // 
            // MaHS19
            // 
            this.MaHS19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.MaHS19.Location = new System.Drawing.Point(308, 0);
            this.MaHS19.Name = "MaHS19";
            this.MaHS19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS19.ParentStyleUsing.UseBorders = false;
            this.MaHS19.Size = new System.Drawing.Size(84, 25);
            this.MaHS19.Text = " ";
            this.MaHS19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DVT19
            // 
            this.DVT19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DVT19.Location = new System.Drawing.Point(392, 0);
            this.DVT19.Name = "DVT19";
            this.DVT19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT19.ParentStyleUsing.UseBorders = false;
            this.DVT19.Size = new System.Drawing.Size(83, 25);
            this.DVT19.Text = " ";
            this.DVT19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong19
            // 
            this.Luong19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Luong19.Location = new System.Drawing.Point(475, 0);
            this.Luong19.Name = "Luong19";
            this.Luong19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong19.ParentStyleUsing.UseBorders = false;
            this.Luong19.Size = new System.Drawing.Size(92, 25);
            this.Luong19.Text = " ";
            this.Luong19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DonGiaNT19
            // 
            this.DonGiaNT19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.DonGiaNT19.Location = new System.Drawing.Point(567, 0);
            this.DonGiaNT19.Name = "DonGiaNT19";
            this.DonGiaNT19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT19.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT19.Size = new System.Drawing.Size(105, 25);
            this.DonGiaNT19.Text = " ";
            this.DonGiaNT19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT19
            // 
            this.TriGiaNT19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.TriGiaNT19.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT19.Name = "TriGiaNT19";
            this.TriGiaNT19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT19.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT19.Size = new System.Drawing.Size(95, 25);
            this.TriGiaNT19.Text = " ";
            this.TriGiaNT19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblSoBanChinh3
            // 
            this.lblSoBanChinh3.Location = new System.Drawing.Point(192, 67);
            this.lblSoBanChinh3.Name = "lblSoBanChinh3";
            this.lblSoBanChinh3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblSoBanChinh3.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanChinh2
            // 
            this.lblSoBanChinh2.Location = new System.Drawing.Point(192, 42);
            this.lblSoBanChinh2.Name = "lblSoBanChinh2";
            this.lblSoBanChinh2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblSoBanChinh2.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanChinh1
            // 
            this.lblSoBanChinh1.Location = new System.Drawing.Point(192, 92);
            this.lblSoBanChinh1.Name = "lblSoBanChinh1";
            this.lblSoBanChinh1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblSoBanChinh1.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanSao1
            // 
            this.lblSoBanSao1.Location = new System.Drawing.Point(292, 42);
            this.lblSoBanSao1.Name = "lblSoBanSao1";
            this.lblSoBanSao1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblSoBanSao1.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanSao1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanSao2
            // 
            this.lblSoBanSao2.Location = new System.Drawing.Point(292, 67);
            this.lblSoBanSao2.Name = "lblSoBanSao2";
            this.lblSoBanSao2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblSoBanSao2.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanSao2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanSao3
            // 
            this.lblSoBanSao3.Location = new System.Drawing.Point(292, 92);
            this.lblSoBanSao3.Name = "lblSoBanSao3";
            this.lblSoBanSao3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblSoBanSao3.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanSao3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoHopDong
            // 
            this.lblSoHopDong.CanGrow = false;
            this.lblSoHopDong.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoHopDong.Location = new System.Drawing.Point(186, 18);
            this.lblSoHopDong.Multiline = true;
            this.lblSoHopDong.Name = "lblSoHopDong";
            this.lblSoHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblSoHopDong.ParentStyleUsing.UseFont = false;
            this.lblSoHopDong.Size = new System.Drawing.Size(100, 33);
            this.lblSoHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayHHHopDong
            // 
            this.lblNgayHHHopDong.Location = new System.Drawing.Point(186, 58);
            this.lblNgayHHHopDong.Name = "lblNgayHHHopDong";
            this.lblNgayHHHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayHHHopDong.Size = new System.Drawing.Size(66, 17);
            this.lblNgayHHHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayHopDong
            // 
            this.lblNgayHopDong.Location = new System.Drawing.Point(183, 42);
            this.lblNgayHopDong.Name = "lblNgayHopDong";
            this.lblNgayHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayHopDong.Size = new System.Drawing.Size(83, 16);
            // 
            // lblSoGiayPhep
            // 
            this.lblSoGiayPhep.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoGiayPhep.Location = new System.Drawing.Point(3, 33);
            this.lblSoGiayPhep.Name = "lblSoGiayPhep";
            this.lblSoGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblSoGiayPhep.ParentStyleUsing.UseFont = false;
            this.lblSoGiayPhep.Size = new System.Drawing.Size(91, 33);
            // 
            // lblNgayGiayPhep
            // 
            this.lblNgayGiayPhep.Location = new System.Drawing.Point(95, 33);
            this.lblNgayGiayPhep.Name = "lblNgayGiayPhep";
            this.lblNgayGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayGiayPhep.Size = new System.Drawing.Size(91, 17);
            // 
            // lblNgayHHGiayPhep
            // 
            this.lblNgayHHGiayPhep.Location = new System.Drawing.Point(103, 58);
            this.lblNgayHHGiayPhep.Name = "lblNgayHHGiayPhep";
            this.lblNgayHHGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayHHGiayPhep.Size = new System.Drawing.Size(75, 17);
            this.lblNgayHHGiayPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTenDoanhNghiep
            // 
            this.lblTenDoanhNghiep.BorderWidth = 0;
            this.lblTenDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenDoanhNghiep.Location = new System.Drawing.Point(17, 30);
            this.lblTenDoanhNghiep.Multiline = true;
            this.lblTenDoanhNghiep.Name = "lblTenDoanhNghiep";
            this.lblTenDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenDoanhNghiep.ParentStyleUsing.UseBorderWidth = false;
            this.lblTenDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblTenDoanhNghiep.Size = new System.Drawing.Size(353, 58);
            this.lblTenDoanhNghiep.Text = "CÔNG TY CỔ PHẦN DỆT MAY 19/3\r\n60 MẸ NHU THANH KHÊ ĐÀ NẴNG\r\nMID CODE: VNMARTEX478D" +
                "AN";
            this.lblTenDoanhNghiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDoanhNghiep
            // 
            this.lblMaDoanhNghiep.BorderWidth = 0;
            this.lblMaDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaDoanhNghiep.Location = new System.Drawing.Point(111, 2);
            this.lblMaDoanhNghiep.Name = "lblMaDoanhNghiep";
            this.lblMaDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblMaDoanhNghiep.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblMaDoanhNghiep.Size = new System.Drawing.Size(260, 24);
            // 
            // xrLabel12
            // 
            this.xrLabel12.BorderWidth = 0;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.Location = new System.Drawing.Point(8, 0);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel12.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel12.ParentStyleUsing.UseFont = false;
            this.xrLabel12.Size = new System.Drawing.Size(102, 17);
            this.xrLabel12.Text = "5. Loại hình: ";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel26
            // 
            this.xrLabel26.BorderWidth = 0;
            this.xrLabel26.Location = new System.Drawing.Point(8, 8);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel26.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel26.Size = new System.Drawing.Size(375, 42);
            this.xrLabel26.Text = "21. Người giao hàng : Cam kết đã giao đúng, đủ các sản phẩm kê khai này và chịu t" +
                "rách nhiệm về nội dung khai trên tờ khai này.";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel48
            // 
            this.xrLabel48.BorderWidth = 0;
            this.xrLabel48.Location = new System.Drawing.Point(183, 50);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel48.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel48.Size = new System.Drawing.Size(184, 20);
            this.xrLabel48.Text = "Ngày          tháng         năm  200...";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel49
            // 
            this.xrLabel49.BorderWidth = 0;
            this.xrLabel49.Location = new System.Drawing.Point(183, 142);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel49.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel49.Size = new System.Drawing.Size(184, 20);
            this.xrLabel49.Text = "(Ký tên, đóng dấu, ghi rõ họ tên)";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel53
            // 
            this.xrLabel53.BorderWidth = 0;
            this.xrLabel53.Location = new System.Drawing.Point(0, 8);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel53.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel53.Size = new System.Drawing.Size(375, 42);
            this.xrLabel53.Text = "21. Người nhận hàng : Cam kết đã nhận đúng, đủ các sản phẩm kê khai này và chịu t" +
                "rách nhiệm về nội dung khai trên tờ khai này.";
            this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel54
            // 
            this.xrLabel54.BorderWidth = 0;
            this.xrLabel54.Location = new System.Drawing.Point(167, 50);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel54.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel54.Size = new System.Drawing.Size(184, 20);
            this.xrLabel54.Text = "Ngày          tháng         năm  200...";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel55
            // 
            this.xrLabel55.BorderWidth = 0;
            this.xrLabel55.Location = new System.Drawing.Point(167, 142);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel55.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel55.Size = new System.Drawing.Size(184, 20);
            this.xrLabel55.Text = "(Ký tên, đóng dấu, ghi rõ họ tên)";
            this.xrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel56
            // 
            this.xrLabel56.BorderWidth = 0;
            this.xrLabel56.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel56.Location = new System.Drawing.Point(258, 58);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel56.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel56.ParentStyleUsing.UseFont = false;
            this.xrLabel56.Size = new System.Drawing.Size(84, 25);
            this.xrLabel56.Text = "................. ";
            this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel57
            // 
            this.xrLabel57.BorderWidth = 0;
            this.xrLabel57.Location = new System.Drawing.Point(258, 18);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel57.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel57.Size = new System.Drawing.Size(75, 20);
            this.xrLabel57.Text = "Bản sao";
            this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel58
            // 
            this.xrLabel58.BorderWidth = 0;
            this.xrLabel58.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel58.Location = new System.Drawing.Point(258, 33);
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel58.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel58.ParentStyleUsing.UseFont = false;
            this.xrLabel58.Size = new System.Drawing.Size(92, 26);
            this.xrLabel58.Text = "................. ";
            this.xrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel59
            // 
            this.xrLabel59.BorderWidth = 0;
            this.xrLabel59.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel59.Location = new System.Drawing.Point(142, 33);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel59.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel59.ParentStyleUsing.UseFont = false;
            this.xrLabel59.Size = new System.Drawing.Size(100, 26);
            this.xrLabel59.Text = "............... ";
            this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel60
            // 
            this.xrLabel60.BorderWidth = 0;
            this.xrLabel60.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel60.Location = new System.Drawing.Point(150, 57);
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel60.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel60.ParentStyleUsing.UseFont = false;
            this.xrLabel60.Size = new System.Drawing.Size(100, 26);
            this.xrLabel60.Text = ".................. ";
            this.xrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel61
            // 
            this.xrLabel61.BorderWidth = 0;
            this.xrLabel61.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel61.Location = new System.Drawing.Point(150, 82);
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel61.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel61.ParentStyleUsing.UseFont = false;
            this.xrLabel61.Size = new System.Drawing.Size(100, 25);
            this.xrLabel61.Text = ".................. ";
            this.xrLabel61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel62
            // 
            this.xrLabel62.BorderWidth = 0;
            this.xrLabel62.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel62.Location = new System.Drawing.Point(17, 83);
            this.xrLabel62.Name = "xrLabel62";
            this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel62.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel62.ParentStyleUsing.UseFont = false;
            this.xrLabel62.Size = new System.Drawing.Size(116, 25);
            this.xrLabel62.Text = "..................... ";
            this.xrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel63
            // 
            this.xrLabel63.BorderWidth = 0;
            this.xrLabel63.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel63.Location = new System.Drawing.Point(17, 58);
            this.xrLabel63.Name = "xrLabel63";
            this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel63.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel63.ParentStyleUsing.UseFont = false;
            this.xrLabel63.Size = new System.Drawing.Size(116, 26);
            this.xrLabel63.Text = "................ ";
            this.xrLabel63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel64
            // 
            this.xrLabel64.BorderWidth = 0;
            this.xrLabel64.Location = new System.Drawing.Point(0, 33);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel64.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel64.Size = new System.Drawing.Size(142, 20);
            this.xrLabel64.Text = "- Chỉ định giao hàng";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel65
            // 
            this.xrLabel65.BorderWidth = 0;
            this.xrLabel65.Location = new System.Drawing.Point(0, 17);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel65.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel65.Size = new System.Drawing.Size(142, 20);
            this.xrLabel65.Text = "19. Chứng từ kèm theo :";
            this.xrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel66
            // 
            this.xrLabel66.BorderWidth = 0;
            this.xrLabel66.Location = new System.Drawing.Point(142, 17);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel66.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel66.Size = new System.Drawing.Size(75, 20);
            this.xrLabel66.Text = "Bản chính";
            this.xrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel67
            // 
            this.xrLabel67.BorderWidth = 0;
            this.xrLabel67.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel67.Location = new System.Drawing.Point(258, 83);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel67.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel67.ParentStyleUsing.UseFont = false;
            this.xrLabel67.Size = new System.Drawing.Size(92, 25);
            this.xrLabel67.Text = ".............. ";
            this.xrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ToKhaiGCCTA4
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.Margins = new System.Drawing.Printing.Margins(5, 15, 19, 24);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRLabel lblMaDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiChiDinhGiaoHang;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoiTac;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblTenDaiLyTTHQ;
        private DevExpress.XtraReports.UI.XRLabel lblSoHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblSoGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblMaDaiLyTTHQ;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell TenHang1;
        private DevExpress.XtraReports.UI.XRTableCell MaHS1;
        private DevExpress.XtraReports.UI.XRTableCell DVT1;
        private DevExpress.XtraReports.UI.XRTableCell Luong1;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell TenHang2;
        private DevExpress.XtraReports.UI.XRTableCell MaHS2;
        private DevExpress.XtraReports.UI.XRTableCell DVT2;
        private DevExpress.XtraReports.UI.XRTableCell Luong2;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell TenHang3;
        private DevExpress.XtraReports.UI.XRTableCell MaHS3;
        private DevExpress.XtraReports.UI.XRTableCell DVT3;
        private DevExpress.XtraReports.UI.XRTableCell Luong3;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT3;
        private DevExpress.XtraReports.UI.XRLabel lblSoTiepNhan;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh3;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh2;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh1;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao1;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao2;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao3;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTriGiaNT;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblBanLuuHaiQuan;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHHDGiao;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHDGiao;
        private DevExpress.XtraReports.UI.XRLabel lblSoHDGiao;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHPKHDGiao;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHD;
        private DevExpress.XtraReports.UI.XRLabel lblSoPKHDGiao;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRLabel xrGiaoSPGCCT;
        private DevExpress.XtraReports.UI.XRLabel xrGiaoMMTB;
        private DevExpress.XtraReports.UI.XRLabel xrGiaoNLD;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoanhNghiep1;
        private DevExpress.XtraReports.UI.XRTableCell lblPhiBaoHiem;
        private DevExpress.XtraReports.UI.XRTable xrTable58;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTable xrTable57;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel lblDiaDiemGiaoHang;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiChiDinhNhanHang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell TenHang4;
        private DevExpress.XtraReports.UI.XRTableCell MaHS4;
        private DevExpress.XtraReports.UI.XRTableCell DVT4;
        private DevExpress.XtraReports.UI.XRTableCell Luong4;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT4;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell TenHang5;
        private DevExpress.XtraReports.UI.XRTableCell MaHS5;
        private DevExpress.XtraReports.UI.XRTableCell DVT5;
        private DevExpress.XtraReports.UI.XRTableCell Luong5;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT5;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell TenHang6;
        private DevExpress.XtraReports.UI.XRTableCell MaHS6;
        private DevExpress.XtraReports.UI.XRTableCell DVT6;
        private DevExpress.XtraReports.UI.XRTableCell Luong6;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT6;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell TenHang7;
        private DevExpress.XtraReports.UI.XRTableCell MaHS7;
        private DevExpress.XtraReports.UI.XRTableCell DVT7;
        private DevExpress.XtraReports.UI.XRTableCell Luong7;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT7;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell TenHang8;
        private DevExpress.XtraReports.UI.XRTableCell MaHS8;
        private DevExpress.XtraReports.UI.XRTableCell DVT8;
        private DevExpress.XtraReports.UI.XRTableCell Luong8;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT8;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell TenHang9;
        private DevExpress.XtraReports.UI.XRTableCell MaHS9;
        private DevExpress.XtraReports.UI.XRTableCell DVT9;
        private DevExpress.XtraReports.UI.XRTableCell Luong9;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT9;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell TenHang10;
        private DevExpress.XtraReports.UI.XRTableCell MaHS10;
        private DevExpress.XtraReports.UI.XRTableCell DVT10;
        private DevExpress.XtraReports.UI.XRTableCell Luong10;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT10;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableCell TenHang11;
        private DevExpress.XtraReports.UI.XRTableCell MaHS11;
        private DevExpress.XtraReports.UI.XRTableCell DVT11;
        private DevExpress.XtraReports.UI.XRTableCell Luong11;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT11;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell TenHang12;
        private DevExpress.XtraReports.UI.XRTableCell MaHS12;
        private DevExpress.XtraReports.UI.XRTableCell DVT12;
        private DevExpress.XtraReports.UI.XRTableCell Luong12;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT12;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell TenHang13;
        private DevExpress.XtraReports.UI.XRTableCell MaHS13;
        private DevExpress.XtraReports.UI.XRTableCell DVT13;
        private DevExpress.XtraReports.UI.XRTableCell Luong13;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT13;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell TenHang14;
        private DevExpress.XtraReports.UI.XRTableCell MaHS14;
        private DevExpress.XtraReports.UI.XRTableCell DVT14;
        private DevExpress.XtraReports.UI.XRTableCell Luong14;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT14;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTableCell TenHang15;
        private DevExpress.XtraReports.UI.XRTableCell MaHS15;
        private DevExpress.XtraReports.UI.XRTableCell DVT15;
        private DevExpress.XtraReports.UI.XRTableCell Luong15;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT15;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell TenHang16;
        private DevExpress.XtraReports.UI.XRTableCell MaHS16;
        private DevExpress.XtraReports.UI.XRTableCell DVT16;
        private DevExpress.XtraReports.UI.XRTableCell Luong16;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT16;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableCell TenHang17;
        private DevExpress.XtraReports.UI.XRTableCell MaHS17;
        private DevExpress.XtraReports.UI.XRTableCell DVT17;
        private DevExpress.XtraReports.UI.XRTableCell Luong17;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT17;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableCell TenHang18;
        private DevExpress.XtraReports.UI.XRTableCell MaHS18;
        private DevExpress.XtraReports.UI.XRTableCell DVT18;
        private DevExpress.XtraReports.UI.XRTableCell Luong18;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT18;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableCell TenHang19;
        private DevExpress.XtraReports.UI.XRTableCell MaHS19;
        private DevExpress.XtraReports.UI.XRTableCell DVT19;
        private DevExpress.XtraReports.UI.XRTableCell Luong19;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT19;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT19;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTable xrTable19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTable xrTable18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel lblBanSao13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel lblBanSao11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel xrLabel58;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRLabel xrLabel60;
        private DevExpress.XtraReports.UI.XRLabel xrLabel61;
        private DevExpress.XtraReports.UI.XRLabel xrLabel62;
        private DevExpress.XtraReports.UI.XRLabel xrLabel63;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrNhanMMTB;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrNhanNLD;
        private DevExpress.XtraReports.UI.XRLabel xrNhanSPGCCT;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHHDNhan;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHDNhan;
        private DevExpress.XtraReports.UI.XRLabel lblsoHDNhan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHPKHDNhan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel71;
        private DevExpress.XtraReports.UI.XRLabel xrLabel70;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel74;
        private DevExpress.XtraReports.UI.XRLabel lblNgaygiaohang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel lblNgaynhanhang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLabel xrLabel79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel80;
        private DevExpress.XtraReports.UI.XRLabel xrLabel81;
        private DevExpress.XtraReports.UI.XRLabel xrLabel82;
        private DevExpress.XtraReports.UI.XRLabel xrLabel83;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLabel xrLabel85;
        private DevExpress.XtraReports.UI.XRLabel xrLabel86;
        private DevExpress.XtraReports.UI.XRLabel xrLabel87;
        private DevExpress.XtraReports.UI.XRLabel xrLabel88;
        private DevExpress.XtraReports.UI.XRLabel xrLabel89;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel90;
        private DevExpress.XtraReports.UI.XRLabel xrLabel91;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHQ1;
        private DevExpress.XtraReports.UI.XRLabel lblCucHQ1;
        private DevExpress.XtraReports.UI.XRLabel lblCucHQ2;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHQ2;
        private DevExpress.XtraReports.UI.XRLabel lblBanSao12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel92;
        private DevExpress.XtraReports.UI.XRLabel lblBanChinh13;
        private DevExpress.XtraReports.UI.XRLabel lblBanChinh12;
        private DevExpress.XtraReports.UI.XRLabel lblBanChinh11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel94;
        private DevExpress.XtraReports.UI.XRLabel lblBanChinh14;
        private DevExpress.XtraReports.UI.XRLabel lblBanSao16;
        private DevExpress.XtraReports.UI.XRLabel lblBanSao15;
        private DevExpress.XtraReports.UI.XRLabel lblBanSao14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel97;
        private DevExpress.XtraReports.UI.XRLabel lblBanChinh15;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiGiao;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiNhan;
        private DevExpress.XtraReports.UI.XRLabel lblMaDailyLTT;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;

    }
}
