﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;

#endif
using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;


//using Company.BLL.KDT;

namespace Company.Interface.Report
{
    public partial class BangkeSoContainer_TT196_CV3742 : DevExpress.XtraReports.UI.XtraReport
    {
        public string phieu;
        public string soTN ;
        public string ngayTN ;
        public string maHaiQuan;
        int STT = 0;
        public int Phulucso;
        public int indexPL = 0;
        public bool isCuaKhau = true;
        public bool ToKhaiTaiCho = false;
        public bool IsToKhaiTaiCho = false;
        public    ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerCollection = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
        /*public Company.Interface.Report.ReportViewTKNTQDTFormTT196 report;*/
        public BangkeSoContainer_TT196_CV3742()
        {
            InitializeComponent();
          
        }
        public string GetChiCucHQCK()
        {
            if (isCuaKhau)
            {
                string chicuc = NhomCuaKhau.GetDonVi(this.TKMD.CuaKhau_ID);
                if (chicuc == string.Empty)
                    chicuc = this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                return chicuc;
            }
            else
                return string.Empty;
        }
        public void BindReport()
        {
            if (IsToKhaiTaiCho)
            {
                lblLoaiToKhai.Text = "Nhập khẩu tại chỗ";
            }
            float sizeFont = float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8")) == 0 ? 8 : float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport", "8"));
            STT = (indexPL - 1) * 20;
            #region Thong tin chinh
            DateTime minDate = new DateTime(1900, 1, 1);
            //Chi cục HD đăng ký
            lblChiCucHQDangKy.Text = DonViHaiQuan.GetName(TKMD.MaHaiQuan);
            lblChiCucHQDangKy.Font = new Font("Times New Roman", sizeFont);

            //Chi cục Hải quan cửa khẩu nhập
            /*lblChiCucHQCuaKhau.Text = CuaKhau.GetName(TKMD.CuaKhau_ID);*/
            //DonViHaiQuan.GetName(this.TKMD.CuaKhau_ID);
            if (!ToKhaiTaiCho)
            {
                lblChiCucHQCuaKhau.Text = GetChiCucHQCK();
                lblChiCucHQCuaKhau.Font = new Font("Times New Roman", sizeFont);
            }
            else
            {
                lblChiCuc.Text = "";
                lblTieuDeGocPhai.Text = "HQ/2012 - PLTKDTNK-TC";
            }

            //So luong phu luc to khai
            lblSoPhuLucToKhai.Text = string.Format("{00}", Phulucso);

            //Số tờ khai
            if (this.TKMD.SoToKhai > 0)
                lblSoToKhai.Text = this.TKMD.SoToKhai.ToString("N0");
            else
                lblSoToKhai.Text = "";

            //Ngày giờ đăng ký
            if (this.TKMD.NgayDangKy > minDate)
                lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy HH:mm");
            else
                lblNgayDangKy.Text = "";
            // Ma Loai Hinh
            string stlh = "";
            stlh = LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
            lblLoaiHinh.Text = TKMD.MaLoaiHinh + " - " + stlh;
            lblLoaiHinh.Font = new Font("Times New Roman", sizeFont);

            #endregion
            //List<Company.KDT.SHARE.QuanLyChungTu.Container> dataSourceContainer = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
            foreach (Company.KDT.SHARE.QuanLyChungTu.Container item in ContainerCollection)
            {
                item.SoHieu = item.SoHieu + "/" + item.Seal_No;
                
            }
            DetailContainer.DataSource = ContainerCollection;
            lblHieuKien.DataBindings.Add("Text", DetailContainer.DataSource, "SoHieu");
            lblSoLuongKien.DataBindings.Add("Text", DetailContainer.DataSource, "SoKien");
            lblTrongLuongKien.DataBindings.Add("Text", DetailContainer.DataSource, "TrongLuong");
            //DataTable dt =  TKMD.GetContainerInfo(TKMD.ID);
            //this.DataSource = dt;
            //List<Company.KDT.SHARE.QuanLyChungTu.SoContainer> socont = (List<Company.KDT.SHARE.QuanLyChungTu.SoContainer>)Company.KDT.SHARE.QuanLyChungTu.SoContainer.SelectCollectionBy_TKMD_ID(TKMD.ID);
            //lblHieuKien.DataBindings.Add("Text",this.DataSource, dt.TableName + ".SoHieu");
            //lblSoLuongKien.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoHieu");
            //lblSoseal.DataBindings.Add("Text",this.DataSource, dt.TableName + ".Seal_No");
            //lblHieuKien.DataBindings.Add("", dt, ".SoHieu", "");
            //lblHieuKien.DataBindings.Add("", dt, ".SoHieu", "");

            if (TKMD.NgayDangKy == new DateTime(1900, 1, 1) || GlobalSettings.NgayHeThong)
                lblNgayThangNam.Text = "Ngày " + DateTime.Today.Day.ToString("00") + " tháng " + DateTime.Today.Month.ToString("00") + " năm " + DateTime.Today.Year;
            else
                lblNgayThangNam.Text = "Ngày " + TKMD.NgayDangKy.Day.ToString("00") + " tháng " + TKMD.NgayDangKy.Month.ToString("00") + " năm " + TKMD.NgayDangKy.Year;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
           this.STT++;
            lblSTT.Text =this.STT+"";
        }

  
    }
}
