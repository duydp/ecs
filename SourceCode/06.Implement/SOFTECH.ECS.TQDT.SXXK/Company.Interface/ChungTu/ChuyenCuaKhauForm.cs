﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
using Company.BLL.KDT;
using Company.BLL;

using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class ChuyenCuaKhauForm : Company.Interface.BaseForm
    {
        public bool isKhaiBoSung = false;

        public ToKhaiMauDich TKMD;
        public DeNghiChuyenCuaKhau deNghiChuyen = new DeNghiChuyenCuaKhau();
        public List<DeNghiChuyenCuaKhau> ListDeNghiChuyen = new List<DeNghiChuyenCuaKhau>();
        public ChuyenCuaKhauForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            //cvError.Validate();
            //if (!cvError.IsValid)
            //    return;

            deNghiChuyen.DiaDiemKiemTra = txtDiaDiemKiemTra.Text.Trim();
            if (isKhaiBoSung)
                deNghiChuyen.LoaiKB = 1;
            else
                deNghiChuyen.LoaiKB = 0;

            deNghiChuyen.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            deNghiChuyen.NgayVanDon = ccNgayVanDon.Value;
            deNghiChuyen.SoVanDon = txtSoVanDon.Text;
            deNghiChuyen.ThoiGianDen = ccThoiHanDen.Value;
            deNghiChuyen.TuyenDuong = txtTuyenDuong.Text;
            deNghiChuyen.ThongTinKhac = txtThongTinKhac.Text.Trim();
            deNghiChuyen.TKMD_ID = TKMD.ID;

            bool isUpdate = false;
            try
            {
                if (deNghiChuyen.ID == 0)
                {
                    deNghiChuyen.GuidStr = Guid.NewGuid().ToString();
                    deNghiChuyen.Insert();
                }
                else
                {
                    deNghiChuyen.Update();
                    isUpdate = true;
                }
                if (!isUpdate)
                    TKMD.listChuyenCuaKhau.Add(deNghiChuyen);
                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void CoForm_Load(object sender, EventArgs e)
        {
            if (TKMD.SoVanDon != "")
                txtSoVanDon.Text = TKMD.SoVanDon;
            else
                txtSoVanDon.Text = "";

            if (this.TKMD.NgayVanDon.Year > 1900)
                ccNgayVanDon.Text = TKMD.NgayVanDon.ToShortDateString();
            else
                ccNgayVanDon.Text = DateTime.Today.ToShortDateString();

            if (this.TKMD.NgayDenPTVT.Year > 1900)
                ccThoiHanDen.Text = TKMD.NgayDenPTVT.ToShortDateString();
            else
                ccThoiHanDen.Text = DateTime.Today.ToShortDateString();

            if (!isKhaiBoSung)
            {
                grpTiepNhan.Visible = false;
                btnKhaiBao.Visible = false;
                btnLayPhanHoi.Visible = false;
            }

            if (deNghiChuyen.ID > 0)
            {
                txtDiaDiemKiemTra.Text = deNghiChuyen.DiaDiemKiemTra;
                txtSoVanDon.Text = deNghiChuyen.SoVanDon;
                txtThongTinKhac.Text = deNghiChuyen.ThongTinKhac;
                txtTuyenDuong.Text = deNghiChuyen.TuyenDuong;
                ccNgayVanDon.Text = deNghiChuyen.NgayVanDon.ToShortDateString();
                ccThoiHanDen.Text = deNghiChuyen.ThoiGianDen.ToShortDateString();
            }
            if (deNghiChuyen.SoTiepNhan > 0 && deNghiChuyen.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                txtSoTiepNhan.Text = deNghiChuyen.SoTiepNhan + "";
                ccNgayTiepNhan.Text = deNghiChuyen.NgayTiepNhan.ToShortDateString();
            }
            else if (deNghiChuyen.SoTiepNhan > 0 && deNghiChuyen.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
                txtSoTiepNhan.Text = deNghiChuyen.SoTiepNhan + "";
                ccNgayTiepNhan.Text = deNghiChuyen.NgayTiepNhan.ToShortDateString();
            }
            // Thiết lập trạng thái các nút trên form.
            // HUNGTQ, Update 07/06/2010.
            SetButtonStateCHUYENCUAKHAU(TKMD, isKhaiBoSung, deNghiChuyen);
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (deNghiChuyen.ID > 0)
            {
                deNghiChuyen.Delete();
                this.Close();
            }
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (deNghiChuyen.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                bool thanhcong = deNghiChuyen.WSKhaiBaoChuyenCK(password, TKMD.MaHaiQuan, (long)TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NamDK, TKMD.ID, TKMD.MaDoanhNghiep, ListDeNghiChuyen, MessageTypes.ToKhaiNhap, MessageFunctions.KhaiBao);

                if (thanhcong)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);

                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                //deNghiChuyen.LayPhanHoi(password, TKMD.MaHaiQuan);
                if (this.deNghiChuyen.SoTiepNhan == 0)
                {
                    this.LayPhanHoiKhaiBao(password);
                }
                else if (this.deNghiChuyen.SoTiepNhan > 0)
                {
                    this.LayPhanHoiDuyet(password);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void LayPhanHoiKhaiBao(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = deNghiChuyen.WSLaySoTiepNhan(password, EntityBase.GetPathProram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.deNghiChuyen.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungDeNghiChuyenCKThanhCong);
                    this.ShowMessage(message, false);

                    btnKhaiBao.Enabled = false;
                    txtSoTiepNhan.Text = this.deNghiChuyen.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.deNghiChuyen.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.deNghiChuyen.NgayTiepNhan.ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiDuyet(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = deNghiChuyen.WSLayPhanHoi(password, EntityBase.GetPathProram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.deNghiChuyen.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungDeNghiChuyenCKDuocChapNhan);
                    if (message.Length == 0)
                    {
                        message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.deNghiChuyen.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        txtSoTiepNhan.Text = "";
                        ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
                        ccNgayTiepNhan.Text = "";
                    }
                    else
                        btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                    this.ShowMessage(message, false);

                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form CHUYENCUAKHAU.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateCHUYENCUAKHAU(BLL.KDT.ToKhaiMauDich tkmd, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau chuyenCuaKhau)
        {
            if (chuyenCuaKhau == null)
                return false;

            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.SoTiepNhan == 0
                            || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                            || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET);

                btnXoa.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXyLy.Enabled = false;

                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else
            {
                //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                if (tkmd.SoToKhai == 0)
                {
                    //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                    //Globals.ShowMessageTQDT(msg, false);

                    //return false;
                }
                else
                {
                    if (chuyenCuaKhau.SoTiepNhan > 0)
                        status = false;
                    else
                        status = true;

                    btnXoa.Enabled = status;
                    btnGhi.Enabled = status;
                    btnKetQuaXyLy.Enabled = true;

                    //Neu hop dong chua co so tiep nhan -> phai khai bao
                    if (chuyenCuaKhau.SoTiepNhan == 0)
                    {
                        btnKhaiBao.Enabled = true;
                        btnLayPhanHoi.Enabled = false;
                    }
                    //Neu hop dong da co so tiep nhan -> co the lay phan hoi
                    else
                    {
                        btnKhaiBao.Enabled = false;
                        //btnLayPhanHoi.Enabled = true;
                    }
                }
            }

            return true;
        }

        #endregion

        private void btnKetQuaXyLy_Click(object sender, EventArgs e)
        {
            if (deNghiChuyen.GuidStr != null && deNghiChuyen.GuidStr != "")
                Globals.ShowKetQuaXuLyBoSung(deNghiChuyen.GuidStr);
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }
    }
}

