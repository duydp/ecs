﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using Company.QuanTri;

namespace Company.Interface.KDT.GC
{
    public partial class ThietBiGCEditForm : BaseFormHaveGuidPanel
    {
        public HopDong HD = new HopDong();
        public ThietBi tbDetail = new ThietBi();
        public bool isBrower = false;
        public LogHistory Log = new LogHistory();
        public String Caption;
        public bool IsChange;
        public ThietBiGCEditForm()
        {
            InitializeComponent();
            ctrNguyenTe.ValueChanged -= new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);
            ctrNuoc.ValueChanged -= new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);
            txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
            txtDonGia.TextChanged -= new EventHandler(txt_TextChanged);
            txtTriGia.TextChanged -= new EventHandler(txt_TextChanged);
            chTinhTrang.CheckedChanged -= new EventHandler(txt_TextChanged);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || HD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || HD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void SetData()
        {
            try
            {
                txtMa.TextChanged -= new EventHandler(txt_TextChanged);
                txtMa.Text = tbDetail.Ma.Trim();
                txtMa.TextChanged += new EventHandler(txt_TextChanged);

                txtTen.TextChanged -= new EventHandler(txt_TextChanged);
                txtTen.Text = tbDetail.Ten.Trim();
                txtTen.TextChanged += new EventHandler(txt_TextChanged);

                txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaHS.Text = tbDetail.MaHS.Trim();
                txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

                cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
                cbDonViTinh.SelectedValue = tbDetail.DVT_ID;
                cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

                ctrNuoc.ValueChanged -= new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);
                ctrNuoc.Ma = tbDetail.NuocXX_ID;
                ctrNuoc.ValueChanged += new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);

                txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong.Text = (tbDetail.SoLuongDangKy.ToString());
                txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

                txtDonGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtDonGia.Text = tbDetail.DonGia.ToString();
                txtDonGia.TextChanged += new EventHandler(txt_TextChanged);

                txtTriGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtTriGia.Text = tbDetail.TriGia.ToString();
                txtTriGia.TextChanged += new EventHandler(txt_TextChanged);

                ctrNguyenTe.ValueChanged -= new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);
                ctrNguyenTe.Ma = tbDetail.NguyenTe_ID;
                ctrNguyenTe.ValueChanged += new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);

                chTinhTrang.CheckedChanged -= new EventHandler(txt_TextChanged);
                chTinhTrang.Checked = tbDetail.TinhTrang == "1";
                chTinhTrang.CheckedChanged += new EventHandler(txt_TextChanged);

                txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                txtGhiChu.Text = tbDetail.GhiChu.ToString();
                txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void ThietBiGCEditForm_Load(object sender, EventArgs e)
        {
            try
            {
                txtSoLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                txtDonGia.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                txtTriGia.DecimalDigits = GlobalSettings.TriGiaNT;
                Caption = this.Text;
                if (!isBrower)
                {
                    txtMa.Focus();
                    this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                    cbDonViTinh.DataSource = this._DonViTinh;
                    cbDonViTinh.DisplayMember = "Ten";
                    cbDonViTinh.ValueMember = "ID";
                    if (!string.IsNullOrEmpty(tbDetail.Ma))
                    {
                        SetData();
                    }
                    else
                    {
                        ctrNuoc.ValueChanged -= new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);
                        ctrNuoc.Ma = GlobalSettings.NUOC;
                        ctrNuoc.ValueChanged += new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);

                        cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
                        cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
                        cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

                        ctrNguyenTe.ValueChanged -= new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);
                        ctrNguyenTe.Ma = GlobalSettings.NGUYEN_TE_MAC_DINH;
                        ctrNguyenTe.ValueChanged += new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);
                    }

                    if (this.OpenType == OpenFormType.View)
                    {
                        btnAdd.Enabled = false;
                        btnXoa.Enabled = false;
                        dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    }

                }
                else
                {
                    uiGroupBox2.Visible = false;
                    HD.LoadCollection();
                    dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    this.Width = dgThietBi.Width;
                    this.Height = dgThietBi.Height;
                }
                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
                DataTable dt = MaHS.SelectAll();
                foreach (DataRow dr in dt.Rows)
                    col.Add(dr["HS10So"].ToString());
                txtMaHS.AutoCompleteCustomSource = col;
                dgThietBi.DataSource = HD.TBCollection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
            

        }
        private void BindData()
        {
            try
            {
                dgThietBi.Refresh();
                dgThietBi.DataSource = HD.TBCollection;
                dgThietBi.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void reset()
        {
            try
            {
                txtMa.TextChanged -= new EventHandler(txt_TextChanged);
                txtMa.Text = "";
                txtMa.TextChanged += new EventHandler(txt_TextChanged);

                txtTen.TextChanged -= new EventHandler(txt_TextChanged);
                txtTen.Text = "";
                txtTen.TextChanged += new EventHandler(txt_TextChanged);

                txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaHS.Text = "";
                txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

                txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong.Text = "";
                txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

                txtDonGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtDonGia.Text = "0";
                txtDonGia.TextChanged += new EventHandler(txt_TextChanged);

                txtTriGia.TextChanged -= new EventHandler(txt_TextChanged);
                txtTriGia.Text = "0";
                txtTriGia.TextChanged += new EventHandler(txt_TextChanged);

                chTinhTrang.CheckedChanged -= new EventHandler(txt_TextChanged);
                chTinhTrang.Checked = false;
                chTinhTrang.CheckedChanged += new EventHandler(txt_TextChanged);

                error.SetError(txtMaHS, null);
                error.SetError(txtMa, null);
                error.SetError(txtTen, null);
                BindData();
                tbDetail = new ThietBi();
            }
            catch (Exception ex)
            {
                dgThietBi.Refresh();
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }

        }
        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!MaHS.Validate(txtMaHS.Text, 10))
                {
                    error.SetIconPadding(txtMaHS, -8);
                    error.SetError(txtMaHS, setText("Mã số HS không hợp lệ.", "This value is invalid"));
                    txtMaHS.Focus();
                }
                else
                {
                    error.SetError(txtMaHS, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
        }
        private void setErro()
        {
            try
            {
                error.Clear();
                error.SetError(txtTen, null);
                error.SetError(txtMa, null);
                error.SetError(txtMaHS, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }

        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                //Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtMa, errorProvider, "Mã thiết bị", isOnlyWarning);
                isValid &= ValidateControl.ValidateSpecialChar(txtMa, errorProvider, "Mã thiết bị", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTen, errorProvider, "Tên/Mô tả thiết bị", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHS, errorProvider, "Mã HS", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtSoLuong, errorProvider, "Số lượng");
                isValid &= ValidateControl.ValidateZero(txtDonGia, errorProvider, "Đơn giá");
                isValid &= ValidateControl.ValidateZero(txtTriGia, errorProvider, "Trị giá");
                isValid &= ValidateControl.ValidateChoose(cbDonViTinh, errorProvider, "Đơn vị tính", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
            return isValid;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                txtTriGia_Leave(null, null);
                txtMa.Focus();
                if (!ValidateForm(false))
                    return;
                checkExitsThietBiAndSTTHang();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
        }
        private void checkExitsThietBiAndSTTHang()
        {
            try
            {
                HD.TBCollection.Remove(tbDetail);

                foreach (ThietBi tb in HD.TBCollection)
                {
                    if (tb.Ma.Trim() == txtMa.Text.Trim())
                    {
                        showMsg("MSG_0203003");
                        //string st = MLMessages("Đã có mã thiết bị này trong danh sách?","MSG_WRN06","", false);                                       
                        if (tbDetail.Ma != "")
                            HD.TBCollection.Add(tbDetail);
                        return;
                    }
                }
                tbDetail.Ma = txtMa.Text.Trim();
                tbDetail.Ten = txtTen.Text.Trim();
                tbDetail.MaHS = txtMaHS.Text.Trim();
                tbDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                tbDetail.NuocXX_ID = ctrNuoc.Ma;
                tbDetail.SoLuongDangKy = Convert.ToDecimal(txtSoLuong.Text);
                tbDetail.DonGia = Convert.ToDouble(txtDonGia.Text);
                tbDetail.TriGia = Convert.ToDouble(txtTriGia.Text);
                tbDetail.NguyenTe_ID = ctrNguyenTe.Ma;
                tbDetail.TinhTrang = chTinhTrang.Checked ? "1" : "0";
                tbDetail.GhiChu = txtGhiChu.Text.Trim();
                tbDetail.HopDong_ID = HD.ID;
                HD.TBCollection.Add(tbDetail);
                reset();
                this.setErro();
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
            
        }
        private void dgThietBi_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                tbDetail = (ThietBi)e.Row.DataRow;
                tbDetail.HopDong_ID = HD.ID;
                if (!isBrower)
                {
                    SetData();
                }
                else
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
            
        }



        private void dgThietBi_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                e.Row.Cells["TenNguyenTe"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe_ID"].Value).Trim();
                e.Row.Cells["XuatXu"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value).Trim();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }

        }

        private void ctrNguyenTe_Load(object sender, EventArgs e)
        {

        }

        private void txtTriGia_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal soluong = Convert.ToDecimal(txtSoLuong.Text);
                decimal dongia = Convert.ToDecimal(txtDonGia.Text);
                decimal trigia = soluong * dongia;
                txtTriGia.Text = trigia.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }

        }

        private void dgThietBi_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.Edit)
                {
                    if (showMsg("MSG_DEL01", true) == "Yes")
                    {
                        GridEXSelectedItemCollection items = dgThietBi.SelectedItems;
                        List<ThietBi> ItemDelete = new List<ThietBi>();
                        foreach (GridEXSelectedItem row in items)
                        {
                            Company.GC.BLL.KDT.GC.ThietBi tbDelete = (Company.GC.BLL.KDT.GC.ThietBi)row.GetRow().DataRow;
                            tbDelete.Delete();
                            Company.GC.BLL.GC.GC_ThietBi.DeleteGC_ThietBi(HD.ID, tbDelete.Ma);
                            HD.TBCollection.Remove(tbDelete);
                            ItemDelete.Add(tbDelete);
                        }
                        if (ItemDelete.Count >= 1)
                            Log.LogHDGC(HD, ItemDelete, MessageTitle.XoaThietBi, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                        BindData();
                        this.SetChange(true);
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }    
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }

        }
        //0112
        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                List<ThietBi> tbColl = new List<ThietBi>();
                if (HD.TBCollection.Count <= 0) return;
                GridEXSelectedItemCollection items = dgThietBi.SelectedItems;
                if (items.Count <= 0) return;
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    foreach (GridEXSelectedItem row in items)
                    {
                        Company.GC.BLL.KDT.GC.ThietBi tbDelete = (Company.GC.BLL.KDT.GC.ThietBi)row.GetRow().DataRow;
                        tbDelete.Delete();
                        Company.GC.BLL.GC.GC_ThietBi.DeleteGC_ThietBi(HD.ID, tbDelete.Ma);
                        tbColl.Add(tbDelete);
                    }
                    foreach (Company.GC.BLL.KDT.GC.ThietBi tbt in tbColl)
                    {
                        HD.TBCollection.Remove(tbt);
                    }
                    if (tbColl.Count >= 1)
                        Log.LogHDGC(HD, tbColl, MessageTitle.XoaThietBi, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    BindData();
                    this.setErro();
                    this.SetChange(true);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }

        }

        private void dgThietBi_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgThietBi.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            tbDetail = (ThietBi)i.GetRow().DataRow;
                            tbDetail.HopDong_ID = HD.ID;
                            if (!isBrower)
                            {
                                SetData();
                            }
                            else
                            {
                                this.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgThietBi.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaTB.Text);
                dgThietBi.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}