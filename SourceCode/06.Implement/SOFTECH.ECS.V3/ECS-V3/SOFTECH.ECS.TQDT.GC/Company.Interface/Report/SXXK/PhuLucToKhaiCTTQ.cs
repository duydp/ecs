﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using System.Threading;

namespace Company.Interface.Report.GC
{
    public partial class PhuLucToKhaiCTTQ : DevExpress.XtraReports.UI.XtraReport
    {
        public List<HangChuyenTiep> HCTCollection = new List<HangChuyenTiep>();
        public ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
        public long SoToKhai;
        public DateTime NgayDangKy;
       // public ToKhaiMauDich TKMD;
        public Company.Interface.Report.ReportViewGCCTTQForm report;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public bool inMaHang = false;
        public PhuLucToKhaiCTTQ()
        {
            InitializeComponent();
        }

        //dinh dang tri gia nguyen te
        string FormatNumber(object obj)
        {
            return Helpers.FormatNumeric(obj, GlobalSettings.TriGiaNT, Thread.CurrentThread.CurrentCulture);

        }
        
        public void BindReport(string pls)
        {
            lblMLHinh.Text = this.TKCT.MaLoaiHinh;//.Substring(1, 2);
            this.PrintingSystem.ShowMarginsWarning = false;
            decimal tongTriGiaNT = 0;
            decimal tongThueXNK = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;
            decimal tongthuetatca = 0;
            DateTime minDate = new DateTime(1900, 1, 1);
            //string MaLH = TKCT.MaLoaiHinh.Substring(0, 1);
            //if (MaLH == "N")
            //{
            //    lblLoaiHinhKhai.Text = "Nhập khẩu";
            //}
            //else if (MaLH == "X")
            //{
            //    lblLoaiHinhKhai.Text = "Xuất khẩu";
            //}
            //else
            //    lblLoaiHinhKhai.Text = "";
            
            lblChiCucHaiQuan.Text = GlobalSettings.TEN_HAI_QUAN;
           
            
            //So to khai
            if (this.TKCT.SoToKhai > 0)
            {
                lblSoToKhai.Text = this.TKCT.SoToKhai + "";

            }
            else
            {
                lblSoToKhai.Text = "";
                labelTQDT.Image = null;
            }

            //Ngay dang ký
            if (this.TKCT.NgayDangKy > minDate)
                lblNgayDangKy.Text = this.TKCT.NgayDangKy.ToString("dd/MM/yyyy");
            else
                lblNgayDangKy.Text = "";


            //5 Loai hinh
           // string stlh = Company.KDT.SHARE.Components.DuLieuChuan.LoaiHinhMauDich.GetName(TKCT.MaLoaiHinh);
            lblMLHinh.Text = TKCT.MaLoaiHinh; //+"-" + stlh;

            lblThongBaoMienThue.Visible = MienThue1;
            lblThongBaoMienThueGTGT.Visible = MienThue2;

            xrLabel1.Text = pls;
            for (int i = 0; i < this.HCTCollection.Count; i++)
            {
                int stt = i + 1;
               
                XRControl control = new XRControl();
                HangChuyenTiep hct = this.HCTCollection[i];
                //so thu tu hang
                control = this.xrTable1.Rows[i + 3].Controls["lblSTTHang" + (i + 1)];//1
                control.Text = stt.ToString();
                

                control = this.xrTable1.Rows[i + 3].Controls["TenHang" + (i + 1)];//2
                

                //if(control !=null )
                if (!inMaHang)
                    control.Text = hct.TenHang;
                else
                {
                    if (hct.MaHang.Trim().Length > 0)
                        control.Text = hct.TenHang + "/" + hct.MaHang;
                    else
                        control.Text = hct.TenHang;
                }

                try
                {
                    control.Font = new Font("Times New Roman", GlobalSettings.FontDongHang);
                }
                catch
                {
                    MessageBox.Show("File config thiếu key 'FontTenHang'", "Thông báo");
                    return;
                }

                control = this.xrTable1.Rows[i + 3].Controls["MaHS" + (i + 1)];
                control.Text = hct.MaHS;

                control = this.xrTable1.Rows[i + 3].Controls["XuatXu" + (i + 1)];
                control.Text = hct.ID_NuocXX;

                control = this.xrTable1.Rows[i + 3].Controls["Luong" + (i + 1)];
                control.Text = hct.SoLuong.ToString("G20");

                control = this.xrTable1.Rows[i + 3].Controls["DVT" + (i + 1)];
                control.Text = DonViTinh.GetName((object)hct.ID_DVT);
               
                
                control = this.xrTable1.Rows[i + 3].Controls["DonGiaNT" + (i + 1)];
                control.Text = hct.DonGia.ToString("G10");

                control = this.xrTable1.Rows[i + 3].Controls["TriGiaNT" + (i + 1)];
                control.Text = FormatNumber(hct.TriGia);
               
                //thue XNK
                //so thu tu thue
                control = this.xrTable2.Rows[i + 2].Controls["lblSTTThue" + (i + 1)];//1
                control.Text = stt.ToString();
                if(hct.ThueXNK.Equals(0))
                {
                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaTT" + (i + 1)];
                    control.Text = "";
                }
                else
                { 
                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaTT" + (i + 1)];
                    control.Text = hct.TriGiaTT.ToString("N0");

                    control = this.xrTable2.Rows[i + 2].Controls["TienThueXNK" + (i + 1)];
                    control.Text = hct.ThueXNK.ToString("N0");

                    control = this.xrTable2.Rows[i + 2].Controls["ThueSuatXNK" + (i + 1)];
                    control.Text = hct.ThueSuatXNK.ToString("N0");
                }
                //thue thu khac
                if (hct.TyLeThuKhac.Equals(0))
                {
                    control = this.xrTable2.Rows[i + 2].Controls["TyLeThuKhac" + (i + 1)];
                    control.Text = "";
                }
                else
                {
                    control = this.xrTable2.Rows[i + 2].Controls["TyLeThuKhac" + (i + 1)];
                    control.Text = hct.TyLeThuKhac.ToString("N0");

                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaThuKhac" + (i + 1)];
                    control.Text = hct.PhuThu.ToString("N0");

                }
                //if (MienThue1) control.Text = "";
                //control = this.xrTable2.Rows[i + 2].Controls["TienThueXNK" + (i + 1)];
                //control.Text = hmd.ThueXNK.ToString("N0");
                //if (MienThue1) control.Text = "";
                //thue GTGT or thue TTDB
                if (hct.ThueGTGT > 0 && hct.ThueTTDB == 0)
                {
                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaTTGTGT" + (i + 1)];
                    decimal TriGiaTTGTGT = hct.TriGiaTT + hct.ThueXNK;
                    control.Text = TriGiaTTGTGT.ToString("N0");
                    if (MienThue2) control.Text = "";

                    control = this.xrTable2.Rows[i + 2].Controls["ThueSuatGTGT" + (i + 1)];
                    control.Text = hct.ThueSuatGTGT.ToString("N0");
                    if (MienThue2) control.Text = "";
                    control = this.xrTable2.Rows[i + 2].Controls["TienThueGTGT" + (i + 1)];
                    control.Text = hct.ThueGTGT.ToString("N0");
                    if (MienThue2) control.Text = "";
                   
                  
                }
                else if ((hct.ThueGTGT == 0 && hct.ThueTTDB > 0)||(hct.ThueGTGT > 0 && hct.ThueTTDB > 0))
                {
                    decimal TriGiaTTTTDB = hct.TriGiaTT + hct.ThueXNK;
                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaTTGTGT" + (i + 1)];
                    control.Text = TriGiaTTTTDB.ToString("N0");

                    control = this.xrTable2.Rows[i + 2].Controls["ThueSuatGTGT" + (i + 1)];
                    control.Text = hct.ThueSuatTTDB.ToString("N0");
                 
                    control = this.xrTable2.Rows[i + 2].Controls["TienThueGTGT" + (i + 1)];
                    control.Text = hct.ThueTTDB.ToString("N0");

                    
                }
               
                //
                tongTriGiaNT += Math.Round(hct.TriGia, 2, MidpointRounding.AwayFromZero);
                tongThueXNK += hct.ThueXNK;
                if (hct.ThueGTGT > 0 && hct.ThueTTDB == 0)
                {

                    tongThueGTGT += hct.ThueGTGT;
                    tongTriGiaThuKhac += hct.PhuThu;
                }
                else if (hct.ThueTTDB > 0 && hct.ThueGTGT == 0)
                {

                    tongThueGTGT += hct.ThueTTDB;
                    tongTriGiaThuKhac += hct.PhuThu;
                }
                else if (hct.ThueTTDB > 0 && hct.ThueGTGT > 0)
                {
                    tongThueGTGT += hct.ThueTTDB;
                    tongTriGiaThuKhac += hct.ThueGTGT;
                }
                //}
            }
            tongthuetatca = tongThueXNK + tongTriGiaThuKhac + tongThueGTGT;
            lblTongbangso.Text = tongthuetatca.ToString("N0"); //chi tinh rieng phan hang tren phan phu luc, khong tinh tat va cac hang. //this.TinhTongThueHMD().ToString();
            string s = Company.GC.BLL.Utils.VNCurrency.ToString(tongthuetatca); //Company.BLL.Utils.VNCurrency.ToString((decimal)this.TinhTongThueHMD()).Trim();
            s = s[0].ToString().ToUpper() + s.Substring(1);
            lblTongThueXNKChu.Text = s.Replace("  ", " ");

            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");
            lblTongTienThueXNK.Text = tongThueXNK.ToString("N0");
            lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
            if (tongTriGiaThuKhac > 0)
                lblTongTriGiaThuKhac.Text = tongTriGiaThuKhac.ToString("N0");
            else
                lblTongTriGiaThuKhac.Text = "";
            if (MienThue1) lblTongTienThueXNK.Text = "";
            if (MienThue2) lblTongTienThueGTGT.Text = "";

        }
        public decimal TinhTongThueHMD()
        {
            decimal tong = 0;
            foreach (HangChuyenTiep hmd in this.TKCT.HCTCollection)
            {
                //if (!hmd.FOC)
                tong += hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.PhuThu;
            }
            return tong;
        }
        public void setVisibleImage(bool t)
        {
            //xrPictureBox1.Visible = t;
        }
        private bool IsHaveTax()
        {
            foreach (HangChuyenTiep hmd in this.TKCT.HCTCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.PhuThu) > 0) return true;
            return false;
        }
        public void ShowMienThue(bool t)
        {
            // lblThongBaoMienThue.Visible = t;
            xrTable2.Visible = !t;
            lblTongTienThueXNK.Visible = lblTongTienThueGTGT.Visible = lblTongTriGiaThuKhac.Visible = !t;
        }
        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        private void TenHang2_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang3_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang4_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang5_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang6_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang7_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang8_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang9_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang1_PreviewClick_1(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        //private void TenHang1_PreviewClick_1(object sender, PreviewMouseEventArgs e)
        //{
        //    XRControl cell = (XRControl)sender;
        //    report.Cell = cell;
        //    report.txtTenNhomHang.Text = cell.Text;
        //    report.label3.Text = cell.Tag.ToString();
        //}
    }
}
