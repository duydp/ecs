﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace Company.KDT.SHARE.VNACCS
{
    public class GlobalVNACC
    {
        public static string NguoiSuDung_Ma { set; get; }
        public static string NguoiSuDung_ID { set; get; }
        public static string NguoiSuDung_Pass { set; get; }
        public static string TerminalID { set; get; }
        public static string AccessKey { set; get; }
        public static string NguoiSuDung_DiaChi { set; get; }
        public static string PathConfig { set; get; }
        public static string Url { get; set; }
        public static int TimerRequest { get; set; }
        public static string TerminalAccessKey { get; set; }

        public static bool isStopRespone { get; set; }
        public static bool isResponding { get; set; }

        /// <summary>
        /// Thiet lap tao file Text du lieu khai bao (Dung cho VNACCS)
        /// </summary>
        public static bool IsGenerateTextDataVNACC { get; set; }

        public static bool GenerateTextData(string fileName, byte[] data)
        {
            try
            {
                //Dung dong code duoi de phan quyen tren folder
                Company.KDT.SHARE.Components.CommonApplicationData direc = new Company.KDT.SHARE.Components.CommonApplicationData(AppDomain.CurrentDomain.BaseDirectory, "TextData", true);

                //byte[] b1 = System.Text.Encoding.UTF8.GetBytes(data);

                return WriteFile(AppDomain.CurrentDomain.BaseDirectory + string.Format("TextData\\{0}.txt", fileName), data);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }

        /// <summary>
        /// Ghi file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="data"></param>
        public static bool WriteFile(string filePath, byte[] data)
        {
            try
            {
                // provide read access to the file
                FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite);

                //Read block of bytes from stream into the byte array
                fs.Write(data, 0, data.Length);

                //Close the File Stream
                fs.Close();

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }

        }
    }
}
