﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class ProductionNorm
    {
        /// <summary>
        /// Sản phẩm được khai báo định mức
        /// </summary>
        [XmlElement("Product")]
        public Product Product { get; set; }
        /// <summary>
        /// Chi tiết định mức
        /// </summary>
        [XmlElement("MaterialsNorm")]
        public List<MaterialsNorm> MaterialsNorms { get; set; }

    }
}
