IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'TINHBIENAG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '50BB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Tịnh Biên' WHERE TableID='A038A' AND CustomsCode = 'TINHBIENAG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','TINHBIENAG','50BB',N'Chi cục HQ CK Tịnh Biên')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HOIDONGAG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '50BC', CustomsOfficeNameInVietnamese =N'Chi cục HQ Vĩnh Hội Đông' WHERE TableID='A038A' AND CustomsCode = 'HOIDONGAG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HOIDONGAG','50BC',N'Chi cục HQ Vĩnh Hội Đông')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'VXUONGAG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '50BD', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Vĩnh Xương' WHERE TableID='A038A' AND CustomsCode = 'VXUONGAG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','VXUONGAG','50BD',N'Chi cục HQ CK Vĩnh Xương')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'BACDAIAG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '50BJ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Đai' WHERE TableID='A038A' AND CustomsCode = 'BACDAIAG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','BACDAIAG','50BJ',N'Chi cục HQ Bắc Đai')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KBINHAG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '50BK', CustomsOfficeNameInVietnamese =N'Chi cục HQ Khánh Bình' WHERE TableID='A038A' AND CustomsCode = 'KBINHAG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KBINHAG','50BK',N'Chi cục HQ Khánh Bình')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CMYTHOIAG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '50CE', CustomsOfficeNameInVietnamese =N'Chi cục HQ Cảng Mỹ Thới' WHERE TableID='A038A' AND CustomsCode = 'CMYTHOIAG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CMYTHOIAG','50CE',N'Chi cục HQ Cảng Mỹ Thới')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CCATLOVT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '51BE', CustomsOfficeNameInVietnamese =N'Chi cục HQ Cảng Cát Lở' WHERE TableID='A038A' AND CustomsCode = 'CCATLOVT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CCATLOVT','51BE',N'Chi cục HQ Cảng Cát Lở')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KNQPMVTAU')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '51C1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Phú Mỹ' WHERE TableID='A038A' AND CustomsCode = 'KNQPMVTAU'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KNQPMVTAU','51C1',N'Chi cục HQ CK Cảng Phú Mỹ')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'PSAPMVTAU')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '51C2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Phú Mỹ' WHERE TableID='A038A' AND CustomsCode = 'PSAPMVTAU'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','PSAPMVTAU','51C2',N'Chi cục HQ CK Cảng Phú Mỹ')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CSANBAYVT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '51CB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng - Sân bay Vũng Tàu' WHERE TableID='A038A' AND CustomsCode = 'CSANBAYVT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CSANBAYVT','51CB',N'Chi cục HQ CK Cảng - Sân bay Vũng Tàu')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CONDAOVT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '51CH', CustomsOfficeNameInVietnamese =N'Chi cục HQ Côn Đảo' WHERE TableID='A038A' AND CustomsCode = 'CONDAOVT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CONDAOVT','51CH',N'Chi cục HQ Côn Đảo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CCAIMEPVT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '51CI', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK cảng Cái Mép' WHERE TableID='A038A' AND CustomsCode = 'CCAIMEPVT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CCAIMEPVT','51CI',N'Chi cục HQ CK cảng Cái Mép')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DKCNYPBN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '18A1', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Ninh' WHERE TableID='A038A' AND CustomsCode = 'DKCNYPBN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DKCNYPBN','18A1',N'Chi cục HQ Bắc Ninh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DKCNQVBN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '18A2', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Ninh' WHERE TableID='A038A' AND CustomsCode = 'DKCNQVBN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DKCNQVBN','18A2',N'Chi cục HQ Bắc Ninh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVCCHQBN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '18A3', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Ninh' WHERE TableID='A038A' AND CustomsCode = 'DNVCCHQBN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVCCHQBN','18A3',N'Chi cục HQ Bắc Ninh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVTNBNINH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '18B1', CustomsOfficeNameInVietnamese =N'Chi cục HQ Thái Nguyên' WHERE TableID='A038A' AND CustomsCode = 'DNVTNBNINH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVTNBNINH','18B1',N'Chi cục HQ Thái Nguyên')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'YBINHTNBN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '18B2', CustomsOfficeNameInVietnamese =N'Chi cục HQ Thái Nguyên' WHERE TableID='A038A' AND CustomsCode = 'YBINHTNBN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','YBINHTNBN','18B2',N'Chi cục HQ Thái Nguyên')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'BACGIANGBN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '18BC', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý các KCN Bắc Giang' WHERE TableID='A038A' AND CustomsCode = 'BACGIANGBN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','BACGIANGBN','18BC',N'Chi cục HQ Quản lý các KCN Bắc Giang')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'TIENSONBN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '18ID', CustomsOfficeNameInVietnamese =N'Chi cục HQ Cảng nội địa Tiên Sơn' WHERE TableID='A038A' AND CustomsCode = 'TIENSONBN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','TIENSONBN','18ID',N'Chi cục HQ Cảng nội địa Tiên Sơn')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'QUINHONBD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '37CB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Qui Nhơn' WHERE TableID='A038A' AND CustomsCode = 'QUINHONBD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','QUINHONBD','37CB',N'Chi cục HQ CK Cảng Qui Nhơn')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'PHUYENBD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '37TC', CustomsOfficeNameInVietnamese =N'Chi cục HQ Phú Yên' WHERE TableID='A038A' AND CustomsCode = 'PHUYENBD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','PHUYENBD','37TC',N'Chi cục HQ Phú Yên')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CTHOPBD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '43CN', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng tổng hợp Bình Dương' WHERE TableID='A038A' AND CustomsCode = 'CTHOPBD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CTHOPBD','43CN',N'Chi cục HQ CK Cảng tổng hợp Bình Dương')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'SONGTHANBD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '43IH', CustomsOfficeNameInVietnamese =N'Chi cục HQ Sóng Thần' WHERE TableID='A038A' AND CustomsCode = 'SONGTHANBD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','SONGTHANBD','43IH',N'Chi cục HQ Sóng Thần')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVCCMPBD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '43K1', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Mỹ Phước' WHERE TableID='A038A' AND CustomsCode = 'DNVCCMPBD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVCCMPBD','43K1',N'Chi cục HQ KCN Mỹ Phước')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DKLHCCMPBD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '43K2', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Mỹ Phước' WHERE TableID='A038A' AND CustomsCode = 'DKLHCCMPBD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DKLHCCMPBD','43K2',N'Chi cục HQ KCN Mỹ Phước')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DTDCCMPBD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '43K3', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Mỹ Phước' WHERE TableID='A038A' AND CustomsCode = 'DTDCCMPBD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DTDCCMPBD','43K3',N'Chi cục HQ KCN Mỹ Phước')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KCNSTHANBD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '43ND', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Sóng Thần' WHERE TableID='A038A' AND CustomsCode = 'KCNSTHANBD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KCNSTHANBD','43ND',N'Chi cục HQ KCN Sóng Thần')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KCNVNSGBD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '43NF', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Việt Nam - Singapore' WHERE TableID='A038A' AND CustomsCode = 'KCNVNSGBD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KCNVNSGBD','43NF',N'Chi cục HQ KCN Việt Nam - Singapore')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'VHUONGBD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '43NG', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Việt Hương' WHERE TableID='A038A' AND CustomsCode = 'VHUONGBD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','VHUONGBD','43NG',N'Chi cục HQ KCN Việt Hương')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'NGOAIKCNBD')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '43PB', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng hóa XNK ngoài KCN' WHERE TableID='A038A' AND CustomsCode = 'NGOAIKCNBD'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','NGOAIKCNBD','43PB',N'Chi cục HQ Quản lý hàng hóa XNK ngoài KCN')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HOALUBP')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '61BA', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Hoa Lư' WHERE TableID='A038A' AND CustomsCode = 'HOALUBP'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HOALUBP','61BA',N'Chi cục HQ CK Quốc tế Hoa Lư')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HOALUBP')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '61BA', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Hoa Lư' WHERE TableID='A038A' AND CustomsCode = 'HOALUBP'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HOALUBP','61BA',N'Chi cục HQ CK Quốc tế Hoa Lư')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CTHANHBP')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '61PA', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chơn Thành' WHERE TableID='A038A' AND CustomsCode = 'CTHANHBP'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CTHANHBP','61PA',N'Chi cục HQ Chơn Thành')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CTHANHBP')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '61PA', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chơn Thành' WHERE TableID='A038A' AND CustomsCode = 'CTHANHBP'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CTHANHBP','61PA',N'Chi cục HQ Chơn Thành')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HDIEUBP')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '61BB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Hoàng Diệu' WHERE TableID='A038A' AND CustomsCode = 'HDIEUBP'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HDIEUBP','61BB',N'Chi cục HQ CK Hoàng Diệu')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HDIEUBP')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '61BB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Hoàng Diệu' WHERE TableID='A038A' AND CustomsCode = 'HDIEUBP'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HDIEUBP','61BB',N'Chi cục HQ CK Hoàng Diệu')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HOATRUNGCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '59BD', CustomsOfficeNameInVietnamese =N'Chi cục HQ Hòa Trung' WHERE TableID='A038A' AND CustomsCode = 'HOATRUNGCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HOATRUNGCM','59BD',N'Chi cục HQ Hòa Trung')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CNAMCANCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '59CB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Năm Căn' WHERE TableID='A038A' AND CustomsCode = 'CNAMCANCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CNAMCANCM','59CB',N'Chi cục HQ CK Cảng Năm Căn')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CANGCANTHO')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '54CB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Cần Thơ' WHERE TableID='A038A' AND CustomsCode = 'CANGCANTHO'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CANGCANTHO','54CB',N'Chi cục HQ CK Cảng Cần Thơ')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'VINHLONGCT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '54CD', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Vĩnh Long' WHERE TableID='A038A' AND CustomsCode = 'VINHLONGCT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','VINHLONGCT','54CD',N'Chi cục HQ CK Vĩnh Long')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'TAYDOCT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '54PH', CustomsOfficeNameInVietnamese =N'Chi cục HQ Tây Đô' WHERE TableID='A038A' AND CustomsCode = 'TAYDOCT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','TAYDOCT','54PH',N'Chi cục HQ Tây Đô')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'SOCTRANGCT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '54PK', CustomsOfficeNameInVietnamese =N'Chi cục HQ Sóc Trăng' WHERE TableID='A038A' AND CustomsCode = 'SOCTRANGCT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','SOCTRANGCT','54PK',N'Chi cục HQ Sóc Trăng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVTLCB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '11B1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Tà Lùng' WHERE TableID='A038A' AND CustomsCode = 'DNVTLCB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVTLCB','11B1',N'Chi cục HQ CK Tà Lùng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNV2TLCB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '11B2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Tà Lùng' WHERE TableID='A038A' AND CustomsCode = 'DNV2TLCB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNV2TLCB','11B2',N'Chi cục HQ CK Tà Lùng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'TRALINHCB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '11BE', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Trà Lĩnh' WHERE TableID='A038A' AND CustomsCode = 'TRALINHCB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','TRALINHCB','11BE',N'Chi cục HQ CK Trà Lĩnh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'SOCGIANGCB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '11BF', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sóc Giang' WHERE TableID='A038A' AND CustomsCode = 'SOCGIANGCB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','SOCGIANGCB','11BF',N'Chi cục HQ CK Sóc Giang')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'POPEOCB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '11BH', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Pò Peo' WHERE TableID='A038A' AND CustomsCode = 'POPEOCB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','POPEOCB','11BH',N'Chi cục HQ CK Pò Peo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVBHCBANG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '11G1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Bí Hà' WHERE TableID='A038A' AND CustomsCode = 'DNVBHCBANG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVBHCBANG','11G1',N'Chi cục HQ CK Bí Hà')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DLVBHCBANG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '11G2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Bí Hà' WHERE TableID='A038A' AND CustomsCode = 'DLVBHCBANG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DLVBHCBANG','11G2',N'Chi cục HQ CK Bí Hà')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'BACKANCB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '11PK', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Kạn' WHERE TableID='A038A' AND CustomsCode = 'BACKANCB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','BACKANCB','11PK',N'Chi cục HQ Bắc Kạn')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'SBQTDN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '34AB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng' WHERE TableID='A038A' AND CustomsCode = 'SBQTDN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','SBQTDN','34AB',N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'SBQTDN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '34AB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng' WHERE TableID='A038A' AND CustomsCode = 'SBQTDN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','SBQTDN','34AB',N'Chi cục HQ CK Sân bay Quốc tế Đà Nẵng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DTGCDANANG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '34CC', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE TableID='A038A' AND CustomsCode = 'DTGCDANANG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DTGCDANANG','34CC',N'Chi cục HQ Quản lý hàng đầu tư - gia công')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CANGDANANG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '34CE', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Đà Nẵng' WHERE TableID='A038A' AND CustomsCode = 'CANGDANANG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CANGDANANG','34CE',N'Chi cục HQ CK Cảng Đà Nẵng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HKHANHDN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '34NG', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Hòa Khánh - Liên Chiểu' WHERE TableID='A038A' AND CustomsCode = 'HKHANHDN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HKHANHDN','34NG',N'Chi cục HQ KCN Hòa Khánh - Liên Chiểu')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KCNDN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '34NH', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Đà Nẵng' WHERE TableID='A038A' AND CustomsCode = 'KCNDN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KCNDN','34NH',N'Chi cục HQ KCN Đà Nẵng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVIBRDL')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '40B1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK BupRăng' WHERE TableID='A038A' AND CustomsCode = 'DNVIBRDL'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVIBRDL','40B1',N'Chi cục HQ CK BupRăng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'BMTHUOTDL')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '40BC', CustomsOfficeNameInVietnamese =N'Chi cục HQ Buôn Mê Thuột' WHERE TableID='A038A' AND CustomsCode = 'BMTHUOTDL'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','BMTHUOTDL','40BC',N'Chi cục HQ Buôn Mê Thuột')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVDLATDL')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '40D1', CustomsOfficeNameInVietnamese =N'Chi cục HQ Đà Lạt' WHERE TableID='A038A' AND CustomsCode = 'DNVDLATDL'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVDLATDL','40D1',N'Chi cục HQ Đà Lạt')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVTTRGDB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '12B1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Tây Trang' WHERE TableID='A038A' AND CustomsCode = 'DNVTTRGDB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVTTRGDB','12B1',N'Chi cục HQ CK Quốc tế Tây Trang')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DHPUOCTTDB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '12B2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Tây Trang' WHERE TableID='A038A' AND CustomsCode = 'DHPUOCTTDB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DHPUOCTTDB','12B2',N'Chi cục HQ CK Quốc tế Tây Trang')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'LONGSAPDB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '12BE', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Lóng Sập' WHERE TableID='A038A' AND CustomsCode = 'LONGSAPDB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','LONGSAPDB','12BE',N'Chi cục HQ CK Lóng Sập')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CKHUONGDB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '12BI', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Chiềng Khương' WHERE TableID='A038A' AND CustomsCode = 'CKHUONGDB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CKHUONGDB','12BI',N'Chi cục HQ CK Chiềng Khương')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HQSONLADB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '12F1', CustomsOfficeNameInVietnamese =N'Chi cục HQ Sơn La' WHERE TableID='A038A' AND CustomsCode = 'HQSONLADB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HQSONLADB','12F1',N'Chi cục HQ Sơn La')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'NACAISLDB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '12F2', CustomsOfficeNameInVietnamese =N'Chi cục HQ Sơn La' WHERE TableID='A038A' AND CustomsCode = 'NACAISLDB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','NACAISLDB','12F2',N'Chi cục HQ Sơn La')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVMLTDB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '12H1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Ma Lu Thàng' WHERE TableID='A038A' AND CustomsCode = 'DNVMLTDB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVMLTDB','12H1',N'Chi cục HQ CK Ma Lu Thàng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DPOTOMLTDB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '12H2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Ma Lu Thàng' WHERE TableID='A038A' AND CustomsCode = 'DPOTOMLTDB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DPOTOMLTDB','12H2',N'Chi cục HQ CK Ma Lu Thàng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVCCLTDN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '47D1', CustomsOfficeNameInVietnamese =N'Chi cục HQ Long Thành' WHERE TableID='A038A' AND CustomsCode = 'DNVCCLTDN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVCCLTDN','47D1',N'Chi cục HQ Long Thành')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNV2CCLTDN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '47D2', CustomsOfficeNameInVietnamese =N'Chi cục HQ Long Thành' WHERE TableID='A038A' AND CustomsCode = 'DNV2CCLTDN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNV2CCLTDN','47D2',N'Chi cục HQ Long Thành')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNV3CCLTDN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '47D3', CustomsOfficeNameInVietnamese =N'Chi cục HQ Long Thành' WHERE TableID='A038A' AND CustomsCode = 'DNV3CCLTDN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNV3CCLTDN','47D3',N'Chi cục HQ Long Thành')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVLBTDN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '47I1', CustomsOfficeNameInVietnamese =N'Chi cục HQ Long Bình Tân' WHERE TableID='A038A' AND CustomsCode = 'DNVLBTDN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVLBTDN','47I1',N'Chi cục HQ Long Bình Tân')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNV2LBTDN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '47I2', CustomsOfficeNameInVietnamese =N'Chi cục HQ Long Bình Tân' WHERE TableID='A038A' AND CustomsCode = 'DNV2LBTDN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNV2LBTDN','47I2',N'Chi cục HQ Long Bình Tân')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'BIENHOADN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '47NB', CustomsOfficeNameInVietnamese =N'Chi cục HQ Biên Hoà' WHERE TableID='A038A' AND CustomsCode = 'BIENHOADN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','BIENHOADN','47NB',N'Chi cục HQ Biên Hoà')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'BIENHOADN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '47NB', CustomsOfficeNameInVietnamese =N'Chi cục HQ Biên Hoà' WHERE TableID='A038A' AND CustomsCode = 'BIENHOADN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','BIENHOADN','47NB',N'Chi cục HQ Biên Hoà')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'TNHATDN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '47NF', CustomsOfficeNameInVietnamese =N'Chi cục HQ Thống Nhất' WHERE TableID='A038A' AND CustomsCode = 'TNHATDN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','TNHATDN','47NF',N'Chi cục HQ Thống Nhất')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'NTRACHDN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '47NG', CustomsOfficeNameInVietnamese =N'Chi cục HQ Nhơn Trạch' WHERE TableID='A038A' AND CustomsCode = 'NTRACHDN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','NTRACHDN','47NG',N'Chi cục HQ Nhơn Trạch')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'BTHUANDN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '47NM', CustomsOfficeNameInVietnamese =N'Chi cục HQ QL KCN Bình Thuận' WHERE TableID='A038A' AND CustomsCode = 'BTHUANDN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','BTHUANDN','47NM',N'Chi cục HQ QL KCN Bình Thuận')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KCXLBINHDN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '47XE', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCX Long Bình' WHERE TableID='A038A' AND CustomsCode = 'KCXLBINHDN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KCXLBINHDN','47XE',N'Chi cục HQ KCX Long Bình')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'THPHUOCDT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '49BB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Thường Phước' WHERE TableID='A038A' AND CustomsCode = 'THPHUOCDT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','THPHUOCDT','49BB',N'Chi cục HQ CK Thường Phước')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'SOTHUONGDT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '49BE', CustomsOfficeNameInVietnamese =N'Chi cục HQ Sở Thượng' WHERE TableID='A038A' AND CustomsCode = 'SOTHUONGDT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','SOTHUONGDT','49BE',N'Chi cục HQ Sở Thượng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'THBINHDT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '49BF', CustomsOfficeNameInVietnamese =N'Chi cục HQ Thông Bình' WHERE TableID='A038A' AND CustomsCode = 'THBINHDT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','THBINHDT','49BF',N'Chi cục HQ Thông Bình')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DINHBADT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '49BG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Dinh Bà' WHERE TableID='A038A' AND CustomsCode = 'DINHBADT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DINHBADT','49BG',N'Chi cục HQ CK Dinh Bà')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CAOLANHCDT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '49C1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Đồng Tháp' WHERE TableID='A038A' AND CustomsCode = 'CAOLANHCDT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CAOLANHCDT','49C1',N'Chi cục HQ CK Cảng Đồng Tháp')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'SADECCDT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '49C2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Đồng Tháp' WHERE TableID='A038A' AND CustomsCode = 'SADECCDT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','SADECCDT','49C2',N'Chi cục HQ CK Cảng Đồng Tháp')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVTHLTGL')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '38B1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Lệ Thanh' WHERE TableID='A038A' AND CustomsCode = 'DNVTHLTGL'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVTHLTGL','38B1',N'Chi cục HQ CK Lệ Thanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DTTLTGL')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '38B2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Lệ Thanh' WHERE TableID='A038A' AND CustomsCode = 'DTTLTGL'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DTTLTGL','38B2',N'Chi cục HQ CK Lệ Thanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'BOYGL')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '38BC', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Bờ Y' WHERE TableID='A038A' AND CustomsCode = 'BOYGL'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','BOYGL','38BC',N'Chi cục HQ CK Bờ Y')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KONTUMGL')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '38PD', CustomsOfficeNameInVietnamese =N'Chi cục HQ Kon Tum' WHERE TableID='A038A' AND CustomsCode = 'KONTUMGL'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KONTUMGL','38PD',N'Chi cục HQ Kon Tum')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'TTHUYHG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '10BB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Thanh Thủy' WHERE TableID='A038A' AND CustomsCode = 'TTHUYHG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','TTHUYHG','10BB',N'Chi cục HQ CK Thanh Thủy')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'XINMANHG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '10BC', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Xín Mần' WHERE TableID='A038A' AND CustomsCode = 'XINMANHG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','XINMANHG','10BC',N'Chi cục HQ CK Xín Mần')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'PHOBANGHG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '10BD', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Phó Bảng' WHERE TableID='A038A' AND CustomsCode = 'PHOBANGHG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','PHOBANGHG','10BD',N'Chi cục HQ CK Phó Bảng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'SAMPUNHG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '10BF', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Săm Pun' WHERE TableID='A038A' AND CustomsCode = 'SAMPUNHG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','SAMPUNHG','10BF',N'Chi cục HQ CK Săm Pun')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'TQUANGHG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '10BI', CustomsOfficeNameInVietnamese =N'Chi cục HQ Tuyên Quang' WHERE TableID='A038A' AND CustomsCode = 'TQUANGHG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','TQUANGHG','10BI',N'Chi cục HQ Tuyên Quang')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DHHXNBHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01B1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay quốc tế Nội Bài' WHERE TableID='A038A' AND CustomsCode = 'DHHXNBHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DHHXNBHN','01B1',N'Chi cục HQ CK Sân bay quốc tế Nội Bài')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DCPNNBHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01B2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay quốc tế Nội Bài' WHERE TableID='A038A' AND CustomsCode = 'DCPNNBHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DCPNNBHN','01B2',N'Chi cục HQ CK Sân bay quốc tế Nội Bài')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DHHNNBHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01B3', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay quốc tế Nội Bài' WHERE TableID='A038A' AND CustomsCode = 'DHHNNBHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DHHNNBHN','01B3',N'Chi cục HQ CK Sân bay quốc tế Nội Bài')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DHLNKNBHN ')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01B6', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay quốc tế Nội Bài' WHERE TableID='A038A' AND CustomsCode = 'DHLNKNBHN '    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DHLNKNBHN ','01B6',N'Chi cục HQ CK Sân bay quốc tế Nội Bài')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DHLXKNBHN ')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01B5', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay quốc tế Nội Bài' WHERE TableID='A038A' AND CustomsCode = 'DHLXKNBHN '    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DHLXKNBHN ','01B5',N'Chi cục HQ CK Sân bay quốc tế Nội Bài')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'YENBAIHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01BT', CustomsOfficeNameInVietnamese =N'Chi cục HQ Yên Bái' WHERE TableID='A038A' AND CustomsCode = 'YENBAIHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','YENBAIHN','01BT',N'Chi cục HQ Yên Bái')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'MYDINHBDHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01D1', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bưu Điện TP Hà Nội' WHERE TableID='A038A' AND CustomsCode = 'MYDINHBDHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','MYDINHBDHN','01D1',N'Chi cục HQ Bưu Điện TP Hà Nội')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'FEDEXBDHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01D2', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bưu Điện TP Hà Nội' WHERE TableID='A038A' AND CustomsCode = 'FEDEXBDHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','FEDEXBDHN','01D2',N'Chi cục HQ Bưu Điện TP Hà Nội')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'UPSBDHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01D3', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bưu Điện TP Hà Nội' WHERE TableID='A038A' AND CustomsCode = 'UPSBDHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','UPSBDHN','01D3',N'Chi cục HQ Bưu Điện TP Hà Nội')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVBHNHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01E1', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Hà Nội' WHERE TableID='A038A' AND CustomsCode = 'DNVBHNHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVBHNHN','01E1',N'Chi cục HQ Bắc Hà Nội')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DHKBHNHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01E2', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Hà Nội' WHERE TableID='A038A' AND CustomsCode = 'DHKBHNHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DHKBHNHN','01E2',N'Chi cục HQ Bắc Hà Nội')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DCPNBHNHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01E3', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Hà Nội' WHERE TableID='A038A' AND CustomsCode = 'DCPNBHNHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DCPNBHNHN','01E3',N'Chi cục HQ Bắc Hà Nội')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'GIATHUYHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01IK', CustomsOfficeNameInVietnamese =N'Chi cục HQ Gia Thụy' WHERE TableID='A038A' AND CustomsCode = 'GIATHUYHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','GIATHUYHN','01IK',N'Chi cục HQ Gia Thụy')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DHDHTHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01M1', CustomsOfficeNameInVietnamese =N'Chi cục HQ Hà Tây' WHERE TableID='A038A' AND CustomsCode = 'DHDHTHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DHDHTHN','01M1',N'Chi cục HQ Hà Tây')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CNCHTHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01M2', CustomsOfficeNameInVietnamese =N'Chi cục HQ Hà Tây' WHERE TableID='A038A' AND CustomsCode = 'CNCHTHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CNCHTHN','01M2',N'Chi cục HQ Hà Tây')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'BTLONGHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01NV', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Bắc Thăng Long' WHERE TableID='A038A' AND CustomsCode = 'BTLONGHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','BTLONGHN','01NV',N'Chi cục HQ KCN Bắc Thăng Long')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'VIETTRIHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01PJ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Phú Thọ' WHERE TableID='A038A' AND CustomsCode = 'VIETTRIHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','VIETTRIHN','01PJ',N'Chi cục HQ Phú Thọ')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HQQLDTGCHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01PL', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE TableID='A038A' AND CustomsCode = 'HQQLDTGCHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HQQLDTGCHN','01PL',N'Chi cục HQ Quản lý hàng đầu tư - gia công')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'VINHPHUCHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01PR', CustomsOfficeNameInVietnamese =N'Chi cục HQ Vĩnh Phúc' WHERE TableID='A038A' AND CustomsCode = 'VINHPHUCHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','VINHPHUCHN','01PR',N'Chi cục HQ Vĩnh Phúc')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CCHQCPNHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01DD', CustomsOfficeNameInVietnamese =N'Chi cục HQ chuyển phát nhanh HN' WHERE TableID='A038A' AND CustomsCode = 'CCHQCPNHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CCHQCPNHN','01DD',N'Chi cục HQ chuyển phát nhanh HN')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CCHQCPNHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01DD', CustomsOfficeNameInVietnamese =N'Chi cục HQ chuyển phát nhanh HN' WHERE TableID='A038A' AND CustomsCode = 'CCHQCPNHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CCHQCPNHN','01DD',N'Chi cục HQ chuyển phát nhanh HN')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'GAYVIENHN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '01SI', CustomsOfficeNameInVietnamese =N'Chi cục HQ Ga đường sắt quốc tế Yên Viên' WHERE TableID='A038A' AND CustomsCode = 'GAYVIENHN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','GAYVIENHN','01SI',N'Chi cục HQ Ga đường sắt quốc tế Yên Viên')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CAUTREOHT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '30BB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Cầu Treo' WHERE TableID='A038A' AND CustomsCode = 'CAUTREOHT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CAUTREOHT','30BB',N'Chi cục HQ CK Quốc tế Cầu Treo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HONGLINHHT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '30BE', CustomsOfficeNameInVietnamese =N'Chi cục HQ Hồng Lĩnh' WHERE TableID='A038A' AND CustomsCode = 'HONGLINHHT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HONGLINHHT','30BE',N'Chi cục HQ Hồng Lĩnh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KKTCTREOHT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '30BI', CustomsOfficeNameInVietnamese =N'Chi cục HQ khu kinh tế CK Cầu Treo' WHERE TableID='A038A' AND CustomsCode = 'KKTCTREOHT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KKTCTREOHT','30BI',N'Chi cục HQ khu kinh tế CK Cầu Treo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CXHAIHT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '30CC', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Xuân Hải' WHERE TableID='A038A' AND CustomsCode = 'CXHAIHT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CXHAIHT','30CC',N'Chi cục HQ CK Cảng Xuân Hải')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVVANGHT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '30F1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Vũng Áng' WHERE TableID='A038A' AND CustomsCode = 'DNVVANGHT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVVANGHT','30F1',N'Chi cục HQ CK Cảng Vũng Áng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'SDUONGVAHT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '30F2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Vũng Áng' WHERE TableID='A038A' AND CustomsCode = 'SDUONGVAHT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','SDUONGVAHT','30F2',N'Chi cục HQ CK Cảng Vũng Áng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CANGHPKVI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '03CC', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK cảng Hải Phòng KV I' WHERE TableID='A038A' AND CustomsCode = 'CANGHPKVI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CANGHPKVI','03CC',N'Chi cục HQ CK cảng Hải Phòng KV I')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'THAIBINHHP')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '03CD', CustomsOfficeNameInVietnamese =N'Chi cục HQ Thái Bình' WHERE TableID='A038A' AND CustomsCode = 'THAIBINHHP'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','THAIBINHHP','03CD',N'Chi cục HQ Thái Bình')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CANGHPKVII')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '03CE', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK cảng Hải Phòng KV II' WHERE TableID='A038A' AND CustomsCode = 'CANGHPKVII'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CANGHPKVII','03CE',N'Chi cục HQ CK cảng Hải Phòng KV II')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CDINHVUHP')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '03EE', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Đình Vũ' WHERE TableID='A038A' AND CustomsCode = 'CDINHVUHP'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CDINHVUHP','03EE',N'Chi cục HQ CK Cảng Đình Vũ')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KCXKCNHP')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '03NK', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCX và KCN' WHERE TableID='A038A' AND CustomsCode = 'KCXKCNHP'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KCXKCNHP','03NK',N'Chi cục HQ KCX và KCN')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DTGCHP')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '03PA', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE TableID='A038A' AND CustomsCode = 'DTGCHP'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DTGCHP','03PA',N'Chi cục HQ Quản lý hàng đầu tư - gia công')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DTGCHP')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '03PA', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng đầu tư - gia công' WHERE TableID='A038A' AND CustomsCode = 'DTGCHP'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DTGCHP','03PA',N'Chi cục HQ Quản lý hàng đầu tư - gia công')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HAIDUONGHP')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '03PJ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Hải Dương' WHERE TableID='A038A' AND CustomsCode = 'HAIDUONGHP'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HAIDUONGHP','03PJ',N'Chi cục HQ Hải Dương')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HUNGYENHP')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '03PL', CustomsOfficeNameInVietnamese =N'Chi cục HQ Hưng Yên' WHERE TableID='A038A' AND CustomsCode = 'HUNGYENHP'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HUNGYENHP','03PL',N'Chi cục HQ Hưng Yên')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CHPKVIII')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '03TG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK cảng Hải Phòng KV III' WHERE TableID='A038A' AND CustomsCode = 'CHPKVIII'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CHPKVIII','03TG',N'Chi cục HQ CK cảng Hải Phòng KV III')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CHPKVIII')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '03TG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK cảng Hải Phòng KV III' WHERE TableID='A038A' AND CustomsCode = 'CHPKVIII'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CHPKVIII','03TG',N'Chi cục HQ CK cảng Hải Phòng KV III')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'NTHUANKH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '41BH', CustomsOfficeNameInVietnamese =N'Chi cục HQ Ninh Thuận' WHERE TableID='A038A' AND CustomsCode = 'NTHUANKH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','NTHUANKH','41BH',N'Chi cục HQ Ninh Thuận')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'NHATRANGKH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '41CB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Nha Trang' WHERE TableID='A038A' AND CustomsCode = 'NHATRANGKH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','NHATRANGKH','41CB',N'Chi cục HQ CK Cảng Nha Trang')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CAMRANHKH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '41CC', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Cam Ranh' WHERE TableID='A038A' AND CustomsCode = 'CAMRANHKH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CAMRANHKH','41CC',N'Chi cục HQ CK Cảng Cam Ranh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CCHQCKSBCR')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '41AB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK sân bay quốc tế Cam Ranh' WHERE TableID='A038A' AND CustomsCode = 'CCHQCKSBCR'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CCHQCKSBCR','41AB',N'Chi cục HQ CK sân bay quốc tế Cam Ranh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'VANPHONGKH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '41PE', CustomsOfficeNameInVietnamese =N'Chi cục HQ Vân Phong' WHERE TableID='A038A' AND CustomsCode = 'VANPHONGKH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','VANPHONGKH','41PE',N'Chi cục HQ Vân Phong')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HATIENKG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '53BC', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc Tế Hà Tiên' WHERE TableID='A038A' AND CustomsCode = 'HATIENKG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HATIENKG','53BC',N'Chi cục HQ CK Quốc Tế Hà Tiên')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'GTHANHKG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '53BK', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Giang Thành' WHERE TableID='A038A' AND CustomsCode = 'GTHANHKG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','GTHANHKG','53BK',N'Chi cục HQ CK Giang Thành')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CHCHONGKG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '53CD', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Hòn Chông' WHERE TableID='A038A' AND CustomsCode = 'CHCHONGKG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CHCHONGKG','53CD',N'Chi cục HQ CK Cảng Hòn Chông')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'PHUQUOCKG')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '53CH', CustomsOfficeNameInVietnamese =N'Chi cục HQ Phú Quốc' WHERE TableID='A038A' AND CustomsCode = 'PHUQUOCKG'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','PHUQUOCKG','53CH',N'Chi cục HQ Phú Quốc')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVHNLSON')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '15B1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Hữu Nghị' WHERE TableID='A038A' AND CustomsCode = 'DNVHNLSON'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVHNLSON','15B1',N'Chi cục HQ CK Hữu Nghị')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVCSLSON')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '15B2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Hữu Nghị' WHERE TableID='A038A' AND CustomsCode = 'DNVCSLSON'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVCSLSON','15B2',N'Chi cục HQ CK Hữu Nghị')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVPNLSON ')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '15B3', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Hữu Nghị' WHERE TableID='A038A' AND CustomsCode = 'DNVPNLSON '    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVPNLSON ','15B3',N'Chi cục HQ CK Hữu Nghị')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CHIMALS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '15BC', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Chi Ma' WHERE TableID='A038A' AND CustomsCode = 'CHIMALS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CHIMALS','15BC',N'Chi cục HQ CK Chi Ma')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'COCNAMLS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '15BD', CustomsOfficeNameInVietnamese =N'Chi cục HQ Cốc Nam' WHERE TableID='A038A' AND CustomsCode = 'COCNAMLS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','COCNAMLS','15BD',N'Chi cục HQ Cốc Nam')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'NHINHTTLS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '15E1', CustomsOfficeNameInVietnamese =N'Chi cục HQ Tân Thanh' WHERE TableID='A038A' AND CustomsCode = 'NHINHTTLS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','NHINHTTLS','15E1',N'Chi cục HQ Tân Thanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'NNUATTLS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '15E2', CustomsOfficeNameInVietnamese =N'Chi cục HQ Tân Thanh' WHERE TableID='A038A' AND CustomsCode = 'NNUATTLS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','NNUATTLS','15E2',N'Chi cục HQ Tân Thanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'BNGHITTLS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '15E3', CustomsOfficeNameInVietnamese =N'Chi cục HQ Tân Thanh' WHERE TableID='A038A' AND CustomsCode = 'BNGHITTLS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','BNGHITTLS','15E3',N'Chi cục HQ Tân Thanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVTTLS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '15E4', CustomsOfficeNameInVietnamese =N'Chi cục HQ Tân Thanh' WHERE TableID='A038A' AND CustomsCode = 'DNVTTLS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVTTLS','15E4',N'Chi cục HQ Tân Thanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DONGDANGLS')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '15SI', CustomsOfficeNameInVietnamese =N'Chi cục HQ Ga Đồng Đăng' WHERE TableID='A038A' AND CustomsCode = 'DONGDANGLS'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DONGDANGLS','15SI',N'Chi cục HQ Ga Đồng Đăng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CCCKLAOCAI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '13BB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Lào Cai' WHERE TableID='A038A' AND CustomsCode = 'CCCKLAOCAI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CCCKLAOCAI','13BB',N'Chi cục HQ CK Quốc tế Lào Cai')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CCCKLAOCAI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '13BB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Lào Cai' WHERE TableID='A038A' AND CustomsCode = 'CCCKLAOCAI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CCCKLAOCAI','13BB',N'Chi cục HQ CK Quốc tế Lào Cai')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'M.KHUONGLC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '13BC', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Mường Khương' WHERE TableID='A038A' AND CustomsCode = 'M.KHUONGLC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','M.KHUONGLC','13BC',N'Chi cục HQ CK Mường Khương')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'BATXATLC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '13BD', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Bát Xát' WHERE TableID='A038A' AND CustomsCode = 'BATXATLC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','BATXATLC','13BD',N'Chi cục HQ CK Bát Xát')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVDSATLC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '13G1', CustomsOfficeNameInVietnamese =N'Chi cục HQ Đường sắt LVQT Lào Cai' WHERE TableID='A038A' AND CustomsCode = 'DNVDSATLC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVDSATLC','13G1',N'Chi cục HQ Đường sắt LVQT Lào Cai')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'VNLDSATLC')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '13G2', CustomsOfficeNameInVietnamese =N'Chi cục HQ Đường sắt LVQT Lào Cai' WHERE TableID='A038A' AND CustomsCode = 'VNLDSATLC'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','VNLDSATLC','13G2',N'Chi cục HQ Đường sắt LVQT Lào Cai')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'MQTAYLA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '48BC', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Mỹ Quý Tây' WHERE TableID='A038A' AND CustomsCode = 'MQTAYLA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','MQTAYLA','48BC',N'Chi cục HQ CK Mỹ Quý Tây')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'BINHHIEPLA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '48BD', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Bình Hiệp' WHERE TableID='A038A' AND CustomsCode = 'BINHHIEPLA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','BINHHIEPLA','48BD',N'Chi cục HQ CK Quốc tế Bình Hiệp')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HUNGDIENLA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '48BE', CustomsOfficeNameInVietnamese =N'Chi cục HQ Hưng Điền' WHERE TableID='A038A' AND CustomsCode = 'HUNGDIENLA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HUNGDIENLA','48BE',N'Chi cục HQ Hưng Điền')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DUCHOALA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '48BI', CustomsOfficeNameInVietnamese =N'Chi cục HQ Đức Hòa' WHERE TableID='A038A' AND CustomsCode = 'DUCHOALA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DUCHOALA','48BI',N'Chi cục HQ Đức Hòa')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'MYTHOLA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '48CG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Mỹ Tho' WHERE TableID='A038A' AND CustomsCode = 'MYTHOLA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','MYTHOLA','48CG',N'Chi cục HQ CK Cảng Mỹ Tho')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'LHAUBLLA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '48F1', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bến Lức' WHERE TableID='A038A' AND CustomsCode = 'LHAUBLLA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','LHAUBLLA','48F1',N'Chi cục HQ Bến Lức')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DTTBLLA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '48F2', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bến Lức' WHERE TableID='A038A' AND CustomsCode = 'DTTBLLA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DTTBLLA','48F2',N'Chi cục HQ Bến Lức')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'NAMCANNA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '29BB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Nậm Cắn' WHERE TableID='A038A' AND CustomsCode = 'NAMCANNA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','NAMCANNA','29BB',N'Chi cục HQ CK Quốc tế Nậm Cắn')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'TTHUYNA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '29BH', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Thanh Thủy' WHERE TableID='A038A' AND CustomsCode = 'TTHUYNA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','TTHUYNA','29BH',N'Chi cục HQ CK Thanh Thủy')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CANGNGHEAN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '29CC', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng' WHERE TableID='A038A' AND CustomsCode = 'CANGNGHEAN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CANGNGHEAN','29CC',N'Chi cục HQ CK Cảng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'VINHNA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '29PF', CustomsOfficeNameInVietnamese =N'Chi cục HQ Vinh' WHERE TableID='A038A' AND CustomsCode = 'VINHNA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','VINHNA','29PF',N'Chi cục HQ Vinh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CHALOQB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '31BB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cha Lo' WHERE TableID='A038A' AND CustomsCode = 'CHALOQB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CHALOQB','31BB',N'Chi cục HQ CK Cha Lo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CAROONGQB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '31BF', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cà Roòng' WHERE TableID='A038A' AND CustomsCode = 'CAROONGQB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CAROONGQB','31BF',N'Chi cục HQ CK Cà Roòng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CHONLAHLQB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '31D1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Hòn La' WHERE TableID='A038A' AND CustomsCode = 'CHONLAHLQB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CHONLAHLQB','31D1',N'Chi cục HQ CK Cảng Hòn La')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DHOIHLQB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '31D2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Hòn La' WHERE TableID='A038A' AND CustomsCode = 'DHOIHLQB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DHOIHLQB','31D2',N'Chi cục HQ CK Cảng Hòn La')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CGIANHHLQB')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '31D3', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Hòn La' WHERE TableID='A038A' AND CustomsCode = 'CGIANHHLQB'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CGIANHHLQB','31D3',N'Chi cục HQ CK Cảng Hòn La')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'NAMGIANGQN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '60BD', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Nam Giang' WHERE TableID='A038A' AND CustomsCode = 'NAMGIANGQN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','NAMGIANGQN','60BD',N'Chi cục HQ CK Nam Giang')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNAMDNGCQN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '60C1', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Điện Nam - Điện Ngọc' WHERE TableID='A038A' AND CustomsCode = 'DNAMDNGCQN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNAMDNGCQN','60C1',N'Chi cục HQ KCN Điện Nam - Điện Ngọc')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNAMDNGCQN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '60C2', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Điện Nam - Điện Ngọc' WHERE TableID='A038A' AND CustomsCode = 'DNAMDNGCQN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNAMDNGCQN','60C2',N'Chi cục HQ KCN Điện Nam - Điện Ngọc')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KYHAQN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '60CB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Kỳ Hà' WHERE TableID='A038A' AND CustomsCode = 'KYHAQN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KYHAQN','60CB',N'Chi cục HQ CK Cảng Kỳ Hà')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CDQUATQN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '35CB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Dung Quất' WHERE TableID='A038A' AND CustomsCode = 'CDQUATQN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CDQUATQN','35CB',N'Chi cục HQ CK Cảng Dung Quất')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KCNQN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '35NC', CustomsOfficeNameInVietnamese =N'Chi cục HQ các KCN Quảng Ngãi' WHERE TableID='A038A' AND CustomsCode = 'KCNQN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KCNQN','35NC',N'Chi cục HQ các KCN Quảng Ngãi')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'BLUANMCQN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '20B1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Móng Cái' WHERE TableID='A038A' AND CustomsCode = 'BLUANMCQN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','BLUANMCQN','20B1',N'Chi cục HQ CK Móng Cái')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KLONGMCQN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '20B2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Móng Cái' WHERE TableID='A038A' AND CustomsCode = 'KLONGMCQN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KLONGMCQN','20B2',N'Chi cục HQ CK Móng Cái')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HOANHMOQN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '20BC', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Hoành Mô' WHERE TableID='A038A' AND CustomsCode = 'HOANHMOQN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HOANHMOQN','20BC',N'Chi cục HQ CK Hoành Mô')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'PSINHQN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '20BD', CustomsOfficeNameInVietnamese =N'Chi cục HQ Bắc Phong Sinh' WHERE TableID='A038A' AND CustomsCode = 'PSINHQN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','PSINHQN','20BD',N'Chi cục HQ Bắc Phong Sinh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CAILANQN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '20CD', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Cái Lân' WHERE TableID='A038A' AND CustomsCode = 'CAILANQN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CAILANQN','20CD',N'Chi cục HQ CK Cảng Cái Lân')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'VANGIAQN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '20CE', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Vạn Gia' WHERE TableID='A038A' AND CustomsCode = 'VANGIAQN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','VANGIAQN','20CE',N'Chi cục HQ CK Cảng Vạn Gia')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HONGAIQN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '20CF', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Hòn Gai' WHERE TableID='A038A' AND CustomsCode = 'HONGAIQN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HONGAIQN','20CF',N'Chi cục HQ CK Cảng Hòn Gai')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CAMPHAQN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '20CG', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Cẩm Phả' WHERE TableID='A038A' AND CustomsCode = 'CAMPHAQN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CAMPHAQN','20CG',N'Chi cục HQ CK Cảng Cẩm Phả')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'LAOBAOQT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '32BB', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Lao Bảo' WHERE TableID='A038A' AND CustomsCode = 'LAOBAOQT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','LAOBAOQT','32BB',N'Chi cục HQ CK Lao Bảo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'LALAYQT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '32BC', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK La Lay' WHERE TableID='A038A' AND CustomsCode = 'LALAYQT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','LALAYQT','32BC',N'Chi cục HQ CK La Lay')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KTMAILBQT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '32BD', CustomsOfficeNameInVietnamese =N'Chi cục HQ Khu thương mại Lao Bảo' WHERE TableID='A038A' AND CustomsCode = 'KTMAILBQT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KTMAILBQT','32BD',N'Chi cục HQ Khu thương mại Lao Bảo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CCUAVIETQT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '32CD', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Cửa Việt' WHERE TableID='A038A' AND CustomsCode = 'CCUAVIETQT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CCUAVIETQT','32CD',N'Chi cục HQ CK Cảng Cửa Việt')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KSOATHQQT')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '32VG', CustomsOfficeNameInVietnamese =N'Đội Kiẻm soát HQ Quảng Trị' WHERE TableID='A038A' AND CustomsCode = 'KSOATHQQT'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KSOATHQQT','32VG',N'Đội Kiẻm soát HQ Quảng Trị')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVMBAITN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '45B1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Mộc Bài' WHERE TableID='A038A' AND CustomsCode = 'DNVMBAITN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVMBAITN','45B1',N'Chi cục HQ CK Mộc Bài')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KTMCNMBTN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '45B2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Mộc Bài' WHERE TableID='A038A' AND CustomsCode = 'KTMCNMBTN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KTMCNMBTN','45B2',N'Chi cục HQ CK Mộc Bài')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'PHUOCTANTN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '45BD', CustomsOfficeNameInVietnamese =N'Chi cục HQ Phước Tân' WHERE TableID='A038A' AND CustomsCode = 'PHUOCTANTN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','PHUOCTANTN','45BD',N'Chi cục HQ Phước Tân')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'KATUMTN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '45BE', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Kà Tum' WHERE TableID='A038A' AND CustomsCode = 'KATUMTN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','KATUMTN','45BE',N'Chi cục HQ CK Kà Tum')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVXAMATTN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '45C1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Xa Mát' WHERE TableID='A038A' AND CustomsCode = 'DNVXAMATTN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVXAMATTN','45C1',N'Chi cục HQ CK Xa Mát')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DCRXAMATTN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '45C2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Xa Mát' WHERE TableID='A038A' AND CustomsCode = 'DCRXAMATTN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DCRXAMATTN','45C2',N'Chi cục HQ CK Xa Mát')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVTBANGTN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '45F1', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Trảng Bàng' WHERE TableID='A038A' AND CustomsCode = 'DNVTBANGTN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVTBANGTN','45F1',N'Chi cục HQ KCN Trảng Bàng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'PDONGTBTN')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '45F2', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCN Trảng Bàng' WHERE TableID='A038A' AND CustomsCode = 'PDONGTBTN'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','PDONGTBTN','45F2',N'Chi cục HQ KCN Trảng Bàng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DNVNMEOTH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '27B1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Na Mèo' WHERE TableID='A038A' AND CustomsCode = 'DNVNMEOTH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DNVNMEOTH','27B1',N'Chi cục HQ CK Quốc tế Na Mèo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DTTNMEOTH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '27B2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Quốc tế Na Mèo' WHERE TableID='A038A' AND CustomsCode = 'DTTNMEOTH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DTTNMEOTH','27B2',N'Chi cục HQ CK Quốc tế Na Mèo')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CTHANHHOA')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '27F1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Thanh Hóa' WHERE TableID='A038A' AND CustomsCode = 'CTHANHHOA'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CTHANHHOA','27F1',N'Chi cục HQ CK Cảng Thanh Hóa')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CNGSONTH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '27F2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Nghi Sơn' WHERE TableID='A038A' AND CustomsCode = 'CNGSONTH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CNGSONTH','27F2',N'Chi cục HQ CK Cảng Nghi Sơn')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'HANAMTH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '27NJ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý các KCN Hà Nam' WHERE TableID='A038A' AND CustomsCode = 'HANAMTH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','HANAMTH','27NJ',N'Chi cục HQ Quản lý các KCN Hà Nam')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'NINHBINHTH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '27PC', CustomsOfficeNameInVietnamese =N'Chi cục HQ Ninh Bình' WHERE TableID='A038A' AND CustomsCode = 'NINHBINHTH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','NINHBINHTH','27PC',N'Chi cục HQ Ninh Bình')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'NINHBINHTH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '27PC', CustomsOfficeNameInVietnamese =N'Chi cục HQ Ninh Bình' WHERE TableID='A038A' AND CustomsCode = 'NINHBINHTH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','NINHBINHTH','27PC',N'Chi cục HQ Ninh Bình')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'NAMDINHTH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '27PE', CustomsOfficeNameInVietnamese =N'Chi cục HQ Nam Định' WHERE TableID='A038A' AND CustomsCode = 'NAMDINHTH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','NAMDINHTH','27PE',N'Chi cục HQ Nam Định')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'ADOTTTH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '33BA', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK A Đớt' WHERE TableID='A038A' AND CustomsCode = 'ADOTTTH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','ADOTTTH','33BA',N'Chi cục HQ CK A Đớt')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'ADOTTTH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '33BA', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK A Đớt' WHERE TableID='A038A' AND CustomsCode = 'ADOTTTH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','ADOTTTH','33BA',N'Chi cục HQ CK A Đớt')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CTANTTH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '33CC', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Thuận An' WHERE TableID='A038A' AND CustomsCode = 'CTANTTH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CTANTTH','33CC',N'Chi cục HQ CK Cảng Thuận An')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CCMAYTTH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '33CF', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Chân Mây' WHERE TableID='A038A' AND CustomsCode = 'CCMAYTTH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CCMAYTTH','33CF',N'Chi cục HQ CK Cảng Chân Mây')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'THUYANTTH')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '33PD', CustomsOfficeNameInVietnamese =N'Chi cục HQ Thủy An' WHERE TableID='A038A' AND CustomsCode = 'THUYANTTH'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','THUYANTTH','33PD',N'Chi cục HQ Thủy An')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02DS  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CPNHANHHCM','02DS  ',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02DS  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CPNHANHHCM','02DS  ',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02DS  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CPNHANHHCM','02DS  ',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02DS  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CPNHANHHCM','02DS  ',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02DS  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CPNHANHHCM','02DS  ',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02DS  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CPNHANHHCM','02DS  ',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02DS  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CPNHANHHCM','02DS  ',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02DS  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CPNHANHHCM','02DS  ',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02DS  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CPNHANHHCM','02DS  ',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02DS  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CPNHANHHCM','02DS  ',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02DS  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CPNHANHHCM','02DS  ',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02DS  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Chuyển phát nhanh' WHERE TableID='A038A' AND CustomsCode = 'CPNHANHHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CPNHANHHCM','02DS  ',N'Chi cục HQ Chuyển phát nhanh')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CHPHUOCHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02CV  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Hiệp Phước' WHERE TableID='A038A' AND CustomsCode = 'CHPHUOCHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CHPHUOCHCM','02CV  ',N'Chi cục HQ CK Cảng Hiệp Phước')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CHPHUOCHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02CV  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Hiệp Phước' WHERE TableID='A038A' AND CustomsCode = 'CHPHUOCHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CHPHUOCHCM','02CV  ',N'Chi cục HQ CK Cảng Hiệp Phước')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CSGONKVI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02CI  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV I' WHERE TableID='A038A' AND CustomsCode = 'CSGONKVI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CSGONKVI','02CI  ',N'Chi cục HQ CK Cảng Sài Gòn KV I')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CSGONKVI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02CI  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV I' WHERE TableID='A038A' AND CustomsCode = 'CSGONKVI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CSGONKVI','02CI  ',N'Chi cục HQ CK Cảng Sài Gòn KV I')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CSGONKVI')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02CI  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV I' WHERE TableID='A038A' AND CustomsCode = 'CSGONKVI'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CSGONKVI','02CI  ',N'Chi cục HQ CK Cảng Sài Gòn KV I')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CSGONKVII')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02CC  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV II' WHERE TableID='A038A' AND CustomsCode = 'CSGONKVII'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CSGONKVII','02CC  ',N'Chi cục HQ CK Cảng Sài Gòn KV II')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CSGONKVII')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02CC  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV II' WHERE TableID='A038A' AND CustomsCode = 'CSGONKVII'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CSGONKVII','02CC  ',N'Chi cục HQ CK Cảng Sài Gòn KV II')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CBNSGKVIII')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02H1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV III' WHERE TableID='A038A' AND CustomsCode = 'CBNSGKVIII'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CBNSGKVIII','02H1',N'Chi cục HQ CK Cảng Sài Gòn KV III')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'GSXDKVIII')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02H2', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV III' WHERE TableID='A038A' AND CustomsCode = 'GSXDKVIII'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','GSXDKVIII','02H2',N'Chi cục HQ CK Cảng Sài Gòn KV III')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CVICTKVIII')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02H3', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV III' WHERE TableID='A038A' AND CustomsCode = 'CVICTKVIII'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CVICTKVIII','02H3',N'Chi cục HQ CK Cảng Sài Gòn KV III')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CCSGKVIV')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02IK  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV IV' WHERE TableID='A038A' AND CustomsCode = 'CCSGKVIV'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CCSGKVIV','02IK  ',N'Chi cục HQ CK Cảng Sài Gòn KV IV')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CCSGKVIV')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02IK  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Cảng Sài Gòn KV IV' WHERE TableID='A038A' AND CustomsCode = 'CCSGKVIV'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CCSGKVIV','02IK  ',N'Chi cục HQ CK Cảng Sài Gòn KV IV')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'DTCSTSNHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02B1', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay Quốc tế Tân Sơn Nhất' WHERE TableID='A038A' AND CustomsCode = 'DTCSTSNHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','DTCSTSNHCM','02B1',N'Chi cục HQ CK Sân bay Quốc tế Tân Sơn Nhất')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'SCSCTSNHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02B4', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Sân bay Quốc tế Tân Sơn Nhất' WHERE TableID='A038A' AND CustomsCode = 'SCSCTSNHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','SCSCTSNHCM','02B4',N'Chi cục HQ CK Sân bay Quốc tế Tân Sơn Nhất')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CTCANGHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02CX  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ CK Tân Cảng' WHERE TableID='A038A' AND CustomsCode = 'CTCANGHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CTCANGHCM','02CX  ',N'Chi cục HQ CK Tân Cảng')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'LTILTHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02F1', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCX Linh Trung' WHERE TableID='A038A' AND CustomsCode = 'LTILTHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','LTILTHCM','02F1',N'Chi cục HQ KCX Linh Trung')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'LTILTHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02F1', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCX Linh Trung' WHERE TableID='A038A' AND CustomsCode = 'LTILTHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','LTILTHCM','02F1',N'Chi cục HQ KCX Linh Trung')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'LTIILTHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02F2', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCX Linh Trung' WHERE TableID='A038A' AND CustomsCode = 'LTIILTHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','LTIILTHCM','02F2',N'Chi cục HQ KCX Linh Trung')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CNCLTHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02F3', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCX Linh Trung' WHERE TableID='A038A' AND CustomsCode = 'CNCLTHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CNCLTHCM','02F3',N'Chi cục HQ KCX Linh Trung')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'TTHUANHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02XE  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ KCX Tân Thuận' WHERE TableID='A038A' AND CustomsCode = 'TTHUANHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','TTHUANHCM','02XE  ',N'Chi cục HQ KCX Tân Thuận')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CCHQDTHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02PG  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng đầu tư' WHERE TableID='A038A' AND CustomsCode = 'CCHQDTHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CCHQDTHCM','02PG  ',N'Chi cục HQ Quản lý hàng đầu tư')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CCHQDTHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02PG  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng đầu tư' WHERE TableID='A038A' AND CustomsCode = 'CCHQDTHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CCHQDTHCM','02PG  ',N'Chi cục HQ Quản lý hàng đầu tư')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CCHQGCHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02PJ  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng gia công' WHERE TableID='A038A' AND CustomsCode = 'CCHQGCHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CCHQGCHCM','02PJ  ',N'Chi cục HQ Quản lý hàng gia công')   
END

IF EXISTS (SELECT * FROM [t_VNACC_Category_CustomsOffice] WHERE TableID='A038A' AND CustomsCode = 'CCHQGCHCM')
BEGIN
 UPDATE [t_VNACC_Category_CustomsOffice] SET CustomsOfficeName = '02PJ  ', CustomsOfficeNameInVietnamese =N'Chi cục HQ Quản lý hàng gia công' WHERE TableID='A038A' AND CustomsCode = 'CCHQGCHCM'    
END
ELSE
BEGIN
 INSERT INTO [t_VNACC_Category_CustomsOffice](TableID,CustomsCode,CustomsOfficeName,CustomsOfficeNameInVietnamese) VALUES('A038A','CCHQGCHCM','02PJ  ',N'Chi cục HQ Quản lý hàng gia công')   
END

DELETE FROM dbo.t_VNACC_Category_CustomsOffice WHERE TableID='A038A' AND CustomsCode='01B2'

DELETE FROM dbo.t_VNACC_Category_CustomsOffice WHERE TableID='A038A' AND CustomsCode='01D1'

DELETE FROM dbo.t_VNACC_Category_CustomsOffice WHERE TableID='A038A' AND CustomsCode='01D2'

DELETE FROM dbo.t_VNACC_Category_CustomsOffice WHERE TableID='A038A' AND CustomsCode='01D3'

DELETE FROM dbo.t_VNACC_Category_CustomsOffice WHERE TableID='A038A' AND CustomsCode='01E3'

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '19.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('19.9',GETDATE(), N'Cập nhật chi cục HQ ')
END	