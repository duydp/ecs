
IF (SELECT COUNT(*) FROM [dbo].[t_HaiQuan_BieuThue] WHERE [MaBieuThue] = 'B99') = 0
begin
  INSERT INTO [dbo].[t_HaiQuan_BieuThue]  ([MaBieuThue]      ,[TenBieuThue]        ,[MoTaKhac])
  VALUES ('B99', N'Biểu thuế khác (B99 - Dành cho hàng miễn thuế)', 'KHAC')
END

UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '6.5'
      ,[Date] = getdate()
      ,[Notes] = ''
