﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using HtmlAgilityPack;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.IO;

namespace Company.KDT.SHARE.Components
{
    public static class Globals
    {
        static string _fontName = "Unicode";
        public static string FontName
        {
            get
            {
                return _fontName;
            }
            set
            {
                _fontName = value;
            }
        }
        public static decimal tinhDinhMucChung(decimal dm, decimal tylehaohut)
        {
            decimal dmc = dm + (dm * tylehaohut / 100);
            return dmc;
        }
        public static string Translate(this string trangthaixuly)
        {
            return Translate(int.Parse(trangthaixuly));
        }
        public static string Translate(int trangthaixuly)
        {
            switch (trangthaixuly)
            {
                case -1: return "Chưa khai báo";
                case 0: return "Chờ duyệt";
                case 1: return "Đã duyệt";
                case 2: return "Không phê duyệt";
                case -2: return "Chờ duyệt đã sửa chữa";
                case 3: return "Đã phân luồng";
                case -3: return "Đã khai báo, chưa có phản hồi";
                case -4: return "Hủy khai báo";
                case 5: return "Sửa tờ khai";
                case -5: return "Hủy tờ khai";
                case 10: return "Đã hủy";
                case 11: return "Chờ hủy";
                default:
                    return string.Empty;
            }
        }

        #region Trim Mã hải quan
        public static string trimMaHaiQuan(string maHQ)
        {
            return (maHQ.Trim());
        }
        #endregion

        #region Tỷ Giá - Exchange Rate

        public static decimal GetNguyenTe(string maNguyenTe)
        {
            try
            {
                return Company.KDT.SHARE.Components.NguyenTe.Load(maNguyenTe).TyGiaTinhThue;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return 0;
        }

        /// <summary>
        /// Lay ty gia ngoai te Online
        /// </summary>
        public static bool GetTyGia()
        {
            try
            {
                //Update by Hungtq
                string path = "http://www.sbv.gov.vn/wps/portal/!ut/p/c5/pVLLboMwEPwkFtsYc3RsMGCCgyGP5hIhtYoSNQ9VUdTm6-skVaXSBg71yqfZmZ0drbf0XO3b82bdnjaHffvqLbwlXYEkUZgEBBivMWRG6AiURiRG3txbAFnVW3Ycf5wWxUWcm-2lgrJp30s5d79C47KYPM_sdMT5di3xm9N8oqsEcYqSFIHJkYSMcMsRNbgm4Y-JJhKBm8iEAJtgFQV3tlA8JWEBoPJKQKbympGRDwDBgN8b-wFuZvgfbGKCwayW144-7zeFB4_DF_4gGz_o6JtURpCxXBeNP8EJwne8L_k-f4x2-KxhseOPwkqzsQ-TXzgBlwCJo8qoWW5D2u8vQ539VIJCt5-Nq5pLhKZBRx-AXP1TkFwnSFkysL_fi-cK-ufHA_551_8f19V3Ie5-yvSwe_GOu7PWuqA2_i72CXkZa0g!/dl3/d3/L0lDU0lKSmdwcGlRb0tVUWtnQSEhL29Pb2dBRUlRaGpFQ1VJZ0FBQUl5RkFNaHdVaFM0TFVFQVVvIS80QzFiOVdfTnIwZ0NVZ3hFbVJDVXdpSkg0QSEhLzdfRjJBNjJGSDIwME5SMjBJNDNOM1BTNzAwVTcvNFd0ME40NTcwMDIzMC8xNTM0OTU1NzkyMjUvYmZfYWN0aW9uL21haW4!";
                HtmlAgilityPack.HtmlDocument htmlAgilityPackDoc = new HtmlAgilityPack.HtmlWeb().Load(path);

                List<Company.KDT.SHARE.Components.NguyenTe> listNgoaiTe = Company.KDT.SHARE.Components.Globals.GetUSDExchangeRateNew(htmlAgilityPackDoc);

                if (listNgoaiTe.Count == 0)
                {
                    return false;
                }

                //Luu file XML
                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("Root");
                doc.AppendChild(root);
                foreach (Company.KDT.SHARE.Components.NguyenTe item in listNgoaiTe)
                {
                    XmlElement itemNT = doc.CreateElement("NgoaiTe");
                    XmlElement itemMaNT = doc.CreateElement("MaNgoaiTe");
                    itemMaNT.InnerText = item.Ten;

                    XmlElement itemTenNT = doc.CreateElement("TenNgoaiTe");
                    itemTenNT.InnerText = item.TenDayDu;

                    XmlElement itemTyNT = doc.CreateElement("TyGiaNgoaiTe");
                    itemTyNT.InnerText = item.TyGiaTinhThue.ToString();

                    itemNT.AppendChild(itemMaNT);
                    itemNT.AppendChild(itemTenNT);
                    itemNT.AppendChild(itemTyNT);
                    root.AppendChild(itemNT);
                }
                doc.Save(System.Windows.Forms.Application.StartupPath + "\\TyGia.xml");

                //Update Data
                Company.KDT.SHARE.Components.NguyenTe.InsertUpdateCollection(listNgoaiTe);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }

            return true;
        }

        public static List<NguyenTe> GetUSDExchangeRateNew(HtmlAgilityPack.HtmlDocument htmlDocument)
        {
            List<NguyenTe> collection = new List<NguyenTe>();

            try
            {
                //Lấy dữ liệu từ website: http://www.sbv.gov.vn/wps/portal/vn

                //string path = "http://www.sbv.gov.vn/wps/portal/!ut/p/c5/pVLLboMwEPwkFtsYc3RsMGCCgyGP5hIhtYoSNQ9VUdTm6-skVaXSBg71yqfZmZ0drbf0XO3b82bdnjaHffvqLbwlXYEkUZgEBBivMWRG6AiURiRG3txbAFnVW3Ycf5wWxUWcm-2lgrJp30s5d79C47KYPM_sdMT5di3xm9N8oqsEcYqSFIHJkYSMcMsRNbgm4Y-JJhKBm8iEAJtgFQV3tlA8JWEBoPJKQKbympGRDwDBgN8b-wFuZvgfbGKCwayW144-7zeFB4_DF_4gGz_o6JtURpCxXBeNP8EJwne8L_k-f4x2-KxhseOPwkqzsQ-TXzgBlwCJo8qoWW5D2u8vQ539VIJCt5-Nq5pLhKZBRx-AXP1TkFwnSFkysL_fi-cK-ufHA_551_8f19V3Ie5-yvSwe_GOu7PWuqA2_i72CXkZa0g!/dl3/d3/L0lDU0lKSmdwcGlRb0tVUWtnQSEhL29Pb2dBRUlRaGpFQ1VJZ0FBQUl5RkFNaHdVaFM0TFVFQVVvIS80QzFiOVdfTnIwZ0NVZ3hFbVJDVXdpSkg0QSEhLzdfRjJBNjJGSDIwME5SMjBJNDNOM1BTNzAwVTcvNFd0ME40NTcwMDIzMC8xNTM0OTU1NzkyMjUvYmZfYWN0aW9uL21haW4!";

                //HtmlDocument htmlDocument = new HtmlWeb().Load(path);

                System.Globalization.CultureInfo culVN = new System.Globalization.CultureInfo("vi-VN");

                #region Lay ty gia nguyen te USD
                HtmlNodeCollection nc = htmlDocument.DocumentNode.SelectNodes("//table[@class='sbvDataTable']");

                for (int i = 0; i < nc.Count; i++)
                {
                    //DateTime ngayApDung = Convert.ToDateTime(nc[i].SelectSingleNode("//span[@name='ngayapdung']").InnerText);
                    string giatriso = nc[i].SelectSingleNode("//span[@name='Giatriso']").InnerText;
                    decimal tyGiaUSD = Convert.ToDecimal(giatriso != "" ? giatriso : "0", culVN);

                    if (tyGiaUSD != 0)
                    {
                        NguyenTe usd = new NguyenTe();
                        usd.ID = "USD";
                        usd.Ten = "Đô la Mỹ";
                        usd.TyGiaTinhThue = tyGiaUSD;

                        collection.Add(usd);

                        break;
                    }
                }
                #endregion

                #region Lay ty gia xuat khau nguyen te khac
                //Get cung tai vi tri 2 trong danh sach
                HtmlNodeCollection ncKhac = htmlDocument.DocumentNode.SelectNodes("//table[@align='center']|//table[@width='590']")[1].SelectNodes("//tr[@name='DataContainer']");

                for (int j = 0; j < ncKhac.Count; j++)
                {
                    //DateTime apDungTuNgay = Convert.ToDateTime(ncKhac[j].SelectSingleNode("//span[@name='startDateD']").InnerText);
                    //DateTime apDungDenNgay = Convert.ToDateTime(ncKhac[j].SelectSingleNode("//span[@name='endDateD']").InnerText);
                    //string maNgoaiTe = ncKhac[j].SelectSingleNode("//span[@name='NGOAITE']").InnerText;
                    //string tenNgoaiTe = ncKhac[j].SelectSingleNode("//span[@name='TENGOI']").InnerText;
                    //decimal tyGiaNgoaiTe = Convert.ToDecimal(ncKhac[j].SelectSingleNode("//span[@name='GIATRI']").InnerText);
                    string[] array = ncKhac[j].InnerText.Trim().Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries);

                    if (array.Length != 0 && array.Length == 4)
                    {
                        NguyenTe nt = new NguyenTe();
                        nt.ID = array[1];
                        nt.Ten = array[2];
                        nt.TyGiaTinhThue = Convert.ToDecimal(array[3], culVN);

                        collection.Add(nt);
                    }
                }
                #endregion
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return collection;
        }

        #endregion

        #region SAVE NODE XML
        public static void SaveNodeXmlAppSettings(string key, object value, object setting)
        {
            SaveNodeXmlAppSettings(key, value);
            try
            {
                setting = value;
            }
            catch { }
        }
        //Hungtq 22/12/2010. Luu cau hinh
        public static void SaveNodeXmlAppSettings(string key, object value)
        {
            try
            {
                System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(config.FilePath);
                string groupSettingName = "appSettings";

                SaveNodeXml(config, doc, groupSettingName, key, value);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static string ReadNodeXmlAppSettings(string key)
        {
            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(config.FilePath);
            string groupSettingName = "appSettings";

            return ReadNodeXml(config, doc, groupSettingName, key);
        }

        //Hungtq 22/12/2010. Luu cau hinh
        public static void SaveNodeXml(System.Configuration.Configuration config, System.Xml.XmlDocument xmlDocument, string groupSettingName, string key, object value)
        {
            try
            {
                xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']").SelectSingleNode("@value").Value = value.ToString();
                xmlDocument.Save(config.FilePath);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static void UpdateNodeXml(System.Xml.XmlDocument xmlDocument, string groupSettingName, string key, object value)
        {
            try
            {
                xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']").SelectSingleNode("@value").Value = value.ToString();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static string ReadNodeXml(System.Configuration.Configuration config, System.Xml.XmlDocument xmlDocument, string groupSettingName, string key)
        {
            string result = "";

            try
            {
                result = xmlDocument.SelectSingleNode("//" + groupSettingName + "//add[@key='" + key + "']").SelectSingleNode("@value").Value;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }

            return result;
        }

        public static void SaveNodeXmlSetting(string name, object value)
        {
            try
            {
                System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(config.FilePath);
                string s = "//setting[@name='" + name + "']";
                doc.SelectSingleNode(s).SelectSingleNode("value").ChildNodes[0].Value = value.ToString();
                doc.Save(config.FilePath);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("Error at key: " + name, ex); }
        }

        public static void SaveNodeXmlConnectionStrings(string connectionString)
        {
            try
            {
                //System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                //System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                //doc.Load(config.FilePath);
                //doc.SelectSingleNode("//connectionStrings").ChildNodes[0].Attributes[1].Value = connectionString;
                //doc.Save(config.FilePath);

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString = connectionString;
                config.Save(ConfigurationSaveMode.Modified);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static void ReadNodeXmlConnectionStrings()
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string connectionString = config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString;

                string[] arr = connectionString.Split(new char[] { ';' });

                sConnectionString.Server = arr[0].Split(new char[] { '=' })[1];
                sConnectionString.Database = arr[1].Split(new char[] { '=' })[1];
                sConnectionString.User = arr[2].Split(new char[] { '=' })[1];
                sConnectionString.Pass = arr[3].Split(new char[] { '=' })[1];
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static string ReadNodeXmlConnectionStrings2()
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string connectionString = config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString;

                return connectionString;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return "";
        }

        public static XmlNode GetNodeConfigDN(XmlDocument doc, string key)
        {
            foreach (XmlNode node in doc.GetElementsByTagName("Config"))
            {
                if (node["Key"].InnerText.Trim().ToLower() == key.Trim().ToLower())
                {
                    return node["Value"];
                }
            }
            XmlDocument docx = new XmlDocument();
            return docx.CreateNode(XmlNodeType.Element, key, "");
        }
        public static string GetConfig(XmlDocument doc, string key, string defaultValue)
        {
            XmlNode node = GetNodeConfigDN(doc, key);
            if (node == null) return defaultValue;
            else if (string.IsNullOrEmpty(node.InnerText)) return defaultValue;
            else
                return node.InnerText;
        }
        static string GetConfig(string key)
        {
            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
            return config.AppSettings.Settings[key].Value.ToString();
        }
        static string _appSend = string.Empty;
        public static string AppSend
        {
            get
            {
                if (!string.IsNullOrEmpty(_appSend))
                    return _appSend;
                else
                {
                    try
                    {
                        _appSend = GetConfig("AssemblyName");
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        _appSend = "DEFAULT_SOFTECH";
                    }
                    return _appSend;
                }
            }
        }

        static string _versionSend = string.Empty;
        /// <summary>
        /// Mặc định khai báo V3 là 3.00 tưng ứng với các version còn lại.
        /// </summary>
        public static string VersionSend
        {
            get
            {
                if (_versionSend != string.Empty)
                    return _versionSend;
                else
                    try
                    {

                        string value = GetConfig("VersionSend");

                        if (!string.IsNullOrEmpty(value))
                        {
                            _versionSend = value;
                        }
                    }
                    catch
                    {
                        _versionSend = "2.00";
                    }

                return _versionSend;
            }
            set
            {
                _versionSend = value;
            }
        }
        static int _timeDelay = 0;
        public static int Delay
        {
            get
            {
                if (_timeDelay > 0)
                    return _timeDelay;
                else
                {
                    try
                    {
                        string value = GetConfig("TimeDelay");

                        if (!string.IsNullOrEmpty(value))
                        {
                            _timeDelay = int.Parse(value);
                        }
                    }
                    catch
                    {
                        _timeDelay = 5;
                    }
                }
                return _timeDelay;
            }
            set
            {
                _timeDelay = value;
            }
        }
        public struct sConnectionString
        {
            public static string Server;
            public static string Database;
            public static string User;
            public static string Pass;
        }

        public static System.Drawing.Printing.Margins GetMargin(string margins)
        {
            string[] arr = margins.Split(new char[] { ',' });

            return new System.Drawing.Printing.Margins(
                int.Parse(arr[0].Trim()),
                int.Parse(arr[1].Trim()),
                int.Parse(arr[2].Trim()),
                int.Parse(arr[3].Trim()));
        }
        #endregion

        /// <summary>
        /// Lay ten 1 file xml trong danh sach trong 1 folder.
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        public static string GetFileName(string folderPath)
        {
            string[] fileNames = null;
            try
            {

                if (System.IO.Directory.Exists(folderPath))
                {
                    fileNames = System.IO.Directory.GetFiles(folderPath, "*.xml");
                }

                if (fileNames.Length > 0)
                {
                    return fileNames[0]; //Lay file xml dau tien trong danh sach (Neu co nhieu hon 1).
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return "";
        }

        public static string GetPathProgram()
        {
            FileInfo fileInfo = new FileInfo(Application.ExecutablePath);
            return fileInfo.DirectoryName;
        }

        #region Send email

        public static void sendEmail(string subject, string body, string imagePath, string tenDN, string maDN, string maHQ, string soDT, string soFax, string nguoiLH, string exception)
        {
            try
            {
                string fromEmail = Globals.ReadNodeXmlAppSettings("FromMail");
                if (fromEmail == "" || fromEmail.Equals(""))
                    fromEmail = "ecs@softech.vn";
                string toEmail = Globals.ReadNodeXmlAppSettings("ToMail");
                if (toEmail == "" || toEmail.Equals(""))
                    toEmail = "ecs@softech.vn";

                string content = "<b>Tên doanh nghiệp:</b> " + tenDN + "<br>";
                content += "<b>Mã Doanh nghiệp: </b>" + maDN + "<br>";
                content += "<b>Mã Hải quan: </b>" + maHQ + "<br>";
                content += "<b>Số điện thoại: </b>" + soDT + "<br>";
                content += "<b>Số Fax: </b>" + soFax + "<br>";
                content += "<b>Người liên hệ: </b>" + nguoiLH + "<br>";
                content += "<b>Thông báo lỗi như sau:</b> <br><br>";
                content += body + "<br><br><\t>";
                content += "<b><i>" + exception + "</i></b>";

                MailMessage messageObj = new MailMessage(fromEmail, toEmail);
                messageObj.Subject = subject;
                messageObj.Body = content;
                messageObj.IsBodyHtml = true;
                messageObj.Priority = MailPriority.Normal;
                if (imagePath != "")
                {
                    Attachment attachment = new Attachment(imagePath);
                    messageObj.Attachments.Add(attachment);
                }
                SmtpClient smtpObj = new SmtpClient();
                smtpObj.Send(messageObj);

                System.Threading.Thread.Sleep(2000);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        public static bool SaveAsImage(string filename)
        {
            try
            {
                Bitmap b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, PixelFormat.Format32bppArgb);
                Size s = new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);

                Graphics g = Graphics.FromImage(b);
                g.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y, 0, 0, s, CopyPixelOperation.SourceCopy);
                string[] files = System.IO.Directory.GetFiles("Errors");
                foreach (string file in files)
                {
                    if (System.IO.File.Exists(file))
                    {
                        System.IO.File.Delete(file);
                    }
                }

                b.Save(filename, ImageFormat.Jpeg);
                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
            //b.Save(@"c:\ERROR.png", ImageFormat.Png);  
        }

        /// <summary>
        /// Kiểm tra chuỗi nhập vào có phải là kiểu số.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool isNumber(string value)
        {
            bool isValid = true;

            char[] arr = value.ToCharArray();

            for (int i = 0; i < arr.Length; i++)
            {
                isValid &= Char.IsNumber(arr[i]);
            }

            return isValid;
        }

        public static bool isEmail(string inputEmail)
        {
            //inputEmail = NulltoString(inputEmail);
            //string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
            //      @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
            //      @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            //Regex re = new Regex(strRegex);
            //if (re.IsMatch(inputEmail))
            //    return (true);
            //else
            return (false);
        }
        #endregion

        #region Save Message
        public static bool XmlSaveMessage(this string xml, long idItem, string tieudethongbao, string noidungthongbao)
        {
            return SaveMessage(xml, idItem, tieudethongbao, noidungthongbao);
        }
        public static bool XmlSaveMessage(this string xml, long idItem, string tieudethongbao)
        {
            return SaveMessage(xml, idItem, tieudethongbao, string.Empty);
        }
        public static bool SaveMessage(string xml, long itemId, string tieudethongbao, string noidungthongbao)
        {
            try
            {
                if (string.IsNullOrEmpty(xml)) return false;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);
                Message message = new Message();
                message.ItemID = itemId;
                message.MessageType = (MessageTypes)int.Parse(doc.SelectSingleNode("Envelope/Header/Subject/type").InnerText);
                message.MessageFunction = (MessageFunctions)int.Parse(doc.SelectSingleNode("Envelope/Header/Subject/function").InnerText);
                message.ReferenceID = new Guid(doc.SelectSingleNode("Envelope/Header/Subject/reference").InnerText);
                message.MessageFrom = doc.SelectSingleNode("Envelope/Header/From/identity").InnerText;
                message.MessageTo = doc.SelectSingleNode("Envelope/Header/To/identity").InnerText;
                message.MessageContent = xml;

                message.TieuDeThongBao = tieudethongbao;
                message.NoiDungThongBao = noidungthongbao;

                message.CreatedTime = DateTime.Now;
                message.Insert();

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }
        public static bool SaveMessage(string xml, long itemId, string tieudethongbao)
        {
            return SaveMessage(xml, itemId, tieudethongbao, string.Empty);
        }
        public static bool SaveMessage(string content, long itemId, string guidstr, MessageTypes messagetype, MessageFunctions function, string tieudethongbao, string noidungthongbao)
        {
            try
            {
                Message message = new Message();
                message.ItemID = itemId;
                message.MessageType = messagetype;
                message.MessageFunction = function;
                message.TieuDeThongBao = tieudethongbao;
                message.NoiDungThongBao = noidungthongbao;
                message.MessageContent = content;
                message.ReferenceID = new Guid(guidstr);
                message.CreatedTime = DateTime.Now;
                message.Insert();

                return true;
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }

        public static bool SaveMessage(string content, long itemId, string guidstr, string messagefrom, string messageto, MessageTypes messagetype, MessageFunctions function, string tieudethongbao, string noidungthongbao)
        {
            try
            {
                Message message = new Message();
                message.ItemID = itemId;
                message.MessageFrom = messagefrom;
                message.MessageTo = messageto;
                message.MessageType = messagetype;
                message.MessageFunction = function;
                message.TieuDeThongBao = tieudethongbao;
                message.NoiDungThongBao = noidungthongbao;
                message.MessageContent = content;
                message.ReferenceID = new Guid(guidstr);
                message.CreatedTime = DateTime.Now;
                message.Insert();

                return true;
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }

        public static void Message2File(string messageError)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(messageError);
                XmlNode node = doc.SelectSingleNode("Envelope/Header/Subject/reference");
                string reference = string.Empty;
                if (node != null) reference = node.InnerText;
                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Errors"))
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Errors");
                StreamWriter sw = File.CreateText(AppDomain.CurrentDomain.BaseDirectory + "Errors\\" + DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss_") + reference + ".xml");
                sw.Write(messageError);
                sw.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(messageError, ex);
            }
        }
        public static bool SaveMessage(string content, long itemId, string guidstr, MessageTypes messagetype, MessageFunctions function, string tieudethongbao)
        {
            return SaveMessage(content, itemId, guidstr, messagetype, function, tieudethongbao, string.Empty);
        }
        #endregion

        /// <summary>
        /// Lấy tên phân luồng
        /// </summary>
        /// <param name="phanLuong"></param>
        /// <returns></returns>
        public static string GetPhanLuong(string phanLuong)
        {
            if (phanLuong.Trim() == "1")
                return "Luồng xanh";
            else if (phanLuong.Trim() == "2")
                return "Luồng vàng";
            if (phanLuong.Trim() == "3")
                return "Luồng đỏ";
            else
                return "";
        }

        /// <summary>
        /// Lây tên trạng thái xử lý
        /// </summary>
        /// <param name="trangThai"></param>
        /// <returns></returns>
        public static string GetTrangThaiXuLyTK(int trangThai)
        {
            if (trangThai == TrangThaiXuLy.DA_DUYET)
                return "Đã duyệt";
            else if (trangThai == TrangThaiXuLy.CHO_DUYET)
                return "Chờ duyệt";
            if (trangThai == TrangThaiXuLy.CHUA_KHAI_BAO)
                return "Chưa khai báo";
            if (trangThai == TrangThaiXuLy.SUATKDADUYET)
                return "Sửa tờ khai";
            if (trangThai == TrangThaiXuLy.HUYTKDADUYET)
                return "Hủy tờ khai";
            if (trangThai == TrangThaiXuLy.CHO_HUY)
                return "Chờ hủy";
            if (trangThai == TrangThaiXuLy.DA_HUY)
                return "Đã hủy";
            if (trangThai == TrangThaiXuLy.KHONG_PHE_DUYET)
                return "Không phê duyệt";
            else
                return "";
        }
        //Update by KhanhHN
        public static string FormatNumber(int number)
        {
            return "{0:###,###,###" + Globals.GetDot(number) + "}";
        }
        /// <summary>
        /// Định dạng số thập phân
        /// chuẩn microsoft "###,###,###.########"
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static String GetPrecision(int num)
        {
            string val = "###,###,###" + GetDot(num);
            return val;
        }
        public static string GetDot(int decimalFormat)
        {
            string ret = string.Empty;
            if (decimalFormat < 0) return ret;
            else if (decimalFormat > 0)
                ret += "0.";
            for (int i = 0; i < decimalFormat; i++)
            {
                ret += "#";
            }
            return ret;
        }
        /// <summary>
        /// Thoi gian cho gui & nhan (second). Mac dinh: 5s -> 5000 milisecond.
        /// </summary>
        /// <returns>MiliSeconds</returns>
        public static int TimeDelay()
        {
            int TimeDelay = 5;

            try
            {
                string delay = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TimeDelay");

                TimeDelay = delay != "" ? int.Parse(delay) : 5;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("TimeDelay", ex); }

            return TimeDelay * 1000; //1s= 1000ms.
        }

        //public static string ConvertToString(object source)
        //{
        //    string ConvertToString;
        //    DateTime dateTime;

        //    if (source == null)
        //        return String.Empty;
        //    if (String.IsNullOrEmpty(Conversions.ToString(source)))
        //        return String.Empty;
        //    if ((source as DateTime))
        //    {
        //        if (DateTime.Compare(Conversions.ToDate(source), DateTime.MinValue) == 0)
        //            return String.Empty;
        //        dateTime = Conversions.ToDate(source);
        //        return dateTime.ToString("dd/MM/yyyy\uFFFD");
        //    }
        //    if ((source as DateTime))
        //    {
        //        if (DateTime.Compare(Conversions.ToDate(source), DateTime.MinValue) == 0)
        //            return String.Empty;
        //        dateTime = Conversions.ToDate(source);
        //        return dateTime.ToString("dd/MM/yyyy HH:mm:ss\uFFFD");
        //    }
        //    return source.ToString();
        //}

        //public static string FormatNumeric(object obj, int percent)
        //{
        //    string FormatNumeric;
        //    decimal dec;

        //    string ret = Conversions.ToString(0);
        //    string sFormat = "#,###,##0.0";

        //    for (int i = 1; i <= percent; i++)
        //    {
        //        sFormat += "#";
        //    }
        //    if (obj is int)
        //    {
        //        int i2 = Int32.Parse(Conversions.ToString(obj));
        //        ret = i2.ToString();
        //    }
        //    else if (obj is double)
        //    {
        //        double d = Double.Parse(Conversions.ToString(obj));
        //        ret = d.ToString(sFormat);
        //    }
        //    else if (obj is decimal)
        //    {
        //        dec = Decimal.Parse(Conversions.ToString(obj));
        //        ret = dec.ToString(sFormat);
        //    }
        //    else if (obj is string)
        //    {
        //        if (String.IsNullOrEmpty(Conversions.ToString(obj)))
        //        {
        //            ret = Conversions.ToString(0);
        //        }
        //        else
        //        {
        //            dec = Decimal.Parse(Conversions.ToString(obj));
        //            ret = dec.ToString(sFormat);
        //        }
        //    }
        //    return ret;
        //}
    }
}
