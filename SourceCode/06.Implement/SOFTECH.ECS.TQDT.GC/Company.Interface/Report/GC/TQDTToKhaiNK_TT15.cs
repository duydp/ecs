﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
#if KD_V2
using Company.KD.BLL.KDT;
using Company.KD.BLL.DuLieuChuan;
using Company.KD.BLL.Utils;
#elif GC_V2
using Company.GC.BLL.KDT;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.Utils;
#elif SXXK_V2
using Company.BLL.KDT;
using Company.BLL.DuLieuChuan;
using Company.BLL.Utils;
#endif

using Company.KDT.SHARE.Components;

namespace Company.Interface.Report
{
    public partial class TQDTToKhaiNK_TT15 : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public Company.Interface.Report.ReportViewTKNTQDTFormTT15 report;
        public bool BanLuuHaiQuan = false;
        public bool MienThueNK = false;
        public bool MienThueGTGT = false;
        public bool InMaHang = false;

        public TQDTToKhaiNK_TT15()
        {
            InitializeComponent();
        }

        public void BindReport()
        {
            try
            {
                ResetEmpty();

                this.PrintingSystem.ShowMarginsWarning = false;
#if KD_V2
                double tongTriGiaNT = 0;
                double tongTienThueXNK = 0;
                double tongTienThueTatCa = 0;
                double tongThueGTGT = 0;
                double tongTriGiaThuKhac = 0;
                double TriGiaTTTTDB;
                double TriGiaTTGTGT;
#elif GC_V2
                decimal tongTriGiaNT = 0;
                decimal tongTienThueXNK = 0;
                decimal tongTienThueTatCa = 0;
                decimal tongThueGTGT = 0;
                decimal tongTriGiaThuKhac = 0;
                decimal TriGiaTTTTDB;
                decimal TriGiaTTGTGT;
#elif SXXK_V2
                decimal tongTriGiaNT = 0;
                decimal tongTienThueXNK = 0;
                decimal tongTienThueTatCa = 0;
                decimal tongThueGTGT = 0;
                decimal tongTriGiaThuKhac = 0;
                decimal TriGiaTTTTDB;
                decimal TriGiaTTGTGT;
#endif

                //Set ngay mac dinh
                DateTime minDate = new DateTime(1900, 1, 1);

                #region Thong tin chinh

                //Cuc HQ
                string maCuc = TKMD.MaHaiQuan.Substring(1, 2);
                lblCucHaiQuan.Text = DonViHaiQuan.GetName("Z" + maCuc + "Z");

                //Chi cuc Hai quan dang ky
                lblChiCucHQDangKy.Text = DonViHaiQuan.GetName(TKMD.MaHaiQuan);
                lblChiCucHQDangKy.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //Chi cuc Hai quan cua khau
                lblChiCucHQCuaKhau.Text = this.TKMD.CuaKhau_ID; //this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                lblChiCucHQCuaKhau.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //So tham chieu
                if (this.TKMD.SoTiepNhan > 0)
                    this.lblSoThamChieu.Text = this.TKMD.SoTiepNhan.ToString();
                else
                    this.lblSoThamChieu.Text = "";

                //Ngay gio so tham chieu
                if (this.TKMD.NgayTiepNhan > minDate)
                    lblNgaySoThamChieu.Text = this.TKMD.NgayTiepNhan.ToString("dd/MM/yyyy HH:mm");
                else
                    lblNgaySoThamChieu.Text = "";

                //Số tờ khai
                if (this.TKMD.SoToKhai > 0)
                    lblSoToKhai.Text = this.TKMD.SoToKhai + "";

                //Ngày giờ đăng ký
                if (this.TKMD.NgayDangKy > minDate)
                    lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy HH:mm");
                else
                    lblNgayDangKy.Text = "";

                //So luong phu luc to khai
                if (this.TKMD.SoLuongPLTK > 0)
                    lblSoPhuLucToKhai.Text = this.TKMD.SoLuongPLTK.ToString("00");
                else
                {
                    lblSoPhuLucToKhai.Text = this.TKMD.HMDCollection.Count > 0 ? (this.TKMD.HMDCollection.Count / 3).ToString("00") : "0";
                }

                //01.Nguoi Xuat khau
                lblNguoiXK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblNguoiXK.Text = TKMD.TenDonViDoiTac.ToString();

                //02.Nguoi nhap khau
                lblNguoiNK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                if (Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("WordWrap")))
                    lblNguoiNK.Text = this.TKMD.TenDoanhNghiep + "\r\n" + GlobalSettings.DIA_CHI;
                else
                    lblNguoiNK.Text = this.TKMD.TenDoanhNghiep + ". " + GlobalSettings.DIA_CHI;
                //MST Nguoi Nhap khau
                lblMSTNguoiNhapKhau.Text = this.ToStringForReport(this.TKMD.MaDoanhNghiep);

                //03.Dơn vi uy thac
                lblTenNguoiUyThac.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblTenNguoiUyThac.Text = TKMD.TenDonViDoiTac;
                lblMSTNguoiUT.Text = this.TKMD.MaDonViUT;

                //04.Dai ly Hai quan
                lblTenDaiLy.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblTenDaiLy.Text = this.TKMD.TenDaiLyTTHQ;
                lblMSTHaiQuan.Text = this.TKMD.MaDaiLyTTHQ;

                //05. Ma Loai Hinh
                string stlh = "";
                stlh = LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                lblLoaiHinh.Text = TKMD.MaLoaiHinh + " - " + stlh;
                lblLoaiHinh.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //06. Hoa Don thuong mai
                lblHoaDonThuongMai.Text = "" + this.TKMD.SoHoaDonThuongMai;
                lblHoaDonThuongMai.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                if (this.TKMD.NgayHoaDonThuongMai > minDate)
                    lblNgayHoaDonThuongMai.Text = this.TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
                else
                    lblNgayHoaDonThuongMai.Text = "";
                lblNgayHoaDonThuongMai.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //07. GiayPhep
                if (TKMD.SoGiayPhep != "")
                    lblGiayPhep.Text = "" + this.TKMD.SoGiayPhep;
                else
                    lblGiayPhep.Text = "";
                lblGiayPhep.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                if (this.TKMD.NgayGiayPhep > minDate)
                    lblNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
                else
                    lblNgayGiayPhep.Text = "";
                lblNgayGiayPhep.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                if (this.TKMD.NgayHetHanGiayPhep > minDate)
                    lblNgayHetHanGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
                else
                    lblNgayHetHanGiayPhep.Text = "";
                lblNgayHetHanGiayPhep.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //08. Hop dong
                lblHopDong.Text = "" + this.TKMD.SoHopDong;
                lblHopDong.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                if (this.TKMD.NgayHopDong > minDate)
                    lblNgayHopDong.Text = this.TKMD.NgayHopDong.ToString("dd/MM/yyyy");
                else
                    lblNgayHopDong.Text = " ";
                lblNgayHopDong.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                if (this.TKMD.NgayHetHanHopDong > minDate)
                    lblNgayHetHanHopDong.Text = "" + this.TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
                else
                    lblNgayHetHanHopDong.Text = " ";
                lblNgayHetHanHopDong.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //09. VanTaiDon
                lblVanTaiDon.Text = this.TKMD.SoVanDon;
                lblVanTaiDon.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                if (this.TKMD.NgayVanDon > minDate)
                    lblNgayVanTaiDon.Text = this.TKMD.NgayVanDon.ToString("dd/MM/yyyy");
                else
                    lblNgayVanTaiDon.Text = "";
                lblNgayVanTaiDon.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //10. CangXepHang
                lblCangXepHang.Text = this.TKMD.DiaDiemXepHang;
                lblCangXepHang.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //11. CangDoHang
                lblMaCangdoHang.Text = this.TKMD.CuaKhau_ID;
                lblMaCangdoHang.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblTenCangDoHang.Text = CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                lblTenCangDoHang.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                //12. MaPTVT
                lblPhuongTienVanTai.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblPhuongTienVanTai.Text = PhuongThucVanTai.getName(TKMD.PTVT_ID);
                //Ten, So hieu PTVT
                lblTenSoHieuPTVT.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                if (TKMD.VanTaiDon != null)
                    lblTenSoHieuPTVT.Text = TKMD.VanTaiDon.TenPTVT + ", " + this.TKMD.VanTaiDon.SoHieuPTVT;
                else
                    lblTenSoHieuPTVT.Text = " " + this.TKMD.SoHieuPTVT;
                //Ngay den PTVT
                if (this.TKMD.NgayDenPTVT > minDate)
                    lblNgayDenPTVT.Text = this.TKMD.NgayDenPTVT.ToString("dd/MM/yyyy");
                else
                    lblNgayDenPTVT.Text = "";

                //13. NuocXK
                lblNuocXK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblNuocXK.Text = this.TKMD.NuocXK_ID;
                lblTenNuoc.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblTenNuoc.Text = Nuoc.GetName(this.TKMD.NuocXK_ID);

                //14. DieuKienGiaoHang
                lblDieuKienGiaoHang.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblDieuKienGiaoHang.Text = this.TKMD.DKGH_ID;

                //15.PhuongThucThanhToan
                lblPhuongThucThanhToan.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblPhuongThucThanhToan.Text = this.TKMD.PTTT_ID;

                //16. DongTienThanhToan
                lblDongTienThanhToan.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblDongTienThanhToan.Text = this.TKMD.NguyenTe_ID;

                //17. TyGiaTinhThue
                lblTyGiaTinhThue.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblTyGiaTinhThue.Text = this.TKMD.TyGiaTinhThue.ToString("G10");

                #endregion

                //18. Thong tin hang hoa va thue
                if (TKMD.HMDCollection.Count > 1)
                {
                    lblMoTaHang1.Text = "(theo phụ lục tờ khai)";
                }
                else
                {
                    #region 1 hàng

                    HangMauDich hmd = this.TKMD.HMDCollection[0];

                    //18. Mo ta hang hoa - Ten hang
                    if (!InMaHang)
                        lblMoTaHang1.Text = hmd.TenHang;
                    else
                    {
                        if (hmd.MaPhu.Trim().Length > 0)
                            lblMoTaHang1.Text = hmd.TenHang + "/" + hmd.MaPhu;
                        else
                            lblMoTaHang1.Text = hmd.TenHang;
                    }

                    lblMoTaHang1.WordWrap = true;
                    lblMoTaHang1.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));

                    //19. Ma so hang hoa - Ma HS
                    lblMaHSHang1.Text = hmd.MaHS;
                    //20. Xuat xu
                    lblXuatXuHang1.Text = hmd.NuocXX_ID;

                    //21. Che do uu dai

                    //22. luong hang
                    lblLuongHang1.Text = hmd.SoLuong.ToString("G15");
                    //23.Don vi tinh
                    lblDVTHang1.Text = DonViTinh.GetName(hmd.DVT_ID);
                    #region Command by KhanhHN - không lưu FOC
                    //Hang thue F.O.C
                    //if (hmd.FOC)
                    // {


                    ////24. Don gia nguyen te
                    //lblDonGiaHang1.Text = "F.O.C";
                    ////25. Tri gia nguyen te
                    //lblTGNTHang1.Text = "F.O.C";

                    ////26. Thue Nhap khau - NK
                    ////Tri gia tinh thue
                    //lblTGTTNKHang1.Text = "";
                    ////Thue suat
                    //lblThueSuatNKHang1.Text = "";
                    ////Tien thue
                    //lblTienThueNKHang1.Text = "";

                    ////27. Thue thieu thu dac biet - TTDB
                    //lblTGTTTTDBHang1.Text = "";
                    //lblThueSuatTTDBHang1.Text = "";
                    //lblTienThueTTDBHang1.Text = "";

                    ////28. Thue Bao ve moi truong - BVMT
                    //lblTGTTBVMTHang1.Text = "";
                    //lblThueSuatBVMTHang1.Text = "";
                    //lblTienThueBVMTHang1.Text = "";

                    ////29. Thue gia tri gia tang - VAT
                    //lblTGTTVATHang1.Text = "";
                    //lblThueSuatVATHang1.Text = "";
                    //lblTienThueVATHang1.Text = "";


                    //}
                    #endregion

                    //Hang tinh thue 
                    //else
                    {
                        //24. Don gia nguyen te
                        lblDonGiaHang1.Text = hmd.DonGiaKB.ToString("G10");
                        //25. Tri gia nguyen te
                        lblTGNTHang1.Text = hmd.TriGiaKB.ToString("N2");

                        //26. Thue Nhap khau - NK
                        //Tri gia tinh thue
                        lblTGTTNKHang1.Text = hmd.TriGiaTT.ToString("N0");
                        //Thue suat
                        //if (hmd.ThueTuyetDoi)
                        //{
                        //    lblThueSuatNKHang1.Text = "??";
                        //}
                        //else
                        //{
                    #if (KD_V2 ||GC_V2)
                        if (hmd.ThueSuatXNKGiam.Trim() == "")
                            lblThueSuatNKHang1.Text = hmd.ThueSuatXNK.ToString("N0");
                        else
                            lblThueSuatNKHang1.Text = hmd.ThueSuatXNKGiam;
                    #elif SXXK_V2
                        lblThueSuatNKHang1.Text = hmd.ThueSuatXNK.ToString("N0");
                    #endif
                       
                        //}
                        //Tien thue
                        lblTienThueNKHang1.Text = hmd.ThueXNK.ToString("N0");

                        //27. Thue thieu thu dac biet - TTDB
                        TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                        lblTGTTTTDBHang1.Text = TriGiaTTTTDB.ToString("N0");
                    #if (KD_V2 ||GC_V2)
                        if (hmd.ThueSuatTTDBGiam.Trim() == "")
                            lblThueSuatTTDBHang1.Text = hmd.ThueSuatTTDB.ToString("N0");
                        else
                            lblThueSuatTTDBHang1.Text = hmd.ThueSuatTTDBGiam;
                    #elif SXXK_V2
                        lblThueSuatTTDBHang1.Text = hmd.ThueSuatTTDB.ToString("N0");
                    #endif
                        
                        lblTienThueTTDBHang1.Text = hmd.ThueTTDB.ToString("N0");

                        //28. Thue Bao ve moi truong - BVMT
                        if (hmd.TyLeThuKhac != 0)
                            lblThueSuatBVMTHang1.Text = hmd.TyLeThuKhac.ToString("N0");
                        else
                            lblThueSuatBVMTHang1.Text = ""; ;
                        lblTienThueBVMTHang1.Text = hmd.TriGiaThuKhac.ToString("N0");

                        //29. Thue gia tri gia tang - VAT
                        TriGiaTTGTGT = TriGiaTTTTDB + hmd.ThueTTDB + hmd.TriGiaThuKhac;
                        lblTGTTVATHang1.Text = TriGiaTTGTGT.ToString("N0");
                    #if (KD_V2 ||GC_V2)
                        if (hmd.ThueSuatVATGiam.Trim() == "")
                            lblThueSuatVATHang1.Text = hmd.ThueSuatGTGT.ToString("N0");
                        else
                            lblThueSuatVATHang1.Text = hmd.ThueSuatVATGiam;
                    #elif SXXK_V2
                        lblThueSuatVATHang1.Text = hmd.ThueSuatGTGT.ToString("N0");
                    #endif
                        
                        lblTienThueVATHang1.Text = hmd.ThueGTGT.ToString("N0");

                        tongTienThueXNK += hmd.ThueXNK;
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;

                    }
                    //if (MienThue1)
                    //{
                    //    lblTGTTNKHang1.Text = "";
                    //    lblThueSuatNKHang1.Text = "";
                    //    lblTienThueNKHang1.Text = "";
                    //}
                    //if (MienThue2)
                    //{

                    //}

                    //if (hmd.MienThue == 1)
                    //{
                    //    lblTGTTNKHang1.Text = "";
                    //    lblThueSuatNKHang1.Text = "";
                    //    lblTienThueNKHang1.Text = "";
                    //    lblTGTTVATHang1.Text = "";
                    //    lblThueSuatVATHang1.Text = "";
                    //    lblTienThueVATHang1.Text = "";
                    //    TyLeThuKhac1.Text = "";
                    //    TriGiaThuKhac1.Text = "";
                    //}
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                    tongTienThueTatCa = tongTienThueTatCa + hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac;

                    lblTongTienThue.Text = tongTienThueTatCa.ToString("N0");

                    string s = VNCurrency.ToString(tongTienThueTatCa).Trim();
                    s = s[0].ToString().ToUpper() + s.Substring(1);
                    lblBangChu.Text = s.Replace("  ", " ");

                    #endregion
                }

                //31. Thong tin Container
                if (TKMD.VanTaiDon != null)
                {
                    if (TKMD.VanTaiDon.ContainerCollection.Count == 0)
                        TKMD.VanTaiDon.LoadContainerCollection();

                    if (TKMD.VanTaiDon.ContainerCollection.Count < 4)
                    {
                        for (int i = 0; i < TKMD.VanTaiDon.ContainerCollection.Count; i++)
                        {
                            //So hieu Contianer
                            this.xrTable4.Rows[i].Controls["lblSoHieuCont" + (i + 1)].Text = string.Format("{0}/{1}", TKMD.VanTaiDon.ContainerCollection[i].SoHieu, TKMD.VanTaiDon.ContainerCollection[i].Seal_No);
                            //So luong kien cua container
                            //int countSoLuongKien = TKMD.VanTaiDon.ListHangOfVanDon.Where(x => x.SoHieuContainer == TKMD.VanTaiDon.ContainerCollection[i].SoHieu).GroupBy(x => x.SoHieuKien).Count();
                            //this.xrTable4.Rows[i].Controls["lblSoLuongKienCont" + (i + 1)].Text = string.Format("{0:00}", countSoLuongKien);
                            ////Trong luong kien cua container
                            //double countTrongLuong = TKMD.VanTaiDon.ListHangOfVanDon.Where(x => x.SoHieuContainer == TKMD.VanTaiDon.ContainerCollection[i].SoHieu).Sum(x => x.TrongLuong);
                            //this.xrTable4.Rows[i].Controls["lblTrongLuongCont" + (i + 1)].Text = string.Format("{0:N3}", countTrongLuong);
                        }
                    }
                    else
                        lblSoHieuCont1.Text = "(Chi tiết phụ lục đính kèm)";

                    //Tong trong luong container
                    //double sumTrongLuong = TKMD.VanTaiDon.ListHangOfVanDon.Sum(x => x.TrongLuong);
                    //lblTongContainer.Text = string.Format("{0:N3}", sumTrongLuong);
                }

                //32. Chung tu di kem
                string ctk = "";
                ctk += TKMD.VanTaiDon != null ? "Vận đơn" : "";
                ctk += (TKMD.GiayPhepCollection.Count != 0 ? "; " + string.Format("Giấy phép ({0})" + TKMD.GiayPhepCollection.Count) : "");
                ctk += (TKMD.HoaDonThuongMaiCollection.Count != 0 ? "; " + string.Format("Hóa đơn ({0})", TKMD.HoaDonThuongMaiCollection.Count) : "");
                ctk += (TKMD.HopDongThuongMaiCollection.Count != 0 ? "; " + string.Format("Hợp đồng ({0})", TKMD.HopDongThuongMaiCollection.Count) : "");
                ctk += (TKMD.COCollection.Count != 0 ? "; " + string.Format("CO ({0})", TKMD.COCollection.Count) : "");
                ctk += (TKMD.listChuyenCuaKhau.Count != 0 ? "; " + string.Format("Đề nghị chuyển cửa khẩu ({0})", TKMD.listChuyenCuaKhau.Count) : "");
                ctk += (TKMD.listCTDK.Count != 0 ? "; " + string.Format("Chứng từ kèm dạng ảnh ({0})", TKMD.listCTDK.Count) : "");
                //ctk += (TKMD.ChungTuNoCollection.Count != 0 ? "; " + string.Format("Chứng từ nợ ({0})", TKMD.ChungTuNoCollection.Count) : "");
                ctk = ctk.Substring(0, 1).Equals(";") ? ctk.Remove(0, 1) : ctk;
                ctk = ctk.Substring(ctk.Length - 1, 1).Equals(";") ? ctk.Remove(ctk.Length - 1, 1) : ctk;
                //lblChungTuDiKem.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblChungTuDiKem.Text = ctk;

                //33. Ky cam doan
                //Ngay thang nam in to khai
                if (TKMD.NgayDangKy == new DateTime(1900, 1, 1))
                    lblNgayIn.Text = "Ngày " + DateTime.Today.Day.ToString("00") + " tháng " + DateTime.Today.Month.ToString("00") + " năm " + DateTime.Today.Year;
                else
                    lblNgayIn.Text = "Ngày " + TKMD.NgayDangKy.Day.ToString("00") + " tháng " + TKMD.NgayDangKy.Month.ToString("00") + " năm " + TKMD.NgayDangKy.Year;

                //34. Ket qua va huong dan phan luong
                lblKetQuaPhanLuong.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
                lblKetQuaPhanLuong.Text = Company.KDT.SHARE.Components.Globals.GetPhanLuong(TKMD.PhanLuong);
                if (TKMD.PhanLuong != "")
                {
                    lblKetQuaPhanLuong.Text += TKMD.HUONGDAN != "" ? " - " + TKMD.HUONGDAN : "";
                }


                /*
                #region <= 3 hàng
                if (this.TKMD.HMDCollection.Count <= 3)
                {
                    
                    #region 2 hàng
                    if (this.TKMD.HMDCollection.Count >= 2)
                    {
                        HangMauDich hmd = this.TKMD.HMDCollection[1];
                        if (!InMaHang)
                            TenHang2.Text = hmd.TenHang;
                        else
                        {
                            if (hmd.MaPhu.Trim().Length > 0)
                                TenHang2.Text = hmd.TenHang + "/" + hmd.MaPhu;
                            else
                                TenHang2.Text = hmd.TenHang;
                        }
                        TenHang2.WordWrap = true;
                        TenHang2.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));
                        MaHS2.Text = hmd.MaHS;
                        XuatXu2.Text = hmd.NuocXX_ID;
                        Luong2.Text = hmd.SoLuong.ToString("G15");
                        DVT2.Text = DonViTinh.GetName(hmd.DVT_ID);
                        if (hmd.FOC)
                        {
                            DonGiaNT2.Text = "F.O.C";
                            TriGiaNT2.Text = "F.O.C";
                            TriGiaTT2.Text = "";
                            ThueSuatXNK2.Text = "";
                            TienThueXNK2.Text = "";
                            TriGiaTTGTGT2.Text = "";
                            ThueSuatGTGT2.Text = "";
                            TienThueGTGT2.Text = "";
                            TyLeThuKhac2.Text = "";
                            TriGiaThuKhac2.Text = "";
                        }
                        else
                        {
                            DonGiaNT2.Text = hmd.DonGiaKB.ToString("G10");
                            TriGiaNT2.Text = hmd.TriGiaKB.ToString("N2");
                            TriGiaTT2.Text = hmd.TriGiaTT.ToString("N0");
                            if (hmd.ThueTuyetDoi)
                            {
                                ThueSuatXNK2.Text = "";
                            }
                            else
                            {
                                if (hmd.ThueSuatXNKGiam.Trim() == "")
                                    ThueSuatXNK2.Text = hmd.ThueSuatXNK.ToString("N0");
                                else
                                    ThueSuatXNK2.Text = hmd.ThueSuatXNKGiam;
                            }
                            TienThueXNK2.Text = hmd.ThueXNK.ToString("N0");
                            if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                            {
                                double TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                                TriGiaTTGTGT2.Text = TriGiaTTGTGT.ToString("N0");
                                if (hmd.ThueSuatVATGiam.Trim() == "")
                                    ThueSuatGTGT2.Text = hmd.ThueSuatGTGT.ToString("N0");
                                else
                                    ThueSuatGTGT2.Text = hmd.ThueSuatVATGiam;
                                TienThueGTGT2.Text = hmd.ThueGTGT.ToString("N0");
                                TyLeThuKhac2.Text = hmd.TyLeThuKhac.ToString("N0");
                                TriGiaThuKhac2.Text = hmd.PhuThu.ToString("N0");
                                tongTienThueXNK += hmd.ThueXNK;
                                tongThueGTGT += hmd.ThueGTGT;
                                tongTriGiaThuKhac += hmd.PhuThu;
                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                            {
                                double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                                TriGiaTTGTGT2.Text = TriGiaTTTTDB.ToString("N0");
                                if (hmd.ThueSuatTTDBGiam.Trim() == "")
                                    ThueSuatGTGT2.Text = hmd.ThueSuatTTDB.ToString("N0");
                                else
                                    ThueSuatGTGT2.Text = hmd.ThueSuatTTDBGiam;
                                TienThueGTGT2.Text = hmd.ThueTTDB.ToString("N0");
                                TyLeThuKhac2.Text = hmd.TyLeThuKhac.ToString("N0");
                                TriGiaThuKhac2.Text = hmd.PhuThu.ToString("N0");
                                tongTienThueXNK += hmd.ThueXNK;
                                tongThueGTGT += hmd.ThueTTDB;
                                tongTriGiaThuKhac += hmd.PhuThu;

                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                            {
                                double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                                TriGiaTTGTGT2.Text = TriGiaTTTTDB.ToString("N0");
                                if (hmd.ThueSuatTTDBGiam.Trim() == "")
                                    ThueSuatGTGT2.Text = hmd.ThueSuatTTDB.ToString("N0");
                                else
                                    ThueSuatGTGT2.Text = hmd.ThueSuatTTDBGiam;
                                TienThueGTGT2.Text = hmd.ThueTTDB.ToString("N0");
                                if (hmd.ThueSuatVATGiam.Length == 0)
                                    TyLeThuKhac2.Text = hmd.ThueSuatGTGT.ToString("N0");
                                else
                                    TyLeThuKhac2.Text = hmd.ThueSuatVATGiam;
                                TriGiaThuKhac2.Text = hmd.ThueGTGT.ToString("N0");
                                tongTienThueXNK += hmd.ThueXNK;
                                tongThueGTGT += hmd.ThueTTDB;
                                tongTriGiaThuKhac += hmd.ThueGTGT;
                            }
                        }
                        //if (hmd.MienThue == 1)
                        //{
                        //    TriGiaTT2.Text = "";
                        //    ThueSuatXNK2.Text = "";
                        //    TienThueXNK2.Text = "";
                        //    TriGiaTTGTGT2.Text = "";
                        //    ThueSuatGTGT2.Text = "";
                        //    TienThueGTGT2.Text = "";
                        //    TyLeThuKhac2.Text = "";
                        //    TriGiaThuKhac2.Text = "";
                        //}

                        tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                        tongTienThueTatCa = tongTienThueTatCa + hmd.ThueXNK + hmd.PhuThu + hmd.ThueTTDB + hmd.ThueGTGT;
                    }
                    #endregion
                    #region 3 hàng
                    if (this.TKMD.HMDCollection.Count == 3)
                    {
                        HangMauDich hmd = this.TKMD.HMDCollection[2];
                        if (!InMaHang)
                            TenHang3.Text = hmd.TenHang;
                        else
                        {
                            if (hmd.MaPhu.Trim().Length > 0)
                                TenHang3.Text = hmd.TenHang + "/" + hmd.MaPhu;
                            else
                                TenHang3.Text = hmd.TenHang;
                        }
                        TenHang3.WordWrap = true;
                        TenHang3.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));
                        MaHS3.Text = hmd.MaHS;
                        XuatXu3.Text = hmd.NuocXX_ID;
                        Luong3.Text = hmd.SoLuong.ToString("G15");
                        DVT3.Text = DonViTinh.GetName(hmd.DVT_ID);
                        if (hmd.FOC)
                        {
                            DonGiaNT3.Text = "F.O.C";
                            TriGiaNT3.Text = "F.O.C";
                            TriGiaTT3.Text = "";
                            ThueSuatXNK3.Text = "";
                            TienThueXNK3.Text = "";
                            TriGiaTTGTGT3.Text = "";
                            ThueSuatGTGT3.Text = "";
                            TienThueGTGT3.Text = "";
                            TyLeThuKhac3.Text = "";
                            TriGiaThuKhac3.Text = "";


                        }
                        else
                        {
                            DonGiaNT3.Text = hmd.DonGiaKB.ToString("G10");
                            TriGiaNT3.Text = hmd.TriGiaKB.ToString("N2");
                            TriGiaTT3.Text = hmd.TriGiaTT.ToString("N0");
                            if (hmd.ThueTuyetDoi)
                            {
                                ThueSuatXNK3.Text = "";
                            }
                            else
                            {
                                if (hmd.ThueSuatXNKGiam.Trim() == "")
                                    ThueSuatXNK3.Text = hmd.ThueSuatXNK.ToString("N0");
                                else
                                    ThueSuatXNK3.Text = hmd.ThueSuatXNKGiam;
                            }
                            TienThueXNK3.Text = hmd.ThueXNK.ToString("N0");
                            if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                            {
                                double TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                                TriGiaTTGTGT3.Text = TriGiaTTGTGT.ToString("N0");
                                if (hmd.ThueSuatVATGiam.Trim() == "")
                                    ThueSuatGTGT3.Text = hmd.ThueSuatGTGT.ToString("N0");
                                else
                                    ThueSuatGTGT3.Text = hmd.ThueSuatVATGiam;
                                TienThueGTGT3.Text = hmd.ThueGTGT.ToString("N0");
                                TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");
                                TriGiaThuKhac3.Text = hmd.PhuThu.ToString("N0");
                                tongTienThueXNK += hmd.ThueXNK;
                                tongThueGTGT += hmd.ThueGTGT;
                                tongTriGiaThuKhac += hmd.PhuThu;
                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                            {
                                double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                                TriGiaTTGTGT3.Text = TriGiaTTTTDB.ToString("N0");
                                if (hmd.ThueSuatTTDBGiam.Trim() == "")
                                    ThueSuatGTGT3.Text = hmd.ThueSuatTTDB.ToString("N0");
                                else
                                    ThueSuatGTGT3.Text = hmd.ThueSuatTTDBGiam;
                                TienThueGTGT3.Text = hmd.ThueTTDB.ToString("N0");
                                TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");
                                TriGiaThuKhac3.Text = hmd.PhuThu.ToString("N0");
                                tongTienThueXNK += hmd.ThueXNK;
                                tongThueGTGT += hmd.ThueTTDB;
                                tongTriGiaThuKhac += hmd.PhuThu;
                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                            {
                                double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                                TriGiaTTGTGT3.Text = TriGiaTTTTDB.ToString("N0");
                                if (hmd.ThueSuatTTDBGiam.Trim() == "")
                                    ThueSuatGTGT3.Text = hmd.ThueSuatTTDB.ToString("N0");
                                else
                                    ThueSuatGTGT3.Text = hmd.ThueSuatTTDBGiam;
                                TienThueGTGT3.Text = hmd.ThueTTDB.ToString("N0");
                                if (hmd.ThueSuatVATGiam.Length == 0)
                                    TyLeThuKhac3.Text = hmd.ThueSuatGTGT.ToString("N0");
                                else
                                    TyLeThuKhac3.Text = hmd.ThueSuatVATGiam;
                                TriGiaThuKhac3.Text = hmd.ThueGTGT.ToString("N0");
                                tongTienThueXNK += hmd.ThueXNK;
                                tongThueGTGT += hmd.ThueTTDB;
                                tongTriGiaThuKhac += hmd.ThueGTGT;
                            }
                        }
                        //if (hmd.MienThue == 1)
                        //{
                        //    TriGiaTT3.Text = "";
                        //    ThueSuatXNK3.Text = "";
                        //    TienThueXNK3.Text = "";
                        //    TriGiaTTGTGT3.Text = "";
                        //    ThueSuatGTGT3.Text = "";
                        //    TienThueGTGT3.Text = "";
                        //    TyLeThuKhac3.Text = "";
                        //    TriGiaThuKhac3.Text = "";
                        //}

                        tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);

                        tongTienThueTatCa += hmd.ThueXNK + hmd.PhuThu + hmd.ThueTTDB + hmd.ThueGTGT;
                    }
                    #endregion
                }
                #endregion
                
                #region nhiều hàng
            
                else
                {
                    //TenHang1.Text = "HÀNG HÓA NHẬP";
                    TenHang1.Text = "(Chi tiết theo phụ lục đính kèm)";
                    //TenHang2.Text = "(Chi tiết theo phụ lục đính kèm)";
                    foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                    {
                        if (!hmd.FOC)
                        {
                            tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                            tongTienThueXNK += hmd.ThueXNK;
                            tongTriGiaTTNK += hmd.TriGiaTT;

                            tongTienThueTatCa += hmd.ThueXNK + hmd.PhuThu + hmd.ThueTTDB + hmd.ThueGTGT;

                            if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                            {
                                tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                                tongThueGTGT += hmd.ThueGTGT;
                                tongTriGiaThuKhac += hmd.PhuThu;
                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                            {
                                tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                                tongThueGTGT += hmd.ThueTTDB;
                                tongTriGiaThuKhac += hmd.PhuThu;
                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                            {
                                tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                                tongThueGTGT += hmd.ThueTTDB;
                                tongTriGiaThuKhac += hmd.ThueGTGT;
                            }

                        }
                    }
                    //XuatXu1.Text = Nuoc.GetName(this.TKMD.HMDCollection[0].NuocXX_ID);
            
                
                     //Khi co phu luc thi khong hien thi :
                    //lblTGNTHang1.Text = tongTriGiaNT.ToString("N2");

                    //lblTGTTNKHang1.Text = tongTriGiaTTNK.ToString("N0");
                    //lblTienThueNKHang1.Text = tongTienThueXNK.ToString("N0");
                    //lblTGTTVATHang1.Text = tongTriGiaTTGTGT.ToString("N0");
                    //lblTienThueVATHang1.Text = tongThueGTGT.ToString("N0");
                    //TriGiaThuKhac1.Text = tongTriGiaThuKhac.ToString("N0");
                 
            
                }
                #endregion

                // - Linhhtn mark 20100622 - Tổng trị giá NT không được cộng các loại phí
                //tongTriGiaNT += Convert.ToDouble(this.TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);
                lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");

                lblTongThueXNK.Text = tongTienThueXNK.ToString("N0");
                if (tongThueGTGT > 0)
                    lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
                else
                    lblTongTienThueGTGT.Text = "";
                if (tongTriGiaThuKhac > 0)
                    lblTongTriGiaThuKhac.Text = tongTriGiaThuKhac.ToString("N0");
                else
                    lblTongTriGiaThuKhac.Text = "";
                lblTongThueXNKSo.Text = tongTienThueTatCa.ToString("N0");// this.TinhTongThueHMD().ToString("N0");

                string s = Company.KD.BLL.Utils.VNCurrency.ToString(this.TinhTongThueHMD()).Trim();
                s = s[0].ToString().ToUpper() + s.Substring(1);
                lblTongThueXNKChu.Text = s.Replace("  ", " ");

                //DATLMQ update Font HuongDan 17/12/2010
                lblHuongDan.Text = TKMD.HUONGDAN;
                lblHuongDan.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                if (MienThue1)
                {
                    lblTGTTNKHang1.Text = "";
                    lblThueSuatNKHang1.Text = "";
                    lblTienThueNKHang1.Text = "";
                    TriGiaTT2.Text = "";
                    ThueSuatXNK2.Text = "";
                    TienThueXNK2.Text = "";
                    TriGiaTT3.Text = "";
                    ThueSuatXNK3.Text = "";
                    TienThueXNK3.Text = "";
                    lblTongThueXNK.Text = "";
                }
                if (MienThue2)
                {
                    lblTGTTVATHang1.Text = "";
                    lblThueSuatVATHang1.Text = "";
                    lblTienThueVATHang1.Text = "";
                    TriGiaTTGTGT2.Text = "";
                    ThueSuatGTGT2.Text = "";
                    TienThueGTGT2.Text = "";
                    TriGiaTTGTGT3.Text = "";
                    ThueSuatGTGT3.Text = "";
                    TienThueGTGT3.Text = "";
                    lblTongTienThueGTGT.Text = "";
                }
                if (MienThue1 && MienThue2)
                {
                    lblTongThueXNKSo.Text = lblTongThueXNKChu.Text = "";
                }

                //datlmq update 29072010
                lblDeXuatKhac.Text = "32. Ghi chép khác: " + TKMD.DeXuatKhac;
                if (TKMD.TrongLuongNet > 0 && TKMD.DeXuatKhac.Length > 0)
                    lblDeXuatKhac.Text += "; " + "Trọng lượng tịnh: " + TKMD.TrongLuongNet.ToString();
                else if (TKMD.TrongLuongNet > 0 && TKMD.DeXuatKhac.Equals(""))
                    lblDeXuatKhac.Text += "Trọng lượng tịnh: " + TKMD.TrongLuongNet.ToString();
                lblDeXuatKhac.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));

                */
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + " ";
            temp += s[s.Length - 1];
            return temp;
        }

        private void ResetEmpty()
        {
            try
            {
                //Cuc HQ
                lblChiCucHQDangKy.Text = "";

                //Chi cuc Hai quan dang ky
                lblChiCucHQDangKy.Text = "";

                //Chi cuc Hai quan cua khau
                lblChiCucHQCuaKhau.Text = "";

                //So tham chieu
                this.lblSoThamChieu.Text = "";

                //Ngay gio so tham chieu
                lblNgaySoThamChieu.Text = "";

                //Số tờ khai
                lblSoToKhai.Text = "";

                //Ngày giờ đăng ký
                lblNgayDangKy.Text = "";

                //So luong phu luc to khai
                lblSoPhuLucToKhai.Text = "";

                //01.Nguoi Xuat khau
                lblNguoiXK.Text = "";

                //02.Nguoi nhap khau
                lblNguoiNK.Text = lblMSTNguoiNhapKhau.Text = "";

                //03.Dơn vi uy thac
                lblTenNguoiUyThac.Text = lblMSTNguoiUyThac.Text = "";

                //04.Dai ly Hai quan
                lblTenDaiLy.Text = lblMSTHaiQuan.Text = "";

                //05. Ma Loai Hinh
                lblLoaiHinh.Text = "";

                //06. Hoa Don thuong mai
                lblHoaDonThuongMai.Text = lblNgayHoaDonThuongMai.Text = "";

                //07. GiayPhep
                lblGiayPhep.Text = lblNgayHetHanGiayPhep.Text = "";

                //08. Hop dong
                lblHopDong.Text = lblNgayHetHanHopDong.Text = " ";

                //09. VanTaiDon
                lblVanTaiDon.Text = lblNgayVanTaiDon.Text = "";

                //10. CangXepHang
                lblCangXepHang.Text = "";

                //11. CangDoHang
                lblMaCangdoHang.Text = lblTenCangDoHang.Text = "";

                //12. MaPTVT
                lblPhuongTienVanTai.Text = lblTenSoHieuPTVT.Text = lblNgayDenPTVT.Text = "";

                //13. NuocXK
                lblNuocXK.Text = lblTenNuoc.Text = "";

                //14. DieuKienGiaoHang
                lblDieuKienGiaoHang.Text = "";

                //15.PhuongThucThanhToan
                lblPhuongThucThanhToan.Text = "";

                //16. DongTienThanhToan
                lblDongTienThanhToan.Text = "";

                //17. TyGiaTinhThue
                lblTyGiaTinhThue.Text = "";

                //32. Chung tu di kem
                lblChungTuDiKem.Text = "";

                //33. Ky cam doan
                //Ngay thang nam in to khai
                lblNgayIn.Text = "Ngày...tháng...năm.....";

                //34. Ket qua va huong dan phan luong
                lblKetQuaPhanLuong.Text = "";

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}
