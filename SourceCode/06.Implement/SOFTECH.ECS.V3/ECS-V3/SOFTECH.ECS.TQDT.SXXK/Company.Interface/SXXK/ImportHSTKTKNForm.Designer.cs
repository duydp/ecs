namespace Company.Interface.SXXK
{
    partial class ImportHSTKTKNForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportHSTKTKNForm));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRow = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSheet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNgayHoanThanhColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNgayDKColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMLHColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSTKColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnReadFile = new Janus.Windows.EditControls.UIButton();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenSheet = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvFilePath = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenSP = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaHS = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cboDateFormat = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.cboDateFormat);
            this.grbMain.Controls.Add(this.label7);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(572, 184);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Excel 97 - 2003 files (*.xls)|*.xls";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(191, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 26);
            this.label2.TabIndex = 188;
            this.label2.Text = "Cột \r\nSố tờ khai";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(274, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 26);
            this.label3.TabIndex = 190;
            this.label3.Text = "Cột \r\nMã loại hình";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(368, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 26);
            this.label4.TabIndex = 192;
            this.label4.Text = "Cột Ngày \r\nđăng ký";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(447, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 26);
            this.label5.TabIndex = 194;
            this.label5.Text = "Cột Ngày \r\nhoàn thành";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtRow
            // 
            this.txtRow.Location = new System.Drawing.Point(119, 51);
            this.txtRow.Name = "txtRow";
            this.txtRow.Size = new System.Drawing.Size(60, 21);
            this.txtRow.TabIndex = 1;
            this.txtRow.Text = "2";
            this.txtRow.Value = 2;
            this.txtRow.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtRow.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 199;
            this.label6.Text = "Tên Sheet";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(116, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 208;
            this.label11.Text = "Dòng đầu";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtSheet);
            this.uiGroupBox1.Controls.Add(this.txtNgayHoanThanhColumn);
            this.uiGroupBox1.Controls.Add(this.txtNgayDKColumn);
            this.uiGroupBox1.Controls.Add(this.txtMLHColumn);
            this.uiGroupBox1.Controls.Add(this.txtSTKColumn);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.txtRow);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(545, 83);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Thông số cấu hình";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtSheet
            // 
            this.txtSheet.Location = new System.Drawing.Point(14, 51);
            this.txtSheet.Name = "txtSheet";
            this.txtSheet.Size = new System.Drawing.Size(99, 21);
            this.txtSheet.TabIndex = 0;
            this.txtSheet.VisualStyleManager = this.vsmMain;
            // 
            // txtNgayHoanThanhColumn
            // 
            this.txtNgayHoanThanhColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayHoanThanhColumn.Location = new System.Drawing.Point(447, 51);
            this.txtNgayHoanThanhColumn.MaxLength = 1;
            this.txtNgayHoanThanhColumn.Name = "txtNgayHoanThanhColumn";
            this.txtNgayHoanThanhColumn.Size = new System.Drawing.Size(71, 21);
            this.txtNgayHoanThanhColumn.TabIndex = 5;
            this.txtNgayHoanThanhColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtNgayDKColumn
            // 
            this.txtNgayDKColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayDKColumn.Location = new System.Drawing.Point(361, 51);
            this.txtNgayDKColumn.MaxLength = 1;
            this.txtNgayDKColumn.Name = "txtNgayDKColumn";
            this.txtNgayDKColumn.Size = new System.Drawing.Size(75, 21);
            this.txtNgayDKColumn.TabIndex = 4;
            this.txtNgayDKColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtMLHColumn
            // 
            this.txtMLHColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMLHColumn.Location = new System.Drawing.Point(274, 51);
            this.txtMLHColumn.MaxLength = 1;
            this.txtMLHColumn.Name = "txtMLHColumn";
            this.txtMLHColumn.Size = new System.Drawing.Size(75, 21);
            this.txtMLHColumn.TabIndex = 3;
            this.txtMLHColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtSTKColumn
            // 
            this.txtSTKColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSTKColumn.Location = new System.Drawing.Point(189, 51);
            this.txtSTKColumn.MaxLength = 1;
            this.txtSTKColumn.Name = "txtSTKColumn";
            this.txtSTKColumn.Size = new System.Drawing.Size(75, 21);
            this.txtSTKColumn.TabIndex = 2;
            this.txtSTKColumn.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnReadFile);
            this.uiGroupBox2.Controls.Add(this.txtFilePath);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 101);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(545, 53);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Đường dẫn file Excel cần nhập";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnReadFile
            // 
            this.btnReadFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReadFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadFile.Icon = ((System.Drawing.Icon)(resources.GetObject("btnReadFile.Icon")));
            this.btnReadFile.Location = new System.Drawing.Point(428, 19);
            this.btnReadFile.Name = "btnReadFile";
            this.btnReadFile.Size = new System.Drawing.Size(108, 23);
            this.btnReadFile.TabIndex = 1;
            this.btnReadFile.Text = "Đọc file";
            this.btnReadFile.VisualStyleManager = this.vsmMain;
            this.btnReadFile.Click += new System.EventHandler(this.btnReadFile_Click);
            // 
            // txtFilePath
            // 
            this.txtFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilePath.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtFilePath.Location = new System.Drawing.Point(13, 19);
            this.txtFilePath.Multiline = true;
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(409, 23);
            this.txtFilePath.TabIndex = 0;
            this.txtFilePath.VisualStyleManager = this.vsmMain;
            this.txtFilePath.ButtonClick += new System.EventHandler(this.txtFilePath_ButtonClick);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(487, 160);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this.grbMain;
            this.cvError.HostingForm = this;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtSTKColumn;
            this.rfvMa.ErrorMessage = "\"Cột mã NPL\" bắt buộc phải nhập.";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            this.rfvMa.Tag = "rfvMa";
            // 
            // rfvTenSheet
            // 
            this.rfvTenSheet.ControlToValidate = this.txtSheet;
            this.rfvTenSheet.ErrorMessage = "\"Tên sheet\" bắt buộc phải nhập.";
            this.rfvTenSheet.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSheet.Icon")));
            this.rfvTenSheet.Tag = "rfvTenSheet";
            // 
            // rfvDVT
            // 
            this.rfvDVT.ControlToValidate = this.txtNgayHoanThanhColumn;
            this.rfvDVT.ErrorMessage = "\"Cột đơn vị tính\" bắt buộc phải chọn.";
            this.rfvDVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDVT.Icon")));
            this.rfvDVT.Tag = "rfvDVT";
            // 
            // rfvFilePath
            // 
            this.rfvFilePath.ControlToValidate = this.txtFilePath;
            this.rfvFilePath.ErrorMessage = "Chơa  chọn file Excel.";
            this.rfvFilePath.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvFilePath.Icon")));
            this.rfvFilePath.Tag = "rfvFilePath";
            // 
            // rfvTenSP
            // 
            this.rfvTenSP.ControlToValidate = this.txtMLHColumn;
            this.rfvTenSP.ErrorMessage = "\"Cột tên NPL\" bắt buộc phải chọn.";
            this.rfvTenSP.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSP.Icon")));
            this.rfvTenSP.Tag = "rfvTenSP";
            // 
            // rfvMaHS
            // 
            this.rfvMaHS.ControlToValidate = this.txtNgayDKColumn;
            this.rfvMaHS.ErrorMessage = "\"Cột mã HS\" bắt buộc phải chọn.";
            this.rfvMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaHS.Icon")));
            this.rfvMaHS.Tag = "rfvMaHS";
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // cboDateFormat
            // 
            this.cboDateFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDateFormat.FormattingEnabled = true;
            this.cboDateFormat.Items.AddRange(new object[] {
            "dd/MM/yyyy",
            "MM/dd/yyy"});
            this.cboDateFormat.Location = new System.Drawing.Point(131, 160);
            this.cboDateFormat.Name = "cboDateFormat";
            this.cboDateFormat.Size = new System.Drawing.Size(121, 21);
            this.cboDateFormat.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(23, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 13);
            this.label7.TabIndex = 216;
            this.label7.Text = "Định dạng ngày";
            // 
            // ImportHSTKTKNForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(572, 184);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportHSTKTKNForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Đọc danh sách tờ khai nhập từ file Excel";
            this.Load += new System.EventHandler(this.ImportHSTKTKNForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRow;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayDKColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtMLHColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtSTKColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayHoanThanhColumn;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSheet;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDVT;
        private Janus.Windows.GridEX.EditControls.EditBox txtSheet;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvFilePath;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSP;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaHS;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIButton btnReadFile;
        private System.Windows.Forms.ComboBox cboDateFormat;
        private System.Windows.Forms.Label label7;
    }
}