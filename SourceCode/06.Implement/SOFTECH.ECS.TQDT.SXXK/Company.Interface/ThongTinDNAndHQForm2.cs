﻿using System;
using System.Xml;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.SXXK.ThanhKhoan;
using System.Threading;
using System.Data;
using System.Configuration;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class ThongTinDNAndHQForm2 : BaseForm
    {
        public ThongTinDNAndHQForm2()
        {
            InitializeComponent();
        }

        private void SendForm_Load(object sender, EventArgs e)
        {
            /* Update by HungTQ 14/01/2011*/
            txtDiaChi.Text = GlobalSettings.DIA_CHI;
            txtMaDN.Text = GlobalSettings.MA_DON_VI;
            txtTenDN.Text = GlobalSettings.TEN_DON_VI;
            txtTenNganHQ.Text = GlobalSettings.TEN_HAI_QUAN_NGAN;
            txtTenCucHQ.Text = GlobalSettings.TEN_CUC_HAI_QUAN;
            txtMailHaiQuan.Text = GlobalSettings.MailHaiQuan;
            txtMaCuc.Text = GlobalSettings.MA_CUC_HAI_QUAN;
            txtMaMid.Text = GlobalSettings.MaMID;
            donViHaiQuanControl1.Ma = GlobalSettings.MA_HAI_QUAN;
            donViHaiQuanControl1.ReadOnly = false;
            txtThongBaoHetHan.Text = GlobalSettings.ThongBaoHetHan.ToString();
            txtSoTienKhoanTKN.Text = GlobalSettings.SoTienKhoanTKN.ToString();
            txtSoTienKhoanTKX.Text = GlobalSettings.SoTienKhoanTKX.ToString();

            txtMailDN.Text = GlobalSettings.MailDoanhNghiep;
            txtDienThoai.Text = GlobalSettings.DienThoai;
            txtFax.Text = GlobalSettings.Fax;
            txtNguoiLienhe.Text = GlobalSettings.NguoiLienHe;
            chkOnlyMe.Visible = GlobalSettings.IsDaiLy;
            chkOnlyMe.Checked = GlobalSettings.IsOnlyMe;
        }

        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            containerValidator1.Validate();
            if (!containerValidator1.IsValid)
                return;

            if (!Company.Interface.Globals.ValidateNull(donViHaiQuanControl1, errorProvider1, "Mã chi cục Hải quan"))
                return;
            if (!Company.Interface.Globals.ValidateNull(txtDienThoai, errorProvider1, "Điện thoại"))
                return;
            if (!Company.Interface.Globals.ValidateNull(txtMailDN, errorProvider1, "Email (Thư điện tử)"))
                return;
            GlobalSettings.RefreshKey();
            #region Oldcode
            //Hungtq 14/01/2011. Luu cau hinh
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DIA_CHI", txtDiaChi.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_DON_VI", txtMaDN.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_HAI_QUAN", donViHaiQuanControl1.Ma.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_HAI_QUAN", donViHaiQuanControl1.Ten);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_HAI_QUAN_NGAN", txtTenNganHQ.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_DON_VI", txtTenDN.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_CUC_HAI_QUAN", txtMaCuc.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_CUC_HAI_QUAN", txtTenCucHQ.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MailHaiQuan", txtMailHaiQuan.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MaMID", txtMaMid.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ThongBaoHetHan", txtThongBaoHetHan.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SoTienKhoanTKN", txtSoTienKhoanTKN.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SoTienKhoanTKX", txtSoTienKhoanTKX.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MailDoanhNghiep", txtMailDN.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FromMail", txtMailDN.Text.Trim());            
            #endregion

            /*DATLMQ update Lưu cấu hình vào file config 18/01/2011.*/
            XmlDocument doc = new XmlDocument();
            string path = Company.BLL.EntityBase.GetPathProram() + "\\ConfigDoanhNghiep";
            //Hungtq update 28/01/2011.
            string fileName = Company.KDT.SHARE.Components.Globals.GetFileName(path);

            doc.Load(fileName);
            //HUNGTQ Updated 07/06/2011
            //Set thông tin MaCucHQ
            GlobalSettings.MA_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaCucHQ").InnerText = txtMaCuc.Text.Trim();
            //Set thông tin TenCucHQ
            GlobalSettings.TEN_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenCucHQ").InnerText = txtTenCucHQ.Text.Trim();
            //Set thông tin MaChiCucHQ
            GlobalSettings.MA_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaChiCucHQ").InnerText = donViHaiQuanControl1.Ma.Trim();
            //Set thông tin TenChiCucHQ
            GlobalSettings.TEN_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenChiCucHQ").InnerText = donViHaiQuanControl1.Ten.Trim();
            //Set thông tin TenNganChiCucHQ
            GlobalSettings.TEN_HAI_QUAN_NGAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenNganChiCucHQ").InnerText = txtTenNganHQ.Text.Trim();
            //Set thông tin MailHQ
            GlobalSettings.MailHaiQuan = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailHQ").InnerText = txtMailHaiQuan.Text.Trim();
            //Set thông tin MaDoanhNghiep
            GlobalSettings.MA_DON_VI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaDoanhNghiep").InnerText = txtMaDN.Text.Trim();
            //Set thông tin TenDoanhNghiep
            GlobalSettings.TEN_DON_VI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenDoanhNghiep").InnerText = txtTenDN.Text.Trim();
            //Set thông tin DiaChiDoanhNghiep
            GlobalSettings.DIA_CHI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DiaChiDoanhNghiep").InnerText = txtDiaChi.Text.Trim();
            //Set thông tin MaMid
            GlobalSettings.MaMID = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaMID").InnerText = txtMaMid.Text.Trim();
            //Set thông tin Email doanh nghiep/ ca nhan
            GlobalSettings.MailDoanhNghiep = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailDoanhNghiep").InnerText = txtMailDN.Text.Trim();
            //Set thông tin Dien thoai
            GlobalSettings.DienThoai = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DienThoai").InnerText = txtDienThoai.Text.Trim();
            //Set thông tin Fax
            GlobalSettings.Fax = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Fax").InnerText = txtFax.Text.Trim();
            //Set thông tin Nguoi lien he
            GlobalSettings.NguoiLienHe = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "NguoiLienHe").InnerText = txtNguoiLienhe.Text.Trim();

            GlobalSettings.IsOnlyMe =chkOnlyMe.Checked;
            Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "OnlyMe").InnerText = chkOnlyMe.Checked.ToString();
            //Lưu file cấu hình
            doc.Save(fileName);

            try
            {
                CheckUser.Bll.UserInfor customer = new CheckUser.Bll.UserInfor();
                customer.MaDonVi = GlobalSettings.MA_DON_VI;
                customer.TenDonVi = GlobalSettings.TEN_DON_VI;
                customer.SoDTLienHe = GlobalSettings.DienThoai;
                customer.Email = GlobalSettings.MailDoanhNghiep;
                customer.DiaChi = GlobalSettings.DIA_CHI;
                customer.MaMayKichHoat = Program.getProcessorID();
                customer.TenPhienBan = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("AssemblyName");
                customer.TinhTrang = Program.lic.licenseName + "\r\nMã chi cục: " + donViHaiQuanControl1.Ma + "\r\nTên chi cục: " + donViHaiQuanControl1.Ten;
                customer.NgayHetHan = System.Convert.ToDateTime(Program.lic.dayExpires);
                customer.KeyLicense = Program.lic.codeActivate;
                WSCustomer.Service1 ws = new Company.Interface.WSCustomer.Service1();
                ws.InsertUser(customer);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("Lưu thông tin Doanh nghiệp qua Web Service.", ex); }

            ShowMessage("Lưu file cấu hình Thông tin Doanh nghiệp và Hải quan thành công.", false);
            this.Close();
        }

        private void donViHaiQuanControl1_Leave(object sender, EventArgs e)
        {
            string maCuc = donViHaiQuanControl1.Ma.Substring(1, 2);
            txtMaCuc.Text = maCuc;
            txtTenCucHQ.Text = Company.BLL.DuLieuChuan.DonViHaiQuan.GetName("Z" + maCuc + "Z");
            txtTenNganHQ.Text = donViHaiQuanControl1.Ten;
        }
    }
}