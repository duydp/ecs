﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho
{
   public class PNK_AdditionalInformation
    {
        /// <summary>
        /// Ghi chú khác
        /// </summary>
       [XmlElement("content")]
       public string Content { get; set; }
    }
}
