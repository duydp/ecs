/*
Run this script on:

        192.168.72.100.ECS_TQDT_KD_V4_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_KD_V4

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 01/29/2013 4:41:54 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [dbo].[t_HaiQuan_LoaiCO]'
GO
ALTER TABLE [dbo].[t_HaiQuan_LoaiCO] DROP CONSTRAINT [PK_SLOAI_CO]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_HaiQuan_LoaiCO]'
GO
ALTER TABLE [dbo].[t_HaiQuan_LoaiCO] ALTER COLUMN [Ma] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SLOAI_CO] on [dbo].[t_HaiQuan_LoaiCO]'
GO
ALTER TABLE [dbo].[t_HaiQuan_LoaiCO] ADD CONSTRAINT [PK_SLOAI_CO] PRIMARY KEY CLUSTERED  ([Ma])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] ADD
[MienThue_SoVB] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MienThue_TS] [float] NULL,
[MienThue_TyLeGiam] [float] NULL,
[IsHangDongBo] [bit] NULL,
[CheDoUuDai] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_SelectBy_MaHang]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_SelectBy_MaHang]
-- Database: ECS_TQDT_KD_V3
-- Author: H.N.KHANH
-- Time created: Friday, May 25, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_SelectBy_MaHang]
	@MaPhu VARCHAR(30)
AS

BEGIN

SELECT
[ID],
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[FOC],
	[ThueTuyetDoi],
	[ThueSuatXNKGiam],
	[ThueSuatTTDBGiam],
	[ThueSuatVATGiam],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[isHangCu],
	[BieuThueXNK],
	[BieuThueTTDB],
	[BieuThueGTGT],
	[BieuThueBVMT],
	[ThongTinKhac],
	[BieuThueCBPG],
	[MienThue_SoVB],
	[MienThue_TS],
	[MienThue_TyLeGiam],
	[IsHangDongBo],
	[CheDoUuDai]
FROM
	[dbo].[t_KDT_HangMauDich]
WHERE
	[MaPhu] = @MaPhu
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_ToKhaiMauDich_SaoChep]'
GO
ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SaoChep]  
 @TKMD_ID BIGINT,  
 @TKMD_ID_New BIGINT OUTPUT  
AS  
BEGIN  
 BEGIN TRANSACTION  
  INSERT INTO dbo.t_KDT_ToKhaiMauDich 
  (
	SoTiepNhan,  
         NgayTiepNhan ,  
         MaHaiQuan ,  
         SoToKhai,  
         MaLoaiHinh ,  
         NgayDangKy,  
         MaDoanhNghiep ,  
         TenDoanhNghiep ,  
         MaDaiLyTTHQ ,  
         TenDaiLyTTHQ ,  
         TenDonViDoiTac ,  
         ChiTietDonViDoiTac ,  
         SoGiayPhep ,  
         NgayGiayPhep ,  
         NgayHetHanGiayPhep ,  
         SoHopDong ,  
         NgayHopDong ,  
         NgayHetHanHopDong ,  
         SoHoaDonThuongMai ,  
         NgayHoaDonThuongMai ,  
         PTVT_ID ,  
         SoHieuPTVT ,  
         NgayDenPTVT ,  
         QuocTichPTVT_ID ,  
         LoaiVanDon ,  
         SoVanDon ,  
         NgayVanDon ,  
         NuocXK_ID ,  
         NuocNK_ID ,  
         DiaDiemXepHang ,  
         CuaKhau_ID ,  
         DKGH_ID ,  
         NguyenTe_ID ,  
         TyGiaTinhThue ,  
         TyGiaUSD ,  
         PTTT_ID ,  
         SoHang ,  
         SoLuongPLTK ,  
         TenChuHang ,  
         ChucVu,
         SoContainer20 ,  
         SoContainer40 ,  
         SoKien ,  
         TrongLuong ,  
         TongTriGiaKhaiBao ,  
         TongTriGiaTinhThue ,  
         LoaiToKhaiGiaCong ,  
         LePhiHaiQuan ,  
         PhiBaoHiem ,  
         PhiVanChuyen ,  
         PhiXepDoHang ,  
         PhiKhac ,  
         CanBoDangKy ,  
         QuanLyMay ,  
         TrangThaiXuLy,  
         LoaiHangHoa ,  
         GiayTo ,  
         PhanLuong ,  
         MaDonViUT ,  
         TenDonViUT ,  
         TrongLuongNet ,  
         SoTienKhoan ,  
         GUIDSTR ,  
         DeXuatKhac,  
         LyDoSua ,  
         ActionStatus ,  
         GuidReference,  
         NamDK ,  
         HUONGDAN 
  )
  SELECT   
         SoTiepNhan = 0 ,  
         NgayTiepNhan = '1900-1-1' ,  
         MaHaiQuan ,  
         SoToKhai = 0,  
         MaLoaiHinh ,  
         NgayDangKy = '1900-1-1',  
         MaDoanhNghiep ,  
         TenDoanhNghiep ,  
         MaDaiLyTTHQ ,  
         TenDaiLyTTHQ ,  
         TenDonViDoiTac ,  
         ChiTietDonViDoiTac ,  
         SoGiayPhep ,  
         NgayGiayPhep ,  
         NgayHetHanGiayPhep ,  
         SoHopDong ,  
         NgayHopDong ,  
         NgayHetHanHopDong ,  
         SoHoaDonThuongMai ,  
         NgayHoaDonThuongMai ,  
         PTVT_ID ,  
         SoHieuPTVT ,  
         NgayDenPTVT ,  
         QuocTichPTVT_ID ,  
         LoaiVanDon ,  
         SoVanDon ,  
         NgayVanDon ,  
         NuocXK_ID ,  
         NuocNK_ID ,  
         DiaDiemXepHang ,  
         CuaKhau_ID ,  
         DKGH_ID ,  
         NguyenTe_ID ,  
         TyGiaTinhThue ,  
         TyGiaUSD ,  
         PTTT_ID ,  
         SoHang ,  
         SoLuongPLTK ,  
         TenChuHang ,  
         ChucVu,
         SoContainer20 ,  
         SoContainer40 ,  
         SoKien ,  
         TrongLuong ,  
         TongTriGiaKhaiBao ,  
         TongTriGiaTinhThue ,  
         LoaiToKhaiGiaCong ,  
         LePhiHaiQuan ,  
         PhiBaoHiem ,  
         PhiVanChuyen ,  
         PhiXepDoHang ,  
         PhiKhac ,  
         CanBoDangKy ,  
         QuanLyMay ,  
         TrangThaiXuLy = -1,  
         LoaiHangHoa ,  
         GiayTo ,  
         PhanLuong = '',  
         MaDonViUT ,  
         TenDonViUT ,  
         TrongLuongNet ,  
         SoTienKhoan ,  
         GUIDSTR = NULL,  
         DeXuatKhac = '',  
         LyDoSua ,  
         ActionStatus = 0,  
         GuidReference = NULL,  
         NamDK = NULL,  
         HUONGDAN = ''   
     FROM dbo.t_KDT_ToKhaiMauDich WHERE ID = @TKMD_ID        
    
  SET @TKMD_ID_New = @@IDENTITY  
  INSERT INTO dbo.t_KDT_HangMauDich 
  (    [TKMD_ID]
      ,[SoThuTuHang]
      ,[MaHS]
      ,[MaPhu]
      ,[TenHang]
      ,[NuocXX_ID]
      ,[DVT_ID]
      ,[SoLuong]
      ,[TrongLuong]
      ,[DonGiaKB]
      ,[DonGiaTT]
      ,[TriGiaKB]
      ,[TriGiaTT]
      ,[TriGiaKB_VND]
      ,[ThueXNK]
      ,[ThueSuatXNK]
      ,[ThueSuatXNKGiam]
      ,[ThueTTDB]
      ,[ThueSuatTTDB]
      ,[ThueSuatTTDBGiam]
      ,[ThueGTGT]
      ,[ThueSuatGTGT]
      ,[ThueSuatVATGiam]
      ,[ThueBVMT]
      ,[ThueSuatBVMT]
      ,[ThueSuatBVMTGiam]
      ,[ThueChongPhaGia]
      ,[ThueSuatChongPhaGia]
      ,[ThueSuatChongPhaGiaGiam]
      ,[PhuThu]
      ,[TyLeThuKhac]
      ,[TriGiaThuKhac]
      ,[MienThue]
      ,[Ma_HTS]
      ,[DVT_HTS]
      ,[SoLuong_HTS]
      ,[FOC]
      ,[ThueTuyetDoi]
      ,[DonGiaTuyetDoi]
      ,[MaHSMoRong]
      ,[NhanHieu]
      ,[QuyCachPhamChat]
      ,[ThanhPhan]
      ,[Model]
      ,[MaHangSX]
      ,[TenHangSX]
      ,[isHangCu]
      ,[BieuThueXNK]
	,[BieuThueTTDB]
	,[BieuThueGTGT]
	,[BieuThueBVMT]
	,[ThongTinKhac]
	,[BieuThueCBPG]
	,[MienThue_SoVB]
	,[MienThue_TS]
	,[MienThue_TyLeGiam]
	,[IsHangDongBo]
	,[CheDoUuDai]
      )  
  SELECT   
        @TKMD_ID_New
      ,[SoThuTuHang]
      ,[MaHS]
      ,[MaPhu]
      ,[TenHang]
      ,[NuocXX_ID]
      ,[DVT_ID]
      ,[SoLuong]
      ,[TrongLuong]
      ,[DonGiaKB]
      ,[DonGiaTT]
      ,[TriGiaKB]
      ,[TriGiaTT]
      ,[TriGiaKB_VND]
      ,[ThueXNK]
      ,[ThueSuatXNK]
      ,[ThueSuatXNKGiam]
      ,[ThueTTDB]
      ,[ThueSuatTTDB]
      ,[ThueSuatTTDBGiam]
      ,[ThueGTGT]
      ,[ThueSuatGTGT]
      ,[ThueSuatVATGiam]
      ,[ThueBVMT]
      ,[ThueSuatBVMT]
      ,[ThueSuatBVMTGiam]
      ,[ThueChongPhaGia]
      ,[ThueSuatChongPhaGia]
      ,[ThueSuatChongPhaGiaGiam]
      ,[PhuThu]
      ,[TyLeThuKhac]
      ,[TriGiaThuKhac]
      ,[MienThue]
      ,[Ma_HTS]
      ,[DVT_HTS]
      ,[SoLuong_HTS]
      ,[FOC]
      ,[ThueTuyetDoi]
      ,[DonGiaTuyetDoi]
      ,[MaHSMoRong]
      ,[NhanHieu]
      ,[QuyCachPhamChat]
      ,[ThanhPhan]
      ,[Model]
      ,[MaHangSX]
      ,[TenHangSX]
      ,[isHangCu]
          ,[BieuThueXNK]
	,[BieuThueTTDB]
	,[BieuThueGTGT]
	,[BieuThueBVMT]
	,[ThongTinKhac]
	,[BieuThueCBPG]
	,[MienThue_SoVB]
	,[MienThue_TS]
	,[MienThue_TyLeGiam]
	,[IsHangDongBo]
	,[CheDoUuDai]
      
  FROM   
   dbo.t_KDT_HangMauDich    
  WHERE   
   TKMD_ID = @TKMD_ID_New          
    
 COMMIT TRANSACTION  
END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 29, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Insert]
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@TrongLuong numeric(18, 5),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueSuatXNK float,
	@ThueSuatTTDB float,
	@ThueSuatGTGT float,
	@ThueXNK float,
	@ThueTTDB float,
	@ThueGTGT float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 5),
	@FOC bit,
	@ThueTuyetDoi bit,
	@ThueSuatXNKGiam float,
	@ThueSuatTTDBGiam float,
	@ThueSuatVATGiam float,
	@DonGiaTuyetDoi float,
	@MaHSMoRong nvarchar(12),
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@MaHangSX nvarchar(30),
	@TenHangSX nvarchar(256),
	@ThueBVMT float,
	@ThueSuatBVMT float,
	@ThueSuatBVMTGiam float,
	@ThueChongPhaGia float,
	@ThueSuatChongPhaGia float,
	@ThueSuatChongPhaGiaGiam float,
	@isHangCu bit,
	@BieuThueXNK nvarchar(255),
	@BieuThueTTDB nvarchar(255),
	@BieuThueGTGT nvarchar(255),
	@BieuThueBVMT nvarchar(255),
	@ThongTinKhac nvarchar(max),
	@BieuThueCBPG nvarchar(255),
	@MienThue_SoVB nvarchar(255),
	@MienThue_TS float,
	@MienThue_TyLeGiam float,
	@IsHangDongBo bit,
	@CheDoUuDai varchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_HangMauDich]
(
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[FOC],
	[ThueTuyetDoi],
	[ThueSuatXNKGiam],
	[ThueSuatTTDBGiam],
	[ThueSuatVATGiam],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[isHangCu],
	[BieuThueXNK],
	[BieuThueTTDB],
	[BieuThueGTGT],
	[BieuThueBVMT],
	[ThongTinKhac],
	[BieuThueCBPG],
	[MienThue_SoVB],
	[MienThue_TS],
	[MienThue_TyLeGiam],
	[IsHangDongBo],
	[CheDoUuDai]
)
VALUES 
(
	@TKMD_ID,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@TrongLuong,
	@DonGiaKB,
	@DonGiaTT,
	@TriGiaKB,
	@TriGiaTT,
	@TriGiaKB_VND,
	@ThueSuatXNK,
	@ThueSuatTTDB,
	@ThueSuatGTGT,
	@ThueXNK,
	@ThueTTDB,
	@ThueGTGT,
	@PhuThu,
	@TyLeThuKhac,
	@TriGiaThuKhac,
	@MienThue,
	@Ma_HTS,
	@DVT_HTS,
	@SoLuong_HTS,
	@FOC,
	@ThueTuyetDoi,
	@ThueSuatXNKGiam,
	@ThueSuatTTDBGiam,
	@ThueSuatVATGiam,
	@DonGiaTuyetDoi,
	@MaHSMoRong,
	@NhanHieu,
	@QuyCachPhamChat,
	@ThanhPhan,
	@Model,
	@MaHangSX,
	@TenHangSX,
	@ThueBVMT,
	@ThueSuatBVMT,
	@ThueSuatBVMTGiam,
	@ThueChongPhaGia,
	@ThueSuatChongPhaGia,
	@ThueSuatChongPhaGiaGiam,
	@isHangCu,
	@BieuThueXNK,
	@BieuThueTTDB,
	@BieuThueGTGT,
	@BieuThueBVMT,
	@ThongTinKhac,
	@BieuThueCBPG,
	@MienThue_SoVB,
	@MienThue_TS,
	@MienThue_TyLeGiam,
	@IsHangDongBo,
	@CheDoUuDai
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 29, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@TrongLuong numeric(18, 5),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueSuatXNK float,
	@ThueSuatTTDB float,
	@ThueSuatGTGT float,
	@ThueXNK float,
	@ThueTTDB float,
	@ThueGTGT float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 5),
	@FOC bit,
	@ThueTuyetDoi bit,
	@ThueSuatXNKGiam float,
	@ThueSuatTTDBGiam float,
	@ThueSuatVATGiam float,
	@DonGiaTuyetDoi float,
	@MaHSMoRong nvarchar(12),
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@MaHangSX nvarchar(30),
	@TenHangSX nvarchar(256),
	@ThueBVMT float,
	@ThueSuatBVMT float,
	@ThueSuatBVMTGiam float,
	@ThueChongPhaGia float,
	@ThueSuatChongPhaGia float,
	@ThueSuatChongPhaGiaGiam float,
	@isHangCu bit,
	@BieuThueXNK nvarchar(255),
	@BieuThueTTDB nvarchar(255),
	@BieuThueGTGT nvarchar(255),
	@BieuThueBVMT nvarchar(255),
	@ThongTinKhac nvarchar(max),
	@BieuThueCBPG nvarchar(255),
	@MienThue_SoVB nvarchar(255),
	@MienThue_TS float,
	@MienThue_TyLeGiam float,
	@IsHangDongBo bit,
	@CheDoUuDai varchar(50)
AS

UPDATE
	[dbo].[t_KDT_HangMauDich]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[TrongLuong] = @TrongLuong,
	[DonGiaKB] = @DonGiaKB,
	[DonGiaTT] = @DonGiaTT,
	[TriGiaKB] = @TriGiaKB,
	[TriGiaTT] = @TriGiaTT,
	[TriGiaKB_VND] = @TriGiaKB_VND,
	[ThueSuatXNK] = @ThueSuatXNK,
	[ThueSuatTTDB] = @ThueSuatTTDB,
	[ThueSuatGTGT] = @ThueSuatGTGT,
	[ThueXNK] = @ThueXNK,
	[ThueTTDB] = @ThueTTDB,
	[ThueGTGT] = @ThueGTGT,
	[PhuThu] = @PhuThu,
	[TyLeThuKhac] = @TyLeThuKhac,
	[TriGiaThuKhac] = @TriGiaThuKhac,
	[MienThue] = @MienThue,
	[Ma_HTS] = @Ma_HTS,
	[DVT_HTS] = @DVT_HTS,
	[SoLuong_HTS] = @SoLuong_HTS,
	[FOC] = @FOC,
	[ThueTuyetDoi] = @ThueTuyetDoi,
	[ThueSuatXNKGiam] = @ThueSuatXNKGiam,
	[ThueSuatTTDBGiam] = @ThueSuatTTDBGiam,
	[ThueSuatVATGiam] = @ThueSuatVATGiam,
	[DonGiaTuyetDoi] = @DonGiaTuyetDoi,
	[MaHSMoRong] = @MaHSMoRong,
	[NhanHieu] = @NhanHieu,
	[QuyCachPhamChat] = @QuyCachPhamChat,
	[ThanhPhan] = @ThanhPhan,
	[Model] = @Model,
	[MaHangSX] = @MaHangSX,
	[TenHangSX] = @TenHangSX,
	[ThueBVMT] = @ThueBVMT,
	[ThueSuatBVMT] = @ThueSuatBVMT,
	[ThueSuatBVMTGiam] = @ThueSuatBVMTGiam,
	[ThueChongPhaGia] = @ThueChongPhaGia,
	[ThueSuatChongPhaGia] = @ThueSuatChongPhaGia,
	[ThueSuatChongPhaGiaGiam] = @ThueSuatChongPhaGiaGiam,
	[isHangCu] = @isHangCu,
	[BieuThueXNK] = @BieuThueXNK,
	[BieuThueTTDB] = @BieuThueTTDB,
	[BieuThueGTGT] = @BieuThueGTGT,
	[BieuThueBVMT] = @BieuThueBVMT,
	[ThongTinKhac] = @ThongTinKhac,
	[BieuThueCBPG] = @BieuThueCBPG,
	[MienThue_SoVB] = @MienThue_SoVB,
	[MienThue_TS] = @MienThue_TS,
	[MienThue_TyLeGiam] = @MienThue_TyLeGiam,
	[IsHangDongBo] = @IsHangDongBo,
	[CheDoUuDai] = @CheDoUuDai
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 29, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@TrongLuong numeric(18, 5),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueSuatXNK float,
	@ThueSuatTTDB float,
	@ThueSuatGTGT float,
	@ThueXNK float,
	@ThueTTDB float,
	@ThueGTGT float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 5),
	@FOC bit,
	@ThueTuyetDoi bit,
	@ThueSuatXNKGiam float,
	@ThueSuatTTDBGiam float,
	@ThueSuatVATGiam float,
	@DonGiaTuyetDoi float,
	@MaHSMoRong nvarchar(12),
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@MaHangSX nvarchar(30),
	@TenHangSX nvarchar(256),
	@ThueBVMT float,
	@ThueSuatBVMT float,
	@ThueSuatBVMTGiam float,
	@ThueChongPhaGia float,
	@ThueSuatChongPhaGia float,
	@ThueSuatChongPhaGiaGiam float,
	@isHangCu bit,
	@BieuThueXNK nvarchar(255),
	@BieuThueTTDB nvarchar(255),
	@BieuThueGTGT nvarchar(255),
	@BieuThueBVMT nvarchar(255),
	@ThongTinKhac nvarchar(max),
	@BieuThueCBPG nvarchar(255),
	@MienThue_SoVB nvarchar(255),
	@MienThue_TS float,
	@MienThue_TyLeGiam float,
	@IsHangDongBo bit,
	@CheDoUuDai varchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HangMauDich] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[TrongLuong] = @TrongLuong,
			[DonGiaKB] = @DonGiaKB,
			[DonGiaTT] = @DonGiaTT,
			[TriGiaKB] = @TriGiaKB,
			[TriGiaTT] = @TriGiaTT,
			[TriGiaKB_VND] = @TriGiaKB_VND,
			[ThueSuatXNK] = @ThueSuatXNK,
			[ThueSuatTTDB] = @ThueSuatTTDB,
			[ThueSuatGTGT] = @ThueSuatGTGT,
			[ThueXNK] = @ThueXNK,
			[ThueTTDB] = @ThueTTDB,
			[ThueGTGT] = @ThueGTGT,
			[PhuThu] = @PhuThu,
			[TyLeThuKhac] = @TyLeThuKhac,
			[TriGiaThuKhac] = @TriGiaThuKhac,
			[MienThue] = @MienThue,
			[Ma_HTS] = @Ma_HTS,
			[DVT_HTS] = @DVT_HTS,
			[SoLuong_HTS] = @SoLuong_HTS,
			[FOC] = @FOC,
			[ThueTuyetDoi] = @ThueTuyetDoi,
			[ThueSuatXNKGiam] = @ThueSuatXNKGiam,
			[ThueSuatTTDBGiam] = @ThueSuatTTDBGiam,
			[ThueSuatVATGiam] = @ThueSuatVATGiam,
			[DonGiaTuyetDoi] = @DonGiaTuyetDoi,
			[MaHSMoRong] = @MaHSMoRong,
			[NhanHieu] = @NhanHieu,
			[QuyCachPhamChat] = @QuyCachPhamChat,
			[ThanhPhan] = @ThanhPhan,
			[Model] = @Model,
			[MaHangSX] = @MaHangSX,
			[TenHangSX] = @TenHangSX,
			[ThueBVMT] = @ThueBVMT,
			[ThueSuatBVMT] = @ThueSuatBVMT,
			[ThueSuatBVMTGiam] = @ThueSuatBVMTGiam,
			[ThueChongPhaGia] = @ThueChongPhaGia,
			[ThueSuatChongPhaGia] = @ThueSuatChongPhaGia,
			[ThueSuatChongPhaGiaGiam] = @ThueSuatChongPhaGiaGiam,
			[isHangCu] = @isHangCu,
			[BieuThueXNK] = @BieuThueXNK,
			[BieuThueTTDB] = @BieuThueTTDB,
			[BieuThueGTGT] = @BieuThueGTGT,
			[BieuThueBVMT] = @BieuThueBVMT,
			[ThongTinKhac] = @ThongTinKhac,
			[BieuThueCBPG] = @BieuThueCBPG,
			[MienThue_SoVB] = @MienThue_SoVB,
			[MienThue_TS] = @MienThue_TS,
			[MienThue_TyLeGiam] = @MienThue_TyLeGiam,
			[IsHangDongBo] = @IsHangDongBo,
			[CheDoUuDai] = @CheDoUuDai
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HangMauDich]
		(
			[TKMD_ID],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[TrongLuong],
			[DonGiaKB],
			[DonGiaTT],
			[TriGiaKB],
			[TriGiaTT],
			[TriGiaKB_VND],
			[ThueSuatXNK],
			[ThueSuatTTDB],
			[ThueSuatGTGT],
			[ThueXNK],
			[ThueTTDB],
			[ThueGTGT],
			[PhuThu],
			[TyLeThuKhac],
			[TriGiaThuKhac],
			[MienThue],
			[Ma_HTS],
			[DVT_HTS],
			[SoLuong_HTS],
			[FOC],
			[ThueTuyetDoi],
			[ThueSuatXNKGiam],
			[ThueSuatTTDBGiam],
			[ThueSuatVATGiam],
			[DonGiaTuyetDoi],
			[MaHSMoRong],
			[NhanHieu],
			[QuyCachPhamChat],
			[ThanhPhan],
			[Model],
			[MaHangSX],
			[TenHangSX],
			[ThueBVMT],
			[ThueSuatBVMT],
			[ThueSuatBVMTGiam],
			[ThueChongPhaGia],
			[ThueSuatChongPhaGia],
			[ThueSuatChongPhaGiaGiam],
			[isHangCu],
			[BieuThueXNK],
			[BieuThueTTDB],
			[BieuThueGTGT],
			[BieuThueBVMT],
			[ThongTinKhac],
			[BieuThueCBPG],
			[MienThue_SoVB],
			[MienThue_TS],
			[MienThue_TyLeGiam],
			[IsHangDongBo],
			[CheDoUuDai]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@TrongLuong,
			@DonGiaKB,
			@DonGiaTT,
			@TriGiaKB,
			@TriGiaTT,
			@TriGiaKB_VND,
			@ThueSuatXNK,
			@ThueSuatTTDB,
			@ThueSuatGTGT,
			@ThueXNK,
			@ThueTTDB,
			@ThueGTGT,
			@PhuThu,
			@TyLeThuKhac,
			@TriGiaThuKhac,
			@MienThue,
			@Ma_HTS,
			@DVT_HTS,
			@SoLuong_HTS,
			@FOC,
			@ThueTuyetDoi,
			@ThueSuatXNKGiam,
			@ThueSuatTTDBGiam,
			@ThueSuatVATGiam,
			@DonGiaTuyetDoi,
			@MaHSMoRong,
			@NhanHieu,
			@QuyCachPhamChat,
			@ThanhPhan,
			@Model,
			@MaHangSX,
			@TenHangSX,
			@ThueBVMT,
			@ThueSuatBVMT,
			@ThueSuatBVMTGiam,
			@ThueChongPhaGia,
			@ThueSuatChongPhaGia,
			@ThueSuatChongPhaGiaGiam,
			@isHangCu,
			@BieuThueXNK,
			@BieuThueTTDB,
			@BieuThueGTGT,
			@BieuThueBVMT,
			@ThongTinKhac,
			@BieuThueCBPG,
			@MienThue_SoVB,
			@MienThue_TS,
			@MienThue_TyLeGiam,
			@IsHangDongBo,
			@CheDoUuDai
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 29, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_HangMauDich]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_DeleteBy_TKMD_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 29, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_HangMauDich]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 29, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[FOC],
	[ThueTuyetDoi],
	[ThueSuatXNKGiam],
	[ThueSuatTTDBGiam],
	[ThueSuatVATGiam],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[isHangCu],
	[BieuThueXNK],
	[BieuThueTTDB],
	[BieuThueGTGT],
	[BieuThueBVMT],
	[ThongTinKhac],
	[BieuThueCBPG],
	[MienThue_SoVB],
	[MienThue_TS],
	[MienThue_TyLeGiam],
	[IsHangDongBo],
	[CheDoUuDai]
FROM
	[dbo].[t_KDT_HangMauDich]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 29, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[FOC],
	[ThueTuyetDoi],
	[ThueSuatXNKGiam],
	[ThueSuatTTDBGiam],
	[ThueSuatVATGiam],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[isHangCu],
	[BieuThueXNK],
	[BieuThueTTDB],
	[BieuThueGTGT],
	[BieuThueBVMT],
	[ThongTinKhac],
	[BieuThueCBPG],
	[MienThue_SoVB],
	[MienThue_TS],
	[MienThue_TyLeGiam],
	[IsHangDongBo],
	[CheDoUuDai]
FROM
	[dbo].[t_KDT_HangMauDich]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 29, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[FOC],
	[ThueTuyetDoi],
	[ThueSuatXNKGiam],
	[ThueSuatTTDBGiam],
	[ThueSuatVATGiam],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[isHangCu],
	[BieuThueXNK],
	[BieuThueTTDB],
	[BieuThueGTGT],
	[BieuThueBVMT],
	[ThongTinKhac],
	[BieuThueCBPG],
	[MienThue_SoVB],
	[MienThue_TS],
	[MienThue_TyLeGiam],
	[IsHangDongBo],
	[CheDoUuDai]
FROM
	[dbo].[t_KDT_HangMauDich]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 29, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_HangMauDich] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 29, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[FOC],
	[ThueTuyetDoi],
	[ThueSuatXNKGiam],
	[ThueSuatTTDBGiam],
	[ThueSuatVATGiam],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX],
	[ThueBVMT],
	[ThueSuatBVMT],
	[ThueSuatBVMTGiam],
	[ThueChongPhaGia],
	[ThueSuatChongPhaGia],
	[ThueSuatChongPhaGiaGiam],
	[isHangCu],
	[BieuThueXNK],
	[BieuThueTTDB],
	[BieuThueGTGT],
	[BieuThueBVMT],
	[ThongTinKhac],
	[BieuThueCBPG],
	[MienThue_SoVB],
	[MienThue_TS],
	[MienThue_TyLeGiam],
	[IsHangDongBo],
	[CheDoUuDai]
FROM [dbo].[t_KDT_HangMauDich] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)
DELETE [dbo].[t_HaiQuan_LoaiCO]
-- Add 9 rows to [dbo].[t_HaiQuan_LoaiCO]
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('861', N'C/O Mẫu Khác')
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('AANZ', N'C/O Mẫu AANZ')
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('AI', N'C/O Mẫu AI')
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('AJ', N'C/O Mẫu AJ')
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('AK', N'C/O Mẫu AK')
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('D', N'C/O Mẫu D')
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('E', N'C/O Mẫu E')
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('JV', N'C/O Mẫu JV')
INSERT INTO [dbo].[t_HaiQuan_LoaiCO] ([Ma], [Ten]) VALUES ('S', N'C/O Mẫu S')
COMMIT TRANSACTION
GO
UPDATE [dbo].[t_HaiQuan_Cuc] SET [ServicePathV3] = 'TNTTService_V4/CISService.asmx' WHERE id = 33
UPDATE [dbo].[t_HaiQuan_Cuc] SET [ServicePathV3] = 'TQDT_V4/CISService.asmx' WHERE id = 34
UPDATE [dbo].[t_HaiQuan_Cuc] SET [ServicePathV3] = 'TNTTservice/CISService.asmx' WHERE id = 27

GO
UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '6.7'
      ,[Date] = getdate()
      ,[Notes] = ''
      GO	