﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KD.BLL.KDT;
//using Company.KDT.SHARE.Components.DuLieuChuan;
#if GC_V3 || GC_V4
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
#endif
namespace Company.Interface.Report.SXXK
{
    public partial class PhuLucTriGiaA4 : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiTriGia TKTG = new ToKhaiTriGia();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public Company.Interface.Report.ReportViewTKTGA4Form report;
        public bool BanLuuHaiQuan = false;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public HangTriGiaCollection HTGCollection = new HangTriGiaCollection();
        public  System.Data.DataTable dt = new System.Data.DataTable();
        public PhuLucTriGiaA4()
        {
            InitializeComponent();
        }
       public void BindReport()
         {
             dt.TableName = "dtPhuLuc";
             //GroupField groupItem = new GroupField("Ten");
             //this.GroupHeader1.GroupFields.Add(groupItem);
             //this.lblGroup.DataBindings.Add("Text", this.DataSource, "Ten");
             this.DataSource = dt;
             if (TKMD.NgayDangKy !=new DateTime(1900,1,1))
             lblNgayDK.Text = "Ngày đăng ký : " + TKMD.NgayDangKy.ToString("dd/MM/yyyy"); ;
             if (TKMD.SoToKhai !=0)
             lblSoTK.Text  = "Số..." + TKMD.SoToKhai + ".../NK/...../.....";
             lblSoPL.Text = " Phụ lục số :       /    tờ ";
             lblSTT.DataBindings.Add("Text", this.DataSource, "ColumnName1", "{0:N0}");
             lblSTTH.DataBindings.Add("Text", this.DataSource, "ColumnName2", "{0:N0}");
             //lblTGGD7.DataBindings.Add("Text", this.DataSource, "ColumnName3", "{0:N2}");
             lblTGGD7.DataBindings.Add("Text", this.DataSource, "ColumnName3");
             //lblTGGD8.DataBindings.Add("Text", this.DataSource, "ColumnName4", "{0:N2}");
             lblTGGD8.DataBindings.Add("Text", this.DataSource, "ColumnName4");
             //lblTGGD9.DataBindings.Add("Text", this.DataSource, "ColumnName5", "{0:N2}");
             lblTGGD9.DataBindings.Add("Text", this.DataSource, "ColumnName5");
             //lblCong10.DataBindings.Add("Text", this.DataSource, "ColumnName6", "{0:N2}");
             lblCong10.DataBindings.Add("Text", this.DataSource, "ColumnName6");
             //lblCong11.DataBindings.Add("Text", this.DataSource, "ColumnName7", "{0:N2}");
             lblCong11.DataBindings.Add("Text", this.DataSource, "ColumnName7");
             //lblCong12.DataBindings.Add("Text", this.DataSource, "ColumnName8", "{0:N2}");
             lblCong12.DataBindings.Add("Text", this.DataSource, "ColumnName8");
             //lblCong13.DataBindings.Add("Text", this.DataSource, "ColumnName9", "{0:N2}");
             lblCong13.DataBindings.Add("Text", this.DataSource, "ColumnName9");
             //lblCong14.DataBindings.Add("Text", this.DataSource, "ColumnName10", "{0:N2}");
             lblCong14.DataBindings.Add("Text", this.DataSource, "ColumnName10");
             //lblCong15.DataBindings.Add("Text", this.DataSource, "ColumnName11", "{0:N2}");
             lblCong15.DataBindings.Add("Text", this.DataSource, "ColumnName11");
             //lblCong16.DataBindings.Add("Text", this.DataSource, "ColumnName12", "{0:N2}");
             lblCong16.DataBindings.Add("Text", this.DataSource, "ColumnName12");
             //lblCong17.DataBindings.Add("Text", this.DataSource, "ColumnName13", "{0:N2}");
             lblCong17.DataBindings.Add("Text", this.DataSource, "ColumnName13");
             //lblTru18.DataBindings.Add("Text", this.DataSource, "ColumnName14", "{0:N2}");
             lblTru18.DataBindings.Add("Text", this.DataSource, "ColumnName14");
             //lblTru19.DataBindings.Add("Text", this.DataSource, "ColumnName15", "{0:N2}");
             lblTru19.DataBindings.Add("Text", this.DataSource, "ColumnName15");
             //lblTru20.DataBindings.Add("Text", this.DataSource, "ColumnName16", "{0:N2}");
             lblTru20.DataBindings.Add("Text", this.DataSource, "ColumnName16");
             //lblTru21.DataBindings.Add("Text", this.DataSource, "ColumnName17", "{0:N2}");
             lblTru21.DataBindings.Add("Text", this.DataSource, "ColumnName17");
             //lblTru22.DataBindings.Add("Text", this.DataSource, "ColumnName18", "{0:N2}");
             lblTru22.DataBindings.Add("Text", this.DataSource, "ColumnName18");
             //lblNT23.DataBindings.Add("Text", this.DataSource, "ColumnName19", "{0:N2}");
             lblNT23.DataBindings.Add("Text", this.DataSource, "ColumnName19");
             //lblVND24.DataBindings.Add("Text", this.DataSource, "ColumnName20", "{0:N2}");
             lblVND24.DataBindings.Add("Text", this.DataSource, "ColumnName20");
        }

        public double TinhTongThueHMD()
        {
            double tong = 0;
            //foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            //{
            //    if (!hmd.FOC)
            //        tong += hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac;
            //}
            return tong;
        }
        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + "   ";
            temp += s[s.Length - 1];
            return temp;
        }
        public void setVisibleImage(bool t)
        {
            
        }
        public void setNhomHang(string tenNhomHang)
        {
          //  TenHang1.Text = tenNhomHang.ToUpper();
        }
        private bool IsHaveTax()
        {
            //foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            //    if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }
        public void ShowMienThue(bool t)
        {
            //lblMienThue2.Visible = t;
            //xrTable2.Visible = !t;
            //lblTongThueXNK.Visible = lblTongTienThueGTGT.Visible = lblTongTriGiaThuKhac.Visible = xrTablesaaa.Visible = lblTongThueXNKChu.Visible = !t;

        }

        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }
        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        private void xrKD_PreviewDoubleClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            if (cell.Text.Trim() == "")
                cell.Text = "X";
            else
                cell.Text = "";
            this.CreateDocument();
        }
    }
}
