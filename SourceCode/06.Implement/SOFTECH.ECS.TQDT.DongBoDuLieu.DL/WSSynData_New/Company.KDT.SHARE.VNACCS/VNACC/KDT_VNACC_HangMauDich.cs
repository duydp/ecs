using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Linq;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_HangMauDich : ICloneable
	{
        public bool tkxuat = true;
        //public string loaihinh = "";
        List<KDT_VNACC_HangMauDich_ThueThuKhac> _ListThueVaThuKhac = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
        public List<KDT_VNACC_HangMauDich_ThueThuKhac> ThueThuKhacCollection
        {
            set { this._ListThueVaThuKhac = value; }
            get { return this._ListThueVaThuKhac; }
        }
        

        public decimal TongSoTienThuKhac { get; set; }
        ///// <summary>
        ///// Mã hàng hóa dùng cho thanh khoản và Tên hàng hóa 
        ///// Được tách từ ô Mô tả hàng hóa
        ///// </summary>
        public string MaHangHoaKhaiBao
        {
            get 
            {
                string xuatxu = this.NuocXuatXu;

                if (!string.IsNullOrEmpty(this.MaHangHoa))
                {
                    //string temp = this.MaHangHoa.Length < 48 ? this.MaHangHoa.PadRight(48 - this.MaHangHoa.Length) : this.MaHangHoa;
                    string temp = this.MaHangHoa;

                    if (this.tkxuat == false)
                    {
                        return temp + "#&" + this.TenHang;
                    }
                    return temp + "#&" + this.TenHang + "#&" + xuatxu;
                    //}
                }
                else 
                {
                    if (this.tkxuat == false)
                    {
                        return  this.TenHang;
                    }
                    return this.TenHang + "#&" + xuatxu;
                }
                    
            }
            set 
            {
                //FIXHOATHO
                if (!string.IsNullOrEmpty(this.MaHangHoa))
                {
                    if (this.tkxuat == false)
                    {
                        if (value.Contains("#&"))
                        {
                            string[] temp = value.Split(new string[] { "#&" }, StringSplitOptions.None);
                            if (temp.Length > 0)
                            {
                                var Code = temp.First();
                                var Name = temp.Last();

                                //this.TenHang = temp[1].Trim();
                                //this.MaHangHoa = temp[0].Trim();
                                if (temp.Count() >= 3)
                                {
                                    for (int i = 1; i < temp.Count(); i++)
                                    {
                                        this.TenHang += temp[i].Trim() + "#&";
                                    }
                                    this.TenHang = this.TenHang.Substring(0, this.TenHang.Length - 2);
                                }
                                else
                                {
                                    this.TenHang = Name.ToString().Trim();
                                }
                                this.MaHangHoa = Code.ToString().Trim();
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(this.TenHang))
                                //if (this.TenHang == null)
                                this.TenHang = value;
                            else
                                //minhnd: fix đồng bộ ko có tên hàng
                                value = this.TenHang.Trim();
                        }
                        //value = this.TenHang.Trim();
                    }
                    else
                    {
                        if (value.Contains("#&"))
                        {
                            string[] temp = value.Split(new string[] { "#&" }, StringSplitOptions.None);
                            if (temp.Length > 0)
                            {
                                var Code = temp.First();
                                var XuatXu = temp.Last();  
                                // DUYDP Comment
                                //this.MaHangHoa = temp[0].Trim();
                                //this.TenHang = temp[1].Trim();
                                ////this.MaHangHoa = temp[0].Trim();
                                //this.NuocXuatXu = temp[2].Trim();

                                // SỬA LỖI TÊN HÀNG THÀNH MÃ HÀNG KHI SPLIT TÊN HÀNG CÓ NHIỀU #&
                                this.MaHangHoa = Code.ToString().Trim();
                                if (temp.Count() >=4)
                                {
                                    for (int i = 1; i < temp.Count(); i++)
                                    {
                                        this.TenHang += temp[i].Trim() + "#&";
                                    }
                                    this.TenHang = this.TenHang.Substring(0, this.TenHang.Length - 2);
                                }
                                else
                                {
                                    this.TenHang = temp[1].Trim();
                                }
                                this.NuocXuatXu = XuatXu.ToString().Trim();
                            }
                        }
                        else
                        {
                            //if (this.TenHang == null)
                            if (string.IsNullOrEmpty(this.TenHang))
                                this.TenHang = value;
                            else
                                value = this.TenHang.Trim();
                        }
                        //value = this.TenHang.Trim();
                    }
                    //FIXHOATHO


                }
                else 
                {
                    if (this.tkxuat == false)
                    {
                        if (value.Contains("#&"))
                        {
                            string[] temp = value.Split(new string[] { "#&" }, StringSplitOptions.None);
                            if (temp.Length > 0)
                            {
                                var Code = temp.First();
                                var Name = temp.Last();

                                //this.TenHang = temp[1].Trim();
                                //this.MaHangHoa = temp[0].Trim();

                                if (temp.Count() >= 3)
                                {
                                    for (int i = 1; i < temp.Count(); i++)
                                    {
                                        this.TenHang += temp[i].Trim() + "#&";
                                    }
                                    this.TenHang = this.TenHang.Substring(0, this.TenHang.Length - 2);
                                }
                                else
                                {
                                    this.TenHang = Name.ToString().Trim();
                                }
                                this.MaHangHoa = Code.ToString().Trim();
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(this.TenHang))
                                //if (this.TenHang == null)
                                this.TenHang = value;
                            else
                                //minhnd: fix đồng bộ ko có tên hàng
                                value = this.TenHang.Trim();
                        }
                        //value = this.TenHang.Trim();
                    }
                    else
                    {
                        if (value.Contains("#&"))
                        {
                            string[] temp = value.Split(new string[] { "#&" }, StringSplitOptions.None);
                            if (temp.Length > 0)
                            {
                                var Name = temp.First();
                                var XuatXu = temp.Last(); 

                                //this.TenHang = temp[0].Trim();
                                ////this.MaHangHoa = temp[0].Trim();
                                //this.NuocXuatXu = temp[1].Trim();

                                if (temp.Count() >= 3)
                                {
                                    for (int i = 0; i < temp.Count(); i++)
                                    {
                                        this.TenHang += temp[i].Trim() + "#&";
                                    }
                                    this.TenHang = this.TenHang.Substring(0, this.TenHang.Length - 2);
                                }
                                else
                                {
                                    this.TenHang = Name.ToString().Trim();
                                }
                                this.NuocXuatXu = XuatXu.ToString().Trim();
                            }
                        }
                        else
                        {
                            //if (this.TenHang == null)
                            if (string.IsNullOrEmpty(this.TenHang))
                                this.TenHang = value;
                            else
                                value = this.TenHang.Trim();
                        }
                        //value = this.TenHang.Trim();
                    }
                }
                
                
            }
        }

        public static List<KDT_VNACC_HangMauDich> LoadHangMauDichbyID (long[] ID )
        {
            if (ID != null && ID.Length > 0)
            {
                string whereID = "(";
                foreach (long item in ID)
                {
                    whereID += item.ToString();
                    whereID += ",";
                }
                whereID = whereID.Remove(whereID.Length - 1);
                whereID += ")";

                string Sql = string.Format(@"select * from 
                                        (Select * from t_KDT_VNACC_HangMauDich where TKMD_ID in {0}) hmd
                                        Left join (select Master_id, sum(SoTienThueVaThuKhac) AS TongThueThuKhac
                                        from t_KDT_VNACC_HangMauDich_ThueThuKhac 
                                        group by Master_id) as ThuKhac 
                                        ON hmd.ID = ThuKhac.Master_id  ", whereID);
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(Sql);
                IDataReader reader = db.ExecuteReader(dbCommand);
                List<KDT_VNACC_HangMauDich> collection = new List<KDT_VNACC_HangMauDich>();
                while (reader.Read())
                {
                    KDT_VNACC_HangMauDich entity = new KDT_VNACC_HangMauDich();
                    if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaSoHang"))) entity.MaSoHang = reader.GetString(reader.GetOrdinal("MaSoHang"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaQuanLy"))) entity.MaQuanLy = reader.GetString(reader.GetOrdinal("MaQuanLy"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                    if (!reader.IsDBNull(reader.GetOrdinal("ThueSuat"))) entity.ThueSuat = reader.GetDecimal(reader.GetOrdinal("ThueSuat"));
                    if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTuyetDoi"))) entity.ThueSuatTuyetDoi = reader.GetDecimal(reader.GetOrdinal("ThueSuatTuyetDoi"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaDVTTuyetDoi"))) entity.MaDVTTuyetDoi = reader.GetString(reader.GetOrdinal("MaDVTTuyetDoi"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaTTTuyetDoi"))) entity.MaTTTuyetDoi = reader.GetString(reader.GetOrdinal("MaTTTuyetDoi"));
                    if (!reader.IsDBNull(reader.GetOrdinal("NuocXuatXu"))) entity.NuocXuatXu = reader.GetString(reader.GetOrdinal("NuocXuatXu"));
                    if (!reader.IsDBNull(reader.GetOrdinal("SoLuong1"))) entity.SoLuong1 = reader.GetDecimal(reader.GetOrdinal("SoLuong1"));
                    if (!reader.IsDBNull(reader.GetOrdinal("DVTLuong1"))) entity.DVTLuong1 = reader.GetString(reader.GetOrdinal("DVTLuong1"));
                    if (!reader.IsDBNull(reader.GetOrdinal("SoLuong2"))) entity.SoLuong2 = reader.GetDecimal(reader.GetOrdinal("SoLuong2"));
                    if (!reader.IsDBNull(reader.GetOrdinal("DVTLuong2"))) entity.DVTLuong2 = reader.GetString(reader.GetOrdinal("DVTLuong2"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaHoaDon"))) entity.TriGiaHoaDon = reader.GetDecimal(reader.GetOrdinal("TriGiaHoaDon"));
                    if (!reader.IsDBNull(reader.GetOrdinal("DonGiaHoaDon"))) entity.DonGiaHoaDon = reader.GetDecimal(reader.GetOrdinal("DonGiaHoaDon"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaTTDonGia"))) entity.MaTTDonGia = reader.GetString(reader.GetOrdinal("MaTTDonGia"));
                    if (!reader.IsDBNull(reader.GetOrdinal("DVTDonGia"))) entity.DVTDonGia = reader.GetString(reader.GetOrdinal("DVTDonGia"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaBieuThueNK"))) entity.MaBieuThueNK = reader.GetString(reader.GetOrdinal("MaBieuThueNK"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaHanNgach"))) entity.MaHanNgach = reader.GetString(reader.GetOrdinal("MaHanNgach"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaThueNKTheoLuong"))) entity.MaThueNKTheoLuong = reader.GetString(reader.GetOrdinal("MaThueNKTheoLuong"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaMienGiamThue"))) entity.MaMienGiamThue = reader.GetString(reader.GetOrdinal("MaMienGiamThue"));
                    if (!reader.IsDBNull(reader.GetOrdinal("SoTienGiamThue"))) entity.SoTienGiamThue = reader.GetDecimal(reader.GetOrdinal("SoTienGiamThue"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTinhThue"))) entity.TriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TriGiaTinhThue"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaTTTriGiaTinhThue"))) entity.MaTTTriGiaTinhThue = reader.GetString(reader.GetOrdinal("MaTTTriGiaTinhThue"));
                    if (!reader.IsDBNull(reader.GetOrdinal("SoMucKhaiKhoanDC"))) entity.SoMucKhaiKhoanDC = reader.GetString(reader.GetOrdinal("SoMucKhaiKhoanDC"));
                    if (!reader.IsDBNull(reader.GetOrdinal("SoTTDongHangTKTNTX"))) entity.SoTTDongHangTKTNTX = reader.GetString(reader.GetOrdinal("SoTTDongHangTKTNTX"));
                    if (!reader.IsDBNull(reader.GetOrdinal("SoDMMienThue"))) entity.SoDMMienThue = reader.GetString(reader.GetOrdinal("SoDMMienThue"));
                    if (!reader.IsDBNull(reader.GetOrdinal("SoDongDMMienThue"))) entity.SoDongDMMienThue = reader.GetString(reader.GetOrdinal("SoDongDMMienThue"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaMienGiam"))) entity.MaMienGiam = reader.GetString(reader.GetOrdinal("MaMienGiam"));
                    if (!reader.IsDBNull(reader.GetOrdinal("SoTienMienGiam"))) entity.SoTienMienGiam = reader.GetDecimal(reader.GetOrdinal("SoTienMienGiam"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaTTSoTienMienGiam"))) entity.MaTTSoTienMienGiam = reader.GetString(reader.GetOrdinal("MaTTSoTienMienGiam"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaVanBanPhapQuyKhac1"))) entity.MaVanBanPhapQuyKhac1 = reader.GetString(reader.GetOrdinal("MaVanBanPhapQuyKhac1"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaVanBanPhapQuyKhac2"))) entity.MaVanBanPhapQuyKhac2 = reader.GetString(reader.GetOrdinal("MaVanBanPhapQuyKhac2"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaVanBanPhapQuyKhac3"))) entity.MaVanBanPhapQuyKhac3 = reader.GetString(reader.GetOrdinal("MaVanBanPhapQuyKhac3"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaVanBanPhapQuyKhac4"))) entity.MaVanBanPhapQuyKhac4 = reader.GetString(reader.GetOrdinal("MaVanBanPhapQuyKhac4"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaVanBanPhapQuyKhac5"))) entity.MaVanBanPhapQuyKhac5 = reader.GetString(reader.GetOrdinal("MaVanBanPhapQuyKhac5"));
                    if (!reader.IsDBNull(reader.GetOrdinal("SoDong"))) entity.SoDong = reader.GetString(reader.GetOrdinal("SoDong"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiTaiXacNhanGia"))) entity.MaPhanLoaiTaiXacNhanGia = reader.GetString(reader.GetOrdinal("MaPhanLoaiTaiXacNhanGia"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TenNoiXuatXu"))) entity.TenNoiXuatXu = reader.GetString(reader.GetOrdinal("TenNoiXuatXu"));
                    if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTinhThue"))) entity.SoLuongTinhThue = reader.GetDecimal(reader.GetOrdinal("SoLuongTinhThue"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaDVTDanhThue"))) entity.MaDVTDanhThue = reader.GetString(reader.GetOrdinal("MaDVTDanhThue"));
                    if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTinhThue"))) entity.DonGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("DonGiaTinhThue"));
                    if (!reader.IsDBNull(reader.GetOrdinal("DV_SL_TrongDonGiaTinhThue"))) entity.DV_SL_TrongDonGiaTinhThue = reader.GetString(reader.GetOrdinal("DV_SL_TrongDonGiaTinhThue"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaTTDonGiaTinhThue"))) entity.MaTTDonGiaTinhThue = reader.GetString(reader.GetOrdinal("MaTTDonGiaTinhThue"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTinhThueS"))) entity.TriGiaTinhThueS = reader.GetDecimal(reader.GetOrdinal("TriGiaTinhThueS"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaTTTriGiaTinhThueS"))) entity.MaTTTriGiaTinhThueS = reader.GetString(reader.GetOrdinal("MaTTTriGiaTinhThueS"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaTTSoTienMienGiam1"))) entity.MaTTSoTienMienGiam1 = reader.GetString(reader.GetOrdinal("MaTTSoTienMienGiam1"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiThueSuatThue"))) entity.MaPhanLoaiThueSuatThue = reader.GetString(reader.GetOrdinal("MaPhanLoaiThueSuatThue"));
                    if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatThue"))) entity.ThueSuatThue = reader.GetString(reader.GetOrdinal("ThueSuatThue"));
                    if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiThueSuatThue"))) entity.PhanLoaiThueSuatThue = reader.GetString(reader.GetOrdinal("PhanLoaiThueSuatThue"));
                    if (!reader.IsDBNull(reader.GetOrdinal("SoTienThue"))) entity.SoTienThue = reader.GetDecimal(reader.GetOrdinal("SoTienThue"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaTTSoTienThueXuatKhau"))) entity.MaTTSoTienThueXuatKhau = reader.GetString(reader.GetOrdinal("MaTTSoTienThueXuatKhau"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TienLePhi_DonGia"))) entity.TienLePhi_DonGia = reader.GetString(reader.GetOrdinal("TienLePhi_DonGia"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TienBaoHiem_DonGia"))) entity.TienBaoHiem_DonGia = reader.GetString(reader.GetOrdinal("TienBaoHiem_DonGia"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TienLePhi_SoLuong"))) entity.TienLePhi_SoLuong = reader.GetDecimal(reader.GetOrdinal("TienLePhi_SoLuong"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TienLePhi_MaDVSoLuong"))) entity.TienLePhi_MaDVSoLuong = reader.GetString(reader.GetOrdinal("TienLePhi_MaDVSoLuong"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TienBaoHiem_SoLuong"))) entity.TienBaoHiem_SoLuong = reader.GetDecimal(reader.GetOrdinal("TienBaoHiem_SoLuong"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TienBaoHiem_MaDVSoLuong"))) entity.TienBaoHiem_MaDVSoLuong = reader.GetString(reader.GetOrdinal("TienBaoHiem_MaDVSoLuong"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TienLePhi_KhoanTien"))) entity.TienLePhi_KhoanTien = reader.GetDecimal(reader.GetOrdinal("TienLePhi_KhoanTien"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TienBaoHiem_KhoanTien"))) entity.TienBaoHiem_KhoanTien = reader.GetDecimal(reader.GetOrdinal("TienBaoHiem_KhoanTien"));
                    if (!reader.IsDBNull(reader.GetOrdinal("DieuKhoanMienGiam"))) entity.DieuKhoanMienGiam = reader.GetString(reader.GetOrdinal("DieuKhoanMienGiam"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MaHangHoa"))) entity.MaHangHoa = reader.GetString(reader.GetOrdinal("MaHangHoa"));
                    if (!reader.IsDBNull(reader.GetOrdinal("Templ_1"))) entity.Templ_1 = reader.GetString(reader.GetOrdinal("Templ_1"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TongThueThuKhac"))) entity.TongSoTienThuKhac = reader.GetDecimal(reader.GetOrdinal("TongThueThuKhac"));
                    collection.Add(entity);
                }
                reader.Close();
                return collection;
            }
            else
                return new List<KDT_VNACC_HangMauDich>();
        }




        public static int DeleteDynamic(string whereCondition, SqlTransaction transaction)
        {
            
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public bool InsertFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                     this.ID = this.Insert(transaction);
                     foreach (KDT_VNACC_HangMauDich_ThueThuKhac item in this.ThueThuKhacCollection)
                     {
                         if (item.ID == 0)
                         {
                             item.Master_id = this.ID;
                             item.ID = item.Insert();
                         }
                         else
                         {
                             item.Master_id = this.ID;
                             item.Update();
                         }
                     }
                     ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public void loadThueVaThuKhac()
        {
            List<KDT_VNACC_HangMauDich_ThueThuKhac> listThueThuKhac = new List<KDT_VNACC_HangMauDich_ThueThuKhac>();
            listThueThuKhac = KDT_VNACC_HangMauDich_ThueThuKhac.SelectCollectionDynamic("Master_id=" + this.ID, "ID");
            foreach (KDT_VNACC_HangMauDich_ThueThuKhac thue in listThueThuKhac)
            {
                this.ThueThuKhacCollection.Add(thue);
            }
        }
	}	
}