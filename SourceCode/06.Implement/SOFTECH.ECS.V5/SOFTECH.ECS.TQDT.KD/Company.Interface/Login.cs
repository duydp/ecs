﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.QuanTri;
//
using System.Collections;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Tab;
using System.Globalization;
using Janus.Windows.GridEX;
using Janus.Windows.ExplorerBar;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Resources;
using Janus.Windows.UI.Dock;
using System.Collections;
using System.Runtime.InteropServices;
using Company.KDT.SHARE.VNACCS;


namespace Company.Interface
{
    public partial class Login : Form
    {

        protected Company.Controls.MessageBoxControl _MsgBox;
        public Login()
        {
            InitializeComponent();
        }
        public string setText(string vnText, string enText)
        {
            if (Properties.Settings.Default.NgonNgu == "1")
            {
                return enText.Trim();
            }
            else
            {
                return vnText.Trim();
            }
        }
        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new Company.Controls.MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog(this);
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            try
            {
                MainForm.EcsQuanTri = ECSPrincipal.ValidateLogin(txtUser.Text.Trim(), txtMatKhau.Text.Trim());
                if (MainForm.EcsQuanTri == null)
                {
                    ShowMessage(setText("Đăng nhập không thành công.", "Login fail!"), false);
                    MainForm.isLoginSuccess = false;
                }
                else
                {
                    if (MainForm.isUseSettingVNACC)
                    {
                        //TODO: Kiem tra bang USER: da co bo sung cac field va store cho VNACC?
                        Company.KDT.SHARE.Components.SQL.CheckUserTableIsHaveVNACCS(GlobalSettings.SERVER_NAME, GlobalSettings.DATABASE_NAME, GlobalSettings.USER, GlobalSettings.PASS);

                        //TODO: Cap nhat IP, HostName cua PC dang nhap vao bang USER
                        Company.QuanTri.User userVNACC = Company.QuanTri.User.Load_VNACC(((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.ID);
                        userVNACC.HostName = System.Environment.MachineName;
                        userVNACC.IP = Globals.GetLocalIPAddress();
                        userVNACC.Update_VNACC();

                        //TODO: Chay store cap nhat trang thai cua Terminal.
                        User.CheckAndUpdateTerminalStatus();

                        //TODO: Kiem tra Terminal co cung ID co dang su dung khong truoc khi dang nhap nguoi dung. Chi cho phep 1 ternimal ID duoc phep khai bao.                    
                        if (Company.QuanTri.User.CheckVNACCTerminalCountOnline(userVNACC.VNACC_TerminalID) > 1
                            || Company.QuanTri.User.CheckVNACCTerminalCountOnline(userVNACC.VNACC_TerminalID, userVNACC.USER_NAME) > 1)
                        {
                            Helper.Controls.MessageBoxControlV.ShowMessage("Phát hiện có 1 Terminal '" + userVNACC.VNACC_TerminalID + "' khác đang kết nối khai báo. Bạn vui lòng thiết lập lại thông tin cho Terminal.", false);

                            userVNACC.VNACC_TerminalID = string.Empty;
                            userVNACC.VNACC_TerminalPass = string.Empty;
                            userVNACC.VNACC_UserID = string.Empty;
                            userVNACC.VNACC_UserCode = string.Empty;
                            userVNACC.VNACC_UserPass = string.Empty;
                            userVNACC.Update_VNACC();
                        }

                        //Cap nhat bien toan cuc VNACC cho chuong trinh
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalID = userVNACC.VNACC_TerminalID;
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalAccessKey = userVNACC.VNACC_TerminalPass;
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_ID = userVNACC.VNACC_UserID;
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Ma = userVNACC.VNACC_UserCode;
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Pass = userVNACC.VNACC_UserPass;
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.Url = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("VNACCAddress", "");
                    }

                    MainForm.isLoginSuccess = true;

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Không kết nối được cơ sở dữ liệu : " + ex.Message);
                linkLabel1_LinkClicked(null, null);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog(this);
        }
        private void Login_Load(object sender, EventArgs e)
        {
            txtUser.Text = txtMatKhau.Text = string.Empty;
            txtUser.Focus();
            try
            {
                uiButton3.Text = setText("Đăng nhập", "Login");
                uiButton2.Text = setText("Thoát", "Exit");
                linkLabel1.Text = setText("Cấu hình thông số kết nối", "Configuration of connected parameters");
                if (Properties.Settings.Default.NgonNgu == "1")
                {
                    this.BackgroundImage = System.Drawing.Image.FromFile("LOGIN-KD-eng.png");
                }
                else
                {
                    this.BackgroundImage = System.Drawing.Image.FromFile("LOGIN-KD.png");
                }

                // this.BackgroundImageLayout = ImageLayout.Stretch;
            }
            catch { }
            foreach (Control item in this.Controls)
            {
                item.KeyDown += Help_KeyDown;
                OnKeyDown(item);
            }
        }
        #region Help
        private void OnKeyDown(Control ctr)
        {
            if (ctr.Controls.Count > 0)
                foreach (Control item in ctr.Controls)
                {
                    item.KeyDown += Help_KeyDown;
                    OnKeyDown(item);
                }
        }
        // This overload is for passing a single uint value as the dwData parameter.
        [DllImport("hhctrl.ocx", CharSet = CharSet.Unicode, EntryPoint = "HtmlHelpW")]
        protected static extern int HtmlHelp(
            int caller,
            String file,
            uint command,
            uint data
            );
        public static void ShowHelpTopic(string topic)
        {
            HtmlHelp(0, AppDomain.CurrentDomain.BaseDirectory + "Helps\\Help.chm" + "::/" + topic.ToLower() + ".html", HH_DISPLAY_TOC, 0);
        }
        protected const int HH_DISPLAY_TOC = 0x0001;  // [Use HtmlHelp_DisplayTOC()]

        void Help_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    {
                        ShowHelpTopic((this.Tag != null && this.Tag.ToString() != "") ? this.Tag.ToString() : this.Name.ToString());
                    }
                    break;

            }
        }
        #endregion

    }
}