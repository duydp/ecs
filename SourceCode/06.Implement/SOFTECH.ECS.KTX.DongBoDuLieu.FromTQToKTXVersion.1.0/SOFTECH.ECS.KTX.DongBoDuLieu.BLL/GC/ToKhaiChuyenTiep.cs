using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using Company.KDT.SHARE.Components.Utils;

namespace DongBoDuLieu.BLL.GC
{
    public class TKCT
    {


        public static DataSet GetDataSetALLToKhaiChuyenTiep(Company.GC.BLL.KDT.GC.HopDong HD, string matKhau)
        {
            
            StringBuilder queryTK = new StringBuilder();
            queryTK.Append("SELECT * ");
            queryTK.Append("FROM DCTGC TK ");
            queryTK.Append("WHERE TK.Ma_HQ = '" + HD.MaHaiQuan + "' AND DVGC = '" + HD.MaDoanhNghiep + "' and So_HD='" + HD.SoHopDong + "'");
            queryTK.Append("AND So_HD = '" + HD.SoHopDong + "'");
            DataSet dsToKhai = WebServiceDB.getDataBySQL(queryTK.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsToKhai;
            
            //TODO: ToKhaiChuyenTiep
            /*
            StringBuilder queryTK = new StringBuilder();
            queryTK.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            queryTK.Append("SELECT * ");
            queryTK.Append("FROM DCTGC TK ");
            queryTK.Append("WHERE TK.Ma_HQ = ''" + HD.MaHaiQuan + "'' AND DVGC = ''" + HD.MaDoanhNghiep + "'' and So_HD=''" + HD.SoHopDong + "''");
            queryTK.Append(" ') AS a;");
            DataSet dsToKhai = WebServiceDB.getDataBySQL(queryTK.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsToKhai;
            */
        }


        public static DataSet GetDataSetToKhaiChuyenTiepByNgay(Company.GC.BLL.KDT.GC.HopDong HD, DateTime NgayBatDau, DateTime NgayKetThuc, string matKhau)
        {
            StringBuilder queryTK = new StringBuilder();
            queryTK.Append("SELECT * ");
            queryTK.Append("FROM DCTGC TK ");
            queryTK.Append("WHERE TK.Ma_HQ = '" + HD.MaHaiQuan + "' AND DVGC = '" + HD.MaDoanhNghiep + "' and So_HD='" + HD.SoHopDong + "'");
            queryTK.Append(" AND So_HD = '" + HD.SoHopDong + "'");
            queryTK.Append(" AND Ngay_CT >= '" + NgayBatDau.ToString("MM/dd/yyyy") + "'");
            queryTK.Append(" AND Ngay_CT <= '" + NgayKetThuc.ToString("MM/dd/yyyy") + "'");
            DataSet dsToKhai = WebServiceDB.getDataBySQL(queryTK.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsToKhai;
        }

        public static void InsertGhiDeALLToKhai(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsToKhai, DataSet dsHang)
        {
            try
            {
                Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection();
                foreach (DataRow row in dsToKhai.Tables[0].Rows)
                {
                    #region ToKhai
                    Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep();
                    tkct.MaDoanhNghiep = HD.MaDoanhNghiep;
                    tkct.MaLoaiHinh = row["Ma_CT"].ToString().Trim();
                    tkct.MaHaiQuanTiepNhan = HD.MaHaiQuan;
                    tkct.NgayDangKy = Convert.ToDateTime(row["Ngay_CT"]);
                    tkct.SoToKhai = Convert.ToInt64(row["So_CT"].ToString());
                    string sohopdong = "";
                    sohopdong = row["So_HD"].ToString();
                    tkct.SoHopDongDV = row["So_HD"].ToString();
                    tkct.SoHDKH = row["So_HDCh"].ToString();
                    if (tkct.MaLoaiHinh.EndsWith("N"))
                    {
                        tkct.DiaDiemXepHang = FontConverter.TCVN2Unicode(row["NguoiHQNhan"].ToString());

                    }
                    else
                    {
                        tkct.DiaDiemXepHang = FontConverter.TCVN2Unicode(row["NguoiHQGiao"].ToString());
                    }
                    tkct.ChungTu = FontConverter.TCVN2Unicode(row["CTKT"].ToString());
                    if (row["LePhiHQ"].ToString() != "")
                        tkct.LePhiHQ = Convert.ToDecimal(row["LePhiHQ"].ToString());
                    tkct.MaHaiQuanKH = row["Ma_HQKH"].ToString();
                    tkct.MaKhachHang = row["Ma_DoiTac"].ToString();

                    tkct.NgayHDDV = Convert.ToDateTime(row["Ngay_Ky"].ToString());
                    if (row["Ngay_KyCh"] != DBNull.Value)
                        tkct.NgayHDKH = Convert.ToDateTime(row["Ngay_KyCh"].ToString());
                    tkct.NgayHetHanHDDV = HD.NgayHetHan;
                    tkct.NguoiChiDinhDV = FontConverter.TCVN2Unicode(row["NguoiNhan"].ToString());
                    tkct.NguoiChiDinhKH = FontConverter.TCVN2Unicode(row["NguoiGiao"].ToString());
                    tkct.NgayTiepNhan = tkct.NgayDangKy;
                    tkct.NguyenTe_ID = row["Ma_NT"].ToString();
                    tkct.SoTiepNhan = 0;
                    tkct.TrangThaiXuLy = 1;
                    tkct.IDHopDong = HD.ID;
                    if (row["TyGiaVND"].ToString() != "")
                        tkct.TyGiaVND = Convert.ToDecimal(row["TyGiaVND"].ToString());

                    #endregion ToKhai

                    #region Hang
                    DataRow[] rowHang = dsHang.Tables[0].Select("So_CT=" + tkct.SoToKhai + " and Ma_CT='" + tkct.MaLoaiHinh + "' and Ma_HQ='" + tkct.MaHaiQuanTiepNhan + "' and Nam_CT=" + tkct.NgayDangKy.Year);
                    if (rowHang != null && rowHang.Length > 0)
                    {
                        tkct.HCTCollection = new Company.GC.BLL.KDT.GC.HangChuyenTiepCollection();
                        foreach (DataRow rowIndex in rowHang)
                        {
                            Company.GC.BLL.KDT.GC.HangChuyenTiep hangCT = new Company.GC.BLL.KDT.GC.HangChuyenTiep();
                            if (rowIndex["DonGia"] != DBNull.Value)
                                hangCT.DonGia = Convert.ToDecimal(rowIndex["DonGia"]);
                            hangCT.ID_DVT = rowIndex["Ma_DVT"].ToString();
                            hangCT.ID_NuocXX = rowIndex["NuocXX"].ToString();
                            hangCT.MaHang = rowIndex["P_Code"].ToString().Substring(1).Trim();
                            hangCT.MaHS = rowIndex["HS_Code"].ToString().Trim();
                            hangCT.SoLuong = Convert.ToDecimal(rowIndex["SoLuong"]);
                            hangCT.TenHang = FontConverter.TCVN2Unicode(rowIndex["TenHang"].ToString().Trim());
                            if (rowIndex["TriGia"] != DBNull.Value)
                                hangCT.TriGia = Convert.ToDecimal(rowIndex["TriGia"]);
                            tkct.HCTCollection.Add(hangCT);
                        }
                    }
                    #endregion Hang


                    TKCTCollection.Add(tkct);
                }
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
                using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCTDelete = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep();
                        TKCTDelete.IDHopDong = HD.ID;
                        TKCTDelete.DeleteBy_IDHopDong(transaction);
                        foreach (Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT in TKCTCollection)
                        {
                            TKCT.IDHopDong = HD.ID;
                            TKCT.InsertTransactionKTX(transaction);

                            int i = 0;
                            foreach (Company.GC.BLL.KDT.GC.HangChuyenTiep HCT in TKCT.HCTCollection)
                            {
                                HCT.SoThuTuHang = ++i;
                                HCT.Master_ID = TKCT.ID;
                                HCT.InsertTransactionKTX(transaction);

                            }
                        }


                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void InsertGhiDuLieuToKhaiMoi(Company.GC.BLL.KDT.GC.HopDong HD, DataSet dsToKhai, DataSet dsHang)
        {
            Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection TKCTCollection = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiepCollection();
            foreach (DataRow row in dsToKhai.Tables[0].Rows)
            {
                #region ToKhai
                Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep();
                tkct.MaDoanhNghiep = HD.MaDoanhNghiep;
                tkct.MaLoaiHinh = row["Ma_CT"].ToString().Trim();
                tkct.MaHaiQuanTiepNhan = HD.MaHaiQuan;
                tkct.NgayDangKy = Convert.ToDateTime(row["Ngay_CT"]);
                tkct.SoToKhai = Convert.ToInt64(row["So_CT"].ToString());
                tkct.SetDabaseMoi("MSSQL");
                if (tkct.LoadBySoToKhai())
                {
                    continue;
                }
                string sohopdong = "";
                sohopdong = row["So_HD"].ToString();
                tkct.SoHopDongDV = row["So_HD"].ToString();
                tkct.SoHDKH = row["So_HDCh"].ToString();
                if (tkct.MaLoaiHinh.EndsWith("N"))
                {
                    tkct.DiaDiemXepHang = FontConverter.TCVN2Unicode(row["NguoiHQNhan"].ToString());

                }
                else
                {
                    tkct.DiaDiemXepHang = FontConverter.TCVN2Unicode(row["NguoiHQGiao"].ToString());
                }
                tkct.ChungTu = FontConverter.TCVN2Unicode(row["CTKT"].ToString());
                if (row["LePhiHQ"].ToString() != "")
                    tkct.LePhiHQ = Convert.ToDecimal(row["LePhiHQ"].ToString());
                tkct.MaHaiQuanKH = row["Ma_HQKH"].ToString();
                tkct.MaKhachHang = row["Ma_DoiTac"].ToString();

                tkct.NgayHDDV = Convert.ToDateTime(row["Ngay_Ky"].ToString());
                tkct.NgayHDKH = Convert.ToDateTime(row["Ngay_KyCh"].ToString());
                tkct.NgayHetHanHDDV = HD.NgayHetHan;
                tkct.NguoiChiDinhDV = FontConverter.TCVN2Unicode(row["NguoiNhan"].ToString());
                tkct.NguoiChiDinhKH = FontConverter.TCVN2Unicode(row["NguoiGiao"].ToString());
                tkct.NgayTiepNhan = tkct.NgayDangKy;
                tkct.NguyenTe_ID = row["Ma_NT"].ToString();
                tkct.SoTiepNhan = 0;
                tkct.TrangThaiXuLy = 1;
                tkct.IDHopDong = HD.ID;
                if (row["TyGiaVND"].ToString() != "")
                    tkct.TyGiaVND = Convert.ToDecimal(row["TyGiaVND"].ToString());

                #endregion ToKhai

                #region Hang
                DataRow[] rowHang = dsHang.Tables[0].Select("So_CT=" + tkct.SoToKhai + " and Ma_CT='" + tkct.MaLoaiHinh + "' and Ma_HQ='" + tkct.MaHaiQuanTiepNhan + "' and Nam_CT=" + tkct.NgayDangKy.Year);
                if (rowHang != null && rowHang.Length > 0)
                {
                    tkct.HCTCollection = new Company.GC.BLL.KDT.GC.HangChuyenTiepCollection();
                    foreach (DataRow rowIndex in rowHang)
                    {
                        Company.GC.BLL.KDT.GC.HangChuyenTiep hangCT = new Company.GC.BLL.KDT.GC.HangChuyenTiep();
                        hangCT.DonGia = Convert.ToDecimal(rowIndex["DonGia"]);
                        hangCT.ID_DVT = rowIndex["Ma_DVT"].ToString();
                        hangCT.ID_NuocXX = rowIndex["NuocXX"].ToString();
                        hangCT.MaHang = rowIndex["P_Code"].ToString().Substring(1).Trim();
                        hangCT.MaHS = rowIndex["HS_Code"].ToString().Trim();
                        hangCT.SoLuong = Convert.ToDecimal(rowIndex["SoLuong"]);
                        hangCT.TenHang = FontConverter.TCVN2Unicode(rowIndex["TenHang"].ToString().Trim());
                        hangCT.TriGia = Convert.ToDecimal(rowIndex["TriGia"]);
                        tkct.HCTCollection.Add(hangCT);
                    }
                }
                #endregion Hang


                TKCTCollection.Add(tkct);
            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCTDelete = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep();
                    TKCTDelete.IDHopDong = HD.ID;
                    TKCTDelete.DeleteBy_IDHopDong(transaction);
                    foreach (Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT in TKCTCollection)
                    {
                        TKCT.IDHopDong = HD.ID;
                        TKCT.InsertTransaction(transaction);

                        int i = 0;
                        foreach (Company.GC.BLL.KDT.GC.HangChuyenTiep HCT in TKCT.HCTCollection)
                        {
                            HCT.SoThuTuHang = ++i;
                            HCT.Master_ID = TKCT.ID;
                            HCT.InsertTransaction(transaction);

                        }
                    }


                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
    }
}
