﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC ;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{
    
    public partial class ReportViewGCCTTQForm : BaseForm
    {
        public ToKhaiGCCTA4Nhap ToKhaiChinhReportNhap = new ToKhaiGCCTA4Nhap();
        //public ToKhaiGCCTA4 ToKhaiChinhReport = new ToKhaiGCCTA4();
        public PhuLucToKhaiCTTQ PhuLucReport = new PhuLucToKhaiCTTQ();
        //public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();

        public XRControl Cell = new XRControl();
        public ReportViewGCCTTQForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {

             cboToKhai.SelectedIndex = 0;
             if (this.TKCT.HCTCollection.Count > 3)
             {
                 int count = (this.TKCT.HCTCollection.Count - 1) / 9 + 1;
                 for (int i = 0; i < count; i++)
                     this.AddItemComboBox();
             }
            this.ToKhaiChinhReportNhap.TKCT = this.TKCT;
            this.ToKhaiChinhReportNhap.BindReport();
            printControl1.PrintingSystem = ToKhaiChinhReportNhap.PrintingSystem;
            this.ToKhaiChinhReportNhap.CreateDocument();
            
            
        }
        public void AddItemComboBox()
        {
            cboToKhai.Items.Add("Phụ lục " + cboToKhai.Items.Count, cboToKhai.Items.Count);
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                printControl1.PrintingSystem = ToKhaiChinhReportNhap.PrintingSystem;
                this.ToKhaiChinhReportNhap.report = this;
                this.ToKhaiChinhReportNhap.CreateDocument();

            }

            else
            {
                HangChuyenTiepCollection HCTReportCollection = new HangChuyenTiepCollection();
                int begin = (cboToKhai.SelectedIndex - 1) * 9;
                int end = cboToKhai.SelectedIndex * 9;
                if (end > this.TKCT.HCTCollection.Count) end = this.TKCT.HCTCollection.Count;
                for (int i = begin; i < end; i++)
                    HCTReportCollection.Add(this.TKCT.HCTCollection[i]);
                this.PhuLucReport = new PhuLucToKhaiCTTQ();
                this.PhuLucReport.report = this;
                //this.PhuLucReport.MaLoaiHinh = this.TKMD.MaLoaiHinh;
                this.PhuLucReport.SoToKhai = this.TKCT.SoToKhai;
                if (this.TKCT.NgayDangKy != new DateTime(1900, 1, 1))
                    this.PhuLucReport.NgayDangKy = this.TKCT.NgayDangKy;
                this.PhuLucReport.HCTCollection = HCTReportCollection;
                //int spl = cboToKhai.SelectedIndex.ToString();

                this.PhuLucReport.BindReport(cboToKhai.SelectedIndex.ToString());//chkInMaHang.Checked
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
                 
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {

                this.ToKhaiChinhReportNhap.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.ToKhaiChinhReportNhap.CreateDocument();
            }
            
            else
            {
                
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.PhuLucReport.CreateDocument();
                 
            }
             
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //switch (cboExport.SelectedValue.ToString())
            //{
            //    case "pdf":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportPdf, new object[] { true });
            //        break;
            //    case "excel":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            //        break;

            //}
        }


        private void uiButton3_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReportNhap.setNhomHang(this.Cell, txtTenNhomHang.Text);
                this.ToKhaiChinhReportNhap.CreateDocument();
            }
                
            else
            {
                this.PhuLucReport.setNhomHang(this.Cell, txtTenNhomHang.Text);
                this.PhuLucReport.CreateDocument();
            }
                 
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReportNhap.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
               // this.ToKhaiChinhReportNhap.setVisibleImage(true);
                this.ToKhaiChinhReportNhap.CreateDocument();
            }
                /*
            else
            {
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                this.PhuLucReport.setVisibleImage(true);
                this.PhuLucReport.CreateDocument();
            }
                 */ 
        }

        private void chkInMaHang_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReportNhap.inMaHang = chkInMaHang.Checked;
                this.ToKhaiChinhReportNhap.BindReport();
                printControl1.PrintingSystem = ToKhaiChinhReportNhap.PrintingSystem;
                this.ToKhaiChinhReportNhap.CreateDocument();

            }
                
            else
            {
                this.PhuLucReport.inMaHang = chkInMaHang.Checked;
                this.PhuLucReport.BindReport(cboToKhai.SelectedIndex.ToString());
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
               
        }

        private void chkHinhNen_CheckedChanged(object sender, EventArgs e)
        {

        }

 

        private void chkInBanLuuHaiQuan_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReportNhap.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.ToKhaiChinhReportNhap.BindReport();//chkInMaHang.Checked
                printControl1.PrintingSystem = ToKhaiChinhReportNhap.PrintingSystem;
                this.ToKhaiChinhReportNhap.CreateDocument();

            }
                /*
            else
            {
                //this.PhuLucReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.PhuLucReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
                 */ 
        }
    }
}