﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using SignRemote;
using System.Threading;
using Company.Interface;
namespace ServerRemote
{
    public partial class frmLogin : Company.Interface.BaseForm
    {
        bool _isAbort = false;
        public frmLogin()
        {
            InitializeComponent();
        }
        public frmLogin(bool isAuto)
            : this()
        {
            ThreadPool.QueueUserWorkItem(DoLoginAuto);
        }
        private void frmLogin_Load(object sender, EventArgs e)
        {
            txtUserName.Focus();
            if (Globals.IsRememberLogin)
            {
                txtUserName.Text = Globals.UserNameLogin;
                txtPassword.Text = Globals.PasswordLogin;
            }
            chIsRemember.Checked = Globals.IsRememberLogin;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            error.Clear();
            if (string.IsNullOrEmpty(txtUserName.Text))
            {
                error.SetError(txtUserName, "Tên đăng nhập không được để trống");
                txtUserName.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                error.SetError(txtPassword, "Mật khẩu không được để trống");
                txtPassword.Focus();
                return;
            }

            try
            {
                if (!Globals.UserNameLogin.Equals(txtUserName.Text.Trim()))
                    Globals.UserNameLogin = txtUserName.Text.Trim();
                if (!Globals.PasswordLogin.Equals(txtPassword.Text.Trim()))
                    Globals.PasswordLogin = Company.KDT.SHARE.Components.Helpers.GetMD5Value(txtPassword.Text.Trim());
                Globals.IsRememberLogin = chIsRemember.Checked;

                if (!Globals.IsSignOnLan)
                {
                    ISignMessage service = WebService.SignMessage();
                    bool valid = service.IsExist(Globals.UserNameLogin, Globals.PasswordLogin);
                    if (!valid)
                    {
                        MessageBoxControl.ShowMessage("Tên đăng nhập hoặc mật khẩu không chính xác");
                        //MessageBox.Show("Tên đăng nhập hoặc mật khẩu không chính xác", "Đăng nhập thất bại", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else
                {
                }
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsRememberLogin", Globals.IsRememberLogin == true ? "true" : "false");
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("UserName", Globals.UserNameLogin);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Password", Helpers.EncryptString(Globals.PasswordLogin, "KEYWORD"));

                DialogResult = DialogResult.OK;
                Application.ExitThread();
                Application.Restart();
            }
            catch (Exception ex)
            {
                MessageBoxControl.ShowMessage(ex.Message);
                //MessageBox.Show(ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _isAbort = true;
            Application.Exit();
        }
        private void DoLoginAuto(object o)
        {
            bool isLogin = false;

            while (!isLogin && !_isAbort)
            {
                try
                {
                    ISignMessage service = WebService.SignMessage();
                    bool valid = service.IsExist(Globals.UserNameLogin, Globals.PasswordLogin);
                    if (!valid)
                    {
                        MessageBoxControl.ShowMessage("Tên đăng nhập hoặc mật khẩu không chính xác");
                        //MessageBox.Show("Tên đăng nhập hoặc mật khẩu không chính xác", "Đăng nhập thất bại", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        _isAbort = true;
                    }
                    isLogin = true;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);

                }
                Thread.Sleep(new TimeSpan(0, 0, 30));
            }
            if (isLogin)
                DialogResult = DialogResult.OK;
            else DialogResult = DialogResult.Cancel;
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog();
        }
    }
}
