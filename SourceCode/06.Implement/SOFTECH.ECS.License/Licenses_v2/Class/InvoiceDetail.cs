﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Activation.Server
{
    public partial class InvoiceDetail
    {
        //---------------------------------------------------------------------------------------------
        public static DataSet SelectBy_InvoiceID_Extend(string invoiceID)
        {
            const string spName = "[dbo].[p_LICENSE_InvoiceDetail_SelectBy_InvoiceID_Extend]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@InvoiceID", SqlDbType.VarChar, invoiceID);

            return db.ExecuteDataSet(dbCommand);
        }
    }
}
