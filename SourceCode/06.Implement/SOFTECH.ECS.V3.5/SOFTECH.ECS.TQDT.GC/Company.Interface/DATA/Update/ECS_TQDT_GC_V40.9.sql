GO
IF OBJECT_ID('p_KDT_ToKhaiMauDich_SelectLuongNPLTrongTKXuatByLSX') IS NOT NULL
DROP PROC p_KDT_ToKhaiMauDich_SelectLuongNPLTrongTKXuatByLSX
GO
		 -----------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectLuongNPLTrongTKXuatByLSX]    
-- Database: ECS_TQDT_GC_V4_TAT    
-- Lượng NPL Quy đổi từ Tờ khai xuất    
-----------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectLuongNPLTrongTKXuatByLSX]    
 @HopDong_ID BIGINT,    
 @MaNguyenPhuLieu NVARCHAR(500)    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL NVARCHAR(MAX)    
    
SET @SQL =     
'SELECT *    
 FROM   t_View_LuongNPLTrongTKXuatByLSX    
 WHERE  IDHopDong = '''+ CAST(@HopDong_ID AS NVARCHAR(12))  + '''    
        AND MaNguyenPhuLieu = ''' + @MaNguyenPhuLieu + ''''    
    
EXEC sp_executesql @SQL   
GO

IF OBJECT_ID('t_View_LuongNPLTrongTKXuatByLSX') IS NOT NULL
DROP VIEW t_View_LuongNPLTrongTKXuatByLSX
GO

  CREATE VIEW [dbo].[t_View_LuongNPLTrongTKXuatByLSX]          
AS          
    SELECT  CASE WHEN t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE '%V%'          
                 THEN t_KDT_ToKhaiMauDich.LoaiVanDon          
                 ELSE CONVERT(VARCHAR(12), t_KDT_ToKhaiMauDich.SoToKhai)          
            END AS SoToKhaiVNACCS ,    
            dbo.t_KDT_ToKhaiMauDich.TrangThaiXuLy,          
            dbo.t_KDT_ToKhaiMauDich.MaHaiQuan ,          
            dbo.t_KDT_ToKhaiMauDich.SoToKhai ,          
            dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh ,          
   dbo.t_KDT_ToKhaiMauDich.NgayDangKy ,          
            dbo.t_KDT_HangMauDich.MaPhu AS MaSP ,          
            dbo.t_KDT_HangMauDich.TenHang ,          
      dbo.t_KDT_HangMauDich.DVT_ID,          
            dbo.t_KDT_HangMauDich.SoLuong ,          
            dbo.t_GC_DinhMuc.MaNguyenPhuLieu ,          
            dbo.t_GC_NguyenPhuLieu.Ten ,          
            dbo.t_KDT_HangMauDich.SoLuong * ( dbo.t_GC_DinhMuc.DinhMucSuDung          
                                              / 100          
                                              * dbo.t_GC_DinhMuc.TyLeHaoHut          
                                              + dbo.t_GC_DinhMuc.DinhMucSuDung ) AS LuongNPL ,        
   TriGiaTT,        
   DonGiaTT,          
            dbo.t_KDT_ToKhaiMauDich.MaDoanhNghiep ,          
            dbo.t_KDT_ToKhaiMauDich.IDHopDong ,          
            dbo.t_KDT_ToKhaiMauDich.ID ,          
            dbo.t_GC_DinhMuc.DinhMucSuDung ,          
            dbo.t_GC_DinhMuc.TyLeHaoHut ,          
            YEAR(dbo.t_KDT_ToKhaiMauDich.NgayDangKy) AS NamDK ,  
   dbo.t_GC_DinhMuc.LenhSanXuat_ID         
    FROM    dbo.t_KDT_ToKhaiMauDich          
            INNER JOIN dbo.t_KDT_HangMauDich ON dbo.t_KDT_ToKhaiMauDich.ID = dbo.t_KDT_HangMauDich.TKMD_ID          
            INNER JOIN dbo.t_GC_DinhMuc ON dbo.t_GC_DinhMuc.MaSanPham = dbo.t_KDT_HangMauDich.MaPhu          
                                           AND dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_KDT_ToKhaiMauDich.IDHopDong          
            INNER JOIN dbo.t_GC_NguyenPhuLieu ON dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_GC_NguyenPhuLieu.HopDong_ID          
                                                 AND dbo.t_GC_DinhMuc.MaNguyenPhuLieu = dbo.t_GC_NguyenPhuLieu.Ma     
   INNER JOIN dbo.t_KDT_LenhSanXuat ON t_KDT_LenhSanXuat.ID = t_GC_DinhMuc.LenhSanXuat_ID AND t_GC_DinhMuc.HopDong_ID = t_KDT_LenhSanXuat.HopDong_ID AND NgayDangKy BETWEEN TuNgay AND DenNgay                    
    WHERE   ( dbo.t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S' AND t_KDT_ToKhaiMauDich.TrangThaiXuLy NOT IN (10,11) AND t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XV%'  )    
					   
GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '40.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('40.9',GETDATE(), N'CẬP NHẬT PROCDEDURE XEM ĐỊNH MỨC ')
END