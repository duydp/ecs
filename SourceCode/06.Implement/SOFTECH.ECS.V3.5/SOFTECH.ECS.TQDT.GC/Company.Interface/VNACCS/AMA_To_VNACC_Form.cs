﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


using Janus.Windows.GridEX;
using Infragistics.Excel;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using Company.KDT.SHARE.VNACCS;
#if SXXK_V4
using Company.BLL.SXXK;
#endif
namespace Company.Interface.SXXK
{
    public partial class AMA_To_VNACC_Form : BaseForm
    {
        public DataSet ds = new DataSet();
        public AMA_To_VNACC_Form()
        {
            InitializeComponent();
        }
        
        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            //if(e.Row.RowType == RowType.Record)    
            //    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        private void XuatEX() 
        {
            //return;
            Workbook wb = new Workbook();
            Worksheet ws = wb.Worksheets.Add("ToKhaiAMA");
            ws.Columns[0].Width = 5000;
            ws.Columns[1].Width = 2000;
            ws.Columns[2].Width = 5000;
            ws.Columns[3].Width = 5000;
            ws.Columns[4].Width = 5000;
            ws.Columns[5].Width = 3000;
            ws.Columns[6].Width = 2000;
            ws.Columns[7].Width = 10000;
            ws.Columns[8].Width = 5000;
            ws.Columns[9].Width = 5000;
            ws.Columns[10].Width = 10000;
            ws.Columns[11].Width = 5000;
            ws.Columns[12].Width = 5000;
            WorksheetRowCollection wsrc = ws.Rows;
            WorksheetRow wsr0 = ws.Rows[0];
            wsr0.Cells[0].Value = "Số TK AMA";
            wsr0.Cells[1].Value = "TKMD_ID";
            wsr0.Cells[2].Value = "Số Tờ Khai";
            wsr0.Cells[3].Value = "Ngày Đăng Ký";
            wsr0.Cells[4].Value = "Mã Loại Hình";
            wsr0.Cells[5].Value = "Trạng Thái";
            wsr0.Cells[6].Value = "STT";
            wsr0.Cells[7].Value = "Mô tả hàng trước";
            wsr0.Cells[8].Value = "Số lượng hàng trước";
            wsr0.Cells[9].Value = "Trị giá tính thuế trước";
            wsr0.Cells[10].Value = "Mô tả hàng sau";
            wsr0.Cells[11].Value = "Số lượng hàng sau";
            wsr0.Cells[12].Value = "Trị giá tính thuế sau";
            DataTable table = this.ds.Tables[0];

            for (int i = 0; i < table.Rows.Count; i++)
            {
                WorksheetRow wsr = ws.Rows[i + 1];
                wsr.Cells[0].Value = table.Rows[i]["SoToKhaiBoSung"].ToString();
                wsr.Cells[1].Value = table.Rows[i]["TKMD_ID"].ToString();
                wsr.Cells[2].Value = table.Rows[i]["SoToKhai"].ToString();
                wsr.Cells[3].Value = table.Rows[i]["NgayKhaiBao"].ToString();
                wsr.Cells[4].Value = table.Rows[i]["MaLoaiHinh"].ToString();
                wsr.Cells[5].Value = table.Rows[i]["TrangThaiXuLy"].ToString();
                wsr.Cells[6].Value = table.Rows[i]["SoThuTuDongHangTrenToKhaiGoc"].ToString();
                wsr.Cells[7].Value = table.Rows[i]["MoTaHangHoaTruocKhiKhaiBoSung"].ToString();
                wsr.Cells[8].Value = table.Rows[i]["SoLuongTinhThueTruocKhiKhaiBoSung"].ToString();
                wsr.Cells[9].Value = table.Rows[i]["TriGiaTinhThueTruocKhiKhaiBoSung"].ToString();
                wsr.Cells[10].Value = table.Rows[i]["MoTaHangHoaSauKhiKhaiBoSung"].ToString();
                wsr.Cells[11].Value = table.Rows[i]["SoLuongTinhThueSauKhiKhaiBoSung"].ToString();
                wsr.Cells[12].Value = table.Rows[i]["TriGiaTinhThueSauKhiKhaiBoSung"].ToString();
            }
            DialogResult rs = new DialogResult();
            saveFileDialog1.FileName = "DanhSachHangTK_AMA_" + DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year;
            rs = saveFileDialog1.ShowDialog(this);
            if (rs == DialogResult.OK)
            {
                string fileName = saveFileDialog1.FileName;
                wb.Save(fileName);
                
            }
        }
        private void CapNhat() 
        {
            //DialogResult rs = new DialogResult();
            //rs = MessageBox.Show("Bạn có muốn xuất excel trước khi cập nhật?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //if (rs == DialogResult.Yes)
            //{
            //    XuatEX();
            //}
            if (ShowMessage("Bạn có muốn xuất excel trước khi cập nhật?", true) == "Yes")
            {
                XuatEX();
            }
            //rs = MessageBox.Show("Bạn có muốn cập nhật các mặt hàng đã khai báo sửa đổi bổ sung có thay đổi về số lượng và tên hàng?","Thông báo",MessageBoxButtons.YesNo,MessageBoxIcon.Question);
            if (ShowMessage("Bạn có muốn cập nhật các mặt hàng đã khai báo sửa đổi bổ sung có thay đổi về số lượng và tên hàng ?", true) == "Yes")
            {
            //if (rs == DialogResult.Yes) 
            //{
                decimal SoToKhai = 0;
                int TKMD_ID = 0;
                int stt = 0;
                string maHangTruoc = "";
                string maHangSau = "";
                string tenHangTruoc = "";
                string tenHangSau = "";
                decimal soLuongTruoc = 0;
                decimal soLuongSau = 0;
                decimal triGiaTruoc = 0;
                decimal triGiaSau = 0;
                string motaTruoc = "";
                string motaSau = "";
                string sqlTruoc = "";
                string sqlSau = "";
                string maHSTruoc = "";
                string maHSSau = "";
                string maDVTTruoc = "";
                string maDVTSau = "";
                string maNuocXXTruoc = "";
                string maNuocXXSau = "";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    SoToKhai = Convert.ToDecimal(dr["SoToKhai"].ToString());
                    TKMD_ID = Convert.ToInt32(dr["TKMD_ID"].ToString());
                    stt = Convert.ToInt32(dr["SoThuTuDongHangTrenToKhaiGoc"].ToString());
                    motaTruoc = dr["MoTaHangHoaTruocKhiKhaiBoSung"].ToString();
                    motaSau = dr["MoTaHangHoaSauKhiKhaiBoSung"].ToString();
                    soLuongTruoc = Convert.ToDecimal(dr["SoLuongTinhThueTruocKhiKhaiBoSung"].ToString());
                    soLuongSau = Convert.ToDecimal(dr["SoLuongTinhThueSauKhiKhaiBoSung"].ToString());
                    triGiaTruoc = Convert.ToDecimal(dr["TriGiaTinhThueTruocKhiKhaiBoSung"].ToString());
                    triGiaSau = Convert.ToDecimal(dr["TriGiaTinhThueSauKhiKhaiBoSung"].ToString());
                    maHSTruoc = dr["MaSoHangHoaTruocKhiKhaiBoSung"].ToString();
                    maHSSau = dr["MaSoHangHoaSauKhiKhaiBoSung"].ToString();
                    maDVTTruoc = dr["MaDonViTinhSoLuongTinhThueTruocKhaiBoSung"].ToString();
                    maDVTSau = dr["MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung"].ToString();
                    maNuocXXTruoc = dr["MaNuocXuatXuTruocKhiKhaiBoSung"].ToString();
                    maNuocXXSau = dr["MaNuocXuatXuSauKhiKhaiBoSung"].ToString();
                    if (motaTruoc.Contains("#&"))
                    {
                        string[] temp = motaTruoc.Split(new string[] { "#&" }, StringSplitOptions.None);
                        if (temp.Length > 0)
                        {
                            maHangTruoc = temp[0];
                            tenHangTruoc = temp[1];
                        }

                    }
                    else 
                    {
                        maHangTruoc = String.Empty;
                        tenHangTruoc = String.Empty;
                    }

                    if (motaSau.Contains("#&"))
                    {
                        string[] temp = motaSau.Split(new string[] { "#&" }, StringSplitOptions.None);
                        if (temp.Length > 0)
                        {
                            maHangSau = temp[0];
                            tenHangSau = temp[1];
                        }
                    }
                    else
                    {
                        maHangSau = String.Empty;
                        tenHangSau = String.Empty;
                    }
                    string SoToKhaiTemp = SoToKhai.ToString();
                    string sttTemp = "";
                    if (SoToKhaiTemp.Substring(0, 1) == "1")
                    {
                        if (stt>=10)
                            sttTemp = stt.ToString();
                        sttTemp = "0" + stt.ToString();
                    }
                    else
                    {
                        sttTemp = stt.ToString();
                    }
                    sqlTruoc = string.Format(" MaHangHoa = '{0}' AND TenHang = N'{1}' AND TKMD_ID = {2} AND SoLuong1 = {3} AND SoDong = '{4}'", maHangTruoc, tenHangTruoc, TKMD_ID, soLuongTruoc, sttTemp);
                    sqlTruoc = sqlTruoc.Replace(',', '.');

                    sqlSau = string.Format(" MaHangHoa = '{0}' AND TenHang = N'{1}' AND TKMD_ID = {2} AND SoLuong1 = {3} AND SoDong = '{4}'", maHangSau, tenHangSau, TKMD_ID, soLuongSau, sttTemp);
                    sqlSau = sqlSau.Replace(',', '.');

                    if (!string.IsNullOrEmpty(maHangTruoc) && !string.IsNullOrEmpty(maHangSau))
                    {
                        KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                        if (KDT_VNACC_HangMauDich.SelectCollectionDynamic(sqlSau, "").Count > 0)
                        {
                            //
                        }
                        else
                        {
                            try
                            {
                                hmd = KDT_VNACC_HangMauDich.SelectCollectionDynamic(sqlTruoc, "")[0];
                                hmd.SoLuong1 = soLuongSau;
                                hmd.TenHang = tenHangSau;
                                hmd.MaHangHoa = maHangSau;
                                hmd.MaSoHang = maHSSau;
                                hmd.TriGiaTinhThueS = triGiaSau;
                                hmd.DVTLuong1 = maDVTSau;
                                hmd.NuocXuatXu = maNuocXXSau;
                                hmd.Update();
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                                //throw;
                            }
                            
                        }
                        
                        
                    }
                    else if (!string.IsNullOrEmpty(maHangTruoc) && string.IsNullOrEmpty(maHangSau))
                    {
                        KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                        sqlSau = string.Format(" TKMD_ID = {0} AND SoDong = '{1}'",TKMD_ID,sttTemp);
                        if (KDT_VNACC_HangMauDich.SelectCollectionDynamic(sqlSau, "").Count == 0)
                        {
                        }
                        else
                        {
                            hmd = KDT_VNACC_HangMauDich.SelectCollectionDynamic(sqlTruoc, "")[0];
                            hmd.Delete();
                        }
                    }
                    else if (string.IsNullOrEmpty(maHangTruoc) && !string.IsNullOrEmpty(maHangSau))
                    {
                        KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                        if (KDT_VNACC_HangMauDich.SelectCollectionDynamic(sqlSau, "").Count > 0)
                        {
                        }
                        else
                        {
                            hmd = KDT_VNACC_HangMauDich.SelectCollectionDynamic(sqlTruoc, "")[0];
                            hmd.SoLuong1 = soLuongSau;
                            hmd.TenHang = tenHangSau;
                            hmd.MaHangHoa = maHangSau;
                            hmd.MaSoHang = maHSSau;
                            hmd.TriGiaTinhThueS = triGiaSau;
                            hmd.DVTLuong1 = maDVTSau;
                            hmd.DonGiaHoaDon = triGiaSau / soLuongSau;
                            hmd.DVTDonGia = maDVTSau;
                            hmd.MaTTDonGia = "VND";
                            hmd.DV_SL_TrongDonGiaTinhThue = maDVTSau;
                            hmd.MaTTDonGiaTinhThue = "VND";
                            hmd.NuocXuatXu = maNuocXXSau;
                            hmd.InsertFull();
                        }
                    }
                    //{
                    //    KDT_VNACC_HangMauDich hmd = new KDT_VNACC_HangMauDich();
                    //    sql = string.Format(" TKMD_ID = {0} AND SoLuong1 = {1} AND SoDong = '{2}'", TKMD_ID, soLuongTruoc,stt);
                    //    sql = sql.Replace(',','.');
                    //    try
                    //    {
                    //        hmd = KDT_VNACC_HangMauDich.SelectCollectionDynamic(sql, "")[0];
                    //        hmd.SoLuong1 = soLuongSau;
                    //        hmd.TenHang = tenHangSau;
                    //        hmd.MaHangHoa = maHangSau;
                    //        hmd.MaSoHang = maHSSau;
                    //        hmd.TriGiaTinhThueS = triGiaSau;
                    //        hmd.DVTLuong1 = maDVTSau;
                    //        hmd.NuocXuatXu = maNuocXXSau;

                    //        if (soLuongSau == 0)
                    //            hmd.Delete();
                    //        else
                    //            hmd.Update();
                    //    }
                    //    catch (Exception ex)
                    //    {

                    //    }
                        
                    //}
                    
                }
                ShowMessage("Cập nhật thành công! Lưu ý chức năng chỉ cập nhật cho các tờ khai VNACC. Cần thực hiên thêm chức năng CẬP NHẬT THANH KHOẢN đối với các tờ khai này", false);
                //MessageBox.Show("Cập nhật thành công! Lưu ý chức năng chỉ cập nhật cho các tờ khai VNACC. Cần thực hiên thêm chức năng CẬP NHẬT THANH KHOẢN đối với các tờ khai này","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
                VNACCS.VNACCS_ChuyenThanhKhoan f = new Company.Interface.VNACCS.VNACCS_ChuyenThanhKhoan();
                f.ShowDialog(this);
            }
            
        }
        
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key) 
            {
                case "XuatExcel":
                    XuatEX();
                    break;
                case "cmdCapNhatAMA":
                    CapNhat();
                    break;
            }
        }

        private void AMA_To_VNACC_Form_Load(object sender, EventArgs e)
        {
            this.dgList.DataSource = ds.Tables[0];
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
    }
}