using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT;
using Janus.Windows.GridEX;
using Company.Interface.Report;

namespace Company.Interface
{
    public partial class ChonHangToKHaiXKForm : BaseForm
    {
        public List<HangMauDich> HMDCollectionAll = new List<HangMauDich>();
        public List<HangMauDich> HMDCollectionSelect = new List<HangMauDich>();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public ChonHangToKHaiXKForm()
        {
            InitializeComponent();
        }
        private HangMauDich getHangMauDich(string maHang, List<HangMauDich> hmdCollection)
        {
            foreach (HangMauDich hmd in hmdCollection)
                if (hmd.MaPhu == maHang) return hmd;
            return null;
        }
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (this.HMDCollectionSelect.Count == 9)
                {
                   // ShowMessage("Số hàng trên tờ khai chính đã tối đa, không thể thêm được nữa.",false);
                    MLMessages("Số hàng trên tờ khai chính đã tối đa, không thể thêm được nữa.", "MSG_PUB34", "", false);
                    return;
                }
                HangMauDich hmd = (HangMauDich)e.Row.DataRow;
                this.HMDCollectionAll.Remove(hmd);
                this.HMDCollectionSelect.Add(hmd);
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
                gridEX1.Refetch();
            }
        }

        private void gridEX1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                HangMauDich hmd = (HangMauDich)e.Row.DataRow;
                this.HMDCollectionSelect.Remove(hmd);
                this.HMDCollectionAll.Add(hmd);
                dgList.Refetch();
                gridEX1.Refetch();
            }
        }

        private void ChonHangToKHaiNKForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = this.HMDCollectionAll;
            gridEX1.DataSource = this.HMDCollectionSelect;
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);

        }

        private void gridEX1_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);

        }

        private void gridEX1_DeletingRecords(object sender, CancelEventArgs e)
        {
            GridEXSelectedItemCollection items = gridEX1.SelectedItems;
            foreach(GridEXSelectedItem item in items)
            {
                if (item.RowType == RowType.Record)
                {
                    HangMauDich hmd = (HangMauDich)item.GetRow().DataRow;
                    this.HMDCollectionSelect.Remove(hmd);
                    this.HMDCollectionAll.Add(hmd);
                    
                }
            }
            dgList.Refetch();
            try
            {
                gridEX1.Refetch();
            }
            catch
            {
                gridEX1.Refresh();
            }

        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            //if (this.HMDCollectionSelect.Count == 0)
            //{
            //    this.ShowMessage("Bạn phải chọn ít nhất một hàng cho tờ khai chính.", false);
            //    return;
            //}
            //this.Cursor = Cursors.WaitCursor;
            //ReportViewTKXForm f = new ReportViewTKXForm();
            //f.HMDToKhaiChinhCollection = this.HMDCollectionSelect;
            //f.HMDPhuLucCollection = this.HMDCollectionAll;
            //int count = (this.HMDCollectionAll.Count - 1) / 18 + 1;
            //for (int i = 0; i < count; i++)
            //    f.AddItemComboBox();
            //f.TKMD = this.TKMD;
            //f.ShowDialog(this);
            //this.Cursor = Cursors.Default;
        }
    }
}