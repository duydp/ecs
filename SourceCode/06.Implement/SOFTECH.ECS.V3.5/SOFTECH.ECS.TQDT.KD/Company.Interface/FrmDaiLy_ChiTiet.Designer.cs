﻿namespace Company.Interface
{
    partial class FrmDaiLy_ChiTiet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDaiLy_ChiTiet));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtNguoiLienHeDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSoFaxDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDienThoaiDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtMaMid = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtMailDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtMaDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label26 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtMaCuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMailHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtTenCucHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtTenNganCucHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label43 = new System.Windows.Forms.Label();
            this.donViHaiQuanControl = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label44 = new System.Windows.Forms.Label();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.btnDong = new Janus.Windows.EditControls.UIButton();
            this.officeFormAdorner = new Janus.Windows.Ribbon.OfficeFormAdorner(this.components);
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.officeFormAdorner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Size = new System.Drawing.Size(397, 420);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox1.Controls.Add(this.uiTab1);
            this.uiGroupBox1.Controls.Add(this.btnSave);
            this.uiGroupBox1.Controls.Add(this.btnDong);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(397, 420);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtNguoiLienHeDN);
            this.uiGroupBox4.Controls.Add(this.label11);
            this.uiGroupBox4.Controls.Add(this.txtSoFaxDN);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.txtDienThoaiDN);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.label21);
            this.uiGroupBox4.Controls.Add(this.txtMaMid);
            this.uiGroupBox4.Controls.Add(this.label23);
            this.uiGroupBox4.Controls.Add(this.txtMailDoanhNghiep);
            this.uiGroupBox4.Controls.Add(this.txtDiaChi);
            this.uiGroupBox4.Controls.Add(this.txtTenDN);
            this.uiGroupBox4.Controls.Add(this.label24);
            this.uiGroupBox4.Controls.Add(this.label25);
            this.uiGroupBox4.Controls.Add(this.txtMaDN);
            this.uiGroupBox4.Controls.Add(this.label26);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(397, 213);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.Text = "Thông tin doanh nghiệp";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtNguoiLienHeDN
            // 
            this.txtNguoiLienHeDN.Location = new System.Drawing.Point(141, 129);
            this.txtNguoiLienHeDN.Name = "txtNguoiLienHeDN";
            this.txtNguoiLienHeDN.Size = new System.Drawing.Size(227, 21);
            this.txtNguoiLienHeDN.TabIndex = 5;
            this.txtNguoiLienHeDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(32, 133);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Người liên hệ:";
            // 
            // txtSoFaxDN
            // 
            this.txtSoFaxDN.Location = new System.Drawing.Point(265, 99);
            this.txtSoFaxDN.Name = "txtSoFaxDN";
            this.txtSoFaxDN.Size = new System.Drawing.Size(103, 21);
            this.txtSoFaxDN.TabIndex = 4;
            this.txtSoFaxDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(237, 103);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "Fax:";
            // 
            // txtDienThoaiDN
            // 
            this.txtDienThoaiDN.Location = new System.Drawing.Point(141, 99);
            this.txtDienThoaiDN.Name = "txtDienThoaiDN";
            this.txtDienThoaiDN.Size = new System.Drawing.Size(90, 21);
            this.txtDienThoaiDN.TabIndex = 3;
            this.txtDienThoaiDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(32, 103);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "Điện thoại:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(32, 188);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 13);
            this.label21.TabIndex = 12;
            this.label21.Text = "Mã MID:";
            // 
            // txtMaMid
            // 
            this.txtMaMid.Location = new System.Drawing.Point(141, 184);
            this.txtMaMid.Name = "txtMaMid";
            this.txtMaMid.Size = new System.Drawing.Size(227, 21);
            this.txtMaMid.TabIndex = 7;
            this.txtMaMid.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Location = new System.Drawing.Point(32, 161);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(103, 13);
            this.label23.TabIndex = 7;
            this.label23.Text = "Email doanh nghiệp:";
            // 
            // txtMailDoanhNghiep
            // 
            this.txtMailDoanhNghiep.Location = new System.Drawing.Point(141, 157);
            this.txtMailDoanhNghiep.Name = "txtMailDoanhNghiep";
            this.txtMailDoanhNghiep.Size = new System.Drawing.Size(227, 21);
            this.txtMailDoanhNghiep.TabIndex = 6;
            this.txtMailDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(141, 72);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(227, 21);
            this.txtDiaChi.TabIndex = 2;
            this.txtDiaChi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenDN
            // 
            this.txtTenDN.Location = new System.Drawing.Point(141, 45);
            this.txtTenDN.Name = "txtTenDN";
            this.txtTenDN.Size = new System.Drawing.Size(227, 21);
            this.txtTenDN.TabIndex = 1;
            this.txtTenDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(32, 76);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(43, 13);
            this.label24.TabIndex = 4;
            this.label24.Text = "Địa chỉ:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Location = new System.Drawing.Point(32, 49);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(97, 13);
            this.label25.TabIndex = 2;
            this.label25.Text = "Tên doanh nghiệp:";
            // 
            // txtMaDN
            // 
            this.txtMaDN.Location = new System.Drawing.Point(141, 18);
            this.txtMaDN.Name = "txtMaDN";
            this.txtMaDN.Size = new System.Drawing.Size(227, 21);
            this.txtMaDN.TabIndex = 0;
            this.txtMaDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Location = new System.Drawing.Point(32, 22);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(93, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Mã doanh nghiệp:";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.label40);
            this.uiGroupBox6.Controls.Add(this.txtMaCuc);
            this.uiGroupBox6.Controls.Add(this.txtMailHaiQuan);
            this.uiGroupBox6.Controls.Add(this.label41);
            this.uiGroupBox6.Controls.Add(this.txtTenCucHQ);
            this.uiGroupBox6.Controls.Add(this.label42);
            this.uiGroupBox6.Controls.Add(this.txtTenNganCucHQ);
            this.uiGroupBox6.Controls.Add(this.label43);
            this.uiGroupBox6.Controls.Add(this.donViHaiQuanControl);
            this.uiGroupBox6.Controls.Add(this.label44);
            this.uiGroupBox6.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 218);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(396, 168);
            this.uiGroupBox6.TabIndex = 3;
            this.uiGroupBox6.Text = "Chi cục Hải quan khai báo";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Location = new System.Drawing.Point(16, 23);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(88, 13);
            this.label40.TabIndex = 13;
            this.label40.Text = "Mã cục hải quan:";
            // 
            // txtMaCuc
            // 
            this.txtMaCuc.Location = new System.Drawing.Point(141, 18);
            this.txtMaCuc.Name = "txtMaCuc";
            this.txtMaCuc.Size = new System.Drawing.Size(227, 21);
            this.txtMaCuc.TabIndex = 8;
            this.txtMaCuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMailHaiQuan
            // 
            this.txtMailHaiQuan.Location = new System.Drawing.Point(141, 128);
            this.txtMailHaiQuan.Name = "txtMailHaiQuan";
            this.txtMailHaiQuan.Size = new System.Drawing.Size(227, 21);
            this.txtMailHaiQuan.TabIndex = 12;
            this.txtMailHaiQuan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Location = new System.Drawing.Point(16, 133);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(80, 13);
            this.label41.TabIndex = 10;
            this.label41.Text = "Email Hải quan:";
            // 
            // txtTenCucHQ
            // 
            this.txtTenCucHQ.Location = new System.Drawing.Point(141, 73);
            this.txtTenCucHQ.Name = "txtTenCucHQ";
            this.txtTenCucHQ.Size = new System.Drawing.Size(227, 21);
            this.txtTenCucHQ.TabIndex = 10;
            this.txtTenCucHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Location = new System.Drawing.Point(16, 78);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(92, 13);
            this.label42.TabIndex = 8;
            this.label42.Text = "Tên cục hải quan:";
            // 
            // txtTenNganCucHQ
            // 
            this.txtTenNganCucHQ.Location = new System.Drawing.Point(141, 100);
            this.txtTenNganCucHQ.Name = "txtTenNganCucHQ";
            this.txtTenNganCucHQ.Size = new System.Drawing.Size(227, 21);
            this.txtTenNganCucHQ.TabIndex = 11;
            this.txtTenNganCucHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Location = new System.Drawing.Point(16, 105);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(100, 13);
            this.label43.TabIndex = 2;
            this.label43.Text = "Tên ngắn hải quan:";
            // 
            // donViHaiQuanControl
            // 
            this.donViHaiQuanControl.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl.Location = new System.Drawing.Point(141, 45);
            this.donViHaiQuanControl.Ma = "";
            this.donViHaiQuanControl.MaCuc = "";
            this.donViHaiQuanControl.Name = "donViHaiQuanControl";
            this.donViHaiQuanControl.ReadOnly = false;
            this.donViHaiQuanControl.Size = new System.Drawing.Size(227, 22);
            this.donViHaiQuanControl.TabIndex = 9;
            this.donViHaiQuanControl.VisualStyleManager = null;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Location = new System.Drawing.Point(16, 50);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(123, 13);
            this.label44.TabIndex = 0;
            this.label44.Text = "Chọn hải quan khai báo:";
            // 
            // uiTab1
            // 
            this.uiTab1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiTab1.BackColor = System.Drawing.Color.Transparent;
            this.uiTab1.Location = new System.Drawing.Point(0, 3);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(397, 210);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2007;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(125, 391);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 13;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDong
            // 
            this.btnDong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDong.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDong.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDong.Icon")));
            this.btnDong.Location = new System.Drawing.Point(206, 391);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(75, 23);
            this.btnDong.TabIndex = 14;
            this.btnDong.Text = "Đóng";
            this.btnDong.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // officeFormAdorner
            // 
            this.officeFormAdorner.Form = this;
            this.officeFormAdorner.Office2007CustomColor = System.Drawing.Color.Empty;
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // FrmDaiLy_ChiTiet
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 420);
            this.Controls.Add(this.uiGroupBox1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(405, 454);
            this.MinimumSize = new System.Drawing.Size(405, 454);
            this.Name = "FrmDaiLy_ChiTiet";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin doanh nghiệp và hải quan";
            this.Load += new System.EventHandler(this.FrmDaiLy_ChiTiet_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FrmDaiLy_ChiTiet_KeyUp);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.officeFormAdorner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiLienHeDN;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoFaxDN;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtDienThoaiDN;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaMid;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.GridEX.EditControls.EditBox txtMailDoanhNghiep;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDN;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDN;
        private System.Windows.Forms.Label label26;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private System.Windows.Forms.Label label40;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaCuc;
        private Janus.Windows.GridEX.EditControls.EditBox txtMailHaiQuan;
        private System.Windows.Forms.Label label41;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCucHQ;
        private System.Windows.Forms.Label label42;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNganCucHQ;
        private System.Windows.Forms.Label label43;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl;
        private System.Windows.Forms.Label label44;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnDong;
        private Janus.Windows.Ribbon.OfficeFormAdorner officeFormAdorner;
        private System.Windows.Forms.ErrorProvider errorProvider;
    }
}