﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;
 

namespace Company.Interface.Report.GC
{
    public partial class BangKe10_HQGC : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HD = new HopDong();
        public DataSet dsBK = new DataSet();
        private int STT = 0;
        public BangKe10_HQGC()
        {
            InitializeComponent();
        }
        public void BindReport(string maSP)
        {

            //dsBK.Tables[0].D
            DataRow []dr =  dsBK.Tables[0].Select("MaSP ='" + maSP + "'");
            DataTable dt = dsBK.Tables[0].Clone();
            for (int i = 0; i < dr.Length; i++)
                {
                    dt.ImportRow(dr[i]);
                }

            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr1 in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr1["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + "," + dr1["TenSanPham"].ToString();
                }
            }
            lblMathang.Text = loaiSP; 

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;

            //DataTable dt  = new Company.GC.BLL.GC.BKDinhMuc().getDinhMuc(this.HD.ID, maSP).Tables[0];
            //dsBK = new Company.GC.BLL.GC.BKDinhMuc().getDinhMuc(this.HD.ID, maSP) ;
             //DataTable dt = dsBK.Tables[0];
            dt.TableName = "t_GC_BKDinhMuc";
            this.DataSource = dt;

            lblMathang.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaSP");
            //lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName +".STT");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenNPL");
            lblMaNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaNPL");
            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");
            lblDMSD.DataBindings.Add("Text", this.DataSource, dt.TableName + ".GhiChu");
            lblTLHH.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TyLeHH", "{00:F00}");
            lblDMSD.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DinhMucSD", "{0:N" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDMTH.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DinhMucTH", "{0:N" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblNguonNL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NguonNL");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, dt.TableName + ".GhiChu");
            xrLabel19.Text = GlobalSettings.TieudeNgay;

        }

        private void BangKe10_HQGC_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }
    }
}
