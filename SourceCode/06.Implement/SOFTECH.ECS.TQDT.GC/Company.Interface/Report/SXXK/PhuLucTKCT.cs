using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.Report.SXXK
{
    public partial class PhuLucTKCT : DevExpress.XtraReports.UI.XtraReport
    {
        public HangChuyenTiepCollection HCTCollection = new HangChuyenTiepCollection();
        public ReportViewTKCTForm report;
        private int STT = 0;
        public PhuLucTKCT()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            this.DataSource = this.HCTCollection;
            //lblSTT.DataBindings.Add("Text", this.DataSource, "SoThuTuHang");
            lblTenHang.DataBindings.Add("Text", this.DataSource, "TenHang");
            lblMaHang.DataBindings.Add("Text", this.DataSource, "MaHS");
            lblLuong.DataBindings.Add("Text", this.DataSource, "SoLuong", "{0:g10}");
            lblDVT.DataBindings.Add("Text", this.DataSource, "ID_DVT");
            lblDonGiaNT.DataBindings.Add("Text", this.DataSource, "DonGia", "{0:g10}");
            lblTriGiaNT.DataBindings.Add("Text", this.DataSource, "TriGia", "{0:n2}");
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            this.STT = 0;
        }

        private void lblDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblDVT.Text = DonViTinh.GetName(lblDVT.Text);
        }
        

        public void setNhomHang(XRTableCell cell, string tenHang)
        {
            cell.Text = tenHang;
        }

    }
}
