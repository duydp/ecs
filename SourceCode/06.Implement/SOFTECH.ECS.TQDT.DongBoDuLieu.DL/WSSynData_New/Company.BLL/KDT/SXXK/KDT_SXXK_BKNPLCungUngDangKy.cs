using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
    public partial class KDT_SXXK_BKNPLCungUngDangKy 
	{
        private List<KDT_SXXK_BKNPLCungUng> _ListTKCungUng = new List<KDT_SXXK_BKNPLCungUng>();

        public List<KDT_SXXK_BKNPLCungUng> TKCungUng_List
        {
            set { this._ListTKCungUng = value; }
            get { return this._ListTKCungUng; }
        }

        public static KDT_SXXK_BKNPLCungUngDangKy LoadBy(int lanthanhly)
        {
            string where = string.Format(" LanThanhLy= {0} ",lanthanhly);
            List<KDT_SXXK_BKNPLCungUngDangKy> list = KDT_SXXK_BKNPLCungUngDangKy.SelectCollectionDynamic(where, "");
            try
            {
                if (list.Count > 0)
                {
                    KDT_SXXK_BKNPLCungUngDangKy NPLCungUngDangKy = new KDT_SXXK_BKNPLCungUngDangKy();
                    NPLCungUngDangKy = list[0];
                    List<KDT_SXXK_BKNPLCungUng> BKTKCUList = KDT_SXXK_BKNPLCungUng.SelectCollectionDynamic("Master_id = " + NPLCungUngDangKy.ID, null);
                    foreach (KDT_SXXK_BKNPLCungUng BKTKCU in BKTKCUList)
                    {
                        List<KDT_SXXK_BKNPLCungUng_Detail> BKDetail = KDT_SXXK_BKNPLCungUng_Detail.SelectCollectionDynamic("Master_id = " + BKTKCU.ID, null);
                        foreach (KDT_SXXK_BKNPLCungUng_Detail item in BKDetail)
                        {
                            BKTKCU.Npl_Detail_List.Add(item);
                        }
                       NPLCungUngDangKy.TKCungUng_List.Add(BKTKCU);
                    }
                    return NPLCungUngDangKy;
                }
                else
                    return null;
            }
            catch (Exception)
            {
                return null;
            }
        
        }
        public static KDT_SXXK_BKNPLCungUngDangKy LoadByID(string id)
        {
            string where = string.Format(" id = {0} ", id);
            List<KDT_SXXK_BKNPLCungUngDangKy> list = KDT_SXXK_BKNPLCungUngDangKy.SelectCollectionDynamic(where, "");
            try
            {
                if (list.Count > 0)
                {
                    KDT_SXXK_BKNPLCungUngDangKy NPLCungUngDangKy = new KDT_SXXK_BKNPLCungUngDangKy();
                    NPLCungUngDangKy = list[0];
                    List<KDT_SXXK_BKNPLCungUng> BKTKCUList = KDT_SXXK_BKNPLCungUng.SelectCollectionDynamic("Master_id = " + NPLCungUngDangKy.ID, null);
                    foreach (KDT_SXXK_BKNPLCungUng BKTKCU in BKTKCUList)
                    {
                        List<KDT_SXXK_BKNPLCungUng_Detail> BKDetail = KDT_SXXK_BKNPLCungUng_Detail.SelectCollectionDynamic("Master_id = " + BKTKCU.ID, null);
                        foreach (KDT_SXXK_BKNPLCungUng_Detail item in BKDetail)
                        {
                            BKTKCU.Npl_Detail_List.Add(item);
                        }
                        NPLCungUngDangKy.TKCungUng_List.Add(BKTKCU);
                    }
                    return NPLCungUngDangKy;
                }
                else
                    return null;
            }
            catch (Exception)
            {
                return null;
            }

        }
        //public static KDT_SXXK_BKNPLCungUng LoadID(long id)
        //{
           
        //    List<KDT_SXXK_BKNPLCungUng> list = KDT_SXXK_BKNPLCungUng.SelectCollectionDynamic("id = "+id, "");
        //    try
        //    {
        //        if (list.Count > 0)
        //        {
        //            KDT_SXXK_BKNPLCungUng BKNPL = new KDT_SXXK_BKNPLCungUng();
        //            BKNPL = list[0];
        //            List<KDT_SXXK_BKNPLCungUng_Detail> BKDetail = KDT_SXXK_BKNPLCungUng_Detail.SelectCollectionDynamic("Master_id = " + BKNPL.ID, null);
        //            foreach (KDT_SXXK_BKNPLCungUng_Detail item in BKDetail)
        //            {
        //                BKNPL.Npl_Detail_List.Add(item);
        //            }
        //            return BKNPL;
        //        }
        //        else
        //            return null;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }

        //}
        public bool InsertUpdatFull()
        {
            bool ret = false;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.TrangThaiXuLy = -1;
                        this.ID = this.Insert(transaction);
                    }
                    else
                        this.Update(transaction);
                   
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    connection.Close();
                    throw new Exception(ex.Message);
                    throw;
                }
            }
            return ret;
        }
        public void DeleteFull(long id)
        {
            this.ID = id;
            List<KDT_SXXK_BKNPLCungUng> NPLCUList = KDT_SXXK_BKNPLCungUng.SelectCollectionDynamic("Master_id =" + id, "");
                foreach (KDT_SXXK_BKNPLCungUng item in NPLCUList)
                {
		             item.DeleteFull(item.ID);
                }
            this.Delete();
        }
        
	}	
}