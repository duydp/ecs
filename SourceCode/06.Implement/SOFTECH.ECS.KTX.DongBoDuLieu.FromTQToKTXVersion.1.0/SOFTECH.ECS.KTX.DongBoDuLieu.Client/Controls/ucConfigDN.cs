﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DongBoDuLieu.BLL;
using System.Security.Cryptography;
namespace DongBo.Controls
{
    public partial class ucConfigDN : ucDesign
    {
        frmMain frmParent = null;

        public ucConfigDN()
        {
            InitializeComponent();

            frmParent = (frmMain)Application.OpenForms["frmMain"];
        }

        public void saveConfigDoanhNghiep()
        {
            GlobalSettings.saveConfigDN(txtServerSource.Text, cboDatabaseSource.Text, txtUserSource.Text, txtPassSource.Text, txtServerTarget.Text, cboDatabaseTarget.Text, txtUserTarget.Text, txtPassTarget.Text, cboxPhanHe.SelectedIndex, dateBatDau.Value, dateKetThuc.Value, chkDaDuyet.Checked, chkCompareData.Checked, chkLayToKhaiSua.Checked);
        }

        private void ucConfigDN_Load(object sender, EventArgs e)
        {
            chkToKhaiMauDich.Checked = false;
            try
            {
                if (frmParent.btnNext.Text != "Tiếp tục")
                {
                    frmParent.btnNext.Text = "Tiếp tục";
                }
                if (!frmParent.btnPrev.Enabled)
                {
                    frmParent.btnPrev.Enabled = true;
                }
                txtMaDN.Text = GlobalSettings.MaDoanhNghiep;
                dateBatDau.Value = GlobalSettings.NgayBatDau;
                dateKetThuc.Value = GlobalSettings.NgayKetThuc;
                cboxPhanHe.SelectedIndex = GlobalSettings.Phanhe;

                txtServerSource.Text = GlobalSettings.DiaChiSQLSource;
                cboDatabaseSource.Text = GlobalSettings.DBSource;
                txtUserSource.Text = GlobalSettings.UserSQLSource;
                txtPassSource.Text = GlobalSettings.PassSQLSource;

                //Neu dong bo tu hai quan -> khong su dung cac tieu chi nay
                //txtServerSource.Enabled = cboDatabaseSource.Enabled = txtUserSource.Enabled = txtPassSource.Enabled = !(GlobalSettings.radSynFromHQ == true);
                if (GlobalSettings.IsSynFromHQ == true)
                {
                    GlobalSettings.IsChiLayDuLieuDaDuyet = GlobalSettings.IsSoSanhDuLieu = GlobalSettings.LayThongTinToKhaiSua = false;
                    chkDaDuyet.Enabled = chkCompareData.Enabled = chkLayToKhaiSua.Enabled = false;

                }
                else
                {
                    chkDaDuyet.Checked = GlobalSettings.IsChiLayDuLieuDaDuyet;
                    chkCompareData.Checked = GlobalSettings.IsSoSanhDuLieu;
                    chkLayToKhaiSua.Checked = GlobalSettings.LayThongTinToKhaiSua;
                    chkLayToKhaiSua.Enabled = (GlobalSettings.PhienBan == "TQDT");
                }

                txtServerTarget.Text = GlobalSettings.DiaChiSQLTarget;
                cboDatabaseTarget.Text = GlobalSettings.DBTarget;
                txtUserTarget.Text = GlobalSettings.UserSQLTarget;
                txtPassTarget.Text = GlobalSettings.PassSQLTarget;

                SetListTable();

                this.validControl();

                btnConnect1.BackColor = Color.Pink;
                btnConnect2.BackColor = Color.Pink;

                button1.Visible = true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void cboxPhanHe_SelectedIndexChanged(object sender, EventArgs e)
        {
            GlobalSettings.Phanhe = cboxPhanHe.SelectedIndex;
            if (GlobalSettings.Phanhe == 1) //SXXK
            {
                //txtMKD.Visible = false;
                //txtMCD.Visible = false;
                //lblCSDL.Visible = false;
                //txtTCD.Visible = false;
                //label14.Visible = false;
                //label13.Visible = false;
                //label12.Visible = false;
                //txtDBD.Visible = false;

                //pnlSXXK.Enabled = true;
                pnlGC.Enabled = true;
            }
            else //GC
            {
                //txtMKD.Visible = true;
                //txtMCD.Visible = true;
                //lblCSDL.Visible = true;
                //txtTCD.Visible = true;
                //label14.Visible = true;
                //label13.Visible = true;
                //label12.Visible = true;
                //txtDBD.Visible = true;

                //pnlSXXK.Enabled = false;
                pnlGC.Enabled = true;
            }


        }

        private void txtMaDN_TextChanged(object sender, EventArgs e)
        {
            GlobalSettings.MaDoanhNghiep = txtMaDN.Text;
            this.validControl();
        }

        private void dateBatDau_ValueChanged(object sender, EventArgs e)
        {
            GlobalSettings.NgayBatDau = dateBatDau.Value;
            this.validControl();
        }

        private void dateKetThuc_ValueChanged(object sender, EventArgs e)
        {
            GlobalSettings.NgayKetThuc = dateKetThuc.Value;
            this.validControl();
        }

        private void validControl()
        {
            if (txtMaDN.Text.Trim() != "" && dateBatDau.Value < dateKetThuc.Value)
            {
                frmParent.btnNext.Enabled = true;
            }
            else
            {
                frmParent.btnNext.Enabled = false;
            }
        }

        private void txtSQLAddress_TextChanged(object sender, EventArgs e)
        {
            GlobalSettings.DiaChiSQLSource = txtServerSource.Text;
            this.validControl();
        }

        private void txtSQLUser_TextChanged(object sender, EventArgs e)
        {
            GlobalSettings.UserSQLSource = txtUserSource.Text;
            this.validControl();
        }

        private void txtSQLPass_TextChanged(object sender, EventArgs e)
        {
            GlobalSettings.PassSQLSource = txtPassSource.Text;
            this.validControl();
        }

        private void txtSQLDB_TextChanged(object sender, EventArgs e)
        {
            GlobalSettings.DBSource = cboDatabaseSource.Text;
            this.validControl();
        }

        private void txtMCD_TextChanged(object sender, EventArgs e)
        {
            GlobalSettings.DiaChiSQLTarget = txtServerTarget.Text.Trim();
            this.validControl();
        }

        private void txtTCD_TextChanged(object sender, EventArgs e)
        {
            GlobalSettings.UserSQLTarget = txtUserTarget.Text.Trim();
            this.validControl();
        }

        private void txtMKD_TextChanged(object sender, EventArgs e)
        {
            GlobalSettings.PassSQLTarget = txtPassTarget.Text.Trim();
            this.validControl();
        }

        private void txtDBD_TextChanged(object sender, EventArgs e)
        {
            GlobalSettings.DBTarget = cboDatabaseTarget.Text.Trim();
            this.validControl();
        }

        private void chkNPL_CheckedChanged(object sender, EventArgs e)
        {
            GlobalSettings.IsGetNPL = chkNPL.Checked;
            this.validControl();
        }

        private void chkSP_CheckedChanged(object sender, EventArgs e)
        {
            GlobalSettings.IsGetSP = chkSP.Checked;
            this.validControl();
        }

        private void chkDM_CheckedChanged(object sender, EventArgs e)
        {
            GlobalSettings.IsGetDM = chkDM.Checked;
            this.validControl();
        }

        private void chkTK_CheckedChanged(object sender, EventArgs e)
        {
            GlobalSettings.IsGetTK = chkTK.Checked;
            this.validControl();
        }

        private void btnConnect1_Click(object sender, EventArgs e)
        {
            saveConfigDoanhNghiep();

            bool ret = false;
            string strSQL = "Server = " + GlobalSettings.DiaChiSQLSource + "; Database = " + GlobalSettings.DBSource + "; UID = " + GlobalSettings.UserSQLSource + "; PWD =" + GlobalSettings.PassSQLSource;
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(strSQL);
            try
            {
                conn.Open();
                ret = true;
                btnConnect1.Image = Properties.Resources.connect;
                btnConnect1.BackColor = Color.Transparent;
                conn.Close();
            }
            catch
            {
                btnConnect1.Image = Properties.Resources.disconnect;
                btnConnect1.BackColor = Color.Pink;
                //MessageBox.Show("Không kết máy chủ doanh nghiệp không thành công! \nVui lòng kiểm tra thông tin kết nối");
            }
        }

        private void btnConnect2_Click(object sender, EventArgs e)
        {
            saveConfigDoanhNghiep();

            bool ret = false;
            string strSQL = "Server = " + GlobalSettings.DiaChiSQLTarget + "; Database = " + GlobalSettings.DBTarget + "; UID = " + GlobalSettings.UserSQLTarget + "; PWD =" + GlobalSettings.PassSQLTarget;
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(strSQL);
            try
            {
                conn.Open();
                ret = true;
                btnConnect2.Image = Properties.Resources.connect;
                btnConnect2.BackColor = Color.Transparent;
                conn.Close();
            }
            catch
            {
                btnConnect2.Image = Properties.Resources.disconnect;
                btnConnect2.BackColor = Color.Pink;
                //MessageBox.Show("Không kết máy chủ doanh nghiệp không thành công! \nVui lòng kiểm tra thông tin kết nối");
            }
        }

        private void btnCopySource_Click(object sender, EventArgs e)
        {
            txtServerTarget.Text = txtServerSource.Text;
            txtUserTarget.Text = txtUserSource.Text;
            txtPassTarget.Text = txtPassSource.Text;
            cboDatabaseTarget.Text = cboDatabaseSource.Text;
            if (GlobalSettings.ListTableNameSource.Count > 0)
            {
                cboDatabaseTarget.Items.Clear();
                cboDatabaseTarget.Items.Add("(-Làm mới-)");

                foreach (string item in GlobalSettings.ListTableNameSource)
                {
                    cboDatabaseTarget.Items.Add(item);
                }
                cboDatabaseTarget.SelectedIndex = GlobalSettings.IndexTableNameSource;
            }
        }

        private void btnSwitch_Click(object sender, EventArgs e)
        {
            string address = "";
            string user = "";
            string pass = "";
            string db = "";

            //1.
            address = txtServerSource.Text;
            user = txtUserSource.Text;
            pass = txtPassSource.Text;
            db = cboDatabaseSource.Text;

            //2.
            txtServerSource.Text = txtServerTarget.Text;
            txtUserSource.Text = txtUserTarget.Text;
            txtPassSource.Text = txtPassTarget.Text;
            cboDatabaseSource.Text = cboDatabaseTarget.Text;

            //3.
            txtServerTarget.Text = address;
            txtUserTarget.Text = user;
            txtPassTarget.Text = pass;
            cboDatabaseTarget.Text = db;
        }

        private void btnCopyDestinate_Click(object sender, EventArgs e)
        {
            txtServerSource.Text = txtServerTarget.Text;
            txtUserSource.Text = txtUserTarget.Text;
            txtPassSource.Text = txtPassTarget.Text;
            cboDatabaseSource.Text = cboDatabaseTarget.Text;
        }

        private void chkDaDuyet_CheckedChanged(object sender, EventArgs e)
        {
            GlobalSettings.IsChiLayDuLieuDaDuyet = chkDaDuyet.Checked;
            this.validControl();


            chkLayToKhaiSua.CheckedChanged -= new EventHandler(chkLayToKhaiSua_CheckedChanged);
            chkCompareData.CheckedChanged -= new EventHandler(chkCompareData_CheckedChanged);
            chkDaDuyet.CheckedChanged -= new EventHandler(chkDaDuyet_CheckedChanged);

            if (chkDaDuyet.Checked)
                chkLayToKhaiSua.Checked = false;
            //Neu khong chon lay tk da duyet -> khong thuc hien so sanh du lieu vi so sanh phai dua vao "Ngay dang ky" cua tk. 
            //Muốn có ngày đăng ký TK phải được cấp số và duyệt.
            else
                chkCompareData.Checked = false;

            GlobalSettings.IsSoSanhDuLieu = chkCompareData.Checked;
            GlobalSettings.IsChiLayToKhaiSuaBoSung = chkLayToKhaiSua.Checked;
            GlobalSettings.IsChiLayDuLieuDaDuyet = chkDaDuyet.Checked;

            chkLayToKhaiSua.CheckedChanged += new EventHandler(chkLayToKhaiSua_CheckedChanged);
            chkCompareData.CheckedChanged += new EventHandler(chkCompareData_CheckedChanged);
            chkDaDuyet.CheckedChanged += new EventHandler(chkDaDuyet_CheckedChanged);

            GlobalSettings.SaveNodeXmlAppSettings("LayThongTinDaDuyet", chkDaDuyet.Checked);
            GlobalSettings.SaveNodeXmlAppSettings("SoSanhDuLieu", chkCompareData.Checked);
            GlobalSettings.SaveNodeXmlAppSettings("LayThongTinToKhaiSua", chkLayToKhaiSua.Checked);
            GlobalSettings.RefreshKey();
        }

        private void chkCompareData_CheckedChanged(object sender, EventArgs e)
        {
            GlobalSettings.IsSoSanhDuLieu = chkCompareData.Checked;
            this.validControl();

            if (chkCompareData.Checked)
            {
                chkLayToKhaiSua.CheckedChanged -= new EventHandler(chkLayToKhaiSua_CheckedChanged);
                chkCompareData.CheckedChanged -= new EventHandler(chkCompareData_CheckedChanged);
                chkDaDuyet.CheckedChanged -= new EventHandler(chkDaDuyet_CheckedChanged);

                chkLayToKhaiSua.Checked = false;

                GlobalSettings.IsSoSanhDuLieu = chkCompareData.Checked;
                GlobalSettings.IsChiLayToKhaiSuaBoSung = chkLayToKhaiSua.Checked;
                GlobalSettings.IsChiLayDuLieuDaDuyet = chkDaDuyet.Checked;

                chkLayToKhaiSua.CheckedChanged += new EventHandler(chkLayToKhaiSua_CheckedChanged);
                chkCompareData.CheckedChanged += new EventHandler(chkCompareData_CheckedChanged);
                chkDaDuyet.CheckedChanged += new EventHandler(chkDaDuyet_CheckedChanged);
            }

            GlobalSettings.SaveNodeXmlAppSettings("LayThongTinDaDuyet", chkDaDuyet.Checked);
            GlobalSettings.SaveNodeXmlAppSettings("SoSanhDuLieu", chkCompareData.Checked);
            GlobalSettings.SaveNodeXmlAppSettings("LayThongTinToKhaiSua", chkLayToKhaiSua.Checked);
            GlobalSettings.RefreshKey();
        }

        #region SQLDMO

        //private SQLDMO.Application fApp = null;
        //private SQLDMO.SQLServerClass fServer = null;
        //private string gAppName = "Sql Server BackUp utility";

        private void cboDatabaseSource_DropDown(object sender, EventArgs e)
        {
            if (cboDatabaseSource.Items.Count <= 1)
                GetListTableSource(cboDatabaseSource);
        }

        private void cboDatabaseTarget_DropDown(object sender, EventArgs e)
        {
            if (cboDatabaseTarget.Items.Count <= 1)
                GetListTableTarget(cboDatabaseTarget);
        }

        private void GetListTableSource(ComboBox comboBox)
        {
            if (txtServerSource.Text == "")
            {
                MessageBox.Show("Enter a Server Name!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                Cursor = Cursors.WaitCursor;

                string strConnString = "server=" + txtServerSource.Text;
                strConnString += ";User Id=" + txtUserSource.Text + ";Password=" + txtPassSource.Text;

                //fServer = new SQLDMO.SQLServerClass();
                //fServer.Connect(txtServerName.Text, txtSa.Text, txtPass.Text);

                Company.KDT.SHARE.Components.SQL.InitializeServer(strConnString);

                comboBox.Items.Clear();
                comboBox.Items.Add("(-Làm mới-)");

                foreach (string dbName in Company.KDT.SHARE.Components.SQL.ListDatabases())
                {
                    comboBox.Items.Add(dbName);
                }

                comboBox.Sorted = true;

                comboBox.SelectedIndexChanged -= new EventHandler(cboDatabaseSource_SelectedIndexChanged);
                comboBox.SelectedIndex = 0;
                comboBox.SelectedIndexChanged += new EventHandler(cboDatabaseSource_SelectedIndexChanged);

            }
            catch (Exception e1)
            {
                //Globals.ShowMessage("Kết nối không thành công. Bạn hãy kiểm tra lại thông tin cấu hình.\r\n\nChi tiết: " + e1.Message, false);
                MessageBox.Show("Kết nối không thành công. Bạn hãy kiểm tra lại thông tin cấu hình.\r\n\nChi tiết: " + e1.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Logger.LocalLogger.Instance().WriteMessage(e1);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetListTableTarget(ComboBox comboBox)
        {
            if (txtServerTarget.Text == "")
            {
                MessageBox.Show("Enter a Server Name!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                Cursor = Cursors.WaitCursor;

                string strConnString = "server=" + txtServerTarget.Text;
                strConnString += ";User Id=" + txtUserTarget.Text + ";Password=" + txtPassTarget.Text;

                Company.KDT.SHARE.Components.SQL.InitializeServer(strConnString);

                comboBox.Items.Clear();
                comboBox.Items.Add("(-Làm mới-)");

                foreach (string dbName in Company.KDT.SHARE.Components.SQL.ListDatabases())
                {
                    comboBox.Items.Add(dbName);
                }

                comboBox.Sorted = true;

                comboBox.SelectedIndexChanged -= new EventHandler(cboDatabaseTarget_SelectedIndexChanged);
                comboBox.SelectedIndex = 0;
                comboBox.SelectedIndexChanged += new EventHandler(cboDatabaseTarget_SelectedIndexChanged);

            }
            catch (Exception e1)
            {
                //Globals.ShowMessage("Kết nối không thành công. Bạn hãy kiểm tra lại thông tin cấu hình.\r\n\nChi tiết: " + e1.Message, false);
                MessageBox.Show("Kết nối không thành công. Bạn hãy kiểm tra lại thông tin cấu hình.\r\n\nChi tiết: " + e1.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Logger.LocalLogger.Instance().WriteMessage(e1);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void cboDatabaseSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((sender as ComboBox).SelectedIndex == 0)
            {
                GetListTableSource(sender as ComboBox);
            }
        }

        private void cboDatabaseTarget_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((sender as ComboBox).SelectedIndex == 0)
            {
                GetListTableTarget(sender as ComboBox);
            }
        }

        private void SetListTable()
        {
            if (GlobalSettings.ListTableNameSource.Count > 0)
            {
                cboDatabaseSource.Items.Clear();
                cboDatabaseSource.Items.Add("(-Làm mới-)");

                foreach (string item in GlobalSettings.ListTableNameSource)
                {
                    cboDatabaseSource.Items.Add(item);
                }

                cboDatabaseSource.SelectedIndex = GlobalSettings.IndexTableNameSource;
            }

            if (GlobalSettings.ListTableNameTarget.Count > 0)
            {
                cboDatabaseTarget.Items.Clear();
                cboDatabaseTarget.Items.Add("(-Làm mới-)");

                foreach (string item in GlobalSettings.ListTableNameTarget)
                {
                    cboDatabaseTarget.Items.Add(item);
                }
                cboDatabaseTarget.SelectedIndex = GlobalSettings.IndexTableNameTarget;
            }
        }

        #endregion SQLDMO

        private void cboDatabaseSource_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (cboDatabaseSource.SelectedIndex == 0)
                {
                    GetListTableSource(cboDatabaseSource);
                }

                GlobalSettings.IndexTableNameSource = cboDatabaseSource.SelectedIndex;

                GlobalSettings.ListTableNameSource.Clear();
                for (int i = 1; i < cboDatabaseSource.Items.Count; i++)
                {
                    GlobalSettings.ListTableNameSource.Add(cboDatabaseSource.Items[i].ToString());
                }

                Cursor = Cursors.Default;
            }
            catch (Exception ex) { }
        }

        private void cboDatabaseTarget_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (cboDatabaseTarget.SelectedIndex == 0)
                {
                    GetListTableSource(cboDatabaseTarget);
                }

                GlobalSettings.IndexTableNameTarget = cboDatabaseTarget.SelectedIndex;

                GlobalSettings.ListTableNameTarget.Clear();
                for (int i = 1; i < cboDatabaseTarget.Items.Count; i++)
                {
                    GlobalSettings.ListTableNameTarget.Add(cboDatabaseTarget.Items[i].ToString());
                }

                Cursor = Cursors.Default;
            }
            catch (Exception ex) { }
        }

        private void chkLayToKhaiSua_CheckedChanged(object sender, EventArgs e)
        {
            GlobalSettings.IsChiLayToKhaiSuaBoSung = chkLayToKhaiSua.Checked;
            this.validControl();

            if (chkLayToKhaiSua.Checked)
            {
                chkLayToKhaiSua.CheckedChanged -= new EventHandler(chkLayToKhaiSua_CheckedChanged);
                chkCompareData.CheckedChanged -= new EventHandler(chkCompareData_CheckedChanged);
                chkDaDuyet.CheckedChanged -= new EventHandler(chkDaDuyet_CheckedChanged);

                chkDaDuyet.Checked = chkCompareData.Checked = false;

                GlobalSettings.IsSoSanhDuLieu = chkCompareData.Checked;
                GlobalSettings.IsChiLayDuLieuDaDuyet = chkDaDuyet.Checked;
                GlobalSettings.IsChiLayToKhaiSuaBoSung = chkLayToKhaiSua.Checked;

                chkLayToKhaiSua.CheckedChanged += new EventHandler(chkLayToKhaiSua_CheckedChanged);
                chkCompareData.CheckedChanged += new EventHandler(chkCompareData_CheckedChanged);
                chkDaDuyet.CheckedChanged += new EventHandler(chkDaDuyet_CheckedChanged);
            }

            GlobalSettings.SaveNodeXmlAppSettings("LayThongTinDaDuyet", chkDaDuyet.Checked);
            GlobalSettings.SaveNodeXmlAppSettings("SoSanhDuLieu", chkCompareData.Checked);
            GlobalSettings.SaveNodeXmlAppSettings("LayThongTinToKhaiSua", chkLayToKhaiSua.Checked);
            GlobalSettings.RefreshKey();
        }

        private string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có thật sự muốn tạo tài khoản cho doanh nghiệp không?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            try
            {
                WebServiceDB.CreateAccount(txtMaDN.Text, GetMD5Value(txtMaDN.Text.Trim() + "+" + txtMaDN.Text.Trim()), txtMaDN.Text);
                MessageBox.Show("Tạo tài khoản thành công\nVui lòng đóng và chạy lại phần mềm", "Thông báo");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void chkToKhaiMauDich_CheckedChanged(object sender, EventArgs e)
        {
            GlobalSettings.UpdateToKhaiMauDich = chkToKhaiMauDich.Checked;
        }
    }
}
