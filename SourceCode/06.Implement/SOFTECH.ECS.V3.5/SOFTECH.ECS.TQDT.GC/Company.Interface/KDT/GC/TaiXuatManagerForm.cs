﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using Company.Interface.GC;
using SOFTECH.ECS.TQDT.GC.KDT.GC;
namespace Company.Interface.KDT.GC
{
    public partial class TaiXuatManagerForm : BaseForm
    {
        KDT_GC_TaiXuatDangKy TaiXuatDangky = new KDT_GC_TaiXuatDangKy();
        List<KDT_GC_TaiXuatDangKy> TXDKList = new List<KDT_GC_TaiXuatDangKy>();
        public HopDong HD = new HopDong();
        public TaiXuatManagerForm()
        {
            InitializeComponent();
        }

      
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                int id= Convert.ToInt32(dgList.CurrentRow.Cells["ID"].Value);
                TaiXuatForm frm = new TaiXuatForm();
                frm.TaiXuatDK= KDT_GC_TaiXuatDangKy.LoadFull(id);
                frm.ShowDialog();
            }
        }

        private void CungUngManagerForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            if (HD.ID > 0)
                TXDKList = KDT_GC_TaiXuatDangKy.SelectCollectionDynamic("HopDong_ID = " + HD.ID, "");
            else
                TXDKList = KDT_GC_TaiXuatDangKy.SelectCollectionAll();
            dgList.DataSource = TXDKList;
            dgList.Refetch();
        }
        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case"cmdNPLCungUng":
                    ThemBangKeTaiXuat();
                    break;

            }
        }
        private void ThemBangKeTaiXuat()
        {
            if (HD.ID > 0)
            {
                HopDongManageForm f = new HopDongManageForm();
                f.IsBrowseForm = true;
                f.IsDaDuyet = true;
                f.ShowDialog();
                if (!string.IsNullOrEmpty(f.HopDongSelected.SoHopDong))
                {
                    HD = f.HopDongSelected;
                }
            }
            TaiXuatDangky = new KDT_GC_TaiXuatDangKy();
            TaiXuatDangky.HopDong_ID = HD.ID;
            TaiXuatDangky.SoHopDong = HD.SoHopDong;
            TaiXuatDangky.NgayHopDong = HD.NgayKy;
            TaiXuatDangky.NgayHetHan = HD.NgayHetHan;
            TaiXuatForm frm = new TaiXuatForm();
            frm.TaiXuatDK = TaiXuatDangky;
            frm.ShowDialog();
            LoadData();
            
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
             if (e.Row.RowType == RowType.Record)
            {
                string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                 switch(trangthai)
                 {
                     case"-1":
                         e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                         break;
                     case "0":
                         e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                         break;
                     case "1":
                         e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                         break;
                     case "2":
                         e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                         break;
                 }
             }
        }
     
        
    }
}
