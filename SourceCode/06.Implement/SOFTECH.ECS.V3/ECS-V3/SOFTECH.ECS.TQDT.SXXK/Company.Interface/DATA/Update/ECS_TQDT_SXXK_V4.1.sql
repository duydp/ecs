﻿/*
Run this script on:

        192.168.72.100.ECS_TQDT_SXXK_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_SXXK_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 08/27/2012 9:36:42 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[p_SXXK_HangMauDich_InsertUpdate]'
GO
    
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_InsertUpdate]    
-- Database: HaiQuan    
-- Author: Ngo Thanh Tung    
-- Time created: Tuesday, October 28, 2008    
------------------------------------------------------------------------------------------------------------------------    
    
ALTER PROCEDURE [dbo].[p_SXXK_HangMauDich_InsertUpdate]
    @SoToKhai INT ,
    @MaLoaiHinh CHAR(5) ,
    @MaHaiQuan CHAR(6) ,
    @NamDangKy SMALLINT ,
    @SoThuTuHang SMALLINT ,
    @MaHS VARCHAR(12) ,
    @MaPhu VARCHAR(30) ,
    @TenHang NVARCHAR(80) ,
    @NuocXX_ID CHAR(3) ,
    @DVT_ID CHAR(3) ,
    @SoLuong NUMERIC(18, 3) ,
    @DonGiaKB NUMERIC(38, 15) ,
    @DonGiaTT NUMERIC(38, 15) ,
    @TriGiaKB NUMERIC(38, 15) ,
    @TriGiaTT NUMERIC(38, 15) ,
    @TriGiaKB_VND NUMERIC(38, 15) ,
    @ThueSuatXNK NUMERIC(5, 2) ,
    @ThueSuatTTDB NUMERIC(5, 2) ,
    @ThueSuatGTGT NUMERIC(5, 2) ,
    @ThueXNK MONEY ,
    @ThueTTDB MONEY ,
    @ThueGTGT MONEY ,
    @PhuThu MONEY ,
    @TyLeThuKhac NUMERIC(5, 2) ,
    @TriGiaThuKhac MONEY ,
    @MienThue TINYINT
AS 
    IF EXISTS ( SELECT  [SoToKhai] ,
                        [MaLoaiHinh] ,
                        [MaHaiQuan] ,
                        [NamDangKy] ,
                        [SoThuTuHang]
                FROM    [dbo].[t_SXXK_HangMauDich]
                WHERE   [SoToKhai] = @SoToKhai
                        AND [MaLoaiHinh] = @MaLoaiHinh
                        AND [MaHaiQuan] = @MaHaiQuan
                        AND [NamDangKy] = @NamDangKy
                        AND [SoThuTuHang] = @SoThuTuHang
                        AND MaPhu = @MaPhu ) 
        BEGIN    
            UPDATE  [dbo].[t_SXXK_HangMauDich]
            SET     [MaHS] = @MaHS ,
                    [MaPhu] = @MaPhu ,
                    [TenHang] = @TenHang ,
                    [NuocXX_ID] = @NuocXX_ID ,
                    [DVT_ID] = @DVT_ID ,
                    [SoLuong] = @SoLuong ,
                    [DonGiaKB] = @DonGiaKB ,
                    [DonGiaTT] = @DonGiaTT ,
                    [TriGiaKB] = @TriGiaKB ,
                    [TriGiaTT] = @TriGiaTT ,
                    [TriGiaKB_VND] = @TriGiaKB_VND ,
                    [ThueSuatXNK] = @ThueSuatXNK ,
                    [ThueSuatTTDB] = @ThueSuatTTDB ,
                    [ThueSuatGTGT] = @ThueSuatGTGT ,
                    [ThueXNK] = @ThueXNK ,
                    [ThueTTDB] = @ThueTTDB ,
                    [ThueGTGT] = @ThueGTGT ,
                    [PhuThu] = @PhuThu ,
                    [TyLeThuKhac] = @TyLeThuKhac ,
                    [TriGiaThuKhac] = @TriGiaThuKhac ,
                    [MienThue] = @MienThue
            WHERE   [SoToKhai] = @SoToKhai
                    AND [MaLoaiHinh] = @MaLoaiHinh
                    AND [MaHaiQuan] = @MaHaiQuan
                    AND [NamDangKy] = @NamDangKy
                    AND [SoThuTuHang] = @SoThuTuHang 
                    AND MaPhu = @MaPhu
        END    
    ELSE 
        BEGIN    
            INSERT  INTO [dbo].[t_SXXK_HangMauDich]
                    ( [SoToKhai] ,
                      [MaLoaiHinh] ,
                      [MaHaiQuan] ,
                      [NamDangKy] ,
                      [SoThuTuHang] ,
                      [MaHS] ,
                      [MaPhu] ,
                      [TenHang] ,
                      [NuocXX_ID] ,
                      [DVT_ID] ,
                      [SoLuong] ,
                      [DonGiaKB] ,
                      [DonGiaTT] ,
                      [TriGiaKB] ,
                      [TriGiaTT] ,
                      [TriGiaKB_VND] ,
                      [ThueSuatXNK] ,
                      [ThueSuatTTDB] ,
                      [ThueSuatGTGT] ,
                      [ThueXNK] ,
                      [ThueTTDB] ,
                      [ThueGTGT] ,
                      [PhuThu] ,
                      [TyLeThuKhac] ,
                      [TriGiaThuKhac] ,
                      [MienThue]    
                    )
            VALUES  ( @SoToKhai ,
                      @MaLoaiHinh ,
                      @MaHaiQuan ,
                      @NamDangKy ,
                      @SoThuTuHang ,
                      @MaHS ,
                      @MaPhu ,
                      @TenHang ,
                      @NuocXX_ID ,
                      @DVT_ID ,
                      @SoLuong ,
                      @DonGiaKB ,
                      @DonGiaTT ,
                      @TriGiaKB ,
                      @TriGiaTT ,
                      @TriGiaKB_VND ,
                      @ThueSuatXNK ,
                      @ThueSuatTTDB ,
                      @ThueSuatGTGT ,
                      @ThueXNK ,
                      @ThueTTDB ,
                      @ThueGTGT ,
                      @PhuThu ,
                      @TyLeThuKhac ,
                      @TriGiaThuKhac ,
                      @MienThue    
                    )     
        END    
    
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_LayThongTin_TKSua_ChuaCapNhat]'
GO
CREATE PROC p_LayThongTin_TKSua_ChuaCapNhat
(
	@MaHaiQuan VARCHAR(10),
	@MaDoanhNghiep VARCHAR(30)
)
AS
/*
Hungtq created 24/08/2012.
Lay danh sach to khai sua da duyet chua cap nhat thong tin moi so voi to khai chinh.
*/
begin

SELECT MaHaiQuan, SoToKhai, MaLoaiHinh, NamDangKy 
FROM 
(
	select b.MaHaiQuan, b.SoToKhai, b.MaLoaiHinh, b.NamDangKy, b.MaPhu, b.SoLuong AS SoLuongMoi, a.SoLuong AS SoLuongCu, a.SoThuTuHang from 
	(
		select t.SoToKhai, t.MaLoaiHinh, year(t.NgayDangKy) as NamDangKy, h.MaPhu, t.MaHaiQuan, t.MaDoanhNghiep, h.SoLuong, h.SoThuTuHang
		from t_KDT_HangMauDich h 
			inner join t_KDT_ToKhaiMauDich t on h.TKMD_ID = t.ID
		WHERE t.ID IN (SELECT ItemID FROM dbo.t_KDT_Messages WHERE TieuDeThongBao LIKE N'Tờ khai sửa được duyệt' GROUP BY ItemID)	
			AND t.TrangThaiXuLy = 1
	) as a 
	full join
	(
		select h.SoToKhai, h.MaLoaiHinh, h.NamDangKy, h.MaPhu, h.MaHaiQuan, h.SoLuong, h.SoThuTuHang
		from t_sxxk_HangMauDich h 
	) as b
	on a.SoToKhai = b.SoToKhai and a.MaLoaiHinh = b.MaLoaiHinh and a.NamDangKy = b.NamDangKy
		and a.MaHaiQuan = b.MaHaiQuan and a.MaPhu = b.MaPhu AND a.SoThuTuHang = b.SoThuTuHang
	where a.SoLuong <> b.SoLuong
	AND a.MaHaiQuan = @MaHaiQuan AND a.MaDoanhNghiep = @MaDoanhNghiep
) AS v
GROUP BY MaHaiQuan, SoToKhai, MaLoaiHinh, NamDangKy

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_LayThongTin_DinhMuc_DuThua]'
GO
CREATE PROC p_LayThongTin_DinhMuc_DuThua
    (
      @MaHaiQuan VARCHAR(10) ,
      @MaDoanhNghiep VARCHAR(30) ,
      @NamDangKy INT
    )
AS /*
Hungtq created 24/08/2012.
Lay danh sach Dinh muc bi du thua giua Da dang ky va Khai bao
*/

    BEGIN

        SELECT  t_SXXK_DinhMuc.MaSanPham AS MaSPDangKy ,
                t_SXXK_DinhMuc.MaNguyenPhuLieu AS MaNPL,
                dbo.t_SXXK_NguyenPhuLieu.Ten AS TenNPL ,
                dbo.t_SXXK_NguyenPhuLieu.MaHS,
				dbo.t_HaiQuan_DonViTinh.Ten AS DVT,
                temp.MaSanPham AS MaSPKhaiBao
        FROM    dbo.t_SXXK_DinhMuc
                LEFT JOIN ( SELECT  t_KDT_SXXK_DinhMuc.MaSanPham ,
                                    t_KDT_SXXK_DinhMuc.MaNguyenPhuLieu ,
                                    t_KDT_SXXK_DinhMucDangKy.TrangThaiXuLy
                            FROM    dbo.t_KDT_SXXK_DinhMuc
                                    INNER JOIN dbo.t_KDT_SXXK_DinhMucDangKy ON dbo.t_KDT_SXXK_DinhMuc.Master_ID = dbo.t_KDT_SXXK_DinhMucDangKy.ID
                            WHERE   YEAR(NgayTiepNhan) = @NamDangKy
                                    AND MaHaiQuan = @MaHaiQuan
                                    AND MaDoanhNghiep = @MaDoanhNghiep
                          ) AS temp ON dbo.t_SXXK_DinhMuc.MaSanPham = temp.MaSanPham
                                       AND dbo.t_SXXK_DinhMuc.MaNguyenPhuLieu = temp.MaNguyenPhuLieu
                LEFT JOIN dbo.t_SXXK_NguyenPhuLieu ON t_SXXK_DinhMuc.MaNguyenPhuLieu = dbo.t_SXXK_NguyenPhuLieu.Ma
                LEFT JOIN dbo.t_HaiQuan_DonViTinh ON dbo.t_SXXK_NguyenPhuLieu.DVT_ID = dbo.t_HaiQuan_DonViTinh.ID
        WHERE   dbo.t_SXXK_DinhMuc.MaSanPham IN (
                SELECT  t_KDT_SXXK_DinhMuc.MaSanPham
                FROM    dbo.t_KDT_SXXK_DinhMuc
                        INNER JOIN dbo.t_KDT_SXXK_DinhMucDangKy ON dbo.t_KDT_SXXK_DinhMuc.Master_ID = dbo.t_KDT_SXXK_DinhMucDangKy.ID
                WHERE   YEAR(NgayTiepNhan) = @NamDangKy
                        AND MaHaiQuan = @MaHaiQuan
                        AND MaDoanhNghiep = @MaDoanhNghiep
                GROUP BY t_KDT_SXXK_DinhMuc.MaSanPham )
                AND temp.MaSanPham IS NULL
        ORDER BY dbo.t_SXXK_DinhMuc.MaSanPham	

    END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='4.1', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('4.1', getdate(), null)
	end 