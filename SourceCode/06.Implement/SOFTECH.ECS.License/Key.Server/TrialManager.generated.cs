using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace KeyServer
{
	public partial class TrialManager : ICloneable
	{
		#region Properties.
		
		public string ProductID { set; get; }
		public string MachineCode { set; get; }
		public string Status { set; get; }
		public DateTime ActivedDate { set; get; }
		public DateTime ExpiredDate { set; get; }
		public DateTime LastCheckStatus { set; get; }
		public string Description { set; get; }
		public string CustomerCode { set; get; }
		public string CustomerName { set; get; }
		public string CustomerAddress { set; get; }
		public string Notice { set; get; }
		public bool IsNotice { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<TrialManager> ConvertToCollection(IDataReader reader)
		{
			IList<TrialManager> collection = new List<TrialManager>();
			while (reader.Read())
			{
				TrialManager entity = new TrialManager();
				if (!reader.IsDBNull(reader.GetOrdinal("ProductID"))) entity.ProductID = reader.GetString(reader.GetOrdinal("ProductID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MachineCode"))) entity.MachineCode = reader.GetString(reader.GetOrdinal("MachineCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("Status"))) entity.Status = reader.GetString(reader.GetOrdinal("Status"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActivedDate"))) entity.ActivedDate = reader.GetDateTime(reader.GetOrdinal("ActivedDate"));
				if (!reader.IsDBNull(reader.GetOrdinal("ExpiredDate"))) entity.ExpiredDate = reader.GetDateTime(reader.GetOrdinal("ExpiredDate"));
				if (!reader.IsDBNull(reader.GetOrdinal("LastCheckStatus"))) entity.LastCheckStatus = reader.GetDateTime(reader.GetOrdinal("LastCheckStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("Description"))) entity.Description = reader.GetString(reader.GetOrdinal("Description"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerCode"))) entity.CustomerCode = reader.GetString(reader.GetOrdinal("CustomerCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerName"))) entity.CustomerName = reader.GetString(reader.GetOrdinal("CustomerName"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerAddress"))) entity.CustomerAddress = reader.GetString(reader.GetOrdinal("CustomerAddress"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notice"))) entity.Notice = reader.GetString(reader.GetOrdinal("Notice"));
				if (!reader.IsDBNull(reader.GetOrdinal("IsNotice"))) entity.IsNotice = reader.GetBoolean(reader.GetOrdinal("IsNotice"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_LM_TrialManager VALUES(@ProductID, @MachineCode, @Status, @ActivedDate, @ExpiredDate, @LastCheckStatus, @Description, @CustomerCode, @CustomerName, @CustomerAddress, @Notice, @IsNotice)";
            string update = "UPDATE t_LM_TrialManager SET ProductID = @ProductID, MachineCode = @MachineCode, Status = @Status, ActivedDate = @ActivedDate, ExpiredDate = @ExpiredDate, LastCheckStatus = @LastCheckStatus, Description = @Description, CustomerCode = @CustomerCode, CustomerName = @CustomerName, CustomerAddress = @CustomerAddress, Notice = @Notice, IsNotice = @IsNotice WHERE ID = @ID";
            string delete = "DELETE FROM t_LM_TrialManager WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ProductID", SqlDbType.VarChar, "ProductID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MachineCode", SqlDbType.NVarChar, "MachineCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Status", SqlDbType.VarChar, "Status", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActivedDate", SqlDbType.DateTime, "ActivedDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExpiredDate", SqlDbType.DateTime, "ExpiredDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LastCheckStatus", SqlDbType.DateTime, "LastCheckStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Description", SqlDbType.NVarChar, "Description", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerAddress", SqlDbType.NVarChar, "CustomerAddress", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notice", SqlDbType.NVarChar, "Notice", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IsNotice", SqlDbType.Bit, "IsNotice", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ProductID", SqlDbType.VarChar, "ProductID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MachineCode", SqlDbType.NVarChar, "MachineCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Status", SqlDbType.VarChar, "Status", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActivedDate", SqlDbType.DateTime, "ActivedDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExpiredDate", SqlDbType.DateTime, "ExpiredDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LastCheckStatus", SqlDbType.DateTime, "LastCheckStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Description", SqlDbType.NVarChar, "Description", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerAddress", SqlDbType.NVarChar, "CustomerAddress", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notice", SqlDbType.NVarChar, "Notice", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IsNotice", SqlDbType.Bit, "IsNotice", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ProductID", SqlDbType.VarChar, "ProductID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MachineCode", SqlDbType.NVarChar, "MachineCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_LM_TrialManager VALUES(@ProductID, @MachineCode, @Status, @ActivedDate, @ExpiredDate, @LastCheckStatus, @Description, @CustomerCode, @CustomerName, @CustomerAddress, @Notice, @IsNotice)";
            string update = "UPDATE t_LM_TrialManager SET ProductID = @ProductID, MachineCode = @MachineCode, Status = @Status, ActivedDate = @ActivedDate, ExpiredDate = @ExpiredDate, LastCheckStatus = @LastCheckStatus, Description = @Description, CustomerCode = @CustomerCode, CustomerName = @CustomerName, CustomerAddress = @CustomerAddress, Notice = @Notice, IsNotice = @IsNotice WHERE ID = @ID";
            string delete = "DELETE FROM t_LM_TrialManager WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ProductID", SqlDbType.VarChar, "ProductID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MachineCode", SqlDbType.NVarChar, "MachineCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Status", SqlDbType.VarChar, "Status", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActivedDate", SqlDbType.DateTime, "ActivedDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ExpiredDate", SqlDbType.DateTime, "ExpiredDate", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LastCheckStatus", SqlDbType.DateTime, "LastCheckStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Description", SqlDbType.NVarChar, "Description", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerAddress", SqlDbType.NVarChar, "CustomerAddress", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notice", SqlDbType.NVarChar, "Notice", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IsNotice", SqlDbType.Bit, "IsNotice", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ProductID", SqlDbType.VarChar, "ProductID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MachineCode", SqlDbType.NVarChar, "MachineCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Status", SqlDbType.VarChar, "Status", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActivedDate", SqlDbType.DateTime, "ActivedDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ExpiredDate", SqlDbType.DateTime, "ExpiredDate", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LastCheckStatus", SqlDbType.DateTime, "LastCheckStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Description", SqlDbType.NVarChar, "Description", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerAddress", SqlDbType.NVarChar, "CustomerAddress", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notice", SqlDbType.NVarChar, "Notice", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IsNotice", SqlDbType.Bit, "IsNotice", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ProductID", SqlDbType.VarChar, "ProductID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MachineCode", SqlDbType.NVarChar, "MachineCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static TrialManager Load(string productID, string machineCode)
		{
			const string spName = "[dbo].[p_LM_TrialManager_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, productID);
			db.AddInParameter(dbCommand, "@MachineCode", SqlDbType.NVarChar, machineCode);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<TrialManager> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<TrialManager> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<TrialManager> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LM_TrialManager_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_TrialManager_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LM_TrialManager_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_TrialManager_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertTrialManager(string productID, string machineCode, string status, DateTime activedDate, DateTime expiredDate, DateTime lastCheckStatus, string description, string customerCode, string customerName, string customerAddress, string notice, bool isNotice)
		{
			TrialManager entity = new TrialManager();	
			entity.ProductID = productID;
			entity.MachineCode = machineCode;
			entity.Status = status;
			entity.ActivedDate = activedDate;
			entity.ExpiredDate = expiredDate;
			entity.LastCheckStatus = lastCheckStatus;
			entity.Description = description;
			entity.CustomerCode = customerCode;
			entity.CustomerName = customerName;
			entity.CustomerAddress = customerAddress;
			entity.Notice = notice;
			entity.IsNotice = isNotice;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_LM_TrialManager_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, ProductID);
			db.AddInParameter(dbCommand, "@MachineCode", SqlDbType.NVarChar, MachineCode);
			db.AddInParameter(dbCommand, "@Status", SqlDbType.VarChar, Status);
			db.AddInParameter(dbCommand, "@ActivedDate", SqlDbType.DateTime, ActivedDate.Year <= 1753 ? DBNull.Value : (object) ActivedDate);
			db.AddInParameter(dbCommand, "@ExpiredDate", SqlDbType.DateTime, ExpiredDate.Year <= 1753 ? DBNull.Value : (object) ExpiredDate);
			db.AddInParameter(dbCommand, "@LastCheckStatus", SqlDbType.DateTime, LastCheckStatus.Year <= 1753 ? DBNull.Value : (object) LastCheckStatus);
			db.AddInParameter(dbCommand, "@Description", SqlDbType.NVarChar, Description);
			db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, CustomerCode);
			db.AddInParameter(dbCommand, "@CustomerName", SqlDbType.NVarChar, CustomerName);
			db.AddInParameter(dbCommand, "@CustomerAddress", SqlDbType.NVarChar, CustomerAddress);
			db.AddInParameter(dbCommand, "@Notice", SqlDbType.NVarChar, Notice);
			db.AddInParameter(dbCommand, "@IsNotice", SqlDbType.Bit, IsNotice);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<TrialManager> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (TrialManager item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateTrialManager(string productID, string machineCode, string status, DateTime activedDate, DateTime expiredDate, DateTime lastCheckStatus, string description, string customerCode, string customerName, string customerAddress, string notice, bool isNotice)
		{
			TrialManager entity = new TrialManager();			
			entity.ProductID = productID;
			entity.MachineCode = machineCode;
			entity.Status = status;
			entity.ActivedDate = activedDate;
			entity.ExpiredDate = expiredDate;
			entity.LastCheckStatus = lastCheckStatus;
			entity.Description = description;
			entity.CustomerCode = customerCode;
			entity.CustomerName = customerName;
			entity.CustomerAddress = customerAddress;
			entity.Notice = notice;
			entity.IsNotice = isNotice;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_LM_TrialManager_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, ProductID);
			db.AddInParameter(dbCommand, "@MachineCode", SqlDbType.NVarChar, MachineCode);
			db.AddInParameter(dbCommand, "@Status", SqlDbType.VarChar, Status);
			db.AddInParameter(dbCommand, "@ActivedDate", SqlDbType.DateTime, ActivedDate.Year <= 1753 ? DBNull.Value : (object) ActivedDate);
			db.AddInParameter(dbCommand, "@ExpiredDate", SqlDbType.DateTime, ExpiredDate.Year <= 1753 ? DBNull.Value : (object) ExpiredDate);
			db.AddInParameter(dbCommand, "@LastCheckStatus", SqlDbType.DateTime, LastCheckStatus.Year <= 1753 ? DBNull.Value : (object) LastCheckStatus);
			db.AddInParameter(dbCommand, "@Description", SqlDbType.NVarChar, Description);
			db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, CustomerCode);
			db.AddInParameter(dbCommand, "@CustomerName", SqlDbType.NVarChar, CustomerName);
			db.AddInParameter(dbCommand, "@CustomerAddress", SqlDbType.NVarChar, CustomerAddress);
			db.AddInParameter(dbCommand, "@Notice", SqlDbType.NVarChar, Notice);
			db.AddInParameter(dbCommand, "@IsNotice", SqlDbType.Bit, IsNotice);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<TrialManager> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (TrialManager item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateTrialManager(string productID, string machineCode, string status, DateTime activedDate, DateTime expiredDate, DateTime lastCheckStatus, string description, string customerCode, string customerName, string customerAddress, string notice, bool isNotice)
		{
			TrialManager entity = new TrialManager();			
			entity.ProductID = productID;
			entity.MachineCode = machineCode;
			entity.Status = status;
			entity.ActivedDate = activedDate;
			entity.ExpiredDate = expiredDate;
			entity.LastCheckStatus = lastCheckStatus;
			entity.Description = description;
			entity.CustomerCode = customerCode;
			entity.CustomerName = customerName;
			entity.CustomerAddress = customerAddress;
			entity.Notice = notice;
			entity.IsNotice = isNotice;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_TrialManager_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, ProductID);
			db.AddInParameter(dbCommand, "@MachineCode", SqlDbType.NVarChar, MachineCode);
			db.AddInParameter(dbCommand, "@Status", SqlDbType.VarChar, Status);
			db.AddInParameter(dbCommand, "@ActivedDate", SqlDbType.DateTime, ActivedDate.Year <= 1753 ? DBNull.Value : (object) ActivedDate);
			db.AddInParameter(dbCommand, "@ExpiredDate", SqlDbType.DateTime, ExpiredDate.Year <= 1753 ? DBNull.Value : (object) ExpiredDate);
			db.AddInParameter(dbCommand, "@LastCheckStatus", SqlDbType.DateTime, LastCheckStatus.Year <= 1753 ? DBNull.Value : (object) LastCheckStatus);
			db.AddInParameter(dbCommand, "@Description", SqlDbType.NVarChar, Description);
			db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, CustomerCode);
			db.AddInParameter(dbCommand, "@CustomerName", SqlDbType.NVarChar, CustomerName);
			db.AddInParameter(dbCommand, "@CustomerAddress", SqlDbType.NVarChar, CustomerAddress);
			db.AddInParameter(dbCommand, "@Notice", SqlDbType.NVarChar, Notice);
			db.AddInParameter(dbCommand, "@IsNotice", SqlDbType.Bit, IsNotice);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<TrialManager> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (TrialManager item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteTrialManager(string productID, string machineCode)
		{
			TrialManager entity = new TrialManager();
			entity.ProductID = productID;
			entity.MachineCode = machineCode;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_TrialManager_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, ProductID);
			db.AddInParameter(dbCommand, "@MachineCode", SqlDbType.NVarChar, MachineCode);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_LM_TrialManager_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<TrialManager> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (TrialManager item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}