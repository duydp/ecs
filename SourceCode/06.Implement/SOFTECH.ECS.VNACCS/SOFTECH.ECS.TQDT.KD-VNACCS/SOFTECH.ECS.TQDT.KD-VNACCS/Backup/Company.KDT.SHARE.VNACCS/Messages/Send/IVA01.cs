﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class IVA01 : BasicVNACC
    {

        public PropertiesAttribute NIV { get; set; }
        public IVA01()
        {
            NIV = new PropertiesAttribute(12, typeof(decimal));
        }
    }
}
