﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.GC;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System.Diagnostics;
using System.IO;

namespace Company.Interface.Report
{
    
    public partial class ReportViewTKNTQDTForm : BaseForm
    {
        public Company.Interface.Report.GC.TQDTToKhaiNK ToKhaiChinhReport = new Company.Interface.Report.GC.TQDTToKhaiNK();
        public Company.Interface.Report.GC.TQDTPhuLucToKhaiNhap PhuLucReport = new Company.Interface.Report.GC.TQDTPhuLucToKhaiNhap();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public XRControl Cell = new XRControl();
        int index = 0;
        private SaveFileDialog saveFileDialog1 = new SaveFileDialog();

        public ReportViewTKNTQDTForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            
            if (this.TKMD.HMDCollection.Count > 3)
            {
                int count = (this.TKMD.HMDCollection.Count - 1) / 9 + 1;
                for (int i = 0; i < count; i++)
                    this.AddItemComboBox();
            }
            cboToKhai.SelectedIndex = 0;

            
        }
        public void AddItemComboBox()
        {
            cboToKhai.Items.Add("Phụ lục " + cboToKhai.Items.Count, cboToKhai.Items.Count);
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                
                this.ToKhaiChinhReport.TKMD = this.TKMD;  
                this.ToKhaiChinhReport.report = this;
                this.ToKhaiChinhReport.BindReport();
                this.ToKhaiChinhReport.Margins.Top = 0;
                this.ToKhaiChinhReport.Margins.Bottom = 0;
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                
                try{
                      index = cboToKhai.SelectedIndex;
                }
                catch { index = 1; }
                HangMauDichCollection HMDReportCollection = new HangMauDichCollection();
                int begin = (cboToKhai.SelectedIndex - 1) * 9;
                int end = cboToKhai.SelectedIndex * 9;
                if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                for (int i = begin; i < end; i++)
                    HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                this.PhuLucReport = new Company.Interface.Report.GC.TQDTPhuLucToKhaiNhap();
                this.PhuLucReport.report = this;
                this.PhuLucReport.TKMD = this.TKMD;
                if(this.TKMD.NgayDangKy!= new DateTime(1900,1,1))
                    this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
                this.PhuLucReport.HMDCollection = HMDReportCollection;
                this.PhuLucReport.BindReport(index.ToString());
                this.PhuLucReport.Margins.Top = 0;
                this.PhuLucReport.Margins.Bottom = 0;
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {

                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.PhuLucReport.CreateDocument();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //switch (cboExport.SelectedValue.ToString())
            //{
            //    case "pdf":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportPdf, new object[] { true });
            //        break;
            //    case "excel":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            //        break;

            //}
        }


        private void uiButton3_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                if (txtTenNhomHang.Text != "")
                    this.ToKhaiChinhReport.setThongTin(this.Cell, txtTenNhomHang.Text);
                if (txtDeXuatKhac.Text != "")
                    this.ToKhaiChinhReport.setDeXuatKhac(txtDeXuatKhac.Text);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.PhuLucReport.setNhomHang(this.Cell, txtTenNhomHang.Text);
                this.PhuLucReport.CreateDocument();
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {

        }

        private void chkInMaHang_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                if (chkInMaHang.Checked)
                {
                    this.ToKhaiChinhReport.inMaHang = true;
                }
                else
                {
                    this.ToKhaiChinhReport.inMaHang = false;
                }

                this.ToKhaiChinhReport.BindReport();
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                if (chkInMaHang.Checked)
                {
                    this.PhuLucReport.inMaHang = true;
                }
                else
                {
                    this.PhuLucReport.inMaHang = false;
                }

                this.PhuLucReport.BindReport(index.ToString());
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }

        private void chkHinhNen_CheckedChanged(object sender, EventArgs e)
        {

        }

 

        private void chkInBanLuuHaiQuan_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.ToKhaiChinhReport.BindReport();
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                //this.PhuLucReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.PhuLucReport.BindReport(index.ToString());
                this.PhuLucReport.CreateDocument();

            }
        }

        private void chkMienThue1_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.ToKhaiChinhReport.MienThue1 = chkMienThue1.Checked;
                this.ToKhaiChinhReport.MienThue2 = chkMienThue2.Checked;
                this.ToKhaiChinhReport.BindReport();
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                this.PhuLucReport.MienThue1 = chkMienThue1.Checked;
                this.PhuLucReport.MienThue2 = chkMienThue2.Checked;
                this.PhuLucReport.BindReport(cboToKhai.Items.Count.ToString());
                this.PhuLucReport.CreateDocument();

            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.CreateDocument();
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            else
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
        }

        private void btnPDF_Click(object sender, EventArgs e)
        {
            if (this.cboToKhai.SelectedIndex == 0)
            {
                saveFileDialog1.Filter = "PDF Files (*.pdf)|*.pdf|All Files (*.*)|*.*";
                saveFileDialog1.FilterIndex = 1;
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    ToKhaiChinhReport.ExportToPdf(saveFileDialog1.FileName);
                    if (File.Exists(saveFileDialog1.FileName))
                    {
                        Process.Start(saveFileDialog1.FileName);
                    }
                }
            }
            else
            {
                saveFileDialog1.Filter = "PDF Files (*.pdf)|*.pdf|All Files (*.*)|*.*";
                saveFileDialog1.FilterIndex = 1;
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    PhuLucReport.ExportToPdf(saveFileDialog1.FileName);
                    if (File.Exists(saveFileDialog1.FileName))
                    {
                        Process.Start(saveFileDialog1.FileName);
                    }
                }
            }
        }
        
    }
}