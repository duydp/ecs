﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.KDT.SHARE.Components
{
    public class MessageContents
    {
        public const string ChuaNhanDuocPhanHoi = "Chưa nhận được phản hồi từ hệ thống Hải quan";

    }

    public partial class Message
    {

        public static List<Message> SelectCollectionBy_ItemID(long itemID)
        {
            return SelectCollectionBy_ItemID(itemID, string.Empty);
        }

        public static List<Message> SelectCollectionBy_ItemID(long itemID, string msgType)
        {

            string whereCondition = string.Format("ItemID = {0}", itemID);
            string orderByExpression = "CreatedTime DESC";
            //Comment by HUNGTQ 26/07/2012.
            //if (!string.IsNullOrEmpty(msgType)) whereCondition += " and MessageType="+ msgType;
            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        public static List<Message> LayToKhaiGocTruocKhiSua_ItemID(long itemID)
        {
            string whereCondition = string.Format("TieuDeThongBao = N'Thay đổi trạng thái' AND NoiDungThongBao = N'Thay đổi sang trạng thái sửa tờ khai' AND ItemID = {0}", itemID);
            string orderByExpression = "CreatedTime DESC";
            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        public static List<Message> SelectCollectionBy_ReferenceID(Guid referenceID)
        {
            string whereCondition = string.Format("ReferenceID = '{0}'", referenceID);
            string orderByExpression = "CreatedTime ASC";

            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        private static List<Message> SelectCollectionBy_ReferenceID_LoaiThongDiep(Guid referenceID, string tieuDeThongBao)
        {
            string whereCondition = string.Format("ReferenceID = '{0}' AND TieuDeThongBao = N'{1}'", referenceID, tieuDeThongBao);
            string orderByExpression = "CreatedTime ASC";

            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        private static List<Message> SelectCollectionBy_ItemID_LoaiThongDiep(long itemID, string tieuDeThongBao)
        {
            string whereCondition = string.Format("ItemID = {0} AND TieuDeThongBao = N'{1}'", itemID, tieuDeThongBao);
            string orderByExpression = "CreatedTime ASC";

            return SelectCollectionDynamic(whereCondition, orderByExpression);
        }

        public static string LayThongDiep(Guid referenceID, string tieuDeThongBao)
        {
            IList<Message> list = SelectCollectionBy_ReferenceID_LoaiThongDiep(referenceID, tieuDeThongBao);
            string msg = string.Empty;
            if (list.Count > 0)
            {
                msg = list[list.Count - 1].TieuDeThongBao + ":\r\n\r\n";
                msg += list[list.Count - 1].NoiDungThongBao;
            }
            return msg;
        }

        public static string LayThongDiep(long itemID, string loaiThongDiep)
        {
            IList<Message> list = SelectCollectionBy_ItemID(itemID, loaiThongDiep);
            //SelectCollectionBy_ItemID_LoaiThongDiep(itemID, loaiThongDiep);
            string msg = string.Empty;

            if (list.Count > 0)
            {
                msg = list[list.Count - 1].TieuDeThongBao + ":\r\n\r\n";
                msg += list[list.Count - 1].NoiDungThongBao;
            }
            return msg;
        }

        /// <summary>
        /// Lấy nội dung Xml.
        /// </summary>
        /// <param name="referenceID">Reference ID</param>
        /// <param name="messageType">Message Type</param>
        /// <param name="messageFunction">Message Function</param>
        /// <returns></returns>
        public static string LayNoiDungXml(Guid referenceID, MessageTypes messageType, MessageFunctions messageFunction)
        {
            string whereCondition = string.Format("ReferenceID = '{0}' AND MessageType = {1} AND MessageFunction = {2}", referenceID, (int)messageType, (int)messageFunction);
            string orderByExpression = "ID DESC";
            IList<Message> collection = SelectCollectionDynamic(whereCondition, orderByExpression);
            if (collection.Count > 0)
            {
                return collection[0].MessageContent;
            }
            return string.Empty;
        }

        public static string CheckNoiDungXml(Guid referenceID, MessageTypes messageType, MessageFunctions messageFunction, string msgTitle)
        {
            string whereCondition = string.Format("ReferenceID = '{0}' AND MessageType = {1} AND MessageFunction = {2} AND TieuDeThongBao = N'{3}'", referenceID, (int)messageType, (int)messageFunction, msgTitle);
            string orderByExpression = "ID DESC";
            IList<Message> collection = SelectCollectionDynamic(whereCondition, orderByExpression);
            if (collection.Count > 0)
            {
                return collection[0].MessageContent;
            }
            return string.Empty;
        }

        public static List<Message> SelectCollectionBy_ItemID_2(long itemID, string msgType)
        {

            string whereCondition = string.Format("ItemID = {0} and MessageType= {1} ", itemID,msgType);
            string orderByExpression = "CreatedTime DESC";
            //Comment by HUNGTQ 26/07/2012.
            //if (!string.IsNullOrEmpty(msgType)) whereCondition += " and MessageType="+ msgType;
            string query = string.Format("select ID,TieuDeThongBao,NoiDungThongBao,CreatedTime,ReferenceID,MessageFunction from [t_KDT_Messages] where  {0} order by {1}", whereCondition, orderByExpression);
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(query);
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            List<Message> collection = new List<Message>();
            while (reader.Read())
            {
                Message entity = new Message();
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                }
                catch (Exception)
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                }
                if (!reader.IsDBNull(reader.GetOrdinal("ReferenceID"))) entity.ReferenceID = reader.GetGuid(reader.GetOrdinal("ReferenceID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreatedTime"))) entity.CreatedTime = reader.GetDateTime(reader.GetOrdinal("CreatedTime"));
                if (!reader.IsDBNull(reader.GetOrdinal("TieuDeThongBao"))) entity.TieuDeThongBao = reader.GetString(reader.GetOrdinal("TieuDeThongBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDungThongBao"))) entity.NoiDungThongBao = reader.GetString(reader.GetOrdinal("NoiDungThongBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageFunction"))) entity.MessageFunction = (MessageFunctions)reader.GetInt32(reader.GetOrdinal("MessageFunction"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
    }
}
