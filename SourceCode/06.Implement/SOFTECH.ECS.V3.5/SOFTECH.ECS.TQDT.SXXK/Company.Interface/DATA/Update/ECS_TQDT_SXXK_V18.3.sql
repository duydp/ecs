IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='33PDA01')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','33PDA01','CANG HKQT PHU BAI')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='20CDX01')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','20CDX01','CN YAZAKI QUANG NINH')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='20CGC09')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','20CGC09','CTY THANH LAM')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CDO06')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CDO06','CTY NGU KIM FORMOSA')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CDC97')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CDC97','CTY TCMN THAI BINH')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CDC98')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CDC98','CTY DATEX')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CDC99')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CDC99','CTY XNK TMDV - MTA')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CDCE0')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CDCE0','CTY CN HUNG YI')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CDCE1')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CDCE1','CTY VCAVA VINA')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CDCE2')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CDCE2','CTY LIEN THANG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CDCE3')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CDCE3','CTY PACIFIC OCEAN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CDCE4')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CDCE4','CTY TRUONG SON THINH')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CDCE5')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CDCE5','CTY MAY TUONG AN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CDCE6')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CDCE6','CTY MAY XK BINH LAN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CDCE7')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CDCE7','CTY DAT GIA CN TBINH')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CDCE8')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CDCE8','CTY CBTS THUY HAI')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CDCE9')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CDCE9','CTY HT SONG LONG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CDCEA')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CDCEA','CTY XNK COYATO')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC07')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC07','CTY CHAU GIANG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC08')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC08','CTY DVTM THAI ANH')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC09')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC09','CTY TRANG SUC GL')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC10')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC10','CTY CO KHI VIET NHAT')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC11')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC11','CTY CAP DIEN HT LS')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC12')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC12','CTY NAM HOA')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC13')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC13','DONG TAU SONG CAM 1')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC14')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC14','CTY KAI NAN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC15')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC15','CTY NHUA CO KHI HP')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC16')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC16','CTY NHUA CHIN HUEI')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC17')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC17','IHI INFRASTRUCTURE')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC18')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC18','CTY DINH VANG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC19')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC19','CTY SAO VANG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC20')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC20','CTY ANH VANG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC21')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC21','LG ELECTRONICS VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC22')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC22','CTY PI VIET NAM')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC23')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC23','CTY NU TRANG D&Q')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC24')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC24','YOSHINO DENKA KOGYO')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC25')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC25','NHUA TN TIEN PHONG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC26')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC26','CTY CO DTPTKT HP')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC27')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC27','CTY TMKT DONG PHUONG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC28')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC28','CTY XNK THUY SAN HP')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC29')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC29','CTY JIKA JIKA')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC30')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC30','CTY CNN DOOSAN VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC31')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC31','CTY XI MANG CHINFON')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC32')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC32','CTY ENVIRONSTAR')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC33')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC33','CTY GIAI LAC')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC34')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC34','CTY LICHI VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC35')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC35','CTY SUNMAX VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC36')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC36','CTY THANH HUNG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC37')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC37','CTY DAMEN-SONG CAM')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PAC38')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PAC38','DONG TAU SONG CAM 2')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03NKC95')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03NKC95','DAU NHON IDEMITSU VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03NKC96')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03NKC96','HOA DAU XO SOI DK')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03NKC97')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03NKC97','GAS HOA LONG VAN LOC')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03NKC98')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03NKC98','JX NIPPON OIL ENERGY')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03NKC99')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03NKC99','CTY MAY VP KYOCERA')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03NKCE0')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03NKCE0','CTY JFE SHOJI HP')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03NKCE1')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03NKCE1','CTY KORG VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03NKCE2')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03NKCE2','CTY LG ELECTRONICS')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLO05')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLO05','CN CARGILL HUNG YEN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLO06')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLO06','CN ONG THEP HOA PHAT')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLO07')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLO07','CTY ABC VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLO08')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLO08','CTY TACN THAI DUONG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC78')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC78','CTY BEEAHN VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC79')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC79','CT CP DTTM HUNG PHAT')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC80')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC80','CTY HUNG PHAT T&M')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC81')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC81','CTY MAY DV HUNG LONG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC82')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC82','CTY PHU HUNG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC83')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC83','CTY TIEN HUNG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC84')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC84','CTY VINH THANH')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC85')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC85','CTY AOCC VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC86')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC86','CTY GIAY TIEN THANH')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC87')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC87','CTY HANESBRANDS VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC88')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC88','CTY HANSUNG HARAM VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC89')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC89','CTY JNC FILTER VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC90')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC90','CTY SMART SHIRT')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC91')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC91','CTY NGOC THIEN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC92')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC92','CTY QUANG DUC PHONG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC93')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC93','CTY TAKAGI VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC94')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC94','CTY NHAT QUANG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC95')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC95','CTY YEN HUNG-AN THI')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC96')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC96','CTY DAY&MAY HUNG YEN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC97')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC97','CTY CP DUC THANG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PLC98')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PLC98','CTY GG VIETNAM')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PJC71')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PJC71','CTY MAY XK SSV')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PJC72')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PJC72','CTY CHI HUA')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PJC73')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PJC73','CTY DIEN TU POYUN VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PJC74')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PJC74','CTY HK VINA')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PJC75')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PJC75','CTY HUNG DUNG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PJC76')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PJC76','CTY MATEX VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PJC77')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PJC77','CTY MAY BROTHER VN 2')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PJC78')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PJC78','CTY MAY EVER - GLORY')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PJC79')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PJC79','CTY NAMYANG DELTA')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PJC80')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PJC80','CTY VN TOYO DENSO')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PJC81')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PJC81','CTY SEES VINA')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PJC82')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PJC82','DNTN NT THANH BINH')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PJC83')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PJC83','CTY POSCO - VNPC')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03PJC84')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03PJC84','CTY SANYU SEIMITSU C')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CCO04')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CCO04','CTY TM VIC')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='03CCO05')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','03CCO05','CTY THEP MINH THANH')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='47D3W02')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','47D3W02','KNQ LOGISTICS SOJITZ')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='47NBW01')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','47NBW01','KNQ CANG DONG NAI')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C22')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C22','CTY HOA CHAT HIEM VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C23')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C23','CTY AIR WATER VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C24')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C24','CTY KINH NSG VIETNAM')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C25')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C25','CTY THEP VINA KYOEI')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C26')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C26','TCTY PHAT DIEN 3')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C27')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C27','CTY NHOM TOAN CAU')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C28')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C28','CTY SAN FANG VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C29')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C29','CTY TE AN VIETNAM')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C30')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C30','CTY TONG HONG TANERY')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C31')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C31','CTY XAY LUA MI VN 1')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C32')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C32','CTY SHENG SHING')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C33')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C33','CTY DET TAH TONG VN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C34')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C34','CTY GS MY XUAN')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C35')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C35','CTY JLG VIETNAM')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C36')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C36','CTY BACONCO')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C37')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C37','CTY NANG LUONG IREX')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C38')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C38','CTY THAI BINH DUONG')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C39')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C39','CTY XAY LUA MI VN 2')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51C1C40')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51C1C40','TCTY CN DAU THUC VAT')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51CIC04')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51CIC04','CTY CAO PHAT')
IF NOT EXISTS(Select * From t_VNACC_Category_Cargo where BondedAreaCode='51CBC22')
INSERT INTO t_VNACC_Category_Cargo(TableID,BondedAreaCode,BondedAreaName) Values('A202A','51CBC22','CTY DVCKHH PTSC')

GO
IF NOT EXISTS(Select * From t_VNACC_Category_CityUNLOCODE where LOCODE='VNITSL' and CountryCode='VN')
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) values('A016A','VNITSL','VN','ICD TIEN SON BAC NINH ',N'ICD Tiên Sơn - Bắc Ninh') 
IF NOT EXISTS(Select * From t_VNACC_Category_CityUNLOCODE where LOCODE='VNATH' and CountryCode='VN')
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) values('A016A','VNATH','VN','CANG AN THOI',N'Cảng An Thới') 
IF NOT EXISTS(Select * From t_VNACC_Category_CityUNLOCODE where LOCODE='VNHTE' and CountryCode='VN')
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) values('A016A','VNHTE','VN','KHU CT PD HA TIEN',N'Khu chuyển tải pháo đài Hà Tiên') 

Go

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCXuatNhapTon_SelectAll]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 28, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_BCXuatNhapTon_GetBCXNTByLanThanhLy]
	@MaDoanhNghiep NVarchar(14),
	@LanThanhLy int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

--SELECT
--	SoToKhaiNhap,
--	NgayDangKyNhap,
--	MaNPL,
--	TenNPL, 
--	LuongTonDau,
--	CASE when  LuongNhap = LuongTonDau then LuongNhap else 0 end as LuongNhap, 
--	Sum(LuongNPLSuDung) as LuongNPLSuDung, 
--	Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat, 
--	0 as LuongNopThue,
--	0 as LuongXuatTheoHD,
--	--case 
--	--	when (LuongTonDau - Sum(LuongNPLSuDung)) >= 0 
--	--	then  LuongTonDau - Sum(LuongNPLSuDung)
--	--	else 0
--	-- end 
--	-- as LuongTonCuoi,
--	Case When Min(LuongTonCuoi) < 0 then 0 else Min(LuongTonCuoi) end as LuongTonCuoi,
--	(select ID from t_HaiQuan_DonViTinh where ten=TenDVT_NPL)as ID_DVT_NPL,
--	DonGiaTT
		
--FROM
--	[dbo].[t_KDT_SXXK_BCXuatNhapTon]
--WHERE MaDoanhNghiep =@MaDoanhNghiep AND LanThanhLy =@LanThanhLy
--GROUP BY NgayDangKyNhap, SoToKhaiNhap, MaLoaiHinhNhap, MaNPL,TenNPL, TenDVT_NPL,DonGiaTT, LuongTonDau,LuongNhap
--order by MaNPL,SoToKhaiNhap,NgayDangKyNhap

Select 
	MaNPL,
	MAX(TenNPL) as TenNPL, 
	SUM(LuongTonDau) as LuongTonDau,
	Sum(LuongNhap) as LuongNhap,
	Sum(LuongNPLSuDung) as LuongNPLSuDung,
	Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,
	Sum(LuongNopThue) as LuongNopThue,
	Sum(LuongXuatTheoHD) as LuongXuatTheoHD,
	Sum(LuongTonCuoi) as LuongTonCuoi,
	ID_DVT_NPL,
	DonGiaTT
	From 
(	
SELECT
	SoToKhaiNhap,
	NgayDangKyNhap,
	MaNPL,
	TenNPL, 
	LuongTonDau,
	CASE when  LuongNhap = LuongTonDau then LuongNhap else 0 end as LuongNhap, 
	Sum(Case when ( LuongNPLSuDung + LuongTonCuoi) > LuongTonDau then LuongTonDau - LuongTonCuoi else LuongNPLSuDung end)  as LuongNPLSuDung, 
	Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat, 
	0 as LuongNopThue,
	0 as LuongXuatTheoHD,
	--case 
	--	when (LuongTonDau - Sum(LuongNPLSuDung)) >= 0 
	--	then  LuongTonDau - Sum(LuongNPLSuDung)
	--	else 0
	-- end 
	-- as LuongTonCuoi,
	Case When Min(LuongTonCuoi) < 0 then 0 else Min(LuongTonCuoi) end as LuongTonCuoi,
	(select ID from t_HaiQuan_DonViTinh where ten=TenDVT_NPL)as ID_DVT_NPL,
	DonGiaTT
		
FROM
	[dbo].[t_KDT_SXXK_BCXuatNhapTon]
WHERE MaDoanhNghiep =@MaDoanhNghiep AND LanThanhLy =@LanThanhLy
GROUP BY NgayDangKyNhap, SoToKhaiNhap, MaLoaiHinhNhap, MaNPL,TenNPL, TenDVT_NPL,DonGiaTT, LuongTonDau,LuongNhap
) as A
Group by MaNPL,DonGiaTT,ID_DVT_NPL
order by MaNPL

Go

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '18.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('18.3',GETDATE(), N' Cap nhat ma luu kho cho thong quan, store p_KDT_SXXK_BCXuatNhapTon_GetBCXNTByLanThanhLy ')
END	






