using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.BLL.DuLieuChuan
{
    public partial class DoiTac : EntityBase
    {
        #region Private members.

        protected long _ID;
        protected string _MaCongTy = String.Empty;
        protected string _TenCongTy = String.Empty;
        protected string _DiaChi = String.Empty;
        protected string _DienThoai = String.Empty;
        protected string _Email = String.Empty;
        protected string _Fax = String.Empty;
        protected string _GhiChu = String.Empty;
        protected string _MaDoanhNghiep = String.Empty;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public string MaCongTy
        {
            set { this._MaCongTy = value; }
            get { return this._MaCongTy; }
        }
        public string TenCongTy
        {
            set { this._TenCongTy = value; }
            get { return this._TenCongTy; }
        }
        public string DiaChi
        {
            set { this._DiaChi = value; }
            get { return this._DiaChi; }
        }
        public string DienThoai
        {
            set { this._DienThoai = value; }
            get { return this._DienThoai; }
        }
        public string Email
        {
            set { this._Email = value; }
            get { return this._Email; }
        }
        public string Fax
        {
            set { this._Fax = value; }
            get { return this._Fax; }
        }
        public string GhiChu
        {
            set { this._GhiChu = value; }
            get { return this._GhiChu; }
        }
        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_HaiQuan_DoiTac_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCongTy"))) this._MaCongTy = reader.GetString(reader.GetOrdinal("MaCongTy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCongTy"))) this._TenCongTy = reader.GetString(reader.GetOrdinal("TenCongTy"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaChi"))) this._DiaChi = reader.GetString(reader.GetOrdinal("DiaChi"));
                if (!reader.IsDBNull(reader.GetOrdinal("DienThoai"))) this._DienThoai = reader.GetString(reader.GetOrdinal("DienThoai"));
                if (!reader.IsDBNull(reader.GetOrdinal("Email"))) this._Email = reader.GetString(reader.GetOrdinal("Email"));
                if (!reader.IsDBNull(reader.GetOrdinal("Fax"))) this._Fax = reader.GetString(reader.GetOrdinal("Fax"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) this._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------



        public DataSet SelectAll()
        {
            string spName = "p_HaiQuan_DoiTac_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_HaiQuan_DoiTac_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_HaiQuan_DoiTac_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_HaiQuan_DoiTac_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DoiTacCollection SelectCollectionAll()
        {
            DoiTacCollection collection = new DoiTacCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                DoiTac entity = new DoiTac();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCongTy"))) entity.MaCongTy = reader.GetString(reader.GetOrdinal("MaCongTy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCongTy"))) entity.TenCongTy = reader.GetString(reader.GetOrdinal("TenCongTy"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaChi"))) entity.DiaChi = reader.GetString(reader.GetOrdinal("DiaChi"));
                if (!reader.IsDBNull(reader.GetOrdinal("DienThoai"))) entity.DienThoai = reader.GetString(reader.GetOrdinal("DienThoai"));
                if (!reader.IsDBNull(reader.GetOrdinal("Email"))) entity.Email = reader.GetString(reader.GetOrdinal("Email"));
                if (!reader.IsDBNull(reader.GetOrdinal("Fax"))) entity.Fax = reader.GetString(reader.GetOrdinal("Fax"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DoiTacCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            DoiTacCollection collection = new DoiTacCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                DoiTac entity = new DoiTac();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCongTy"))) entity.MaCongTy = reader.GetString(reader.GetOrdinal("MaCongTy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCongTy"))) entity.TenCongTy = reader.GetString(reader.GetOrdinal("TenCongTy"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaChi"))) entity.DiaChi = reader.GetString(reader.GetOrdinal("DiaChi"));
                if (!reader.IsDBNull(reader.GetOrdinal("DienThoai"))) entity.DienThoai = reader.GetString(reader.GetOrdinal("DienThoai"));
                if (!reader.IsDBNull(reader.GetOrdinal("Email"))) entity.Email = reader.GetString(reader.GetOrdinal("Email"));
                if (!reader.IsDBNull(reader.GetOrdinal("Fax"))) entity.Fax = reader.GetString(reader.GetOrdinal("Fax"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_HaiQuan_DoiTac_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@MaCongTy", SqlDbType.VarChar, this._MaCongTy);
            this.db.AddInParameter(dbCommand, "@TenCongTy", SqlDbType.VarChar, this._TenCongTy);
            this.db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, this._DiaChi);
            this.db.AddInParameter(dbCommand, "@DienThoai", SqlDbType.VarChar, this._DienThoai);
            this.db.AddInParameter(dbCommand, "@Email", SqlDbType.VarChar, this._Email);
            this.db.AddInParameter(dbCommand, "@Fax", SqlDbType.VarChar, this._Fax);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(DoiTacCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (DoiTac item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, DoiTacCollection collection)
        {
            foreach (DoiTac item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_HaiQuan_DoiTac_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaCongTy", SqlDbType.VarChar, this._MaCongTy);
            this.db.AddInParameter(dbCommand, "@TenCongTy", SqlDbType.VarChar, this._TenCongTy);
            this.db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, this._DiaChi);
            this.db.AddInParameter(dbCommand, "@DienThoai", SqlDbType.VarChar, this._DienThoai);
            this.db.AddInParameter(dbCommand, "@Email", SqlDbType.VarChar, this._Email);
            this.db.AddInParameter(dbCommand, "@Fax", SqlDbType.VarChar, this._Fax);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

   

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_HaiQuan_DoiTac_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaCongTy", SqlDbType.VarChar, this._MaCongTy);
            this.db.AddInParameter(dbCommand, "@TenCongTy", SqlDbType.VarChar, this._TenCongTy);
            this.db.AddInParameter(dbCommand, "@DiaChi", SqlDbType.NVarChar, this._DiaChi);
            this.db.AddInParameter(dbCommand, "@DienThoai", SqlDbType.VarChar, this._DienThoai);
            this.db.AddInParameter(dbCommand, "@Email", SqlDbType.VarChar, this._Email);
            this.db.AddInParameter(dbCommand, "@Fax", SqlDbType.VarChar, this._Fax);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(DoiTacCollection collection, SqlTransaction transaction)
        {
            foreach (DoiTac item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_HaiQuan_DoiTac_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(DoiTacCollection collection, SqlTransaction transaction)
        {
            foreach (DoiTac item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(DoiTacCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (DoiTac item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion
    }
}