﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.VNACCS;
using Company.Interface.Report.WareHouse;
using Company.Interface.DanhMucChuan;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseExportForm : BaseFormHaveGuidPanel
    {
        public T_KHOKETOAN_PHIEUXNKHO PhieuXNKho = new T_KHOKETOAN_PHIEUXNKHO();
        public T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
        public HopDong HD;
        public KDT_VNACC_ToKhaiMauDich TKMD;
        public string LOAICT = "";
        string SoChungTu = "";
        public WareHouseExportForm()
        {
            InitializeComponent();
            cbbMaKho.DataSource = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
            cbbMaKho.DisplayMember = "MAKHO";
            cbbMaKho.ValueMember = "MAKHO";
        }

        private void WareHouseExportForm_Load(object sender, EventArgs e)
        {            
            cbbMaKho.SelectedIndex = 0;
            cbbPhanBo.SelectedIndex = 0;
            cbbMaNT.Code = "VND";
            txtMa.Text = GlobalSettings.MA_DON_VI;
            txtTen.Text = GlobalSettings.TEN_DON_VI;
            txtDiaChi.Text = GlobalSettings.DIA_CHI;
            cbbLoaiChungTu.SelectedValue = LOAICT;
            List<T_KHOKETOAN_QUYTACDANHSO> ConfigCollection = T_KHOKETOAN_QUYTACDANHSO.SelectCollectionAll();

            foreach (T_KHOKETOAN_QUYTACDANHSO item in ConfigCollection)
            {
                if (item.LOAICHUNGTU == "PNK" && LOAICT == "N")
                {
                    SoChungTu = item.HIENTHI.Substring(0, item.HIENTHI.Length - 1);
                    string GetSoCT = T_KHOKETOAN_PHIEUXNKHO.GetChungTu(LOAICT);
                    if (!String.IsNullOrEmpty(GetSoCT))
                    {
                        if (GetSoCT.Substring(0, SoChungTu.Length) == SoChungTu)
                        {
                            int LastIndex = Convert.ToInt32(GetSoCT.Substring(SoChungTu.Length, GetSoCT.Length - SoChungTu.Length));
                            SoChungTu = SoChungTu + (LastIndex + 1);
                            txtSoCT.Text = SoChungTu;
                        }
                        else
                        {
                            SoChungTu = item.HIENTHI;
                            txtSoCT.Text = SoChungTu;
                        }
                    }
                    else
                    {
                        SoChungTu = item.HIENTHI;
                        txtSoCT.Text = SoChungTu;
                    }
                    cbbLoaiHang.SelectedValue = "N";
                }
                else if (item.LOAICHUNGTU == "PXK" && LOAICT == "X")
                {
                    SoChungTu = item.HIENTHI.Substring(0, item.HIENTHI.Length - 1);
                    string GetSoCT = T_KHOKETOAN_PHIEUXNKHO.GetChungTu(LOAICT);
                    if (!String.IsNullOrEmpty(GetSoCT))
                    {
                        if (GetSoCT.Substring(0, SoChungTu.Length) == SoChungTu)
                        {
                            int LastIndex = Convert.ToInt32(GetSoCT.Substring(SoChungTu.Length, GetSoCT.Length - SoChungTu.Length));
                            SoChungTu = SoChungTu + (LastIndex + 1);
                            txtSoCT.Text = SoChungTu;
                        }
                        else
                        {
                            SoChungTu = item.HIENTHI;
                            txtSoCT.Text = SoChungTu;
                        }
                    }
                    else
                    {
                        SoChungTu = item.HIENTHI;
                        txtSoCT.Text = SoChungTu;
                    }
                    cbbLoaiHang.SelectedValue = "S";
                }
            }
            if (HD != null)
            {
                txtSoHopDong.Text = HD.SoHopDong;
                clcNgayHopDong.Value = HD.NgayDangKy;
            }
            if (PhieuXNKho.ID > 0)
            {
                SetPhieuXNKho();
                BindData();
                txtPhiVC_TextChanged(null,null);
            }
            else
            {
                // Set Value mặc định dựa vào Thông số cấu hình kho 
            }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdAdd":
                    Add();
                    break;
                case "cmdAddExcel":
                    AddExcel();
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdPrint":
                    Print();
                    break;
                default:
                    break;
            }
        }

        private void Print()
        {
            try
            {
                if (PhieuXNKho.LOAICHUNGTU=="N")
                {
                    ReportViewPhieuNhapKho f = new ReportViewPhieuNhapKho();
                    f.PhieuXNKho = PhieuXNKho;
                    f.BindReport();
                    f.ShowPreview();
                }
                else
                {
                    ReportViewPhieuXuatKho f = new ReportViewPhieuXuatKho();
                    f.PhieuXNKho = PhieuXNKho;
                    f.BindReport();
                    f.ShowPreview();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtSoCT, errorProvider, " Số chứng từ ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcNgayCT, errorProvider, " Ngày chứng từ ");
                isValid &= ValidateControl.ValidateNull(txtSoCTGoc, errorProvider, " Số chứng từ gốc ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcNgayCTGoc, errorProvider, " Ngày chứng từ gốc ", isOnlyWarning);

                isValid &= ValidateControl.ValidateNull(txtSoHoaDon, errorProvider, " Số hóa đơn ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcNgayHoaDon, errorProvider, " Ngày hóa đơn ");
                isValid &= ValidateControl.ValidateNull(txtSoHopDong, errorProvider, " Số hợp đồng ");
                isValid &= ValidateControl.ValidateNull(clcNgayHopDong, errorProvider, " Ngày hợp đồng ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMa, errorProvider, " Mã khách hàng ");
                isValid &= ValidateControl.ValidateNull(txtTen, errorProvider, " Tên khách hàng ");
                isValid &= ValidateControl.ValidateNull(txtDiaChi, errorProvider, " Địa chỉ khách hàng ");
                isValid &= ValidateControl.ValidateNull(txtNguoiGiao, errorProvider, " Người giao hàng ");
                isValid &= ValidateControl.ValidateChoose(cbbMaKho, errorProvider, " Mã kho ");
                isValid &= ValidateControl.ValidateNull(txtTenKho, errorProvider, " Tên kho ");
                isValid &= ValidateControl.ValidateNull(cbbLoaiHang, errorProvider, " Loại hàng hóa ");
                isValid &= ValidateControl.ValidateNull(cbbMaNT, errorProvider, " Mã nguyên tệ ");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void GetPhieuXNKho()
        {
            try
            {
                if (PhieuXNKho == null)
                    PhieuXNKho = new T_KHOKETOAN_PHIEUXNKHO();
                if (String.IsNullOrEmpty(LOAICT))
                    LOAICT = cbbLoaiChungTu.SelectedValue.ToString();
                PhieuXNKho.LOAICHUNGTU = LOAICT;
                PhieuXNKho.SOCT = txtSoCT.Text.ToString();
                PhieuXNKho.NGAYCT = clcNgayCT.Value;
                PhieuXNKho.SOCTGOC = txtSoCTGoc.Text.ToString();
                PhieuXNKho.NGAYCTGOC = clcNgayCTGoc.Value;
                PhieuXNKho.SOTK = Convert.ToDecimal(txtSoTK.Text.ToString());
                PhieuXNKho.NGAYTK = clcNgayTK.Value;
                PhieuXNKho.MAHQ = GlobalSettings.MA_HAI_QUAN;
                PhieuXNKho.SOHOADON = txtSoHoaDon.Text;
                PhieuXNKho.NGAYHOADON = clcNgayHoaDon.Value;
                PhieuXNKho.HOPDONG_ID = HD.ID;
                PhieuXNKho.SOHOPDONG = txtSoHopDong.Text.ToString();
                PhieuXNKho.NGAYHOPDONG = clcNgayHopDong.Value;
                PhieuXNKho.MAKH = txtMa.Text.ToString();
                PhieuXNKho.TENKH = txtTen.Text.ToString();
                PhieuXNKho.DIACHIKH = txtDiaChi.Text.ToString();
                PhieuXNKho.MADN = GlobalSettings.MA_DON_VI;
                PhieuXNKho.TENDN = GlobalSettings.TEN_DON_VI;
                PhieuXNKho.DIACHIDN = GlobalSettings.DIA_CHI;
                PhieuXNKho.DIENGIAI = txtDienGiai.Text.ToString();
                PhieuXNKho.NGUOIVANCHUYEN = txtNguoiGiao.Text.ToString();
                PhieuXNKho.MAKHO = cbbMaKho.Value.ToString();
                PhieuXNKho.TENKHO = txtTenKho.Text.ToString();
                PhieuXNKho.LOAIHANGHOA = cbbLoaiHang.SelectedValue.ToString();
                PhieuXNKho.MANGUYENTE = cbbMaNT.Code;
                PhieuXNKho.TYGIA = Convert.ToDecimal(txtTyGia.Text.ToString());
                PhieuXNKho.GHICHU = txtGhiChu.Text.ToString();
                PhieuXNKho.PHIVANCHUYEN = Convert.ToDecimal(txtPhiVC.Text.ToString());
                PhieuXNKho.CHIPHI = Convert.ToDecimal(txtChiPhiKhac.Text.ToString());
                PhieuXNKho.PHANBO = cbbPhanBo.SelectedValue.ToString() == "0" ? true : false; // 0 : Phân bổ theo đơn giá ; 1: Phân bổ theo số lượng
                PhieuXNKho.TONGLUONGHANG = Convert.ToDecimal(txtTongLuongHang.Text.ToString());
                PhieuXNKho.TONGTIENHANG = Convert.ToDecimal(txtTongTriGiaHang.Text.ToString());
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetPhieuXNKho()
        {
            try
            {
                txtSoCT.Text = PhieuXNKho.SOCT;
                clcNgayCT.Value = PhieuXNKho.NGAYCT;
                txtSoCTGoc.Text = PhieuXNKho.SOCTGOC;
                clcNgayCTGoc.Value = PhieuXNKho.NGAYCTGOC;
                txtSoTK.Text = PhieuXNKho.SOTK.ToString();
                clcNgayTK.Value = PhieuXNKho.NGAYTK;
                txtSoHopDong.Text = PhieuXNKho.SOHOPDONG;
                clcNgayHopDong.Value = PhieuXNKho.NGAYHOPDONG;
                txtSoHoaDon.Text = PhieuXNKho.SOHOADON;
                clcNgayHoaDon.Value = PhieuXNKho.NGAYHOADON;
                txtMa.Text = PhieuXNKho.MAKH;
                txtTen.Text = PhieuXNKho.TENKH;
                txtDiaChi.Text = PhieuXNKho.DIACHIKH;
                txtDienGiai.Text = PhieuXNKho.DIENGIAI;
                txtNguoiGiao.Text = PhieuXNKho.NGUOIVANCHUYEN;
                cbbMaKho.Value = PhieuXNKho.MAKHO;
                txtTenKho.Text = PhieuXNKho.TENKHO;
                cbbLoaiHang.SelectedValue = PhieuXNKho.LOAIHANGHOA;
                cbbMaNT.Code = PhieuXNKho.MANGUYENTE;
                txtTyGia.Text = PhieuXNKho.TYGIA.ToString();
                txtGhiChu.Text = PhieuXNKho.GHICHU;
                txtPhiVC.Text = PhieuXNKho.PHIVANCHUYEN.ToString();
                txtChiPhiKhac.Text = PhieuXNKho.CHIPHI.ToString();
                txtGhiChu.Text = PhieuXNKho.GHICHU;
                cbbPhanBo.SelectedValue = PhieuXNKho.PHANBO == true ? "0" : "1" ;
                cbbLoaiChungTu.SelectedValue = PhieuXNKho.LOAICHUNGTU;
                txtTongLuongHang.Text = PhieuXNKho.TONGLUONGHANG.ToString();
                txtTongTriGiaHang.Text = PhieuXNKho.TONGTIENHANG.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = PhieuXNKho.HangCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Save()
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetPhieuXNKho();
                if (PhieuXNKho.HangCollection.Count==0)
                {
                    ShowMessage("Bạn chưa Thêm thông tin hàng hóa",false);
                    return;
                }
                PhieuXNKho.InsertUpdateFull();
                ShowMessage("Lưu thành công !", false);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void AddExcel()
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Add()
        {
            try
            {
                WareHouseExportValueForm f = new WareHouseExportValueForm();
                PhieuXNKho.LOAIHANGHOA = cbbLoaiHang.SelectedValue.ToString();
                f.PhieuXNKho = PhieuXNKho;
                f.HD = HD;
                f.ShowDialog(this);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtSoTK_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                //string whereCondition = " AND TrangThaiXuLy IN (2,3)";
                string whereCondition = "";
                string LoaiChungTu = cbbLoaiChungTu.SelectedValue.ToString();
                string LoaiHang = cbbLoaiHang.SelectedValue.ToString();
                VNACC_ToKhaiManager f = new VNACC_ToKhaiManager();
                f.IsShowChonToKhai = true;
                if (cbbLoaiChungTu.SelectedValue == "N")
                {
                    if (cbbLoaiHang.SelectedValue == "N")
                    {
                        whereCondition += " AND SUBSTRING(CAST(SoToKhai AS varchar(12)), 1, 1)='1' AND LoaiHang='N' ";
                    }
                    else if (cbbLoaiHang.SelectedValue == "S")
                    {
                        whereCondition += " AND SUBSTRING(CAST(SoToKhai AS varchar(12)), 1, 1)='3' AND LoaiHang='S' ";
                    }
                    else
                    {

                    }
                }
                else
                {
                    if (cbbLoaiHang.SelectedValue == "N")
                    {
                        whereCondition += " AND SUBSTRING(CAST(SoToKhai AS varchar(12)), 1, 1)='3' AND LoaiHang IN ('N','S') ";
                    }
                    else if (cbbLoaiHang.SelectedValue == "S")
                    {
                        whereCondition += " AND SUBSTRING(CAST(SoToKhai AS varchar(12)), 1, 1)='3' AND LoaiHang='S' ";
                    }
                    else
                    {

                    }
                }
                f.whereCondition = whereCondition;
                f.LoaiChungTu = LoaiChungTu;
                f.LoaiHang = LoaiHang;
                if (HD != null)
                {
                    f.hd = HD;
                }
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.OK)
                {
                    foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA item in PhieuXNKho.HangCollection)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        PhieuXNKho.HangCollection.Remove(item);
                    }
                    KDT_VNACC_ToKhaiMauDich TKMD = f.TKMDDuocChon;
                    txtSoTK.Text = TKMD.SoToKhai.ToString();
                    clcNgayTK.Value = TKMD.NgayDangKy;
                    txtSoHoaDon.Text = TKMD.SoHoaDon;
                    clcNgayHoaDon.Value = TKMD.NgayPhatHanhHD;
                    cbbMaNT.Code = TKMD.MaTTHoaDon;
                    if (TKMD.TyGiaCollection.Count >=1)
                    {
                        txtTyGia.Text = TKMD.TyGiaCollection[0].TyGiaTinhThue.ToString();   
                    }
                    //HD = HopDong.Load(TKMD.HopDong_ID);
                    //txtSoHopDong.Text = HD.SoHopDong;
                    //clcNgayHopDong.Value = HD.NgayDangKy;

                    // Kiểm tra loại hàng hóa của TKMD
                    decimal TongLuongHang = 0;
                    decimal TongTriGiaHang = 0;
                    string LoaiHangHoa = TKMD.LoaiHang;
                    decimal TongTLQD = 0;
                    int i = 1;

                    string errorMAHANGHOAMAP = "";
                    string errorDINHMUCMAP = "";
                    string errorTotal = "";

                    //Kiểm tra loại Phiếu Nhập kho hoặc Xuất kho

                    if (LoaiChungTu =="N")
                    {
                        // 1. Phiếu nhập kho
                        if (LoaiHangHoa == "N")
                        {
                            // Nguyên phụ liệu
                            foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                            {
                                TongLuongHang += HMD.SoLuong1;
                                TongTriGiaHang += HMD.TriGiaTinhThueS;
                                //Lấy danh sách NPL Map của kho kế toán                       
                                List<T_KHOKETOAN_NGUYENPHULIEU_MAP> NPLMapCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionDynamic("MANPLMAP ='" + HMD.MaHangHoa + "' AND HOPDONG_ID =" + HD.ID, "");
                                if (NPLMapCollection.Count==0)
                                {
                                    errorMAHANGHOAMAP += "[" + (i) + "]-[" + HMD.MaHangHoa + "]-[" + HMD.TenHang + "]-[" + HMD.DVTLuong1 + "]\n";
                                    break;
                                }
                                TongTLQD = 0;
                                foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in NPLMapCollection)
                                {
                                    TongTLQD += item.TYLEQD;
                                }
                                foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP NPL in NPLMapCollection)
                                {
                                    T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                                    PhieuXNKHang.STT = i;
                                    PhieuXNKHang.LOAIHANG = LoaiHangHoa;
                                    PhieuXNKHang.MAHANG = NPL.MANPL;
                                    PhieuXNKHang.TENHANG = NPL.TENNPL;
                                    PhieuXNKHang.DVT = NPL.DVT;
                                    PhieuXNKHang.MAHANGMAP = HMD.MaHangHoa;
                                    PhieuXNKHang.TENHANGMAP = HMD.TenHang;
                                    PhieuXNKHang.DVTMAP = DonViTinh_GetName(Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(HMD.DVTLuong1));
                                    PhieuXNKHang.SOLUONG = HMD.SoLuong1;
                                    PhieuXNKHang.DONGIA = HMD.DonGiaTinhThue;
                                    PhieuXNKHang.DONGIANT = HMD.DonGiaHoaDon;
                                    PhieuXNKHang.TRIGIANT = (HMD.TriGiaHoaDon * NPL.TYLEQD) / TongTLQD;
                                    PhieuXNKHang.THUEXNK = (HMD.SoTienThue * NPL.TYLEQD) / TongTLQD;
                                    PhieuXNKHang.THUETTDB = 0;

                                    foreach (KDT_VNACC_HangMauDich_ThueThuKhac ThueThuKhac in HMD.ThueThuKhacCollection)
                                    {
                                        decimal TongThueThuKhac = 0;
                                        if (HMD.ID == ThueThuKhac.Master_id)
                                        {
                                            TongThueThuKhac += ThueThuKhac.SoTienThueVaThuKhac;
                                        }
                                        PhieuXNKHang.THUEKHAC = TongThueThuKhac;
                                    }
                                    // 
                                    PhieuXNKHang.THANHTIEN = (HMD.TriGiaTinhThueS + HMD.SoTienThue + PhieuXNKHang.THUEKHAC) * NPL.TYLEQD / TongTLQD;
                                    PhieuXNKHang.TRIGIANTSAUPB = PhieuXNKHang.THANHTIEN;

                                    PhieuXNKHang.MAKHO = cbbMaKho.Value.ToString();
                                    PhieuXNKHang.TAIKHOANNO = "152";
                                    PhieuXNKHang.TAIKHOANCO = "152";
                                    PhieuXNKHang.TYLEQD = NPL.TYLEQD;
                                    PhieuXNKHang.SOLUONGQD = HMD.SoLuong1 * NPL.TYLEQD;
                                    PhieuXNKho.HangCollection.Add(PhieuXNKHang);
                                    i++;
                                }
                            }
                            PhieuXNKho.TONGLUONGHANG = TongLuongHang;
                            PhieuXNKho.TONGTIENHANG = TongTriGiaHang;
                            txtTongLuongHang.Text = PhieuXNKho.TONGLUONGHANG.ToString();
                            txtTongTriGiaHang.Text = PhieuXNKho.TONGTIENHANG.ToString();
                        }
                        else if (LoaiHangHoa == "S")
                        {
                            // Sản phẩm
                            foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                            {
                                TongLuongHang += HMD.SoLuong1;
                                TongTriGiaHang += HMD.TriGiaTinhThueS;
                                //Lấy danh sách NPL Map của kho kế toán                       
                                List<T_KHOKETOAN_SANPHAM_MAP> SPMapCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionDynamic("MASPMAP ='" + HMD.MaHangHoa + "' AND HOPDONG_ID =" + HD.ID, "");
                                if (SPMapCollection.Count == 0)
                                {
                                    errorMAHANGHOAMAP += "[" + (i) + "]-[" + HMD.MaHangHoa + "]-[" + HMD.TenHang + "]-[" + HMD.DVTLuong1 + "]\n";
                                    break;
                                }
                                TongTLQD = 0;
                                foreach (T_KHOKETOAN_SANPHAM_MAP item in SPMapCollection)
                                {
                                    TongTLQD += item.TYLEQD;
                                }
                                foreach (T_KHOKETOAN_SANPHAM_MAP SP in SPMapCollection)
                                {
                                    T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                                    PhieuXNKHang.STT = i;
                                    PhieuXNKHang.LOAIHANG = LoaiHangHoa;
                                    PhieuXNKHang.MAHANG = SP.MASP;
                                    PhieuXNKHang.TENHANG = SP.TENSP;
                                    PhieuXNKHang.DVT = SP.DVT;
                                    PhieuXNKHang.MAHANGMAP = HMD.MaHangHoa;
                                    PhieuXNKHang.TENHANGMAP = HMD.TenHang;
                                    PhieuXNKHang.DVTMAP = DonViTinh_GetName(Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(HMD.DVTLuong1));
                                    PhieuXNKHang.SOLUONG = HMD.SoLuong1;
                                    PhieuXNKHang.DONGIA = HMD.DonGiaTinhThue;
                                    PhieuXNKHang.DONGIANT = HMD.DonGiaHoaDon;
                                    PhieuXNKHang.TRIGIANT = (HMD.TriGiaHoaDon * SP.TYLEQD) / TongTLQD ;
                                    PhieuXNKHang.THANHTIEN = (HMD.TriGiaTinhThueS * SP.TYLEQD) / TongTLQD;
                                    PhieuXNKHang.TRIGIANTSAUPB = (HMD.TriGiaTinhThueS * SP.TYLEQD) * TongTLQD ;
                                    PhieuXNKHang.THUEXNK = (HMD.SoTienThue * SP.TYLEQD ) / TongTLQD;
                                    PhieuXNKHang.THUETTDB = 0;
                                    PhieuXNKHang.THUEKHAC = 0;
                                    PhieuXNKHang.MAKHO = cbbMaKho.Value.ToString();
                                    PhieuXNKHang.TAIKHOANNO = "155";
                                    PhieuXNKHang.TAIKHOANCO = "155";
                                    PhieuXNKHang.TYLEQD = SP.TYLEQD;
                                    PhieuXNKHang.SOLUONGQD = HMD.SoLuong1 * SP.TYLEQD;
                                    PhieuXNKho.HangCollection.Add(PhieuXNKHang);
                                    i++;
                                }
                            }
                            PhieuXNKho.TONGLUONGHANG = TongLuongHang;
                            PhieuXNKho.TONGTIENHANG = TongTriGiaHang;
                            txtTongLuongHang.Text = PhieuXNKho.TONGLUONGHANG.ToString();
                            txtTongTriGiaHang.Text = PhieuXNKho.TONGTIENHANG.ToString();
                        }
                        else if (LoaiHangHoa == "T")
                        {
                            // Thiết bị
                        }
                        else
                        {
                            // Hàng mẫu
                        }
                    }
                    else
                    {
                        // 2. Phiếu xuất kho
                        if (LoaiHangHoa == "N")
                        {
                            // Nguyên phụ liệu
                            foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                            {
                                TongLuongHang += HMD.SoLuong1;
                                TongTriGiaHang += HMD.TriGiaTinhThueS;
                                //Lấy danh sách NPL Map của kho kế toán                       
                                List<T_KHOKETOAN_NGUYENPHULIEU_MAP> NPLMapCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionDynamic("MANPLMAP ='" + HMD.MaHangHoa + "' AND HOPDONG_ID =" + HD.ID, "");
                                if (NPLMapCollection.Count == 0)
                                {
                                    errorMAHANGHOAMAP += "[" + (i) + "]-[" + HMD.MaHangHoa + "]-[" + HMD.TenHang + "]-[" + HMD.DVTLuong1 + "]\n";
                                    break;
                                }
                                TongTLQD = 0;
                                foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in NPLMapCollection)
                                {
                                    TongTLQD += item.TYLEQD;
                                }
                                foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP NPL in NPLMapCollection)
                                {
                                    T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                                    PhieuXNKHang.STT = i;
                                    PhieuXNKHang.LOAIHANG = LoaiHangHoa;
                                    PhieuXNKHang.MAHANG = NPL.MANPL;
                                    PhieuXNKHang.TENHANG = NPL.TENNPL;
                                    PhieuXNKHang.DVT = NPL.DVT;
                                    PhieuXNKHang.MAHANGMAP = HMD.MaHangHoa;
                                    PhieuXNKHang.TENHANGMAP = HMD.TenHang;
                                    PhieuXNKHang.DVTMAP = DonViTinh_GetName(Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(HMD.DVTLuong1));
                                    PhieuXNKHang.SOLUONG = HMD.SoLuong1;
                                    PhieuXNKHang.DONGIA = HMD.DonGiaTinhThue;
                                    PhieuXNKHang.DONGIANT = HMD.DonGiaHoaDon;
                                    PhieuXNKHang.TRIGIANT = (HMD.TriGiaHoaDon * NPL.TYLEQD) / TongTLQD;
                                    PhieuXNKHang.THUEXNK = (HMD.SoTienThue * NPL.TYLEQD) / TongTLQD;
                                    PhieuXNKHang.THUETTDB = 0;

                                    foreach (KDT_VNACC_HangMauDich_ThueThuKhac ThueThuKhac in HMD.ThueThuKhacCollection)
                                    {
                                        decimal TongThueThuKhac = 0;
                                        if (HMD.ID == ThueThuKhac.Master_id)
                                        {
                                            TongThueThuKhac += ThueThuKhac.SoTienThueVaThuKhac;
                                        }
                                        PhieuXNKHang.THUEKHAC = TongThueThuKhac;
                                    }
                                    // 
                                    PhieuXNKHang.THANHTIEN = (HMD.TriGiaTinhThueS + HMD.SoTienThue + PhieuXNKHang.THUEKHAC) * NPL.TYLEQD / TongTLQD;
                                    PhieuXNKHang.TRIGIANTSAUPB = PhieuXNKHang.THANHTIEN;

                                    PhieuXNKHang.MAKHO = cbbMaKho.Value.ToString();
                                    PhieuXNKHang.TAIKHOANNO = "152";
                                    PhieuXNKHang.TAIKHOANCO = "152";
                                    PhieuXNKHang.TYLEQD = NPL.TYLEQD;
                                    PhieuXNKHang.SOLUONGQD = HMD.SoLuong1 * NPL.TYLEQD;
                                    PhieuXNKho.HangCollection.Add(PhieuXNKHang);
                                    i++;
                                }
                            }
                            PhieuXNKho.TONGLUONGHANG = TongLuongHang;
                            PhieuXNKho.TONGTIENHANG = TongTriGiaHang;
                            txtTongLuongHang.Text = PhieuXNKho.TONGLUONGHANG.ToString();
                            txtTongTriGiaHang.Text = PhieuXNKho.TONGTIENHANG.ToString();
                        }
                        else if (LoaiHangHoa == "S")
                        {
                            //                             
                            if (LoaiHang == "N")
                            {
                                // Tính định mức sản phẩm quy đổi ra NPL
                                foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
                                {
                                    TongLuongHang += hmd.SoLuong1;
                                    TongTriGiaHang += hmd.TriGiaTinhThueS;
                                    //Lấy danh sách NPL Map của kho kế toán                       
                                    List<T_KHOKETOAN_SANPHAM_MAP> SPMapCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionDynamic("MASPMAP ='" + hmd.MaHangHoa + "' AND HOPDONG_ID =" + HD.ID, "");
                                    if (SPMapCollection.Count == 0)
                                    {
                                        errorMAHANGHOAMAP += "[" + (i) + "]-[" + hmd.MaHangHoa + "]-[" + hmd.TenHang + "]-[" + hmd.DVTLuong1 + "]\n";
                                        break;
                                    }
                                    TongTLQD = 0;
                                    foreach (T_KHOKETOAN_SANPHAM_MAP item in SPMapCollection)
                                    {
                                        TongTLQD += item.TYLEQD;
                                    }
                                    foreach (T_KHOKETOAN_SANPHAM_MAP SP in SPMapCollection)
                                    {
                                        //Định mức sản phẩm MAP
                                        List<T_KHOKETOAN_DINHMUC> DMCollection = T_KHOKETOAN_DINHMUC.SelectCollectionDynamic("MASP ='" + SP.MASP + "' AND HOPDONG_ID =" + HD.ID, "");
                                        if (DMCollection.Count == 0)
                                        {
                                            errorDINHMUCMAP += "[" + (i) + "]-[" + SP.MASP + "]-[" + SP.TENSP + "]-[" + SP.DVT + "]\n";
                                            break;
                                        }
                                        decimal TongSLQDSPToNPL = 0;
                                        decimal DonGiaNTSP = hmd.DonGiaHoaDon;
                                        decimal DonGia = hmd.DonGiaTinhThue;
                                        decimal SoLuongSPQD = (hmd.SoLuong1 * SP.TYLEQD) / TongTLQD;
                                        decimal TrigiaNT = SoLuongSPQD * DonGiaNTSP;
                                        decimal TriGia = SoLuongSPQD * DonGia;
                                        decimal ThueXNK = hmd.SoTienThue * SP.TYLEQD / TongTLQD;
                                        foreach (T_KHOKETOAN_DINHMUC item in DMCollection)
                                        {
                                            TongSLQDSPToNPL += item.DMCHUNG * ((hmd.SoLuong1 * SP.TYLEQD) / TongTLQD);
                                        }
                                        foreach (T_KHOKETOAN_DINHMUC DM in DMCollection)
                                        {
                                            T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                                            PhieuXNKHang.STT = i;
                                            PhieuXNKHang.LOAIHANG = LoaiHangHoa;
                                            PhieuXNKHang.MAHANG = DM.MANPL;
                                            PhieuXNKHang.TENHANG = DM.TENNPL;
                                            PhieuXNKHang.DVT = DM.DVTNPL;
                                            PhieuXNKHang.MAHANGMAP = DM.MANPL;
                                            PhieuXNKHang.TENHANGMAP = DM.TENNPL;
                                            PhieuXNKHang.DVTMAP = DM.DVTNPL;
                                            PhieuXNKHang.SOLUONG = SoLuongSPQD * DM.DMCHUNG;
                                            PhieuXNKHang.DONGIA = TriGia / TongSLQDSPToNPL;
                                            PhieuXNKHang.DONGIANT = TrigiaNT / TongSLQDSPToNPL;
                                            PhieuXNKHang.TRIGIANT = PhieuXNKHang.SOLUONG * PhieuXNKHang.DONGIANT ;
                                            PhieuXNKHang.THANHTIEN = PhieuXNKHang.SOLUONG * PhieuXNKHang.DONGIA;
                                            PhieuXNKHang.TRIGIANTSAUPB = PhieuXNKHang.THANHTIEN ;
                                            PhieuXNKHang.THUEXNK = ThueXNK;
                                            PhieuXNKHang.THUETTDB = 0;
                                            PhieuXNKHang.THUEKHAC = 0;
                                            PhieuXNKHang.MAKHO = cbbMaKho.Value.ToString();
                                            PhieuXNKHang.TAIKHOANNO = "155";
                                            PhieuXNKHang.TAIKHOANCO = "155";
                                            PhieuXNKHang.TYLEQD = SP.TYLEQD;
                                            PhieuXNKHang.SOLUONGQD = SoLuongSPQD * DM.DMCHUNG;
                                            PhieuXNKho.HangCollection.Add(PhieuXNKHang);
                                            i++;                                                       
                                        }
                                    }
                                }
                                PhieuXNKho.TONGLUONGHANG = TongLuongHang;
                                PhieuXNKho.TONGTIENHANG = TongTriGiaHang;
                                txtTongLuongHang.Text = PhieuXNKho.TONGLUONGHANG.ToString();
                                txtTongTriGiaHang.Text = PhieuXNKho.TONGTIENHANG.ToString();
                            }
                            else if (LoaiHang == "S")
                            {
                                // Xuất sản phẩm
                                foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                                {
                                    TongLuongHang += HMD.SoLuong1;
                                    TongTriGiaHang += HMD.TriGiaTinhThueS;
                                    //Lấy danh sách SP Map của kho kế toán                       
                                    List<T_KHOKETOAN_SANPHAM_MAP> SPMapCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionDynamic("MASPMAP ='" + HMD.MaHangHoa + "' AND HOPDONG_ID =" + HD.ID, "");
                                    if (SPMapCollection.Count == 0)
                                    {
                                        errorMAHANGHOAMAP += "[" + (i) + "]-[" + HMD.MaHangHoa + "]-[" + HMD.TenHang + "]-[" + HMD.DVTLuong1 + "]\n";
                                        break;
                                    }
                                    TongTLQD = 0;
                                    foreach (T_KHOKETOAN_SANPHAM_MAP item in SPMapCollection)
                                    {
                                        TongTLQD += item.TYLEQD;
                                    }
                                    foreach (T_KHOKETOAN_SANPHAM_MAP SP in SPMapCollection)
                                    {
                                        T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                                        PhieuXNKHang.STT = i;
                                        PhieuXNKHang.LOAIHANG = LoaiHangHoa;
                                        PhieuXNKHang.MAHANG = SP.MASP;
                                        PhieuXNKHang.TENHANG = SP.TENSP;
                                        PhieuXNKHang.DVT = SP.DVT;
                                        PhieuXNKHang.MAHANGMAP = HMD.MaHangHoa;
                                        PhieuXNKHang.TENHANGMAP = HMD.TenHang;
                                        PhieuXNKHang.DVTMAP = DonViTinh_GetName(Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(HMD.DVTLuong1));
                                        PhieuXNKHang.SOLUONG = HMD.SoLuong1;
                                        PhieuXNKHang.DONGIA = HMD.DonGiaTinhThue;
                                        PhieuXNKHang.DONGIANT = HMD.DonGiaHoaDon;
                                        PhieuXNKHang.TRIGIANT = (HMD.TriGiaHoaDon * SP.TYLEQD) / TongTLQD;
                                        PhieuXNKHang.THANHTIEN = (HMD.TriGiaTinhThueS * SP.TYLEQD) / TongTLQD;
                                        PhieuXNKHang.TRIGIANTSAUPB = (HMD.TriGiaTinhThueS * SP.TYLEQD) * TongTLQD;
                                        PhieuXNKHang.THUEXNK = (HMD.SoTienThue * SP.TYLEQD) / TongTLQD;
                                        PhieuXNKHang.THUETTDB = 0;
                                        PhieuXNKHang.THUEKHAC = 0;
                                        PhieuXNKHang.MAKHO = cbbMaKho.Value.ToString();
                                        PhieuXNKHang.TAIKHOANNO = "155";
                                        PhieuXNKHang.TAIKHOANCO = "155";
                                        PhieuXNKHang.TYLEQD = SP.TYLEQD;
                                        PhieuXNKHang.SOLUONGQD = HMD.SoLuong1 * SP.TYLEQD;
                                        PhieuXNKho.HangCollection.Add(PhieuXNKHang);
                                        i++;
                                    }
                                }
                                PhieuXNKho.TONGLUONGHANG = TongLuongHang;
                                PhieuXNKho.TONGTIENHANG = TongTriGiaHang;
                                txtTongLuongHang.Text = PhieuXNKho.TONGLUONGHANG.ToString();
                                txtTongTriGiaHang.Text = PhieuXNKho.TONGTIENHANG.ToString();
                            }
                            else
                            {
                                
                            }
                        }
                        else if (LoaiHangHoa == "T")
                        {
                            // Thiết bị
                        }
                        else
                        {
                            // Hàng mẫu
                        }
                    }
                    #region
                    //if (LoaiHangHoa == "N")
                    //{
                    //    cbbLoaiHang.SelectedValue = LoaiHangHoa;
                    //    foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                    //    {
                    //        TongLuongHang += HMD.SoLuong1;
                    //        TongTriGiaHang += HMD.TriGiaTinhThueS;
                    //        //Lấy danh sách NPL Map của kho kế toán                       
                    //        List<T_KHOKETOAN_NGUYENPHULIEU_MAP> NPLMapCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionDynamic("MANPLMAP ='" + HMD.MaHangHoa + "' AND HOPDONG_ID =" + HD.ID, "");
                    //        foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP NPL in NPLMapCollection)
                    //        {
                    //            T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                    //            PhieuXNKHang.LOAIHANG = LoaiHangHoa;
                    //            PhieuXNKHang.MAHANG = NPL.MANPL;
                    //            PhieuXNKHang.TENHANG = NPL.TENNPL;
                    //            PhieuXNKHang.DVT = NPL.DVT;
                    //            PhieuXNKHang.MAHANGMAP = HMD.MaHangHoa;
                    //            PhieuXNKHang.TENHANGMAP = HMD.TenHang;
                    //            PhieuXNKHang.DVTMAP = DonViTinh_GetName(Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(HMD.DVTLuong1));
                    //            PhieuXNKHang.SOLUONG = HMD.SoLuong1;
                    //            PhieuXNKHang.DONGIA = HMD.DonGiaTinhThue;
                    //            PhieuXNKHang.DONGIANT = HMD.DonGiaHoaDon;
                    //            PhieuXNKHang.TRIGIANT = HMD.TriGiaHoaDon;
                    //            PhieuXNKHang.THUEXNK = HMD.SoTienThue;
                    //            PhieuXNKHang.THUETTDB = 0;

                    //            foreach (KDT_VNACC_HangMauDich_ThueThuKhac ThueThuKhac in HMD.ThueThuKhacCollection)
                    //            {
                    //                decimal TongThueThuKhac = 0;
                    //                if (HMD.ID == ThueThuKhac.Master_id)
                    //                {
                    //                    TongThueThuKhac += ThueThuKhac.SoTienThueVaThuKhac;
                    //                }
                    //                PhieuXNKHang.THUEKHAC = TongThueThuKhac;
                    //            }
                    //            // 
                    //            PhieuXNKHang.THANHTIEN = HMD.TriGiaTinhThueS + HMD.SoTienThue + PhieuXNKHang.THUEKHAC;
                    //            PhieuXNKHang.TRIGIANTSAUPB = PhieuXNKHang.THANHTIEN;

                    //            PhieuXNKHang.MAKHO = cbbMaKho.Value.ToString();
                    //            PhieuXNKHang.TAIKHOANNO = "152";
                    //            PhieuXNKHang.TAIKHOANCO = "152";
                    //            PhieuXNKHang.TYLEQD = NPL.TYLEQD;
                    //            PhieuXNKHang.SOLUONGQD = HMD.SoLuong1 * NPL.TYLEQD;
                    //            PhieuXNKho.HangCollection.Add(PhieuXNKHang);
                    //        }
                    //    }
                    //    PhieuXNKho.TONGLUONGHANG = TongLuongHang;
                    //    PhieuXNKho.TONGTIENHANG = TongTriGiaHang;
                    //    txtTongLuongHang.Text = PhieuXNKho.TONGLUONGHANG.ToString();
                    //    txtTongTriGiaHang.Text = PhieuXNKho.TONGTIENHANG.ToString();
                    //}
                    //else if (LoaiHangHoa == "S")
                    //{
                    //    cbbLoaiHang.SelectedValue = LoaiHangHoa;
                    //    foreach (KDT_VNACC_HangMauDich HMD in TKMD.HangCollection)
                    //    {
                    //        TongLuongHang += HMD.SoLuong1;
                    //        TongTriGiaHang += HMD.TriGiaTinhThueS;
                    //        //Lấy danh sách SP Map của kho kế toán                       
                    //        List<T_KHOKETOAN_SANPHAM_MAP> SPMapCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionDynamic("MASPMAP ='" + HMD.MaHangHoa + "' AND HOPDONG_ID =" + HD.ID, "");
                    //        foreach (T_KHOKETOAN_SANPHAM_MAP item in SPMapCollection)
                    //        {
                    //            TongTLQD += item.TYLEQD;
                    //        }
                    //        foreach (T_KHOKETOAN_SANPHAM_MAP SP in SPMapCollection)
                    //        {
                    //            T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                    //            PhieuXNKHang.LOAIHANG = LoaiHangHoa;
                    //            PhieuXNKHang.MAHANG = SP.MASP;
                    //            PhieuXNKHang.TENHANG = SP.TENSP;
                    //            PhieuXNKHang.DVT = SP.DVT;
                    //            PhieuXNKHang.MAHANGMAP = HMD.MaHangHoa;
                    //            PhieuXNKHang.TENHANGMAP = HMD.TenHang;
                    //            PhieuXNKHang.DVTMAP = DonViTinh_GetName(Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeV4DVT(HMD.DVTLuong1));
                    //            PhieuXNKHang.SOLUONG = HMD.SoLuong1;
                    //            PhieuXNKHang.DONGIA = HMD.DonGiaTinhThue;
                    //            PhieuXNKHang.DONGIANT = HMD.DonGiaHoaDon;
                    //            PhieuXNKHang.TRIGIANT = (HMD.TriGiaHoaDon * SP.TYLEQD) / TongTLQD;
                    //            PhieuXNKHang.THANHTIEN = (HMD.TriGiaTinhThueS * SP.TYLEQD) / TongTLQD;
                    //            PhieuXNKHang.TRIGIANTSAUPB = (HMD.TriGiaTinhThueS * SP.TYLEQD) * TongTLQD;
                    //            PhieuXNKHang.THUEXNK = (HMD.SoTienThue * SP.TYLEQD) / TongTLQD;
                    //            PhieuXNKHang.THUETTDB = 0;
                    //            PhieuXNKHang.THUEKHAC = 0;
                    //            PhieuXNKHang.MAKHO = cbbMaKho.Value.ToString();
                    //            PhieuXNKHang.TAIKHOANNO = "155";
                    //            PhieuXNKHang.TAIKHOANCO = "155";
                    //            PhieuXNKHang.TYLEQD = SP.TYLEQD;
                    //            PhieuXNKHang.SOLUONGQD = HMD.SoLuong1 * SP.TYLEQD;
                    //            PhieuXNKho.HangCollection.Add(PhieuXNKHang);
                    //        }
                    //    }
                    //    PhieuXNKho.TONGLUONGHANG = TongLuongHang;
                    //    PhieuXNKho.TONGTIENHANG = TongTriGiaHang;
                    //    txtTongLuongHang.Text = PhieuXNKho.TONGLUONGHANG.ToString();
                    //    txtTongTriGiaHang.Text = PhieuXNKho.TONGTIENHANG.ToString();
                    //}
                    //else if (LoaiHangHoa == "T")
                    //{
                    //    cbbLoaiHang.SelectedValue = LoaiHangHoa;
                    //}
                    //else if (LoaiHangHoa == "H")
                    //{
                    //    cbbLoaiHang.SelectedValue = LoaiHangHoa;
                    //}
                    //else
                    //{
                    //    ShowMessage("", false);
                    //}
                    #endregion
                    if (!String.IsNullOrEmpty(errorMAHANGHOAMAP))
                        errorTotal += "\n - [STT] -[MÃ HÀNG HÓA]-[TÊN HÀNG HÓA]-[ĐVT] : " + errorMAHANGHOAMAP + "CHƯA ĐƯỢC MAP VỚI MÃ HÀNG HÓA TRONG KHO KẾ TOÁN";
                    if (!String.IsNullOrEmpty(errorDINHMUCMAP))
                        errorTotal += "\n - [STT] -[MÃ HÀNG HÓA]-[TÊN HÀNG HÓA]-[ĐVT] : " + errorDINHMUCMAP + "CHƯA ĐƯỢC MAP ĐỊNH MỨC TRONG KHO KẾ TOÁN";
                    if (!String.IsNullOrEmpty(errorTotal))
                    {
                        ShowMessageTQDT("LẤY THÔNG TIN HÀNG HÓA TỜ KHAI KHÔNG THÀNH CÔNG !" + errorTotal, false);
                        PhieuXNKho.HangCollection.Clear();
                        return;
                    }
                    BindData();
                    txtPhiVC_TextChanged(null,null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtNguoiGiao_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                DonViDoiTacForm f = new DonViDoiTacForm();
                f.isBrower = true;
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.OK)
                {
                    txtNguoiGiao.Text = string.IsNullOrEmpty(f.doiTac.MaCongTy) ? "" : f.doiTac.MaCongTy.Trim().ToUpper();
                    txtDienGiai.Text = string.IsNullOrEmpty(f.doiTac.TenCongTy) ? "" : f.doiTac.TenCongTy.Trim().ToUpper();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {

        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                    PhieuXNKHang = (T_KHOKETOAN_PHIEUXNKHO_HANGHOA)e.Row.DataRow;
                    WareHouseExportValueForm f = new WareHouseExportValueForm();
                    PhieuXNKho.LOAIHANGHOA = cbbLoaiHang.SelectedValue.ToString();
                    f.PhieuXNKho = PhieuXNKho;
                    f.PhieuXNKHang = PhieuXNKHang;
                    f.isAdd = false;
                    f.HD = HD;
                    f.ShowDialog(this);
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaKho_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaKho.Value !=null)
                {
                    List<T_KHOKETOAN_DANHSACHKHO> KHOCollection =  T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
                    foreach (T_KHOKETOAN_DANHSACHKHO item in KHOCollection)
                    {
                        if (item.MAKHO == cbbMaKho.Value.ToString())
                        {
                            txtTenKho.Text = item.TENKHO.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtPhiVC_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal TongLuongHang = 0;
                decimal TongTienHang = 0;
                foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA hangHoa in PhieuXNKho.HangCollection)
                {
                    TongLuongHang += hangHoa.SOLUONG;
                    TongTienHang += hangHoa.TRIGIANT;
                }
                // Tính phân bổ 
                if (Convert.ToDecimal(txtChiPhiKhac.Text.ToString()) != 0 || Convert.ToDecimal(txtPhiVC.Text.ToString()) != 0)
                {
                    decimal TongChiPhi = Convert.ToDecimal(txtChiPhiKhac.Text.ToString()) + Convert.ToDecimal(txtPhiVC.Text.ToString());
                    foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA hangHoa in PhieuXNKho.HangCollection)
                    {
                        if (cbbPhanBo.SelectedValue.ToString() == "1")
                        {
                            // Phân bổ Theo số lượng
                            decimal SoLuongPB = TongChiPhi / TongLuongHang;
                            hangHoa.TRIGIANTSAUPB = hangHoa.THANHTIEN + SoLuongPB * hangHoa.SOLUONG;
                        }
                        else
                        {
                            // Phân bổ Theo đơn giá
                            decimal TriGiaPB = TongChiPhi / TongTienHang;
                            hangHoa.TRIGIANTSAUPB = hangHoa.THANHTIEN + TriGiaPB * hangHoa.SOLUONG;
                        }
                    }
                    TongTienHang = 0;
                    foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA hangHoa in PhieuXNKho.HangCollection)
                    {
                        TongTienHang += hangHoa.TRIGIANTSAUPB;
                    }
                }
                else
                {
                    TongTienHang = 0;
                    foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA hangHoa in PhieuXNKho.HangCollection)
                    {
                        hangHoa.TRIGIANTSAUPB = hangHoa.THANHTIEN;
                        TongTienHang += hangHoa.THANHTIEN;
                    }
                }
                txtTongLuongHang.Text = TongLuongHang.ToString();
                txtTongTriGiaHang.Text = TongTienHang.ToString();
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtChiPhiKhac_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal TongLuongHang = 0;
                decimal TongTienHang = 0;
                foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA hangHoa in PhieuXNKho.HangCollection)
                {
                    TongLuongHang += hangHoa.SOLUONG;
                    TongTienHang += hangHoa.TRIGIANT;
                }
                // Tính phân bổ 
                if (Convert.ToDecimal(txtChiPhiKhac.Text.ToString()) != 0 || Convert.ToDecimal(txtPhiVC.Text.ToString()) != 0)
                {
                    decimal TongChiPhi = Convert.ToDecimal(txtChiPhiKhac.Text.ToString()) + Convert.ToDecimal(txtPhiVC.Text.ToString());
                    foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA hangHoa in PhieuXNKho.HangCollection)
                    {
                        if (cbbPhanBo.SelectedValue.ToString() == "1")
                        {
                            // Phân bổ Theo số lượng
                            decimal SoLuongPB = TongChiPhi / TongLuongHang;
                            hangHoa.TRIGIANTSAUPB = hangHoa.THANHTIEN + SoLuongPB * hangHoa.SOLUONG;
                        }
                        else
                        {
                            // Phân bổ Theo đơn giá
                            decimal TriGiaPB = TongChiPhi / TongTienHang;
                            hangHoa.TRIGIANTSAUPB = hangHoa.THANHTIEN + TriGiaPB * hangHoa.SOLUONG;
                        }
                    }
                    TongTienHang = 0;
                    foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA hangHoa in PhieuXNKho.HangCollection)
                    {
                        TongTienHang += hangHoa.TRIGIANTSAUPB;
                    }
                }
                else
                {
                    TongTienHang = 0;
                    foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA hangHoa in PhieuXNKho.HangCollection)
                    {
                        hangHoa.TRIGIANTSAUPB = hangHoa.THANHTIEN;
                        TongTienHang += hangHoa.THANHTIEN;
                    }
                }
                txtTongLuongHang.Text = TongLuongHang.ToString();
                txtTongTriGiaHang.Text = TongTienHang.ToString();
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbPhanBo_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                decimal TongLuongHang = 0;
                decimal TongTienHang = 0;
                foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA hangHoa in PhieuXNKho.HangCollection)
                {
                    TongLuongHang += hangHoa.SOLUONG;
                    TongTienHang += hangHoa.TRIGIANT;
                }
                // Tính phân bổ 
                if (Convert.ToDecimal(txtChiPhiKhac.Text.ToString()) != 0 || Convert.ToDecimal(txtPhiVC.Text.ToString()) != 0)
                {
                    decimal TongChiPhi = Convert.ToDecimal(txtChiPhiKhac.Text.ToString()) + Convert.ToDecimal(txtPhiVC.Text.ToString());
                    foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA hangHoa in PhieuXNKho.HangCollection)
                    {
                        if (cbbPhanBo.SelectedValue.ToString() == "1")
                        {
                            // Phân bổ Theo số lượng
                            decimal SoLuongPB = TongChiPhi / TongLuongHang;
                            hangHoa.TRIGIANTSAUPB = hangHoa.THANHTIEN + SoLuongPB * hangHoa.SOLUONG;
                        }
                        else
                        {
                            // Phân bổ Theo đơn giá
                            decimal TriGiaPB = TongChiPhi / TongTienHang;
                            hangHoa.TRIGIANTSAUPB = hangHoa.THANHTIEN + TriGiaPB * hangHoa.SOLUONG;
                        }
                    }
                    TongTienHang = 0;
                    foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA hangHoa in PhieuXNKho.HangCollection)
                    {
                        TongTienHang += hangHoa.TRIGIANTSAUPB;
                    }
                }
                else
                {
                    TongTienHang = 0;
                    foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA hangHoa in PhieuXNKho.HangCollection)
                    {
                        hangHoa.TRIGIANTSAUPB = hangHoa.THANHTIEN;
                        TongTienHang += hangHoa.THANHTIEN;
                    }
                }
                txtTongLuongHang.Text = TongLuongHang.ToString();
                txtTongTriGiaHang.Text = TongTienHang.ToString();
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
