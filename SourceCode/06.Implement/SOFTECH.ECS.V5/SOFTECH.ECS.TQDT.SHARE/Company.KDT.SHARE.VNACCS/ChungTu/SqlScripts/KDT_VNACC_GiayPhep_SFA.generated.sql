-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Insert]
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaDonViCapPhep varchar(6),
	@TenDonViCapPhep nvarchar(300),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@MaThuongNhanXuatKhau varchar(13),
	@TenThuongNhanXuatKhau nvarchar(210),
	@MaBuuChinhXuatKhau varchar(9),
	@SoNhaTenDuongXuatKhau varchar(35),
	@PhuongXaXuatKhau nvarchar(35),
	@QuanHuyenXuatKhau nvarchar(35),
	@TinhThanhPhoXuatKhau nvarchar(35),
	@MaQuocGiaXuatKhau varchar(2),
	@SoDienThoaiXuatKhau varchar(20),
	@SoFaxXuatKhau varchar(20),
	@EmailXuatKhau varchar(210),
	@SoHopDong nvarchar(35),
	@SoVanDon varchar(35),
	@MaBenDi varchar(6),
	@TenBenDi varchar(35),
	@MaThuongNhanNhapKhau varchar(13),
	@TenThuongNhanNhapKhau nvarchar(300),
	@MaBuuChinhNhapKhau varchar(7),
	@SoNhaTenDuongNhapKhau nvarchar(300),
	@MaQuocGiaNhapKhau varchar(2),
	@SoDienThoaiNhapKhau varchar(20),
	@SoFaxNhapKhau varchar(20),
	@EmailNhapKhau varchar(70),
	@MaBenDen varchar(6),
	@TenBenDen varchar(35),
	@ThoiGianNhapKhauDuKien datetime,
	@GiaTriHangHoa numeric(19, 2),
	@DonViTienTe varchar(3),
	@DiaDiemTapKetHangHoa nvarchar(70),
	@ThoiGianKiemTra datetime,
	@DiaDiemKiemTra nvarchar(70),
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@DaiDienThuongNhanNhapKhau nvarchar(300),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(70),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@GhiChuDanhChoCoQuanCapPhep nvarchar(500),
	@DaiDienCoQuanKiemTra nvarchar(500),
	@NgayKhaiBao datetime,
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@TrangThaiXuLy int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SFA]
(
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[SoDienThoaiXuatKhau],
	[SoFaxXuatKhau],
	[EmailXuatKhau],
	[SoHopDong],
	[SoVanDon],
	[MaBenDi],
	[TenBenDi],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[SoNhaTenDuongNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[MaBenDen],
	[TenBenDen],
	[ThoiGianNhapKhauDuKien],
	[GiaTriHangHoa],
	[DonViTienTe],
	[DiaDiemTapKetHangHoa],
	[ThoiGianKiemTra],
	[DiaDiemKiemTra],
	[HoSoLienQuan],
	[GhiChu],
	[DaiDienThuongNhanNhapKhau],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienCoQuanKiemTra],
	[NgayKhaiBao],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[TrangThaiXuLy]
)
VALUES 
(
	@MaNguoiKhai,
	@TenNguoiKhai,
	@MaDonViCapPhep,
	@TenDonViCapPhep,
	@SoDonXinCapPhep,
	@ChucNangChungTu,
	@LoaiGiayPhep,
	@MaThuongNhanXuatKhau,
	@TenThuongNhanXuatKhau,
	@MaBuuChinhXuatKhau,
	@SoNhaTenDuongXuatKhau,
	@PhuongXaXuatKhau,
	@QuanHuyenXuatKhau,
	@TinhThanhPhoXuatKhau,
	@MaQuocGiaXuatKhau,
	@SoDienThoaiXuatKhau,
	@SoFaxXuatKhau,
	@EmailXuatKhau,
	@SoHopDong,
	@SoVanDon,
	@MaBenDi,
	@TenBenDi,
	@MaThuongNhanNhapKhau,
	@TenThuongNhanNhapKhau,
	@MaBuuChinhNhapKhau,
	@SoNhaTenDuongNhapKhau,
	@MaQuocGiaNhapKhau,
	@SoDienThoaiNhapKhau,
	@SoFaxNhapKhau,
	@EmailNhapKhau,
	@MaBenDen,
	@TenBenDen,
	@ThoiGianNhapKhauDuKien,
	@GiaTriHangHoa,
	@DonViTienTe,
	@DiaDiemTapKetHangHoa,
	@ThoiGianKiemTra,
	@DiaDiemKiemTra,
	@HoSoLienQuan,
	@GhiChu,
	@DaiDienThuongNhanNhapKhau,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@MaKetQuaXuLy,
	@KetQuaXuLySoGiayPhep,
	@KetQuaXuLyNgayCap,
	@KetQuaXuLyHieuLucTuNgay,
	@KetQuaXuLyHieuLucDenNgay,
	@GhiChuDanhChoCoQuanCapPhep,
	@DaiDienCoQuanKiemTra,
	@NgayKhaiBao,
	@HoSoKemTheo_1,
	@HoSoKemTheo_2,
	@HoSoKemTheo_3,
	@HoSoKemTheo_4,
	@HoSoKemTheo_5,
	@HoSoKemTheo_6,
	@HoSoKemTheo_7,
	@HoSoKemTheo_8,
	@HoSoKemTheo_9,
	@HoSoKemTheo_10,
	@TrangThaiXuLy
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Update]
	@ID bigint,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaDonViCapPhep varchar(6),
	@TenDonViCapPhep nvarchar(300),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@MaThuongNhanXuatKhau varchar(13),
	@TenThuongNhanXuatKhau nvarchar(210),
	@MaBuuChinhXuatKhau varchar(9),
	@SoNhaTenDuongXuatKhau varchar(35),
	@PhuongXaXuatKhau nvarchar(35),
	@QuanHuyenXuatKhau nvarchar(35),
	@TinhThanhPhoXuatKhau nvarchar(35),
	@MaQuocGiaXuatKhau varchar(2),
	@SoDienThoaiXuatKhau varchar(20),
	@SoFaxXuatKhau varchar(20),
	@EmailXuatKhau varchar(210),
	@SoHopDong nvarchar(35),
	@SoVanDon varchar(35),
	@MaBenDi varchar(6),
	@TenBenDi varchar(35),
	@MaThuongNhanNhapKhau varchar(13),
	@TenThuongNhanNhapKhau nvarchar(300),
	@MaBuuChinhNhapKhau varchar(7),
	@SoNhaTenDuongNhapKhau nvarchar(300),
	@MaQuocGiaNhapKhau varchar(2),
	@SoDienThoaiNhapKhau varchar(20),
	@SoFaxNhapKhau varchar(20),
	@EmailNhapKhau varchar(70),
	@MaBenDen varchar(6),
	@TenBenDen varchar(35),
	@ThoiGianNhapKhauDuKien datetime,
	@GiaTriHangHoa numeric(19, 2),
	@DonViTienTe varchar(3),
	@DiaDiemTapKetHangHoa nvarchar(70),
	@ThoiGianKiemTra datetime,
	@DiaDiemKiemTra nvarchar(70),
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@DaiDienThuongNhanNhapKhau nvarchar(300),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(70),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@GhiChuDanhChoCoQuanCapPhep nvarchar(500),
	@DaiDienCoQuanKiemTra nvarchar(500),
	@NgayKhaiBao datetime,
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@TrangThaiXuLy int
AS

UPDATE
	[dbo].[t_KDT_VNACC_GiayPhep_SFA]
SET
	[MaNguoiKhai] = @MaNguoiKhai,
	[TenNguoiKhai] = @TenNguoiKhai,
	[MaDonViCapPhep] = @MaDonViCapPhep,
	[TenDonViCapPhep] = @TenDonViCapPhep,
	[SoDonXinCapPhep] = @SoDonXinCapPhep,
	[ChucNangChungTu] = @ChucNangChungTu,
	[LoaiGiayPhep] = @LoaiGiayPhep,
	[MaThuongNhanXuatKhau] = @MaThuongNhanXuatKhau,
	[TenThuongNhanXuatKhau] = @TenThuongNhanXuatKhau,
	[MaBuuChinhXuatKhau] = @MaBuuChinhXuatKhau,
	[SoNhaTenDuongXuatKhau] = @SoNhaTenDuongXuatKhau,
	[PhuongXaXuatKhau] = @PhuongXaXuatKhau,
	[QuanHuyenXuatKhau] = @QuanHuyenXuatKhau,
	[TinhThanhPhoXuatKhau] = @TinhThanhPhoXuatKhau,
	[MaQuocGiaXuatKhau] = @MaQuocGiaXuatKhau,
	[SoDienThoaiXuatKhau] = @SoDienThoaiXuatKhau,
	[SoFaxXuatKhau] = @SoFaxXuatKhau,
	[EmailXuatKhau] = @EmailXuatKhau,
	[SoHopDong] = @SoHopDong,
	[SoVanDon] = @SoVanDon,
	[MaBenDi] = @MaBenDi,
	[TenBenDi] = @TenBenDi,
	[MaThuongNhanNhapKhau] = @MaThuongNhanNhapKhau,
	[TenThuongNhanNhapKhau] = @TenThuongNhanNhapKhau,
	[MaBuuChinhNhapKhau] = @MaBuuChinhNhapKhau,
	[SoNhaTenDuongNhapKhau] = @SoNhaTenDuongNhapKhau,
	[MaQuocGiaNhapKhau] = @MaQuocGiaNhapKhau,
	[SoDienThoaiNhapKhau] = @SoDienThoaiNhapKhau,
	[SoFaxNhapKhau] = @SoFaxNhapKhau,
	[EmailNhapKhau] = @EmailNhapKhau,
	[MaBenDen] = @MaBenDen,
	[TenBenDen] = @TenBenDen,
	[ThoiGianNhapKhauDuKien] = @ThoiGianNhapKhauDuKien,
	[GiaTriHangHoa] = @GiaTriHangHoa,
	[DonViTienTe] = @DonViTienTe,
	[DiaDiemTapKetHangHoa] = @DiaDiemTapKetHangHoa,
	[ThoiGianKiemTra] = @ThoiGianKiemTra,
	[DiaDiemKiemTra] = @DiaDiemKiemTra,
	[HoSoLienQuan] = @HoSoLienQuan,
	[GhiChu] = @GhiChu,
	[DaiDienThuongNhanNhapKhau] = @DaiDienThuongNhanNhapKhau,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[MaKetQuaXuLy] = @MaKetQuaXuLy,
	[KetQuaXuLySoGiayPhep] = @KetQuaXuLySoGiayPhep,
	[KetQuaXuLyNgayCap] = @KetQuaXuLyNgayCap,
	[KetQuaXuLyHieuLucTuNgay] = @KetQuaXuLyHieuLucTuNgay,
	[KetQuaXuLyHieuLucDenNgay] = @KetQuaXuLyHieuLucDenNgay,
	[GhiChuDanhChoCoQuanCapPhep] = @GhiChuDanhChoCoQuanCapPhep,
	[DaiDienCoQuanKiemTra] = @DaiDienCoQuanKiemTra,
	[NgayKhaiBao] = @NgayKhaiBao,
	[HoSoKemTheo_1] = @HoSoKemTheo_1,
	[HoSoKemTheo_2] = @HoSoKemTheo_2,
	[HoSoKemTheo_3] = @HoSoKemTheo_3,
	[HoSoKemTheo_4] = @HoSoKemTheo_4,
	[HoSoKemTheo_5] = @HoSoKemTheo_5,
	[HoSoKemTheo_6] = @HoSoKemTheo_6,
	[HoSoKemTheo_7] = @HoSoKemTheo_7,
	[HoSoKemTheo_8] = @HoSoKemTheo_8,
	[HoSoKemTheo_9] = @HoSoKemTheo_9,
	[HoSoKemTheo_10] = @HoSoKemTheo_10,
	[TrangThaiXuLy] = @TrangThaiXuLy
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_InsertUpdate]
	@ID bigint,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaDonViCapPhep varchar(6),
	@TenDonViCapPhep nvarchar(300),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@MaThuongNhanXuatKhau varchar(13),
	@TenThuongNhanXuatKhau nvarchar(210),
	@MaBuuChinhXuatKhau varchar(9),
	@SoNhaTenDuongXuatKhau varchar(35),
	@PhuongXaXuatKhau nvarchar(35),
	@QuanHuyenXuatKhau nvarchar(35),
	@TinhThanhPhoXuatKhau nvarchar(35),
	@MaQuocGiaXuatKhau varchar(2),
	@SoDienThoaiXuatKhau varchar(20),
	@SoFaxXuatKhau varchar(20),
	@EmailXuatKhau varchar(210),
	@SoHopDong nvarchar(35),
	@SoVanDon varchar(35),
	@MaBenDi varchar(6),
	@TenBenDi varchar(35),
	@MaThuongNhanNhapKhau varchar(13),
	@TenThuongNhanNhapKhau nvarchar(300),
	@MaBuuChinhNhapKhau varchar(7),
	@SoNhaTenDuongNhapKhau nvarchar(300),
	@MaQuocGiaNhapKhau varchar(2),
	@SoDienThoaiNhapKhau varchar(20),
	@SoFaxNhapKhau varchar(20),
	@EmailNhapKhau varchar(70),
	@MaBenDen varchar(6),
	@TenBenDen varchar(35),
	@ThoiGianNhapKhauDuKien datetime,
	@GiaTriHangHoa numeric(19, 2),
	@DonViTienTe varchar(3),
	@DiaDiemTapKetHangHoa nvarchar(70),
	@ThoiGianKiemTra datetime,
	@DiaDiemKiemTra nvarchar(70),
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@DaiDienThuongNhanNhapKhau nvarchar(300),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(70),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@GhiChuDanhChoCoQuanCapPhep nvarchar(500),
	@DaiDienCoQuanKiemTra nvarchar(500),
	@NgayKhaiBao datetime,
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@TrangThaiXuLy int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_GiayPhep_SFA] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_GiayPhep_SFA] 
		SET
			[MaNguoiKhai] = @MaNguoiKhai,
			[TenNguoiKhai] = @TenNguoiKhai,
			[MaDonViCapPhep] = @MaDonViCapPhep,
			[TenDonViCapPhep] = @TenDonViCapPhep,
			[SoDonXinCapPhep] = @SoDonXinCapPhep,
			[ChucNangChungTu] = @ChucNangChungTu,
			[LoaiGiayPhep] = @LoaiGiayPhep,
			[MaThuongNhanXuatKhau] = @MaThuongNhanXuatKhau,
			[TenThuongNhanXuatKhau] = @TenThuongNhanXuatKhau,
			[MaBuuChinhXuatKhau] = @MaBuuChinhXuatKhau,
			[SoNhaTenDuongXuatKhau] = @SoNhaTenDuongXuatKhau,
			[PhuongXaXuatKhau] = @PhuongXaXuatKhau,
			[QuanHuyenXuatKhau] = @QuanHuyenXuatKhau,
			[TinhThanhPhoXuatKhau] = @TinhThanhPhoXuatKhau,
			[MaQuocGiaXuatKhau] = @MaQuocGiaXuatKhau,
			[SoDienThoaiXuatKhau] = @SoDienThoaiXuatKhau,
			[SoFaxXuatKhau] = @SoFaxXuatKhau,
			[EmailXuatKhau] = @EmailXuatKhau,
			[SoHopDong] = @SoHopDong,
			[SoVanDon] = @SoVanDon,
			[MaBenDi] = @MaBenDi,
			[TenBenDi] = @TenBenDi,
			[MaThuongNhanNhapKhau] = @MaThuongNhanNhapKhau,
			[TenThuongNhanNhapKhau] = @TenThuongNhanNhapKhau,
			[MaBuuChinhNhapKhau] = @MaBuuChinhNhapKhau,
			[SoNhaTenDuongNhapKhau] = @SoNhaTenDuongNhapKhau,
			[MaQuocGiaNhapKhau] = @MaQuocGiaNhapKhau,
			[SoDienThoaiNhapKhau] = @SoDienThoaiNhapKhau,
			[SoFaxNhapKhau] = @SoFaxNhapKhau,
			[EmailNhapKhau] = @EmailNhapKhau,
			[MaBenDen] = @MaBenDen,
			[TenBenDen] = @TenBenDen,
			[ThoiGianNhapKhauDuKien] = @ThoiGianNhapKhauDuKien,
			[GiaTriHangHoa] = @GiaTriHangHoa,
			[DonViTienTe] = @DonViTienTe,
			[DiaDiemTapKetHangHoa] = @DiaDiemTapKetHangHoa,
			[ThoiGianKiemTra] = @ThoiGianKiemTra,
			[DiaDiemKiemTra] = @DiaDiemKiemTra,
			[HoSoLienQuan] = @HoSoLienQuan,
			[GhiChu] = @GhiChu,
			[DaiDienThuongNhanNhapKhau] = @DaiDienThuongNhanNhapKhau,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[MaKetQuaXuLy] = @MaKetQuaXuLy,
			[KetQuaXuLySoGiayPhep] = @KetQuaXuLySoGiayPhep,
			[KetQuaXuLyNgayCap] = @KetQuaXuLyNgayCap,
			[KetQuaXuLyHieuLucTuNgay] = @KetQuaXuLyHieuLucTuNgay,
			[KetQuaXuLyHieuLucDenNgay] = @KetQuaXuLyHieuLucDenNgay,
			[GhiChuDanhChoCoQuanCapPhep] = @GhiChuDanhChoCoQuanCapPhep,
			[DaiDienCoQuanKiemTra] = @DaiDienCoQuanKiemTra,
			[NgayKhaiBao] = @NgayKhaiBao,
			[HoSoKemTheo_1] = @HoSoKemTheo_1,
			[HoSoKemTheo_2] = @HoSoKemTheo_2,
			[HoSoKemTheo_3] = @HoSoKemTheo_3,
			[HoSoKemTheo_4] = @HoSoKemTheo_4,
			[HoSoKemTheo_5] = @HoSoKemTheo_5,
			[HoSoKemTheo_6] = @HoSoKemTheo_6,
			[HoSoKemTheo_7] = @HoSoKemTheo_7,
			[HoSoKemTheo_8] = @HoSoKemTheo_8,
			[HoSoKemTheo_9] = @HoSoKemTheo_9,
			[HoSoKemTheo_10] = @HoSoKemTheo_10,
			[TrangThaiXuLy] = @TrangThaiXuLy
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SFA]
		(
			[MaNguoiKhai],
			[TenNguoiKhai],
			[MaDonViCapPhep],
			[TenDonViCapPhep],
			[SoDonXinCapPhep],
			[ChucNangChungTu],
			[LoaiGiayPhep],
			[MaThuongNhanXuatKhau],
			[TenThuongNhanXuatKhau],
			[MaBuuChinhXuatKhau],
			[SoNhaTenDuongXuatKhau],
			[PhuongXaXuatKhau],
			[QuanHuyenXuatKhau],
			[TinhThanhPhoXuatKhau],
			[MaQuocGiaXuatKhau],
			[SoDienThoaiXuatKhau],
			[SoFaxXuatKhau],
			[EmailXuatKhau],
			[SoHopDong],
			[SoVanDon],
			[MaBenDi],
			[TenBenDi],
			[MaThuongNhanNhapKhau],
			[TenThuongNhanNhapKhau],
			[MaBuuChinhNhapKhau],
			[SoNhaTenDuongNhapKhau],
			[MaQuocGiaNhapKhau],
			[SoDienThoaiNhapKhau],
			[SoFaxNhapKhau],
			[EmailNhapKhau],
			[MaBenDen],
			[TenBenDen],
			[ThoiGianNhapKhauDuKien],
			[GiaTriHangHoa],
			[DonViTienTe],
			[DiaDiemTapKetHangHoa],
			[ThoiGianKiemTra],
			[DiaDiemKiemTra],
			[HoSoLienQuan],
			[GhiChu],
			[DaiDienThuongNhanNhapKhau],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[MaKetQuaXuLy],
			[KetQuaXuLySoGiayPhep],
			[KetQuaXuLyNgayCap],
			[KetQuaXuLyHieuLucTuNgay],
			[KetQuaXuLyHieuLucDenNgay],
			[GhiChuDanhChoCoQuanCapPhep],
			[DaiDienCoQuanKiemTra],
			[NgayKhaiBao],
			[HoSoKemTheo_1],
			[HoSoKemTheo_2],
			[HoSoKemTheo_3],
			[HoSoKemTheo_4],
			[HoSoKemTheo_5],
			[HoSoKemTheo_6],
			[HoSoKemTheo_7],
			[HoSoKemTheo_8],
			[HoSoKemTheo_9],
			[HoSoKemTheo_10],
			[TrangThaiXuLy]
		)
		VALUES 
		(
			@MaNguoiKhai,
			@TenNguoiKhai,
			@MaDonViCapPhep,
			@TenDonViCapPhep,
			@SoDonXinCapPhep,
			@ChucNangChungTu,
			@LoaiGiayPhep,
			@MaThuongNhanXuatKhau,
			@TenThuongNhanXuatKhau,
			@MaBuuChinhXuatKhau,
			@SoNhaTenDuongXuatKhau,
			@PhuongXaXuatKhau,
			@QuanHuyenXuatKhau,
			@TinhThanhPhoXuatKhau,
			@MaQuocGiaXuatKhau,
			@SoDienThoaiXuatKhau,
			@SoFaxXuatKhau,
			@EmailXuatKhau,
			@SoHopDong,
			@SoVanDon,
			@MaBenDi,
			@TenBenDi,
			@MaThuongNhanNhapKhau,
			@TenThuongNhanNhapKhau,
			@MaBuuChinhNhapKhau,
			@SoNhaTenDuongNhapKhau,
			@MaQuocGiaNhapKhau,
			@SoDienThoaiNhapKhau,
			@SoFaxNhapKhau,
			@EmailNhapKhau,
			@MaBenDen,
			@TenBenDen,
			@ThoiGianNhapKhauDuKien,
			@GiaTriHangHoa,
			@DonViTienTe,
			@DiaDiemTapKetHangHoa,
			@ThoiGianKiemTra,
			@DiaDiemKiemTra,
			@HoSoLienQuan,
			@GhiChu,
			@DaiDienThuongNhanNhapKhau,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@MaKetQuaXuLy,
			@KetQuaXuLySoGiayPhep,
			@KetQuaXuLyNgayCap,
			@KetQuaXuLyHieuLucTuNgay,
			@KetQuaXuLyHieuLucDenNgay,
			@GhiChuDanhChoCoQuanCapPhep,
			@DaiDienCoQuanKiemTra,
			@NgayKhaiBao,
			@HoSoKemTheo_1,
			@HoSoKemTheo_2,
			@HoSoKemTheo_3,
			@HoSoKemTheo_4,
			@HoSoKemTheo_5,
			@HoSoKemTheo_6,
			@HoSoKemTheo_7,
			@HoSoKemTheo_8,
			@HoSoKemTheo_9,
			@HoSoKemTheo_10,
			@TrangThaiXuLy
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_GiayPhep_SFA]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_GiayPhep_SFA] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[SoDienThoaiXuatKhau],
	[SoFaxXuatKhau],
	[EmailXuatKhau],
	[SoHopDong],
	[SoVanDon],
	[MaBenDi],
	[TenBenDi],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[SoNhaTenDuongNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[MaBenDen],
	[TenBenDen],
	[ThoiGianNhapKhauDuKien],
	[GiaTriHangHoa],
	[DonViTienTe],
	[DiaDiemTapKetHangHoa],
	[ThoiGianKiemTra],
	[DiaDiemKiemTra],
	[HoSoLienQuan],
	[GhiChu],
	[DaiDienThuongNhanNhapKhau],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienCoQuanKiemTra],
	[NgayKhaiBao],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SFA]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[SoDienThoaiXuatKhau],
	[SoFaxXuatKhau],
	[EmailXuatKhau],
	[SoHopDong],
	[SoVanDon],
	[MaBenDi],
	[TenBenDi],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[SoNhaTenDuongNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[MaBenDen],
	[TenBenDen],
	[ThoiGianNhapKhauDuKien],
	[GiaTriHangHoa],
	[DonViTienTe],
	[DiaDiemTapKetHangHoa],
	[ThoiGianKiemTra],
	[DiaDiemKiemTra],
	[HoSoLienQuan],
	[GhiChu],
	[DaiDienThuongNhanNhapKhau],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienCoQuanKiemTra],
	[NgayKhaiBao],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[TrangThaiXuLy]
FROM [dbo].[t_KDT_VNACC_GiayPhep_SFA] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[SoDienThoaiXuatKhau],
	[SoFaxXuatKhau],
	[EmailXuatKhau],
	[SoHopDong],
	[SoVanDon],
	[MaBenDi],
	[TenBenDi],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[SoNhaTenDuongNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[MaBenDen],
	[TenBenDen],
	[ThoiGianNhapKhauDuKien],
	[GiaTriHangHoa],
	[DonViTienTe],
	[DiaDiemTapKetHangHoa],
	[ThoiGianKiemTra],
	[DiaDiemKiemTra],
	[HoSoLienQuan],
	[GhiChu],
	[DaiDienThuongNhanNhapKhau],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienCoQuanKiemTra],
	[NgayKhaiBao],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SFA]	

GO

