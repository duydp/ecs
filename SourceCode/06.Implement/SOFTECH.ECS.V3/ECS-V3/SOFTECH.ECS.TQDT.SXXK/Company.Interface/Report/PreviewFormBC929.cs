using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using Company.Interface.Report.SXXK;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.Report
{
    public partial class PreviewFormBC929 : BaseForm
    {
        private BangKeToKhaiNhap_929 BKTKN = new BangKeToKhaiNhap_929();
        private BangKeToKhaiXuat_929 BKTKX = new BangKeToKhaiXuat_929();
        private BCNPLXuatNhapTon_929 BCNPLXNT = new BCNPLXuatNhapTon_929();
        private BCThueXNK_929 BCThue = new BCThueXNK_929();
        private BC03_929 BCNLSXXK = new BC03_929();
        public int LanThanhLy;
        public int SoHoSo;
        public DateTime NgayHoSo;
        public BKToKhaiNhapCollection TKNHoanThueCollection = new BKToKhaiNhapCollection();
        public BKToKhaiNhapCollection TKNKhongThuCollection = new BKToKhaiNhapCollection();
        private string WhereHoanThue = "";
        private string WhereKhongThu = "";
        private string WhereHoanThue1 = "";
        private string WhereKhongThu1 = "";
        public PreviewFormBC929()
        {
            InitializeComponent();
        }

        private void explorerBar1_ItemClick(object sender, Janus.Windows.ExplorerBar.ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                case "BKTKN":
                    this.BKTKN.LanThanhLy = this.LanThanhLy;
                    this.BKTKN.SoHoSo = this.SoHoSo;
                    this.BKTKN.BindReport("");
                    printControl1.PrintingSystem = this.BKTKN.PrintingSystem;
                    this.BKTKN.CreateDocument();
                    break;
                case "BKTKX":
                    this.BKTKX.BindReport(this.LanThanhLy);
                    this.BKTKX.SoHoSo = this.SoHoSo;
                    printControl1.PrintingSystem = this.BKTKX.PrintingSystem;
                    this.BKTKX.CreateDocument();
                    break;
                case "BCNPLNXT":
                    this.BCNPLXNT.LanThanhLy = this.LanThanhLy;
                    this.BCNPLXNT.SoHoSo = this.SoHoSo;
                    this.BCNPLXNT.BindReport("");
                    printControl1.PrintingSystem = this.BCNPLXNT.PrintingSystem;
                    this.BCNPLXNT.CreateDocument();
                    break;
                case "BCNLSXXK":
                    this.BCNLSXXK.LanThanhLy = this.LanThanhLy;
                    this.BCNLSXXK.SoHoSo = this.SoHoSo;
                    this.BCNLSXXK.BindReport("");
                    printControl1.PrintingSystem = this.BCNLSXXK.PrintingSystem;
                    this.BCNLSXXK.CreateDocument();
                    break;
                case "BCThueNK":
                    this.BCThue.LanThanhLy = this.LanThanhLy;
                    this.BCThue.SoHoSo = this.SoHoSo;
                    this.BCThue.BindReport("");
                    printControl1.PrintingSystem = this.BCThue.PrintingSystem;
                    this.BCThue.CreateDocument();
                    break;
                case "BKTKNHoanThue":
                    this.BKTKN.LanThanhLy = this.LanThanhLy;
                    this.BKTKN.SoHoSo = this.SoHoSo;
                    this.BKTKN.BindReport(this.WhereHoanThue1);
                    printControl1.PrintingSystem = this.BKTKN.PrintingSystem;
                    this.BKTKN.CreateDocument();
                    break;
                case "BKTKXHoanThue":
                    this.BKTKX.BindReport(this.LanThanhLy);
                    this.BKTKX.SoHoSo = this.SoHoSo;
                    printControl1.PrintingSystem = this.BKTKX.PrintingSystem;
                    this.BKTKX.CreateDocument();
                    break;
                case "BCNLSXXKHoanThue":
                    this.BCNLSXXK.LanThanhLy = this.LanThanhLy;
                    this.BCNLSXXK.SoHoSo = this.SoHoSo;
                    this.BCNLSXXK.BindReport(this.WhereHoanThue);
                    printControl1.PrintingSystem = this.BCNLSXXK.PrintingSystem;
                    this.BCNLSXXK.CreateDocument();
                    break;
                case "BCNPLNXTHoanThue":
                    this.BCNPLXNT.LanThanhLy = this.LanThanhLy;
                    this.BCNPLXNT.SoHoSo = this.SoHoSo;
                    this.BCNPLXNT.BindReport(this.WhereHoanThue);
                    printControl1.PrintingSystem = this.BCNPLXNT.PrintingSystem;
                    this.BCNPLXNT.CreateDocument();
                    break;
                case "BCThueNKHoanThue":
                    this.BCThue.LanThanhLy = this.LanThanhLy;
                    this.BCThue.SoHoSo = this.SoHoSo;
                    this.BCThue.BindReport(this.WhereHoanThue);
                    printControl1.PrintingSystem = this.BCThue.PrintingSystem;
                    this.BCThue.CreateDocument();
                    break;
                case "BKTKNKhongThu":
                    this.BKTKN.LanThanhLy = this.LanThanhLy;
                    this.BKTKN.SoHoSo = this.SoHoSo;
                    this.BKTKN.BindReport(this.WhereKhongThu1);
                    printControl1.PrintingSystem = this.BKTKN.PrintingSystem;
                    this.BKTKN.CreateDocument();
                    break;
                case "BKTKXKhongThu":
                    this.BKTKX.BindReport(this.LanThanhLy);
                    this.BKTKX.SoHoSo = this.SoHoSo;
                    printControl1.PrintingSystem = this.BKTKX.PrintingSystem;
                    this.BKTKX.CreateDocument();
                    break;
                case "BCNLSXXKKhongThu":
                    this.BCNLSXXK.LanThanhLy = this.LanThanhLy;
                    this.BCNLSXXK.SoHoSo = this.SoHoSo;
                    this.BCNLSXXK.BindReport(this.WhereKhongThu);
                    printControl1.PrintingSystem = this.BCNLSXXK.PrintingSystem;
                    this.BCNLSXXK.CreateDocument();
                    break;
                case "BCNPLNXTKhongThu":
                    this.BCNPLXNT.LanThanhLy = this.LanThanhLy;
                    this.BCNPLXNT.SoHoSo = this.SoHoSo;
                    this.BCNPLXNT.BindReport(this.WhereKhongThu);
                    printControl1.PrintingSystem = this.BCNPLXNT.PrintingSystem;
                    this.BCNPLXNT.CreateDocument();
                    break;
                case "BCThueNKKhongThu":
                    this.BCThue.LanThanhLy = this.LanThanhLy;
                    this.BCThue.SoHoSo = this.SoHoSo;
                    this.BCThue.BindReport(this.WhereKhongThu);
                    printControl1.PrintingSystem = this.BCThue.PrintingSystem;
                    this.BCThue.CreateDocument();
                    break;
            }
        }

        private void PreviewForm_Load(object sender, EventArgs e)
        {
            printPreviewBarItem4.Enabled = printPreviewBarItem5.Enabled = Program.isActivated;

            if (this.TKNHoanThueCollection.Count <= 0) explorerBar1.Groups[2].Visible = false;
            if (this.TKNKhongThuCollection.Count <= 0) explorerBar1.Groups[1].Visible = false;
            if (this.TKNHoanThueCollection.Count > 0)
            {
                this.WhereHoanThue = " AND (";
                foreach (BKToKhaiNhap tkn in this.TKNHoanThueCollection)
                {
                    this.WhereHoanThue += " (SoToKhaiNhap = " + tkn.SoToKhai + " AND MaLoaiHinhNhap = '" + tkn.MaLoaiHinh + "' AND year(NgayDangKyNhap)= " + tkn.NamDangKy + ") OR";
                }
                this.WhereHoanThue = this.WhereHoanThue.Remove(this.WhereHoanThue.Length - 2);
                this.WhereHoanThue += ")";
            }
            if (this.TKNKhongThuCollection.Count > 0)
            {
                this.WhereKhongThu = " AND (";
                foreach (BKToKhaiNhap tkn in this.TKNKhongThuCollection)
                {
                    this.WhereKhongThu += " (SoToKhaiNhap = " + tkn.SoToKhai + " AND MaLoaiHinhNhap = '" + tkn.MaLoaiHinh + "' AND year(NgayDangKyNhap)= " + tkn.NamDangKy + ") OR";
                }
                this.WhereKhongThu = this.WhereKhongThu.Remove(this.WhereKhongThu.Length - 2);
                this.WhereKhongThu += ")";
            }
            if (this.TKNHoanThueCollection.Count > 0)
            {
                this.WhereHoanThue1 = " WHERE ";
                foreach (BKToKhaiNhap tkn in this.TKNHoanThueCollection)
                {
                    this.WhereHoanThue1 += " (a.SoToKhai = " + tkn.SoToKhai + " AND a.MaLoaiHinh = '" + tkn.MaLoaiHinh + "' AND year(a.NgayDangKy)= " + tkn.NamDangKy + ") OR";
                }
                this.WhereHoanThue1 = this.WhereHoanThue1.Remove(this.WhereHoanThue1.Length - 2);
            }
            if (this.TKNKhongThuCollection.Count > 0)
            {
                this.WhereKhongThu1 = " WHERE ";
                foreach (BKToKhaiNhap tkn in this.TKNKhongThuCollection)
                {
                    this.WhereKhongThu1 += " (a.SoToKhai = " + tkn.SoToKhai + " AND a.MaLoaiHinh = '" + tkn.MaLoaiHinh + "' AND year(a.NgayDangKy)= " + tkn.NamDangKy + ") OR";
                }
                this.WhereKhongThu1 = this.WhereKhongThu1.Remove(this.WhereKhongThu1.Length - 2);
            }
            if(GlobalSettings.GiaoDien.Id == "Office2003")
                this.defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.BKTKN.LanThanhLy = this.LanThanhLy;
            this.BKTKN.SoHoSo = this.SoHoSo;
            this.BKTKN.BindReport("");
            printControl1.PrintingSystem = this.BKTKN.PrintingSystem;
            this.BKTKN.CreateDocument();

        }
    }
}