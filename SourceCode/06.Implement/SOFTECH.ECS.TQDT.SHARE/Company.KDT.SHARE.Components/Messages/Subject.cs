﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Chức năng khai báo
    /// </summary>
    [XmlRoot("Subject")]
    public class Subject : SubjectBase
    {

        string _sendApp = Globals.AppSend;
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("sendApplication")]
        public string SendApplication
        {
            get
            {
                return _sendApp;
            }
            set
            {
                _sendApp = value;
            }
        }
        string _receiveApp = "ECS";
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("receiveApplication")]
        public string ReceiveApplication
        {
            get { return _receiveApp; }
            set
            {
                _receiveApp = value;
            }
        }

        string _decraction = "1";
        /// <summary>
        /// Loại hình khai báo, mặc định là 1
        /// 0. Khai từ xa
        /// 1. Thông quan điện tử
        /// </summary>
        [XmlElement("declarationType")]
        public string DeclarationType { get { return _decraction; } set { _decraction = value; } }
    }
}
