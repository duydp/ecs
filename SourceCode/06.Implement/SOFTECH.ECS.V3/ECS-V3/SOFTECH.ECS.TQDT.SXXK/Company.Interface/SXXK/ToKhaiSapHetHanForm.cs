using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ToKhai;
using Company.Interface.Report.SXXK;

namespace Company.Interface.SXXK
{
    public partial class ToKhaiSapHetHanForm : Company.Interface.BaseForm
    {
        private  DataTable dt = new DataTable();
        public ToKhaiSapHetHanForm()
        {
            InitializeComponent();
        }

        private void ToKhaiSapHetHanForm_Load(object sender, EventArgs e)
        {
            int hanThanhKhoan = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["HanThanhKhoan"]);
            int thoiGianTK = GlobalSettings.ThongBaoHetHan;
            this.dt = new ToKhaiMauDich().GetToKhaiSapHetHanTK(hanThanhKhoan, thoiGianTK, GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
            dgList.DataSource = this.dt;
            dgList.Tables[0].Columns["Luong"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["Ton"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
        }


        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (this.dt.Rows.Count > 0)
            {
                ToKhaiHetHanTKReport report = new ToKhaiHetHanTKReport();
                report.dt = this.dt;
                report.BindReport();
                report.ShowPreview();
            }
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}

