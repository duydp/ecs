using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace Company.GC.BLL.GC
{
    public partial class NhomSanPham : EntityBase
    {
        #region Private members.

        protected long _HopDong_ID;
        protected string _MaSanPham = String.Empty;
        protected decimal _SoLuong;
        protected decimal _GiaGiaCong;
        protected int _STTHang;
        protected string _TenSanPham = String.Empty;
        protected int _TrangThai;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long HopDong_ID
        {
            set { this._HopDong_ID = value; }
            get { return this._HopDong_ID; }
        }
        public string MaSanPham
        {
            set { this._MaSanPham = value; }
            get { return this._MaSanPham; }
        }
        public decimal SoLuong
        {
            set { this._SoLuong = value; }
            get { return this._SoLuong; }
        }
        public decimal GiaGiaCong
        {
            set { this._GiaGiaCong = value; }
            get { return this._GiaGiaCong; }
        }
        public int STTHang
        {
            set { this._STTHang = value; }
            get { return this._STTHang; }
        }
        public string TenSanPham
        {
            set { this._TenSanPham = value; }
            get { return this._TenSanPham; }
        }
        public int TrangThai
        {
            set { this._TrangThai = value; }
            get { return this._TrangThai; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_GC_NhomSanPham_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) this._MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) this._SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaGiaCong"))) this._GiaGiaCong = reader.GetDecimal(reader.GetOrdinal("GiaGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) this._TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this._TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public List<NhomSanPham> SelectCollectionBy_HopDong_ID()
        {
            string spName = "p_GC_NhomSanPham_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            List<NhomSanPham> collection = new List<NhomSanPham>();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                NhomSanPham entity = new NhomSanPham();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaGiaCong"))) entity.GiaGiaCong = reader.GetDecimal(reader.GetOrdinal("GiaGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_HopDong_ID()
        {
            string spName = "p_GC_NhomSanPham_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public DataSet SelectAll()
        {
            string spName = "p_GC_NhomSanPham_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_GC_NhomSanPham_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_GC_NhomSanPham_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_GC_NhomSanPham_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public List<NhomSanPham> SelectCollectionAll()
        {
            List<NhomSanPham> collection = new List<NhomSanPham>();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                NhomSanPham entity = new NhomSanPham();

                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaGiaCong"))) entity.GiaGiaCong = reader.GetDecimal(reader.GetOrdinal("GiaGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public List<NhomSanPham> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            List<NhomSanPham> collection = new List<NhomSanPham>();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                NhomSanPham entity = new NhomSanPham();

                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaGiaCong"))) entity.GiaGiaCong = reader.GetDecimal(reader.GetOrdinal("GiaGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_NhomSanPham_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@GiaGiaCong", SqlDbType.Money, this._GiaGiaCong);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(List<NhomSanPham> collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NhomSanPham item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, List<NhomSanPham> collection)
        {
            foreach (NhomSanPham item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_NhomSanPham_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@GiaGiaCong", SqlDbType.Money, this._GiaGiaCong);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(List<NhomSanPham> collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NhomSanPham item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_NhomSanPham_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@GiaGiaCong", SqlDbType.Money, this._GiaGiaCong);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(List<NhomSanPham> collection, SqlTransaction transaction)
        {
            foreach (NhomSanPham item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_NhomSanPham_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(List<NhomSanPham> collection, SqlTransaction transaction)
        {
            foreach (NhomSanPham item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(List<NhomSanPham> collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NhomSanPham item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public int DeleteBy_HopDong_ID()
        {
            string spName = "p_GC_NhomSanPham_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand);
        }
        public int DeleteBy_HopDong_ID(SqlTransaction transaction)
        {
            string spName = "p_GC_NhomSanPham_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }
        #endregion
    }
}