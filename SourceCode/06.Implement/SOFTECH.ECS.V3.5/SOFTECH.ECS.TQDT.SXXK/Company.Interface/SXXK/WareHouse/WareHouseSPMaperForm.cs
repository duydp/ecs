﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.Interface.SXXK;
using Company.KDT.SHARE.QuanLyChungTu.CX;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseSPMaperForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public T_KHOKETOAN_SANPHAM SP = new T_KHOKETOAN_SANPHAM();
        public T_KHOKETOAN_SANPHAM_MAP SPMap = new T_KHOKETOAN_SANPHAM_MAP();
        public bool isAdd = true;

        NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;

        public WareHouseSPMaperForm()
        {
            InitializeComponent();

            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            txtDonViTinhKT.DataSource = this._DonViTinh;
            txtDonViTinhKT.DisplayMember = "Ten";
            txtDonViTinhKT.ValueMember = "Ten";
            txtDonViTinhKT.ReadOnly = false;

            txtDonViTinhSP.DataSource = this._DonViTinh;
            txtDonViTinhSP.DisplayMember = "Ten";
            txtDonViTinhSP.ValueMember = "Ten";
            txtDonViTinhSP.ReadOnly = false;

            cbbMaKho.DataSource = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
            cbbMaKho.DisplayMember = "MAKHO";
            cbbMaKho.ValueMember = "MAKHO";

            cbbTK.DataSource = T_KHOKETOAN_TAIKHOAN.SelectCollectionAll();
            cbbTK.DisplayMember = "MATK";
            cbbTK.ValueMember = "MATK";
        }

        private void WareHouseSPMaperForm_Load(object sender, EventArgs e)
        {
            SetSP();
        }
        private void BindData()
        {
            try
            {
                grList.Refetch();
                grList.DataSource = SP.SPCollection;
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetSPMap()
        {
            try
            {

                SPMap.MASP = txtMaSP.Text.ToString();
                SPMap.TENSP = txtTenSP.Text.ToString();
                SPMap.DVT = txtDonViTinhSP.Text.ToString();
                SPMap.MASPMAP = txtMaSPKT.Text.ToString();
                SPMap.TENSPMAP = txtTenSPKT.Text.ToString();
                SPMap.DVTMAP = txtDonViTinhKT.Text.ToString();
                SPMap.TYLEQD = Convert.ToDecimal(txtTLQD.Text);
                if (isAdd)
                    SP.SPCollection.Add(SPMap);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetSP()
        {
            SP.MASP = txtMaSPKT.Text.ToString();
            SP.TENSP = txtTenSPKT.Text.ToString();
            SP.MAHS = ctrMaSoHang.Code;
            SP.DVT = txtDonViTinhKT.Text.ToString();
            SP.TENTIENGANH = txtTenTiengAnh.Text.ToString();
            SP.GHICHU = txtGhiChu.Text.ToString();
            SP.MAKHO = cbbMaKho.Text.ToString();
            SP.TAIKHOAN = cbbTK.Text.ToString();
            SP.DONHANG = txtDonHang.Text.ToString();
            SP.NGUOIBAN = txtNguoiBan.Text.ToString();
            SP.KICHTHUOC = txtKichThuoc.Text.ToString();
            SP.MAUSAC = txtMauSac.Text.ToString();

            GetSPMap();
        }
        private void SetSP()
        {
            txtMaSPKT.Text = SP.MASP;
            txtTenSPKT.Text = SP.TENSP;
            ctrMaSoHang.Code = SP.MAHS;
            txtDonViTinhKT.Text = SP.DVT;
            txtTenTiengAnh.Text = SP.TENTIENGANH;
            txtGhiChu.Text = SP.GHICHU;
            cbbMaKho.Value = SP.MAKHO;
            cbbTK.Value = SP.TAIKHOAN;
            txtDonHang.Text = SP.DONHANG;
            txtNguoiBan.Text = SP.NGUOIBAN;
            txtMauSac.Text = SP.MAUSAC;
            txtKichThuoc.Text = SP.KICHTHUOC;

            BindData();
        }
        private void SetSPMap()
        {
            txtMaSP.Text = SPMap.MASP;
            txtTenSP.Text = SPMap.TENSP;
            txtDonViTinhSP.Text = SPMap.DVT;
            txtTLQD.Text = SPMap.TYLEQD.ToString();
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                ctrMaSoHang.SetValidate = !isOnlyWarning;
                ctrMaSoHang.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrMaSoHang.IsValidate;

                isValid &= ValidateControl.ValidateNull(txtMaSPKT, errorProvider, " Mã Sản phẩm ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenSPKT, errorProvider, " Tên Sản phẩm ");
                isValid &= ValidateControl.ValidateNull(txtDonViTinhKT, errorProvider, " Đơn vị tính ", isOnlyWarning);

                isValid &= ValidateControl.ValidateNull(txtMaSP, errorProvider, " Mã Sản phẩm ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenSP, errorProvider, " Tên Sản phẩm ");
                isValid &= ValidateControl.ValidateNull(txtMaHSSP, errorProvider, " Mã HS ");
                isValid &= ValidateControl.ValidateNull(txtDonViTinhSP, errorProvider, " Đơn vị tính ", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtTLQD, errorProvider, " Tỷ lệ quy đổi ");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void txtMaSP_ButtonClick(object sender, EventArgs e)
        {
            string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                if (this.NPLRegistedForm_CX == null)
                    this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
                this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm_CX.LoaiNPL = "3";
                this.NPLRegistedForm_CX.ShowDialog(this);
                if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                {
                    txtMaSP.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                    txtTenSP.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
                    txtMaHSSP.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                    txtDonViTinhSP.Text = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));

                }
            }
            else
            {
                this.SPRegistedForm = new SanPhamRegistedForm();
                this.SPRegistedForm.CalledForm = "HangMauDichForm";
                this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.SPRegistedForm.ShowDialog();
                if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                {
                    txtMaSP.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                    txtTenSP.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                    txtMaHSSP.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
                    txtDonViTinhSP.Text = DonViTinh_GetName(this.SPRegistedForm.SanPhamSelected.DVT_ID);
                }
            }
        }

        private void txtMaSP_Leave(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                IList<HangDuaVao> listTB = HangDuaVao.SelectCollectionDynamic(string.Format("Ma = '{0}' and LoaiNPL = '3'", txtMaSP.Text.Trim()), null);
                if (listTB != null && listTB.Count > 0)
                {
                    HangDuaVao tb = listTB[0];
                    txtMaHSSP.Text = tb.MaHS;
                    txtTenSP.Text = tb.Ten;
                    txtMaHSSP.Text = tb.MaHS;
                    txtDonViTinhSP.Text = DonViTinh_GetName(tb.DVT_ID);
                }
            }
            else 
            {
                Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = txtMaSP.Text;
                sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (sp.Load())
                {
                    txtMaSP.Text = sp.Ma;                                        
                    txtTenSP.Text = sp.Ten;
                    txtMaHSSP.Text = sp.MaHS;
                    txtDonViTinhSP.Text = DonViTinh_GetName(sp.DVT_ID);
                }
            }
        }
        private void CheckExitSP()
        {
            //Kiểm tra SP Map đã được khai báo .
            //if (isAdd)
            //{
            //    List<T_KHOKETOAN_SANPHAM> SPCollection = T_KHOKETOAN_SANPHAM.SelectCollectionDynamic("HOPDONG_ID = " + HD.ID + " AND MASP = '" + txtMaSPKT.Text.ToString().Trim() + "'", "");
            //    if (SPCollection.Count >= 1)
            //    {
            //        errorProvider.SetError(txtMaSPKT, "MÃ SẢN PHẨM  : ''" + txtMaSPKT.Text.ToString() + "'' NÀY ĐÃ MAP TRƯỚC ĐÓ .");
            //        return;
            //    }   
            //}
            if (isAdd)
            {
                //Kiểm tra SP VNACCS đã có trên lưới .
                foreach (T_KHOKETOAN_SANPHAM_MAP item in SP.SPCollection)
                {
                    if (item.MASPMAP == txtMaSP.Text.ToString().Trim())
                    {
                        errorProvider.SetError(txtMaSP, "MÃ SẢN PHẨM : ''" + txtMaSP.Text.ToString() + "'' ĐÃ CÓ TRÊN DANH SÁCH PHÍA DƯỚI .");
                        return;
                    }
                }
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetSP();
                CheckExitSP();
                SP.InsertUpdateFull();
                isAdd = true;
                BindData();
                SPMap = new T_KHOKETOAN_SANPHAM_MAP();
                txtMaSP.Text = String.Empty;
                txtTenSP.Text = String.Empty;
                txtMaHSSP.Text = String.Empty;
                txtDonViTinhSP.Text = String.Empty;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<T_KHOKETOAN_SANPHAM_MAP> ItemColl = new List<T_KHOKETOAN_SANPHAM_MAP>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((T_KHOKETOAN_SANPHAM_MAP)i.GetRow().DataRow);
                        }

                    }
                    foreach (T_KHOKETOAN_SANPHAM_MAP item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        SP.SPCollection.Remove(item);
                    }
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    SPMap = new T_KHOKETOAN_SANPHAM_MAP();
                    SPMap = (T_KHOKETOAN_SANPHAM_MAP)e.Row.DataRow;
                    isAdd = false;
                    SetSPMap();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
