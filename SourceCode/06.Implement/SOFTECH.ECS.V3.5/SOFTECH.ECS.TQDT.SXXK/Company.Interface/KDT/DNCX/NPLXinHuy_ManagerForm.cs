using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface.KDT.SXXK
{
    public partial class NPLXinHuy_ManagerForm : BaseForm
    {
        public List<KDT_CX_NPLXinHuyDangKy> ListTXDK = new List<KDT_CX_NPLXinHuyDangKy>();
        private KDT_CX_NPLXinHuyDangKy TXDK = new KDT_CX_NPLXinHuyDangKy();
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public NPLXinHuy_ManagerForm()
        {
            InitializeComponent();
        }

        private void BKNPLCungUngForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            dgList2.DataSource = ListTXDK;
            dgList2.Refetch();
        }

        

        private void dgList2_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    long id = Convert.ToInt32(dgList2.CurrentRow.Cells["ID"].Value.ToString());
                    TXDK = KDT_CX_NPLXinHuyDangKy.LoadFull(id);
                    NPLXinHuyForm bkTX = new NPLXinHuyForm();
                    bkTX.HSTL = this.HSTL;
                    bkTX.TaiXuatDK = TXDK;
                    bkTX.ShowDialog(this);
                    LoadData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {

                case "cmdThemMoi":
                    ThemMoi();
                    break;
                //case"cmdKhaiBao":
                //    SendV5();
                //    break;
                //case"cmdNhanPhanHoi":
                //    FeedBackV5();
                //    break;
                //case"cmdKetQua":
                //    KetQuaXuLyTCU();
                //    break;


            }
        }

        private void ThemMoi()
        {
            TXDK = new KDT_CX_NPLXinHuyDangKy();
            NPLXinHuyForm TaiXuat = new NPLXinHuyForm();
            TXDK.TrangThaiXuLy = -1;
            TaiXuat.TaiXuatDK = TXDK;
            TaiXuat.HSTL = this.HSTL;
            TaiXuat.ShowDialog(this);
            LoadData();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(dgList2.CurrentRow.Cells["ID"].Value.ToString());

                if (ID > 0)
                {
                    if (ShowMessage("Bạn muốn xóa bảng kê cung ứng?", true) == "Yes")
                    {
                        TXDK.DeleteFull(ID);
                    }
                }
                dgList2.CurrentRow.Delete();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList2_LoadingRow(object sender, RowLoadEventArgs e)
        {

            if (e.Row.RowType == RowType.Record)
            {
                string th = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (th)
                {
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "5":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Sửa bảng kê";
                        break;
                    case "-5":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đang hủy";
                        break;
                    case "11":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ hủy";
                        break;
                    case "10":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
                        break;
                }
            }
        }
    }
}