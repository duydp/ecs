/*
Run this script on:

        ecsteam.ECS_TQDT_SXXK_VERSION    -  This database will be modified

to synchronize it with:

        ecsteam.ECS_TQDT_SXXK

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 10/3/2011 10:22:02 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping [dbo].[NguyenPhuLieuDangKyCopy]'
GO
DROP PROCEDURE [dbo].[NguyenPhuLieuDangKyCopy]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_NguyenPhuLieu_Copy]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Copy]
 @ID BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO t_KDT_SXXK_NguyenPhuLieu_Delete
	
	 SELECT     * FROM t_KDT_SXXK_NguyenPhuLieu	
	 
	WHERE ID=@ID
	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_SanPham_Copy]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Copy]
	-- Add the parameters for the stored procedure here
	@ID BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--select * from t_KDT_SXXK_SanPham_Delete
	SET NOCOUNT ON;
	INSERT INTO t_KDT_SXXK_SanPham_Delete
	SELECT * FROM  t_kdt_sxxk_SanPham
	WHERE  ID=@ID
    -- Insert statements for procedure here

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_SanPhamDangKy_Copy]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPhamDangKy_Copy]
	-- Add the parameters for the stored procedure here
	@ID BIGINT
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


    -- Insert statements for procedure here
   
	INSERT INTO t_KDT_SXXK_SanPham_Delete
	
	SELECT     t_KDT_SXXK_SanPham.*
	FROM         t_KDT_SXXK_SanPham INNER JOIN
						  t_KDT_SXXK_SanPhamDangKy ON t_KDT_SXXK_SanPham.Master_ID = t_KDT_SXXK_SanPhamDangKy.ID						  
	WHERE     (t_KDT_SXXK_SanPhamDangKy.ID = @ID)    
	
	
	INSERT INTO t_KDT_SXXK_SanPhamDangKy_Delete
	
	SELECT * FROM dbo.t_KDT_SXXK_SanPhamDangKy WHERE ID=@ID
	
	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_DinhMucDangKy_Copy]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_Copy]
	-- Add the parameters for the stored procedure here
	@ID BIGINT
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--select * from dbo.t_KDT_SXXK_MsgSend

    -- Insert statements for procedure here
   --delete from t_KDT_SXXK_DinhMuc_Delete
   -- delete from t_KDT_SXXK_DinhMucDangKy_Delete
	INSERT INTO t_KDT_SXXK_DinhMuc_Delete
	
	SELECT     t_KDT_SXXK_DinhMuc.*
	FROM         t_KDT_SXXK_DinhMuc INNER JOIN
						  t_KDT_SXXK_DinhMucDangKy ON t_KDT_SXXK_DinhMuc.Master_ID = t_KDT_SXXK_DinhMucDangKy.ID						  
	WHERE     (t_KDT_SXXK_DinhMucDangKy.ID = @ID)    
	
	
	INSERT INTO t_KDT_SXXK_DinhMucDangKy_Delete
	
	SELECT * FROM dbo.t_KDT_SXXK_DinhMucDangKy WHERE ID=@ID
	
	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_DinhMuc_Copy]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Copy]
	-- Add the parameters for the stored procedure here
	@ID BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--select * from t_KDT_SXXK_SanPham_Delete
	SET NOCOUNT ON;
	INSERT INTO t_KDT_SXXK_DinhMuc_Delete
	SELECT * FROM  t_kdt_sxxk_DinhMuc
	WHERE  ID=@ID
    -- Insert statements for procedure here

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[f_KiemTra_TienTrinh_ThanhLy]'
GO

ALTER FUNCTION [dbo].[f_KiemTra_TienTrinh_ThanhLy]
(
	@MaHaiQuan VARCHAR(10),
	@MaDoanhNghiep VARCHAR(50),
	@SoToKhai INT,
	@MaLoaihinh VARCHAR(10),
	@NamDangKy INT,
	@MaNPL NVARCHAR(255)
)
RETURNS NVARCHAR(500)
AS 
    
/*
@SOFTECH
HUNGTQ, CREATED 04/08/2011.
Kiem tra tien trinh thuc hien chay thanh ly: . 
Gia tri dung phai la so ton cuoi cua lan thanh ly dau = so ton dau cua lan thanh ly tiep theo. 
*/
	
BEGIN

--SET @MaHaiQuan = 'C34C'
--SET @MaDoanhNghiep = '0400101556'
--SET @SoToKhai = 152
--SET @MaLoaihinh = 'NSX01'
--SET @NamDangKy = 2010
--SET @MaNPL = 'NEPLUNG'
			
DECLARE
	@LanThanhLy INT,
	@Luong NUMERIC(18,8),
	@Luong_BanLuu NUMERIC(18,8),
	@Luong_NhapTon NUMERIC(18,8),
	@TonDau NUMERIC(18,8),
	@TonCuoi NUMERIC(18,8),
	@TonCuoi_BanLuu NUMERIC(18,8),
	@TonDauThueXNK NUMERIC(18,8),
	@TonCuoiThueXNK NUMERIC(18,8),
	@cnt1 INT,--Bien dem
	@str NVARCHAR(500),
	@ketqua NVARCHAR(500), --hien thi ket qua
	@SaiSoLuong INT,
	@Valid BIT
	
/*<I> Cap nhat lech luong nhap do bi lam tron so thap phan*/	
DECLARE curThanhLy CURSOR FOR
	SELECT  LanThanhLy, Luong, TonDau, TonCuoi, TonDauThueXNK, TonCuoiThueXNK
	FROM      [dbo].[t_KDT_SXXK_NPLNhapTon]
	WHERE     SoToKhai = @SoToKhai
			AND MaLoaiHinh = @MaLoaiHinh
			AND NamDangKy = @NamDangKy
			AND MaNPL = @MaNPL
			AND TonCuoi < TonDau
			AND LanThanhLy IN ( SELECT  LanThanhLy
								FROM    t_KDT_SXXK_HoSoThanhLyDangKy )
			AND MaHaiQuan = @MaHaiQuan
			AND MaDoanhNghiep = @MaDoanhNghiep

--BEGIN TRANSACTION T1 ;

	SET @Valid = 1
	SET @SaiSoLuong = 0	
    SET @cnt1 = 0;
    
	SET @str = '....................................................................' + char(10)
	SET @str = @str + '		STT' + ' - ' + + 'SoToKhai' + ' - ' + 'MaLoaiHinh' + ' - ' + 'NamDangKy' + ' - ' + 'MaNPL' + char(10)
	SET @str = @str + STR(@cnt1) + ' - ' + STR(@SoToKhai) + ' - ' + @MaLoaiHinh + ' - ' + str(@NamDangKy)+ ' - ' + @MaNPL + char(10)
	--PRINT @str								
	
    OPEN curThanhLy 
    FETCH NEXT FROM curThanhLy INTO @LanThanhLy, @Luong, @TonDau, @TonCuoi, @TonDauThueXNK, @TonCuoiThueXNK
	--Fetch next record
    WHILE @@FETCH_STATUS = 0 
		BEGIN
							
			SET @cnt1 = @cnt1 + 1
						
			--PRINT STR(@cnt1) + '	-> Lan thanh ly : ' + cast(@LanThanhLy AS VARCHAR(50)) + '	Luong' + cast(@Luong AS VARCHAR(50)) + '	-	TonDau: ' + cast(@TonDau AS VARCHAR(50)) + '	-	TonCuoi: ' + cast(@TonCuoi AS VARCHAR(50))
										
			IF (@cnt1 = 1)
				BEGIN
					
					--Kiem tra so luong nhap dang ky ban dau so voi luong ton dau.
					IF (@Luong <> @TonDau)
						BEGIN
							SET @ketqua = N'Sai lượng nhập và lượng tồn đầu tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
							SET @Valid = 0
						END				
					
						  
					IF(@Valid = 1)
						BEGIN
							--Kiem tra so luong nhap dang ky ban dau so voi luong nhap luu trong ton.
							SELECT  @SaiSoLuong =  COUNT(*), @Luong_NhapTon = nt.Luong
								  FROM      [dbo].t_SXXK_ThanhLy_NPLNhapTon nt
											INNER JOIN dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu
																				   AND nt.SoToKhai = h.SoToKhai
																				   AND nt.MaLoaiHinh = h.MaLoaiHinh
																				   AND nt.NamDangKy = h.NamDangKy
																				   AND nt.MaHaiQuan = h.MaHaiQuan
								  WHERE     nt.SoToKhai = @SoToKhai
											AND nt.MaLoaiHinh = @MaLoaiHinh
											AND nt.NamDangKy = @NamDangKy
											AND nt.MaNPL = @MaNPL
											AND nt.MaHaiQuan = @MaHaiQuan
											AND nt.MaDoanhNghiep = @MaDoanhNghiep
											AND nt.Luong <> h.SoLuong
								  GROUP BY nt.Luong
						  
							IF (@SaiSoLuong > 0 AND @Luong_NhapTon <> @Luong)
								BEGIN
									SET @ketqua = N'Sai lượng nhập tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
									SET @Valid = 0
									--PRINT @ketqua
								END
							ELSE
								BEGIN	  
									SET @ketqua = N'Thanh lý đúng'
									SET @Valid = 1
									SET @TonCuoi_BanLuu = @TonCuoi
								END
						END
				END

			--Kiem tra so ton
			IF (@cnt1 > 1 AND @Valid = 1) --kiem tra tu dong thu 2 tro di
				BEGIN

					SET @str = STR(@cnt1) + '	-> Lan thanh ly : ' + cast(@LanThanhLy AS VARCHAR(50)) + '	Luong' + cast(@Luong AS VARCHAR(50)) + '	-	TonDau' + cast(@TonDau AS VARCHAR(50)) + '	-	TonCuoi' + cast(@TonCuoi AS VARCHAR(50))
					
					IF (@TonDau <> @TonCuoi_BanLuu)
						BEGIN
							
							SET @str = @str +  '	=> Sai so lieu ton: ' + cast(@TonDau AS VARCHAR(50)) + ' <> ' + cast(@TonCuoi_BanLuu AS VARCHAR(50))
							--PRINT @str
							
							SET @ketqua = N'Sai tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
						END
					ELSE
						SET @ketqua = N'Thanh lý đúng'
				END						
			
			--Luu lai ton cuoi cua row truoc do
			SET @TonCuoi_BanLuu = @TonCuoi
				
			FETCH NEXT FROM curThanhLy INTO @LanThanhLy, @Luong, @TonDau, @TonCuoi, @TonDauThueXNK, @TonCuoiThueXNK
        END
        
    CLOSE curThanhLy --Close cursor
    DEALLOCATE curThanhLy --Deallocate cursor
	
	--PRINT @ketqua
	
    --IF @@ERROR != 0 
    --    BEGIN
            --PRINT 'Qua trinh thuc hien xay ra loi.'
            --PRINT STR(@@ERROR)
            ----ROLLBACK TRANSACTION T1 ;
            --PRINT 'ROLLBACK TRANSACTION T1'
  --      END
  --  ELSE 
		--BEGIN
			----COMMIT TRANSACTION T1 ;	
			--PRINT ''
			----PRINT 'COMMIT TRANSACTION T1'
		--END
	
	RETURN @ketqua
	
END	

--SELECT ECS_TQDT_SXXK.dbo.[f_KiemTra_TienTrinh_ThanhLy]('C34C', '0400101556', 1782, 'NSX01', 2010, 'ÐINHTAN')
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_SXXK_NguyenPhuLieuDangKy_Copy]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuDangKy_Copy]
	-- Add the parameters for the stored procedure here
	@ID BIGINT
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


    -- Insert statements for procedure here
    
	INSERT INTO t_KDT_SXXK_NguyenPhuLieu_Delete
	
	SELECT     t_KDT_SXXK_NguyenPhuLieu.*
	FROM         t_KDT_SXXK_NguyenPhuLieu INNER JOIN
						  t_KDT_SXXK_NguyenPhuLieuDangKy ON t_KDT_SXXK_NguyenPhuLieu.Master_ID = t_KDT_SXXK_NguyenPhuLieuDangKy.ID
	WHERE     (t_KDT_SXXK_NguyenPhuLieuDangKy.ID = @ID)    
	
	INSERT INTO t_KDT_SXXK_NguyenPhuLieuDangKy_Delete
	
	SELECT * FROM dbo.t_KDT_SXXK_NguyenPhuLieuDangKy WHERE ID=@ID
	
	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
