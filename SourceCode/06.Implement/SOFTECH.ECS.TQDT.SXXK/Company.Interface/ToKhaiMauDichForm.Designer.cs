﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Company.Controls.CustomValidation;
using Janus.Windows.CalendarCombo;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Dock;
using CuaKhauControl = Company.Interface.Controls.CuaKhauControl;
using DonViHaiQuanControl = Company.Interface.Controls.DonViHaiQuanControl;
using LoaiHinhMauDichControl01 = Company.Interface.Controls.LoaiHinhMauDichVControl;
using NguyenTeControl = Company.Interface.Controls.NguyenTeControl;
using NuocControl = Company.Interface.Controls.NuocControl;

namespace Company.Interface
{
    partial class ToKhaiMauDichForm
    {

        private Panel pnlToKhaiMauDich;
        private NumericEditBox txtTrongLuong;
        private NumericEditBox txtSoKien;
        private NumericEditBox txtSoContainer40;
        private NumericEditBox txtSoContainer20;
        private EditBox txtTenChuHang;
        private Label label29;
        private Label label31;
        private Label label32;
        private Label label21;
        private Label label22;
        private UIGroupBox grbNguyenTe;
        private Label label9;
        private Label label30;
        private UIGroupBox grbNuocXK;
        private UIGroupBox uiGroupBox6;
        private EditBox txtTenDaiLy;
        private EditBox txtMaDaiLy;
        private Label label7;
        private Label label8;
        private UIGroupBox uiGroupBox5;
        private EditBox txtMaDonViUyThac;
        private Label label5;
        private Label label6;
        private EditBox txtTenDonViUyThac;
        private UIGroupBox grbNguoiXK;
        private EditBox txtTenDonViDoiTac;
        private UIGroupBox uiGroupBox7;
        private CalendarCombo ccNgayHHHopDong;
        private EditBox txtSoHopDong;
        private Label label13;
        private Label label14;
        private Label label34;
        private CalendarCombo ccNgayHopDong;
        private UIGroupBox uiGroupBox18;
        private UIGroupBox uiGroupBox16;
        private UIGroupBox grbDiaDiemDoHang;
        private EditBox txtDiaDiemXepHang;
        private CalendarCombo ccNgayVanTaiDon;
        private EditBox txtSoVanTaiDon;
        private Label label19;
        private Label label20;
        private UIGroupBox uiGroupBox11;
        private UIComboBox cbPTVT;
        private Label label17;
        private Label lblSoHieuPTVT;
        private EditBox txtSoHieuPTVT;
        private Label lblNgayDenPTVT;
        private EditBox txtSoHoaDonThuongMai;
        private Label label15;
        private Label label16;
        private CalendarCombo ccNgayHHGiayPhep;
        private EditBox txtSoGiayPhep;
        private Label label11;
        private Label label12;
        private Label label33;
        private CalendarCombo ccNgayGiayPhep;
        private UIGroupBox grbHoaDonThuongMai;
        private UIGroupBox grbHopDong;
        private UIGroupBox grbDiaDiemXepHang;
        private UIGroupBox grbVanTaiDon;
        private UIGroupBox gbGiayPhep;
        private NumericEditBox txtTyGiaTinhThue;
        private NumericEditBox txtTyGiaUSD;
        private DataSet ds;
        private DataTable dtHangMauDich;
        private DataColumn dataColumn1;
        private DataColumn dataColumn2;
        private DataColumn dataColumn3;
        private DataColumn dataColumn4;
        private DataColumn dataColumn5;
        private DataColumn dataColumn6;
        private DataColumn dataColumn7;
        private DataColumn dataColumn8;
        private DataColumn dataColumn9;
        private DataTable dtLoaiHinhMauDich;
        private DataColumn dataColumn10;
        private DataColumn dataColumn11;
        private DataTable dtPTTT;
        private DataColumn dataColumn12;
        private DataColumn dataColumn13;
        private DataTable dtCuaKhau;
        private DataColumn dataColumn14;
        private DataColumn dataColumn15;
        private DataColumn dataColumn16;
        private DataTable dtNguyenTe;
        private DataColumn dataColumn17;
        private DataColumn dataColumn18;
        private DataTable dtCompanyNuoc;
        private DataColumn dataColumn19;
        private DataColumn dataColumn20;
        private ErrorProvider epError;
        private ContainerValidator cvError;
        private ListValidationSummary lvsError;
        private RequiredFieldValidator rfvSoHieuPTVT;
        private Label label1;
        private Label label2;
        private NumericEditBox txtLePhiHQ;
        private NumericEditBox txtPhiVanChuyen;
        private NumericEditBox txtPhiBaoHiem;
        private Label label4;
        private UIGroupBox uiGroupBox2;
        private DataTable dtDonViHaiQuan;
        private DataColumn dataColumn21;
        private DataColumn dataColumn22;
        private UIGroupBox uiGroupBox1;
        private UIPanelManager uiPanelManager1;
        private UIPanelGroup uiPanel0;
        private UIPanel uiPanel1;
        private UIPanelInnerContainer uiPanel1Container;
        private Label label23;
        private NumericEditBox txtSoLuongPLTK;
        private UIGroupBox grbNguoiNK;
        private EditBox txtTenDonVi;
        private EditBox txtMaDonVi;
        private CalendarCombo ccNgayDen;
        private CalendarCombo ccNgayHDTM;
        private RequiredFieldValidator rfvNguoiXuatKhau;
        private RangeValidator rvTyGiaTT;
        private RangeValidator rvTyGiaUSD;
        private UICommandManager cmMain;
        private UICommand cmdSave;
        private UIRebar TopRebar1;
        private UICommandBar cmbToolBar;
        private UIRebar BottomRebar1;
        private UIRebar LeftRebar1;
        private UIRebar RightRebar1;
        private IContainer components;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout gridEX1_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiMauDichForm));
            this.pnlToKhaiMauDich = new System.Windows.Forms.Panel();
            this.btnNoiDungDieuChinhTKForm = new Janus.Windows.EditControls.UIButton();
            this.txtLyDoSua = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDeXuatKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtTrongLuongTinh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtPhiNganHang = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtChungTu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grpLoaiHangHoa = new Janus.Windows.EditControls.UIGroupBox();
            this.radTB = new Janus.Windows.EditControls.UIRadioButton();
            this.radNPL = new Janus.Windows.EditControls.UIRadioButton();
            this.radSP = new Janus.Windows.EditControls.UIRadioButton();
            this.grbNguoiNK = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhiBaoHiem = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.grbDiaDiemXepHang = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaDiemXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPhiVanChuyen = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLePhiHQ = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTrongLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoKien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoContainer40 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoContainer20 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTenChuHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.grbNguyenTe = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNguyenTe = new Company.Interface.Controls.NguyenTeControl();
            this.txtTyGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtTyGiaUSD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.grbNuocXK = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNuocXuatKhau = new Company.Interface.Controls.NuocControl();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDaiLy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDaiLy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grbNguoiXK = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDonViDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrLoaiHinhMauDich = new Company.Interface.Controls.LoaiHinhMauDichVControl();
            this.grbHopDong = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayHHHopDong = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.ccNgayHopDong = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.uiGroupBox18 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbPTTT = new Janus.Windows.EditControls.UIComboBox();
            this.uiGroupBox16 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbDKGH = new Janus.Windows.EditControls.UIComboBox();
            this.grbDiaDiemDoHang = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrCuaKhau = new Company.Interface.Controls.CuaKhauControl();
            this.grbVanTaiDon = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayVanTaiDon = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoVanTaiDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayDen = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.cbPTVT = new Janus.Windows.EditControls.UIComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lblSoHieuPTVT = new System.Windows.Forms.Label();
            this.txtSoHieuPTVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblNgayDenPTVT = new System.Windows.Forms.Label();
            this.gbGiayPhep = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayHHGiayPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.ccNgayGiayPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.grbHoaDonThuongMai = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayHDTM = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoHoaDonThuongMai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.ds = new System.Data.DataSet();
            this.dtHangMauDich = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dtLoaiHinhMauDich = new System.Data.DataTable();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dtPTTT = new System.Data.DataTable();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dtCuaKhau = new System.Data.DataTable();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dtNguyenTe = new System.Data.DataTable();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dtCompanyNuoc = new System.Data.DataTable();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dtDonViHaiQuan = new System.Data.DataTable();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvSoHieuPTVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.lblPhanLuong = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.lblTongSoHang = new System.Windows.Forms.Label();
            this.lblTongThue = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lblTongTGNT = new System.Windows.Forms.Label();
            this.lblTongTGKB = new System.Windows.Forms.Label();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblSoTiepNhan = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSoLuongPLTK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label23 = new System.Windows.Forms.Label();
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.uiPanel2 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel2Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.gridEX1 = new Janus.Windows.GridEX.GridEX();
            this.rfvNguoiXuatKhau = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rvTyGiaTT = new Company.Controls.CustomValidation.RangeValidator();
            this.rvTyGiaUSD = new Company.Controls.CustomValidation.RangeValidator();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbToolBar = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.ThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("ThemHang");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdChungTuDinhKem1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDinhKem");
            this.cmdTruyenDuLieuTuXa1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTruyenDuLieuTuXa");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdTinhLaiThue1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTinhLaiThue");
            this.cmdChungTuBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuBoSung");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdThemHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdReadExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdReadExcel");
            this.cmdTinhLaiThue = new Janus.Windows.UI.CommandBars.UICommand("cmdTinhLaiThue");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.InPhieuTN2 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.InToKhai2 = new Janus.Windows.UI.CommandBars.UICommand("InToKhai");
            this.cmdPrintA41 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrintA4");
            this.cmdInToKhaiTQDT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInToKhaiTQDT");
            this.cmdInBangKeKemTheo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInBangKeKemTheo");
            this.cmdInTKDTSuaDoiBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKDTSuaDoiBoSung");
            this.cmdInAnDinhThue1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInAnDinhThue");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.ChungTuKemTheo = new Janus.Windows.UI.CommandBars.UICommand("ChungTuKemTheo");
            this.NhanDuLieu = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieu");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.ThemHang2 = new Janus.Windows.UI.CommandBars.UICommand("ThemHang");
            this.cmdThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdReadExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdReadExcel");
            this.ChungTuKemTheo1 = new Janus.Windows.UI.CommandBars.UICommand("ChungTuKemTheo");
            this.FileDinhKem = new Janus.Windows.UI.CommandBars.UICommand("FileDinhKem");
            this.InPhieuTN = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.ToKhai = new Janus.Windows.UI.CommandBars.UICommand("InToKhai");
            this.cmdPrintA4 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrintA4");
            this.cmdToKhaiTriGia = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTriGia");
            this.cmdToKhaiTriGiaPP11 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTriGiaPP1");
            this.cmdToKhaiTriGiaPP21 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTriGiaPP2");
            this.cmdToKhaiTriGiaPP31 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTriGiaPP3");
            this.cmdChungTuDinhKem = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDinhKem");
            this.cmdVanDon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdVanDon");
            this.cmdHopDong1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHopDong");
            this.cmdGiayPhep1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhep");
            this.cmdHoaDonThuongMai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHoaDonThuongMai");
            this.cmdCO1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCO");
            this.cmdChuyenCuaKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChuyenCuaKhau");
            this.cmdChungTuDangAnh1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDangAnh");
            this.cmdTruyenDuLieuTuXa = new Janus.Windows.UI.CommandBars.UICommand("cmdTruyenDuLieuTuXa");
            this.cmdSend2 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.NhanDuLieu2 = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieu");
            this.XacNhan2 = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.cmdHuyKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyKhaiBao");
            this.Separator3 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdSuaToKhaiDaDuyet2 = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaToKhaiDaDuyet");
            this.cmdHuyToKhaiDaDuyet2 = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyToKhaiDaDuyet");
            this.Separator4 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdKetQuaXuLy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdVanDon = new Janus.Windows.UI.CommandBars.UICommand("cmdVanDon");
            this.cmdHopDong = new Janus.Windows.UI.CommandBars.UICommand("cmdHopDong");
            this.cmdGiayPhep = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhep");
            this.cmdHoaDonThuongMai = new Janus.Windows.UI.CommandBars.UICommand("cmdHoaDonThuongMai");
            this.cmdCO = new Janus.Windows.UI.CommandBars.UICommand("cmdCO");
            this.cmdChuyenCuaKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdChuyenCuaKhau");
            this.cmdToKhaiTriGiaPP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTriGiaPP1");
            this.cmdToKhaiTriGiaPP2 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTriGiaPP2");
            this.cmdToKhaiTriGiaPP3 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTriGiaPP3");
            this.cmdHuyKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyKhaiBao");
            this.cmdHuyToKhaiDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyToKhaiDaDuyet");
            this.cmdSuaToKhaiDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaToKhaiDaDuyet");
            this.cmdChungTuBoSung = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuBoSung");
            this.cmdHopDongBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHopDongBoSung");
            this.cmdVanDonBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdVanDonBoSung");
            this.CO1 = new Janus.Windows.UI.CommandBars.UICommand("CO");
            this.cmdChuyenCuaKhauBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChuyenCuaKhauBoSung");
            this.cmdGiayPhepBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhepBoSung");
            this.cmdHoaDonThuongMaiBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHoaDonThuongMaiBoSung");
            this.cmdChungTuDangAnhBS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDangAnhBS");
            this.cmdHopDongBoSung = new Janus.Windows.UI.CommandBars.UICommand("cmdHopDongBoSung");
            this.cmdVanDonBoSung = new Janus.Windows.UI.CommandBars.UICommand("cmdVanDonBoSung");
            this.CO = new Janus.Windows.UI.CommandBars.UICommand("CO");
            this.cmdChuyenCuaKhauBoSung = new Janus.Windows.UI.CommandBars.UICommand("cmdChuyenCuaKhauBoSung");
            this.cmdGiayPhepBoSung = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhepBoSung");
            this.cmdHoaDonThuongMaiBoSung = new Janus.Windows.UI.CommandBars.UICommand("cmdHoaDonThuongMaiBoSung");
            this.cmdChungTuDangAnh = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDangAnh");
            this.cmdInToKhaiTQDT = new Janus.Windows.UI.CommandBars.UICommand("cmdInToKhaiTQDT");
            this.cmdKetQuaXuLy = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdChungTuDangAnhBS = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDangAnhBS");
            this.cmdInBangKeKemTheo = new Janus.Windows.UI.CommandBars.UICommand("cmdInBangKeKemTheo");
            this.cmdInTKDTSuaDoiBoSung = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKDTSuaDoiBoSung");
            this.cmdInAnDinhThue = new Janus.Windows.UI.CommandBars.UICommand("cmdInAnDinhThue");
            this.cmdFixAnhDinhThue = new Janus.Windows.UI.CommandBars.UICommand("cmdFixAnhDinhThue");
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdReadExcel2 = new Janus.Windows.UI.CommandBars.UICommand("cmdReadExcel");
            this.ChungTuKemTheo2 = new Janus.Windows.UI.CommandBars.UICommand("ChungTuKemTheo");
            this.cvSoContainer40 = new Company.Controls.CustomValidation.CompareValidator();
            this.cvSoKienHang = new Company.Controls.CustomValidation.CompareValidator();
            this.cvTrongLuong = new Company.Controls.CustomValidation.CompareValidator();
            this.cvLePhiHQ = new Company.Controls.CustomValidation.CompareValidator();
            this.cvPhiBaoHiem = new Company.Controls.CustomValidation.CompareValidator();
            this.cvPhiVanChuyen = new Company.Controls.CustomValidation.CompareValidator();
            this.cvPhiKhac = new Company.Controls.CustomValidation.CompareValidator();
            this.cvSoContainer20 = new Company.Controls.CustomValidation.CompareValidator();
            this.cvNgayHHGP = new Company.Controls.CustomValidation.CompareValidator();
            this.cvNgayHHHD = new Company.Controls.CustomValidation.CompareValidator();
            this.cvTrongLuongTinh = new Company.Controls.CustomValidation.CompareValidator();
            this.rfvNgayDen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.InToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("InToKhai");
            this.InPhieuTN1 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.rangeValidator1 = new Company.Controls.CustomValidation.RangeValidator();
            this.cmdThemHang2 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdInToKhaiTT15 = new Janus.Windows.UI.CommandBars.UICommand("cmdInToKhaiTT15");
            this.cmdInToKhaiTT151 = new Janus.Windows.UI.CommandBars.UICommand("cmdInToKhaiTT15");
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.pnlToKhaiMauDich.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpLoaiHangHoa)).BeginInit();
            this.grpLoaiHangHoa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiNK)).BeginInit();
            this.grbNguoiNK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemXepHang)).BeginInit();
            this.grbDiaDiemXepHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguyenTe)).BeginInit();
            this.grbNguyenTe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNuocXK)).BeginInit();
            this.grbNuocXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiXK)).BeginInit();
            this.grbNguoiXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDong)).BeginInit();
            this.grbHopDong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).BeginInit();
            this.uiGroupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).BeginInit();
            this.uiGroupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemDoHang)).BeginInit();
            this.grbDiaDiemDoHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbVanTaiDon)).BeginInit();
            this.grbVanTaiDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbGiayPhep)).BeginInit();
            this.gbGiayPhep.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHoaDonThuongMai)).BeginInit();
            this.grbHoaDonThuongMai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHangMauDich)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiHinhMauDich)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPTTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCuaKhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompanyNuoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieuPTVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).BeginInit();
            this.uiPanel2.SuspendLayout();
            this.uiPanel2Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiXuatKhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaUSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToolBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoContainer40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoKienHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvTrongLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvLePhiHQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvPhiBaoHiem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvPhiVanChuyen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvPhiKhac)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoContainer20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvNgayHHGP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvNgayHHHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvTrongLuongTinh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayDen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(3, 37);
            this.grbMain.Size = new System.Drawing.Size(802, 685);
            // 
            // pnlToKhaiMauDich
            // 
            this.pnlToKhaiMauDich.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlToKhaiMauDich.AutoScroll = true;
            this.pnlToKhaiMauDich.AutoScrollMargin = new System.Drawing.Size(4, 4);
            this.pnlToKhaiMauDich.BackColor = System.Drawing.Color.Transparent;
            this.pnlToKhaiMauDich.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlToKhaiMauDich.Controls.Add(this.btnNoiDungDieuChinhTKForm);
            this.pnlToKhaiMauDich.Controls.Add(this.txtLyDoSua);
            this.pnlToKhaiMauDich.Controls.Add(this.txtDeXuatKhac);
            this.pnlToKhaiMauDich.Controls.Add(this.label28);
            this.pnlToKhaiMauDich.Controls.Add(this.label3);
            this.pnlToKhaiMauDich.Controls.Add(this.label25);
            this.pnlToKhaiMauDich.Controls.Add(this.txtTrongLuongTinh);
            this.pnlToKhaiMauDich.Controls.Add(this.txtPhiNganHang);
            this.pnlToKhaiMauDich.Controls.Add(this.label24);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox4);
            this.pnlToKhaiMauDich.Controls.Add(this.grpLoaiHangHoa);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNguoiNK);
            this.pnlToKhaiMauDich.Controls.Add(this.txtPhiBaoHiem);
            this.pnlToKhaiMauDich.Controls.Add(this.grbDiaDiemXepHang);
            this.pnlToKhaiMauDich.Controls.Add(this.label4);
            this.pnlToKhaiMauDich.Controls.Add(this.txtPhiVanChuyen);
            this.pnlToKhaiMauDich.Controls.Add(this.label2);
            this.pnlToKhaiMauDich.Controls.Add(this.txtLePhiHQ);
            this.pnlToKhaiMauDich.Controls.Add(this.label1);
            this.pnlToKhaiMauDich.Controls.Add(this.txtTrongLuong);
            this.pnlToKhaiMauDich.Controls.Add(this.txtSoKien);
            this.pnlToKhaiMauDich.Controls.Add(this.txtSoContainer40);
            this.pnlToKhaiMauDich.Controls.Add(this.txtSoContainer20);
            this.pnlToKhaiMauDich.Controls.Add(this.txtTenChuHang);
            this.pnlToKhaiMauDich.Controls.Add(this.label29);
            this.pnlToKhaiMauDich.Controls.Add(this.label31);
            this.pnlToKhaiMauDich.Controls.Add(this.label32);
            this.pnlToKhaiMauDich.Controls.Add(this.label21);
            this.pnlToKhaiMauDich.Controls.Add(this.label22);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNguyenTe);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNuocXK);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox6);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox5);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNguoiXK);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox7);
            this.pnlToKhaiMauDich.Controls.Add(this.grbHopDong);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox18);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox16);
            this.pnlToKhaiMauDich.Controls.Add(this.grbDiaDiemDoHang);
            this.pnlToKhaiMauDich.Controls.Add(this.grbVanTaiDon);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox11);
            this.pnlToKhaiMauDich.Controls.Add(this.gbGiayPhep);
            this.pnlToKhaiMauDich.Controls.Add(this.grbHoaDonThuongMai);
            this.pnlToKhaiMauDich.Location = new System.Drawing.Point(0, 83);
            this.pnlToKhaiMauDich.Name = "pnlToKhaiMauDich";
            this.pnlToKhaiMauDich.Size = new System.Drawing.Size(802, 599);
            this.pnlToKhaiMauDich.TabIndex = 8;
            // 
            // btnNoiDungDieuChinhTKForm
            // 
            this.btnNoiDungDieuChinhTKForm.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNoiDungDieuChinhTKForm.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoiDungDieuChinhTKForm.Location = new System.Drawing.Point(627, 569);
            this.btnNoiDungDieuChinhTKForm.Name = "btnNoiDungDieuChinhTKForm";
            this.btnNoiDungDieuChinhTKForm.Size = new System.Drawing.Size(50, 23);
            this.btnNoiDungDieuChinhTKForm.TabIndex = 45;
            this.btnNoiDungDieuChinhTKForm.Text = "...";
            this.btnNoiDungDieuChinhTKForm.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnNoiDungDieuChinhTKForm.VisualStyleManager = this.vsmMain;
            this.btnNoiDungDieuChinhTKForm.Click += new System.EventHandler(this.btnNoiDungDieuChinhTKForm_Click);
            // 
            // txtLyDoSua
            // 
            this.txtLyDoSua.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLyDoSua.Location = new System.Drawing.Point(312, 521);
            this.txtLyDoSua.Multiline = true;
            this.txtLyDoSua.Name = "txtLyDoSua";
            this.txtLyDoSua.Size = new System.Drawing.Size(309, 70);
            this.txtLyDoSua.TabIndex = 43;
            this.txtLyDoSua.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLyDoSua.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtDeXuatKhac
            // 
            this.txtDeXuatKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDeXuatKhac.Location = new System.Drawing.Point(9, 520);
            this.txtDeXuatKhac.Multiline = true;
            this.txtDeXuatKhac.Name = "txtDeXuatKhac";
            this.txtDeXuatKhac.Size = new System.Drawing.Size(291, 70);
            this.txtDeXuatKhac.TabIndex = 44;
            this.txtDeXuatKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDeXuatKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(312, 505);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(61, 13);
            this.label28.TabIndex = 41;
            this.label28.Text = "Lý do sửa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 504);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 42;
            this.label3.Text = "Đề xuất khác";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(630, 415);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(131, 13);
            this.label25.TabIndex = 30;
            this.label25.Text = "Trọng lượng tịnh (NW)";
            // 
            // txtTrongLuongTinh
            // 
            this.txtTrongLuongTinh.DecimalDigits = 2;
            this.txtTrongLuongTinh.Location = new System.Drawing.Point(633, 431);
            this.txtTrongLuongTinh.Name = "txtTrongLuongTinh";
            this.txtTrongLuongTinh.Size = new System.Drawing.Size(95, 21);
            this.txtTrongLuongTinh.TabIndex = 31;
            this.txtTrongLuongTinh.Tag = "TrongLuongTinh";
            this.txtTrongLuongTinh.Text = "0.00";
            this.txtTrongLuongTinh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTrongLuongTinh.Value = 0;
            this.txtTrongLuongTinh.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTrongLuongTinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTrongLuongTinh.VisualStyleManager = this.vsmMain;
            // 
            // txtPhiNganHang
            // 
            this.txtPhiNganHang.DecimalDigits = 2;
            this.txtPhiNganHang.Location = new System.Drawing.Point(525, 480);
            this.txtPhiNganHang.Name = "txtPhiNganHang";
            this.txtPhiNganHang.Size = new System.Drawing.Size(96, 21);
            this.txtPhiNganHang.TabIndex = 39;
            this.txtPhiNganHang.Tag = "PhiKhac";
            this.txtPhiNganHang.Text = "0.00";
            this.txtPhiNganHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtPhiNganHang.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtPhiNganHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtPhiNganHang.VisualStyleManager = this.vsmMain;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(525, 463);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 13);
            this.label24.TabIndex = 38;
            this.label24.Text = "Phí khác";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Controls.Add(this.txtChungTu);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(556, 303);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(172, 104);
            this.uiGroupBox4.TabIndex = 19;
            this.uiGroupBox4.Text = "Chứng từ";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtChungTu
            // 
            this.txtChungTu.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtChungTu.ButtonText = "...";
            this.txtChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChungTu.Location = new System.Drawing.Point(8, 18);
            this.txtChungTu.MaxLength = 40;
            this.txtChungTu.Multiline = true;
            this.txtChungTu.Name = "txtChungTu";
            this.txtChungTu.Size = new System.Drawing.Size(152, 75);
            this.txtChungTu.TabIndex = 1;
            this.txtChungTu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtChungTu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtChungTu.VisualStyleManager = this.vsmMain;
            this.txtChungTu.ButtonClick += new System.EventHandler(this.txtChungTu_ButtonClick);
            // 
            // grpLoaiHangHoa
            // 
            this.grpLoaiHangHoa.BorderColor = System.Drawing.Color.Transparent;
            this.grpLoaiHangHoa.Controls.Add(this.radTB);
            this.grpLoaiHangHoa.Controls.Add(this.radNPL);
            this.grpLoaiHangHoa.Controls.Add(this.radSP);
            this.grpLoaiHangHoa.Location = new System.Drawing.Point(9, 597);
            this.grpLoaiHangHoa.Name = "grpLoaiHangHoa";
            this.grpLoaiHangHoa.Size = new System.Drawing.Size(508, 33);
            this.grpLoaiHangHoa.TabIndex = 40;
            this.grpLoaiHangHoa.VisualStyleManager = this.vsmMain;
            // 
            // radTB
            // 
            this.radTB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTB.Location = new System.Drawing.Point(248, 9);
            this.radTB.Name = "radTB";
            this.radTB.Size = new System.Drawing.Size(175, 23);
            this.radTB.TabIndex = 2;
            this.radTB.Text = "Thiết bị";
            this.radTB.VisualStyleManager = this.vsmMain;
            this.radTB.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
            // 
            // radNPL
            // 
            this.radNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radNPL.Location = new System.Drawing.Point(118, 9);
            this.radNPL.Name = "radNPL";
            this.radNPL.Size = new System.Drawing.Size(124, 23);
            this.radNPL.TabIndex = 1;
            this.radNPL.Text = "Nguyên phụ liệu";
            this.radNPL.VisualStyleManager = this.vsmMain;
            this.radNPL.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
            // 
            // radSP
            // 
            this.radSP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.radSP.Checked = true;
            this.radSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radSP.Location = new System.Drawing.Point(23, 10);
            this.radSP.Name = "radSP";
            this.radSP.Size = new System.Drawing.Size(104, 23);
            this.radSP.TabIndex = 0;
            this.radSP.TabStop = true;
            this.radSP.Text = "Sản phẩm";
            this.radSP.VisualStyleManager = this.vsmMain;
            this.radSP.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
            // 
            // grbNguoiNK
            // 
            this.grbNguoiNK.Controls.Add(this.txtTenDonVi);
            this.grbNguoiNK.Controls.Add(this.txtMaDonVi);
            this.grbNguoiNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguoiNK.Location = new System.Drawing.Point(12, 8);
            this.grbNguoiNK.Name = "grbNguoiNK";
            this.grbNguoiNK.Size = new System.Drawing.Size(184, 104);
            this.grbNguoiNK.TabIndex = 0;
            this.grbNguoiNK.Text = "Người nhập khẩu";
            this.grbNguoiNK.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbNguoiNK.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDonVi
            // 
            this.txtTenDonVi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTenDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonVi.Location = new System.Drawing.Point(8, 48);
            this.txtTenDonVi.Multiline = true;
            this.txtTenDonVi.Name = "txtTenDonVi";
            this.txtTenDonVi.ReadOnly = true;
            this.txtTenDonVi.Size = new System.Drawing.Size(168, 48);
            this.txtTenDonVi.TabIndex = 1;
            this.txtTenDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDonVi.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDonVi
            // 
            this.txtMaDonVi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtMaDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonVi.Location = new System.Drawing.Point(8, 20);
            this.txtMaDonVi.Name = "txtMaDonVi";
            this.txtMaDonVi.ReadOnly = true;
            this.txtMaDonVi.Size = new System.Drawing.Size(168, 21);
            this.txtMaDonVi.TabIndex = 0;
            this.txtMaDonVi.Text = "0400100457";
            this.txtMaDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaDonVi.VisualStyleManager = this.vsmMain;
            // 
            // txtPhiBaoHiem
            // 
            this.txtPhiBaoHiem.DecimalDigits = 2;
            this.txtPhiBaoHiem.Location = new System.Drawing.Point(312, 479);
            this.txtPhiBaoHiem.Name = "txtPhiBaoHiem";
            this.txtPhiBaoHiem.Size = new System.Drawing.Size(96, 21);
            this.txtPhiBaoHiem.TabIndex = 35;
            this.txtPhiBaoHiem.Tag = "LePhiBaoHiem";
            this.txtPhiBaoHiem.Text = "0.00";
            this.txtPhiBaoHiem.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtPhiBaoHiem.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtPhiBaoHiem.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtPhiBaoHiem.VisualStyleManager = this.vsmMain;
            // 
            // grbDiaDiemXepHang
            // 
            this.grbDiaDiemXepHang.Controls.Add(this.txtDiaDiemXepHang);
            this.grbDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDiaDiemXepHang.Location = new System.Drawing.Point(556, 220);
            this.grbDiaDiemXepHang.Name = "grbDiaDiemXepHang";
            this.grbDiaDiemXepHang.Size = new System.Drawing.Size(168, 80);
            this.grbDiaDiemXepHang.TabIndex = 15;
            this.grbDiaDiemXepHang.Tag = "CangXepHang";
            this.grbDiaDiemXepHang.Text = "Địa điểm xếp hàng";
            this.grbDiaDiemXepHang.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbDiaDiemXepHang.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaDiemXepHang
            // 
            this.txtDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiemXepHang.Location = new System.Drawing.Point(8, 24);
            this.txtDiaDiemXepHang.Multiline = true;
            this.txtDiaDiemXepHang.Name = "txtDiaDiemXepHang";
            this.txtDiaDiemXepHang.Size = new System.Drawing.Size(152, 48);
            this.txtDiaDiemXepHang.TabIndex = 0;
            this.txtDiaDiemXepHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaDiemXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDiaDiemXepHang.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(312, 463);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "Phí bảo hiểm";
            // 
            // txtPhiVanChuyen
            // 
            this.txtPhiVanChuyen.DecimalDigits = 2;
            this.txtPhiVanChuyen.Location = new System.Drawing.Point(420, 479);
            this.txtPhiVanChuyen.Name = "txtPhiVanChuyen";
            this.txtPhiVanChuyen.Size = new System.Drawing.Size(92, 21);
            this.txtPhiVanChuyen.TabIndex = 37;
            this.txtPhiVanChuyen.Tag = "PhiVanChuyen";
            this.txtPhiVanChuyen.Text = "0.00";
            this.txtPhiVanChuyen.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtPhiVanChuyen.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtPhiVanChuyen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtPhiVanChuyen.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(420, 463);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 36;
            this.label2.Text = "Phí vận chuyển";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtLePhiHQ
            // 
            this.txtLePhiHQ.DecimalDigits = 2;
            this.txtLePhiHQ.Location = new System.Drawing.Point(205, 479);
            this.txtLePhiHQ.Name = "txtLePhiHQ";
            this.txtLePhiHQ.Size = new System.Drawing.Size(96, 21);
            this.txtLePhiHQ.TabIndex = 33;
            this.txtLePhiHQ.Tag = "LePhiHQ";
            this.txtLePhiHQ.Text = "0.00";
            this.txtLePhiHQ.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtLePhiHQ.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtLePhiHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtLePhiHQ.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(205, 463);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Lệ phí HQ";
            // 
            // txtTrongLuong
            // 
            this.txtTrongLuong.DecimalDigits = 2;
            this.txtTrongLuong.Location = new System.Drawing.Point(525, 431);
            this.txtTrongLuong.Name = "txtTrongLuong";
            this.txtTrongLuong.Size = new System.Drawing.Size(96, 21);
            this.txtTrongLuong.TabIndex = 29;
            this.txtTrongLuong.Tag = "TrongLuong";
            this.txtTrongLuong.Text = "0.00";
            this.txtTrongLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTrongLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtTrongLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTrongLuong.VisualStyleManager = this.vsmMain;
            // 
            // txtSoKien
            // 
            this.txtSoKien.DecimalDigits = 0;
            this.txtSoKien.Location = new System.Drawing.Point(420, 431);
            this.txtSoKien.Name = "txtSoKien";
            this.txtSoKien.Size = new System.Drawing.Size(92, 21);
            this.txtSoKien.TabIndex = 27;
            this.txtSoKien.Tag = "SoKienHang";
            this.txtSoKien.Text = "0";
            this.txtSoKien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoKien.Value = 0;
            this.txtSoKien.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtSoKien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoKien.VisualStyleManager = this.vsmMain;
            // 
            // txtSoContainer40
            // 
            this.txtSoContainer40.DecimalDigits = 2;
            this.txtSoContainer40.Location = new System.Drawing.Point(312, 431);
            this.txtSoContainer40.Name = "txtSoContainer40";
            this.txtSoContainer40.Size = new System.Drawing.Size(96, 21);
            this.txtSoContainer40.TabIndex = 25;
            this.txtSoContainer40.Tag = "SoContainer40";
            this.txtSoContainer40.Text = "0.00";
            this.txtSoContainer40.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoContainer40.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtSoContainer40.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoContainer40.VisualStyleManager = this.vsmMain;
            // 
            // txtSoContainer20
            // 
            this.txtSoContainer20.DecimalDigits = 2;
            this.txtSoContainer20.Location = new System.Drawing.Point(205, 431);
            this.txtSoContainer20.Name = "txtSoContainer20";
            this.txtSoContainer20.Size = new System.Drawing.Size(96, 21);
            this.txtSoContainer20.TabIndex = 23;
            this.txtSoContainer20.Tag = "SoContainer20";
            this.txtSoContainer20.Text = "0.00";
            this.txtSoContainer20.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoContainer20.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtSoContainer20.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoContainer20.VisualStyleManager = this.vsmMain;
            // 
            // txtTenChuHang
            // 
            this.txtTenChuHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenChuHang.Location = new System.Drawing.Point(9, 431);
            this.txtTenChuHang.Multiline = true;
            this.txtTenChuHang.Name = "txtTenChuHang";
            this.txtTenChuHang.Size = new System.Drawing.Size(184, 70);
            this.txtTenChuHang.TabIndex = 21;
            this.txtTenChuHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenChuHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenChuHang.VisualStyleManager = this.vsmMain;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(9, 415);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(82, 13);
            this.label29.TabIndex = 20;
            this.label29.Text = "Tên chủ hàng";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(205, 415);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(95, 13);
            this.label31.TabIndex = 22;
            this.label31.Text = "Số container 20";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(312, 415);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(95, 13);
            this.label32.TabIndex = 24;
            this.label32.Text = "Số container 40";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(420, 415);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 13);
            this.label21.TabIndex = 26;
            this.label21.Text = "Số kiện hàng";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(522, 415);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(107, 13);
            this.label22.TabIndex = 28;
            this.label22.Text = "Trọng lượng (GW)";
            // 
            // grbNguyenTe
            // 
            this.grbNguyenTe.Controls.Add(this.ctrNguyenTe);
            this.grbNguyenTe.Controls.Add(this.txtTyGiaTinhThue);
            this.grbNguyenTe.Controls.Add(this.label9);
            this.grbNguyenTe.Controls.Add(this.label30);
            this.grbNguyenTe.Controls.Add(this.txtTyGiaUSD);
            this.grbNguyenTe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguyenTe.Location = new System.Drawing.Point(380, 303);
            this.grbNguyenTe.Name = "grbNguyenTe";
            this.grbNguyenTe.Size = new System.Drawing.Size(168, 104);
            this.grbNguyenTe.TabIndex = 18;
            this.grbNguyenTe.Text = "Đồng tiền thanh toán";
            this.grbNguyenTe.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbNguyenTe.VisualStyleManager = this.vsmMain;
            // 
            // ctrNguyenTe
            // 
            this.ctrNguyenTe.BackColor = System.Drawing.Color.Transparent;
            this.ctrNguyenTe.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.ctrNguyenTe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNguyenTe.Location = new System.Drawing.Point(8, 20);
            this.ctrNguyenTe.Ma = "";
            this.ctrNguyenTe.Name = "ctrNguyenTe";
            this.ctrNguyenTe.ReadOnly = false;
            this.ctrNguyenTe.Size = new System.Drawing.Size(166, 22);
            this.ctrNguyenTe.TabIndex = 0;
            this.ctrNguyenTe.Tag = "DongTienThanhToan";
            this.ctrNguyenTe.VisualStyleManager = this.vsmMain;
            this.ctrNguyenTe.ValueChanged += new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(this.ctrNguyenTe_ValueChanged);
            // 
            // txtTyGiaTinhThue
            // 
            this.txtTyGiaTinhThue.DecimalDigits = 3;
            this.txtTyGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGiaTinhThue.Location = new System.Drawing.Point(80, 47);
            this.txtTyGiaTinhThue.Name = "txtTyGiaTinhThue";
            this.txtTyGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyGiaTinhThue.Size = new System.Drawing.Size(81, 21);
            this.txtTyGiaTinhThue.TabIndex = 2;
            this.txtTyGiaTinhThue.Tag = "TyGiaTinhThue";
            this.txtTyGiaTinhThue.Text = "0.000";
            this.txtTyGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTyGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTyGiaTinhThue.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(4, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Tỷ giá USD";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(4, 55);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(51, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "Tỷ giá TT";
            // 
            // txtTyGiaUSD
            // 
            this.txtTyGiaUSD.DecimalDigits = 0;
            this.txtTyGiaUSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGiaUSD.Location = new System.Drawing.Point(80, 74);
            this.txtTyGiaUSD.Name = "txtTyGiaUSD";
            this.txtTyGiaUSD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyGiaUSD.Size = new System.Drawing.Size(81, 21);
            this.txtTyGiaUSD.TabIndex = 4;
            this.txtTyGiaUSD.Text = "0";
            this.txtTyGiaUSD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyGiaUSD.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTyGiaUSD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTyGiaUSD.VisualStyleManager = this.vsmMain;
            // 
            // grbNuocXK
            // 
            this.grbNuocXK.Controls.Add(this.ctrNuocXuatKhau);
            this.grbNuocXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNuocXK.Location = new System.Drawing.Point(204, 220);
            this.grbNuocXK.Name = "grbNuocXK";
            this.grbNuocXK.Size = new System.Drawing.Size(168, 80);
            this.grbNuocXK.TabIndex = 13;
            this.grbNuocXK.Text = "Nước xuất khẩu";
            this.grbNuocXK.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbNuocXK.VisualStyleManager = this.vsmMain;
            // 
            // ctrNuocXuatKhau
            // 
            this.ctrNuocXuatKhau.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocXuatKhau.ErrorMessage = "\"Nước xuất khẩu\" bắt buộc phải chọn.";
            this.ctrNuocXuatKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocXuatKhau.Location = new System.Drawing.Point(7, 24);
            this.ctrNuocXuatKhau.Ma = "";
            this.ctrNuocXuatKhau.Name = "ctrNuocXuatKhau";
            this.ctrNuocXuatKhau.ReadOnly = false;
            this.ctrNuocXuatKhau.Size = new System.Drawing.Size(169, 50);
            this.ctrNuocXuatKhau.TabIndex = 0;
            this.ctrNuocXuatKhau.Tag = "NuocXK";
            this.ctrNuocXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Controls.Add(this.txtTenDaiLy);
            this.uiGroupBox6.Controls.Add(this.txtMaDaiLy);
            this.uiGroupBox6.Controls.Add(this.label7);
            this.uiGroupBox6.Controls.Add(this.label8);
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(12, 303);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(184, 104);
            this.uiGroupBox6.TabIndex = 16;
            this.uiGroupBox6.Text = "Đại lý làm TTHQ";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDaiLy
            // 
            this.txtTenDaiLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDaiLy.Location = new System.Drawing.Point(40, 47);
            this.txtTenDaiLy.Multiline = true;
            this.txtTenDaiLy.Name = "txtTenDaiLy";
            this.txtTenDaiLy.Size = new System.Drawing.Size(136, 48);
            this.txtTenDaiLy.TabIndex = 3;
            this.txtTenDaiLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDaiLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDaiLy.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDaiLy
            // 
            this.txtMaDaiLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDaiLy.Location = new System.Drawing.Point(40, 20);
            this.txtMaDaiLy.Name = "txtMaDaiLy";
            this.txtMaDaiLy.Size = new System.Drawing.Size(136, 21);
            this.txtMaDaiLy.TabIndex = 1;
            this.txtMaDaiLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDaiLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaDaiLy.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Mã";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Tên";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.txtMaDonViUyThac);
            this.uiGroupBox5.Controls.Add(this.label5);
            this.uiGroupBox5.Controls.Add(this.label6);
            this.uiGroupBox5.Controls.Add(this.txtTenDonViUyThac);
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(12, 220);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(184, 80);
            this.uiGroupBox5.TabIndex = 12;
            this.uiGroupBox5.Text = "Người ủy thác";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDonViUyThac
            // 
            this.txtMaDonViUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonViUyThac.Location = new System.Drawing.Point(40, 20);
            this.txtMaDonViUyThac.Name = "txtMaDonViUyThac";
            this.txtMaDonViUyThac.Size = new System.Drawing.Size(136, 21);
            this.txtMaDonViUyThac.TabIndex = 1;
            this.txtMaDonViUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonViUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaDonViUyThac.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Mã";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 2;
            this.label6.Tag = "TenDVUT";
            this.label6.Text = "Tên";
            // 
            // txtTenDonViUyThac
            // 
            this.txtTenDonViUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonViUyThac.Location = new System.Drawing.Point(40, 47);
            this.txtTenDonViUyThac.Name = "txtTenDonViUyThac";
            this.txtTenDonViUyThac.Size = new System.Drawing.Size(136, 21);
            this.txtTenDonViUyThac.TabIndex = 3;
            this.txtTenDonViUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonViUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDonViUyThac.VisualStyleManager = this.vsmMain;
            // 
            // grbNguoiXK
            // 
            this.grbNguoiXK.Controls.Add(this.txtTenDonViDoiTac);
            this.grbNguoiXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguoiXK.Location = new System.Drawing.Point(12, 114);
            this.grbNguoiXK.Name = "grbNguoiXK";
            this.grbNguoiXK.Size = new System.Drawing.Size(184, 104);
            this.grbNguoiXK.TabIndex = 6;
            this.grbNguoiXK.Tag = "NguoiXuatKhau";
            this.grbNguoiXK.Text = "Người xuất khẩu";
            this.grbNguoiXK.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbNguoiXK.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDonViDoiTac
            // 
            this.txtTenDonViDoiTac.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTenDonViDoiTac.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTenDonViDoiTac.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtTenDonViDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonViDoiTac.Location = new System.Drawing.Point(8, 20);
            this.txtTenDonViDoiTac.Multiline = true;
            this.txtTenDonViDoiTac.Name = "txtTenDonViDoiTac";
            this.txtTenDonViDoiTac.Size = new System.Drawing.Size(168, 76);
            this.txtTenDonViDoiTac.TabIndex = 0;
            this.txtTenDonViDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonViDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDonViDoiTac.VisualStyleManager = this.vsmMain;
            this.txtTenDonViDoiTac.ButtonClick += new System.EventHandler(this.txtTenDonViDoiTac_ButtonClick);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.Controls.Add(this.ctrLoaiHinhMauDich);
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.Location = new System.Drawing.Point(204, 8);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(168, 104);
            this.uiGroupBox7.TabIndex = 1;
            this.uiGroupBox7.Text = "Loại hình mậu dịch";
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // ctrLoaiHinhMauDich
            // 
            this.ctrLoaiHinhMauDich.BackColor = System.Drawing.Color.Transparent;
            this.ctrLoaiHinhMauDich.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrLoaiHinhMauDich.Location = new System.Drawing.Point(7, 24);
            this.ctrLoaiHinhMauDich.Ma = "";
            this.ctrLoaiHinhMauDich.Name = "ctrLoaiHinhMauDich";
            this.ctrLoaiHinhMauDich.Nhom = "";
            this.ctrLoaiHinhMauDich.ReadOnly = false;
            this.ctrLoaiHinhMauDich.Size = new System.Drawing.Size(169, 50);
            this.ctrLoaiHinhMauDich.TabIndex = 0;
            this.ctrLoaiHinhMauDich.VisualStyleManager = this.vsmMain;
            // 
            // grbHopDong
            // 
            this.grbHopDong.Controls.Add(this.ccNgayHHHopDong);
            this.grbHopDong.Controls.Add(this.txtSoHopDong);
            this.grbHopDong.Controls.Add(this.label13);
            this.grbHopDong.Controls.Add(this.label14);
            this.grbHopDong.Controls.Add(this.label34);
            this.grbHopDong.Controls.Add(this.ccNgayHopDong);
            this.grbHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbHopDong.Location = new System.Drawing.Point(556, 8);
            this.grbHopDong.Name = "grbHopDong";
            this.grbHopDong.Size = new System.Drawing.Size(168, 104);
            this.grbHopDong.TabIndex = 4;
            this.grbHopDong.Text = "Hợp đồng";
            this.grbHopDong.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbHopDong.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayHHHopDong
            // 
            // 
            // 
            // 
            this.ccNgayHHHopDong.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHHHopDong.DropDownCalendar.Name = "";
            this.ccNgayHHHopDong.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHHHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHHHopDong.IsNullDate = true;
            this.ccNgayHHHopDong.Location = new System.Drawing.Point(64, 77);
            this.ccNgayHHHopDong.Name = "ccNgayHHHopDong";
            this.ccNgayHHHopDong.Nullable = true;
            this.ccNgayHHHopDong.NullButtonText = "Xóa";
            this.ccNgayHHHopDong.ReadOnly = true;
            this.ccNgayHHHopDong.ShowNullButton = true;
            this.ccNgayHHHopDong.Size = new System.Drawing.Size(96, 21);
            this.ccNgayHHHopDong.TabIndex = 5;
            this.ccNgayHHHopDong.TodayButtonText = "Hôm nay";
            this.ccNgayHHHopDong.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHHHopDong.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDong.Location = new System.Drawing.Point(64, 20);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(96, 21);
            this.txtSoHopDong.TabIndex = 1;
            this.txtSoHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoHopDong.VisualStyleManager = this.vsmMain;
            this.txtSoHopDong.ButtonClick += new System.EventHandler(this.txtSoHopDong_ButtonClick);
            this.txtSoHopDong.Leave += new System.EventHandler(this.txtSoHopDong_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(4, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 0;
            this.label13.Tag = "SoHD";
            this.label13.Text = "Số";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 54);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 13);
            this.label14.TabIndex = 2;
            this.label14.Tag = "NgayHD";
            this.label14.Text = "Ngày";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(3, 79);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(49, 13);
            this.label34.TabIndex = 4;
            this.label34.Tag = "NgayHetHanHD";
            this.label34.Text = "Ngày HH";
            // 
            // ccNgayHopDong
            // 
            // 
            // 
            // 
            this.ccNgayHopDong.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHopDong.DropDownCalendar.Name = "";
            this.ccNgayHopDong.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHopDong.IsNullDate = true;
            this.ccNgayHopDong.Location = new System.Drawing.Point(64, 48);
            this.ccNgayHopDong.Name = "ccNgayHopDong";
            this.ccNgayHopDong.Nullable = true;
            this.ccNgayHopDong.NullButtonText = "Xóa";
            this.ccNgayHopDong.ReadOnly = true;
            this.ccNgayHopDong.ShowNullButton = true;
            this.ccNgayHopDong.Size = new System.Drawing.Size(96, 21);
            this.ccNgayHopDong.TabIndex = 3;
            this.ccNgayHopDong.TodayButtonText = "Hôm nay";
            this.ccNgayHopDong.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHopDong.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox18
            // 
            this.uiGroupBox18.Controls.Add(this.cbPTTT);
            this.uiGroupBox18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox18.Location = new System.Drawing.Point(204, 358);
            this.uiGroupBox18.Name = "uiGroupBox18";
            this.uiGroupBox18.Size = new System.Drawing.Size(168, 49);
            this.uiGroupBox18.TabIndex = 19;
            this.uiGroupBox18.Text = "Phương thức thanh toán";
            this.uiGroupBox18.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox18.VisualStyleManager = this.vsmMain;
            // 
            // cbPTTT
            // 
            this.cbPTTT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTTT.DisplayMember = "Name";
            this.cbPTTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTTT.Location = new System.Drawing.Point(7, 19);
            this.cbPTTT.Name = "cbPTTT";
            this.cbPTTT.Size = new System.Drawing.Size(152, 21);
            this.cbPTTT.TabIndex = 0;
            this.cbPTTT.Tag = "PhuongThucThanhToan";
            this.cbPTTT.ValueMember = "ID";
            this.cbPTTT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbPTTT.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox16
            // 
            this.uiGroupBox16.Controls.Add(this.cbDKGH);
            this.uiGroupBox16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox16.Location = new System.Drawing.Point(204, 303);
            this.uiGroupBox16.Name = "uiGroupBox16";
            this.uiGroupBox16.Size = new System.Drawing.Size(168, 48);
            this.uiGroupBox16.TabIndex = 17;
            this.uiGroupBox16.Text = "Điều kiện giao hàng";
            this.uiGroupBox16.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox16.VisualStyleManager = this.vsmMain;
            // 
            // cbDKGH
            // 
            this.cbDKGH.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDKGH.DisplayMember = "ID";
            this.cbDKGH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDKGH.Location = new System.Drawing.Point(7, 19);
            this.cbDKGH.Name = "cbDKGH";
            this.cbDKGH.Size = new System.Drawing.Size(152, 21);
            this.cbDKGH.TabIndex = 0;
            this.cbDKGH.Tag = "DieuKienGiaoHang";
            this.cbDKGH.ValueMember = "ID";
            this.cbDKGH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbDKGH.VisualStyleManager = this.vsmMain;
            // 
            // grbDiaDiemDoHang
            // 
            this.grbDiaDiemDoHang.Controls.Add(this.ctrCuaKhau);
            this.grbDiaDiemDoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDiaDiemDoHang.Location = new System.Drawing.Point(380, 220);
            this.grbDiaDiemDoHang.Name = "grbDiaDiemDoHang";
            this.grbDiaDiemDoHang.Size = new System.Drawing.Size(168, 80);
            this.grbDiaDiemDoHang.TabIndex = 14;
            this.grbDiaDiemDoHang.Tag = "CangDoHang";
            this.grbDiaDiemDoHang.Text = "Địa điểm dỡ hàng";
            this.grbDiaDiemDoHang.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbDiaDiemDoHang.VisualStyleManager = this.vsmMain;
            // 
            // ctrCuaKhau
            // 
            this.ctrCuaKhau.BackColor = System.Drawing.Color.Transparent;
            this.ctrCuaKhau.ErrorMessage = "\"Địa điểm dỡ hàng\" bắt buộc phải chọn.";
            this.ctrCuaKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrCuaKhau.Location = new System.Drawing.Point(8, 24);
            this.ctrCuaKhau.Ma = "";
            this.ctrCuaKhau.Name = "ctrCuaKhau";
            this.ctrCuaKhau.ReadOnly = false;
            this.ctrCuaKhau.Size = new System.Drawing.Size(168, 50);
            this.ctrCuaKhau.TabIndex = 0;
            this.ctrCuaKhau.VisualStyleManager = this.vsmMain;
            // 
            // grbVanTaiDon
            // 
            this.grbVanTaiDon.Controls.Add(this.ccNgayVanTaiDon);
            this.grbVanTaiDon.Controls.Add(this.txtSoVanTaiDon);
            this.grbVanTaiDon.Controls.Add(this.label19);
            this.grbVanTaiDon.Controls.Add(this.label20);
            this.grbVanTaiDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbVanTaiDon.Location = new System.Drawing.Point(556, 114);
            this.grbVanTaiDon.Name = "grbVanTaiDon";
            this.grbVanTaiDon.Size = new System.Drawing.Size(168, 104);
            this.grbVanTaiDon.TabIndex = 11;
            this.grbVanTaiDon.Text = "Vận tải đơn";
            this.grbVanTaiDon.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbVanTaiDon.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayVanTaiDon
            // 
            // 
            // 
            // 
            this.ccNgayVanTaiDon.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayVanTaiDon.DropDownCalendar.Name = "";
            this.ccNgayVanTaiDon.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayVanTaiDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayVanTaiDon.IsNullDate = true;
            this.ccNgayVanTaiDon.Location = new System.Drawing.Point(64, 47);
            this.ccNgayVanTaiDon.Name = "ccNgayVanTaiDon";
            this.ccNgayVanTaiDon.Nullable = true;
            this.ccNgayVanTaiDon.NullButtonText = "Xóa";
            this.ccNgayVanTaiDon.ReadOnly = true;
            this.ccNgayVanTaiDon.ShowNullButton = true;
            this.ccNgayVanTaiDon.Size = new System.Drawing.Size(96, 21);
            this.ccNgayVanTaiDon.TabIndex = 3;
            this.ccNgayVanTaiDon.TodayButtonText = "Hôm nay";
            this.ccNgayVanTaiDon.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayVanTaiDon.VisualStyleManager = this.vsmMain;
            // 
            // txtSoVanTaiDon
            // 
            this.txtSoVanTaiDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanTaiDon.Location = new System.Drawing.Point(64, 20);
            this.txtSoVanTaiDon.Name = "txtSoVanTaiDon";
            this.txtSoVanTaiDon.Size = new System.Drawing.Size(96, 21);
            this.txtSoVanTaiDon.TabIndex = 1;
            this.txtSoVanTaiDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanTaiDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoVanTaiDon.VisualStyleManager = this.vsmMain;
            this.txtSoVanTaiDon.ButtonClick += new System.EventHandler(this.txtSoVanTaiDon_ButtonClick);
            this.txtSoVanTaiDon.Leave += new System.EventHandler(this.txtSoVanTaiDon_Leave);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(8, 26);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 13);
            this.label19.TabIndex = 0;
            this.label19.Tag = "SoVanDon";
            this.label19.Text = "Số";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(8, 53);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 13);
            this.label20.TabIndex = 2;
            this.label20.Tag = "NgayVanDon";
            this.label20.Text = "Ngày";
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.Controls.Add(this.ccNgayDen);
            this.uiGroupBox11.Controls.Add(this.cbPTVT);
            this.uiGroupBox11.Controls.Add(this.label17);
            this.uiGroupBox11.Controls.Add(this.lblSoHieuPTVT);
            this.uiGroupBox11.Controls.Add(this.txtSoHieuPTVT);
            this.uiGroupBox11.Controls.Add(this.lblNgayDenPTVT);
            this.uiGroupBox11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox11.Location = new System.Drawing.Point(204, 114);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(168, 104);
            this.uiGroupBox11.TabIndex = 7;
            this.uiGroupBox11.Text = "Phương tiện vận tải";
            this.uiGroupBox11.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox11.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayDen
            // 
            // 
            // 
            // 
            this.ccNgayDen.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayDen.DropDownCalendar.Name = "";
            this.ccNgayDen.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayDen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayDen.IsNullDate = true;
            this.ccNgayDen.Location = new System.Drawing.Point(65, 75);
            this.ccNgayDen.Name = "ccNgayDen";
            this.ccNgayDen.NullButtonText = "Xóa";
            this.ccNgayDen.Size = new System.Drawing.Size(96, 21);
            this.ccNgayDen.TabIndex = 5;
            this.ccNgayDen.TodayButtonText = "Hôm nay";
            this.ccNgayDen.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayDen.VisualStyleManager = this.vsmMain;
            // 
            // cbPTVT
            // 
            this.cbPTVT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTVT.DisplayMember = "Ten";
            this.cbPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTVT.Location = new System.Drawing.Point(65, 20);
            this.cbPTVT.Name = "cbPTVT";
            this.cbPTVT.Size = new System.Drawing.Size(96, 21);
            this.cbPTVT.TabIndex = 1;
            this.cbPTVT.ValueMember = "ID";
            this.cbPTVT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbPTVT.VisualStyleManager = this.vsmMain;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(8, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 13);
            this.label17.TabIndex = 0;
            this.label17.Tag = "LoaiPTVT";
            this.label17.Text = "PTVT";
            // 
            // lblSoHieuPTVT
            // 
            this.lblSoHieuPTVT.AutoSize = true;
            this.lblSoHieuPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHieuPTVT.Location = new System.Drawing.Point(8, 56);
            this.lblSoHieuPTVT.Name = "lblSoHieuPTVT";
            this.lblSoHieuPTVT.Size = new System.Drawing.Size(42, 13);
            this.lblSoHieuPTVT.TabIndex = 2;
            this.lblSoHieuPTVT.Tag = "SoHieuPTVT";
            this.lblSoHieuPTVT.Text = "Số hiệu";
            // 
            // txtSoHieuPTVT
            // 
            this.txtSoHieuPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHieuPTVT.Location = new System.Drawing.Point(65, 47);
            this.txtSoHieuPTVT.Name = "txtSoHieuPTVT";
            this.txtSoHieuPTVT.Size = new System.Drawing.Size(96, 21);
            this.txtSoHieuPTVT.TabIndex = 3;
            this.txtSoHieuPTVT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHieuPTVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoHieuPTVT.VisualStyleManager = this.vsmMain;
            // 
            // lblNgayDenPTVT
            // 
            this.lblNgayDenPTVT.AutoSize = true;
            this.lblNgayDenPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDenPTVT.Location = new System.Drawing.Point(8, 83);
            this.lblNgayDenPTVT.Name = "lblNgayDenPTVT";
            this.lblNgayDenPTVT.Size = new System.Drawing.Size(53, 13);
            this.lblNgayDenPTVT.TabIndex = 4;
            this.lblNgayDenPTVT.Tag = "NgayDenPTVT";
            this.lblNgayDenPTVT.Text = "Ngày đến";
            // 
            // gbGiayPhep
            // 
            this.gbGiayPhep.Controls.Add(this.ccNgayHHGiayPhep);
            this.gbGiayPhep.Controls.Add(this.txtSoGiayPhep);
            this.gbGiayPhep.Controls.Add(this.label11);
            this.gbGiayPhep.Controls.Add(this.label12);
            this.gbGiayPhep.Controls.Add(this.label33);
            this.gbGiayPhep.Controls.Add(this.ccNgayGiayPhep);
            this.gbGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbGiayPhep.Location = new System.Drawing.Point(380, 8);
            this.gbGiayPhep.Name = "gbGiayPhep";
            this.gbGiayPhep.Size = new System.Drawing.Size(168, 104);
            this.gbGiayPhep.TabIndex = 2;
            this.gbGiayPhep.Text = "Giấy phép";
            this.gbGiayPhep.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.gbGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayHHGiayPhep
            // 
            // 
            // 
            // 
            this.ccNgayHHGiayPhep.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHHGiayPhep.DropDownCalendar.Name = "";
            this.ccNgayHHGiayPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHHGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHHGiayPhep.IsNullDate = true;
            this.ccNgayHHGiayPhep.Location = new System.Drawing.Point(64, 77);
            this.ccNgayHHGiayPhep.Name = "ccNgayHHGiayPhep";
            this.ccNgayHHGiayPhep.Nullable = true;
            this.ccNgayHHGiayPhep.NullButtonText = "Xóa";
            this.ccNgayHHGiayPhep.ReadOnly = true;
            this.ccNgayHHGiayPhep.ShowNullButton = true;
            this.ccNgayHHGiayPhep.Size = new System.Drawing.Size(96, 21);
            this.ccNgayHHGiayPhep.TabIndex = 5;
            this.ccNgayHHGiayPhep.TodayButtonText = "Hôm nay";
            this.ccNgayHHGiayPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHHGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // txtSoGiayPhep
            // 
            this.txtSoGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoGiayPhep.Location = new System.Drawing.Point(64, 20);
            this.txtSoGiayPhep.Name = "txtSoGiayPhep";
            this.txtSoGiayPhep.Size = new System.Drawing.Size(96, 21);
            this.txtSoGiayPhep.TabIndex = 1;
            this.txtSoGiayPhep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoGiayPhep.VisualStyleManager = this.vsmMain;
            this.txtSoGiayPhep.ButtonClick += new System.EventHandler(this.txtSoGiayPhep_ButtonClick);
            this.txtSoGiayPhep.Leave += new System.EventHandler(this.txtSoGiayPhep_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(4, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 0;
            this.label11.Tag = "SoGiayPhep";
            this.label11.Text = "Số";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(2, 54);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 2;
            this.label12.Tag = "NgayGiayPhep";
            this.label12.Text = "Ngày";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(4, 79);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(49, 13);
            this.label33.TabIndex = 4;
            this.label33.Tag = "NgayHHGiay Phep";
            this.label33.Text = "Ngày HH";
            // 
            // ccNgayGiayPhep
            // 
            // 
            // 
            // 
            this.ccNgayGiayPhep.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayGiayPhep.DropDownCalendar.Name = "";
            this.ccNgayGiayPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayGiayPhep.IsNullDate = true;
            this.ccNgayGiayPhep.Location = new System.Drawing.Point(64, 48);
            this.ccNgayGiayPhep.Name = "ccNgayGiayPhep";
            this.ccNgayGiayPhep.Nullable = true;
            this.ccNgayGiayPhep.NullButtonText = "Xóa";
            this.ccNgayGiayPhep.ReadOnly = true;
            this.ccNgayGiayPhep.ShowNullButton = true;
            this.ccNgayGiayPhep.Size = new System.Drawing.Size(96, 21);
            this.ccNgayGiayPhep.TabIndex = 3;
            this.ccNgayGiayPhep.TodayButtonText = "Hôm nay";
            this.ccNgayGiayPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // grbHoaDonThuongMai
            // 
            this.grbHoaDonThuongMai.Controls.Add(this.ccNgayHDTM);
            this.grbHoaDonThuongMai.Controls.Add(this.txtSoHoaDonThuongMai);
            this.grbHoaDonThuongMai.Controls.Add(this.label15);
            this.grbHoaDonThuongMai.Controls.Add(this.label16);
            this.grbHoaDonThuongMai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbHoaDonThuongMai.Location = new System.Drawing.Point(380, 114);
            this.grbHoaDonThuongMai.Name = "grbHoaDonThuongMai";
            this.grbHoaDonThuongMai.Size = new System.Drawing.Size(168, 104);
            this.grbHoaDonThuongMai.TabIndex = 8;
            this.grbHoaDonThuongMai.Text = "Hóa đơn thương mại";
            this.grbHoaDonThuongMai.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbHoaDonThuongMai.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayHDTM
            // 
            // 
            // 
            // 
            this.ccNgayHDTM.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHDTM.DropDownCalendar.Name = "";
            this.ccNgayHDTM.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHDTM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHDTM.IsNullDate = true;
            this.ccNgayHDTM.Location = new System.Drawing.Point(63, 48);
            this.ccNgayHDTM.Name = "ccNgayHDTM";
            this.ccNgayHDTM.Nullable = true;
            this.ccNgayHDTM.NullButtonText = "Xóa";
            this.ccNgayHDTM.ReadOnly = true;
            this.ccNgayHDTM.ShowNullButton = true;
            this.ccNgayHDTM.Size = new System.Drawing.Size(96, 21);
            this.ccNgayHDTM.TabIndex = 3;
            this.ccNgayHDTM.TodayButtonText = "Hôm nay";
            this.ccNgayHDTM.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHDTM.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHoaDonThuongMai
            // 
            this.txtSoHoaDonThuongMai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHoaDonThuongMai.Location = new System.Drawing.Point(63, 20);
            this.txtSoHoaDonThuongMai.Name = "txtSoHoaDonThuongMai";
            this.txtSoHoaDonThuongMai.Size = new System.Drawing.Size(96, 21);
            this.txtSoHoaDonThuongMai.TabIndex = 1;
            this.txtSoHoaDonThuongMai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHoaDonThuongMai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoHoaDonThuongMai.VisualStyleManager = this.vsmMain;
            this.txtSoHoaDonThuongMai.ButtonClick += new System.EventHandler(this.txtSoHoaDonThuongMai_ButtonClick);
            this.txtSoHoaDonThuongMai.Leave += new System.EventHandler(this.txtSoHoaDonThuongMai_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(8, 26);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 13);
            this.label15.TabIndex = 0;
            this.label15.Tag = "SoHDTM";
            this.label15.Text = "Số";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(8, 54);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 13);
            this.label16.TabIndex = 2;
            this.label16.Tag = "NgayHDTM";
            this.label16.Text = "Ngày";
            // 
            // ds
            // 
            this.ds.DataSetName = "NewDataSet";
            this.ds.Locale = new System.Globalization.CultureInfo("en-US");
            this.ds.Tables.AddRange(new System.Data.DataTable[] {
            this.dtHangMauDich,
            this.dtLoaiHinhMauDich,
            this.dtPTTT,
            this.dtCuaKhau,
            this.dtNguyenTe,
            this.dtCompanyNuoc,
            this.dtDonViHaiQuan});
            // 
            // dtHangMauDich
            // 
            this.dtHangMauDich.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9});
            this.dtHangMauDich.TableName = "HangMauDich";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "hmd_SoThuTuHang";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "hmd_MaHS";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "hmd_MaPhu";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "hmd_TenHang";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "nuoc_Ten";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "donvitinh_Ten";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "hmd_Luong";
            this.dataColumn7.DataType = typeof(decimal);
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "hmd_DonGiaKB";
            this.dataColumn8.DataType = typeof(decimal);
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "hmd_TriGiaKB";
            this.dataColumn9.DataType = typeof(decimal);
            // 
            // dtLoaiHinhMauDich
            // 
            this.dtLoaiHinhMauDich.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn10,
            this.dataColumn11});
            this.dtLoaiHinhMauDich.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "loaihinh_Ma"}, true)});
            this.dtLoaiHinhMauDich.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn10};
            this.dtLoaiHinhMauDich.TableName = "LoaiHinhMauDich";
            // 
            // dataColumn10
            // 
            this.dataColumn10.AllowDBNull = false;
            this.dataColumn10.ColumnName = "loaihinh_Ma";
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "loaihinh_Ten";
            // 
            // dtPTTT
            // 
            this.dtPTTT.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn12,
            this.dataColumn13});
            this.dtPTTT.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "pttt_Ma"}, true)});
            this.dtPTTT.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn12};
            this.dtPTTT.TableName = "PhuongThucThanhToan";
            // 
            // dataColumn12
            // 
            this.dataColumn12.AllowDBNull = false;
            this.dataColumn12.ColumnName = "pttt_Ma";
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnName = "pttt_GhiChu";
            // 
            // dtCuaKhau
            // 
            this.dtCuaKhau.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn16});
            this.dtCuaKhau.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "cuakhau_Ma"}, true)});
            this.dtCuaKhau.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn14};
            this.dtCuaKhau.TableName = "CuaKhau";
            // 
            // dataColumn14
            // 
            this.dataColumn14.AllowDBNull = false;
            this.dataColumn14.ColumnName = "cuakhau_Ma";
            // 
            // dataColumn15
            // 
            this.dataColumn15.ColumnName = "cuakhau_Ten";
            // 
            // dataColumn16
            // 
            this.dataColumn16.ColumnName = "cuc_ma";
            // 
            // dtNguyenTe
            // 
            this.dtNguyenTe.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn17,
            this.dataColumn18});
            this.dtNguyenTe.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nguyente_Ma"}, true)});
            this.dtNguyenTe.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn17};
            this.dtNguyenTe.TableName = "NguyenTe";
            // 
            // dataColumn17
            // 
            this.dataColumn17.AllowDBNull = false;
            this.dataColumn17.ColumnName = "nguyente_Ma";
            // 
            // dataColumn18
            // 
            this.dataColumn18.ColumnName = "nguyente_Ten";
            // 
            // dtCompanyNuoc
            // 
            this.dtCompanyNuoc.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn19,
            this.dataColumn20});
            this.dtCompanyNuoc.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nuoc_Ma"}, true)});
            this.dtCompanyNuoc.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn19};
            this.dtCompanyNuoc.TableName = "CompanyNuoc";
            // 
            // dataColumn19
            // 
            this.dataColumn19.AllowDBNull = false;
            this.dataColumn19.ColumnName = "nuoc_Ma";
            // 
            // dataColumn20
            // 
            this.dataColumn20.ColumnName = "nuoc_Ten";
            // 
            // dtDonViHaiQuan
            // 
            this.dtDonViHaiQuan.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn21,
            this.dataColumn22});
            this.dtDonViHaiQuan.TableName = "DonViHaiQuan";
            // 
            // dataColumn21
            // 
            this.dataColumn21.ColumnName = "donvihaiquan_Ma";
            // 
            // dataColumn22
            // 
            this.dataColumn22.ColumnName = "donvihaiquan_Ten";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvSoHieuPTVT
            // 
            this.rfvSoHieuPTVT.ControlToValidate = this.txtSoHieuPTVT;
            this.rfvSoHieuPTVT.ErrorMessage = "\"Số hiệu phương tiện vận tải\" không được để trống.";
            this.rfvSoHieuPTVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoHieuPTVT.Icon")));
            this.rfvSoHieuPTVT.Tag = "rfvSoHieuPTVT";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(754, 139);
            this.uiGroupBox2.TabIndex = 258;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(232)))), ((int)(((byte)(226)))));
            this.uiGroupBox1.Controls.Add(this.lblPhanLuong);
            this.uiGroupBox1.Controls.Add(this.label26);
            this.uiGroupBox1.Controls.Add(this.lblTongSoHang);
            this.uiGroupBox1.Controls.Add(this.lblTongThue);
            this.uiGroupBox1.Controls.Add(this.label35);
            this.uiGroupBox1.Controls.Add(this.label27);
            this.uiGroupBox1.Controls.Add(this.lblTongTGNT);
            this.uiGroupBox1.Controls.Add(this.lblTongTGKB);
            this.uiGroupBox1.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.lblSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox1.Controls.Add(this.lblTrangThai);
            this.uiGroupBox1.Controls.Add(this.label18);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.pnlToKhaiMauDich);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongPLTK);
            this.uiGroupBox1.Controls.Add(this.label23);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 37);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(802, 685);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // lblPhanLuong
            // 
            this.lblPhanLuong.AutoSize = true;
            this.lblPhanLuong.BackColor = System.Drawing.Color.Transparent;
            this.lblPhanLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLuong.ForeColor = System.Drawing.Color.Red;
            this.lblPhanLuong.Location = new System.Drawing.Point(603, 35);
            this.lblPhanLuong.Name = "lblPhanLuong";
            this.lblPhanLuong.Size = new System.Drawing.Size(101, 13);
            this.lblPhanLuong.TabIndex = 13;
            this.lblPhanLuong.Text = "Chưa phân luồng";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(495, 33);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(75, 13);
            this.label26.TabIndex = 12;
            this.label26.Text = "Phân luồng :";
            // 
            // lblTongSoHang
            // 
            this.lblTongSoHang.AutoSize = true;
            this.lblTongSoHang.BackColor = System.Drawing.Color.Transparent;
            this.lblTongSoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongSoHang.Location = new System.Drawing.Point(614, 61);
            this.lblTongSoHang.Name = "lblTongSoHang";
            this.lblTongSoHang.Size = new System.Drawing.Size(88, 13);
            this.lblTongSoHang.TabIndex = 11;
            this.lblTongSoHang.Text = "[TongSoHang]";
            // 
            // lblTongThue
            // 
            this.lblTongThue.AutoSize = true;
            this.lblTongThue.BackColor = System.Drawing.Color.Transparent;
            this.lblTongThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongThue.Location = new System.Drawing.Point(302, 61);
            this.lblTongThue.Name = "lblTongThue";
            this.lblTongThue.Size = new System.Drawing.Size(73, 13);
            this.lblTongThue.TabIndex = 11;
            this.lblTongThue.Text = "[TongThue]";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(495, 61);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(120, 13);
            this.label35.TabIndex = 11;
            this.label35.Text = "Tổng số lượng hàng:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(166, 61);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(67, 13);
            this.label27.TabIndex = 11;
            this.label27.Text = "Tổng thuế:";
            // 
            // lblTongTGNT
            // 
            this.lblTongTGNT.AutoSize = true;
            this.lblTongTGNT.BackColor = System.Drawing.Color.Transparent;
            this.lblTongTGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongTGNT.Location = new System.Drawing.Point(302, 35);
            this.lblTongTGNT.Name = "lblTongTGNT";
            this.lblTongTGNT.Size = new System.Drawing.Size(74, 13);
            this.lblTongTGNT.TabIndex = 11;
            this.lblTongTGNT.Text = "[TongTGNT]";
            // 
            // lblTongTGKB
            // 
            this.lblTongTGKB.AutoSize = true;
            this.lblTongTGKB.BackColor = System.Drawing.Color.Transparent;
            this.lblTongTGKB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongTGKB.Location = new System.Drawing.Point(167, 35);
            this.lblTongTGKB.Name = "lblTongTGKB";
            this.lblTongTGKB.Size = new System.Drawing.Size(134, 13);
            this.lblTongTGKB.TabIndex = 11;
            this.lblTongTGKB.Text = "Tổng trị giá nguyên tệ:";
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(88, 3);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(67, 21);
            this.txtSoTiepNhan.TabIndex = 10;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            this.txtSoTiepNhan.TextChanged += new System.EventHandler(this.editBox1_TextChanged);
            // 
            // lblSoTiepNhan
            // 
            this.lblSoTiepNhan.AutoSize = true;
            this.lblSoTiepNhan.BackColor = System.Drawing.Color.Transparent;
            this.lblSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoTiepNhan.Location = new System.Drawing.Point(3, 10);
            this.lblSoTiepNhan.Name = "lblSoTiepNhan";
            this.lblSoTiepNhan.Size = new System.Drawing.Size(77, 13);
            this.lblSoTiepNhan.TabIndex = 9;
            this.lblSoTiepNhan.Text = "Số tiếp nhận";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.Location = new System.Drawing.Point(88, 30);
            this.txtSoToKhai.MaxLength = 10;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.ReadOnly = true;
            this.txtSoToKhai.Size = new System.Drawing.Size(67, 21);
            this.txtSoToKhai.TabIndex = 3;
            this.txtSoToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(603, 8);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(133, 13);
            this.lblTrangThai.TabIndex = 7;
            this.lblTrangThai.Text = "Chưa gửi đến Hải quan";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(495, 8);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(102, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Trạng thái xử lý :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 33);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Số tờ khai";
            // 
            // txtSoLuongPLTK
            // 
            this.txtSoLuongPLTK.Location = new System.Drawing.Point(305, 3);
            this.txtSoLuongPLTK.MaxLength = 3;
            this.txtSoLuongPLTK.Name = "txtSoLuongPLTK";
            this.txtSoLuongPLTK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongPLTK.Size = new System.Drawing.Size(56, 21);
            this.txtSoLuongPLTK.TabIndex = 5;
            this.txtSoLuongPLTK.Text = "0";
            this.txtSoLuongPLTK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongPLTK.Value = ((byte)(0));
            this.txtSoLuongPLTK.ValueType = Janus.Windows.GridEX.NumericEditValueType.Byte;
            this.txtSoLuongPLTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoLuongPLTK.VisualStyleManager = this.vsmMain;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(166, 8);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(86, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Số lượng PLTK";
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(241)))), ((int)(((byte)(228)))));
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.uiPanelManager1.DefaultPanelSettings.CloseButtonVisible = false;
            this.uiPanelManager1.Tag = null;
            this.uiPanel0.Id = new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2");
            this.uiPanel0.StaticGroup = true;
            this.uiPanel1.Id = new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8");
            this.uiPanel0.Panels.Add(this.uiPanel1);
            this.uiPanelManager1.Panels.Add(this.uiPanel0);
            this.uiPanel2.Id = new System.Guid("146f6615-6820-40ed-9958-87376ff81f1b");
            this.uiPanelManager1.Panels.Add(this.uiPanel2);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), Janus.Windows.UI.Dock.PanelGroupStyle.HorizontalTiles, Janus.Windows.UI.Dock.PanelDockStyle.Bottom, true, new System.Drawing.Size(786, 210), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8"), new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), 174, true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("146f6615-6820-40ed-9958-87376ff81f1b"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(754, 200), true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), Janus.Windows.UI.Dock.PanelGroupStyle.HorizontalTiles, true, new System.Drawing.Point(22, 29), new System.Drawing.Size(56, 56), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("146f6615-6820-40ed-9958-87376ff81f1b"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.AutoHide = true;
            this.uiPanel0.CaptionFormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.uiPanel0.CloseButtonVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel0.FloatingLocation = new System.Drawing.Point(22, 29);
            this.uiPanel0.FloatingSize = new System.Drawing.Size(56, 56);
            this.uiPanel0.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiPanel0.Location = new System.Drawing.Point(3, 197);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.Size = new System.Drawing.Size(786, 210);
            this.uiPanel0.TabIndex = 1;
            this.uiPanel0.Text = "Thông tin hàng hóa";
            // 
            // uiPanel1
            // 
            this.uiPanel1.CaptionVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(0, 26);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(786, 184);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Panel 1";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.dgList);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 1);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(784, 182);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(784, 182);
            this.dgList.TabIndex = 182;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // uiPanel2
            // 
            this.uiPanel2.AutoHide = true;
            this.uiPanel2.InnerContainer = this.uiPanel2Container;
            this.uiPanel2.Location = new System.Drawing.Point(3, 60);
            this.uiPanel2.Name = "uiPanel2";
            this.uiPanel2.Size = new System.Drawing.Size(754, 200);
            this.uiPanel2.TabIndex = 4;
            this.uiPanel2.Text = "Thông tin chứng từ";
            // 
            // uiPanel2Container
            // 
            this.uiPanel2Container.Controls.Add(this.gridEX1);
            this.uiPanel2Container.Location = new System.Drawing.Point(1, 27);
            this.uiPanel2Container.Name = "uiPanel2Container";
            this.uiPanel2Container.Size = new System.Drawing.Size(752, 172);
            this.uiPanel2Container.TabIndex = 0;
            // 
            // gridEX1
            // 
            this.gridEX1.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridEX1.AlternatingColors = true;
            this.gridEX1.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.gridEX1.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.gridEX1.ColumnAutoResize = true;
            gridEX1_DesignTimeLayout.LayoutString = resources.GetString("gridEX1_DesignTimeLayout.LayoutString");
            this.gridEX1.DesignTimeLayout = gridEX1_DesignTimeLayout;
            this.gridEX1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridEX1.GroupByBoxVisible = false;
            this.gridEX1.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX1.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX1.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEX1.Location = new System.Drawing.Point(0, 0);
            this.gridEX1.Name = "gridEX1";
            this.gridEX1.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridEX1.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX1.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.gridEX1.Size = new System.Drawing.Size(752, 172);
            this.gridEX1.TabIndex = 2;
            this.gridEX1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.gridEX1.VisualStyleManager = this.vsmMain;
            this.gridEX1.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.gridEX1_DeletingRecord);
            this.gridEX1.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.gridEX1_RowDoubleClick);
            // 
            // rfvNguoiXuatKhau
            // 
            this.rfvNguoiXuatKhau.ControlToValidate = this.txtTenDonViDoiTac;
            this.rfvNguoiXuatKhau.ErrorMessage = "\"Người xuất khẩu\" không được để trống.";
            this.rfvNguoiXuatKhau.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNguoiXuatKhau.Icon")));
            this.rfvNguoiXuatKhau.Tag = "rfvNguoiXuatKhau";
            // 
            // rvTyGiaTT
            // 
            this.rvTyGiaTT.ControlToValidate = this.txtTyGiaTinhThue;
            this.rvTyGiaTT.ErrorMessage = "\"Tỷ giá tính thuế\" không hợp lệ.";
            this.rvTyGiaTT.Icon = ((System.Drawing.Icon)(resources.GetObject("rvTyGiaTT.Icon")));
            this.rvTyGiaTT.MaximumValue = "999999";
            this.rvTyGiaTT.MinimumValue = "1";
            this.rvTyGiaTT.Tag = "rvTyGiaTT";
            this.rvTyGiaTT.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rvTyGiaUSD
            // 
            this.rvTyGiaUSD.ControlToValidate = this.txtTyGiaUSD;
            this.rvTyGiaUSD.ErrorMessage = "\"Tỷ giá USD\" không hợp lệ.";
            this.rvTyGiaUSD.Icon = ((System.Drawing.Icon)(resources.GetObject("rvTyGiaUSD.Icon")));
            this.rvTyGiaUSD.MaximumValue = "999999";
            this.rvTyGiaUSD.MinimumValue = "1";
            this.rvTyGiaUSD.Tag = "rvTyGiaUSD";
            this.rvTyGiaUSD.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbToolBar});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave,
            this.cmdThemHang,
            this.cmdReadExcel,
            this.cmdTinhLaiThue,
            this.cmdPrint,
            this.cmdSend,
            this.ChungTuKemTheo,
            this.NhanDuLieu,
            this.XacNhan,
            this.ThemHang2,
            this.FileDinhKem,
            this.InPhieuTN,
            this.ToKhai,
            this.cmdPrintA4,
            this.cmdToKhaiTriGia,
            this.cmdChungTuDinhKem,
            this.cmdTruyenDuLieuTuXa,
            this.cmdVanDon,
            this.cmdHopDong,
            this.cmdGiayPhep,
            this.cmdHoaDonThuongMai,
            this.cmdCO,
            this.cmdChuyenCuaKhau,
            this.cmdToKhaiTriGiaPP1,
            this.cmdToKhaiTriGiaPP2,
            this.cmdToKhaiTriGiaPP3,
            this.cmdHuyKhaiBao,
            this.cmdHuyToKhaiDaDuyet,
            this.cmdSuaToKhaiDaDuyet,
            this.cmdChungTuBoSung,
            this.cmdHopDongBoSung,
            this.cmdVanDonBoSung,
            this.CO,
            this.cmdChuyenCuaKhauBoSung,
            this.cmdGiayPhepBoSung,
            this.cmdHoaDonThuongMaiBoSung,
            this.cmdChungTuDangAnh,
            this.cmdInToKhaiTQDT,
            this.cmdKetQuaXuLy,
            this.cmdChungTuDangAnhBS,
            this.cmdInBangKeKemTheo,
            this.cmdInTKDTSuaDoiBoSung,
            this.cmdInAnDinhThue,
            this.cmdFixAnhDinhThue,
            this.cmdInToKhaiTT15});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.ImageList = this.ImageList1;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmbToolBar
            // 
            this.cmbToolBar.CommandManager = this.cmMain;
            this.cmbToolBar.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ThemHang1,
            this.cmdSave1,
            this.Separator1,
            this.cmdChungTuDinhKem1,
            this.cmdTruyenDuLieuTuXa1,
            this.Separator2,
            this.cmdPrint1,
            this.cmdTinhLaiThue1,
            this.cmdChungTuBoSung1});
            this.cmbToolBar.FullRow = true;
            this.cmbToolBar.Key = "cmdToolBar";
            this.cmbToolBar.Location = new System.Drawing.Point(0, 0);
            this.cmbToolBar.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.cmbToolBar.MergeRowOrder = 1;
            this.cmbToolBar.Name = "cmbToolBar";
            this.cmbToolBar.RowIndex = 0;
            this.cmbToolBar.Size = new System.Drawing.Size(808, 34);
            this.cmbToolBar.Text = "cmdToolBar";
            this.cmbToolBar.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmbToolBar.Wrappable = Janus.Windows.UI.InheritableBoolean.True;
            // 
            // ThemHang1
            // 
            this.ThemHang1.Key = "ThemHang";
            this.ThemHang1.Name = "ThemHang1";
            this.ThemHang1.Text = "Thêm hàng";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            this.cmdSave1.Text = "Lưu";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdChungTuDinhKem1
            // 
            this.cmdChungTuDinhKem1.ImageIndex = 4;
            this.cmdChungTuDinhKem1.Key = "cmdChungTuDinhKem";
            this.cmdChungTuDinhKem1.Name = "cmdChungTuDinhKem1";
            // 
            // cmdTruyenDuLieuTuXa1
            // 
            this.cmdTruyenDuLieuTuXa1.ImageIndex = 5;
            this.cmdTruyenDuLieuTuXa1.Key = "cmdTruyenDuLieuTuXa";
            this.cmdTruyenDuLieuTuXa1.Name = "cmdTruyenDuLieuTuXa1";
            this.cmdTruyenDuLieuTuXa1.Text = "Thông quan điện tử";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            this.cmdPrint1.Text = "In";
            // 
            // cmdTinhLaiThue1
            // 
            this.cmdTinhLaiThue1.Key = "cmdTinhLaiThue";
            this.cmdTinhLaiThue1.Name = "cmdTinhLaiThue1";
            this.cmdTinhLaiThue1.Text = "Tính lại thuế";
            // 
            // cmdChungTuBoSung1
            // 
            this.cmdChungTuBoSung1.ImageIndex = 4;
            this.cmdChungTuBoSung1.Key = "cmdChungTuBoSung";
            this.cmdChungTuBoSung1.Name = "cmdChungTuBoSung1";
            // 
            // cmdSave
            // 
            this.cmdSave.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSave.Icon")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.MergeOrder = 3;
            this.cmdSave.MergeType = Janus.Windows.UI.CommandBars.CommandMergeType.MergeItems;
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSave.Text = "Lưu thông tin";
            // 
            // cmdThemHang
            // 
            this.cmdThemHang.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThemHang.Icon")));
            this.cmdThemHang.Key = "cmdThemHang";
            this.cmdThemHang.Name = "cmdThemHang";
            this.cmdThemHang.Text = "Thêm hàng";
            // 
            // cmdReadExcel
            // 
            this.cmdReadExcel.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdReadExcel.Icon")));
            this.cmdReadExcel.Key = "cmdReadExcel";
            this.cmdReadExcel.Name = "cmdReadExcel";
            this.cmdReadExcel.Text = "Đọc Excel";
            // 
            // cmdTinhLaiThue
            // 
            this.cmdTinhLaiThue.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdTinhLaiThue.Icon")));
            this.cmdTinhLaiThue.Key = "cmdTinhLaiThue";
            this.cmdTinhLaiThue.Name = "cmdTinhLaiThue";
            this.cmdTinhLaiThue.Text = "Tính lại thuế";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.InPhieuTN2,
            this.InToKhai2,
            this.cmdPrintA41,
            this.cmdInToKhaiTQDT1,
            this.cmdInBangKeKemTheo1,
            this.cmdInTKDTSuaDoiBoSung1,
            this.cmdInAnDinhThue1,
            this.cmdInToKhaiTT151});
            this.cmdPrint.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdPrint.Icon")));
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.cmdPrint.Text = "In tờ khai";
            // 
            // InPhieuTN2
            // 
            this.InPhieuTN2.ImageIndex = 9;
            this.InPhieuTN2.Key = "InPhieuTN";
            this.InPhieuTN2.Name = "InPhieuTN2";
            this.InPhieuTN2.Text = "Phiếu tiếp nhận";
            // 
            // InToKhai2
            // 
            this.InToKhai2.Key = "InToKhai";
            this.InToKhai2.Name = "InToKhai2";
            this.InToKhai2.Text = "Tờ khai";
            this.InToKhai2.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdPrintA41
            // 
            this.cmdPrintA41.Key = "cmdPrintA4";
            this.cmdPrintA41.Name = "cmdPrintA41";
            this.cmdPrintA41.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdInToKhaiTQDT1
            // 
            this.cmdInToKhaiTQDT1.ImageIndex = 9;
            this.cmdInToKhaiTQDT1.Key = "cmdInToKhaiTQDT";
            this.cmdInToKhaiTQDT1.Name = "cmdInToKhaiTQDT1";
            this.cmdInToKhaiTQDT1.Text = "Tờ khai thông quan điện tử";
            // 
            // cmdInBangKeKemTheo1
            // 
            this.cmdInBangKeKemTheo1.ImageIndex = 9;
            this.cmdInBangKeKemTheo1.Key = "cmdInBangKeKemTheo";
            this.cmdInBangKeKemTheo1.Name = "cmdInBangKeKemTheo1";
            // 
            // cmdInTKDTSuaDoiBoSung1
            // 
            this.cmdInTKDTSuaDoiBoSung1.ImageIndex = 9;
            this.cmdInTKDTSuaDoiBoSung1.Key = "cmdInTKDTSuaDoiBoSung";
            this.cmdInTKDTSuaDoiBoSung1.Name = "cmdInTKDTSuaDoiBoSung1";
            // 
            // cmdInAnDinhThue1
            // 
            this.cmdInAnDinhThue1.ImageIndex = 9;
            this.cmdInAnDinhThue1.Key = "cmdInAnDinhThue";
            this.cmdInAnDinhThue1.Name = "cmdInAnDinhThue1";
            // 
            // cmdSend
            // 
            this.cmdSend.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSend.Icon")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Shortcut = System.Windows.Forms.Shortcut.CtrlK;
            this.cmdSend.Text = "Khai báo";
            this.cmdSend.ToolTipText = "Khai báo (Ctrl + K)";
            // 
            // ChungTuKemTheo
            // 
            this.ChungTuKemTheo.Icon = ((System.Drawing.Icon)(resources.GetObject("ChungTuKemTheo.Icon")));
            this.ChungTuKemTheo.Key = "ChungTuKemTheo";
            this.ChungTuKemTheo.Name = "ChungTuKemTheo";
            this.ChungTuKemTheo.Text = "Thêm chứng từ";
            // 
            // NhanDuLieu
            // 
            this.NhanDuLieu.Icon = ((System.Drawing.Icon)(resources.GetObject("NhanDuLieu.Icon")));
            this.NhanDuLieu.Key = "NhanDuLieu";
            this.NhanDuLieu.Name = "NhanDuLieu";
            this.NhanDuLieu.Text = "Nhận dữ liệu";
            // 
            // XacNhan
            // 
            this.XacNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan.Icon")));
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận thông tin";
            // 
            // ThemHang2
            // 
            this.ThemHang2.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang1,
            this.cmdReadExcel1,
            this.ChungTuKemTheo1});
            this.ThemHang2.Icon = ((System.Drawing.Icon)(resources.GetObject("ThemHang2.Icon")));
            this.ThemHang2.Key = "ThemHang";
            this.ThemHang2.Name = "ThemHang2";
            this.ThemHang2.Text = "Thêm hàng";
            // 
            // cmdThemHang1
            // 
            this.cmdThemHang1.Key = "cmdThemHang";
            this.cmdThemHang1.Name = "cmdThemHang1";
            this.cmdThemHang1.Text = "&Thêm hàng";
            // 
            // cmdReadExcel1
            // 
            this.cmdReadExcel1.Key = "cmdReadExcel";
            this.cmdReadExcel1.Name = "cmdReadExcel1";
            this.cmdReadExcel1.Text = "Đọc &Excel";
            // 
            // ChungTuKemTheo1
            // 
            this.ChungTuKemTheo1.Key = "ChungTuKemTheo";
            this.ChungTuKemTheo1.Name = "ChungTuKemTheo1";
            this.ChungTuKemTheo1.Text = "Thêm &chứng từ";
            // 
            // FileDinhKem
            // 
            this.FileDinhKem.Icon = ((System.Drawing.Icon)(resources.GetObject("FileDinhKem.Icon")));
            this.FileDinhKem.Key = "FileDinhKem";
            this.FileDinhKem.Name = "FileDinhKem";
            this.FileDinhKem.Text = "File đính kèm";
            // 
            // InPhieuTN
            // 
            this.InPhieuTN.Key = "InPhieuTN";
            this.InPhieuTN.Name = "InPhieuTN";
            this.InPhieuTN.Text = "Phiếu tiếp nhận";
            // 
            // ToKhai
            // 
            this.ToKhai.Key = "InToKhai";
            this.ToKhai.Name = "ToKhai";
            this.ToKhai.Text = "Tờ khai";
            // 
            // cmdPrintA4
            // 
            this.cmdPrintA4.Key = "cmdPrintA4";
            this.cmdPrintA4.Name = "cmdPrintA4";
            this.cmdPrintA4.Text = "Tờ khai A4";
            // 
            // cmdToKhaiTriGia
            // 
            this.cmdToKhaiTriGia.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdToKhaiTriGiaPP11,
            this.cmdToKhaiTriGiaPP21,
            this.cmdToKhaiTriGiaPP31});
            this.cmdToKhaiTriGia.Key = "cmdToKhaiTriGia";
            this.cmdToKhaiTriGia.Name = "cmdToKhaiTriGia";
            this.cmdToKhaiTriGia.Text = "Tờ khai trị giá";
            // 
            // cmdToKhaiTriGiaPP11
            // 
            this.cmdToKhaiTriGiaPP11.Key = "cmdToKhaiTriGiaPP1";
            this.cmdToKhaiTriGiaPP11.Name = "cmdToKhaiTriGiaPP11";
            // 
            // cmdToKhaiTriGiaPP21
            // 
            this.cmdToKhaiTriGiaPP21.Key = "cmdToKhaiTriGiaPP2";
            this.cmdToKhaiTriGiaPP21.Name = "cmdToKhaiTriGiaPP21";
            // 
            // cmdToKhaiTriGiaPP31
            // 
            this.cmdToKhaiTriGiaPP31.Key = "cmdToKhaiTriGiaPP3";
            this.cmdToKhaiTriGiaPP31.Name = "cmdToKhaiTriGiaPP31";
            // 
            // cmdChungTuDinhKem
            // 
            this.cmdChungTuDinhKem.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdVanDon1,
            this.cmdHopDong1,
            this.cmdGiayPhep1,
            this.cmdHoaDonThuongMai1,
            this.cmdCO1,
            this.cmdChuyenCuaKhau1,
            this.cmdChungTuDangAnh1});
            this.cmdChungTuDinhKem.Key = "cmdChungTuDinhKem";
            this.cmdChungTuDinhKem.Name = "cmdChungTuDinhKem";
            this.cmdChungTuDinhKem.Text = "Chứng từ đính kèm";
            // 
            // cmdVanDon1
            // 
            this.cmdVanDon1.ImageIndex = 0;
            this.cmdVanDon1.Key = "cmdVanDon";
            this.cmdVanDon1.Name = "cmdVanDon1";
            // 
            // cmdHopDong1
            // 
            this.cmdHopDong1.ImageIndex = 0;
            this.cmdHopDong1.Key = "cmdHopDong";
            this.cmdHopDong1.Name = "cmdHopDong1";
            // 
            // cmdGiayPhep1
            // 
            this.cmdGiayPhep1.ImageIndex = 0;
            this.cmdGiayPhep1.Key = "cmdGiayPhep";
            this.cmdGiayPhep1.Name = "cmdGiayPhep1";
            // 
            // cmdHoaDonThuongMai1
            // 
            this.cmdHoaDonThuongMai1.ImageIndex = 0;
            this.cmdHoaDonThuongMai1.Key = "cmdHoaDonThuongMai";
            this.cmdHoaDonThuongMai1.Name = "cmdHoaDonThuongMai1";
            // 
            // cmdCO1
            // 
            this.cmdCO1.ImageIndex = 0;
            this.cmdCO1.Key = "cmdCO";
            this.cmdCO1.Name = "cmdCO1";
            // 
            // cmdChuyenCuaKhau1
            // 
            this.cmdChuyenCuaKhau1.ImageIndex = 0;
            this.cmdChuyenCuaKhau1.Key = "cmdChuyenCuaKhau";
            this.cmdChuyenCuaKhau1.Name = "cmdChuyenCuaKhau1";
            // 
            // cmdChungTuDangAnh1
            // 
            this.cmdChungTuDangAnh1.ImageIndex = 0;
            this.cmdChungTuDangAnh1.Key = "cmdChungTuDangAnh";
            this.cmdChungTuDangAnh1.Name = "cmdChungTuDangAnh1";
            // 
            // cmdTruyenDuLieuTuXa
            // 
            this.cmdTruyenDuLieuTuXa.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend2,
            this.NhanDuLieu2,
            this.XacNhan2,
            this.cmdHuyKhaiBao1,
            this.Separator3,
            this.cmdSuaToKhaiDaDuyet2,
            this.cmdHuyToKhaiDaDuyet2,
            this.Separator4,
            this.cmdKetQuaXuLy1});
            this.cmdTruyenDuLieuTuXa.IsEditableControl = Janus.Windows.UI.InheritableBoolean.True;
            this.cmdTruyenDuLieuTuXa.Key = "cmdTruyenDuLieuTuXa";
            this.cmdTruyenDuLieuTuXa.MdiList = Janus.Windows.UI.InheritableBoolean.True;
            this.cmdTruyenDuLieuTuXa.Name = "cmdTruyenDuLieuTuXa";
            this.cmdTruyenDuLieuTuXa.Text = "Truyền dữ liệu từ xa";
            // 
            // cmdSend2
            // 
            this.cmdSend2.Key = "cmdSend";
            this.cmdSend2.Name = "cmdSend2";
            // 
            // NhanDuLieu2
            // 
            this.NhanDuLieu2.Key = "NhanDuLieu";
            this.NhanDuLieu2.Name = "NhanDuLieu2";
            // 
            // XacNhan2
            // 
            this.XacNhan2.Key = "XacNhan";
            this.XacNhan2.Name = "XacNhan2";
            // 
            // cmdHuyKhaiBao1
            // 
            this.cmdHuyKhaiBao1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdHuyKhaiBao1.Icon")));
            this.cmdHuyKhaiBao1.Key = "cmdHuyKhaiBao";
            this.cmdHuyKhaiBao1.Name = "cmdHuyKhaiBao1";
            // 
            // Separator3
            // 
            this.Separator3.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator3.Key = "Separator";
            this.Separator3.Name = "Separator3";
            // 
            // cmdSuaToKhaiDaDuyet2
            // 
            this.cmdSuaToKhaiDaDuyet2.ImageIndex = 7;
            this.cmdSuaToKhaiDaDuyet2.Key = "cmdSuaToKhaiDaDuyet";
            this.cmdSuaToKhaiDaDuyet2.Name = "cmdSuaToKhaiDaDuyet2";
            // 
            // cmdHuyToKhaiDaDuyet2
            // 
            this.cmdHuyToKhaiDaDuyet2.ImageIndex = 8;
            this.cmdHuyToKhaiDaDuyet2.Key = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet2.Name = "cmdHuyToKhaiDaDuyet2";
            // 
            // Separator4
            // 
            this.Separator4.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator4.Key = "Separator";
            this.Separator4.Name = "Separator4";
            // 
            // cmdKetQuaXuLy1
            // 
            this.cmdKetQuaXuLy1.ImageIndex = 6;
            this.cmdKetQuaXuLy1.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy1.Name = "cmdKetQuaXuLy1";
            // 
            // cmdVanDon
            // 
            this.cmdVanDon.Key = "cmdVanDon";
            this.cmdVanDon.Name = "cmdVanDon";
            this.cmdVanDon.Text = "Vận Đơn";
            // 
            // cmdHopDong
            // 
            this.cmdHopDong.Key = "cmdHopDong";
            this.cmdHopDong.Name = "cmdHopDong";
            this.cmdHopDong.Text = "Hợp Đồng";
            // 
            // cmdGiayPhep
            // 
            this.cmdGiayPhep.Key = "cmdGiayPhep";
            this.cmdGiayPhep.Name = "cmdGiayPhep";
            this.cmdGiayPhep.Text = "Giấy Phép";
            // 
            // cmdHoaDonThuongMai
            // 
            this.cmdHoaDonThuongMai.Key = "cmdHoaDonThuongMai";
            this.cmdHoaDonThuongMai.Name = "cmdHoaDonThuongMai";
            this.cmdHoaDonThuongMai.Text = "Hóa đơn thương mại";
            // 
            // cmdCO
            // 
            this.cmdCO.Key = "cmdCO";
            this.cmdCO.Name = "cmdCO";
            this.cmdCO.Text = "CO";
            // 
            // cmdChuyenCuaKhau
            // 
            this.cmdChuyenCuaKhau.Key = "cmdChuyenCuaKhau";
            this.cmdChuyenCuaKhau.Name = "cmdChuyenCuaKhau";
            this.cmdChuyenCuaKhau.Text = "Đề nghị chuyển cửa khẩu";
            // 
            // cmdToKhaiTriGiaPP1
            // 
            this.cmdToKhaiTriGiaPP1.Key = "cmdToKhaiTriGiaPP1";
            this.cmdToKhaiTriGiaPP1.Name = "cmdToKhaiTriGiaPP1";
            this.cmdToKhaiTriGiaPP1.Text = "Tờ khai trị giá PP1";
            // 
            // cmdToKhaiTriGiaPP2
            // 
            this.cmdToKhaiTriGiaPP2.Key = "cmdToKhaiTriGiaPP2";
            this.cmdToKhaiTriGiaPP2.Name = "cmdToKhaiTriGiaPP2";
            this.cmdToKhaiTriGiaPP2.Text = "Tờ khai trị giá PP2";
            // 
            // cmdToKhaiTriGiaPP3
            // 
            this.cmdToKhaiTriGiaPP3.Key = "cmdToKhaiTriGiaPP3";
            this.cmdToKhaiTriGiaPP3.Name = "cmdToKhaiTriGiaPP3";
            this.cmdToKhaiTriGiaPP3.Text = "Tờ khai trị giá PP3";
            // 
            // cmdHuyKhaiBao
            // 
            this.cmdHuyKhaiBao.Key = "cmdHuyKhaiBao";
            this.cmdHuyKhaiBao.Name = "cmdHuyKhaiBao";
            this.cmdHuyKhaiBao.Text = "Hủy khai báo";
            // 
            // cmdHuyToKhaiDaDuyet
            // 
            this.cmdHuyToKhaiDaDuyet.Key = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet.Name = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet.Text = "Hủy tờ khai đã duyệt";
            // 
            // cmdSuaToKhaiDaDuyet
            // 
            this.cmdSuaToKhaiDaDuyet.Key = "cmdSuaToKhaiDaDuyet";
            this.cmdSuaToKhaiDaDuyet.Name = "cmdSuaToKhaiDaDuyet";
            this.cmdSuaToKhaiDaDuyet.Text = "Sửa tờ khai đã duyệt";
            // 
            // cmdChungTuBoSung
            // 
            this.cmdChungTuBoSung.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHopDongBoSung1,
            this.cmdVanDonBoSung1,
            this.CO1,
            this.cmdChuyenCuaKhauBoSung1,
            this.cmdGiayPhepBoSung1,
            this.cmdHoaDonThuongMaiBoSung1,
            this.cmdChungTuDangAnhBS1});
            this.cmdChungTuBoSung.Key = "cmdChungTuBoSung";
            this.cmdChungTuBoSung.Name = "cmdChungTuBoSung";
            this.cmdChungTuBoSung.Text = "Chứng từ bổ sung";
            // 
            // cmdHopDongBoSung1
            // 
            this.cmdHopDongBoSung1.ImageIndex = 0;
            this.cmdHopDongBoSung1.Key = "cmdHopDongBoSung";
            this.cmdHopDongBoSung1.Name = "cmdHopDongBoSung1";
            this.cmdHopDongBoSung1.Text = "Hợp đồng ";
            // 
            // cmdVanDonBoSung1
            // 
            this.cmdVanDonBoSung1.ImageIndex = 0;
            this.cmdVanDonBoSung1.Key = "cmdVanDonBoSung";
            this.cmdVanDonBoSung1.Name = "cmdVanDonBoSung1";
            // 
            // CO1
            // 
            this.CO1.ImageIndex = 0;
            this.CO1.Key = "CO";
            this.CO1.Name = "CO1";
            // 
            // cmdChuyenCuaKhauBoSung1
            // 
            this.cmdChuyenCuaKhauBoSung1.ImageIndex = 0;
            this.cmdChuyenCuaKhauBoSung1.Key = "cmdChuyenCuaKhauBoSung";
            this.cmdChuyenCuaKhauBoSung1.Name = "cmdChuyenCuaKhauBoSung1";
            // 
            // cmdGiayPhepBoSung1
            // 
            this.cmdGiayPhepBoSung1.ImageIndex = 0;
            this.cmdGiayPhepBoSung1.Key = "cmdGiayPhepBoSung";
            this.cmdGiayPhepBoSung1.Name = "cmdGiayPhepBoSung1";
            // 
            // cmdHoaDonThuongMaiBoSung1
            // 
            this.cmdHoaDonThuongMaiBoSung1.ImageIndex = 0;
            this.cmdHoaDonThuongMaiBoSung1.Key = "cmdHoaDonThuongMaiBoSung";
            this.cmdHoaDonThuongMaiBoSung1.Name = "cmdHoaDonThuongMaiBoSung1";
            // 
            // cmdChungTuDangAnhBS1
            // 
            this.cmdChungTuDangAnhBS1.ImageIndex = 0;
            this.cmdChungTuDangAnhBS1.Key = "cmdChungTuDangAnhBS";
            this.cmdChungTuDangAnhBS1.Name = "cmdChungTuDangAnhBS1";
            // 
            // cmdHopDongBoSung
            // 
            this.cmdHopDongBoSung.Key = "cmdHopDongBoSung";
            this.cmdHopDongBoSung.Name = "cmdHopDongBoSung";
            this.cmdHopDongBoSung.Text = "Hợp đồng bổ sung";
            // 
            // cmdVanDonBoSung
            // 
            this.cmdVanDonBoSung.Key = "cmdVanDonBoSung";
            this.cmdVanDonBoSung.Name = "cmdVanDonBoSung";
            this.cmdVanDonBoSung.Text = "Vận đơn";
            // 
            // CO
            // 
            this.CO.Key = "CO";
            this.CO.Name = "CO";
            this.CO.Text = "CO";
            // 
            // cmdChuyenCuaKhauBoSung
            // 
            this.cmdChuyenCuaKhauBoSung.Key = "cmdChuyenCuaKhauBoSung";
            this.cmdChuyenCuaKhauBoSung.Name = "cmdChuyenCuaKhauBoSung";
            this.cmdChuyenCuaKhauBoSung.Text = "Chuyển cửa khẩu";
            // 
            // cmdGiayPhepBoSung
            // 
            this.cmdGiayPhepBoSung.Key = "cmdGiayPhepBoSung";
            this.cmdGiayPhepBoSung.Name = "cmdGiayPhepBoSung";
            this.cmdGiayPhepBoSung.Text = "Giấy phép";
            // 
            // cmdHoaDonThuongMaiBoSung
            // 
            this.cmdHoaDonThuongMaiBoSung.Key = "cmdHoaDonThuongMaiBoSung";
            this.cmdHoaDonThuongMaiBoSung.Name = "cmdHoaDonThuongMaiBoSung";
            this.cmdHoaDonThuongMaiBoSung.Text = "Hóa đơn thương mại";
            // 
            // cmdChungTuDangAnh
            // 
            this.cmdChungTuDangAnh.Key = "cmdChungTuDangAnh";
            this.cmdChungTuDangAnh.Name = "cmdChungTuDangAnh";
            this.cmdChungTuDangAnh.Text = "Chứng từ dạng ảnh";
            // 
            // cmdInToKhaiTQDT
            // 
            this.cmdInToKhaiTQDT.Key = "cmdInToKhaiTQDT";
            this.cmdInToKhaiTQDT.Name = "cmdInToKhaiTQDT";
            this.cmdInToKhaiTQDT.Text = "In tờ khai thông quan điện tử";
            // 
            // cmdKetQuaXuLy
            // 
            this.cmdKetQuaXuLy.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Name = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Text = "Kết quả xử lý";
            // 
            // cmdChungTuDangAnhBS
            // 
            this.cmdChungTuDangAnhBS.Key = "cmdChungTuDangAnhBS";
            this.cmdChungTuDangAnhBS.Name = "cmdChungTuDangAnhBS";
            this.cmdChungTuDangAnhBS.Text = "Chứng từ dạng ảnh";
            // 
            // cmdInBangKeKemTheo
            // 
            this.cmdInBangKeKemTheo.Key = "cmdInBangKeKemTheo";
            this.cmdInBangKeKemTheo.Name = "cmdInBangKeKemTheo";
            this.cmdInBangKeKemTheo.Text = "In bảng kê kèm theo";
            // 
            // cmdInTKDTSuaDoiBoSung
            // 
            this.cmdInTKDTSuaDoiBoSung.Key = "cmdInTKDTSuaDoiBoSung";
            this.cmdInTKDTSuaDoiBoSung.Name = "cmdInTKDTSuaDoiBoSung";
            this.cmdInTKDTSuaDoiBoSung.Text = "In tờ khai điện tử sửa đổi bổ sung";
            // 
            // cmdInAnDinhThue
            // 
            this.cmdInAnDinhThue.Key = "cmdInAnDinhThue";
            this.cmdInAnDinhThue.Name = "cmdInAnDinhThue";
            this.cmdInAnDinhThue.Text = "In Ấn định thuế tờ khai";
            // 
            // cmdFixAnhDinhThue
            // 
            this.cmdFixAnhDinhThue.Key = "cmdFixAnhDinhThue";
            this.cmdFixAnhDinhThue.Name = "cmdFixAnhDinhThue";
            this.cmdFixAnhDinhThue.Text = "Kiểm tra và cập nhật Ấn định thuế";
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "attached.ico");
            this.ImageList1.Images.SetKeyName(5, "internet.ico");
            this.ImageList1.Images.SetKeyName(6, "explorerBarItem25.Icon.ico");
            this.ImageList1.Images.SetKeyName(7, "page_edit.png");
            this.ImageList1.Images.SetKeyName(8, "page_red.png");
            this.ImageList1.Images.SetKeyName(9, "printer.png");
            this.ImageList1.Images.SetKeyName(10, "export.png");
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbToolBar});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.cmbToolBar);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(808, 34);
            // 
            // cmdReadExcel2
            // 
            this.cmdReadExcel2.Key = "cmdReadExcel";
            this.cmdReadExcel2.Name = "cmdReadExcel2";
            // 
            // ChungTuKemTheo2
            // 
            this.ChungTuKemTheo2.Key = "ChungTuKemTheo";
            this.ChungTuKemTheo2.Name = "ChungTuKemTheo2";
            // 
            // cvSoContainer40
            // 
            this.cvSoContainer40.ControlToValidate = this.txtSoContainer40;
            this.cvSoContainer40.ErrorMessage = "\"Số Container 20\" không hợp lệ.";
            this.cvSoContainer40.Icon = ((System.Drawing.Icon)(resources.GetObject("cvSoContainer40.Icon")));
            this.cvSoContainer40.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThanEqual;
            this.cvSoContainer40.Tag = "cvSoContainer40";
            this.cvSoContainer40.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvSoContainer40.ValueToCompare = "0";
            // 
            // cvSoKienHang
            // 
            this.cvSoKienHang.ControlToValidate = this.txtSoKien;
            this.cvSoKienHang.ErrorMessage = "\"Số kiện hàng\" không hợp lệ.";
            this.cvSoKienHang.Icon = ((System.Drawing.Icon)(resources.GetObject("cvSoKienHang.Icon")));
            this.cvSoKienHang.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThanEqual;
            this.cvSoKienHang.Tag = "cvSoKienHang";
            this.cvSoKienHang.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvSoKienHang.ValueToCompare = "0";
            // 
            // cvTrongLuong
            // 
            this.cvTrongLuong.ControlToValidate = this.txtTrongLuong;
            this.cvTrongLuong.ErrorMessage = "\"Trọng lượng\" không hợp lệ.";
            this.cvTrongLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("cvTrongLuong.Icon")));
            this.cvTrongLuong.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThanEqual;
            this.cvTrongLuong.Tag = "cvTrongLuong";
            this.cvTrongLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvTrongLuong.ValueToCompare = "0";
            // 
            // cvLePhiHQ
            // 
            this.cvLePhiHQ.ControlToValidate = this.txtLePhiHQ;
            this.cvLePhiHQ.ErrorMessage = "\"Lệ phí HQ\" không hợp lệ.";
            this.cvLePhiHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("cvLePhiHQ.Icon")));
            this.cvLePhiHQ.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThanEqual;
            this.cvLePhiHQ.Tag = "cvLePhiHQ";
            this.cvLePhiHQ.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvLePhiHQ.ValueToCompare = "0";
            // 
            // cvPhiBaoHiem
            // 
            this.cvPhiBaoHiem.ControlToValidate = this.txtPhiBaoHiem;
            this.cvPhiBaoHiem.ErrorMessage = "\"Phí bảo hiểm\" không hợp lệ.";
            this.cvPhiBaoHiem.Icon = ((System.Drawing.Icon)(resources.GetObject("cvPhiBaoHiem.Icon")));
            this.cvPhiBaoHiem.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThanEqual;
            this.cvPhiBaoHiem.Tag = "cvPhiBaoHiem";
            this.cvPhiBaoHiem.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvPhiBaoHiem.ValueToCompare = "0";
            // 
            // cvPhiVanChuyen
            // 
            this.cvPhiVanChuyen.ControlToValidate = this.txtPhiVanChuyen;
            this.cvPhiVanChuyen.ErrorMessage = "\"Phí bảo hiểm\" không hợp lệ.";
            this.cvPhiVanChuyen.Icon = ((System.Drawing.Icon)(resources.GetObject("cvPhiVanChuyen.Icon")));
            this.cvPhiVanChuyen.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThanEqual;
            this.cvPhiVanChuyen.Tag = "cvPhiVanChuyen";
            this.cvPhiVanChuyen.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvPhiVanChuyen.ValueToCompare = "0";
            // 
            // cvPhiKhac
            // 
            this.cvPhiKhac.ControlToValidate = this.txtPhiNganHang;
            this.cvPhiKhac.ErrorMessage = "\"Phí khác\" không hợp lệ.";
            this.cvPhiKhac.Icon = ((System.Drawing.Icon)(resources.GetObject("cvPhiKhac.Icon")));
            this.cvPhiKhac.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThanEqual;
            this.cvPhiKhac.Tag = "cvPhiKhac";
            this.cvPhiKhac.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvPhiKhac.ValueToCompare = "0";
            // 
            // cvSoContainer20
            // 
            this.cvSoContainer20.ControlToValidate = this.txtSoContainer20;
            this.cvSoContainer20.ErrorMessage = "\"Số Container 20\" không hợp lệ.";
            this.cvSoContainer20.Icon = ((System.Drawing.Icon)(resources.GetObject("cvSoContainer20.Icon")));
            this.cvSoContainer20.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThanEqual;
            this.cvSoContainer20.Tag = "cvSoContainer20";
            this.cvSoContainer20.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvSoContainer20.ValueToCompare = "0";
            // 
            // cvNgayHHGP
            // 
            this.cvNgayHHGP.ControlToCompare = this.ccNgayGiayPhep;
            this.cvNgayHHGP.ControlToValidate = this.ccNgayHHGiayPhep;
            this.cvNgayHHGP.ErrorMessage = "\"Ngày hết hạn giấy phép phải lớn hơn ngày giấy phép\"";
            this.cvNgayHHGP.Icon = ((System.Drawing.Icon)(resources.GetObject("cvNgayHHGP.Icon")));
            this.cvNgayHHGP.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvNgayHHGP.Type = Company.Controls.CustomValidation.ValidationDataType.Date;
            this.cvNgayHHGP.ValueToCompare = "0";
            // 
            // cvNgayHHHD
            // 
            this.cvNgayHHHD.ControlToCompare = this.ccNgayHopDong;
            this.cvNgayHHHD.ControlToValidate = this.ccNgayHHHopDong;
            this.cvNgayHHHD.ErrorMessage = "\"Ngày hết hạn phải lớn hơn ngày ký\"";
            this.cvNgayHHHD.Icon = ((System.Drawing.Icon)(resources.GetObject("cvNgayHHHD.Icon")));
            this.cvNgayHHHD.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvNgayHHHD.Type = Company.Controls.CustomValidation.ValidationDataType.Date;
            this.cvNgayHHHD.ValueToCompare = "0";
            // 
            // cvTrongLuongTinh
            // 
            this.cvTrongLuongTinh.ControlToValidate = this.txtTrongLuongTinh;
            this.cvTrongLuongTinh.ErrorMessage = "\"Trọng lượng tịnh\" không hợp lệ.";
            this.cvTrongLuongTinh.Icon = ((System.Drawing.Icon)(resources.GetObject("cvTrongLuongTinh.Icon")));
            this.cvTrongLuongTinh.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThanEqual;
            this.cvTrongLuongTinh.Tag = "cvTrongLuongTinh";
            this.cvTrongLuongTinh.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvTrongLuongTinh.ValueToCompare = "0";
            // 
            // rfvNgayDen
            // 
            this.rfvNgayDen.ControlToValidate = this.ccNgayDen;
            this.rfvNgayDen.ErrorMessage = "\"Ngày đến\" không được để trống.";
            this.rfvNgayDen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayDen.Icon")));
            this.rfvNgayDen.Tag = "rfvNgayDen";
            // 
            // InToKhai1
            // 
            this.InToKhai1.Key = "InToKhai";
            this.InToKhai1.Name = "InToKhai1";
            // 
            // InPhieuTN1
            // 
            this.InPhieuTN1.Key = "InPhieuTN";
            this.InPhieuTN1.Name = "InPhieuTN1";
            // 
            // rangeValidator1
            // 
            this.rangeValidator1.ControlToValidate = this.txtSoLuongPLTK;
            this.rangeValidator1.ErrorMessage = "Giá trị không hợp lệ";
            this.rangeValidator1.Icon = ((System.Drawing.Icon)(resources.GetObject("rangeValidator1.Icon")));
            this.rangeValidator1.MaximumValue = "255";
            this.rangeValidator1.MinimumValue = "0";
            this.rangeValidator1.Tag = "rangeValidator1";
            this.rangeValidator1.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // cmdThemHang2
            // 
            this.cmdThemHang2.Key = "cmdThemHang";
            this.cmdThemHang2.Name = "cmdThemHang2";
            // 
            // cmdInToKhaiTT15
            // 
            this.cmdInToKhaiTT15.Key = "cmdInToKhaiTT15";
            this.cmdInToKhaiTT15.Name = "cmdInToKhaiTT15";
            this.cmdInToKhaiTT15.Text = "In tờ khai thông tư 15";
            // 
            // cmdInToKhaiTT151
            // 
            this.cmdInToKhaiTT151.ImageIndex = 9;
            this.cmdInToKhaiTT151.Key = "cmdInToKhaiTT15";
            this.cmdInToKhaiTT151.Name = "cmdInToKhaiTT151";
            // 
            // ToKhaiMauDichForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(808, 743);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ToKhaiMauDichForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Tờ khai nhập khẩu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ToKhaiNhapKhauSXXK_Form_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.pnlToKhaiMauDich.ResumeLayout(false);
            this.pnlToKhaiMauDich.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grpLoaiHangHoa)).EndInit();
            this.grpLoaiHangHoa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiNK)).EndInit();
            this.grbNguoiNK.ResumeLayout(false);
            this.grbNguoiNK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemXepHang)).EndInit();
            this.grbDiaDiemXepHang.ResumeLayout(false);
            this.grbDiaDiemXepHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguyenTe)).EndInit();
            this.grbNguyenTe.ResumeLayout(false);
            this.grbNguyenTe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNuocXK)).EndInit();
            this.grbNuocXK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiXK)).EndInit();
            this.grbNguoiXK.ResumeLayout(false);
            this.grbNguoiXK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDong)).EndInit();
            this.grbHopDong.ResumeLayout(false);
            this.grbHopDong.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).EndInit();
            this.uiGroupBox18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).EndInit();
            this.uiGroupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemDoHang)).EndInit();
            this.grbDiaDiemDoHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbVanTaiDon)).EndInit();
            this.grbVanTaiDon.ResumeLayout(false);
            this.grbVanTaiDon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbGiayPhep)).EndInit();
            this.gbGiayPhep.ResumeLayout(false);
            this.gbGiayPhep.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHoaDonThuongMai)).EndInit();
            this.grbHoaDonThuongMai.ResumeLayout(false);
            this.grbHoaDonThuongMai.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHangMauDich)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiHinhMauDich)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPTTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCuaKhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompanyNuoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieuPTVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).EndInit();
            this.uiPanel2.ResumeLayout(false);
            this.uiPanel2Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiXuatKhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaUSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToolBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cvSoContainer40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoKienHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvTrongLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvLePhiHQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvPhiBaoHiem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvPhiVanChuyen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvPhiKhac)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoContainer20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvNgayHHGP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvNgayHHHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvTrongLuongTinh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayDen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private LoaiHinhMauDichControl01 ctrLoaiHinhMauDich;
        private NguyenTeControl ctrNguyenTe;
        private NuocControl ctrNuocXuatKhau;
        private UIComboBox cbPTTT;
        private UIComboBox cbDKGH;
        private CuaKhauControl ctrCuaKhau;
        private UICommand cmdThemHang;
        private UICommand cmdTinhLaiThue;
        private UICommand cmdPrint;
        private UICommand cmdSend;
        private UIGroupBox grpLoaiHangHoa;
        private UIRadioButton radNPL;
        private UIRadioButton radSP;
        private UIRadioButton radTB;
        private UIPanel uiPanel2;
        private UIPanelInnerContainer uiPanel2Container;
        private UICommand ChungTuKemTheo;
        private UIGroupBox uiGroupBox4;
        private EditBox txtChungTu;
        private Label label18;
        private Label label10;
        private Label lblTrangThai;
        private EditBox txtSoToKhai;
        private GridEX gridEX1;
        private GridEX dgList;
        private UICommand cmdReadExcel;
        private UICommand NhanDuLieu;
        private UICommand ThemHang2;
        private UICommand cmdThemHang1;
        private UICommand cmdReadExcel1;
        private UICommand ChungTuKemTheo1;
        private UICommand cmdReadExcel2;
        private UICommand ChungTuKemTheo2;
        private EditBox txtSoTiepNhan;
        private Label lblSoTiepNhan;
        private UICommand XacNhan;
        private UICommand FileDinhKem;
        private NumericEditBox txtPhiNganHang;
        private Label label24;
        private Label label25;
        private NumericEditBox txtTrongLuongTinh;
        private CompareValidator cvSoContainer40;
        private CompareValidator cvSoKienHang;
        private CompareValidator cvTrongLuong;
        private CompareValidator cvLePhiHQ;
        private CompareValidator cvPhiBaoHiem;
        private CompareValidator cvPhiVanChuyen;
        private CompareValidator cvPhiKhac;
        private CompareValidator cvSoContainer20;
        private CompareValidator cvNgayHHGP;
        private CompareValidator cvNgayHHHD;
        private CompareValidator cvTrongLuongTinh;
        private RequiredFieldValidator rfvNgayDen;
        private UICommand InPhieuTN;
        private UICommand ToKhai;
        private UICommand InToKhai2;
        private UICommand InPhieuTN2;
        private UICommand InToKhai1;
        private UICommand InPhieuTN1;
        private RangeValidator rangeValidator1;
        private UICommand cmdPrintA4;
        private UICommand cmdPrintA41;
        private Label lblTongTGKB;
        private UICommand cmdToKhaiTriGia;
        private UICommand cmdChungTuDinhKem;
        private UICommand cmdTruyenDuLieuTuXa;
        private UICommand cmdThemHang2;
        private UICommand cmdSend2;
        private UICommand NhanDuLieu2;
        private UICommand XacNhan2;
        private UICommand cmdVanDon1;
        private UICommand cmdHopDong1;
        private UICommand cmdGiayPhep1;
        private UICommand cmdHoaDonThuongMai1;
        private UICommand cmdCO1;
        private UICommand cmdChuyenCuaKhau1;
        private UICommand cmdVanDon;
        private UICommand cmdHopDong;
        private UICommand cmdGiayPhep;
        private UICommand cmdHoaDonThuongMai;
        private UICommand cmdCO;
        private UICommand cmdChuyenCuaKhau;
        private UICommand cmdHuyKhaiBao1;
        private UICommand cmdToKhaiTriGiaPP1;
        private UICommand cmdToKhaiTriGiaPP2;
        private UICommand cmdToKhaiTriGiaPP3;
        private UICommand cmdHuyKhaiBao;
        private UICommand cmdToKhaiTriGiaPP11;
        private UICommand cmdToKhaiTriGiaPP21;
        private UICommand cmdToKhaiTriGiaPP31;
        private UICommand ThemHang1;
        private UICommand cmdSave1;
        private UICommand Separator1;
        private UICommand cmdChungTuDinhKem1;
        private UICommand cmdTruyenDuLieuTuXa1;
        private UICommand Separator2;
        private UICommand cmdPrint1;
        private UICommand cmdTinhLaiThue1;
        private UICommand cmdHuyToKhaiDaDuyet;
        private UICommand cmdSuaToKhaiDaDuyet;
        private Label lblPhanLuong;
        private Label label26;
        private UICommand cmdChungTuBoSung;
        private UICommand cmdHopDongBoSung;
        private UICommand cmdVanDonBoSung;
        private UICommand CO;
        private UICommand cmdChuyenCuaKhauBoSung;
        private UICommand cmdChungTuBoSung1;
        private UICommand cmdGiayPhepBoSung;
        private UICommand cmdHoaDonThuongMaiBoSung;
        private UICommand cmdChungTuDangAnh;
        private UICommand cmdChungTuDangAnh1;
        private UICommand cmdInToKhaiTQDT;
        private UICommand cmdInToKhaiTQDT1;
        private UICommand cmdKetQuaXuLy1;
        private UICommand cmdKetQuaXuLy;
        private UICommand cmdHopDongBoSung1;
        private UICommand cmdVanDonBoSung1;
        private UICommand CO1;
        private UICommand cmdChuyenCuaKhauBoSung1;
        private UICommand cmdGiayPhepBoSung1;
        private UICommand cmdHoaDonThuongMaiBoSung1;
        private UICommand cmdChungTuDangAnhBS1;
        private UICommand cmdChungTuDangAnhBS;
        private UICommand cmdInBangKeKemTheo1;
        private UICommand cmdInBangKeKemTheo;
        private EditBox txtLyDoSua;
        private EditBox txtDeXuatKhac;
        private Label label28;
        private Label label3;
        private UICommand Separator3;
        private UICommand cmdSuaToKhaiDaDuyet2;
        private UICommand cmdHuyToKhaiDaDuyet2;
        private UICommand Separator4;
        private UICommand cmdInTKDTSuaDoiBoSung1;
        private UICommand cmdInTKDTSuaDoiBoSung;
        private UIButton btnNoiDungDieuChinhTKForm;
        private ImageList ImageList1;
        private UICommand cmdInAnDinhThue1;
        private UICommand cmdInAnDinhThue;
        private Label label27;
        private Label lblTongThue;
        private Label lblTongTGNT;
        private Label lblTongSoHang;
        private Label label35;
        private UICommand cmdFixAnhDinhThue;
        private UICommand cmdInToKhaiTT151;
        private UICommand cmdInToKhaiTT15;
    }
}
