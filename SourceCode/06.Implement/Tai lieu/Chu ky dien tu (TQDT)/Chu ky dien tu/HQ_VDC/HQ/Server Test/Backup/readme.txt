using VNPT_CA.CryptokiLib;


//class ServerSide

/*Hàm ký.
         * In: data (binary) dùng để ký
         *      certFilePath đường đẫn đến file Certificate để ký (*.p12,*.pfx).
		 *		sPIN mật khẩu của file *.p12 hoặc *.pfx
         * Out: Chữ ký số (binary) 
*/
public static byte[] Sign(byte[] data, string certFilePath , string sPIN);


/*Hàm thẩm tra chữ ký.
         * In: data (binary) dùng để thẩm tra
		 *		signature chữ ký dùng để thẩm tra
         *      certFilePath đường đẫn đến file Certificate để thẩm tra.
         * Out: Trạng thái kiểm tra chữ ký 
*/
public static bool Verify(byte[] data, byte[] signature, string certFilePath);


/*Hàm mã hóa.
         * In: data (binary)
         *      certFilePath đường đẫn đến file Certificate để mã hóa.
         * Out: Dữ liệu đã mã hóa (binary) 
*/
public static byte[] Encrypt(byte[] data, String certFilePath);


/*Hàm giải mã
		* In: data (binary)
         *      certFilePath đường đẫn đến file Certificate để giải mã (*.p12,*.pfx).
         *		sPIN mật khẩu của file *.p12 hoặc *.pfx
         * Out: Dữ liệu đã giải mã (binary) 
 */
public static byte[] Decrypt(byte[] data, String certFilePath, string sPIN);


/*Kiểm tra trạng thái trust (root path)
        * In: 
        *      certFilePath đường đẫn đến file Certificate để kiểm tra
        *      trustCertFilePath đường đẫn đến file trust Certificate  để kiểm tra
        * Out: bool   Trạng thái Certificate 
*/
public static bool VerifyTrustCertificate(string certFilePath, string trustCertFilePath)


/*Kiểm tra trạng thái trust (root path) của  file *.p12,*.pfx
        * In: 
        *      certFilePath đường đẫn đến file Certificate để kiểm tra(*.p12,*.pfx)
        *	   sPIN mật khẩu của file *.p12 hoặc *.pfx
        *      trustCertFilePath đường đẫn đến file trust Certificate  để kiểm tra
        * Out: bool   Trạng thái Certificate 
*/
public static bool VerifyTrustCertificate(string certFilePath,string sPIN, string trustCertFilePath)



/*Kiểm tra trạng thái Certifciate
        * In: 
        *      certFilePath đường đẫn đến file Certificate để kiểm tra
        *      bool OnlineMode  Kiểm tra Online hay Offline
        * Out: bool   Trạng thái Certificate 
*/
public static bool VerifyCertificate(string certFilePath, bool bOnlineMode);




/*Kiểm tra trạng thái Certifciate ( file *.p12,*.pfx)
        * In: 
        *      certFilePath đường đẫn đến file Certificate để kiểm tra (*.p12,*.pfx)
		*	   sPIN mật khẩu của file *.p12 hoặc *.pfx			
        *      bool OnlineMode  Kiểm tra Online hay Offline
        * Out: bool   Trạng thái Certificate 
*/
public static bool VerifyCertificate(string certFilePath,string sPIN, bool bOnlineMode);



//class CertificateInfo
/*Hàm khởi tạo
		* In:	certFilePath đường đẫn đến file Certificate
*/
public CertificateInfo (String sCertFilePath);

/*Hàm khởi tạo
		* In:	certFilePath đường đẫn đến file Certificate (*.p12,*.pfx)
		* sPIN mật khẩu của file *.p12 hoặc *.pfx	
*/
public CertificateInfo(String sCertFilePath, String sPIN);

//Serial Number của Certificate
public String SerialNumber

//Subject của Certificate
public String Subject

//Issuer của Certificate
public String Issuer

//Thời gian bắt đầu có hiệu lực của Certificate
public DateTime NotBefore

//Thời gian hết hiệu lực của Certificate
public DateTime NotAfter

//Dữ liệu Certificate
public byte[] RawData 


Pass: 

1. ncs01 -- ncs01
2. other vnptca