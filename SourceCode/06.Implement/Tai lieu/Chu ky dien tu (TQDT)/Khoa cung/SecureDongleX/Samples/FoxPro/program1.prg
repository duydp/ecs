clear 

DECLARE integer   SDX_Find   IN SDX.dll
DECLARE integer   SDX_Open   IN SDX.dll integer mode, integer uid, integer @ hid  
DECLARE integer   SDX_Close  IN SDX.dll integer  hic
DECLARE integer   SDX_Read   IN SDX.dll integer handle, integer block_index, string @ buffer512
DECLARE integer   SDX_Write  IN SDX.dll integer handle, integer block_index, string @ buffer512
DECLARE integer	  SDX_GetVersion IN SDX.dll integer handle
DECLARE integer   SDX_Transform IN SDX.dll integer handle, integer lenData, string @ buffer512
DECLARE integer   SDX_RSAEncrypt IN SDX.dll integer handle, integer startIndex, string @ bufferData, integer @ lenData, string @ key512
DECLARE integer   SDX_RSADecrypt IN SDX.dll integer handle, integer startIndex, string @ bufferData, integer @ lenData, string @ key512
store -1 to HID_MODE

buffer = space(512)
temp = space(512)
bufferData = space(1000)
plainText = space(1000)
key512 = space(512)

store 0 to  flag
store 0 to  retcode, handle, startIndex, bufferLength, block_index
store 0 to  uid, hid
store 0 to  i, j, opt
store 0 to  strlen
do while opt != 0
	? "1. Write Data"
	? "2. Read Data" 
	? "3. Transform Data"
	? "4. Encrypt Data"
	? "5. Decrypt Data"
	? "6. Write Mapped Data"
	? "7. Read Mapped Data"
	? "0. Exit"
	Input "Please enter selection: " to opt

	* Write to SecureDongle X with specified UID
	if (opt == 1)	 
	   retcode = SDX_Find()
		if (retcode < 0)
			? "Error Finding SecureDongle X: ", retcode
			return 
		endif	
		if (retcode == 0)
			? "No SecureDongle X plugged" 
			return 
		endif
		? "Found SecureDongle X: ", retcode
		
		input "Please input UID (i.e. 715400947)" to uid 
		retcode = SDX_Open(1, uid, @hid) 
		if (retcode < 0)		 
			? "Error: ", retcode 
			return 
		endif
		? "Succeeded Opening SecureDongle X, UID: ", uid
		handle = retcode 

		input "Please input write block index (0-4):" to block_index 
		
		buffer = 0
		accept "Please input data to be written (i.e. SDX - sample data): " to buffer 
		
        strlen = len(buffer)
        buffer = left(buffer, strlen) + space(512-strlen)
        		
 		retcode = SDX_Write(handle, block_index, @buffer) 
		if (retcode < 0)		 
			? "Error: ", retcode 
			return 
		endif
		? "Write: ", buffer

		SDX_Close(handle) 
	endif

	* Read SecureDongle X with specified UID 
	if (opt == 2)	 
	   retcode = SDX_Find()
		if (retcode < 0)
			? "Error Finding SecureDongle X: ", retcode
			return 
		endif	
		if (retcode == 0)
			? "No SecureDongle X plugged" 
			return 
		endif
		? "Found SecureDongle X: ", retcode
		
		input "Please input UID (i.e. 715400947)" to uid 
		retcode = SDX_Open(1, uid, @hid) 
		if (retcode < 0)		 
			? "Error: ", retcode 
			return 
		endif
		? "Succeeded Opening SecureDongle X, UID: ", uid
		handle = retcode 

		input "Please input Read block index (0-4): " to block_index 
		buffer = space(512)
		retcode = SDX_Read(handle, block_index, @buffer) 						
		if (retcode < 0)		 
			? "Error: ", retcode 
			return 
		endif 		
		? "Read: ", buffer	

		SDX_Close(handle) 
	endif
	
	* Transform data using specified SecureDongle X
	if (opt == 3)
	   retcode = SDX_Find()
		if (retcode < 0)
			? "Error Finding SecureDongle X: ", retcode
			return 
		endif	
		if (retcode == 0)
			? "No SecureDongle X plugged" 
			return 
		endif
		? "Found SecureDongle X: ", retcode
		
		input "Please input UID (i.e. 715400947)" to uid 
		retcode = SDX_Open(1, uid, @hid) 
		if (retcode < 0)		 
			? "Error: ", retcode 
			return 
		endif
		? "Succeeded Opening SecureDongle X, UID: ", uid
		handle = retcode 

		buffer = 0
		accept "Please input data (i.e. SDXTransform test): " to buffer 
			
	    strlen = len(buffer)
	    buffer = left(buffer, strlen) + space(512-strlen)
	        		
	 	retcode = SDX_Transform(handle, strlen, @buffer) 			
		if (retcode < 0)		 
			? "Error: ", retcode 
			return 
		endif
		? "Transform result: ", buffer

		SDX_Close(handle) 	
	endif 

	* Encrypt using RSA and store to SecureDongle X with specified UID	
	if (opt == 4)
	   retcode = SDX_Find()
		if (retcode < 0)
			? "Error Finding SecureDongle X: ", retcode
			return 
		endif	
		if (retcode == 0)
			? "No SecureDongle X plugged" 
			return 
		endif
		? "Found SecureDongle X: ", retcode
		
		input "Please input UID (i.e. 715400947)" to uid 
		retcode = SDX_Open(1, uid, @hid) 
		if (retcode < 0)		 
			? "Error: ", retcode 
			return 
		endif
		? "Succeeded Opening SecureDongle X, UID: ", uid
		handle = retcode 

		bufferData = 0
		input "Please enter start index (0-2559): " to startIndex
		accept "Please enter data to encrypt: " to bufferData
		bufferLength = len(bufferData)	
		retcode = SDX_RSAEncrypt(handle, startIndex, @bufferData, @bufferLength, @key512) 							
		if (retcode < 0)		 
			? "Error: ", retcode 
			return 
		endif 			 
		? "Write success, size: ", bufferLength
		? "Key to decrypt is stored inside key512 variable."
		? "You can try reading the contents of the SDX"
		
		SDX_Close(handle) 			
	endif

	* Read from SecureDongle X with specified UID and decrypt using RSA	
	if (opt == 5)
	   retcode = SDX_Find()
		if (retcode < 0)
			? "Error Finding SecureDongle X: ", retcode
			return 
		endif	
		if (retcode == 0)
			? "No SecureDongle X plugged" 
			return 
		endif
		? "Found SecureDongle X: ", retcode
		
		input "Please input UID (i.e. 715400947)" to uid 
		retcode = SDX_Open(1, uid, @hid) 
		if (retcode < 0)		 
			? "Error: ", retcode 
			return 
		endif
		? "Succeeded Opening SecureDongle X, UID: ", uid
		handle = retcode 

		bufferData = 0
		input "Please enter start index (0-2559): " to startIndex
		input "Please enter length of data to decrypt: " to bufferLength
		retcode = SDX_RSADecrypt(handle, startIndex, @plainText, @bufferLength, @key512) 							
		if (retcode < 0)		 
			? "Error: ", retcode 
			return 
		endif 		
	 	? "Decrypted Data: ", plainText

		SDX_Close(handle) 			 
	endif
	
	* Write to SecureDongle X with specified UID
	* This is a sample of simple Data Mapping to put plain text data not in order
	* For example, in 5-byte data, byte 1 will be put in SDX memory location 10, byte 2 will be put in SDX memory location 1
	if (opt == 6)	 
	   retcode = SDX_Find()
		if (retcode < 0)
			? "Error Finding SecureDongle X: ", retcode
			return 
		endif	
		if (retcode == 0)
			? "No SecureDongle X plugged" 
			return 
		endif
		? "Found SecureDongle X: ", retcode
		
		input "Please input UID (i.e. 715400947)" to uid 
		retcode = SDX_Open(1, uid, @hid) 
		if (retcode < 0)		 
			? "Error: ", retcode 
			return 
		endif
		? "Succeeded Opening SecureDongle X, UID: ", uid
		handle = retcode 

		input "Please input write block index (0-4):" to block_index 
		
		buffer = 0
		temp = Space(0)
		accept "Please input data to be written (i.e. SDX - Sample Map Encryption): " to buffer 
		
        strlen = len(buffer)
		? "Data Length = ", strlen
		 
		******************* ENCRYPTION SAMPLE *******************
		if (strlen > 512)
			? "Error: Data size max is 512"
			return
		endif

		*Create a 512-byte Full Map with randomized content
		for i = 0 to 512-1
			if (int(rand()*2)+1 = 1)			
				temp = temp + chr(int(rand()*26) + 65)
			else
				temp = temp + chr(int(rand()*26) + 97)
			endif
		endfor
		
      	*Calculate where to put the data, and put it there
		for i = 0 to strlen-1
			sze = i * (strlen-1)			
			indx = sze % 512
			temp1 = left(temp, indx)
			temp2 = right(temp, 511-indx)
			temp = temp1 + substr(buffer, i+1, 1) + temp2
		endfor
		*********************************************************
        ? "Len: ", len(temp)  		
 		retcode = SDX_Write(handle, block_index, @temp) 
		if (retcode < 0)		 
			? "Error: ", retcode 
			return 
		endif
		? "Write: ", temp

		SDX_Close(handle) 
	endif

	* Read SecureDongle X with specified UID 
	* This is the sample to read the Mapped data
	if (opt == 7)	 
	   retcode = SDX_Find()
		if (retcode < 0)
			? "Error Finding SecureDongle X: ", retcode
			return 
		endif	
		if (retcode == 0)
			? "No SecureDongle X plugged" 
			return 
		endif
		? "Found SecureDongle X: ", retcode
		
		input "Please input UID (i.e. 715400947)" to uid 
		retcode = SDX_Open(1, uid, @hid) 
		if (retcode < 0)		 
			? "Error: ", retcode 
			return 
		endif
		? "Succeeded Opening SecureDongle X, UID: ", uid
		handle = retcode 

		input "Please input Read block index (0-4): " to block_index 
		input "Please enter size of the data to be read (1-512)" to datasize

		buffer = space(512)		
		retcode = SDX_Read(handle, block_index, @buffer) 						
		if (retcode < 0)		 
			? "Error: ", retcode 
			return 
		endif 		
		 
	    ******************** DECRYPTION **********************
		buff = space(0)
		for i = 0 to datasize-1
			sze = i * (datasize-1)
			indx = sze % 512
			buff = buff + substr(buffer, indx+1, 1)
		endfor
		******************************************************

		? "Read: ", buff	

		SDX_Close(handle) 
	endif	
enddo

accept "Press any key to exit" To retcode

clear dlls
return