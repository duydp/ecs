/*
Run this script on:

        192.168.72.151\sqlserver.ECS_TQDT_SXXK_V4_HOA_THO_28_07_2015    -  This database will be modified

to synchronize it with:

        192.168.72.151\sqlserver.ECS_TQDT_SXXK_V4_HOATHO_4_7_2015_TEST

You are recommended to back up your database before running this script

Script created by SQL Compare version 11.2.1 from Red Gate Software Ltd at 18/08/2015 9:39:12 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[v_KDT_SXXK_NPLNhapTon]'
GO
EXEC sp_refreshview N'[dbo].[v_KDT_SXXK_NPLNhapTon]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[v_KDT_SXXK_NPLXuatTon]'
GO
EXEC sp_refreshview N'[dbo].[v_KDT_SXXK_NPLXuatTon]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy] ADD
[ListTKNK_Hoan] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListTKNK_KhongThu] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Insert]
-- Database: ECS_TQDT_SXXK_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 03, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Insert]
	@SoTiepNhan bigint,
	@MaHaiQuanTiepNhan char(6),
	@NamTiepNhan smallint,
	@NgayTiepNhan datetime,
	@MaDoanhNghiep varchar(14),
	@TrangThaiXuLy int,
	@TrangThaiThanhKhoan int,
	@SoHoSo int,
	@NgayBatDau datetime,
	@NgayKetThuc datetime,
	@LanThanhLy int,
	@SoQuyetDinh nvarchar(100),
	@NgayQuyetDinh datetime,
	@UserName varchar(50),
	@GuidStr nvarchar(36),
	@ListTKNK_Hoan nvarchar(4000),
	@ListTKNK_KhongThu nvarchar(4000),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy]
(
	[SoTiepNhan],
	[MaHaiQuanTiepNhan],
	[NamTiepNhan],
	[NgayTiepNhan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[TrangThaiThanhKhoan],
	[SoHoSo],
	[NgayBatDau],
	[NgayKetThuc],
	[LanThanhLy],
	[SoQuyetDinh],
	[NgayQuyetDinh],
	[UserName],
	[GuidStr],
	[ListTKNK_Hoan],
	[ListTKNK_KhongThu]
)
VALUES 
(
	@SoTiepNhan,
	@MaHaiQuanTiepNhan,
	@NamTiepNhan,
	@NgayTiepNhan,
	@MaDoanhNghiep,
	@TrangThaiXuLy,
	@TrangThaiThanhKhoan,
	@SoHoSo,
	@NgayBatDau,
	@NgayKetThuc,
	@LanThanhLy,
	@SoQuyetDinh,
	@NgayQuyetDinh,
	@UserName,
	@GuidStr,
	@ListTKNK_Hoan,
	@ListTKNK_KhongThu
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Update]
-- Database: ECS_TQDT_SXXK_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 03, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@MaHaiQuanTiepNhan char(6),
	@NamTiepNhan smallint,
	@NgayTiepNhan datetime,
	@MaDoanhNghiep varchar(14),
	@TrangThaiXuLy int,
	@TrangThaiThanhKhoan int,
	@SoHoSo int,
	@NgayBatDau datetime,
	@NgayKetThuc datetime,
	@LanThanhLy int,
	@SoQuyetDinh nvarchar(100),
	@NgayQuyetDinh datetime,
	@UserName varchar(50),
	@GuidStr nvarchar(36),
	@ListTKNK_Hoan nvarchar(4000),
	@ListTKNK_KhongThu nvarchar(4000)
AS

UPDATE
	[dbo].[t_KDT_SXXK_HoSoThanhLyDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[MaHaiQuanTiepNhan] = @MaHaiQuanTiepNhan,
	[NamTiepNhan] = @NamTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[TrangThaiThanhKhoan] = @TrangThaiThanhKhoan,
	[SoHoSo] = @SoHoSo,
	[NgayBatDau] = @NgayBatDau,
	[NgayKetThuc] = @NgayKetThuc,
	[LanThanhLy] = @LanThanhLy,
	[SoQuyetDinh] = @SoQuyetDinh,
	[NgayQuyetDinh] = @NgayQuyetDinh,
	[UserName] = @UserName,
	[GuidStr] = @GuidStr,
	[ListTKNK_Hoan] = @ListTKNK_Hoan,
	[ListTKNK_KhongThu] = @ListTKNK_KhongThu
WHERE
	[ID] = @ID


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 03, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@MaHaiQuanTiepNhan char(6),
	@NamTiepNhan smallint,
	@NgayTiepNhan datetime,
	@MaDoanhNghiep varchar(14),
	@TrangThaiXuLy int,
	@TrangThaiThanhKhoan int,
	@SoHoSo int,
	@NgayBatDau datetime,
	@NgayKetThuc datetime,
	@LanThanhLy int,
	@SoQuyetDinh nvarchar(100),
	@NgayQuyetDinh datetime,
	@UserName varchar(50),
	@GuidStr nvarchar(36),
	@ListTKNK_Hoan nvarchar(4000),
	@ListTKNK_KhongThu nvarchar(4000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_HoSoThanhLyDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[MaHaiQuanTiepNhan] = @MaHaiQuanTiepNhan,
			[NamTiepNhan] = @NamTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[TrangThaiThanhKhoan] = @TrangThaiThanhKhoan,
			[SoHoSo] = @SoHoSo,
			[NgayBatDau] = @NgayBatDau,
			[NgayKetThuc] = @NgayKetThuc,
			[LanThanhLy] = @LanThanhLy,
			[SoQuyetDinh] = @SoQuyetDinh,
			[NgayQuyetDinh] = @NgayQuyetDinh,
			[UserName] = @UserName,
			[GuidStr] = @GuidStr,
			[ListTKNK_Hoan] = @ListTKNK_Hoan,
			[ListTKNK_KhongThu] = @ListTKNK_KhongThu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy]
		(
			[SoTiepNhan],
			[MaHaiQuanTiepNhan],
			[NamTiepNhan],
			[NgayTiepNhan],
			[MaDoanhNghiep],
			[TrangThaiXuLy],
			[TrangThaiThanhKhoan],
			[SoHoSo],
			[NgayBatDau],
			[NgayKetThuc],
			[LanThanhLy],
			[SoQuyetDinh],
			[NgayQuyetDinh],
			[UserName],
			[GuidStr],
			[ListTKNK_Hoan],
			[ListTKNK_KhongThu]
		)
		VALUES 
		(
			@SoTiepNhan,
			@MaHaiQuanTiepNhan,
			@NamTiepNhan,
			@NgayTiepNhan,
			@MaDoanhNghiep,
			@TrangThaiXuLy,
			@TrangThaiThanhKhoan,
			@SoHoSo,
			@NgayBatDau,
			@NgayKetThuc,
			@LanThanhLy,
			@SoQuyetDinh,
			@NgayQuyetDinh,
			@UserName,
			@GuidStr,
			@ListTKNK_Hoan,
			@ListTKNK_KhongThu
		)		
	END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[v_HangTon]'
GO
EXEC sp_refreshview N'[dbo].[v_HangTon]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[v_HangTon_Backup]'
GO
EXEC sp_refreshview N'[dbo].[v_HangTon_Backup]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Load]
-- Database: ECS_TQDT_SXXK_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 03, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[MaHaiQuanTiepNhan],
	[NamTiepNhan],
	[NgayTiepNhan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[TrangThaiThanhKhoan],
	[SoHoSo],
	[NgayBatDau],
	[NgayKetThuc],
	[LanThanhLy],
	[SoQuyetDinh],
	[NgayQuyetDinh],
	[UserName],
	[GuidStr],
	[ListTKNK_Hoan],
	[ListTKNK_KhongThu]
FROM
	[dbo].[t_KDT_SXXK_HoSoThanhLyDangKy]
WHERE
	[ID] = @ID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[t_KDT_ContainerBS]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[t_KDT_ContainerBS] ADD
[CustomsSeal] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerBS_Insert]'
GO
SET QUOTED_IDENTIFIER OFF
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ContainerBS_Insert]
	@Master_id bigint,
	@SoVanDon varchar(100),
	@SoContainer varchar(100),
	@SoSeal varchar(100),
	@GhiChu nvarchar(255),
	@Code nvarchar(max),
	@KVGS int,
	@CustomsSeal varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ContainerBS]
(
	[Master_id],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChu],
	[Code],
	[KVGS],
	[CustomsSeal]
)
VALUES 
(
	@Master_id,
	@SoVanDon,
	@SoContainer,
	@SoSeal,
	@GhiChu,
	@Code,
	@KVGS,
	@CustomsSeal
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerBS_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ContainerBS_Update]
	@ID bigint,
	@Master_id bigint,
	@SoVanDon varchar(100),
	@SoContainer varchar(100),
	@SoSeal varchar(100),
	@GhiChu nvarchar(255),
	@Code nvarchar(max),
	@KVGS int,
	@CustomsSeal varchar(100)
AS

UPDATE
	[dbo].[t_KDT_ContainerBS]
SET
	[Master_id] = @Master_id,
	[SoVanDon] = @SoVanDon,
	[SoContainer] = @SoContainer,
	[SoSeal] = @SoSeal,
	[GhiChu] = @GhiChu,
	[Code] = @Code,
	[KVGS] = @KVGS,
	[CustomsSeal] = @CustomsSeal
WHERE
	[ID] = @ID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerBS_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ContainerBS_InsertUpdate]
	@ID bigint,
	@Master_id bigint,
	@SoVanDon varchar(100),
	@SoContainer varchar(100),
	@SoSeal varchar(100),
	@GhiChu nvarchar(255),
	@Code nvarchar(max),
	@KVGS int,
	@CustomsSeal varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ContainerBS] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ContainerBS] 
		SET
			[Master_id] = @Master_id,
			[SoVanDon] = @SoVanDon,
			[SoContainer] = @SoContainer,
			[SoSeal] = @SoSeal,
			[GhiChu] = @GhiChu,
			[Code] = @Code,
			[KVGS] = @KVGS,
			[CustomsSeal] = @CustomsSeal
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ContainerBS]
		(
			[Master_id],
			[SoVanDon],
			[SoContainer],
			[SoSeal],
			[GhiChu],
			[Code],
			[KVGS],
			[CustomsSeal]
		)
		VALUES 
		(
			@Master_id,
			@SoVanDon,
			@SoContainer,
			@SoSeal,
			@GhiChu,
			@Code,
			@KVGS,
			@CustomsSeal
		)		
	END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerBS_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ContainerBS_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChu],
	[Code],
	[KVGS],
	[CustomsSeal]
FROM
	[dbo].[t_KDT_ContainerBS]
WHERE
	[ID] = @ID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerBS_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ContainerBS_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChu],
	[Code],
	[KVGS],
	[CustomsSeal]
FROM
	[dbo].[t_KDT_ContainerBS]	

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_SelectAll]'
GO
SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_SelectAll]
-- Database: ECS_TQDT_SXXK_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 03, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[MaHaiQuanTiepNhan],
	[NamTiepNhan],
	[NgayTiepNhan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[TrangThaiThanhKhoan],
	[SoHoSo],
	[NgayBatDau],
	[NgayKetThuc],
	[LanThanhLy],
	[SoQuyetDinh],
	[NgayQuyetDinh],
	[UserName],
	[GuidStr],
	[ListTKNK_Hoan],
	[ListTKNK_KhongThu]
FROM
	[dbo].[t_KDT_SXXK_HoSoThanhLyDangKy]	

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_SXXK_BKToKhaiNhap_TT38]'
GO




      
      
------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_SXXK_BKToKhaiNhap_TT38]      
-- Database: ECS_TQDT_SXXK      
-- Author: ecs 
-- Alter: minhnd 04/05/2015     
-- Time created: Friday, October 07, 2011      
------------------------------------------------------------------------------------------------------------------------      
      create PROCEDURE [dbo].[p_SXXK_BKToKhaiNhap_TT38]      
   @LanThanhLy bigint      
  ,@NamThanhLy int      
  ,@MaDoanhNghiep varchar(50)      
  ,@Distinct int
AS      
      
BEGIN      
       
       
 if OBJECT_ID('tempdb..#BKToKhaiNhap_TT38') is not null drop table #BKToKhaiNhap_TT38      
      
CREATE TABLE #BKToKhaiNhap_TT38      
(      
 STT BIGINT      
 ,SoToKhaiNhap NVARCHAR(255)
 ,NgayDangKyNhap NVARCHAR(255)      
 ,TenNPL NVARCHAR(1000)      
 ,DVT NVARCHAR(30)      
 ,LuongNhapTK DECIMAL(18,5)      
 ,LuongNhapTonDauKy DECIMAL(18,5)      
 ,LuongNPLSuDung DECIMAL(18,5)
 ,LuongTonCuoi DECIMAL(18,5)
 ,ThueDongHang DECIMAL(18,8)      
 ,ThueTonDauKy DECIMAL(18,8)
 ,ThueKhongThu DECIMAL(18,8)
 ,ThueTheoDoiTiep DECIMAL(18,8) 
 ,SoCTTT_N     NVARCHAR(255)      
 ,SoHopDongNhap NVARCHAR(255)      
 
       
)      
   
        
INSERT INTO #BKToKhaiNhap_TT38   
(      
 STT
 ,SoToKhaiNhap 
 ,NgayDangKyNhap
 ,TenNPL 
 ,DVT 
 ,LuongNhapTK
 ,LuongNhapTonDauKy
 ,LuongNPLSuDung 
 ,LuongTonCuoi 
 ,ThueDongHang 
 ,ThueTonDauKy 
 ,ThueKhongThu 
 ,ThueTheoDoiTiep 
 ,SoCTTT_N
 ,SoHopDongNhap 
       
)      
--select  
SELECT  
( ROW_NUMBER() OVER ( ORDER BY  SoToKhaiNhap) ) AS STT,
e.SoToKhaiNhap ,
        e.NgayDangKyNhap ,
        e.TenNPL ,
        e.DVT ,
        MAX(e.LuongNhapTK) AS LuongNhapTK ,
        MAX(e.LuongTonDauKy) AS LuongTonDauKy ,
        SUM(e.LuongNPLSuDung) AS LuongNPLSuDung ,
        MAX(e.LuongTonDauKy) - SUM(e.LuongNPLSuDung) AS LuongTonCuoi ,
        MAX(e.ThueDongHang) AS ThueDongHang ,
        MAX(e.ThueTonDauKy) AS ThueTonDauKy ,
        --SUM(e.ThueKhongThu) AS ThueKhongThu ,
        --SUM(e.LuongNPLSuDung)*(MAX(e.ThueTonDauKy)/MAX(e.LuongTonDauKy)) as ThueKhongThu,
        CASE WHEN SUM(e.LuongNPLSuDung)= MAX(e.LuongTonDauKy) THEN MAX(e.ThueTonDauKy) ELSE ROUND(SUM(e.LuongNPLSuDung)*(MAX(e.ThueDongHang)/MAX(e.Luong)),0) END AS  ThueKhongThu,
        --MAX(e.ThueTonDauKy) - (SUM(e.LuongNPLSuDung)*(MAX(e.ThueTonDauKy)/MAX(e.LuongTonDauKy))) AS ThueTheoDoiTiep ,
        MAX(e.ThueTonDauKy) - (CASE WHEN SUM(e.LuongNPLSuDung)= MAX(e.LuongTonDauKy) THEN MAX(e.ThueTonDauKy) ELSE ROUND(SUM(e.LuongNPLSuDung)*(MAX(e.ThueDongHang)/MAX(e.Luong)),0) END) AS ThueTheoDoiTiep ,
        CASE WHEN MAX(e.NgayDangKyNhapFull) IS NOT NULL THEN (SELECT TOP 1 SoNgayChungTu FROM dbo.t_KDT_SXXK_ChungTuThanhToan WHERE NgayDangKyXuat = MAX(e.NgayDangKyNhapFull) and LanThanhLy = @LanThanhLy) ELSE '' END AS SoCTTT_N,
        MAX(e.SoHopDongNhap) AS SoHopDongNhap
FROM    (
--select  
          SELECT    ( ROW_NUMBER() OVER ( ORDER BY SoToKhaiNhap) ) AS STT ,
        --SoToKhaiNhap AS So ,
                    ( CONVERT(NVARCHAR(12), CASE WHEN MaLoaiHinhNhap LIKE '%V%'
                                                 THEN ( SELECT
                                                              SoTKVNACCS
                                                        FROM  t_vnaccs_capsotokhai
                                                        WHERE SoTK = SoToKhaiNhap
                                                      )
                                                 ELSE SoToKhaiNhap
                                            END) + '/'
                      + ( CASE WHEN MaLoaiHinhNhap LIKE '%V%'
                               THEN ( SELECT    Ten_VT
                                      FROM      t_HaiQuan_LoaiHinhMauDich
                                      WHERE     ID = MaLoaiHinhNhap
                                    )
                               ELSE MaLoaiHinhNhap
                          END ) ) AS SoToKhaiNhap ,
                    CONVERT(NVARCHAR(20), NgayDangKyNhap, 103) AS NgayDangKyNhap ,
                    NgayDangKyNhap AS NgayDangKyNhapFull,
                    ( CONVERT(NVARCHAR(2), ( CASE WHEN t.MaLoaiHinhNhap LIKE '%V%'
                                                  THEN ( SELECT TOP 1
                                                              SoThuTuHang
                                                         FROM dbo.t_SXXK_hangmaudich
                                                         WHERE
                                                              TenHang = TenNPL
                                                              AND t_SXXK_hangmaudich.MaLoaiHinh = t.MaLoaiHinhNhap
                                                              AND t_SXXK_hangmaudich.SoToKhai = t.SoToKhaiNhap
                                                              AND t_SXXK_hangmaudich.MaPhu = t.MaNPL
                                                       )
                                                  ELSE ( SELECT TOP 1
                                                              SoThuTuHang
                                                         FROM dbo.t_SXXK_hangmaudich
                                                         WHERE
                                                              TenHang = TenNPL
                                                              AND t_SXXK_hangmaudich.MaLoaiHinh = t.MaLoaiHinhNhap
                                                              AND t_SXXK_hangmaudich.SoToKhai = t.SoToKhaiNhap
                                                              AND t_SXXK_hangmaudich.MaPhu = t.MaNPL
                                                       )
                                             END ), 103) + ' / '
                      + MIN([TenNPL]) + ' / ' + UPPER([MaNPL]) ) AS TenNPL ,
                    MAX(TenDVT_NPL) AS DVT ,
                    CASE WHEN t.MaLoaiHinhNhap LIKE '%V%'
                         THEN ( SELECT TOP 1
                                        SoLuong
                                FROM    t_sxxk_HangMauDich
                                WHERE   TenHang = TenNPL
                                        AND t_SXXK_hangmaudich.MaLoaiHinh = t.MaLoaiHinhNhap
                                        AND t_SXXK_hangmaudich.SoToKhai = t.SoToKhaiNhap
                                        AND t_SXXK_hangmaudich.MaPhu = t.MaNPL
                              )
                         ELSE ( SELECT TOP 1
                                        SoLuong
                                FROM    t_sxxk_HangMauDich
                                WHERE   TenHang = TenNPL
                                        AND t_SXXK_hangmaudich.MaLoaiHinh = t.MaLoaiHinhNhap
                                        AND t_SXXK_hangmaudich.SoToKhai = t.SoToKhaiNhap
                                        AND t_SXXK_hangmaudich.MaPhu = t.MaNPL
                              )
                    END AS LuongNhapTK ,
                    MAX(LuongNhap) AS LuongTonDauKy ,
                    MAX(Luong) AS Luong ,
        --CASE WHEN SUM(LuongNPLSuDung)>MAX(LuongNhap) THEN MAX(LuongNhap) ELSE SUM(LuongNPLSuDung) END  AS LuongNPLSuDung ,
                    CASE WHEN LuongNPLSuDung + LuongNPLTon > Luong
                         THEN Luong - LuongNPLTon
                         ELSE LuongNPLSuDung
                    END AS LuongNPLSuDung ,
        --MAX(LuongNhap) - (CASE WHEN SUM(LuongNPLSuDung)>MAX(LuongNhap) THEN MAX(LuongNhap) ELSE SUM(LuongNPLSuDung) END) AS LuongTonCuoi
                    MAX(LuongNhap)
                    - ( CASE WHEN LuongNPLSuDung + LuongNPLTon > Luong
                             THEN Luong - LuongNPLTon
                             ELSE LuongNPLSuDung
                        END ) AS LuongTonCuoi              
                        
                        --,MAX(TyGiaTT) as TyGiaTT
                        --,MAX(DonGiaTT) as DonGiaTT
                        --,MAX(ThueSuat) as ThueSuat 
                    ,
                    MAX(ThueXNK) AS ThueDongHang ,
                    MAX(ThueNKNop) AS ThueTonDauKy ,
                    --CASE WHEN ( CASE WHEN thuesuat >= 0
                    --                 THEN ROUND(( SUM(CASE WHEN LuongNPLSuDung
                    --                                          + LuongNPLTon > Luong
                    --                                       THEN Luong
                    --                                          - LuongNPLTon
                    --                                       ELSE LuongNPLSuDung
                    --                                  END) * DonGiaTT
                    --                              * ( ThueSuat / 100 ) ), 0)
                    --                 ELSE ROUND(SUM(CASE WHEN LuongNPLSuDung
                    --                                          + LuongNPLTon > Luong
                    --                                     THEN Luong
                    --                                          - LuongNPLTon
                    --                                     ELSE LuongNPLSuDung
                    --                                END) * DonGiaTT, 0)
                    --            END ) > MAX(ThueNKNop) THEN MAX(ThueNKNop)
                    --     ELSE ( CASE WHEN thuesuat >= 0
                    --                 THEN ROUND(( SUM(CASE WHEN LuongNPLSuDung
                    --                                          + LuongNPLTon > Luong
                    --                                       THEN Luong
                    --                                          - LuongNPLTon
                    --                                       ELSE LuongNPLSuDung
                    --                                  END) * DonGiaTT
                    --                              * ( ThueSuat / 100 ) ), 0)
                    --                 ELSE ROUND(SUM(CASE WHEN LuongNPLSuDung
                    --                                          + LuongNPLTon > Luong
                    --                                     THEN Luong
                    --                                          - LuongNPLTon
                    --                                     ELSE LuongNPLSuDung
                    --                                END) * DonGiaTT, 0)
                    --            END )
                    --END AS ThueKhongThu ,
                    --( MAX(ThueXNK) / MAX(Luong) ) AS la ,
                    --DonGiaTT AS DonGiaTT ,
                    --CASE WHEN thuesuat >= 0
                    --     THEN ( CASE WHEN ( MAX(ThueNKNop)
                    --                        - ROUND(( SUM(LuongNPLSuDung)
                    --                                  * DonGiaTT * ( ThueSuat
                    --                                          / 100 ) ), 0) ) < 0
                    --                 THEN 0
                    --                 ELSE MAX(ThueNKNop)
                    --                      - ROUND(( SUM(LuongNPLSuDung)
                    --                                * DonGiaTT * ( ThueSuat
                    --                                          / 100 ) ), 0)
                    --            END )
                    --     ELSE ( CASE WHEN ( MAX(ThueNKNop)
                    --                        - ROUND(SUM(LuongNPLSuDung)
                    --                                * DonGiaTT, 0) ) < 0
                    --                 THEN 0
                    --                 ELSE MAX(ThueNKNop)
                    --                      - ROUND(SUM(LuongNPLSuDung)
                    --                              * DonGiaTT, 0)
                    --            END )
                    --END AS ThueTheoDoiTiep ,
                    0 AS SoCTTT_N ,
                    CASE WHEN t.MaLoaiHinhNhap LIKE '%V%'
                         THEN ( SELECT TOP 1
                                        GhiChu
                                FROM    t_KDT_VNACC_ToKhaiMauDich
                                WHERE   SoToKhai = ( SELECT TOP 1
                                                            SoTKVNACCS
                                                     FROM   t_VNACCS_CapSoToKhai
                                                     WHERE  SoTK = t.SoToKhaiNhap
                                                   )
                              )
                         ELSE ( SELECT TOP 1
                                        SoHopDong
                                FROM    t_SXXK_ToKhaiMauDich
                                WHERE   SoToKhai = t.SoToKhaiNhap
                              )
                    END AS SoHopDongNhap
          FROM      t_KDT_SXXK_BCThueXNK AS t
--minhnd 04/05/2015
          WHERE     LanThanhLy = @LanThanhLy and MaDoanhNghiep = @MaDoanhNghiep  and NamThanhLy = @NamThanhLy   
GROUP BY            SoToKhaiNhap ,
                    NgayDangKyNhap ,
                    MaLoaiHinhNhap ,
                    LuongNhap ,
                    TenNPL ,
                    MaNPL --,
                    ,
                    Luong ,
                    LuongNPLSuDung ,
                    LuongNPLTon ,
                    t.DonGiaTT
                        --ThueNKNop,
                        --tygiatt,
                        --,thuexnk
                    ,
                    thuesuat
    
    ---end select  
          
        ) AS e
    --SELECT * FROM dbo.t_KDT_SXXK_BCThueXNK WHERE LanThanhLy  = 277
GROUP BY e.SoToKhaiNhap ,
        e.NgayDangKyNhap ,
        e.TenNPL ,
        e.DVT
ORDER BY SoToKhaiNhap    
    ---end select  
  END      
      
-- UPDATE #BKToKhaiNhap_TT38    
--SET      
-- LuongNPLSuDung =LuongNhap   where    LuongNPLSuDung>LuongNhap
--UPDATE #BKToKhaiNhap_TT38     
--SET      
-- TienThueHoan = ROUND((LuongNPLSuDung/LuongNhap) * ThueNKNop,0)   --where    LuongNPLSuDung<=LuongNhap
 
--UPDATE #BKToKhaiNhap_TT38    
--SET       
-- ThuePhaiThu =  ThueNKNop - TienThueHoan      
      
if @Distinct=1
	      SELECT distinct SoToKhaiNhap FROM #BKToKhaiNhap_TT38      
SELECT * FROM #BKToKhaiNhap_TT38      
      
DROP TABLE #BKToKhaiNhap_TT38    
       
--END      




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[p_SXXK_BKToKhaiNhapXinHoan_TT38]'
GO
CREATE proc [dbo].[p_SXXK_BKToKhaiNhapXinHoan_TT38]
@LanTL BIGINT      
  ,@NamTL INT      
  ,@MaDN VARCHAR(50)
  ,@Distinct1 int 
  ,@ListTKN nvarchar(4000)
as
begin

CREATE TABLE #BKToKhaiNhapXinHoan_TT38      
(      
 STT BIGINT      
 ,SoToKhaiNhap NVARCHAR(255)
 ,NgayDangKyNhap NVARCHAR(255)      
 ,TenNPL NVARCHAR(1000)      
 ,DVT NVARCHAR(30)      
 ,LuongNhapTK DECIMAL(18,5)      
 ,LuongNhapTonDauKy DECIMAL(18,5)      
 ,LuongNPLSuDung DECIMAL(18,5)
 ,LuongTonCuoi DECIMAL(18,5)
 ,ThueDongHang DECIMAL(18,8)      
 ,ThueTonDauKy DECIMAL(18,8)
 ,ThueKhongThu DECIMAL(18,8)
 ,ThueTheoDoiTiep DECIMAL(18,8) 
 ,SoCTTT_N     NVARCHAR(255)      
 ,SoHopDongNhap NVARCHAR(255)      
 
       
) 
INSERT INTO #BKToKhaiNhapXinHoan_TT38   
(      
 STT
 ,SoToKhaiNhap 
 ,NgayDangKyNhap
 ,TenNPL 
 ,DVT 
 ,LuongNhapTK
 ,LuongNhapTonDauKy
 ,LuongNPLSuDung 
 ,LuongTonCuoi 
 ,ThueDongHang 
 ,ThueTonDauKy 
 ,ThueKhongThu 
 ,ThueTheoDoiTiep 
 ,SoCTTT_N
 ,SoHopDongNhap 
       
) 
exec [dbo].[p_SXXK_BKToKhaiNhap_TT38] @LanThanhLy = @LanTL, @NamThanhLy = @NamTL, @MaDoanhNghiep = @MaDN, @Distinct = @Distinct1

declare @SQL nvarchar(4000)

set @SQL = 'select * from #BKToKhaiNhapXinHoan_TT38 Where ' + @ListTKN
EXEC sp_executesql @SQL

--select * from #BKToKhaiNhapXinHoan_TT38 where SoToKhaiNhap in (@ListTKN)
drop table #BKToKhaiNhapXinHoan_TT38
end

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[t_KDT_SXXK_DinhMucMaHang]'
GO
CREATE TABLE [dbo].[t_KDT_SXXK_DinhMucMaHang]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[Master_ID] [bigint] NOT NULL,
[TenKH] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PO] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Style] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MatHang] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoLuongPO] [bigint] NULL,
[SLXuat] [numeric] (16, 4) NULL,
[ChenhLech] [numeric] (16, 4) NULL,
[MaNPL] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenNPL] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Art] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_NPL] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NCC] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPTieuThu] [numeric] (16, 4) NULL,
[DinhMuc] [numeric] (18, 10) NULL,
[TyLeHaoHut] [numeric] (18, 5) NULL,
[DinhMuc_HaoHut] [numeric] (18, 10) NULL,
[NhuCauXuat] [numeric] (16, 4) NULL,
[XuatKho] [numeric] (16, 4) NULL,
[ThuHoi] [numeric] (16, 4) NULL,
[ThucXuatKho] [numeric] (16, 4) NULL,
[DinhMucThucTe] [numeric] (18, 10) NULL,
[SoTKN_VNACCS] [decimal] (15, 0) NULL,
[Invoid_HD] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayChungTu] [datetime] NULL,
[SoLuongNhapKho] [numeric] (16, 4) NOT NULL,
[SoLuongSuDung] [numeric] (16, 4) NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_t_KDT_SXXK_DinhMucMaHangDangKy] on [dbo].[t_KDT_SXXK_DinhMucMaHang]'
GO
ALTER TABLE [dbo].[t_KDT_SXXK_DinhMucMaHang] ADD CONSTRAINT [PK_t_KDT_SXXK_DinhMucMaHangDangKy] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerBS_SelectDynamic]'
GO
SET QUOTED_IDENTIFIER OFF
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ContainerBS_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_id],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChu],
	[Code],
	[KVGS],
	[CustomsSeal]
FROM [dbo].[t_KDT_ContainerBS] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_SelectDynamic]'
GO
SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 03, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[MaHaiQuanTiepNhan],
	[NamTiepNhan],
	[NgayTiepNhan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[TrangThaiThanhKhoan],
	[SoHoSo],
	[NgayBatDau],
	[NgayKetThuc],
	[LanThanhLy],
	[SoQuyetDinh],
	[NgayQuyetDinh],
	[UserName],
	[GuidStr],
	[ListTKNK_Hoan],
	[ListTKNK_KhongThu]
FROM [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
----------------------------------------------------
--USE [ECS_TQDT_SXXK_V4_HOA_THO_28_07_2015]
GO
/****** Object:  StoredProcedure [dbo].[p_SXXK_BKToKhaiXuat_TT38]    Script Date: 17/09/2015 9:37:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



      
      
------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_SXXK_BKToKhaiXuat_TT38]      
-- Database: ECS_TQDT_SXXK      
-- Author: minhnd 
-- Alter: minhnd 
-- Time created: 06/05/2015      
------------------------------------------------------------------------------------------------------------------------      
      CREATE PROCEDURE [dbo].[p_SXXK_BKToKhaiXuat_TT38]      
   @LanThanhLy bigint      
  ,@NamThanhLy int      
  ,@MaDoanhNghiep varchar(50)      
  
AS      
      
BEGIN      
       
       
SELECT  ( ROW_NUMBER() OVER ( ORDER BY NgayDangKyXuat, SoToKhaiXuat ) ) AS STT ,
        CONVERT(NVARCHAR(30), CASE WHEN MaLoaiHinhXuat LIKE '%V%'
                                   THEN ( SELECT TOP 1
                                                    SoTKVNACCS
                                          FROM      dbo.t_VNACCS_CapSoToKhai
                                          WHERE     SoTK = t_KDT_SXXK_BCThueXNK.SoToKhaiXuat
                                        )
                                   ELSE t_KDT_SXXK_BCThueXNK.SoToKhaiXuat
                              END, 103) + '/'
        + CASE WHEN MaLoaiHinhXuat LIKE '%V%'
               THEN SUBSTRING(MaLoaiHinhXuat, 3, 3)
               ELSE MaLoaiHinhXuat
          END AS SoToKhaiXuat ,
--CASE WHEN MaLoaiHinhXuat LIKE '%V%' THEN SUBSTRING(MaLoaiHinhXuat,3,3) ELSE MaLoaiHinhXuat END AS MaLoaiHinh,
--CASE WHEN MaLoaiHinhXuat LIKE '%V%' THEN (SELECT TOP 1 SoTKVNACCS FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTK = t_KDT_SXXK_BCThueXNK.SoToKhaiXuat) ELSE t_KDT_SXXK_BCThueXNK.SoToKhaiXuat END SoToKhaiXuat,
        CONVERT(NVARCHAR(12), NgayDangKyXuat, 103) AS NgayDangKyXuat ,
        CONVERT(NVARCHAR(12), NgayThucXuat, 103) AS NgayThucXuat ,
        CASE WHEN MaLoaiHinhXuat LIKE '%V%'
             THEN ( SELECT TOP 1
                            SoHopDong
                    FROM    dbo.t_KDT_ToKhaiMauDich
                    WHERE   SoToKhai = SoToKhaiXuat
                  )
             ELSE ''
        END AS SoNgayHopDong ,
        CASE WHEN MaLoaiHinhXuat LIKE '%V%'
             THEN ( SELECT TOP 1
                            SoNgayChungTu
                    FROM    t_KDT_SXXK_ChungTuThanhToan
                    WHERE   NgayDangKyXuat = t_KDT_SXXK_BCThueXNK.NgayDangKyXuat
                            AND LanThanhLy = @LanThanhLy
                  )
             ELSE ''
        END AS SoCTTT_X ,
        '' AS NgayCTTT,
        CASE WHEN MaLoaiHinhXuat LIKE '%V%' THEN (SELECT TOP 1 GhiChu FROM dbo.t_KDT_SXXK_ChungTuThanhToan WHERE   NgayDangKyXuat = t_KDT_SXXK_BCThueXNK.NgayDangKyXuat ) ELSE '' END  AS GhiChu
FROM    dbo.t_KDT_SXXK_BCThueXNK
WHERE   LanThanhLy = @LanThanhLy AND NamThanhLy = @NamThanhLy AND MaDoanhNghiep = @MaDoanhNghiep
        AND SoToKhaiXuat <> 0
GROUP BY SoToKhaiXuat ,
        dbo.t_KDT_SXXK_BCThueXNK.NgayDangKyXuat ,
        MaLoaiHinhXuat ,
        NgayThucXuat
--SELECT * FROM t_KDT_SXXK_ChungTuThanhToan WHERE LanThanhLy = 277

END      


GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '31.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('31.8',GETDATE(), N'Tờ khai nhập/xuất tt38 and news')
END	

