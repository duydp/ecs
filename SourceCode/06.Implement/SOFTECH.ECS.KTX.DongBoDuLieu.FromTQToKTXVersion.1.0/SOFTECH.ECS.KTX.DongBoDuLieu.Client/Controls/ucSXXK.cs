﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DongBoDuLieu.BLL.SXXK;
using Janus.Windows.GridEX;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
namespace DongBo.Controls
{
    public partial class ucSXXK : ucDesign
    {
        frmMain frmMain = null;

        public ucSXXK()
        {
            InitializeComponent();
            frmMain = (frmMain)Application.OpenForms["frmMain"];
        }

        #region
        /*
        private void getDSNPL()
        {
            DataTable dt = NPL.GetALLDanhSachNPL(GlobalSettings.MaHQ, GlobalSettings.MaDN, "", false).Tables[0];
            dgListNPLHQ.DataSource = dt;
            dgListNPLHQ.RetrieveStructure();

            dgListNPLHQ.Tables[0].Columns["MaHaiQuan"].Caption = "Mã HQ";
            dgListNPLHQ.Tables[0].Columns["MaHaiQuan"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["Ma"].Caption = "Mã NPL";
            dgListNPLHQ.Tables[0].Columns["Ma"].Width = 100;
            dgListNPLHQ.Tables[0].Columns["Ten"].Caption = "Tên Nguyên Phụ Liệu";
            dgListNPLHQ.Tables[0].Columns["Ten"].Width = 200;
            dgListNPLHQ.Tables[0].Columns["MaHS"].Caption = "Mã HS";
            dgListNPLHQ.Tables[0].Columns["MaHS"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["DVT_ID"].Caption = "ĐVT";
            dgListNPLHQ.Tables[0].Columns["DVT_ID"].Width = 40;
            GridEXColumn colSelected = new GridEXColumn("Selected");
            colSelected.ActAsSelector = true;
            colSelected.Width = 20;
            dgListNPLHQ.Tables[0].Columns.Insert(0, colSelected);
        }

        private void getDSSP()
        {
            DataTable dt = SP.GetALLDanhSachSP(GlobalSettings.MaHQ, GlobalSettings.MaDN, "", false).Tables[0];
            dgListNPLHQ.DataSource = dt;
            dgListNPLHQ.RetrieveStructure();

            dgListNPLHQ.Tables[0].Columns["MaHaiQuan"].Caption = "Mã HQ";
            dgListNPLHQ.Tables[0].Columns["MaHaiQuan"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["Ma"].Caption = "Mã SP";
            dgListNPLHQ.Tables[0].Columns["Ma"].Width = 100;
            dgListNPLHQ.Tables[0].Columns["Ten"].Caption = "Tên Sản Phẩm";
            dgListNPLHQ.Tables[0].Columns["Ten"].Width = 200;
            dgListNPLHQ.Tables[0].Columns["MaHS"].Caption = "Mã HS";
            dgListNPLHQ.Tables[0].Columns["MaHS"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["DVT_ID"].Caption = "ĐVT";
            dgListNPLHQ.Tables[0].Columns["DVT_ID"].Width = 40;
            GridEXColumn colSelected = new GridEXColumn("Selected");
            colSelected.ActAsSelector = true;
            colSelected.Width = 20;
            dgListNPLHQ.Tables[0].Columns.Insert(0, colSelected);
        }

        private void getDSDM()
        {
            DataTable dt = DM.GetALLDanhSachDM(GlobalSettings.MaHQ, GlobalSettings.MaDN, false, "").Tables[0];
            dgListNPLHQ.DataSource = dt;
            dgListNPLHQ.RetrieveStructure();

            dgListNPLHQ.Tables[0].Columns["Ma_HQ"].Caption = "Mã HQ";
            dgListNPLHQ.Tables[0].Columns["Ma_HQ"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["Ma_DV"].Caption = "Mã Đơn Vị";
            dgListNPLHQ.Tables[0].Columns["Ma_DV"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["Ma_SP"].Caption = "Mã SP";
            dgListNPLHQ.Tables[0].Columns["Ma_SP"].Width = 100;
            dgListNPLHQ.Tables[0].Columns["Ma_NPL"].Caption = "Mã NPL";
            dgListNPLHQ.Tables[0].Columns["Ma_NPL"].Width = 100;
            dgListNPLHQ.Tables[0].Columns["DM_SD"].Caption = "ĐM SD";
            dgListNPLHQ.Tables[0].Columns["DM_SD"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["TL_HH"].Caption = "Tỷ lệ HH";
            dgListNPLHQ.Tables[0].Columns["TL_HH"].Width = 60;
            dgListNPLHQ.Tables[0].Columns["DM_CHUNG"].Caption = "ĐM Chung";
            dgListNPLHQ.Tables[0].Columns["DM_CHUNG"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["GHI_CHU"].Caption = "Ghi Chú";
            dgListNPLHQ.Tables[0].Columns["GHI_CHU"].Width = 150;
            GridEXColumn colSelected = new GridEXColumn("Selected");
            colSelected.ActAsSelector = true;
            colSelected.Width = 20;
            dgListNPLHQ.Tables[0].Columns.Insert(0, colSelected);
        }

        private void getDSTK()
        {
            DataTable dt = TK.GetALLDanhSachToKhai(GlobalSettings.MaHQ, GlobalSettings.MaDN, false, GlobalSettings.NgayBatDau.Year, "").Tables[0];
            dgListNPLHQ.DataSource = dt;
            dgListNPLHQ.RetrieveStructure();

            dgListNPLHQ.Tables[0].Columns["SoTK"].Caption = "Số TK";
            dgListNPLHQ.Tables[0].Columns["SoTK"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["Ma_LH"].Caption = "Mã LH";
            dgListNPLHQ.Tables[0].Columns["Ma_LH"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["Ma_HQ"].Caption = "Mã HQ";
            dgListNPLHQ.Tables[0].Columns["Ma_HQ"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["NamDK"].Caption = "Năm ĐK";
            dgListNPLHQ.Tables[0].Columns["NamDK"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["Ngay_DK"].Caption = "Ngày ĐK";
            dgListNPLHQ.Tables[0].Columns["Ngay_DK"].Width = 70;
            dgListNPLHQ.Tables[0].Columns["Ma_DV"].Caption = "Mã Đơn Vị";
            dgListNPLHQ.Tables[0].Columns["Ma_DV"].Width = 90;
            dgListNPLHQ.Tables[0].Columns["Ma_DVUT"].Caption = "Mã ĐV Ủy Thác";
            dgListNPLHQ.Tables[0].Columns["Ma_DVUT"].Width = 90;
            dgListNPLHQ.Tables[0].Columns["DV_DT"].Caption = "ĐV Đối tác";
            dgListNPLHQ.Tables[0].Columns["DV_DT"].Width = 150;
            dgListNPLHQ.Tables[0].Columns["Ma_PTVT"].Caption = "Mã PTVT";
            dgListNPLHQ.Tables[0].Columns["Ma_PTVT"].Width = 60;
            dgListNPLHQ.Tables[0].Columns["Ten_PTVT"].Caption = "Tên PTVT";
            dgListNPLHQ.Tables[0].Columns["Ten_PTVT"].Width = 70;
            //dgList.Tables[0].Columns["NgayKH"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["NgayKH"].Visible = false;
            dgListNPLHQ.Tables[0].Columns["NgayDen"].Caption = "Ngay đến PTVT";
            dgListNPLHQ.Tables[0].Columns["NgayDen"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["Van_Don"].Caption = "Số Vận Đơn";
            dgListNPLHQ.Tables[0].Columns["Van_Don"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["Ma_CK"].Caption = "Mã Cửa Khẩu";
            dgListNPLHQ.Tables[0].Columns["Ma_CK"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["CangNN"].Caption = "Cảng NK";
            dgListNPLHQ.Tables[0].Columns["CangNN"].Width = 100;
            dgListNPLHQ.Tables[0].Columns["Ngay_GP"].Caption = "Ngày Giấy Phép";
            dgListNPLHQ.Tables[0].Columns["Ngay_GP"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["So_GP"].Caption = "Số Giấy Phép";
            dgListNPLHQ.Tables[0].Columns["So_GP"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["Ngay_HHGP"].Caption = "Ngày HH Giấy Phép";
            dgListNPLHQ.Tables[0].Columns["Ngay_HHGP"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["So_HD"].Caption = "Số HĐ";
            dgListNPLHQ.Tables[0].Columns["So_HD"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["TyGia_USD"].Caption = "T/g USD";
            dgListNPLHQ.Tables[0].Columns["TyGia_USD"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["Ngay_HD"].Caption = "Ngày HĐ";
            dgListNPLHQ.Tables[0].Columns["Ngay_HD"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["Ngay_HHHD"].Caption = "Ngày HH HĐ";
            dgListNPLHQ.Tables[0].Columns["Ngay_HHHD"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["Nuoc_XK"].Caption = "Nước XK";
            dgListNPLHQ.Tables[0].Columns["Nuoc_XK"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["Nuoc_NK"].Caption = "Nước NK";
            dgListNPLHQ.Tables[0].Columns["Nuoc_NK"].Width = 80;

            dgListNPLHQ.Tables[0].Columns["Ma_GH"].Caption = "Mã Giao Hàng";
            dgListNPLHQ.Tables[0].Columns["Ma_GH"].Width = 60;
            dgListNPLHQ.Tables[0].Columns["SoHang"].Caption = "Số Hàng";
            dgListNPLHQ.Tables[0].Columns["SoHang"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["Ma_PTTT"].Caption = "Mã PTTT";
            dgListNPLHQ.Tables[0].Columns["Ma_PTTT"].Width = 70;
            dgListNPLHQ.Tables[0].Columns["Ma_NT"].Caption = "Mã Nguyên Tệ";
            dgListNPLHQ.Tables[0].Columns["Ma_NT"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["TyGia_VND"].Caption = "T/g VNĐ";
            dgListNPLHQ.Tables[0].Columns["TyGia_VND"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["LePhi_HQ"].Caption = "Lệ Phí HQ";
            dgListNPLHQ.Tables[0].Columns["LePhi_HQ"].Width = 70;
            //dgList.Tables[0].Columns["BL_LPHQ"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["BL_LPHQ"].Visible = false;
            dgListNPLHQ.Tables[0].Columns["GiayTo"].Caption = "Chứng từ";
            dgListNPLHQ.Tables[0].Columns["GiayTo"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["TenCH"].Caption = "Tên Chủ Hàng";
            dgListNPLHQ.Tables[0].Columns["TenCH"].Width = 100;
            //dgList.Tables[0].Columns["Ngay_KB"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["Ngay_KB"].Visible = false;
            //dgList.Tables[0].Columns["User_DK"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["User_DK"].Visible = false;

            //dgList.Tables[0].Columns["DDiem_KH"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["DDiem_KH"].Visible = false;
            //dgList.Tables[0].Columns["PThuc_KH"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["PThuc_KH"].Visible = false;
            //dgList.Tables[0].Columns["Ngay_KH"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["Ngay_KH"].Visible = false;
            //dgList.Tables[0].Columns["User_KH1"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["User_KH1"].Visible = false;
            //dgList.Tables[0].Columns["User_KH2"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["User_KH2"].Visible = false;
            //dgList.Tables[0].Columns["User_PCKH"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["User_PCKH"].Visible = false;
            dgListNPLHQ.Tables[0].Columns["Phi_BH"].Caption = "Phí Bảo Hiểm";
            dgListNPLHQ.Tables[0].Columns["Phi_BH"].Width = 60;
            dgListNPLHQ.Tables[0].Columns["Phi_VC"].Caption = "Phí Vận Chuyển";
            dgListNPLHQ.Tables[0].Columns["Phi_VC"].Width = 60;
            dgListNPLHQ.Tables[0].Columns["Ngay_TH"].Caption = "Ngày TH";
            dgListNPLHQ.Tables[0].Columns["Ngay_TH"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["User_TH"].Caption = "Người TH";
            dgListNPLHQ.Tables[0].Columns["User_TH"].Width = 60;
            dgListNPLHQ.Tables[0].Columns["TongTGKB"].Caption = "Tổng TGKB";
            dgListNPLHQ.Tables[0].Columns["TongTGKB"].Width = 80;

            dgListNPLHQ.Tables[0].Columns["TongTGTT"].Caption = "Tổng TGTT";
            dgListNPLHQ.Tables[0].Columns["TongTGTT"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["Tr_luong"].Caption = "Trọng Lượng";
            dgListNPLHQ.Tables[0].Columns["Tr_luong"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["So_Kien"].Caption = "Số Kiện";
            dgListNPLHQ.Tables[0].Columns["So_Kien"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["So_Container"].Caption = "Số Container 20";
            dgListNPLHQ.Tables[0].Columns["So_Container"].Width = 50;
            //dgList.Tables[0].Columns["NguoiNhap"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["NguoiNhap"].Visible = false;
            //dgList.Tables[0].Columns["NgayNhap"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["NgayNhap"].Visible = false;
            //dgList.Tables[0].Columns["PLuong"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["PLuong"].Visible = false;
            //dgList.Tables[0].Columns["Pass_DK"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["Pass_DK"].Visible = false;
            //dgList.Tables[0].Columns["Pass_TH"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["Pass_TH"].Visible = false;
            //dgList.Tables[0].Columns["Pass_KH"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["Pass_KH"].Visible = false;
            //dgList.Tables[0].Columns["Pass_GC"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["Pass_GC"].Visible = false;

            //dgList.Tables[0].Columns["TTTK"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["TTTK"].Visible = false;
            //dgList.Tables[0].Columns["PPT_GTGTUT"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["PPT_GTGTUT"].Visible = false;
            //dgList.Tables[0].Columns["DaTruyen"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["DaTruyen"].Visible = false;
            //dgList.Tables[0].Columns["Ma_DVKT"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["Ma_DVKT"].Visible = false;
            //dgList.Tables[0].Columns["ThanhKhoan"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["ThanhKhoan"].Visible = false;
            //dgList.Tables[0].Columns["PPT_GTGTKT"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["PPT_GTGTKT"].Visible = false;
            //dgList.Tables[0].Columns["Ngay_GiaoTK"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["Ngay_GiaoTK"].Visible = false;
            //dgList.Tables[0].Columns["Ngay_NhanTK"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["Ngay_NhanTK"].Visible = false;
            //dgList.Tables[0].Columns["Ngay_LuuTK"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["Ngay_LuuTK"].Visible = false;
            //dgList.Tables[0].Columns["NVPSinh"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["NVPSinh"].Visible = false;
            dgListNPLHQ.Tables[0].Columns["KhaiBao_01"].Caption = "Ghi Chú";
            dgListNPLHQ.Tables[0].Columns["KhaiBao_01"].Width = 150;

            //dgList.Tables[0].Columns["NguoiNhap_DK"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["NguoiNhap_DK"].Visible = false;
            //dgList.Tables[0].Columns["NgayNhap_DK"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["NgayNhap_DK"].Visible = false;
            //dgList.Tables[0].Columns["NguoiNhap_KH"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["NguoiNhap_KH"].Visible = false;
            //dgList.Tables[0].Columns["NgayNhap_KH"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["NgayNhap_KH"].Visible = false;
            //dgList.Tables[0].Columns["NguoiNhap_TH"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["NguoiNhap_TH"].Visible = false;
            //dgList.Tables[0].Columns["NgayNhap_TH"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["NgayNhap_TH"].Visible = false;
            dgListNPLHQ.Tables[0].Columns["So_Container40"].Caption = "Số Container 40";
            dgListNPLHQ.Tables[0].Columns["So_Container40"].Width = 50;
            //dgList.Tables[0].Columns["KiemHoaNK"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["KiemHoaNK"].Visible = false;
            dgListNPLHQ.Tables[0].Columns["So_HDTM"].Caption = "Số HĐTM";
            dgListNPLHQ.Tables[0].Columns["So_HDTM"].Width = 50;
            dgListNPLHQ.Tables[0].Columns["Ngay_HDTM"].Caption = "Ngày HĐTM";
            dgListNPLHQ.Tables[0].Columns["Ngay_HDTM"].Width = 80;
            dgListNPLHQ.Tables[0].Columns["Ngay_VanDon"].Caption = "Ngày Vận Đơn";
            dgListNPLHQ.Tables[0].Columns["Ngay_VanDon"].Width = 80;

            //dgList.Tables[0].Columns["Thue"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["Thue"].Visible = false;
            //dgList.Tables[0].Columns["HTKT"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["HTKT"].Visible = false;
            //dgList.Tables[0].Columns["TyLe_KT"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["TyLe_KT"].Visible = false;
            //dgList.Tables[0].Columns["DenNgay_KH"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["DenNgay_KH"].Visible = false;
            //dgList.Tables[0].Columns["DaiDien_DN"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["DaiDien_DN"].Visible = false;
            //dgList.Tables[0].Columns["NguoiQDHTKT"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["NguoiQDHTKT"].Visible = false;
            dgListNPLHQ.Tables[0].Columns["So_PLTK"].Caption = "Số Phụ Lục";
            dgListNPLHQ.Tables[0].Columns["So_PLTK"].Width = 50;
            //dgList.Tables[0].Columns["MienKT"].Caption = " ";
            dgListNPLHQ.Tables[0].Columns["MienKT"].Visible = false;
            dgListNPLHQ.Tables[0].Columns["Ma_MID"].Caption = "Mã MID";
            dgListNPLHQ.Tables[0].Columns["Ma_MID"].Width = 50;

            GridEXColumn colSelected = new GridEXColumn("Selected");
            colSelected.ActAsSelector = true;
            colSelected.Width = 20;
            dgListNPLHQ.Tables[0].Columns.Insert(0, colSelected);
        }
        */
        #endregion
        private void ucSXXK_Load(object sender, EventArgs e)
        {
            dgListNPLHQ.RootTable.RowHeaderWidth = dgListSPHQ.RootTable.RowHeaderWidth = dgListDMHQ.RootTable.RowHeaderWidth = dgListTKHQ.RootTable.RowHeaderWidth = 70;
            dgListNPLDN.RootTable.RowHeaderWidth = dgListSPDN.RootTable.RowHeaderWidth = dgListDMDN.RootTable.RowHeaderWidth = dgListTKDN.RootTable.RowHeaderWidth = 70;

            lblTitleSource.Text = GlobalSettings.radSynLocal == true ? "DỮ LIỆU NGUỒN SERVER [" + GlobalSettings.DiaChiSQLSource + "]" : "DỮ LIỆU HẢI QUAN";
            lblTitleDestination.Text = GlobalSettings.radSynLocal == true ? "DỮ LIỆU ĐÍCH SERVER [" + GlobalSettings.DiaChiSQLTarget + "]" : "DỮ LIỆU DOANH NGHIỆP";

            if (GlobalSettings.IsGetNPL && GlobalSettings.dsHQ[0] != null)
                dgListNPLHQ.DataSource = GlobalSettings.dsHQ[0].Tables[0];
            if (GlobalSettings.IsGetSP && GlobalSettings.dsHQ[1] != null)
                dgListSPHQ.DataSource = GlobalSettings.dsHQ[1].Tables[0];
            if (GlobalSettings.IsGetDM && GlobalSettings.dsHQ[2] != null)
                dgListDMHQ.DataSource = GlobalSettings.dsHQ[2].Tables[0];
            if (GlobalSettings.IsGetTK && GlobalSettings.dsHQ[3] != null)
                dgListTKHQ.DataSource = GlobalSettings.dsHQ[3].Tables[0];
            this.createSchema();

        }

        private void createSchema()
        {
            for (int i = 0; i < GlobalSettings.dsHQ.Length; i++)
            {
                if (GlobalSettings.dsDNNew[i] == null && GlobalSettings.dsHQ[i] != null)
                {
                    GlobalSettings.dsDNNew[i] = GlobalSettings.dsHQ[i].Clone();
                    GlobalSettings.dsDNNew[i].Tables[0].Rows.Clear();
                }
            }
            if (GlobalSettings.dsDNNew[0] != null)
                dgListNPLDN.DataSource = GlobalSettings.dsDNNew[0].Tables[0];
            if (GlobalSettings.dsDNNew[1] != null)
                dgListSPDN.DataSource = GlobalSettings.dsDNNew[1].Tables[0];
            if (GlobalSettings.dsDNNew[2] != null)
                dgListDMDN.DataSource = GlobalSettings.dsDNNew[2].Tables[0];
            if (GlobalSettings.dsDNNew[3] != null)
                dgListTKDN.DataSource = GlobalSettings.dsDNNew[3].Tables[0];
        }

        private void btnMoveRight_Click(object sender, EventArgs e)
        {
            if (tabHQ.SelectedIndex == 0)
            {
                tabDN.SelectedIndex = 0;
                GridEXRow[] row = dgListNPLHQ.GetCheckedRows();
                if (row.Length > 0)
                {
                    for (int i1 = 0; i1 < row.Length; i1++)
                    {
                        DataRow dr = GlobalSettings.dsDNNew[0].Tables[0].NewRow();
                        for (int i2 = 0; i2 < GlobalSettings.dsHQ[0].Tables[0].Rows[row[i1].RowIndex].ItemArray.Length; i2++)
                        {
                            dr[i2] = GlobalSettings.dsHQ[0].Tables[0].Rows[row[i1].RowIndex].ItemArray[i2];
                        }
                        GlobalSettings.dsDNNew[0].Tables[0].Rows.Add(dr);
                        GlobalSettings.dsHQ[0].Tables[0].Rows.RemoveAt(row[i1].RowIndex);
                    }
                }
                try
                {
                    dgListNPLHQ.Refresh();
                }
                catch
                {
                    dgListNPLHQ.Refetch();
                }
                try
                {
                    dgListNPLDN.Refresh();
                }
                catch
                {
                    dgListNPLDN.Refetch();
                }
            }
            else if (tabHQ.SelectedIndex == 1)
            {
                tabDN.SelectedIndex = 1;
                GridEXRow[] row = dgListSPHQ.GetCheckedRows();
                if (row.Length > 0)
                {
                    for (int i1 = 0; i1 < row.Length; i1++)
                    {
                        DataRow dr = GlobalSettings.dsDNNew[1].Tables[0].NewRow();
                        for (int i2 = 0; i2 < GlobalSettings.dsHQ[1].Tables[0].Rows[row[i1].RowIndex].ItemArray.Length; i2++)
                        {
                            dr[i2] = GlobalSettings.dsHQ[1].Tables[0].Rows[row[i1].RowIndex].ItemArray[i2];
                        }
                        GlobalSettings.dsDNNew[1].Tables[0].Rows.Add(dr);
                        GlobalSettings.dsHQ[1].Tables[0].Rows.RemoveAt(row[i1].RowIndex);
                    }
                }
                try
                {
                    dgListSPHQ.Refresh();
                }
                catch
                {
                    dgListSPHQ.Refetch();
                }
                try
                {
                    dgListSPDN.Refresh();
                }
                catch
                {
                    dgListSPDN.Refetch();
                }
            }
            else if (tabHQ.SelectedIndex == 2)
            {
                tabDN.SelectedIndex = 2;
                GridEXRow[] row = dgListDMHQ.GetCheckedRows();
                if (row.Length > 0)
                {
                    for (int i1 = 0; i1 < row.Length; i1++)
                    {
                        DataRow dr = GlobalSettings.dsDNNew[2].Tables[0].NewRow();
                        for (int i2 = 0; i2 < GlobalSettings.dsHQ[2].Tables[0].Rows[row[i1].RowIndex].ItemArray.Length; i2++)
                        {
                            dr[i2] = GlobalSettings.dsHQ[2].Tables[0].Rows[row[i1].RowIndex].ItemArray[i2];
                        }
                        GlobalSettings.dsDNNew[2].Tables[0].Rows.Add(dr);
                        GlobalSettings.dsHQ[2].Tables[0].Rows.RemoveAt(row[i1].RowIndex);
                    }
                }
                try
                {
                    dgListDMHQ.Refresh();
                }
                catch
                {
                    dgListDMHQ.Refetch();
                }
                try
                {
                    dgListDMDN.Refresh();
                }
                catch
                {
                    dgListDMDN.Refetch();
                }
            }
            else if (tabHQ.SelectedIndex == 3)
            {
                tabDN.SelectedIndex = 3;
                GridEXRow[] row = dgListTKHQ.GetCheckedRows();
                if (row.Length > 0)
                {
                    for (int i1 = 0; i1 < row.Length; i1++)
                    {
                        DataRow dr = GlobalSettings.dsDNNew[3].Tables[0].NewRow();
                        for (int i2 = 0; i2 < GlobalSettings.dsHQ[3].Tables[0].Rows[row[i1].RowIndex].ItemArray.Length; i2++)
                        {
                            dr[i2] = GlobalSettings.dsHQ[3].Tables[0].Rows[row[i1].RowIndex].ItemArray[i2];
                        }
                        GlobalSettings.dsDNNew[3].Tables[0].Rows.Add(dr);
                        for (int j = 0; j < GlobalSettings.dsHQ[4].Tables[0].Rows.Count; j++)
                        {
                            DataRow dr1 = GlobalSettings.dsHQ[4].Tables[0].Rows[j];
                            if (dr[0].Equals(dr1[0]) && dr[1].Equals(dr1[1]) && dr[2].Equals(dr1[2]) && dr[3].Equals(dr1[3]))
                            {
                                DataRow dr2 = GlobalSettings.dsDNNew[4].Tables[0].NewRow();
                                for (int j1 = 0; j1 < dr1.ItemArray.Length; j1++)
                                {
                                    dr2[j1] = dr1[j1];
                                }
                                GlobalSettings.dsDNNew[4].Tables[0].Rows.Add(dr2);
                                GlobalSettings.dsHQ[4].Tables[0].Rows.Remove(dr1);
                                j--;
                            }
                        }
                        GlobalSettings.dsHQ[3].Tables[0].Rows.RemoveAt(row[i1].RowIndex);
                    }
                }
                try
                {
                    dgListTKHQ.Refresh();
                }
                catch
                {
                    dgListTKHQ.Refetch();
                }
                try
                {
                    dgListTKDN.Refresh();
                }
                catch
                {
                    dgListTKDN.Refetch();
                }
            }
        }

        private void btnMoveLeft_Click(object sender, EventArgs e)
        {
            if (tabDN.SelectedIndex == 0)
            {
                tabHQ.SelectedIndex = 0;
                GridEXRow[] row = dgListNPLDN.GetCheckedRows();
                if (row.Length > 0)
                {
                    for (int i1 = 0; i1 < row.Length; i1++)
                    {
                        DataRow dr = GlobalSettings.dsHQ[0].Tables[0].NewRow();
                        for (int i2 = 0; i2 < GlobalSettings.dsDNNew[0].Tables[0].Rows[row[i1].RowIndex].ItemArray.Length; i2++)
                        {
                            dr[i2] = GlobalSettings.dsDNNew[0].Tables[0].Rows[row[i1].RowIndex].ItemArray[i2];
                        }
                        GlobalSettings.dsHQ[0].Tables[0].Rows.Add(dr);
                        GlobalSettings.dsDNNew[0].Tables[0].Rows.RemoveAt(row[i1].RowIndex);
                    }
                }
                try
                {
                    dgListNPLHQ.Refresh();
                }
                catch
                {
                    dgListNPLHQ.Refetch();
                }
                try
                {
                    dgListNPLDN.Refresh();
                }
                catch
                {
                    dgListNPLDN.Refetch();
                }
            }

            else if (tabDN.SelectedIndex == 1)
            {
                tabHQ.SelectedIndex = 1;
                GridEXRow[] row = dgListSPDN.GetCheckedRows();
                if (row.Length > 0)
                {
                    for (int i1 = 0; i1 < row.Length; i1++)
                    {
                        DataRow dr = GlobalSettings.dsHQ[1].Tables[0].NewRow();
                        for (int i2 = 0; i2 < GlobalSettings.dsDNNew[1].Tables[0].Rows[row[i1].RowIndex].ItemArray.Length; i2++)
                        {
                            dr[i2] = GlobalSettings.dsDNNew[1].Tables[0].Rows[row[i1].RowIndex].ItemArray[i2];
                        }
                        GlobalSettings.dsHQ[1].Tables[0].Rows.Add(dr);
                        GlobalSettings.dsDNNew[1].Tables[0].Rows.RemoveAt(row[i1].RowIndex);
                    }
                }
                try
                {
                    dgListSPHQ.Refresh();
                }
                catch
                {
                    dgListSPHQ.Refetch();
                }
                try
                {
                    dgListSPDN.Refresh();
                }
                catch
                {
                    dgListSPDN.Refetch();
                }
            }
            else if (tabDN.SelectedIndex == 2)
            {
                tabHQ.SelectedIndex = 2;
                GridEXRow[] row = dgListDMDN.GetCheckedRows();
                if (row.Length > 0)
                {
                    for (int i1 = 0; i1 < row.Length; i1++)
                    {
                        DataRow dr = GlobalSettings.dsHQ[2].Tables[0].NewRow();
                        for (int i2 = 0; i2 < GlobalSettings.dsDNNew[2].Tables[0].Rows[row[i1].RowIndex].ItemArray.Length; i2++)
                        {
                            dr[i2] = GlobalSettings.dsDNNew[2].Tables[0].Rows[row[i1].RowIndex].ItemArray[i2];
                        }
                        GlobalSettings.dsHQ[2].Tables[0].Rows.Add(dr);
                        GlobalSettings.dsDNNew[2].Tables[0].Rows.RemoveAt(row[i1].RowIndex);
                    }
                }
                try
                {
                    dgListDMHQ.Refresh();
                }
                catch
                {
                    dgListDMHQ.Refetch();
                }
                try
                {
                    dgListDMDN.Refresh();
                }
                catch
                {
                    dgListDMDN.Refetch();
                }
            }
            else if (tabDN.SelectedIndex == 3)
            {
                tabHQ.SelectedIndex = 3;
                GridEXRow[] row = dgListTKDN.GetCheckedRows();
                if (row.Length > 0)
                {
                    for (int i1 = 0; i1 < row.Length; i1++)
                    {
                        DataRow dr = GlobalSettings.dsHQ[3].Tables[0].NewRow();
                        for (int i2 = 0; i2 < GlobalSettings.dsDNNew[3].Tables[0].Rows[row[i1].RowIndex].ItemArray.Length; i2++)
                        {
                            dr[i2] = GlobalSettings.dsDNNew[3].Tables[0].Rows[row[i1].RowIndex].ItemArray[i2];
                        }
                        GlobalSettings.dsHQ[3].Tables[0].Rows.Add(dr);
                        for (int j = 0; j < GlobalSettings.dsDNNew[4].Tables[0].Rows.Count; j++)
                        {
                            DataRow dr1 = GlobalSettings.dsDNNew[4].Tables[0].Rows[j];
                            if (dr[0].Equals(dr1[0]) && dr[1].Equals(dr1[1]) && dr[2].Equals(dr1[2]) && dr[3].Equals(dr1[3]))
                            {
                                DataRow dr2 = GlobalSettings.dsHQ[4].Tables[0].NewRow();
                                for (int j1 = 0; j1 < dr1.ItemArray.Length; j1++)
                                {
                                    dr2[j1] = dr1[j1];
                                }
                                GlobalSettings.dsHQ[4].Tables[0].Rows.Add(dr2);
                                GlobalSettings.dsDNNew[4].Tables[0].Rows.Remove(dr1);
                                j--;
                            }
                        }
                        GlobalSettings.dsDNNew[3].Tables[0].Rows.RemoveAt(row[i1].RowIndex);
                    }
                }
                try
                {
                    dgListTKHQ.Refresh();
                }
                catch
                {
                    dgListTKHQ.Refetch();
                }
                try
                {
                    dgListTKDN.Refresh();
                }
                catch
                {
                    dgListTKDN.Refetch();
                }
            }

        }

        private void chkOver_CheckedChanged(object sender, EventArgs e)
        {
            GlobalSettings.GhiDe = chkOver.Checked;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        public void Serialization(string fileName, DataSXXKCollection dataSXXKCollection)
        {
            try
            {
                Stream stream = null;
                XmlSerializer serializer = new XmlSerializer(typeof(DataSXXKCollection));
                stream = new FileStream(fileName, FileMode.OpenOrCreate);
                serializer.Serialize(stream, dataSXXKCollection);
                stream.Close();
                MessageBox.Show("Lưu thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show("Lưu không thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnExportXML_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {

                DataSXXKCollection dataCollection = new DataSXXKCollection();
                DataSXXK dataSXXK = new DataSXXK();
                dataSXXK.DinhMuc = (DataTable)dgListDMDN.DataSource;
                dataSXXK.ToKhai = (DataTable)dgListTKDN.DataSource;
                dataSXXK.SanPham = (DataTable)dgListSPDN.DataSource;
                dataSXXK.NguyenPhuLieu = (DataTable)dgListNPLDN.DataSource;
                dataCollection.Add(dataSXXK);
                Serialization(saveFileDialog1.FileName, dataCollection);
            }
        }

        private static bool check = true;
        private void CheckRows(KeyEventArgs e, GridEX dataGrid)
        {
            if (e.KeyCode == Keys.Space)
            {
                foreach (GridEXSelectedItem item in dataGrid.SelectedItems)
                {
                    dataGrid.GetRow(item.Position).CheckState = (check == true ? RowCheckState.Checked : RowCheckState.Unchecked);
                }

                dataGrid.Refresh();

                check = !check;
            }
        }

        private void dgListNPLHQ_KeyDown(object sender, KeyEventArgs e)
        {
            CheckRows(e, dgListNPLHQ);
        }

        private void dgListSPHQ_KeyDown(object sender, KeyEventArgs e)
        {
            CheckRows(e, dgListSPHQ);
        }

        private void dgListDMHQ_KeyDown(object sender, KeyEventArgs e)
        {
            CheckRows(e, dgListDMHQ);
        }

        private void dgListTKHQ_KeyDown(object sender, KeyEventArgs e)
        {
            CheckRows(e, dgListTKHQ);
        }

        private void dgListNPLDN_KeyDown(object sender, KeyEventArgs e)
        {
            CheckRows(e, dgListNPLDN);
        }

        private void dgListSPDN_KeyDown(object sender, KeyEventArgs e)
        {
            CheckRows(e, dgListSPDN);
        }

        private void dgListDMDN_KeyDown(object sender, KeyEventArgs e)
        {
            CheckRows(e, dgListDMDN);
        }

        private void dgListTKDN_KeyDown(object sender, KeyEventArgs e)
        {
            CheckRows(e, dgListTKDN);
        }
    }
}
