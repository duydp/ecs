using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.BLL.KDT;
using Company.BLL.KDT.SXXK;
using System.IO;
using Company.BLL;

namespace Company.Interface
{
    public partial class FormSendToKhai : BaseForm
    {
        ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();//tat ca to khai
        ToKhaiMauDichCollection collectionSelected = new ToKhaiMauDichCollection();//to khai dc chon
        ToKhaiMauDich tkmd = new ToKhaiMauDich();
        private string xmlCurrent = "";
        public FormSendToKhai()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            LaySoTiepNhanDT();
        }

    
        private void BK02WizardForm_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }

        private void RemoveTKMDCollection(long id)
        {
            for (int i = 0; i < this.collection.Count; i++)
            {
                if (this.collection[i].ID==id)
                {
                    this.collection.Remove(collection[i]);
                    break;
                }
            }
        }
        private void RemoveTKMDCollectionSelect(long id)
        {
            for (int i = 0; i < this.collectionSelected.Count; i++)
            {
                if (this.collectionSelected[i].ID == id)
                {
                    this.collectionSelected.Remove(collectionSelected[i]);
                    break;
                }
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            DateTime fromDate = ccFromDate.Value;
            DateTime toDate = ccToDate.Value;
            string sql = "select * from t_KDT_ToKhaiMauDich where 1=1 and MaHaiQuan='"+GlobalSettings.MA_HAI_QUAN+"' and MaDoanhNghiep='"+GlobalSettings.MA_DON_VI+"'";
            if (btnXacNhan.Enabled == true)
            {                
                sql += " and id in (select master_id from t_KDT_SXXK_MsgSend where LoaiHS = 'TK')";
                
            }
            else
            {                
                sql += " and id not in (select master_id from t_KDT_SXXK_MsgSend where LoaiHS = 'TK')";
                if (btnSend.Enabled == true)
                {
                    sql += " and trangthaixuly = -1";
                }
                else if (btnNhan.Enabled == true)
                {
                    sql += "  and NgayTiepNhan >= @From and NgayTiepNhan<=@To ";
                    sql += " and trangthaixuly = 0";
                }
                else if (btnHuy.Enabled == true)
                {
                    sql += "  and NgayTiepNhan >= @From and NgayTiepNhan<=@To ";
                    sql += " and trangthaixuly = 0 or trangthaixuly = 2";
                }
            }                        
            collection = tkmd.SelectCollectionDynamic(sql,fromDate,toDate);
            foreach (ToKhaiMauDich tkSelect in collectionSelected)
            {
                RemoveTKMDCollection(tkSelect.ID);
            }
            dgTK.DataSource = collection;
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            sendItemsSelect();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {            
            foreach (GridEXRow row in dgTK.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    ToKhaiMauDich tkmdOld = (ToKhaiMauDich)row.DataRow;
                    ToKhaiMauDich tkmdNew = new ToKhaiMauDich();
                    tkmdNew.ID = tkmdOld.ID;
                    tkmdNew.NgayTiepNhan = tkmdOld.NgayTiepNhan;
                    tkmdNew.SoTiepNhan = tkmdOld.SoTiepNhan;
                    tkmdNew.MaLoaiHinh = tkmdOld.MaLoaiHinh;
                    tkmdNew.TrangThaiXuLy = tkmdOld.TrangThaiXuLy;
                    this.collectionSelected.Add(tkmdNew);         
                }
            }
            foreach (ToKhaiMauDich tkSelect in collectionSelected)
            {
                RemoveTKMDCollection(tkSelect.ID);
            }
            dgTK.DataSource = collection;
            try
            {
                dgTK.Refetch();
            }
            catch
            {
                dgTK.Refresh();
            }
            gridEX1.DataSource = collectionSelected;
            gridEX1.Refetch(); 
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            ToKhaiMauDichCollection collectionUnSelected = new ToKhaiMauDichCollection();//tat ca to khai bo chon
            foreach (GridEXRow row in gridEX1.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    ToKhaiMauDich tkmdOld = (ToKhaiMauDich)row.DataRow;
                    ToKhaiMauDich tkmdNew = new ToKhaiMauDich();
                    tkmdNew.ID = tkmdOld.ID;
                    tkmdNew.NgayTiepNhan = tkmdOld.NgayTiepNhan;
                    tkmdNew.MaLoaiHinh = tkmdOld.MaLoaiHinh;
                    tkmdNew.TrangThaiXuLy = tkmdOld.TrangThaiXuLy;
                    this.collection.Add(tkmdNew);         
                    collectionUnSelected.Add(tkmdNew);
                }
            }
            foreach (ToKhaiMauDich tk in collectionUnSelected)
            {
                RemoveTKMDCollectionSelect(tk.ID);
            }            
            gridEX1.DataSource = collectionSelected;
            try
            {
                gridEX1.Refetch(); 
            }
            catch
            {
                gridEX1.Refresh();
            }
            
            btnSearch_Click(null, null);
        }
        private void LaySoTiepNhanDT()
        {
            if (collectionSelected.Count == 0)
            {
              //  ShowMessage("Bạn chưa chọn danh sách định mức.", false);
                MLMessages("Bạn chưa chọn danh sách định mức.", "MSG_DMC01", "", false);
                return;
            }
            string password = "";
            try
            {
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }

                this.Cursor = Cursors.WaitCursor;
                if(GlobalSettings.PassWordDT!="")
                    password=GlobalSettings.PassWordDT;
                else
                    password=wsForm.txtMatKhau.Text.Trim();
                bool ok = true;
                int k = 0;
                int itemOK = 0;
                this.Cursor = Cursors.WaitCursor;
                foreach (ToKhaiMauDich tkmd in collectionSelected)
                {

                    tkmd.MaHaiQuan = ctrDonViHaiQuan.Ma;
                    tkmd.Load();                                        
                    try
                    {
                        k++;
                        itemOK++;
                        {                            
                            MsgSend sendXML = new MsgSend();
                            sendXML.LoaiHS = "TK";
                            sendXML.master_id = tkmd.ID;
                            string st = "";
                            if (sendXML.Load())
                            {
                                if (tkmd.MaLoaiHinh.StartsWith("NSX") || tkmd.MaLoaiHinh.StartsWith("XSX"))
                                    st = tkmd.LayPhanHoi(password, sendXML.msg);                                
                                if (st != "")
                                    --itemOK;
                                else
                                    sendXML.Delete();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        itemOK--;
                        this.Cursor = Cursors.Default;
                        ok = false;
                        string message = "";
                        if (k == collectionSelected.Count)
                        {
                            message = "Tờ khai thứ " + k.ToString() + " không nhận thông tin về được?";
                        }
                        else
                        {
                            message = "Tờ khai thứ " + k.ToString() + " không nhận thông tin về được?";
                        }
                        string st = ShowMessage(message, true);
                        if (st != "Yes")
                            break;
                        else
                        {
                            if (k < collectionSelected.Count)
                                ok = true;
                            else
                                ok = false;

                            //Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();
                            //hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //hd.TrangThai = tkmd.TrangThaiXuLy;
                            //hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //hd.PassWord = password;
                            //hd.ID = tkmd.ID;
                            //MainForm.AddToQueueForm(hd);
                            //MainForm.ShowQueueForm();
                        }
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();

                    }
                }
                this.Cursor = Cursors.Default;
                if (ok)
                {
                    if (itemOK > 0)
                    {
                        ShowMessage("Nhận thành công " + itemOK.ToString() + " tờ khai.", false);
                        btnXacNhan.Enabled = false;
                    }
                    else
                        ShowMessage("Chưa có phản hồi tử hải quan ! ", false);
                }
                else
                {
                    if (itemOK > 0)
                    {
                        ShowMessage("Nhận thành công " + itemOK.ToString() + " tờ khai.", false);
                        btnXacNhan.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                ShowMessage(ex.Message, false);
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void sendItemsSelect()
        {
            if (collectionSelected.Count == 0)
            {
               // ShowMessage("Bạn chưa chọn danh sách định mức.", false);
                return;
            }
            string password = "";
            try
            {
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                bool ok = true;
                int k = 0;
                int itemOK = 0;
                this.Cursor = Cursors.WaitCursor;
                foreach (ToKhaiMauDich tkmd in collectionSelected)
                {
                    
                        tkmd.MaHaiQuan = ctrDonViHaiQuan.Ma;
                        tkmd.Load();
                        tkmd.LoadHMDCollection();
                        //tkmd.LoadChungTuTKCollection();
                        try
                        {
                            k++;
                            itemOK++;
                            {
                                if (tkmd.MaLoaiHinh.StartsWith("NSX"))
                                {
                                    //if (tkmd.SoTiepNhan == 0)
                                        xmlCurrent = tkmd.WSSendXMLNHAP(password);
                                    //else
                                        //xmlCurrent = tkmd.WSUpdateXMLNHAP(password);
                                }
                                else if (tkmd.MaLoaiHinh.StartsWith("XSX"))
                                {
                                    //if (tkmd.SoTiepNhan == 0)
                                        xmlCurrent = tkmd.WSSendXMLXuat(password,GlobalSettings.MaMID);
                                    //else
                                        //xmlCurrent = tkmd.WSUpdateXMLXUAT(password);
                                }
                                                                                    
                                    MsgSend sendXML = new MsgSend();
                                    sendXML.LoaiHS = "TK";
                                    sendXML.master_id = tkmd.ID;
                                    sendXML.msg = xmlCurrent;
                                    sendXML.func = 1;
                                    xmlCurrent = "";
                                    sendXML.Insert();                                
                            }
                        }
                        catch (Exception ex)
                        {
                            itemOK--;
                            this.Cursor = Cursors.Default;
                            ok = false;
                            string message = "";
                            if (k == collectionSelected.Count)
                            {
                                message = "Tờ khai thứ " + k.ToString() + " không khai báo được ";
                            }
                            else
                            {
                                message = "Tờ khai thứ " + k.ToString() + " không khai báo được?\nBạn có muốn tiếp tục gửi dữ liệu của các tờ khai tiếp không";
                            }
                            string st = ShowMessage(message, true);
                            if (st != "Yes")
                                break;
                            else
                            {
                                if (k < collectionSelected.Count)
                                    ok = true;
                                else
                                    ok = false;
                                //Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();
                                //hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                //hd.TrangThai = tkmd.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.KHAI_BAO;
                                //hd.PassWord = password;
                                //hd.ID = tkmd.ID;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                            StreamWriter write = File.AppendText("Error.txt");
                            write.WriteLine("--------------------------------");
                            write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                            write.WriteLine(ex.StackTrace);
                            write.WriteLine("Lỗi là : ");
                            write.WriteLine(ex.Message);
                            write.WriteLine("--------------------------------");
                            write.Flush();
                            write.Close();

                        }                    
                }
                LayPhanHoi(password);
                btnXacNhan.Enabled = true;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                ShowMessage(ex.Message, false);
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
           
        }
        private void downloadItemsSelect()
        {
            if (collectionSelected.Count == 0)
            {
               // ShowMessage("Bạn chưa chọn danh sách định mức.", false);
                MLMessages("Bạn chưa chọn danh sách định mức.", "MSG_DMC01", "", false);
                return;
            }
            string password = "";
            try
            {
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                bool ok = true;
                int k = 0;
                int itemOK = 0;
                this.Cursor = Cursors.WaitCursor;
                foreach (ToKhaiMauDich tkmd in collectionSelected)
                {

                    tkmd.MaHaiQuan = ctrDonViHaiQuan.Ma;
                    tkmd.Load();
                    tkmd.LoadHMDCollection();
                    //tkmd.LoadChungTuTKCollection();
                    try
                    {
                        k++;
                        itemOK++;
                        {
                            if (tkmd.MaLoaiHinh.StartsWith("NSX") || tkmd.MaLoaiHinh.StartsWith("XSX"))
                                xmlCurrent = tkmd.WSRequestXML(password);
                          
                            MsgSend sendXML = new MsgSend();
                            sendXML.LoaiHS = "TK";
                            sendXML.master_id = tkmd.ID;
                            sendXML.msg = xmlCurrent;
                            sendXML.func = 2;
                            xmlCurrent = "";
                            sendXML.Insert();
                        }
                    }
                    catch (Exception ex)
                    {
                        itemOK--;
                        this.Cursor = Cursors.Default;
                        ok = false;
                        string message = "";
                        if (k == collectionSelected.Count)
                        {
                            message = "Tờ khai thứ " + k.ToString() + " không gửi thông tin được";
                        }
                        else
                        {
                            message = "Tờ khai thứ " + k.ToString() + " không gửi thông tin được?\nBạn có muốn tiếp tục gửi dữ liệu của các tờ khai tiếp không";
                        }
                        string st = ShowMessage(message, true);
                        if (st != "Yes")
                            break;
                        else
                        {
                            if (k < collectionSelected.Count)
                                ok = true;
                            else
                                ok = false;
                            //Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();
                            //hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //hd.TrangThai = tkmd.TrangThaiXuLy;
                            //hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                            //hd.PassWord = password;
                            //hd.ID = tkmd.ID;
                            //MainForm.AddToQueueForm(hd);
                            //MainForm.ShowQueueForm();
                        }
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();

                    }
                }
                LayPhanHoi(password);
                btnXacNhan.Enabled = true;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                ShowMessage(ex.Message, false);
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận trạng tháitờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void cancelItemsSelect()
        {
            if (collectionSelected.Count == 0)
            {
                ShowMessage("Bạn chưa chọn danh sách định mức.", false);
                return;
            }
            string password = "";
            try
            {
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }

                
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                bool ok = true;
                int k = 0;
                int itemOK = 0;
                foreach (ToKhaiMauDich tkmd in collectionSelected)
                {
                    tkmd.MaHaiQuan = ctrDonViHaiQuan.Ma;
                    tkmd.Load();
                    tkmd.LoadHMDCollection();                    
                    {                       
                        try
                        {
                            k++;
                            itemOK++;
                            {
                                if (tkmd.MaLoaiHinh.StartsWith("NSX"))
                                    xmlCurrent = tkmd.WSCancelXMLNhap(password);
                                else if (tkmd.MaLoaiHinh.StartsWith("XSX"))
                                    xmlCurrent = tkmd.WSCancelXMLXuat(password);
                                       
                                MsgSend sendXML = new MsgSend();
                                sendXML.LoaiHS = "TK";
                                sendXML.master_id = tkmd.ID;
                                sendXML.msg = xmlCurrent;
                                sendXML.func = 3;
                                xmlCurrent = "";
                                sendXML.Insert();
                            }
                        }
                        catch (Exception ex)
                        {
                            itemOK--;
                            this.Cursor = Cursors.Default;
                            ok = false;
                            string message = "";
                            if (k == collectionSelected.Count)
                            {
                                message = "Tờ khai có số tiếp nhận " + tkmd.SoTiepNhan.ToString() + " không hủy khai báo được";
                            }
                            else
                            {
                                message = "Tờ khai có số tiếp nhận " + tkmd.SoTiepNhan.ToString() + " hủy khai báo được ?\nBạn có muốn tiếp tục hủy các tờ khai tiếp không";
                            }
                            string st = ShowMessage(message, true);
                            if (st != "Yes")
                                break;
                            else
                            {
                                if (k < collectionSelected.Count)
                                    ok = true;
                                else
                                    ok = false;
                                //Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();
                                //hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                //hd.TrangThai = tkmd.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.HUY_KHAI_BAO;
                                //hd.PassWord = password;
                                //hd.ID = tkmd.ID;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                            StreamWriter write = File.AppendText("Error.txt");
                            write.WriteLine("--------------------------------");
                            write.WriteLine("Lỗi khi Hủy khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                            write.WriteLine(ex.StackTrace);
                            write.WriteLine("Lỗi là : ");
                            write.WriteLine(ex.Message);
                            write.WriteLine("--------------------------------");
                            write.Flush();
                            write.Close();


                        }

                    }
                }
                LayPhanHoi(password);
                btnXacNhan.Enabled = true;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                ShowMessage(ex.Message, false);
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }           
        }

        private void LayPhanHoi(string pass)
        {
            try
            {              
                bool ok = true;
                int k = 0;
                int itemOK = 0;
                this.Cursor = Cursors.WaitCursor;
                foreach (ToKhaiMauDich tkmd in collectionSelected)
                {

                    tkmd.MaHaiQuan = ctrDonViHaiQuan.Ma;
                    tkmd.Load();
                    try
                    {
                        k++;
                        itemOK++;
                        {
                            MsgSend sendXML = new MsgSend();
                            sendXML.LoaiHS = "TK";
                            sendXML.master_id = tkmd.ID;
                            string st = "";
                            if (sendXML.Load())
                            {
                                if (tkmd.MaLoaiHinh.StartsWith("NSX") || tkmd.MaLoaiHinh.StartsWith("XSX"))
                                    st = tkmd.LayPhanHoi(pass, sendXML.msg);
                              
                                if (st != "")
                                    --itemOK;
                                else
                                    sendXML.Delete();
                            }
                            else
                                itemOK--;
                        }
                    }
                    catch (Exception ex)
                    {
                        itemOK--;
                        this.Cursor = Cursors.Default;
                        ok = false;
                        string message = "";
                        if (k == collectionSelected.Count)
                        {
                            message = "Tờ khai thứ " + k.ToString() + " không nhận thông tin về được";
                        }
                        else
                        {
                            message = "Tờ khai thứ " + k.ToString() + " không nhận thông tin về được?\nBạn có muốn tiếp tục gửi dữ liệu của các tờ khai tiếp không";
                        }
                        string st = ShowMessage(message, true);
                        if (st != "Yes")
                            break;
                        else
                        {
                            if (k < collectionSelected.Count)
                                ok = true;
                            else
                               ok = false;
                            //Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();
                            //hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //hd.TrangThai = tkmd.TrangThaiXuLy;
                            //hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //hd.PassWord = pass;
                            //hd.ID = tkmd.ID;
                            //MainForm.AddToQueueForm(hd);
                            //MainForm.ShowQueueForm();
                        }
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();

                    }
                }
                this.Cursor = Cursors.Default;
                if (ok)
                {
                    if (itemOK > 0)
                    {
                        ShowMessage("Nhận thành công " + itemOK.ToString() + " tờ khai.", false);
                        btnXacNhan.Enabled = false;
                    }
                    else
                        ShowMessage("Chưa có phản hồi tử hải quan ! ", false);
                }
                else
                {
                    if (itemOK > 0)
                    {
                        ShowMessage("Nhận thành công " + itemOK.ToString() + " tờ khai.", false);
                        btnXacNhan.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                ShowMessage(ex.Message, false);
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnNhan_Click(object sender, EventArgs e)
        {
            downloadItemsSelect();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            cancelItemsSelect();
        }

        private void dgTK_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["NgayTiepNhan"].Text != "")
            {
                DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Text);
                if (dt.Year <= 1900)
                    e.Row.Cells["NgayTiepNhan"].Text = "";
            }
            switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
            {
                case -1:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                    break;
                case 0:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                    break;
                case 1:
                    {                        
                    e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";                     
                    }
                    break;
                case 2:
                    e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                    break;
            }
        }
    }
}