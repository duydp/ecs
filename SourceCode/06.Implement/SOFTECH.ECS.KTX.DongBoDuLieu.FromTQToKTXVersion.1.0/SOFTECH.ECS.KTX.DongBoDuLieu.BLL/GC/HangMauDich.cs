using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace DongBoDuLieu.BLL.GC
{
    public class HMD
    {
        public static DataSet GetDataSetHang(Company.GC.BLL.KDT.GC.HopDong HD,string matKhau)
        {
            
            string queryHang =
                           "SELECT distinct H.* " +
                           " from DTOKHAIMD T  inner join DHANGMDDK H" +
                           " on H.SOTK=T.SOTK and H.MA_LH=T.MA_LH and H.MA_HQ=T.MA_HQ " +
                           " and H.NAMDK=T.NAMDK where T.MA_HQ = '" + HD.MaHaiQuan +"' AND T.MA_DV = '" + HD.MaDoanhNghiep + "' AND So_HD = '" + HD.SoHopDong + "'";
            DataSet dsHang = WebServiceDB.getDataBySQL(queryHang.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsHang;
            
            //TODO: HangMauDich
            /*
            StringBuilder queryHang = new StringBuilder();
            queryHang.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            queryHang.Append(" SELECT H.* ");
            queryHang.Append(" from DTOKHAIMD T  inner join DHANGMDDK H ");
            queryHang.Append(" on H.SOTK=T.SOTK and H.MA_LH=T.MA_LH and H.MA_HQ=T.MA_HQ and H.NAMDK=T.NAMDK ");
            queryHang.Append("WHERE T.Ma_HQ = ''" + HD.MaHaiQuan + "'' AND T.Ma_DV = ''" + HD.MaDoanhNghiep + "'' ");
            queryHang.Append(" AND So_HD = ''" + HD.SoHopDong + "''");
            queryHang.Append(" ') AS a;");

            DataSet dsHang = WebServiceDB.getDataBySQL(queryHang.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsHang;
            */
        }

        public static DataSet GetDataSetHang(Company.GC.BLL.KDT.GC.HopDong HD, string matKhau, int soToKhai)
        {

            string queryHang =
                           "SELECT H.* " +
                           " from DTOKHAIMD T  inner join DHANGMDDK H" +
                           " on H.SOTK=T.SOTK and H.MA_LH=T.MA_LH and H.MA_HQ=T.MA_HQ " +
                           " and H.NAMDK=T.NAMDK where T.MA_HQ = '" + HD.MaHaiQuan + "' AND T.MA_DV = '" + HD.MaDoanhNghiep + "' AND T.So_HD = '" + HD.SoHopDong + "' AND T.SOTK = " + soToKhai;
            DataSet dsHang = WebServiceDB.getDataBySQL(queryHang.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsHang;

            //TODO: HangMauDich
            /*
            StringBuilder queryHang = new StringBuilder();
            queryHang.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            queryHang.Append(" SELECT H.* ");
            queryHang.Append(" from DTOKHAIMD T  inner join DHANGMDDK H ");
            queryHang.Append(" on H.SOTK=T.SOTK and H.MA_LH=T.MA_LH and H.MA_HQ=T.MA_HQ and H.NAMDK=T.NAMDK ");
            queryHang.Append("WHERE T.Ma_HQ = ''" + HD.MaHaiQuan + "'' AND T.Ma_DV = ''" + HD.MaDoanhNghiep + "'' ");
            queryHang.Append(" AND So_HD = ''" + HD.SoHopDong + "''");
            queryHang.Append(" ') AS a;");

            DataSet dsHang = WebServiceDB.getDataBySQL(queryHang.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsHang;
            */
        }

        public static DataSet GetDataSetHangByNgay(Company.GC.BLL.KDT.GC.HopDong HD, DateTime NgayBatDau, DateTime NgayKetThuc,string matKhau)
        {
            string queryHang =
                           "SELECT H.* " +
                           " from DTOKHAIMD T  inner join DHANGMDDK H" +
                           " on H.SOTK=T.SOTK and H.MA_LH=T.MA_LH and H.MA_HQ=T.MA_HQ " +
                           " and H.NAMDK=T.NAMDK where T.MA_HQ = '" + HD.MaHaiQuan + "' AND T.MA_DV = '" + HD.MaDoanhNghiep + "' AND So_HD = '" + HD.SoHopDong + "'"+
                           " AND (T.Ngay_DK) >='" + NgayBatDau.ToString("MM/dd/yyyy") + "'"+
                           " AND (T.Ngay_DK) <='" + NgayKetThuc.ToString("MM/dd/yyyy") + "'";

            DataSet dsHang = WebServiceDB.getDataBySQL(queryHang.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsHang;
        }
    }
}
