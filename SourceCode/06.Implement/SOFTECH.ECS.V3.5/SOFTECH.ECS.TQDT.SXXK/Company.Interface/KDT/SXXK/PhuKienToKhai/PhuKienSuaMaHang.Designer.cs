﻿namespace Company.Interface.KDT.SXXK
{
    partial class PhuKienSuaMaHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout grvHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhuKienSuaMaHang));
            this.txtMaHangCu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnDong = new Janus.Windows.EditControls.UIButton();
            this.cmbPhanLoaiCu = new Janus.Windows.EditControls.UIComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaHQCu = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoTT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaHQMoi = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.cmbPhanLoaiMoi = new Janus.Windows.EditControls.UIComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMaHangMoi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.grvHang = new Janus.Windows.GridEX.GridEX();
            this.cbxChonNhieuHang = new Janus.Windows.EditControls.UICheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvHang)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grvHang);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.btnGhi);
            this.grbMain.Controls.Add(this.btnDong);
            this.grbMain.Size = new System.Drawing.Size(690, 507);
            // 
            // txtMaHangCu
            // 
            this.txtMaHangCu.Location = new System.Drawing.Point(88, 20);
            this.txtMaHangCu.Name = "txtMaHangCu";
            this.txtMaHangCu.Size = new System.Drawing.Size(220, 21);
            this.txtMaHangCu.TabIndex = 0;
            this.txtMaHangCu.VisualStyleManager = this.vsmMain;
            // 
            // btnDong
            // 
            this.btnDong.Image = global::Company.Interface.Properties.Resources.exit;
            this.btnDong.Location = new System.Drawing.Point(596, 184);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(75, 23);
            this.btnDong.TabIndex = 1;
            this.btnDong.Text = "Đóng";
            this.btnDong.VisualStyleManager = this.vsmMain;
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // cmbPhanLoaiCu
            // 
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Chưa có";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Nguyên phụ liệu";
            uiComboBoxItem6.Value = "1";
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Sản phẩm";
            uiComboBoxItem7.Value = "2";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Thiết bị";
            uiComboBoxItem8.Value = "3";
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Hàng mẫu";
            uiComboBoxItem9.Value = "4";
            this.cmbPhanLoaiCu.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem5,
            uiComboBoxItem6,
            uiComboBoxItem7,
            uiComboBoxItem8,
            uiComboBoxItem9});
            this.cmbPhanLoaiCu.Location = new System.Drawing.Point(88, 88);
            this.cmbPhanLoaiCu.Name = "cmbPhanLoaiCu";
            this.cmbPhanLoaiCu.Size = new System.Drawing.Size(103, 21);
            this.cmbPhanLoaiCu.TabIndex = 2;
            this.cmbPhanLoaiCu.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(9, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Mã hàng";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ctrMaHQCu);
            this.uiGroupBox1.Controls.Add(this.cmbPhanLoaiCu);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtMaHangCu);
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 59);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(331, 119);
            this.uiGroupBox1.TabIndex = 4;
            this.uiGroupBox1.Text = "Thông tin đã khai báo";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaHQCu
            // 
            this.ctrMaHQCu.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaHQCu.Appearance.Options.UseBackColor = true;
            this.ctrMaHQCu.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrMaHQCu.Code = "";
            this.ctrMaHQCu.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaHQCu.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaHQCu.IsOnlyWarning = false;
            this.ctrMaHQCu.IsValidate = true;
            this.ctrMaHQCu.Location = new System.Drawing.Point(88, 52);
            this.ctrMaHQCu.Name = "ctrMaHQCu";
            this.ctrMaHQCu.Name_VN = "";
            this.ctrMaHQCu.SetOnlyWarning = false;
            this.ctrMaHQCu.SetValidate = false;
            this.ctrMaHQCu.ShowColumnCode = true;
            this.ctrMaHQCu.ShowColumnName = true;
            this.ctrMaHQCu.Size = new System.Drawing.Size(220, 21);
            this.ctrMaHQCu.TabIndex = 4;
            this.ctrMaHQCu.TagCode = "";
            this.ctrMaHQCu.TagName = "";
            this.ctrMaHQCu.Where = null;
            this.ctrMaHQCu.WhereCondition = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(9, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Phân loại";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(9, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Mã HQ";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.cbxChonNhieuHang);
            this.uiGroupBox2.Controls.Add(this.txtSoTT);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(690, 53);
            this.uiGroupBox2.TabIndex = 4;
            this.uiGroupBox2.Text = "Dòng hàng muốn sửa đổi";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTT
            // 
            this.txtSoTT.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoTT.DecimalDigits = 0;
            this.txtSoTT.Location = new System.Drawing.Point(170, 20);
            this.txtSoTT.Name = "txtSoTT";
            this.txtSoTT.Size = new System.Drawing.Size(104, 21);
            this.txtSoTT.TabIndex = 5;
            this.txtSoTT.Text = "0";
            this.txtSoTT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTT.VisualStyleManager = this.vsmMain;
            this.txtSoTT.ButtonClick += new System.EventHandler(this.txtSoTT_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(6, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(158, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Số thứ tự của hàng trên tờ khai";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.ctrMaHQMoi);
            this.uiGroupBox3.Controls.Add(this.cmbPhanLoaiMoi);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.label8);
            this.uiGroupBox3.Controls.Add(this.txtMaHangMoi);
            this.uiGroupBox3.Location = new System.Drawing.Point(340, 59);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(331, 119);
            this.uiGroupBox3.TabIndex = 4;
            this.uiGroupBox3.Text = "Thông tin sửa đổi";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaHQMoi
            // 
            this.ctrMaHQMoi.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaHQMoi.Appearance.Options.UseBackColor = true;
            this.ctrMaHQMoi.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrMaHQMoi.Code = "";
            this.ctrMaHQMoi.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaHQMoi.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaHQMoi.IsOnlyWarning = false;
            this.ctrMaHQMoi.IsValidate = true;
            this.ctrMaHQMoi.Location = new System.Drawing.Point(88, 52);
            this.ctrMaHQMoi.Name = "ctrMaHQMoi";
            this.ctrMaHQMoi.Name_VN = "";
            this.ctrMaHQMoi.SetOnlyWarning = false;
            this.ctrMaHQMoi.SetValidate = false;
            this.ctrMaHQMoi.ShowColumnCode = true;
            this.ctrMaHQMoi.ShowColumnName = true;
            this.ctrMaHQMoi.Size = new System.Drawing.Size(220, 21);
            this.ctrMaHQMoi.TabIndex = 4;
            this.ctrMaHQMoi.TagCode = "";
            this.ctrMaHQMoi.TagName = "";
            this.ctrMaHQMoi.Where = null;
            this.ctrMaHQMoi.WhereCondition = "";
            // 
            // cmbPhanLoaiMoi
            // 
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Nguyên phụ liệu";
            uiComboBoxItem1.Value = "1";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Sản phẩm";
            uiComboBoxItem2.Value = "2";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Thiết bị";
            uiComboBoxItem3.Value = "3";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Hàng mẫu";
            uiComboBoxItem4.Value = "4";
            this.cmbPhanLoaiMoi.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cmbPhanLoaiMoi.Location = new System.Drawing.Point(88, 88);
            this.cmbPhanLoaiMoi.Name = "cmbPhanLoaiMoi";
            this.cmbPhanLoaiMoi.Size = new System.Drawing.Size(103, 21);
            this.cmbPhanLoaiMoi.TabIndex = 2;
            this.cmbPhanLoaiMoi.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(9, 93);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Phân loại";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(9, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Mã HQ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(9, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Mã hàng";
            // 
            // txtMaHangMoi
            // 
            this.txtMaHangMoi.Location = new System.Drawing.Point(88, 20);
            this.txtMaHangMoi.Name = "txtMaHangMoi";
            this.txtMaHangMoi.Size = new System.Drawing.Size(220, 21);
            this.txtMaHangMoi.TabIndex = 0;
            this.txtMaHangMoi.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Image = global::Company.Interface.Properties.Resources.disk;
            this.btnGhi.Location = new System.Drawing.Point(515, 184);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 1;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // grvHang
            // 
            this.grvHang.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grvHang.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            grvHang_DesignTimeLayout.LayoutString = resources.GetString("grvHang_DesignTimeLayout.LayoutString");
            this.grvHang.DesignTimeLayout = grvHang_DesignTimeLayout;
            this.grvHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grvHang.GroupByBoxVisible = false;
            this.grvHang.Location = new System.Drawing.Point(3, 213);
            this.grvHang.Name = "grvHang";
            this.grvHang.Size = new System.Drawing.Size(687, 308);
            this.grvHang.TabIndex = 5;
            this.grvHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grvHang.VisualStyleManager = this.vsmMain;
            this.grvHang.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grvHang_RowDoubleClick);
            this.grvHang.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.grvHang_DeletingRecords);
            // 
            // cbxChonNhieuHang
            // 
            this.cbxChonNhieuHang.Location = new System.Drawing.Point(352, 20);
            this.cbxChonNhieuHang.Name = "cbxChonNhieuHang";
            this.cbxChonNhieuHang.Size = new System.Drawing.Size(104, 23);
            this.cbxChonNhieuHang.TabIndex = 6;
            this.cbxChonNhieuHang.Text = "Chọn nhiều hàng";
            // 
            // PhuKienSuaMaHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 507);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "PhuKienSuaMaHang";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Phụ kiện sửa thông tin hàng của tờ khai đưa vào thanh khoản";
            this.Load += new System.EventHandler(this.PhuKienSuaMaHang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvHang)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIComboBox cmbPhanLoaiCu;
        private Janus.Windows.EditControls.UIButton btnDong;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangCu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaHQCu;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTT;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaHQMoi;
        private Janus.Windows.EditControls.UIComboBox cmbPhanLoaiMoi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangMoi;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.GridEX.GridEX grvHang;
        private Janus.Windows.EditControls.UICheckBox cbxChonNhieuHang;
    }
}