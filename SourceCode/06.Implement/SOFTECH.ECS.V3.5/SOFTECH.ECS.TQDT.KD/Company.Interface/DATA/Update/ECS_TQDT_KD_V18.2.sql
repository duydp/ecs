

IF NOT EXISTS(Select * From t_VNACC_Category_Common where ReferenceDB='A402' and Code='1')
INSERT into t_VNACC_Category_Common (ReferenceDB, Code, Name_VN) values ('A402','1',N' Mức thuế tuyệt đối 5000 - xe ô tô chở người từ 9 chỗ ngồi trở xuống (kể cả lái xe) có dung tích xi lanh dưới 1.000cc')
IF NOT EXISTS(Select * From t_VNACC_Category_Common where ReferenceDB='A402' and Code='2')
INSERT into t_VNACC_Category_Common (ReferenceDB, Code, Name_VN) values ('A402','2',N' Mức thuế tuyệt đối 10000 - xe ô tô chở người từ 9 chỗ ngồi trở xuống (kể cả lái xe) có dung tích xi lanh từ 1.000cc đến dưới 1.500cc')
IF NOT EXISTS(Select * From t_VNACC_Category_Common where ReferenceDB='A402' and Code='3')
INSERT into t_VNACC_Category_Common (ReferenceDB, Code, Name_VN) values ('A402','3',N' Mức thuế tuyệt đối 9500 - xe ô tô chở người từ 10 đến 15 chỗ ngồi (kể cả lái xe) có dung tích xi lanh từ 2.000cc trở xuống')
IF NOT EXISTS(Select * From t_VNACC_Category_Common where ReferenceDB='A402' and Code='4')
INSERT into t_VNACC_Category_Common (ReferenceDB, Code, Name_VN) values ('A402','4',N' Mức thuế tuyệt đối 13000 - xe ô tô chở người từ 10 đến 15 chỗ ngồi (kể cả lái xe) có dung tích xi lanh trên 2.000cc đến 3.000cc')
IF NOT EXISTS(Select * From t_VNACC_Category_Common where ReferenceDB='A402' and Code='5')
INSERT into t_VNACC_Category_Common (ReferenceDB, Code, Name_VN) values ('A402','5',N' Mức thuế tuyệt đối 17000 - xe ô tô chở người từ 10 đến 15 chỗ ngồi (kể cả lái xe) có dung tích xi lanh trên 3.000cc')
IF NOT EXISTS(Select * From t_VNACC_Category_Common where ReferenceDB='A402' and Code='6')
INSERT into t_VNACC_Category_Common (ReferenceDB, Code, Name_VN) values ('A402','6',N' Mức thuế tuyệt đối 5000 - xe ô tô chở người từ 9 chỗ ngồi trở xuống (kể cả lái xe) có dung tích xi lanh từ 1.500cc đến dưới 2.500cc')
IF NOT EXISTS(Select * From t_VNACC_Category_Common where ReferenceDB='A402' and Code='7')
INSERT into t_VNACC_Category_Common (ReferenceDB, Code, Name_VN) values ('A402','7',N' Mức thuế tuyệt đối 15000 - xe ô tô chở người từ 9 chỗ ngồi trở xuống (kể cả lái xe) có dung tích xi lanh từ 2.500cc trở lên')
Go

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '18.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('18.2',GETDATE(), N'Cap nhat bieu thue tuyệt đối A402')
END	

