﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.KDT.GC
{
    public partial class HistoryDetailDinhMucForm : Company.Interface.BaseForm
    {
        public List<KDT_GC_DinhMuc_Log> DMCollection = new List<KDT_GC_DinhMuc_Log>();
        public HistoryDetailDinhMucForm()
        {
            InitializeComponent();
        }

        private void HistoryDetailDinhMucForm_Load(object sender, EventArgs e)
        {
            BinData();
        }
        private void BinData()
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = DMCollection;
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaSP.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearchNPL_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["MaNguyenPhuLieu"], ConditionOperator.Contains, txtMaNPL.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH LỊCH SỬ ĐỊNH MỨC.xls";
                sfNPL.Filter = "Excel files| *.xls";
                Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter = new Janus.Windows.GridEX.Export.GridEXExporter();
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    gridEXExporter.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {                
               Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
