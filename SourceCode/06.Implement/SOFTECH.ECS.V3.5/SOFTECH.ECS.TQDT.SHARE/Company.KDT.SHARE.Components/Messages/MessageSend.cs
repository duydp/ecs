﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [Serializable]
    [XmlRoot("Envelope", Namespace="")]    
    public class MessageSend<T>
    {
        [XmlElement("Header")]
        public MessageHeader MessageHeader { get; set; }

        [XmlElement("Body")]
        public MessageBody<T> MessageBody { get; set; }

    }
}
