GO
 IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_ThongKeToKhaiXNK]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThongKeToKhaiXNK]
GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThongKeToKhaiXNK]
-- Database: ECS_TQDT_GC_V5  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, January 23, 2014  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThongKeToKhaiXNK]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT DISTINCT TKMD.*,
CASE TKMD.TrangThaiXuLy WHEN ''0'' THEN N''Chưa khai báo''
											   WHEN ''1'' THEN N''Đã khai báo''
											   WHEN ''2'' THEN N''Đã xác nhận khai báo''
											   WHEN ''3'' THEN N''Thông quan''
											   WHEN ''4'' THEN N''Đang sửa''
											   ELSE N''Đã hủy'' END AS TrangThai,
VD.*,
     CTN.TKMD_ID ,
     CTN.MaDiaDiem1 ,
     CTN.MaDiaDiem2 ,
     CTN.MaDiaDiem3 ,
     CTN.MaDiaDiem4 ,
     CTN.MaDiaDiem5 ,
     CTN.TenDiaDiem ,
     CTN.DiaChiDiaDiem ,
     CTN.InputMessageID ,
     CTN.MessageTag ,
     CTN.IndexTag,DKDT.*,GP.*,KDC.*,ST.*,TG.*,CT.*,TC.*,SCTN.*
FROM dbo.t_KDT_VNACC_ToKhaiMauDich TKMD INNER JOIN dbo.t_KDT_VNACC_HangMauDich HMD ON HMD.TKMD_ID = TKMD.ID
FULL JOIN dbo.t_KDT_VNACC_TK_SoVanDon VD ON VD.TKMD_ID = TKMD.ID
FULL JOIN dbo.t_KDT_VNACC_TK_Container CTN ON CTN.TKMD_ID=TKMD.ID
FULL JOIN dbo.t_KDT_VNACC_TK_DinhKemDienTu DKDT ON DKDT.TKMD_ID = TKMD.ID 
FULL JOIN dbo.t_KDT_VNACC_TK_GiayPhep GP ON GP.TKMD_ID = TKMD.ID 
FULL JOIN dbo.t_KDT_VNACC_TK_KhoanDieuChinh KDC ON KDC.TKMD_ID = TKMD.ID
FULL JOIN dbo.t_KDT_VNACC_TK_PhanHoi_SacThue ST ON ST.Master_ID = TKMD.ID
FULL JOIN dbo.t_KDT_VNACC_TK_PhanHoi_TyGia TG ON TG.Master_ID = TKMD.ID 
FULL JOIN dbo.t_KDT_VNACC_ChiThiHaiQuan CT ON CT.Master_ID = TKMD.ID
FULL JOIN dbo.t_KDT_VNACC_TK_TrungChuyen TC ON TC.TKMD_ID = TKMD.ID
FULL JOIN dbo.t_KDT_VNACC_TK_SoContainer SCTN ON SCTN.Master_id = TKMD.ID
WHERE ' +  @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  

GO

GO
 IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_BaoCaoChiTietHHXNK]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_BaoCaoChiTietHHXNK]
GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_BaoCaoChiTietHHXNK]
-- Database: ECS_TQDT_GC_V5  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, January 23, 2014  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_BaoCaoChiTietHHXNK]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT TKMD.*,
CASE TKMD.TrangThaiXuLy WHEN ''0'' THEN N''Chưa khai báo''
											   WHEN ''1'' THEN N''Đã khai báo''
											   WHEN ''2'' THEN N''Đã xác nhận khai báo''
											   WHEN ''3'' THEN N''Thông quan''
											   WHEN ''4'' THEN N''Đang sửa''
											   ELSE N''Đã hủy'' END AS TrangThai,
					HMD.*,HMDTTK.ID ,
                    HMDTTK.Master_id ,
                    HMDTTK.MaTSThueThuKhac ,
				    CASE WHEN SUBSTRING(HMDTTK.MaTSThueThuKhac,1,1)=''V'' THEN HMDTTK.SoTienThueVaThuKhac ELSE 0 END AS ThueGTGT ,
				    CASE WHEN SUBSTRING(HMDTTK.MaTSThueThuKhac,1,1)=''T'' THEN HMDTTK.SoTienThueVaThuKhac ELSE 0 END AS ThueTTDB ,
				    CASE WHEN SUBSTRING(HMDTTK.MaTSThueThuKhac,1,1)=''M'' THEN HMDTTK.SoTienThueVaThuKhac ELSE 0 END AS ThueBVMT ,
				    CASE WHEN SUBSTRING(HMDTTK.MaTSThueThuKhac,1,1) IN (''B'',''G'',''C'',''P'',''D'',''E'') THEN HMDTTK.SoTienThueVaThuKhac ELSE 0 END AS ThueTVCBPG ,
                    HMDTTK.MaMGThueThuKhac ,
                    HMDTTK.SoTienGiamThueThuKhac ,
                    HMDTTK.TenKhoanMucThueVaThuKhac ,
                    HMDTTK.TriGiaTinhThueVaThuKhac ,
                    HMDTTK.SoLuongTinhThueVaThuKhac ,
                    HMDTTK.MaDVTDanhThueVaThuKhac ,
                    HMDTTK.ThueSuatThueVaThuKhac ,
                    HMDTTK.SoTienThueVaThuKhac ,
                    HMDTTK.DieuKhoanMienGiamThueVaThuKhac FROM dbo.t_KDT_VNACC_ToKhaiMauDich TKMD INNER JOIN dbo.t_KDT_VNACC_HangMauDich HMD ON HMD.TKMD_ID = TKMD.ID INNER JOIN dbo.t_KDT_VNACC_HangMauDich_ThueThuKhac HMDTTK ON HMDTTK.Master_id = HMD.ID
WHERE ' +  @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  

GO

GO
 IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_BaoCaoTongHopHHXNK]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_BaoCaoTongHopHHXNK]
GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_BaoCaoTongHopHHXNK]
-- Database: ECS_TQDT_GC_V5  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, January 23, 2014  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_BaoCaoTongHopHHXNK]  
 @WhereCondition NVARCHAR(500),  
 @GroupByExpression NVARCHAR(MAX) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'				   SELECT 
				   DISTINCT
				   TKMD.MaDonVi,
				   TKMD.TenDonVi,
				   TKMD.TenDoiTac,
				   HMD.MaHangHoa,
				   HMD.TenHang,
				   HMD.MaSoHang,
				   HMD.DVTLuong1,
				   SUM(HMD.SoLuong1) AS SoLuong1,
				   SUM(HMD.TriGiaHoaDon) AS TriGiaHoaDon,
				   SUM(HMD.ThueSuat) AS ThueSuat,
				   SUM(HMD.TriGiaTinhThueS) AS TriGiaTinhThue,
				   SUM(HMD.SoTienMienGiam) AS SoTienMienGiam,
				   SUM(HMD.SoTienGiamThue) AS SoTienGiamThue,
				   CASE WHEN SUBSTRING(MaTSThueThuKhac,1,1)=''V'' THEN SUM(SoTienThueVaThuKhac) ELSE 0 END AS ThueGTGT ,
				   CASE WHEN SUBSTRING(MaTSThueThuKhac,1,1)=''T'' THEN SUM(SoTienThueVaThuKhac) ELSE 0 END AS ThueTTDB ,
				   CASE WHEN SUBSTRING(MaTSThueThuKhac,1,1)=''M'' THEN SUM(SoTienThueVaThuKhac) ELSE 0 END AS ThueBVMT ,
				   CASE WHEN SUBSTRING(MaTSThueThuKhac,1,1) IN (''B'',''G'',''C'',''P'',''D'',''E'') THEN SUM(SoTienThueVaThuKhac) ELSE 0 END AS ThueTVCBPG 
				   FROM dbo.t_KDT_VNACC_ToKhaiMauDich TKMD INNER JOIN dbo.t_KDT_VNACC_HangMauDich HMD ON HMD.TKMD_ID = TKMD.ID INNER JOIN dbo.t_KDT_VNACC_HangMauDich_ThueThuKhac HMDTTK ON HMDTTK.Master_id = HMD.ID 
WHERE ' +  @WhereCondition  

BEGIN  
 SET @SQL = @SQL + 'GROUP BY TKMD.MaDonVi,TKMD.TenDonVi,TKMD.TenDoiTac,HMD.MaHangHoa,HMD.TenHang,HMD.MaSoHang,HMD.DVTLuong1,HMDTTK.MaTSThueThuKhac' + @GroupByExpression  
END  
  
EXEC sp_executesql @SQL  

GO

  
  
GO
 IF OBJECT_ID(N'[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_ThongKeToKhaiAMA]') IS NOT NULL
	DROP PROCEDURE [p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_ThongKeToKhaiAMA]
GO  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_ThongKeToKhaiAMA]  
-- Database: ECS_TQDT_KD_V5  
-- Author: Ngo Thanh Tung  
-- Time created: Friday, November 01, 2013  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_ThongKeToKhaiAMA]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT TKBS.*,
CASE TKBS.TrangThaiXuLy WHEN ''0'' THEN N''Chưa khai báo'' 
											   WHEN ''1'' THEN N''Đã khai báo''
											   WHEN ''2'' THEN N''Đã xác nhận khai báo''
											   WHEN ''3'' THEN N''Đã duyệt''
											   ELSE N''Từ chối'' END AS TrangThai,
HMDKBS.*,
HMDKBSTTK.* 
FROM dbo.t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBS 
INNER JOIN dbo.t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue HMDKBS 
ON HMDKBS.TKMDBoSung_ID = TKBS.ID 
FULL JOIN dbo.t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac HMDKBSTTK 
ON HMDKBSTTK.HMDBoSung_ID = HMDKBS.ID 
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  
  
  

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '37.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('37.2',GETDATE(), N'CẬP NHẬT PROCEDURE KẾT XUẤT DỮ LIỆU')
END