using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BK07Form : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public BKNPLNopThueTieuThuNoiDiaCollection bkCollection = new BKNPLNopThueTieuThuNoiDiaCollection();
        private int SoToKhai;
        private string MaLoaiHinh;
        private DateTime NgayDangKy;
        private string TenNPL;
        private string DVT_ID;
        private decimal SoLuong;
        private int Postion;
        public BK07Form()
        {
            InitializeComponent();
        }

        private void dgList1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {

                this.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Value);
                this.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
                this.TenNPL = e.Row.Cells["TenHang"].Text;
                this.DVT_ID = Convert.ToString(e.Row.Cells["DVT_ID"].Value);
                this.SoLuong = Convert.ToDecimal(e.Row.Cells["Ton"].Value);
                this.MaHaiQuan = Convert.ToString(e.Row.Cells["MaHaiQuan"].Value);
                txtSoToKhai.Text = e.Row.Cells["SoToKhai"].Text;
                txtMaNPL.Text = e.Row.Cells["Ma"].Text;
                btnSave.Enabled = false;
                btnAdd.Enabled = true;
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (cvError.IsValid)
            {
                if (checkExit(this.SoToKhai, this.MaLoaiHinh, Convert.ToInt16(this.NgayDangKy.Year), this.MaHaiQuan.Trim(), txtMaNPL.Text))
                {
                    // ShowMessage("Nguyên phụ liệu của tờ khai này đã có, bạn hãy chọn nguyên phụ liệu khác", false);
                    MLMessages("Nguyên phụ liệu của tờ khai này đã có, bạn hãy chọn nguyên phụ liệu khác", "MSG_THK81", "", false);

                    txtSoToKhai.Text = "";
                    txtLuong.Value = 0;
                    txtMaNPL.Text = "";
                    return;
                }
                if (this.SoLuong < Convert.ToDecimal(txtLuong.Value))
                {
                    //ShowMessage("Lượng nộp thuế phải nhỏ hơn lượng nguyên phụ liệu tồn của tờ khai.", false);
                    MLMessages("Lượng nộp thuế phải nhỏ hơn lượng nguyên phụ liệu tồn của tờ khai.", "MSG_THK81", "", false);
                    txtLuong.Value = 0;
                    txtLuong.Focus();
                    return;
                }
                BKNPLNopThueTieuThuNoiDia bk = new BKNPLNopThueTieuThuNoiDia();
                bk.BangKeHoSoThanhLy_ID = 0;
                bk.SoToKhai = this.SoToKhai;
                bk.MaLoaiHinh = this.MaLoaiHinh;
                bk.NgayDangKy = this.NgayDangKy;
                bk.MaHaiQuan = this.MaHaiQuan.Trim();
                bk.NamDangKy = Convert.ToInt16(this.NgayDangKy.Year);
                bk.MaNPL = txtMaNPL.Text;
                bk.TenNPL = this.TenNPL;
                bk.DVT_ID = this.DVT_ID;
                bk.TenDVT = DonViTinh_GetName(this.DVT_ID);
                bk.LuongNopThue = Convert.ToDecimal(txtLuong.Value);
                this.bkCollection.Add(bk);
                dgList2.Refetch();
                txtSoToKhai.Text = "";
                txtMaNPL.Text = "";
                txtLuong.Value = 0;
            }
        }
        private bool checkExit(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan, string maNPL)
        {
            foreach (BKNPLNopThueTieuThuNoiDia bk in this.bkCollection)
            {
                if (bk.SoToKhai == soToKhai && bk.MaLoaiHinh == maLoaiHinh && bk.NamDangKy == namDangKy && bk.MaHaiQuan == maHaiQuan && bk.MaNPL == maNPL)
                    return true;
            }
            return false;
        }
        private void BK07Form_Load(object sender, EventArgs e)
        {
            dgList2.Tables[0].Columns["LuongNopThue"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList1.Tables[0].Columns["Ton"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

            txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            if (this.HSTL.TrangThaiThanhKhoan == 401)
            {
                btnAdd.Enabled = cmdSave.Enabled = false;
                dgList2.AllowDelete = InheritableBoolean.False;
            }
            dgList1.DataSource = new BKToKhaiNhap().getNPL_BKToKhaiNhap(this.HSTL.BKCollection[this.HSTL.getBKToKhaiNhap()].ID).Tables[0];
            dgList2.DataSource = this.bkCollection;
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            if (this.bkCollection.Count == 0)
            {
                ShowMessage("Bạn chưa nhập NPL nộp thuế tiêu thụ nội địa.", false);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (this.HSTL.getBKNPLNopThue() < 0)
                {
                    BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
                    bk.MaterID = this.HSTL.ID;
                    bk.STTHang = this.HSTL.BKCollection.Count + 1;
                    bk.MaBangKe = "DTLNPLNT";
                    bk.TenBangKe = "DTLNPLNT";
                    bk.bkNPLNTCollection = this.bkCollection;
                    this.HSTL.BKCollection.Add(bk);
                }
                else
                {
                    this.HSTL.BKCollection[this.HSTL.getBKNPLNopThue()].bkNPLNTCollection = this.bkCollection;
                }
                this.HSTL.BKCollection[this.HSTL.getBKNPLNopThue()].InsertUpdate_BKNPLNT(this.bkCollection);
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi lưu bảng kê: " + ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void dgList1_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
                e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
            }
        }

        private void dgList2_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
        }

        //private void dgList2_UpdatingCell(object sender, UpdatingCellEventArgs e)
        //{
        //    if (e.Column.Key == "LuongNopThue")
        //    {
        //        if (Convert.ToDecimal(e.Value)>this.SoLuong)
        //        {
        //            ShowMessage("Lượng nộp thuế phải nhỏ hơn số lượng nguyên phụ liệu của tờ khai.", false);
        //            e.Cancel = true;
        //        }
        //    }

        //}

        private void dgList2_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            txtSoToKhai.Text = e.Row.Cells["SoToKhai"].Text;
            this.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Value);
            this.MaLoaiHinh = Convert.ToString(e.Row.Cells["MaLoaiHinh"].Value);
            this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
            this.TenNPL = e.Row.Cells["TenNPL"].Text;
            this.DVT_ID = e.Row.Cells["DVT_ID"].Text;
            this.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
            txtMaNPL.Text = e.Row.Cells["MaNPL"].Text;
            txtLuong.Value = Convert.ToDecimal(e.Row.Cells["LuongNopThue"].Value);
            this.Postion = e.Row.Position;
            btnSave.Enabled = true;
            btnAdd.Enabled = false;
            this.SoLuong = new BKToKhaiNhap().getLuongTon(this.SoToKhai, this.MaLoaiHinh, this.MaHaiQuan.Trim(), Convert.ToInt16(this.NgayDangKy.Year), txtMaNPL.Text);

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (cvError.IsValid)
            {
                if (this.SoLuong < Convert.ToDecimal(txtLuong.Value))
                {
                    // ShowMessage("Lượng nộp thuế phải nhỏ hơn lượng nguyên phụ liệu tồn của tờ khai.", false);
                    MLMessages("Lượng nộp thuế phải nhỏ hơn lượng nguyên phụ liệu tồn của tờ khai.", "MSG_THK81", "", false);
                    txtLuong.Value = 0;
                    txtLuong.Focus();
                    return;
                }
                BKNPLNopThueTieuThuNoiDia bk = new BKNPLNopThueTieuThuNoiDia();
                bk.BangKeHoSoThanhLy_ID = 0;
                bk.SoToKhai = this.SoToKhai;
                bk.MaLoaiHinh = this.MaLoaiHinh;
                bk.NgayDangKy = this.NgayDangKy;
                bk.MaHaiQuan = this.MaHaiQuan.Trim();
                bk.NamDangKy = Convert.ToInt16(this.NgayDangKy.Year);
                bk.MaNPL = txtMaNPL.Text;
                bk.TenNPL = this.TenNPL;
                bk.DVT_ID = this.DVT_ID;
                bk.TenDVT = DonViTinh_GetName(this.DVT_ID);
                bk.LuongNopThue = Convert.ToDecimal(txtLuong.Value);
                this.bkCollection[this.Postion] = bk;
                dgList2.Refetch();
                txtSoToKhai.Text = "";
                txtMaNPL.Text = "";
                txtLuong.Value = 0;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}