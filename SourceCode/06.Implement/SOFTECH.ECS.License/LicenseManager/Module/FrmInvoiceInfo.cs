using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Activation.Server;
using DevExpress.XtraGrid.Columns;

namespace Licenses_v2.Module
{
    public partial class FrmInvoiceInfo : DevExpress.XtraEditors.XtraForm
    {
        private Invoice Inv;
        public Boolean isEdit = false;

        public FrmInvoiceInfo(string ID)
        {
            InitializeComponent();
            cboxCustID.EditValue = DBNull.Value;
            if (ID == null)
            {
                txtInvID.Text = Math.Abs(DateTime.Now.GetHashCode()).ToString();
                this.Text = "Thêm hóa đơn";
                lblCaption.Text = "Thêm hóa đơn";
                Inv = new Invoice();
                this.Name = "Inv_New";
                this.isEdit = true;
                txtInvCreateDate.Text = DateTime.Now.ToString();
                txtInvEmployeeID.Text = Program.LoginID;
                this.DisableInvoiceDetail(false);
            }
            else
            {
                Inv = Invoice.Load(ID);
                this.Text = "Hóa đơn: " + ID;
                lblCaption.Text = "Hóa đơn: " + ID;
                this.Name = "Inv_" + ID;
                this.FillData();
            }
        }

        private void FrmInvoiceInfo_Load(object sender, EventArgs e)
        {
            this.RefreshCustBox();
            this.DisableButton(this.isEdit);
        }

        private void cboxCustID_EditValueChanged(object sender, EventArgs e)
        {
            this.FillCustomer();
        }

        #region Add Methods.

        public void SelectCustomer(string ID)
        {
            this.RefreshCustBox();
            cboxCustID.EditValue = ID;
        }

        private void RefreshCustBox()
        {
            cboxCustID.Properties.DataSource = Customer.SelectAll().Tables[0];
            this.FillCustomer();
        }
        private void GetData()
        {
            Inv.ID = txtInvID.Text;
            Inv.CustomerID = cboxCustID.EditValue.ToString();
            Inv.Notes = txtInvNotes.Text;
            Inv.EmployeeID = txtInvEmployeeID.Text;
            Inv.CreateDate = DateTime.Parse(txtInvCreateDate.Text);
        }

        private void FillData()
        {
            txtInvID.Text = Inv.ID;
            cboxCustID.EditValue = Inv.CustomerID;
            txtInvNotes.Text = Inv.Notes;
            txtInvEmployeeID.Text = Inv.EmployeeID;
            txtInvCreateDate.Text = Inv.CreateDate.ToShortDateString();

            this.RefershListOrder();
        }

        private DataTable tblInvoiceDetail()
        {
            //Commented by HungTQ, 21/05/2012.
            //DataTable dt = InvoiceDetail.SelectBy_InvoiceID(Inv.ID).Tables[0];

            DataTable dt = InvoiceDetail.SelectBy_InvoiceID_Extend(Inv.ID).Tables[0];

            dt.Columns["ID"].Caption = "ID";
            dt.Columns["InvoiceID"].Caption = "Mã hóa đơn";
            dt.Columns["ProductID"].Caption = "Mã sản phẩm";
            dt.Columns["ProductName"].Caption = "Tên sản phẩm";
            dt.Columns["LicenseID"].Caption = "Mã bản quyền";
            dt.Columns["LicenseName"].Caption = "Tên bản quyền";
            dt.Columns["SerialCode"].Caption = "Serial";
            dt.Columns["PaymentTerms"].Caption = "Thời hạn";
            dt.Columns["Price"].Caption = "Giá thành";

            dt.Columns.Remove("InvoiceID");
            dt.Columns.Remove("ProductID");
            dt.Columns.Remove("LicenseID");

            dt.TableName = "tblInvoiceDetail";

            return dt;
        }

        private void FillCustomer()
        {
            if (cboxCustID.EditValue != DBNull.Value)
            {
                Customer Cust = Customer.Load(cboxCustID.EditValue.ToString());
                txtCustName.Text = Cust.Name;
                txtCustFirstName.Text = Cust.ContactFirstName;
                txtCustLastName.Text = Cust.ContactLastName;
                txtCustPhone.Text = Cust.PhoneNumber;
                txtCustAddress.Text = Cust.BillingAddress;
                txtCustCity.Text = Cust.City;
                txtCustEmail.Text = Cust.Email;
            }
        }

        private void DisableButton(Boolean val)
        {
            btnCustEdit.Enabled = val;
            btnCustNew.Enabled = val;
            txtInvNotes.Properties.ReadOnly = !val;
            cboxCustID.Enabled = val;

            btnInvCancel.Enabled = val;
            btnInvSave.Enabled = val;
            btnInvEdit.Enabled = !val;
        }

        private void DisableInvoiceDetail(Boolean val)
        {
            btnInvDetailDelete.Enabled = val;
            btnInvDetailEdit.Enabled = val;
            btnInvDetailNew.Enabled = val;
        }

        public void RefershListOrder()
        {
            DataTable dtInvoieDetail = this.tblInvoiceDetail();
            gridInvDetail.DataSource = dtInvoieDetail;

            if (ClassLib.IsFormShowed("FrmInvoiceManager"))
            {
                FrmInvoiceManager obj = (FrmInvoiceManager)Application.OpenForms["FrmInvoiceManager"];
                obj.RefreshData();
            }
        }
        #endregion

        #region Button Invoice Detail.
        private void btnInvDetailNew_Click(object sender, EventArgs e)
        {
            FrmInvoiceDetailInfo obj = new FrmInvoiceDetailInfo(null, Inv.ID);
            obj.FormBorderStyle = FormBorderStyle.None;
            obj.StartPosition = FormStartPosition.CenterParent;
            obj.ShowDialog();
        }

        private void btnInvDetailEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int i = gridData.GetSelectedRows()[0];
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    FrmInvoiceDetailInfo obj = new FrmInvoiceDetailInfo(ID, Inv.ID);
                    obj.FormBorderStyle = FormBorderStyle.None;
                    obj.StartPosition = FormStartPosition.CenterParent;
                    obj.isEdit = true;
                    obj.ShowDialog();
                }
            }
            catch
            {

            }
        }

        private void btnInvDetailDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int i = gridData.GetSelectedRows()[0];
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    InvoiceDetail.DeleteInvoiceDetail(ID);
                }
                this.RefershListOrder();
            }
            catch
            {

            }
        }
        #endregion

        #region Button Customer.
        private void btnCustNew_Click(object sender, EventArgs e)
        {
            FrmCustomerInfo obj = new FrmCustomerInfo(null);
            obj.FormBorderStyle = FormBorderStyle.None;
            obj.StartPosition = FormStartPosition.CenterParent;
            obj.objInfo = this;
            obj.ShowDialog();
        }

        private void btnCustEdit_Click(object sender, EventArgs e)
        {
            FrmCustomerInfo obj = new FrmCustomerInfo(cboxCustID.EditValue.ToString());
            obj.FormBorderStyle = FormBorderStyle.None;
            obj.StartPosition = FormStartPosition.CenterParent;
            obj.objInfo = this;
            obj.isEdit = true;
            obj.ShowDialog();
        }
        #endregion

        #region Button Invoice.
        private void btnInvEdit_Click(object sender, EventArgs e)
        {
            this.isEdit = true;
            this.DisableButton(true);
        }

        private void btnInvSave_Click(object sender, EventArgs e)
        {
            if (cboxCustID.EditValue != DBNull.Value)
            {
                this.GetData();
                if (this.Name == "Inv_New")
                {
                    Inv.Insert();
                    this.Text = "Hóa đơn: " + Inv.ID;
                    lblCaption.Text = "Hóa đơn: " + Inv.ID;
                    this.Name = "Inv_" + Inv.ID;
                    this.DisableInvoiceDetail(true);
                }
                else
                {
                    Inv.Update();
                }
                this.DisableButton(false);
                if (ClassLib.IsFormShowed("FrmInvoiceManager"))
                {
                    FrmInvoiceManager obj = (FrmInvoiceManager)Application.OpenForms["FrmInvoiceManager"];
                    obj.RefreshData();
                }
            }
            else MessageBox.Show("Kiểm tra thông tin");
        }

        private void btnInvCancel_Click(object sender, EventArgs e)
        {
            if (this.Name == "Inv_New")
            {
                this.Close();
            }
            else
            {
                this.FillData();
                this.DisableButton(false);
            }
        }

        private void btnInvClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void gridInvDetail_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                int i = gridData.GetSelectedRows()[0];
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    FrmInvoiceDetailInfo obj = new FrmInvoiceDetailInfo(ID, Inv.ID);
                    obj.FormBorderStyle = FormBorderStyle.None;
                    obj.StartPosition = FormStartPosition.CenterParent;
                    obj.isEdit = false;
                    obj.ShowDialog();
                }
            }
            catch
            {

            }
        }
    }
}