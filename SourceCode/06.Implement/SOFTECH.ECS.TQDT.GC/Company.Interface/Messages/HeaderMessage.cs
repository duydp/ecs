﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
namespace Company.KDT.SHARE.Components.Messages
{
    public class HeaderMessage
    {

        XmlDocument doc = null;
        public HeaderMessage(XmlDocument doc)
        {
            this.doc = doc;
        }
        public HeaderMessage(string xml)
        {
            doc.LoadXml(xml);
        }
        public string From
        {
            get
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/From/identity");
                return node != null ? node.InnerText : string.Empty;
            }
            set
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/From/identity");
                node.InnerText = value;
            }
        }
        public string FromName
        {
            get
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/From/name");
                return node != null ? node.InnerText : string.Empty;
            }
            set
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/From/name");
                node.InnerText = value;
            }
        }
        public string To
        {
            get
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/To/identity");
                return node != null ? node.InnerText : string.Empty;
            }
            set
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/To/identity");
                node.InnerText = value;
            }
        }
        public string ToName
        {
            get
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/To/name");
                return node != null ? node.InnerText : string.Empty;
            }
            set
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/To/name");
                node.InnerText = value;
            }
        }
        public string Reference
        {
            get
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/Subject/reference");
                return node != null ? node.InnerText : string.Empty;
            }
            set
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/Subject/reference");
                node.InnerText = value;
            }
        }
        public string MessageId
        {
            get
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/Reference/messageId");
                return node != null ? node.InnerText : string.Empty;
            }
            set
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/Reference/messageId");
                node.InnerText = value;
            }
        }
        public int? MessageType
        {
            get
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/Reference/type");
                if (node != null)
                    return Convert.ToInt32(node.InnerText);
                else
                    return null;            
            }
            set
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/Reference/type");
                node.InnerText = value.Value.ToString();
            }
        }
        public long? FunctionType
        {
            get
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/Reference/function");
                if (node != null)
                    return Convert.ToInt32(node.InnerText);
                else
                    return null;
            }
            set
            {
                XmlNode node = doc.SelectSingleNode("Envelope/Header/Reference/function");
                node.InnerText = value.Value.ToString();
            }
        }
    }
}
