﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
#if GC_V3 || GC_V4
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
#endif
namespace Company.Interface.Report
{
    
    public partial class ReportViewTKTGA4Form : BaseForm
    {
        public ToKhaiTriGiaA4 ToKhaiChinhReport = new ToKhaiTriGiaA4();
        public PhuLucTriGiaA4 PhuLucReport = new PhuLucTriGiaA4();
        public ToKhaiTriGia TKTG = new ToKhaiTriGia();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public XRControl Cell = new XRControl();
        public DataTable dt = new DataTable();
        public ReportViewTKTGA4Form()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            
            if (this.TKTG.HTGCollection.Count >8)
            {
                //int count = (this.TKTG.HTGCollection .Count - 1) / 8 + 1;
               // for (int i = 0; i < count; i++)
                    this.AddItemComboBox1();
            }
            cboToKhai.SelectedIndex = 0;
          
         
            if (TKTG.HTGCollection.Count > 8)
            {
                dt.TableName = "dtPhuLuc";
                for (int i = 1; i <= 20; i++)
                {
                    dt.Columns.Add("ColumnName" + i, typeof(string));
                } 
                foreach (HangTriGia htg in TKTG.HTGCollection)
                {
                    dt.Rows.Add(htg.STTHang, htg.STTHang, convert(htg.GiaTrenHoaDon), convert(htg.KhoanThanhToanGianTiep), convert(htg.TraTruoc), convert(htg.HoaHong), convert(htg.ChiPhiBaoBi), convert(htg.ChiPhiDongGoi), convert(htg.TroGiup), convert(htg.BanQuyen), convert(htg.TienTraSuDung), convert(htg.ChiPhiVanChuyen), convert(htg.PhiBaoHiem), convert(htg.ChiPhiNoiDia), convert(htg.ChiPhiPhatSinh), convert(htg.TienLai), convert(htg.TienThue), convert(htg.GiamGia), convert(htg.TriGiaNguyenTe), convert(htg.TriGiaVND));
                }
            }
         }

        private string convert(double value)
        {
            if (value > 0)
                return value.ToString("N2");
            else
                return "";
        }
        
        public void AddItemComboBox()
        {
            cboToKhai.Items.Add("Phụ lục " + cboToKhai.Items.Count, cboToKhai.Items.Count);
        }
        public void AddItemComboBox1()
        {
            cboToKhai.Items.Add("Phụ lục ");
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.TKTG = this.TKTG;
                this.ToKhaiChinhReport.TKMD = this.TKMD;
                this.ToKhaiChinhReport.report = this;
                this.ToKhaiChinhReport.BindReport();
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                HangTriGiaCollection HTGReportCollection = new HangTriGiaCollection();               
                int begin = (cboToKhai.SelectedIndex - 1) * 8;
                int end = cboToKhai.SelectedIndex * 8;
             //   if (end > this.TKTG.HTGCollection.Count) end = this.TKTG.HTGCollection.Count;
            //    for (int i = begin; i < end; i++)
             //       HTGReportCollection.Add(this.TKTG.HTGCollection[i]);
                this.PhuLucReport = new PhuLucTriGiaA4 ();
                this.PhuLucReport.dt = this.dt;
                this.PhuLucReport.report = this;
                this.PhuLucReport.TKTG = this.TKTG;
                this.PhuLucReport.TKMD = this.TKMD;
                //if(this.TKTG.NgayKhaiBao != new DateTime(1900,1,1))
                  //e  this.PhuLucReport.ng = this.TKTG.NgayKhaiBao;
                    this.PhuLucReport.HTGCollection = HTGReportCollection;
                this.PhuLucReport.BindReport();
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.ToKhaiChinhReport.CreateDocument();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //switch (cboExport.SelectedValue.ToString())
            //{
            //    case "pdf":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportPdf, new object[] { true });
            //        break;
            //    case "excel":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            //        break;

            //}
           
        }


        private void uiButton3_Click(object sender, EventArgs e)
        {
            //if (cboToKhai.SelectedIndex == 0)
            //{
            //    this.ToKhaiChinhReport.setNhomHang(this.Cell, txtTenNhomHang.Text);
            //    this.ToKhaiChinhReport.CreateDocument();
            //}
            //else
            //{
            //    this.PhuLucReport.setNhomHang(this.Cell, txtTenNhomHang.Text);
            //    this.PhuLucReport.CreateDocument();
            //}
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch (Exception ex) { MessageBox.Show("Lỗi : " + ex.Message); }

        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                this.ToKhaiChinhReport.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                this.PhuLucReport.setVisibleImage(true);
                this.PhuLucReport.CreateDocument();
            }
        }

        

        private void chkHinhNen_CheckedChanged(object sender, EventArgs e)
        {

        }

 

        private void chkInBanLuuHaiQuan_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPdf.Checked)
            {
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportPdf, new object[] { true });
                }
                catch (Exception ex) { MessageBox.Show("Lỗi : " + ex.Message); }
            }
        }

        

        private void btnExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch (Exception ex) { MessageBox.Show("Lỗi : " + ex.Message); }
        }

        
    }
}