﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml.Serialization;

namespace KeyServer
{
    [XmlRoot("Messages")]
    public class StatusMessages
    {
        public string Licensed { get; set; }
        public string TrialVersion { get; set; }
        public string Expired { get; set; }
        public string MachineHashMismatch { get; set; }
        public string NotFound { get; set; }
        public string Invalid { get; set; }
        public string InternalError { get; set; }
        public string Lock { get; set; }
    }
}
