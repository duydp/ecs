﻿

using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using System.IO;
using Company.Interface.Report;
using Company.GC.BLL.KDT.SXXK;
using System.Xml.Serialization;
using Company.Interface.KDT.GC;

namespace Company.Interface
{
    public partial class ToKhaiMauDichRegistedForm : BaseForm
    {
        public System.Data.DataTable dt = new System.Data.DataTable();

        public ToKhaiMauDichRegistedForm()
        {
            InitializeComponent();
        }

        private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                dgList.DataSource = this.dt;

                //TODO: Hungtq, 18/10/2014. hien thi thong tin cot so to khai thay vi so to khai vnaccs, do thong tin du lieu tra ve chung cho cot SoToKhai
                dgList.RootTable.Columns["SoToKhai"].Visible = false;
                dgList.RootTable.Columns["SoToKhaiVNACCS"].Visible = true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["MaLoaiHinh"].Text = this.LoaiHinhMauDich_GetName(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void menuExportExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.FileName = "Danh sách tờ khai _" +DateTime.Now.ToString("dd/MM/yyyy").Replace("/","_")+".xls";
            sf.Filter = "Excel File | *.xls ";

            if (sf.ShowDialog(this) == DialogResult.OK)
            {
                Janus.Windows.GridEX.Export.GridEXExporter grdExport = new Janus.Windows.GridEX.Export.GridEXExporter();
                grdExport.GridEX = dgList;

                try
                {
                    System.IO.Stream str = sf.OpenFile();
                    grdExport.Export(str);
                    str.Close();
                    if (ShowMessage("Bạn có muốn mở File này không", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sf.FileName);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi trong quá trình mở File" + ex.Message, false);
                }
            }
            else
            {
                ShowMessage("Xuất Excel không thành công",false);
            }

        }

    }
}