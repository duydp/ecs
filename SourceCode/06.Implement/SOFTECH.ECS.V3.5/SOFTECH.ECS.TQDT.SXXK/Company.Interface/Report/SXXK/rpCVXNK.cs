﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class rpCVXNK : DevExpress.XtraReports.UI.XtraReport
    {
        //private ToKhaiMauDich TKMD = null;

        public rpCVXNK(ToKhaiMauDich tkmd)
        {
            InitializeComponent();
            lblDoanhNghiep.Text = GlobalSettings.TEN_DON_VI.ToUpper();
            lblNgaythang.Text = GlobalSettings.TieudeNgay;
            lblChiCuc1.Text = "- " + DonViHaiQuan.GetName(tkmd.MaHaiQuan).ToUpper();
            
            this.dispReport(tkmd);
        }

        private void dispReport(ToKhaiMauDich TKMD)
        { 
            TKMD.LoadHMDCollection();
            if (TKMD.HMDCollection.Count > 0)
            {
                HangMauDich hmd = TKMD.HMDCollection[0];
                lblTenHang.Text = hmd.TenHang;
                lblSoLuong.Text = hmd.SoLuong.ToString() + " " + DonViTinh.GetName(hmd.DVT_ID);
                lblHDXK.Text = TKMD.SoHopDong;
                lblVanDon.Text = TKMD.SoVanDon;
                lblTenKH.Text = TKMD.TenDonViDoiTac;
            }
        }

        private void lbl_PreviewDoubleClick(object sender, PreviewMouseEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            DoiNoiDung obj = new DoiNoiDung(lbl.Text);
            obj.ShowDialog();
            if (obj.NoiDungMoi != "")
            {
                lbl.Text = obj.NoiDungMoi.Replace(@"\n", "\n");
                this.CreateDocument();
            }
        }

        private void cellSoCV_PreviewDoubleClick(object sender, PreviewMouseEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            DoiNoiDung obj = new DoiNoiDung(cell.Text);
            obj.ShowDialog();
            if (obj.NoiDungMoi != "")
            {
                cell.Text = obj.NoiDungMoi.Replace(@"\n", "\n");
                this.CreateDocument();
            }
        }

        public void dispBackColor(bool val)
        {
            if (val)
            {
                cellSoCV.BackColor = Color.Silver;
                lblNgaythang.BackColor = Color.Silver;               
                lblChiCuc1.BackColor = Color.Silver;
                lblChiCuc2.BackColor = Color.Silver;
                lblSoBill.BackColor = Color.Silver;
                lblTenHang.BackColor = Color.Silver;
                lblSoLuong.BackColor = Color.Silver;
                lblVanDon.BackColor = Color.Silver;
                lblHDXK.BackColor = Color.Silver;
                lblTenKH.BackColor = Color.Silver;
                lblGiamDoc.BackColor = Color.Silver;
            }
            else
            {
                cellSoCV.BackColor = Color.Transparent;
                lblNgaythang.BackColor = Color.Transparent;               
                lblChiCuc1.BackColor = Color.Transparent;
                lblChiCuc2.BackColor = Color.Transparent;
                lblSoBill.BackColor = Color.Transparent;
                lblTenHang.BackColor = Color.Transparent;
                lblSoLuong.BackColor = Color.Transparent;
                lblVanDon.BackColor = Color.Transparent;
                lblHDXK.BackColor = Color.Transparent;
                lblTenKH.BackColor = Color.Transparent;
                lblGiamDoc.BackColor = Color.Transparent;
            }
            this.CreateDocument();
        }
    }
}
