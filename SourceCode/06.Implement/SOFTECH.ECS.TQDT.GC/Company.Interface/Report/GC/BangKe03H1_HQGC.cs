﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;
 

namespace Company.Interface.Report.GC
{
    public partial class BangKe03H1_HQGC : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HD = new HopDong();
        public BangKe03H1_HQGC()
        {
            InitializeComponent();
        }
        public void BindReport(DataTable dt)
        {
            //this.PrintingSystem.ShowMarginsWarning = false;
            //DataTable dt = new Company.GC.BLL.GC.ThanhKhoanHDGC().SelectDynamic("" + dk, "").Tables[0]; 

            dt.TableName = "DataTable";
            this.DataSource = dt;
            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;
            lblSoLuong.Text = "";
            lblDateFirst.Text = this.HD.NgayDangKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong; 

            if (dt.Columns[4].Caption.ToString() != "")
                lbltkSo1.Text = dt.Columns[4].Caption + "/" + GlobalSettings.TEN_HAI_QUAN_NGAN.Substring(0, 3);
            if (dt.Columns[5].Caption.ToString() != "")
                lbltkSo2.Text = dt.Columns[5].Caption + "/" + GlobalSettings.TEN_HAI_QUAN_NGAN.Substring(0, 3);
            if (dt.Columns[6].Caption.ToString() != "")
                lbltkSo3.Text = dt.Columns[6].Caption + "/" + GlobalSettings.TEN_HAI_QUAN_NGAN.Substring(0, 3);
            if (dt.Columns[7].Caption.ToString() != "")
                lbltkSo4.Text = dt.Columns[7].Caption + "/" + GlobalSettings.TEN_HAI_QUAN_NGAN.Substring(0, 3);
            if (dt.Columns[8].Caption.ToString() != "")
                lbltkSo5.Text = dt.Columns[8].Caption + "/" + GlobalSettings.TEN_HAI_QUAN_NGAN.Substring(0, 3);
           
            
            lblMaHang.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenTB");
            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");
            lblLuong1.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[4].ColumnName, "{0:N3}");
            lblLuong2.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[5].ColumnName, "{0:N3}");
            lblLuong3.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[6].ColumnName, "{0:N3}");
            lblLuong4.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[7].ColumnName, "{0:N3}");
            lblLuong5.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[8].ColumnName, "{0:N3}");            
            lblTongCong.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Tong", "{0:N3}");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, dt.TableName + ".GhiChu");
            xrLabel19.Text = GlobalSettings.TieudeNgay;

        }
    }
}
