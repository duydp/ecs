﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
#if GC_V4
using Company.GC.BLL.GC;
#endif
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using System.IO;
using Company.KDT.SHARE.VNACCS;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.KDT.SHARE.Components.DuLieuChuan;
namespace Company.Interface.GC
{
    public partial class ReadExcelFormMMTB : BaseForm
    {
        public KDT_VNACCS_BaoCaoQuyetToan_MMTB goodItem;
        public KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail goodItemHD;
        public ReadExcelFormMMTB()
        {
            InitializeComponent();
        }

        private void linkFileExcel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_GoodItem();
            }

            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog sf = new OpenFileDialog();
                sf.ShowDialog(this);
                txtFilePath.Text = sf.FileName;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnReadFile_Click(object sender, EventArgs e)
        {
            int beginRow = Convert.ToInt32(txtRowIndex.Value)-1;

            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);

            }
            catch (Exception ex)
            {
                MLMessages("Lỗi khi đọc file. Doanh nghiệp hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                return;
            }
            try
            {
                ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"MSG_EXC01", "", txtSheet.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;

            char SoHDColumn = Convert.ToChar(txtSoHD.Text.Trim());
            int SoHDCol = ConvertCharToInt(SoHDColumn);

            char STTHHColumn = Convert.ToChar(txtSTTHH.Text.Trim());
            int STTHHCol = ConvertCharToInt(STTHHColumn);

            char MaHHColumn = Convert.ToChar(txtMaHangHoa.Text.Trim());
            int MaHHCol = ConvertCharToInt(MaHHColumn);

            char TenHHColumn = Convert.ToChar(txtTenHangHoa.Text.Trim());
            int TenHHCol = ConvertCharToInt(TenHHColumn);

            char MaHSColumn = Convert.ToChar(txtMaHS.Text.Trim());
            int MaHSCol = ConvertCharToInt(MaHSColumn);

            char GhiChuHHColumn = Convert.ToChar(txtGhiChuHH.Text.Trim());
            int GhiChuHHCol = ConvertCharToInt(GhiChuHHColumn);

            char LuongTNColumn = Convert.ToChar(txtLuongTN.Text.Trim());
            int LuongTNCol = ConvertCharToInt(LuongTNColumn);

            char LuongTXColumn = Convert.ToChar(txtLuongTX.Text.Trim());
            int LuongTXCol = ConvertCharToInt(LuongTXColumn);

            char LuongCTColumn = Convert.ToChar(txtLuongCT.Text.Trim());
            int  LuongCTCol = ConvertCharToInt(LuongCTColumn);

            char SoHDCTColumn = Convert.ToChar(txtSoHDCT.Text.Trim());
            int SoHDCTCol = ConvertCharToInt(SoHDCTColumn);

            char LuongConLaiColumn = Convert.ToChar(txtLuongConlai.Text.Trim());
            int  LuongConLaiCol = ConvertCharToInt(LuongConLaiColumn);

            char DVTColumn = Convert.ToChar(txtDVT.Text.Trim());
            int DVTCol = ConvertCharToInt(DVTColumn);

            string errorTotal = "";
            string errorSTT = "";
            string errorMaHangHoa = "";
            string errorMaHangHoaExits = "";
            string errorTenHangHoa = "";
            string errorMaHS = "";
            string errorMaHSExits = "";
            string errorDVT = "";
            string errorDVTExits = "";
            string errorDVTCheck = "";
            string errorLuongTamNhap = "";
            string errorLuongTamNhapValid = "";
            string errorLuongTaiXuat = "";
            string errorLuongTaiXuatValid = "";
            string errorLuongChuyenTiep = "";
            string errorLuongChuyenTiepValid = "";
            string errorLuongConLai = "";
            string errorLuongConLaiValid = "";
            string errorSoHopDong = "";
            string errorSoHopDongExits = "";

            string errorSoHopDongCT = "";
            string errorSoHopDongCTExits = "";
            string errorToTalTonCuoiKy = "";

            List<VNACC_Category_HSCode> HSCollection = VNACC_Category_HSCode.SelectCollectionAll();
            List<VNACC_Category_QuantityUnit> DVTCollection = VNACC_Category_QuantityUnit.SelectCollectionAll();
            List<Company.GC.BLL.KDT.GC.ThietBi> TBCollection = new List<Company.GC.BLL.KDT.GC.ThietBi>();
            List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail> GoodItemDetailCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail>();
            if (!chkMutilHD.Checked)
            {
                Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
                HD.LoadHD(goodItemHD.ID_HopDong);
                HD.LoadCollection();
                TBCollection = HD.TBCollection;
                foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail item in goodItemHD.GoodItemDetailCollection)
                {
                    if (item.ID > 0)
                        item.Delete();
                }
                goodItemHD.GoodItemDetailCollection.Clear();
            }
            else
            {
                foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in goodItem.GoodItemCollection)
                {
                    item.GoodItemDetailCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail.SelectCollectionBy_Contract_ID(item.ID);
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail ite in item.GoodItemDetailCollection)
                    {
                        if (ite.ID > 0)
                            ite.Delete();
                    }
                    item.GoodItemDetailCollection.Clear();
                }
            }
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        bool isAdd = true;
                        bool isExits = false;  
                        if (chkMutilHD.Checked)
                        {
                            string SoHopDong = "";
                            foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail items in goodItem.GoodItemCollection)
                            {
                                try
                                {
                                    SoHopDong = (wsr.Cells[SoHDCol].Value).ToString().Trim();
                                }
                                catch (Exception)
                                {
                                    errorSoHopDong += "[" + (wsr.Index + 1) + "]-[" + SoHopDong + "]\n";
                                    isAdd = false;
                                }
                                if (SoHopDong.ToString().Length == 0)
                                {
                                    errorSoHopDong += "[" + (wsr.Index + 1) + "]-[" + SoHopDong + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (items.SoHopDong == SoHopDong)
                                    {
                                        isExits = true;
                                        TBCollection = new List<Company.GC.BLL.KDT.GC.ThietBi>();
                                        TBCollection = Company.GC.BLL.KDT.GC.ThietBi.SelectCollectionDynamic("HopDong_ID = " + items.ID_HopDong + "","ID");
                                        KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail GoodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail();
                                        GoodItemDetail.Contract_ID = items.ID;
                                        GoodItemDetail.ID_HopDong = items.ID_HopDong;
                                        try
                                        {
                                            GoodItemDetail.STT = Convert.ToInt32(wsr.Cells[STTHHCol].Value);
                                            if (GoodItemDetail.STT.ToString().Trim().Length == 0)
                                            {
                                                errorSTT += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.STT + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorSTT += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.STT + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            GoodItemDetail.TenHangHoa = Convert.ToString(wsr.Cells[TenHHCol].Value);
                                            if (GoodItemDetail.TenHangHoa.ToString().Trim().Length == 0)
                                            {
                                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.TenHangHoa + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.TenHangHoa + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            GoodItemDetail.MaHangHoa = Convert.ToString(wsr.Cells[MaHHCol].Value);
                                            if (GoodItemDetail.MaHangHoa.Trim().Length == 0)
                                            {
                                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.MaHangHoa + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                foreach (Company.GC.BLL.KDT.GC.ThietBi item in TBCollection)
                                                {
                                                    if (item.Ma == GoodItemDetail.MaHangHoa)
                                                    {
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                                if (!isExits)
                                                {
                                                    errorMaHangHoaExits += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.MaHangHoa + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.MaHangHoa + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            GoodItemDetail.MaHS = Convert.ToDecimal(wsr.Cells[MaHSCol].Value);
                                            if (GoodItemDetail.MaHS.ToString().Trim().Length == 0)
                                            {
                                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.MaHS + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                foreach (VNACC_Category_HSCode item in HSCollection)
                                                {
                                                    if (item.HSCode == GoodItemDetail.MaHS.ToString())
                                                    {
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (!isExits)
                                            {
                                                errorMaHSExits += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.MaHS + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.MaHS + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            isExits = false;
                                            GoodItemDetail.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim().ToUpper();
                                            if (GoodItemDetail.DVT.Trim().Length == 0)
                                            {
                                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.DVT + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                //KIỂM TRA ĐVT ĐÚNG CHUẨN VNACCS
                                                foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                                {
                                                    if (item.Code == GoodItemDetail.DVT)
                                                    {
                                                        isExits = true;
                                                    }
                                                }
                                                if (isExits)
                                                {
                                                    //isExits = false;
                                                    foreach (Company.GC.BLL.KDT.GC.ThietBi item in TBCollection)
                                                    {
                                                        if (item.Ma == GoodItemDetail.MaHangHoa)
                                                        {
                                                            if (VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) == GoodItemDetail.DVT)
                                                            {
                                                                isExits = true;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    if (!isExits)
                                                    {
                                                        errorDVTCheck += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.MaHangHoa + "]-[" + GoodItemDetail.DVT + "]\n";
                                                        isAdd = false;
                                                    }
                                                }
                                                else
                                                {
                                                    errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.DVT + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.DVT + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            GoodItemDetail.LuongTamNhap = Convert.ToDecimal(wsr.Cells[LuongTNCol].Value);
                                            if (GoodItemDetail.LuongTamNhap.ToString().Trim().Length == 0)
                                            {
                                                errorLuongTamNhap += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongTamNhap + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                if (Decimal.Round(GoodItemDetail.LuongTamNhap, 4) != GoodItemDetail.LuongTamNhap)
                                                {
                                                    errorLuongTamNhapValid += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongTamNhap + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorLuongTamNhap += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongTamNhap + "]\n";
                                            isAdd = false;
                                        }

                                        try
                                        {
                                            GoodItemDetail.LuongTaiXuat = Convert.ToDecimal(wsr.Cells[LuongTXCol].Value);
                                            if (GoodItemDetail.LuongTaiXuat.ToString().Trim().Length == 0)
                                            {
                                                errorLuongTaiXuat += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongTaiXuat + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                if (Decimal.Round(GoodItemDetail.LuongTaiXuat, 4) != GoodItemDetail.LuongTaiXuat)
                                                {
                                                    errorLuongTaiXuatValid += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongTaiXuat + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorLuongTaiXuat += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongTaiXuat + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            GoodItemDetail.LuongChuyenTiep = Convert.ToDecimal(wsr.Cells[LuongCTCol].Value);
                                            if (GoodItemDetail.LuongChuyenTiep.ToString().Trim().Length == 0)
                                            {
                                                errorLuongChuyenTiep += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongChuyenTiep + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                if (Decimal.Round(GoodItemDetail.LuongChuyenTiep, 4) != GoodItemDetail.LuongChuyenTiep)
                                                {
                                                    errorLuongChuyenTiepValid += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongChuyenTiep + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorLuongChuyenTiep += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongChuyenTiep + "]\n";
                                            isAdd = false;
                                        }
                                        String SoHopDongCT = String.Empty;
                                        try
                                        {
                                            if (GoodItemDetail.LuongChuyenTiep >= 1)
                                            {
                                                try
                                                {
                                                    SoHopDongCT = Convert.ToString(wsr.Cells[SoHDCTCol].Value);
                                                    if (SoHopDongCT.ToString().Trim().Length == 0)
                                                    {
                                                        errorSoHopDongCT += "[" + (wsr.Index + 1) + "]-[" + SoHopDongCT + "]\n";
                                                        isAdd = false;
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            GoodItemDetail.SoHopDong = SoHopDong;
                                                            long HopDong_ID = HopDong.GetID(GoodItemDetail.SoHopDong);
                                                            HopDong HDCT = HopDong.Load(HopDong_ID);
                                                            GoodItemDetail.NgayHopDong = HDCT.NgayKy;
                                                            GoodItemDetail.MaHQ = HDCT.MaHaiQuan;
                                                            GoodItemDetail.NgayHetHan = HDCT.NgayGiaHan.Year == 1900 ? HDCT.NgayHetHan : HDCT.NgayGiaHan;
                                                        }
                                                        catch (Exception)
                                                        {
                                                            errorSoHopDongCTExits += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.SoHopDong + "]\n";
                                                            isAdd = false;
                                                        }
                                                    }
                                                }
                                                catch (Exception)
                                                {
                                                    errorSoHopDongCT += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.SoHopDong + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                            else
                                            {
                                                GoodItemDetail.SoHopDong = String.Empty;
                                                GoodItemDetail.NgayHopDong = DateTime.Now;
                                                GoodItemDetail.NgayHetHan = DateTime.Now;
                                                GoodItemDetail.MaHQ = GlobalSettings.MA_HAI_QUAN;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorSoHopDongCT += "[" + (wsr.Index + 1) + "]-[" + SoHopDong + "]\n";
                                            isAdd = false;
                                        }

                                        try
                                        {
                                            GoodItemDetail.LuongConLai = Convert.ToDecimal(wsr.Cells[LuongConLaiCol].Value);
                                            if (GoodItemDetail.LuongConLai.ToString().Trim().Length == 0)
                                            {
                                                errorLuongConLai += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongConLai + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                if (Decimal.Round(GoodItemDetail.LuongConLai, 4) != GoodItemDetail.LuongConLai)
                                                {
                                                    errorLuongConLaiValid += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongConLai + "]\n";
                                                    isAdd = false;
                                                }
                                                if (GoodItemDetail.LuongTamNhap - GoodItemDetail.LuongTaiXuat - GoodItemDetail.LuongChuyenTiep != GoodItemDetail.LuongConLai)
                                                {
                                                    errorToTalTonCuoiKy += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongTamNhap + "]-[" + GoodItemDetail.LuongTaiXuat + "]-[" + GoodItemDetail.LuongChuyenTiep + "]-[" + GoodItemDetail.LuongConLai + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorLuongConLai += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongConLai + "]\n";
                                            isAdd = false;
                                        }
                                        GoodItemDetail.GhiChu = Convert.ToString(wsr.Cells[GhiChuHHCol].Value).Trim();
                                        if (isAdd)
                                            items.GoodItemDetailCollection.Add(GoodItemDetail);
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorSoHopDongExits += "[" + (wsr.Index + 1) + "]-[" + SoHopDong + "]\n";
                                isAdd = false;
                            }
                        }
                        else
                        {
                            KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail GoodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail();
                            try
                            {
                                GoodItemDetail.STT = Convert.ToInt32(wsr.Cells[STTHHCol].Value);
                                if (GoodItemDetail.STT.ToString().Trim().Length == 0)
                                {
                                    errorSTT += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.STT + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorSTT += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.STT + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                GoodItemDetail.TenHangHoa = Convert.ToString(wsr.Cells[TenHHCol].Value);
                                if (GoodItemDetail.TenHangHoa.ToString().Trim().Length == 0)
                                {
                                    errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.TenHangHoa + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.TenHangHoa + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                GoodItemDetail.MaHangHoa = Convert.ToString(wsr.Cells[MaHHCol].Value);
                                if (GoodItemDetail.MaHangHoa.Trim().Length == 0)
                                {
                                    errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.MaHangHoa + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    foreach (Company.GC.BLL.KDT.GC.ThietBi item in TBCollection)
                                    {
                                        if (item.Ma == GoodItemDetail.MaHangHoa)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorMaHangHoaExits += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.MaHangHoa + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.MaHangHoa + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                GoodItemDetail.MaHS = Convert.ToDecimal(wsr.Cells[MaHSCol].Value);
                                if (GoodItemDetail.MaHS.ToString().Trim().Length == 0)
                                {
                                    errorMaHS += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.MaHS + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    foreach (VNACC_Category_HSCode item in HSCollection)
                                    {
                                        if (item.HSCode == GoodItemDetail.MaHS.ToString())
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                }
                                if (!isExits)
                                {
                                    errorMaHSExits += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.MaHS + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.MaHS + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                isExits = false;
                                GoodItemDetail.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim().ToUpper();
                                if (GoodItemDetail.DVT.Trim().Length == 0)
                                {
                                    errorDVT += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.DVT + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    //KIỂM TRA ĐVT ĐÚNG CHUẨN VNACCS
                                    foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                    {
                                        if (item.Code == GoodItemDetail.DVT)
                                        {
                                            isExits = true;
                                        }
                                    }
                                    if (isExits)
                                    {
                                        //isExits = false;
                                        foreach (Company.GC.BLL.KDT.GC.ThietBi item in TBCollection)
                                        {
                                            if (item.Ma == GoodItemDetail.MaHangHoa)
                                            {
                                                if (VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) == GoodItemDetail.DVT)
                                                {
                                                    isExits = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (!isExits)
                                        {
                                            errorDVTCheck += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.MaHangHoa + "]-[" + GoodItemDetail.DVT + "]\n";
                                            isAdd = false;
                                        }
                                    }
                                    else
                                    {
                                        errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.DVT + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.DVT + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                GoodItemDetail.LuongTamNhap = Convert.ToDecimal(wsr.Cells[LuongTNCol].Value);
                                if (GoodItemDetail.LuongTamNhap.ToString().Trim().Length == 0)
                                {
                                    errorLuongTamNhap += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongTamNhap + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(GoodItemDetail.LuongTamNhap, 4) != GoodItemDetail.LuongTamNhap)
                                    {
                                        errorLuongTamNhapValid += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongTamNhap + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorLuongTamNhap += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongTamNhap + "]\n";
                                isAdd = false;
                            }

                            try
                            {
                                GoodItemDetail.LuongTaiXuat = Convert.ToDecimal(wsr.Cells[LuongTXCol].Value);
                                if (GoodItemDetail.LuongTaiXuat.ToString().Trim().Length == 0)
                                {
                                    errorLuongTaiXuat += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongTaiXuat + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(GoodItemDetail.LuongTaiXuat, 4) != GoodItemDetail.LuongTaiXuat)
                                    {
                                        errorLuongTaiXuatValid += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongTaiXuat + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorLuongTaiXuat += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongTaiXuat + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                GoodItemDetail.LuongChuyenTiep = Convert.ToDecimal(wsr.Cells[LuongCTCol].Value);
                                if (GoodItemDetail.LuongChuyenTiep.ToString().Trim().Length == 0)
                                {
                                    errorLuongChuyenTiep += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongChuyenTiep + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(GoodItemDetail.LuongChuyenTiep, 4) != GoodItemDetail.LuongChuyenTiep)
                                    {
                                        errorLuongChuyenTiepValid += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongChuyenTiep + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorLuongChuyenTiep += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongChuyenTiep + "]\n";
                                isAdd = false;
                            }
                            String SoHopDong = String.Empty;
                            try
                            {
                                if (GoodItemDetail.LuongChuyenTiep >= 1)
                                {
                                    try
                                    {
                                        SoHopDong = Convert.ToString(wsr.Cells[SoHDCTCol].Value);
                                        if (SoHopDong.ToString().Trim().Length == 0)
                                        {
                                            errorSoHopDongCT += "[" + (wsr.Index + 1) + "]-[" + SoHopDong + "]\n";
                                            isAdd = false;
                                        }
                                        else
                                        {
                                            try
                                            {
                                                GoodItemDetail.SoHopDong = SoHopDong;
                                                long HopDong_ID = HopDong.GetID(GoodItemDetail.SoHopDong);
                                                HopDong HDCT = HopDong.Load(HopDong_ID);
                                                GoodItemDetail.NgayHopDong = HDCT.NgayKy;
                                                GoodItemDetail.MaHQ = HDCT.MaHaiQuan;
                                                GoodItemDetail.NgayHetHan = HDCT.NgayGiaHan.Year == 1900 ? HDCT.NgayHetHan : HDCT.NgayGiaHan;
                                            }
                                            catch (Exception)
                                            {
                                                errorSoHopDongCTExits += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.SoHopDong + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        errorSoHopDongCT += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.SoHopDong + "]\n";
                                        isAdd = false;
                                    }
                                }
                                else
                                {
                                    GoodItemDetail.SoHopDong = String.Empty;
                                    GoodItemDetail.NgayHopDong = DateTime.Now;
                                    GoodItemDetail.NgayHetHan = DateTime.Now;
                                    GoodItemDetail.MaHQ = GlobalSettings.MA_HAI_QUAN;
                                }
                            }
                            catch (Exception)
                            {
                                errorSoHopDong += "[" + (wsr.Index + 1) + "]-[" + SoHopDong + "]\n";
                                isAdd = false;
                            }

                            try
                            {
                                GoodItemDetail.LuongConLai = Convert.ToDecimal(wsr.Cells[LuongConLaiCol].Value);
                                if (GoodItemDetail.LuongConLai.ToString().Trim().Length == 0)
                                {
                                    errorLuongConLai += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongConLai + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(GoodItemDetail.LuongConLai, 4) != GoodItemDetail.LuongConLai)
                                    {
                                        errorLuongConLaiValid += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongConLai + "]\n";
                                        isAdd = false;
                                    }
                                    if (GoodItemDetail.LuongTamNhap - GoodItemDetail.LuongTaiXuat - GoodItemDetail.LuongChuyenTiep != GoodItemDetail.LuongConLai)
                                    {
                                        errorToTalTonCuoiKy += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongTamNhap + "]-[" + GoodItemDetail.LuongTaiXuat + "]-[" + GoodItemDetail.LuongChuyenTiep + "]-[" + GoodItemDetail.LuongConLai + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorLuongConLai += "[" + (wsr.Index + 1) + "]-[" + GoodItemDetail.LuongConLai + "]\n";
                                isAdd = false;
                            }
                            GoodItemDetail.GhiChu = Convert.ToString(wsr.Cells[GhiChuHHCol].Value).Trim();
                            if (isAdd)
                                 GoodItemDetailCollection.Add(GoodItemDetail);
                        }                        
                    }
                    catch (Exception ex)
                    {
                        return;
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
            }
            if (!String.IsNullOrEmpty(errorSTT))
                errorTotal += "\n - [STT] -[SỐ THỨ TỰ] : \n" + errorSTT + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorMaHangHoa))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorMaHangHoaExits))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaExits + " ĐÃ ĐƯỢC ĐĂNG KÝ\n";
            if (!String.IsNullOrEmpty(errorTenHangHoa))
                errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : \n" + errorTenHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDVT))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVT + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDVTExits))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVTExits + " KHÔNG HỢP LỆ \n";
            if (!String.IsNullOrEmpty(errorDVTCheck))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] - [ĐVT] : \n" + errorDVTCheck + " KHÁC VỚI ĐVT ĐÃ ĐĂNG KÝ CỦA THIẾT BỊ\n";
            if (!String.IsNullOrEmpty(errorMaHS))
                errorTotal += "\n - [STT] -[MÃ HS] : \n" + errorMaHS + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorMaHSExits))
                errorTotal += "\n - [STT] -[MÃ HS] : \n" + errorMaHSExits + " KHÔNG HỢP LỆ\n";

            if (!String.IsNullOrEmpty(errorLuongTamNhap))
                errorTotal += "\n - [STT] -[TẠM NHẬP] : \n" + errorLuongTamNhap + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorLuongTamNhapValid))
                errorTotal += "\n - [STT] -[TẠM NHẬP] : \n" + errorLuongTamNhapValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";
            if (!String.IsNullOrEmpty(errorLuongTaiXuat))
                errorTotal += "\n - [STT] -[TÁI XUẤT] : \n" + errorLuongTaiXuat + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorLuongTaiXuatValid))
                errorTotal += "\n - [STT] -[TÁI XUẤT] : \n" + errorLuongTaiXuatValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";
            if (!String.IsNullOrEmpty(errorLuongChuyenTiep))
                errorTotal += "\n - [STT] -[CHUYỂN TIẾP] : \n" + errorLuongChuyenTiep + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorLuongChuyenTiepValid))
                errorTotal += "\n - [STT] -[CHUYỂN TIẾP] : \n" + errorLuongChuyenTiepValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";
            if (!String.IsNullOrEmpty(errorLuongConLai))
                errorTotal += "\n - [STT] -[CÒN LẠI] : \n" + errorLuongConLai + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorLuongConLaiValid))
                errorTotal += "\n - [STT] -[CÒN LẠI] : \n" + errorLuongConLaiValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";

            if (!String.IsNullOrEmpty(errorSoHopDong))
                errorTotal += "\n - [STT] -[SỐ HỢP ĐỒNG] : \n" + errorSoHopDong + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorSoHopDongExits))
                errorTotal += "\n - [STT] -[SỐ HỢP ĐỒNG] : \n" + errorSoHopDongExits + " CHƯA ĐƯỢC ĐĂNG KÝ \n";

            if (!String.IsNullOrEmpty(errorSoHopDongCT))
                errorTotal += "\n - [STT] -[SỐ HỢP ĐỒNG CHUYỂN TIẾP] : \n" + errorSoHopDong + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorSoHopDongCTExits))
                errorTotal += "\n - [STT] -[SỐ HỢP ĐỒNG CHUYỂN TIẾP] : \n" + errorSoHopDongExits + " CHƯA ĐƯỢC ĐĂNG KÝ \n";

            if (String.IsNullOrEmpty(errorTotal))
            {
                if (!chkMutilHD.Checked)
                {
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail item in GoodItemDetailCollection)
                    {
                        goodItemHD.GoodItemDetailCollection.Add(item);
                    }
                }
                ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP HÀNG TỪ EXCEL THÀNH CÔNG .", false);
            }
            else
            {
                if (chkMutilHD.Checked)
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "LỖI NHẬP TỪ EXCEL ." + errorTotal, false);
                    return;
                }
                else
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP TỪ EXCEL KHÔNG THÀNH CÔNG ." + errorTotal, false);
                    return;
                }
            }
            this.Close();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
