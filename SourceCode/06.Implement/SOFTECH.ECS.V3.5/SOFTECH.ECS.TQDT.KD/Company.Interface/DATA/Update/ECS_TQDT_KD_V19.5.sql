/*
Run this script on:

        192.168.72.151\sqlserver.ECS_TQDT_GC_V4_THIENANPHAT_21_03_2015    -  This database will be modified

to synchronize it with:

        192.168.72.151\sqlserver.ECS_TQDT_GC_V4_HT

You are recommended to back up your database before running this script

Script created by SQL Compare version 11.2.1 from Red Gate Software Ltd at 19/08/2015 11:29:09 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[t_KDT_ContainerDangKy]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF NOT EXISTS   (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='t_KDT_ContainerDangKy' AND COLUMN_NAME = 'CustomsStatus')
	ALTER TABLE [dbo].[t_KDT_ContainerDangKy] ADD
	[CustomsStatus] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Note] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CargoPiece] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PieceUnitCode] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CargoWeight] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WeightUnitCode] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerDangKy_Insert]'
GO
SET QUOTED_IDENTIFIER OFF
GO

GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_ContainerDangKy_Insert]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_Insert]
GO 
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TKMD_ID bigint,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoSua nvarchar(250),
	@ThoiGianXuatDL datetime,
	@LuongTK int,
	@TrangThaiToKhai int,
	@CustomsStatus VARCHAR(10),
	@Note VARCHAR(500),
	@CargoPiece varchar(100),
	@PieceUnitCode varchar(100),
	@CargoWeight varchar(100),
	@WeightUnitCode varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ContainerDangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua],
	[ThoiGianXuatDL],
	[LuongTK],
	[TrangThaiToKhai],
	[CustomsStatus],
	[Note],
	CargoPiece,
	PieceUnitCode,
	CargoWeight,
	WeightUnitCode
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@TKMD_ID,
	@TrangThaiXuLy,
	@MaHQ,
	@MaDoanhNghiep,
	@GuidStr,
	@DeXuatKhac,
	@LyDoSua,
	@ThoiGianXuatDL,
	@LuongTK,
	@TrangThaiToKhai,
	@CustomsStatus,
	@Note,
	@CargoPiece,
	@PieceUnitCode,
	@CargoWeight,
	@WeightUnitCode
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerDangKy_Update]'
GO
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_ContainerDangKy_Update]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_Update]
GO 
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TKMD_ID bigint,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoSua nvarchar(250),
	@ThoiGianXuatDL datetime,
	@LuongTK int,
	@TrangThaiToKhai INT,
	@CustomsStatus VARCHAR(10),
	@Note VARCHAR(500),
	@CargoPiece varchar(100),
	@PieceUnitCode varchar(100),
	@CargoWeight varchar(100),
	@WeightUnitCode varchar(100)
AS

UPDATE
	[dbo].[t_KDT_ContainerDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TKMD_ID] = @TKMD_ID,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[GuidStr] = @GuidStr,
	[DeXuatKhac] = @DeXuatKhac,
	[LyDoSua] = @LyDoSua,
	[ThoiGianXuatDL] = @ThoiGianXuatDL,
	[LuongTK] = @LuongTK,
	[TrangThaiToKhai] = @TrangThaiToKhai,
	CustomsStatus = @CustomsStatus,
	Note = @Note,
	CargoPiece =@CargoPiece,
	PieceUnitCode = @PieceUnitCode,
	CargoWeight = @CargoWeight,
	WeightUnitCode = @WeightUnitCode
WHERE
	[ID] = @ID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerDangKy_InsertUpdate]'
GO
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_ContainerDangKy_InsertUpdate]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_InsertUpdate]
GO 
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TKMD_ID bigint,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoSua nvarchar(250),
	@ThoiGianXuatDL datetime,
	@LuongTK int,
	@TrangThaiToKhai INT,
	@CustomsStatus VARCHAR(10),
	@Note VARCHAR(500),
	@CargoPiece varchar(100),
	@PieceUnitCode varchar(100),
	@CargoWeight varchar(100),
	@WeightUnitCode varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ContainerDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ContainerDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TKMD_ID] = @TKMD_ID,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[GuidStr] = @GuidStr,
			[DeXuatKhac] = @DeXuatKhac,
			[LyDoSua] = @LyDoSua,
			[ThoiGianXuatDL] = @ThoiGianXuatDL,
			[LuongTK] = @LuongTK,
			[TrangThaiToKhai] = @TrangThaiToKhai,
			CustomsStatus = @CustomsStatus,
			Note = @Note,
			CargoPiece =@CargoPiece,
	PieceUnitCode = @PieceUnitCode,
	CargoWeight = @CargoWeight,
	WeightUnitCode = @WeightUnitCode
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ContainerDangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[TKMD_ID],
			[TrangThaiXuLy],
			[MaHQ],
			[MaDoanhNghiep],
			[GuidStr],
			[DeXuatKhac],
			[LyDoSua],
			[ThoiGianXuatDL],
			[LuongTK],
			[TrangThaiToKhai],
			CustomsStatus,
			Note,
			CargoPiece,
	PieceUnitCode,
	CargoWeight,
	WeightUnitCode
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@TKMD_ID,
			@TrangThaiXuLy,
			@MaHQ,
			@MaDoanhNghiep,
			@GuidStr,
			@DeXuatKhac,
			@LyDoSua,
			@ThoiGianXuatDL,
			@LuongTK,
			@TrangThaiToKhai,
			@CustomsStatus,
			@Note,
			@CargoPiece,
	@PieceUnitCode,
	@CargoWeight,
	@WeightUnitCode
		)		
	END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerDangKy_Load]'
GO
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_ContainerDangKy_Load]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_Load]
GO 
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua],
	[ThoiGianXuatDL],
	[LuongTK],
	[TrangThaiToKhai],
	CustomsStatus,
	Note,
	CargoPiece,
	PieceUnitCode,
	CargoWeight,
	WeightUnitCode
FROM
	[dbo].[t_KDT_ContainerDangKy]
WHERE
	[ID] = @ID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerDangKy_SelectAll]'
GO
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_ContainerDangKy_SelectAll]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_SelectAll]
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua],
	[ThoiGianXuatDL],
	[LuongTK],
	[TrangThaiToKhai],
	CustomsStatus,
	Note,
	CargoPiece,
	PieceUnitCode,
	CargoWeight,
	WeightUnitCode

FROM
	[dbo].[t_KDT_ContainerDangKy]	

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[t_KDT_ContainerBS]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF NOT EXISTS   (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='t_KDT_ContainerBS' AND COLUMN_NAME = 'CustomsSeal')
	ALTER TABLE [dbo].[t_KDT_ContainerBS] ADD
	[CustomsSeal] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerBS_Insert]'
GO

GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_ContainerBS_Insert]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_KDT_ContainerBS_Insert]
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerBS_Insert]
	@Master_id bigint,
	@SoVanDon varchar(100),
	@SoContainer varchar(100),
	@SoSeal varchar(100),
	@CustomsSeal varchar(100),
	@GhiChu nvarchar(255),
	@Code nvarchar(max),
	@KVGS int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ContainerBS]
(
	[Master_id],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[CustomsSeal],
	[GhiChu],
	[Code],
	[KVGS]
)
VALUES 
(
	@Master_id,
	@SoVanDon,
	@SoContainer,
	@SoSeal,
	@CustomsSeal,
	@GhiChu,
	@Code,
	@KVGS
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerBS_Update]'
GO
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_ContainerBS_Update]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_KDT_ContainerBS_Update]
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerBS_Update]
	@ID bigint,
	@Master_id bigint,
	@SoVanDon varchar(100),
	@SoContainer varchar(100),
	@SoSeal varchar(100),
	@CustomsSeal VARCHAR(100),
	@GhiChu nvarchar(255),
	@Code nvarchar(max),
	@KVGS int
AS

UPDATE
	[dbo].[t_KDT_ContainerBS]
SET
	[Master_id] = @Master_id,
	[SoVanDon] = @SoVanDon,
	[SoContainer] = @SoContainer,
	[SoSeal] = @SoSeal,
	[CustomsSeal] = @CustomsSeal,
	[GhiChu] = @GhiChu,
	[Code] = @Code,
	[KVGS] = @KVGS
WHERE
	[ID] = @ID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerBS_InsertUpdate]'
GO
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_ContainerBS_InsertUpdate]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_KDT_ContainerBS_InsertUpdate]
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerBS_InsertUpdate]
	@ID bigint,
	@Master_id bigint,
	@SoVanDon varchar(100),
	@SoContainer varchar(100),
	@CustomsSeal VARCHAR(100),
	@SoSeal varchar(100),
	@GhiChu nvarchar(255),
	@Code nvarchar(max),
	@KVGS int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ContainerBS] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ContainerBS] 
		SET
			[Master_id] = @Master_id,
			[SoVanDon] = @SoVanDon,
			[SoContainer] = @SoContainer,
			[SoSeal] = @SoSeal,
			[CustomsSeal] = @CustomsSeal,
			[GhiChu] = @GhiChu,
			[Code] = @Code,
			[KVGS] = @KVGS
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ContainerBS]
		(
			[Master_id],
			[SoVanDon],
			[SoContainer],
			[SoSeal],
			[CustomsSeal],
			[GhiChu],
			[Code],
			[KVGS]
		)
		VALUES 
		(
			@Master_id,
			@SoVanDon,
			@SoContainer,
			@SoSeal,
			@CustomsSeal,
			@GhiChu,
			@Code,
			@KVGS
		)		
	END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerBS_Load]'
GO
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_ContainerBS_Load]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_KDT_ContainerBS_Load]
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerBS_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[CustomsSeal],
	[GhiChu],
	[Code],
	[KVGS]
FROM
	[dbo].[t_KDT_ContainerBS]
WHERE
	[ID] = @ID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerBS_SelectAll]'
GO
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_ContainerBS_SelectAll]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_KDT_ContainerBS_SelectAll]
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerBS_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[CustomsSeal],
	[GhiChu],
	[Code],
	[KVGS]
FROM
	[dbo].[t_KDT_ContainerBS]	

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerBS_SelectDynamic]'
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_ContainerBS_SelectDynamic]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_KDT_ContainerBS_SelectDynamic]
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerBS_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_id],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[CustomsSeal],
	[GhiChu],
	[Code],
	[KVGS]
FROM [dbo].[t_KDT_ContainerBS] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[p_KDT_ContainerDangKy_SelectDynamic]'
GO
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[p_KDT_ContainerDangKy_SelectDynamic]') AND TYPE IN (N'P',N'PC',N'V'))
DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_SelectDynamic]
GO
------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua],
	[ThoiGianXuatDL],
	[LuongTK],
	[TrangThaiToKhai],
	CustomsStatus,
	Note,
	CargoPiece,
	PieceUnitCode,
	CargoWeight,
	WeightUnitCode
FROM [dbo].[t_KDT_ContainerDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '19.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('19.5',GETDATE(), N'Fix lỗi tờ khai và Container')
END	