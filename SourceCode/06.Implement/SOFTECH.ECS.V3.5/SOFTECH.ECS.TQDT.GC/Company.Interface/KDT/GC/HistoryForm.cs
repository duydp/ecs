﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.KDT.GC;
#if SXXK_V4
using Company.BLL.KDT.SXXK;
#endif
namespace Company.Interface.KDT.GC
{
    public partial class HistoryForm : Company.Interface.BaseForm
    {
        public string where = "";
        public long ID { set; get; }
        public string loaiKhaiBao { get; set; }
        public List<LogKhaiBao> LogKhaiBaoCollection = new List<LogKhaiBao>();
        public HistoryForm()
        {
            InitializeComponent();
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1=1";
                if (!String.IsNullOrEmpty(loaiKhaiBao))
                    where += " AND LoaiKhaiBao ='" + loaiKhaiBao + "'";
                if (ID != null)
                    where += " AND ID_DK =" + ID + "";
                DateTime fromDate = clcTuNgay.Value;
                DateTime toDate = clcDenNgay.Value;
                where = where + " AND (NgayKhaiBao Between '" + fromDate.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + toDate.ToString("yyyy-MM-dd 23:59:59") + "')";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = LogKhaiBao.SelectCollectionDynamic(GetSearchWhere(), "");
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = LogKhaiBaoCollection;
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void HistoryForm_Load(object sender, EventArgs e)
        {
            BindData();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    LogKhaiBao Log = new LogKhaiBao();
                    Log = (LogKhaiBao)e.Row.DataRow;
#if GC_V4
                    switch (Log.LoaiKhaiBao)
                    {
                        case "HD":                            
                            HistoryDetaiHopDongForm f = new HistoryDetaiHopDongForm();
                            List<KDT_GC_NhomSanPham_Log> NhomSPCollection = new List<KDT_GC_NhomSanPham_Log>();
                            List<KDT_GC_NguyenPhuLieu_Log> NPLCollection = new List<KDT_GC_NguyenPhuLieu_Log>();
                            List<KDT_GC_SanPham_Log> SPCollection = new List<KDT_GC_SanPham_Log>();
                            List<KDT_GC_ThietBi_Log> TBCollection = new List<KDT_GC_ThietBi_Log>();
                            List<KDT_GC_HangMau_Log> HMCollection = new List<KDT_GC_HangMau_Log>();
                            NhomSPCollection = KDT_GC_NhomSanPham_Log.SelectCollectionDynamic("Master_ID = " + Log.IDLog, "");
                            NPLCollection = KDT_GC_NguyenPhuLieu_Log.SelectCollectionDynamic("Master_ID = " + Log.IDLog, "");
                            SPCollection = KDT_GC_SanPham_Log.SelectCollectionDynamic("Master_ID = " + Log.IDLog, "");
                            TBCollection = KDT_GC_ThietBi_Log.SelectCollectionDynamic("Master_ID = " + Log.IDLog, "");
                            HMCollection = KDT_GC_HangMau_Log.SelectCollectionDynamic("Master_ID = " + Log.IDLog, "");
                            f.NhomSPCollection = NhomSPCollection;
                            f.SPCollection = SPCollection;
                            f.NPLCollection = NPLCollection;
                            f.TBCollection = TBCollection;
                            f.HMCollection = HMCollection;
                            f.ShowDialog(this);
                            break;
                        case "DM":
                            HistoryDetailDinhMucForm form = new HistoryDetailDinhMucForm();
                            List<KDT_GC_DinhMuc_Log> DMCollection = new List<KDT_GC_DinhMuc_Log>();
                            DMCollection = KDT_GC_DinhMuc_Log.SelectCollectionDynamic("Master_ID = " + Log.IDLog, "");
                            form.DMCollection = DMCollection;
                            form.ShowDialog(this);
                            break;
                        case "PK":
                            HistoryDetailPhuKienForm frm = new HistoryDetailPhuKienForm();
                            List<KDT_GC_LoaiPhuKien_Log> LoaiPKCollection = new List<KDT_GC_LoaiPhuKien_Log>();
                            LoaiPKCollection = KDT_GC_LoaiPhuKien_Log.SelectCollectionDynamic("Master_ID = " + Log.IDLog, "");
                            frm.LoaiPKCollection = LoaiPKCollection;
                            frm.ShowDialog(this);
                            break;
                        default:
                            break;
                    }
#endif
#if SXXK_V4
                    switch (Log.LoaiKhaiBao)
                    {
                        case "NPL":
                            List<KDT_SXXK_NguyenPhuLieu_Log> NPLCollection = new List<KDT_SXXK_NguyenPhuLieu_Log>();
                            NPLCollection = KDT_SXXK_NguyenPhuLieu_Log.SelectCollectionDynamic("Master_ID = " + Log.IDLog, "");
                            HistoryDetailNguyenPhuLieuForm f = new HistoryDetailNguyenPhuLieuForm();
                            f.NPLCollection = NPLCollection;
                            f.ShowDialog(this);
                            break;
                        case "SP":
                            List<KDT_SXXK_SanPham_Log> SPCollection = new List<KDT_SXXK_SanPham_Log>();
                            SPCollection = KDT_SXXK_SanPham_Log.SelectCollectionDynamic("Master_ID = " + Log.IDLog, "");
                            HistoryDetailSanPhamForm frm = new HistoryDetailSanPhamForm();
                            frm.SPCollection = SPCollection;
                            frm.ShowDialog(this);
                            break;
                        case "DM":
                            List<KDT_SXXK_DinhMuc_Log> DMCollection = new List<KDT_SXXK_DinhMuc_Log>();
                            DMCollection = KDT_SXXK_DinhMuc_Log.SelectCollectionDynamic("Master_ID = " + Log.IDLog, "");
                            HistoryDetailDinhMucForm form = new HistoryDetailDinhMucForm();
                            form.DMCollection = DMCollection;
                            form.ShowDialog(this);
                            break;
                        default:
                            break;
                    }
#endif
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
