﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class AttachDocumentItem : IssueBase
    {
        /// <summary>
        /// Số thứ tự
        /// </summary>
        [XmlElement("sequence")]
        public string Sequence { get; set; }
        /// <summary>
        /// Thông tin khác
        /// </summary>
        [XmlElement("description")]
        public string Description { get; set; }
        /// <summary>
        /// Danh sách file đính kèm, lặp lại nhiều lần
        /// </summary>
        [XmlArray("AttachedFiles")]
        [XmlArrayItem("AttachedFile")]
        public List<AttachedFile> AttachedFiles { get; set; }
        
    }
}
