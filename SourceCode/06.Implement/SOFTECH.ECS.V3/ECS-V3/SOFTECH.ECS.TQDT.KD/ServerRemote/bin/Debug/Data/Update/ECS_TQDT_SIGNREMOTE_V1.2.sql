--ALTER DATABASE ECS_SIGNREMOTE SET ENABLE_BROKER WITH ROLLBACK IMMEDIATE;
GO
 IF OBJECT_ID(N'[dbo].[t_MSG_FILE_INBOX]') IS NOT NULL
	DROP TABLE [dbo].[t_MSG_FILE_INBOX]
GO

CREATE TABLE [dbo].[t_MSG_FILE_INBOX]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[MSG_FROM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSG_TO] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FILE_NAME] NVARCHAR(255) NOT NULL,
[FILE_DATA] NVARCHAR(MAX) NOT NULL,
[CREATE_TIME] [datetime] NULL CONSTRAINT [DF_t_MSG_FILE_INBOX_CREATE_TIME] DEFAULT (getdate()),
[STATUS] [int] NULL,
[GUIDSTR] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
 IF OBJECT_ID(N'[dbo].[t_MSG_FILE_OUTBOX]') IS NOT NULL
	DROP TABLE [dbo].[t_MSG_FILE_OUTBOX]
GO
CREATE TABLE [dbo].[t_MSG_FILE_OUTBOX]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1) PRIMARY KEY,
[MSG_FROM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSG_TO] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FILE_NAME] NVARCHAR(255) NOT NULL,
[FILE_DATA] NVARCHAR(MAX) NOT NULL,
[CREATE_TIME] [datetime] NULL CONSTRAINT [DF_t_MSG_FILE_OUTBOX_CREATE_TIME] DEFAULT (getdate()),
[STATUS] [int] NULL,
[GUIDSTR] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
IF OBJECT_ID(N'[dbo].[GetFileMessages]') IS NOT NULL
	DROP PROCEDURE [dbo].[GetFileMessages]
GO
CREATE PROC [dbo].[GetFileMessages]    
AS   
SELECT MSG_FROM ,MSG_TO ,FILE_NAME ,FILE_DATA ,CREATE_TIME ,STATUS ,GUIDSTR 
FROM dbo.t_MSG_FILE_INBOX
GO

IF OBJECT_ID(N'[dbo].[GetMessages]') IS NOT NULL
	DROP PROCEDURE [dbo].[GetMessages]
GO

CREATE PROC [dbo].[GetMessages]    
AS    
SELECT MSG_FROM,MSG_TO,MSG_ORIGIN,MSG_SIGN_DATA,MSG_SIGN_CERT,CREATE_TIME,STATUS,GUIDSTR   
FROM dbo.t_MSG_INBOX
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_INBOX_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_INBOX_Insert]

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_INBOX_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_INBOX_Update]

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_INBOX_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_INBOX_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_INBOX_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_INBOX_Delete]

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_INBOX_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_INBOX_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_INBOX_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_INBOX_Load]

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_INBOX_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_INBOX_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_INBOX_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_INBOX_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_INBOX_Insert]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_INBOX_Insert]
	@MSG_FROM varchar(50),
	@MSG_TO varchar(50),
	@FILE_NAME nvarchar(255),
	@FILE_DATA nvarchar(max),
	@CREATE_TIME datetime,
	@STATUS int,
	@GUIDSTR nvarchar(36),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_MSG_FILE_INBOX]
(
	[MSG_FROM],
	[MSG_TO],
	[FILE_NAME],
	[FILE_DATA],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
)
VALUES 
(
	@MSG_FROM,
	@MSG_TO,
	@FILE_NAME,
	@FILE_DATA,
	@CREATE_TIME,
	@STATUS,
	@GUIDSTR
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_INBOX_Update]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_INBOX_Update]
	@ID bigint,
	@MSG_FROM varchar(50),
	@MSG_TO varchar(50),
	@FILE_NAME nvarchar(255),
	@FILE_DATA nvarchar(max),
	@CREATE_TIME datetime,
	@STATUS int,
	@GUIDSTR nvarchar(36)
AS

UPDATE
	[dbo].[t_MSG_FILE_INBOX]
SET
	[MSG_FROM] = @MSG_FROM,
	[MSG_TO] = @MSG_TO,
	[FILE_NAME] = @FILE_NAME,
	[FILE_DATA] = @FILE_DATA,
	[CREATE_TIME] = @CREATE_TIME,
	[STATUS] = @STATUS,
	[GUIDSTR] = @GUIDSTR
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_INBOX_InsertUpdate]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_INBOX_InsertUpdate]
	@ID bigint,
	@MSG_FROM varchar(50),
	@MSG_TO varchar(50),
	@FILE_NAME nvarchar(255),
	@FILE_DATA nvarchar(max),
	@CREATE_TIME datetime,
	@STATUS int,
	@GUIDSTR nvarchar(36)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_MSG_FILE_INBOX] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_MSG_FILE_INBOX] 
		SET
			[MSG_FROM] = @MSG_FROM,
			[MSG_TO] = @MSG_TO,
			[FILE_NAME] = @FILE_NAME,
			[FILE_DATA] = @FILE_DATA,
			[CREATE_TIME] = @CREATE_TIME,
			[STATUS] = @STATUS,
			[GUIDSTR] = @GUIDSTR
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_MSG_FILE_INBOX]
		(
			[MSG_FROM],
			[MSG_TO],
			[FILE_NAME],
			[FILE_DATA],
			[CREATE_TIME],
			[STATUS],
			[GUIDSTR]
		)
		VALUES 
		(
			@MSG_FROM,
			@MSG_TO,
			@FILE_NAME,
			@FILE_DATA,
			@CREATE_TIME,
			@STATUS,
			@GUIDSTR
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_INBOX_Delete]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_INBOX_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_MSG_FILE_INBOX]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_INBOX_DeleteDynamic]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_INBOX_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_MSG_FILE_INBOX] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_INBOX_Load]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_INBOX_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[FILE_NAME],
	[FILE_DATA],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
FROM
	[dbo].[t_MSG_FILE_INBOX]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_INBOX_SelectDynamic]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_INBOX_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[FILE_NAME],
	[FILE_DATA],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
FROM [dbo].[t_MSG_FILE_INBOX] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_INBOX_SelectAll]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_INBOX_SelectAll]








AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[FILE_NAME],
	[FILE_DATA],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
FROM
	[dbo].[t_MSG_FILE_INBOX]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_OUTBOX_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_Insert]

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_OUTBOX_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_Update]

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_OUTBOX_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_OUTBOX_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_Delete]

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_OUTBOX_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_OUTBOX_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_Load]

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_OUTBOX_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_MSG_FILE_OUTBOX_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_OUTBOX_Insert]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_Insert]
	@MSG_FROM varchar(50),
	@MSG_TO varchar(50),
	@FILE_NAME nvarchar(255),
	@FILE_DATA nvarchar(max),
	@CREATE_TIME datetime,
	@STATUS int,
	@GUIDSTR nvarchar(36),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_MSG_FILE_OUTBOX]
(
	[MSG_FROM],
	[MSG_TO],
	[FILE_NAME],
	[FILE_DATA],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
)
VALUES 
(
	@MSG_FROM,
	@MSG_TO,
	@FILE_NAME,
	@FILE_DATA,
	@CREATE_TIME,
	@STATUS,
	@GUIDSTR
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_OUTBOX_Update]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_Update]
	@ID bigint,
	@MSG_FROM varchar(50),
	@MSG_TO varchar(50),
	@FILE_NAME nvarchar(255),
	@FILE_DATA nvarchar(max),
	@CREATE_TIME datetime,
	@STATUS int,
	@GUIDSTR nvarchar(36)
AS

UPDATE
	[dbo].[t_MSG_FILE_OUTBOX]
SET
	[MSG_FROM] = @MSG_FROM,
	[MSG_TO] = @MSG_TO,
	[FILE_NAME] = @FILE_NAME,
	[FILE_DATA] = @FILE_DATA,
	[CREATE_TIME] = @CREATE_TIME,
	[STATUS] = @STATUS,
	[GUIDSTR] = @GUIDSTR
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_OUTBOX_InsertUpdate]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_InsertUpdate]
	@ID bigint,
	@MSG_FROM varchar(50),
	@MSG_TO varchar(50),
	@FILE_NAME nvarchar(255),
	@FILE_DATA nvarchar(max),
	@CREATE_TIME datetime,
	@STATUS int,
	@GUIDSTR nvarchar(36)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_MSG_FILE_OUTBOX] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_MSG_FILE_OUTBOX] 
		SET
			[MSG_FROM] = @MSG_FROM,
			[MSG_TO] = @MSG_TO,
			[FILE_NAME] = @FILE_NAME,
			[FILE_DATA] = @FILE_DATA,
			[CREATE_TIME] = @CREATE_TIME,
			[STATUS] = @STATUS,
			[GUIDSTR] = @GUIDSTR
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_MSG_FILE_OUTBOX]
		(
			[MSG_FROM],
			[MSG_TO],
			[FILE_NAME],
			[FILE_DATA],
			[CREATE_TIME],
			[STATUS],
			[GUIDSTR]
		)
		VALUES 
		(
			@MSG_FROM,
			@MSG_TO,
			@FILE_NAME,
			@FILE_DATA,
			@CREATE_TIME,
			@STATUS,
			@GUIDSTR
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_OUTBOX_Delete]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_MSG_FILE_OUTBOX]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_OUTBOX_DeleteDynamic]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_MSG_FILE_OUTBOX] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_OUTBOX_Load]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[FILE_NAME],
	[FILE_DATA],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
FROM
	[dbo].[t_MSG_FILE_OUTBOX]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_OUTBOX_SelectDynamic]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[FILE_NAME],
	[FILE_DATA],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
FROM [dbo].[t_MSG_FILE_OUTBOX] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_FILE_OUTBOX_SelectAll]
-- Database: ECS_SIGNREMOTE
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_FILE_OUTBOX_SelectAll]








AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[FILE_NAME],
	[FILE_DATA],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
FROM
	[dbo].[t_MSG_FILE_OUTBOX]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '1.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('1.2',GETDATE(), N'CẬP NHẬT TABLE KÝ FILE QUA MẠNG LAN')
END