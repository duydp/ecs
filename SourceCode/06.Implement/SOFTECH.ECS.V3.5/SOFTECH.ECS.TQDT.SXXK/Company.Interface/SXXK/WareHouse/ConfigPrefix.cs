﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Janus.Windows.GridEX;

namespace Company.Interface.GC.WareHouse
{
    public partial class ConfigPrefix : BaseFormHaveGuidPanel
    {
        public T_KHOKETOAN_QUYTACDANHSO Config = new T_KHOKETOAN_QUYTACDANHSO();
        public bool isAdd = true;
        public ConfigPrefix()
        {
            InitializeComponent();
        }

        private void ConfigPrefix_Load(object sender, EventArgs e)
        {
            BindData();
        }
        private void BindData()
        {
            try
            {
                grList.Refetch();
                grList.DataSource = T_KHOKETOAN_QUYTACDANHSO.SelectCollectionAll();
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtTienTo, errorProvider, "Tiền tố", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtDoDaiSo, errorProvider, "Độ dài số");
                isValid &= ValidateControl.ValidateNull(txtPhanSo, errorProvider, "Phần số");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetConfigPrefix();
                if (isAdd)
                {
                    //Kiểm tra tồn tại Cấu hình chứng từ
                    foreach (T_KHOKETOAN_QUYTACDANHSO item in T_KHOKETOAN_QUYTACDANHSO.SelectCollectionAll())
                    {
                        if (item.LOAICHUNGTU == cbbLoaiChungTu.SelectedValue.ToString())
                        {
                            errorProvider.SetError(cbbLoaiChungTu, "Loại chứng từ : ''" + cbbLoaiChungTu.Text.ToString() + "'' này đã được Cấu hình");
                            return;
                        }
                    }
                    Config.Insert();
                }
                else
                {
                    Config.Update();
                }
                BindData();
                Config = new T_KHOKETOAN_QUYTACDANHSO();
                txtTienTo.Text = String.Empty;
                txtHienThi.Text = String.Empty;
                isAdd = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void GetConfigPrefix()
        {
            try
            {
                Config.LOAICHUNGTU = cbbLoaiChungTu.SelectedValue.ToString();
                Config.TIENTO = txtTienTo.Text.ToString().Trim();
                Config.GIATRIPHANSO = Convert.ToInt64(txtPhanSo.Text.ToString());
                Config.DODAISO = Convert.ToInt64(txtDoDaiSo.Text.ToString());
                Config.HIENTHI = txtHienThi.Text.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetConfigPrefix()
        {
            try
            {
                cbbLoaiChungTu.SelectedValue = Config.LOAICHUNGTU.ToString();
                txtTienTo.Text = Config.TIENTO.ToString();
                txtPhanSo.Text = Config.GIATRIPHANSO.ToString();
                txtDoDaiSo.Text = Config.DODAISO.ToString();
                txtHienThi.Text = Config.HIENTHI.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    Config = new T_KHOKETOAN_QUYTACDANHSO();
                    Config = (T_KHOKETOAN_QUYTACDANHSO)e.Row.DataRow;
                    SetConfigPrefix();
                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtPhanSo_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                txtPhanSo.MaxLength = Convert.ToInt32(txtDoDaiSo.Text.ToString()) - 1;
                if (Convert.ToInt32(txtDoDaiSo.Text.ToString()) != txtPhanSo.Text.Length + 1)
                {
                    //ShowMessage("Giá trị phần số vượt quá hoặc nhỏ hơn Độ dài số ", false);
                    errorProvider.SetError(txtPhanSo, setText("Giá trị phần số vượt quá hoặc nhỏ hơn Độ dài số ", "This value must be greater than 0 "));
                    return;
                }
                txtPhanSo.MaxLength = Convert.ToInt32(txtDoDaiSo.Text.ToString());
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1);
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtTienTo_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1);
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtDoDaiSo_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                txtPhanSo.MaxLength = Convert.ToInt32(txtDoDaiSo.Text.ToString()) - 1;
                if (Convert.ToInt32(txtDoDaiSo.Text.ToString()) != txtPhanSo.Text.Length + 1)
                {
                    errorProvider.SetError(txtDoDaiSo, setText("Giá trị Độ dài số vượt quá hoặc nhỏ hơn Giá trị Phần số ", "This value must be greater than 0 "));
                    return;
                }
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1);
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
        }

        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string LoaiChungTu = e.Row.Cells["LOAICHUNGTU"].Value.ToString();
                    switch (LoaiChungTu)
                    {
                        case "PNK":
                            e.Row.Cells["LOAICHUNGTU"].Text = "Phiếu nhập kho kế toán ";
                            break;
                        case "PXK":
                            e.Row.Cells["LOAICHUNGTU"].Text = "Phiếu xuất kho kế toán ";
                            break;
                        case "PKK":
                            e.Row.Cells["LOAICHUNGTU"].Text = "Phiếu kiểm kê ";
                            break;
                        case "PCK":
                            e.Row.Cells["LOAICHUNGTU"].Text = "Phiếu chuyển kho ";
                            break;

                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
