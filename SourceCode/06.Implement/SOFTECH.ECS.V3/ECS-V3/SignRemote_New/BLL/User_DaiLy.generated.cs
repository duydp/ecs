﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SignRemote
{
	public partial class User_DaiLy : ICloneable
	{
		#region Properties.
		
		public int ID { set; get; }
		public string USER_NAME { set; get; }
		public string PASSWORD { set; get; }
		public string HO_TEN { set; get; }
		public string MO_TA { set; get; }
		public bool isAdmin { set; get; }
		public string MaDoanhNghiep { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<User_DaiLy> ConvertToCollection(IDataReader reader)
		{
			List<User_DaiLy> collection = new List<User_DaiLy>();
			while (reader.Read())
			{
				User_DaiLy entity = new User_DaiLy();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("USER_NAME"))) entity.USER_NAME = reader.GetString(reader.GetOrdinal("USER_NAME"));
				if (!reader.IsDBNull(reader.GetOrdinal("PASSWORD"))) entity.PASSWORD = reader.GetString(reader.GetOrdinal("PASSWORD"));
				if (!reader.IsDBNull(reader.GetOrdinal("HO_TEN"))) entity.HO_TEN = reader.GetString(reader.GetOrdinal("HO_TEN"));
				if (!reader.IsDBNull(reader.GetOrdinal("MO_TA"))) entity.MO_TA = reader.GetString(reader.GetOrdinal("MO_TA"));
				if (!reader.IsDBNull(reader.GetOrdinal("isAdmin"))) entity.isAdmin = reader.GetBoolean(reader.GetOrdinal("isAdmin"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<User_DaiLy> collection, int id)
        {
            foreach (User_DaiLy item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_User_DaiLy VALUES(@USER_NAME, @PASSWORD, @HO_TEN, @MO_TA, @isAdmin, @MaDoanhNghiep)";
            string update = "UPDATE t_User_DaiLy SET USER_NAME = @USER_NAME, PASSWORD = @PASSWORD, HO_TEN = @HO_TEN, MO_TA = @MO_TA, isAdmin = @isAdmin, MaDoanhNghiep = @MaDoanhNghiep WHERE ID = @ID";
            string delete = "DELETE FROM t_User_DaiLy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@USER_NAME", SqlDbType.VarChar, "USER_NAME", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PASSWORD", SqlDbType.VarChar, "PASSWORD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HO_TEN", SqlDbType.NVarChar, "HO_TEN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MO_TA", SqlDbType.NVarChar, "MO_TA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@isAdmin", SqlDbType.Bit, "isAdmin", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@USER_NAME", SqlDbType.VarChar, "USER_NAME", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PASSWORD", SqlDbType.VarChar, "PASSWORD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HO_TEN", SqlDbType.NVarChar, "HO_TEN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MO_TA", SqlDbType.NVarChar, "MO_TA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@isAdmin", SqlDbType.Bit, "isAdmin", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_User_DaiLy VALUES(@USER_NAME, @PASSWORD, @HO_TEN, @MO_TA, @isAdmin, @MaDoanhNghiep)";
            string update = "UPDATE t_User_DaiLy SET USER_NAME = @USER_NAME, PASSWORD = @PASSWORD, HO_TEN = @HO_TEN, MO_TA = @MO_TA, isAdmin = @isAdmin, MaDoanhNghiep = @MaDoanhNghiep WHERE ID = @ID";
            string delete = "DELETE FROM t_User_DaiLy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@USER_NAME", SqlDbType.VarChar, "USER_NAME", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PASSWORD", SqlDbType.VarChar, "PASSWORD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HO_TEN", SqlDbType.NVarChar, "HO_TEN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MO_TA", SqlDbType.NVarChar, "MO_TA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@isAdmin", SqlDbType.Bit, "isAdmin", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@USER_NAME", SqlDbType.VarChar, "USER_NAME", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PASSWORD", SqlDbType.VarChar, "PASSWORD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HO_TEN", SqlDbType.NVarChar, "HO_TEN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MO_TA", SqlDbType.NVarChar, "MO_TA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@isAdmin", SqlDbType.Bit, "isAdmin", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static User_DaiLy Load(int id)
		{
			const string spName = "[dbo].[p_User_DaiLy_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<User_DaiLy> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<User_DaiLy> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<User_DaiLy> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_User_DaiLy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_User_DaiLy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_User_DaiLy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_User_DaiLy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertUser_DaiLy(string uSER_NAME, string pASSWORD, string hO_TEN, string mO_TA, bool isAdmin, string maDoanhNghiep)
		{
			User_DaiLy entity = new User_DaiLy();	
			entity.USER_NAME = uSER_NAME;
			entity.PASSWORD = pASSWORD;
			entity.HO_TEN = hO_TEN;
			entity.MO_TA = mO_TA;
			entity.isAdmin = isAdmin;
			entity.MaDoanhNghiep = maDoanhNghiep;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_User_DaiLy_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, USER_NAME);
			db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, PASSWORD);
			db.AddInParameter(dbCommand, "@HO_TEN", SqlDbType.NVarChar, HO_TEN);
			db.AddInParameter(dbCommand, "@MO_TA", SqlDbType.NVarChar, MO_TA);
			db.AddInParameter(dbCommand, "@isAdmin", SqlDbType.Bit, isAdmin);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<User_DaiLy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (User_DaiLy item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateUser_DaiLy(int id, string uSER_NAME, string pASSWORD, string hO_TEN, string mO_TA, bool isAdmin, string maDoanhNghiep)
		{
			User_DaiLy entity = new User_DaiLy();			
			entity.ID = id;
			entity.USER_NAME = uSER_NAME;
			entity.PASSWORD = pASSWORD;
			entity.HO_TEN = hO_TEN;
			entity.MO_TA = mO_TA;
			entity.isAdmin = isAdmin;
			entity.MaDoanhNghiep = maDoanhNghiep;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_User_DaiLy_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, USER_NAME);
			db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, PASSWORD);
			db.AddInParameter(dbCommand, "@HO_TEN", SqlDbType.NVarChar, HO_TEN);
			db.AddInParameter(dbCommand, "@MO_TA", SqlDbType.NVarChar, MO_TA);
			db.AddInParameter(dbCommand, "@isAdmin", SqlDbType.Bit, isAdmin);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<User_DaiLy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (User_DaiLy item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateUser_DaiLy(int id, string uSER_NAME, string pASSWORD, string hO_TEN, string mO_TA, bool isAdmin, string maDoanhNghiep)
		{
			User_DaiLy entity = new User_DaiLy();			
			entity.ID = id;
			entity.USER_NAME = uSER_NAME;
			entity.PASSWORD = pASSWORD;
			entity.HO_TEN = hO_TEN;
			entity.MO_TA = mO_TA;
			entity.isAdmin = isAdmin;
			entity.MaDoanhNghiep = maDoanhNghiep;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_User_DaiLy_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@USER_NAME", SqlDbType.VarChar, USER_NAME);
			db.AddInParameter(dbCommand, "@PASSWORD", SqlDbType.VarChar, PASSWORD);
			db.AddInParameter(dbCommand, "@HO_TEN", SqlDbType.NVarChar, HO_TEN);
			db.AddInParameter(dbCommand, "@MO_TA", SqlDbType.NVarChar, MO_TA);
			db.AddInParameter(dbCommand, "@isAdmin", SqlDbType.Bit, isAdmin);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<User_DaiLy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (User_DaiLy item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteUser_DaiLy(int id)
		{
			User_DaiLy entity = new User_DaiLy();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_User_DaiLy_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_User_DaiLy_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<User_DaiLy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (User_DaiLy item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}