using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.BLL.KDT.ExportToExcel
{
    public partial class NguyenPhuLieu : EntityBase
    {
        #region Private members.

        protected string _MaHaiQuan = String.Empty;
        protected string _MaDoanhNghiep = String.Empty;
        protected string _Ma = String.Empty;
        protected string _Ten = String.Empty;
        protected string _MaHS = String.Empty;
        protected string _DVT_ID = String.Empty;
        protected int _IsExist = 0;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public string MaHaiQuan
        {
            set { this._MaHaiQuan = value; }
            get { return this._MaHaiQuan; }
        }

        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }

        public string Ma
        {
            set { this._Ma = value; }
            get { return this._Ma; }
        }

        public string Ten
        {
            set { this._Ten = value; }
            get { return this._Ten; }
        }

        public string MaHS
        {
            set { this._MaHS = value; }
            get { return this._MaHS; }
        }

        public string DVT_ID
        {
            set { this._DVT_ID = value; }
            get { return this._DVT_ID; }
        }

        //---------------------------------------------------------------------------------------------
        public int IsExist
        {
            set { this._IsExist = value; }
            get { return this._IsExist; }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_SXXK_NguyenPhuLieu_Load";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) this._Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) this._Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------


        //public DataSet SelectAll()
        //{
        //    string spName = "p_SXXK_NguyenPhuLieu_SelectAll";
        //    SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
        //    return this.db.ExecuteDataSet(dbCommand);
        //}

        ////------------------------------------ThoiLV---------------------------------------------------------

       public DataSet SelectDynamic(string maHaiQuan, string maDoanhNghiep)
		{
            string sql = "SELECT a.*,b.Ten FROM t_SXXK_NguyenPhuLieu a INNER JOIN t_HaiQuan_DonViTinh b ON a.DVT_ID = b.ID  WHERE MaHaiQuan = @MaHQ AND MaDoanhNghiep =@MaDV";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@MaDV", SqlDbType.VarChar, maDoanhNghiep);
            
            return db.ExecuteDataSet(dbCommand);        				
		}

        public DataSet SelectDynamicKhai(string maHaiQuan, string maDoanhNghiep)
        {
            string sql = "SELECT a.*,b.Ten FROM t_KDT_GC_NguyenPhuLieu a INNER JOIN t_HaiQuan_DonViTinh b ON a.DVT_ID = b.ID  WHERE MaHaiQuan = @MaHQ AND MaDoanhNghiep =@MaDV";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@MaDV", SqlDbType.VarChar, maDoanhNghiep);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        //public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        //{
        //    string spName = "p_SXXK_NguyenPhuLieu_SelectDynamic";
        //    SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

        //    this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
        //    this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

        //    return this.db.ExecuteDataSet(dbCommand);
        //}

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_NguyenPhuLieu_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        //public NguyenPhuLieuCollection SelectCollectionAll()
        //{
        //    NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();

        //    IDataReader reader = this.SelectReaderAll();
        //    while (reader.Read())
        //    {
        //        NguyenPhuLieu entity = new NguyenPhuLieu();

        //        if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
        //        if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
        //        if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
        //        if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
        //        if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
        //        if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
        //        collection.Add(entity);
        //    }
        //    return collection;
        //}

        //---------------------------------------------------------------------------------------------

        public NguyenPhuLieuCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                NguyenPhuLieu entity = new NguyenPhuLieu();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_NguyenPhuLieu_Insert";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(NguyenPhuLieuCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection) db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NguyenPhuLieu item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, NguyenPhuLieuCollection collection)
        {
            foreach (NguyenPhuLieu item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_NguyenPhuLieu_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(NguyenPhuLieuCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection) db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NguyenPhuLieu item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_NguyenPhuLieu_Update";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(NguyenPhuLieuCollection collection, SqlTransaction transaction)
        {
            foreach (NguyenPhuLieu item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_NguyenPhuLieu_Delete";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(NguyenPhuLieuCollection collection, SqlTransaction transaction)
        {
            foreach (NguyenPhuLieu item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(NguyenPhuLieuCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection) db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NguyenPhuLieu item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion
    }
}