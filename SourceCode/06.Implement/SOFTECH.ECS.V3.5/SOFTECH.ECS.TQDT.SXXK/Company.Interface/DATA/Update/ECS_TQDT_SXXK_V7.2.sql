    
      ------------------------------------------------------------------------------------------------------------------------            
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_InsertUpdate]            
-- Database: HaiQuan            
-- Author: Ngo Thanh Tung            
-- Time created: Tuesday, October 28, 2008            
------------------------------------------------------------------------------------------------------------------------            
            
alter PROCEDURE [dbo].[p_SXXK_HangMauDich_InsertUpdate]        
    @SoToKhai INT ,        
    @MaLoaiHinh CHAR(5) ,        
    @MaHaiQuan CHAR(6) ,        
    @NamDangKy SMALLINT ,        
    @SoThuTuHang SMALLINT ,        
    @MaHS VARCHAR(12) ,        
    @MaPhu VARCHAR(30) ,        
    @TenHang NVARCHAR(80) ,        
    @NuocXX_ID CHAR(3) ,        
    @DVT_ID CHAR(3) ,        
    @SoLuong NUMERIC(18, 5) ,        
    @DonGiaKB NUMERIC(38, 15) ,        
    @DonGiaTT NUMERIC(38, 15) ,        
    @TriGiaKB NUMERIC(38, 15) ,        
    @TriGiaTT NUMERIC(38, 15) ,        
    @TriGiaKB_VND NUMERIC(38, 15) ,        
    @ThueSuatXNK NUMERIC(5, 2) ,        
    @ThueSuatTTDB NUMERIC(5, 2) ,        
    @ThueSuatGTGT NUMERIC(5, 2) ,        
    @ThueXNK MONEY ,        
    @ThueTTDB MONEY ,        
    @ThueGTGT MONEY ,        
    @PhuThu MONEY ,        
    @TyLeThuKhac NUMERIC(5, 2) ,        
    @TriGiaThuKhac MONEY ,        
    @MienThue TINYINT        
AS         
    declare @Suffix varchar(1)
    
    set @Suffix = SUBSTRING(@MaLoaiHinh, 1, 1)
    
if @Suffix = 'N'
	begin    
		IF EXISTS ( SELECT  [SoToKhai] ,        
							[MaLoaiHinh] ,        
							[MaHaiQuan] ,        
							[NamDangKy] ,        
							[SoThuTuHang]        
					FROM    [dbo].[t_SXXK_HangMauDich]        
					WHERE   [SoToKhai] = @SoToKhai        
							AND [MaLoaiHinh] = @MaLoaiHinh        
							AND [MaHaiQuan] = @MaHaiQuan        
							AND [NamDangKy] = @NamDangKy        
							AND MaPhu = @MaPhu )         
			BEGIN            
				UPDATE  [dbo].[t_SXXK_HangMauDich]        
				SET     [MaHS] = @MaHS ,        
						[MaPhu] = @MaPhu ,        
						[TenHang] = @TenHang ,        
						[NuocXX_ID] = @NuocXX_ID ,        
						[DVT_ID] = @DVT_ID ,        
						[SoLuong] = @SoLuong ,        
						[DonGiaKB] = @DonGiaKB ,        
						[DonGiaTT] = @DonGiaTT ,        
						[TriGiaKB] = @TriGiaKB ,        
						[TriGiaTT] = @TriGiaTT ,        
						[TriGiaKB_VND] = @TriGiaKB_VND ,        
						[ThueSuatXNK] = @ThueSuatXNK ,        
						[ThueSuatTTDB] = @ThueSuatTTDB ,        
						[ThueSuatGTGT] = @ThueSuatGTGT ,        
						[ThueXNK] = @ThueXNK ,        
						[ThueTTDB] = @ThueTTDB ,        
						[ThueGTGT] = @ThueGTGT ,        
						[PhuThu] = @PhuThu ,        
						[TyLeThuKhac] = @TyLeThuKhac ,        
						[TriGiaThuKhac] = @TriGiaThuKhac ,        
						[MienThue] = @MienThue        
				WHERE   [SoToKhai] = @SoToKhai        
						AND [MaLoaiHinh] = @MaLoaiHinh        
						AND [MaHaiQuan] = @MaHaiQuan        
						AND [NamDangKy] = @NamDangKy        
						AND MaPhu = @MaPhu        
			END            
		ELSE         
			BEGIN            
				INSERT  INTO [dbo].[t_SXXK_HangMauDich]        
						( [SoToKhai] ,        
						  [MaLoaiHinh] ,        
						  [MaHaiQuan] ,        
						  [NamDangKy] ,        
						  [SoThuTuHang] ,        
						  [MaHS] ,        
						  [MaPhu] ,        
						  [TenHang] ,        
						  [NuocXX_ID] ,        
		 [DVT_ID] ,        
						  [SoLuong] ,        
						  [DonGiaKB] ,        
						  [DonGiaTT] ,        
						  [TriGiaKB] ,        
					   [TriGiaTT] ,        
						  [TriGiaKB_VND] ,        
						  [ThueSuatXNK] ,        
						  [ThueSuatTTDB] ,        
		 [ThueSuatGTGT] ,        
						  [ThueXNK] ,        
						  [ThueTTDB] ,        
						  [ThueGTGT] ,        
						  [PhuThu] ,        
						  [TyLeThuKhac] ,        
						  [TriGiaThuKhac] ,        
						  [MienThue]            
						)        
				VALUES  ( @SoToKhai ,        
						  @MaLoaiHinh ,        
						  @MaHaiQuan ,        
						  @NamDangKy ,        
						  @SoThuTuHang ,        
						  @MaHS ,        
						  @MaPhu ,        
						  @TenHang ,        
						  @NuocXX_ID ,        
						  @DVT_ID ,        
						  @SoLuong ,        
						  @DonGiaKB ,        
						  @DonGiaTT ,        
						  @TriGiaKB ,        
						  @TriGiaTT ,        
						  @TriGiaKB_VND ,        
						  @ThueSuatXNK ,        
						  @ThueSuatTTDB ,        
						  @ThueSuatGTGT ,        
						  @ThueXNK ,        
						  @ThueTTDB ,        
						  @ThueGTGT ,        
						  @PhuThu ,        
						  @TyLeThuKhac ,        
						  @TriGiaThuKhac ,        
						  @MienThue            
						)             
			END  
	end
else
	begin
		IF EXISTS ( SELECT  [SoToKhai] ,        
							[MaLoaiHinh] ,        
							[MaHaiQuan] ,        
							[NamDangKy] ,        
							[SoThuTuHang]        
					FROM    [dbo].[t_SXXK_HangMauDich]        
					WHERE   [SoToKhai] = @SoToKhai        
							AND [MaLoaiHinh] = @MaLoaiHinh        
							AND [MaHaiQuan] = @MaHaiQuan        
							AND [NamDangKy] = @NamDangKy        
							AND MaPhu = @MaPhu 
							AND SoThuTuHang = @SoThuTuHang )         
			BEGIN            
				UPDATE  [dbo].[t_SXXK_HangMauDich]        
				SET     [MaHS] = @MaHS ,        
						[MaPhu] = @MaPhu ,        
						[TenHang] = @TenHang ,        
						[NuocXX_ID] = @NuocXX_ID ,        
						[DVT_ID] = @DVT_ID ,        
						[SoLuong] = @SoLuong ,        
						[DonGiaKB] = @DonGiaKB ,        
						[DonGiaTT] = @DonGiaTT ,        
						[TriGiaKB] = @TriGiaKB ,        
						[TriGiaTT] = @TriGiaTT ,        
						[TriGiaKB_VND] = @TriGiaKB_VND ,        
						[ThueSuatXNK] = @ThueSuatXNK ,        
						[ThueSuatTTDB] = @ThueSuatTTDB ,        
						[ThueSuatGTGT] = @ThueSuatGTGT ,        
						[ThueXNK] = @ThueXNK ,        
						[ThueTTDB] = @ThueTTDB ,        
						[ThueGTGT] = @ThueGTGT ,        
						[PhuThu] = @PhuThu ,        
						[TyLeThuKhac] = @TyLeThuKhac ,        
						[TriGiaThuKhac] = @TriGiaThuKhac ,        
						[MienThue] = @MienThue        
				WHERE   [SoToKhai] = @SoToKhai        
						AND [MaLoaiHinh] = @MaLoaiHinh        
						AND [MaHaiQuan] = @MaHaiQuan        
						AND [NamDangKy] = @NamDangKy        
						AND MaPhu = @MaPhu        
			END            
		ELSE         
			BEGIN            
				INSERT  INTO [dbo].[t_SXXK_HangMauDich]        
						( [SoToKhai] ,        
						  [MaLoaiHinh] ,        
						  [MaHaiQuan] ,        
						  [NamDangKy] ,        
						  [SoThuTuHang] ,        
						  [MaHS] ,        
						  [MaPhu] ,        
						  [TenHang] ,        
						  [NuocXX_ID] ,        
		 [DVT_ID] ,        
						  [SoLuong] ,        
						  [DonGiaKB] ,        
						  [DonGiaTT] ,        
						  [TriGiaKB] ,        
					   [TriGiaTT] ,        
						  [TriGiaKB_VND] ,        
						  [ThueSuatXNK] ,        
						  [ThueSuatTTDB] ,        
		 [ThueSuatGTGT] ,        
						  [ThueXNK] ,        
						  [ThueTTDB] ,        
						  [ThueGTGT] ,        
						  [PhuThu] ,        
						  [TyLeThuKhac] ,        
						  [TriGiaThuKhac] ,        
						  [MienThue]            
						)        
				VALUES  ( @SoToKhai ,        
						  @MaLoaiHinh ,        
						  @MaHaiQuan ,        
						  @NamDangKy ,        
						  @SoThuTuHang ,        
						  @MaHS ,        
						  @MaPhu ,        
						  @TenHang ,        
						  @NuocXX_ID ,        
						  @DVT_ID ,        
						  @SoLuong ,        
						  @DonGiaKB ,        
						  @DonGiaTT ,        
						  @TriGiaKB ,        
						  @TriGiaTT ,        
						  @TriGiaKB_VND ,        
						  @ThueSuatXNK ,        
						  @ThueSuatTTDB ,        
						  @ThueSuatGTGT ,        
						  @ThueXNK ,        
						  @ThueTTDB ,        
						  @ThueGTGT ,        
						  @PhuThu ,        
						  @TyLeThuKhac ,        
						  @TriGiaThuKhac ,        
						  @MienThue            
						)             
			END 
	end         

GO
UPDATE [dbo].[t_HaiQuan_Version]
SET [Version] = '7.2'
  ,[Date] = getdate()
  ,[Notes] = ''