using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu.CTTT
{
	public partial class CTTT_ChungTuChiTiet : ICloneable
	{
		#region Properties.
		
		public int IDChiTiet { set; get; }
		public int IDCT { set; get; }
		public int SoToKhai { set; get; }
		public string MaLoaiHinh { set; get; }
		public DateTime NgayDangKy { set; get; }
		public decimal TriGia { set; get; }
		public string GhiChu { set; get; }
		public string MaHaiQuan { set; get; }
		
		#endregion
		

		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<CTTT_ChungTuChiTiet> ConvertToCollection(IDataReader reader)
		{
			List<CTTT_ChungTuChiTiet> collection = new List<CTTT_ChungTuChiTiet>();
			while (reader.Read())
			{
				CTTT_ChungTuChiTiet entity = new CTTT_ChungTuChiTiet();
				if (!reader.IsDBNull(reader.GetOrdinal("IDChiTiet"))) entity.IDChiTiet = reader.GetInt32(reader.GetOrdinal("IDChiTiet"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDCT"))) entity.IDCT = reader.GetInt32(reader.GetOrdinal("IDCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<CTTT_ChungTuChiTiet> collection, int iDChiTiet)
        {
            foreach (CTTT_ChungTuChiTiet item in collection)
            {
                if (item.IDChiTiet == iDChiTiet)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_CTTT_ChungTuChiTiet VALUES(@IDCT, @SoToKhai, @MaLoaiHinh, @NgayDangKy, @TriGia, @GhiChu, @MaHaiQuan)";
            string update = "UPDATE t_CTTT_ChungTuChiTiet SET IDCT = @IDCT, SoToKhai = @SoToKhai, MaLoaiHinh = @MaLoaiHinh, NgayDangKy = @NgayDangKy, TriGia = @TriGia, GhiChu = @GhiChu, MaHaiQuan = @MaHaiQuan WHERE IDChiTiet = @IDChiTiet";
            string delete = "DELETE FROM t_CTTT_ChungTuChiTiet WHERE IDChiTiet = @IDChiTiet";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@IDChiTiet", SqlDbType.Int, "IDChiTiet", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IDCT", SqlDbType.Int, "IDCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Int, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@IDChiTiet", SqlDbType.Int, "IDChiTiet", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IDCT", SqlDbType.Int, "IDCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Int, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@IDChiTiet", SqlDbType.Int, "IDChiTiet", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_CTTT_ChungTuChiTiet VALUES(@IDCT, @SoToKhai, @MaLoaiHinh, @NgayDangKy, @TriGia, @GhiChu, @MaHaiQuan)";
            string update = "UPDATE t_CTTT_ChungTuChiTiet SET IDCT = @IDCT, SoToKhai = @SoToKhai, MaLoaiHinh = @MaLoaiHinh, NgayDangKy = @NgayDangKy, TriGia = @TriGia, GhiChu = @GhiChu, MaHaiQuan = @MaHaiQuan WHERE IDChiTiet = @IDChiTiet";
            string delete = "DELETE FROM t_CTTT_ChungTuChiTiet WHERE IDChiTiet = @IDChiTiet";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@IDChiTiet", SqlDbType.Int, "IDChiTiet", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IDCT", SqlDbType.Int, "IDCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Int, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@IDChiTiet", SqlDbType.Int, "IDChiTiet", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IDCT", SqlDbType.Int, "IDCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Int, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGia", SqlDbType.Decimal, "TriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@IDChiTiet", SqlDbType.Int, "IDChiTiet", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static CTTT_ChungTuChiTiet Load(int iDChiTiet)
		{
			const string spName = "[dbo].[p_CTTT_ChungTuChiTiet_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDChiTiet", SqlDbType.Int, iDChiTiet);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<CTTT_ChungTuChiTiet> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<CTTT_ChungTuChiTiet> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<CTTT_ChungTuChiTiet> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<CTTT_ChungTuChiTiet> SelectCollectionBy_IDCT(int iDCT)
		{
            IDataReader reader = SelectReaderBy_IDCT(iDCT);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_IDCT(int iDCT)
		{
			const string spName = "[dbo].[p_CTTT_ChungTuChiTiet_SelectBy_IDCT]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDCT", SqlDbType.Int, iDCT);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_CTTT_ChungTuChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_CTTT_ChungTuChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_CTTT_ChungTuChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_CTTT_ChungTuChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_IDCT(int iDCT)
		{
			const string spName = "p_CTTT_ChungTuChiTiet_SelectBy_IDCT";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDCT", SqlDbType.Int, iDCT);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertCTTT_ChungTuChiTiet(int iDCT, int soToKhai, string maLoaiHinh, DateTime ngayDangKy, decimal triGia, string ghiChu, string maHaiQuan)
		{
			CTTT_ChungTuChiTiet entity = new CTTT_ChungTuChiTiet();	
			entity.IDCT = iDCT;
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NgayDangKy = ngayDangKy;
			entity.TriGia = triGia;
			entity.GhiChu = ghiChu;
			entity.MaHaiQuan = maHaiQuan;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_CTTT_ChungTuChiTiet_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@IDChiTiet", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@IDCT", SqlDbType.Int, IDCT);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, TriGia);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				IDChiTiet = (int) db.GetParameterValue(dbCommand, "@IDChiTiet");
				return IDChiTiet;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				IDChiTiet = (int) db.GetParameterValue(dbCommand, "@IDChiTiet");
				return IDChiTiet;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<CTTT_ChungTuChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (CTTT_ChungTuChiTiet item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateCTTT_ChungTuChiTiet(int iDChiTiet, int iDCT, int soToKhai, string maLoaiHinh, DateTime ngayDangKy, decimal triGia, string ghiChu, string maHaiQuan)
		{
			CTTT_ChungTuChiTiet entity = new CTTT_ChungTuChiTiet();			
			entity.IDChiTiet = iDChiTiet;
			entity.IDCT = iDCT;
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NgayDangKy = ngayDangKy;
			entity.TriGia = triGia;
			entity.GhiChu = ghiChu;
			entity.MaHaiQuan = maHaiQuan;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_CTTT_ChungTuChiTiet_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@IDChiTiet", SqlDbType.Int, IDChiTiet);
			db.AddInParameter(dbCommand, "@IDCT", SqlDbType.Int, IDCT);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, TriGia);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<CTTT_ChungTuChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (CTTT_ChungTuChiTiet item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateCTTT_ChungTuChiTiet(int iDChiTiet, int iDCT, int soToKhai, string maLoaiHinh, DateTime ngayDangKy, decimal triGia, string ghiChu, string maHaiQuan)
		{
			CTTT_ChungTuChiTiet entity = new CTTT_ChungTuChiTiet();			
			entity.IDChiTiet = iDChiTiet;
			entity.IDCT = iDCT;
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NgayDangKy = ngayDangKy;
			entity.TriGia = triGia;
			entity.GhiChu = ghiChu;
			entity.MaHaiQuan = maHaiQuan;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_CTTT_ChungTuChiTiet_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@IDChiTiet", SqlDbType.Int, IDChiTiet);
			db.AddInParameter(dbCommand, "@IDCT", SqlDbType.Int, IDCT);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, TriGia);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<CTTT_ChungTuChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (CTTT_ChungTuChiTiet item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteCTTT_ChungTuChiTiet(int iDChiTiet)
		{
			CTTT_ChungTuChiTiet entity = new CTTT_ChungTuChiTiet();
			entity.IDChiTiet = iDChiTiet;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_CTTT_ChungTuChiTiet_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDChiTiet", SqlDbType.Int, IDChiTiet);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_IDCT(int iDCT)
		{
			const string spName = "[dbo].[p_CTTT_ChungTuChiTiet_DeleteBy_IDCT]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDCT", SqlDbType.Int, iDCT);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_CTTT_ChungTuChiTiet_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<CTTT_ChungTuChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (CTTT_ChungTuChiTiet item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}