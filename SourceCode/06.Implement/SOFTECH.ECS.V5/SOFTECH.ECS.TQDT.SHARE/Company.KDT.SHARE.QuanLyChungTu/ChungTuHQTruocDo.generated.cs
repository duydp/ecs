using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class ChungTuHQTruocDo : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public string Type { set; get; }
		public long LoaiChungTu { set; get; }
		public DateTime NgayKhaiCT { set; get; }
		public long SoDangKyCT { set; get; }
		public DateTime NgayDangKyCT { set; get; }
		public string MaHaiQuan { set; get; }
		public string MaNguoiKhaiHQ { set; get; }
		public string TenNguoiKhaiHQ { set; get; }
		public string SoChungTu { set; get; }
		public DateTime NgayChungTu { set; get; }
		public DateTime NgayHHChungTu { set; get; }
		public string MaNguoiPhatHanh { set; get; }
		public string TenNguoiPhatHanh { set; get; }
		public string MaNguoiDuocCap { set; get; }
		public string TenNguoiDuocCap { set; get; }
		public string GhiChu { set; get; }
		public bool XinNo { set; get; }
		public DateTime ThoiHanNop { set; get; }
		public string Temp1 { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<ChungTuHQTruocDo> ConvertToCollection(IDataReader reader)
		{
			IList<ChungTuHQTruocDo> collection = new List<ChungTuHQTruocDo>();
			while (reader.Read())
			{
				ChungTuHQTruocDo entity = new ChungTuHQTruocDo();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Type"))) entity.Type = reader.GetString(reader.GetOrdinal("Type"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiChungTu"))) entity.LoaiChungTu = reader.GetInt64(reader.GetOrdinal("LoaiChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiCT"))) entity.NgayKhaiCT = reader.GetDateTime(reader.GetOrdinal("NgayKhaiCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDangKyCT"))) entity.SoDangKyCT = reader.GetInt64(reader.GetOrdinal("SoDangKyCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyCT"))) entity.NgayDangKyCT = reader.GetDateTime(reader.GetOrdinal("NgayDangKyCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiKhaiHQ"))) entity.MaNguoiKhaiHQ = reader.GetString(reader.GetOrdinal("MaNguoiKhaiHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiKhaiHQ"))) entity.TenNguoiKhaiHQ = reader.GetString(reader.GetOrdinal("TenNguoiKhaiHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChungTu"))) entity.SoChungTu = reader.GetString(reader.GetOrdinal("SoChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayChungTu"))) entity.NgayChungTu = reader.GetDateTime(reader.GetOrdinal("NgayChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHHChungTu"))) entity.NgayHHChungTu = reader.GetDateTime(reader.GetOrdinal("NgayHHChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiPhatHanh"))) entity.MaNguoiPhatHanh = reader.GetString(reader.GetOrdinal("MaNguoiPhatHanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiPhatHanh"))) entity.TenNguoiPhatHanh = reader.GetString(reader.GetOrdinal("TenNguoiPhatHanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiDuocCap"))) entity.MaNguoiDuocCap = reader.GetString(reader.GetOrdinal("MaNguoiDuocCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiDuocCap"))) entity.TenNguoiDuocCap = reader.GetString(reader.GetOrdinal("TenNguoiDuocCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("XinNo"))) entity.XinNo = reader.GetBoolean(reader.GetOrdinal("XinNo"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanNop"))) entity.ThoiHanNop = reader.GetDateTime(reader.GetOrdinal("ThoiHanNop"));
				if (!reader.IsDBNull(reader.GetOrdinal("Temp1"))) entity.Temp1 = reader.GetString(reader.GetOrdinal("Temp1"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<ChungTuHQTruocDo> collection, long id)
        {
            foreach (ChungTuHQTruocDo item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_ChungTuHQTruocDo VALUES(@Master_ID, @Type, @LoaiChungTu, @NgayKhaiCT, @SoDangKyCT, @NgayDangKyCT, @MaHaiQuan, @MaNguoiKhaiHQ, @TenNguoiKhaiHQ, @SoChungTu, @NgayChungTu, @NgayHHChungTu, @MaNguoiPhatHanh, @TenNguoiPhatHanh, @MaNguoiDuocCap, @TenNguoiDuocCap, @GhiChu, @XinNo, @ThoiHanNop, @Temp1)";
            string update = "UPDATE t_KDT_ChungTuHQTruocDo SET Master_ID = @Master_ID, Type = @Type, LoaiChungTu = @LoaiChungTu, NgayKhaiCT = @NgayKhaiCT, SoDangKyCT = @SoDangKyCT, NgayDangKyCT = @NgayDangKyCT, MaHaiQuan = @MaHaiQuan, MaNguoiKhaiHQ = @MaNguoiKhaiHQ, TenNguoiKhaiHQ = @TenNguoiKhaiHQ, SoChungTu = @SoChungTu, NgayChungTu = @NgayChungTu, NgayHHChungTu = @NgayHHChungTu, MaNguoiPhatHanh = @MaNguoiPhatHanh, TenNguoiPhatHanh = @TenNguoiPhatHanh, MaNguoiDuocCap = @MaNguoiDuocCap, TenNguoiDuocCap = @TenNguoiDuocCap, GhiChu = @GhiChu, XinNo = @XinNo, ThoiHanNop = @ThoiHanNop, Temp1 = @Temp1 WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_ChungTuHQTruocDo WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Type", SqlDbType.VarChar, "Type", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.BigInt, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiCT", SqlDbType.DateTime, "NgayKhaiCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDangKyCT", SqlDbType.BigInt, "SoDangKyCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyCT", SqlDbType.DateTime, "NgayDangKyCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhaiHQ", SqlDbType.NVarChar, "MaNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, "TenNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHHChungTu", SqlDbType.DateTime, "NgayHHChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiPhatHanh", SqlDbType.NVarChar, "MaNguoiPhatHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiPhatHanh", SqlDbType.NVarChar, "TenNguoiPhatHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiDuocCap", SqlDbType.NVarChar, "MaNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiDuocCap", SqlDbType.NVarChar, "TenNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@XinNo", SqlDbType.Bit, "XinNo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiHanNop", SqlDbType.DateTime, "ThoiHanNop", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Temp1", SqlDbType.NVarChar, "Temp1", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Type", SqlDbType.VarChar, "Type", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.BigInt, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiCT", SqlDbType.DateTime, "NgayKhaiCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDangKyCT", SqlDbType.BigInt, "SoDangKyCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyCT", SqlDbType.DateTime, "NgayDangKyCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhaiHQ", SqlDbType.NVarChar, "MaNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, "TenNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHHChungTu", SqlDbType.DateTime, "NgayHHChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiPhatHanh", SqlDbType.NVarChar, "MaNguoiPhatHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiPhatHanh", SqlDbType.NVarChar, "TenNguoiPhatHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiDuocCap", SqlDbType.NVarChar, "MaNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiDuocCap", SqlDbType.NVarChar, "TenNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@XinNo", SqlDbType.Bit, "XinNo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiHanNop", SqlDbType.DateTime, "ThoiHanNop", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Temp1", SqlDbType.NVarChar, "Temp1", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_ChungTuHQTruocDo VALUES(@Master_ID, @Type, @LoaiChungTu, @NgayKhaiCT, @SoDangKyCT, @NgayDangKyCT, @MaHaiQuan, @MaNguoiKhaiHQ, @TenNguoiKhaiHQ, @SoChungTu, @NgayChungTu, @NgayHHChungTu, @MaNguoiPhatHanh, @TenNguoiPhatHanh, @MaNguoiDuocCap, @TenNguoiDuocCap, @GhiChu, @XinNo, @ThoiHanNop, @Temp1)";
            string update = "UPDATE t_KDT_ChungTuHQTruocDo SET Master_ID = @Master_ID, Type = @Type, LoaiChungTu = @LoaiChungTu, NgayKhaiCT = @NgayKhaiCT, SoDangKyCT = @SoDangKyCT, NgayDangKyCT = @NgayDangKyCT, MaHaiQuan = @MaHaiQuan, MaNguoiKhaiHQ = @MaNguoiKhaiHQ, TenNguoiKhaiHQ = @TenNguoiKhaiHQ, SoChungTu = @SoChungTu, NgayChungTu = @NgayChungTu, NgayHHChungTu = @NgayHHChungTu, MaNguoiPhatHanh = @MaNguoiPhatHanh, TenNguoiPhatHanh = @TenNguoiPhatHanh, MaNguoiDuocCap = @MaNguoiDuocCap, TenNguoiDuocCap = @TenNguoiDuocCap, GhiChu = @GhiChu, XinNo = @XinNo, ThoiHanNop = @ThoiHanNop, Temp1 = @Temp1 WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_ChungTuHQTruocDo WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Type", SqlDbType.VarChar, "Type", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.BigInt, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiCT", SqlDbType.DateTime, "NgayKhaiCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDangKyCT", SqlDbType.BigInt, "SoDangKyCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyCT", SqlDbType.DateTime, "NgayDangKyCT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhaiHQ", SqlDbType.NVarChar, "MaNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, "TenNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHHChungTu", SqlDbType.DateTime, "NgayHHChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiPhatHanh", SqlDbType.NVarChar, "MaNguoiPhatHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiPhatHanh", SqlDbType.NVarChar, "TenNguoiPhatHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiDuocCap", SqlDbType.NVarChar, "MaNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiDuocCap", SqlDbType.NVarChar, "TenNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@XinNo", SqlDbType.Bit, "XinNo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiHanNop", SqlDbType.DateTime, "ThoiHanNop", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Temp1", SqlDbType.NVarChar, "Temp1", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Type", SqlDbType.VarChar, "Type", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.BigInt, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiCT", SqlDbType.DateTime, "NgayKhaiCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDangKyCT", SqlDbType.BigInt, "SoDangKyCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyCT", SqlDbType.DateTime, "NgayDangKyCT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhaiHQ", SqlDbType.NVarChar, "MaNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, "TenNguoiKhaiHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHHChungTu", SqlDbType.DateTime, "NgayHHChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiPhatHanh", SqlDbType.NVarChar, "MaNguoiPhatHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiPhatHanh", SqlDbType.NVarChar, "TenNguoiPhatHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiDuocCap", SqlDbType.NVarChar, "MaNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiDuocCap", SqlDbType.NVarChar, "TenNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@XinNo", SqlDbType.Bit, "XinNo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiHanNop", SqlDbType.DateTime, "ThoiHanNop", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Temp1", SqlDbType.NVarChar, "Temp1", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static ChungTuHQTruocDo Load(long id)
		{
			const string spName = "[dbo].[p_KDT_ChungTuHQTruocDo_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<ChungTuHQTruocDo> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<ChungTuHQTruocDo> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<ChungTuHQTruocDo> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_ChungTuHQTruocDo_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ChungTuHQTruocDo_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_ChungTuHQTruocDo_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ChungTuHQTruocDo_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertChungTuHQTruocDo(long master_ID, string type, long loaiChungTu, DateTime ngayKhaiCT, long soDangKyCT, DateTime ngayDangKyCT, string maHaiQuan, string maNguoiKhaiHQ, string tenNguoiKhaiHQ, string soChungTu, DateTime ngayChungTu, DateTime ngayHHChungTu, string maNguoiPhatHanh, string tenNguoiPhatHanh, string maNguoiDuocCap, string tenNguoiDuocCap, string ghiChu, bool xinNo, DateTime thoiHanNop, string temp1)
		{
			ChungTuHQTruocDo entity = new ChungTuHQTruocDo();	
			entity.Master_ID = master_ID;
			entity.Type = type;
			entity.LoaiChungTu = loaiChungTu;
			entity.NgayKhaiCT = ngayKhaiCT;
			entity.SoDangKyCT = soDangKyCT;
			entity.NgayDangKyCT = ngayDangKyCT;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaNguoiKhaiHQ = maNguoiKhaiHQ;
			entity.TenNguoiKhaiHQ = tenNguoiKhaiHQ;
			entity.SoChungTu = soChungTu;
			entity.NgayChungTu = ngayChungTu;
			entity.NgayHHChungTu = ngayHHChungTu;
			entity.MaNguoiPhatHanh = maNguoiPhatHanh;
			entity.TenNguoiPhatHanh = tenNguoiPhatHanh;
			entity.MaNguoiDuocCap = maNguoiDuocCap;
			entity.TenNguoiDuocCap = tenNguoiDuocCap;
			entity.GhiChu = ghiChu;
			entity.XinNo = xinNo;
			entity.ThoiHanNop = thoiHanNop;
			entity.Temp1 = temp1;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_ChungTuHQTruocDo_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@Type", SqlDbType.VarChar, Type);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.BigInt, LoaiChungTu);
			db.AddInParameter(dbCommand, "@NgayKhaiCT", SqlDbType.DateTime, NgayKhaiCT.Year <= 1753 ? DBNull.Value : (object) NgayKhaiCT);
			db.AddInParameter(dbCommand, "@SoDangKyCT", SqlDbType.BigInt, SoDangKyCT);
			db.AddInParameter(dbCommand, "@NgayDangKyCT", SqlDbType.DateTime, NgayDangKyCT.Year <= 1753 ? DBNull.Value : (object) NgayDangKyCT);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaNguoiKhaiHQ", SqlDbType.NVarChar, MaNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, TenNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@NgayHHChungTu", SqlDbType.DateTime, NgayHHChungTu.Year <= 1753 ? DBNull.Value : (object) NgayHHChungTu);
			db.AddInParameter(dbCommand, "@MaNguoiPhatHanh", SqlDbType.NVarChar, MaNguoiPhatHanh);
			db.AddInParameter(dbCommand, "@TenNguoiPhatHanh", SqlDbType.NVarChar, TenNguoiPhatHanh);
			db.AddInParameter(dbCommand, "@MaNguoiDuocCap", SqlDbType.NVarChar, MaNguoiDuocCap);
			db.AddInParameter(dbCommand, "@TenNguoiDuocCap", SqlDbType.NVarChar, TenNguoiDuocCap);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@XinNo", SqlDbType.Bit, XinNo);
			db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year <= 1753 ? DBNull.Value : (object) ThoiHanNop);
			db.AddInParameter(dbCommand, "@Temp1", SqlDbType.NVarChar, Temp1);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<ChungTuHQTruocDo> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuHQTruocDo item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateChungTuHQTruocDo(long id, long master_ID, string type, long loaiChungTu, DateTime ngayKhaiCT, long soDangKyCT, DateTime ngayDangKyCT, string maHaiQuan, string maNguoiKhaiHQ, string tenNguoiKhaiHQ, string soChungTu, DateTime ngayChungTu, DateTime ngayHHChungTu, string maNguoiPhatHanh, string tenNguoiPhatHanh, string maNguoiDuocCap, string tenNguoiDuocCap, string ghiChu, bool xinNo, DateTime thoiHanNop, string temp1)
		{
			ChungTuHQTruocDo entity = new ChungTuHQTruocDo();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.Type = type;
			entity.LoaiChungTu = loaiChungTu;
			entity.NgayKhaiCT = ngayKhaiCT;
			entity.SoDangKyCT = soDangKyCT;
			entity.NgayDangKyCT = ngayDangKyCT;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaNguoiKhaiHQ = maNguoiKhaiHQ;
			entity.TenNguoiKhaiHQ = tenNguoiKhaiHQ;
			entity.SoChungTu = soChungTu;
			entity.NgayChungTu = ngayChungTu;
			entity.NgayHHChungTu = ngayHHChungTu;
			entity.MaNguoiPhatHanh = maNguoiPhatHanh;
			entity.TenNguoiPhatHanh = tenNguoiPhatHanh;
			entity.MaNguoiDuocCap = maNguoiDuocCap;
			entity.TenNguoiDuocCap = tenNguoiDuocCap;
			entity.GhiChu = ghiChu;
			entity.XinNo = xinNo;
			entity.ThoiHanNop = thoiHanNop;
			entity.Temp1 = temp1;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_ChungTuHQTruocDo_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@Type", SqlDbType.VarChar, Type);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.BigInt, LoaiChungTu);
			db.AddInParameter(dbCommand, "@NgayKhaiCT", SqlDbType.DateTime, NgayKhaiCT.Year <= 1753 ? DBNull.Value : (object) NgayKhaiCT);
			db.AddInParameter(dbCommand, "@SoDangKyCT", SqlDbType.BigInt, SoDangKyCT);
			db.AddInParameter(dbCommand, "@NgayDangKyCT", SqlDbType.DateTime, NgayDangKyCT.Year <= 1753 ? DBNull.Value : (object) NgayDangKyCT);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaNguoiKhaiHQ", SqlDbType.NVarChar, MaNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, TenNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@NgayHHChungTu", SqlDbType.DateTime, NgayHHChungTu.Year <= 1753 ? DBNull.Value : (object) NgayHHChungTu);
			db.AddInParameter(dbCommand, "@MaNguoiPhatHanh", SqlDbType.NVarChar, MaNguoiPhatHanh);
			db.AddInParameter(dbCommand, "@TenNguoiPhatHanh", SqlDbType.NVarChar, TenNguoiPhatHanh);
			db.AddInParameter(dbCommand, "@MaNguoiDuocCap", SqlDbType.NVarChar, MaNguoiDuocCap);
			db.AddInParameter(dbCommand, "@TenNguoiDuocCap", SqlDbType.NVarChar, TenNguoiDuocCap);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@XinNo", SqlDbType.Bit, XinNo);
			db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year <= 1753 ? DBNull.Value : (object) ThoiHanNop);
			db.AddInParameter(dbCommand, "@Temp1", SqlDbType.NVarChar, Temp1);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<ChungTuHQTruocDo> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuHQTruocDo item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateChungTuHQTruocDo(long id, long master_ID, string type, long loaiChungTu, DateTime ngayKhaiCT, long soDangKyCT, DateTime ngayDangKyCT, string maHaiQuan, string maNguoiKhaiHQ, string tenNguoiKhaiHQ, string soChungTu, DateTime ngayChungTu, DateTime ngayHHChungTu, string maNguoiPhatHanh, string tenNguoiPhatHanh, string maNguoiDuocCap, string tenNguoiDuocCap, string ghiChu, bool xinNo, DateTime thoiHanNop, string temp1)
		{
			ChungTuHQTruocDo entity = new ChungTuHQTruocDo();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.Type = type;
			entity.LoaiChungTu = loaiChungTu;
			entity.NgayKhaiCT = ngayKhaiCT;
			entity.SoDangKyCT = soDangKyCT;
			entity.NgayDangKyCT = ngayDangKyCT;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaNguoiKhaiHQ = maNguoiKhaiHQ;
			entity.TenNguoiKhaiHQ = tenNguoiKhaiHQ;
			entity.SoChungTu = soChungTu;
			entity.NgayChungTu = ngayChungTu;
			entity.NgayHHChungTu = ngayHHChungTu;
			entity.MaNguoiPhatHanh = maNguoiPhatHanh;
			entity.TenNguoiPhatHanh = tenNguoiPhatHanh;
			entity.MaNguoiDuocCap = maNguoiDuocCap;
			entity.TenNguoiDuocCap = tenNguoiDuocCap;
			entity.GhiChu = ghiChu;
			entity.XinNo = xinNo;
			entity.ThoiHanNop = thoiHanNop;
			entity.Temp1 = temp1;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ChungTuHQTruocDo_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@Type", SqlDbType.VarChar, Type);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.BigInt, LoaiChungTu);
			db.AddInParameter(dbCommand, "@NgayKhaiCT", SqlDbType.DateTime, NgayKhaiCT.Year <= 1753 ? DBNull.Value : (object) NgayKhaiCT);
			db.AddInParameter(dbCommand, "@SoDangKyCT", SqlDbType.BigInt, SoDangKyCT);
			db.AddInParameter(dbCommand, "@NgayDangKyCT", SqlDbType.DateTime, NgayDangKyCT.Year <= 1753 ? DBNull.Value : (object) NgayDangKyCT);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaNguoiKhaiHQ", SqlDbType.NVarChar, MaNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@TenNguoiKhaiHQ", SqlDbType.NVarChar, TenNguoiKhaiHQ);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@NgayHHChungTu", SqlDbType.DateTime, NgayHHChungTu.Year <= 1753 ? DBNull.Value : (object) NgayHHChungTu);
			db.AddInParameter(dbCommand, "@MaNguoiPhatHanh", SqlDbType.NVarChar, MaNguoiPhatHanh);
			db.AddInParameter(dbCommand, "@TenNguoiPhatHanh", SqlDbType.NVarChar, TenNguoiPhatHanh);
			db.AddInParameter(dbCommand, "@MaNguoiDuocCap", SqlDbType.NVarChar, MaNguoiDuocCap);
			db.AddInParameter(dbCommand, "@TenNguoiDuocCap", SqlDbType.NVarChar, TenNguoiDuocCap);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@XinNo", SqlDbType.Bit, XinNo);
			db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year <= 1753 ? DBNull.Value : (object) ThoiHanNop);
			db.AddInParameter(dbCommand, "@Temp1", SqlDbType.NVarChar, Temp1);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<ChungTuHQTruocDo> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuHQTruocDo item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteChungTuHQTruocDo(long id)
		{
			ChungTuHQTruocDo entity = new ChungTuHQTruocDo();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ChungTuHQTruocDo_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_ChungTuHQTruocDo_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<ChungTuHQTruocDo> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungTuHQTruocDo item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}