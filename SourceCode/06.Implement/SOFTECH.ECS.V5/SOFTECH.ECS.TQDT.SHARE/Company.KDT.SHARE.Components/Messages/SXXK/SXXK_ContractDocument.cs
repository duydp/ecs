﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Danh Sách hợp đồng khai kèm
    /// </summary>
    public class SXXK_ContractDocument : DeclarationBase
    {
        [XmlElement("Material")]
        public List<SXXK_Product> Materials { get; set; }

        [XmlElement("Commodity")]
        public List<Commodity> Commoditys { get; set; }

        #region Phục vụ bảng kê NPL tái xuất
        [XmlElement("ExportDeclarationDocument")]
        public List<SXXK_ContractDocument> ExportDeclarationDocument { get; set; }

        [XmlElement("GoodsMeasure")]
        public List<GoodsMeasure> GoodsMeasures { get; set; }
        #endregion


        [XmlElement("PaymentDocument")]
        public PaymentDocument PaymentDocument { get; set; }
        [XmlElement("customsValue")]
        public string CustomsValue { get; set; }
        /// <summary>
        /// Hình thức xử lý (1=tiêu thụ nội địa, 2=tiêu huỷ,...)
        /// </summary>
        [XmlElement("methodOfProcess")]
        public string MethodOfProcess { get; set; }
    }
}
