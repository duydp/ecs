using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Activation.Server
{
    public partial class InvoiceDetail
    {
        #region Private members.

        protected string _ID = string.Empty;
        protected string _InvoiceID = string.Empty;
        protected string _ProductID = string.Empty;
        protected string _LicenseID = string.Empty;
        protected string _SerialCode = string.Empty;
        protected int _PaymentTerms;
        protected double _Price;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public string ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }

        public string InvoiceID
        {
            set { this._InvoiceID = value; }
            get { return this._InvoiceID; }
        }

        public string ProductID
        {
            set { this._ProductID = value; }
            get { return this._ProductID; }
        }

        public string LicenseID
        {
            set { this._LicenseID = value; }
            get { return this._LicenseID; }
        }

        public string SerialCode
        {
            set { this._SerialCode = value; }
            get { return this._SerialCode; }
        }

        public int PaymentTerms
        {
            set { this._PaymentTerms = value; }
            get { return this._PaymentTerms; }
        }

        public double Price
        {
            set { this._Price = value; }
            get { return this._Price; }
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static InvoiceDetail Load(string iD)
        {
            const string spName = "[dbo].[p_LICENSE_InvoiceDetail_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, iD);
            InvoiceDetail entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new InvoiceDetail();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetString(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("InvoiceID"))) entity.InvoiceID = reader.GetString(reader.GetOrdinal("InvoiceID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ProductID"))) entity.ProductID = reader.GetString(reader.GetOrdinal("ProductID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LicenseID"))) entity.LicenseID = reader.GetString(reader.GetOrdinal("LicenseID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SerialCode"))) entity.SerialCode = reader.GetString(reader.GetOrdinal("SerialCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("PaymentTerms"))) entity.PaymentTerms = reader.GetInt32(reader.GetOrdinal("PaymentTerms"));
                if (!reader.IsDBNull(reader.GetOrdinal("Price"))) entity.Price = reader.GetDouble(reader.GetOrdinal("Price"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_LicenseID(string licenseID)
        {
            const string spName = "[dbo].[p_LICENSE_InvoiceDetail_SelectBy_LicenseID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@LicenseID", SqlDbType.VarChar, licenseID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static DataSet SelectBy_SerialCode(string serialCode)
        {
            const string spName = "[dbo].[p_LICENSE_InvoiceDetail_SelectBy_SerialCode]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, serialCode);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static DataSet SelectBy_InvoiceID(string invoiceID)
        {
            const string spName = "[dbo].[p_LICENSE_InvoiceDetail_SelectBy_InvoiceID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@InvoiceID", SqlDbType.VarChar, invoiceID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static DataSet SelectBy_ProductID(string productID)
        {
            const string spName = "[dbo].[p_LICENSE_InvoiceDetail_SelectBy_ProductID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, productID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LICENSE_InvoiceDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_LICENSE_InvoiceDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LICENSE_InvoiceDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_LICENSE_InvoiceDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_LicenseID(string licenseID)
        {
            const string spName = "p_LICENSE_InvoiceDetail_SelectBy_LicenseID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@LicenseID", SqlDbType.VarChar, licenseID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static IDataReader SelectReaderBy_SerialCode(string serialCode)
        {
            const string spName = "p_LICENSE_InvoiceDetail_SelectBy_SerialCode";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, serialCode);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static IDataReader SelectReaderBy_InvoiceID(string invoiceID)
        {
            const string spName = "p_LICENSE_InvoiceDetail_SelectBy_InvoiceID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@InvoiceID", SqlDbType.VarChar, invoiceID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static IDataReader SelectReaderBy_ProductID(string productID)
        {
            const string spName = "p_LICENSE_InvoiceDetail_SelectBy_ProductID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, productID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static string InsertInvoiceDetail(string invoiceID, string productID, string licenseID, string serialCode, int paymentTerms, double price)
        {
            InvoiceDetail entity = new InvoiceDetail();
            entity.InvoiceID = invoiceID;
            entity.ProductID = productID;
            entity.LicenseID = licenseID;
            entity.SerialCode = serialCode;
            entity.PaymentTerms = paymentTerms;
            entity.Price = price;
            return entity.Insert();
        }

        public string Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public string Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_InvoiceDetail_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
            db.AddInParameter(dbCommand, "@InvoiceID", SqlDbType.VarChar, InvoiceID);
            db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, ProductID);
            db.AddInParameter(dbCommand, "@LicenseID", SqlDbType.VarChar, LicenseID);
            db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, SerialCode);
            db.AddInParameter(dbCommand, "@PaymentTerms", SqlDbType.Int, PaymentTerms);
            db.AddInParameter(dbCommand, "@Price", SqlDbType.Float, Price);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (string)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                this._ID = (string)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateInvoiceDetail(string iD, string invoiceID, string productID, string licenseID, string serialCode, int paymentTerms, double price)
        {
            InvoiceDetail entity = new InvoiceDetail();
            entity.InvoiceID = invoiceID;
            entity.ProductID = productID;
            entity.LicenseID = licenseID;
            entity.SerialCode = serialCode;
            entity.PaymentTerms = paymentTerms;
            entity.Price = price;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_LICENSE_InvoiceDetail_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
            db.AddInParameter(dbCommand, "@InvoiceID", SqlDbType.VarChar, InvoiceID);
            db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, ProductID);
            db.AddInParameter(dbCommand, "@LicenseID", SqlDbType.VarChar, LicenseID);
            db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, SerialCode);
            db.AddInParameter(dbCommand, "@PaymentTerms", SqlDbType.Int, PaymentTerms);
            db.AddInParameter(dbCommand, "@Price", SqlDbType.Float, Price);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateInvoiceDetail(string iD, string invoiceID, string productID, string licenseID, string serialCode, int paymentTerms, double price)
        {
            InvoiceDetail entity = new InvoiceDetail();
            entity.ID = iD;
            entity.InvoiceID = invoiceID;
            entity.ProductID = productID;
            entity.LicenseID = licenseID;
            entity.SerialCode = serialCode;
            entity.PaymentTerms = paymentTerms;
            entity.Price = price;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_InvoiceDetail_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
            db.AddInParameter(dbCommand, "@InvoiceID", SqlDbType.VarChar, InvoiceID);
            db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, ProductID);
            db.AddInParameter(dbCommand, "@LicenseID", SqlDbType.VarChar, LicenseID);
            db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, SerialCode);
            db.AddInParameter(dbCommand, "@PaymentTerms", SqlDbType.Int, PaymentTerms);
            db.AddInParameter(dbCommand, "@Price", SqlDbType.Float, Price);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteInvoiceDetail(string iD)
        {
            InvoiceDetail entity = new InvoiceDetail();
            entity.ID = iD;
            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_InvoiceDetail_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_LicenseID(string licenseID)
        {
            string spName = "[dbo].[p_LICENSE_InvoiceDetail_DeleteBy_LicenseID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@LicenseID", SqlDbType.VarChar, licenseID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_SerialCode(string serialCode)
        {
            string spName = "[dbo].[p_LICENSE_InvoiceDetail_DeleteBy_SerialCode]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SerialCode", SqlDbType.VarChar, serialCode);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_InvoiceID(string invoiceID)
        {
            string spName = "[dbo].[p_LICENSE_InvoiceDetail_DeleteBy_InvoiceID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@InvoiceID", SqlDbType.VarChar, invoiceID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_ProductID(string productID)
        {
            string spName = "[dbo].[p_LICENSE_InvoiceDetail_DeleteBy_ProductID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ProductID", SqlDbType.VarChar, productID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        #endregion
    }
}