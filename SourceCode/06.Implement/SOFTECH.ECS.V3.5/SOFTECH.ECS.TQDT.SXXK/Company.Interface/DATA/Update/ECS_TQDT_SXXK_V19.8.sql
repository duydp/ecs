
/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BC07_GetBCXNTByLanThanhLy]    Script Date: 09/22/2014 15:41:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
----------------------------------------------------------------------------------------------------  
  
ALTER PROCEDURE [dbo].[p_KDT_SXXK_BC07_GetBCXNTByLanThanhLy]  
 @MaDoanhNghiep NVarchar(14),
 @MaHaiQuan varchar (6),  
 @LanThanhLy int  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  

Create table #BangKeNPLTon 
		    ( MaNPL nvarchar(80),
			 TenNPL nvarchar(255),
			 LuongTonDau numeric(24,8),
			 LuongNhap	numeric(24,8),
			LuongNPLSuDung numeric(24,8),
			LuongNPLTaiXuat numeric(24,8),
			LuongNopThue numeric(24,8),
			LuongXuatTheoHD numeric(24,8),
			LuongTonCuoi numeric(24,8),
			DVT_ID varchar(10)
			)

insert into #BangKeNPLTon (MaNPL ,
			 TenNPL ,
			 LuongTonDau ,
			 LuongNhap	,
			LuongNPLSuDung ,
			LuongNPLTaiXuat ,
			LuongNopThue,
			LuongXuatTheoHD ,
			LuongTonCuoi,
			DVT_ID )

Select   
 A.MaNPL,  
 MAX(B.Ten) as TenNPL,   
 (SUM(A.LuongTonDau) - Sum(LuongNhap)) as LuongTonDau,  
 Sum(LuongNhap) as LuongNhap,  
 Sum(LuongNPLSuDung) as LuongNPLSuDung,  
 Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,  
 Sum(LuongNopThue) as LuongNopThue,  
 Sum(LuongXuatTheoHD) as LuongXuatTheoHD,  
 Sum(LuongTonCuoi) as LuongTonCuoi,  
 B.DVT_ID --MAX(ID_DVT_NPL) as ID_DVT_NPL,  

 From   
(   
SELECT  
 SoToKhaiNhap,  
 NgayDangKyNhap,  
 MaNPL,  
 TenNPL,   
 LuongTonDau,  
 CASE when  LuongNhap = LuongTonDau then LuongNhap else 0 end as LuongNhap,   
 Sum(Case when ( LuongNPLSuDung + LuongTonCuoi) > LuongTonDau then LuongTonDau - LuongTonCuoi else LuongNPLSuDung end)  as LuongNPLSuDung,   
 Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,   
 0 as LuongNopThue,  
 0 as LuongXuatTheoHD,  
 Case When Min(LuongTonCuoi) < 0 then 0 else Min(LuongTonCuoi) end as LuongTonCuoi,  
 (select ID from t_HaiQuan_DonViTinh where ten=TenDVT_NPL)as ID_DVT_NPL  

    
FROM  
 [dbo].[t_KDT_SXXK_BCXuatNhapTon]  
WHERE MaDoanhNghiep =@MaDoanhNghiep AND LanThanhLy =@LanThanhLy  
GROUP BY NgayDangKyNhap, SoToKhaiNhap, MaLoaiHinhNhap, MaNPL,TenNPL, TenDVT_NPL,DonGiaTT, LuongTonDau,LuongNhap  
) as A inner join t_SXXK_NguyenPhuLieu B  
on A.MaNPL = B.Ma  AND B.MaDoanhNghiep = @MaDoanhNghiep And B.MaHaiQuan= @MaHaiQuan
-- inner join (select MaNPL,TenNPL, Luong as LuongTonDau from [t_KDT_SXXK_BKNPLChuaThanhLy] where BangKeHoSoThanhLy_ID=2476 )D
--on A.MaNPL =  D.MaNPL   
Group by A.MaNPL,B.DVT_ID  
order by A.MaNPL  

 if(exists( select * from t_KDT_SXXK_BangKeHoSoThanhLy where MaterID = (select top 1 ID from  t_KDT_SXXK_HoSoThanhLyDangKy where LanThanhLy = @LanThanhLy AND MaBangKe = 'DTLNPLCHUATL')))
 begin 
  update #BangKeNPLTon set LuongTonCuoi = LuongTonCuoi + b.Luong , LuongTonDau = LuongTonDau + b.Luong
  from #BangKeNPLTon a inner join (select sum(Luong)as Luong,MaNPL,DVT_ID  from t_KDT_SXXK_BKNPLChuaThanhLy where BangKeHoSoThanhLy_ID = (select top 1 ID from t_KDT_SXXK_BangKeHoSoThanhLy where MaterID = (select top 1 ID from  t_KDT_SXXK_HoSoThanhLyDangKy where LanThanhLy = @LanThanhLY AND MaBangKe = 'DTLNPLCHUATL')) group by MaNPL,DVT_ID) b
  on a.MaNPL = b.MaNPL and a.DVT_ID = b.DVT_ID
end

select * from #BangKeNPLTon
drop table #BangKeNPLTon

Go

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BCXuatNhapTon_GetBCXNTByLanThanhLy]    Script Date: 09/25/2014 14:01:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
ALTER PROCEDURE [dbo].[p_KDT_SXXK_BCXuatNhapTon_GetBCXNTByLanThanhLy]      
 @MaDoanhNghiep NVarchar(14),      
 @LanThanhLy int      
AS      
      
SET NOCOUNT ON      
SET TRANSACTION ISOLATION LEVEL READ COMMITTED      
  
Select    
MAX(SoToKhaiNhap) AS SoToKhaiNhap,     
MAX(A.MaLoaiHinhNhap) AS MaLoaiHinh,
YEAR(MAX(NgayDangKyNhap)) AS NamDK,
 MaNPL,      
 MAX(TenNPL) as TenNPL,       
 SUM(LuongTonDau) as LuongTonDau,      
 Sum(LuongNhap) as LuongNhap,      
 Sum(LuongNPLSuDung) as LuongNPLSuDung,      
 Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,      
 Sum(LuongNopThue) as LuongNopThue,      
 Sum(LuongXuatTheoHD) as LuongXuatTheoHD,      
 Sum(LuongTonCuoi) as LuongTonCuoi,      
 B.DVT_ID, --MAX(ID_DVT_NPL) as ID_DVT_NPL,      
 DonGiaTT,
 ThueSuat as ThueSuat     
 From       
(       
SELECT      
 SoToKhaiNhap, 
 MaLoaiHinhNhap,     
 NgayDangKyNhap,      
 MaNPL,      
 TenNPL,       
 LuongTonDau,      
 CASE when  LuongNhap = LuongTonDau then LuongNhap else 0 end as LuongNhap,       
 Sum(Case when ( LuongNPLSuDung + LuongTonCuoi) > LuongTonDau then LuongTonDau - LuongTonCuoi else LuongNPLSuDung end)  as LuongNPLSuDung,       
 Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,       
 0 as LuongNopThue,      
 0 as LuongXuatTheoHD,      
 --case       
 -- when (LuongTonDau - Sum(LuongNPLSuDung)) >= 0       
 -- then  LuongTonDau - Sum(LuongNPLSuDung)      
 -- else 0      
 -- end       
 -- as LuongTonCuoi,      
 Case When Min(LuongTonCuoi) < 0 then 0 else Min(LuongTonCuoi) end as LuongTonCuoi,      
 (select ID from t_HaiQuan_DonViTinh where ten=TenDVT_NPL)as ID_DVT_NPL,      
 DonGiaTT,      
 ThueSuat       
FROM      
 [dbo].[t_KDT_SXXK_BCXuatNhapTon]      
WHERE MaDoanhNghiep =@MaDoanhNghiep AND LanThanhLy =@LanThanhLy      
GROUP BY NgayDangKyNhap, SoToKhaiNhap, MaLoaiHinhNhap, MaNPL,TenNPL, TenDVT_NPL,DonGiaTT, LuongTonDau,LuongNhap,ThueSuat      
) as A inner join t_SXXK_NguyenPhuLieu B      
on A.MaNPL = B.Ma  AND B.MaDoanhNghiep = @MaDoanhNghiep    
      
Group by MaNPL,DonGiaTT,B.DVT_ID, ThueSuat      
order by MaNPL 

Go



IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_SXXK_BKNPLTuCungUng_Detail]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_SXXK_BKNPLTuCungUng_Detail](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[BangKeHoSoThanhLy_ID] [bigint] NOT NULL,
	[MaNPL] [nvarchar](100) NOT NULL,
	[TenNPL] [nvarchar](255) NULL,
	[DVT_ID] [varchar](6) NULL,
	[DonGiaTT] [numeric](24, 6) NULL,
	[ThueSuat] [numeric](8, 4) NULL,
	[LuongTuCungUng] [numeric](18, 6) NOT NULL,
	[SoChungTu] [varchar](50) NOT NULL,
	[NgayChungTu] [datetime] NOT NULL,
	[LoaiNPL] [int] NULL,
	[GiaiTrinh] [nvarchar](255) NULL,
 CONSTRAINT [PK_t_KDT_SXXK_BKNPLTuCungUng_Detail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
End

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Insert]
	@BangKeHoSoThanhLy_ID bigint,
	@MaNPL nvarchar(100),
	@TenNPL nvarchar(255),
	@DVT_ID varchar(6),
	@DonGiaTT numeric(24, 6),
	@ThueSuat numeric(8, 4),
	@LuongTuCungUng numeric(18, 6),
	@SoChungTu varchar(50),
	@NgayChungTu datetime,
	@LoaiNPL int,
	@GiaiTrinh nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_BKNPLTuCungUng_Detail]
(
	[BangKeHoSoThanhLy_ID],
	[MaNPL],
	[TenNPL],
	[DVT_ID],
	[DonGiaTT],
	[ThueSuat],
	[LuongTuCungUng],
	[SoChungTu],
	[NgayChungTu],
	[LoaiNPL],
	[GiaiTrinh]
)
VALUES 
(
	@BangKeHoSoThanhLy_ID,
	@MaNPL,
	@TenNPL,
	@DVT_ID,
	@DonGiaTT,
	@ThueSuat,
	@LuongTuCungUng,
	@SoChungTu,
	@NgayChungTu,
	@LoaiNPL,
	@GiaiTrinh
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Update]
	@ID bigint,
	@BangKeHoSoThanhLy_ID bigint,
	@MaNPL nvarchar(100),
	@TenNPL nvarchar(255),
	@DVT_ID varchar(6),
	@DonGiaTT numeric(24, 6),
	@ThueSuat numeric(8, 4),
	@LuongTuCungUng numeric(18, 6),
	@SoChungTu varchar(50),
	@NgayChungTu datetime,
	@LoaiNPL int,
	@GiaiTrinh nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_SXXK_BKNPLTuCungUng_Detail]
SET
	[BangKeHoSoThanhLy_ID] = @BangKeHoSoThanhLy_ID,
	[MaNPL] = @MaNPL,
	[TenNPL] = @TenNPL,
	[DVT_ID] = @DVT_ID,
	[DonGiaTT] = @DonGiaTT,
	[ThueSuat] = @ThueSuat,
	[LuongTuCungUng] = @LuongTuCungUng,
	[SoChungTu] = @SoChungTu,
	[NgayChungTu] = @NgayChungTu,
	[LoaiNPL] = @LoaiNPL,
	[GiaiTrinh] = @GiaiTrinh
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_InsertUpdate]
	@ID bigint,
	@BangKeHoSoThanhLy_ID bigint,
	@MaNPL nvarchar(100),
	@TenNPL nvarchar(255),
	@DVT_ID varchar(6),
	@DonGiaTT numeric(24, 6),
	@ThueSuat numeric(8, 4),
	@LuongTuCungUng numeric(18, 6),
	@SoChungTu varchar(50),
	@NgayChungTu datetime,
	@LoaiNPL int,
	@GiaiTrinh nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_BKNPLTuCungUng_Detail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_BKNPLTuCungUng_Detail] 
		SET
			[BangKeHoSoThanhLy_ID] = @BangKeHoSoThanhLy_ID,
			[MaNPL] = @MaNPL,
			[TenNPL] = @TenNPL,
			[DVT_ID] = @DVT_ID,
			[DonGiaTT] = @DonGiaTT,
			[ThueSuat] = @ThueSuat,
			[LuongTuCungUng] = @LuongTuCungUng,
			[SoChungTu] = @SoChungTu,
			[NgayChungTu] = @NgayChungTu,
			[LoaiNPL] = @LoaiNPL,
			[GiaiTrinh] = @GiaiTrinh
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_BKNPLTuCungUng_Detail]
		(
			[BangKeHoSoThanhLy_ID],
			[MaNPL],
			[TenNPL],
			[DVT_ID],
			[DonGiaTT],
			[ThueSuat],
			[LuongTuCungUng],
			[SoChungTu],
			[NgayChungTu],
			[LoaiNPL],
			[GiaiTrinh]
		)
		VALUES 
		(
			@BangKeHoSoThanhLy_ID,
			@MaNPL,
			@TenNPL,
			@DVT_ID,
			@DonGiaTT,
			@ThueSuat,
			@LuongTuCungUng,
			@SoChungTu,
			@NgayChungTu,
			@LoaiNPL,
			@GiaiTrinh
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_BKNPLTuCungUng_Detail]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_BKNPLTuCungUng_Detail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BangKeHoSoThanhLy_ID],
	[MaNPL],
	[TenNPL],
	[DVT_ID],
	[DonGiaTT],
	[ThueSuat],
	[LuongTuCungUng],
	[SoChungTu],
	[NgayChungTu],
	[LoaiNPL],
	[GiaiTrinh]
FROM
	[dbo].[t_KDT_SXXK_BKNPLTuCungUng_Detail]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[BangKeHoSoThanhLy_ID],
	[MaNPL],
	[TenNPL],
	[DVT_ID],
	[DonGiaTT],
	[ThueSuat],
	[LuongTuCungUng],
	[SoChungTu],
	[NgayChungTu],
	[LoaiNPL],
	[GiaiTrinh]
FROM [dbo].[t_KDT_SXXK_BKNPLTuCungUng_Detail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, September 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTuCungUng_Detail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BangKeHoSoThanhLy_ID],
	[MaNPL],
	[TenNPL],
	[DVT_ID],
	[DonGiaTT],
	[ThueSuat],
	[LuongTuCungUng],
	[SoChungTu],
	[NgayChungTu],
	[LoaiNPL],
	[GiaiTrinh]
FROM
	[dbo].[t_KDT_SXXK_BKNPLTuCungUng_Detail]	

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Insert]    Script Date: 09/25/2014 09:13:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Insert]
-- Database: ECS_TQDT_SXXK_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 03, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Insert]
	@SoTiepNhan bigint,
	@MaHaiQuanTiepNhan char(6),
	@NamTiepNhan smallint,
	@NgayTiepNhan datetime,
	@MaDoanhNghiep varchar(14),
	@TrangThaiXuLy int,
	@TrangThaiThanhKhoan int,
	@SoHoSo int,
	@NgayBatDau datetime,
	@NgayKetThuc datetime,
	@LanThanhLy int,
	@SoQuyetDinh nvarchar(100),
	@NgayQuyetDinh datetime,
	@UserName varchar(50),
	@GuidStr nvarchar(36),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy]
(
	[SoTiepNhan],
	[MaHaiQuanTiepNhan],
	[NamTiepNhan],
	[NgayTiepNhan],
	[MaDoanhNghiep],
	[TrangThaiXuLy],
	[TrangThaiThanhKhoan],
	[SoHoSo],
	[NgayBatDau],
	[NgayKetThuc],
	[LanThanhLy],
	[SoQuyetDinh],
	[NgayQuyetDinh],
	[UserName],
	[GuidStr]
)
VALUES 
(
	@SoTiepNhan,
	@MaHaiQuanTiepNhan,
	@NamTiepNhan,
	@NgayTiepNhan,
	@MaDoanhNghiep,
	@TrangThaiXuLy,
	@TrangThaiThanhKhoan,
	@SoHoSo,
	@NgayBatDau,
	@NgayKetThuc,
	@LanThanhLy,
	@SoQuyetDinh,
	@NgayQuyetDinh,
	@UserName,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

Go

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_InsertUpdate]    Script Date: 09/25/2014 09:14:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 03, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@MaHaiQuanTiepNhan char(6),
	@NamTiepNhan smallint,
	@NgayTiepNhan datetime,
	@MaDoanhNghiep varchar(14),
	@TrangThaiXuLy int,
	@TrangThaiThanhKhoan int,
	@SoHoSo int,
	@NgayBatDau datetime,
	@NgayKetThuc datetime,
	@LanThanhLy int,
	@SoQuyetDinh nvarchar(100),
	@NgayQuyetDinh datetime,
	@UserName varchar(50),
	@GuidStr nvarchar(36)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_HoSoThanhLyDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[MaHaiQuanTiepNhan] = @MaHaiQuanTiepNhan,
			[NamTiepNhan] = @NamTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[TrangThaiThanhKhoan] = @TrangThaiThanhKhoan,
			[SoHoSo] = @SoHoSo,
			[NgayBatDau] = @NgayBatDau,
			[NgayKetThuc] = @NgayKetThuc,
			[LanThanhLy] = @LanThanhLy,
			[SoQuyetDinh] = @SoQuyetDinh,
			[NgayQuyetDinh] = @NgayQuyetDinh,
			[UserName] = @UserName,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy]
		(
			[SoTiepNhan],
			[MaHaiQuanTiepNhan],
			[NamTiepNhan],
			[NgayTiepNhan],
			[MaDoanhNghiep],
			[TrangThaiXuLy],
			[TrangThaiThanhKhoan],
			[SoHoSo],
			[NgayBatDau],
			[NgayKetThuc],
			[LanThanhLy],
			[SoQuyetDinh],
			[NgayQuyetDinh],
			[UserName],
			[GuidStr]
		)
		VALUES 
		(
			@SoTiepNhan,
			@MaHaiQuanTiepNhan,
			@NamTiepNhan,
			@NgayTiepNhan,
			@MaDoanhNghiep,
			@TrangThaiXuLy,
			@TrangThaiThanhKhoan,
			@SoHoSo,
			@NgayBatDau,
			@NgayKetThuc,
			@LanThanhLy,
			@SoQuyetDinh,
			@NgayQuyetDinh,
			@UserName,
			@GuidStr
		)		
	END


GO
/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Update]    Script Date: 09/25/2014 09:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Update]
-- Database: ECS_TQDT_SXXK_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 03, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@MaHaiQuanTiepNhan char(6),
	@NamTiepNhan smallint,
	@NgayTiepNhan datetime,
	@MaDoanhNghiep varchar(14),
	@TrangThaiXuLy int,
	@TrangThaiThanhKhoan int,
	@SoHoSo int,
	@NgayBatDau datetime,
	@NgayKetThuc datetime,
	@LanThanhLy int,
	@SoQuyetDinh nvarchar(100),
	@NgayQuyetDinh datetime,
	@UserName varchar(50),
	@GuidStr nvarchar(36)
AS

UPDATE
	[dbo].[t_KDT_SXXK_HoSoThanhLyDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[MaHaiQuanTiepNhan] = @MaHaiQuanTiepNhan,
	[NamTiepNhan] = @NamTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[TrangThaiThanhKhoan] = @TrangThaiThanhKhoan,
	[SoHoSo] = @SoHoSo,
	[NgayBatDau] = @NgayBatDau,
	[NgayKetThuc] = @NgayKetThuc,
	[LanThanhLy] = @LanThanhLy,
	[SoQuyetDinh] = @SoQuyetDinh,
	[NgayQuyetDinh] = @NgayQuyetDinh,
	[UserName] = @UserName,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID


GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '19.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('19.8',GETDATE(), N' Cập nhật store và NPL CUNG UNG')
END