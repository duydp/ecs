using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class HoSoThanhLyDangKy
	{
		#region Properties.
		
		public long ID { set; get; }
		public long SoTiepNhan { set; get; }
		public string MaHaiQuanTiepNhan { set; get; }
		public short NamTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public string MaDoanhNghiep { set; get; }
		public int TrangThaiXuLy { set; get; }
		public int TrangThaiThanhKhoan { set; get; }
		public int SoHoSo { set; get; }
		public DateTime NgayBatDau { set; get; }
		public DateTime NgayKetThuc { set; get; }
		public int LanThanhLy { set; get; }
		public string SoQuyetDinh { set; get; }
		public DateTime NgayQuyetDinh { set; get; }
		public string UserName { set; get; }
		public string GuidStr { set; get; }
        public string ListTKNK_Hoan { set; get; }
        public string ListTKNK_KhongThu { set; get; }

		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static HoSoThanhLyDangKy Load(long id)
		{
			const string spName = "[dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			HoSoThanhLyDangKy entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new HoSoThanhLyDangKy();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt16(reader.GetOrdinal("NamTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetInt32(reader.GetOrdinal("TrangThaiThanhKhoan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoSo"))) entity.SoHoSo = reader.GetInt32(reader.GetOrdinal("SoHoSo"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayBatDau"))) entity.NgayBatDau = reader.GetDateTime(reader.GetOrdinal("NgayBatDau"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKetThuc"))) entity.NgayKetThuc = reader.GetDateTime(reader.GetOrdinal("NgayKetThuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoQuyetDinh"))) entity.SoQuyetDinh = reader.GetString(reader.GetOrdinal("SoQuyetDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayQuyetDinh"))) entity.NgayQuyetDinh = reader.GetDateTime(reader.GetOrdinal("NgayQuyetDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserName"))) entity.UserName = reader.GetString(reader.GetOrdinal("UserName"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("ListTKNK_Hoan"))) entity.ListTKNK_Hoan = reader.GetString(reader.GetOrdinal("ListTKNK_Hoan"));
                    if (!reader.IsDBNull(reader.GetOrdinal("ListTKNK_KhongThu"))) entity.ListTKNK_KhongThu = reader.GetString(reader.GetOrdinal("ListTKNK_KhongThu"));
                }
                catch (Exception)
                {
                    //throw;
                }
                
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<HoSoThanhLyDangKy> SelectCollectionAll()
		{
			List<HoSoThanhLyDangKy> collection = new List<HoSoThanhLyDangKy>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				HoSoThanhLyDangKy entity = new HoSoThanhLyDangKy();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt16(reader.GetOrdinal("NamTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetInt32(reader.GetOrdinal("TrangThaiThanhKhoan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoSo"))) entity.SoHoSo = reader.GetInt32(reader.GetOrdinal("SoHoSo"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayBatDau"))) entity.NgayBatDau = reader.GetDateTime(reader.GetOrdinal("NgayBatDau"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKetThuc"))) entity.NgayKetThuc = reader.GetDateTime(reader.GetOrdinal("NgayKetThuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoQuyetDinh"))) entity.SoQuyetDinh = reader.GetString(reader.GetOrdinal("SoQuyetDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayQuyetDinh"))) entity.NgayQuyetDinh = reader.GetDateTime(reader.GetOrdinal("NgayQuyetDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserName"))) entity.UserName = reader.GetString(reader.GetOrdinal("UserName"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("ListTKNK_Hoan"))) entity.ListTKNK_Hoan = reader.GetString(reader.GetOrdinal("ListTKNK_Hoan"));
                    if (!reader.IsDBNull(reader.GetOrdinal("ListTKNK_KhongThu"))) entity.ListTKNK_KhongThu = reader.GetString(reader.GetOrdinal("ListTKNK_KhongThu"));
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //throw;
                }
                
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<HoSoThanhLyDangKy> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<HoSoThanhLyDangKy> collection = new List<HoSoThanhLyDangKy>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				HoSoThanhLyDangKy entity = new HoSoThanhLyDangKy();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt16(reader.GetOrdinal("NamTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetInt32(reader.GetOrdinal("TrangThaiThanhKhoan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoSo"))) entity.SoHoSo = reader.GetInt32(reader.GetOrdinal("SoHoSo"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayBatDau"))) entity.NgayBatDau = reader.GetDateTime(reader.GetOrdinal("NgayBatDau"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKetThuc"))) entity.NgayKetThuc = reader.GetDateTime(reader.GetOrdinal("NgayKetThuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoQuyetDinh"))) entity.SoQuyetDinh = reader.GetString(reader.GetOrdinal("SoQuyetDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayQuyetDinh"))) entity.NgayQuyetDinh = reader.GetDateTime(reader.GetOrdinal("NgayQuyetDinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserName"))) entity.UserName = reader.GetString(reader.GetOrdinal("UserName"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("ListTKNK_Hoan"))) entity.ListTKNK_Hoan = reader.GetString(reader.GetOrdinal("ListTKNK_Hoan"));
                    if (!reader.IsDBNull(reader.GetOrdinal("ListTKNK_KhongThu"))) entity.ListTKNK_KhongThu = reader.GetString(reader.GetOrdinal("ListTKNK_KhongThu"));
                }
                catch (Exception)
                {

                    //throw;
                }
                
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.

        public static long InsertHoSoThanhLyDangKy(long soTiepNhan, string maHaiQuanTiepNhan, short namTiepNhan, DateTime ngayTiepNhan, string maDoanhNghiep, int trangThaiXuLy, int trangThaiThanhKhoan, int soHoSo, DateTime ngayBatDau, DateTime ngayKetThuc, string soQuyetDinh, DateTime ngayQuyetDinh, string userName, string guidStr, string listTKNK_Hoan, string listTKNK_KhongThu)
		{
			HoSoThanhLyDangKy entity = new HoSoThanhLyDangKy();	
			entity.SoTiepNhan = soTiepNhan;
			entity.MaHaiQuanTiepNhan = maHaiQuanTiepNhan;
			entity.NamTiepNhan = namTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.TrangThaiThanhKhoan = trangThaiThanhKhoan;
			entity.SoHoSo = soHoSo;
			entity.NgayBatDau = ngayBatDau;
			entity.NgayKetThuc = ngayKetThuc;
			entity.SoQuyetDinh = soQuyetDinh;
			entity.NgayQuyetDinh = ngayQuyetDinh;
			entity.UserName = userName;
			entity.GuidStr = guidStr;
            entity.ListTKNK_Hoan = listTKNK_Hoan;
            entity.ListTKNK_KhongThu = listTKNK_KhongThu;

			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
			db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.SmallInt, NamTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, TrangThaiThanhKhoan);
			db.AddInParameter(dbCommand, "@SoHoSo", SqlDbType.Int, SoHoSo);
			db.AddInParameter(dbCommand, "@NgayBatDau", SqlDbType.DateTime, NgayBatDau.Year <= 1753 ? DBNull.Value : (object) NgayBatDau);
			db.AddInParameter(dbCommand, "@NgayKetThuc", SqlDbType.DateTime, NgayKetThuc.Year <= 1753 ? DBNull.Value : (object) NgayKetThuc);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);
			db.AddInParameter(dbCommand, "@SoQuyetDinh", SqlDbType.NVarChar, SoQuyetDinh);
			db.AddInParameter(dbCommand, "@NgayQuyetDinh", SqlDbType.DateTime, NgayQuyetDinh.Year <= 1753 ? DBNull.Value : (object) NgayQuyetDinh);
			db.AddInParameter(dbCommand, "@UserName", SqlDbType.VarChar, UserName);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.NVarChar, GuidStr);
            db.AddInParameter(dbCommand, "@ListTKNK_Hoan", SqlDbType.NVarChar, ListTKNK_Hoan);
            db.AddInParameter(dbCommand, "@ListTKNK_KhongThu", SqlDbType.NVarChar, ListTKNK_KhongThu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<HoSoThanhLyDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HoSoThanhLyDangKy item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.

        public static int InsertUpdateHoSoThanhLyDangKy(long id, long soTiepNhan, string maHaiQuanTiepNhan, short namTiepNhan, DateTime ngayTiepNhan, string maDoanhNghiep, int trangThaiXuLy, int trangThaiThanhKhoan, int soHoSo, DateTime ngayBatDau, DateTime ngayKetThuc, int lanThanhLy, string soQuyetDinh, DateTime ngayQuyetDinh, string userName, string guidStr, string listTKNK_Hoan, string listTKNK_KhongThu)
		{
			HoSoThanhLyDangKy entity = new HoSoThanhLyDangKy();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.MaHaiQuanTiepNhan = maHaiQuanTiepNhan;
			entity.NamTiepNhan = namTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.TrangThaiThanhKhoan = trangThaiThanhKhoan;
			entity.SoHoSo = soHoSo;
			entity.NgayBatDau = ngayBatDau;
			entity.NgayKetThuc = ngayKetThuc;
			entity.LanThanhLy = lanThanhLy;
			entity.SoQuyetDinh = soQuyetDinh;
			entity.NgayQuyetDinh = ngayQuyetDinh;
			entity.UserName = userName;
			entity.GuidStr = guidStr;
            entity.ListTKNK_Hoan = listTKNK_Hoan;
            entity.ListTKNK_KhongThu = listTKNK_KhongThu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_SXXK_HoSoThanhLyDangKy_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
			db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.SmallInt, NamTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, TrangThaiThanhKhoan);
			db.AddInParameter(dbCommand, "@SoHoSo", SqlDbType.Int, SoHoSo);
			db.AddInParameter(dbCommand, "@NgayBatDau", SqlDbType.DateTime, NgayBatDau.Year == 1753 ? DBNull.Value : (object) NgayBatDau);
			db.AddInParameter(dbCommand, "@NgayKetThuc", SqlDbType.DateTime, NgayKetThuc.Year == 1753 ? DBNull.Value : (object) NgayKetThuc);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);
			db.AddInParameter(dbCommand, "@SoQuyetDinh", SqlDbType.NVarChar, SoQuyetDinh);
			db.AddInParameter(dbCommand, "@NgayQuyetDinh", SqlDbType.DateTime, NgayQuyetDinh.Year == 1753 ? DBNull.Value : (object) NgayQuyetDinh);
			db.AddInParameter(dbCommand, "@UserName", SqlDbType.VarChar, UserName);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.NVarChar, GuidStr);
            db.AddInParameter(dbCommand, "@ListTKNK_Hoan", SqlDbType.NVarChar, ListTKNK_Hoan);
            db.AddInParameter(dbCommand, "@ListTKNK_KhongThu", SqlDbType.NVarChar, ListTKNK_KhongThu);
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<HoSoThanhLyDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HoSoThanhLyDangKy item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.

        public static int UpdateHoSoThanhLyDangKy(long id, long soTiepNhan, string maHaiQuanTiepNhan, short namTiepNhan, DateTime ngayTiepNhan, string maDoanhNghiep, int trangThaiXuLy, int trangThaiThanhKhoan, int soHoSo, DateTime ngayBatDau, DateTime ngayKetThuc, int lanThanhLy, string soQuyetDinh, DateTime ngayQuyetDinh, string userName, string guidStr, string listTKNK_Hoan, string listTKNK_KhongThu)
		{
			HoSoThanhLyDangKy entity = new HoSoThanhLyDangKy();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.MaHaiQuanTiepNhan = maHaiQuanTiepNhan;
			entity.NamTiepNhan = namTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.TrangThaiThanhKhoan = trangThaiThanhKhoan;
			entity.SoHoSo = soHoSo;
			entity.NgayBatDau = ngayBatDau;
			entity.NgayKetThuc = ngayKetThuc;
			entity.LanThanhLy = lanThanhLy;
			entity.SoQuyetDinh = soQuyetDinh;
			entity.NgayQuyetDinh = ngayQuyetDinh;
			entity.UserName = userName;
			entity.GuidStr = guidStr;
            entity.ListTKNK_Hoan = listTKNK_Hoan;
            entity.ListTKNK_KhongThu = listTKNK_KhongThu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
			db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.SmallInt, NamTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, TrangThaiThanhKhoan);
			db.AddInParameter(dbCommand, "@SoHoSo", SqlDbType.Int, SoHoSo);
			db.AddInParameter(dbCommand, "@NgayBatDau", SqlDbType.DateTime, NgayBatDau.Year == 1753 ? DBNull.Value : (object) NgayBatDau);
			db.AddInParameter(dbCommand, "@NgayKetThuc", SqlDbType.DateTime, NgayKetThuc.Year == 1753 ? DBNull.Value : (object) NgayKetThuc);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);
			db.AddInParameter(dbCommand, "@SoQuyetDinh", SqlDbType.NVarChar, SoQuyetDinh);
			db.AddInParameter(dbCommand, "@NgayQuyetDinh", SqlDbType.DateTime, NgayQuyetDinh.Year == 1753 ? DBNull.Value : (object) NgayQuyetDinh);
			db.AddInParameter(dbCommand, "@UserName", SqlDbType.VarChar, UserName);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.NVarChar, GuidStr);
            db.AddInParameter(dbCommand, "@ListTKNK_Hoan", SqlDbType.NVarChar, ListTKNK_Hoan);
            db.AddInParameter(dbCommand, "@ListTKNK_KhongThu", SqlDbType.NVarChar, ListTKNK_KhongThu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<HoSoThanhLyDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HoSoThanhLyDangKy item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteHoSoThanhLyDangKy(long id)
		{
			HoSoThanhLyDangKy entity = new HoSoThanhLyDangKy();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_SXXK_HoSoThanhLyDangKy_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<HoSoThanhLyDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HoSoThanhLyDangKy item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}