﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.GC.BLL.KDT.GC
{
    public partial class HangMau
    {
        public static int DeleteBy_HopDong_ID(SqlTransaction transaction, long id)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string spName = "p_KDT_GC_HangMau_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, id);
            return db.ExecuteNonQuery(dbCommand, transaction);
        }
    }
}
