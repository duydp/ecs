GO
IF NOT EXISTS (SELECT * FROM sys.columns WHERE name='LoaiDinhMuc')
	ALTER TABLE dbo.t_KDT_GC_DinhMucDangKy
	ADD LoaiDinhMuc INT DEFAULT (0) NULL

GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucDangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucDangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucDangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucDangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucDangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucDangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucDangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucDangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDong]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDong]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucDangKy_DeleteBy_ID_HopDong]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_DeleteBy_ID_HopDong]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucDangKy_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_Insert]
	@SoTiepNhan bigint,
	@TrangThaiXuLy int,
	@NgayTiepNhan datetime,
	@ID_HopDong bigint,
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus int,
	@GuidReference nvarchar(500),
	@NamTN nvarchar(500),
	@HUONGDAN nvarchar(500),
	@PhanLuong varchar(50),
	@Huongdan_PL nvarchar(500),
	@LoaiDinhMuc int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_DinhMucDangKy]
(
	[SoTiepNhan],
	[TrangThaiXuLy],
	[NgayTiepNhan],
	[ID_HopDong],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamTN],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL],
	[LoaiDinhMuc]
)
VALUES 
(
	@SoTiepNhan,
	@TrangThaiXuLy,
	@NgayTiepNhan,
	@ID_HopDong,
	@MaHaiQuan,
	@MaDoanhNghiep,
	@GUIDSTR,
	@DeXuatKhac,
	@LyDoSua,
	@ActionStatus,
	@GuidReference,
	@NamTN,
	@HUONGDAN,
	@PhanLuong,
	@Huongdan_PL,
	@LoaiDinhMuc
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucDangKy_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@TrangThaiXuLy int,
	@NgayTiepNhan datetime,
	@ID_HopDong bigint,
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus int,
	@GuidReference nvarchar(500),
	@NamTN nvarchar(500),
	@HUONGDAN nvarchar(500),
	@PhanLuong varchar(50),
	@Huongdan_PL nvarchar(500),
	@LoaiDinhMuc int
AS

UPDATE
	[dbo].[t_KDT_GC_DinhMucDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[NgayTiepNhan] = @NgayTiepNhan,
	[ID_HopDong] = @ID_HopDong,
	[MaHaiQuan] = @MaHaiQuan,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[GUIDSTR] = @GUIDSTR,
	[DeXuatKhac] = @DeXuatKhac,
	[LyDoSua] = @LyDoSua,
	[ActionStatus] = @ActionStatus,
	[GuidReference] = @GuidReference,
	[NamTN] = @NamTN,
	[HUONGDAN] = @HUONGDAN,
	[PhanLuong] = @PhanLuong,
	[Huongdan_PL] = @Huongdan_PL,
	[LoaiDinhMuc] = @LoaiDinhMuc
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucDangKy_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@TrangThaiXuLy int,
	@NgayTiepNhan datetime,
	@ID_HopDong bigint,
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus int,
	@GuidReference nvarchar(500),
	@NamTN nvarchar(500),
	@HUONGDAN nvarchar(500),
	@PhanLuong varchar(50),
	@Huongdan_PL nvarchar(500),
	@LoaiDinhMuc int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_DinhMucDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_DinhMucDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[NgayTiepNhan] = @NgayTiepNhan,
			[ID_HopDong] = @ID_HopDong,
			[MaHaiQuan] = @MaHaiQuan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[GUIDSTR] = @GUIDSTR,
			[DeXuatKhac] = @DeXuatKhac,
			[LyDoSua] = @LyDoSua,
			[ActionStatus] = @ActionStatus,
			[GuidReference] = @GuidReference,
			[NamTN] = @NamTN,
			[HUONGDAN] = @HUONGDAN,
			[PhanLuong] = @PhanLuong,
			[Huongdan_PL] = @Huongdan_PL,
			[LoaiDinhMuc] = @LoaiDinhMuc
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_DinhMucDangKy]
		(
			[SoTiepNhan],
			[TrangThaiXuLy],
			[NgayTiepNhan],
			[ID_HopDong],
			[MaHaiQuan],
			[MaDoanhNghiep],
			[GUIDSTR],
			[DeXuatKhac],
			[LyDoSua],
			[ActionStatus],
			[GuidReference],
			[NamTN],
			[HUONGDAN],
			[PhanLuong],
			[Huongdan_PL],
			[LoaiDinhMuc]
		)
		VALUES 
		(
			@SoTiepNhan,
			@TrangThaiXuLy,
			@NgayTiepNhan,
			@ID_HopDong,
			@MaHaiQuan,
			@MaDoanhNghiep,
			@GUIDSTR,
			@DeXuatKhac,
			@LyDoSua,
			@ActionStatus,
			@GuidReference,
			@NamTN,
			@HUONGDAN,
			@PhanLuong,
			@Huongdan_PL,
			@LoaiDinhMuc
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucDangKy_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_DinhMucDangKy]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucDangKy_DeleteBy_ID_HopDong]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_DeleteBy_ID_HopDong]
	@ID_HopDong bigint
AS

DELETE FROM [dbo].[t_KDT_GC_DinhMucDangKy]
WHERE
	[ID_HopDong] = @ID_HopDong

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucDangKy_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_DinhMucDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucDangKy_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[TrangThaiXuLy],
	[NgayTiepNhan],
	[ID_HopDong],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamTN],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL],
	[LoaiDinhMuc]
FROM
	[dbo].[t_KDT_GC_DinhMucDangKy]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDong]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_SelectBy_ID_HopDong]
	@ID_HopDong bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[TrangThaiXuLy],
	[NgayTiepNhan],
	[ID_HopDong],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamTN],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL],
	[LoaiDinhMuc]
FROM
	[dbo].[t_KDT_GC_DinhMucDangKy]
WHERE
	[ID_HopDong] = @ID_HopDong

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucDangKy_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[TrangThaiXuLy],
	[NgayTiepNhan],
	[ID_HopDong],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamTN],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL],
	[LoaiDinhMuc]
FROM [dbo].[t_KDT_GC_DinhMucDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucDangKy_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucDangKy_SelectAll]

















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[TrangThaiXuLy],
	[NgayTiepNhan],
	[ID_HopDong],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamTN],
	[HUONGDAN],
	[PhanLuong],
	[Huongdan_PL],
	[LoaiDinhMuc]
FROM
	[dbo].[t_KDT_GC_DinhMucDangKy]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '26.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('26.1',GETDATE(), N'CẬP NHẬT SỬA KHAI BÁO ĐỊNH MỨC THEO TT MỚI')
END