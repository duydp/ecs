using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class HoaDonThuongMai
	{
		#region Properties.
		
		public long ID { set; get; }
		public string SoHoaDon { set; get; }
		public DateTime NgayHoaDon { set; get; }
		public string NguyenTe_ID { set; get; }
		public string PTTT_ID { set; get; }
		public string DKGH_ID { set; get; }
		public string MaDonViMua { set; get; }
		public string TenDonViMua { set; get; }
		public string MaDonViBan { set; get; }
		public string TenDonViBan { set; get; }
		public string ThongTinKhac { set; get; }
		public long TKMD_ID { set; get; }
		public string GuidStr { set; get; }
		public int LoaiKB { set; get; }
		public long SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public int TrangThai { set; get; }
		public int NamTiepNhan { set; get; }
		public string MaDoanhNghiep { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static HoaDonThuongMai Load(long id)
		{
			const string spName = "[dbo].[p_KDT_HoaDonThuongMai_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			HoaDonThuongMai entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new HoaDonThuongMai();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDon"))) entity.SoHoaDon = reader.GetString(reader.GetOrdinal("SoHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDon"))) entity.NgayHoaDon = reader.GetDateTime(reader.GetOrdinal("NgayHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViMua"))) entity.MaDonViMua = reader.GetString(reader.GetOrdinal("MaDonViMua"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViMua"))) entity.TenDonViMua = reader.GetString(reader.GetOrdinal("TenDonViMua"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViBan"))) entity.MaDonViBan = reader.GetString(reader.GetOrdinal("MaDonViBan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViBan"))) entity.TenDonViBan = reader.GetString(reader.GetOrdinal("TenDonViBan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<HoaDonThuongMai> SelectCollectionAll()
		{
			List<HoaDonThuongMai> collection = new List<HoaDonThuongMai>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				HoaDonThuongMai entity = new HoaDonThuongMai();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDon"))) entity.SoHoaDon = reader.GetString(reader.GetOrdinal("SoHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDon"))) entity.NgayHoaDon = reader.GetDateTime(reader.GetOrdinal("NgayHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViMua"))) entity.MaDonViMua = reader.GetString(reader.GetOrdinal("MaDonViMua"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViMua"))) entity.TenDonViMua = reader.GetString(reader.GetOrdinal("TenDonViMua"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViBan"))) entity.MaDonViBan = reader.GetString(reader.GetOrdinal("MaDonViBan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViBan"))) entity.TenDonViBan = reader.GetString(reader.GetOrdinal("TenDonViBan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<HoaDonThuongMai> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<HoaDonThuongMai> collection = new List<HoaDonThuongMai>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				HoaDonThuongMai entity = new HoaDonThuongMai();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDon"))) entity.SoHoaDon = reader.GetString(reader.GetOrdinal("SoHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDon"))) entity.NgayHoaDon = reader.GetDateTime(reader.GetOrdinal("NgayHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViMua"))) entity.MaDonViMua = reader.GetString(reader.GetOrdinal("MaDonViMua"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViMua"))) entity.TenDonViMua = reader.GetString(reader.GetOrdinal("TenDonViMua"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViBan"))) entity.MaDonViBan = reader.GetString(reader.GetOrdinal("MaDonViBan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViBan"))) entity.TenDonViBan = reader.GetString(reader.GetOrdinal("TenDonViBan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<HoaDonThuongMai> SelectCollectionBy_TKMD_ID(long tKMD_ID)
		{
			List<HoaDonThuongMai> collection = new List<HoaDonThuongMai>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_TKMD_ID(tKMD_ID);
			while (reader.Read())
			{
				HoaDonThuongMai entity = new HoaDonThuongMai();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDon"))) entity.SoHoaDon = reader.GetString(reader.GetOrdinal("SoHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDon"))) entity.NgayHoaDon = reader.GetDateTime(reader.GetOrdinal("NgayHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViMua"))) entity.MaDonViMua = reader.GetString(reader.GetOrdinal("MaDonViMua"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViMua"))) entity.TenDonViMua = reader.GetString(reader.GetOrdinal("TenDonViMua"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViBan"))) entity.MaDonViBan = reader.GetString(reader.GetOrdinal("MaDonViBan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViBan"))) entity.TenDonViBan = reader.GetString(reader.GetOrdinal("TenDonViBan"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_HoaDonThuongMai_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMai_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_HoaDonThuongMai_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMai_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_HoaDonThuongMai_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "p_KDT_HoaDonThuongMai_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertHoaDonThuongMai(string soHoaDon, DateTime ngayHoaDon, string nguyenTe_ID, string pTTT_ID, string dKGH_ID, string maDonViMua, string tenDonViMua, string maDonViBan, string tenDonViBan, string thongTinKhac, long tKMD_ID, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan, string maDoanhNghiep)
		{
			HoaDonThuongMai entity = new HoaDonThuongMai();	
			entity.SoHoaDon = soHoaDon;
			entity.NgayHoaDon = ngayHoaDon;
			entity.NguyenTe_ID = nguyenTe_ID;
			entity.PTTT_ID = pTTT_ID;
			entity.DKGH_ID = dKGH_ID;
			entity.MaDonViMua = maDonViMua;
			entity.TenDonViMua = tenDonViMua;
			entity.MaDonViBan = maDonViBan;
			entity.TenDonViBan = tenDonViBan;
			entity.ThongTinKhac = thongTinKhac;
			entity.TKMD_ID = tKMD_ID;
			entity.GuidStr = guidStr;
			entity.LoaiKB = loaiKB;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThai = trangThai;
			entity.NamTiepNhan = namTiepNhan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_HoaDonThuongMai_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.VarChar, SoHoaDon);
			db.AddInParameter(dbCommand, "@NgayHoaDon", SqlDbType.DateTime, NgayHoaDon.Year <= 1753 ? DBNull.Value : (object) NgayHoaDon);
			db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
			db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.Char, PTTT_ID);
			db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.Char, DKGH_ID);
			db.AddInParameter(dbCommand, "@MaDonViMua", SqlDbType.VarChar, MaDonViMua);
			db.AddInParameter(dbCommand, "@TenDonViMua", SqlDbType.NVarChar, TenDonViMua);
			db.AddInParameter(dbCommand, "@MaDonViBan", SqlDbType.VarChar, MaDonViBan);
			db.AddInParameter(dbCommand, "@TenDonViBan", SqlDbType.NVarChar, TenDonViBan);
			db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<HoaDonThuongMai> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HoaDonThuongMai item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateHoaDonThuongMai(long id, string soHoaDon, DateTime ngayHoaDon, string nguyenTe_ID, string pTTT_ID, string dKGH_ID, string maDonViMua, string tenDonViMua, string maDonViBan, string tenDonViBan, string thongTinKhac, long tKMD_ID, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan, string maDoanhNghiep)
		{
			HoaDonThuongMai entity = new HoaDonThuongMai();			
			entity.ID = id;
			entity.SoHoaDon = soHoaDon;
			entity.NgayHoaDon = ngayHoaDon;
			entity.NguyenTe_ID = nguyenTe_ID;
			entity.PTTT_ID = pTTT_ID;
			entity.DKGH_ID = dKGH_ID;
			entity.MaDonViMua = maDonViMua;
			entity.TenDonViMua = tenDonViMua;
			entity.MaDonViBan = maDonViBan;
			entity.TenDonViBan = tenDonViBan;
			entity.ThongTinKhac = thongTinKhac;
			entity.TKMD_ID = tKMD_ID;
			entity.GuidStr = guidStr;
			entity.LoaiKB = loaiKB;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThai = trangThai;
			entity.NamTiepNhan = namTiepNhan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_HoaDonThuongMai_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.VarChar, SoHoaDon);
			db.AddInParameter(dbCommand, "@NgayHoaDon", SqlDbType.DateTime, NgayHoaDon.Year == 1753 ? DBNull.Value : (object) NgayHoaDon);
			db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
			db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.Char, PTTT_ID);
			db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.Char, DKGH_ID);
			db.AddInParameter(dbCommand, "@MaDonViMua", SqlDbType.VarChar, MaDonViMua);
			db.AddInParameter(dbCommand, "@TenDonViMua", SqlDbType.NVarChar, TenDonViMua);
			db.AddInParameter(dbCommand, "@MaDonViBan", SqlDbType.VarChar, MaDonViBan);
			db.AddInParameter(dbCommand, "@TenDonViBan", SqlDbType.NVarChar, TenDonViBan);
			db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<HoaDonThuongMai> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HoaDonThuongMai item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateHoaDonThuongMai(long id, string soHoaDon, DateTime ngayHoaDon, string nguyenTe_ID, string pTTT_ID, string dKGH_ID, string maDonViMua, string tenDonViMua, string maDonViBan, string tenDonViBan, string thongTinKhac, long tKMD_ID, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan, string maDoanhNghiep)
		{
			HoaDonThuongMai entity = new HoaDonThuongMai();			
			entity.ID = id;
			entity.SoHoaDon = soHoaDon;
			entity.NgayHoaDon = ngayHoaDon;
			entity.NguyenTe_ID = nguyenTe_ID;
			entity.PTTT_ID = pTTT_ID;
			entity.DKGH_ID = dKGH_ID;
			entity.MaDonViMua = maDonViMua;
			entity.TenDonViMua = tenDonViMua;
			entity.MaDonViBan = maDonViBan;
			entity.TenDonViBan = tenDonViBan;
			entity.ThongTinKhac = thongTinKhac;
			entity.TKMD_ID = tKMD_ID;
			entity.GuidStr = guidStr;
			entity.LoaiKB = loaiKB;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThai = trangThai;
			entity.NamTiepNhan = namTiepNhan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_HoaDonThuongMai_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.VarChar, SoHoaDon);
			db.AddInParameter(dbCommand, "@NgayHoaDon", SqlDbType.DateTime, NgayHoaDon.Year == 1753 ? DBNull.Value : (object) NgayHoaDon);
			db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
			db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.Char, PTTT_ID);
			db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.Char, DKGH_ID);
			db.AddInParameter(dbCommand, "@MaDonViMua", SqlDbType.VarChar, MaDonViMua);
			db.AddInParameter(dbCommand, "@TenDonViMua", SqlDbType.NVarChar, TenDonViMua);
			db.AddInParameter(dbCommand, "@MaDonViBan", SqlDbType.VarChar, MaDonViBan);
			db.AddInParameter(dbCommand, "@TenDonViBan", SqlDbType.NVarChar, TenDonViBan);
			db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<HoaDonThuongMai> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HoaDonThuongMai item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteHoaDonThuongMai(long id)
		{
			HoaDonThuongMai entity = new HoaDonThuongMai();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_HoaDonThuongMai_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_HoaDonThuongMai_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_HoaDonThuongMai_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<HoaDonThuongMai> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HoaDonThuongMai item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}