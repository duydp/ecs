
 IF  EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'p_KDT_SXXK_NPLNhapTon_SelectTotalNPLTonByYear') AND TYPE IN (N'U',N'PV',N'P',N'PC'))
 DROP PROCEDURE p_KDT_SXXK_NPLNhapTon_SelectTotalNPLTonByYear
 GO
 CREATE PROCEDURE p_KDT_SXXK_NPLNhapTon_SelectTotalNPLTonByYear
 @NAMQUYETTOAN INT
 AS
 BEGIN
     SELECT
 T.MaNPL AS MANPL,
 T.TenNPL AS TENNPL,
 T.TenDVT AS DVT,
 SUM(T.Ton) AS LUONGTONDK,
 SUM(T.ThueXNKTon) AS TRIGIATONDK
 FROM
 (
 SELECT  SoToKhaiVNACCS ,
         MaDoanhNghiep ,
         MaHaiQuan ,
         SoToKhai ,
         MaLoaiHinh ,
         NamDangKy ,
         MaNPL ,
         TenNPL ,
         DVT_ID ,
         TenDVT ,
         NuocXX_ID ,
         TriGiaKB ,
         Luong ,
         Ton ,
         ThueXNK ,
         ThueXNKTon ,
         ThueTTDB ,
         ThueVAT ,
         ThueCLGia ,
         PhuThu ,
         NgayDangKy ,
         SoLanThanhLy ,
         LanThanhLy ,
         TrangThaiThanhKhoan ,
         SaiSoLuong ,
         SoLuongDangKy ,
         TienTrinhChayThanhLy ,
         ThanhLy ,
         LechTon ,
         TenChuHang ,
         SoHopDong  
FROM    dbo.v_HangTon WHERE NamDangKy <= @NAMQUYETTOAN
) T GROUP BY T.MaNPL,T.TenNPL,T.TenDVT ORDER BY T.MaNPL      
 END
   

GO

 IF  EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'p_KDT_SXXK_NPLNhapTon_SelectTotalNPLNhapByYear') AND TYPE IN (N'U',N'P',N'PV',N'PC'))
 DROP PROCEDURE p_KDT_SXXK_NPLNhapTon_SelectTotalNPLNhapByYear
 GO
 CREATE PROC p_KDT_SXXK_NPLNhapTon_SelectTotalNPLNhapByYear
 @NAMQUYETTOAN INT
 AS
  BEGIN
   SELECT 
TB.MaNPL AS MANPL,
TB.TENNPL,
TB.DVT,
SUM(TB.Luong) AS LUONGNHAPTK,
SUM(TB.ThueXNK + TB.ThueTTDB + TB.ThueVAT + TB.PhuThu + TB.TriGiaTT) AS TRIGIANHAPTK
FROM
(
SELECT SoToKhai ,
       MaLoaiHinh ,
       NamDangKy ,
       MaHaiQuan ,
       MaNPL ,
	   CASE WHEN T.MaNPL IS NOT NULL THEN (SELECT TOP 1 TenHang FROM dbo.t_SXXK_HangMauDich WHERE SoToKhai=T.SoToKhai AND MaLoaiHinh=T.MaLoaiHinh AND MaHaiQuan= T.MaHaiQuan AND MaPhu = T.MaNPL AND SoLuong =T.Luong) ELSE '' END AS TENNPL,
	   CASE WHEN T.MaNPL IS NOT NULL THEN (SELECT TOP 1 DVT_ID FROM dbo.t_SXXK_HangMauDich WHERE SoToKhai=T.SoToKhai AND MaLoaiHinh=T.MaLoaiHinh AND MaHaiQuan= T.MaHaiQuan AND MaPhu = T.MaNPL AND SoLuong =T.Luong) ELSE '' END AS DVT,
       LanThanhLy ,
       Luong ,
	   CASE WHEN Luong > 0 THEN (SELECT TOP 1 TriGiaTT FROM dbo.t_SXXK_HangMauDich WHERE SoToKhai=T.SoToKhai AND MaLoaiHinh=T.MaLoaiHinh AND MaHaiQuan= T.MaHaiQuan AND MaPhu = T.MaNPL AND SoLuong =T.Luong) ELSE 0 END AS TriGiaTT,
       TonDau ,
       TonCuoi ,
       ThueXNK ,
       ThueTTDB ,
       ThueVAT ,
       PhuThu ,
       ThueCLGia ,
       TonDauThueXNK ,
       TonCuoiThueXNK ,
       MaDoanhNghiep FROM dbo.t_KDT_SXXK_NPLNhapTon T WHERE NamDangKy =@NAMQUYETTOAN
	   ) TB GROUP BY TB.MaNPL,TB.TENNPL,TB.DVT ORDER BY MANPL   
  END

  GO

 IF  EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'p_KDT_SXXK_BCXuatNhapTon_SelectNPLXuatDetailByYear') AND TYPE IN (N'U',N'P',N'PV',N'PC'))
 DROP PROCEDURE p_KDT_SXXK_BCXuatNhapTon_SelectNPLXuatDetailByYear
 GO
 CREATE PROC p_KDT_SXXK_BCXuatNhapTon_SelectNPLXuatDetailByYear
 @NAMQUYETTOAN INT
 AS
 BEGIN
     SELECT
TB.TOKHAIXUAT,
TB.TOKHAINHAP,
TB.MaNPL AS MANPL,
TB.TenNPL AS TENNPL,
TB.TenDVT_NPL AS DVT_NPL,
TB.MaSP AS MASP,
TB.TenSP AS TENSP,
TB.TenDVT_SP AS DVT_SP,
TB.LuongNhap AS LUONGNHAP,
TB.LuongTonDau AS LUONGTONDAU,
(TB.TriGiaTT + TB.ThueXNK + TB.ThueTTDB + TB.ThueGTGT + TB.PhuThu ) / TB.LuongNhap AS DONGIANHAP,
(TB.TriGiaTT + TB.ThueXNK + TB.ThueTTDB + TB.ThueGTGT + TB.PhuThu ) AS TRIGIANHAP,
TB.LuongNPLSuDung AS LUONGXUAT ,
TB.LuongNPLSuDung * (TB.TriGiaTT + TB.ThueXNK + TB.ThueTTDB + TB.ThueGTGT + TB.PhuThu ) / TB.LuongNhap AS TRIGIAXUAT,
TB.LuongTonCuoi AS LUONGTONCUOI,
TB.ThueXNKTon AS TRIGIATON,
TB.TyGiaTT AS TYGIATINHTHUE
FROM
(
SELECT ID ,
       STT ,
       LanThanhLy ,
       NamThanhLy ,
       MaDoanhNghiep ,
       MaNPL ,
       TenNPL ,
       SoToKhaiNhap ,
	   CASE WHEN SoToKhaiNhap = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTK = SoToKhaiNhap AND MaLoaiHinh = SUBSTRING(MaLoaiHinhNhap,3,3) AND NamDangKy = YEAR(NgayDangKyNhap))  THEN (SELECT TOP 1 SoTKVNACCS FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTK = SoToKhaiNhap AND MaLoaiHinh = SUBSTRING(MaLoaiHinhNhap,3,3) AND NamDangKy = YEAR(NgayDangKyNhap)) ELSE SoToKhaiNhap END AS TOKHAINHAP,
       NgayDangKyNhap ,
       NgayHoanThanhNhap ,
       MaLoaiHinhNhap ,
       LuongNhap ,
       LuongTonDau ,
       TenDVT_NPL ,
       MaSP ,
       TenSP ,
       SoToKhaiXuat ,
	   CASE WHEN SoToKhaiXuat  = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTK = SoToKhaiXuat AND MaLoaiHinh = SUBSTRING(MaLoaiHinhXuat,3,3) AND NamDangKy = YEAR(NgayDangKyXuat)) THEN (SELECT TOP 1 SoTKVNACCS FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTK = SoToKhaiXuat AND MaLoaiHinh = SUBSTRING(MaLoaiHinhXuat,3,3) AND NamDangKy = YEAR(NgayDangKyXuat)) ELSE SoToKhaiXuat END AS TOKHAIXUAT,
       NgayDangKyXuat ,
       NgayHoanThanhXuat ,
       MaLoaiHinhXuat ,
       LuongSPXuat ,
       TenDVT_SP ,
       DinhMuc ,
       LuongNPLSuDung ,
       SoToKhaiTaiXuat ,
       NgayTaiXuat ,
       LuongNPLTaiXuat ,
       LuongTonCuoi ,
       ThanhKhoanTiep ,
       ChuyenMucDichKhac ,
       DonGiaTT ,
	   CASE WHEN LuongNhap > 0 THEN (SELECT TOP 1 TriGiaTT FROM dbo.t_SXXK_HangMauDich WHERE SoToKhai=SoToKhaiNhap AND MaLoaiHinh=MaLoaiHinhNhap AND NamDangKy=YEAR(NgayDangKyNhap) AND MaNPL = MaPhu AND TenNPL= TenHang AND SoLuong=LuongNhap) ELSE 0 END AS TriGiaTT,
	   CASE WHEN LuongNhap > 0 THEN (SELECT TOP 1 ThueTTDB FROM dbo.t_SXXK_HangMauDich WHERE SoToKhai=SoToKhaiNhap AND MaLoaiHinh=MaLoaiHinhNhap AND NamDangKy=YEAR(NgayDangKyNhap) AND MaNPL = MaPhu AND TenNPL= TenHang AND SoLuong=LuongNhap) ELSE 0 END AS ThueTTDB,
	   CASE WHEN LuongNhap > 0 THEN (SELECT TOP 1 ThueGTGT FROM dbo.t_SXXK_HangMauDich WHERE SoToKhai=SoToKhaiNhap AND MaLoaiHinh=MaLoaiHinhNhap AND NamDangKy=YEAR(NgayDangKyNhap) AND MaNPL = MaPhu AND TenNPL= TenHang AND SoLuong=LuongNhap) ELSE 0 END AS ThueGTGT,
	   CASE WHEN LuongNhap > 0 THEN (SELECT TOP 1 PhuThu FROM dbo.t_SXXK_HangMauDich WHERE SoToKhai=SoToKhaiNhap AND MaLoaiHinh=MaLoaiHinhNhap AND NamDangKy=YEAR(NgayDangKyNhap) AND MaNPL = MaPhu AND TenNPL= TenHang AND SoLuong=LuongNhap) ELSE 0 END AS PhuThu,
       TyGiaTT ,
       ThueSuat ,
       ThueXNK ,
       ThueXNKTon ,
       NgayThucXuat FROM dbo.t_KDT_SXXK_BCXuatNhapTon WHERE NamThanhLy =@NAMQUYETTOAN
	   ) TB
 END

 GO

GO
 IF  EXISTS (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID(N'p_KDT_SXXK_NPLNhapTon_SelectTotalNPLTonByTimes') AND TYPE IN (N'U',N'PV',N'P',N'PC'))
 DROP PROCEDURE p_KDT_SXXK_NPLNhapTon_SelectTotalNPLTonByTimes
GO
CREATE PROCEDURE p_KDT_SXXK_NPLNhapTon_SelectTotalNPLTonByTimes
 @DateFrom DATETIME ,
 @DateTo DATETIME
 AS
 BEGIN
     SELECT
 T.MaNPL AS MANPL,
 T.TenNPL AS TENNPL,
 T.TenDVT AS DVT,
 SUM(T.Ton) AS LUONGTONDK,
 SUM(T.ThueXNKTon) AS TRIGIATONDK
 FROM
 (
 SELECT  SoToKhaiVNACCS ,
         MaDoanhNghiep ,
         MaHaiQuan ,
         SoToKhai ,
         MaLoaiHinh ,
         NamDangKy ,
         MaNPL ,
         TenNPL ,
         DVT_ID ,
         TenDVT ,
         NuocXX_ID ,
         TriGiaKB ,
         Luong ,
         Ton ,
         ThueXNK ,
         ThueXNKTon ,
         ThueTTDB ,
         ThueVAT ,
         ThueCLGia ,
         PhuThu ,
         NgayDangKy ,
         SoLanThanhLy ,
         LanThanhLy ,
         TrangThaiThanhKhoan ,
         SaiSoLuong ,
         SoLuongDangKy ,
         TienTrinhChayThanhLy ,
         ThanhLy ,
         LechTon ,
         TenChuHang ,
         SoHopDong  
FROM    dbo.v_HangTon WHERE NgayDangKy BETWEEN @DateFrom AND @DateTo
) T GROUP BY T.MaNPL,T.TenNPL,T.TenDVT ORDER BY T.MaNPL      
 END

 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '40.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('40.0',GETDATE(), N'CẬP NHẬT PROCEDURE XỬ LÝ BÁO CÁO QUYẾT TOÁN MỚI')
END