/*
Run this script on:

        192.168.72.100.ECS_TQDT_KD_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_KD_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 08/24/2012 2:46:47 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_PhuongThucVanTai]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_Load]
	@ID varchar(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_PhuongThucVanTai]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_InsertUpdate]
	@ID varchar(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_PhuongThucVanTai] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_PhuongThucVanTai] 
		SET
			[Ten] = @Ten,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_PhuongThucVanTai]
	(
			[ID],
			[Ten],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_Insert]
	@ID varchar(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_PhuongThucVanTai]
(
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_Delete]
	@ID varchar(3)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_PhuongThucVanTai]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucThanhToan_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_Update]
	@ID varchar(10),
	@GhiChu nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_PhuongThucThanhToan]
SET
	[GhiChu] = @GhiChu,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucThanhToan_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GhiChu],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_PhuongThucThanhToan]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucThanhToan_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_Load]
	@ID varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GhiChu],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_PhuongThucThanhToan]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucThanhToan_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_InsertUpdate]
	@ID varchar(10),
	@GhiChu nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_PhuongThucThanhToan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_PhuongThucThanhToan] 
		SET
			[GhiChu] = @GhiChu,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_PhuongThucThanhToan]
	(
			[ID],
			[GhiChu],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@GhiChu,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucThanhToan_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_Insert]
	@ID varchar(10),
	@GhiChu nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_PhuongThucThanhToan]
(
	[ID],
	[GhiChu],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@GhiChu,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucThanhToan_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_Delete]
	@ID varchar(10)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_PhuongThucThanhToan]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_Update]
	@ID char(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_Nuoc]
SET
	[Ten] = @Ten,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_Nuoc]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_Load]
	@ID char(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_Nuoc]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_InsertUpdate]
	@ID char(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_Nuoc] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_Nuoc] 
		SET
			[Ten] = @Ten,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_Nuoc]
	(
			[ID],
			[Ten],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_SXXK_HangMauDich]'
GO
ALTER TABLE [dbo].[t_SXXK_HangMauDich] ALTER COLUMN [SoLuong] [numeric] (18, 5) NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_Insert]
	@ID char(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_Nuoc]
(
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_Delete]
	@ID char(3)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_Nuoc]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_Update]
	@ID char(3),
	@Ten nvarchar(50),
	@TyGiaTinhThue money,
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_NguyenTe]
SET
	[Ten] = @Ten,
	[TyGiaTinhThue] = @TyGiaTinhThue,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_HangMauDich_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_InsertUpdate]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_SXXK_HangMauDich_InsertUpdate]
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@MaHaiQuan char(6),
	@NamDangKy smallint,
	@SoThuTuHang smallint,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueSuatXNK float,
	@ThueSuatTTDB float,
	@ThueSuatGTGT float,
	@ThueXNK float,
	@ThueTTDB float,
	@ThueGTGT float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue float
AS
IF EXISTS(SELECT [SoToKhai], [MaLoaiHinh], [MaHaiQuan], [NamDangKy], [SoThuTuHang] FROM [dbo].[t_SXXK_HangMauDich] WHERE [SoToKhai] = @SoToKhai AND [MaLoaiHinh] = @MaLoaiHinh AND [MaHaiQuan] = @MaHaiQuan AND [NamDangKy] = @NamDangKy AND [SoThuTuHang] = @SoThuTuHang)
	BEGIN
		UPDATE
			[dbo].[t_SXXK_HangMauDich] 
		SET
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[DonGiaKB] = @DonGiaKB,
			[DonGiaTT] = @DonGiaTT,
			[TriGiaKB] = @TriGiaKB,
			[TriGiaTT] = @TriGiaTT,
			[TriGiaKB_VND] = @TriGiaKB_VND,
			[ThueSuatXNK] = @ThueSuatXNK,
			[ThueSuatTTDB] = @ThueSuatTTDB,
			[ThueSuatGTGT] = @ThueSuatGTGT,
			[ThueXNK] = @ThueXNK,
			[ThueTTDB] = @ThueTTDB,
			[ThueGTGT] = @ThueGTGT,
			[PhuThu] = @PhuThu,
			[TyLeThuKhac] = @TyLeThuKhac,
			[TriGiaThuKhac] = @TriGiaThuKhac,
			[MienThue] = @MienThue
		WHERE
			[SoToKhai] = @SoToKhai
			AND [MaLoaiHinh] = @MaLoaiHinh
			AND [MaHaiQuan] = @MaHaiQuan
			AND [NamDangKy] = @NamDangKy
			AND [SoThuTuHang] = @SoThuTuHang
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_SXXK_HangMauDich]
	(
			[SoToKhai],
			[MaLoaiHinh],
			[MaHaiQuan],
			[NamDangKy],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[DonGiaKB],
			[DonGiaTT],
			[TriGiaKB],
			[TriGiaTT],
			[TriGiaKB_VND],
			[ThueSuatXNK],
			[ThueSuatTTDB],
			[ThueSuatGTGT],
			[ThueXNK],
			[ThueTTDB],
			[ThueGTGT],
			[PhuThu],
			[TyLeThuKhac],
			[TriGiaThuKhac],
			[MienThue]
	)
	VALUES
	(
			@SoToKhai,
			@MaLoaiHinh,
			@MaHaiQuan,
			@NamDangKy,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@DonGiaKB,
			@DonGiaTT,
			@TriGiaKB,
			@TriGiaTT,
			@TriGiaKB_VND,
			@ThueSuatXNK,
			@ThueSuatTTDB,
			@ThueSuatGTGT,
			@ThueXNK,
			@ThueTTDB,
			@ThueGTGT,
			@PhuThu,
			@TyLeThuKhac,
			@TriGiaThuKhac,
			@MienThue
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[TyGiaTinhThue],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_NguyenTe]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_HangMauDich_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_Insert]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_SXXK_HangMauDich_Insert]
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@MaHaiQuan char(6),
	@NamDangKy smallint,
	@SoThuTuHang smallint,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18,5),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueSuatXNK float,
	@ThueSuatTTDB float,
	@ThueSuatGTGT float,
	@ThueXNK float,
	@ThueTTDB float,
	@ThueGTGT float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue float
AS
INSERT INTO [dbo].[t_SXXK_HangMauDich]
(
	[SoToKhai],
	[MaLoaiHinh],
	[MaHaiQuan],
	[NamDangKy],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue]
)
VALUES
(
	@SoToKhai,
	@MaLoaiHinh,
	@MaHaiQuan,
	@NamDangKy,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@DonGiaKB,
	@DonGiaTT,
	@TriGiaKB,
	@TriGiaTT,
	@TriGiaKB_VND,
	@ThueSuatXNK,
	@ThueSuatTTDB,
	@ThueSuatGTGT,
	@ThueXNK,
	@ThueTTDB,
	@ThueGTGT,
	@PhuThu,
	@TyLeThuKhac,
	@TriGiaThuKhac,
	@MienThue
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_Load]
	@ID char(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[TyGiaTinhThue],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_NguyenTe]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_SXXK_HangMauDich_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_Update]
-- Database: HaiQuanKD
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 03, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_SXXK_HangMauDich_Update]
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@MaHaiQuan char(6),
	@NamDangKy smallint,
	@SoThuTuHang smallint,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueSuatXNK float,
	@ThueSuatTTDB float,
	@ThueSuatGTGT float,
	@ThueXNK float,
	@ThueTTDB float,
	@ThueGTGT float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue float
AS
UPDATE
	[dbo].[t_SXXK_HangMauDich]
SET
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[DonGiaKB] = @DonGiaKB,
	[DonGiaTT] = @DonGiaTT,
	[TriGiaKB] = @TriGiaKB,
	[TriGiaTT] = @TriGiaTT,
	[TriGiaKB_VND] = @TriGiaKB_VND,
	[ThueSuatXNK] = @ThueSuatXNK,
	[ThueSuatTTDB] = @ThueSuatTTDB,
	[ThueSuatGTGT] = @ThueSuatGTGT,
	[ThueXNK] = @ThueXNK,
	[ThueTTDB] = @ThueTTDB,
	[ThueGTGT] = @ThueGTGT,
	[PhuThu] = @PhuThu,
	[TyLeThuKhac] = @TyLeThuKhac,
	[TriGiaThuKhac] = @TriGiaThuKhac,
	[MienThue] = @MienThue
WHERE
	[SoToKhai] = @SoToKhai
	AND [MaLoaiHinh] = @MaLoaiHinh
	AND [MaHaiQuan] = @MaHaiQuan
	AND [NamDangKy] = @NamDangKy
	AND [SoThuTuHang] = @SoThuTuHang

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_InsertUpdate]
	@ID char(3),
	@Ten nvarchar(50),
	@TyGiaTinhThue money,
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_NguyenTe] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_NguyenTe] 
		SET
			[Ten] = @Ten,
			[TyGiaTinhThue] = @TyGiaTinhThue,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_NguyenTe]
	(
			[ID],
			[Ten],
			[TyGiaTinhThue],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@TyGiaTinhThue,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_HopDongThuongMaiDetail]'
GO
ALTER TABLE [dbo].[t_KDT_HopDongThuongMaiDetail] ALTER COLUMN [SoLuong] [numeric] (18, 5) NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_Insert]
	@ID char(3),
	@Ten nvarchar(50),
	@TyGiaTinhThue money,
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_NguyenTe]
(
	[ID],
	[Ten],
	[TyGiaTinhThue],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@TyGiaTinhThue,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_Delete]
	@ID char(3)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_NguyenTe]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]'
GO
CREATE TABLE [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]
(
[ID] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Ten] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateCreated] [datetime] NULL,
[DateModified] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_HaiQuan_LoaiPhiChungTuThanhToan] on [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]'
GO
ALTER TABLE [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan] ADD CONSTRAINT [PK_t_HaiQuan_LoaiPhiChungTuThanhToan] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Update]
	@ID varchar(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]
SET
	[Ten] = @Ten,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Load]
	@ID varchar(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_InsertUpdate]
	@ID varchar(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan] 
		SET
			[Ten] = @Ten,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]
	(
			[ID],
			[Ten],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Insert]
	@ID varchar(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]
(
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_Delete]
	@ID varchar(3)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViTinh_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViTinh_Update]
	@ID char(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_DonViTinh]
SET
	[Ten] = @Ten,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViTinh_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViTinh_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DonViTinh]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViTinh_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViTinh_Load]
	@ID char(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DonViTinh]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViTinh_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViTinh_InsertUpdate]
	@ID char(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_DonViTinh] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_DonViTinh] 
		SET
			[Ten] = @Ten,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_DonViTinh]
	(
			[ID],
			[Ten],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViTinh_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViTinh_Insert]
	@ID char(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_DonViTinh]
(
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViTinh_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViTinh_Delete]
	@ID char(3)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_DonViTinh]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Update]
	@ID char(6),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_DonViHaiQuan]
SET
	[Ten] = @Ten,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DonViHaiQuan]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Load]
	@ID char(6)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DonViHaiQuan]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]
	@ID char(6),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_DonViHaiQuan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_DonViHaiQuan] 
		SET
			[Ten] = @Ten,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_DonViHaiQuan]
	(
			[ID],
			[Ten],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Insert]
	@ID char(6),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_DonViHaiQuan]
(
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_HoaDonThuongMaiDetail]'
GO
ALTER TABLE [dbo].[t_KDT_HoaDonThuongMaiDetail] ALTER COLUMN [SoLuong] [numeric] (18, 5) NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Delete]
	@ID char(6)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_DonViHaiQuan]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_Update]
	@ID varchar(7),
	@MoTa nvarchar(30),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_DieuKienGiaoHang]
SET
	[MoTa] = @MoTa,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MoTa],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DieuKienGiaoHang]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_Load]
	@ID varchar(7)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MoTa],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DieuKienGiaoHang]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_InsertUpdate]
	@ID varchar(7),
	@MoTa nvarchar(30),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_DieuKienGiaoHang] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_DieuKienGiaoHang] 
		SET
			[MoTa] = @MoTa,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_DieuKienGiaoHang]
	(
			[ID],
			[MoTa],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@MoTa,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_Insert]
	@ID varchar(7),
	@MoTa nvarchar(30),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_DieuKienGiaoHang]
(
	[ID],
	[MoTa],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@MoTa,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_Delete]
	@ID varchar(7)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_DieuKienGiaoHang]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_HangVanDonDetail]'
GO
ALTER TABLE [dbo].[t_KDT_HangVanDonDetail] ALTER COLUMN [SoLuong] [numeric] (18, 5) NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangVanDonDetail_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 28, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Insert]
	@VanDon_ID bigint,
	@MaNguyenTe varchar(50),
	@MaChuyenNganh varchar(100),
	@GhiChu nvarchar(255),
	@HMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@DonGiaKB float,
	@TriGiaKB float,
	@LoaiKien nvarchar(10),
	@SoHieuContainer nvarchar(50),
	@SoHieuKien nvarchar(50),
	@TrongLuong float,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HangVanDonDetail]
(
	[VanDon_ID],
	[MaNguyenTe],
	[MaChuyenNganh],
	[GhiChu],
	[HMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[DonGiaKB],
	[TriGiaKB],
	[LoaiKien],
	[SoHieuContainer],
	[SoHieuKien],
	[TrongLuong]
)
VALUES 
(
	@VanDon_ID,
	@MaNguyenTe,
	@MaChuyenNganh,
	@GhiChu,
	@HMD_ID,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@DonGiaKB,
	@TriGiaKB,
	@LoaiKien,
	@SoHieuContainer,
	@SoHieuKien,
	@TrongLuong
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_Update]
	@ID varchar(6),
	@Ten nvarchar(50),
	@MaCuc char(2),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_CuaKhau]
SET
	[Ten] = @Ten,
	[MaCuc] = @MaCuc,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangVanDonDetail_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 28, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangVanDonDetail_Update]
	@ID bigint,
	@VanDon_ID bigint,
	@MaNguyenTe varchar(50),
	@MaChuyenNganh varchar(100),
	@GhiChu nvarchar(255),
	@HMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@DonGiaKB float,
	@TriGiaKB float,
	@LoaiKien nvarchar(10),
	@SoHieuContainer nvarchar(50),
	@SoHieuKien nvarchar(50),
	@TrongLuong float
AS

UPDATE
	[dbo].[t_KDT_HangVanDonDetail]
SET
	[VanDon_ID] = @VanDon_ID,
	[MaNguyenTe] = @MaNguyenTe,
	[MaChuyenNganh] = @MaChuyenNganh,
	[GhiChu] = @GhiChu,
	[HMD_ID] = @HMD_ID,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[DonGiaKB] = @DonGiaKB,
	[TriGiaKB] = @TriGiaKB,
	[LoaiKien] = @LoaiKien,
	[SoHieuContainer] = @SoHieuContainer,
	[SoHieuKien] = @SoHieuKien,
	[TrongLuong] = @TrongLuong
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangVanDonDetail_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangVanDonDetail_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 28, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangVanDonDetail_InsertUpdate]
	@ID bigint,
	@VanDon_ID bigint,
	@MaNguyenTe varchar(50),
	@MaChuyenNganh varchar(100),
	@GhiChu nvarchar(255),
	@HMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@DonGiaKB float,
	@TriGiaKB float,
	@LoaiKien nvarchar(10),
	@SoHieuContainer nvarchar(50),
	@SoHieuKien nvarchar(50),
	@TrongLuong float
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangVanDonDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HangVanDonDetail] 
		SET
			[VanDon_ID] = @VanDon_ID,
			[MaNguyenTe] = @MaNguyenTe,
			[MaChuyenNganh] = @MaChuyenNganh,
			[GhiChu] = @GhiChu,
			[HMD_ID] = @HMD_ID,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[DonGiaKB] = @DonGiaKB,
			[TriGiaKB] = @TriGiaKB,
			[LoaiKien] = @LoaiKien,
			[SoHieuContainer] = @SoHieuContainer,
			[SoHieuKien] = @SoHieuKien,
			[TrongLuong] = @TrongLuong
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HangVanDonDetail]
		(
			[VanDon_ID],
			[MaNguyenTe],
			[MaChuyenNganh],
			[GhiChu],
			[HMD_ID],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[DonGiaKB],
			[TriGiaKB],
			[LoaiKien],
			[SoHieuContainer],
			[SoHieuKien],
			[TrongLuong]
		)
		VALUES 
		(
			@VanDon_ID,
			@MaNguyenTe,
			@MaChuyenNganh,
			@GhiChu,
			@HMD_ID,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@DonGiaKB,
			@TriGiaKB,
			@LoaiKien,
			@SoHieuContainer,
			@SoHieuKien,
			@TrongLuong
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[MaCuc],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_CuaKhau]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_Load]
	@ID varchar(6)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[MaCuc],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_CuaKhau]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_InsertUpdate]
	@ID varchar(6),
	@Ten nvarchar(50),
	@MaCuc char(2),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_CuaKhau] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_CuaKhau] 
		SET
			[Ten] = @Ten,
			[MaCuc] = @MaCuc,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_CuaKhau]
	(
			[ID],
			[Ten],
			[MaCuc],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@MaCuc,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_Insert]
	@ID varchar(6),
	@Ten nvarchar(50),
	@MaCuc char(2),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_CuaKhau]
(
	[ID],
	[Ten],
	[MaCuc],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@MaCuc,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_Delete]
	@ID varchar(6)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_CuaKhau]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_MaHS_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_Update]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_MaHS_Update]
	@Nhom nvarchar(255),
	@PN1 nvarchar(255),
	@PN2 nvarchar(255),
	@Pn3 nvarchar(255),
	@Mo_ta nvarchar(255),
	@HS10SO nvarchar(255),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_MaHS]
SET
	[Nhom] = @Nhom,
	[PN1] = @PN1,
	[PN2] = @PN2,
	[Pn3] = @Pn3,
	[Mo_ta] = @Mo_ta,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[HS10SO] = @HS10SO


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_MaHS_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_SelectAll]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_MaHS_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_MaHS]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaHS_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_Load]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_Load]
	@HS10SO nvarchar(255)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_MaHS]
WHERE
	[HS10SO] = @HS10SO

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaHS_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_InsertUpdate]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_InsertUpdate]
	@Nhom nvarchar(255),
	@PN1 nvarchar(255),
	@PN2 nvarchar(255),
	@Pn3 nvarchar(255),
	@Mo_ta nvarchar(255),
	@HS10SO nvarchar(255),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [HS10SO] FROM [dbo].[t_HaiQuan_MaHS] WHERE [HS10SO] = @HS10SO)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_MaHS] 
		SET
			[Nhom] = @Nhom,
			[PN1] = @PN1,
			[PN2] = @PN2,
			[Pn3] = @Pn3,
			[Mo_ta] = @Mo_ta,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[HS10SO] = @HS10SO
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_MaHS]
	(
			[Nhom],
			[PN1],
			[PN2],
			[Pn3],
			[Mo_ta],
			[HS10SO],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@Nhom,
			@PN1,
			@PN2,
			@Pn3,
			@Mo_ta,
			@HS10SO,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaHS_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_Insert]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_Insert]
	@Nhom nvarchar(255),
	@PN1 nvarchar(255),
	@PN2 nvarchar(255),
	@Pn3 nvarchar(255),
	@Mo_ta nvarchar(255),
	@HS10SO nvarchar(255),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_MaHS]
(
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@Nhom,
	@PN1,
	@PN2,
	@Pn3,
	@Mo_ta,
	@HS10SO,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaHS_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_Delete]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_Delete]
	@HS10SO nvarchar(255)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_MaHS]
WHERE
	[HS10SO] = @HS10SO


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_Update]
	@ID varchar(3),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_PhuongThucVanTai]
SET
	[Ten] = @Ten,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] ALTER COLUMN [SoLuong] [numeric] (18, 5) NULL
ALTER TABLE [dbo].[t_KDT_HangMauDich] ALTER COLUMN [TrongLuong] [numeric] (18, 5) NULL
ALTER TABLE [dbo].[t_KDT_HangMauDich] ALTER COLUMN [SoLuong_HTS] [numeric] (18, 5) NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, March 02, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Insert]
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@TrongLuong numeric(18, 5),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueSuatXNK float,
	@ThueSuatTTDB float,
	@ThueSuatGTGT float,
	@ThueXNK float,
	@ThueTTDB float,
	@ThueGTGT float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 5),
	@FOC bit,
	@ThueTuyetDoi bit,
	@ThueSuatXNKGiam varchar(50),
	@ThueSuatTTDBGiam varchar(50),
	@ThueSuatVATGiam varchar(50),
	@DonGiaTuyetDoi float,
	@MaHSMoRong nvarchar(12),
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@MaHangSX nvarchar(30),
	@TenHangSX nvarchar(256),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HangMauDich]
(
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[FOC],
	[ThueTuyetDoi],
	[ThueSuatXNKGiam],
	[ThueSuatTTDBGiam],
	[ThueSuatVATGiam],
	[DonGiaTuyetDoi],
	[MaHSMoRong],
	[NhanHieu],
	[QuyCachPhamChat],
	[ThanhPhan],
	[Model],
	[MaHangSX],
	[TenHangSX]
)
VALUES 
(
	@TKMD_ID,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@TrongLuong,
	@DonGiaKB,
	@DonGiaTT,
	@TriGiaKB,
	@TriGiaTT,
	@TriGiaKB_VND,
	@ThueSuatXNK,
	@ThueSuatTTDB,
	@ThueSuatGTGT,
	@ThueXNK,
	@ThueTTDB,
	@ThueGTGT,
	@PhuThu,
	@TyLeThuKhac,
	@TriGiaThuKhac,
	@MienThue,
	@Ma_HTS,
	@DVT_HTS,
	@SoLuong_HTS,
	@FOC,
	@ThueTuyetDoi,
	@ThueSuatXNKGiam,
	@ThueSuatTTDBGiam,
	@ThueSuatVATGiam,
	@DonGiaTuyetDoi,
	@MaHSMoRong,
	@NhanHieu,
	@QuyCachPhamChat,
	@ThanhPhan,
	@Model,
	@MaHangSX,
	@TenHangSX
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, March 02, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@TrongLuong numeric(18, 5),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueSuatXNK float,
	@ThueSuatTTDB float,
	@ThueSuatGTGT float,
	@ThueXNK float,
	@ThueTTDB float,
	@ThueGTGT float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 5),
	@FOC bit,
	@ThueTuyetDoi bit,
	@ThueSuatXNKGiam varchar(50),
	@ThueSuatTTDBGiam varchar(50),
	@ThueSuatVATGiam varchar(50),
	@DonGiaTuyetDoi float,
	@MaHSMoRong nvarchar(12),
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@MaHangSX nvarchar(30),
	@TenHangSX nvarchar(256)
AS

UPDATE
	[dbo].[t_KDT_HangMauDich]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[TrongLuong] = @TrongLuong,
	[DonGiaKB] = @DonGiaKB,
	[DonGiaTT] = @DonGiaTT,
	[TriGiaKB] = @TriGiaKB,
	[TriGiaTT] = @TriGiaTT,
	[TriGiaKB_VND] = @TriGiaKB_VND,
	[ThueSuatXNK] = @ThueSuatXNK,
	[ThueSuatTTDB] = @ThueSuatTTDB,
	[ThueSuatGTGT] = @ThueSuatGTGT,
	[ThueXNK] = @ThueXNK,
	[ThueTTDB] = @ThueTTDB,
	[ThueGTGT] = @ThueGTGT,
	[PhuThu] = @PhuThu,
	[TyLeThuKhac] = @TyLeThuKhac,
	[TriGiaThuKhac] = @TriGiaThuKhac,
	[MienThue] = @MienThue,
	[Ma_HTS] = @Ma_HTS,
	[DVT_HTS] = @DVT_HTS,
	[SoLuong_HTS] = @SoLuong_HTS,
	[FOC] = @FOC,
	[ThueTuyetDoi] = @ThueTuyetDoi,
	[ThueSuatXNKGiam] = @ThueSuatXNKGiam,
	[ThueSuatTTDBGiam] = @ThueSuatTTDBGiam,
	[ThueSuatVATGiam] = @ThueSuatVATGiam,
	[DonGiaTuyetDoi] = @DonGiaTuyetDoi,
	[MaHSMoRong] = @MaHSMoRong,
	[NhanHieu] = @NhanHieu,
	[QuyCachPhamChat] = @QuyCachPhamChat,
	[ThanhPhan] = @ThanhPhan,
	[Model] = @Model,
	[MaHangSX] = @MaHangSX,
	[TenHangSX] = @TenHangSX
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, March 02, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(252),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(18, 5),
	@TrongLuong numeric(18, 5),
	@DonGiaKB float,
	@DonGiaTT float,
	@TriGiaKB float,
	@TriGiaTT float,
	@TriGiaKB_VND float,
	@ThueSuatXNK float,
	@ThueSuatTTDB float,
	@ThueSuatGTGT float,
	@ThueXNK float,
	@ThueTTDB float,
	@ThueGTGT float,
	@PhuThu float,
	@TyLeThuKhac float,
	@TriGiaThuKhac float,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 5),
	@FOC bit,
	@ThueTuyetDoi bit,
	@ThueSuatXNKGiam varchar(50),
	@ThueSuatTTDBGiam varchar(50),
	@ThueSuatVATGiam varchar(50),
	@DonGiaTuyetDoi float,
	@MaHSMoRong nvarchar(12),
	@NhanHieu nvarchar(256),
	@QuyCachPhamChat nvarchar(1000),
	@ThanhPhan nvarchar(100),
	@Model nvarchar(35),
	@MaHangSX nvarchar(30),
	@TenHangSX nvarchar(256)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HangMauDich] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[TrongLuong] = @TrongLuong,
			[DonGiaKB] = @DonGiaKB,
			[DonGiaTT] = @DonGiaTT,
			[TriGiaKB] = @TriGiaKB,
			[TriGiaTT] = @TriGiaTT,
			[TriGiaKB_VND] = @TriGiaKB_VND,
			[ThueSuatXNK] = @ThueSuatXNK,
			[ThueSuatTTDB] = @ThueSuatTTDB,
			[ThueSuatGTGT] = @ThueSuatGTGT,
			[ThueXNK] = @ThueXNK,
			[ThueTTDB] = @ThueTTDB,
			[ThueGTGT] = @ThueGTGT,
			[PhuThu] = @PhuThu,
			[TyLeThuKhac] = @TyLeThuKhac,
			[TriGiaThuKhac] = @TriGiaThuKhac,
			[MienThue] = @MienThue,
			[Ma_HTS] = @Ma_HTS,
			[DVT_HTS] = @DVT_HTS,
			[SoLuong_HTS] = @SoLuong_HTS,
			[FOC] = @FOC,
			[ThueTuyetDoi] = @ThueTuyetDoi,
			[ThueSuatXNKGiam] = @ThueSuatXNKGiam,
			[ThueSuatTTDBGiam] = @ThueSuatTTDBGiam,
			[ThueSuatVATGiam] = @ThueSuatVATGiam,
			[DonGiaTuyetDoi] = @DonGiaTuyetDoi,
			[MaHSMoRong] = @MaHSMoRong,
			[NhanHieu] = @NhanHieu,
			[QuyCachPhamChat] = @QuyCachPhamChat,
			[ThanhPhan] = @ThanhPhan,
			[Model] = @Model,
			[MaHangSX] = @MaHangSX,
			[TenHangSX] = @TenHangSX
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HangMauDich]
		(
			[TKMD_ID],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[TrongLuong],
			[DonGiaKB],
			[DonGiaTT],
			[TriGiaKB],
			[TriGiaTT],
			[TriGiaKB_VND],
			[ThueSuatXNK],
			[ThueSuatTTDB],
			[ThueSuatGTGT],
			[ThueXNK],
			[ThueTTDB],
			[ThueGTGT],
			[PhuThu],
			[TyLeThuKhac],
			[TriGiaThuKhac],
			[MienThue],
			[Ma_HTS],
			[DVT_HTS],
			[SoLuong_HTS],
			[FOC],
			[ThueTuyetDoi],
			[ThueSuatXNKGiam],
			[ThueSuatTTDBGiam],
			[ThueSuatVATGiam],
			[DonGiaTuyetDoi],
			[MaHSMoRong],
			[NhanHieu],
			[QuyCachPhamChat],
			[ThanhPhan],
			[Model],
			[MaHangSX],
			[TenHangSX]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@TrongLuong,
			@DonGiaKB,
			@DonGiaTT,
			@TriGiaKB,
			@TriGiaTT,
			@TriGiaKB_VND,
			@ThueSuatXNK,
			@ThueSuatTTDB,
			@ThueSuatGTGT,
			@ThueXNK,
			@ThueTTDB,
			@ThueGTGT,
			@PhuThu,
			@TyLeThuKhac,
			@TriGiaThuKhac,
			@MienThue,
			@Ma_HTS,
			@DVT_HTS,
			@SoLuong_HTS,
			@FOC,
			@ThueTuyetDoi,
			@ThueSuatXNKGiam,
			@ThueSuatTTDBGiam,
			@ThueSuatVATGiam,
			@DonGiaTuyetDoi,
			@MaHSMoRong,
			@NhanHieu,
			@QuyCachPhamChat,
			@ThanhPhan,
			@Model,
			@MaHangSX,
			@TenHangSX
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_Update]
	@ID nvarchar(2),
	@Ten nvarchar(50),
	@IPService varchar(150),
	@ServicePathV1 varchar(50),
	@ServicePathV2 varchar(50),
	@ServicePathV3 varchar(50),
	@IPServiceCKS varchar(150),
	@ServicePathCKS varchar(50),
	@IsDefault varchar(3),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_Cuc]
SET
	[Ten] = @Ten,
	[IPService] = @IPService,
	[ServicePathV1] = @ServicePathV1,
	[ServicePathV2] = @ServicePathV2,
	[ServicePathV3] = @ServicePathV3,
	[IPServiceCKS] = @IPServiceCKS,
	[ServicePathCKS] = @ServicePathCKS,
	[IsDefault] = @IsDefault,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[IPService],
	[ServicePathV1],
	[ServicePathV2],
	[ServicePathV3],
	[IPServiceCKS],
	[ServicePathCKS],
	[IsDefault],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_Cuc]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_Load]
	@ID nvarchar(2)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[IPService],
	[ServicePathV1],
	[ServicePathV2],
	[ServicePathV3],
	[IPServiceCKS],
	[ServicePathCKS],
	[IsDefault],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_Cuc]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_InsertUpdate]
	@ID nvarchar(2),
	@Ten nvarchar(50),
	@IPService varchar(150),
	@ServicePathV1 varchar(50),
	@ServicePathV2 varchar(50),
	@ServicePathV3 varchar(50),
	@IPServiceCKS varchar(150),
	@ServicePathCKS varchar(50),
	@IsDefault varchar(3),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_Cuc] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_Cuc] 
		SET
			[Ten] = @Ten,
			[IPService] = @IPService,
			[ServicePathV1] = @ServicePathV1,
			[ServicePathV2] = @ServicePathV2,
			[ServicePathV3] = @ServicePathV3,
			[IPServiceCKS] = @IPServiceCKS,
			[ServicePathCKS] = @ServicePathCKS,
			[IsDefault] = @IsDefault,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_Cuc]
	(
			[ID],
			[Ten],
			[IPService],
			[ServicePathV1],
			[ServicePathV2],
			[ServicePathV3],
			[IPServiceCKS],
			[ServicePathCKS],
			[IsDefault],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@IPService,
			@ServicePathV1,
			@ServicePathV2,
			@ServicePathV3,
			@IPServiceCKS,
			@ServicePathCKS,
			@IsDefault,
			@DateCreated,
			@DateModified
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_Insert]
	@ID nvarchar(2),
	@Ten nvarchar(50),
	@IPService varchar(150),
	@ServicePathV1 varchar(50),
	@ServicePathV2 varchar(50),
	@ServicePathV3 varchar(50),
	@IPServiceCKS varchar(150),
	@ServicePathCKS varchar(50),
	@IsDefault varchar(3),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_Cuc]
(
	[ID],
	[Ten],
	[IPService],
	[ServicePathV1],
	[ServicePathV2],
	[ServicePathV3],
	[IPServiceCKS],
	[ServicePathCKS],
	[IsDefault],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@IPService,
	@ServicePathV1,
	@ServicePathV2,
	@ServicePathV3,
	@IPServiceCKS,
	@ServicePathCKS,
	@IsDefault,
	@DateCreated,
	@DateModified
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_Delete]
	@ID nvarchar(2)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_Cuc]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_Nuoc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Nuoc_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Nuoc_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Nuoc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_Nuoc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[TyGiaTinhThue],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_NguyenTe] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_NguyenTe_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_NguyenTe_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_NguyenTe_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_NguyenTe] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_DonViHaiQuan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_DonViHaiQuan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[IPService],
	[ServicePathV1],
	[ServicePathV2],
	[ServicePathV3],
	[IPServiceCKS],
	[ServicePathCKS],
	[IsDefault],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_Cuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_Cuc_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_Cuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_Cuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[MaCuc],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_CuaKhau] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_HaiQuan_CuaKhau_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_CuaKhau_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_HaiQuan_CuaKhau_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_CuaKhau] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_PhuongThucVanTai] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucThanhToan_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GhiChu],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_PhuongThucThanhToan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucThanhToan_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucThanhToan_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucThanhToan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_PhuongThucThanhToan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiPhiChungTuThanhToan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_LoaiPhiChungTuThanhToan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViTinh_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViTinh_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_DonViTinh] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DonViTinh_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViTinh_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViTinh_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_DonViTinh] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MoTa],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_DieuKienGiaoHang] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_DieuKienGiaoHang_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DieuKienGiaoHang_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DieuKienGiaoHang_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_DieuKienGiaoHang] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaHS_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_SelectDynamic]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[Nhom],
	[PN1],
	[PN2],
	[Pn3],
	[Mo_ta],
	[HS10SO],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_MaHS] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_MaHS_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_MaHS_DeleteDynamic]
-- Database: ECS_TQDT_DANHMUC
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_MaHS_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_MaHS] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_HaiQuan_PhuongThucVanTai_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_PhuongThucVanTai_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_PhuongThucVanTai_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_PhuongThucVanTai] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='3.8', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('3.8', getdate(), null)
	end 
	go

