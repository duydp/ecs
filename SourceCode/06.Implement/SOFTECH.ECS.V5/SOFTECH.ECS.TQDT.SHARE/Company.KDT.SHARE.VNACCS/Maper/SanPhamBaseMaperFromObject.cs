using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS.Maper
{
    public partial class SanPhamMaperFromObject
    {
        #region Private members.

        protected string _MaHaiQuan = String.Empty;
        protected string _MaDoanhNghiep = String.Empty;
        protected string _Ma = String.Empty;
        protected string _Ten = String.Empty;
        protected string _MaHS = String.Empty;
        protected string _DVT_ID = String.Empty;
        protected int _IsExist = 0;
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public string MaHaiQuan
        {
            set { this._MaHaiQuan = value.Trim(); }
            get { return this._MaHaiQuan.Trim(); }
        }

        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value.Trim(); }
            get { return this._MaDoanhNghiep.Trim(); }
        }

        public string Ma
        {
            set { this._Ma = value; }
            get { return this._Ma; }
        }

        public string Ten
        {
            set { this._Ten = value; }
            get { return this._Ten; }
        }

        public string MaHS
        {
            set { this._MaHS = value; }
            get { return this._MaHS; }
        }

        public string DVT_ID
        {
            set { this._DVT_ID = value; }
            get { return this._DVT_ID; }
        }

        //---------------------------------------------------------------------------------------------

        public int IsExist
        {
            set { this._IsExist = value; }
            get { return this._IsExist; }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_SXXK_SanPham_Load";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) this._Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) this._Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                reader.Close();
                return true;
                
            }
            reader.Close();
            return false;
        }
        public bool Load_SP()
        {
            string spName = "p_SXXK_SanPham_Load_SP";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) this._Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) this._Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                reader.Close();
                return true;

            }
            reader.Close();
            return false;
        }
        #endregion
    }
}