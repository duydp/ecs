﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
 

namespace Company.Interface.Report.GC
{
    public partial class DinhMucReport : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HD = new HopDong();
        private int STT = 0;
        public DataTable dt = new DataTable();        
        public DinhMucReport()
        {
            InitializeComponent();
        }
        public void BindReport(Company.GC.BLL.GC.SanPham SP)
        {

            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI.ToUpper();
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;
            lblSoLuong.Text = "";
            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac.ToUpper();
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;
            lblMathang.Text = SP.Ten + "  (Mã hàng: " + SP.Ma + ")";
            lblSoLuong.Text = SP.SoLuongDangKy.ToString("N" + GlobalSettings.SoThapPhan.LuongSP) + " " + DonViTinh.GetName(SP.DVT_ID);

            dt.TableName = "t_GC_DinhMuc";
            this.DataSource = dt;

            lblTenNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenNPL");
            lblMaNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaNguyenPhuLieu");
            lblDMSD.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DinhMucSuDung", "{0:N" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblTLHH.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TyLeHaoHut", "{00:F00}");
            xrLabel19.Text= GlobalSettings.TieudeNgay;
            

        }

        private void DinhMucReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }

        private void lblDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblDVT.Text = DonViTinh.GetName(GetCurrentColumnValue("DVT_ID"));
        }
    }
}
