using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Activation.Server;

namespace Licenses_v2.Module
{
    public partial class FrmGroupEmployeeManager : DevExpress.XtraEditors.XtraForm
    {
        public FrmGroupEmployeeManager()
        {
            InitializeComponent();
        }

        #region Adding Method

        private void OpenForm(string id, bool isEdit)
        {
            if (!ClassLib.IsFormShowed("Group_" + id))
            {
                FrmGroupEmployeeInfo obj = new FrmGroupEmployeeInfo(id);
                obj.MdiParent = this.MdiParent;
                obj.isEdit = isEdit;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Group_" + id].Activate();
            }
        }

        public void RefreshData()
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(this.tblGroupEmployee());
            gridEmployee.DataSource = ds.Tables["tblGROUP"];
        }

        private DataTable tblGroupEmployee()
        {
            DataTable dt = Group.SelectAll().Tables[0].Copy();

            dt.Columns["ID"].Caption = "Mã nhóm";
            dt.Columns["Name"].Caption = "Tên nhóm";
            dt.Columns["Description"].Caption = "Mô tả";
            dt.TableName = "tblGROUP";

            return dt;
        }

        #endregion

        private void FrmGroupEmployeeManager_Load(object sender, EventArgs e)
        {
            this.RefreshData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    string Message = "Bạn có muốn xóa nhóm nhân viên: " + ID;
                    if (MessageBox.Show(Message, "Xóa nhóm nhân viên", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Group.DeleteGroup(Convert.ToInt32(ID));
                    }
                }
            }
            this.RefreshData();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    this.OpenForm(ID, true);
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!ClassLib.IsFormShowed("Group_New"))
            {
                FrmGroupEmployeeInfo obj = new FrmGroupEmployeeInfo(null);
                obj.MdiParent = this.MdiParent;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Group_New"].Activate();
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            gridData.ClearColumnsFilter();
            gridData.ClearGrouping();
            gridData.ClearSorting();
            this.RefreshData();
        }

        private void gridEmployee_DoubleClick(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    this.OpenForm(ID, false);
                }
            }
        }

    }
}