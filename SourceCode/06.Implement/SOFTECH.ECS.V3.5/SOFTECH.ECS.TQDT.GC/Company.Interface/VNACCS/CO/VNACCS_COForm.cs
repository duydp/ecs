﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.CO;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface.VNACCS.CO
{
    public partial class VNACCS_COForm : DevExpress.XtraEditors.XtraForm
    {
        public KDT_VNACC_CO CO = new KDT_VNACC_CO();
        public KDT_VNACC_CO_Detail CODetail = new KDT_VNACC_CO_Detail();
        public List<KDT_VNACC_CO_Detail> listCO = new List<KDT_VNACC_CO_Detail>();
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        string filebase64 = "";
        long filesize = 0;
        public BaseForm f = new BaseForm();
        public VNACCS_COForm()
        {
            InitializeComponent();
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave" :
                    Save();
                    break;
                case "cmdSend" :
                    Send();
                    break;
                case "cmdFeedback" :
                    Feedback();
                    break;
                default:
                    break;
            }
        }
        private void Save()
        {
            if (listCO.Count == 0 || listCO == null)
            {
                f.ShowMessage("Bạn chưa thêm tệp tin chứng từ đính kèm", false);
                xtraTabPage1.Select();
                return;
            }
            getCO();
            CO.InsertUpdate();

        }
        public void getCO()
        {
            CO.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            CO.MaHQ = ctrHaiQuan.Code;
            CO.TKMD_ID = TKMD.ID;
        }

        public void setCO()
        {

        }
        public void BindData()
        {
            dgList.DataSource = listCO;
            try
            {
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Send()
        {
            if (f.ShowMessage("Bạn có muốn khai phụ kiện này đến HQ không ?",true)=="Yes")
            {
                if (CODetail.ID < 0 )
                {
                    f.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    CO = KDT_VNACC_CO.Load(CODetail.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.CO;
                sendXML.master_id = CO.ID;

                if (sendXML.Load())
                {
                    f.ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    CO.GuidStr = Guid.NewGuid().ToString();
                    CO_VNACCS coVNACCS = new CO_VNACCS();
                    coVNACCS = Mapper_V4.ToDataTransferCO(CO, CODetail, TKMD, GlobalSettings.TEN_DON_VI);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = CO.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(CO.MaHQ)),
                              Identity = CO.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = coVNACCS.Issuer,
                              Function = DeclarationFunction.KHAI_BAO,
                              Reference = CO.GuidStr,
                          },
                          coVNACCS
                        );
                    CO.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(CO.ID, MessageTitle.KhaiBaoBSContainer);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        btnLuu.Enabled = false;
                        btnDelete.Enabled = false;
                        sendXML.LoaiHS = LoaiKhaiBao.CO;
                        sendXML.master_id = CO.ID;
                        sendXML.msg = msgSend.ToString();
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        CO.Update();
                        Feedback();
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        f.ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }
        private void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = CO.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.CO,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.CO,
                };
                    subjectBase.Type = DeclarationIssuer.CO;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = CO.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(CO.MaHQ.Trim())),
                                                  Identity = CO.MaHQ
                                              }, subjectBase, null);
                if (CO.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                if (isFeedBack)
                {
                    isFeedBack = f.ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }        
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            OpenFileDialog OpenFileDialog = new OpenFileDialog();

            OpenFileDialog.FileName  = "";
            OpenFileDialog.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.bmp;*.tiff)|*.jpg;*.jpeg;*.gif;*.bmp;*.tiff|"
                                    + "Text files (*.txt;*.xml;*xsl)|*.txt;*.xml;*xsl|"
                                    + "Application File (*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;)|*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;";

            OpenFileDialog.Multiselect = false;
            System.IO.FileInfo fin = new System.IO.FileInfo(OpenFileDialog.FileName);
           if  (fin.Extension.ToUpper() != ".jpg".ToUpper()
             && fin.Extension.ToUpper() != ".jpeg".ToUpper()
             && fin.Extension.ToUpper() != ".gif".ToUpper()
             && fin.Extension.ToUpper() != ".tiff".ToUpper()
             && fin.Extension.ToUpper() != ".txt".ToUpper()
             && fin.Extension.ToUpper() != ".xml".ToUpper()
             && fin.Extension.ToUpper() != ".xsl".ToUpper()
             && fin.Extension.ToUpper() != ".csv".ToUpper()
             && fin.Extension.ToUpper() != ".doc".ToUpper()
             && fin.Extension.ToUpper() != ".mdb".ToUpper()
             && fin.Extension.ToUpper() != ".pdf".ToUpper()
             && fin.Extension.ToUpper() != ".ppt".ToUpper()
             && fin.Extension.ToUpper() != ".xls".ToUpper())
              {
                f.ShowMessage("Tệp tin " + fin.Name + "Không đúng định dạng tiếp nhận của Hải Quan", false);
              }
           else
           {
               System.IO.FileStream fs = new System.IO.FileStream(OpenFileDialog.FileName,System.IO.FileMode.Open,System.IO.FileAccess.Read);
               long size=0;
               size = fs.Length;
               byte[] data = new byte[fs.Length];
               fs.Read(data, 0, data.Length);
               filebase64 = System.Convert.ToBase64String(data);
               filesize = fs.Length;
               KDT_VNACC_CO_Detail coDetail = new KDT_VNACC_CO_Detail();
               coDetail.FileName = fin.Name;
               coDetail.FileSize = filesize;
               coDetail.NoiDung = data;
               dgList.DataSource = coDetail;
               try
               {
                   dgList.Refetch();
               }
               catch (Exception ex)
               {
                   dgList.Refresh();
                   Logger.LocalLogger.Instance().WriteMessage(ex);
               }
           }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string path = Application.StartupPath + "\\Temp";

                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                if (dgList.GetRow() == null)
                {
                    f.ShowMessage("Bạn chưa chọn File để xem.", false);
                    return;
                }

                KDT_VNACC_CO_Detail fileData = (KDT_VNACC_CO_Detail)dgList.GetRow().DataRow;
                string fileName = path + "\\" + fileData.FileName;
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }

                System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                fs.Write(fileData.NoiDung, 0, fileData.NoiDung.Length);
                System.Diagnostics.Process.Start(fileName);

            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0)
            {
                return;
            }
            if (items.Count < 0)
            {
                return;
            }
            if (f.ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    KDT_VNACC_CO_Detail coDetail = new KDT_VNACC_CO_Detail();
                    coDetail = (KDT_VNACC_CO_Detail)dgList.GetRow(i).DataRow;
                    if (coDetail == null)
                    {
                        continue;
                    }
                    if (coDetail.ID > 0)
                    {
                        coDetail.Delete();
                    }
                    listCO.Remove(coDetail);
                }
            }
            BindData();
        }

        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.COSendHandler(CO, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
    }
}