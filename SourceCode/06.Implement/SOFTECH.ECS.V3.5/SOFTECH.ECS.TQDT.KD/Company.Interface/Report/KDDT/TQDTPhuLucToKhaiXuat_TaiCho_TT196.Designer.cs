﻿namespace Company.Interface.Report
{
    partial class TQDTPhuLucToKhaiXuat_TaiCho_TT196
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TQDTPhuLucToKhaiXuat_TaiCho_TT196));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcSoPhuLuc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcSoToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xtcChiCucHaiQuan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcChiCucCuaKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcNgayDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcLoaiHinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_Sohieu = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_HQVN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.DetailHangHoa = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xtcHang_STT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcHang_MoTa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcHang_MaSo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcHang_XuatXu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcHang_LuongHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcHang_DVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcHang_DonGiaNT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcHang_TriGiaNT = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter1 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcHang_Cong = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailThue = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xtcThueXK_STT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThueXK_TriGiaTinhThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThueXK_ThueSuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThueXK_Thue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThuKhac_TriGiaTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThuKhac_TyLe = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThuKhac_SoTien = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader2 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter2 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThueXK_Cong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcThuKhac_Cong = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailLuongHang = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xtcLuongHang_STT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcLuonghang_SoHieu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcLuongHang_SoLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcLuongHang_TrongLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcLuongHang_DiaDiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter3 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtcLuongHang_Cong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader3 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtratablecell = new DevExpress.XtraReports.UI.XRTableCell();
            this.xtc = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayThangNam = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2,
            this.xrLabel1,
            this.lbl_Sohieu,
            this.lbl_HQVN,
            this.xrLabel2,
            this.winControlContainer1});
            this.PageHeader.HeightF = 155F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(33F, 95F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7,
            this.xrTableRow10,
            this.xrTableRow8});
            this.xrTable2.SizeF = new System.Drawing.SizeF(745F, 60F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCell21,
            this.xtcSoPhuLuc,
            this.xrTableCell4,
            this.xtcSoToKhai});
            this.xrTableRow7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.StylePriority.UseFont = false;
            this.xrTableRow7.Weight = 0.32168186423505574;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.Text = "Chi cục Hải quan đăng ký tờ khai:";
            this.xrTableCell22.Weight = 0.49665495837759238;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseBorders = false;
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.Text = "Phụ lục số:";
            this.xrTableCell21.Weight = 0.13134348330043713;
            // 
            // xtcSoPhuLuc
            // 
            this.xtcSoPhuLuc.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xtcSoPhuLuc.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtcSoPhuLuc.Name = "xtcSoPhuLuc";
            this.xtcSoPhuLuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xtcSoPhuLuc.StylePriority.UseBorders = false;
            this.xtcSoPhuLuc.StylePriority.UseFont = false;
            this.xtcSoPhuLuc.StylePriority.UsePadding = false;
            this.xtcSoPhuLuc.StylePriority.UseTextAlignment = false;
            this.xtcSoPhuLuc.Text = "000";
            this.xtcSoPhuLuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcSoPhuLuc.Weight = 0.15850355134771368;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.Text = "Số tờ khai:";
            this.xrTableCell4.Weight = 0.076340439373187352;
            // 
            // xtcSoToKhai
            // 
            this.xtcSoToKhai.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xtcSoToKhai.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtcSoToKhai.Name = "xtcSoToKhai";
            this.xtcSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xtcSoToKhai.StylePriority.UseBorders = false;
            this.xtcSoToKhai.StylePriority.UseFont = false;
            this.xtcSoToKhai.StylePriority.UsePadding = false;
            this.xtcSoToKhai.StylePriority.UseTextAlignment = false;
            this.xtcSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcSoToKhai.Weight = 0.13715756760106959;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xtcChiCucHaiQuan,
            this.xrTableCell20,
            this.xrTableCell24});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.StylePriority.UseBorders = false;
            this.xrTableRow10.Weight = 0.19300911854103336;
            // 
            // xtcChiCucHaiQuan
            // 
            this.xtcChiCucHaiQuan.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xtcChiCucHaiQuan.Name = "xtcChiCucHaiQuan";
            this.xtcChiCucHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xtcChiCucHaiQuan.StylePriority.UseBorders = false;
            this.xtcChiCucHaiQuan.StylePriority.UsePadding = false;
            this.xtcChiCucHaiQuan.Weight = 0.49665492022454005;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.Weight = 0.28984700111561684;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.Weight = 0.21349807865984311;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.xtcChiCucCuaKhau,
            this.xrTableCell29,
            this.xtcNgayDangKy,
            this.xrTableCell10,
            this.xtcLoaiHinh});
            this.xrTableRow8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.StylePriority.UseFont = false;
            this.xrTableRow8.Weight = 0.25734549138804452;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell36.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseBorders = false;
            this.xrTableCell36.StylePriority.UseFont = false;
            this.xrTableCell36.Text = "Chi cục Hải quan cửa khẩu xuất:";
            this.xrTableCell36.Weight = 0.2247188822581502;
            // 
            // xtcChiCucCuaKhau
            // 
            this.xtcChiCucCuaKhau.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xtcChiCucCuaKhau.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtcChiCucCuaKhau.Name = "xtcChiCucCuaKhau";
            this.xtcChiCucCuaKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xtcChiCucCuaKhau.StylePriority.UseBorders = false;
            this.xtcChiCucCuaKhau.StylePriority.UseFont = false;
            this.xtcChiCucCuaKhau.StylePriority.UsePadding = false;
            this.xtcChiCucCuaKhau.Weight = 0.27193603796638988;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.Text = "Ngày, giờ đăng ký:";
            this.xrTableCell29.Weight = 0.12865895769510016;
            // 
            // xtcNgayDangKy
            // 
            this.xtcNgayDangKy.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xtcNgayDangKy.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtcNgayDangKy.Name = "xtcNgayDangKy";
            this.xtcNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xtcNgayDangKy.StylePriority.UseBorders = false;
            this.xtcNgayDangKy.StylePriority.UseFont = false;
            this.xtcNgayDangKy.StylePriority.UsePadding = false;
            this.xtcNgayDangKy.Weight = 0.16118804342051674;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell10.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.Text = "Loại hình:";
            this.xrTableCell10.Weight = 0.10855527615944467;
            // 
            // xtcLoaiHinh
            // 
            this.xtcLoaiHinh.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xtcLoaiHinh.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xtcLoaiHinh.Name = "xtcLoaiHinh";
            this.xtcLoaiHinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xtcLoaiHinh.StylePriority.UseBorders = false;
            this.xtcLoaiHinh.StylePriority.UseFont = false;
            this.xtcLoaiHinh.StylePriority.UsePadding = false;
            this.xtcLoaiHinh.Weight = 0.10494280250039845;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(167F, 50F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(408F, 25F);
            this.xrLabel1.Text = "PHỤ LỤC TỜ KHAI HẢI QUAN ĐIỆN TỬ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lbl_Sohieu
            // 
            this.lbl_Sohieu.LocationFloat = new DevExpress.Utils.PointFloat(625F, 75F);
            this.lbl_Sohieu.Name = "lbl_Sohieu";
            this.lbl_Sohieu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_Sohieu.SizeF = new System.Drawing.SizeF(150F, 20F);
            this.lbl_Sohieu.StylePriority.UseTextAlignment = false;
            this.lbl_Sohieu.Text = "HQ/2012-PLTKXK-TC";
            this.lbl_Sohieu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl_HQVN
            // 
            this.lbl_HQVN.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbl_HQVN.LocationFloat = new DevExpress.Utils.PointFloat(17F, 25F);
            this.lbl_HQVN.Name = "lbl_HQVN";
            this.lbl_HQVN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_HQVN.SizeF = new System.Drawing.SizeF(183F, 23F);
            this.lbl_HQVN.StylePriority.UseFont = false;
            this.lbl_HQVN.Text = "HẢI QUAN VIỆT NAM";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(317F, 75F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(142F, 20F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Xuất khẩu tại chỗ";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.LocationFloat = new DevExpress.Utils.PointFloat(617F, 50F);
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.SizeF = new System.Drawing.SizeF(175F, 25F);
            this.winControlContainer1.WinControl = this.label1;
            // 
            // label1
            // 
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 24);
            this.label1.TabIndex = 0;
            // 
            // DetailHangHoa
            // 
            this.DetailHangHoa.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportFooter1,
            this.ReportHeader1});
            this.DetailHangHoa.Level = 0;
            this.DetailHangHoa.Name = "DetailHangHoa";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.Detail1.HeightF = 25F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable4
            // 
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(745F, 25F);
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xtcHang_STT,
            this.xtcHang_MoTa,
            this.xtcHang_MaSo,
            this.xtcHang_XuatXu,
            this.xtcHang_LuongHang,
            this.xtcHang_DVT,
            this.xtcHang_DonGiaNT,
            this.xtcHang_TriGiaNT});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 1;
            // 
            // xtcHang_STT
            // 
            this.xtcHang_STT.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcHang_STT.Name = "xtcHang_STT";
            this.xtcHang_STT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xtcHang_STT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcHang_STT.Weight = 0.052770448548812666;
            // 
            // xtcHang_MoTa
            // 
            this.xtcHang_MoTa.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcHang_MoTa.Multiline = true;
            this.xtcHang_MoTa.Name = "xtcHang_MoTa";
            this.xtcHang_MoTa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xtcHang_MoTa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xtcHang_MoTa.Weight = 0.28746392977471819;
            // 
            // xtcHang_MaSo
            // 
            this.xtcHang_MaSo.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcHang_MaSo.Name = "xtcHang_MaSo";
            this.xtcHang_MaSo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xtcHang_MaSo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcHang_MaSo.Weight = 0.088525516135572163;
            // 
            // xtcHang_XuatXu
            // 
            this.xtcHang_XuatXu.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcHang_XuatXu.Multiline = true;
            this.xtcHang_XuatXu.Name = "xtcHang_XuatXu";
            this.xtcHang_XuatXu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xtcHang_XuatXu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcHang_XuatXu.Weight = 0.088390501319261225;
            // 
            // xtcHang_LuongHang
            // 
            this.xtcHang_LuongHang.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcHang_LuongHang.Multiline = true;
            this.xtcHang_LuongHang.Name = "xtcHang_LuongHang";
            this.xtcHang_LuongHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xtcHang_LuongHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcHang_LuongHang.Weight = 0.12137203166226918;
            this.xtcHang_LuongHang.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcHang_LuongHang_BeforePrint);
            // 
            // xtcHang_DVT
            // 
            this.xtcHang_DVT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcHang_DVT.Name = "xtcHang_DVT";
            this.xtcHang_DVT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xtcHang_DVT.StylePriority.UseTextAlignment = false;
            this.xtcHang_DVT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcHang_DVT.Weight = 0.097625329815303433;
            this.xtcHang_DVT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcHang_DVT_BeforePrint);
            // 
            // xtcHang_DonGiaNT
            // 
            this.xtcHang_DonGiaNT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcHang_DonGiaNT.Name = "xtcHang_DonGiaNT";
            this.xtcHang_DonGiaNT.StylePriority.UseBorders = false;
            this.xtcHang_DonGiaNT.StylePriority.UseTextAlignment = false;
            this.xtcHang_DonGiaNT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcHang_DonGiaNT.Weight = 0.11081794195250665;
            this.xtcHang_DonGiaNT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcHang_LuongHang_BeforePrint);
            // 
            // xtcHang_TriGiaNT
            // 
            this.xtcHang_TriGiaNT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcHang_TriGiaNT.Name = "xtcHang_TriGiaNT";
            this.xtcHang_TriGiaNT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xtcHang_TriGiaNT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcHang_TriGiaNT.Weight = 0.15303430079155678;
            this.xtcHang_TriGiaNT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcHang_LuongHang_BeforePrint);
            // 
            // ReportFooter1
            // 
            this.ReportFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.ReportFooter1.HeightF = 25F;
            this.ReportFooter1.Name = "ReportFooter1";
            // 
            // xrTable7
            // 
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable7.SizeF = new System.Drawing.SizeF(745F, 25F);
            this.xrTable7.StylePriority.UseFont = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xtcHang_Cong});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow5.Weight = 1;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell34.StylePriority.UseBorders = false;
            this.xrTableCell34.Text = "Cộng:";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell34.Weight = 0.84696578113482757;
            // 
            // xtcHang_Cong
            // 
            this.xtcHang_Cong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcHang_Cong.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xtcHang_Cong.Name = "xtcHang_Cong";
            this.xtcHang_Cong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xtcHang_Cong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcHang_Cong.Weight = 0.15303421886517249;
            // 
            // ReportHeader1
            // 
            this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11});
            this.ReportHeader1.HeightF = 30F;
            this.ReportHeader1.Name = "ReportHeader1";
            // 
            // xrTable11
            // 
            this.xrTable11.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable11.SizeF = new System.Drawing.SizeF(745F, 30F);
            this.xrTable11.StylePriority.UseFont = false;
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow14.StylePriority.UseTextAlignment = false;
            this.xrTableRow14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow14.Weight = 0.51489361702127678;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell61.Text = "Số TT";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell61.Weight = 0.052770448548812666;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell62.Multiline = true;
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell62.Text = "15.Mô tả hàng hóa";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell62.Weight = 0.28746392977471819;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell63.Multiline = true;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell63.Text = "16.Mã số hàng hóa";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell63.Weight = 0.088525516135572163;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseBorders = false;
            this.xrTableCell64.StylePriority.UseTextAlignment = false;
            this.xrTableCell64.Text = "17.Xuất xứ";
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell64.Weight = 0.088390501319261211;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell65.Multiline = true;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell65.Text = "18.Lượng hàng";
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell65.Weight = 0.12137203166226913;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell66.Multiline = true;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell66.Text = "19.Đơn vị tính";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell66.Weight = 0.097625329815303447;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell67.Text = "20.Đơn giá nguyên tệ";
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell67.Weight = 0.11081794195250659;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell68.Text = "21.Trị giá nguyên tệ";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell68.Weight = 0.15303430079155675;
            // 
            // DetailThue
            // 
            this.DetailThue.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.ReportHeader2,
            this.ReportFooter2});
            this.DetailThue.Level = 1;
            this.DetailThue.Name = "DetailThue";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.Detail2.HeightF = 25F;
            this.Detail2.Name = "Detail2";
            // 
            // xrTable5
            // 
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable5.SizeF = new System.Drawing.SizeF(745F, 25F);
            this.xrTable5.StylePriority.UseFont = false;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xtcThueXK_STT,
            this.xtcThueXK_TriGiaTinhThue,
            this.xtcThueXK_ThueSuat,
            this.xtcThueXK_Thue,
            this.xtcThuKhac_TriGiaTT,
            this.xtcThuKhac_TyLe,
            this.xtcThuKhac_SoTien});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.StylePriority.UseBorders = false;
            this.xrTableRow9.Weight = 1.32;
            // 
            // xtcThueXK_STT
            // 
            this.xtcThueXK_STT.Name = "xtcThueXK_STT";
            this.xtcThueXK_STT.StylePriority.UseTextAlignment = false;
            this.xtcThueXK_STT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xtcThueXK_STT.Weight = 0.17;
            // 
            // xtcThueXK_TriGiaTinhThue
            // 
            this.xtcThueXK_TriGiaTinhThue.Name = "xtcThueXK_TriGiaTinhThue";
            this.xtcThueXK_TriGiaTinhThue.StylePriority.UseTextAlignment = false;
            this.xtcThueXK_TriGiaTinhThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcThueXK_TriGiaTinhThue.Weight = 0.59121372031662267;
            this.xtcThueXK_TriGiaTinhThue.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcThueXK_TriGiaTinhThue_BeforePrint);
            // 
            // xtcThueXK_ThueSuat
            // 
            this.xtcThueXK_ThueSuat.Name = "xtcThueXK_ThueSuat";
            this.xtcThueXK_ThueSuat.StylePriority.UseTextAlignment = false;
            this.xtcThueXK_ThueSuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcThueXK_ThueSuat.Weight = 0.46437994722955145;
            this.xtcThueXK_ThueSuat.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcHang_LuongHang_BeforePrint);
            // 
            // xtcThueXK_Thue
            // 
            this.xtcThueXK_Thue.Name = "xtcThueXK_Thue";
            this.xtcThueXK_Thue.StylePriority.UseTextAlignment = false;
            this.xtcThueXK_Thue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcThueXK_Thue.Weight = 0.4287598944591029;
            this.xtcThueXK_Thue.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcThueXK_Thue_BeforePrint);
            // 
            // xtcThuKhac_TriGiaTT
            // 
            this.xtcThuKhac_TriGiaTT.Name = "xtcThuKhac_TriGiaTT";
            this.xtcThuKhac_TriGiaTT.StylePriority.UseTextAlignment = false;
            this.xtcThuKhac_TriGiaTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcThuKhac_TriGiaTT.Weight = 0.49604221635883905;
            this.xtcThuKhac_TriGiaTT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcThuKhac_TriGiaTT_BeforePrint);
            // 
            // xtcThuKhac_TyLe
            // 
            this.xtcThuKhac_TyLe.Name = "xtcThuKhac_TyLe";
            this.xtcThuKhac_TyLe.StylePriority.UseTextAlignment = false;
            this.xtcThuKhac_TyLe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcThuKhac_TyLe.Weight = 0.3970976253298153;
            this.xtcThuKhac_TyLe.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcHang_LuongHang_BeforePrint);
            // 
            // xtcThuKhac_SoTien
            // 
            this.xtcThuKhac_SoTien.Name = "xtcThuKhac_SoTien";
            this.xtcThuKhac_SoTien.StylePriority.UseTextAlignment = false;
            this.xtcThuKhac_SoTien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcThuKhac_SoTien.Weight = 0.4525065963060686;
            this.xtcThuKhac_SoTien.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcThuKhac_SoTien_BeforePrint);
            // 
            // ReportHeader2
            // 
            this.ReportHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.ReportHeader2.HeightF = 50F;
            this.ReportHeader2.Name = "ReportHeader2";
            // 
            // xrTable3
            // 
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3,
            this.xrTableRow15});
            this.xrTable3.SizeF = new System.Drawing.SizeF(745F, 50F);
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell6});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseBorders = false;
            this.xrTableRow3.StylePriority.UseTextAlignment = false;
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Số";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.17;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "22.Thuế xuất khẩu";
            this.xrTableCell2.Weight = 1.4843534371702196;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "23.Thu khác";
            this.xrTableCell6.Weight = 1.3456465628297805;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell69,
            this.xrTableCell70});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "TT";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell7.Weight = 0.17;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "Trị giá tính thuế";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.59121372031662267;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "Thuế xuất (%)";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell16.Weight = 0.46437994722955145;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "Tiền thuế";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell17.Weight = 0.4287598944591029;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "Trị giá tính thu khác";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell18.Weight = 0.49604221635883905;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseTextAlignment = false;
            this.xrTableCell69.Text = "Tỷ lệ (%)";
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell69.Weight = 0.3970976253298153;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseTextAlignment = false;
            this.xrTableCell70.Text = "Số tiền";
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell70.Weight = 0.4525065963060686;
            // 
            // ReportFooter2
            // 
            this.ReportFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.ReportFooter2.HeightF = 25F;
            this.ReportFooter2.Name = "ReportFooter2";
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(745F, 25F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xtcThueXK_Cong,
            this.xrTableCell11,
            this.xtcThuKhac_Cong});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "Cộng:";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell8.Weight = 0.40853119412577366;
            // 
            // xtcThueXK_Cong
            // 
            this.xtcThueXK_Cong.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcThueXK_Cong.Name = "xtcThueXK_Cong";
            this.xtcThueXK_Cong.StylePriority.UseBorders = false;
            this.xtcThueXK_Cong.StylePriority.UseTextAlignment = false;
            this.xtcThueXK_Cong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcThueXK_Cong.Weight = 0.14291996118993705;
            this.xtcThueXK_Cong.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcHang_LuongHang_BeforePrint);
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.Text = "Cộng:";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 0.29771342756382119;
            // 
            // xtcThuKhac_Cong
            // 
            this.xtcThuKhac_Cong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcThuKhac_Cong.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xtcThuKhac_Cong.Name = "xtcThuKhac_Cong";
            this.xtcThuKhac_Cong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xtcThuKhac_Cong.StylePriority.UseTextAlignment = false;
            this.xtcThuKhac_Cong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xtcThuKhac_Cong.Weight = 0.15083541712046822;
            // 
            // DetailLuongHang
            // 
            this.DetailLuongHang.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.ReportFooter3,
            this.ReportHeader3});
            this.DetailLuongHang.Level = 2;
            this.DetailLuongHang.Name = "DetailLuongHang";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9});
            this.Detail3.HeightF = 25F;
            this.Detail3.Name = "Detail3";
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable9.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable9.SizeF = new System.Drawing.SizeF(745F, 25F);
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.StylePriority.UseFont = false;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xtcLuongHang_STT,
            this.xtcLuonghang_SoHieu,
            this.xtcLuongHang_SoLuong,
            this.xtcLuongHang_TrongLuong,
            this.xtcLuongHang_DiaDiem});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.StylePriority.UseBorders = false;
            this.xrTableRow13.StylePriority.UseTextAlignment = false;
            this.xrTableRow13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow13.Weight = 1.32;
            // 
            // xtcLuongHang_STT
            // 
            this.xtcLuongHang_STT.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcLuongHang_STT.Multiline = true;
            this.xtcLuongHang_STT.Name = "xtcLuongHang_STT";
            this.xtcLuongHang_STT.StylePriority.UseBorders = false;
            this.xtcLuongHang_STT.StylePriority.UseTextAlignment = false;
            this.xtcLuongHang_STT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xtcLuongHang_STT.Weight = 0.16754617414248019;
            this.xtcLuongHang_STT.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcLuongHang_STT_BeforePrint);
            // 
            // xtcLuonghang_SoHieu
            // 
            this.xtcLuonghang_SoHieu.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcLuonghang_SoHieu.Name = "xtcLuonghang_SoHieu";
            this.xtcLuonghang_SoHieu.StylePriority.UseBorders = false;
            this.xtcLuonghang_SoHieu.Weight = 0.662269129287599;
            // 
            // xtcLuongHang_SoLuong
            // 
            this.xtcLuongHang_SoLuong.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcLuongHang_SoLuong.Name = "xtcLuongHang_SoLuong";
            this.xtcLuongHang_SoLuong.StylePriority.UseBorders = false;
            this.xtcLuongHang_SoLuong.Weight = 0.76497140125019891;
            this.xtcLuongHang_SoLuong.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcLuongHang_SoLuong_BeforePrint);
            // 
            // xtcLuongHang_TrongLuong
            // 
            this.xtcLuongHang_TrongLuong.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcLuongHang_TrongLuong.Name = "xtcLuongHang_TrongLuong";
            this.xtcLuongHang_TrongLuong.StylePriority.UseBorders = false;
            this.xtcLuongHang_TrongLuong.Weight = 0.72400524162844648;
            this.xtcLuongHang_TrongLuong.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcLuongHang_TrongLuong_BeforePrint);
            // 
            // xtcLuongHang_DiaDiem
            // 
            this.xtcLuongHang_DiaDiem.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcLuongHang_DiaDiem.Name = "xtcLuongHang_DiaDiem";
            this.xtcLuongHang_DiaDiem.StylePriority.UseBorders = false;
            this.xtcLuongHang_DiaDiem.Weight = 0.68120805369127513;
            // 
            // ReportFooter3
            // 
            this.ReportFooter3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10});
            this.ReportFooter3.HeightF = 25F;
            this.ReportFooter3.Name = "ReportFooter3";
            // 
            // xrTable10
            // 
            this.xrTable10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable10.SizeF = new System.Drawing.SizeF(745F, 25F);
            this.xrTable10.StylePriority.UseFont = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xtcLuongHang_Cong,
            this.xrTableCell60});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.StylePriority.UseBorders = false;
            this.xrTableRow12.StylePriority.UseTextAlignment = false;
            this.xrTableRow12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow12.Weight = 1.32;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Multiline = true;
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseTextAlignment = false;
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell45.Weight = 0.16754617414248019;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Weight = 0.662269129287599;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Weight = 0.76497140125019891;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell59.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.StylePriority.UseFont = false;
            this.xrTableCell59.StylePriority.UseTextAlignment = false;
            this.xrTableCell59.Text = "Cộng:";
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell59.Weight = 0.24865683271059483;
            // 
            // xtcLuongHang_Cong
            // 
            this.xtcLuongHang_Cong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xtcLuongHang_Cong.Name = "xtcLuongHang_Cong";
            this.xtcLuongHang_Cong.StylePriority.UseBorders = false;
            this.xtcLuongHang_Cong.Weight = 0.47534842972369462;
            this.xtcLuongHang_Cong.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xtcLuongHang_Cong_BeforePrint);
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Weight = 0.68120803288543208;
            // 
            // ReportHeader3
            // 
            this.ReportHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.ReportHeader3.HeightF = 52F;
            this.ReportHeader3.Name = "ReportHeader3";
            // 
            // xrTable6
            // 
            this.xrTable6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(33F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(745F, 52F);
            this.xrTable6.StylePriority.UseFont = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseBorders = false;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "25.Lượng hàng, số hiệu container";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 3;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell9,
            this.xrTableCell13,
            this.xtratablecell,
            this.xtc});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.StylePriority.UseBorders = false;
            this.xrTableRow6.StylePriority.UseTextAlignment = false;
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow6.Weight = 1.3226666666666669;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Multiline = true;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "Số\r\n TT";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell5.Weight = 0.16754617414248019;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "a.Số hiệu container";
            this.xrTableCell9.Weight = 0.662269129287599;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "b.Số lượng kiện trong container";
            this.xrTableCell13.Weight = 0.76497140125019891;
            // 
            // xtratablecell
            // 
            this.xtratablecell.Name = "xtratablecell";
            this.xtratablecell.Text = "c.Nới đóng hàng vào container";
            this.xtratablecell.Weight = 0.72400524162844648;
            // 
            // xtc
            // 
            this.xtc.Name = "xtc";
            this.xtc.Text = "d.Địa điểm đóng hàng";
            this.xtc.Weight = 0.68120805369127513;
            // 
            // PageFooter
            // 
            this.PageFooter.Name = "PageFooter";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(533F, 0F);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(217F, 33F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "27.Tôi xin cam đoan, chịu trách nhiệm trước pháp luật về nội dung khai trên tờ kh" +
                "ai";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(517F, 55F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(233F, 20F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "(Người khai ký, ghi rõ họ tên, đóng dấu)";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayThangNam
            // 
            this.lblNgayThangNam.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblNgayThangNam.LocationFloat = new DevExpress.Utils.PointFloat(542F, 35F);
            this.lblNgayThangNam.Name = "lblNgayThangNam";
            this.lblNgayThangNam.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayThangNam.SizeF = new System.Drawing.SizeF(175F, 17F);
            this.lblNgayThangNam.StylePriority.UseFont = false;
            this.lblNgayThangNam.StylePriority.UseTextAlignment = false;
            this.lblNgayThangNam.Text = "Ngày     tháng     năm";
            this.lblNgayThangNam.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4});
            this.DetailReport.Level = 3;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail4
            // 
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel5,
            this.xrLabel6,
            this.lblNgayThangNam});
            this.Detail4.HeightF = 75F;
            this.Detail4.Name = "Detail4";
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 4F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 18F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // TQDTPhuLucToKhaiXuat_TaiCho_TT196
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.DetailHangHoa,
            this.DetailThue,
            this.DetailLuongHang,
            this.PageFooter,
            this.DetailReport,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Margins = new System.Drawing.Printing.Margins(5, 15, 4, 18);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRLabel lbl_Sohieu;
        private DevExpress.XtraReports.UI.XRLabel lbl_HQVN;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xtcSoToKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xtcChiCucCuaKhau;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xtcLoaiHinh;
        private DevExpress.XtraReports.UI.DetailReportBand DetailHangHoa;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xtcHang_STT;
        private DevExpress.XtraReports.UI.XRTableCell xtcHang_MoTa;
        private DevExpress.XtraReports.UI.XRTableCell xtcHang_MaSo;
        private DevExpress.XtraReports.UI.XRTableCell xtcHang_XuatXu;
        private DevExpress.XtraReports.UI.XRTableCell xtcHang_LuongHang;
        private DevExpress.XtraReports.UI.XRTableCell xtcHang_DVT;
        private DevExpress.XtraReports.UI.XRTableCell xtcHang_DonGiaNT;
        private DevExpress.XtraReports.UI.XRTableCell xtcHang_TriGiaNT;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter1;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xtcHang_Cong;
        private DevExpress.XtraReports.UI.DetailReportBand DetailThue;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xtcThueXK_STT;
        private DevExpress.XtraReports.UI.XRTableCell xtcThueXK_TriGiaTinhThue;
        private DevExpress.XtraReports.UI.XRTableCell xtcThueXK_ThueSuat;
        private DevExpress.XtraReports.UI.XRTableCell xtcThueXK_Thue;
        private DevExpress.XtraReports.UI.XRTableCell xtcThuKhac_TriGiaTT;
        private DevExpress.XtraReports.UI.XRTableCell xtcThuKhac_TyLe;
        private DevExpress.XtraReports.UI.XRTableCell xtcThuKhac_SoTien;
        private DevExpress.XtraReports.UI.DetailReportBand DetailLuongHang;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xtcLuongHang_STT;
        private DevExpress.XtraReports.UI.XRTableCell xtcLuonghang_SoHieu;
        private DevExpress.XtraReports.UI.XRTableCell xtcLuongHang_SoLuong;
        private DevExpress.XtraReports.UI.XRTableCell xtcLuongHang_TrongLuong;
        private DevExpress.XtraReports.UI.XRTableCell xtcLuongHang_DiaDiem;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter3;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader2;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter2;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xtcThuKhac_Cong;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader3;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xtratablecell;
        private DevExpress.XtraReports.UI.XRTableCell xtc;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel lblNgayThangNam;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xtcSoPhuLuc;
        private DevExpress.XtraReports.UI.XRTableCell xtcNgayDangKy;
        private DevExpress.XtraReports.UI.XRTableCell xtcThueXK_Cong;
        private DevExpress.XtraReports.UI.XRTableCell xtcLuongHang_Cong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xtcChiCucHaiQuan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
    }
}
