using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.Common
{
	public partial class NguyenPhuLieuBoSung : ICloneable
	{
		#region Properties.
		
		public long HopDong_ID { set; get; }
		public string Ma { set; get; }
		public string NguonCungCap { set; get; }
		public string GhiChu { set; get; }
		public bool TuCungUng { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<NguyenPhuLieuBoSung> ConvertToCollection(IDataReader reader)
		{
			IList<NguyenPhuLieuBoSung> collection = new List<NguyenPhuLieuBoSung>();
			while (reader.Read())
			{
				NguyenPhuLieuBoSung entity = new NguyenPhuLieuBoSung();
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguonCungCap"))) entity.NguonCungCap = reader.GetString(reader.GetOrdinal("NguonCungCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TuCungUng"))) entity.TuCungUng = reader.GetBoolean(reader.GetOrdinal("TuCungUng"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_NguyenPhuLieuBoSung VALUES(@HopDong_ID, @Ma, @NguonCungCap, @GhiChu, @TuCungUng)";
            string update = "UPDATE t_KDT_GC_NguyenPhuLieuBoSung SET HopDong_ID = @HopDong_ID, Ma = @Ma, NguonCungCap = @NguonCungCap, GhiChu = @GhiChu, TuCungUng = @TuCungUng WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_NguyenPhuLieuBoSung WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguonCungCap", SqlDbType.NVarChar, "NguonCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TuCungUng", SqlDbType.Bit, "TuCungUng", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguonCungCap", SqlDbType.NVarChar, "NguonCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TuCungUng", SqlDbType.Bit, "TuCungUng", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_NguyenPhuLieuBoSung VALUES(@HopDong_ID, @Ma, @NguonCungCap, @GhiChu, @TuCungUng)";
            string update = "UPDATE t_KDT_GC_NguyenPhuLieuBoSung SET HopDong_ID = @HopDong_ID, Ma = @Ma, NguonCungCap = @NguonCungCap, GhiChu = @GhiChu, TuCungUng = @TuCungUng WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_NguyenPhuLieuBoSung WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguonCungCap", SqlDbType.NVarChar, "NguonCungCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TuCungUng", SqlDbType.Bit, "TuCungUng", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguonCungCap", SqlDbType.NVarChar, "NguonCungCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TuCungUng", SqlDbType.Bit, "TuCungUng", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static NguyenPhuLieuBoSung Load(long hopDong_ID, string ma)
		{
			const string spName = "[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, ma);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<NguyenPhuLieuBoSung> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<NguyenPhuLieuBoSung> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<NguyenPhuLieuBoSung> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<NguyenPhuLieuBoSung> SelectCollectionBy_HopDong_ID_Ma(long hopDong_ID, string ma)
		{
            IDataReader reader = SelectReaderBy_HopDong_ID_Ma(hopDong_ID, ma);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_HopDong_ID_Ma(long hopDong_ID, string ma)
		{
			const string spName = "[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectBy_HopDong_ID_Ma]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, ma);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_HopDong_ID_Ma(long hopDong_ID, string ma)
		{
			const string spName = "p_KDT_GC_NguyenPhuLieuBoSung_SelectBy_HopDong_ID_Ma";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, ma);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertNguyenPhuLieuBoSung(long hopDong_ID, string ma, string nguonCungCap, string ghiChu, bool tuCungUng)
		{
			NguyenPhuLieuBoSung entity = new NguyenPhuLieuBoSung();	
			entity.HopDong_ID = hopDong_ID;
			entity.Ma = ma;
			entity.NguonCungCap = nguonCungCap;
			entity.GhiChu = ghiChu;
			entity.TuCungUng = tuCungUng;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@NguonCungCap", SqlDbType.NVarChar, NguonCungCap);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@TuCungUng", SqlDbType.Bit, TuCungUng);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<NguyenPhuLieuBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NguyenPhuLieuBoSung item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateNguyenPhuLieuBoSung(long hopDong_ID, string ma, string nguonCungCap, string ghiChu, bool tuCungUng)
		{
			NguyenPhuLieuBoSung entity = new NguyenPhuLieuBoSung();			
			entity.HopDong_ID = hopDong_ID;
			entity.Ma = ma;
			entity.NguonCungCap = nguonCungCap;
			entity.GhiChu = ghiChu;
			entity.TuCungUng = tuCungUng;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_NguyenPhuLieuBoSung_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@NguonCungCap", SqlDbType.NVarChar, NguonCungCap);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@TuCungUng", SqlDbType.Bit, TuCungUng);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<NguyenPhuLieuBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NguyenPhuLieuBoSung item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateNguyenPhuLieuBoSung(long hopDong_ID, string ma, string nguonCungCap, string ghiChu, bool tuCungUng)
		{
			NguyenPhuLieuBoSung entity = new NguyenPhuLieuBoSung();			
			entity.HopDong_ID = hopDong_ID;
			entity.Ma = ma;
			entity.NguonCungCap = nguonCungCap;
			entity.GhiChu = ghiChu;
			entity.TuCungUng = tuCungUng;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@NguonCungCap", SqlDbType.NVarChar, NguonCungCap);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@TuCungUng", SqlDbType.Bit, TuCungUng);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<NguyenPhuLieuBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NguyenPhuLieuBoSung item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteNguyenPhuLieuBoSung(long hopDong_ID, string ma)
		{
			NguyenPhuLieuBoSung entity = new NguyenPhuLieuBoSung();
			entity.HopDong_ID = hopDong_ID;
			entity.Ma = ma;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_HopDong_ID_Ma(long hopDong_ID, string ma)
		{
			const string spName = "[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_DeleteBy_HopDong_ID_Ma]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, ma);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_NguyenPhuLieuBoSung_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<NguyenPhuLieuBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NguyenPhuLieuBoSung item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}