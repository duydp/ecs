﻿using System;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using Company.BLL.KDT;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.Interface.KDT.GC;

namespace Company.Interface.KDT.SXXK
{
    public partial class SanPhamManageForm_CX : BaseForm
    {
        /// <summary>
        /// Dùng cho kết quả tìm kiếm.
        /// </summary>
        private SanPhamDangKyCollection spDangKyCollection = new SanPhamDangKyCollection();
        private SanPhamDangKyCollection tmpCollection = new SanPhamDangKyCollection();
        /// <summary>
        /// Thông tin sản phẩm đang dược chọn.
        /// </summary>
        private readonly SanPhamDangKy currentSPDangKy = new SanPhamDangKy();
        private SanPhamDangKy spdk = new SanPhamDangKy();
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;

        public SanPhamManageForm_CX()
        {
            InitializeComponent();

        }

        //-----------------------------------------------------------------------------------------
        private void SanPhamManageForm_CX_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                cbStatus.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            setCommandStatus();
        }

        //-----------------------------------------------------------------------------------------

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtMaSP.Text))
            {
                this.search();
            }
            else
            {
                searchSanPham(txtMaSP.Text.ToString());
            }
            setCommandStatus();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
            where += string.Format(" AND MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoTiepNhan like '%" + txtSoTiepNhan.Value + "%'";
            }

            if (Convert.ToInt32(cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                if (txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
                }
            }
            if (ckbTimKiem.Checked)
            {
                DateTime fromDate = clcTuNgay.Value;
                DateTime toDate = ucCalendar1.Value;
                if (toDate.Year <= 1900) toDate = DateTime.Now;
                toDate = toDate.AddDays(1);
                where = where + " AND (NgayTiepNhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
            }
            where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;

            // Thực hiện tìm kiếm.            
            this.spDangKyCollection = SanPhamDangKy.SelectCollectionDynamic(where, "ID desc");
            dgList.DataSource = this.spDangKyCollection;

            this.setCommandStatus();

            this.currentSPDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
        }
        private void searchSanPham(string masanpham)
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.
                string where = " Where SP.Ma like '%" + masanpham + "%'";
                where += " AND MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND MaHaiQuan = '{0}'", donViHaiQuanNewControl1.Ma);

                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoTiepNhan like '%" + txtSoTiepNhan.Value + "%'";
                }

                if (Convert.ToInt32(cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    if (txtNamTiepNhan.TextLength > 0)
                    {
                        where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
                    }
                }
                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (NgayTiepNhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;

                // Thực hiện tìm kiếm.            
                this.spDangKyCollection = SanPhamDangKy.SelectCollectionSanPham(where, "ID desc");
                dgList.DataSource = this.spDangKyCollection;
                try
                {
                    this.tmpCollection = SanPhamDangKy.SelectCollectionDynamicAll(where, "ID desc");
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                this.currentSPDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        //-----------------------------------------------------------------------------------------

        private void setCommandStatus()
        {
            DongBoDuLieu.Enabled = InheritableBoolean.True;
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_DUYET || Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_HUY)
            {
                InheritableBoolean cho_duyet = Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHO_DUYET ? InheritableBoolean.True : InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = cmdSend2.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled = cmdSingleDownload1.Enabled = cmdSingleDownload2.Enabled = InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = cmdCancel3.Enabled =  cho_duyet;
                InPhieuTN.Enabled = InheritableBoolean.True;
                cmdXoa.Enabled = cmdXoa1.Enabled = InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                cmdXuatSanPhamChoPhongKhai.Enabled = InheritableBoolean.False;
                btnDelete.Enabled = false;
                cmdSuaSP.Enabled = InheritableBoolean.False;//Sua sp
                //btnSuaSP.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.DA_DUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = cmdSend2.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled = cmdSingleDownload1.Enabled = cmdSingleDownload2.Enabled = InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = cmdCancel3.Enabled =  InheritableBoolean.False;
                cmdXoa.Enabled = cmdXoa1.Enabled = InheritableBoolean.False;
                XacNhan.Enabled = InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                cmdXuatSanPhamChoPhongKhai.Enabled = InheritableBoolean.False;
                InPhieuTN.Enabled = InheritableBoolean.True;
                btnDelete.Enabled = false;
                cmdSuaSP.Enabled = InheritableBoolean.True;//Sua sp
                //btnSuaSP.Enabled = true;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                cmdSend.Enabled = cmdSend1.Enabled = cmdSend2.Enabled = InheritableBoolean.True;
                cmdXoa.Enabled = cmdXoa1.Enabled = InheritableBoolean.True;
                cmdSingleDownload.Enabled = cmdSingleDownload1.Enabled = cmdSingleDownload2.Enabled = InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = cmdCancel3.Enabled =  InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.True;
                cmdXuatSanPhamChoPhongKhai.Enabled = InheritableBoolean.True;
                InPhieuTN.Enabled = InheritableBoolean.False;
                btnDelete.Enabled = true;
                cmdSuaSP.Enabled = InheritableBoolean.False;//Sua sp
                //btnSuaSP.Enabled = false;
            }
            else if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.KHONG_PHE_DUYET || Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.DA_HUY)
            {
                //dgList.RootTable.Columns["SoTiepNhan"].Visible = true;
                //dgList.RootTable.Columns["NgayTiepNhan"].Visible = true;
                cmdSend.Enabled = cmdSend1.Enabled = cmdSend2.Enabled = InheritableBoolean.True;
                cmdXoa.Enabled = cmdXoa1.Enabled = InheritableBoolean.False;
                cmdSingleDownload.Enabled = cmdSingleDownload1.Enabled = cmdSingleDownload2.Enabled = InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = cmdCancel3.Enabled =  InheritableBoolean.False;
                DongBoDuLieu.Enabled = InheritableBoolean.False;
                cmdCSDaDuyet.Enabled = InheritableBoolean.False;
                cmdXuatSanPhamChoPhongKhai.Enabled = InheritableBoolean.False;
                InPhieuTN.Enabled = InheritableBoolean.True;
                btnDelete.Enabled = true;
                cmdSuaSP.Enabled = InheritableBoolean.False;//Sua sp
                //btnSuaSP.Enabled = false;
            }
        }


        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["TenHaiQuan"].Text = this.DonViHaiQuan_GetName(e.Row.Cells["MaHaiQuan"].Value.ToString().Trim());
                switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
                {
                    case -1:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Not declared yet";

                        }
                        break;
                    case 0:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Wait for approval";

                        }
                        break;
                    case 1:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Approved";

                        }
                        break;
                    case 2:
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Not Approved";

                        }
                        break;
                    //Update by Huỳnh Ngọc Khánh - 28/02/2012 
                    //Contents: Thêm trạng thái CHỜ HỦY và ĐÃ HỦY 
                    case 11:
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ Hủy";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Wait for canceled";

                        }
                        break;
                    case 10:
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã Hủy";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Canceled";

                        }
                        break;
                }


                //TODO: Cao Hữu Tú updated:15-09-2011
                //Contents: bổ sung cột mã sản phẩm vào Grid Danh sách sản phẩm


                Company.BLL.KDT.SXXK.SanPhamCollection SanPhamCollection = new SanPhamCollection();
                int ValueIdCell = Int32.Parse(e.Row.Cells["ID"].Value.ToString());
                string TatCaMaNPL = "";

                SanPhamCollection = Company.BLL.KDT.SXXK.SanPham.SelectCollectionBy_Master_ID(ValueIdCell);
                Company.BLL.KDT.SXXK.SanPham entitySanPham = new SanPham();

                if (SanPhamCollection.Count > 0)
                {
                    //lấy mã đầu tiên trong NguyenPhuLieuCollection đưa vào Cột mã nguyên phụ liệu
                    entitySanPham = SanPhamCollection[0];
                    e.Row.Cells["MaSanPham"].Text = entitySanPham.Ma;

                    //lấy mã sản phẩm bắt đầu từ phần tử thứ 2 đưa vào 
                    foreach (Company.BLL.KDT.SXXK.SanPham EntitySP in SanPhamCollection)
                    {

                        TatCaMaNPL = TatCaMaNPL + EntitySP.Ma + "\n";

                    }
                    e.Row.Cells["MaSanPham"].ToolTipText = TatCaMaNPL;
                }

            }
        }

        //-----------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------

        private void cmMain_CommandClick(object sender, CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "Export":
                    this.ExportData();
                    break;
                case "Import":
                    this.ImportData();
                    break;
                case "cmdXoa":
                    this.Delete();
                    break;
                case "cmdCSDaDuyet":
                    this.ChuyenTrangThai();
                    break;
                case "cmdXuatSanPhamChoPhongKhai":
                    XuatSanPhamChoPhongKhai();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdSuaSP":
                    this.SuaSPDaDuyet();
                    break;
                case "cmdUpdateStatus":
                    mnuUpdateStatus_Click(null, null);
                    break;
            }
        }
        private void mnuUpdateStatus_Click(object sender, EventArgs e)
        {
            try
            {
                WSForm2 login = new WSForm2();
                login.ShowDialog(this);
                if (WSForm2.IsSuccess == true)
                {
                    if (dgList.SelectedItems.Count > 0)
                    {
                        List<SanPhamDangKy> NpldkColl = new List<SanPhamDangKy>();
                        foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                        {
                            NpldkColl.Add((SanPhamDangKy)grItem.GetRow().DataRow);
                        }
                        for (int i = 0; i < NpldkColl.Count; i++)
                        {
                            UpdateStatusForm f = new UpdateStatusForm();
                            f.spDangKy = NpldkColl[i];
                            f.formType = "SP";
                            f.ShowDialog(this);
                        }
                        this.btnSearch_Click(null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SuaSPDaDuyet()
        {
            SanPhamDangKy spdk = new SanPhamDangKy();
            spdk = (SanPhamDangKy)dgList.GetRow().DataRow;
            spdk.LoadSPCollection();
            if (SanPhamDangKySUA.SelectCollectionBy_IDSPDK(spdk.ID).Count == 0)
            {
                string msg = "DOANH NGHIỆP CÓ MUỐN CHUYỂN SANG TRẠNG THÁI SỬA SẢN PHẨM KHÔNG?";
                msg += "\n\nSỐ TIẾP NHẬN: " + spdk.SoTiepNhan.ToString();
                msg += "\n----------------------";
                msg += "\nCÓ " + spdk.SPCollection.Count.ToString() + " SẢN PHẨM ĐĂNG KÝ ĐÃ ĐƯỢC DUYỆT";
                if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                {
                    SanPhamSuaForm f = new SanPhamSuaForm();
                    f.SPDangKy = spdk;
                    f.masterId = spdk.ID;
                    f.spdkSUA = new SanPhamDangKySUA() { TrangThaiXuLy = -1 };
                    f.ShowDialog(this);
                }
            }
            else
            {
                SanPhamSuaListForm listForm = new SanPhamSuaListForm();
                listForm.spdk = spdk;
                listForm.ShowDialog(this);
            }
        }
        private void inPhieuTN()
        {
            if (dgList.GetRows().Length < 1) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
            phieuTNForm.TenPhieu = "SẢN PHẨM";
            Company.Interface.Report.SXXK.PhieuTN phieuTNAll = new Company.Interface.Report.SXXK.PhieuTN();
            string[,] arrPhieuTN = new string[items.Count, 2];
            int j = 0;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    SanPhamDangKy spDangKySelected = (SanPhamDangKy)i.GetRow().DataRow;
                    arrPhieuTN[j, 0] = spDangKySelected.SoTiepNhan.ToString();
                    arrPhieuTN[j, 1] = spDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                    j++;
                }
            }
            phieuTNForm.phieuTN = arrPhieuTN;
            phieuTNForm.Show();

        }
        private void XuatSanPhamChoPhongKhai()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                // ShowMessage("Chưa chọn danh sách sản phẩm cần xuất.", false);
                MLMessages("Chưa chọn danh sách sản phẩm cần xuất.", "MSG_PUB45", "", false);
                return;
            }
            try
            {
                SanPhamDangKyCollection col = new SanPhamDangKyCollection();
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SanPhamDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    int sosp = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            SanPhamDangKy spSelected = (SanPhamDangKy)i.GetRow().DataRow;
                            spSelected.LoadSPCollection();
                            col.Add(spSelected);
                            sosp++;
                        }
                    }
                    serializer.Serialize(fs, col);
                    //ShowMessage("Xuất thành công " + sosp + " sản phẩm.", false);
                    MLMessages("Xuất thành công " + sosp + " sản phẩm.", "MSG_NPL04", "" + sosp, false);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
        }

        private void ChuyenTrangThai()
        {

            try
            {
                if (dgList.SelectedItems.Count > 0)
                {
                    SanPhamDangKyCollection spDKColl = new SanPhamDangKyCollection();
                    foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                    {
                        spDKColl.Add((SanPhamDangKy)grItem.GetRow().DataRow);
                    }
                    string msgWarning = "\nDOANH NGHIỆP CÓ MUỐN CHUYỂN DANH SÁCH KHAI BÁO SẢN PHẨM NÀY SANG ĐÃ DUYỆT KHÔNG?\n";
                    string msgAccept = "";
                    for (int i = 0; i < spDKColl.Count; i++)
                    {
                        if (spDKColl[i].TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            spDKColl[i].LoadSPCollection();
                            msgAccept += "[" + (i + 1) + "]-[" + spDKColl[i].ID + "]-[" + spDKColl[i].SoTiepNhan + "]-[" + spDKColl[i].NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(spDKColl[i].TrangThaiXuLy) + "]\n";
                        }
                    }
                    msgWarning += "\n - [STT] -[ID]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + msgAccept + "\n";
                    if (ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ", msgWarning, true) == "Yes")
                    {
                        foreach (SanPhamDangKy item in spDKColl)
                        {
                            item.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            item.TransgferDataToSXXK();
                        }
                    }
                    ShowMessage("CHUYỂN TRẠNG THÁI THÀNH CÔNG ", false);
                    btnSearch_Click(null, null);
                }
                else
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "CHƯA CÓ DỮ LIỆU ĐƯỢC CHỌN", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        public void XoaSanPhamDangKy(GridEXSelectedItemCollection items)
        {
            try
            {
                if (items.Count == 0) return;
                List<SanPhamDangKy> itemsDelete = new List<SanPhamDangKy>();
                List<String> SPCollection = new List<String>();
                int k = 1;
                string msgWarning = "\nDOANH NGHIỆP CÓ MUỐN XÓA DANH SÁCH KHAI BÁO SẢN PHẨM NÀY KHÔNG ?\n";
                string msgDelete = "";
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SanPhamDangKy npldk = (SanPhamDangKy)i.GetRow().DataRow;
                        npldk.LoadSPCollection();
                        msgDelete += "[" + (k) + "]-[" + npldk.ID + "]-[" + npldk.SoTiepNhan + "]-[" + npldk.NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(npldk.TrangThaiXuLy) + "]\n";
                        k++;
                        itemsDelete.Add(npldk);
                    }
                }
                if (itemsDelete.Count >= 1)
                {
                    msgWarning += "\n - [STT] -[ID]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + msgDelete + "\n";
                    if (ShowMessageTQDT(" THÔNG BÁO TỪ HỆ THỐNG ",msgWarning, true) == "Yes")
                    {
                        foreach (SanPhamDangKy item in itemsDelete)
                        {
                            foreach (SanPham itemDelete in item.SPCollection)
                            {
                                try
                                {
                                    Company.BLL.SXXK.SanPham SP = new Company.BLL.SXXK.SanPham();
                                    SP.Ma = itemDelete.Ma;
                                    SP.MaHaiQuan = item.MaHaiQuan;
                                    SP.MaDoanhNghiep = item.MaDoanhNghiep;
                                    SP.Delete();
                                }
                                catch (Exception ex)
                                {
                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                }
                            }                         
                            item.Delete(); 
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Delete()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            XoaSanPhamDangKy(items);
            btnSearch_Click(null,null);
        }
        private string checkDataHangImport(SanPhamDangKy spDangky)
        {
            string st = "";
            foreach (SanPham sp in spDangky.SPCollection)
            {
                BLL.SXXK.SanPham spDaDuyet = new Company.BLL.SXXK.SanPham();
                spDaDuyet.Ma = sp.Ma;
                spDaDuyet.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                spDaDuyet.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (spDaDuyet.Load())
                {
                    //if (sp.STTHang == 1)
                    //    st = "Danh sách có ID='" + spDangky.ID + "'\n";
                    //st += "Sản phẩm có mã '" + sp.Ma + "' đã có trong hệ thống.\n";
                    if (sp.STTHang == 1)
                    {
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            st = "Danh sách có ID='" + spDangky.ID + "'\n";

                        }
                        else
                        {
                            st = "List has ID='" + spDangky.ID + "'\n";

                        }
                    }
                    if (GlobalSettings.NGON_NGU == "0")
                    {

                        st += "Sản phẩm có mã '" + sp.Ma + "' đã có trong hệ thống.\n";
                    }
                    else
                    {

                        st += "Product has code '" + sp.Ma + "' already exist in system.\n";
                    }
                }
            }
            return st;
        }
        private string checkDataImport(SanPhamDangKyCollection collection)
        {
            string st = "";
            foreach (SanPhamDangKy spDangky in collection)
            {
                SanPhamDangKy spInDatabase = SanPhamDangKy.Load(spDangky.ID);
                if (spInDatabase != null)
                {
                    if (spInDatabase.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        // st += "Danh sách có ID=" + spDangky.ID + " đã được duyệt.\n";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            st += "Danh sách có ID=" + spDangky.ID + " đã được duyệt.\n";
                        }
                        else
                        {
                            st += "List has ID=" + spDangky.ID + " already approved.\n";
                        }
                    }
                    else
                    {
                        string tmp = checkDataHangImport(spDangky);
                        st += tmp;
                        if (tmp == "")
                            tmpCollection.Add(spDangky);
                    }
                }
                else
                {
                    if (spInDatabase.ID > 0)
                        spInDatabase.ID = 0;
                    tmpCollection.Add(spDangky);
                }
            }
            return st;
        }
        private void ImportData()
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    tmpCollection.Clear();
                    XmlSerializer serializer = new XmlSerializer(typeof(SanPhamDangKyCollection));
                    FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    SanPhamDangKyCollection spDKCollection = (SanPhamDangKyCollection)serializer.Deserialize(fs);
                    fs.Close();
                    string st = checkDataImport(spDKCollection);
                    if (st != "")
                    {
                        // if (ShowMessage("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", true) == "Yes")
                        if (MLMessages("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", "MSG_PUB04", "", true) == "Yes")
                        {
                            SanPhamDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                            //ShowMessage("Import thành công", false);
                            MLMessages("Import thành công", "MSG_PUB02", "", false);
                        }
                    }
                    else
                    {
                        SanPhamDangKy.DongBoDuLieuPhongKhai(tmpCollection);
                        //ShowMessage("Import thành công", false);
                        MLMessages("Import thành công", "MSG_PUB02", "", false);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage(" " + ex.Message, false);
                }
            }
        }
        private void ExportData()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                ShowMessage("Chưa chọn danh sách sản phẩm", false);
                return;
            }
            try
            {
                SanPhamDangKyCollection col = new SanPhamDangKyCollection();
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SanPhamDangKyCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            SanPhamDangKy spDangKySelected = (SanPhamDangKy)i.GetRow().DataRow;
                            spDangKySelected.LoadSPCollection();
                            col.Add(spDangKySelected);
                        }
                    }
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }

        }
        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                SanPhamSendForm f = new SanPhamSendForm();
                f.spDangKy = (SanPhamDangKy)e.Row.DataRow;
                f.ShowDialog(this);
                btnSearch_Click(null,null);
            }
        }

        //-----------------------------------------------------------------------------------------

        private void cbStatus_SelectedValueChanged(object sender, EventArgs e)
        {
            //Edit by Huỳnh Ngọc Khánh - 28/02/2012
            //Contents: Hiển thị các button phụ thuộc vào trạng thái của tờ khai
            if (Convert.ToInt32(cbStatus.SelectedValue) == TrangThaiXuLy.CHUA_KHAI_BAO || (Convert.ToInt32(this.cbStatus.SelectedValue) == TrangThaiXuLy.KHONG_PHE_DUYET))
            {
                txtNamTiepNhan.Text = string.Empty;
                txtNamTiepNhan.Value = 0;
                txtNamTiepNhan.Enabled = false;
                txtSoTiepNhan.Value = 0;
                txtSoTiepNhan.Text = string.Empty;
                txtSoTiepNhan.Enabled = false;

            }
            else
            {
                txtNamTiepNhan.Value = DateTime.Today.Year;
                txtNamTiepNhan.Enabled = true;
                txtSoTiepNhan.Enabled = true;
            }
            btnSearch_Click(null, null);
        }

        //-----------------------------------------------------------------------------------------


        private void SanPhamManageForm_CX_Shown(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Delete();
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.Delete();
        }

        private void uiContextMenu1_CommandClick(object sender, CommandEventArgs e)
        {

        }

        private void btnSuaSP_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() != null)
            {
                this.SuaSPDaDuyet();
            }
            else
            {
                ShowMessage("CHƯA CHỌN THÔNG TIN ĐỂ SỬA.", false);
                return;
            }
        }

        private void uiMessage_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;

            SanPhamDangKy nplSanPhamDangKy = (SanPhamDangKy)dgList.CurrentRow.DataRow;
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = nplSanPhamDangKy.ID;
            form.DeclarationIssuer = DeclarationIssuer.SXXK_SP;
            form.ShowDialog(this);
        }


        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            if (dgList.GetRow() != null)
                spdk = (SanPhamDangKy)dgList.GetRow().DataRow;

            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.SanPham;
            sendXML.master_id = spdk.ID;
            bool isSend = false;/*sendXML.Load() || spdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || spdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET*/
                /*|| spdk.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY;*/

            cmdSend.Enabled = cmdSend1.Enabled = cmdSend2.Enabled = isSend ? InheritableBoolean.False : InheritableBoolean.True; 
            cmdSingleDownload.Enabled = cmdSingleDownload1.Enabled = cmdSingleDownload2.Enabled = isSend ? InheritableBoolean.True : InheritableBoolean.False;


        }

        private void ckbTimKiem_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;
        }
    }
}