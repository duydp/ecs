using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.ExportToExcel
{
	public partial class SHAIQUAN 
	{
		#region Private members.
		
		protected string _MA_HQ = String.Empty;
		protected string _TEN_HQ = String.Empty;
		protected short _CAP_HQ;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public string MA_HQ
		{
			set {this._MA_HQ = value;}
			get {return this._MA_HQ;}
		}
		public string TEN_HQ
		{
			set {this._TEN_HQ = value;}
			get {return this._TEN_HQ;}
		}
		public short CAP_HQ
		{
			set {this._CAP_HQ = value;}
			get {return this._CAP_HQ;}
		}
		
		//---------------------------------------------------------------------------------------------
        
		
		#endregion
		
		public DataSet SelectAll()
        {
            string sql = "SELECT * FROM SHAIQUAN";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(dbCommand);
        }
		
		public DataSet SelectDynamic(string whereCondition)
		{
            string sql = "SELECT * FROM SHAIQUAN WHERE " + whereCondition;
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);            
            return db.ExecuteDataSet(dbCommand);        				
		}


    }	
}