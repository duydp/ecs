namespace Company.Interface.Report.VNACCS
{
    partial class BanXacNhanNoiDungToKhaiHangHoaXuatKhau_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.lblTennguoiuythacxuatkhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblManguoiuythacxuatkhau = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiachinguoixuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenxuatkhau = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.lblSodtnguoixuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaxuatkhau = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lblGiothaydoidangky = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaythaydoidangky = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNhomxulyhoso = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMahieuphuongthucvanchuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaphanloaihanghoa = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaloaihinh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMasothuedaidien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongsotokhaichianho = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSonhanhtokhaichianho = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBieuthitruonghophethan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotokhaidautien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGiodangky = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThoihantainhapxuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaydangky = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTencoquanhaiquantiepnhantokhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaphanloaikiemtra = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotokhaitamnhaptaixuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotokhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanloaihinhthuchoadon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel126 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaydukienden = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiadiem3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaykhoihanhvanchuyen3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaykhoihanhvanchuyen2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel120 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel121 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaydukiendentrungchuyen3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaydukiendentrungchuyen2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel117 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiadiemdich = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiadiem2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel114 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel113 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaykhoihanhvanchuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaydukiendentrungchuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiadiem = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel109 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel108 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel107 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel106 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel104 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaykhoihanh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel102 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel89 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine11 = new DevExpress.XtraReports.UI.XRLine();
            this.lblSoquanlynguoisudung = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel103 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoquanlynoibodoanhnghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel93 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanghichu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel105 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSodinhkemkhaibao3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel99 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanloaidinhkemkhaibaodientu3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel98 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSodinhkemkhaibao2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel96 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanloaidinhkemkhaibaodientu2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel90 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel94 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanloaidinhkemkhaibaodientu1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel92 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel87 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSodinhkemkhaibao1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine9 = new DevExpress.XtraReports.UI.XRLine();
            this.lblTongsodonghangtokhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel88 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongsotokhaicuatrang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.lblTongsotienbaolanh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel83 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMatientetienbaolanh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMatientetongtienthuexuat = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongsotienlephi = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongsotienthuexuatkhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaxacdinhthoihannopthue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel85 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanloainopthue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoinopthue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel82 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanloaikhongcanquydoi = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.lblMaphanloainhaplieutonggia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTonghesophanbothue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel81 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTygiatinhthue2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadongtientygiathue2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTygiatinhthue1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadongtientygiathue1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel70 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongthue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadongtientongthue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaphanloaigiahoadon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongtrigiahoadon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadongtienhoadon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadkgiahoadon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhuongthucthanhtoan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayphathanh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSohoadon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotiepnhanhoadondientu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSogiayphep5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSogiayphep4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSogiayphep3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSogiayphep2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSogiayphep1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanloaigiayphepxuatkhau5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanloaigiayphepxuatkhau4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanloaigiayphepxuatkhau3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanloaigiayphepxuatkhau2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanloaigiayphepxuatkhau1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSTT5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSTT3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSTT2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.lblTenphuongtienvanchuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayhangdidukien = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaphuongtienvanchuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTendiadiemxephang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadiadiemxephang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTendiadiemnhanhangcuoi = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadiadiemnhanhangcuoi = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTendiadiemluukho = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadonvitinhtrongluong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadonvitinh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongtrongluonghang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadiadiemluukho = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoluong = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSovandon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.lblManhanvienhaiquan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTendailyhaiquan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMadailyhaiquan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblManuoc = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiachi4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiachi3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiachi2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.lblDiachi1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMabuuchinh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblManguoinhapkhau = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTennguoinhapkhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblKyhieusohieu = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSTT4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine12 = new DevExpress.XtraReports.UI.XRLine();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2,
            this.lblTennguoiuythacxuatkhau,
            this.lblManguoiuythacxuatkhau,
            this.xrLabel18,
            this.xrLabel16,
            this.lblDiachinguoixuat,
            this.xrLabel31,
            this.lblTenxuatkhau,
            this.xrLine3,
            this.lblSodtnguoixuat,
            this.xrLabel33,
            this.lblMaxuatkhau,
            this.xrLabel25,
            this.xrLabel24,
            this.xrLabel23,
            this.xrLabel28,
            this.xrLabel27,
            this.xrLabel26});
            this.Detail.HeightF = 172.5F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine2
            // 
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 158.0001F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(734.9999F, 4.499939F);
            // 
            // lblTennguoiuythacxuatkhau
            // 
            this.lblTennguoiuythacxuatkhau.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTennguoiuythacxuatkhau.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 129F);
            this.lblTennguoiuythacxuatkhau.Multiline = true;
            this.lblTennguoiuythacxuatkhau.Name = "lblTennguoiuythacxuatkhau";
            this.lblTennguoiuythacxuatkhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTennguoiuythacxuatkhau.SizeF = new System.Drawing.SizeF(622.5F, 29.00008F);
            this.lblTennguoiuythacxuatkhau.StylePriority.UseFont = false;
            this.lblTennguoiuythacxuatkhau.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            // 
            // lblManguoiuythacxuatkhau
            // 
            this.lblManguoiuythacxuatkhau.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManguoiuythacxuatkhau.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 118F);
            this.lblManguoiuythacxuatkhau.Name = "lblManguoiuythacxuatkhau";
            this.lblManguoiuythacxuatkhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblManguoiuythacxuatkhau.SizeF = new System.Drawing.SizeF(145.8333F, 11F);
            this.lblManguoiuythacxuatkhau.StylePriority.UseFont = false;
            this.lblManguoiuythacxuatkhau.Text = "XXXXXXXXX1-XXE";
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 129F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.Text = "Tên";
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 118F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.Text = "Mã";
            // 
            // lblDiachinguoixuat
            // 
            this.lblDiachinguoixuat.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiachinguoixuat.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 65.00002F);
            this.lblDiachinguoixuat.Multiline = true;
            this.lblDiachinguoixuat.Name = "lblDiachinguoixuat";
            this.lblDiachinguoixuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiachinguoixuat.SizeF = new System.Drawing.SizeF(622.5F, 29.00001F);
            this.lblDiachinguoixuat.StylePriority.UseFont = false;
            this.lblDiachinguoixuat.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            // 
            // xrLabel31
            // 
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 54.00001F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.Text = "XXXXXXE";
            // 
            // lblTenxuatkhau
            // 
            this.lblTenxuatkhau.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenxuatkhau.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 25F);
            this.lblTenxuatkhau.Multiline = true;
            this.lblTenxuatkhau.Name = "lblTenxuatkhau";
            this.lblTenxuatkhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenxuatkhau.SizeF = new System.Drawing.SizeF(622.5F, 29.00001F);
            this.lblTenxuatkhau.StylePriority.UseFont = false;
            this.lblTenxuatkhau.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            // 
            // xrLine3
            // 
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 105F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(735F, 2.000008F);
            // 
            // lblSodtnguoixuat
            // 
            this.lblSodtnguoixuat.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSodtnguoixuat.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 93.99999F);
            this.lblSodtnguoixuat.Name = "lblSodtnguoixuat";
            this.lblSodtnguoixuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSodtnguoixuat.SizeF = new System.Drawing.SizeF(218.7499F, 11F);
            this.lblSodtnguoixuat.StylePriority.UseFont = false;
            this.lblSodtnguoixuat.Text = "XXXXXXXXX1XXXXXXXXXE";
            // 
            // xrLabel33
            // 
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 94.00001F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.Text = "Số điện thoại";
            // 
            // lblMaxuatkhau
            // 
            this.lblMaxuatkhau.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxuatkhau.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 12.5F);
            this.lblMaxuatkhau.Name = "lblMaxuatkhau";
            this.lblMaxuatkhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaxuatkhau.SizeF = new System.Drawing.SizeF(137.4998F, 11F);
            this.lblMaxuatkhau.StylePriority.UseFont = false;
            this.lblMaxuatkhau.Text = "XXXXXXXXX1-XXE";
            // 
            // xrLabel25
            // 
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 12.5F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.Text = "Mã";
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.Text = "Người xuất khẩu";
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 107F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(135F, 11.00002F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.Text = "Người ủy thác xuất khẩu";
            // 
            // xrLabel28
            // 
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 65.00001F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.Text = "Địa chỉ";
            // 
            // xrLabel27
            // 
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 25F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.Text = "Tên";
            // 
            // xrLabel26
            // 
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 54.00001F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.Text = "Mã bưu chính";
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.lblGiothaydoidangky,
            this.lblNgaythaydoidangky,
            this.lblNhomxulyhoso,
            this.lblMahieuphuongthucvanchuyen,
            this.lblMaphanloaihanghoa,
            this.lblMaloaihinh,
            this.lblMasothuedaidien,
            this.xrLabel15,
            this.xrLabel14,
            this.xrLabel13,
            this.lblTongsotokhaichianho,
            this.xrLabel12,
            this.lblSonhanhtokhaichianho,
            this.xrLabel11,
            this.lblBieuthitruonghophethan,
            this.xrLabel10,
            this.xrLabel9,
            this.lblSotokhaidautien,
            this.xrLabel8,
            this.lblGiodangky,
            this.lblThoihantainhapxuat,
            this.xrLabel7,
            this.lblNgaydangky,
            this.xrLabel6,
            this.lblTencoquanhaiquantiepnhantokhai,
            this.xrLabel5,
            this.lblMaphanloaikiemtra,
            this.xrLabel4,
            this.lblSotokhaitamnhaptaixuat,
            this.xrLabel3,
            this.lblSotokhai,
            this.xrLabel2,
            this.xrLabel1});
            this.TopMargin.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.TopMargin.HeightF = 145F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.StylePriority.UseFont = false;
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 112.0417F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(735F, 2.083336F);
            // 
            // lblGiothaydoidangky
            // 
            this.lblGiothaydoidangky.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiothaydoidangky.LocationFloat = new DevExpress.Utils.PointFloat(650F, 90.625F);
            this.lblGiothaydoidangky.Name = "lblGiothaydoidangky";
            this.lblGiothaydoidangky.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGiothaydoidangky.SizeF = new System.Drawing.SizeF(65.00006F, 11F);
            this.lblGiothaydoidangky.StylePriority.UseFont = false;
            this.lblGiothaydoidangky.Text = "hh:mm:ss";
            // 
            // lblNgaythaydoidangky
            // 
            this.lblNgaythaydoidangky.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaythaydoidangky.LocationFloat = new DevExpress.Utils.PointFloat(572.9168F, 90.625F);
            this.lblNgaythaydoidangky.Name = "lblNgaythaydoidangky";
            this.lblNgaythaydoidangky.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaythaydoidangky.SizeF = new System.Drawing.SizeF(77.08319F, 11F);
            this.lblNgaythaydoidangky.StylePriority.UseFont = false;
            this.lblNgaythaydoidangky.Text = "dd/MM/yyyy";
            // 
            // lblNhomxulyhoso
            // 
            this.lblNhomxulyhoso.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNhomxulyhoso.LocationFloat = new DevExpress.Utils.PointFloat(572.9167F, 79.62501F);
            this.lblNhomxulyhoso.Name = "lblNhomxulyhoso";
            this.lblNhomxulyhoso.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNhomxulyhoso.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.lblNhomxulyhoso.StylePriority.UseFont = false;
            this.lblNhomxulyhoso.Text = "XE";
            // 
            // lblMahieuphuongthucvanchuyen
            // 
            this.lblMahieuphuongthucvanchuyen.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMahieuphuongthucvanchuyen.LocationFloat = new DevExpress.Utils.PointFloat(381.875F, 68.62501F);
            this.lblMahieuphuongthucvanchuyen.Name = "lblMahieuphuongthucvanchuyen";
            this.lblMahieuphuongthucvanchuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMahieuphuongthucvanchuyen.SizeF = new System.Drawing.SizeF(25F, 11F);
            this.lblMahieuphuongthucvanchuyen.StylePriority.UseFont = false;
            this.lblMahieuphuongthucvanchuyen.Text = "X";
            // 
            // lblMaphanloaihanghoa
            // 
            this.lblMaphanloaihanghoa.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaphanloaihanghoa.LocationFloat = new DevExpress.Utils.PointFloat(356.25F, 68.62501F);
            this.lblMaphanloaihanghoa.Name = "lblMaphanloaihanghoa";
            this.lblMaphanloaihanghoa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaphanloaihanghoa.SizeF = new System.Drawing.SizeF(25.62503F, 11F);
            this.lblMaphanloaihanghoa.StylePriority.UseFont = false;
            this.lblMaphanloaihanghoa.Text = "X";
            // 
            // lblMaloaihinh
            // 
            this.lblMaloaihinh.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaloaihinh.LocationFloat = new DevExpress.Utils.PointFloat(310.4167F, 68.62501F);
            this.lblMaloaihinh.Name = "lblMaloaihinh";
            this.lblMaloaihinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaloaihinh.SizeF = new System.Drawing.SizeF(45.83337F, 11F);
            this.lblMaloaihinh.StylePriority.UseFont = false;
            this.lblMaloaihinh.Text = "XXE";
            // 
            // lblMasothuedaidien
            // 
            this.lblMasothuedaidien.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMasothuedaidien.LocationFloat = new DevExpress.Utils.PointFloat(572.9169F, 68.62501F);
            this.lblMasothuedaidien.Name = "lblMasothuedaidien";
            this.lblMasothuedaidien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMasothuedaidien.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.lblMasothuedaidien.StylePriority.UseFont = false;
            this.lblMasothuedaidien.Text = "XXXE";
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(421.875F, 90.625F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(140.6251F, 11F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.Text = "Ngày thay đổi đăng ký";
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(421.875F, 79.62501F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(151.0418F, 11F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.Text = "Mã bộ phận xử lý tờ khai";
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(421.875F, 68.62501F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(127.0833F, 11F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.Text = "Mã số thuế đại diện";
            // 
            // lblTongsotokhaichianho
            // 
            this.lblTongsotokhaichianho.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongsotokhaichianho.LocationFloat = new DevExpress.Utils.PointFloat(579.1663F, 45.62499F);
            this.lblTongsotokhaichianho.Name = "lblTongsotokhaichianho";
            this.lblTongsotokhaichianho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongsotokhaichianho.SizeF = new System.Drawing.SizeF(59.375F, 11F);
            this.lblTongsotokhaichianho.StylePriority.UseFont = false;
            this.lblTongsotokhaichianho.StylePriority.UseTextAlignment = false;
            this.lblTongsotokhaichianho.Text = "NE";
            this.lblTongsotokhaichianho.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(565.6248F, 45.62499F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(13.5416F, 11F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "/";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSonhanhtokhaichianho
            // 
            this.lblSonhanhtokhaichianho.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSonhanhtokhaichianho.LocationFloat = new DevExpress.Utils.PointFloat(526.6665F, 45.62502F);
            this.lblSonhanhtokhaichianho.Name = "lblSonhanhtokhaichianho";
            this.lblSonhanhtokhaichianho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSonhanhtokhaichianho.SizeF = new System.Drawing.SizeF(38.95828F, 11F);
            this.lblSonhanhtokhaichianho.StylePriority.UseFont = false;
            this.lblSonhanhtokhaichianho.StylePriority.UseTextAlignment = false;
            this.lblSonhanhtokhaichianho.Text = "NE";
            this.lblSonhanhtokhaichianho.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(499.5833F, 45.62502F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(27.08328F, 11F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "-";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblBieuthitruonghophethan
            // 
            this.lblBieuthitruonghophethan.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBieuthitruonghophethan.LocationFloat = new DevExpress.Utils.PointFloat(264.1666F, 101.0417F);
            this.lblBieuthitruonghophethan.Name = "lblBieuthitruonghophethan";
            this.lblBieuthitruonghophethan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBieuthitruonghophethan.SizeF = new System.Drawing.SizeF(69.79169F, 11F);
            this.lblBieuthitruonghophethan.StylePriority.UseFont = false;
            this.lblBieuthitruonghophethan.StylePriority.UseTextAlignment = false;
            this.lblBieuthitruonghophethan.Text = "X";
            this.lblBieuthitruonghophethan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(248.5416F, 101.0417F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(15.62492F, 11F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.Text = "-";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(210.4167F, 68.62501F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Mã loại hình";
            // 
            // lblSotokhaidautien
            // 
            this.lblSotokhaidautien.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotokhaidautien.LocationFloat = new DevExpress.Utils.PointFloat(381.875F, 45.62499F);
            this.lblSotokhaidautien.Name = "lblSotokhaidautien";
            this.lblSotokhaidautien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotokhaidautien.SizeF = new System.Drawing.SizeF(117.7083F, 11F);
            this.lblSotokhaidautien.StylePriority.UseFont = false;
            this.lblSotokhaidautien.Text = "XXXXXXXXX1XE";
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(268.7501F, 45.62499F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(113.125F, 11F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.Text = "Số tờ khai đầu tiên";
            // 
            // lblGiodangky
            // 
            this.lblGiodangky.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiodangky.LocationFloat = new DevExpress.Utils.PointFloat(196.4583F, 90.625F);
            this.lblGiodangky.Name = "lblGiodangky";
            this.lblGiodangky.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGiodangky.SizeF = new System.Drawing.SizeF(67.70828F, 10.41669F);
            this.lblGiodangky.StylePriority.UseFont = false;
            this.lblGiodangky.Text = "hh:mm:ss";
            // 
            // lblThoihantainhapxuat
            // 
            this.lblThoihantainhapxuat.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThoihantainhapxuat.LocationFloat = new DevExpress.Utils.PointFloat(169.375F, 101.0417F);
            this.lblThoihantainhapxuat.Name = "lblThoihantainhapxuat";
            this.lblThoihantainhapxuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThoihantainhapxuat.SizeF = new System.Drawing.SizeF(79.16667F, 11F);
            this.lblThoihantainhapxuat.StylePriority.UseFont = false;
            this.lblThoihantainhapxuat.Text = "dd/MM/yyyy";
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 101.0417F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(159.375F, 11F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.Text = "Thời hạn tái nhập/ tái xuất";
            // 
            // lblNgaydangky
            // 
            this.lblNgaydangky.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaydangky.LocationFloat = new DevExpress.Utils.PointFloat(110F, 90.625F);
            this.lblNgaydangky.Name = "lblNgaydangky";
            this.lblNgaydangky.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaydangky.SizeF = new System.Drawing.SizeF(86.45834F, 10.41669F);
            this.lblNgaydangky.StylePriority.UseFont = false;
            this.lblNgaydangky.Text = "dd/MM/yyyy";
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 90.625F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(100F, 10.41668F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "Ngày đăng ký";
            // 
            // lblTencoquanhaiquantiepnhantokhai
            // 
            this.lblTencoquanhaiquantiepnhantokhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTencoquanhaiquantiepnhantokhai.LocationFloat = new DevExpress.Utils.PointFloat(238.125F, 79.62501F);
            this.lblTencoquanhaiquantiepnhantokhai.Name = "lblTencoquanhaiquantiepnhantokhai";
            this.lblTencoquanhaiquantiepnhantokhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTencoquanhaiquantiepnhantokhai.SizeF = new System.Drawing.SizeF(107.2916F, 11F);
            this.lblTencoquanhaiquantiepnhantokhai.StylePriority.UseFont = false;
            this.lblTencoquanhaiquantiepnhantokhai.Text = "XXXXXXXXXE";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 79.62501F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(228.125F, 11F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.Text = "Tên cơ quan Hải quan tiếp nhận tờ khai";
            // 
            // lblMaphanloaikiemtra
            // 
            this.lblMaphanloaikiemtra.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaphanloaikiemtra.LocationFloat = new DevExpress.Utils.PointFloat(137.0833F, 68.62501F);
            this.lblMaphanloaikiemtra.Name = "lblMaphanloaikiemtra";
            this.lblMaphanloaikiemtra.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaphanloaikiemtra.SizeF = new System.Drawing.SizeF(42.70833F, 11F);
            this.lblMaphanloaikiemtra.StylePriority.UseFont = false;
            this.lblMaphanloaikiemtra.Text = "XXE ";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 68.62501F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(127.0833F, 11F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "Mã phân loại kiểm tra";
            // 
            // lblSotokhaitamnhaptaixuat
            // 
            this.lblSotokhaitamnhaptaixuat.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotokhaitamnhaptaixuat.LocationFloat = new DevExpress.Utils.PointFloat(268.7501F, 57.16665F);
            this.lblSotokhaitamnhaptaixuat.Name = "lblSotokhaitamnhaptaixuat";
            this.lblSotokhaitamnhaptaixuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotokhaitamnhaptaixuat.SizeF = new System.Drawing.SizeF(118.75F, 11F);
            this.lblSotokhaitamnhaptaixuat.StylePriority.UseFont = false;
            this.lblSotokhaitamnhaptaixuat.Text = "NNNNNNNNN1NE";
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 57.16665F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(216.6667F, 11F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "Số tờ khai tạm nhập tái xuất tương ứng";
            // 
            // lblSotokhai
            // 
            this.lblSotokhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotokhai.LocationFloat = new DevExpress.Utils.PointFloat(73.54167F, 45.62499F);
            this.lblSotokhai.Name = "lblSotokhai";
            this.lblSotokhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotokhai.SizeF = new System.Drawing.SizeF(122.9167F, 11F);
            this.lblSotokhai.StylePriority.UseFont = false;
            this.lblSotokhai.Text = "NNNNNNNNN1NE";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 45.62499F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(63.54166F, 11F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "Số tờ khai";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(125F, 10.00001F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(511.5419F, 29.66667F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Bản xác nhận nội dung tờ khai hàng hóa xuất khẩu";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 77F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel29,
            this.xrLabel30,
            this.lblPhanloaihinhthuchoadon,
            this.xrLabel126,
            this.lblNgaydukienden,
            this.lblDiadiem3,
            this.lblNgaykhoihanhvanchuyen3,
            this.lblNgaykhoihanhvanchuyen2,
            this.xrLabel120,
            this.xrLabel121,
            this.lblNgaydukiendentrungchuyen3,
            this.lblNgaydukiendentrungchuyen2,
            this.xrLabel117,
            this.lblDiadiemdich,
            this.lblDiadiem2,
            this.xrLabel114,
            this.xrLabel113,
            this.lblNgaykhoihanhvanchuyen,
            this.lblNgaydukiendentrungchuyen,
            this.lblDiadiem,
            this.xrLabel109,
            this.xrLabel108,
            this.xrLabel107,
            this.xrLabel106,
            this.xrLabel104,
            this.lblNgaykhoihanh,
            this.xrLabel102,
            this.xrLabel89,
            this.xrLine11,
            this.lblSoquanlynguoisudung,
            this.xrLabel103,
            this.lblSoquanlynoibodoanhnghiep,
            this.xrLabel93,
            this.lblPhanghichu,
            this.xrLabel105,
            this.lblSodinhkemkhaibao3,
            this.xrLabel99,
            this.lblPhanloaidinhkemkhaibaodientu3,
            this.xrLabel98,
            this.lblSodinhkemkhaibao2,
            this.xrLabel96,
            this.lblPhanloaidinhkemkhaibaodientu2,
            this.xrLabel90,
            this.xrLabel94,
            this.lblPhanloaidinhkemkhaibaodientu1,
            this.xrLabel92,
            this.xrLine10,
            this.xrLabel87,
            this.lblSodinhkemkhaibao1,
            this.xrLine9,
            this.lblTongsodonghangtokhai,
            this.xrLabel88,
            this.lblTongsotokhaicuatrang,
            this.xrLabel84,
            this.xrLine8,
            this.lblTongsotienbaolanh,
            this.xrLabel83,
            this.xrLabel78,
            this.lblMatientetienbaolanh,
            this.lblMatientetongtienthuexuat,
            this.lblTongsotienlephi,
            this.lblTongsotienthuexuatkhau,
            this.lblMaxacdinhthoihannopthue,
            this.xrLabel86,
            this.xrLabel85,
            this.lblPhanloainopthue,
            this.lblNguoinopthue,
            this.xrLabel82,
            this.lblPhanloaikhongcanquydoi,
            this.xrLabel77,
            this.xrLabel75,
            this.xrLabel74,
            this.xrLine7,
            this.lblMaphanloainhaplieutonggia,
            this.xrLabel71,
            this.lblTonghesophanbothue,
            this.xrLabel81,
            this.lblTygiatinhthue2,
            this.xrLabel79,
            this.lblMadongtientygiathue2,
            this.lblTygiatinhthue1,
            this.xrLabel76,
            this.lblMadongtientygiathue1,
            this.xrLabel70,
            this.lblTongthue,
            this.xrLabel73,
            this.lblMadongtientongthue,
            this.xrLabel54,
            this.lblMaphanloaigiahoadon,
            this.xrLabel64,
            this.lblTongtrigiahoadon,
            this.xrLabel59,
            this.lblMadongtienhoadon,
            this.xrLabel49,
            this.lblMadkgiahoadon,
            this.lblPhuongthucthanhtoan,
            this.lblNgayphathanh,
            this.xrLabel67,
            this.xrLabel68,
            this.xrLabel69,
            this.lblSohoadon,
            this.xrLabel66,
            this.lblSotiepnhanhoadondientu,
            this.xrLabel72,
            this.lblSogiayphep5,
            this.lblSogiayphep4,
            this.lblSogiayphep3,
            this.lblSogiayphep2,
            this.lblSogiayphep1,
            this.lblPhanloaigiayphepxuatkhau5,
            this.lblPhanloaigiayphepxuatkhau4,
            this.lblPhanloaigiayphepxuatkhau3,
            this.lblPhanloaigiayphepxuatkhau2,
            this.lblPhanloaigiayphepxuatkhau1,
            this.lblSTT5,
            this.lblSTT3,
            this.lblSTT2,
            this.xrLabel45,
            this.xrLine6,
            this.lblTenphuongtienvanchuyen,
            this.lblNgayhangdidukien,
            this.lblMaphuongtienvanchuyen,
            this.lblTendiadiemxephang,
            this.lblMadiadiemxephang,
            this.lblTendiadiemnhanhangcuoi,
            this.lblMadiadiemnhanhangcuoi,
            this.lblTendiadiemluukho,
            this.lblMadonvitinhtrongluong,
            this.lblMadonvitinh,
            this.lblTongtrongluonghang,
            this.xrLabel46,
            this.xrLabel42,
            this.xrLabel39,
            this.lblMadiadiemluukho,
            this.xrLabel48,
            this.lblSoluong,
            this.xrLabel53,
            this.xrLabel41,
            this.xrLabel40,
            this.xrLabel43,
            this.xrLabel44,
            this.lblSovandon,
            this.xrLine5,
            this.lblManhanvienhaiquan,
            this.xrLabel37,
            this.lblTendailyhaiquan,
            this.lblMadailyhaiquan,
            this.xrLabel36,
            this.lblManuoc,
            this.lblDiachi4,
            this.lblDiachi3,
            this.lblDiachi2,
            this.xrLabel21,
            this.xrLine4,
            this.lblDiachi1,
            this.lblMabuuchinh,
            this.lblManguoinhapkhau,
            this.xrLabel20,
            this.xrLabel19,
            this.xrLabel17,
            this.xrLabel35,
            this.xrLabel22,
            this.lblTennguoinhapkhau,
            this.lblKyhieusohieu,
            this.lblSTT4,
            this.xrLine12});
            this.Detail1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Detail1.HeightF = 507.7637F;
            this.Detail1.Name = "Detail1";
            this.Detail1.StylePriority.UseFont = false;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(12.50005F, 225.0416F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "1";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(419.5836F, 214.0415F);
            this.xrLabel30.Multiline = true;
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(13.45837F, 11.00011F);
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.Text = "-";
            // 
            // lblPhanloaihinhthuchoadon
            // 
            this.lblPhanloaihinhthuchoadon.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanloaihinhthuchoadon.LocationFloat = new DevExpress.Utils.PointFloat(406.8751F, 214.0416F);
            this.lblPhanloaihinhthuchoadon.Multiline = true;
            this.lblPhanloaihinhthuchoadon.Name = "lblPhanloaihinhthuchoadon";
            this.lblPhanloaihinhthuchoadon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanloaihinhthuchoadon.SizeF = new System.Drawing.SizeF(12.70859F, 10.99997F);
            this.lblPhanloaihinhthuchoadon.StylePriority.UseFont = false;
            this.lblPhanloaihinhthuchoadon.StylePriority.UseTextAlignment = false;
            this.lblPhanloaihinhthuchoadon.Text = "X";
            this.lblPhanloaihinhthuchoadon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel126
            // 
            this.xrLabel126.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel126.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 483.979F);
            this.xrLabel126.Name = "xrLabel126";
            this.xrLabel126.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel126.SizeF = new System.Drawing.SizeF(186.4583F, 12.41653F);
            this.xrLabel126.StylePriority.UseFont = false;
            this.xrLabel126.Text = "Địa điểm đích cho vận chuyển bảo thuế";
            // 
            // lblNgaydukienden
            // 
            this.lblNgaydukienden.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaydukienden.LocationFloat = new DevExpress.Utils.PointFloat(321.4583F, 483.979F);
            this.lblNgaydukienden.Name = "lblNgaydukienden";
            this.lblNgaydukienden.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaydukienden.SizeF = new System.Drawing.SizeF(85.41681F, 11.37497F);
            this.lblNgaydukienden.StylePriority.UseFont = false;
            this.lblNgaydukienden.Text = "dd/MM/yyyy";
            // 
            // lblDiadiem3
            // 
            this.lblDiadiem3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiadiem3.LocationFloat = new DevExpress.Utils.PointFloat(196.4583F, 472.6041F);
            this.lblDiadiem3.Name = "lblDiadiem3";
            this.lblDiadiem3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiadiem3.SizeF = new System.Drawing.SizeF(85.41681F, 11.37497F);
            this.lblDiadiem3.StylePriority.UseFont = false;
            this.lblDiadiem3.Text = "XXXE";
            // 
            // lblNgaykhoihanhvanchuyen3
            // 
            this.lblNgaykhoihanhvanchuyen3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaykhoihanhvanchuyen3.LocationFloat = new DevExpress.Utils.PointFloat(469.7085F, 472.6041F);
            this.lblNgaykhoihanhvanchuyen3.Name = "lblNgaykhoihanhvanchuyen3";
            this.lblNgaykhoihanhvanchuyen3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaykhoihanhvanchuyen3.SizeF = new System.Drawing.SizeF(85.41681F, 11.37497F);
            this.lblNgaykhoihanhvanchuyen3.StylePriority.UseFont = false;
            this.lblNgaykhoihanhvanchuyen3.Text = "dd/MM/yyyy";
            // 
            // lblNgaykhoihanhvanchuyen2
            // 
            this.lblNgaykhoihanhvanchuyen2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaykhoihanhvanchuyen2.LocationFloat = new DevExpress.Utils.PointFloat(469.7085F, 461.6041F);
            this.lblNgaykhoihanhvanchuyen2.Name = "lblNgaykhoihanhvanchuyen2";
            this.lblNgaykhoihanhvanchuyen2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaykhoihanhvanchuyen2.SizeF = new System.Drawing.SizeF(85.41681F, 10.25009F);
            this.lblNgaykhoihanhvanchuyen2.StylePriority.UseFont = false;
            this.lblNgaykhoihanhvanchuyen2.Text = "dd/MM/yyyy";
            // 
            // xrLabel120
            // 
            this.xrLabel120.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel120.LocationFloat = new DevExpress.Utils.PointFloat(433.042F, 472.6041F);
            this.xrLabel120.Name = "xrLabel120";
            this.xrLabel120.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel120.SizeF = new System.Drawing.SizeF(13.54166F, 10.25009F);
            this.xrLabel120.StylePriority.UseFont = false;
            this.xrLabel120.StylePriority.UseTextAlignment = false;
            this.xrLabel120.Text = "~";
            this.xrLabel120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel121
            // 
            this.xrLabel121.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel121.LocationFloat = new DevExpress.Utils.PointFloat(433.042F, 461.6041F);
            this.xrLabel121.Name = "xrLabel121";
            this.xrLabel121.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel121.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.xrLabel121.StylePriority.UseFont = false;
            this.xrLabel121.StylePriority.UseTextAlignment = false;
            this.xrLabel121.Text = "~";
            this.xrLabel121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgaydukiendentrungchuyen3
            // 
            this.lblNgaydukiendentrungchuyen3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaydukiendentrungchuyen3.LocationFloat = new DevExpress.Utils.PointFloat(321.4582F, 472.6041F);
            this.lblNgaydukiendentrungchuyen3.Name = "lblNgaydukiendentrungchuyen3";
            this.lblNgaydukiendentrungchuyen3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaydukiendentrungchuyen3.SizeF = new System.Drawing.SizeF(85.41681F, 11.37497F);
            this.lblNgaydukiendentrungchuyen3.StylePriority.UseFont = false;
            this.lblNgaydukiendentrungchuyen3.Text = "dd/MM/yyyy";
            // 
            // lblNgaydukiendentrungchuyen2
            // 
            this.lblNgaydukiendentrungchuyen2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaydukiendentrungchuyen2.LocationFloat = new DevExpress.Utils.PointFloat(321.4582F, 460.8541F);
            this.lblNgaydukiendentrungchuyen2.Name = "lblNgaydukiendentrungchuyen2";
            this.lblNgaydukiendentrungchuyen2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaydukiendentrungchuyen2.SizeF = new System.Drawing.SizeF(85.41681F, 11.37497F);
            this.lblNgaydukiendentrungchuyen2.StylePriority.UseFont = false;
            this.lblNgaydukiendentrungchuyen2.Text = "dd/MM/yyyy";
            // 
            // xrLabel117
            // 
            this.xrLabel117.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel117.LocationFloat = new DevExpress.Utils.PointFloat(172.5F, 471.8541F);
            this.xrLabel117.Name = "xrLabel117";
            this.xrLabel117.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel117.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.xrLabel117.StylePriority.UseFont = false;
            this.xrLabel117.StylePriority.UseTextAlignment = false;
            this.xrLabel117.Text = "3";
            this.xrLabel117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblDiadiemdich
            // 
            this.lblDiadiemdich.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiadiemdich.LocationFloat = new DevExpress.Utils.PointFloat(196.4583F, 483.979F);
            this.lblDiadiemdich.Name = "lblDiadiemdich";
            this.lblDiadiemdich.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiadiemdich.SizeF = new System.Drawing.SizeF(85.41681F, 12.4165F);
            this.lblDiadiemdich.StylePriority.UseFont = false;
            this.lblDiadiemdich.Text = "XXXE";
            // 
            // lblDiadiem2
            // 
            this.lblDiadiem2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiadiem2.LocationFloat = new DevExpress.Utils.PointFloat(196.4583F, 461.2291F);
            this.lblDiadiem2.Name = "lblDiadiem2";
            this.lblDiadiem2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiadiem2.SizeF = new System.Drawing.SizeF(85.41681F, 11.37497F);
            this.lblDiadiem2.StylePriority.UseFont = false;
            this.lblDiadiem2.Text = "XXXE";
            // 
            // xrLabel114
            // 
            this.xrLabel114.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel114.LocationFloat = new DevExpress.Utils.PointFloat(172.5F, 460.8541F);
            this.xrLabel114.Name = "xrLabel114";
            this.xrLabel114.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel114.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.xrLabel114.StylePriority.UseFont = false;
            this.xrLabel114.StylePriority.UseTextAlignment = false;
            this.xrLabel114.Text = "2";
            this.xrLabel114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel113
            // 
            this.xrLabel113.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel113.LocationFloat = new DevExpress.Utils.PointFloat(433.042F, 450.2291F);
            this.xrLabel113.Name = "xrLabel113";
            this.xrLabel113.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel113.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.xrLabel113.StylePriority.UseFont = false;
            this.xrLabel113.StylePriority.UseTextAlignment = false;
            this.xrLabel113.Text = "~";
            this.xrLabel113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgaykhoihanhvanchuyen
            // 
            this.lblNgaykhoihanhvanchuyen.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaykhoihanhvanchuyen.LocationFloat = new DevExpress.Utils.PointFloat(469.7085F, 449.4792F);
            this.lblNgaykhoihanhvanchuyen.Name = "lblNgaykhoihanhvanchuyen";
            this.lblNgaykhoihanhvanchuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaykhoihanhvanchuyen.SizeF = new System.Drawing.SizeF(85.41681F, 11.37497F);
            this.lblNgaykhoihanhvanchuyen.StylePriority.UseFont = false;
            this.lblNgaykhoihanhvanchuyen.Text = "dd/MM/yyyy";
            // 
            // lblNgaydukiendentrungchuyen
            // 
            this.lblNgaydukiendentrungchuyen.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaydukiendentrungchuyen.LocationFloat = new DevExpress.Utils.PointFloat(321.4583F, 449.4792F);
            this.lblNgaydukiendentrungchuyen.Name = "lblNgaydukiendentrungchuyen";
            this.lblNgaydukiendentrungchuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaydukiendentrungchuyen.SizeF = new System.Drawing.SizeF(85.41681F, 11.37497F);
            this.lblNgaydukiendentrungchuyen.StylePriority.UseFont = false;
            this.lblNgaydukiendentrungchuyen.Text = "dd/MM/yyyy";
            // 
            // lblDiadiem
            // 
            this.lblDiadiem.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiadiem.LocationFloat = new DevExpress.Utils.PointFloat(196.4583F, 449.8541F);
            this.lblDiadiem.Name = "lblDiadiem";
            this.lblDiadiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiadiem.SizeF = new System.Drawing.SizeF(85.41681F, 11.37497F);
            this.lblDiadiem.StylePriority.UseFont = false;
            this.lblDiadiem.Text = "XXXE";
            // 
            // xrLabel109
            // 
            this.xrLabel109.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel109.LocationFloat = new DevExpress.Utils.PointFloat(172.5F, 449.8541F);
            this.xrLabel109.Name = "xrLabel109";
            this.xrLabel109.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel109.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.xrLabel109.StylePriority.UseFont = false;
            this.xrLabel109.StylePriority.UseTextAlignment = false;
            this.xrLabel109.Text = "1";
            this.xrLabel109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel108
            // 
            this.xrLabel108.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel108.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 450.2291F);
            this.xrLabel108.Name = "xrLabel108";
            this.xrLabel108.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel108.SizeF = new System.Drawing.SizeF(124.5833F, 12.4166F);
            this.xrLabel108.StylePriority.UseFont = false;
            this.xrLabel108.Text = "Thông tin trung chuyển";
            // 
            // xrLabel107
            // 
            this.xrLabel107.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel107.LocationFloat = new DevExpress.Utils.PointFloat(469.7084F, 431.4791F);
            this.xrLabel107.Name = "xrLabel107";
            this.xrLabel107.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel107.SizeF = new System.Drawing.SizeF(85.41681F, 11.37497F);
            this.xrLabel107.StylePriority.UseFont = false;
            this.xrLabel107.Text = "Ngày khởi hành";
            // 
            // xrLabel106
            // 
            this.xrLabel106.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel106.LocationFloat = new DevExpress.Utils.PointFloat(321.4583F, 431.4791F);
            this.xrLabel106.Name = "xrLabel106";
            this.xrLabel106.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel106.SizeF = new System.Drawing.SizeF(85.41681F, 11.37497F);
            this.xrLabel106.StylePriority.UseFont = false;
            this.xrLabel106.Text = "Ngày đến";
            // 
            // xrLabel104
            // 
            this.xrLabel104.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel104.LocationFloat = new DevExpress.Utils.PointFloat(196.4583F, 431.4791F);
            this.xrLabel104.Name = "xrLabel104";
            this.xrLabel104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel104.SizeF = new System.Drawing.SizeF(85.41681F, 11.37497F);
            this.xrLabel104.StylePriority.UseFont = false;
            this.xrLabel104.Text = "Địa điểm";
            // 
            // lblNgaykhoihanh
            // 
            this.lblNgaykhoihanh.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaykhoihanh.LocationFloat = new DevExpress.Utils.PointFloat(264.1665F, 420.1042F);
            this.lblNgaykhoihanh.Name = "lblNgaykhoihanh";
            this.lblNgaykhoihanh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaykhoihanh.SizeF = new System.Drawing.SizeF(85.41681F, 11.37497F);
            this.lblNgaykhoihanh.StylePriority.UseFont = false;
            this.lblNgaykhoihanh.Text = "dd/MM/yyyy";
            // 
            // xrLabel102
            // 
            this.xrLabel102.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel102.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 420.1042F);
            this.xrLabel102.Name = "xrLabel102";
            this.xrLabel102.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel102.SizeF = new System.Drawing.SizeF(254.1665F, 11.37497F);
            this.xrLabel102.StylePriority.UseFont = false;
            this.xrLabel102.Text = "Thời hạn cho phép vận chuyển bảo thuế (khởi hành)";
            // 
            // xrLabel89
            // 
            this.xrLabel89.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel89.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 408.5833F);
            this.xrLabel89.Name = "xrLabel89";
            this.xrLabel89.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel89.SizeF = new System.Drawing.SizeF(148.9583F, 11.52084F);
            this.xrLabel89.StylePriority.UseFont = false;
            this.xrLabel89.Text = "Mục thông báo của Hải quan";
            // 
            // xrLine11
            // 
            this.xrLine11.LocationFloat = new DevExpress.Utils.PointFloat(7.947286E-05F, 406.5833F);
            this.xrLine11.Name = "xrLine11";
            this.xrLine11.SizeF = new System.Drawing.SizeF(734.9999F, 2F);
            // 
            // lblSoquanlynguoisudung
            // 
            this.lblSoquanlynguoisudung.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoquanlynguoisudung.LocationFloat = new DevExpress.Utils.PointFloat(593.1254F, 395.5833F);
            this.lblSoquanlynguoisudung.Name = "lblSoquanlynguoisudung";
            this.lblSoquanlynguoisudung.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoquanlynguoisudung.SizeF = new System.Drawing.SizeF(127.9163F, 11F);
            this.lblSoquanlynguoisudung.StylePriority.UseFont = false;
            this.lblSoquanlynguoisudung.Text = "XXXXE";
            // 
            // xrLabel103
            // 
            this.xrLabel103.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel103.LocationFloat = new DevExpress.Utils.PointFloat(423.3337F, 395.5833F);
            this.xrLabel103.Name = "xrLabel103";
            this.xrLabel103.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel103.SizeF = new System.Drawing.SizeF(169.7916F, 11F);
            this.xrLabel103.StylePriority.UseFont = false;
            this.xrLabel103.Text = "Số quản lý người sử dụng";
            // 
            // lblSoquanlynoibodoanhnghiep
            // 
            this.lblSoquanlynoibodoanhnghiep.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoquanlynoibodoanhnghiep.LocationFloat = new DevExpress.Utils.PointFloat(179.7916F, 395.5833F);
            this.lblSoquanlynoibodoanhnghiep.Name = "lblSoquanlynoibodoanhnghiep";
            this.lblSoquanlynoibodoanhnghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoquanlynoibodoanhnghiep.SizeF = new System.Drawing.SizeF(169.7916F, 11F);
            this.lblSoquanlynoibodoanhnghiep.StylePriority.UseFont = false;
            this.lblSoquanlynoibodoanhnghiep.Text = "XXXXXXXXX1XXXXXXXXXE";
            // 
            // xrLabel93
            // 
            this.xrLabel93.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel93.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 395.5833F);
            this.xrLabel93.Name = "xrLabel93";
            this.xrLabel93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel93.SizeF = new System.Drawing.SizeF(169.7916F, 11F);
            this.xrLabel93.StylePriority.UseFont = false;
            this.xrLabel93.Text = "Số quản lý của nội bộ doanh nghiệp";
            // 
            // lblPhanghichu
            // 
            this.lblPhanghichu.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanghichu.LocationFloat = new DevExpress.Utils.PointFloat(98.54167F, 366.5833F);
            this.lblPhanghichu.Multiline = true;
            this.lblPhanghichu.Name = "lblPhanghichu";
            this.lblPhanghichu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanghichu.SizeF = new System.Drawing.SizeF(622.5F, 29.00001F);
            this.lblPhanghichu.StylePriority.UseFont = false;
            this.lblPhanghichu.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            // 
            // xrLabel105
            // 
            this.xrLabel105.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel105.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 366.5833F);
            this.xrLabel105.Name = "xrLabel105";
            this.xrLabel105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel105.SizeF = new System.Drawing.SizeF(88.54166F, 11F);
            this.xrLabel105.StylePriority.UseFont = false;
            this.xrLabel105.Text = "Phần ghi chú";
            // 
            // lblSodinhkemkhaibao3
            // 
            this.lblSodinhkemkhaibao3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSodinhkemkhaibao3.LocationFloat = new DevExpress.Utils.PointFloat(606.5837F, 352.0836F);
            this.lblSodinhkemkhaibao3.Name = "lblSodinhkemkhaibao3";
            this.lblSodinhkemkhaibao3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSodinhkemkhaibao3.SizeF = new System.Drawing.SizeF(105.5831F, 10.99994F);
            this.lblSodinhkemkhaibao3.StylePriority.UseFont = false;
            this.lblSodinhkemkhaibao3.Text = "NNNNNNNNN1NE";
            // 
            // xrLabel99
            // 
            this.xrLabel99.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel99.LocationFloat = new DevExpress.Utils.PointFloat(593.1254F, 352.0415F);
            this.xrLabel99.Multiline = true;
            this.xrLabel99.Name = "xrLabel99";
            this.xrLabel99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel99.SizeF = new System.Drawing.SizeF(13.45831F, 11.00006F);
            this.xrLabel99.StylePriority.UseFont = false;
            this.xrLabel99.StylePriority.UseTextAlignment = false;
            this.xrLabel99.Text = "-";
            this.xrLabel99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblPhanloaidinhkemkhaibaodientu3
            // 
            this.lblPhanloaidinhkemkhaibaodientu3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanloaidinhkemkhaibaodientu3.LocationFloat = new DevExpress.Utils.PointFloat(564.2503F, 352.0416F);
            this.lblPhanloaidinhkemkhaibaodientu3.Name = "lblPhanloaidinhkemkhaibaodientu3";
            this.lblPhanloaidinhkemkhaibaodientu3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanloaidinhkemkhaibaodientu3.SizeF = new System.Drawing.SizeF(28.87506F, 10.99994F);
            this.lblPhanloaidinhkemkhaibaodientu3.StylePriority.UseFont = false;
            this.lblPhanloaidinhkemkhaibaodientu3.Text = "XXE";
            // 
            // xrLabel98
            // 
            this.xrLabel98.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel98.LocationFloat = new DevExpress.Utils.PointFloat(550.2088F, 352.0416F);
            this.xrLabel98.Name = "xrLabel98";
            this.xrLabel98.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel98.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.xrLabel98.StylePriority.UseFont = false;
            this.xrLabel98.StylePriority.UseTextAlignment = false;
            this.xrLabel98.Text = "3";
            this.xrLabel98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSodinhkemkhaibao2
            // 
            this.lblSodinhkemkhaibao2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSodinhkemkhaibao2.LocationFloat = new DevExpress.Utils.PointFloat(433.042F, 352.0836F);
            this.lblSodinhkemkhaibao2.Name = "lblSodinhkemkhaibao2";
            this.lblSodinhkemkhaibao2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSodinhkemkhaibao2.SizeF = new System.Drawing.SizeF(117.1668F, 10.99994F);
            this.lblSodinhkemkhaibao2.StylePriority.UseFont = false;
            this.lblSodinhkemkhaibao2.Text = "NNNNNNNNN1NE";
            // 
            // xrLabel96
            // 
            this.xrLabel96.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel96.LocationFloat = new DevExpress.Utils.PointFloat(419.5837F, 352.0835F);
            this.xrLabel96.Multiline = true;
            this.xrLabel96.Name = "xrLabel96";
            this.xrLabel96.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel96.SizeF = new System.Drawing.SizeF(13.45831F, 11.00006F);
            this.xrLabel96.StylePriority.UseFont = false;
            this.xrLabel96.StylePriority.UseTextAlignment = false;
            this.xrLabel96.Text = "-";
            this.xrLabel96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblPhanloaidinhkemkhaibaodientu2
            // 
            this.lblPhanloaidinhkemkhaibaodientu2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanloaidinhkemkhaibaodientu2.LocationFloat = new DevExpress.Utils.PointFloat(368.1252F, 352.0834F);
            this.lblPhanloaidinhkemkhaibaodientu2.Name = "lblPhanloaidinhkemkhaibaodientu2";
            this.lblPhanloaidinhkemkhaibaodientu2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanloaidinhkemkhaibaodientu2.SizeF = new System.Drawing.SizeF(51.45847F, 10.99994F);
            this.lblPhanloaidinhkemkhaibaodientu2.StylePriority.UseFont = false;
            this.lblPhanloaidinhkemkhaibaodientu2.Text = "XXE";
            // 
            // xrLabel90
            // 
            this.xrLabel90.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel90.LocationFloat = new DevExpress.Utils.PointFloat(354.5836F, 352.0416F);
            this.xrLabel90.Name = "xrLabel90";
            this.xrLabel90.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel90.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.xrLabel90.StylePriority.UseFont = false;
            this.xrLabel90.StylePriority.UseTextAlignment = false;
            this.xrLabel90.Text = "2";
            this.xrLabel90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel94
            // 
            this.xrLabel94.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel94.LocationFloat = new DevExpress.Utils.PointFloat(223.9585F, 352.0834F);
            this.xrLabel94.Multiline = true;
            this.xrLabel94.Name = "xrLabel94";
            this.xrLabel94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel94.SizeF = new System.Drawing.SizeF(13.45831F, 11.00006F);
            this.xrLabel94.StylePriority.UseFont = false;
            this.xrLabel94.StylePriority.UseTextAlignment = false;
            this.xrLabel94.Text = "-";
            this.xrLabel94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblPhanloaidinhkemkhaibaodientu1
            // 
            this.lblPhanloaidinhkemkhaibaodientu1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanloaidinhkemkhaibaodientu1.LocationFloat = new DevExpress.Utils.PointFloat(172.5F, 352.0834F);
            this.lblPhanloaidinhkemkhaibaodientu1.Name = "lblPhanloaidinhkemkhaibaodientu1";
            this.lblPhanloaidinhkemkhaibaodientu1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanloaidinhkemkhaibaodientu1.SizeF = new System.Drawing.SizeF(51.45847F, 10.99994F);
            this.lblPhanloaidinhkemkhaibaodientu1.StylePriority.UseFont = false;
            this.lblPhanloaidinhkemkhaibaodientu1.Text = "XXE";
            // 
            // xrLabel92
            // 
            this.xrLabel92.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel92.LocationFloat = new DevExpress.Utils.PointFloat(158.9583F, 352.0833F);
            this.xrLabel92.Name = "xrLabel92";
            this.xrLabel92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel92.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.xrLabel92.StylePriority.UseFont = false;
            this.xrLabel92.StylePriority.UseTextAlignment = false;
            this.xrLabel92.Text = "1";
            this.xrLabel92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLine10
            // 
            this.xrLine10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 364.5833F);
            this.xrLine10.Name = "xrLine10";
            this.xrLine10.SizeF = new System.Drawing.SizeF(734.9999F, 2F);
            // 
            // xrLabel87
            // 
            this.xrLabel87.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel87.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 352.0833F);
            this.xrLabel87.Name = "xrLabel87";
            this.xrLabel87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel87.SizeF = new System.Drawing.SizeF(148.9583F, 11F);
            this.xrLabel87.StylePriority.UseFont = false;
            this.xrLabel87.Text = "Số đính kèm khai báo điện tử";
            // 
            // lblSodinhkemkhaibao1
            // 
            this.lblSodinhkemkhaibao1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSodinhkemkhaibao1.LocationFloat = new DevExpress.Utils.PointFloat(237.4167F, 352.0417F);
            this.lblSodinhkemkhaibao1.Name = "lblSodinhkemkhaibao1";
            this.lblSodinhkemkhaibao1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSodinhkemkhaibao1.SizeF = new System.Drawing.SizeF(117.1668F, 10.99994F);
            this.lblSodinhkemkhaibao1.StylePriority.UseFont = false;
            this.lblSodinhkemkhaibao1.Text = "NNNNNNNNN1NE";
            // 
            // xrLine9
            // 
            this.xrLine9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 350.0417F);
            this.xrLine9.Name = "xrLine9";
            this.xrLine9.SizeF = new System.Drawing.SizeF(734.9999F, 2F);
            // 
            // lblTongsodonghangtokhai
            // 
            this.lblTongsodonghangtokhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongsodonghangtokhai.LocationFloat = new DevExpress.Utils.PointFloat(541.6669F, 339.0417F);
            this.lblTongsodonghangtokhai.Name = "lblTongsodonghangtokhai";
            this.lblTongsodonghangtokhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongsodonghangtokhai.SizeF = new System.Drawing.SizeF(51.45847F, 10.99994F);
            this.lblTongsodonghangtokhai.StylePriority.UseFont = false;
            this.lblTongsodonghangtokhai.StylePriority.UseTextAlignment = false;
            this.lblTongsodonghangtokhai.Text = "NE";
            this.lblTongsodonghangtokhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel88
            // 
            this.xrLabel88.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel88.LocationFloat = new DevExpress.Utils.PointFloat(387.6671F, 339.0417F);
            this.xrLabel88.Name = "xrLabel88";
            this.xrLabel88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel88.SizeF = new System.Drawing.SizeF(153.9998F, 11F);
            this.xrLabel88.StylePriority.UseFont = false;
            this.xrLabel88.Text = "Tổng số dòng hàng của tờ khai";
            // 
            // lblTongsotokhaicuatrang
            // 
            this.lblTongsotokhaicuatrang.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongsotokhaicuatrang.LocationFloat = new DevExpress.Utils.PointFloat(303.1251F, 339.0417F);
            this.lblTongsotokhaicuatrang.Name = "lblTongsotokhaicuatrang";
            this.lblTongsotokhaicuatrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongsotokhaicuatrang.SizeF = new System.Drawing.SizeF(51.45847F, 10.99994F);
            this.lblTongsotokhaicuatrang.StylePriority.UseFont = false;
            this.lblTongsotokhaicuatrang.StylePriority.UseTextAlignment = false;
            this.lblTongsotokhaicuatrang.Text = "NE";
            this.lblTongsotokhaicuatrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel84
            // 
            this.xrLabel84.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(147.4999F, 339.0417F);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(155.6251F, 11F);
            this.xrLabel84.StylePriority.UseFont = false;
            this.xrLabel84.Text = "Tổng số trang của tờ khai";
            // 
            // xrLine8
            // 
            this.xrLine8.LocationFloat = new DevExpress.Utils.PointFloat(7.947286E-05F, 337.0417F);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.SizeF = new System.Drawing.SizeF(734.9999F, 2F);
            // 
            // lblTongsotienbaolanh
            // 
            this.lblTongsotienbaolanh.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongsotienbaolanh.LocationFloat = new DevExpress.Utils.PointFloat(147.4999F, 326.0416F);
            this.lblTongsotienbaolanh.Name = "lblTongsotienbaolanh";
            this.lblTongsotienbaolanh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongsotienbaolanh.SizeF = new System.Drawing.SizeF(90.625F, 10.99994F);
            this.lblTongsotienbaolanh.StylePriority.UseFont = false;
            this.lblTongsotienbaolanh.Text = "12,345,678,901";
            // 
            // xrLabel83
            // 
            this.xrLabel83.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel83.LocationFloat = new DevExpress.Utils.PointFloat(541.6669F, 315.0415F);
            this.xrLabel83.Name = "xrLabel83";
            this.xrLabel83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel83.SizeF = new System.Drawing.SizeF(51.45847F, 11F);
            this.xrLabel83.StylePriority.UseFont = false;
            this.xrLabel83.Text = "VND";
            // 
            // xrLabel78
            // 
            this.xrLabel78.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(303.1251F, 315.0417F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(147.8333F, 11.00006F);
            this.xrLabel78.StylePriority.UseFont = false;
            this.xrLabel78.Text = "Tổng số tiền lệ phí";
            // 
            // lblMatientetienbaolanh
            // 
            this.lblMatientetienbaolanh.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMatientetienbaolanh.LocationFloat = new DevExpress.Utils.PointFloat(238.1249F, 326.0416F);
            this.lblMatientetienbaolanh.Name = "lblMatientetienbaolanh";
            this.lblMatientetienbaolanh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMatientetienbaolanh.SizeF = new System.Drawing.SizeF(51.45847F, 11F);
            this.lblMatientetienbaolanh.StylePriority.UseFont = false;
            this.lblMatientetienbaolanh.Text = "XXE";
            // 
            // lblMatientetongtienthuexuat
            // 
            this.lblMatientetongtienthuexuat.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMatientetongtienthuexuat.LocationFloat = new DevExpress.Utils.PointFloat(238.1249F, 315.0419F);
            this.lblMatientetongtienthuexuat.Name = "lblMatientetongtienthuexuat";
            this.lblMatientetongtienthuexuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMatientetongtienthuexuat.SizeF = new System.Drawing.SizeF(51.45847F, 10.99994F);
            this.lblMatientetongtienthuexuat.StylePriority.UseFont = false;
            this.lblMatientetongtienthuexuat.Text = "XXE";
            // 
            // lblTongsotienlephi
            // 
            this.lblTongsotienlephi.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongsotienlephi.LocationFloat = new DevExpress.Utils.PointFloat(451.0419F, 315.0416F);
            this.lblTongsotienlephi.Name = "lblTongsotienlephi";
            this.lblTongsotienlephi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongsotienlephi.SizeF = new System.Drawing.SizeF(90.625F, 10.99994F);
            this.lblTongsotienlephi.StylePriority.UseFont = false;
            this.lblTongsotienlephi.Text = "12,345,678,901";
            // 
            // lblTongsotienthuexuatkhau
            // 
            this.lblTongsotienthuexuatkhau.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongsotienthuexuatkhau.LocationFloat = new DevExpress.Utils.PointFloat(147.4999F, 315.0419F);
            this.lblTongsotienthuexuatkhau.Name = "lblTongsotienthuexuatkhau";
            this.lblTongsotienthuexuatkhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongsotienthuexuatkhau.SizeF = new System.Drawing.SizeF(90.625F, 10.99994F);
            this.lblTongsotienthuexuatkhau.StylePriority.UseFont = false;
            this.lblTongsotienthuexuatkhau.Text = "12,345,678,901";
            // 
            // lblMaxacdinhthoihannopthue
            // 
            this.lblMaxacdinhthoihannopthue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxacdinhthoihannopthue.LocationFloat = new DevExpress.Utils.PointFloat(450.9583F, 304.0416F);
            this.lblMaxacdinhthoihannopthue.Name = "lblMaxacdinhthoihannopthue";
            this.lblMaxacdinhthoihannopthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaxacdinhthoihannopthue.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.lblMaxacdinhthoihannopthue.StylePriority.UseFont = false;
            this.lblMaxacdinhthoihannopthue.StylePriority.UseTextAlignment = false;
            this.lblMaxacdinhthoihannopthue.Text = "X";
            this.lblMaxacdinhthoihannopthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel86
            // 
            this.xrLabel86.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel86.LocationFloat = new DevExpress.Utils.PointFloat(481.5836F, 304.0416F);
            this.xrLabel86.Name = "xrLabel86";
            this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel86.SizeF = new System.Drawing.SizeF(109.0415F, 11.00006F);
            this.xrLabel86.StylePriority.UseFont = false;
            this.xrLabel86.Text = "Phân loại nộp thuế";
            // 
            // xrLabel85
            // 
            this.xrLabel85.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel85.LocationFloat = new DevExpress.Utils.PointFloat(303.1251F, 304.0418F);
            this.xrLabel85.Name = "xrLabel85";
            this.xrLabel85.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel85.SizeF = new System.Drawing.SizeF(147.8333F, 11.00006F);
            this.xrLabel85.StylePriority.UseFont = false;
            this.xrLabel85.Text = "Mã xác định thời hạn nộp thuế";
            // 
            // lblPhanloainopthue
            // 
            this.lblPhanloainopthue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanloainopthue.LocationFloat = new DevExpress.Utils.PointFloat(590.625F, 304.0419F);
            this.lblPhanloainopthue.Name = "lblPhanloainopthue";
            this.lblPhanloainopthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanloainopthue.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.lblPhanloainopthue.StylePriority.UseFont = false;
            this.lblPhanloainopthue.StylePriority.UseTextAlignment = false;
            this.lblPhanloainopthue.Text = "X";
            this.lblPhanloainopthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNguoinopthue
            // 
            this.lblNguoinopthue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguoinopthue.LocationFloat = new DevExpress.Utils.PointFloat(289.5834F, 304.0419F);
            this.lblNguoinopthue.Name = "lblNguoinopthue";
            this.lblNguoinopthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNguoinopthue.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.lblNguoinopthue.StylePriority.UseFont = false;
            this.lblNguoinopthue.StylePriority.UseTextAlignment = false;
            this.lblNguoinopthue.Text = "X";
            this.lblNguoinopthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel82
            // 
            this.xrLabel82.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel82.LocationFloat = new DevExpress.Utils.PointFloat(210.4167F, 304.0418F);
            this.xrLabel82.Name = "xrLabel82";
            this.xrLabel82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel82.SizeF = new System.Drawing.SizeF(79.1667F, 11.00006F);
            this.xrLabel82.StylePriority.UseFont = false;
            this.xrLabel82.Text = "Người nộp thuế";
            // 
            // lblPhanloaikhongcanquydoi
            // 
            this.lblPhanloaikhongcanquydoi.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanloaikhongcanquydoi.LocationFloat = new DevExpress.Utils.PointFloat(182.9167F, 304.0419F);
            this.lblPhanloaikhongcanquydoi.Name = "lblPhanloaikhongcanquydoi";
            this.lblPhanloaikhongcanquydoi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanloaikhongcanquydoi.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.lblPhanloaikhongcanquydoi.StylePriority.UseFont = false;
            this.lblPhanloaikhongcanquydoi.StylePriority.UseTextAlignment = false;
            this.lblPhanloaikhongcanquydoi.Text = "X";
            this.lblPhanloaikhongcanquydoi.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel77
            // 
            this.xrLabel77.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 326.0417F);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel77.SizeF = new System.Drawing.SizeF(137.5F, 11F);
            this.xrLabel77.StylePriority.UseFont = false;
            this.xrLabel77.Text = "Số tiền bảo lãnh";
            // 
            // xrLabel75
            // 
            this.xrLabel75.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 315.0417F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(137.5F, 10.99997F);
            this.xrLabel75.StylePriority.UseFont = false;
            this.xrLabel75.Text = "Tổng số tiền thuế xuất khẩu";
            // 
            // xrLabel74
            // 
            this.xrLabel74.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel74.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 304.0417F);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel74.SizeF = new System.Drawing.SizeF(169.7916F, 11.00006F);
            this.xrLabel74.StylePriority.UseFont = false;
            this.xrLabel74.Text = "Phân loại không cần quy đổi VND";
            // 
            // xrLine7
            // 
            this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 302.0417F);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.SizeF = new System.Drawing.SizeF(734.9999F, 2F);
            // 
            // lblMaphanloainhaplieutonggia
            // 
            this.lblMaphanloainhaplieutonggia.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaphanloainhaplieutonggia.LocationFloat = new DevExpress.Utils.PointFloat(590.6251F, 291.0417F);
            this.lblMaphanloainhaplieutonggia.Multiline = true;
            this.lblMaphanloainhaplieutonggia.Name = "lblMaphanloainhaplieutonggia";
            this.lblMaphanloainhaplieutonggia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaphanloainhaplieutonggia.SizeF = new System.Drawing.SizeF(25.08362F, 10.99997F);
            this.lblMaphanloainhaplieutonggia.StylePriority.UseFont = false;
            this.lblMaphanloainhaplieutonggia.Text = "X";
            // 
            // xrLabel71
            // 
            this.xrLabel71.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(577.1667F, 291.0416F);
            this.xrLabel71.Multiline = true;
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel71.SizeF = new System.Drawing.SizeF(13.45837F, 11.00009F);
            this.xrLabel71.StylePriority.UseFont = false;
            this.xrLabel71.StylePriority.UseTextAlignment = false;
            this.xrLabel71.Text = "-";
            this.xrLabel71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTonghesophanbothue
            // 
            this.lblTonghesophanbothue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTonghesophanbothue.LocationFloat = new DevExpress.Utils.PointFloat(437.5002F, 291.0416F);
            this.lblTonghesophanbothue.Multiline = true;
            this.lblTonghesophanbothue.Name = "lblTonghesophanbothue";
            this.lblTonghesophanbothue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTonghesophanbothue.SizeF = new System.Drawing.SizeF(139.5832F, 10.99997F);
            this.lblTonghesophanbothue.StylePriority.UseFont = false;
            this.lblTonghesophanbothue.StylePriority.UseTextAlignment = false;
            this.lblTonghesophanbothue.Text = "12,345,678,901,234,567,890";
            this.lblTonghesophanbothue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel81
            // 
            this.xrLabel81.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel81.LocationFloat = new DevExpress.Utils.PointFloat(248.5417F, 291.0416F);
            this.xrLabel81.Name = "xrLabel81";
            this.xrLabel81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel81.SizeF = new System.Drawing.SizeF(188.9585F, 10.36108F);
            this.xrLabel81.StylePriority.UseFont = false;
            this.xrLabel81.Text = "Tổng hệ số phân bổ trị giá tính thuế";
            // 
            // lblTygiatinhthue2
            // 
            this.lblTygiatinhthue2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTygiatinhthue2.LocationFloat = new DevExpress.Utils.PointFloat(555.1252F, 281.3225F);
            this.lblTygiatinhthue2.Multiline = true;
            this.lblTygiatinhthue2.Name = "lblTygiatinhthue2";
            this.lblTygiatinhthue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTygiatinhthue2.SizeF = new System.Drawing.SizeF(60.58337F, 9.719177F);
            this.lblTygiatinhthue2.StylePriority.UseFont = false;
            this.lblTygiatinhthue2.StylePriority.UseTextAlignment = false;
            this.lblTygiatinhthue2.Text = "123456789";
            this.lblTygiatinhthue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel79
            // 
            this.xrLabel79.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(541.6669F, 281.3225F);
            this.xrLabel79.Multiline = true;
            this.xrLabel79.Name = "xrLabel79";
            this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel79.SizeF = new System.Drawing.SizeF(13.45837F, 9.719086F);
            this.xrLabel79.StylePriority.UseFont = false;
            this.xrLabel79.StylePriority.UseTextAlignment = false;
            this.xrLabel79.Text = "-";
            this.xrLabel79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMadongtientygiathue2
            // 
            this.lblMadongtientygiathue2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadongtientygiathue2.LocationFloat = new DevExpress.Utils.PointFloat(511.0418F, 281.3225F);
            this.lblMadongtientygiathue2.Multiline = true;
            this.lblMadongtientygiathue2.Name = "lblMadongtientygiathue2";
            this.lblMadongtientygiathue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadongtientygiathue2.SizeF = new System.Drawing.SizeF(30.62509F, 9.719086F);
            this.lblMadongtientygiathue2.StylePriority.UseFont = false;
            this.lblMadongtientygiathue2.Text = "XXE";
            // 
            // lblTygiatinhthue1
            // 
            this.lblTygiatinhthue1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTygiatinhthue1.LocationFloat = new DevExpress.Utils.PointFloat(450.9583F, 281.3226F);
            this.lblTygiatinhthue1.Multiline = true;
            this.lblTygiatinhthue1.Name = "lblTygiatinhthue1";
            this.lblTygiatinhthue1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTygiatinhthue1.SizeF = new System.Drawing.SizeF(60.08344F, 9.719086F);
            this.lblTygiatinhthue1.StylePriority.UseFont = false;
            this.lblTygiatinhthue1.StylePriority.UseTextAlignment = false;
            this.lblTygiatinhthue1.Text = "123456789";
            this.lblTygiatinhthue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel76
            // 
            this.xrLabel76.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(437.5002F, 280.0415F);
            this.xrLabel76.Multiline = true;
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(13.45831F, 11.00009F);
            this.xrLabel76.StylePriority.UseFont = false;
            this.xrLabel76.StylePriority.UseTextAlignment = false;
            this.xrLabel76.Text = "-";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMadongtientygiathue1
            // 
            this.lblMadongtientygiathue1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadongtientygiathue1.LocationFloat = new DevExpress.Utils.PointFloat(406.8751F, 280.0416F);
            this.lblMadongtientygiathue1.Multiline = true;
            this.lblMadongtientygiathue1.Name = "lblMadongtientygiathue1";
            this.lblMadongtientygiathue1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadongtientygiathue1.SizeF = new System.Drawing.SizeF(30.62509F, 11.00006F);
            this.lblMadongtientygiathue1.StylePriority.UseFont = false;
            this.lblMadongtientygiathue1.Text = "XXE";
            // 
            // xrLabel70
            // 
            this.xrLabel70.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel70.LocationFloat = new DevExpress.Utils.PointFloat(248.5416F, 280.0416F);
            this.xrLabel70.Name = "xrLabel70";
            this.xrLabel70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel70.SizeF = new System.Drawing.SizeF(158.3333F, 11.00006F);
            this.xrLabel70.StylePriority.UseFont = false;
            this.xrLabel70.Text = "Tỷ giá tính thuế";
            // 
            // lblTongthue
            // 
            this.lblTongthue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongthue.LocationFloat = new DevExpress.Utils.PointFloat(495.0418F, 270.321F);
            this.lblTongthue.Multiline = true;
            this.lblTongthue.Name = "lblTongthue";
            this.lblTongthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongthue.SizeF = new System.Drawing.SizeF(141.5001F, 11.00153F);
            this.lblTongthue.StylePriority.UseFont = false;
            this.lblTongthue.StylePriority.UseTextAlignment = false;
            this.lblTongthue.Text = "12,345,678,901,234,567,890";
            this.lblTongthue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel73
            // 
            this.xrLabel73.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(481.5834F, 270.321F);
            this.xrLabel73.Multiline = true;
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(13.45837F, 11.00153F);
            this.xrLabel73.StylePriority.UseFont = false;
            this.xrLabel73.StylePriority.UseTextAlignment = false;
            this.xrLabel73.Text = "-";
            this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMadongtientongthue
            // 
            this.lblMadongtientongthue.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadongtientongthue.LocationFloat = new DevExpress.Utils.PointFloat(450.9583F, 270.321F);
            this.lblMadongtientongthue.Multiline = true;
            this.lblMadongtientongthue.Name = "lblMadongtientongthue";
            this.lblMadongtientongthue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadongtientongthue.SizeF = new System.Drawing.SizeF(30.62509F, 11.00153F);
            this.lblMadongtientongthue.StylePriority.UseFont = false;
            this.lblMadongtientongthue.Text = "XXE";
            // 
            // xrLabel54
            // 
            this.xrLabel54.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(248.5416F, 270.321F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(202.4167F, 9.720612F);
            this.xrLabel54.StylePriority.UseFont = false;
            this.xrLabel54.Text = "Tổng trị giá tính thuế";
            // 
            // lblMaphanloaigiahoadon
            // 
            this.lblMaphanloaigiahoadon.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaphanloaigiahoadon.LocationFloat = new DevExpress.Utils.PointFloat(650.0002F, 258.0414F);
            this.lblMaphanloaigiahoadon.Multiline = true;
            this.lblMaphanloaigiahoadon.Name = "lblMaphanloaigiahoadon";
            this.lblMaphanloaigiahoadon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaphanloaigiahoadon.SizeF = new System.Drawing.SizeF(30.62512F, 11.00012F);
            this.lblMaphanloaigiahoadon.StylePriority.UseFont = false;
            this.lblMaphanloaigiahoadon.Text = "X";
            // 
            // xrLabel64
            // 
            this.xrLabel64.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(636.5419F, 258.0415F);
            this.xrLabel64.Multiline = true;
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(13.45837F, 11F);
            this.xrLabel64.StylePriority.UseFont = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "-";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongtrigiahoadon
            // 
            this.lblTongtrigiahoadon.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongtrigiahoadon.LocationFloat = new DevExpress.Utils.PointFloat(495.0418F, 258.0415F);
            this.lblTongtrigiahoadon.Multiline = true;
            this.lblTongtrigiahoadon.Name = "lblTongtrigiahoadon";
            this.lblTongtrigiahoadon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongtrigiahoadon.SizeF = new System.Drawing.SizeF(141.5001F, 11F);
            this.lblTongtrigiahoadon.StylePriority.UseFont = false;
            this.lblTongtrigiahoadon.StylePriority.UseTextAlignment = false;
            this.lblTongtrigiahoadon.Text = "12,345,678,901,234,567,890";
            this.lblTongtrigiahoadon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel59
            // 
            this.xrLabel59.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(481.5836F, 258.0415F);
            this.xrLabel59.Multiline = true;
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(13.45837F, 11.00006F);
            this.xrLabel59.StylePriority.UseFont = false;
            this.xrLabel59.StylePriority.UseTextAlignment = false;
            this.xrLabel59.Text = "-";
            this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMadongtienhoadon
            // 
            this.lblMadongtienhoadon.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadongtienhoadon.LocationFloat = new DevExpress.Utils.PointFloat(450.9585F, 258.0415F);
            this.lblMadongtienhoadon.Multiline = true;
            this.lblMadongtienhoadon.Name = "lblMadongtienhoadon";
            this.lblMadongtienhoadon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadongtienhoadon.SizeF = new System.Drawing.SizeF(30.62509F, 11.00006F);
            this.lblMadongtienhoadon.StylePriority.UseFont = false;
            this.lblMadongtienhoadon.Text = "XXE";
            // 
            // xrLabel49
            // 
            this.xrLabel49.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(437.5002F, 258.0415F);
            this.xrLabel49.Multiline = true;
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(13.45831F, 11.00006F);
            this.xrLabel49.StylePriority.UseFont = false;
            this.xrLabel49.StylePriority.UseTextAlignment = false;
            this.xrLabel49.Text = "-";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMadkgiahoadon
            // 
            this.lblMadkgiahoadon.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadkgiahoadon.LocationFloat = new DevExpress.Utils.PointFloat(406.8748F, 258.0415F);
            this.lblMadkgiahoadon.Multiline = true;
            this.lblMadkgiahoadon.Name = "lblMadkgiahoadon";
            this.lblMadkgiahoadon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadkgiahoadon.SizeF = new System.Drawing.SizeF(30.62512F, 11.00006F);
            this.lblMadkgiahoadon.StylePriority.UseFont = false;
            this.lblMadkgiahoadon.Text = "XXE";
            // 
            // lblPhuongthucthanhtoan
            // 
            this.lblPhuongthucthanhtoan.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhuongthucthanhtoan.LocationFloat = new DevExpress.Utils.PointFloat(406.875F, 247.0416F);
            this.lblPhuongthucthanhtoan.Multiline = true;
            this.lblPhuongthucthanhtoan.Name = "lblPhuongthucthanhtoan";
            this.lblPhuongthucthanhtoan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhuongthucthanhtoan.SizeF = new System.Drawing.SizeF(74.70844F, 11.00005F);
            this.lblPhuongthucthanhtoan.StylePriority.UseFont = false;
            this.lblPhuongthucthanhtoan.Text = "XXXXXXE";
            // 
            // lblNgayphathanh
            // 
            this.lblNgayphathanh.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayphathanh.LocationFloat = new DevExpress.Utils.PointFloat(406.875F, 236.0416F);
            this.lblNgayphathanh.Multiline = true;
            this.lblNgayphathanh.Name = "lblNgayphathanh";
            this.lblNgayphathanh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayphathanh.SizeF = new System.Drawing.SizeF(70.20821F, 11.00002F);
            this.lblNgayphathanh.StylePriority.UseFont = false;
            this.lblNgayphathanh.Text = "dd/MM/yyyy";
            // 
            // xrLabel67
            // 
            this.xrLabel67.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(248.5416F, 236.0416F);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(158.3334F, 11F);
            this.xrLabel67.StylePriority.UseFont = false;
            this.xrLabel67.Text = "Ngày phát hành";
            // 
            // xrLabel68
            // 
            this.xrLabel68.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(248.5418F, 214.0416F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(158.3333F, 11F);
            this.xrLabel68.StylePriority.UseFont = false;
            this.xrLabel68.Text = "Số hóa đơn";
            // 
            // xrLabel69
            // 
            this.xrLabel69.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(248.5416F, 225.0416F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(158.3334F, 11F);
            this.xrLabel69.StylePriority.UseFont = false;
            this.xrLabel69.Text = "Số tiếp nhận hóa đơn điện tử";
            // 
            // lblSohoadon
            // 
            this.lblSohoadon.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSohoadon.LocationFloat = new DevExpress.Utils.PointFloat(433.5417F, 214.0416F);
            this.lblSohoadon.Name = "lblSohoadon";
            this.lblSohoadon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSohoadon.SizeF = new System.Drawing.SizeF(301.4583F, 11F);
            this.lblSohoadon.StylePriority.UseFont = false;
            this.lblSohoadon.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            // 
            // xrLabel66
            // 
            this.xrLabel66.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(248.5416F, 247.0416F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(158.3334F, 10.99998F);
            this.xrLabel66.StylePriority.UseFont = false;
            this.xrLabel66.Text = "Phương thức thanh toán";
            // 
            // lblSotiepnhanhoadondientu
            // 
            this.lblSotiepnhanhoadondientu.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotiepnhanhoadondientu.LocationFloat = new DevExpress.Utils.PointFloat(406.875F, 225.0416F);
            this.lblSotiepnhanhoadondientu.Multiline = true;
            this.lblSotiepnhanhoadondientu.Name = "lblSotiepnhanhoadondientu";
            this.lblSotiepnhanhoadondientu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotiepnhanhoadondientu.SizeF = new System.Drawing.SizeF(131.2502F, 11F);
            this.lblSotiepnhanhoadondientu.StylePriority.UseFont = false;
            this.lblSotiepnhanhoadondientu.Text = "NNNNNNNNN1NE";
            // 
            // xrLabel72
            // 
            this.xrLabel72.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(248.5416F, 258.0415F);
            this.xrLabel72.Name = "xrLabel72";
            this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel72.SizeF = new System.Drawing.SizeF(158.3333F, 11.00006F);
            this.xrLabel72.StylePriority.UseFont = false;
            this.xrLabel72.Text = "Tổng trị giá hóa đơn";
            // 
            // lblSogiayphep5
            // 
            this.lblSogiayphep5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSogiayphep5.LocationFloat = new DevExpress.Utils.PointFloat(61.87512F, 269.0416F);
            this.lblSogiayphep5.Name = "lblSogiayphep5";
            this.lblSogiayphep5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSogiayphep5.SizeF = new System.Drawing.SizeF(176.2499F, 11F);
            this.lblSogiayphep5.StylePriority.UseFont = false;
            this.lblSogiayphep5.Text = "XXXXXXXXX1XXXXXXXXXE";
            // 
            // lblSogiayphep4
            // 
            this.lblSogiayphep4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSogiayphep4.LocationFloat = new DevExpress.Utils.PointFloat(61.87512F, 258.0416F);
            this.lblSogiayphep4.Name = "lblSogiayphep4";
            this.lblSogiayphep4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSogiayphep4.SizeF = new System.Drawing.SizeF(176.2498F, 11F);
            this.lblSogiayphep4.StylePriority.UseFont = false;
            this.lblSogiayphep4.Text = "XXXXXXXXX1XXXXXXXXXE";
            // 
            // lblSogiayphep3
            // 
            this.lblSogiayphep3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSogiayphep3.LocationFloat = new DevExpress.Utils.PointFloat(61.87512F, 247.0416F);
            this.lblSogiayphep3.Name = "lblSogiayphep3";
            this.lblSogiayphep3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSogiayphep3.SizeF = new System.Drawing.SizeF(176.2498F, 11.00005F);
            this.lblSogiayphep3.StylePriority.UseFont = false;
            this.lblSogiayphep3.Text = "XXXXXXXXX1XXXXXXXXXE";
            // 
            // lblSogiayphep2
            // 
            this.lblSogiayphep2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSogiayphep2.LocationFloat = new DevExpress.Utils.PointFloat(61.87512F, 236.0416F);
            this.lblSogiayphep2.Name = "lblSogiayphep2";
            this.lblSogiayphep2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSogiayphep2.SizeF = new System.Drawing.SizeF(176.2499F, 11F);
            this.lblSogiayphep2.StylePriority.UseFont = false;
            this.lblSogiayphep2.Text = "XXXXXXXXX1XXXXXXXXXE";
            // 
            // lblSogiayphep1
            // 
            this.lblSogiayphep1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSogiayphep1.LocationFloat = new DevExpress.Utils.PointFloat(61.87512F, 225.0416F);
            this.lblSogiayphep1.Name = "lblSogiayphep1";
            this.lblSogiayphep1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSogiayphep1.SizeF = new System.Drawing.SizeF(176.2499F, 11F);
            this.lblSogiayphep1.StylePriority.UseFont = false;
            this.lblSogiayphep1.Text = "XXXXXXXXX1XXXXXXXXXE";
            // 
            // lblPhanloaigiayphepxuatkhau5
            // 
            this.lblPhanloaigiayphepxuatkhau5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanloaigiayphepxuatkhau5.LocationFloat = new DevExpress.Utils.PointFloat(26.04171F, 269.0416F);
            this.lblPhanloaigiayphepxuatkhau5.Multiline = true;
            this.lblPhanloaigiayphepxuatkhau5.Name = "lblPhanloaigiayphepxuatkhau5";
            this.lblPhanloaigiayphepxuatkhau5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanloaigiayphepxuatkhau5.SizeF = new System.Drawing.SizeF(35.83342F, 11F);
            this.lblPhanloaigiayphepxuatkhau5.StylePriority.UseFont = false;
            this.lblPhanloaigiayphepxuatkhau5.Text = "XXXE";
            // 
            // lblPhanloaigiayphepxuatkhau4
            // 
            this.lblPhanloaigiayphepxuatkhau4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanloaigiayphepxuatkhau4.LocationFloat = new DevExpress.Utils.PointFloat(26.04171F, 258.0416F);
            this.lblPhanloaigiayphepxuatkhau4.Multiline = true;
            this.lblPhanloaigiayphepxuatkhau4.Name = "lblPhanloaigiayphepxuatkhau4";
            this.lblPhanloaigiayphepxuatkhau4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanloaigiayphepxuatkhau4.SizeF = new System.Drawing.SizeF(35.83341F, 11F);
            this.lblPhanloaigiayphepxuatkhau4.StylePriority.UseFont = false;
            this.lblPhanloaigiayphepxuatkhau4.Text = "XXXE";
            // 
            // lblPhanloaigiayphepxuatkhau3
            // 
            this.lblPhanloaigiayphepxuatkhau3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanloaigiayphepxuatkhau3.LocationFloat = new DevExpress.Utils.PointFloat(26.04171F, 247.0416F);
            this.lblPhanloaigiayphepxuatkhau3.Multiline = true;
            this.lblPhanloaigiayphepxuatkhau3.Name = "lblPhanloaigiayphepxuatkhau3";
            this.lblPhanloaigiayphepxuatkhau3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanloaigiayphepxuatkhau3.SizeF = new System.Drawing.SizeF(35.83341F, 11.00005F);
            this.lblPhanloaigiayphepxuatkhau3.StylePriority.UseFont = false;
            this.lblPhanloaigiayphepxuatkhau3.Text = "XXXE";
            // 
            // lblPhanloaigiayphepxuatkhau2
            // 
            this.lblPhanloaigiayphepxuatkhau2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanloaigiayphepxuatkhau2.LocationFloat = new DevExpress.Utils.PointFloat(26.04171F, 236.0416F);
            this.lblPhanloaigiayphepxuatkhau2.Multiline = true;
            this.lblPhanloaigiayphepxuatkhau2.Name = "lblPhanloaigiayphepxuatkhau2";
            this.lblPhanloaigiayphepxuatkhau2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanloaigiayphepxuatkhau2.SizeF = new System.Drawing.SizeF(35.83341F, 11F);
            this.lblPhanloaigiayphepxuatkhau2.StylePriority.UseFont = false;
            this.lblPhanloaigiayphepxuatkhau2.Text = "XXXE";
            // 
            // lblPhanloaigiayphepxuatkhau1
            // 
            this.lblPhanloaigiayphepxuatkhau1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanloaigiayphepxuatkhau1.LocationFloat = new DevExpress.Utils.PointFloat(26.04171F, 225.0416F);
            this.lblPhanloaigiayphepxuatkhau1.Multiline = true;
            this.lblPhanloaigiayphepxuatkhau1.Name = "lblPhanloaigiayphepxuatkhau1";
            this.lblPhanloaigiayphepxuatkhau1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanloaigiayphepxuatkhau1.SizeF = new System.Drawing.SizeF(35.83341F, 11F);
            this.lblPhanloaigiayphepxuatkhau1.StylePriority.UseFont = false;
            this.lblPhanloaigiayphepxuatkhau1.Text = "XXXE";
            // 
            // lblSTT5
            // 
            this.lblSTT5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSTT5.LocationFloat = new DevExpress.Utils.PointFloat(12.50005F, 269.0416F);
            this.lblSTT5.Name = "lblSTT5";
            this.lblSTT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTT5.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.lblSTT5.StylePriority.UseFont = false;
            this.lblSTT5.StylePriority.UseTextAlignment = false;
            this.lblSTT5.Text = "5";
            this.lblSTT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSTT3
            // 
            this.lblSTT3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSTT3.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 247.0416F);
            this.lblSTT3.Name = "lblSTT3";
            this.lblSTT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTT3.SizeF = new System.Drawing.SizeF(13.54171F, 10.99998F);
            this.lblSTT3.StylePriority.UseFont = false;
            this.lblSTT3.StylePriority.UseTextAlignment = false;
            this.lblSTT3.Text = "3";
            this.lblSTT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSTT2
            // 
            this.lblSTT2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSTT2.LocationFloat = new DevExpress.Utils.PointFloat(12.50005F, 236.0416F);
            this.lblSTT2.Name = "lblSTT2";
            this.lblSTT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTT2.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.lblSTT2.StylePriority.UseFont = false;
            this.lblSTT2.StylePriority.UseTextAlignment = false;
            this.lblSTT2.Text = "2";
            this.lblSTT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(12.50005F, 214.0416F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel45.StylePriority.UseFont = false;
            this.xrLabel45.Text = "Giấy phép xuất khẩu";
            // 
            // xrLine6
            // 
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 212.0416F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.SizeF = new System.Drawing.SizeF(734.9999F, 2F);
            // 
            // lblTenphuongtienvanchuyen
            // 
            this.lblTenphuongtienvanchuyen.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenphuongtienvanchuyen.LocationFloat = new DevExpress.Utils.PointFloat(258.3333F, 160.0833F);
            this.lblTenphuongtienvanchuyen.Multiline = true;
            this.lblTenphuongtienvanchuyen.Name = "lblTenphuongtienvanchuyen";
            this.lblTenphuongtienvanchuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenphuongtienvanchuyen.SizeF = new System.Drawing.SizeF(414.5834F, 10.3611F);
            this.lblTenphuongtienvanchuyen.StylePriority.UseFont = false;
            this.lblTenphuongtienvanchuyen.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            // 
            // lblNgayhangdidukien
            // 
            this.lblNgayhangdidukien.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayhangdidukien.LocationFloat = new DevExpress.Utils.PointFloat(179.7916F, 172.8055F);
            this.lblNgayhangdidukien.Multiline = true;
            this.lblNgayhangdidukien.Name = "lblNgayhangdidukien";
            this.lblNgayhangdidukien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayhangdidukien.SizeF = new System.Drawing.SizeF(78.54173F, 11F);
            this.lblNgayhangdidukien.StylePriority.UseFont = false;
            this.lblNgayhangdidukien.Text = "dd/MM/yyyy";
            // 
            // lblMaphuongtienvanchuyen
            // 
            this.lblMaphuongtienvanchuyen.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaphuongtienvanchuyen.LocationFloat = new DevExpress.Utils.PointFloat(179.7915F, 160.0833F);
            this.lblMaphuongtienvanchuyen.Multiline = true;
            this.lblMaphuongtienvanchuyen.Name = "lblMaphuongtienvanchuyen";
            this.lblMaphuongtienvanchuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaphuongtienvanchuyen.SizeF = new System.Drawing.SizeF(78.54173F, 12.72218F);
            this.lblMaphuongtienvanchuyen.StylePriority.UseFont = false;
            this.lblMaphuongtienvanchuyen.Text = "XXXXXXXXE";
            // 
            // lblTendiadiemxephang
            // 
            this.lblTendiadiemxephang.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTendiadiemxephang.LocationFloat = new DevExpress.Utils.PointFloat(258.3333F, 149.0833F);
            this.lblTendiadiemxephang.Multiline = true;
            this.lblTendiadiemxephang.Name = "lblTendiadiemxephang";
            this.lblTendiadiemxephang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTendiadiemxephang.SizeF = new System.Drawing.SizeF(414.5834F, 10.3611F);
            this.lblTendiadiemxephang.StylePriority.UseFont = false;
            this.lblTendiadiemxephang.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            // 
            // lblMadiadiemxephang
            // 
            this.lblMadiadiemxephang.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadiadiemxephang.LocationFloat = new DevExpress.Utils.PointFloat(179.7916F, 149.7222F);
            this.lblMadiadiemxephang.Multiline = true;
            this.lblMadiadiemxephang.Name = "lblMadiadiemxephang";
            this.lblMadiadiemxephang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadiadiemxephang.SizeF = new System.Drawing.SizeF(78.54173F, 10.3611F);
            this.lblMadiadiemxephang.StylePriority.UseFont = false;
            this.lblMadiadiemxephang.Text = "XXXXXE";
            // 
            // lblTendiadiemnhanhangcuoi
            // 
            this.lblTendiadiemnhanhangcuoi.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTendiadiemnhanhangcuoi.LocationFloat = new DevExpress.Utils.PointFloat(258.3334F, 138.7221F);
            this.lblTendiadiemnhanhangcuoi.Multiline = true;
            this.lblTendiadiemnhanhangcuoi.Name = "lblTendiadiemnhanhangcuoi";
            this.lblTendiadiemnhanhangcuoi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTendiadiemnhanhangcuoi.SizeF = new System.Drawing.SizeF(290.6249F, 10.3611F);
            this.lblTendiadiemnhanhangcuoi.StylePriority.UseFont = false;
            this.lblTendiadiemnhanhangcuoi.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            // 
            // lblMadiadiemnhanhangcuoi
            // 
            this.lblMadiadiemnhanhangcuoi.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadiadiemnhanhangcuoi.LocationFloat = new DevExpress.Utils.PointFloat(179.7916F, 138.7222F);
            this.lblMadiadiemnhanhangcuoi.Multiline = true;
            this.lblMadiadiemnhanhangcuoi.Name = "lblMadiadiemnhanhangcuoi";
            this.lblMadiadiemnhanhangcuoi.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadiadiemnhanhangcuoi.SizeF = new System.Drawing.SizeF(78.54173F, 10.3611F);
            this.lblMadiadiemnhanhangcuoi.StylePriority.UseFont = false;
            this.lblMadiadiemnhanhangcuoi.Text = "XXXXE";
            // 
            // lblTendiadiemluukho
            // 
            this.lblTendiadiemluukho.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTendiadiemluukho.LocationFloat = new DevExpress.Utils.PointFloat(258.3334F, 128.361F);
            this.lblTendiadiemluukho.Multiline = true;
            this.lblTendiadiemluukho.Name = "lblTendiadiemluukho";
            this.lblTendiadiemluukho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTendiadiemluukho.SizeF = new System.Drawing.SizeF(290.6249F, 10.3611F);
            this.lblTendiadiemluukho.StylePriority.UseFont = false;
            this.lblTendiadiemluukho.Text = "XXXXXXXXX1XXXXXXXXXE";
            // 
            // lblMadonvitinhtrongluong
            // 
            this.lblMadonvitinhtrongluong.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadonvitinhtrongluong.LocationFloat = new DevExpress.Utils.PointFloat(258.3333F, 116.7222F);
            this.lblMadonvitinhtrongluong.Multiline = true;
            this.lblMadonvitinhtrongluong.Name = "lblMadonvitinhtrongluong";
            this.lblMadonvitinhtrongluong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadonvitinhtrongluong.SizeF = new System.Drawing.SizeF(70.20821F, 11.00002F);
            this.lblMadonvitinhtrongluong.StylePriority.UseFont = false;
            this.lblMadonvitinhtrongluong.Text = "XXE";
            // 
            // lblMadonvitinh
            // 
            this.lblMadonvitinh.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadonvitinh.LocationFloat = new DevExpress.Utils.PointFloat(258.3333F, 105.7221F);
            this.lblMadonvitinh.Multiline = true;
            this.lblMadonvitinh.Name = "lblMadonvitinh";
            this.lblMadonvitinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadonvitinh.SizeF = new System.Drawing.SizeF(70.20821F, 11.00002F);
            this.lblMadonvitinh.StylePriority.UseFont = false;
            this.lblMadonvitinh.Text = "XXE";
            // 
            // lblTongtrongluonghang
            // 
            this.lblTongtrongluonghang.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongtrongluonghang.LocationFloat = new DevExpress.Utils.PointFloat(179.7916F, 116.7222F);
            this.lblTongtrongluonghang.Multiline = true;
            this.lblTongtrongluonghang.Name = "lblTongtrongluonghang";
            this.lblTongtrongluonghang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongtrongluonghang.SizeF = new System.Drawing.SizeF(78.5417F, 11.00002F);
            this.lblTongtrongluonghang.StylePriority.UseFont = false;
            this.lblTongtrongluonghang.Text = "1,234,567,890";
            // 
            // xrLabel46
            // 
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(12.50004F, 183.8056F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.Text = "Ký hiệu và số hiệu";
            // 
            // xrLabel42
            // 
            this.xrLabel42.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(12.50004F, 172.8056F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(114.1666F, 11.00002F);
            this.xrLabel42.StylePriority.UseFont = false;
            this.xrLabel42.Text = "Ngày hàng đi dự kiến";
            // 
            // xrLabel39
            // 
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 161.8056F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(156.875F, 10.99997F);
            this.xrLabel39.StylePriority.UseFont = false;
            this.xrLabel39.Text = "Phương tiện vận chuyển dự kiến";
            // 
            // lblMadiadiemluukho
            // 
            this.lblMadiadiemluukho.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadiadiemluukho.LocationFloat = new DevExpress.Utils.PointFloat(179.7916F, 128.361F);
            this.lblMadiadiemluukho.Multiline = true;
            this.lblMadiadiemluukho.Name = "lblMadiadiemluukho";
            this.lblMadiadiemluukho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadiadiemluukho.SizeF = new System.Drawing.SizeF(78.54173F, 10.3611F);
            this.lblMadiadiemluukho.StylePriority.UseFont = false;
            this.lblMadiadiemluukho.Text = "XXXE";
            // 
            // xrLabel48
            // 
            this.xrLabel48.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(12.50004F, 138.7222F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(167.2916F, 11.00002F);
            this.xrLabel48.StylePriority.UseFont = false;
            this.xrLabel48.Text = "Địa điểm nhận hàng cuối cùng";
            // 
            // lblSoluong
            // 
            this.lblSoluong.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoluong.LocationFloat = new DevExpress.Utils.PointFloat(179.7916F, 105.7222F);
            this.lblSoluong.Multiline = true;
            this.lblSoluong.Name = "lblSoluong";
            this.lblSoluong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoluong.SizeF = new System.Drawing.SizeF(78.54161F, 11.00002F);
            this.lblSoluong.StylePriority.UseFont = false;
            this.lblSoluong.Text = "12,345,678";
            // 
            // xrLabel53
            // 
            this.xrLabel53.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(12.50004F, 150.8056F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel53.StylePriority.UseFont = false;
            this.xrLabel53.Text = "Địa điểm xếp hàng";
            // 
            // xrLabel41
            // 
            this.xrLabel41.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(12.50004F, 105.7222F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel41.StylePriority.UseFont = false;
            this.xrLabel41.Text = "Số lượng";
            // 
            // xrLabel40
            // 
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(12.50004F, 94.72215F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel40.StylePriority.UseFont = false;
            this.xrLabel40.Text = "Số vận đơn";
            // 
            // xrLabel43
            // 
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(12.50004F, 116.7222F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(167.2916F, 10.99998F);
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.Text = "Tổng trọng lượng hàng (Gross)";
            // 
            // xrLabel44
            // 
            this.xrLabel44.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(12.50004F, 127.7222F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel44.StylePriority.UseFont = false;
            this.xrLabel44.Text = "Địa điểm lưu kho";
            // 
            // lblSovandon
            // 
            this.lblSovandon.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSovandon.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 94.72215F);
            this.lblSovandon.Name = "lblSovandon";
            this.lblSovandon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSovandon.SizeF = new System.Drawing.SizeF(354.1664F, 11.00002F);
            this.lblSovandon.StylePriority.UseFont = false;
            this.lblSovandon.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            // 
            // xrLine5
            // 
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 92.72216F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(735F, 2F);
            // 
            // lblManhanvienhaiquan
            // 
            this.lblManhanvienhaiquan.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManhanvienhaiquan.LocationFloat = new DevExpress.Utils.PointFloat(693.7501F, 81.72214F);
            this.lblManhanvienhaiquan.Name = "lblManhanvienhaiquan";
            this.lblManhanvienhaiquan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblManhanvienhaiquan.SizeF = new System.Drawing.SizeF(41.24994F, 11.00001F);
            this.lblManhanvienhaiquan.StylePriority.UseFont = false;
            this.lblManhanvienhaiquan.StylePriority.UseTextAlignment = false;
            this.lblManhanvienhaiquan.Text = "XXXXE";
            this.lblManhanvienhaiquan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(577.0834F, 81.72213F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(116.6667F, 11.00002F);
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.StylePriority.UseTextAlignment = false;
            this.xrLabel37.Text = "Mã nhân viên Hải quan";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblTendailyhaiquan
            // 
            this.lblTendailyhaiquan.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTendailyhaiquan.LocationFloat = new DevExpress.Utils.PointFloat(169.375F, 81.72213F);
            this.lblTendailyhaiquan.Name = "lblTendailyhaiquan";
            this.lblTendailyhaiquan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTendailyhaiquan.SizeF = new System.Drawing.SizeF(407.7084F, 11.00002F);
            this.lblTendailyhaiquan.StylePriority.UseFont = false;
            this.lblTendailyhaiquan.StylePriority.UseTextAlignment = false;
            this.lblTendailyhaiquan.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXXE";
            this.lblTendailyhaiquan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblMadailyhaiquan
            // 
            this.lblMadailyhaiquan.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMadailyhaiquan.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 81.72213F);
            this.lblMadailyhaiquan.Name = "lblMadailyhaiquan";
            this.lblMadailyhaiquan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMadailyhaiquan.SizeF = new System.Drawing.SizeF(56.875F, 11.00002F);
            this.lblMadailyhaiquan.StylePriority.UseFont = false;
            this.lblMadailyhaiquan.StylePriority.UseTextAlignment = false;
            this.lblMadailyhaiquan.Text = "XXXXE";
            this.lblMadailyhaiquan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(12.50004F, 81.72215F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.StylePriority.UseTextAlignment = false;
            this.xrLabel36.Text = "Đại lý Hải quan";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify;
            // 
            // lblManuoc
            // 
            this.lblManuoc.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManuoc.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 68.36111F);
            this.lblManuoc.Name = "lblManuoc";
            this.lblManuoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblManuoc.SizeF = new System.Drawing.SizeF(100F, 11.36102F);
            this.lblManuoc.StylePriority.UseFont = false;
            this.lblManuoc.Text = "XE";
            // 
            // lblDiachi4
            // 
            this.lblDiachi4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiachi4.LocationFloat = new DevExpress.Utils.PointFloat(406.8751F, 58.00002F);
            this.lblDiachi4.Multiline = true;
            this.lblDiachi4.Name = "lblDiachi4";
            this.lblDiachi4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiachi4.SizeF = new System.Drawing.SizeF(318.1249F, 10.3611F);
            this.lblDiachi4.StylePriority.UseFont = false;
            this.lblDiachi4.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            // 
            // lblDiachi3
            // 
            this.lblDiachi3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiachi3.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 58.00002F);
            this.lblDiachi3.Multiline = true;
            this.lblDiachi3.Name = "lblDiachi3";
            this.lblDiachi3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiachi3.SizeF = new System.Drawing.SizeF(294.375F, 10.3611F);
            this.lblDiachi3.StylePriority.UseFont = false;
            this.lblDiachi3.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            // 
            // lblDiachi2
            // 
            this.lblDiachi2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiachi2.LocationFloat = new DevExpress.Utils.PointFloat(406.875F, 47F);
            this.lblDiachi2.Multiline = true;
            this.lblDiachi2.Name = "lblDiachi2";
            this.lblDiachi2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiachi2.SizeF = new System.Drawing.SizeF(318.125F, 11.00002F);
            this.lblDiachi2.StylePriority.UseFont = false;
            this.lblDiachi2.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            // 
            // xrLabel21
            // 
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(12.50004F, 68.36111F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.Text = "Mã nước";
            // 
            // xrLine4
            // 
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 79.72212F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(734.9999F, 2.000008F);
            // 
            // lblDiachi1
            // 
            this.lblDiachi1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiachi1.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 47F);
            this.lblDiachi1.Multiline = true;
            this.lblDiachi1.Name = "lblDiachi1";
            this.lblDiachi1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiachi1.SizeF = new System.Drawing.SizeF(294.375F, 11.00002F);
            this.lblDiachi1.StylePriority.UseFont = false;
            this.lblDiachi1.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            // 
            // lblMabuuchinh
            // 
            this.lblMabuuchinh.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMabuuchinh.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 36.00002F);
            this.lblMabuuchinh.Name = "lblMabuuchinh";
            this.lblMabuuchinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMabuuchinh.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.lblMabuuchinh.StylePriority.UseFont = false;
            this.lblMabuuchinh.Text = "XXXXXXE";
            // 
            // lblManguoinhapkhau
            // 
            this.lblManguoinhapkhau.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManguoinhapkhau.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 12.5F);
            this.lblManguoinhapkhau.Name = "lblManguoinhapkhau";
            this.lblManguoinhapkhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblManguoinhapkhau.SizeF = new System.Drawing.SizeF(137.4998F, 11F);
            this.lblManguoinhapkhau.StylePriority.UseFont = false;
            this.lblManguoinhapkhau.Text = "XXXXXXXXX1-XXE";
            // 
            // xrLabel20
            // 
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 47.00002F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.Text = "Địa chỉ";
            // 
            // xrLabel19
            // 
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 25F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.Text = "Tên";
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 36.00002F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.Text = "Mã bưu chính";
            // 
            // xrLabel35
            // 
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 12.5F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel35.StylePriority.UseFont = false;
            this.xrLabel35.Text = "Mã";
            // 
            // xrLabel22
            // 
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.Text = "Người nhập khẩu";
            // 
            // lblTennguoinhapkhau
            // 
            this.lblTennguoinhapkhau.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTennguoinhapkhau.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 23.49998F);
            this.lblTennguoinhapkhau.Multiline = true;
            this.lblTennguoinhapkhau.Name = "lblTennguoinhapkhau";
            this.lblTennguoinhapkhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTennguoinhapkhau.SizeF = new System.Drawing.SizeF(622.5F, 12.50004F);
            this.lblTennguoinhapkhau.StylePriority.UseFont = false;
            this.lblTennguoinhapkhau.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6";
            // 
            // lblKyhieusohieu
            // 
            this.lblKyhieusohieu.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKyhieusohieu.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 183.8056F);
            this.lblKyhieusohieu.Multiline = true;
            this.lblKyhieusohieu.Name = "lblKyhieusohieu";
            this.lblKyhieusohieu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblKyhieusohieu.SizeF = new System.Drawing.SizeF(622.4999F, 28.23601F);
            this.lblKyhieusohieu.StylePriority.UseFont = false;
            this.lblKyhieusohieu.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXXE";
            // 
            // lblSTT4
            // 
            this.lblSTT4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSTT4.LocationFloat = new DevExpress.Utils.PointFloat(12.50005F, 258.0416F);
            this.lblSTT4.Name = "lblSTT4";
            this.lblSTT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTT4.SizeF = new System.Drawing.SizeF(13.54167F, 11F);
            this.lblSTT4.StylePriority.UseFont = false;
            this.lblSTT4.StylePriority.UseTextAlignment = false;
            this.lblSTT4.Text = "4";
            this.lblSTT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLine12
            // 
            this.xrLine12.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine12.LocationFloat = new DevExpress.Utils.PointFloat(246.4583F, 214.0416F);
            this.xrLine12.Name = "xrLine12";
            this.xrLine12.SizeF = new System.Drawing.SizeF(2.083328F, 112F);
            // 
            // BanXacNhanNoiDungToKhaiHangHoaXuatKhau_1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(63, 47, 145, 77);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblSotokhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblMaphanloaikiemtra;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel lblSotokhaitamnhaptaixuat;
        private DevExpress.XtraReports.UI.XRLabel lblTencoquanhaiquantiepnhantokhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel lblNgaydangky;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel lblThoihantainhapxuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel lblGiodangky;
        private DevExpress.XtraReports.UI.XRLabel lblSotokhaidautien;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lblSonhanhtokhaichianho;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel lblBieuthitruonghophethan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel lblMasothuedaidien;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel lblTongsotokhaichianho;
        private DevExpress.XtraReports.UI.XRLabel lblMaloaihinh;
        private DevExpress.XtraReports.UI.XRLabel lblMahieuphuongthucvanchuyen;
        private DevExpress.XtraReports.UI.XRLabel lblMaphanloaihanghoa;
        private DevExpress.XtraReports.UI.XRLabel lblNhomxulyhoso;
        private DevExpress.XtraReports.UI.XRLabel lblGiothaydoidangky;
        private DevExpress.XtraReports.UI.XRLabel lblNgaythaydoidangky;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel lblDiachinguoixuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel lblTenxuatkhau;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel lblSodtnguoixuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel lblMaxuatkhau;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel lblTennguoiuythacxuatkhau;
        private DevExpress.XtraReports.UI.XRLabel lblManguoiuythacxuatkhau;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel lblDiachi1;
        private DevExpress.XtraReports.UI.XRLabel lblMabuuchinh;
        private DevExpress.XtraReports.UI.XRLabel lblManguoinhapkhau;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel lblManuoc;
        private DevExpress.XtraReports.UI.XRLabel lblDiachi4;
        private DevExpress.XtraReports.UI.XRLabel lblDiachi3;
        private DevExpress.XtraReports.UI.XRLabel lblDiachi2;
        private DevExpress.XtraReports.UI.XRLabel lblTennguoinhapkhau;
        private DevExpress.XtraReports.UI.XRLabel lblTendailyhaiquan;
        private DevExpress.XtraReports.UI.XRLabel lblMadailyhaiquan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel lblManhanvienhaiquan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel lblMadiadiemluukho;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel lblSoluong;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel lblSovandon;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel lblTendiadiemluukho;
        private DevExpress.XtraReports.UI.XRLabel lblMadonvitinhtrongluong;
        private DevExpress.XtraReports.UI.XRLabel lblMadonvitinh;
        private DevExpress.XtraReports.UI.XRLabel lblTongtrongluonghang;
        private DevExpress.XtraReports.UI.XRLabel lblTenphuongtienvanchuyen;
        private DevExpress.XtraReports.UI.XRLabel lblKyhieusohieu;
        private DevExpress.XtraReports.UI.XRLabel lblNgayhangdidukien;
        private DevExpress.XtraReports.UI.XRLabel lblMaphuongtienvanchuyen;
        private DevExpress.XtraReports.UI.XRLabel lblTendiadiemxephang;
        private DevExpress.XtraReports.UI.XRLabel lblMadiadiemxephang;
        private DevExpress.XtraReports.UI.XRLabel lblTendiadiemnhanhangcuoi;
        private DevExpress.XtraReports.UI.XRLabel lblMadiadiemnhanhangcuoi;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel lblPhanloaigiayphepxuatkhau5;
        private DevExpress.XtraReports.UI.XRLabel lblPhanloaigiayphepxuatkhau4;
        private DevExpress.XtraReports.UI.XRLabel lblPhanloaigiayphepxuatkhau3;
        private DevExpress.XtraReports.UI.XRLabel lblPhanloaigiayphepxuatkhau2;
        private DevExpress.XtraReports.UI.XRLabel lblPhanloaigiayphepxuatkhau1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel lblPhanloaigiayphepxuatkhau;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel lblSogiayphep5;
        private DevExpress.XtraReports.UI.XRLabel lblSogiayphep4;
        private DevExpress.XtraReports.UI.XRLabel lblSogiayphep3;
        private DevExpress.XtraReports.UI.XRLabel lblSogiayphep2;
        private DevExpress.XtraReports.UI.XRLabel lblSogiayphep1;
        private DevExpress.XtraReports.UI.XRLabel lblMadkgiahoadon;
        private DevExpress.XtraReports.UI.XRLabel lblPhuongthucthanhtoan;
        private DevExpress.XtraReports.UI.XRLabel lblNgayphathanh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRLabel lblSohoadon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel lblSotiepnhanhoadondientu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel72;
        private DevExpress.XtraReports.UI.XRLabel lblMaphanloaigiahoadon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel lblTongtrigiahoadon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRLabel lblMadongtienhoadon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel79;
        private DevExpress.XtraReports.UI.XRLabel lblMadongtientygiathue2;
        private DevExpress.XtraReports.UI.XRLabel lblTygiatinhthue1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRLabel lblMadongtientygiathue1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel70;
        private DevExpress.XtraReports.UI.XRLabel lblTongthue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel73;
        private DevExpress.XtraReports.UI.XRLabel lblMadongtientongthue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel lblTonghesophanbothue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel81;
        private DevExpress.XtraReports.UI.XRLabel lblTygiatinhthue2;
        private DevExpress.XtraReports.UI.XRLabel lblMaphanloainhaplieutonggia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel71;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel86;
        private DevExpress.XtraReports.UI.XRLabel xrLabel85;
        private DevExpress.XtraReports.UI.XRLabel lblPhanloainopthue;
        private DevExpress.XtraReports.UI.XRLabel lblNguoinopthue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel82;
        private DevExpress.XtraReports.UI.XRLabel lblPhanloaikhongcanquydoi;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel xrLabel74;
        private DevExpress.XtraReports.UI.XRLabel lblMaxacdinhthoihannopthue;
        private DevExpress.XtraReports.UI.XRLabel lblMatientetienbaolanh;
        private DevExpress.XtraReports.UI.XRLabel lblMatientetongtienthuexuat;
        private DevExpress.XtraReports.UI.XRLabel lblTongsotienlephi;
        private DevExpress.XtraReports.UI.XRLabel lblTongsotienthuexuatkhau;
        private DevExpress.XtraReports.UI.XRLabel xrLabel83;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLine xrLine9;
        private DevExpress.XtraReports.UI.XRLabel lblTongsodonghangtokhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel88;
        private DevExpress.XtraReports.UI.XRLabel lblTongsotokhaicuatrang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLine xrLine8;
        private DevExpress.XtraReports.UI.XRLabel lblTongsotienbaolanh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel96;
        private DevExpress.XtraReports.UI.XRLabel lblPhanloaidinhkemkhaibaodientu2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel90;
        private DevExpress.XtraReports.UI.XRLabel xrLabel94;
        private DevExpress.XtraReports.UI.XRLabel lblPhanloaidinhkemkhaibaodientu1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel92;
        private DevExpress.XtraReports.UI.XRLine xrLine10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel87;
        private DevExpress.XtraReports.UI.XRLabel lblSodinhkemkhaibao1;
        private DevExpress.XtraReports.UI.XRLabel lblSodinhkemkhaibao3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel99;
        private DevExpress.XtraReports.UI.XRLabel lblPhanloaidinhkemkhaibaodientu3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel98;
        private DevExpress.XtraReports.UI.XRLabel lblSodinhkemkhaibao2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel105;
        private DevExpress.XtraReports.UI.XRLabel lblSoquanlynguoisudung;
        private DevExpress.XtraReports.UI.XRLabel xrLabel103;
        private DevExpress.XtraReports.UI.XRLabel lblSoquanlynoibodoanhnghiep;
        private DevExpress.XtraReports.UI.XRLabel xrLabel93;
        private DevExpress.XtraReports.UI.XRLabel lblPhanghichu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel109;
        private DevExpress.XtraReports.UI.XRLabel xrLabel108;
        private DevExpress.XtraReports.UI.XRLabel xrLabel107;
        private DevExpress.XtraReports.UI.XRLabel xrLabel106;
        private DevExpress.XtraReports.UI.XRLabel xrLabel104;
        private DevExpress.XtraReports.UI.XRLabel lblNgaykhoihanh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel102;
        private DevExpress.XtraReports.UI.XRLabel xrLabel89;
        private DevExpress.XtraReports.UI.XRLine xrLine11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel126;
        private DevExpress.XtraReports.UI.XRLabel lblNgaydukienden;
        private DevExpress.XtraReports.UI.XRLabel lblDiadiem3;
        private DevExpress.XtraReports.UI.XRLabel lblNgaykhoihanhvanchuyen3;
        private DevExpress.XtraReports.UI.XRLabel lblNgaykhoihanhvanchuyen2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel120;
        private DevExpress.XtraReports.UI.XRLabel xrLabel121;
        private DevExpress.XtraReports.UI.XRLabel lblNgaydukiendentrungchuyen3;
        private DevExpress.XtraReports.UI.XRLabel lblNgaydukiendentrungchuyen2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel117;
        private DevExpress.XtraReports.UI.XRLabel lblDiadiemdich;
        private DevExpress.XtraReports.UI.XRLabel lblDiadiem2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel114;
        private DevExpress.XtraReports.UI.XRLabel xrLabel113;
        private DevExpress.XtraReports.UI.XRLabel lblNgaykhoihanhvanchuyen;
        private DevExpress.XtraReports.UI.XRLabel lblNgaydukiendentrungchuyen;
        private DevExpress.XtraReports.UI.XRLabel lblDiadiem;
        private DevExpress.XtraReports.UI.XRLine xrLine12;
        private DevExpress.XtraReports.UI.XRLabel lblSTT5;
        private DevExpress.XtraReports.UI.XRLabel lblSTT3;
        private DevExpress.XtraReports.UI.XRLabel lblSTT2;
        private DevExpress.XtraReports.UI.XRLabel lblSTT4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel lblPhanloaihinhthuchoadon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
    }
}
