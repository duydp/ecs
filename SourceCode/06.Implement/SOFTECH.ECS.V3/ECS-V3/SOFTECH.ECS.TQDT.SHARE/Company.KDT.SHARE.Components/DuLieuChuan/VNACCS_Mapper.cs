using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;


namespace Company.KDT.SHARE.Components.DuLieuChuan
{
	public partial class VNACCS_Mapper : BaseClass
	{
        public static string GetCodeVNACC(string codeV4)
        {
            //string query = string.Format("SELECT Ten FROM t_HaiQuan_Nuoc WHERE [ID] = '{0}'", id.PadRight(3));
            //DbCommand dbCommand = db.GetSqlStringCommand(query);
            //IDataReader reader = db.ExecuteReader(dbCommand);
            //if (reader.Read())
            //{
            //    return reader["Ten"].ToString();
            //}
            //return string.Empty;

            try
            {
                string query = string.Format("[CodeV4] = '{0}' and [LoaiMapper]='DVT'", codeV4.Trim());

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["DVTMapper"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["CodeV5"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return string.Empty;
        }
        public static string GetCodeV4DVT(string codeV5)
        {
            //string query = string.Format("SELECT Ten FROM t_HaiQuan_Nuoc WHERE [ID] = '{0}'", id.PadRight(3));
            //DbCommand dbCommand = db.GetSqlStringCommand(query);
            //IDataReader reader = db.ExecuteReader(dbCommand);
            //if (reader.Read())
            //{
            //    return reader["Ten"].ToString();
            //}
            //return string.Empty;

            try
            {
                string query = string.Format("[CodeV5] = '{0}' and [LoaiMapper]='DVT'", codeV5.Trim());

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["DVTMapper"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["CodeV4"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return string.Empty;
        }

        public static string GetCodeVNACCMaHaiQuan(string codeV4)
        {

            try
            {
                string query = string.Format("[CodeV4] = '{0}' and [LoaiMapper]='MaHQ'", codeV4.Trim());

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["MaHQMapper"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["CodeV5"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return string.Empty;
        }

        public static string GetCodeV4MaHaiQuan(string codeV5)
        {

            try
            {
                string query = string.Format("[CodeV5] = '{0}' and [LoaiMapper]='MaHQ'", codeV5.Trim());

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["MaHQMapper"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["CodeV4"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return string.Empty;
        }

         public static string GetCodeVNACC(string codeV4, string CodeType)
        {
            try
            {

                List<VNACCS_Mapper> list = (List<VNACCS_Mapper>)SelectCollectionDynamic(string.Format("[CodeV4] = '{0}' and [LoaiMapper]='{1}'", codeV4, CodeType), null);
                if (list != null && list.Count > 0)
                {
                    return list[0].CodeV5;
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return string.Empty;
        }
         public static bool SaveCodeVNACCS(string codeV4, string _codeV5, string CodeType)
         {
             try
             {
           
             List<VNACCS_Mapper> list = (List<VNACCS_Mapper>)SelectCollectionDynamic(string.Format("[CodeV4] = '{0}' and [LoaiMapper]='{}'",codeV4,CodeType), null);
             if (list != null && list.Count > 0)
             {
                 VNACCS_Mapper mapper = list[0];
                 mapper.CodeV5 = _codeV5;
                 mapper.InsertUpdate();
             }
             else return false;
             }
             catch (System.Exception ex)
             {
                 Logger.LocalLogger.Instance().WriteMessage(ex);
                 return false;
             }
             return true;
         }
	}	
}