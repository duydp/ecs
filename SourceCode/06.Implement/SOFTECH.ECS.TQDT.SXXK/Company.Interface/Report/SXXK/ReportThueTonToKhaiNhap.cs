﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK;
using System.Data;
using Company.BLL.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class ReportThueTonToKhaiNhap : DevExpress.XtraReports.UI.XtraReport
    {
        private int STT = 0;
        public ReportThueTonToKhaiNhap()
        {
            InitializeComponent();
        }
        public void BindReport(DataTable dt, DateTime fromDate, DateTime toDate)
        {
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblNgay.Text = "TỪ NGÀY " + fromDate.ToString("dd/MM/yyyy") + " ĐẾN NGÀY " + toDate.ToString("dd/MM/yyyy");
            lblNgayIn.Text = DateTime.Today.ToString("dd/MM/yyyy");
            this.DataSource = dt;
            lblSoToKhai.DataBindings.Add("Text", this.DataSource, "SoToKhai");
            lblMaLoaiHinh.DataBindings.Add("Text", this.DataSource, "MaLoaiHinh");
            lblNgayDangKy.DataBindings.Add("Text", this.DataSource, "NgayDangKy", "{0:dd/MM/yyyy}");
            lblThueTon.DataBindings.Add("Text", this.DataSource, "TongThueTon", "{0:n0}");
            lblTongSoThue.DataBindings.Add("Text", this.DataSource, "TongThueTon", "{0:n0}");
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }

        private void ReportThueTonToKhaiNhap_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
        }

    }
}
