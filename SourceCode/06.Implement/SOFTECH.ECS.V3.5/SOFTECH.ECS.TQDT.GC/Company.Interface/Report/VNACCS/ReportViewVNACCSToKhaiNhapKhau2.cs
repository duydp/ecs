using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;
using BarcodeLib;
namespace Company.Interface.Report.VNACCS
{
    public partial class ReportViewVNACCSToKhaiNhapKhau2 : DevExpress.XtraReports.UI.XtraReport
    {
        public int count;
        public ReportViewVNACCSToKhaiNhapKhau2()
        {
            InitializeComponent();
        }
        public Image GenerateBarCode(string Text)
        {
            BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
            {
                IncludeLabel = false,
                Alignment = AlignmentPositions.CENTER,
                Width = 300,
                Height = 30,
                RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                BackColor = Color.White,
                ForeColor = Color.Black,
            };

            Image img = barcode.Encode(TYPE.CODE128B, Text);
            return img;
        }
        public void BindingReport(KDT_VNACC_ToKhaiMauDich tkmd)
        {
            try
            {
                pictureBox1.Image = GenerateBarCode(tkmd.SoToKhai.ToString());
                lblTenThongTinXuat.Text = "Tờ khai hàng hóa nhập khẩu (thông quan)";
                count = tkmd.HangCollection.Count;
                lblSoToKhai.Text = tkmd.SoToKhai.ToString();
                lblSoToKhaiDauTien.Text = String.IsNullOrEmpty(tkmd.SoToKhaiDauTien) ? "" : tkmd.SoToKhaiDauTien.ToString();
                lblSoNhanhToKhaiChiaNho.Text =  tkmd.SoNhanhToKhai.ToString();
                lblSoToKhaiTamNhapTaiXuat.Text =  tkmd.SoToKhaiTNTX.ToString();
                lblMaPhanLoaiKiemTra.Text = String.IsNullOrEmpty(tkmd.MaPhanLoaiKiemTra) ? "" : tkmd.MaPhanLoaiKiemTra.ToString();
                lblMaLoaiHinh.Text = String.IsNullOrEmpty(tkmd.MaLoaiHinh) ? "" : tkmd.MaLoaiHinh.ToString();
                lblMaPhanLoaiHangHoa.Text = String.IsNullOrEmpty(tkmd.MaPhanLoaiHH) ? "" : tkmd.MaPhanLoaiHH.ToString();
                lblMaHieuPhuongThucVanChuyen.Text = String.IsNullOrEmpty(tkmd.MaPhuongThucVT) ? "" : tkmd.MaPhuongThucVT.ToString();
                lblPhanLoaiCaNhanToChuc.Text = String.IsNullOrEmpty(tkmd.PhanLoaiToChuc) ? "" : tkmd.PhanLoaiToChuc.ToString();
                //lblMaSoHangHoaDaiDienToKhai.Text = ;
                lblTenCoQuanHaiQuanTiepNhanToKhai.Text = String.IsNullOrEmpty(tkmd.CoQuanHaiQuan) ? "" : tkmd.CoQuanHaiQuan.ToString();
                //lblMaBoPhanXuLyToKhai.Text =
                if (tkmd.NgayDangKy.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayDangKy.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgayDangKy.Text = tkmd.NgayDangKy.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayDangKy.Text = "";

                }
                if (tkmd.NgayDangKy.Hour.ToString() != "" && tkmd.NgayDangKy.Hour.ToString() != "0")
                {
                    lblGioDangKy.Text = tkmd.NgayDangKy.Hour.ToString() + ":" + tkmd.NgayDangKy.Minute.ToString() + ":" + tkmd.NgayDangKy.Second.ToString();// vad1ac.AD1.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ac.AD1.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ac.AD1.GetValue().ToString().ToUpper().Substring(4, 2);
                }
                else
                {
                    lblGioDangKy.Text = "";
                }
                if (tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgayThayDoiDangKy.Text = tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayThayDoiDangKy.Text = "";
                }
                if (tkmd.NgayThayDoiDangKy.Hour.ToString() != "" && tkmd.NgayThayDoiDangKy.Hour.ToString().ToUpper() != "0")
                {
                    lblGioThayDoiDangKy.Text = tkmd.NgayThayDoiDangKy.Hour.ToString() + ":" + tkmd.NgayThayDoiDangKy.Minute.ToString() + ":" + tkmd.NgayThayDoiDangKy.Second.ToString();//vad1ac.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ac.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ac.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
                }
                else
                {
                    lblGioThayDoiDangKy.Text = "";
                }
                if (tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblThoiHanTaiNhapTaiXuat.Text = tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblThoiHanTaiNhapTaiXuat.Text = "";
                }
                lblBieuThiTHHetHan.Text = String.IsNullOrEmpty(tkmd.BieuThiTruongHopHetHan) ? "" : tkmd.BieuThiTruongHopHetHan.ToString().ToUpper();
                for (int i = 0; i < tkmd.DinhKemCollection.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            lblPhanLoaiDinhKemKhaiBaoDienTu1.Text = String.IsNullOrEmpty(tkmd.DinhKemCollection[i].PhanLoai) ? "" : tkmd.DinhKemCollection[i].PhanLoai.ToString().ToUpper();
                            lblSoDinhKemKhaiBaoDienTu1.Text = tkmd.DinhKemCollection[i].SoDinhKemKhaiBaoDT.ToString().ToUpper();
                            break;
                        case 1:
                            lblPhanLoaiDinhKemKhaiBaoDienTu2.Text = String.IsNullOrEmpty(tkmd.DinhKemCollection[i].PhanLoai) ? "" : tkmd.DinhKemCollection[i].PhanLoai.ToString().ToUpper();
                            lblSoDinhKemKhaiBaoDienTu2.Text = tkmd.DinhKemCollection[i].SoDinhKemKhaiBaoDT.ToString().ToUpper();
                            break;
                        case 2:
                            lblPhanLoaiDinhKemKhaiBaoDienTu3.Text = String.IsNullOrEmpty(tkmd.DinhKemCollection[i].PhanLoai) ? "" : tkmd.DinhKemCollection[i].PhanLoai.ToString().ToUpper();
                            lblSoDinhKemKhaiBaoDienTu3.Text = tkmd.DinhKemCollection[i].SoDinhKemKhaiBaoDT.ToString().ToUpper();
                            break;
                    }
                }
                lblPhanGhiChu.Text = String.IsNullOrEmpty(tkmd.GhiChu) ? "" : tkmd.GhiChu.ToString().ToUpper();
                lblSoQuanLyNoiBoDN.Text = String.IsNullOrEmpty(tkmd.SoQuanLyNoiBoDN) ? "" : tkmd.SoQuanLyNoiBoDN.ToString().ToUpper();
                lblSoQuanLyNSD.Text = String.IsNullOrEmpty(tkmd.SoQuanLyNguoiSuDung) ? "" : tkmd.SoQuanLyNguoiSuDung.ToString().ToUpper();
                //lblPhanLoaiChiThiHQ.Text = tkmd..ToString().ToUpper();
                if (String.IsNullOrEmpty(tkmd.CoQuanHaiQuan))
                {
                    lblTenTruongDvHQ.Text = "";
                }
                else
                {
                    lblTenTruongDvHQ.Text = tkmd.CoQuanHaiQuan.ToString();
                }
                if (tkmd.NgayCapPhep.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayCapPhep.ToString("dd/MM/yyyy") != "01/01/0001")
                    lblNgayCapPhep.Text = tkmd.NgayCapPhep.ToString("dd/MM/yyyy");
                else
                    lblNgayCapPhep.Text = "";
                if (tkmd.NgayCapPhep.Hour.ToString().ToUpper() != "" && tkmd.NgayCapPhep.Hour.ToString().ToUpper() != "0")
                    lblGioCapPhep.Text = tkmd.NgayCapPhep.Hour.ToString().ToUpper() + ":" + tkmd.NgayCapPhep.Minute.ToString().ToUpper() + ":" + tkmd.NgayCapPhep.Second.ToString().ToUpper();
                else
                    lblGioCapPhep.Text = "";
                if (tkmd.NgayHoanThanhKiemTra.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayHoanThanhKiemTra.ToString("dd/MM/yyyy") != "01/01/0001")
                    lblNgayHoanThanhKT.Text = tkmd.NgayHoanThanhKiemTra.ToString("dd/MM/yyyy");
                else
                    lblNgayHoanThanhKT.Text = "";
                if (tkmd.NgayHoanThanhKiemTra.Hour.ToString().ToUpper() != "" && tkmd.NgayHoanThanhKiemTra.Hour.ToString().ToUpper() != "0")
                    lblGioHoanThanhKT.Text = tkmd.NgayHoanThanhKiemTra.Hour.ToString() + ":" + tkmd.NgayHoanThanhKiemTra.Minute.ToString() + ":" + tkmd.NgayHoanThanhKiemTra.Second.ToString();
                if (String.IsNullOrEmpty(tkmd.PhanLoaiThamTraSauThongQuan))
                {
                    lblPhanLoaiThamTra.Text = "";
                }
                else
                {
                    lblPhanLoaiThamTra.Text = tkmd.PhanLoaiThamTraSauThongQuan.ToString();
                }
                if (tkmd.NgayPheDuyetBP.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayPheDuyetBP.ToString("dd/MM/yyyy") != "01/01/0001")
                    lblNgayPheDuyet.Text = tkmd.NgayPheDuyetBP.ToString("dd/MM/yyyy");
                else
                    lblNgayPheDuyet.Text = "";
                if (tkmd.NgayPheDuyetBP.Hour.ToString().ToUpper() != "" && tkmd.NgayPheDuyetBP.Hour.ToString().ToUpper() != "0")
                    lblGioPheDuyet.Text = tkmd.NgayPheDuyetBP.Hour.ToString().ToUpper() + ":" + tkmd.NgayPheDuyetBP.Minute.ToString().ToUpper() + ":" + tkmd.NgayPheDuyetBP.Second.ToString().ToUpper();
                else
                    lblGioPheDuyet.Text = "";
                if (tkmd.NgayHoanThanhKiemTraBP.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayHoanThanhKiemTraBP.ToString("dd/MM/yyyy") != "01/01/0001")
                    lblNgayHoanThanhKiemTraBP.Text = tkmd.NgayHoanThanhKiemTraBP.ToString("dd/MM/yyyy");
                else
                    lblNgayHoanThanhKiemTraBP.Text = "";
                if (tkmd.NgayHoanThanhKiemTraBP.Hour.ToString().ToUpper() != "" && tkmd.NgayHoanThanhKiemTraBP.Hour.ToString() != "0")
                    lblGioHoanThanhKiemTraBP.Text = tkmd.NgayHoanThanhKiemTraBP.Hour.ToString() + ":" + tkmd.NgayHoanThanhKiemTraBP.Minute.ToString() + ":" + tkmd.NgayHoanThanhKiemTraBP.Second.ToString();
                else
                    lblGioHoanThanhKiemTraBP.Text = "";
                lblSoNgayMongDoi.Text = tkmd.SoNgayDoiCapPhepNhapKhau.ToString();

                //lblMaSacThueAnHanVAT.Text = tkmd.MaSacThueAnHan_VAT.ToString();
                //lblTenSacThueAnHanVAT.Text = tkmd.TenSacThueAnHan_VAT.ToString();
                if (tkmd.HanNopThueSauKhiAnHan_VAT.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.HanNopThueSauKhiAnHan_VAT.ToString("dd/MM/yyyy") != "01/01/0001")
                    lblHanNopThueVAT.Text = tkmd.HanNopThueSauKhiAnHan_VAT.ToString("dd/MM/yyyy");
                else
                    lblHanNopThueVAT.Text = "";
                //if (tkmd.NgayKhaiBaoNopThue.ToString("dd/MM/yyyy") != "01/01/1900")
                //{
                //    lblNgayKhaiBaoNopThue.Text = tkmd.NgayKhaiBaoNopThue.ToString("dd/MM/yyyy");
                //}
                //else
                //{
                //    lblNgayKhaiBaoNopThue.Text = "";
                //}
                //if (tkmd.NgayKhaiBaoNopThue.Hour.ToString() != "" && tkmd.NgayKhaiBaoNopThue.Hour.ToString().ToUpper() != "0")
                //{
                //    lblGioKhaiBaoNT.Text =tkmd.NgayKhaiBaoNopThue.Hour.ToString() +":"+ tkmd.NgayKhaiBaoNopThue.Minute.ToString() +":"+tkmd.NgayKhaiBaoNopThue.Second.ToString() ;//vad1ac.ADZ.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ac.ADZ.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ac.ADZ.GetValue().ToString().ToUpper().Substring(4, 2);
                //}
                //else
                //{
                //    lblGioKhaiBaoNT.Text = "";
                //}
                if (String.IsNullOrEmpty(tkmd.TieuDe))
                {
                    lblTieuDe.Text = "";
                }
                lblTieuDe.Text = String.IsNullOrEmpty(tkmd.TieuDe) ? "" : tkmd.TieuDe.ToString().ToUpper();
                for (int i = 0; i < tkmd.SacThueCollection.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            lblMaSacThueAnHan1.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].MaSacThue) ? "" : tkmd.SacThueCollection[i].MaSacThue.ToString().ToUpper();
                            lblTenSacThueAnHan1.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].MaSacThue) ? "" : tkmd.SacThueCollection[i].TenSacThue.ToString().ToUpper();
                            if (tkmd.HanNopThueSauKhiAnHan_VAT.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.HanNopThueSauKhiAnHan_VAT.ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblHanNopThue1.Text = tkmd.HanNopThueSauKhiAnHan_VAT.ToString();//Convert.ToDateTime(vad1ac.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblHanNopThue1.Text = "";
                            }
                            break;
                        case 1:
                            lblMaSacThueAnHan2.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].MaSacThue) ? "" : tkmd.SacThueCollection[i].MaSacThue.ToString().ToUpper();
                            lblTenSacThueAnHan2.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].TenSacThue) ? "" : tkmd.SacThueCollection[i].TenSacThue.ToString().ToUpper();
                            if (tkmd.HanNopThueSauKhiAnHan_VAT.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.HanNopThueSauKhiAnHan_VAT.ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblHanNopThue2.Text = tkmd.HanNopThueSauKhiAnHan_VAT.ToString();//Convert.ToDateTime(vad1ac.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblHanNopThue2.Text = "";
                            }
                            break;
                        case 2:
                            lblMaSacThueAnHan3.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].MaSacThue) ? "" : tkmd.SacThueCollection[i].MaSacThue.ToString().ToUpper();
                            lblTenSacThueAnHan3.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].TenSacThue) ? "" : tkmd.SacThueCollection[i].TenSacThue.ToString().ToUpper();
                            if (tkmd.HanNopThueSauKhiAnHan_VAT.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.HanNopThueSauKhiAnHan_VAT.ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblHanNopThue3.Text = tkmd.HanNopThueSauKhiAnHan_VAT.ToString();//Convert.ToDateTime(vad1ac.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblHanNopThue3.Text = "";
                            }
                            break;
                        case 3:
                            lblMaSacThueAnHan4.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].MaSacThue) ? "" : tkmd.SacThueCollection[i].MaSacThue.ToString().ToUpper();
                            lblTenSacThueAnHan4.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].TenSacThue) ? "" : tkmd.SacThueCollection[i].TenSacThue.ToString().ToUpper();
                            if (tkmd.HanNopThueSauKhiAnHan_VAT.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.HanNopThueSauKhiAnHan_VAT.ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblHanNopThue4.Text = tkmd.HanNopThueSauKhiAnHan_VAT.ToString();//Convert.ToDateTime(vad1ac.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblHanNopThue4.Text = "";
                            }
                            break;
                        case 4:
                            lblMaSacThueAnHan5.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].MaSacThue) ? "" : tkmd.SacThueCollection[i].MaSacThue.ToString().ToUpper();
                            lblTenSacThueAnHan5.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].TenSacThue) ? "" : tkmd.SacThueCollection[i].TenSacThue.ToString().ToUpper();
                            if (tkmd.HanNopThueSauKhiAnHan_VAT.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.HanNopThueSauKhiAnHan_VAT.ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblHanNopThue5.Text = tkmd.HanNopThueSauKhiAnHan_VAT.ToString();//Convert.ToDateTime(vad1ac.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblHanNopThue5.Text = "";
                            }
                            break;
                        case 5:
                            lblMaSacThueAnHan6.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].MaSacThue) ? "" : tkmd.SacThueCollection[i].MaSacThue.ToString().ToUpper();
                            lblTenSacThueAnHan6.Text = String.IsNullOrEmpty(tkmd.SacThueCollection[i].TenSacThue) ? "" : tkmd.SacThueCollection[i].TenSacThue.ToString().ToUpper();
                            if (tkmd.HanNopThueSauKhiAnHan_VAT.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.HanNopThueSauKhiAnHan_VAT.ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblHanNopThue6.Text = tkmd.HanNopThueSauKhiAnHan_VAT.ToString();//Convert.ToDateTime(vad1ac.KN1.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblHanNopThue6.Text = "";
                            }
                            break;
                    }
                }
                if (tkmd.NgayKhoiHanhVC.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayKhoiHanhVC.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgayKhoiHanhVanChuyen.Text = tkmd.NgayKhoiHanhVC.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayKhoiHanhVanChuyen.Text = "";
                }
                for (int i = 0; i < tkmd.TrungChuyenCollection.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            lblDiaDiemTrungChuyen1.Text = String.IsNullOrEmpty(tkmd.TrungChuyenCollection[i].MaDiaDiem) ? "" : tkmd.TrungChuyenCollection[i].MaDiaDiem.ToString().ToUpper();
                            if (tkmd.TrungChuyenCollection[i].NgayDen.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.TrungChuyenCollection[i].NgayDen.ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblNgayDuKienDenDDTC1.Text = tkmd.TrungChuyenCollection[i].NgayDen.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblNgayDuKienDenDDTC1.Text = "";
                            }
                            if (tkmd.TrungChuyenCollection[i].NgayKhoiHanh.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.TrungChuyenCollection[i].NgayKhoiHanh.ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblNgayKhoiHanh1.Text = tkmd.TrungChuyenCollection[i].NgayKhoiHanh.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblNgayKhoiHanh1.Text = "";
                            }
                            break;
                        case 1:
                            lblDiaDiemTrungChuyen2.Text = String.IsNullOrEmpty(tkmd.TrungChuyenCollection[i].MaDiaDiem) ? "" : tkmd.TrungChuyenCollection[i].MaDiaDiem.ToString().ToUpper();
                            if (tkmd.TrungChuyenCollection[i].NgayDen.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.TrungChuyenCollection[i].NgayDen.ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblNgayDuKienDenDDTC2.Text = tkmd.TrungChuyenCollection[i].NgayDen.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblNgayDuKienDenDDTC2.Text = "";
                            }
                            if (tkmd.TrungChuyenCollection[i].NgayKhoiHanh.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.TrungChuyenCollection[i].NgayKhoiHanh.ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblNgayKhoiHanh2.Text = tkmd.TrungChuyenCollection[i].NgayKhoiHanh.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblNgayKhoiHanh2.Text = "";
                            }
                            break;
                        case 2:
                            lblDiaDiemTrungChuyen3.Text = String.IsNullOrEmpty(tkmd.TrungChuyenCollection[i].MaDiaDiem) ? "" : tkmd.TrungChuyenCollection[i].MaDiaDiem.ToString().ToUpper();
                            if (tkmd.TrungChuyenCollection[i].NgayDen.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.TrungChuyenCollection[i].NgayDen.ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblNgayDuKienDenDDTC3.Text = tkmd.TrungChuyenCollection[i].NgayDen.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblNgayDuKienDenDDTC3.Text = "";
                            }
                            if (tkmd.TrungChuyenCollection[i].NgayKhoiHanh.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.TrungChuyenCollection[i].NgayKhoiHanh.ToString("dd/MM/yyyy") != "01/01/0001")
                            {
                                lblNgayKhoiHanh3.Text = tkmd.TrungChuyenCollection[i].NgayKhoiHanh.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblNgayKhoiHanh3.Text = "";
                            }
                            break;
                    }
                }
                lblDiaDiemDichVanChuyen.Text = String.IsNullOrEmpty(tkmd.DiaDiemDichVC) ? "" : tkmd.DiaDiemDichVC.ToString().ToUpper();
                if (tkmd.NgayDen.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayDen.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgayDuKienDen.Text = tkmd.NgayDen.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayDuKienDen.Text = "";
                }
                BindReportChiThiHQ(tkmd);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
        }
        public void BindReportChiThiHQ( KDT_VNACC_ToKhaiMauDich tkmd)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Date", typeof(string));
                dt.Columns.Add("Ten", typeof(string));
                dt.Columns.Add("NoiDung", typeof(string));
                dt.Columns.Add("STT", typeof(string));
                int STT = 0;

                for (int i = 0; i < tkmd.ChiThiHQCollection.Count; i++)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    if (Convert.ToDateTime(tkmd.ChiThiHQCollection[i].Ngay).ToString("dd/MM/yyyy") != ("01/01/1900") && tkmd.NgayDen.ToString("dd/MM/yyyy") != "01/01/0001")
                    {
                        dr["Date"] = Convert.ToDateTime(tkmd.ChiThiHQCollection[i].Ngay).ToString("dd/MM/yyyy");
                    }
                    dr["Ten"] = String.IsNullOrEmpty(tkmd.ChiThiHQCollection[i].Ten) ? "" : tkmd.ChiThiHQCollection[i].Ten.ToString().ToUpper();
                    dr["STT"] = STT.ToString().ToUpper();
                    dr["NoiDung"] = String.IsNullOrEmpty(tkmd.ChiThiHQCollection[i].NoiDung) ? "" : tkmd.ChiThiHQCollection[i].NoiDung.ToString().ToUpper();
                    dt.Rows.Add(dr);
                }

                while (dt.Rows.Count < 10)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    dr["Date"] = "";
                    dr["Ten"] = "";
                    dr["NoiDung"] = "";
                    dr["STT"] = STT.ToString().ToUpper();
                    dt.Rows.Add(dr);
                }
                DetailReport.DataSource = dt;
                lblNgaychithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "Date");
                lblTenchithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "Ten");
                lblNoidungchithihaiquan1.DataBindings.Add("Text", DetailReport.DataSource, "NoiDung");
                lblSTTChiThiHQ.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrTable5_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblSoTrang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblSoTrang.Text = 2 + "/" + (count + 2);
        }
    }
}
