using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.Feedback.Components
{
	public partial class ThuVienCauHoi : ICloneable
	{
		#region Properties.
		
		public string MaCauHoi { set; get; }
		public string LinhVuc { set; get; }
		public string Phanloai { set; get; }
		public string TieuDe { set; get; }
		public string NoiDungCauHoi { set; get; }
		public string TraLoi { set; get; }
		public DateTime NgayTao { set; get; }
		public DateTime NgayCapNhat { set; get; }
		public bool HienThi { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<ThuVienCauHoi> ConvertToCollection(IDataReader reader)
		{
			IList<ThuVienCauHoi> collection = new List<ThuVienCauHoi>();
			while (reader.Read())
			{
				ThuVienCauHoi entity = new ThuVienCauHoi();
				if (!reader.IsDBNull(reader.GetOrdinal("MaCauHoi"))) entity.MaCauHoi = reader.GetString(reader.GetOrdinal("MaCauHoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("LinhVuc"))) entity.LinhVuc = reader.GetString(reader.GetOrdinal("LinhVuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("Phanloai"))) entity.Phanloai = reader.GetString(reader.GetOrdinal("Phanloai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuDe"))) entity.TieuDe = reader.GetString(reader.GetOrdinal("TieuDe"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungCauHoi"))) entity.NoiDungCauHoi = reader.GetString(reader.GetOrdinal("NoiDungCauHoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("TraLoi"))) entity.TraLoi = reader.GetString(reader.GetOrdinal("TraLoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTao"))) entity.NgayTao = reader.GetDateTime(reader.GetOrdinal("NgayTao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCapNhat"))) entity.NgayCapNhat = reader.GetDateTime(reader.GetOrdinal("NgayCapNhat"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThi"))) entity.HienThi = reader.GetBoolean(reader.GetOrdinal("HienThi"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<ThuVienCauHoi> collection, string maCauHoi)
        {
            foreach (ThuVienCauHoi item in collection)
            {
                if (item.MaCauHoi == maCauHoi)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_GopY_ThuVienCauHoi VALUES(@MaCauHoi, @LinhVuc, @Phanloai, @TieuDe, @NoiDungCauHoi, @TraLoi, @NgayTao, @NgayCapNhat, @HienThi, @GhiChu)";
            string update = "UPDATE t_GopY_ThuVienCauHoi SET LinhVuc = @LinhVuc, Phanloai = @Phanloai, TieuDe = @TieuDe, NoiDungCauHoi = @NoiDungCauHoi, TraLoi = @TraLoi, NgayTao = @NgayTao, NgayCapNhat = @NgayCapNhat, HienThi = @HienThi, GhiChu = @GhiChu WHERE MaCauHoi = @MaCauHoi";
            string delete = "DELETE FROM t_GopY_ThuVienCauHoi WHERE MaCauHoi = @MaCauHoi";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@MaCauHoi", SqlDbType.VarChar, "MaCauHoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LinhVuc", SqlDbType.VarChar, "LinhVuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Phanloai", SqlDbType.VarChar, "Phanloai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, "NoiDungCauHoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TraLoi", SqlDbType.NVarChar, "TraLoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTao", SqlDbType.DateTime, "NgayTao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapNhat", SqlDbType.DateTime, "NgayCapNhat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThi", SqlDbType.Bit, "HienThi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@MaCauHoi", SqlDbType.VarChar, "MaCauHoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LinhVuc", SqlDbType.VarChar, "LinhVuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Phanloai", SqlDbType.VarChar, "Phanloai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, "NoiDungCauHoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TraLoi", SqlDbType.NVarChar, "TraLoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTao", SqlDbType.DateTime, "NgayTao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapNhat", SqlDbType.DateTime, "NgayCapNhat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThi", SqlDbType.Bit, "HienThi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@MaCauHoi", SqlDbType.VarChar, "MaCauHoi", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_GopY_ThuVienCauHoi VALUES(@MaCauHoi, @LinhVuc, @Phanloai, @TieuDe, @NoiDungCauHoi, @TraLoi, @NgayTao, @NgayCapNhat, @HienThi, @GhiChu)";
            string update = "UPDATE t_GopY_ThuVienCauHoi SET LinhVuc = @LinhVuc, Phanloai = @Phanloai, TieuDe = @TieuDe, NoiDungCauHoi = @NoiDungCauHoi, TraLoi = @TraLoi, NgayTao = @NgayTao, NgayCapNhat = @NgayCapNhat, HienThi = @HienThi, GhiChu = @GhiChu WHERE MaCauHoi = @MaCauHoi";
            string delete = "DELETE FROM t_GopY_ThuVienCauHoi WHERE MaCauHoi = @MaCauHoi";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@MaCauHoi", SqlDbType.VarChar, "MaCauHoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LinhVuc", SqlDbType.VarChar, "LinhVuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Phanloai", SqlDbType.VarChar, "Phanloai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, "NoiDungCauHoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TraLoi", SqlDbType.NVarChar, "TraLoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTao", SqlDbType.DateTime, "NgayTao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapNhat", SqlDbType.DateTime, "NgayCapNhat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThi", SqlDbType.Bit, "HienThi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@MaCauHoi", SqlDbType.VarChar, "MaCauHoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LinhVuc", SqlDbType.VarChar, "LinhVuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Phanloai", SqlDbType.VarChar, "Phanloai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, "NoiDungCauHoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TraLoi", SqlDbType.NVarChar, "TraLoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTao", SqlDbType.DateTime, "NgayTao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapNhat", SqlDbType.DateTime, "NgayCapNhat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThi", SqlDbType.Bit, "HienThi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@MaCauHoi", SqlDbType.VarChar, "MaCauHoi", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static ThuVienCauHoi Load(string maCauHoi)
		{
			const string spName = "[dbo].[p_GopY_ThuVienCauHoi_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaCauHoi", SqlDbType.VarChar, maCauHoi);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<ThuVienCauHoi> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<ThuVienCauHoi> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<ThuVienCauHoi> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<ThuVienCauHoi> SelectCollectionBy_LinhVuc(string linhVuc)
		{
            IDataReader reader = SelectReaderBy_LinhVuc(linhVuc);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_LinhVuc(string linhVuc)
		{
			const string spName = "[dbo].[p_GopY_ThuVienCauHoi_SelectBy_LinhVuc]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LinhVuc", SqlDbType.VarChar, linhVuc);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_GopY_ThuVienCauHoi_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_GopY_ThuVienCauHoi_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_GopY_ThuVienCauHoi_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_GopY_ThuVienCauHoi_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_LinhVuc(string linhVuc)
		{
			const string spName = "p_GopY_ThuVienCauHoi_SelectBy_LinhVuc";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LinhVuc", SqlDbType.VarChar, linhVuc);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.

        public static int InsertThuVienCauHoi(string maCauHoi, string linhVuc, string phanloai, string tieuDe, string noiDungCauHoi, string traLoi, DateTime ngayTao, DateTime ngayCapNhat, bool hienThi, string ghiChu)
		{
			ThuVienCauHoi entity = new ThuVienCauHoi();	
			entity.LinhVuc = linhVuc;
			entity.Phanloai = phanloai;
			entity.TieuDe = tieuDe;
			entity.NoiDungCauHoi = noiDungCauHoi;
			entity.TraLoi = traLoi;
			entity.NgayTao = ngayTao;
			entity.NgayCapNhat = ngayCapNhat;
			entity.HienThi = hienThi;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_GopY_ThuVienCauHoi_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaCauHoi", SqlDbType.VarChar, MaCauHoi);
			db.AddInParameter(dbCommand, "@LinhVuc", SqlDbType.VarChar, LinhVuc);
			db.AddInParameter(dbCommand, "@Phanloai", SqlDbType.VarChar, Phanloai);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.NVarChar, TieuDe);
			db.AddInParameter(dbCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, NoiDungCauHoi);
			db.AddInParameter(dbCommand, "@TraLoi", SqlDbType.NVarChar, TraLoi);
			db.AddInParameter(dbCommand, "@NgayTao", SqlDbType.DateTime, NgayTao.Year <= 1753 ? DBNull.Value : (object) NgayTao);
			db.AddInParameter(dbCommand, "@NgayCapNhat", SqlDbType.DateTime, NgayCapNhat.Year <= 1753 ? DBNull.Value : (object) NgayCapNhat);
			db.AddInParameter(dbCommand, "@HienThi", SqlDbType.Bit, HienThi);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<ThuVienCauHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ThuVienCauHoi item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateThuVienCauHoi(string maCauHoi, string linhVuc, string phanloai, string tieuDe, string noiDungCauHoi, string traLoi, DateTime ngayTao, DateTime ngayCapNhat, bool hienThi, string ghiChu)
		{
			ThuVienCauHoi entity = new ThuVienCauHoi();			
			entity.MaCauHoi = maCauHoi;
			entity.LinhVuc = linhVuc;
			entity.Phanloai = phanloai;
			entity.TieuDe = tieuDe;
			entity.NoiDungCauHoi = noiDungCauHoi;
			entity.TraLoi = traLoi;
			entity.NgayTao = ngayTao;
			entity.NgayCapNhat = ngayCapNhat;
			entity.HienThi = hienThi;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_GopY_ThuVienCauHoi_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaCauHoi", SqlDbType.VarChar, MaCauHoi);
			db.AddInParameter(dbCommand, "@LinhVuc", SqlDbType.VarChar, LinhVuc);
			db.AddInParameter(dbCommand, "@Phanloai", SqlDbType.VarChar, Phanloai);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.NVarChar, TieuDe);
			db.AddInParameter(dbCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, NoiDungCauHoi);
			db.AddInParameter(dbCommand, "@TraLoi", SqlDbType.NVarChar, TraLoi);
			db.AddInParameter(dbCommand, "@NgayTao", SqlDbType.DateTime, NgayTao.Year <= 1753 ? DBNull.Value : (object) NgayTao);
			db.AddInParameter(dbCommand, "@NgayCapNhat", SqlDbType.DateTime, NgayCapNhat.Year <= 1753 ? DBNull.Value : (object) NgayCapNhat);
			db.AddInParameter(dbCommand, "@HienThi", SqlDbType.Bit, HienThi);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<ThuVienCauHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ThuVienCauHoi item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateThuVienCauHoi(string maCauHoi, string linhVuc, string phanloai, string tieuDe, string noiDungCauHoi, string traLoi, DateTime ngayTao, DateTime ngayCapNhat, bool hienThi, string ghiChu)
		{
			ThuVienCauHoi entity = new ThuVienCauHoi();			
			entity.MaCauHoi = maCauHoi;
			entity.LinhVuc = linhVuc;
			entity.Phanloai = phanloai;
			entity.TieuDe = tieuDe;
			entity.NoiDungCauHoi = noiDungCauHoi;
			entity.TraLoi = traLoi;
			entity.NgayTao = ngayTao;
			entity.NgayCapNhat = ngayCapNhat;
			entity.HienThi = hienThi;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_GopY_ThuVienCauHoi_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaCauHoi", SqlDbType.VarChar, MaCauHoi);
			db.AddInParameter(dbCommand, "@LinhVuc", SqlDbType.VarChar, LinhVuc);
			db.AddInParameter(dbCommand, "@Phanloai", SqlDbType.VarChar, Phanloai);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.NVarChar, TieuDe);
			db.AddInParameter(dbCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, NoiDungCauHoi);
			db.AddInParameter(dbCommand, "@TraLoi", SqlDbType.NVarChar, TraLoi);
			db.AddInParameter(dbCommand, "@NgayTao", SqlDbType.DateTime, NgayTao.Year <= 1753 ? DBNull.Value : (object) NgayTao);
			db.AddInParameter(dbCommand, "@NgayCapNhat", SqlDbType.DateTime, NgayCapNhat.Year <= 1753 ? DBNull.Value : (object) NgayCapNhat);
			db.AddInParameter(dbCommand, "@HienThi", SqlDbType.Bit, HienThi);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<ThuVienCauHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ThuVienCauHoi item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteThuVienCauHoi(string maCauHoi)
		{
			ThuVienCauHoi entity = new ThuVienCauHoi();
			entity.MaCauHoi = maCauHoi;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_GopY_ThuVienCauHoi_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaCauHoi", SqlDbType.VarChar, MaCauHoi);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_LinhVuc(string linhVuc)
		{
			const string spName = "[dbo].[p_GopY_ThuVienCauHoi_DeleteBy_LinhVuc]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LinhVuc", SqlDbType.VarChar, linhVuc);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_GopY_ThuVienCauHoi_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<ThuVienCauHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ThuVienCauHoi item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}