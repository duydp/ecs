﻿using System;
using System.Drawing;
using Company.BLL.DuLieuChuan;

using Company.BLL.KDT;

using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.BLL;
using Company.BLL.Utils;
using System.Data;

namespace Company.Interface
{
    public partial class HangMauDichForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        private decimal clg;
        private decimal dgnt;
        public HangMauDichCollection HMDCollection = new HangMauDichCollection();
        public string LoaiHangHoa;
        public ToKhaiMauDich TKMD;
        private decimal luong;

        public string MaNguyenTe;
        public string MaNuocXX;
        public string NhomLoaiHinh = string.Empty;
        private decimal st_clg;
        private bool edit = false;
        //-----------------------------------------------------------------------------------------				
        private decimal tgnt;
        private decimal tgtt_gtgt;

        private decimal tgtt_nk;
        private decimal tgtt_ttdb;
        private decimal tl_clg;
        private decimal tongTGKB;
        private decimal ts_gtgt;

        private decimal ts_nk;
        private decimal ts_ttdb;
        private decimal tt_gtgt;

        private decimal tt_nk;
        private decimal tt_ttdb;
        public decimal TyGiaTT;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
        private decimal LuongCon = 0;
        //-----------------------------------------------------------------------------------------
        //DATLMQ bổ sung biến phục vụ cho việc ghi Log Xóa thông tin HMD ngày 12/08/2011
        public static bool isDeleted = false;
        //-----------------------------------------------------------------------------------------

        public HangMauDichForm()
        {
            InitializeComponent();
        }
        private void SetVisibleHTS(bool b)
        {
            txtMa_HTS.Visible = lblMa_HTS.Visible = txtSoLuong_HTS.Visible = lblSoLuong_HTS.Visible = cbbDVT_HTS.Visible = lblDVT_HTS.Visible = b;
        }
        private void khoitao_GiaoDien()
        {
            if (this.NhomLoaiHinh.StartsWith("N")) SetVisibleHTS(false);
            else
            {
                if (this.LoaiHangHoa != "S" || GlobalSettings.MaHTS == 0) SetVisibleHTS(false);
            }
            if (this.NhomLoaiHinh.Substring(1, 2) == "SX")
            {
                txtMaHang.ButtonStyle = EditButtonStyle.Ellipsis;
                cbDonViTinh.ReadOnly = true;
            }
            if (this.LoaiHangHoa == "S")
            {
                dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
                txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
            }
            else if (this.LoaiHangHoa == "N")
            {
                dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            }

        }

        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            cbbDVT_HTS.DataSource = this._DonViTinh;
            cbbDVT_HTS.SelectedValue = "15 ";
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
        }

        private decimal tinhthue()
        {

            this.tgnt = this.dgnt * this.luong;
            this.tgtt_nk = this.tgnt * this.TyGiaTT;

            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            //txtTGTT_TTDB.Value = this.tgtt_ttdb;
            //txtTGTT_GTGT.Value = this.tgtt_gtgt;
            //txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            //txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb,0);
            //txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt,0);
            //txtTien_CLG.Value = Math.Round(this.st_clg);

            //txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg,0);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        private decimal tinhthue2()
        {
            this.dgnt = Convert.ToDecimal(txtDGNT.Value);
            this.luong = Convert.ToDecimal(txtLuong.Value);
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            this.tgnt = this.dgnt * this.luong;
            this.tgtt_nk = Convert.ToDecimal(txtTGTT_NK.Value);
            if (tgtt_nk == 0)
                tgtt_nk = tgnt * TyGiaTT;
            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            //txtTGTT_TTDB.Value = this.tgtt_ttdb;
            //txtTGTT_GTGT.Value = this.tgtt_gtgt;
            //txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            //txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb,0);
            //txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt,0);
            //txtTien_CLG.Value = Math.Round(this.st_clg,0);

            //txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg,0);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }

        //-----------------------------------------------------------------------------------------
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangMauDich hmd in HMDCollection)
            {
                if (hmd.MaPhu.Trim() == maHang) return true;
            }
            return false;
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            if (txtMaHang.Text.Trim() == "") return;
            //this.fillNguyenPhuLieu(txtMaNPL.Text);
            if (NhomLoaiHinh.IndexOf("SX") > 0)
            {
                if (LoaiHangHoa == "N")
                {
                    Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                    npl.Ma = txtMaHang.Text.Trim();
                    npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    if (npl.Load())
                    {
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        epError.SetIconPadding(txtMaHang, -8);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            epError.SetError(txtMaHang, "Không tồn tại nguyên phụ liệu này.");
                        }
                        else
                        {
                            epError.SetError(txtMaHang, "This material haven't exist.");
                        }

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                        cbDonViTinh.SelectedValue = "";
                    }
                }
                else
                {
                    Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                    sp.Ma = txtMaHang.Text.Trim();
                    sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    if (sp.Load())
                    {
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        epError.SetIconPadding(txtMaHang, -8);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            epError.SetError(txtMaHang, "Không tồn tại sản phẩm này.");
                        }
                        else
                        {
                            epError.SetError(txtMaHang, "This product haven't exist.");
                        }

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                        cbDonViTinh.SelectedValue = "";
                    }
                }

            }
            tinhthue2();
            cvError.Validate();
            if (!cvError.IsValid) return;
            txtMaHang.Focus();
            //if (!MaHS.Validate(txtMaHS.Text, 10))
            //{
            //    epError.SetIconPadding(txtMaHS, -8);
            //    if (GlobalSettings.NGON_NGU == "0")
            //    {
            //        epError.SetError(txtMaHS, "Mã số HS không hợp lệ.");
            //    }
            //    else
            //    {
            //        epError.SetError(txtMaHS, "HS code is invalid");
            //    }

            //    return;
            //}
            if (checkMaHangExit(txtMaHang.Text))
            {
                //  ShowMessage("Mặt hàng này đã có rồi, bạn hãy chọn mặt hàng khác.", false);
                string result = ShowMessage("Mặt hàng này đã có rồi, bạn có muốn tiếp tục không.", true);
                if (result != "Yes")
                    return;
            }

            HangMauDich hmd = new HangMauDich();
            hmd.SoThuTuHang = 0;
            hmd.MaHS = txtMaHS.Text;
            hmd.MaPhu = txtMaHang.Text.Trim();
            hmd.TenHang = txtTenHang.Text.Trim();
            hmd.NuocXX_ID = ctrNuocXX.Ma;
            hmd.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            hmd.SoLuong = Convert.ToDecimal(txtLuong.Value);
            hmd.DonGiaKB = Convert.ToDecimal(txtDGNT.Value);
            hmd.Ma_HTS = txtMa_HTS.Text;
            hmd.DVT_HTS = cbbDVT_HTS.SelectedValue.ToString();
            hmd.SoLuong_HTS = Convert.ToDecimal(txtSoLuong_HTS.Value);
            hmd.TriGiaKB = Convert.ToDecimal(txtTGNT.Value);
            hmd.TriGiaKB_VND = Convert.ToDecimal(txtTriGiaKB.Value);
            hmd.TriGiaTT = Convert.ToDecimal(txtTGTT_NK.Value);
            hmd.ThueSuatXNK = Convert.ToDecimal(txtTS_NK.Value);
            //hmd.ThueSuatTTDB = Convert.ToDecimal(txtTS_TTDB.Value);
            //hmd.ThueSuatGTGT = Convert.ToDecimal(txtTS_GTGT.Value);
            hmd.TyLeThuKhac = Convert.ToDecimal(txtTyLeThuKhac.Value);
            hmd.PhuThu = (txtPhuThu.Text != "" ? Convert.ToDecimal(txtPhuThu.Text) : 0);
            hmd.ThueXNK = Math.Round(Convert.ToDecimal(txtTienThue_NK.Value), MidpointRounding.AwayFromZero);
            //hmd.ThueTTDB = Math.Round(Convert.ToDecimal(txtTienThue_TTDB.Value),0);
            //hmd.ThueGTGT = Math.Round(Convert.ToDecimal(txtTienThue_GTGT.Value),0);
            hmd.DonGiaTT = hmd.TriGiaTT / hmd.SoLuong;
            hmd.ThueSuatGiam = txtTSXNKGiam.Text.Trim();
            //if (chkMienThue.Checked) 
            //    hmd.MienThue = 1;
            //else
            //    hmd.MienThue = 0;

            // Tính thuế.
            //hmd.TinhThue(this.TyGiaTT);

            this.HMDCollection.Add(hmd);

            TKMD.HeSoNhan = Convert.ToDouble(txtHeSoNhan.Value);

            this.refresh_STTHang();
            dgList.DataSource = this.HMDCollection;
            dgList.Refetch();
            txtMaHang.Text = "";
            txtTenHang.Text = "";
            txtMaHS.Text = "";
            txtLuong.Value = 0;
            txtDGNT.Value = 0;
            txtTGTT_NK.Value = 0;
            txtMa_HTS.Text = "";
            txtSoLuong_HTS.Value = 0;
            txtTGNT.Value = 0;
            txtTriGiaKB.Value = 0;
            this.edit = false;
            epError.Clear();
            LuongCon = 0;

        }
        private void TinhTriGiaTinhThue()
        {
            decimal Phi = TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen;
            decimal TongTriGiaHang = 0;
            foreach (HangMauDich hmd in HMDCollection)
            {
                TongTriGiaHang += hmd.TriGiaKB;
            }
            decimal TriGiaTTMotDong = Phi / TongTriGiaHang;
            foreach (HangMauDich hmd in HMDCollection)
            {
                hmd.TriGiaTT = (TriGiaTTMotDong * hmd.TriGiaKB + hmd.TriGiaKB) * TyGiaTT;
                hmd.DonGiaTT = hmd.TriGiaTT / hmd.SoLuong;
                hmd.ThueXNK = (hmd.TriGiaTT * hmd.ThueSuatXNK) / 100;
                //Them Thue Khac
                hmd.PhuThu = (hmd.TyLeThuKhac * hmd.ThueXNK) / 10;
            }

        }
        //-----------------------------------------------------------------------------------------
        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            if (txtMaHang.Text.Trim() == "") return;
            //this.fillNguyenPhuLieu(txtMaNPL.Text);
            if (NhomLoaiHinh.IndexOf("SX") > 0)
            {
                if (LoaiHangHoa == "N")
                {
                    Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                    npl.Ma = txtMaHang.Text.Trim();
                    npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    npl.MaHaiQuan = MaHaiQuan;
                    if (npl.Load())
                    {
                        txtMaHang.Text = npl.Ma;
                        //if(txtMaHS.Text.Trim().Length==0)
                        txtMaHS.Text = npl.MaHS;
                        txtTenHang.Text = npl.Ten;
                        cbDonViTinh.SelectedValue = npl.DVT_ID;
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        epError.SetIconPadding(txtMaHang, -8);

                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            epError.SetError(txtMaHang, "Không tồn tại nguyên phụ liệu này.");
                        }
                        else
                        {
                            epError.SetError(txtMaHang, "This material hasn't exist.");
                        }

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                        cbDonViTinh.SelectedValue = "";
                    }
                }
                else
                {
                    Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                    sp.Ma = txtMaHang.Text.Trim();
                    sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    sp.MaHaiQuan = MaHaiQuan;
                    if (sp.Load())
                    {
                        txtMaHang.Text = sp.Ma;
                        //if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = sp.MaHS;
                        txtTenHang.Text = sp.Ten;
                        cbDonViTinh.SelectedValue = sp.DVT_ID;
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        epError.SetIconPadding(txtMaHang, -8);

                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            epError.SetError(txtMaHang, "Không tồn tại sản phẩm này.");
                        }
                        else
                        {
                            epError.SetError(txtMaHang, "This product hasn't exist .");
                        }
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                        cbDonViTinh.SelectedValue = "";
                    }
                }

            }


        }

        private void HangMauDichForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDien();
            if (LoaiHangHoa == "N")
                txtSoLuong_HTS.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            else
                txtSoLuong_HTS.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
            lblNguyenTe_DGNT.Text = lblNguyenTe_TGNT.Text = "(" + this.MaNguyenTe + ")";
            toolTip1.SetToolTip(lblNguyenTe_DGNT, this.MaNguyenTe);
            toolTip1.SetToolTip(lblNguyenTe_TGNT, this.MaNguyenTe);

            this.tongTGKB = 0;
            decimal tongSoLuongHang = 0;
            decimal tongThue = 0;

            foreach (HangMauDich hmd in this.HMDCollection)
            {
                // Tính lại thuế.
                //hmd.TinhThue(this.TyGiaTT);
                // Tổng trị giá khai báo.
                this.tongTGKB += hmd.TriGiaKB;

                tongSoLuongHang += hmd.SoLuong;
                tongThue += hmd.ThueXNK + hmd.PhuThu;

            }
            dgList.DataSource = this.HMDCollection;
            txtHeSoNhan.Text = TKMD.HeSoNhan.ToString();
            if (GlobalSettings.NGON_NGU == "0")
            {
                //lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
                string val = TKMD.TongTriGiaKhaiBao.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT));

                lblTongTGNT.Text = string.Format("{0} ({1})", val != "" ? val : "0", "USD");
                lblTongThue.Text = string.Format("{0} (VND)", tongThue.ToString("N"));
                lblTongSoHang.Text = string.Format("{0}", tongSoLuongHang.ToString("N" + GlobalSettings.SoThapPhan.LuongSP));
            }
            else
            {
                lblTongTGKB.Text = string.Format("Total of value original currency: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
            }
            //lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
            //lblTyGiaTT.Text = "Tỷ giá tính thuế: " + this.TyGiaTT.ToString("N");
            if (this.OpenType == OpenFormType.View)
                btnAddNew.Visible = false;
        }

        private void txtLuong_Leave(object sender, EventArgs e)
        {
            try
            {
                this.luong = Convert.ToDecimal(txtLuong.Value);
                txtSoLuong_HTS.Value = Math.Round(this.luong / 12);
                this.tinhthue();
                epError.SetError(txtDGNT, null);
                epError.SetError(txtLuong, null);
            }
            catch
            {
                epError.SetError(txtDGNT, setText("Dữ liệu không hợp lệ", "Invalid input value"));
                epError.SetError(txtLuong, setText("Dữ liệu không hợp lệ", "Invalid input value"));
            }
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            try
            {
                this.dgnt = Convert.ToDecimal(txtDGNT.Value);
                this.tinhthue();
                epError.SetError(txtDGNT, null);
                epError.SetError(txtLuong, null);
            }
            catch
            {
                epError.SetError(txtDGNT, setText("Dữ liệu không hợp lệ", "Invalid input value"));
                epError.SetError(txtLuong, setText("Dữ liệu không hợp lệ", "Invalid input value"));
            }
        }

        private void txtTS_NK_Leave(object sender, EventArgs e)
        {
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            this.tinhthue2();
        }

        //private void txtTS_TTDB_Leave(object sender, EventArgs e)
        //{
        //    this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value)/100;
        //    this.tinhthue2();
        //}

        //private void txtTS_GTGT_Leave(object sender, EventArgs e)
        //{
        //    this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value)/100;
        //    this.tinhthue2();
        //}

        //private void txtTS_CLG_Leave(object sender, EventArgs e)
        //{
        //    this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value)/100;
        //    this.tinhthue2();
        //}

        //-----------------------------------------------------------------------------------------------
        private void refresh_STTHang()
        {
            int i = 1;
            this.tongTGKB = 0;
            decimal tongThue = 0;
            decimal tongSoLuongHang = 0;

            foreach (HangMauDich hmd in this.HMDCollection)
            {
                hmd.SoThuTuHang = i++;
                this.tongTGKB += Convert.ToDecimal(hmd.TriGiaKB);
                tongThue += hmd.ThueXNK + hmd.PhuThu;

                tongSoLuongHang += hmd.SoLuong;
            }
            TKMD.TongTriGiaKhaiBao = this.tongTGKB;
            TKMD.TongTriGiaTinhThue = this.tongTGKB * TyGiaTT;
            if (GlobalSettings.NGON_NGU == "0")
            {
                //lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1})         Tổng thuế : {2} (VND)", this.tongTGKB.ToString("N"), this.MaNguyenTe, tongThue);
                string val = TKMD.TongTriGiaKhaiBao.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT));

                lblTongTGNT.Text = string.Format("{0} ({1})", val != "" ? val : "0", "USD");
                lblTongThue.Text = string.Format("{0} (VND)", tongThue.ToString("N"));
                lblTongSoHang.Text = string.Format("{0}", tongSoLuongHang.ToString("N" + GlobalSettings.SoThapPhan.LuongSP));

            }
            else
            {
                lblTongTGKB.Text = string.Format("Total of value original currency: {0} ({1})         Tổng thuế : {2} (VND)", this.tongTGKB.ToString("N"), this.MaNguyenTe, tongThue);
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);
        }

        //private void chkMienThue_CheckedChanged(object sender, EventArgs e)
        //{
        //    grbThue.Enabled = !chkMienThue.Checked;
        //    txtTS_NK.Value = 0;
        //    txtTS_TTDB.Value = 0;
        //    txtTS_GTGT.Value = 0;
        //    txtTL_CLG.Value = 0;
        //    this.tinhthue2();
        //}

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            switch (this.NhomLoaiHinh)
            {
                case "NSX":
                    if (this.NPLRegistedForm == null)
                        this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                    this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                    this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ;
                    this.NPLRegistedForm.ShowDialog();
                    if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
                    {
                        txtMaHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    break;
                case "XSX":
                    if (this.LoaiHangHoa == "N")
                    {
                        if (this.NPLRegistedForm == null)
                            this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                        this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                        this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ;
                        this.NPLRegistedForm.ShowDialog();
                        if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
                        {
                            txtMaHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                            txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                            txtMaHS.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                            cbDonViTinh.SelectedValue = this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                    }
                    else
                    {
                        if (this.SPRegistedForm == null)
                            this.SPRegistedForm = new SanPhamRegistedForm();
                        this.SPRegistedForm.CalledForm = "HangMauDichForm";
                        this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        this.SPRegistedForm.ShowDialog();
                        if (this.SPRegistedForm.SanPhamSelected.Ma != "" && this.SPRegistedForm.SanPhamSelected != null)
                        {
                            txtMaHang.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                            txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                            txtMaHS.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
                            cbDonViTinh.SelectedValue = this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                    }
                    break;

            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    HangMauDichEditForm f = new HangMauDichEditForm();
                    f.HMD = this.HMDCollection[i.Position];

                    f.NhomLoaiHinh = this.NhomLoaiHinh;
                    f.LoaiHangHoa = this.LoaiHangHoa;
                    f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    f.MaNguyenTe = this.MaNguyenTe;
                    f.TyGiaTT = this.TyGiaTT;
                    f.collection = this.HMDCollection;
                    f.TKMD = this.TKMD;
                    if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        f.OpenType = OpenFormType.Edit;
                    else
                        f.OpenType = OpenFormType.View;
                    f.ShowDialog();
                    if (f.IsEdited)
                    {
                        if (f.HMD.TKMD_ID > 0)
                            f.HMD.Update();
                        else
                            this.HMDCollection[i.Position] = f.HMD;
                    }
                    else if (f.IsDeleted)
                    {
                        if (f.HMD.TKMD_ID > 0)
                            f.HMD.Delete();
                        this.HMDCollection.RemoveAt(i.Position);
                    }
                    dgList.DataSource = this.HMDCollection;
                    try
                    {
                        dgList.Refetch();
                    }
                    catch { dgList.DataSource = this.HMDCollection; }
                    refresh_STTHang();
                    //lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
                }
                break;
            }
        }


        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                //  if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                if (MLMessages("Bạn có muốn xóa hàng hóa này không?", "MSG_DEL01", "", true) == "Yes")
                {
                    HangMauDich hmd = (HangMauDich)e.Row.DataRow;
                    if (hmd.ID > 0)
                    {
                        hmd.Delete();
                    }
                    this.tongTGKB -= hmd.TriGiaKB;
                    //lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
                    }
                    else
                    {
                        lblTongTGKB.Text = string.Format("Total of value original currency: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (true)
            {
                if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                {
                    // ShowMessage("Tờ khai đã được duyệt không được sửa", false);
                    MLMessages("Tờ khai đã được duyệt không được sửa", "MSG_SEN12", "", false);
                    e.Cancel = true;
                    return;
                }
                // if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                if (MLMessages("Bạn có muốn xóa hàng hóa này không?", "MSG_DEL01", "", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            HangMauDich hmd = (HangMauDich)i.GetRow().DataRow;
                            if (hmd.ID > 0)
                            {
                                if (TKMD.HMDCollection.Count > 0)
                                {
                                    this.ShowMessageTQDT("Thông báo", "Thông tin nguyên phụ liệu '" + i.GetRow().Cells["TenHang"].Text + "'" +
                                    " này đang sử dụng trong các chứng từ Hợp đồng, Hóa đơn thương mại, Giấy phép.\nKhông thể xóa!.", false);
                                    return;
                                }

                                hmd.Delete();
                            }
                        }
                    }
                    refresh_STTHang();
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO && TKMD.ID > 0)
                    {
                        this.TKMD.Update();
                    }

                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void SaoChepCha_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            HangMauDich hmd = (HangMauDich)dgList.GetRow().DataRow;
            txtMaHang.Text = hmd.MaPhu;
            txtTenHang.Text = hmd.TenHang;
            txtMaHS.Text = hmd.MaHS;
            cbDonViTinh.SelectedValue = hmd.DVT_ID;
            ctrNuocXX.Ma = hmd.NuocXX_ID;
            txtDGNT.Value = this.dgnt = hmd.DonGiaKB;
            txtLuong.Value = this.luong = hmd.SoLuong;
            txtTGNT.Value = hmd.TriGiaKB;
            txtTriGiaKB.Value = hmd.TriGiaKB_VND;
            txtTS_NK.Value = this.ts_nk = hmd.ThueSuatXNK;
            //txtTS_TTDB.Value = this.ts_ttdb = hmd.ThueSuatTTDB;
            //txtTS_GTGT.Value = this.ts_gtgt = hmd.ThueSuatGTGT;
            //txtTL_CLG.Value = this.tl_clg = hmd.TyLeThuKhac;
            txtLuong.Focus();
        }

        private void txtTGTT_NK_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtTGTT_NK_Leave(object sender, EventArgs e)
        {
            this.tinhthue2();
            txtPhuThu.Value = (decimal.Parse(txtTGTT_NK.Text) / 100) * decimal.Parse(txtTyLeThuKhac.Text);
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                epError.SetIconPadding(txtMaHS, -8);

                if (GlobalSettings.NGON_NGU == "0")
                {
                    epError.SetError(txtMaHS, "Mã HS không hợp lệ.");
                }
                else
                {
                    epError.SetError(txtMaHS, "HS code is invalid.");
                }
            }
            else
            {
                epError.SetError(txtMaHS, string.Empty);
            }
        }

        private void txtTGNT_Leave(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(txtLuong.Value) > 0)
                txtDGNT.Value = Convert.ToDecimal(txtTGNT.Value) / Convert.ToDecimal(txtLuong.Value);
            this.tinhthue2();
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }

        public static bool checkExistHMD(long _TKMD_ID, string _mahang)
        {
            HangMauDich hmd = new HangMauDich();
            string where = "1 = 1";
            where += string.Format(" AND TKMD_ID = {0} AND MaPhu = '{1}'", _TKMD_ID, _mahang);
            HangMauDichCollection hmdColl = hmd.SelectCollectionDynamic(where, "");
            if (hmdColl.Count > 0)
            {
                return true;
            }
            return false;
        }

        private void xoa()
        {
            try
            {
                HangMauDichCollection hmdcoll = new HangMauDichCollection();
                //if (showMsg("MSG_DEL01", true) == "Yes")
                if (MLMessages("Bạn có muốn xóa không?", "MSG_DEL01", "", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            HangMauDich hmd = (HangMauDich)i.GetRow().DataRow;

                            if (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID).Count > 0)
                            {
                                this.ShowMessageTQDT("Thông báo", "Thông tin hàng mậu dịch '" + hmd.MaPhu + "'" +
                                " này đang sử dụng trong chứng từ: \n Hóa đơn thương mại.\n\nKhông thể xóa!.", false);
                                return;
                            }

                            else if (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID).Count > 0)
                            {
                                this.ShowMessageTQDT("Thông báo", "Thông tin hàng mậu dịch '" + hmd.MaPhu + "'" +
                                " này đang sử dụng trong chứng từ: \n Hợp đồng thương mại.\n\nKhông thể xóa!.", false);
                                return;
                            }

                            else if (Company.KDT.SHARE.QuanLyChungTu.HangGiayPhepDetail.SelectCollectionDynamic("HMD_ID = " + hmd.ID, "").Count > 0)
                            {
                                this.ShowMessageTQDT("Thông báo", "Thông tin hàng mậu dịch '" + hmd.MaPhu + "'" +
                                " này đang sử dụng trong chứng từ: \n Giấy phép.\n\nKhông thể xóa!.", false);
                                return;
                            }

                            //Them object bi xoa vao collection -> de xoa tren luoi va collection trong TKMD.
                            hmdcoll.Add(hmd);

                            //DATLMQ bổ sung Lưu tạm giá trị Hàng mậu dịch vào DeleteHMDCollection ngày 11/08/2011
                            if (checkExistHMD(TKMD.ID, hmd.MaPhu))
                            {
                                ToKhaiMauDichForm.deleteHMDCollection.Add(hmd);
                                isDeleted = true;
                            }

                            if (hmd.ID > 0)
                            {
                                hmd.Delete();
                            }
                        }
                    }
                    foreach (HangMauDich hmdDelete in hmdcoll)
                    {
                        this.TKMD.HMDCollection.Remove(hmdDelete);
                    }

                    dgList.DataSource = this.TKMD.HMDCollection;

                    try
                    {
                        dgList.Refetch();
                    }
                    catch (Exception ex)
                    {
                        dgList.Refresh();
                    }

                    decimal tongSoLuongHang = 0;
                    decimal tongThue = 0;

                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        // Tính lại thuế.
                        //hmd.TinhThue(this.TyGiaTT);
                        // Tổng trị giá khai báo.
                        this.tongTGKB += hmd.TriGiaKB;

                        tongSoLuongHang += hmd.SoLuong;
                        tongThue += hmd.ThueXNK + hmd.PhuThu;
                    }

                    TKMD.TongTriGiaKhaiBao = this.HMDCollection.Count > 0 ? this.tongTGKB : 0;
                    string val = TKMD.TongTriGiaKhaiBao.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT));

                    lblTongTGNT.Text = string.Format("{0} ({1})", val != "" ? val : "0", "USD");
                    lblTongThue.Text = string.Format("{0} (VND)", tongThue.ToString("N"));
                    lblTongSoHang.Text = string.Format("{0}", tongSoLuongHang.ToString("N" + GlobalSettings.SoThapPhan.LuongSP));

                }
            }
            catch (Exception ex)
            {
                Globals.ShowMessage("Lỗi xóa hàng mậu dịch ", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.xoa();
        }

        private void txtTGNT_Click(object sender, EventArgs e)
        {

        }

        private void HangMauDichForm_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (LoaiHangHoa == "N")
            {
                double heso = Convert.ToDouble(txtHeSoNhan.Value);
                if (heso != TKMD.HeSoNhan)
                {
                    if (ShowMessage("Bạn có muốn lưu lại hệ số quy đổi giá CIF không ?", true) == "Yes")
                    {
                        TKMD.HeSoNhan = heso;
                    }
                }
            }
        }

        private void txtTyLeThuKhac_Leave(object sender, EventArgs e)
        {
            txtPhuThu.Value = (Convert.ToDecimal(txtTGTT_NK.Value) / 100) * Convert.ToDecimal(txtTyLeThuKhac.Value);
        }

        private void txtPhuThu_Leave(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(txtTGTT_NK.Value) == 0) return;

            txtTyLeThuKhac.Value = Convert.ToDecimal(txtPhuThu.Value) / (Convert.ToDecimal(txtTGTT_NK.Value) / 100);

        }
    }
}
