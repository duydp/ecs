using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using GemBox.Spreadsheet;
namespace Company.Interface.SXXK
{
    public partial class ImportSPForm : BaseForm
    {
        public SanPhamCollection SPCollection = new SanPhamCollection();
        public SanPhamCollection SPReadCollection = new SanPhamCollection();
        public SanPhamCollection SPChuaCoCollection = new SanPhamCollection();
        public ImportSPForm()
        {
            InitializeComponent();
        }

        private int ConvertCharToInt(char ch) 
        {
            return ch - 'A';
        }
        private int checkSPExit1(string maSP)
        {
            for (int i = 0; i < this.SPCollection.Count; i++)
            {
                if (this.SPCollection[i].Ma.ToUpper() == maSP.ToUpper()) return i;
            }
            return -1;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            btnSave.Enabled = false;
            if (chkSaveSetting.Checked == true)
            {
                GlobalSettings.Luu_SP(txtMaHangColumn.Text, txtTenHangColumn.Text, txtMaHSColumn.Text, txtDVTColumn.Text);
            }
            try
            {
                if (chkOverwrite.Checked == true)
                {
                    new SanPham().InsertUpdate(this.SPReadCollection);
                }
                else
                    new SanPham().InsertUpdate(this.SPChuaCoCollection);
                //ShowMessage("Import thành công.", false);
                MLMessages("Nhập thành công.", "MSG_PUB02", "", false);
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
            this.SPCollection.AddRange(this.SPChuaCoCollection);
            btnSave.Enabled = true;
            this.Cursor = Cursors.Default;
            this.Close();
        }

        private void ImportSPForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            txtMaHangColumn.Text = GlobalSettings.SanPham.Ma;
            txtTenHangColumn.Text = GlobalSettings.SanPham.Ten;
            txtMaHSColumn.Text = GlobalSettings.SanPham.MaHS;
            txtDVTColumn.Text = GlobalSettings.SanPham.DVT;
            this.dgList.DataSource = this.SPReadCollection;
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                //error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                }
                else
                {
                    error.SetError(txtRow, "Begin row must be greater than zero ");
                }
                error.SetIconPadding(txtRow, 8);
                return;

            }
            this.Cursor = Cursors.WaitCursor;
            uiButton1.Enabled = false;
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text,true);
            }
            catch
            {
                //ShowMessage("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn và đóng file lại trước khi đọc.", false);
                MLMessages("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                this.Cursor = Cursors.Default;
                uiButton1.Enabled = true;
                return;
            }
            try
            {
                ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                //ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"", "MSG_EXC01", txtSheet.Text, false);
                this.Cursor = Cursors.Default;
                uiButton1.Enabled = true;
                return;
            }
            this.SPReadCollection.Clear();
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn = Convert.ToChar(txtMaHangColumn.Text);
            int maHangCol = ConvertCharToInt(maHangColumn);
            char tenHangColumn = Convert.ToChar(txtTenHangColumn.Text);
            int tenHangCol = ConvertCharToInt(tenHangColumn);
            char maHSColumn = Convert.ToChar(txtMaHSColumn.Text);
            int maHSCol = ConvertCharToInt(maHSColumn);
            char dvtColumn = Convert.ToChar(txtDVTColumn.Text);
            int dvtCol = ConvertCharToInt(dvtColumn);
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        SanPham SP = new SanPham();
                        SP.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        SP.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        SP.Ma = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                        SP.Ten = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                        SP.MaHS = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                        SP.DVT_ID = Convert.ToString(wsr.Cells[dvtCol].Value);
                        if (checkSPExit1(SP.Ma) >= 0)
                        {
                            SP.IsExist = 1;
                        }
                        else
                            this.SPChuaCoCollection.Add(SP);
                        this.SPReadCollection.Add(SP);
                    }
                    catch
                    {
                        //if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                        if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\n Bạn có muốn tiếp tục không?", "MSG_EXC06", "", true) != "Yes")
                        {
                            this.SPReadCollection.Clear();
                            this.Cursor = Cursors.Default;
                            uiButton1.Enabled = true;
                            return;
                        }
                    }
                }
            }
            dgList.DataSource = this.SPReadCollection;
            dgList.Refetch();
            this.Cursor = Cursors.Default;
            uiButton1.Enabled = true;
        }

        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog(this);
            txtFilePath.Text = openFileDialog1.FileName;
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());

        }


    }
}