using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
namespace Company.Interface.Report.VNACCS
{
    public partial class QuyetDinhThongQuanHangHoaNhapKhau_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public QuyetDinhThongQuanHangHoaNhapKhau_1()
        {
            InitializeComponent();
        }
        string sotokhai = string.Empty;
        private void xrLabel2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void TopMargin_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }
        public void BindingReport(VAD1AG0 vad1ag)
        {
            lblSoTrang.Text = "1/" + vad1ag.B12.GetValue().ToString().ToUpper();
            lblSoToKhai.Text = vad1ag.ICN.GetValue().ToString().ToUpper();
            sotokhai = vad1ag.ICN.GetValue().ToString().ToUpper();
            lblSoToKhaiDauTien.Text = vad1ag.FIC.GetValue().ToString().ToUpper();
            lblSoNhanhToKhaiChiaNho.Text = vad1ag.BNO.GetValue().ToString().ToUpper();
            lblTongSoToKhaiChiaNho.Text = vad1ag.DNO.GetValue().ToString().ToUpper();
            lblSoToKhaiTamNhapTaiXuat.Text = vad1ag.TDN.GetValue().ToString().ToUpper();
            lblMaPhanLoaiKiemTra.Text = vad1ag.A06.GetValue().ToString().ToUpper();
            lblMaLoaiHinh.Text = vad1ag.ICB.GetValue().ToString().ToUpper();
            lblMaPhanLoaiHangHoa.Text = vad1ag.CCC.GetValue().ToString().ToUpper();
            lblMaHieuPhuongThucVanChuyen.Text = vad1ag.MTC.GetValue().ToString().ToUpper();
            lblPhanLoaiCaNhanToChuc.Text = vad1ag.SKB.GetValue().ToString().ToUpper();
            lblMaSoHangHoaDaiDienToKhai.Text = vad1ag.A00.GetValue().ToString().ToUpper();
            lblTenCoQuanHaiQuanTiepNhanToKhai.Text = vad1ag.A07.GetValue().ToString().ToUpper();
            lblMaBoPhanXuLyToKhai.Text = vad1ag.CHB.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad1ag.A09.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDangKy.Text = Convert.ToDateTime(vad1ag.A09.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDangKy.Text = "";
            }
            if (vad1ag.AD1.GetValue().ToString().ToUpper() != "" && vad1ag.AD1.GetValue().ToString().ToUpper() != "0")
            {
                lblGioDangKy.Text = vad1ag.AD1.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ag.AD1.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ag.AD1.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad1ag.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayThayDoiDangKy.Text = Convert.ToDateTime(vad1ag.AD2.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayThayDoiDangKy.Text = "";
            }
            if (vad1ag.AD3.GetValue().ToString().ToUpper() != "" && vad1ag.AD3.GetValue().ToString().ToUpper() != "0")
            {
                lblGioThayDoiDangKy.Text = vad1ag.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ag.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ag.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioThayDoiDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad1ag.RED.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoiHanTaiNhapTaiXuat.Text = Convert.ToDateTime(vad1ag.RED.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoiHanTaiNhapTaiXuat.Text = "";
            }
            lblBieuThiTHHetHan.Text = vad1ag.AAA.GetValue().ToString().ToUpper();
            lblMaNguoiNhapKhau.Text = vad1ag.IMC.GetValue().ToString().ToUpper();
            lblTenNguoiNhapKhau.Text = vad1ag.IMN.GetValue().ToString().ToUpper();
            lblMaBuuChinhNhapKhau.Text = vad1ag.IMY.GetValue().ToString().ToUpper();
            lblDiaChiNguoiNhapKhau.Text = vad1ag.IMA.GetValue().ToString().ToUpper();
            lblSoDienThoaiNguoiNhapKhau.Text = vad1ag.IMT.GetValue().ToString().ToUpper();
            lblMaNguoiUyThacNhapKhau.Text = vad1ag.NMC.GetValue().ToString().ToUpper();
            lblTenNguoiUyThacNhapKhau.Text = vad1ag.NMN.GetValue().ToString().ToUpper();
            lblMaNguoiXuatKhau.Text = vad1ag.EPC.GetValue().ToString().ToUpper();
            lblTenNguoiXuatKhau.Text = vad1ag.EPN.GetValue().ToString().ToUpper();
            lblMaBuuChinhXuatKhau.Text = vad1ag.EPY.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau1.Text = vad1ag.EPA.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau2.Text = vad1ag.EP2.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau3.Text = vad1ag.EP3.GetValue().ToString().ToUpper();
            lblDiaChiNguoiXuatKhau4.Text = vad1ag.EP4.GetValue().ToString().ToUpper();
            lblMaNuoc.Text = vad1ag.EPO.GetValue().ToString().ToUpper();
            lblNguoiUyThacXuatKhau.Text = vad1ag.ENM.GetValue().ToString().ToUpper();
            lblMaDaiLyHaiQuan.Text = vad1ag.A37.GetValue().ToString().ToUpper();
            lblTenDaiLyHaiQuan.Text = vad1ag.A38.GetValue().ToString().ToUpper();
            lblMaNhanVienHaiQuan.Text = vad1ag.A39.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad1ag.BL_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblSoVanDon1.Text = vad1ag.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblSoVanDon2.Text = vad1ag.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblSoVanDon3.Text = vad1ag.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblSoVanDon4.Text = vad1ag.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblSoVanDon5.Text = vad1ag.BL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblSoLuong.Text = vad1ag.NO.GetValue().ToString().ToUpper();
            lblMaDonViTinh.Text = vad1ag.NOT.GetValue().ToString().ToUpper();
            lblTongTrongLuongHang.Text = vad1ag.GW.GetValue().ToString().ToUpper();
            lblMaDonViTinhTrongLuong.Text = vad1ag.GWT.GetValue().ToString().ToUpper();
            lblSoLuongContainer.Text = vad1ag.COC.GetValue().ToString().ToUpper();
            lblMaDiaDiemLuuKhoHang.Text = vad1ag.ST.GetValue().ToString().ToUpper();
            lblTenDiaDiemLuuKhoHang.Text = vad1ag.A51.GetValue().ToString().ToUpper();
            lblMaDiaDiemDoHang.Text = vad1ag.DST.GetValue().ToString().ToUpper();
            lblTenDiaDiemDoHang.Text = vad1ag.DSN.GetValue().ToString().ToUpper();
            lblMaDiaDiemXepHang.Text = vad1ag.PSC.GetValue().ToString().ToUpper();
            lblTenDiaDiemXepHang.Text = vad1ag.PSN.GetValue().ToString().ToUpper();
            lblMaPhuongTienVanChuyen.Text = vad1ag.VSC.GetValue().ToString().ToUpper();
            lblTenPhuongTienVanChuyen.Text = vad1ag.VSN.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad1ag.ARR.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHangDen.Text = Convert.ToDateTime(vad1ag.ARR.GetValue()).ToString("dd/MM/yyyy");
            }
            else 
            {
                lblNgayHangDen.Text = "";
            }
            lblKyHieuSoHieu.Text = vad1ag.MRK.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad1ag.ISD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDuocPhepNhapKho.Text = Convert.ToDateTime(vad1ag.ISD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDuocPhepNhapKho.Text = "";
            }
            for (int i = 0; i < vad1ag.OL_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaVanBanPhapQuyKhac1.Text = vad1ag.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblMaVanBanPhapQuyKhac2.Text = vad1ag.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblMaVanBanPhapQuyKhac3.Text = vad1ag.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblMaVanBanPhapQuyKhac4.Text = vad1ag.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblMaVanBanPhapQuyKhac5.Text = vad1ag.OL_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblPhanLoaiHinhThucHoaDon.Text = vad1ag.IV1.GetValue().ToString().ToUpper();
            lblSoHoaDon.Text = vad1ag.IV3.GetValue().ToString().ToUpper();
            lblSoTiepNhanHoaDonDienTu.Text = vad1ag.IV2.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad1ag.IVD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayPhatHanh.Text = Convert.ToDateTime(vad1ag.IVD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayPhatHanh.Text = "";
            }
            lblPhuongThucThanhToan.Text = vad1ag.IVP.GetValue().ToString().ToUpper();
            lblMaPhanLoaiGiaHoaDon.Text = vad1ag.IP1.GetValue().ToString().ToUpper();
            lblMaDieuKienGiaHoaDon.Text = vad1ag.IP2.GetValue().ToString().ToUpper();
            lblMaDongTienHoaDon.Text = vad1ag.IP3.GetValue().ToString().ToUpper();
            lblTongTriGiaHoaDon.Text = vad1ag.IP4.GetValue().ToString().ToUpper();
            lblTongTriGiaTinhThue.Text = vad1ag.A86.GetValue().ToString().ToUpper();

            //minhnd Fix Trị giá Tờ khai
            KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.LoadBySoTK(lblSoToKhai.Text);
            decimal triGiaHang = 0;
            TKMD.LoadFull();
            foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
            {
                triGiaHang += hmd.TriGiaHoaDon;
            }
            lblTongHeSoPhanBoTriGia.Text = triGiaHang.ToString("#,#.0000#;(#,#.0000#)");
            //minhnd Fix Trị giá Tờ khai

            //lblTongHeSoPhanBoTriGia.Text = vad1ag.TP.GetValue().ToString().ToUpper();

            lblMaPhanLoaiNhapLieu.Text = vad1ag.A97.GetValue().ToString().ToUpper();
            lblMaKetQuaKiemTraNoiDung.Text = vad1ag.N4.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad1ag.SS_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblPhanLoaiGiayPhepNhapKhau1.Text = vad1ag.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep1.Text = vad1ag.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblPhanLoaiGiayPhepNhapKhau2.Text = vad1ag.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep2.Text = vad1ag.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblPhanLoaiGiayPhepNhapKhau3.Text = vad1ag.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep3.Text = vad1ag.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblPhanLoaiGiayPhepNhapKhau4.Text = vad1ag.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep4.Text = vad1ag.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblPhanLoaiGiayPhepNhapKhau5.Text = vad1ag.SS_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblSoGiayPhep5.Text = vad1ag.SS_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblMaPhanLoaiKhaiTriGia.Text = vad1ag.VD1.GetValue().ToString().ToUpper();
            lblSoTiepNhanKhaiTriGiaTongHop.Text = vad1ag.VD2.GetValue().ToString().ToUpper() == "0" ? "" : vad1ag.VD2.GetValue().ToString().ToUpper();
            lblPhanLoaiCongThucChuan.Text = vad1ag.A93.GetValue().ToString().ToUpper();
            lblMaPhanLoaiDieuChinhTriGia.Text = vad1ag.A94.GetValue().ToString().ToUpper();
            lblPhuongPhapDieuChinhTriGia.Text = vad1ag.A95.GetValue().ToString().ToUpper();
            lblMaTienTe.Text = vad1ag.VCC.GetValue().ToString().ToUpper();
            lblGiaCoSo.Text = vad1ag.VPC.GetValue().ToString().ToUpper();
            lblMaPhanLoaiPhiVanChuyen.Text = vad1ag.FR1.GetValue().ToString().ToUpper();
            lblMaTienTePhiVanChuyen.Text = vad1ag.FR2.GetValue().ToString().ToUpper();
            lblPhiVanChuyen.Text = vad1ag.FR3.GetValue().ToString().ToUpper();
            lblMaPhanLoaiBaoHiem.Text = vad1ag.IN1.GetValue().ToString().ToUpper();
            lblMaTienTeCuaTienBaoHiem.Text = vad1ag.IN2.GetValue().ToString().ToUpper();
            lblPhiBaoHiem.Text = vad1ag.IN3.GetValue().ToString().ToUpper();
            lblSoDangKyBaoHiemTongHop.Text = vad1ag.IN4.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad1ag.VR_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaTenKhoanDieuChinh1.Text = vad1ag.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh1.Text = vad1ag.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia1.Text = vad1ag.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh1.Text = vad1ag.VR_.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.Text = vad1ag.VR_.listAttribute[4].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblMaTenKhoanDieuChinh2.Text = vad1ag.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh2.Text = vad1ag.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia2.Text = vad1ag.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh2.Text = vad1ag.VR_.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.Text = vad1ag.VR_.listAttribute[4].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblMaTenKhoanDieuChinh3.Text = vad1ag.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh3.Text = vad1ag.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia3.Text = vad1ag.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh3.Text = vad1ag.VR_.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.Text = vad1ag.VR_.listAttribute[4].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblMaTenKhoanDieuChinh4.Text = vad1ag.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh4.Text = vad1ag.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia4.Text = vad1ag.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh4.Text = vad1ag.VR_.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.Text = vad1ag.VR_.listAttribute[4].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblMaTenKhoanDieuChinh5.Text = vad1ag.VR_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaPhanLoaiDieuChinh5.Text = vad1ag.VR_.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblMaDongTienDieuChinhTriGia5.Text = vad1ag.VR_.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaKhoanDieuChinh5.Text = vad1ag.VR_.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.Text = vad1ag.VR_.listAttribute[4].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblChiTietKhaiTriGia.Text = vad1ag.VLD.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad1ag.KF1.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaSacThue1.Text = vad1ag.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue1.Text = vad1ag.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue1.Text = vad1ag.KF1.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblSoDongTong1.Text = vad1ag.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblMaSacThue2.Text = vad1ag.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue2.Text = vad1ag.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue2.Text = vad1ag.KF1.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblSoDongTong2.Text = vad1ag.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblMaSacThue3.Text = vad1ag.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue3.Text = vad1ag.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue3.Text = vad1ag.KF1.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblSoDongTong3.Text = vad1ag.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblMaSacThue4.Text = vad1ag.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue4.Text = vad1ag.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue4.Text = vad1ag.KF1.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblSoDongTong4.Text = vad1ag.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblMaSacThue5.Text = vad1ag.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue5.Text = vad1ag.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue5.Text = vad1ag.KF1.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblSoDongTong5.Text = vad1ag.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 5:
                        lblMaSacThue6.Text = vad1ag.KF1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTenSacThue6.Text = vad1ag.KF1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTongTienThue6.Text = vad1ag.KF1.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblSoDongTong6.Text = vad1ag.KF1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblTongTienThuePhaiNop.Text = vad1ag.B02.GetValue().ToString().ToUpper();
            lblSoTienBaoLanh.Text = vad1ag.B03.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad1ag.KJ1.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMaDongTienTyGiaTinhThue1.Text = vad1ag.KJ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTyGiaTinhThue1.Text = vad1ag.KJ1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblMaDongTienTyGiaTinhThue2.Text = vad1ag.KJ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTyGiaTinhThue2.Text = vad1ag.KJ1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblMaDongTienTyGiaTinhThue3.Text = vad1ag.KJ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblTyGiaTinhThue3.Text = vad1ag.KJ1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblMaXacDinhThoiHanNopThue.Text = vad1ag.ENC.GetValue().ToString().ToUpper();
            lblNguoiNopThue.Text = vad1ag.TPM.GetValue().ToString().ToUpper();
            lblMaLyDoDeNghi.Text = vad1ag.BP.GetValue().ToString().ToUpper();
            lblPhanLoaiNopThue.Text = vad1ag.B08.GetValue().ToString().ToUpper();
            lblTongSoTrangToKhai.Text = vad1ag.B12.GetValue().ToString().ToUpper();
            lblTongSoDongHang.Text = vad1ag.B13.GetValue().ToString().ToUpper();

        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0")
                lbl.Text = "";
        }

        private void txtBarCode_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            txtBarCode.Text = sotokhai;
            
        }
    }
}
