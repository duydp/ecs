﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using ServerRemote;
using System.Xml;
using System.Configuration;
using System.Data.SqlClient;

namespace SignRemoteVNACCS
{
    public partial class ConfigConnectDatabase : Form
    {
        public static List<string> ListTableNameSource = new List<string>();
        public static string DATABASE_NAME;
        public static string USER;
        public static string PASS;
        public static string SERVER_NAME;
        public ConfigConnectDatabase()
        {
            InitializeComponent();
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            try
            {
                string MSSQLConnectionString = XML.ReadNodeXmlConnectionStrings2();
                if (cbDatabaseSource.Text == "")
                {
                    MessageBoxControl.ShowMessage("Cơ sơ dữ liệu không được trống");
                    return;
                }

                    //Cấu hình lại connectionString.
                    string st = "Server=" + txtServerName.Text.Trim() + ";Database=" + cbDatabaseSource.Text.Trim() + ";Uid=" + txtSa.Text.Trim() + ";Pwd=" + txtPass.Text.Trim();
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString = st;
                    //cấu hình WS
                    SqlConnection con = new SqlConnection(st);
                    try
                    {
                        con.Open();
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        MessageBoxControl.ShowMessage("Không kết nối được tới máy chủ này\r\nLý do: " + ex.Message);
                        return;
                    }

                    //Hungtq 14/01/2011. Luu cau hinh
                    XML.SaveNodeXmlAppSettings("pass", txtPass.Text.Trim());
                    XML.SaveNodeXmlAppSettings("DATABASE_NAME", cbDatabaseSource.Text.Trim());
                    XML.SaveNodeXmlAppSettings("user", txtSa.Text.Trim());
                    XML.SaveNodeXmlAppSettings("ServerName", txtServerName.Text.Trim());
                    MessageBoxControl.ShowMessage("Lưu file cấu hình Kết nối Database thành công.");
                    string MSSQLConnectionStringModified = "";
                    string[] arr = MSSQLConnectionString.Split(new char[] { ';' });

                    arr[0] = arr[0].Split(new char[] { '=' })[0] + "=" + txtServerName.Text.Trim().ToUpper();

                    for (int i = 0; i < arr.Length; i++)
                    {
                        MSSQLConnectionStringModified += arr[i];

                        if (i < arr.Length - 1)
                            MSSQLConnectionStringModified += ";";
                    }
                    XML.SaveNodeXmlConnectionStrings(MSSQLConnectionStringModified);
                    this.Close();
                    Application.ExitThread();
                    Application.Restart();
            }
            catch (Exception ex)
            {
                MessageBoxControl.ShowMessage(ex.Message);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnList_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void ConfigConnectDatabase_Load(object sender, EventArgs e)
        {
            GetServerInstances();
            txtServerName.Text = XML.ReadNodeXmlAppSettings("ServerName").ToString();
            txtSa.Text = XML.ReadNodeXmlAppSettings("user").ToString();
            txtPass.Text = XML.ReadNodeXmlAppSettings("pass").ToString();
            cbDatabaseSource.Text = XML.ReadNodeXmlAppSettings("DATABASE_NAME").ToString();
            SetListTable();
        }
        private void SetListTable()
        {
            if (ListTableNameSource.Count > 0)
            {
                cbDatabaseSource.Items.Clear();
                cbDatabaseSource.Items.Add("(-Làm mới-)");

                foreach (string item in ListTableNameSource)
                {
                    cbDatabaseSource.Items.Add(item);
                }

                cbDatabaseSource.Sorted = true;
                if (cbDatabaseSource.SelectedItem == null) cbDatabaseSource.SelectedItem = new Janus.Windows.EditControls.UIComboBoxItem(DATABASE_NAME);
                cbDatabaseSource.SelectedItem.Value = DATABASE_NAME;
            }
        }
        private void GetServerInstances()
        {
            txtServerName.Items.Clear();
            txtServerName.Items.Add("(- Làm mới -)");
            Cursor = Cursors.WaitCursor;
            try
            {
                SqlDataSourceEnumerator instance = SqlDataSourceEnumerator.Instance;
                System.Data.DataTable table = instance.GetDataSources();
                foreach (DataRow item in table.Rows)
                {
                    string serverName = item[0].ToString();
                    serverName += "\\";
                    serverName += item[1].ToString();
                    txtServerName.Items.Add(serverName);
                }
                txtServerName.SelectedIndexChanged -= new EventHandler(txtServerName_SelectedIndexChanged);
                txtServerName.SelectedIndex = 0;
                txtServerName.SelectedIndexChanged += new EventHandler(txtServerName_SelectedIndexChanged);
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void GetListTableSource(Janus.Windows.EditControls.UIComboBox comboBox)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string strConnString = "server=" + txtServerName.Text;
                strConnString += ";User Id=" + txtSa.Text + ";Password=" + txtPass.Text;

                //fServer = new SQLDMO.SQLServerClass();
                //fServer.Connect(txtServerName.Text, txtSa.Text, txtPass.Text);

                SQL.InitializeServer(strConnString);

                comboBox.Items.Clear();
                comboBox.Items.Add("(-Làm mới-)");

                //foreach (SQLDMO.Database db in fServer.Databases)
                //{
                //    comboBox.Items.Add(db.Name);
                //}

                foreach (string dbName in SQL.ListDatabases())
                {
                    comboBox.Items.Add(dbName);
                }

                comboBox.Sorted = true;

                comboBox.SelectedIndexChanged -= new EventHandler(cbDatabaseSource_SelectedIndexChanged);
                comboBox.SelectedIndex = 0;
                comboBox.SelectedIndexChanged += new EventHandler(cbDatabaseSource_SelectedIndexChanged);

            }
            catch (Exception e1)
            {
                MessageBoxControl.ShowMessage("Kết nối không thành công. Bạn hãy kiểm tra lại thông tin cấu hình.\r\n\nChi tiết: " + e1.Message);
                Logger.LocalLogger.Instance().WriteMessage(e1);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void txtServerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtServerName.SelectedIndex==0)
            {
                GetServerInstances();
            }
        }

        private void cbDatabaseSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbDatabaseSource.SelectedIndex == 0)
            {
                GetListTableSource(cbDatabaseSource);
            }
        }

        private void cbDatabaseSource_DropDown(object sender, EventArgs e)
        {
            if (cbDatabaseSource.Items.Count <= 1)
                GetListTableSource(cbDatabaseSource);
        }

        private void cbDatabaseSource_Closed(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (cbDatabaseSource.SelectedIndex == 0)
                {
                    GetListTableSource(cbDatabaseSource);
                }

                ListTableNameSource.Clear();
                for (int i = 1; i < cbDatabaseSource.Items.Count; i++)
                {
                    ListTableNameSource.Add(cbDatabaseSource.Items[i].Text);
                }

                Cursor = Cursors.Default;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}
