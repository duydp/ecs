using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.ToKhai
{
    public partial class ThietBiRegistedForm : BaseForm
    {
        public ThietBi ThietBiSelected = new ThietBi();
        public Company.GC.BLL.KDT.ToKhaiMauDich TKMD;
        public Company.GC.BLL.KDT.HangMauDichCollection HMDCollection;
        public ThietBiRegistedForm()
        {
            InitializeComponent();
        }

        public void BindData()
        {
            ThietBiSelected.HopDong_ID = TKMD.IDHopDong;            
            dgList.DataSource =ThietBiSelected.SelectCollectionBy_HopDong_ID();
        }

        //-----------------------------------------------------------------------------------------
        
        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll();
            
          
            
            lblHint.Visible = this.CalledForm != string.Empty;
        }

        //-----------------------------------------------------------------------------------------

        private void ThietBiRegistedForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            BindData(); 
        }

        //-----------------------------------------------------------------------------------------
        
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------
        
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            
        }

      

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool KiemTraMaHang(string MaHang)
        {
            foreach (Company.GC.BLL.KDT.HangMauDich HMD in HMDCollection)
            {
                if (HMD.MaPhu.Trim().ToUpper() == MaHang.Trim().ToUpper())                    
                    return true;
            }
            return false;
        }
        private void uiButton1_Click(object sender, EventArgs e)
        {
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                Company.GC.BLL.GC.ThietBi TB = (Company.GC.BLL.GC.ThietBi)row.DataRow;
                if (KiemTraMaHang(TB.Ma))
                {
                    string st = showMsg("MSG_WRN31", TB.Ma, true);
                    //string st = MLMessages("Mặt hàng " + TB.Ma + " đã cónên sẽ được bỏ qua.Bạn có muốn tiếp tục không ?", "MSG_WRN31", TB.Ma, true);
                    if (st == "Yes")
                    {
                        return;
                    }
                    else
                        continue;
                }
                else
                {
                    Company.GC.BLL.KDT.HangMauDich HMD = new Company.GC.BLL.KDT.HangMauDich();
                    HMD.MaPhu = TB.Ma;
                    HMD.TenHang = TB.Ten;
                    HMD.MaHS = TB.MaHS;
                    HMD.DVT_ID = TB.DVT_ID;
                    HMDCollection.Add(HMD);
                }
            }
            this.Close();
        }

        //-----------------------------------------------------------------------------------------
    }
}