using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace Company.BLL.KDT.SXXK
{
    public partial class BKToKhaiNhap
    {
        protected DateTime _NgayHoanThanh = new DateTime(1900, 01, 01);
        public decimal SoToKhaiVNACCS { get; set; }
        public DateTime NgayHoanThanh
        {
            set { this._NgayHoanThanh = value; }
            get { return this._NgayHoanThanh; }
        }

        public string PhanLuong { get; set; }

        public DataSet getNPL_BKToKhaiNhap(long BangKeHoSoThanhLy_ID)
        {
            try
            {

                string sql = "Select a.ID as TKMD_ID, a.SoToKhai,CASE WHEN a.MaLoaiHinh like'%V%' THEN (select SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=a.SoToKhai)" +
        "ELSE a.SoToKhai END AS SoToKhaiVNACCS,a.MaLoaiHinh,a.NamDangKy,a.NgayDangKy,a.MaHaiQuan,b.MaPhu,b.MaHS,b.TenHang,b.DVT_ID, b.SoLuong, c.Ton " +
                            "from t_KDT_SXXK_BKToKhaiNhap a inner join t_SXXK_HangMauDich b " +
                            "on a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh " +
                            "AND a.NamDangKy = b.NamDangKy AND a.MaHaiQuan = b.MaHaiQuan " +
                            "left join t_SXXK_ThanhLy_NPLNhapTon c " +
                            "on b.SoToKhai = c.SoToKhai AND b.MaLoaiHinh = c.MaLoaiHinh " +
                            "AND b.NamDangKy = c.NamDangKy AND b.MaHaiQuan = c.MaHaiQuan " +
                            "AND b.MaPhu = c.MaNPL AND b.SoLuong = c.Luong " +
                            "WHERE BangKeHoSoThanhLy_ID =" + BangKeHoSoThanhLy_ID +
                            " ORDER BY a.NgayDangKy";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                DbCommand cmd = db.GetSqlStringCommand(sql);
                return db.ExecuteDataSet(cmd);
            }
            catch
            {
                return null;
            }

        }

        public DataSet getBKToKhaiNhapChuaNPL(long BangKeHoSoThanhLy_ID, string maNPL)
        {
            try
            {
                string sql = "Select a.SoToKhai,CASE WHEN a.MaLoaiHinh like'%V%' THEN (select SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=a.SoToKhai)" +
        "ELSE a.SoToKhai END AS SoToKhaiVNACCS,a.MaLoaiHinh,a.NamDangKy,a.NgayDangKy,a.MaHaiQuan " +
                            "from t_KDT_SXXK_BKToKhaiNhap a inner join t_SXXK_HangMauDich b " +
                            "on a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh " +
                            "AND a.NamDangKy = b.NamDangKy AND a.MaHaiQuan = b.MaHaiQuan " +
                            "WHERE BangKeHoSoThanhLy_ID =" + BangKeHoSoThanhLy_ID +
                            " AND b.MaPhu= '" + maNPL + "'";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                DbCommand cmd = db.GetSqlStringCommand(sql);
                return db.ExecuteDataSet(cmd);
            }
            catch
            {
                return null;
            }

        }
        public DataSet getBKToKhaiNKD(long BangKeHoSoThanhLy_ID)
        {
            try
            {
                string sql = "Select a.SoToKhai,a.MaLoaiHinh,a.NamDangKy,a.NgayDangKy,a.MaHaiQuan,b.MaPhu,b.MaHS,b.TenHang, b.SoLuong, b.DVT_ID, ISNULL(c.Ton,b.SoLuong)as Ton " +
                            "from t_KDT_SXXK_BKToKhaiNhap a inner join t_SXXK_HangMauDich b " +
                            "on a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh " +
                            "AND a.NamDangKy = b.NamDangKy AND a.MaHaiQuan = b.MaHaiQuan " +
                            "INNER join t_SXXK_ThanhLy_NPLNhapTon c " +
                            "on b.SoToKhai = c.SoToKhai AND b.MaLoaiHinh = c.MaLoaiHinh " +
                            "AND b.NamDangKy = c.NamDangKy AND b.MaHaiQuan = c.MaHaiQuan  AND b.MaPhu = c.MaNPL " +
                            "WHERE BangKeHoSoThanhLy_ID =" + BangKeHoSoThanhLy_ID +
                            " AND a.MaLoaiHinh LIKE 'NKD%'";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                DbCommand cmd = db.GetSqlStringCommand(sql);
                return db.ExecuteDataSet(cmd);
            }
            catch
            {
                return null;
            }

        }
        public decimal getLuongTon(int soToKhai, string maLoaiHinh, string maHaiQuan, short namDangKy, string maNPL)
        {
            try
            {
                string sql = "SELECT Ton FROM t_SXXK_ThanhLy_NPLNhapTon WHERE SoToKhai = @SoToKhai AND MaLoaiHinh = @MaLoaiHinh AND MaHaiQuan =@MaHaiQuan AND NamDangKy = @NamDangKy AND MaNPL = @MaNPL";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                DbCommand cmd = db.GetSqlStringCommand(sql);
                db.AddInParameter(cmd, "@SoToKhai", DbType.Int32, soToKhai);
                db.AddInParameter(cmd, "@MaLoaiHinh", DbType.String, maLoaiHinh);
                db.AddInParameter(cmd, "@NamDangKy", DbType.Int16, namDangKy);
                db.AddInParameter(cmd, "@MaHaiQuan", DbType.String, maHaiQuan);
                db.AddInParameter(cmd, "@MaNPL", DbType.String, maNPL);
                decimal ton = Convert.ToDecimal(db.ExecuteScalar(cmd));
                if (ton == 0)
                {
                    sql = "SELECT SoLuong FROM t_SXXK_HangMauDich WHERE SoToKhai = @SoToKhai AND MaLoaiHinh = @MaLoaiHinh AND MaHaiQuan =@MaHaiQuan AND NamDangKy = @NamDangKy AND MaPhu = @MaNPL";
                    cmd = db.GetSqlStringCommand(sql);
                    db.AddInParameter(cmd, "@SoToKhai", DbType.Int32, soToKhai);
                    db.AddInParameter(cmd, "@MaLoaiHinh", DbType.String, maLoaiHinh);
                    db.AddInParameter(cmd, "@NamDangKy", DbType.Int16, namDangKy);
                    db.AddInParameter(cmd, "@MaHaiQuan", DbType.String, maHaiQuan);
                    db.AddInParameter(cmd, "@MaNPL", DbType.String, maNPL);
                    ton = Convert.ToDecimal(db.ExecuteScalar(cmd));
                }
                return ton;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }
        public DataSet getBKToKhaiNhapForReport(int lanThanhLy, int MauBC01, string maDoanhNghiep, string MaHaiQuan, string where)
        {
            try
            {
                string sql = "";
                if (MauBC01 == 1)
                {

                    if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                    {
                        sql = "SELECT " +
                               "row_number() OVER (ORDER BY b.NgayDangKyNhap,b.SoToKhaiNhap) as STT, " +
                               "Cast((case when b.MaLoaiHinhNhap like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = b.SoToKhaiNhap) else b.SoToKhaiNhap end ) as varchar(100)) +'/' + c.Ten_VT as SoToKhai, b.SoToKhaiNhap, " +
                               "a.NgayDangKy as NgayDangKy, a.NgayHoanThanh as NgayHoanThanh, a.SoHopDong, a.NgayHopDong ,  a.MaLoaiHinh, b.ThueNK , a.MaHaiQuan " +
                               "FROM (SELECT SoToKhaiNhap,NgayDangKyNhap," +
                               "case when Year(Max(NgayThucNhap)) <= 1900 then (Select top 1 temp.NgayHoanThanh from t_SXXK_ToKhaiMauDich temp where  temp.SoToKhai = c.SoToKhaiNhap and temp.NgayDangKy = c.NgayDangKyNhap and temp.MaLoaiHinh = c.MaLoaiHinhNhap) else MAX(NgayThucNhap) end" +
                               " As NgayThucNhap,MaLoaiHinhNhap, sum(TienThueHoan) as ThueNK  FROM " +
                               "(SELECT SoToKhaiNhap, NgayDangKyNhap, Max(NgayThucNhap) As NgayThucNhap ,MaLoaiHinhNhap, CASE WHEN sum(LuongNPLSuDung) = LuongNhap THEN ThueNKNop ELSE  Round(sum(LuongNPLSuDung)*ThueXNK/Luong,0) END as TienThueHoan FROM t_KDT_SXXK_BCThueXNK WHERE LanThanhLy = " + lanThanhLy + " AND MaDoanhNghiep = '" + maDoanhNghiep + "' GROUP BY  SoToKhaiNhap,NgayDangKyNhap,MaNPL,MaLoaiHinhNhap,Luong,LuongNhap,ThueXNK,ThueNKNop ) c " +
                               "GROUP BY SoToKhaiNhap, NgayDangKyNhap, MaLoaiHinhNhap) b " +
                               " INNER JOIN t_SXXK_ToKhaiMauDich a ON b.SoToKhaiNhap = a.SoToKhai " +
                               " AND year(b.NgayDangKyNhap) = year(a.NgayDangKy) " +
                            // " AND Cast(b.NgayDangKyNhap as Date) = cast(a.NgayDangKy as Date)"+
                               " AND b.MaLoaiHinhNhap = a.MaLoaiHinh " +
                               " INNER JOIN t_HaiQuan_LoaiHinhMauDich c ON a.MaLoaiHinh = c.ID " + where;
                    }
                    else
                    {
                        //sql = "SELECT " +
                        //        "row_number() OVER (ORDER BY b.NgayDangKyNhap,b.SoToKhaiNhap) as STT, " +
                        //        "Cast((case when b.MaLoaiHinhNhap like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = b.SoToKhaiNhap) else b.SoToKhaiNhap end ) as varchar(100)) +'/' + c.Ten_VT as SoToKhai, b.SoToKhaiNhap, " +
                        //        "a.NgayDangKy as NgayDangKy," +
                        //"b.NgayThucNhap as NgayHoanThanh,a.SoHopDong, a.NgayHopDong ,  a.MaLoaiHinh, b.ThueNK , a.MaHaiQuan " +
                        //    "FROM (SELECT SoToKhaiNhap,NgayDangKyNhap,case when YEAR(MAX(NgayThucNhap))=1900 then (Select NgayHoanThanh from t_SXXK_ToKhaiMauDich where SoToKhai = SoToKhaiNhap and Ngaydangky= ngaydangkynhap and MaLoaiHinh=MaLoaiHinhNhap) else MAX(NgayThucNhap)end AS NgayThucNhap ,MaLoaiHinhNhap, sum(TienThueHoan) as ThueNK  FROM " +
                        //                "(SELECT SoToKhaiNhap, NgayDangKyNhap, Max(NgayThucNhap) As NgayThucNhap ,MaLoaiHinhNhap,  Round(sum(LuongNPLSuDung)*ThueXNK/Luong,0)  as TienThueHoan FROM t_KDT_SXXK_BCThueXNK WHERE LanThanhLy = " + lanThanhLy + " AND MaDoanhNghiep = '" + maDoanhNghiep + "' GROUP BY  SoToKhaiNhap,NgayDangKyNhap,MaNPL,MaLoaiHinhNhap,Luong,LuongNhap,ThueXNK,ThueNKNop ) c " +
                        //                "GROUP BY SoToKhaiNhap, NgayDangKyNhap, MaLoaiHinhNhap) b " +
                        //    " INNER JOIN t_SXXK_ToKhaiMauDich a ON b.SoToKhaiNhap = a.SoToKhai " +
                        //    " AND year(b.NgayDangKyNhap) = year(a.NgayDangKy) " + //hungTQ, update 14/03/2013
                        //    " AND b.MaLoaiHinhNhap = a.MaLoaiHinh " +
                        //    " INNER JOIN t_HaiQuan_LoaiHinhMauDich c ON a.MaLoaiHinh = c.ID " + where +
                        //    " ORDER BY b.NgayDangKyNhap,b.SoToKhaiNhap";
                        sql = " SELECT row_number() OVER (ORDER BY b.NgayDangKyNhap,b.SoToKhaiNhap) as STT," +
                                          " Cast((case when b.MaLoaiHinhNhap like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = b.SoToKhaiNhap) else b.SoToKhaiNhap end ) as varchar(100)) +'/' + c.Ten_VT as SoToKhai, " +
                                          " b.SoToKhaiNhap, " +
                                          " a.NgayDangKy as NgayDangKy," +
                                          " b.NgayThucNhap as NgayHoanThanh," +
                                          " a.SoHopDong, a.NgayHopDong ,  " +
                                          " a.MaLoaiHinh," +
                                          " b.ThueNK , " +
                                          " a.MaHaiQuan " +
                                 " FROM " +
                                     " ( " +
                                         " SELECT " +
                                            " SoToKhaiNhap," +
                                            " NgayDangKyNhap," +
                                            " case when YEAR(MAX(NgayThucNhap))=1900 then (Select NgayHoanThanh from t_SXXK_ToKhaiMauDich where SoToKhai = SoToKhaiNhap and Ngaydangky= ngaydangkynhap and MaLoaiHinh=MaLoaiHinhNhap) else MAX(NgayThucNhap)end AS NgayThucNhap ," +
                                            " MaLoaiHinhNhap, " +
                                            " sum(TienThueHoan) as ThueNK " +
                                         " FROM " +
                                            " ( " +
                                                 " SELECT " +
                                                     " SoToKhaiNhap, " +
                                                     " NgayDangKyNhap, " +
                                                     " Max(NgayThucNhap) As NgayThucNhap ," +
                                                     " MaLoaiHinhNhap,  " +
                                                     " Round(sum(LuongNPLSuDung)*ThueXNK/Luong,4)  as TienThueHoan " +
                                                 " FROM " +
                                                     " (" +
                                                     " select SoToKhaiNhap," +
                                                             " NgayDangKyNhap," +
                                                             " MaNPL," +
                                                             " MaLoaiHinhNhap," +
                                                             " NgayThucNhap," +
                                                             " Luong," +
                                                             " Case When LuongNPLSuDung+ LuongNPLTon>Luong then Luong - LuongNPLTon else LuongNPLSuDung end AS LuongNPLSuDung ,    " +
                                                             " LuongNhap," +
                                                             " ThueXNK," +
                                                             " ThueNKNop " +
                                                     " From t_KDT_SXXK_BCThueXNK" +
                                                      " WHERE LanThanhLy = " + lanThanhLy + " AND MaDoanhNghiep = '" + maDoanhNghiep + "'"+
                                                     " )e" +
                                                     " GROUP BY  SoToKhaiNhap,NgayDangKyNhap,MaNPL,MaLoaiHinhNhap,Luong,LuongNhap,ThueXNK,ThueNKNop " +
                                             " ) c " +
                                         " GROUP BY SoToKhaiNhap, NgayDangKyNhap, MaLoaiHinhNhap" +
                                     " ) b " +
                                  " INNER JOIN t_SXXK_ToKhaiMauDich a " +
                                     " ON b.SoToKhaiNhap = a.SoToKhai  " +
                                         " AND year(b.NgayDangKyNhap) = year(a.NgayDangKy) " +
                                         " AND b.MaLoaiHinhNhap = a.MaLoaiHinh  " +
                                 " INNER JOIN t_HaiQuan_LoaiHinhMauDich c " +
                                     " ON a.MaLoaiHinh = c.ID  " +
                                 " ORDER BY b.NgayDangKyNhap,b.SoToKhaiNhap";
                        
                    }
                }
                else
                    sql = "SELECT row_number() OVER (ORDER BY a.NgayDangKy, a.SoToKhai) as STT, " +
                            "Cast((case when a.MaLoaiHinh like '%V%' then (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = a.SoToKhai) else a.SoToKhai end ) as varchar(100)) +'/' + substring(a.MaLoaiHinh,0,4) as SoToKhai, a.SoToKhai as SoToKhaiNhap, " +
                            "a.NgayDangKy, CASE WHEN year(a.NGAY_THN_THX) > 1900 THEN a.Ngay_THN_THX ELSE a.NgayDangKy END as NgayHoanThanh, a.SoHopDong, a.NgayHopDong, b.ThueNK, a.MaLoaiHinh, a.ThanhLy " +
                            "FROM t_SXXK_ToKhaiMauDich a " +
                            "INNER JOIN " +
                            "(SELECT SoToKhai, NamDangKy,MaLoaiHinh, MaHaiQuan, Sum(ThueNK) as ThueNK " +
                            "FROM (SELECT  SoToKhai, NamDangKy,MaLoaiHinh,MaHaiQuan, MaNPL, TonDauThueXNK as ThueNK FROM t_KDT_SXXK_NPLNhapTon WHERE lanthanhly =" + lanThanhLy + " AND MaDoanhNghiep ='" + maDoanhNghiep + "' AND TonDau >0)c " +
                            "GROUP BY SoToKhai, NamDangKy, MaLoaiHinh, MaHaiQuan) b " +
                            "ON a.SoToKhai = b.SoToKhai " +
                            "AND a.MaLoaiHinh = b.MaLoaiHinh AND a.MaHaiQuan = b.MaHaiQuan " +
                            "AND a.NamDangKy = b.NamDangKy " + where +
                            " ORDER BY a.NgayDangKy, a.SoToKhai ";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                DbCommand cmd = db.GetSqlStringCommand(sql);
                return db.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }

        }
        public DataSet getBKToKhaiNhap(long BangKeHoSoThanhLy_ID)
        {
            try
            {
                string sql = "Select a.SoToKhai,CASE WHEN a.MaLoaiHinh like'%V%' THEN (select SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK=a.SoToKhai)" +
       "ELSE a.SoToKhai END AS SoToKhaiVNACCS,a.MaLoaiHinh,a.NamDangKy,a.NgayDangKy,a.MaHaiQuan " +
                           "from t_KDT_SXXK_BKToKhaiNhap a inner join t_SXXK_HangMauDich b " +
                           "on a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh " +
                           "AND a.NamDangKy = b.NamDangKy AND a.MaHaiQuan = b.MaHaiQuan " +
                           "WHERE BangKeHoSoThanhLy_ID =" + BangKeHoSoThanhLy_ID;
              

                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                DbCommand cmd = db.GetSqlStringCommand(sql);
                return db.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }

        }

        public DataSet getBKToKhaiNhapForCTTT(int lanThanhLy, int MauBC01, string maDoanhNghiep, string MaHaiQuan, string where)
        {
            try
            {
                string sql = "";
                if (MauBC01 == 1)
                    sql = "SELECT " +
                            "row_number() OVER (ORDER BY b.NgayDangKyNhap,b.SoToKhaiNhap) as STT, " +
                            "Cast(b.SoToKhaiNhap as varchar(100)) +'/' + c.Ten_VT as SoToKhai, b.SoToKhaiNhap, " +
                            "b.NgayDangKyNhap as NgayDangKy, b.NgayThucNhap as NgayHoanThanh, a.SoHopDong, a.NgayHopDong ,  a.MaLoaiHinh, b.ThueNK , a.MaHaiQuan, c.ID as MaLoaiHinh " +
                        "FROM (SELECT SoToKhaiNhap,NgayDangKyNhap,NgayThucNhap,MaLoaiHinhNhap, sum(TienThueHoan) as ThueNK  FROM " +
                                    "(SELECT SoToKhaiNhap, NgayDangKyNhap, NgayThucNhap ,MaLoaiHinhNhap, CASE WHEN sum(LuongNPLSuDung) = LuongNhap THEN ThueNKNop ELSE  Round(sum(LuongNPLSuDung)*ThueXNK/Luong,0) END as TienThueHoan FROM t_KDT_SXXK_BCThueXNK WHERE LanThanhLy = " + lanThanhLy + " AND MaDoanhNghiep = '" + maDoanhNghiep + "' GROUP BY  SoToKhaiNhap,NgayDangKyNhap,NgayThucNhap,MaNPL,MaLoaiHinhNhap,Luong,LuongNhap,ThueXNK,ThueNKNop ) c " +
                                    "GROUP BY SoToKhaiNhap, NgayDangKyNhap,NgayThucNhap, MaLoaiHinhNhap) b " +
                        " INNER JOIN t_SXXK_ToKhaiMauDich a ON b.SoToKhaiNhap = a.SoToKhai " +
                        " AND year(b.NgayDangKyNhap) = year(a.NgayDangKy) " + //hungTQ, update 14/03/2013
                        //" AND Month(b.NgayDangKyNhap) = Month(a.NgayDangKy) " +
                        //" AND Day(b.NgayDangKyNhap) = Day(a.NgayDangKy)   " +
                        " AND b.MaLoaiHinhNhap = a.MaLoaiHinh " +
                        " INNER JOIN t_HaiQuan_LoaiHinhMauDich c ON a.MaLoaiHinh = c.ID " + where;
                else
                    sql = "SELECT row_number() OVER (ORDER BY a.NgayDangKy, a.SoToKhai) as STT, " +
                            "Cast(a.SoToKhai as varchar(100)) +'/' + substring(a.MaLoaiHinh,0,4) as SoToKhai, a.SoToKhai as SoToKhaiNhap, " +
                            "a.NgayDangKy, CASE WHEN year(a.NGAY_THN_THX) > 1900 THEN a.Ngay_THN_THX ELSE a.NgayDangKy END as NgayHoanThanh, a.SoHopDong, a.NgayHopDong, b.ThueNK, a.MaLoaiHinh, a.ThanhLy " +
                            "FROM t_SXXK_ToKhaiMauDich a " +
                            "INNER JOIN " + 
                            "(SELECT SoToKhai, NamDangKy,MaLoaiHinh, MaHaiQuan, Sum(ThueNK) as ThueNK " +
                            "FROM (SELECT  SoToKhai, NamDangKy,MaLoaiHinh,MaHaiQuan, MaNPL, TonDauThueXNK as ThueNK FROM t_KDT_SXXK_NPLNhapTon WHERE lanthanhly =" + lanThanhLy + " AND MaDoanhNghiep ='" + maDoanhNghiep + "' AND TonDau >0)c " +
                            "GROUP BY SoToKhai, NamDangKy, MaLoaiHinh, MaHaiQuan) b " +
                            "ON a.SoToKhai = b.SoToKhai " +
                            "AND a.MaLoaiHinh = b.MaLoaiHinh AND a.MaHaiQuan = b.MaHaiQuan " +
                            "AND a.NamDangKy = b.NamDangKy " + where +
                            " ORDER BY a.NgayDangKy, a.SoToKhai ";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                DbCommand cmd = db.GetSqlStringCommand(sql);
                return db.ExecuteDataSet(cmd);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }

        }

        public static DataTable getTKNhapKoThamGiaTK(int lanThanhLy, int maBangKeNhap)
        {
            try
            {
                string sql = "SELECT     TOP (100) PERCENT a.SoToKhai, a.MaLoaiHinh, a.NamDangKy, a.NgayDangKy, a.MaHaiQuan, b.MaPhu AS MaNPL, b.TenHang AS TenNPL," +
                    " d.Ten AS TenDVT_NPL, ISNULL(c.Luong, b.SoLuong) AS Luong, ISNULL(c.Ton, b.SoLuong) AS TonDau, ISNULL(c.Ton, b.SoLuong) AS TonCuoi, " +
                    " CASE WHEN year(e.NGAY_THN_THX) > 1900 THEN e.Ngay_THN_THX ELSE a.NgayDangKy END AS NgayThucNhap, a.BangKeHoSoThanhLy_ID, " +
                    " c.ThueXNK, c.ThueXNKTon AS TonDauThueXNK, c.ThueXNKTon AS TonCuoiThueXNK, b.DonGiaTT, b.ThueSuatXNK AS ThueSuat, " +
                    " e.TyGiaTinhThue AS TyGiaTT, e.NgayHoanThanh" +
                    " FROM         dbo.t_KDT_SXXK_BKToKhaiNhap AS a INNER JOIN" +
                    " dbo.t_SXXK_ToKhaiMauDich AS e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = e.NamDangKy AND " +
                    " a.MaHaiQuan = e.MaHaiQuan INNER JOIN" +
                    " dbo.t_SXXK_HangMauDich AS b ON a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh AND a.NamDangKy = b.NamDangKy AND " +
                    " a.MaHaiQuan = b.MaHaiQuan LEFT OUTER JOIN" +
                    " dbo.t_SXXK_ThanhLy_NPLNhapTon AS c ON a.SoToKhai = c.SoToKhai AND a.MaLoaiHinh = c.MaLoaiHinh AND a.NamDangKy = c.NamDangKy AND " +
                    " a.MaHaiQuan = c.MaHaiQuan AND b.MaPhu = c.MaNPL INNER JOIN" +
                    " dbo.t_HaiQuan_DonViTinh AS d ON d.ID = b.DVT_ID  " +
                    " where a.BangKeHoSoThanhLy_ID =" + maBangKeNhap +
                    " and a.SoToKhai not in (" +
                    " select sotokhainhap from t_KDT_SXXK_BCXuatNhapTon where " +
                    " lanthanhly = " + lanThanhLy + " ) " +
                    " ORDER BY a.NgayDangKy";

                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                DbCommand cmd = db.GetSqlStringCommand(sql);
                return db.ExecuteDataSet(cmd).Tables[0];
            }
            catch
            {
                return null;
            }

        }

        public static BKToKhaiNhapCollection SelectCollectionBy_BangKeHoSoThanhLy_ID_NgayHoanThanh(long bangKeHoSoThanhLy_ID)
        {
            string spName = "p_KDT_SXXK_BKToKhaiNhap_SelectBy_BangKeHoSoThanhLy_ID_NgayHoanThanh";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 300;
            db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.BigInt, bangKeHoSoThanhLy_ID);

            BKToKhaiNhapCollection collection = new BKToKhaiNhapCollection();
            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                BKToKhaiNhap entity = new BKToKhaiNhap();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("BangKeHoSoThanhLy_ID"))) entity.BangKeHoSoThanhLy_ID = reader.GetInt64(reader.GetOrdinal("BangKeHoSoThanhLy_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiVNACCS"))) entity.SoToKhaiVNACCS = reader.GetDecimal(reader.GetOrdinal("SoToKhaiVNACCS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayThucNhap"))) entity.NgayThucNhap = reader.GetDateTime(reader.GetOrdinal("NgayThucNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("UserName"))) entity.UserName = reader.GetString(reader.GetOrdinal("UserName"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //Hungtq updated 27/07/2012.
        /// <summary>
        /// Kiem tra va xoa thong tin to khai bi trung.
        /// </summary>
        /// <param name="maHQ"></param>
        /// <param name="maDN"></param>
        /// <returns></returns>
        public static int DeleteDuplicateRow(string maHQ, string maDN)
        {
            string spName = "p_KDT_SXXK_BKToKhaiNhap_DeleteDuplicate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.VarChar, maHQ);
            db.AddInParameter(dbCommand, "@MaDN", SqlDbType.VarChar, maDN);

            object obj = db.ExecuteScalar(dbCommand);

            return obj != null ? Convert.ToInt32(obj) : 0;
        }
        public static DataTable KiemTraLuongNhapNPL(int LanThanhLy)
        {
            string spName = "p_KDT_SXXK_BKToKhaiNhap_KiemTraChenhLechSoLuongHMD";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public  DataTable getBKToKhaiNhap_CX(string MaDoanhNghiep,int NamDangKy,int LanThanhLy, int BangKeHSTL_ID)
        {
            string spName = "p_KDT_SXXK_BKToKhaiNhap_CX";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@NamDangky", SqlDbType.Int, NamDangKy);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);
            db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.Int, BangKeHSTL_ID);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }


    }

}