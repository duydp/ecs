﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Infragistics.Excel;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.KDT.SXXK
{
    public partial class BaoCaoChotTonReadExcelForm : Company.Interface.BaseForm
    {
        public KDT_VNACCS_TotalInventoryReport totalInventoryReport;
        public KDT_VNACCS_TotalInventoryReport_Detail totalInventoryReport_Detail;
        public List<KDT_VNACCS_TotalInventoryReport_Detail> DeTailCollection = new List<KDT_VNACCS_TotalInventoryReport_Detail>();
        public BaoCaoChotTonReadExcelForm()
        {
            InitializeComponent();
        }

        private void BaoCaoChotTonReadExcelForm_Load(object sender, EventArgs e)
        {

        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    MLMessages("LỖI KHI CHỌN FILE. DOANH NGHIỆP HÃY KIỂM TRA LẠI ĐƯỜNG DẪN HOẶC ĐÓNG FILE TRƯỚC KHI ĐỌC.", "MSG_EXC03", "", false);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return null;
                }
                List<String> Collection = new List<string>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                MLMessages("LỖI KHI ĐỌC SHEET. DOANH NGHIỆP HÃY KIỂM TRA LẠI TÊN SHEET TRƯỚC KHI CHỌN FILE.", "MSG_EXC03", "", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog sf = new OpenFileDialog();
                sf.ShowDialog(this);
                txtFilePath.Text = sf.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage("Có lỗi trong quá trình xử lý :\r\n" + ex.Message, false);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int beginRow = Convert.ToInt32(txtRow.Value) -1;
            if (beginRow < 0)
            {
                return;
            }

            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);

            }
            catch (Exception ex)
            {
                MLMessages("LỖI KHI ĐỌC FILE. BẠN HÃY KIỂM TRA LẠI ĐƯỜNG DẪN HOẶC ĐÓNG FILE TRƯỚC KHI ĐỌC.", "MSG_EXC03", "", false);
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                MLMessages("KHÔNG TỒN TẠI SHEET \"" + cbbSheetName.Text + "\"MSG_EXC01", "", cbbSheetName.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;

            char MaHHColumn = Convert.ToChar(txtMaHangColumn.Text.Trim());
            int MaHHCol = ConvertCharToInt(MaHHColumn);

            char TenHHColumn = Convert.ToChar(txtTenHangColumn.Text.Trim());
            int TenHHCol = ConvertCharToInt(TenHHColumn);

            char DVTColumn = Convert.ToChar(txtDVTColumn.Text.Trim());
            int DVTCol = ConvertCharToInt(DVTColumn);

            char TonKhoSSColumn = Convert.ToChar(txtTKSSColumn.Text.Trim());
            int TonKhoSSCol = ConvertCharToInt(TonKhoSSColumn);

            char TonKhoTTColumn = Convert.ToChar(txtTKTTColumn.Text.Trim());
            int TonKhoTTCol = ConvertCharToInt(TonKhoTTColumn);

            string errorTotal = "";
            string errorMaHangHoa = "";
            string errorMaHangHoaExits = "";
            string errorTenHangHoa = "";
            string errorDVT = "";
            string errorDVTExits = "";
            string errorSoLuongTonKhoSoSach = "";
            string errorSoLuongTonKhoSoSachValid = "";
            string errorSoLuongTonKhoThucTe = "";
            string errorSoLuongTonKhoThucTeValid = "";
            List<SXXK_NguyenPhuLieu> NPLCollection = SXXK_NguyenPhuLieu.SelectCollectionAll();
            List<SXXK_SanPham> SPCollection = SXXK_SanPham.SelectCollectionAll();
            foreach (KDT_VNACCS_TotalInventoryReport_Detail item in totalInventoryReport.DetailCollection)
            {
                if (item.ID > 0)
                    item.Delete();
            }
            totalInventoryReport.DetailCollection.Clear();
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        bool isAdd = true;
                        bool isExits = false;
                        KDT_VNACCS_TotalInventoryReport_Detail totalInventoryReportDetail = new KDT_VNACCS_TotalInventoryReport_Detail();
                        try
                        {
                            totalInventoryReportDetail.MaHangHoa = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                            if (totalInventoryReportDetail.MaHangHoa.Trim().Length == 0)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.MaHangHoa + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                //// KIỂM TRA MÃ NPL HOẶC SP ĐÃ ĐĂNG KÝ
                                if (totalInventoryReport.LoaiBaoCao == 1)
                                {
                                    foreach (SXXK_NguyenPhuLieu item in NPLCollection)
                                    {
                                        if (item.Ma == totalInventoryReportDetail.MaHangHoa)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (SXXK_SanPham item in SPCollection)
                                    {
                                        if (item.Ma == totalInventoryReportDetail.MaHangHoa)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorMaHangHoaExits += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.MaHangHoa + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.MaHangHoa + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            totalInventoryReportDetail.TenHangHoa = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                            if (totalInventoryReportDetail.TenHangHoa.Trim().Length == 0)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.TenHangHoa + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.TenHangHoa + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            isExits = false;
                            totalInventoryReportDetail.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim();
                            if (totalInventoryReportDetail.DVT.Trim().Length == 0)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.DVT + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                List<VNACC_Category_QuantityUnit> DVTCollection = VNACC_Category_QuantityUnit.SelectCollectionAll();
                                foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                {
                                    if (item.Code == totalInventoryReportDetail.DVT)
                                    {
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.DVT + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.DVT + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            totalInventoryReportDetail.SoLuongTonKhoSoSach = Convert.ToDecimal(wsr.Cells[TonKhoSSCol].Value);
                            if (totalInventoryReportDetail.SoLuongTonKhoSoSach.ToString().Trim().Length == 0)
                            {
                                errorSoLuongTonKhoSoSach += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoSoSach + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (Decimal.Round(totalInventoryReportDetail.SoLuongTonKhoSoSach, 4) != totalInventoryReportDetail.SoLuongTonKhoSoSach)
                                {
                                    errorSoLuongTonKhoSoSachValid += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoSoSach + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorSoLuongTonKhoSoSach += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoSoSach + "]\n";
                            isAdd = false;
                        }
                        try
                        {
                            totalInventoryReportDetail.SoLuongTonKhoThucTe = Convert.ToDecimal(wsr.Cells[TonKhoTTCol].Value);
                            if (totalInventoryReportDetail.SoLuongTonKhoThucTe.ToString().Trim().Length == 0)
                            {
                                errorSoLuongTonKhoThucTe += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoThucTe + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                if (Decimal.Round(totalInventoryReportDetail.SoLuongTonKhoThucTe, 4) != totalInventoryReportDetail.SoLuongTonKhoThucTe)
                                {
                                    errorSoLuongTonKhoThucTeValid += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoThucTe + "]\n";
                                    isAdd = false;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorSoLuongTonKhoThucTe += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoThucTe + "]\n";
                            isAdd = false;
                        }
                        if(isAdd)
                            DeTailCollection.Add(totalInventoryReportDetail);
                    }
                    catch (Exception ex)
                    {
                        return;
                    }
                }
            }
            if (!String.IsNullOrEmpty(errorMaHangHoa))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorMaHangHoaExits))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaExits + " CHƯA ĐƯỢC ĐĂNG KÝ ";
            if (!String.IsNullOrEmpty(errorTenHangHoa))
                errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : \n" + errorTenHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorDVT))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVT + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorDVTExits))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVTExits + " KHÔNG HỢP LỆ. ĐVT PHẢI LÀ ĐVT VNACCS (VÍ DỤ : CÁI/CHIẾC LÀ : PCE)";
            if (!String.IsNullOrEmpty(errorSoLuongTonKhoSoSach))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG TỒN KHO SỔ SÁCH] : \n" + errorSoLuongTonKhoSoSach + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorSoLuongTonKhoSoSachValid))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG TỒN KHO SỔ SÁCH] : \n" + errorSoLuongTonKhoSoSachValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)";
            if (!String.IsNullOrEmpty(errorSoLuongTonKhoThucTe))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG TỒN KHO THỰC TẾ] : " + errorSoLuongTonKhoThucTe + " KHÔNG ĐƯỢC ĐỂ TRỐNG";
            if (!String.IsNullOrEmpty(errorSoLuongTonKhoThucTeValid))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG TỒN KHO THỰC TẾ] : \n" + errorSoLuongTonKhoThucTeValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)";

            if (String.IsNullOrEmpty(errorTotal))
            {
                foreach (KDT_VNACCS_TotalInventoryReport_Detail totalInventoryReport_Detail in DeTailCollection)
                {
                    totalInventoryReport.DetailCollection.Add(totalInventoryReport_Detail);
                }
                ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "NHẬP HÀNG TỪ EXCEL THÀNH CÔNG .", false);
            }
            else
            {
                ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "NHẬP TỪ EXCEL KHÔNG THÀNH CÔNG ." + errorTotal, false);
                return;
            }

            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkExcelMau_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_TotalInventoryReport();
            }

            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
