﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_GiayPhepManageForm_SFA : BaseForm
    {
        public KDT_VNACC_GiayPhep_SFA giayPhep = new KDT_VNACC_GiayPhep_SFA();
        public List<KDT_VNACC_GiayPhep_SFA> listGiayPhep = new List<KDT_VNACC_GiayPhep_SFA>();
        public List<KDT_VNACC_HangGiayPhep> listHangGP = null;

        public VNACC_GiayPhepManageForm_SFA()
        {
            InitializeComponent();
        }

        private void VNACC_GiayPhep_TheoDoi_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                this.Text = "Theo dõi Giấy phép - SFA";

                //Fill ValueList
                this.grdList.RootTable.Columns["ChucNangChungTu"].HasValueList = true;
                Janus.Windows.GridEX.GridEXValueListItemCollection valueList = this.grdList.RootTable.Columns["ChucNangChungTu"].ValueList;
                System.Data.DataView view = VNACC_Category_Common.SelectDynamic("ReferenceDB = 'E025'", "").Tables[0].DefaultView;
                view.Sort = "Code ASC";
                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["Code"].ToString(), row["Name_VN"].ToString()));
                }

                grdList.DataSource = KDT_VNACC_GiayPhep_SFA.SelectAll().Tables[0];
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void gridEX1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);

                VNACC_GiayPhepForm_SFA f = new VNACC_GiayPhepForm_SFA();
                f.GiayPhep = getGiayPhepID(id);
                f.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private KDT_VNACC_GiayPhep_SFA getGiayPhepID(long id)
        {
            listGiayPhep = KDT_VNACC_GiayPhep_SFA.SelectCollectionAll();
            foreach (KDT_VNACC_GiayPhep_SFA gp in listGiayPhep)
            {
                if (gp.ID == id)
                {
                    string where = "GiayPhep_ID =" + gp.ID;
                    listHangGP = new List<KDT_VNACC_HangGiayPhep>();
                    listHangGP = KDT_VNACC_HangGiayPhep.SelectCollectionDynamic(where, "ID", EGiayPhep.SFA);
                    foreach (KDT_VNACC_HangGiayPhep hang in listHangGP)
                    {
                        gp.HangCollection.Add(hang);
                    }
                    return gp;
                }
            }
            return null;
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string soTN = txtSoTiepNhan.Text.Trim();
                string soGP = txtSoGiayPhep.Text.Trim();

                string where = String.Empty;
                if (soTN != "")
                    where = "SoTiepNhan='" + soTN + "' and ";
                if (soGP != "")
                    where = where + "SoDonXinCapPhep='" + soGP + "' and ";

                listGiayPhep.Clear();
                if (where == "")
                {
                    listGiayPhep = KDT_VNACC_GiayPhep_SFA.SelectCollectionAll();
                }
                else
                {
                    listGiayPhep = KDT_VNACC_GiayPhep_SFA.SelectCollectionDynamic(where, "ID");
                }

                grdList.DataSource = listGiayPhep;
                grdList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            DeleteGiayPhep();
        }

        private void grdList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            DeleteGiayPhep();
        }

        private void DeleteGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (ShowMessage("Bạn có muốn xóa giấy phép này không?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = grdList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            int id = Convert.ToInt32(i.GetRow().Cells["ID"].Value);
                            giayPhep = getGiayPhepID(id);

                            foreach (KDT_VNACC_HangGiayPhep hang in giayPhep.HangCollection)
                            {
                                hang.Delete();
                            }
                            giayPhep.Delete();
                        }
                    }

                    btnTimKiem.PerformClick();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
    }
}
