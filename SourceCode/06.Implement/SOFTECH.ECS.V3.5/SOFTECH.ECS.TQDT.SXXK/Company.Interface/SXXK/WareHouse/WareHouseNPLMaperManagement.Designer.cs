﻿namespace Company.Interface.GC.WareHouse
{
    partial class WareHouseNPLMaperManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grListNPLMap_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference grListNPLMap_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WareHouseNPLMaperManagement));
            Janus.Windows.GridEX.GridEXLayout grListNPL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference grListNPL_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListNPLMap = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchNPLMap = new Janus.Windows.EditControls.UIButton();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNPLMap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListNPL = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchNPL = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnAuto = new Janus.Windows.EditControls.UIButton();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListNPLMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListNPL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(1210, 754);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1210, 754);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.uiGroupBox9);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(606, 8);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(601, 700);
            this.uiGroupBox2.TabIndex = 8;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.grListNPLMap);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox9.Location = new System.Drawing.Point(3, 54);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(595, 643);
            this.uiGroupBox9.TabIndex = 6;
            this.uiGroupBox9.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListNPLMap
            // 
            this.grListNPLMap.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListNPLMap.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListNPLMap.ColumnAutoResize = true;
            grListNPLMap_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("grListNPLMap_DesignTimeLayout_Reference_0.Instance")));
            grListNPLMap_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            grListNPLMap_DesignTimeLayout_Reference_0});
            grListNPLMap_DesignTimeLayout.LayoutString = resources.GetString("grListNPLMap_DesignTimeLayout.LayoutString");
            this.grListNPLMap.DesignTimeLayout = grListNPLMap_DesignTimeLayout;
            this.grListNPLMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListNPLMap.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListNPLMap.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListNPLMap.FrozenColumns = 5;
            this.grListNPLMap.GroupByBoxVisible = false;
            this.grListNPLMap.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListNPLMap.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListNPLMap.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListNPLMap.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListNPLMap.Hierarchical = true;
            this.grListNPLMap.Location = new System.Drawing.Point(3, 8);
            this.grListNPLMap.Name = "grListNPLMap";
            this.grListNPLMap.RecordNavigator = true;
            this.grListNPLMap.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListNPLMap.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListNPLMap.Size = new System.Drawing.Size(589, 632);
            this.grListNPLMap.TabIndex = 86;
            this.grListNPLMap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListNPLMap.VisualStyleManager = this.vsmMain;
            this.grListNPLMap.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListNPLMap_RowDoubleClick);
            this.grListNPLMap.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListNPLMap_LoadingRow);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.btnSearchNPLMap);
            this.uiGroupBox7.Controls.Add(this.label2);
            this.uiGroupBox7.Controls.Add(this.txtNPLMap);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox7.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(595, 46);
            this.uiGroupBox7.TabIndex = 5;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchNPLMap
            // 
            this.btnSearchNPLMap.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchNPLMap.Image")));
            this.btnSearchNPLMap.Location = new System.Drawing.Point(400, 15);
            this.btnSearchNPLMap.Name = "btnSearchNPLMap";
            this.btnSearchNPLMap.Size = new System.Drawing.Size(75, 23);
            this.btnSearchNPLMap.TabIndex = 87;
            this.btnSearchNPLMap.Text = "Tìm kiếm";
            this.btnSearchNPLMap.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchNPLMap.Click += new System.EventHandler(this.btnSearchNPLMap_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 88;
            this.label2.Text = "Mã NPL";
            // 
            // txtNPLMap
            // 
            this.txtNPLMap.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtNPLMap.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtNPLMap.BackColor = System.Drawing.Color.White;
            this.txtNPLMap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNPLMap.Location = new System.Drawing.Point(79, 16);
            this.txtNPLMap.Name = "txtNPLMap";
            this.txtNPLMap.Size = new System.Drawing.Size(315, 21);
            this.txtNPLMap.TabIndex = 87;
            this.txtNPLMap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNPLMap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNPLMap.TextChanged += new System.EventHandler(this.btnSearchNPLMap_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox3.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(603, 700);
            this.uiGroupBox3.TabIndex = 7;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.grListNPL);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox8.Location = new System.Drawing.Point(3, 54);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(597, 643);
            this.uiGroupBox8.TabIndex = 5;
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListNPL
            // 
            this.grListNPL.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListNPL.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListNPL.ColumnAutoResize = true;
            grListNPL_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("grListNPL_DesignTimeLayout_Reference_0.Instance")));
            grListNPL_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            grListNPL_DesignTimeLayout_Reference_0});
            grListNPL_DesignTimeLayout.LayoutString = resources.GetString("grListNPL_DesignTimeLayout.LayoutString");
            this.grListNPL.DesignTimeLayout = grListNPL_DesignTimeLayout;
            this.grListNPL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListNPL.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListNPL.FrozenColumns = 5;
            this.grListNPL.GroupByBoxVisible = false;
            this.grListNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListNPL.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListNPL.Hierarchical = true;
            this.grListNPL.Location = new System.Drawing.Point(3, 8);
            this.grListNPL.Name = "grListNPL";
            this.grListNPL.RecordNavigator = true;
            this.grListNPL.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListNPL.Size = new System.Drawing.Size(591, 632);
            this.grListNPL.TabIndex = 85;
            this.grListNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListNPL.VisualStyleManager = this.vsmMain;
            this.grListNPL.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListNPL_RowDoubleClick);
            this.grListNPL.SelectionChanged += new System.EventHandler(this.grListNPL_SelectionChanged);
            this.grListNPL.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grListNPL_LoadingRow);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnSearchNPL);
            this.uiGroupBox6.Controls.Add(this.label1);
            this.uiGroupBox6.Controls.Add(this.txtNPL);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(597, 46);
            this.uiGroupBox6.TabIndex = 4;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchNPL
            // 
            this.btnSearchNPL.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchNPL.Image")));
            this.btnSearchNPL.Location = new System.Drawing.Point(399, 16);
            this.btnSearchNPL.Name = "btnSearchNPL";
            this.btnSearchNPL.Size = new System.Drawing.Size(75, 23);
            this.btnSearchNPL.TabIndex = 87;
            this.btnSearchNPL.Text = "Tìm kiếm";
            this.btnSearchNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchNPL.Click += new System.EventHandler(this.btnSearchNPL_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 86;
            this.label1.Text = "Mã NPL";
            // 
            // txtNPL
            // 
            this.txtNPL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtNPL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtNPL.BackColor = System.Drawing.Color.White;
            this.txtNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNPL.Location = new System.Drawing.Point(69, 17);
            this.txtNPL.Name = "txtNPL";
            this.txtNPL.Size = new System.Drawing.Size(315, 21);
            this.txtNPL.TabIndex = 85;
            this.txtNPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNPL.TextChanged += new System.EventHandler(this.btnSearchNPL_Click);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.btnAuto);
            this.uiGroupBox4.Controls.Add(this.btnAdd);
            this.uiGroupBox4.Controls.Add(this.btnClose);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 708);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1204, 43);
            this.uiGroupBox4.TabIndex = 6;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnAuto
            // 
            this.btnAuto.Image = ((System.Drawing.Image)(resources.GetObject("btnAuto.Image")));
            this.btnAuto.Location = new System.Drawing.Point(9, 14);
            this.btnAuto.Name = "btnAuto";
            this.btnAuto.Size = new System.Drawing.Size(158, 23);
            this.btnAuto.TabIndex = 87;
            this.btnAuto.Text = "Tự động Map dữ liệu";
            this.btnAuto.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAuto.Click += new System.EventHandler(this.btnAuto_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(1039, 14);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 87;
            this.btnAdd.Text = "Thêm mới";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(1120, 14);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 87;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnSearchNPLMap_Click);
            // 
            // WareHouseNPLMaperManagement
            // 
            this.ClientSize = new System.Drawing.Size(1210, 754);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "WareHouseNPLMaperManagement";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Theo dõi danh sách Nguyên phụ liệu hợp đồng Map với Nguyên phụ liệu kho kế toán";
            this.Load += new System.EventHandler(this.WareHouseNPLMaperManagement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListNPLMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListNPL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.GridEX grListNPL;
        private Janus.Windows.GridEX.GridEX grListNPLMap;
        private Janus.Windows.GridEX.EditControls.EditBox txtNPL;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtNPLMap;
        private Janus.Windows.EditControls.UIButton btnSearchNPL;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton btnSearchNPLMap;
        private Janus.Windows.EditControls.UIButton btnAuto;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.EditControls.UIButton btnClose;
    }
}
