﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.KDT;
using Company.Interface.Report.KDDT;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace Company.Interface.Report
{
    
    public partial class ReportHangXKViewForm : BaseForm
    {

        public DevExpress.XtraReports.UI.XtraReport report;
        public ReportHangXKViewForm()
        {
            InitializeComponent();
        }

        private void ReportHangXKViewForm_Load(object sender, EventArgs e)
        {
            //cboToKhai.SelectedIndex = 0;
            //if (this.TKMD.HMDCollection.Count > 3)
            //{
            //    int count = (this.TKMD.HMDCollection.Count - 1) / 9 + 1;
            //    for (int i = 0; i < count; i++)
            //        this.AddItemComboBox();
            //}
            //else
            //uiGroupBox2.Visible = false;
           // this.report.Binddata();
            printControl1.PrintingSystem = report.PrintingSystem;
            this.report.CreateDocument();

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi khi in: " + ex.Message, false);
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi khi xuất: " + ex.Message, false);
            }
        }

        private void printControl1_Load(object sender, EventArgs e)
        {

        }
   
       
    }
}