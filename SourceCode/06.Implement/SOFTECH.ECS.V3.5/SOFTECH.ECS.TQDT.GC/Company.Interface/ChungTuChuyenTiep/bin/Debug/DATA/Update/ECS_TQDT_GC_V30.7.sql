GO
IF OBJECT_ID(N'dbo.p_KDT_ToKhaiMauDich_SelectLuongNPLTrongTKXuatTemp') IS NOT NULL
	DROP PROCEDURE  dbo.p_KDT_ToKhaiMauDich_SelectLuongNPLTrongTKXuatTemp   
GO
-----------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectLuongNPLTrongTKXuatTemp]  
-- Database: ECS_TQDT_GC_V4_TAT  
-- Lượng NPL Quy đổi từ Tờ khai xuất  
-----------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectLuongNPLTrongTKXuatTemp]  
 @HopDong_ID BIGINT,  
 @MaNguyenPhuLieu NVARCHAR(500)  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT *  
 FROM   t_View_LuongNPLTrongTKXuatTemp  
 WHERE  IDHopDong = '''+ CAST(@HopDong_ID AS NVARCHAR(12))  + '''  
        AND MaNguyenPhuLieu = ''' + @MaNguyenPhuLieu + ''''  
  
EXEC sp_executesql @SQL  
  

GO
IF OBJECT_ID(N'dbo.t_View_LuongNPLTrongTKXuatTemp') IS NOT NULL
	DROP VIEW  dbo.t_View_LuongNPLTrongTKXuatTemp   
GO
	CREATE VIEW [dbo].[t_View_LuongNPLTrongTKXuatTemp]      
AS      
    SELECT  CASE WHEN t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE '%V%'      
                 THEN t_KDT_ToKhaiMauDich.LoaiVanDon      
                 ELSE CONVERT(VARCHAR(12), t_KDT_ToKhaiMauDich.SoToKhai)      
            END AS SoToKhaiVNACCS ,
			TrangThaiXuLy,      
            dbo.t_KDT_ToKhaiMauDich.MaHaiQuan ,      
            dbo.t_KDT_ToKhaiMauDich.SoToKhai ,      
            dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh ,      
			dbo.t_KDT_ToKhaiMauDich.NgayDangKy ,      
            dbo.t_KDT_HangMauDich.MaPhu AS MaSP ,      
            dbo.t_KDT_HangMauDich.TenHang ,      
			dbo.t_KDT_HangMauDich.DVT_ID,      
            dbo.t_KDT_HangMauDich.SoLuong ,      
            dbo.t_KDT_GC_DinhMuc.MaNguyenPhuLieu ,      
            dbo.t_GC_NguyenPhuLieu.Ten ,      
            dbo.t_KDT_HangMauDich.SoLuong * ( dbo.t_KDT_GC_DinhMuc.DinhMucSuDung      
                                              / 100      
                                              * dbo.t_KDT_GC_DinhMuc.TyLeHaoHut      
                                              + dbo.t_KDT_GC_DinhMuc.DinhMucSuDung ) AS LuongNPL ,    
   TriGiaTT,    
   DonGiaTT,      
            dbo.t_KDT_ToKhaiMauDich.MaDoanhNghiep ,      
            dbo.t_KDT_ToKhaiMauDich.IDHopDong ,      
            dbo.t_KDT_ToKhaiMauDich.ID ,      
            dbo.t_KDT_GC_DinhMuc.DinhMucSuDung ,      
            dbo.t_KDT_GC_DinhMuc.TyLeHaoHut ,      
            YEAR(dbo.t_KDT_ToKhaiMauDich.NgayDangKy) AS NamDK      
    FROM    dbo.t_KDT_ToKhaiMauDich      
            INNER JOIN dbo.t_KDT_HangMauDich ON dbo.t_KDT_ToKhaiMauDich.ID = dbo.t_KDT_HangMauDich.TKMD_ID      
            INNER JOIN dbo.t_KDT_GC_DinhMuc ON dbo.t_KDT_GC_DinhMuc.MaSanPham = dbo.t_KDT_HangMauDich.MaPhu      
                                           AND (SELECT TOP 1 ID_HopDong FROM dbo.t_KDT_GC_DinhMucDangKy WHERE ID =t_KDT_GC_DinhMuc.Master_ID)= dbo.t_KDT_ToKhaiMauDich.IDHopDong      
            INNER JOIN dbo.t_GC_NguyenPhuLieu ON (SELECT TOP 1 ID_HopDong FROM dbo.t_KDT_GC_DinhMucDangKy WHERE ID =t_KDT_GC_DinhMuc.Master_ID) = dbo.t_GC_NguyenPhuLieu.HopDong_ID      
                                                 AND dbo.t_KDT_GC_DinhMuc.MaNguyenPhuLieu = dbo.t_GC_NguyenPhuLieu.Ma      
    WHERE   ( dbo.t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S' AND t_KDT_ToKhaiMauDich.TrangThaiXuLy NOT IN (10,11) AND t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'XV%'  )  
    
    


GO

 IF OBJECT_ID(N'dbo.p_GC_DinhMuc_SelectDynamicTemp') IS NOT NULL
	DROP PROCEDURE  dbo.p_GC_DinhMuc_SelectDynamicTemp   
GO
-----------------------------------------------  
-- Stored procedure name: [dbo].[p_GC_DinhMuc_SelectDynamicTemp]  
-- Database: ECS_TQDT_GC_V4_TAT  
-----------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_GC_DinhMuc_SelectDynamicTemp]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT 
       MaSanPham ,
       MaNguyenPhuLieu ,
       DinhMucSuDung ,
       TyLeHaoHut ,
       STTHang ,
       NPL_TuCungUng ,
       TenSanPham ,
       TenNPL ,
       ID_HopDong AS HopDong_ID 
 FROM dbo.t_KDT_GC_DinhMuc INNER JOIN dbo.t_KDT_GC_DinhMucDangKy ON t_KDT_GC_DinhMucDangKy.ID = t_KDT_GC_DinhMuc.Master_ID
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '30.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('30.7',GETDATE(), N'CẬP NHẬT PROCEDURE XEM ĐỊNH MỨC CHƯA ĐĂNG KÝ ')
END

GO