﻿namespace Company.Interface.DanhMucChuan
{
    partial class MaHSForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaHSForm));
            Janus.Windows.GridEX.GridEXLayout dgListNhomBT_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.filterEditor1 = new Janus.Windows.FilterEditor.FilterEditor();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.btnReset = new Janus.Windows.EditControls.UIButton();
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.pnlNhomBieuThue = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dgListNhomBT = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlNhomBieuThue)).BeginInit();
            this.pnlNhomBieuThue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNhomBT)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnReset);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.filterEditor1);
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Location = new System.Drawing.Point(3, 3);
            this.grbMain.Size = new System.Drawing.Size(792, 247);
            // 
            // filterEditor1
            // 
            this.filterEditor1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.filterEditor1.AutoApply = true;
            this.filterEditor1.BackColor = System.Drawing.Color.Transparent;
            this.filterEditor1.DefaultConditionOperator = Janus.Data.ConditionOperator.BeginsWith;
            this.filterEditor1.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.UseFormatStyle;
            this.filterEditor1.Location = new System.Drawing.Point(3, 10);
            this.filterEditor1.MinSize = new System.Drawing.Size(401, 44);
            this.filterEditor1.Name = "filterEditor1";
            this.filterEditor1.Office2007ColorScheme = Janus.Windows.Common.Office2007ColorScheme.Default;
            this.filterEditor1.ScrollMode = Janus.Windows.UI.Dock.ScrollMode.Both;
            this.filterEditor1.ScrollStep = 15;
            this.filterEditor1.Size = new System.Drawing.Size(789, 45);
            this.filterEditor1.SourceControl = this.dgList;
            this.filterEditor1.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.DynamicFiltering = true;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.IncrementalSearchMode = Janus.Windows.GridEX.IncrementalSearchMode.FirstCharacter;
            this.dgList.Location = new System.Drawing.Point(5, 61);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(784, 152);
            this.dgList.TabIndex = 0;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.UpdatingCell += new Janus.Windows.GridEX.UpdatingCellEventHandler(this.dgList_UpdatingCell);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LinkClicked += new Janus.Windows.GridEX.ColumnActionEventHandler(this.dgList_LinkClicked);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(714, 219);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Enabled = false;
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(633, 219);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.Icon = ((System.Drawing.Icon)(resources.GetObject("btnReset.Icon")));
            this.btnReset.Location = new System.Drawing.Point(552, 219);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 1;
            this.btnReset.Text = "Làm mới";
            this.btnReset.VisualStyleManager = this.vsmMain;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.Tag = null;
            this.pnlNhomBieuThue.Id = new System.Guid("d88ad08b-89d8-458c-86fd-bce4148672fd");
            this.pnlNhomBieuThue.StaticGroup = true;
            this.uiPanel1.Id = new System.Guid("06722f6d-9b82-471e-a758-3de8b87d012d");
            this.pnlNhomBieuThue.Panels.Add(this.uiPanel1);
            this.uiPanelManager1.Panels.Add(this.pnlNhomBieuThue);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("d88ad08b-89d8-458c-86fd-bce4148672fd"), Janus.Windows.UI.Dock.PanelGroupStyle.HorizontalTiles, Janus.Windows.UI.Dock.PanelDockStyle.Bottom, true, new System.Drawing.Size(792, 201), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("06722f6d-9b82-471e-a758-3de8b87d012d"), new System.Guid("d88ad08b-89d8-458c-86fd-bce4148672fd"), 175, true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("d88ad08b-89d8-458c-86fd-bce4148672fd"), Janus.Windows.UI.Dock.PanelGroupStyle.HorizontalTiles, true, new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("06722f6d-9b82-471e-a758-3de8b87d012d"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // pnlNhomBieuThue
            // 
            this.pnlNhomBieuThue.Location = new System.Drawing.Point(3, 250);
            this.pnlNhomBieuThue.Name = "pnlNhomBieuThue";
            this.pnlNhomBieuThue.Size = new System.Drawing.Size(792, 201);
            this.pnlNhomBieuThue.TabIndex = 4;
            this.pnlNhomBieuThue.Text = "Nhóm biểu thuế  (Thông tin được tham khảo từ website Tổng Cục Hải Quan Việt nam)";
            // 
            // uiPanel1
            // 
            this.uiPanel1.CaptionVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel1.CloseButtonVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(0, 26);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(792, 175);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Panel 1";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.dgListNhomBT);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 1);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(790, 173);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // dgListNhomBT
            // 
            this.dgListNhomBT.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNhomBT.AlternatingColors = true;
            this.dgListNhomBT.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNhomBT.ColumnAutoResize = true;
            dgListNhomBT_DesignTimeLayout.LayoutString = resources.GetString("dgListNhomBT_DesignTimeLayout.LayoutString");
            this.dgListNhomBT.DesignTimeLayout = dgListNhomBT_DesignTimeLayout;
            this.dgListNhomBT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNhomBT.DynamicFiltering = true;
            this.dgListNhomBT.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNhomBT.GroupByBoxVisible = false;
            this.dgListNhomBT.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNhomBT.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNhomBT.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNhomBT.ImageList = this.ImageList1;
            this.dgListNhomBT.IncrementalSearchMode = Janus.Windows.GridEX.IncrementalSearchMode.FirstCharacter;
            this.dgListNhomBT.Location = new System.Drawing.Point(0, 0);
            this.dgListNhomBT.Name = "dgListNhomBT";
            this.dgListNhomBT.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNhomBT.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListNhomBT.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNhomBT.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListNhomBT.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListNhomBT.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNhomBT.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListNhomBT.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListNhomBT.Size = new System.Drawing.Size(790, 173);
            this.dgListNhomBT.TabIndex = 1;
            this.dgListNhomBT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListNhomBT.VisualStyleManager = this.vsmMain;
            // 
            // MaHSForm
            // 
            this.ClientSize = new System.Drawing.Size(798, 454);
            this.Controls.Add(this.pnlNhomBieuThue);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "MaHSForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Danh mục mã HS";
            this.Load += new System.EventHandler(this.MaHSForm_Load);
            this.Controls.SetChildIndex(this.pnlNhomBieuThue, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlNhomBieuThue)).EndInit();
            this.pnlNhomBieuThue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNhomBT)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.FilterEditor.FilterEditor filterEditor1;
        private Janus.Windows.GridEX.GridEX dgList;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnReset;
        private Janus.Windows.UI.Dock.UIPanelManager uiPanelManager1;
        private Janus.Windows.UI.Dock.UIPanelGroup pnlNhomBieuThue;
        private Janus.Windows.UI.Dock.UIPanel uiPanel1;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel1Container;
        private Janus.Windows.GridEX.GridEX dgListNhomBT;
    }
}
