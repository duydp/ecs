﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.Interface;
using SignRemote;
using Company.KDT.SHARE.Components;

namespace ServerRemote
{
    public partial class UserDaiLyForm : BaseForm
    {
        public User_DaiLy user;
        public UserDaiLyForm()
        {
            InitializeComponent();
        }

        private void UserDaiLyForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (user != null)
                {
                    txtUser.Text = user.USER_NAME;
                    txtPass.Text = Helpers.DecryptString(user.PASSWORD, "KEYWORD");
                    txtMaDN.Text = user.MaDoanhNghiep;
                    txtTenDN.Text = user.MO_TA;
                    chkAdmin.Checked = Convert.ToBoolean(user.isAdmin);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                user.USER_NAME = txtUser.Text;
                user.PASSWORD = Helpers.EncryptString(txtPass.Text, "KEYWORD");
                user.MaDoanhNghiep = txtMaDN.Text;
                user.MO_TA = txtTenDN.Text;
                user.isAdmin = Convert.ToBoolean(chkAdmin.Checked);
                user.Update();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
