using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.SXXK.ToKhai;
using Company.Interface.Report;
using DevExpress.XtraPrinting;

namespace Company.Interface
{
    public partial class BaocaoTriGiaXK : Company.Interface.BaseForm
    {

        public ToKhaiMauDich tokhai = new ToKhaiMauDich();
        public ToKhaiMauDichCollection tokhaiColl = new ToKhaiMauDichCollection();
        public DataTable DTSourceGrid1 = new DataTable();
        public DataTable DTSourceGrid2 = new DataTable();
        Company.Interface.Report.KDDT.BangKe bk = new Company.Interface.Report.KDDT.BangKe();

        public BaocaoTriGiaXK()
        {
            InitializeComponent();
        }

        private void BaocaoTriGiaXK_Load(object sender, EventArgs e)
        {
            txtDVBC.Text = Properties.Settings.Default.DonViBaoCao;
            txtNguoiDB.Text = Properties.Settings.Default.NguoiDuyetBieu;
            txtNguoiLB.Text = Properties.Settings.Default.NguoiLapBieu;

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
           string ten = "";
           string dvt = "";
           string sanpham = "";
           if (ckbLuuMacdinh.Checked)
           {
               Properties.Settings.Default.NguoiLapBieu = txtNguoiLB.Text.ToUpper();
               Properties.Settings.Default.NguoiDuyetBieu = txtNguoiDB.Text.ToUpper();
               Properties.Settings.Default.DonViBaoCao = txtDVBC.Text.ToUpper();
               Properties.Settings.Default.Save();
               GlobalSettings.RefreshKey();
           }
           //nuocnhap khau theo ky(hang, hangDP ) 
           DataSet ds = tokhai.SelectTriGiaHangToKhaiXK_ByDate(CbTuNgay.Value, CbDenNgay.Value);
           DataSet ds2 = tokhai.SelectTriGiaHangDPToKhaiXK_ByDate(CbTuNgay.Value, CbDenNgay.Value);
           ds.Tables[0].Columns.Add("TongSoLuongDP",typeof(decimal));
           ds.Tables[0].Columns.Add("TongTriGiaDP",typeof(decimal));
           foreach (DataRow dr in ds.Tables[0].Rows) {
               ten = "";
               dvt = "";
               dvt = dr["DVT"].ToString();
               ten= dr["Ten"].ToString();
               //ds2 = null --> error DBnull
               dr["TongSoLuongDP"] = 0;
               dr["TongTriGiaDP"] = 0;
               foreach (DataRow dr2 in ds2.Tables[0].Rows){                   
                   if (dr2["Ten"].ToString().Trim() == ten.Trim() && dr2["DVT"].ToString().Trim() == dvt.Trim())
                   {
                       dr["TongSoLuongDP"] = dr2["TongSoLuongDP"];
                       dr["TongTriGiaDP"] = dr2["TongTriGiaDP"];
                       break;
                   }
               }               
           }           
           //hang/nuoc nhap khau theo ky(hang/hangDP)
           DataSet ds3 = tokhai.SelectTriGiaHang_NuocToKhaiXK_ByDate(CbTuNgay.Value, CbDenNgay.Value);
           DataSet ds4 = tokhai.SelectTriGiaHangDP_NuocToKhaiXK_ByDate(CbTuNgay.Value, CbDenNgay.Value);
           ds3.Tables[0].Columns.Add("TongSoLuongDP", typeof(decimal));
           ds3.Tables[0].Columns.Add("TongTriGiaDP", typeof(decimal));
           foreach (DataRow dr in ds3.Tables[0].Rows)
           {
               ten = "";
               sanpham = "";
               ten = dr["Ten"].ToString();
               sanpham = dr["TenSP"].ToString();
               dr["TongSoLuongDP"] = 0;
               dr["TongTriGiaDP"] = 0;
               foreach (DataRow dr2 in ds4.Tables[0].Rows)
               {
                   if (dr2["Ten"].ToString().Trim() == ten.Trim() && dr2["TenSP"].ToString().Trim() == sanpham.Trim())
                   {
                       dr["TongSoLuongDP"] = dr2["TongSoLuongDP"];
                       dr["TongTriGiaDP"] = dr2["TongTriGiaDP"];
                       break;
                   }
               }
               dr["Ten"] = dr["TenSP"] + " / " + dr["Ten"];

           }
           ds3.Tables[0].Columns.Remove("TenSP");
           //add tb hang/nuoc nhap khau theo ky --> tb nuoc nhap khau theo ky
           DataRow row;                      
           row = ds.Tables[0].NewRow();
           row["Ten"] = "B.Mặt hàng/Nước nhập khẩu";
           row["DVT"] = "";
           row["TongSoLuong"] = 0;
           row["TongSoLuongDP"] = 0;
           row["TongTriGia"] = 0;
           row["TongTriGiaDP"] = 0;
           row["NguyenTe_ID"] = 0;
           ds.Tables[0].Rows.Add(row);
           foreach (DataRow dr in ds3.Tables[0].Rows){                              
               row=ds.Tables[0].NewRow();
               row["Ten"] = dr["Ten"];
               row["DVT"] = dr["DVT"];
               row["TongSoLuong"] = dr["TongSoLuong"];
               row["TongSoLuongDP"] = dr["TongSoLuongDP"];
               row["TongTriGia"] = dr["TongTriGia"];
               row["TongTriGiaDP"] = dr["TongTriGiaDP"];
               row["NguyenTe_ID"] = dr["NguyenTe_ID"];               
               ds.Tables[0].Rows.Add(row);
           }
           //--------------------------báo cáo cộng dồn tử đầu năm-------------------------------
           //theo nuoc NK (hang,hangDP)
           string strNgay;
           DateTime tungay = new DateTime();
           strNgay = "1/1/"+ CbDenNgay.Value.Year.ToString();
           tungay= Convert.ToDateTime(strNgay);           
           DataSet ds5 = tokhai.SelectTriGiaHangToKhaiXK_ByDate(tungay, CbDenNgay.Value);
           DataSet ds6 = tokhai.SelectTriGiaHangDPToKhaiXK_ByDate(tungay, CbDenNgay.Value);
           ds5.Tables[0].Columns.Add("TongSoLuongDP", typeof(decimal));
           ds5.Tables[0].Columns.Add("TongTriGiaDP", typeof(decimal));
           foreach (DataRow dr in ds5.Tables[0].Rows)
           {
               ten = "";
               dvt = "";
               dvt = dr["DVT"].ToString();
               ten = dr["Ten"].ToString();
               dr["TongSoLuongDP"] = 0;
               dr["TongTriGiaDP"] = 0;
               foreach (DataRow dr2 in ds6.Tables[0].Rows)
               {                   
                   if (dr2["Ten"].ToString().Trim() == ten.Trim() && dr2["DVT"].ToString().Trim() == dvt.Trim())
                   {
                       dr["TongSoLuongDP"] = dr2["TongSoLuongDP"];
                       dr["TongTriGiaDP"] = dr2["TongTriGiaDP"];
                       break;
                   }
               }
           }
           //theo Mat hang/Nuoc NK (hang,hangDP) : 
           DataSet ds7 = tokhai.SelectTriGiaHang_NuocToKhaiXK_ByDate(tungay, CbDenNgay.Value);
           DataSet ds8 = tokhai.SelectTriGiaHangDP_NuocToKhaiXK_ByDate(tungay, CbDenNgay.Value);
           ds7.Tables[0].Columns.Add("TongSoLuongDP", typeof(decimal));
           ds7.Tables[0].Columns.Add("TongTriGiaDP", typeof(decimal));
           foreach (DataRow dr in ds7.Tables[0].Rows)
           {
               ten = "";
               sanpham = "";
               ten = dr["Ten"].ToString();
               sanpham = dr["TenSP"].ToString();
               dr["TongSoLuongDP"] = 0;
               dr["TongTriGiaDP"] = 0;
               foreach (DataRow dr2 in ds8.Tables[0].Rows)
               {                  
                   if (dr2["Ten"].ToString().Trim() == ten.Trim() && dr2["TenSP"].ToString().Trim() == sanpham.Trim())
                   {
                       dr["TongSoLuongDP"] = dr2["TongSoLuongDP"];
                       dr["TongTriGiaDP"] = dr2["TongTriGiaDP"];
                       break;
                   }
               }
               dr["Ten"] = dr["TenSP"] + " / " + dr["Ten"];

           }
           ds7.Tables[0].Columns.Remove("TenSP");     
           //add row tb hang/nuoc --> hang 
           foreach (DataRow dr in ds7.Tables[0].Rows)
           {
               row = ds5.Tables[0].NewRow();
               row["Ten"] = dr["Ten"];
               row["DVT"] = dr["DVT"];
               row["TongSoLuong"] = dr["TongSoLuong"];
               row["TongSoLuongDP"] = dr["TongSoLuongDP"];
               row["TongTriGia"] = dr["TongTriGia"];
               row["TongTriGiaDP"] = dr["TongTriGiaDP"];
               row["NguyenTe_ID"] = dr["NguyenTe_ID"];
               ds5.Tables[0].Rows.Add(row);
           }
           //add tb cong don  --> tb theo ky
           //them cac cot 
           ds.Tables[0].Columns.Add("TongSoLuongTuDN", typeof(decimal));//so luong cong don tu dau nam
           ds.Tables[0].Columns.Add("TongTriGiaTuDN", typeof(decimal)); //tri gia hang cong don tu dau nam
           ds.Tables[0].Columns.Add("TongSoLuongDPTuDN", typeof(decimal));//so luong DP cong don tu dau nam
           ds.Tables[0].Columns.Add("TongTriGiaDPTuDN", typeof(decimal)); //tri gia ĐP cong don tu dau nam
           foreach (DataRow dr in ds.Tables[0].Rows)//theo ky
           {
               ten = "";
               dvt = "";
               ten = dr["Ten"].ToString();
               dvt = dr["DVT"].ToString();
               //case null
               dr["TongSoLuongTuDN"] = 0;
               dr["TongTriGiaTuDN"] = 0;
               dr["TongSoLuongDPTuDN"] = 0;
               dr["TongTriGiaDPTuDN"] = 0;
               foreach (DataRow dr2 in ds5.Tables[0].Rows)//cong don
               {                   
                   if (dr2["Ten"].ToString().Trim() == ten.Trim() && dr2["DVT"].ToString().Trim() == dvt.Trim())
                   {
                       dr["TongSoLuongTuDN"] = dr2["TongSoLuong"];
                       dr["TongTriGiaTuDN"] = dr2["TongTriGia"];
                       dr["TongSoLuongDPTuDN"] = dr2["TongSoLuongDP"];
                       dr["TongTriGiaDPTuDN"] = dr2["TongTriGiaDP"];
                       break;
                   }
               }
           }
           if (ds.Tables[0].Rows.Count > 1)
           {
               btnXemBC.Enabled = true;
           }
           else {
               btnXemBC.Enabled = false;
               ShowMessage(setText("Không có kết quả thống kê cho trường hợp này", "No results for this case"), false);
               return;
           }           
           this.DTSourceGrid1 = ds.Tables[0];
           grdList.DataSource = this.DTSourceGrid1;
           
        }
        private void btnXemBC_Click(object sender, EventArgs e)
        {            
            
            ReportHangXKViewForm viewform = new ReportHangXKViewForm();
            viewform.report = bk;            
            bk.DTSource= this.DTSourceGrid1;
            bk.thoigian= CbDenNgay.Value;
            bk.NguoiLapBieu = txtNguoiLB.Text.ToUpper();
            bk.NguoiDuyetBieu = txtNguoiDB.Text.ToUpper();
            bk.DonViBaoCao = txtDVBC.Text.ToUpper();
            bk.Binddata();          
            viewform.Show();
        }

       

        
       
    }
}

