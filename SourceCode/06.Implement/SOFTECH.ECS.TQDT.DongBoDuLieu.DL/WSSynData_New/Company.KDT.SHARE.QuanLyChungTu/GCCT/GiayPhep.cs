﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Transactions;
using System.Globalization;
using System.Xml;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;


namespace Company.KDT.SHARE.QuanLyChungTu.GCCT
{
    /// <summary>
    /// XML root cho việc Serializer ra XML đồng bộ dữ liệu
    /// </summary>
    [System.Xml.Serialization.XmlRoot(Namespace = "Company.KDT.SHARE.QuanLyChungTu.GCCT")]
    public partial class GiayPhep
    {
        public List<GiayPhepChiTiet> ListHMDofGiayPhep = new List<GiayPhepChiTiet>();

        /// <summary>
        /// Lay du lieu hang hoa nam trong giay phep
        /// </summary>
        /// <param name="GiayPhep_TKMD_ID">tuong duong voi ID_TK va GiayPhep_ID</param>
        public void LoadListHMDofGiayPhep()
        {
            ListHMDofGiayPhep = (List<GiayPhepChiTiet>)GiayPhepChiTiet.SelectCollectionDynamic("GiayPhep_ID=" + this.ID, "");
        }
        public DataTable ConvertListToDataSet(DataTable dsHMD)
        {
            DataTable tmp = dsHMD.Copy();
            tmp.Rows.Clear();
            tmp.Columns.Add("GhiChu");
            dsHMD.Columns.Add("GhiChu");
            dsHMD.Columns.Add("HCT_ID");
            dsHMD.Columns.Add("MaChuyenNganh");
            dsHMD.Columns.Add("MaNguyenTe");
            tmp.Columns["ID"].ColumnName = "HCT_ID";
            tmp.Columns["HCT_ID"].Caption = "HCT_ID";
            tmp.Columns.Add("ID");
            tmp.Columns.Add("MaChuyenNganh");
            tmp.Columns.Add("MaNguyenTe");

            foreach (GiayPhepChiTiet item in ListHMDofGiayPhep)
            {
                DataRow[] row = dsHMD.Select("ID=" + item.HCT_ID);
                if (row != null && row.Length != 0)
                {
                    row[0]["HCT_ID"] = row[0]["ID"];
                    row[0]["MaChuyenNganh"] = item.MaChuyenNganh;
                    row[0]["MaNguyenTe"] = item.MaNguyenTe;
                    row[0]["ID"] = item.ID.ToString();

                    row[0]["SoThuTuHang"] = item.SoThuTuHang;
                    row[0]["MaHS"] = item.MaHS;
                    row[0]["MaHang"] = item.MaPhu;
                    row[0]["TenHang"] = item.TenHang;
                    row[0]["ID_NuocXX"] = item.NuocXX_ID;
                    row[0]["ID_DVT"] = item.DVT_ID;
                    row[0]["SoLuong"] = item.SoLuong;
                    row[0]["DonGia"] = item.DonGiaKB;
                    row[0]["TriGia"] = item.TriGiaKB;

                    tmp.ImportRow(row[0]);
                }
            }
            return tmp;

        }
        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    long id = this.ID;
                    try
                    {
                        if (this.ID == 0)
                            this.Insert(transaction);
                        else
                            this.Update(transaction);


                        foreach (GiayPhepChiTiet hangGP in this.ListHMDofGiayPhep)
                        {
                            hangGP.GiayPhep_ID = this.ID;
                            if (id == 0)
                                hangGP.ID = 0;

                            if (hangGP.ID == 0)
                                hangGP.Insert(transaction);
                            else
                                hangGP.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public static List<GiayPhep> SelectListGiayPhepByMaDanhNghiepAndKhacTKMD(long ID_TK, string MaDoanhNghiep)
        {
            List<GiayPhep> collection = new List<GiayPhep>();
            string sql = "select * from t_KDT_ChungTuKem_GiayPhep where TKCT_ID <> @ID_TK and MaDoanhNghiep=@MaDoanhNghiep";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbcommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbcommand, "@ID_TK", SqlDbType.BigInt, ID_TK);
            db.AddInParameter(dbcommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbcommand);
            while (reader.Read())
            {
                GiayPhep entity = new GiayPhep();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiCap"))) entity.NguoiCap = reader.GetString(reader.GetOrdinal("NguoiCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiCap"))) entity.NoiCap = reader.GetString(reader.GetOrdinal("NoiCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViDuocCap"))) entity.MaDonViDuocCap = reader.GetString(reader.GetOrdinal("MaDonViDuocCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDuocCap"))) entity.TenDonViDuocCap = reader.GetString(reader.GetOrdinal("TenDonViDuocCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCoQuanCap"))) entity.MaCoQuanCap = reader.GetString(reader.GetOrdinal("MaCoQuanCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenQuanCap"))) entity.TenQuanCap = reader.GetString(reader.GetOrdinal("TenQuanCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        public static XmlNode ConvertCollectionGiayPhepToXML(XmlDocument doc, List<GiayPhep> GiayPhepCollection, long ID_TK, List<HangChuyenTiep> listHang)
        {
            try
            {
                NumberFormatInfo f = new NumberFormatInfo();
                f.NumberDecimalSeparator = ".";
                f.NumberGroupSeparator = ",";
                if (GiayPhepCollection == null || GiayPhepCollection.Count == 0)
                {
                    GiayPhepCollection = (List<GiayPhep>)GiayPhep.SelectCollectionDynamic("TKCT_ID=" + ID_TK, "");
                }
                XmlElement CHUNG_TU_GIAYPHEP = doc.CreateElement("Licenses");
                foreach (GiayPhep giayphep in GiayPhepCollection)
                {
                    XmlElement License = doc.CreateElement("License");
                    CHUNG_TU_GIAYPHEP.AppendChild(License);
                    XmlElement issuer = doc.CreateElement("issuer");
                    License.AppendChild(issuer);
                    issuer.InnerText = "1";
                    XmlElement reference = doc.CreateElement("reference");
                    License.AppendChild(reference);
                    reference.InnerText = giayphep.SoGiayPhep;
                    XmlElement issue = doc.CreateElement("issue");
                    License.AppendChild(issue);
                    issue.InnerText = giayphep.NgayGiayPhep.ToString(NgayThang.yyyyMMdd);
                    XmlElement issueLocation = doc.CreateElement("issueLocation");
                    License.AppendChild(issueLocation);
                    issueLocation.InnerText = giayphep.NoiCap;
                    XmlElement type = doc.CreateElement("type");
                    License.AppendChild(type);
                    XmlElement expire = doc.CreateElement("expire");
                    License.AppendChild(expire);
                    expire.InnerText = giayphep.NgayHetHan.ToString(NgayThang.yyyyMMdd);
                    XmlElement AdditionalInformation = doc.CreateElement("AdditionalInformation");
                    License.AppendChild(AdditionalInformation);
                    XmlElement content = doc.CreateElement("content");
                    AdditionalInformation.AppendChild(content);
                    content.InnerText = giayphep.ThongTinKhac;
                    //
                    giayphep.LoadListHMDofGiayPhep();
                    foreach (GiayPhepChiTiet hct in giayphep.ListHMDofGiayPhep)
                    {
                        XmlElement GoodsItem = doc.CreateElement("GoodsItem");
                        License.AppendChild(GoodsItem);
                        XmlElement sequence = doc.CreateElement("sequence");
                        GoodsItem.AppendChild(sequence);
                        sequence.InnerText = hct.SoThuTuHang.ToString();
                        XmlElement statisticalValue = doc.CreateElement("statisticalValue");// Trị giá
                        GoodsItem.AppendChild(statisticalValue);
                        statisticalValue.InnerText = hct.TriGiaKB.ToString();
                        //
                        XmlElement CurrencyExchange = doc.CreateElement("CurrencyExchange");
                        GoodsItem.AppendChild(CurrencyExchange);
                        XmlElement currencyType = doc.CreateElement("currencyType");
                        CurrencyExchange.AppendChild(currencyType);
                        currencyType.InnerText = hct.MaNguyenTe;
                        //
                        XmlElement Commodity = doc.CreateElement("Commodity");
                        GoodsItem.AppendChild(Commodity);
                        XmlElement description = doc.CreateElement("description");
                        Commodity.AppendChild(description);
                        description.InnerText = hct.TenHang;
                        XmlElement identification = doc.CreateElement("identification");
                        Commodity.AppendChild(identification);
                        identification.InnerText = hct.MaPhu;
                        XmlElement tariffClassification = doc.CreateElement("tariffClassification");// Mã HS
                        Commodity.AppendChild(tariffClassification);
                        tariffClassification.InnerText = hct.MaHS;
                        //
                        XmlElement GoodsMeasure = doc.CreateElement("GoodsMeasure");
                        GoodsItem.AppendChild(GoodsMeasure);
                        XmlElement quantity = doc.CreateElement("quantity");
                        GoodsMeasure.AppendChild(quantity);
                        quantity.InnerText = Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hct.SoLuong, 9);
                        XmlElement measureUnit = doc.CreateElement("measureUnit");
                        GoodsMeasure.AppendChild(measureUnit);
                        measureUnit.InnerText = hct.DVT_ID;
                        //    
                        XmlElement AdditionalInformation_GoodsItem = doc.CreateElement("AdditionalInformation");
                        GoodsItem.AppendChild(AdditionalInformation_GoodsItem);
                        XmlElement content_GoodsItem = doc.CreateElement("content");
                        AdditionalInformation_GoodsItem.AppendChild(content_GoodsItem);
                    }
                }

                if (CHUNG_TU_GIAYPHEP.ChildNodes.Count == 0)
                    return null;
                return CHUNG_TU_GIAYPHEP;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return null; }
        }

        public string WSSend(string password, string maHQ, long soTK, string maLH, int namDK, long ID_TK, List<GCCT.HangChuyenTiep> listHang)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\KhaiBoSung.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.KhaiBao).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];


            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = GuidStr;

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            if (GuidStr == "")
            {
                GuidStr = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GuidStr;
                this.Update();
            }

            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ;


            XmlNode nodeTO_KHAI = doc.GetElementsByTagName("TO_KHAI")[0];
            nodeTO_KHAI.Attributes["MA_LH"].Value = maLH;
            nodeTO_KHAI.Attributes["MA_HQ"].Value = maHQ;
            nodeTO_KHAI.Attributes["SOTK"].Value = soTK + "";
            nodeTO_KHAI.Attributes["NAMDK"].Value = namDK + "";


            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            List<GiayPhep> listGiayPhep = new List<GiayPhep>();
            listGiayPhep.Add(this);
            nodeDulieu.AppendChild(GiayPhep.ConvertCollectionGiayPhepToXML(doc, listGiayPhep, ID_TK, listHang));


            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node1 = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return "";

        }

        public bool WSKhaiBaoGP(string password, string maHaiQuan, long soToKhai, string maLoaiHinh, int namDangKy, long ID_TK, string maDoanhNghiep, List<GiayPhep> list, MessageTypes messageType, MessageFunctions messageFunction, List<GCCT.HangChuyenTiep> listHang)
        {
            XmlDocument xmlDocument = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            xmlDocument.Load(path + @"\XMLChungTu\KhaiBoSung.xml");

            // Message ID.
            XmlNode xmlNodeMessageID = xmlDocument.GetElementsByTagName("messageId")[0];
            xmlNodeMessageID.InnerText = Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode xmlNodeFrom = xmlDocument.GetElementsByTagName("From")[0];

            XmlNode xmlNodeFromName = xmlNodeFrom.ChildNodes[0];
            xmlNodeFromName.InnerText = maDoanhNghiep;
            XmlNode xmlNodeFromIdentity = xmlNodeFrom.ChildNodes[1];
            xmlNodeFromIdentity.InnerText = maDoanhNghiep;

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode xmlNodeTo = xmlDocument.GetElementsByTagName("To")[0];

            XmlNode xmlNodeToName = xmlNodeTo.ChildNodes[0];
            xmlNodeToName.InnerText = maHaiQuan;
            XmlNode xmlNodeToIdentity = xmlNodeTo.ChildNodes[1];
            xmlNodeToIdentity.InnerText = maHaiQuan;

            // Subject.
            XmlNode xmlNodeSubject = xmlDocument.GetElementsByTagName("Subject")[0];

            // Message Type.
            XmlNode xmlNodeType = xmlNodeSubject.ChildNodes[0];
            xmlNodeType.InnerText = ((int)messageType).ToString();

            // Message Function.
            XmlNode xmlNodeFunction = xmlNodeSubject.ChildNodes[1];
            xmlNodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference.
            XmlNode nodeReference = xmlNodeSubject.ChildNodes[2];
            GuidStr = nodeReference.InnerText = System.Guid.NewGuid().ToString();
            this.Update();
            if (GuidStr == "")
            {
                GuidStr = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GuidStr;
                this.Update();
            }

            XmlNode xmlNodeThongTin = xmlDocument.GetElementsByTagName("THONG_TIN")[0];
            xmlNodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDoanhNghiep;
            xmlNodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = maDoanhNghiep;

            xmlNodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHaiQuan;
            xmlNodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHaiQuan;

            XmlNode xmlNodeToKhai = xmlDocument.GetElementsByTagName("TO_KHAI")[0];

            xmlNodeToKhai.Attributes["MA_LH"].Value = maLoaiHinh;
            xmlNodeToKhai.Attributes["MA_HQ"].Value = maHaiQuan.Trim();
            xmlNodeToKhai.Attributes["SOTK"].Value = soToKhai + "";
            xmlNodeToKhai.Attributes["NAMDK"].Value = namDangKy + "";


            XmlNode xmlNodeDulieu = xmlDocument.GetElementsByTagName("DU_LIEU")[0];
            List<GiayPhep> listGiayPhep = new List<GiayPhep>();
            listGiayPhep.Add(this);
            xmlNodeDulieu.AppendChild(GiayPhep.ConvertCollectionGiayPhepToXML(xmlDocument, listGiayPhep, ID_TK, listHang));

            //Service
            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                // Lưu message.
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ToKhaiNhap;
                message.MessageFunction = MessageFunctions.KhaiBao;
                message.MessageFrom = maDoanhNghiep;
                message.MessageTo = maHaiQuan.Trim();
                message.MessageContent = xmlDocument.InnerXml;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungGiayPhep;
                message.NoiDungThongBao = string.Empty;
                message.CreatedTime = DateTime.Now;
                message.Insert();

                // Khai báo.
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    string noiDung = FontConverter.TCVN2Unicode(docResult.GetElementsByTagName("Root")[0].InnerText.Trim());
                    XmlNode node1 = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);

                    // Lưu message.
                    Message message = new Message();
                    message.ItemID = this.ID;
                    message.ReferenceID = new Guid(this.GuidStr);
                    message.MessageType = MessageTypes.ThongTin;
                    message.MessageFunction = MessageFunctions.LayPhanHoi;
                    message.MessageFrom = maHaiQuan.Trim();
                    message.MessageTo = maDoanhNghiep;
                    message.MessageContent = xmlDocument.InnerXml;
                    message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungGiayPhep;
                    message.NoiDungThongBao = noiDung;// string.Empty;
                    message.CreatedTime = DateTime.Now;
                    message.Insert();

                    return true;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return false;

        }

        public bool WSLaySoTiepNhan(string password)
        {
            KDTService kdt = WebService.GetWS();

            // Đọc lại thông tin trả về từ Hải quan.
            string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            XmlDocument xmlDocumentRequest = new XmlDocument();
            xmlDocumentRequest.LoadXml(xmlRequest);

            xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocumentRequest.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }

            if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.Update();

                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungGiayPhepThanhCong;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }

            return false;
        }

        public bool WSLayPhanHoi(string password)
        {
            KDTService kdt = WebService.GetWS();

            // Đọc lại thông tin trả về từ Hải quan.
            string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            XmlDocument xmlDocumentRequest = new XmlDocument();
            xmlDocumentRequest.LoadXml(xmlRequest);

            string tmp = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText;
            xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText;
            xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText = tmp;

            xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocumentRequest.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }
            if (xmlNodeResult.Attributes["TuChoi"] != null && xmlNodeResult.Attributes["TuChoi"].Value == "yes")
            {
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.TuChoiTiepNhan;
                string lydo = FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã bị Hải quan từ chối tiếp nhận với lý do:\r\n{2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), lydo);
                message.CreatedTime = DateTime.Now;
                message.Insert();

                // Cập lại số tiếp nhận
                this.SoTiepNhan = 0;
                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                this.Update();

                return true;
            }
            else if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.Update();

                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungGiayPhepThanhCong;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }
            else if (xmlNodeResult.Attributes["SOTN"] != null)
            {
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungGiayPhepDuocChapNhan;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã được Hải quan chấp nhận.", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }


            return false;
        }

    }
}