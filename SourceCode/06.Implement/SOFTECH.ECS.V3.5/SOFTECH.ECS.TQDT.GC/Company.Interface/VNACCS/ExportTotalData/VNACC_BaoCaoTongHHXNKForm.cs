﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.ExportTotalData
{
    public partial class VNACC_BaoCaoTongHHXNKForm : BaseForm
    {
        public VNACC_BaoCaoTongHHXNKForm()
        {
            InitializeComponent();
        }
        private string GetSeachWhere()
        {
            try
            {

                //this.Cursor = Cursors.WaitCursor;

                string where = String.Empty;
                where = "TKMD.MaDonVi='" + GlobalSettings.MA_DON_VI + "' ";
                if (!String.IsNullOrEmpty(txtSoHoaDon.Text.Trim()))
                    where = where + " AND TKMD.SoHoaDon LIKE '%" + txtSoHoaDon.Text.Trim() + "%' ";
#if GC_V4
                if (!String.IsNullOrEmpty(txtSoHopDong.Text.Trim()))
                    where = where + " AND TKMD.HopDong_ID =(SELECT TOP 1 ID FROM dbo.t_KDT_GC_HopDong WHERE SoHopDong LIKE '%" + txtSoHopDong.Text.Trim() + "%') ";
#endif
                if (!String.IsNullOrEmpty(txtSoQLNB.Text.Trim()))
                    where = where + " AND TKMD.SoQuanLyNoiBoDN LIKE '%" + txtSoQLNB.Text.Trim() + "%' ";
                if (!string.IsNullOrEmpty(txtMaLoaiHinh.Text))
                {
                    if (txtMaLoaiHinh.Text.Contains(";"))
                    {
                        string[] listMlh = txtMaLoaiHinh.Text.Split(';');
                        string seachMlh = string.Empty;
                        foreach (string item in listMlh)
                        {
                            seachMlh += "'" + item.Trim() + "'" + ",";
                        }
                        seachMlh = seachMlh.Remove(seachMlh.Length - 1);
                        where = where + " AND TKMD.MaLoaiHinh in (" + seachMlh + ") ";
                    }
                    else
                    {
                        where = where + " AND TKMD.MaLoaiHinh = '" + txtMaLoaiHinh.Text.Trim() + "' ";
                    }
                }
                if (!String.IsNullOrEmpty(txtTenDoiTac.Text))
                    where = where + " AND TKMD.TenDoiTac LIKE '%" + txtTenDoiTac.Text.Trim() + "%' ";
                if (!String.IsNullOrEmpty(cbbLoaiBC.SelectedValue.ToString()))
                {
                    string LoaiBaoCao = cbbLoaiBC.SelectedValue.ToString();
                    switch (LoaiBaoCao)
                    {
                        case "V":
                            where = where + " AND SUBSTRING(CAST(TKMD.SoToKhai AS NVARCHAR(12)),1,1) IN (1,3) ";
                            break;
                        case "NV":
                            where = where + " AND SUBSTRING(CAST(TKMD.SoToKhai AS NVARCHAR(12)),1,1) IN (1) ";
                            break;
                        case "XV":
                            where = where + " AND SUBSTRING(CAST(TKMD.SoToKhai AS NVARCHAR(12)),1,1) IN (3) ";
                            break;
                    }
                }

                DateTime fromDate = clcTuNgay.Value;
                DateTime toDate = clcDenNgay.Value;
                where = where + " AND (TKMD.NgayDangKy Between '" + fromDate.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + toDate.ToString("yyyy-MM-dd 23:59:59") + "')";

                if (!String.IsNullOrEmpty(txtMaHangHoa.Text.Trim()))
                    where = where + " AND HMD.MaHangHoa LIKE '%" + txtMaHangHoa.Text.Trim() + "%' ";
                if (!String.IsNullOrEmpty(txtTenHangHoa.Text.Trim()))
                    where = where + " AND HMD.TenHang LIKE '%" + txtTenHangHoa.Text.Trim() + "%' ";
                if (!String.IsNullOrEmpty(txtMaHS.Text.Trim()))
                    where = where + " AND HMD.MaSoHang LIKE '%" + txtMaHS.Text.Trim() + "%' ";

                return where;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return " 1=1 ";
            }

        }
        private void VNACC_BaoCaoTongHHXNKForm_Load(object sender, EventArgs e)
        {
            ctrMaHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            clcDenNgay.Value = clcTuNgay.Value.AddDays(30);
            cbbLoaiBC.SelectedIndex = 0;
            btnTimKiem_Click(null, null);
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                grList.Refresh();
                grList.DataSource = KDT_VNACC_ToKhaiMauDich.BaoCaoTongHopHHXNK(GetSeachWhere(), "").Tables[0];
                grList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Báo cáo Tổng hợp hàng hóa Tờ khai xuất nhập khẩu VNACCS  Từ ngày_" + clcTuNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + " Đến ngày _" + clcDenNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = grList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbGroupBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                btnTimKiem_Click(null, null);
                grList.RootTable.Groups.Clear();
                grList.RootTable.GroupHeaderTotals.Clear();

                GridEXGroup group = new GridEXGroup();
                GridEXColumn column = new GridEXColumn();
                column = grList.RootTable.Columns["" + cbbGroupBy.SelectedValue.ToString() + ""];
                group = new GridEXGroup(column, Janus.Windows.GridEX.SortOrder.Ascending);
                grList.RootTable.Groups.Add(group);

                column = grList.RootTable.Columns["SoLuong1"];
                GridEXGroupHeaderTotal groupHeaderSL = new GridEXGroupHeaderTotal();
                groupHeaderSL.AggregateFunction = AggregateFunction.Sum;
                groupHeaderSL.Column = column;
                groupHeaderSL.TotalPrefix = "( Tổng Lượng : ";
                groupHeaderSL.TotalSuffix = ")";
                grList.RootTable.GroupHeaderTotals.Add(groupHeaderSL);

                column = grList.RootTable.Columns["TriGiaHoaDon"];
                GridEXGroupHeaderTotal groupHeaderTGHD = new GridEXGroupHeaderTotal();
                groupHeaderTGHD.AggregateFunction = AggregateFunction.Sum;
                groupHeaderTGHD.Column = column;
                groupHeaderTGHD.TotalPrefix = "( Tổng Trị giá hóa đơn : ";
                groupHeaderTGHD.TotalSuffix = ")";
                grList.RootTable.GroupHeaderTotals.Add(groupHeaderTGHD);

                column = grList.RootTable.Columns["TriGiaTinhThue"];
                GridEXGroupHeaderTotal groupHeaderTGTT = new GridEXGroupHeaderTotal();
                groupHeaderTGTT.AggregateFunction = AggregateFunction.Sum;
                groupHeaderTGTT.Column = column;
                groupHeaderTGTT.TotalPrefix = "( Tổng Trị giá tính thuế : ";
                groupHeaderTGTT.TotalSuffix = ")";
                grList.RootTable.GroupHeaderTotals.Add(groupHeaderTGTT);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
