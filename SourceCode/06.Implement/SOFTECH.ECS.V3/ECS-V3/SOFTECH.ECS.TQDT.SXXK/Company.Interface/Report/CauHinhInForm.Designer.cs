﻿namespace Company.Interface.Report
{
    partial class CauHinhInForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem11 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem12 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem13 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem14 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem15 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem16 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem17 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem18 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem19 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem20 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem21 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem22 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem23 = new Janus.Windows.EditControls.UIComboBoxItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CauHinhInForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem24 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem25 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem26 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem27 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem28 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem29 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem30 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem31 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem32 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem33 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem34 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem35 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem36 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem37 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem38 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem39 = new Janus.Windows.EditControls.UIComboBoxItem();
            this._lblDinhMuc = new System.Windows.Forms.Label();
            this.txtDinhMuc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this._lblLuongNPL = new System.Windows.Forms.Label();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.cboHienThiHD = new Janus.Windows.EditControls.UIComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.cboTKX = new Janus.Windows.EditControls.UIComboBox();
            this.cboChenhLech = new Janus.Windows.EditControls.UIComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.cbbAmTKTiep = new Janus.Windows.EditControls.UIComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtBCTK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtTongTriGiaNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDonGiaNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGiaNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this._lblDonGiaNT = new System.Windows.Forms.Label();
            this._lblTriGiaNT = new System.Windows.Forms.Label();
            this.txtTLHH = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this._lblTyLeHH = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbMauBC04 = new Janus.Windows.EditControls.UIComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbTachLam2 = new Janus.Windows.EditControls.UIComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbMauBC01 = new Janus.Windows.EditControls.UIComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cboTKhaiKoTK = new Janus.Windows.EditControls.UIComboBox();
            this.cbNPLKoTK = new Janus.Windows.EditControls.UIComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbbSapXep = new Janus.Windows.EditControls.UIComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLuongSP = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this._lblLuongSp = new System.Windows.Forms.Label();
            this.txtLuongNPL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvDinhMuc = new Company.Controls.CustomValidation.RangeValidator();
            this.rfvLuongNPL = new Company.Controls.CustomValidation.RangeValidator();
            this.rfvLuongSP = new Company.Controls.CustomValidation.RangeValidator();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.rdNgayTuNhap = new Janus.Windows.EditControls.UIRadioButton();
            this.rdNgayTuDong = new Janus.Windows.EditControls.UIRadioButton();
            this.txtDiaPhuong = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.rangeValidator1 = new Company.Controls.CustomValidation.RangeValidator();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.ch07DMDK = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.numTimeDelay = new System.Windows.Forms.NumericUpDown();
            this.numericFontBCXNT = new System.Windows.Forms.NumericUpDown();
            this.label28 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label26 = new System.Windows.Forms.Label();
            this.cboXuongDongThongTinDiaChi = new Janus.Windows.EditControls.UIComboBox();
            this.cboKCX = new Janus.Windows.EditControls.UIComboBox();
            this.cboChiPhi = new Janus.Windows.EditControls.UIComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cboDVT = new Janus.Windows.EditControls.UIComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.cboLoaiHinh = new Janus.Windows.EditControls.UIComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cboTGTT = new Janus.Windows.EditControls.UIComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cboSTNDT = new Janus.Windows.EditControls.UIComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPageBaoCao = new Janus.Windows.UI.Tab.UITabPage();
            this.uiTabPageToKhai = new Janus.Windows.UI.Tab.UITabPage();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.numericFontTK = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDinhMuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvLuongNPL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvLuongSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericFontBCXNT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPageBaoCao.SuspendLayout();
            this.uiTabPageToKhai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericFontTK)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Size = new System.Drawing.Size(477, 436);
            // 
            // _lblDinhMuc
            // 
            this._lblDinhMuc.BackColor = System.Drawing.Color.Transparent;
            this._lblDinhMuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblDinhMuc.Location = new System.Drawing.Point(11, 27);
            this._lblDinhMuc.Name = "_lblDinhMuc";
            this._lblDinhMuc.Size = new System.Drawing.Size(68, 13);
            this._lblDinhMuc.TabIndex = 0;
            this._lblDinhMuc.Text = "Định mức";
            this._lblDinhMuc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDinhMuc
            // 
            this.txtDinhMuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDinhMuc.Location = new System.Drawing.Point(93, 22);
            this.txtDinhMuc.Name = "txtDinhMuc";
            this.txtDinhMuc.Size = new System.Drawing.Size(86, 21);
            this.txtDinhMuc.TabIndex = 0;
            this.txtDinhMuc.Text = "0";
            this.txtDinhMuc.Value = 0;
            this.txtDinhMuc.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtDinhMuc.VisualStyleManager = this.vsmMain;
            // 
            // _lblLuongNPL
            // 
            this._lblLuongNPL.BackColor = System.Drawing.Color.Transparent;
            this._lblLuongNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblLuongNPL.Location = new System.Drawing.Point(1, 54);
            this._lblLuongNPL.Name = "_lblLuongNPL";
            this._lblLuongNPL.Size = new System.Drawing.Size(85, 13);
            this._lblLuongNPL.TabIndex = 2;
            this._lblLuongNPL.Text = "Lượng NPL";
            this._lblLuongNPL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSave
            // 
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(161, 405);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(74, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.label33);
            this.uiGroupBox1.Controls.Add(this.label37);
            this.uiGroupBox1.Controls.Add(this.label39);
            this.uiGroupBox1.Controls.Add(this.label32);
            this.uiGroupBox1.Controls.Add(this.cboHienThiHD);
            this.uiGroupBox1.Controls.Add(this.label24);
            this.uiGroupBox1.Controls.Add(this.label21);
            this.uiGroupBox1.Controls.Add(this.cboTKX);
            this.uiGroupBox1.Controls.Add(this.cboChenhLech);
            this.uiGroupBox1.Controls.Add(this.label22);
            this.uiGroupBox1.Controls.Add(this.cbbAmTKTiep);
            this.uiGroupBox1.Controls.Add(this.label16);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.txtBCTK);
            this.uiGroupBox1.Controls.Add(this.label31);
            this.uiGroupBox1.Controls.Add(this.label36);
            this.uiGroupBox1.Controls.Add(this.txtTongTriGiaNT);
            this.uiGroupBox1.Controls.Add(this.txtDonGiaNT);
            this.uiGroupBox1.Controls.Add(this.txtTriGiaNT);
            this.uiGroupBox1.Controls.Add(this._lblDonGiaNT);
            this.uiGroupBox1.Controls.Add(this._lblTriGiaNT);
            this.uiGroupBox1.Controls.Add(this.txtTLHH);
            this.uiGroupBox1.Controls.Add(this._lblTyLeHH);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.cbMauBC04);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this._lblDinhMuc);
            this.uiGroupBox1.Controls.Add(this._lblLuongNPL);
            this.uiGroupBox1.Controls.Add(this.cbTachLam2);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.cbMauBC01);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.cboTKhaiKoTK);
            this.uiGroupBox1.Controls.Add(this.cbNPLKoTK);
            this.uiGroupBox1.Controls.Add(this.label23);
            this.uiGroupBox1.Controls.Add(this.txtDinhMuc);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.cbbSapXep);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.txtLuongSP);
            this.uiGroupBox1.Controls.Add(this._lblLuongSp);
            this.uiGroupBox1.Controls.Add(this.txtLuongNPL);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(11, 77);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(446, 285);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Báo cáo thông thư 194";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Location = new System.Drawing.Point(185, 220);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(29, 13);
            this.label33.TabIndex = 35;
            this.label33.Text = "0 - 8";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Location = new System.Drawing.Point(184, 185);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(29, 13);
            this.label37.TabIndex = 34;
            this.label37.Text = "0 - 5";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Location = new System.Drawing.Point(184, 161);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(29, 13);
            this.label39.TabIndex = 34;
            this.label39.Text = "0 - 6";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Location = new System.Drawing.Point(184, 136);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(29, 13);
            this.label32.TabIndex = 34;
            this.label32.Text = "0 - 5";
            // 
            // cboHienThiHD
            // 
            this.cboHienThiHD.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboHienThiHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Không";
            uiComboBoxItem1.Value = 0;
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Có";
            uiComboBoxItem2.Value = 1;
            this.cboHienThiHD.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cboHienThiHD.Location = new System.Drawing.Point(315, 215);
            this.cboHienThiHD.Name = "cboHienThiHD";
            this.cboHienThiHD.Size = new System.Drawing.Size(122, 21);
            this.cboHienThiHD.TabIndex = 15;
            this.cboHienThiHD.VisualStyleManager = this.vsmMain;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(233, 209);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(77, 42);
            this.label24.TabIndex = 32;
            this.label24.Text = "Hiển thị Hợp đồng trong BC Thanh khoản";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(238, 185);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(67, 13);
            this.label21.TabIndex = 31;
            this.label21.Text = "Sắp xếp TKX";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboTKX
            // 
            this.cboTKX.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboTKX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Ngày đăng ký";
            uiComboBoxItem3.Value = 0;
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Ngày thực xuất";
            uiComboBoxItem4.Value = 1;
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Ngày hoàn thành";
            uiComboBoxItem5.Value = 2;
            this.cboTKX.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4,
            uiComboBoxItem5});
            this.cboTKX.Location = new System.Drawing.Point(316, 182);
            this.cboTKX.Name = "cboTKX";
            this.cboTKX.Size = new System.Drawing.Size(121, 21);
            this.cboTKX.TabIndex = 14;
            this.cboTKX.VisualStyleManager = this.vsmMain;
            // 
            // cboChenhLech
            // 
            this.cboChenhLech.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboChenhLech.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "0";
            uiComboBoxItem6.Value = 0;
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "1";
            uiComboBoxItem7.Value = 1;
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "5";
            uiComboBoxItem8.Value = 5;
            this.cboChenhLech.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem6,
            uiComboBoxItem7,
            uiComboBoxItem8});
            this.cboChenhLech.Location = new System.Drawing.Point(315, 254);
            this.cboChenhLech.Name = "cboChenhLech";
            this.cboChenhLech.Size = new System.Drawing.Size(122, 21);
            this.cboChenhLech.TabIndex = 7;
            this.cboChenhLech.VisualStyleManager = this.vsmMain;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(237, 262);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 13);
            this.label22.TabIndex = 28;
            this.label22.Text = "Chênh lệch";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbbAmTKTiep
            // 
            this.cbbAmTKTiep.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbAmTKTiep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Không";
            uiComboBoxItem9.Value = 0;
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Có";
            uiComboBoxItem10.Value = 1;
            this.cbbAmTKTiep.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem9,
            uiComboBoxItem10});
            this.cbbAmTKTiep.Location = new System.Drawing.Point(93, 254);
            this.cbbAmTKTiep.Name = "cbbAmTKTiep";
            this.cbbAmTKTiep.Size = new System.Drawing.Size(86, 21);
            this.cbbAmTKTiep.TabIndex = 6;
            this.cbbAmTKTiep.VisualStyleManager = this.vsmMain;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(28, 259);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(58, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "Âm TK tiếp";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(185, 109);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "0 - 8";
            // 
            // txtBCTK
            // 
            this.txtBCTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBCTK.Location = new System.Drawing.Point(92, 215);
            this.txtBCTK.Name = "txtBCTK";
            this.txtBCTK.Size = new System.Drawing.Size(86, 21);
            this.txtBCTK.TabIndex = 5;
            this.txtBCTK.Text = "0";
            this.txtBCTK.Value = 0;
            this.txtBCTK.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtBCTK.VisualStyleManager = this.vsmMain;
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(7, 213);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(83, 33);
            this.label31.TabIndex = 19;
            this.label31.Text = "Số thập phân \r\nBC thanh khoản";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(8, 187);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(82, 13);
            this.label36.TabIndex = 19;
            this.label36.Text = "Tổng Trị giá NT";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTongTriGiaNT
            // 
            this.txtTongTriGiaNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTriGiaNT.Location = new System.Drawing.Point(92, 182);
            this.txtTongTriGiaNT.Name = "txtTongTriGiaNT";
            this.txtTongTriGiaNT.Size = new System.Drawing.Size(86, 21);
            this.txtTongTriGiaNT.TabIndex = 4;
            this.txtTongTriGiaNT.Text = "0";
            this.txtTongTriGiaNT.Value = 0;
            this.txtTongTriGiaNT.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtTongTriGiaNT.VisualStyleManager = this.vsmMain;
            // 
            // txtDonGiaNT
            // 
            this.txtDonGiaNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaNT.Location = new System.Drawing.Point(93, 156);
            this.txtDonGiaNT.Name = "txtDonGiaNT";
            this.txtDonGiaNT.Size = new System.Drawing.Size(86, 21);
            this.txtDonGiaNT.TabIndex = 4;
            this.txtDonGiaNT.Text = "0";
            this.txtDonGiaNT.Value = 0;
            this.txtDonGiaNT.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtDonGiaNT.VisualStyleManager = this.vsmMain;
            // 
            // txtTriGiaNT
            // 
            this.txtTriGiaNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaNT.Location = new System.Drawing.Point(93, 131);
            this.txtTriGiaNT.Name = "txtTriGiaNT";
            this.txtTriGiaNT.Size = new System.Drawing.Size(86, 21);
            this.txtTriGiaNT.TabIndex = 4;
            this.txtTriGiaNT.Text = "0";
            this.txtTriGiaNT.Value = 0;
            this.txtTriGiaNT.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtTriGiaNT.VisualStyleManager = this.vsmMain;
            // 
            // _lblDonGiaNT
            // 
            this._lblDonGiaNT.BackColor = System.Drawing.Color.Transparent;
            this._lblDonGiaNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblDonGiaNT.Location = new System.Drawing.Point(-8, 163);
            this._lblDonGiaNT.Name = "_lblDonGiaNT";
            this._lblDonGiaNT.Size = new System.Drawing.Size(82, 13);
            this._lblDonGiaNT.TabIndex = 19;
            this._lblDonGiaNT.Text = "Đơn giá NT";
            this._lblDonGiaNT.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _lblTriGiaNT
            // 
            this._lblTriGiaNT.BackColor = System.Drawing.Color.Transparent;
            this._lblTriGiaNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblTriGiaNT.Location = new System.Drawing.Point(-8, 138);
            this._lblTriGiaNT.Name = "_lblTriGiaNT";
            this._lblTriGiaNT.Size = new System.Drawing.Size(82, 13);
            this._lblTriGiaNT.TabIndex = 19;
            this._lblTriGiaNT.Text = "Trị giá NT";
            this._lblTriGiaNT.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTLHH
            // 
            this.txtTLHH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTLHH.Location = new System.Drawing.Point(93, 104);
            this.txtTLHH.Name = "txtTLHH";
            this.txtTLHH.Size = new System.Drawing.Size(86, 21);
            this.txtTLHH.TabIndex = 3;
            this.txtTLHH.Text = "0";
            this.txtTLHH.Value = 0;
            this.txtTLHH.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtTLHH.VisualStyleManager = this.vsmMain;
            // 
            // _lblTyLeHH
            // 
            this._lblTyLeHH.BackColor = System.Drawing.Color.Transparent;
            this._lblTyLeHH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblTyLeHH.Location = new System.Drawing.Point(-8, 111);
            this._lblTyLeHH.Name = "_lblTyLeHH";
            this._lblTyLeHH.Size = new System.Drawing.Size(82, 13);
            this._lblTyLeHH.TabIndex = 19;
            this._lblTyLeHH.Text = "Tỷ lệ HH";
            this._lblTyLeHH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(185, 54);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "0 - 8";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(185, 81);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "0 - 8";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(185, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "0 - 10";
            // 
            // cbMauBC04
            // 
            this.cbMauBC04.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbMauBC04.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem11.FormatStyle.Alpha = 0;
            uiComboBoxItem11.IsSeparator = false;
            uiComboBoxItem11.Text = "Ngày hoàn thành";
            uiComboBoxItem11.Value = 0;
            uiComboBoxItem12.FormatStyle.Alpha = 0;
            uiComboBoxItem12.IsSeparator = false;
            uiComboBoxItem12.Text = "Ngày thực xuất";
            uiComboBoxItem12.Value = 1;
            this.cbMauBC04.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem11,
            uiComboBoxItem12});
            this.cbMauBC04.Location = new System.Drawing.Point(316, 128);
            this.cbMauBC04.Name = "cbMauBC04";
            this.cbMauBC04.Size = new System.Drawing.Size(121, 21);
            this.cbMauBC04.TabIndex = 12;
            this.cbMauBC04.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(238, 132);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Mẫu BC 04";
            // 
            // cbTachLam2
            // 
            this.cbTachLam2.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbTachLam2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem13.FormatStyle.Alpha = 0;
            uiComboBoxItem13.IsSeparator = false;
            uiComboBoxItem13.Text = "Không tách làm 2";
            uiComboBoxItem13.Value = 0;
            uiComboBoxItem14.FormatStyle.Alpha = 0;
            uiComboBoxItem14.IsSeparator = false;
            uiComboBoxItem14.Text = "Tách làm 2";
            uiComboBoxItem14.Value = 1;
            this.cbTachLam2.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem13,
            uiComboBoxItem14});
            this.cbTachLam2.Location = new System.Drawing.Point(316, 101);
            this.cbTachLam2.Name = "cbTachLam2";
            this.cbTachLam2.Size = new System.Drawing.Size(121, 21);
            this.cbTachLam2.TabIndex = 11;
            this.cbTachLam2.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(238, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Báo cáo TT79";
            // 
            // cbMauBC01
            // 
            this.cbMauBC01.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbMauBC01.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem15.FormatStyle.Alpha = 0;
            uiComboBoxItem15.IsSeparator = false;
            uiComboBoxItem15.Text = "Thuế NK phải nộp";
            uiComboBoxItem15.Value = 0;
            uiComboBoxItem16.FormatStyle.Alpha = 0;
            uiComboBoxItem16.IsSeparator = false;
            uiComboBoxItem16.Text = "Thuế NK không thu";
            uiComboBoxItem16.Value = 1;
            this.cbMauBC01.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem15,
            uiComboBoxItem16});
            this.cbMauBC01.Location = new System.Drawing.Point(316, 74);
            this.cbMauBC01.Name = "cbMauBC01";
            this.cbMauBC01.Size = new System.Drawing.Size(121, 21);
            this.cbMauBC01.TabIndex = 10;
            this.cbMauBC01.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(238, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Mẫu BC 01";
            // 
            // cboTKhaiKoTK
            // 
            this.cboTKhaiKoTK.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboTKhaiKoTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem17.FormatStyle.Alpha = 0;
            uiComboBoxItem17.IsSeparator = false;
            uiComboBoxItem17.Text = "Không";
            uiComboBoxItem17.Value = 0;
            uiComboBoxItem18.FormatStyle.Alpha = 0;
            uiComboBoxItem18.IsSeparator = false;
            uiComboBoxItem18.Text = "Có";
            uiComboBoxItem18.Value = 1;
            this.cboTKhaiKoTK.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem17,
            uiComboBoxItem18});
            this.cboTKhaiKoTK.Location = new System.Drawing.Point(316, 46);
            this.cboTKhaiKoTK.Name = "cboTKhaiKoTK";
            this.cboTKhaiKoTK.Size = new System.Drawing.Size(121, 21);
            this.cboTKhaiKoTK.TabIndex = 9;
            this.cboTKhaiKoTK.VisualStyleManager = this.vsmMain;
            // 
            // cbNPLKoTK
            // 
            this.cbNPLKoTK.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbNPLKoTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem19.FormatStyle.Alpha = 0;
            uiComboBoxItem19.IsSeparator = false;
            uiComboBoxItem19.Text = "Không";
            uiComboBoxItem19.Value = 0;
            uiComboBoxItem20.FormatStyle.Alpha = 0;
            uiComboBoxItem20.IsSeparator = false;
            uiComboBoxItem20.Text = "Có";
            uiComboBoxItem20.Value = 1;
            this.cbNPLKoTK.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem19,
            uiComboBoxItem20});
            this.cbNPLKoTK.Location = new System.Drawing.Point(316, 22);
            this.cbNPLKoTK.Name = "cbNPLKoTK";
            this.cbNPLKoTK.Size = new System.Drawing.Size(121, 21);
            this.cbNPLKoTK.TabIndex = 8;
            this.cbNPLKoTK.VisualStyleManager = this.vsmMain;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(237, 51);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(66, 13);
            this.label23.TabIndex = 8;
            this.label23.Text = "TK không TK";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(238, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "NPL không TK";
            // 
            // cbbSapXep
            // 
            this.cbbSapXep.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbSapXep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem21.FormatStyle.Alpha = 0;
            uiComboBoxItem21.IsSeparator = false;
            uiComboBoxItem21.Text = "Theo ngày ĐK tờ khai";
            uiComboBoxItem21.Value = 1;
            uiComboBoxItem22.FormatStyle.Alpha = 0;
            uiComboBoxItem22.IsSeparator = false;
            uiComboBoxItem22.Text = "Theo ngày thực nhập";
            uiComboBoxItem22.Value = 2;
            uiComboBoxItem23.FormatStyle.Alpha = 0;
            uiComboBoxItem23.IsSeparator = false;
            uiComboBoxItem23.Text = "Theo mã NPL";
            uiComboBoxItem23.Value = 0;
            this.cbbSapXep.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem21,
            uiComboBoxItem22,
            uiComboBoxItem23});
            this.cbbSapXep.Location = new System.Drawing.Point(316, 155);
            this.cbbSapXep.Name = "cbbSapXep";
            this.cbbSapXep.Size = new System.Drawing.Size(121, 21);
            this.cbbSapXep.TabIndex = 13;
            this.cbbSapXep.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(238, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Sắp xếp TKN";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLuongSP
            // 
            this.txtLuongSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongSP.Location = new System.Drawing.Point(93, 76);
            this.txtLuongSP.Name = "txtLuongSP";
            this.txtLuongSP.Size = new System.Drawing.Size(86, 21);
            this.txtLuongSP.TabIndex = 2;
            this.txtLuongSP.Text = "0";
            this.txtLuongSP.Value = 0;
            this.txtLuongSP.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtLuongSP.VisualStyleManager = this.vsmMain;
            // 
            // _lblLuongSp
            // 
            this._lblLuongSp.BackColor = System.Drawing.Color.Transparent;
            this._lblLuongSp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblLuongSp.Location = new System.Drawing.Point(-2, 80);
            this._lblLuongSp.Name = "_lblLuongSp";
            this._lblLuongSp.Size = new System.Drawing.Size(82, 13);
            this._lblLuongSp.TabIndex = 4;
            this._lblLuongSp.Text = "Lượng SP";
            this._lblLuongSp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtLuongNPL
            // 
            this.txtLuongNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongNPL.Location = new System.Drawing.Point(93, 49);
            this.txtLuongNPL.Name = "txtLuongNPL";
            this.txtLuongNPL.Size = new System.Drawing.Size(86, 21);
            this.txtLuongNPL.TabIndex = 1;
            this.txtLuongNPL.Text = "0";
            this.txtLuongNPL.Value = 0;
            this.txtLuongNPL.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtLuongNPL.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(241, 405);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvDinhMuc
            // 
            this.rfvDinhMuc.ControlToValidate = this.txtDinhMuc;
            this.rfvDinhMuc.ErrorMessage = "Số thập phân định mức không hợp lệ";
            this.rfvDinhMuc.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDinhMuc.Icon")));
            this.rfvDinhMuc.MaximumValue = "10";
            this.rfvDinhMuc.MinimumValue = "0";
            this.rfvDinhMuc.Type = Company.Controls.CustomValidation.ValidationDataType.Integer;
            // 
            // rfvLuongNPL
            // 
            this.rfvLuongNPL.ControlToValidate = this.txtLuongNPL;
            this.rfvLuongNPL.ErrorMessage = "Số thập phân lượng NPL không hợp lệ";
            this.rfvLuongNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvLuongNPL.Icon")));
            this.rfvLuongNPL.MaximumValue = "8";
            this.rfvLuongNPL.MinimumValue = "0";
            this.rfvLuongNPL.Type = Company.Controls.CustomValidation.ValidationDataType.Integer;
            // 
            // rfvLuongSP
            // 
            this.rfvLuongSP.ControlToValidate = this.txtLuongSP;
            this.rfvLuongSP.ErrorMessage = "Số thập phân sản phẩm không hợp lệ";
            this.rfvLuongSP.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvLuongSP.Icon")));
            this.rfvLuongSP.MaximumValue = "8";
            this.rfvLuongSP.MinimumValue = "0";
            this.rfvLuongSP.Type = Company.Controls.CustomValidation.ValidationDataType.Integer;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.rdNgayTuNhap);
            this.uiGroupBox2.Controls.Add(this.rdNgayTuDong);
            this.uiGroupBox2.Controls.Add(this.txtDiaPhuong);
            this.uiGroupBox2.Controls.Add(this.label13);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(11, 3);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(446, 68);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Báo cáo chung";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(18, 40);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(133, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Ngày..tháng..năm..";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // rdNgayTuNhap
            // 
            this.rdNgayTuNhap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdNgayTuNhap.Location = new System.Drawing.Point(303, 37);
            this.rdNgayTuNhap.Name = "rdNgayTuNhap";
            this.rdNgayTuNhap.Size = new System.Drawing.Size(104, 23);
            this.rdNgayTuNhap.TabIndex = 2;
            this.rdNgayTuNhap.Text = "Để trống";
            // 
            // rdNgayTuDong
            // 
            this.rdNgayTuDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdNgayTuDong.Location = new System.Drawing.Point(157, 37);
            this.rdNgayTuDong.Name = "rdNgayTuDong";
            this.rdNgayTuDong.Size = new System.Drawing.Size(124, 23);
            this.rdNgayTuDong.TabIndex = 1;
            this.rdNgayTuDong.Text = "Ngày hệ thống";
            // 
            // txtDiaPhuong
            // 
            this.txtDiaPhuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaPhuong.Location = new System.Drawing.Point(138, 16);
            this.txtDiaPhuong.Name = "txtDiaPhuong";
            this.txtDiaPhuong.Size = new System.Drawing.Size(269, 21);
            this.txtDiaPhuong.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(7, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(120, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Tỉnh/Thành phố";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // rangeValidator1
            // 
            this.rangeValidator1.ControlToValidate = this.txtTLHH;
            this.rangeValidator1.ErrorMessage = "Số thập phân sản phẩm không hợp lệ";
            this.rangeValidator1.Icon = ((System.Drawing.Icon)(resources.GetObject("rangeValidator1.Icon")));
            this.rangeValidator1.MaximumValue = "8";
            this.rangeValidator1.MinimumValue = "0";
            this.rangeValidator1.Type = Company.Controls.CustomValidation.ValidationDataType.Integer;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label35);
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.ch07DMDK);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.numTimeDelay);
            this.groupBox1.Controls.Add(this.numericFontBCXNT);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.numericFontTK);
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.cboXuongDongThongTinDiaChi);
            this.groupBox1.Controls.Add(this.cboKCX);
            this.groupBox1.Controls.Add(this.cboChiPhi);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.cboDVT);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.cboLoaiHinh);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.cboTGTT);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.cboSTNDT);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(11, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(446, 355);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin in tờ khai";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(219, 164);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(35, 13);
            this.label35.TabIndex = 38;
            this.label35.Text = "(giây)";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(4, 154);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(83, 32);
            this.label34.TabIndex = 37;
            this.label34.Text = "Thời gian chờ gửi và nhận thông tin:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ch07DMDK
            // 
            this.ch07DMDK.AutoSize = true;
            this.ch07DMDK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ch07DMDK.Location = new System.Drawing.Point(27, 191);
            this.ch07DMDK.Name = "ch07DMDK";
            this.ch07DMDK.Size = new System.Drawing.Size(186, 17);
            this.ch07DMDK.TabIndex = 9;
            this.ch07DMDK.Text = "Hiển thị số tờ khai mẫu  07/ĐMĐK";
            this.ch07DMDK.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(1, 122);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(89, 27);
            this.label29.TabIndex = 35;
            this.label29.Text = "Xuống dòng thông tin địa chỉ";
            // 
            // numTimeDelay
            // 
            this.numTimeDelay.Location = new System.Drawing.Point(92, 157);
            this.numTimeDelay.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numTimeDelay.Name = "numTimeDelay";
            this.numTimeDelay.Size = new System.Drawing.Size(121, 21);
            this.numTimeDelay.TabIndex = 8;
            this.numTimeDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // numericFontBCXNT
            // 
            this.numericFontBCXNT.Location = new System.Drawing.Point(315, 97);
            this.numericFontBCXNT.Name = "numericFontBCXNT";
            this.numericFontBCXNT.Size = new System.Drawing.Size(122, 21);
            this.numericFontBCXNT.TabIndex = 8;
            this.numericFontBCXNT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(228, 97);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(88, 27);
            this.label28.TabIndex = 33;
            this.label28.Text = "Font BC Xuất Nhập tồn";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(315, 68);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(122, 21);
            this.numericUpDown1.TabIndex = 7;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(227, 70);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(88, 13);
            this.label26.TabIndex = 33;
            this.label26.Text = "Font dòng hàng";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboXuongDongThongTinDiaChi
            // 
            this.cboXuongDongThongTinDiaChi.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboXuongDongThongTinDiaChi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem24.FormatStyle.Alpha = 0;
            uiComboBoxItem24.IsSeparator = false;
            uiComboBoxItem24.Text = "Không";
            uiComboBoxItem24.Value = false;
            uiComboBoxItem25.FormatStyle.Alpha = 0;
            uiComboBoxItem25.IsSeparator = false;
            uiComboBoxItem25.Text = "Có";
            uiComboBoxItem25.Value = true;
            this.cboXuongDongThongTinDiaChi.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem24,
            uiComboBoxItem25});
            this.cboXuongDongThongTinDiaChi.Location = new System.Drawing.Point(92, 124);
            this.cboXuongDongThongTinDiaChi.Name = "cboXuongDongThongTinDiaChi";
            this.cboXuongDongThongTinDiaChi.Size = new System.Drawing.Size(121, 21);
            this.cboXuongDongThongTinDiaChi.TabIndex = 4;
            this.cboXuongDongThongTinDiaChi.VisualStyleManager = this.vsmMain;
            // 
            // cboKCX
            // 
            this.cboKCX.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboKCX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem26.FormatStyle.Alpha = 0;
            uiComboBoxItem26.IsSeparator = false;
            uiComboBoxItem26.Text = "Không";
            uiComboBoxItem26.Value = 0;
            uiComboBoxItem27.FormatStyle.Alpha = 0;
            uiComboBoxItem27.IsSeparator = false;
            uiComboBoxItem27.Text = "Có";
            uiComboBoxItem27.Value = 1;
            this.cboKCX.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem26,
            uiComboBoxItem27});
            this.cboKCX.Location = new System.Drawing.Point(92, 97);
            this.cboKCX.Name = "cboKCX";
            this.cboKCX.Size = new System.Drawing.Size(121, 21);
            this.cboKCX.TabIndex = 3;
            this.cboKCX.VisualStyleManager = this.vsmMain;
            // 
            // cboChiPhi
            // 
            this.cboChiPhi.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboChiPhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem28.FormatStyle.Alpha = 0;
            uiComboBoxItem28.IsSeparator = false;
            uiComboBoxItem28.Text = "Không";
            uiComboBoxItem28.Value = 0;
            uiComboBoxItem29.FormatStyle.Alpha = 0;
            uiComboBoxItem29.IsSeparator = false;
            uiComboBoxItem29.Text = "Có";
            uiComboBoxItem29.Value = 1;
            this.cboChiPhi.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem28,
            uiComboBoxItem29});
            this.cboChiPhi.Location = new System.Drawing.Point(92, 70);
            this.cboChiPhi.Name = "cboChiPhi";
            this.cboChiPhi.Size = new System.Drawing.Size(121, 21);
            this.cboChiPhi.TabIndex = 2;
            this.cboChiPhi.VisualStyleManager = this.vsmMain;
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(227, 44);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(59, 13);
            this.label20.TabIndex = 31;
            this.label20.Text = "Đơn vị tính";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboDVT
            // 
            this.cboDVT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboDVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem30.FormatStyle.Alpha = 0;
            uiComboBoxItem30.IsSeparator = false;
            uiComboBoxItem30.Text = "Kiện";
            uiComboBoxItem30.Value = 0;
            uiComboBoxItem31.FormatStyle.Alpha = 0;
            uiComboBoxItem31.IsSeparator = false;
            uiComboBoxItem31.Text = "Pallet";
            uiComboBoxItem31.Value = 1;
            uiComboBoxItem32.FormatStyle.Alpha = 0;
            uiComboBoxItem32.IsSeparator = false;
            uiComboBoxItem32.Text = "Viên";
            uiComboBoxItem32.Value = 2;
            uiComboBoxItem33.FormatStyle.Alpha = 0;
            uiComboBoxItem33.IsSeparator = false;
            uiComboBoxItem33.Text = "Thùng";
            uiComboBoxItem33.Value = 3;
            this.cboDVT.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem30,
            uiComboBoxItem31,
            uiComboBoxItem32,
            uiComboBoxItem33});
            this.cboDVT.Location = new System.Drawing.Point(315, 44);
            this.cboDVT.Name = "cboDVT";
            this.cboDVT.Size = new System.Drawing.Size(121, 21);
            this.cboDVT.TabIndex = 6;
            this.cboDVT.VisualStyleManager = this.vsmMain;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(5, 65);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 32);
            this.label25.TabIndex = 29;
            this.label25.Text = "Hiển thị chi phí trên tờ khai";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(10, 44);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 13);
            this.label19.TabIndex = 29;
            this.label19.Text = "Loại Hình";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboLoaiHinh
            // 
            this.cboLoaiHinh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboLoaiHinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem34.FormatStyle.Alpha = 0;
            uiComboBoxItem34.IsSeparator = false;
            uiComboBoxItem34.Text = "Sản xuất khẩu";
            uiComboBoxItem34.Value = 0;
            uiComboBoxItem35.FormatStyle.Alpha = 0;
            uiComboBoxItem35.IsSeparator = false;
            uiComboBoxItem35.Text = "Khu Chế Xuất";
            uiComboBoxItem35.Value = 1;
            this.cboLoaiHinh.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem34,
            uiComboBoxItem35});
            this.cboLoaiHinh.Location = new System.Drawing.Point(92, 44);
            this.cboLoaiHinh.Name = "cboLoaiHinh";
            this.cboLoaiHinh.Size = new System.Drawing.Size(121, 21);
            this.cboLoaiHinh.TabIndex = 1;
            this.cboLoaiHinh.VisualStyleManager = this.vsmMain;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(227, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 13);
            this.label18.TabIndex = 27;
            this.label18.Text = "Trị giá Tính Thuế";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboTGTT
            // 
            this.cboTGTT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboTGTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem36.FormatStyle.Alpha = 0;
            uiComboBoxItem36.IsSeparator = false;
            uiComboBoxItem36.Text = "In TGTT Tờ khai chính";
            uiComboBoxItem36.Value = 0;
            uiComboBoxItem37.FormatStyle.Alpha = 0;
            uiComboBoxItem37.IsSeparator = false;
            uiComboBoxItem37.Text = "Không in TGTT Tờ khai chính";
            uiComboBoxItem37.Value = 1;
            this.cboTGTT.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem36,
            uiComboBoxItem37});
            this.cboTGTT.Location = new System.Drawing.Point(315, 17);
            this.cboTGTT.Name = "cboTGTT";
            this.cboTGTT.Size = new System.Drawing.Size(121, 21);
            this.cboTGTT.TabIndex = 5;
            this.cboTGTT.VisualStyleManager = this.vsmMain;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(11, 25);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 13);
            this.label17.TabIndex = 25;
            this.label17.Text = "Số TNĐKĐT";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboSTNDT
            // 
            this.cboSTNDT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cboSTNDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem38.FormatStyle.Alpha = 0;
            uiComboBoxItem38.IsSeparator = false;
            uiComboBoxItem38.Text = "In số TNĐKĐT";
            uiComboBoxItem38.Value = 0;
            uiComboBoxItem39.FormatStyle.Alpha = 0;
            uiComboBoxItem39.IsSeparator = false;
            uiComboBoxItem39.Text = "Không in số TNĐKĐT";
            uiComboBoxItem39.Value = 1;
            this.cboSTNDT.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem38,
            uiComboBoxItem39});
            this.cboSTNDT.Location = new System.Drawing.Point(92, 17);
            this.cboSTNDT.Name = "cboSTNDT";
            this.cboSTNDT.Size = new System.Drawing.Size(121, 21);
            this.cboSTNDT.TabIndex = 0;
            this.cboSTNDT.VisualStyleManager = this.vsmMain;
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(0, 90);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(94, 31);
            this.label27.TabIndex = 29;
            this.label27.Text = "Thanh Khoản KCX";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uiTab1
            // 
            this.uiTab1.Location = new System.Drawing.Point(3, 3);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(471, 392);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPageBaoCao,
            this.uiTabPageToKhai});
            this.uiTab1.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPageBaoCao
            // 
            this.uiTabPageBaoCao.AutoScroll = true;
            this.uiTabPageBaoCao.Controls.Add(this.uiGroupBox2);
            this.uiTabPageBaoCao.Controls.Add(this.uiGroupBox1);
            this.uiTabPageBaoCao.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageBaoCao.Name = "uiTabPageBaoCao";
            this.uiTabPageBaoCao.Size = new System.Drawing.Size(469, 370);
            this.uiTabPageBaoCao.TabStop = true;
            this.uiTabPageBaoCao.Text = "Thông tin báo cáo";
            // 
            // uiTabPageToKhai
            // 
            this.uiTabPageToKhai.Controls.Add(this.groupBox1);
            this.uiTabPageToKhai.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageToKhai.Name = "uiTabPageToKhai";
            this.uiTabPageToKhai.Size = new System.Drawing.Size(469, 370);
            this.uiTabPageToKhai.TabStop = true;
            this.uiTabPageToKhai.Text = "Thông tin tờ khai";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(227, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Font tờ khai";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numericFontTK
            // 
            this.numericFontTK.Location = new System.Drawing.Point(316, 124);
            this.numericFontTK.Name = "numericFontTK";
            this.numericFontTK.Size = new System.Drawing.Size(122, 21);
            this.numericFontTK.TabIndex = 7;
            this.numericFontTK.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CauHinhInForm
            // 
            this.AcceptButton = this.btnSave;
            this.ClientSize = new System.Drawing.Size(477, 436);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CauHinhInForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cấu hình báo cáo ";
            this.Load += new System.EventHandler(this.CauHinhInForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDinhMuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvLuongNPL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvLuongSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rangeValidator1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericFontBCXNT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPageBaoCao.ResumeLayout(false);
            this.uiTabPageToKhai.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericFontTK)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label _lblLuongNPL;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDinhMuc;
        private System.Windows.Forms.Label _lblDinhMuc;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuongSP;
        private System.Windows.Forms.Label _lblLuongSp;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuongNPL;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIComboBox cbbSapXep;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIComboBox cbNPLKoTK;
        private Janus.Windows.EditControls.UIComboBox cbMauBC01;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIComboBox cbTachLam2;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UIComboBox cbMauBC04;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.RangeValidator rfvDinhMuc;
        private Company.Controls.CustomValidation.RangeValidator rfvLuongNPL;
        private Company.Controls.CustomValidation.RangeValidator rfvLuongSP;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.EditControls.UIRadioButton rdNgayTuNhap;
        private Janus.Windows.EditControls.UIRadioButton rdNgayTuDong;
        private System.Windows.Forms.TextBox txtDiaPhuong;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTLHH;
        private System.Windows.Forms.Label _lblTyLeHH;
        private Company.Controls.CustomValidation.RangeValidator rangeValidator1;
        private Janus.Windows.EditControls.UIComboBox cbbAmTKTiep;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox1;
        private Janus.Windows.EditControls.UIComboBox cboSTNDT;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.EditControls.UIComboBox cboTGTT;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.EditControls.UIComboBox cboLoaiHinh;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.EditControls.UIComboBox cboDVT;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.EditControls.UIComboBox cboTKX;
        private Janus.Windows.EditControls.UIComboBox cboChenhLech;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.EditControls.UIComboBox cboTKhaiKoTK;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.EditControls.UIComboBox cboHienThiHD;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.EditControls.UIComboBox cboChiPhi;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private Janus.Windows.EditControls.UIComboBox cboKCX;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.NumericUpDown numericFontBCXNT;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private Janus.Windows.EditControls.UIComboBox cboXuongDongThongTinDiaChi;
        private System.Windows.Forms.CheckBox ch07DMDK;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtBCTK;
        private System.Windows.Forms.Label label31;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaNT;
        private System.Windows.Forms.Label _lblTriGiaNT;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageBaoCao;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageToKhai;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.NumericUpDown numTimeDelay;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTongTriGiaNT;
        private System.Windows.Forms.Label label39;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaNT;
        private System.Windows.Forms.Label _lblDonGiaNT;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NumericUpDown numericFontTK;
        private System.Windows.Forms.Label label1;
    }
}
