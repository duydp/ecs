using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.SXXK.ThanhKhoan
{
    public partial class BCXuatNhapTon
    {
        public static DataSet SelectBCXuatNhapTon(string whereCondition, string orderByExpression)
        {
            try
            {
                string spName = "SELECT * FROM v_t_KDT_SXXK_BCXuatNhapTon WHERE " + whereCondition;
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

                return db.ExecuteDataSet(dbCommand);
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }


        }
        public static DataTable GetSoLanNPLThamGiaThanhKhoan(string maDoanhNghiep, int lanThanhLy)
        {
            //Tuannq 05062015
            //Thay đổi GROUP theo LUONGNHAP
            string sql = "SELECT MaNPL, CASE WHEN MaLoaiHinhNhap LIKE '%V%' THEN (SELECT TOP 1 SoTKVNACCS FROM t_VNACCS_CapSoToKhai WHERE SoTK = SoToKhaiNhap) ELSE SoToKhaiNhap END AS SoToKhaiNhap, MaLoaiHinhNhap FROM t_KDT_SXXK_BCXuatNhapTon " +
                         "WHERE (MaDoanhNghiep = @MaDoanhNghiep AND LanThanhLy = @LanThanhLy) " +
                         "GROUP BY MaNPL, SoToKhaiNhap, MaLoaiHinhNhap, LuongNhap";
            //
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", DbType.String, maDoanhNghiep);
            db.AddInParameter(dbCommand, "@LanThanhLy", DbType.Int32, lanThanhLy);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public DataTable GetToKhaiNhapDaPhanBo(DateTime minDate, DateTime maxDate)
        {
            string sql =
                         "SELECT DISTINCT SoToKhaiNhap as SoToKhai ,Year(NgayDangKyNhap) as NamDangKy ,MaHaiQuan, MaLoaiHinhNhap as MaLoaiHinh FROM t_SXXK_BCXuatNhapTon " +
                         "WHERE NgayDangKyNhap BETWEEN @MinDate AND @MaxDate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MinDate", DbType.DateTime, minDate);
            db.AddInParameter(dbCommand, "@MaxDate", DbType.DateTime, maxDate);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public DataTable GetToKhaiXuatDaPhanBo(DateTime minDate, DateTime maxDate)
        {
            string sql =
                         "SELECT DISTINCT SoToKhaiXuat as SoToKhai,Year(NgayDangKyXuat) as NamDangKy, MaHaiQuan, MaLoaiHinhXuat as MaLoaiHinh FROM t_SXXK_BCXuatNhapTon " +
                         "WHERE NgayDangKyXuat BETWEEN @MinDate AND @MaxDate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MinDate", DbType.DateTime, minDate);
            db.AddInParameter(dbCommand, "@MaxDate", DbType.DateTime, maxDate);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public DataSet GetPhanBoToKhaiXuat(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan)
        {
            string sp = "p_SXXK_GetPhanBoToKhaiXuat";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(sp);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, soToKhai);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHaiQuan);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, maLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, namDangKy);
            return db.ExecuteDataSet(dbCommand);
        }
        public static int UpdateByDonGiaTT( string Mahang, double DonGiaTTKB, double donGiaTT, int lanthanhly, double ThueSuat)
        {
            string spName = "update t_KDT_SXXK_BCXuatNhapTon set DonGiaTT = @DonGiaTT, ThueSuat = @ThueSuat where LanThanhLy = @LanThanhLy and MaNPL=@MaPhu and Round(DonGiaTT,0) = Round(@DonGiaTTKB,0)";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, lanthanhly);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Float, donGiaTT);
            db.AddInParameter(dbCommand, "@DonGiaTTKB", SqlDbType.Float, DonGiaTTKB);
            db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Float, ThueSuat);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, Mahang);

            return db.ExecuteNonQuery(dbCommand);
        }
        public static decimal GetNPLSuDung(string MaNPL,int SoTKNhap)
        {
            string sql = String.Format(@"SELECT TOP 1 A.LuongTonCuoi FROM (SELECT TOP 1 LuongTonCuoi FROM dbo.t_KDT_SXXK_BCXuatNhapTon WHERE MaNPL='{0}' AND SoToKhaiNhap={1} AND LuongTonCuoi > 0 ORDER BY LuongTonCuoi ASC) A ORDER BY A.LuongTonCuoi",MaNPL,SoTKNhap);
            SqlDatabase sqlData = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand sqlCommand = (SqlCommand)sqlData.GetSqlStringCommand(sql);

            object obj = sqlData.ExecuteScalar(sqlCommand);
            if (obj == null || String.IsNullOrEmpty(obj.ToString()))
            {
                return 0;
            }
            else
            {
                return (decimal)obj;
            }
        }
    }
}