
GO
IF EXISTS(SELECT * FROM sys.columns
WHERE Name = N'StorageAreasProduction_ID' AND OBJECT_ID = OBJECT_ID(N't_KDT_VNACCS_Careeries'))
BEGIN
    	DECLARE @FOREIGN_KEY_NAME VARCHAR(100);
		SET @FOREIGN_KEY_NAME = (SELECT CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
		WHERE TABLE_NAME='t_KDT_VNACCS_Careeries' AND CONSTRAINT_TYPE='FOREIGN KEY');
		BEGIN
			DECLARE @DROP_COMMAND NVARCHAR(150) = 'ALTER TABLE t_KDT_VNACCS_Careeries DROP CONSTRAINT' + ' ' + @FOREIGN_KEY_NAME;
			EXECUTE Sp_executesql @DROP_COMMAND ;
		END
END  

GO

IF EXISTS(SELECT * FROM sys.columns
WHERE Name = N'StorageAreasProduction_ID' AND OBJECT_ID = OBJECT_ID(N't_KDT_VNACCS_Careeries'))
BEGIN
       ALTER TABLE t_KDT_VNACCS_Careeries 
	   DROP COLUMN  StorageAreasProduction_ID
END 

GO
GO

IF NOT EXISTS(SELECT * FROM sys.columns
WHERE Name = N'ManufactureFactory_ID' AND OBJECT_ID = OBJECT_ID(N't_KDT_VNACCS_Careeries'))
BEGIN
    ALTER TABLE t_KDT_VNACCS_Careeries 
	ADD ManufactureFactory_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_ManufactureFactory(ID)   
END 

GO

IF NOT EXISTS(SELECT * FROM sys.columns
WHERE Name = N'NgayKetThucNamTC' AND OBJECT_ID = OBJECT_ID(N't_KDT_VNACCS_StorageAreasProduction'))
BEGIN
	ALTER TABLE dbo.t_KDT_VNACCS_StorageAreasProduction
		ADD NgayKetThucNamTC DATETIME NULL,
			LoaiHinhDN INT  NULL,
			LoaiSua INT,
			SoLuongSoHuu NUMERIC(10)  NULL,
			SoLuongDiThue NUMERIC(10)  NULL,
			SoLuongKhac NUMERIC(10)  NULL,
			TongSoLuong NUMERIC(10)  NULL ; 
END 

GO

IF NOT EXISTS(SELECT * FROM sys.columns
WHERE Name = N'DienTichNhaXuong' AND OBJECT_ID = OBJECT_ID(N't_KDT_VNACCS_ManufactureFactory'))
BEGIN
   	ALTER TABLE t_KDT_VNACCS_ManufactureFactory
		ADD DienTichNhaXuong NUMERIC(20,4)  NULL,
			SoLuongCongNhan NUMERIC(10,0)  NULL ;
END 

GO
 IF OBJECT_ID(N't_KDT_VNACCS_StorageOfGoods') IS NOT NULL 
	DROP TABLE t_KDT_VNACCS_StorageOfGoods
GO
 IF OBJECT_ID(N't_KDT_VNACCS_StorageAreasProduction_AttachedFiles') IS NOT NULL 
	DROP TABLE t_KDT_VNACCS_StorageAreasProduction_AttachedFiles
GO
 IF OBJECT_ID(N't_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product') IS NOT NULL 
	DROP TABLE t_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product
GO
 IF OBJECT_ID(N't_KDT_VNACCS_OutsourcingManufactureFactory_Product') IS NOT NULL 
	DROP TABLE t_KDT_VNACCS_OutsourcingManufactureFactory_Product
GO
 IF OBJECT_ID(N't_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory') IS NOT NULL 
	DROP TABLE t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory
GO
 IF OBJECT_ID(N't_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument') IS NOT NULL 
	DROP TABLE t_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument
GO
 IF OBJECT_ID(N't_KDT_VNACCS_OutsourcingManufactureFactory') IS NOT NULL 
	DROP TABLE t_KDT_VNACCS_OutsourcingManufactureFactory
GO
 IF OBJECT_ID(N't_KDT_VNACCS_ProductionCapacity_Product') IS NOT NULL 
	DROP TABLE t_KDT_VNACCS_ProductionCapacity_Product
GO
 IF OBJECT_ID(N't_KDT_VNACCS_Careeries_Product') IS NOT NULL 
	DROP TABLE t_KDT_VNACCS_Careeries_Product
GO

CREATE TABLE t_KDT_VNACCS_StorageOfGoods
(
ID BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY,
StorageAreasProduction_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_StorageAreasProduction(ID),
Ten NVARCHAR(255) NOT NULL,
Ma NVARCHAR(7) NOT NULL
)
GO
CREATE TABLE t_KDT_VNACCS_Careeries_Product
(
ID BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY,
Careeries_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_Careeries(ID),
MaSP NVARCHAR(50) NOT NULL,
MaHS NVARCHAR(12) NOT NULL,
ChuKySXTG NUMERIC(5) NOT NULL,
ChuKySXDVT INT NOT NULL
)

GO
CREATE TABLE t_KDT_VNACCS_ProductionCapacity_Product
(
ID BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY,
Careeries_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_Careeries(ID),
ThoiGianSXTG NUMERIC(5),
ThoiGianSXDVT INT,
MaSP NVARCHAR(50) NOT NULL,
MaHS NVARCHAR(12) NOT NULL,
DVT NVARCHAR(4) NOT NULL,
SoLuong NUMERIC(10,0)
)
GO
CREATE TABLE t_KDT_VNACCS_OutsourcingManufactureFactory
(
ID BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY,
StorageAreasProduction_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_StorageAreasProduction(ID),
TenDoiTac NVARCHAR(255) NOT NULL,
MaDoiTac NVARCHAR(7) NOT NULL,
DiaChiDoiTac NVARCHAR(255) NOT NULL
)
GO
CREATE TABLE t_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument
(
ID BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY,
OutsourcingManufactureFactory_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_OutsourcingManufactureFactory(ID),
SoHopDong NVARCHAR(80) NOT NULL,
NgayHopDong DATETIME NOT NULL,
NgayHetHan DATETIME NOT NULL
)
GO
CREATE TABLE t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory
(
ID BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY,
OutsourcingManufactureFactory_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_OutsourcingManufactureFactory(ID),
DiaChiCSSX NVARCHAR(255) NOT NULL,
DienTichNX NUMERIC(20,4) NOT NULL,
SoLuongCongNhan NUMERIC(10) NOT NULL,
SoLuongSoHuu NUMERIC(10) NOT NULL,
SoLuongDiThue NUMERIC(10) NOT NULL,
SoLuongKhac NUMERIC(10) NOT NULL,
TongSoLuong NUMERIC(10) NOT NULL,
NangLucSX NVARCHAR(200) NOT NULL
)
GO
CREATE TABLE t_KDT_VNACCS_OutsourcingManufactureFactory_Product
(
ID BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY,
OutsourcingManufactureFactory_ManufactureFactory_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory(ID),
MaSP NVARCHAR(50) NOT NULL,
MaHS NVARCHAR(12) NOT NULL,
ChuKySXTG NUMERIC(5) NOT NULL,
ChuKySXDVT INT NOT NULL
)
GO
CREATE TABLE t_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product
(
ID BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY,
OutsourcingManufactureFactory_ManufactureFactory_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory(ID),
ThoiGianSXTG NUMERIC(5),
ThoiGianSXDVT INT,
MaSP NVARCHAR(50) NOT NULL,
MaHS NVARCHAR(12) NOT NULL,
DVT NVARCHAR(4) NOT NULL,
SoLuong NUMERIC(10,0)
)
GO
CREATE TABLE t_KDT_VNACCS_StorageAreasProduction_AttachedFiles
(
ID BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY,
StorageAreasProduction_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_StorageAreasProduction(ID),
[FileName] NVARCHAR(25) NOT NULL,
[FileSize] BIGINT,
Content NVARCHAR(MAX) NOT NULL
)

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '27.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('27.7',GETDATE(), N'CẬP NHẬT KHAI BÁO CƠ SỞ SẢN XUẤT')
END
