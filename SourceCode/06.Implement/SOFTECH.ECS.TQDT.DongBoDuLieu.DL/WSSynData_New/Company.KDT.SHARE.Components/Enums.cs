﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.KDT.SHARE.Components
{
    public enum EDanhMucHaiQuan
    {
        CuaKhau,
        Cuc,
        DieuKienGiaoHang,
        DonViHaiQuan,
        DonViTinh,
        LoaiHinhMauDich,
        LoaiPhiChungTuThanhToan,
        MaHS,
        NguyenTe,
        Nuoc,
        PhuongThucThanhToan,
        PhuongThucVanTai
    }

    public enum TrangThaiThanhKhoan
    {
        DaDongHoSo = 401,
        DaChayThanhKhoan = 400
    }
    public enum RoleHopDong
    {
        KhaiDienTu = 100,
        CapNhatDuLieu = 101,
        XemDuLieu = 102,
    };
    public enum RoleToKhaiCoHD
    {
        KhaiDienTuXuat = 400,
        KhaiDienTuNhap = 401,
        CapNhatDuLieu = 402,
        XemDuLieu = 403,
    };
    public enum RoleNPLTuCungUng
    {
        KhaiDienTu = 700,
        CapNhatDuLieu = 701,
        XemDuLieu = 702,
    };
    public enum RoleToKhaiCoGCCT
    {
        KhaiDienTuXuat = 500,
        KhaiDienTuNhap = 501,
        CapNhatDuLieu = 502,
        XemDuLieu = 503,
    };
    public enum RoleToKhaiKhongCoHD
    {
        KhaiDienTuXuat = 600,
        KhaiDienTuNhap = 601,
        CapNhatDuLieu = 602,
        XemDuLieu = 603,
    };
    public class LoaiHinhGiaCongChuyenTiep
    {
        public static readonly string Nhap_NguyenLieu_Tu_HDGC_Khac = "NGC18";
        public static readonly string Nhap_SanPham_GCCT_Tu_HDGC_Khac = "NGC19";
        public static readonly string Nhap_MayMoc_ThietBi_Tu_HDGC_Khac = "NGC20";
        public static readonly string Xuat_NguyenLieu_Cho_HDGC_Khac = "XGC18";
        public static readonly string Xuat_SanPham_GCCT_Cho_HDGC_Khac = "XGC19";
        public static readonly string Xuat_MayMoc_ThietBi_Cho_HDGC_Khac = "XGC20";
    }

    /// <summary>
    /// Trạng thái tờ khai hiển thị trên form.
    /// </summary>
    [Serializable]
    public enum ActionStatus
    {
        ToKhai = 0,
        ToKhaiSua = 100,     //Tờ khai sửa
        ToKhaiXinHuy = 200,  //Tờ khai xin hủy
        ToKhaiDaHuy = 300,   //Tờ khai đã hủy    
        HopDongSua = 400,
        DN_GiamSatSua = 500,
        ThanhKhoanSua = 600,
        GiaHanSua = 700,
        PhuKienSua=0
    }
    public class LoaiGiayKiemTra
    {
        public static readonly string DANG_KY_KIEM_TRA = "DK";
        public static readonly string KET_QUA_KIEM_TRA = "KQ";
    }
    [Serializable]
    public class TrangThaiXuLy
    {
        public static readonly int HUY_KHAI_BAO = -4;
        public static readonly int DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI = -3;
        public static readonly int CHO_DUYET_DASUACHUA = -2;
        public static readonly int CHUA_KHAI_BAO = -1;
        public static readonly int CHO_DUYET = 0;
        public static readonly int DA_DUYET = 1;
        public static readonly int KHONG_PHE_DUYET = 2;
        public static readonly int DA_PHAN_LUONG = 3;
        public static readonly int SUA_KHAI_BAO = 4;
        public static readonly int SUATKDADUYET = 5;
        public static readonly int HUYTKDADUYET = -5;
        public static readonly int DA_HUY = 10;
        public static readonly int CHO_HUY = 11;
        public static readonly int DA_CO_DANH_SACH_HANG_QUA_KVGS = 6;
        public static readonly int CHUA_CO_PHAN_HOI_QUA_KVGS = 7;
        public static readonly int DA_CO_PHAN_HOI_QUA_KVGS = 8;

        public static readonly string strCHUAKHAIBAO = "Chưa khai báo";
        public static readonly string strCHODUYET = "Đã khai báo, chờ duyệt";
        public static readonly string strDADUYET = "Đã duyệt";
        public static readonly string strDAHUY = "Đã hủy";
        public static readonly string strCHOHUY = "Chờ hủy";
        public static readonly string strTUCHOI = "Từ chối";
        public static readonly string strSUATKDADUYET = "Đang sửa";
        public static readonly string strKHONGPHEDUYET = "Không phê duyệt";
        public static readonly string strSUAKHAIBAO = "Đã khai báo sửa";
    }
    public class TrangThaiToKhaiNopPhi
    {
        public static readonly int CHUA_KHAI_BAO = -1;
        public static readonly int DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI = -3;
        public static readonly int DA_CO_SO_TIEP_NHAN = 0;
        public static readonly int DA_CO_THONG_BAO_NOP_PHI = 1;
        public static readonly int DUOC_CHAP_NHAN = 2;
        public static readonly int DA_THUC_HIEN_NOP_PHI = 3;

        public static readonly string strCHUAKHAIBAO = "Chưa khai báo";
        public static readonly string strDACOSOTIEPNHAN = "Đã có số tiếp nhận";
        public static readonly string strDACOTHONGBAONOPPHI = "Đã có thông báo nộp phí";
        public static readonly string strDUOCCHAPNHAN = "Được chấp nhận";
        public static readonly string strDATHUCHIENNOPPHI = "Đã thực hiện nộp phí";
    }
    public enum ELoaiContainer
    {
        Container20 = 2,
        Container40 = 4,
        Container45 = 5,
        ContainerKhac = 9
    }
    public class LoaiVanDon
    {
        public static readonly string DUONG_BIEN = "1";
        public static readonly string DUONG_KHONG = "2";
        public static readonly string DUONG_BO = "3";
        public static readonly string KHAC = "9";
    }

    public class TrangThaiPhanLuong
    {
        public static readonly string LUONG_XANH = "1";
        public static readonly string LUONG_VANG = "2";
        public static readonly string LUONG_DO = "3";

    }

    [Serializable]
    public class LoaiKhaiBao
    {
        public static readonly string NguyenPhuLieu = "NPL";
        public static readonly string SanPham = "SP";
        public static readonly string DinhMuc = "DM";
        public static readonly string ToKhai = "TK";
        public static readonly string HopDong = "HD";
        public static readonly string PhuKien = "PK";
        public static readonly string GiamSatTieuHuy = "GT";
        public static readonly string ThanhKhoanHopDong = "TKHD";
        public static readonly string GiaHanThanhKhoan = "GHTK";
        public static readonly string ThanhKhoan = "SXXK_TK";
        public static readonly string DMHH = "DMHH";
        public static readonly string ToKhaiCT = "TKCT";
        public static readonly string ThanhLyTaiSanCoDinh = "TLTSCD";
        public static readonly string PhuKien_TK = "PK_TK";
        public static readonly string TuCungUng = "TuCungUng";
        public static readonly string TaiXuat = "TaiXuat";
        public static readonly string TieuHuy = "TieuHuy";
        public static readonly string HuyTieuHuy = "HuyTieuHuy";
        public static readonly string HangTon = "HangTon";
        public static readonly string HuyHangTon = "HuyHangTon";
        public static readonly string Container = "Cont";
        public static readonly string ContainerKVGS = "ContKVGS";

        public static readonly string PhieuNhapKho = "PhieuKhoCFS";
        //
        public static readonly string License = "License";
        public static readonly string ContractDocument = "ContractDocument";
        public static readonly string ContractReference = "ContractReference";
        public static readonly string CommercialInvoice = "CommercialInvoice";
        public static readonly string CertificateOfOrigin = "CertificateOfOrigin";
        public static readonly string BillOfLading = "BillOfLading";
        public static readonly string CapSoDinhDanh = "CapSoDinhDanh";
        public static readonly string Containers = "Container";
        public static readonly string AdditionalDocument = "AdditionalDocument";
        public static readonly string OverTime = "OverTime";
        public static readonly string GoodItem = "GoodItem";
        public static readonly string GoodItemContract = "GoodItemContract";
        public static readonly string StorageAreasProduction = "StorageAreasProduction";
        public static readonly string BillOfLadingNew = "BillOfLadingNew";
        public static readonly string ScrapInformation = "ScrapInformation";
        public static readonly string TransportEquipment = "TransportEquipment";
        public static readonly string WareHouseImport = "WareHouseImport";
        public static readonly string WareHouseExport = "WareHouseExport";
        public static readonly string TotalInventoryReport = "TotalInventoryReport";

        // Khai báo thu phí cảng HP

        public static readonly string RegisterInformaton = "RegisterInformaton";
        public static readonly string RegisterHangContainer = "RegisterHangContainer";
        public static readonly string RegisterHangRoi = "RegisterHangRoi";
        public static readonly string SearchInformaton = "SearchInformaton";
  


    }

    public class NgayThang
    {
        public static readonly string yyyyMMdd = "yyyy-MM-dd";
    }

    [Serializable]
    public enum OpenFormType
    {
        View,
        Insert,
        Edit,
        EditAll
    }

    [Serializable]
    public enum MessageTypes
    {
        // Update by KhanhHn - Types Version 3
        /// <summary>
        /// Tờ khai nhập = 1
        /// </summary>
        ToKhaiNhap = 929,

        /// <summary>
        /// Tờ khai xuất = 2
        /// </summary>
        ToKhaiXuat = 930,

        /// <summary>
        /// Tờ khai chuyễn tiếp nhập = 3
        /// </summary>
        ToKhaiChuyenTiepNhap = 3,

        /// <summary>
        /// Tờ khai chuyển tiếp xuất = 4
        /// </summary>
        ToKhaiChuyenTiepXuat = 4,

        /// <summary>
        /// Danh mục nguyên phụ liệu = 5
        /// </summary>
        DanhMucNguyenPhuLieu = 5,

        /// <summary>
        /// Danh mục sản phẩm = 6
        /// </summary>
        DanhMucSanPham = 6,

        /// <summary>
        /// Hợp đồng = 7
        /// </summary>
        HopDong = 7,

        /// <summary>
        /// Phụ kiện = 8
        /// </summary>
        PhuKien = 8,

        /// <summary>
        /// Định mức = 9
        /// </summary>
        DinhMuc = 9,

        /// <summary>
        /// Định mức cung ứng = 10
        /// </summary>
        DinhMucCungUng = 10,

        /// <summary>
        /// Thông tin = 11
        /// </summary>
        ThongTin = 11,

        /// <summary>
        /// Hồ sơ thanh khoản SXXK = 12
        /// </summary>
        HoSoThanhKhoanSXXK = 12,
        DeNghiGiamSatTieuHuy = 13
    }
    public class MessgaseType
    {
        public static readonly int ToKhaiNhap = 1;
        public static readonly int ToKhaiXuat = 2;
        public static readonly int ToKhaiChuyenTiepNhap = 3;
        public static readonly int ToKhaiChuyenTiepXuat = 4;
        public static readonly int DanhMucNguyenPhuLieu = 5;
        public static readonly int DanhMucSanPham = 6;
        public static readonly int HopDong = 7;
        public static readonly int PhuKien = 8;
        public static readonly int DinhMuc = 9;
        public static readonly int DinhMucCungUng = 10;
        public static readonly int ThongTin = 11;
        public static readonly int HoSoThanhKhoanSXXK = 12;
    }
    [Serializable]
    public enum MessageFunctions
    {
        /// <summary>
        /// Khai báo = 1
        /// </summary>
        KhaiBao = 1,

        /// <summary>
        /// Hủy khai báo = 2
        /// </summary>
        HuyKhaiBao = 2,

        /// <summary>
        /// Hỏi trạng thái = 3
        /// </summary>
        HoiTrangThai = 3,

        /// <summary>
        /// Lấy thông báo Hải quan = 4
        /// </summary>
        LayThongBaoHQ = 4,

        /// <summary>
        /// Lấy phản hồi = 5
        /// </summary>
        LayPhanHoi = 5,

        /// <summary>
        /// Phản hồi = 6
        /// </summary>
        PhanHoi = 6,

        /// <summary>
        /// Đổi mật khẩu = 7
        /// </summary>
        DoiMatKhau = 7,
        DaKhaiBao = 8,
        CapSoTiepNhan = 29,
        DuyetSoTiepNhan = 32,
        KhaiBaoSua = 5,
        KhaiBaoHuy =1,
        /// <summary>
        /// Chuyển trạng thái bằng tay
        /// </summary>
        ChuyenTrangThaiTay = -1,
        SuaToKhai = -2
    }

    [Serializable]
    public class MessageTitle
    {
        // Thu phí cảng HP
        public static readonly string RegisterInformation = "Đăng ký thông tin doanh nghiệp";
        public static readonly string RegisterHangContainer = "Đăng ký Tờ khai nộp phí hàng Container";
        public static readonly string RegisterHangRoi = "Đăng ký Tờ khai nộp phí hàng lỏng, rời";
        public static readonly string SearchInformation = "Tra cứu thông tin biên lai ";

        public static readonly string PhieuNhapKho = "Phiếu nhập kho";

        public static readonly string ResultFeedback  = "HQ trả kết quả phản hồi";
        public static readonly string ChuyenKhaiBaoSua = "Chuyển sang Trạng thái Khai báo sửa";
        public static readonly string ChuyenTrangThaiTay = "Chuyển trạng thái bằng tay";
        public static readonly string TKChapNhanThongQuan = "Tờ khai được chấp nhận thông quan";

        public static readonly string HuyKhaiBaoToKhai = "Hủy khai báo tờ khai";
        public static readonly string HuyKhaiBaoThanhCong = "Hủy khai báo thành công";
        public static readonly string SuaToKhai = "Sửa tờ khai";
        public static readonly string HQHuyKhaiBaoToKhai = "Hải quan hủy khai báo tờ khai";


        public static readonly string HuyToKhai = "Hủy tờ khai";
        public static readonly string HuyToKhaiThanhCong = "Hủy tờ khai thành công";
        public static readonly string HuyTokhaiDuocCapSo = "Hủy tờ khai được cấp số";
        public static readonly string KhaiBaoToKhai = "Khai báo tờ khai";
        public static readonly string KhaiBaoThanhCong = "Khai báo thành công";
        public static readonly string KhaiBaoToKhaiSua = "Khai báo tờ khai sửa";
        public static readonly string Error = "Thông tin từ hải quan";
        public static readonly string None = "Không xác định";
        public static readonly string KhaiBaoLayThongBaoHQ = "Khai báo tờ khai lấy thông báo hải quan";
        public static readonly string KhaiBaoLayPhanHoiToKhai = "Khai báo lấy phản hồi tờ khai";
        public static readonly string KhaiBaoPhanHoi = "Khai báo phản hồi tờ khai";

        public static readonly string ToKhaiDuocCapSo = "Tờ khai được cấp số";
        public static readonly string ToKhaiDuocCapSoTiepNhan = "Tờ khai được cấp số tiếp nhận";
        public static readonly string ToKhaiDuocPhanLuong = "Tờ khai được phân luồng";
        public static readonly string ToKhaiAnDinhThue = "Ấn định thuế tờ khai";

        public static readonly string TuChoiTiepNhan = "Từ chối tiếp nhận";
        public static readonly string ToKhaiSuaDuocDuyet = "Tờ khai sửa được duyệt";
        //DATLMQ bổ sung trạng thái NhanThongTin ngày 21/07/2011
        public static readonly string HeThongHQDaNhanDuocThongTin = "Hệ thống HQ đã nhận được thông tin";


        public static readonly string KhaiBaoBoSungChungTuDinhKem = "Khai báo bổ sung chứng từ đính kèm";
        public static readonly string KhaiBaoBoSungLayPhanHoiChungTuDinhKem = "Khai báo bổ sung lấy phản hồi chứng từ đính kèm";

        public static readonly string KhaiBaoBoSungChungTuDinhKemThanhCong = "Khai báo bổ sung chứng từ đính kèm thành công";
        public static readonly string KhaiBaoBoSungChungTuDinhKemCoSoTN = "Khai báo bổ sung chứng từ đính kèm cấp số tiếp nhận";
        public static readonly string KhaiBaoBoSungChungTuDinhKemDuocChapNhan = "Khai báo bổ sung chứng từ đính kèm được chấp nhận";
        public static readonly string KhaiBaoBoSungAnh = "Khai báo bổ sung chứng từ ảnh đính kèm";


        public static readonly string KhaiBaoBoSungDeNghiChuyenCK = "Khai báo bổ sung đề nghị chuyển cửa khẩu";
        public static readonly string KhaiBaoBoSungHQTuChoiDeNghiChuyenCK = "Khai báo bổ sung hải quan từ chôi đề nghị chuyển cửa khẩu";

        public static readonly string KhaiBaoBoSungLayPhanHoiDeNghiChuyenCK = "Khai báo bổ sung lấy phản hồi đề nghị chuyển cửa khẩu";

        public static readonly string KhaiBaoBoSungDeNghiChuyenCKThanhCong = "Khai báo bổ sung đề nghị chuyển cửa khẩu thành công";
        public static readonly string KhaiBaoBoSungDeNghiChuyenCKDuocChapNhan = "Khai báo bổ sung đề nghị chuyển cửa khẩu được chấp nhận";



        public static readonly string KhaiBaoBoSungGiayPhep = "Khai báo bổ sung giấy phép";
        public static readonly string KhaiBaoBoSungLayPhanhoiGiayPhep = "Khai báo bổ sung giấy phép";

        public static readonly string KhaiBaoBoSungGiayPhepThanhCong = "Khai báo bổ sung giấy phép thành công";
        public static readonly string KhaiBaoBoSungGiayPhepDuocChapNhan = "Khai báo bổ sung giấy phép được chấp nhận";
        public static readonly string KhaiBaoBoSungGiayPhepDuocCapSoTN = "Khai báo bổ sung giấy phép được cấp số tiếp nhận";

        public static readonly string KhaiBaoBoSungGiayPhepTuChoiChapNhan = "Khai báo bổ sung giấy phép được chấp nhận";


        public static readonly string KhaiBaoBoSungHoaDonTM = "Khai báo bổ sung hóa đơn thương mại";
        public static readonly string KhaiBaoBoSungLayPhanHoiHoaDonTM = "Khai báo bổ sung lấy phản hồi hóa đơn thương mại";

        public static readonly string KhaiBaoBoSungHoaDonTMThanhCong = "Khai báo bổ sung hóa đơn thương mại thành công";
        public static readonly string KhaiBaoBoSungHoaDonTMDuocChapNhan = "Khai báo bổ sung hóa đơn thương mại được chấp nhận";

        public static readonly string KhaiBaoBoSungCO = "Khai báo bổ sung CO";
        public static readonly string KhaiBaoHQTuChoiCO = "Hải quan từ chối bổ sung CO";
        public static readonly string KhaiBaoHQChapNhanBoSungCO = "Hải quan chấp nhận bổ sung CO";


        public static readonly string KhaiBaoBoSungLayPhanhoiCO = "Khai báo bổ sung lấy phản hồi CO";

        public static readonly string KhaiBaoBoSungCOThanhCong = "Khai báo bổ sung CO thành công";
        public static readonly string KhaiBaoBoSungLaySoTiepNhanCO = "Khai báo bổ sung lấy số tiếp nhận CO thành công";

        public static readonly string KhaiBaoBoSungCODuocChapNhan = "Khai báo bổ sung CO được chấp nhận";

        public static readonly string KhaiBoSungGiayNopTienChapNhan = "Khai bổ sung giấy nộp tiền được chấp nhận";
        public static readonly string KhaiBoSungGiayNopTienCapSo = "Hải quan cấp số tiếp nhận giấy nộp tiền";

        public static readonly string KhaiBaoBoSungVanDon = "Khai báo bổ sung vận đơn";
        public static readonly string KhaiBaoBoSungVanDonThanhCong = "Khai báo bổ sung vận đơn thành công";
        public static readonly string KhaiBaoBoSungVanDonDuocChapNhan = "Khai báo bổ sung vận đơn được chấp nhận";

        public static readonly string KhaiBaoBoSungHopDong = "Khai báo bổ sung hợp đồng";
        public static readonly string KhaiBaoBoSungHopDongThanhCong = "Khai báo bổ sung hợp đồng thành công";
        public static readonly string KhaiBaoBoSungHopDongDuocChapNhan = "Khai báo bổ sung hợp đồng được chấp nhận";
        public static readonly string KhaiBaoBoSungLayPhanHoiHopDong = "Khai báo bổ sung lấy phản hồi hợp đồng";
        public static readonly string KhaiBaoBoSungLoiNhanDuLieuHoiHopDong = "Lỗi nhận dữ liệu hợp đồng";
        public static readonly string KhaiBaoBoSungGiayNopTien = "Khai báo bổ sung giấy nộp tiền";

        public static readonly string KhaiBaoThanhKhoanHopDong = "Khai báo thanh khoản hợp đồng";

        public static readonly string KhaiBaoHuyTK = "Khai báo hủy tờ khai";
        public static readonly string KhaiBaoHuyTKThanhCong = "Khai báo hủy tờ khai thành công";
        public static readonly string KhaiBaoHuyTKDuocChapNhan = "Khai báo hủy tờ khai được chấp nhận";

        public static readonly string KhaiBaoSuaTK = "Khai báo sửa tờ khai";

        public static readonly string KhaiBaoNguyenPhuLieu = "Khai báo nguyên phụ liệu";
        public static readonly string KhaiBaoLayPhanHoiNguyenPhuLieu = "Khai báo lấy phản hồi nguyên phụ liệu";
        public static readonly string KhaiBaoHuyNguyenPhuLieu = "Hủy khai báo nguyên phụ liệu";
        public static readonly string KhaiBaoSuaNguyenPhuLieu = "Khai báo sửa nguyên phụ liệu";
        public static readonly string KhaiBaoPhanHoiNguyenPhuLieu = "Khai báo phản hồi nguyên phụ liệu";
        public static readonly string KhaiBaoLayThongBaoHQNguyenPhuLieu = "Khai báo lấy thông báo nguyên phụ liệu hải quan";
        public static readonly string KhaiBaoHQDuyetNguyennPhuLieu = "Nguyên phụ liệu đã duyệt";
        public static readonly string KhaiBaoChapNhanHuyNguyenPhuLieu = "Chấp nhận hủy khai báo nguyên phụ liệu";
        public static readonly string KhaiBaoHQKhongDuyetNguyenPhuLieu = "Không phê duyệt nguyên phụ liệu";
        public static readonly string KhaiBaoHQDaDuyetNguyenPhuLieu = "Đã duyệt nguyên phụ liệu";
        public static readonly string KhaiBaoHQDaDuyetDMHH = "Đã duyệt DMHH";
        public static readonly string KhaiBaoHQKhongDuyetDMHH = "Không phê duyệt DMHH";

        public static readonly string KhaiBaoSanPham = "Khai báo sản phẩm";
        public static readonly string KhaiBaoSuaSanPham = "Khai báo  sửa sản phẩm";
        public static readonly string KhaiBaoHuySanPham = "Khai báo hủy sản phẩm";


        public static readonly string KhaiBaoLayPhanHoiSanPham = "Khai báo lấy phản hồi sản phẩm";
        public static readonly string KhaiBaoPhanHoiSanPham = "Khai báo phản hồi sản phẩm";
        public static readonly string KhaiBaoLayThongBaoHQSanPham = "Khai báo lấy thông báo sản phẩm hải quan";

        public static readonly string KhaiBaoHQDaDuyetSanPham = "Đã duyệt sản phẩm";
        public static readonly string KhaiBaoHQKhongDuyetSanPham = "Hải quan Không phê duyệt sản phẩm";
        public static readonly string KhaiBaoHQHuySanPham = "Chấp nhận hủy sản phẩm";
        public static readonly string KhaiBaoHQTuChoiSanPham = "Hải quan từ chối sản phẩm";


        public static readonly string KhaiBaoDinhMuc = "Khai báo định mức";
        public static readonly string KhaiBaoDinhMucSua = "Khai báo sửa định mức";
        public static readonly string KhaiBaoLayPhanHoiDinhMuc = "Lấy phản hồi định mức";
        public static readonly string KhaiBaoHuyDinhMuc = "Khai báo hủy định mức";
        public static readonly string KhaiBaoPhanHoiDinhMuc = "Khai báo phản hồi định mức";
        public static readonly string KhaiBaoLayThongBaoHQDinhMuc = "Khai báo lấy thông báo định mức hải quan";

        public static readonly string AddPhuKien = "Thêm phụ kiện";
        public static readonly string UpdatePhuKien = "Sửa phụ kiện";
        public static readonly string DeletePhuKien = "Xoá phụ kiện";

        public static readonly string LuuThongTin = "Lưu Thông tin";
        public static readonly string XoaThongTin = "Xoá Thông tin";
        public static readonly string CapNhatKetQuaXuLy = "Cập nhật Kết quả xử lý";
        public static readonly string CapNhatTrangThai = "Cập nhật Trạng thái";
        public static readonly string NhapExcel = "Nhập Excel";

        public static readonly string KhaiBaoHQHuyDinhMuc = "Chấp nhận hủy định mức";
        public static readonly string KhaiBaoHQDaXyLyDinhMuc = "HQ đã xử lý định mức";
        public static readonly string KhaiBaoHQTuChoiDinhMuc = "HQ từ chối định mức";
        public static readonly string KhaiBaoHQDuyetDinhMuc = "HQ đã duyệt định mức";

        public static readonly string KhaiBaoDMHH = "Khai báo DMHH";

        public static readonly string KhaiBaoHQHuyDMHH = "Chấp nhận hủy DMHH";
        public static readonly string KhaiBaoHQTuChoiDMHH = "HQ từ chối DMHH";
        public static readonly string KhaiBaoLayPhanHoiDMHH = "Lấy phản hồi DMHH";

        public static readonly string KhaiBaoHuyDMHH = "Khai báo hủy DMHH";
        public static readonly string KhaiBaoPhanHoiDMHH = "Khai báo phản hồi DMHH";
        public static readonly string KhaiBaoLayThongBaoHQDMHH = "Khai báo lấy thông báo DMHH hải quan";

        public static readonly string KhaiBaoHopDong = "Khai báo hợp đồng";
        public static readonly string HuyKhaiBaoHopDong = "Hủy khai báo hợp đồng";
        public static readonly string KhaiBaoHuyHopDong = "Khai báo hủy hợp đồng";
        public static readonly string KhaiBaoLayPhanHoiHopDong = "Khai báo lấy phản hồi hợp đồng";

        public static readonly string KhaiBaoPhuKien = "Khai báo phụ kiện";
        public static readonly string KhaiBaoTuChoiPhuKien = "Khai báo phụ kiện";
        public static readonly string KhaiBaoLayPhanHoiPhuKien = "Khai báo lấy phản hồi phụ kiện";
        public static readonly string KhaiBaoPhuKienHQDuyet = "Hải quan duyệt chính thức";

        public static readonly string KhaiBaoTaiXuat = "Khai báo bảng kê tái xuất";
        public static readonly string KhaiBaoLayPhanHoiTaiXuat = "Khai báo lấy phản hồi bảng kê tái xuất";
        public static readonly string KhaiBaoSuaTaiXuat = "Khai báo sửa Tái xuất";

        public static readonly string KhaiBaoTieuHuy = "Khai báo bảng kê tiêu hủy";
        public static readonly string KhaiBaoLayPhanHoiTieuHuy = "Khai báo lấy phản hồi bảng kê tiêu hủy";
        public static readonly string KhaiBaoSuaTieuHuy = "Khai báo sửa tiêu hủy";

        public static readonly string KhaiBaoHangTon = "Khai báo hàng tồn ";
        public static readonly string KhaiBaoTCU = "Khai báo NPL tự cung ứng ";
        public static readonly string KhaiBaoSuaTCU = "Khai báo sửa NPL tự cung ứng ";
        public static readonly string KhaiBaoLayPhanHoiTCU = "Khai báo lấy phản hồi NPL tự cung ứng";
        public static readonly string KhaiBaoTCUHQDuyet = "Hải quan duyệt chính thức";

        public static readonly string KhaiBaoCX_HangTon = "Khai báo BK hàng tồn kho ";
        public static readonly string KhaiBaoLayPhanHoiCX_HangTon = "Khai báo lấy phản hồi BK hàng tồn kho";

        public static readonly string KhaiBaoHQLoiKetNoi = "Lỗi kết nối hoặc hệ thống hải quan không xử lý được";

        public static readonly string KhaiBaoHQTuChoiHopDong = "Hải quan từ chối hợp đồng";
        public static readonly string KhaiBaoHQCapSoTiepNhan = "Hải quan cấp số tiếp nhận hợp đồng";
        public static readonly string KhaiBaoHQDuyet = "Hải quan duyệt chính thức";
        public static readonly string KhaiBaoHQHuyHopDong = "Hải quan hủy khai báo hợp đồng";
        public static readonly string KhaiBaoLayThongTinHopDong = "Khai báo lấy thông tin hợp đồng";
        public static readonly string KhaiBaoHQPhanLuong = "Hải quan phân luồng hợp đồng";



        public static readonly string KhaiBaoToKhaiSuaCTDuyet = "Khai báo tờ khai sửa chuyển tiếp được duyệt";
        public static readonly string KhaiBaoToKhaiSuaCTCapSo = "Khai báo sửa tờ khai chuyển tiếp được cấp số";
        public static readonly string KhaiBaoToKhaiCTKoDuyet = "Khai báo tờ khai chuyển tiếp không phê duyệt";
        public static readonly string KhaiBaoToKhaiCTDuocDuyet = "Khai báo tờ khai chuyển tiếp được duyệt";
        public static readonly string KhaiBaoToKhaiCTChoDuyet = "Khai báo tờ khai chuyển tiếp được duyệt";
        public static readonly string KhaiBaoToKhaiCTDuocPhanLuong = "Khai báo tờ khai chuyển tiếp được duyệt";
        public static readonly string HuyKhaiBaoToKhaiCT = "Hủy khai báo tờ khai chuyển tiếp";
        public static readonly string KhaiBaoToKhaiCT = "Khai báo tờ khai  chuyển tiếp";
        public static readonly string KhaiBaoSuaToKhaiCT = "Khai sửa  báo tờ khai  chuyển tiếp";

        public static readonly string KhaiBaoNPLTuCungUng = "Khai báo nguyên phụ liệu tự cung ứng";
        public static readonly string KhaiBaoNPLTuCungUngLayPhanHoi = "Khai báo lấy phản hồi nguyên phụ liệu tự cung ứng";
        public static readonly string KhaiBaoHuyNPLTuCungUng = "Khai báo hủy nguyên phụ liệu tự cung ứng";

        public static readonly string KhaiBaoDNTieuHuy = "Khai báo đề nghị tiêu hủy";
        public static readonly string LayPhanHoiDNTieuHuy = "Lấy phản hồi đề nghị tiêu hủy";
        public static readonly string CapSoTNDNTieuHuy = "Cấp số tiếp nhận đề nghị tiêu hủy";
        public static readonly string DuyetDNTieuHuy = "Duyệt đề nghị tiêu hủy";
        public static readonly string TuChoiDNTieuHuy = "Từ chối đề nghị tiêu hủy";
        public static readonly string HuyKhaiBaoThanhCongDNTieuHuy = "Hủy khai báo thành công đề nghị tiêu hủy";
        public static readonly string HuyKhaiBaoDNTieuHuy = "Hủy khai báo đề nghị tiêu hủy";

        public static readonly string KhaiBaoGiaHanTK = "Khai báo gia hạn thanh khoản";
        public static readonly string LayPhanHoiGiaHanTK = "Lấy phản hồi gia hạn thanh khoản";
        public static readonly string CapSoTNGiaHanTK = "Cấp số tiếp nhận gia hạn thanh khoản";
        public static readonly string DuyetGiaHanTK = "Duyệt gia hạn thanh khoản";
        public static readonly string TuChoiGiaHanTK = "Từ chối khai báo gia hạn thanh khoản";
        public static readonly string HuyKhaiBaoGiaHanTK = "Hủy khai báo gia hạn thanh khoản";

        public static readonly string XoaNguyenPhuLieu = "Xóa nguyên phụ liệu";
        public static readonly string XoaSanPham = "Xóa sản phẩm";
        public static readonly string XoaThietBi = "Xóa thiết bị";
        public static readonly string XoaHangMau = "Xóa hàng mẫu";
        public static readonly string XoaDinhMuc = "Xóa định mức";


        public static readonly string KhaiBaoNPLCungUng = "Khai báo NPL cung ứng";
        public static readonly string DuyetNPLCungUng = "Hải quan duyệt bản NPL cung ứng";

        public static readonly string KhaiBaoBSContainer = "Khai báo danh sách Container ";
        //
        public static readonly string RegisterLicense = "Đăng ký Giấy phép điện tử";
        public static readonly string RegisterContractDocument = "Đăng ký Hợp đồng điện tử";
        public static readonly string RegisterCommercialInvoicer = "Đăng ký Hóa đơn điện tử";
        public static readonly string RegisterCertificateOfOrigin = "Đăng ký CO điện tử";
        public static readonly string RegisterBillOfLading = "Đăng ký Vận đơn điện tử";
        public static readonly string RegisterDinhDanh = "Đăng ký số định danh hàng hóa";
        public static readonly string RegisterContainer = "Đăng ký danh sách Container đính kèm ";
        public static readonly string RegisterAdditionalDocumentr = "Đăng ký Chứng từ khác điện tử";
        public static readonly string RegisterOverTime = "Đăng ký đăng ký thủ tục HQ ngoài giờ hành chínhs ";
        public static readonly string RegisterGoodItem = "Đăng ký báo cáo quyết toán ";
        public static readonly string RegisterContractRefrence = "Đăng ký báo cáo quyết toán máy móc thiết bị ";
        public static readonly string RegisterStorageAreasProduction = "THÔNG BÁO CƠ SỞ SẢN XUẤT, NƠI LƯU GIỮ NL, VT, MMTB VÀ SP XUẤT KHẨU";
        public static readonly string RegisterBillOfLadingNew = "Khai báo Tách vận đơn";
        public static readonly string RegisterScrapInformation = "Thông tin tiêu hủy sản xuất xuất khẩu";
        public static readonly string RegisterTotalInventoryReport = "Báo cáo chốt tồn SXXK";
        public static readonly string RegisterTransportEquipment = "Tờ khai vận chuyển độc lập đủ điều kiện qua KVG";

        public static readonly string RegisterWareHouseImport = "Khai báo phiếu nhập kho";
        public static readonly string RegisterWareHouseExport = "Khai báo phiếu xuất kho";

        public static readonly string KhaiBaoTSCD = "Khai báo Thanh lý tài sản";
        public static readonly string CapSoTiepNhanTSCD = "Hải quan cấp số tiếp nhận";
        public static readonly string DuyetTSCD = "Hải quan duyệt bản thanh lý";
    }

    public class LoaiPhuKien
    {
        public static readonly string BO_SUNG_DANH_MUC_NGUYEN_PHU_LIEU_NHAP_KHAU = "N01";
        public static readonly string MUA_NGUYEN_PHU_LIEU_TAI_THI_TRUONG_VIET_NAM = "N11";
    }
    [Serializable]
    public class LoaiToKhai
    {
        public static readonly string TO_KHAI_MAU_DICH = "Tờ khai mậu dịch";
        public static readonly string TO_KHAI_CHUYEN_TIEP = "Tờ khai chuyển tiếp";
        public static readonly string DANH_MUC_NGUYEN_PHU_LIEU = "Danh mục nguyên phụ liệu";
        public static readonly string DANH_MUC_SAN_PHAM = "Danh mục sản phẩm";
        public static readonly string DINH_MUC = "Tờ khai định mức";
        public static readonly string HOP_DONG = "Tờ khai hợp đồng gia công";
        public static readonly string PHU_KIEN = "Phụ kiện hợp đồng gia công";
        public static readonly string DINH_MUC_HOP_DONG = "Định mức hợp đồng gia công";
        public static readonly string HO_SO_THANH_KHOAN = "Hồ sơ thanh khoản";
        public static readonly string DIEU_CHINH_THUE = "Điều chỉnh thuế";
    }
    [Serializable]
    public class ChucNang
    {
        public static readonly string KHAI_BAO = "Khai báo";//1
        public static readonly string NHAN_THONG_TIN = "Nhận thông tin";//2
        public static readonly string HUY_KHAI_BAO = "Hủy khai báo";       //3 
        public static readonly string DONG_BO_DU_LIEU = "Đồng bộ dữ liệu với hải quan";//4
        public static readonly string XAC_NHAN_THONG_TIN = "Xác nhận thông tin";//5
    }
    [Serializable]
    public class LAY_THONG_TIN
    {
        public static readonly string DINH_MUC = "LAY_TT_DM";
        public static readonly string NPL = "LAY_TT_NPL";
        public static readonly string SP = "LAY_TT_SP";
        public static readonly string TOKHAI = "LAY_TT_TK";
    }

    [Serializable]
    public class THONG_TIN_DANG_KY
    {
        public static readonly string DINH_MUC = "DANG_KY_DM";
        public static readonly string NPL = "DANG_KY_NPL";
        public static readonly string SP = "DANG_KY_SP";
        public static readonly string TOKHAINHAP = "DANG_KY_TKN";
        public static readonly string TOKHAIXUAT = "DANG_KY_TKX";
        public static readonly string CAPNHATTOKHAINHAP = "CAP_NHAT_TKN";
        public static readonly string CAPNHATTOKHAIXUAT = "CAP_NHAT_TKX";
        public static readonly string HOSOTHANHLY = "DANG_KY_TL_HO_SO";
        public static readonly string BK_TOKHAINHAP = "DANG_KY_TL_DSTKN";
        public static readonly string BK_TOKHAIXUAT = "DANG_KY_TL_DSTKX";
        public static readonly string BK_TAM_NOP_THUE = "DANG_KY_TL_CHI_TIET_NT";
        public static readonly string BK_CHUA_THANH_LY = "DANG_KY_TL_NPL_CHUA_TL";
        public static readonly string BK_TL_NKD = "DANG_KY_TL_NPL_NKD";
        public static readonly string BK_XUAT_GC = "DANG_KY_TL_NPL_XGC";
        public static readonly string BK_HUY = "DANG_KY_TL_NPL_HUY";
        public static readonly string BK_NOP_THUE = "DANG_KY_TL_NPL_NT";
        public static readonly string BK_TAI_XUAT = "DANG_KY_TL_NPL_TX";

    }

    [Serializable]
    public class THONG_TIN_HUY
    {
        public static readonly string DINH_MUC = "HUY_DM";
        public static readonly string NPL = "HUY_NPL";
        public static readonly string SP = "HUY_SP";
        public static readonly string TOKHAINHAP = "HUY_TKN";
        public static readonly string TOKHAIXUAT = "HUY_TKX";
        public static readonly string HOSOTHANHLY = "HUY_TL_HO_SO";
        public static readonly string BK_TOKHAINHAP = "HUY_TL_DSTKN";
        public static readonly string BK_TOKHAIXUAT = "HUY_TL_DSTKX";
        public static readonly string BK_TAM_NOP_THUE = "HUY_TL_CHI_TIET_NT";
        public static readonly string BK_CHUA_THANH_LY = "HUY_TL_NPL_CHUA_TL";
        public static readonly string BK_TL_NKD = "HUY_TL_NPL_NKD";
        public static readonly string BK_XUAT_GC = "HUY_TL_NPL_XGC";
        public static readonly string BK_HUY = "HUY_TL_NPL_HUY";
        public static readonly string BK_NOP_THUE = "HUY_TL_NPL_NT";
        public static readonly string BK_TAI_XUAT = "HUY_TL_NPL_TX";
    }

    public enum RoleNguyenPhuLieu
    {
        KhaiDienTu = 100,
        CapNhatDuLieu = 101,
        XemDuLieu = 102,
    };
    public enum RoleSanPham
    {
        KhaiDienTu = 200,
        CapNhatDuLieu = 201,
        XemDuLieu = 202,
    };
    public enum RolePhuKien
    {
        KhaiDienTu = 300,
        CapNhatDuLieu = 301,
        XemDuLieu = 302,
    };
    public enum RoleDinhMuc
    {
        KhaiDienTu = 300,
        CapNhatDuLieu = 301,
        XemDuLieu = 302,
    };
    public enum RoleToKhai
    {
        KhaiDienTuNhap = 400,
        KhaiDienTuXuat = 401,
        CapNhatDuLieu = 402,
        XemDuLieu = 403,
        ToKhaiSapHetHan = 404,
        CchiPhiXNK = 405

    };
    public enum RoleThanhKhoan
    {
        ThanhKhoan = 500
    };
    public enum RoleSystem
    {
        CreateUser = 600,   //Tạo người dùng mới
        DeleteUser = 601,   //Xóa người dùng
        UpdateUser = 602,   //Cập nhật người dùng
        CreateGroup = 603,  //Tạo mới nhóm người dùng mới
        DeleteGroup = 604,  //Xóa nhóm người dùng
        UpdateGroup = 605,  //Cập nhật nhóm người dùng
        Permission = 606,   //Phân quyền
        Management = 607,   //Xem quản trị
    };

    public enum RoleSystem_GC
    {
        CreateUser = 800,   //Tạo người dùng mới
        DeleteUser = 801,   //Xóa người dùng
        UpdateUser = 802,   //Cập nhật người dùng
        CreateGroup = 803,  //Tạo mới nhóm người dùng mới
        DeleteGroup = 804,  //Xóa nhóm người dùng
        UpdateGroup = 805,  //Cập nhật nhóm người dùng
        Permission = 806,   //Phân quyền
        Management = 807,   //Xem quản trị
    };

    public enum RoleNPLTon
    {
        TheoDoiNPLTon = 700,
        ThongKeNPLTon = 701,
    };

    public enum RoleToKhaiHTKhac
    {
        ToKhaiHTKhacNhap = 800,
        ToKhaiHTKhacXuat = 801,
    };

    public enum RoleQuanLyToKhai
    {
        ThueTonKhoTKNhap = 900,
        ThueTonKhoTKXuat = 901,
    };
}
