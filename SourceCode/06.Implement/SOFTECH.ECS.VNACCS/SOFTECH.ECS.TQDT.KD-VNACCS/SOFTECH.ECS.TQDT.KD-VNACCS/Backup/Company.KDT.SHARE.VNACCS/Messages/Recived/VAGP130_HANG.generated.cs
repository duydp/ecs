using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAGP130_HANG : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute MDC { get; set; }
public PropertiesAttribute HSC { get; set; }
public PropertiesAttribute ACE { get; set; }
public PropertiesAttribute QSD { get; set; }
public PropertiesAttribute RNO { get; set; }
public PropertiesAttribute EOM { get; set; }
public PropertiesAttribute QTT { get; set; }
public PropertiesAttribute QTU { get; set; }
public PropertiesAttribute ACA { get; set; }
public PropertiesAttribute EFF { get; set; }
public PropertiesAttribute TWE { get; set; }
public PropertiesAttribute TWU { get; set; }

public VAGP130_HANG()
        {
MDC = new PropertiesAttribute(768, typeof(string));
HSC = new PropertiesAttribute(12, typeof(string));
ACE = new PropertiesAttribute(35, typeof(string));
QSD = new PropertiesAttribute(35, typeof(string));
RNO = new PropertiesAttribute(17, typeof(string));
EOM = new PropertiesAttribute(8, typeof(DateTime));
QTT = new PropertiesAttribute(8, typeof(int));
QTU = new PropertiesAttribute(3, typeof(string));
ACA = new PropertiesAttribute(70, typeof(string));
EFF = new PropertiesAttribute(50, typeof(string));
TWE = new PropertiesAttribute(10, typeof(int));
TWU = new PropertiesAttribute(3, typeof(string));
TongSoByte = 1043;
}

}public partial class EnumGroupID
    {
}

}
