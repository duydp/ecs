﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using Cryptography = Company.KDT.SHARE.Components.Cryptography;
using ServerRemote.Mapper;
using System.Security.Cryptography.X509Certificates;
using SignRemote;
namespace ServerRemote
{
    public partial class frmLoginUSB : Company.Interface.BaseForm
    {
        public frmLoginUSB()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private string GetName(string signName)
        {
            string[] infors = signName.Split(',');
            string name = string.Empty;
            string mst = string.Empty;
            foreach (string info in infors)
            {
                int index = info.IndexOf("CN=");
                if (index > -1)
                {
                    name = info.Substring(index + 3);
                }
                index = info.IndexOf("OID");

                if (index > -1)
                {
                    index = info.IndexOf("=");
                    if (index > 0)
                        mst = info.Substring(index + 1);
                }
            }
            if (!string.IsNullOrEmpty(mst)) return string.Format("{0} - [{1}]", name, mst);
            else return string.Format("{0}", name);
        }
        private void btnRefersh_Click(object sender, EventArgs e)
        {
            
            List<System.Security.Cryptography.X509Certificates.X509Certificate2> items = Cryptography.GetX509CertificatedNames();
            cbSigns.Items.Clear();

            foreach (System.Security.Cryptography.X509Certificates.X509Certificate2 item in items)
            {
                string signName = item.SubjectName.Name.ToString();
                string name = "";
                name = GetName(signName);
                cbSigns.Items.Add(new Company.KDT.SHARE.Components.ItemSign() { Name = name, Value = signName });
            }
            try
            {
                cbSigns.DropDownWidth = DropDownWidth(cbSigns);
            }
            catch (Exception)
            {

                MessageBoxControl.ShowMessage("Thiết bị chữ ký số đã được tháo ra . Vui lòng gắn thiết bị chữ ký số vào để cấu hình.");
            }

        }

        private void cbSigns_TextChanged(object sender, EventArgs e)
        {
            Company.KDT.SHARE.Components.ItemSign sign = (Company.KDT.SHARE.Components.ItemSign)cbSigns.SelectedItem;
            X509Certificate2 x059 = Cryptography.GetStoreX509Certificate2(sign.Value);

            if (x059 != null)
            {
                lblProduct.Text = GetName(x059.IssuerName.Name);
            }
        }
        private string Check()
        {
            string msg = string.Empty;
            try
            {
                Company.KDT.SHARE.Components.ItemSign item = (Company.KDT.SHARE.Components.ItemSign)cbSigns.SelectedItem;
                Company.KDT.SHARE.Components.Globals.GetX509CertificatedName = item.Value;
                Company.KDT.SHARE.Components.Globals.PasswordSign = txtPassword.Text;
                Company.KDT.SHARE.Components.Globals.IsSignature = true;
                Helpers.GetSignature("Test");
                return "Mật khẩu được xác nhận là hợp lệ";
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        private void frmLoginUSB_Load(object sender, EventArgs e)
        {
            btnRefersh_Click(null, null);
            foreach (Company.KDT.SHARE.Components.ItemSign item in cbSigns.Items)
            {
                if (item.Value == Company.KDT.SHARE.Components.Globals.GetX509CertificatedName)
                {
                    cbSigns.SelectedItem = item;
                    break;
                }
            }

            chkIsUseSign.Checked = Company.KDT.SHARE.Components.Globals.IsSignature;
            cbSigns.SelectedValue = Company.KDT.SHARE.Components.Globals.GetX509CertificatedName;
            txtPassword.Text = Company.KDT.SHARE.Components.Globals.PasswordSign;
            if (Company.KDT.SHARE.Components.Globals.IsSignOnLan)
                rdSignOnLan.Checked = true;
            else
                rdSignOnline.Checked = true;

        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            MessageBoxControl.ShowMessage(Check());
            //MessageBox.Show(Check(), "Thông báo");
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(cbSigns.Text))
                {
                    MessageBoxControl.ShowMessage("Chưa có chữ ký số được chọn\r\nBạn có muốn lưu không?");
                    //if (MessageBox.Show("Chưa có chữ ký số được chọn\r\nBạn có muốn lưu không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    //    return;
                }
                #region Set Value
                Company.KDT.SHARE.Components.Globals.IsSignature = chkIsUseSign.Checked;
                Company.KDT.SHARE.Components.ItemSign sign = (Company.KDT.SHARE.Components.ItemSign)cbSigns.SelectedItem;
                Company.KDT.SHARE.Components.Globals.GetX509CertificatedName = sign.Value;
                Company.KDT.SHARE.Components.Globals.PasswordSign = txtPassword.Text;
                Company.KDT.SHARE.Components.Globals.IsSignOnLan = rdSignOnLan.Checked ? true: false;

                #endregion

                #region Save Value
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SuDungChuKySo", chkIsUseSign.Checked == true ? "true" : "false");
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ChuKySo", sign.Value);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PasswordSign", Helpers.EncryptString(txtPassword.Text, "KEYWORD"));
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsSignOnLan", rdSignOnLan.Checked == true ? "True" : "False");
                #endregion
                MessageBoxControl.ShowMessage("Lưu cấu hình chữ ký số thành công.");
                //MessageBox.Show("Lưu cấu hình chữ ký số thành công.", "Thông báo");
                DialogResult = DialogResult.OK;
                Globals.GetX509CertificatedName = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ChuKySo");
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBoxControl.ShowMessage(ex.Message);
                //MessageBox.Show(ex.Message, "Thông báo");
            }
        }

        int DropDownWidth(ComboBox myCombo)
        {
            int maxWidth = 0;
            int temp = 0;
            Label label1 = new Label();

            foreach (var obj in myCombo.Items)
            {
                label1.Text = obj.ToString();
                temp = label1.PreferredWidth;
                if (temp > maxWidth)
                {
                    maxWidth = temp;
                }
            }
            label1.Dispose();
            return maxWidth;
        }
    }
}
