using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.BLL.SXXK.ThanhKhoan
{
    public partial class NPLNhapTon_TH : EntityBase
    {
        #region Private members.

        protected int _LanThanhLy;
        protected int _SoToKhai;
        protected string _MaLoaiHinh = String.Empty;
        protected short _NamDangKy;
        protected string _MaHaiQuan = String.Empty;
        protected string _MaPhu = String.Empty;
        protected int _LanDieuChinh;
        protected string _MaDV = String.Empty;
        protected string _MaNPL = String.Empty;
        protected decimal _THUE_XNK;
        protected decimal _THUE_TTDB;
        protected decimal _THUE_VAT;
        protected decimal _THUE_CLGIA;
        protected decimal _TON_DAU_XNK;
        protected decimal _TON_DAU_TTDB;
        protected decimal _TON_DAU_VAT;
        protected decimal _TON_DAU_CLGIA;
        protected decimal _TON_XNK;
        protected decimal _TON_TTDB;
        protected decimal _TON_VAT;
        protected decimal _TON_CLGIA;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public int LanThanhLy
        {
            set { this._LanThanhLy = value; }
            get { return this._LanThanhLy; }
        }
        public int SoToKhai
        {
            set { this._SoToKhai = value; }
            get { return this._SoToKhai; }
        }
        public string MaLoaiHinh
        {
            set { this._MaLoaiHinh = value; }
            get { return this._MaLoaiHinh; }
        }
        public short NamDangKy
        {
            set { this._NamDangKy = value; }
            get { return this._NamDangKy; }
        }
        public string MaHaiQuan
        {
            set { this._MaHaiQuan = value; }
            get { return this._MaHaiQuan; }
        }
        public string MaPhu
        {
            set { this._MaPhu = value; }
            get { return this._MaPhu; }
        }
        public int LanDieuChinh
        {
            set { this._LanDieuChinh = value; }
            get { return this._LanDieuChinh; }
        }
        public string MaDV
        {
            set { this._MaDV = value; }
            get { return this._MaDV; }
        }
        public string MaNPL
        {
            set { this._MaNPL = value; }
            get { return this._MaNPL; }
        }
        public decimal THUE_XNK
        {
            set { this._THUE_XNK = value; }
            get { return this._THUE_XNK; }
        }
        public decimal THUE_TTDB
        {
            set { this._THUE_TTDB = value; }
            get { return this._THUE_TTDB; }
        }
        public decimal THUE_VAT
        {
            set { this._THUE_VAT = value; }
            get { return this._THUE_VAT; }
        }
        public decimal THUE_CLGIA
        {
            set { this._THUE_CLGIA = value; }
            get { return this._THUE_CLGIA; }
        }
        public decimal TON_DAU_XNK
        {
            set { this._TON_DAU_XNK = value; }
            get { return this._TON_DAU_XNK; }
        }
        public decimal TON_DAU_TTDB
        {
            set { this._TON_DAU_TTDB = value; }
            get { return this._TON_DAU_TTDB; }
        }
        public decimal TON_DAU_VAT
        {
            set { this._TON_DAU_VAT = value; }
            get { return this._TON_DAU_VAT; }
        }
        public decimal TON_DAU_CLGIA
        {
            set { this._TON_DAU_CLGIA = value; }
            get { return this._TON_DAU_CLGIA; }
        }
        public decimal TON_XNK
        {
            set { this._TON_XNK = value; }
            get { return this._TON_XNK; }
        }
        public decimal TON_TTDB
        {
            set { this._TON_TTDB = value; }
            get { return this._TON_TTDB; }
        }
        public decimal TON_VAT
        {
            set { this._TON_VAT = value; }
            get { return this._TON_VAT; }
        }
        public decimal TON_CLGIA
        {
            set { this._TON_CLGIA = value; }
            get { return this._TON_CLGIA; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TH_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);
            this.db.AddInParameter(dbCommand, "@MaDV", SqlDbType.VarChar, this._MaDV);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) this._LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) this._MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinh"))) this._LanDieuChinh = reader.GetInt32(reader.GetOrdinal("LanDieuChinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDV"))) this._MaDV = reader.GetString(reader.GetOrdinal("MaDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_XNK"))) this._THUE_XNK = reader.GetDecimal(reader.GetOrdinal("THUE_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_TTDB"))) this._THUE_TTDB = reader.GetDecimal(reader.GetOrdinal("THUE_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_VAT"))) this._THUE_VAT = reader.GetDecimal(reader.GetOrdinal("THUE_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_CLGIA"))) this._THUE_CLGIA = reader.GetDecimal(reader.GetOrdinal("THUE_CLGIA"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_XNK"))) this._TON_DAU_XNK = reader.GetDecimal(reader.GetOrdinal("TON_DAU_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_TTDB"))) this._TON_DAU_TTDB = reader.GetDecimal(reader.GetOrdinal("TON_DAU_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_VAT"))) this._TON_DAU_VAT = reader.GetDecimal(reader.GetOrdinal("TON_DAU_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_CLGIA"))) this._TON_DAU_CLGIA = reader.GetDecimal(reader.GetOrdinal("TON_DAU_CLGIA"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_XNK"))) this._TON_XNK = reader.GetDecimal(reader.GetOrdinal("TON_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_TTDB"))) this._TON_TTDB = reader.GetDecimal(reader.GetOrdinal("TON_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_VAT"))) this._TON_VAT = reader.GetDecimal(reader.GetOrdinal("TON_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_CLGIA"))) this._TON_CLGIA = reader.GetDecimal(reader.GetOrdinal("TON_CLGIA"));
                return true;
            }
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public NPLNhapTon_THCollection SelectCollectionBy_LanThanhLy_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan_AND_MaPhu_AND_MaDV()
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TH_SelectBy_LanThanhLy_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan_AND_MaPhu_AND_MaDV";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            this.db.AddInParameter(dbCommand, "@MaDV", SqlDbType.VarChar, this._MaDV);

            NPLNhapTon_THCollection collection = new NPLNhapTon_THCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                NPLNhapTon_TH entity = new NPLNhapTon_TH();
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinh"))) entity.LanDieuChinh = reader.GetInt32(reader.GetOrdinal("LanDieuChinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDV"))) entity.MaDV = reader.GetString(reader.GetOrdinal("MaDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_XNK"))) entity.THUE_XNK = reader.GetDecimal(reader.GetOrdinal("THUE_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_TTDB"))) entity.THUE_TTDB = reader.GetDecimal(reader.GetOrdinal("THUE_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_VAT"))) entity.THUE_VAT = reader.GetDecimal(reader.GetOrdinal("THUE_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_CLGIA"))) entity.THUE_CLGIA = reader.GetDecimal(reader.GetOrdinal("THUE_CLGIA"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_XNK"))) entity.TON_DAU_XNK = reader.GetDecimal(reader.GetOrdinal("TON_DAU_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_TTDB"))) entity.TON_DAU_TTDB = reader.GetDecimal(reader.GetOrdinal("TON_DAU_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_VAT"))) entity.TON_DAU_VAT = reader.GetDecimal(reader.GetOrdinal("TON_DAU_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_CLGIA"))) entity.TON_DAU_CLGIA = reader.GetDecimal(reader.GetOrdinal("TON_DAU_CLGIA"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_XNK"))) entity.TON_XNK = reader.GetDecimal(reader.GetOrdinal("TON_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_TTDB"))) entity.TON_TTDB = reader.GetDecimal(reader.GetOrdinal("TON_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_VAT"))) entity.TON_VAT = reader.GetDecimal(reader.GetOrdinal("TON_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_CLGIA"))) entity.TON_CLGIA = reader.GetDecimal(reader.GetOrdinal("TON_CLGIA"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_LanThanhLy_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan_AND_MaPhu_AND_MaDV()
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TH_SelectBy_LanThanhLy_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan_AND_MaPhu_AND_MaDV";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            this.db.AddInParameter(dbCommand, "@MaDV", SqlDbType.VarChar, this._MaDV);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public DataSet SelectAll()
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TH_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TH_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TH_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TH_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public NPLNhapTon_THCollection SelectCollectionAll()
        {
            NPLNhapTon_THCollection collection = new NPLNhapTon_THCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                NPLNhapTon_TH entity = new NPLNhapTon_TH();

                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinh"))) entity.LanDieuChinh = reader.GetInt32(reader.GetOrdinal("LanDieuChinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDV"))) entity.MaDV = reader.GetString(reader.GetOrdinal("MaDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_XNK"))) entity.THUE_XNK = reader.GetDecimal(reader.GetOrdinal("THUE_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_TTDB"))) entity.THUE_TTDB = reader.GetDecimal(reader.GetOrdinal("THUE_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_VAT"))) entity.THUE_VAT = reader.GetDecimal(reader.GetOrdinal("THUE_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_CLGIA"))) entity.THUE_CLGIA = reader.GetDecimal(reader.GetOrdinal("THUE_CLGIA"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_XNK"))) entity.TON_DAU_XNK = reader.GetDecimal(reader.GetOrdinal("TON_DAU_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_TTDB"))) entity.TON_DAU_TTDB = reader.GetDecimal(reader.GetOrdinal("TON_DAU_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_VAT"))) entity.TON_DAU_VAT = reader.GetDecimal(reader.GetOrdinal("TON_DAU_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_CLGIA"))) entity.TON_DAU_CLGIA = reader.GetDecimal(reader.GetOrdinal("TON_DAU_CLGIA"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_XNK"))) entity.TON_XNK = reader.GetDecimal(reader.GetOrdinal("TON_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_TTDB"))) entity.TON_TTDB = reader.GetDecimal(reader.GetOrdinal("TON_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_VAT"))) entity.TON_VAT = reader.GetDecimal(reader.GetOrdinal("TON_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_CLGIA"))) entity.TON_CLGIA = reader.GetDecimal(reader.GetOrdinal("TON_CLGIA"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public NPLNhapTon_THCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            NPLNhapTon_THCollection collection = new NPLNhapTon_THCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                NPLNhapTon_TH entity = new NPLNhapTon_TH();

                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinh"))) entity.LanDieuChinh = reader.GetInt32(reader.GetOrdinal("LanDieuChinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDV"))) entity.MaDV = reader.GetString(reader.GetOrdinal("MaDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_XNK"))) entity.THUE_XNK = reader.GetDecimal(reader.GetOrdinal("THUE_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_TTDB"))) entity.THUE_TTDB = reader.GetDecimal(reader.GetOrdinal("THUE_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_VAT"))) entity.THUE_VAT = reader.GetDecimal(reader.GetOrdinal("THUE_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_CLGIA"))) entity.THUE_CLGIA = reader.GetDecimal(reader.GetOrdinal("THUE_CLGIA"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_XNK"))) entity.TON_DAU_XNK = reader.GetDecimal(reader.GetOrdinal("TON_DAU_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_TTDB"))) entity.TON_DAU_TTDB = reader.GetDecimal(reader.GetOrdinal("TON_DAU_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_VAT"))) entity.TON_DAU_VAT = reader.GetDecimal(reader.GetOrdinal("TON_DAU_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_DAU_CLGIA"))) entity.TON_DAU_CLGIA = reader.GetDecimal(reader.GetOrdinal("TON_DAU_CLGIA"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_XNK"))) entity.TON_XNK = reader.GetDecimal(reader.GetOrdinal("TON_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_TTDB"))) entity.TON_TTDB = reader.GetDecimal(reader.GetOrdinal("TON_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_VAT"))) entity.TON_VAT = reader.GetDecimal(reader.GetOrdinal("TON_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TON_CLGIA"))) entity.TON_CLGIA = reader.GetDecimal(reader.GetOrdinal("TON_CLGIA"));
                collection.Add(entity);
            }
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TH_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);
            this.db.AddInParameter(dbCommand, "@MaDV", SqlDbType.VarChar, this._MaDV);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@THUE_XNK", SqlDbType.Money, this._THUE_XNK);
            this.db.AddInParameter(dbCommand, "@THUE_TTDB", SqlDbType.Money, this._THUE_TTDB);
            this.db.AddInParameter(dbCommand, "@THUE_VAT", SqlDbType.Money, this._THUE_VAT);
            this.db.AddInParameter(dbCommand, "@THUE_CLGIA", SqlDbType.Money, this._THUE_CLGIA);
            this.db.AddInParameter(dbCommand, "@TON_DAU_XNK", SqlDbType.Money, this._TON_DAU_XNK);
            this.db.AddInParameter(dbCommand, "@TON_DAU_TTDB", SqlDbType.Money, this._TON_DAU_TTDB);
            this.db.AddInParameter(dbCommand, "@TON_DAU_VAT", SqlDbType.Money, this._TON_DAU_VAT);
            this.db.AddInParameter(dbCommand, "@TON_DAU_CLGIA", SqlDbType.Money, this._TON_DAU_CLGIA);
            this.db.AddInParameter(dbCommand, "@TON_XNK", SqlDbType.Money, this._TON_XNK);
            this.db.AddInParameter(dbCommand, "@TON_TTDB", SqlDbType.Money, this._TON_TTDB);
            this.db.AddInParameter(dbCommand, "@TON_VAT", SqlDbType.Money, this._TON_VAT);
            this.db.AddInParameter(dbCommand, "@TON_CLGIA", SqlDbType.Money, this._TON_CLGIA);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(NPLNhapTon_THCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NPLNhapTon_TH item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, NPLNhapTon_THCollection collection)
        {
            foreach (NPLNhapTon_TH item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TH_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);
            this.db.AddInParameter(dbCommand, "@MaDV", SqlDbType.VarChar, this._MaDV);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@THUE_XNK", SqlDbType.Money, this._THUE_XNK);
            this.db.AddInParameter(dbCommand, "@THUE_TTDB", SqlDbType.Money, this._THUE_TTDB);
            this.db.AddInParameter(dbCommand, "@THUE_VAT", SqlDbType.Money, this._THUE_VAT);
            this.db.AddInParameter(dbCommand, "@THUE_CLGIA", SqlDbType.Money, this._THUE_CLGIA);
            this.db.AddInParameter(dbCommand, "@TON_DAU_XNK", SqlDbType.Money, this._TON_DAU_XNK);
            this.db.AddInParameter(dbCommand, "@TON_DAU_TTDB", SqlDbType.Money, this._TON_DAU_TTDB);
            this.db.AddInParameter(dbCommand, "@TON_DAU_VAT", SqlDbType.Money, this._TON_DAU_VAT);
            this.db.AddInParameter(dbCommand, "@TON_DAU_CLGIA", SqlDbType.Money, this._TON_DAU_CLGIA);
            this.db.AddInParameter(dbCommand, "@TON_XNK", SqlDbType.Money, this._TON_XNK);
            this.db.AddInParameter(dbCommand, "@TON_TTDB", SqlDbType.Money, this._TON_TTDB);
            this.db.AddInParameter(dbCommand, "@TON_VAT", SqlDbType.Money, this._TON_VAT);
            this.db.AddInParameter(dbCommand, "@TON_CLGIA", SqlDbType.Money, this._TON_CLGIA);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(NPLNhapTon_THCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NPLNhapTon_TH item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TH_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);
            this.db.AddInParameter(dbCommand, "@MaDV", SqlDbType.VarChar, this._MaDV);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@THUE_XNK", SqlDbType.Money, this._THUE_XNK);
            this.db.AddInParameter(dbCommand, "@THUE_TTDB", SqlDbType.Money, this._THUE_TTDB);
            this.db.AddInParameter(dbCommand, "@THUE_VAT", SqlDbType.Money, this._THUE_VAT);
            this.db.AddInParameter(dbCommand, "@THUE_CLGIA", SqlDbType.Money, this._THUE_CLGIA);
            this.db.AddInParameter(dbCommand, "@TON_DAU_XNK", SqlDbType.Money, this._TON_DAU_XNK);
            this.db.AddInParameter(dbCommand, "@TON_DAU_TTDB", SqlDbType.Money, this._TON_DAU_TTDB);
            this.db.AddInParameter(dbCommand, "@TON_DAU_VAT", SqlDbType.Money, this._TON_DAU_VAT);
            this.db.AddInParameter(dbCommand, "@TON_DAU_CLGIA", SqlDbType.Money, this._TON_DAU_CLGIA);
            this.db.AddInParameter(dbCommand, "@TON_XNK", SqlDbType.Money, this._TON_XNK);
            this.db.AddInParameter(dbCommand, "@TON_TTDB", SqlDbType.Money, this._TON_TTDB);
            this.db.AddInParameter(dbCommand, "@TON_VAT", SqlDbType.Money, this._TON_VAT);
            this.db.AddInParameter(dbCommand, "@TON_CLGIA", SqlDbType.Money, this._TON_CLGIA);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(NPLNhapTon_THCollection collection, SqlTransaction transaction)
        {
            foreach (NPLNhapTon_TH item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TH_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);
            this.db.AddInParameter(dbCommand, "@MaDV", SqlDbType.VarChar, this._MaDV);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(NPLNhapTon_THCollection collection, SqlTransaction transaction)
        {
            foreach (NPLNhapTon_TH item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(NPLNhapTon_THCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (NPLNhapTon_TH item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #endregion

        public static void Updatebase(DataSet ds, SqlTransaction transaction)
        {
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                NPLNhapTon_TH nplNhapTonTH = new NPLNhapTon_TH();
                nplNhapTonTH.LanDieuChinh = Convert.ToInt32(row["LAN_DC"].ToString());
                nplNhapTonTH.LanThanhLy = Convert.ToInt32(row["LAN_TL"].ToString());
                nplNhapTonTH.MaDV = (row["MA_DV"].ToString());
                nplNhapTonTH.MaHaiQuan = (row["MA_HQ"].ToString());
                nplNhapTonTH.MaLoaiHinh = (row["MA_LH"].ToString());
                nplNhapTonTH.MaNPL = (row["MA_NPL"].ToString());
                nplNhapTonTH.MaPhu = (row["MA_PHU"].ToString());
                nplNhapTonTH.NamDangKy = Convert.ToInt16(row["NAMDK"].ToString());
                nplNhapTonTH.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                nplNhapTonTH.THUE_CLGIA = Convert.ToDecimal(row["THUE_CLGIA"]);
                nplNhapTonTH.THUE_TTDB = Convert.ToDecimal(row["THUE_TTDB"]);
                nplNhapTonTH.THUE_VAT = Convert.ToDecimal(row["THUE_VAT"]);
                nplNhapTonTH.THUE_XNK = Convert.ToDecimal(row["THUE_XNK"]);
                nplNhapTonTH.TON_CLGIA = Convert.ToDecimal(row["TON_CLGIA"]);
                nplNhapTonTH.TON_DAU_CLGIA = Convert.ToDecimal(row["TON_DAU_CLGIA"]);
                nplNhapTonTH.TON_DAU_TTDB = Convert.ToDecimal(row["TON_DAU_TTDB"]);
                nplNhapTonTH.TON_DAU_VAT = Convert.ToDecimal(row["TON_DAU_VAT"]);
                nplNhapTonTH.TON_DAU_XNK = Convert.ToDecimal(row["TON_DAU_XNK"]);
                nplNhapTonTH.TON_TTDB = Convert.ToDecimal(row["TON_TTDB"]);
                nplNhapTonTH.TON_VAT = Convert.ToDecimal(row["TON_VAT"]);
                nplNhapTonTH.TON_XNK = Convert.ToDecimal(row["TON_XNK"]);
                nplNhapTonTH.InsertUpdateTransaction(transaction);
            }
        }

    }
}