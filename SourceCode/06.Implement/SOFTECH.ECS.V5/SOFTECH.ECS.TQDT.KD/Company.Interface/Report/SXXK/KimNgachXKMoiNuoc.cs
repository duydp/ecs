using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace Company.Interface.Report.SXXK
{
    public partial class KimNgachXKMoiNuoc : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DateTime tungay;
        public DateTime denngay;
        public string NguoiLapBieu;
        public string NguoiDuyetBieu;
        public string DonViBaoCao;
        public KimNgachXKMoiNuoc()
        {
            InitializeComponent();
        }

        public void BindData() {
            GroupField groupItem = new GroupField("Ten");           
            this.GroupHeader1.GroupFields.Add(groupItem);
            this.lblGroup.DataBindings.Add("Text", this.DataSource, "Ten");
            lblGroup.Text.ToUpper();

            lblSoToKhai.DataBindings.Add("Text", this.DataSource,"TenHang");
            lblSoToKhai.Text.ToUpper();                       
            lblMaHaiQuan.DataBindings.Add("Text", this.DataSource, "DonGiaKB","{0:N2}");
            lblNgayDangKy.DataBindings.Add("Text", this.DataSource, "NgayDangKy","{0:dd/MM/yyyy}");
            lblTriGiaKB.DataBindings.Add("Text", this.DataSource, "TongSoLuong", "{0:N2}");
            lblTriGiaTT.DataBindings.Add("Text", this.DataSource, "TongTriGia", "{0:N2}");
            lblNguyenTe.DataBindings.Add("Text", this.DataSource, "NguyenTe_ID");            
            DateTime today = new DateTime();
            today = DateTime.Now;
            lbNgay.Text = "Từ ngày   " + tungay.ToString("dd/MM/yyyy") + "   Đến ngày   " + denngay.ToString("dd/MM/yyyy");
            lblNgayKy.Text = "..........., ngày........tháng...... năm........ "; ;
            lblNguoiLapBieu.Text = this.NguoiLapBieu.ToUpper();
            lblNguoiDuyetBieu.Text = this.NguoiDuyetBieu.ToUpper();
            lblTenNganDN.Text = "ĐVBC : " + this.DonViBaoCao.ToUpper();

            lblTenDN.Text = GlobalSettings.TEN_DON_VI.ToUpper();
            lblMaDN.Text = "Mã số : " + GlobalSettings.MA_DON_VI;
            

        }

    }
}
