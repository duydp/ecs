//using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Collections.Generic;
using System;

namespace Company.KD.BLL.KDT
{
    public partial class HangMauDich
    {
        public string NhomHang { get; set; }
        public string Ma { get; set; }
        public string Ten { get; set; }
        public string MaHSMoi { get; set; }

        public void TinhThue(decimal tygiaTT)
        {
            //decimal dongia_TT = this._DonGiaKB*tygiaTT;
            //decimal trigiaNT = this._DonGiaKB*this._SoLuong;
            ////decimal trigiaTT_XNK = (dongia_TT*this._SoLuong) + (this._ASEAN_KhoanPhaiCong*tygiaTT) - (this._ASEAN_KhoanPhaiTru*tygiaTT);
            //decimal trigiaTT_XNK = this.TriGiaTT;
            //decimal tienthue_XNK = trigiaTT_XNK*this._ThueSuatXNK/100;
            //decimal tienthue_TTDB = (trigiaTT_XNK + tienthue_XNK)*this._ThueSuatTTDB/100;
            //decimal tienthue_GTGT = (trigiaTT_XNK + tienthue_XNK + tienthue_TTDB)*this._ThueSuatGTGT/100;
            //decimal tienthukhac = this._TyLeThuKhac*trigiaTT_XNK/100;


            //this._DonGiaTT = dongia_TT;
            //this._TriGiaKB = trigiaNT;
            //this._TriGiaTT = trigiaTT_XNK;
            //this._TriGiaKB_VND = trigiaNT*tygiaTT;
            //this._ThueXNK = tienthue_XNK;
            //this._ThueTTDB = tienthue_TTDB;
            //this._ThueGTGT = tienthue_GTGT;
            //this._TriGiaThuKhac = tienthukhac;
        }

        //private bool _FOC = false;

        //public bool FOC
        //{
        //    set { this._FOC = value; }
        //    get { return this._FOC; }
        //}

        //private bool _ThueTuyetDoi = false;

        //public bool ThueTuyetDoi
        //{
        //    set { this._ThueTuyetDoi = value; }
        //    get { return this._ThueTuyetDoi; }
        //}


        //-----------------------------------------------------------------------------------------

        public static long checkHMDbyMaHang(string MaHang)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_SelectBy_MaHang]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.NVarChar, MaHang);
            HangMauDich entity = new HangMauDich();
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDouble(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDouble(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDouble(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDouble(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDouble(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDouble(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDouble(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDouble(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDouble(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDouble(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDouble(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDouble(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDouble(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("FOC"))) entity.FOC = reader.GetBoolean(reader.GetOrdinal("FOC"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTuyetDoi"))) entity.ThueTuyetDoi = reader.GetBoolean(reader.GetOrdinal("ThueTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetString(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetString(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetString(reader.GetOrdinal("ThueSuatVATGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTuyetDoi"))) entity.DonGiaTuyetDoi = reader.GetDouble(reader.GetOrdinal("DonGiaTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHSMoRong"))) entity.MaHSMoRong = reader.GetString(reader.GetOrdinal("MaHSMoRong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhanHieu"))) entity.NhanHieu = reader.GetString(reader.GetOrdinal("NhanHieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuyCachPhamChat"))) entity.QuyCachPhamChat = reader.GetString(reader.GetOrdinal("QuyCachPhamChat"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhPhan"))) entity.ThanhPhan = reader.GetString(reader.GetOrdinal("ThanhPhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("Model"))) entity.Model = reader.GetString(reader.GetOrdinal("Model"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangSX"))) entity.MaHangSX = reader.GetString(reader.GetOrdinal("MaHangSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangSX"))) entity.TenHangSX = reader.GetString(reader.GetOrdinal("TenHangSX"));
            }
            reader.Close();
            if (entity == null) return 0;
            else
                return entity.ID;
        }

        public static List<HangMauDich> SelectHang(string MaHQ, string maDN, string maLoaiHinh)
        {
            List<HangMauDich> collection = new List<HangMauDich>();

            try
            {
                const string spName = "p_KD_SelectHang";

                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

                db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHQ);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDN);
                db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, maLoaiHinh);

                SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
                while (reader.Read())
                {
                    HangMauDich entity = new HangMauDich();
                    if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                    if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                    if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                    if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                    entity.MaHSMoi = string.Empty;
                    collection.Add(entity);
                }
                reader.Close();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return collection;
        }

    }


}