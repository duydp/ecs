-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_SelectBy_DinhMucThucTeSP_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_SelectBy_DinhMucThucTeSP_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_DeleteBy_DinhMucThucTeSP_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_DeleteBy_DinhMucThucTeSP_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Insert]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Insert]
	@DinhMucThucTeSP_ID bigint,
	@STT int,
	@MaSanPham varchar(50),
	@TenSanPham nvarchar(255),
	@DVT_SP char(3),
	@MaNPL nvarchar(50),
	@TenNPL nvarchar(255),
	@DVT_NPL char(3),
	@MaHS varchar(12),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@NPL_TuCungUng numeric(18, 6),
	@GhiChu nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_DinhMucThucTe_DinhMuc]
(
	[DinhMucThucTeSP_ID],
	[STT],
	[MaSanPham],
	[TenSanPham],
	[DVT_SP],
	[MaNPL],
	[TenNPL],
	[DVT_NPL],
	[MaHS],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[NPL_TuCungUng],
	[GhiChu]
)
VALUES 
(
	@DinhMucThucTeSP_ID,
	@STT,
	@MaSanPham,
	@TenSanPham,
	@DVT_SP,
	@MaNPL,
	@TenNPL,
	@DVT_NPL,
	@MaHS,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@NPL_TuCungUng,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Update]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Update]
	@ID bigint,
	@DinhMucThucTeSP_ID bigint,
	@STT int,
	@MaSanPham varchar(50),
	@TenSanPham nvarchar(255),
	@DVT_SP char(3),
	@MaNPL nvarchar(50),
	@TenNPL nvarchar(255),
	@DVT_NPL char(3),
	@MaHS varchar(12),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@NPL_TuCungUng numeric(18, 6),
	@GhiChu nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_SXXK_DinhMucThucTe_DinhMuc]
SET
	[DinhMucThucTeSP_ID] = @DinhMucThucTeSP_ID,
	[STT] = @STT,
	[MaSanPham] = @MaSanPham,
	[TenSanPham] = @TenSanPham,
	[DVT_SP] = @DVT_SP,
	[MaNPL] = @MaNPL,
	[TenNPL] = @TenNPL,
	[DVT_NPL] = @DVT_NPL,
	[MaHS] = @MaHS,
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[NPL_TuCungUng] = @NPL_TuCungUng,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_InsertUpdate]
	@ID bigint,
	@DinhMucThucTeSP_ID bigint,
	@STT int,
	@MaSanPham varchar(50),
	@TenSanPham nvarchar(255),
	@DVT_SP char(3),
	@MaNPL nvarchar(50),
	@TenNPL nvarchar(255),
	@DVT_NPL char(3),
	@MaHS varchar(12),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@NPL_TuCungUng numeric(18, 6),
	@GhiChu nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_DinhMucThucTe_DinhMuc] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_DinhMucThucTe_DinhMuc] 
		SET
			[DinhMucThucTeSP_ID] = @DinhMucThucTeSP_ID,
			[STT] = @STT,
			[MaSanPham] = @MaSanPham,
			[TenSanPham] = @TenSanPham,
			[DVT_SP] = @DVT_SP,
			[MaNPL] = @MaNPL,
			[TenNPL] = @TenNPL,
			[DVT_NPL] = @DVT_NPL,
			[MaHS] = @MaHS,
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[NPL_TuCungUng] = @NPL_TuCungUng,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_DinhMucThucTe_DinhMuc]
		(
			[DinhMucThucTeSP_ID],
			[STT],
			[MaSanPham],
			[TenSanPham],
			[DVT_SP],
			[MaNPL],
			[TenNPL],
			[DVT_NPL],
			[MaHS],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[NPL_TuCungUng],
			[GhiChu]
		)
		VALUES 
		(
			@DinhMucThucTeSP_ID,
			@STT,
			@MaSanPham,
			@TenSanPham,
			@DVT_SP,
			@MaNPL,
			@TenNPL,
			@DVT_NPL,
			@MaHS,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@NPL_TuCungUng,
			@GhiChu
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Delete]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_DinhMucThucTe_DinhMuc]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_DeleteBy_DinhMucThucTeSP_ID]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_DeleteBy_DinhMucThucTeSP_ID]
	@DinhMucThucTeSP_ID bigint
AS

DELETE FROM [dbo].[t_KDT_SXXK_DinhMucThucTe_DinhMuc]
WHERE
	[DinhMucThucTeSP_ID] = @DinhMucThucTeSP_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_DinhMucThucTe_DinhMuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Load]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[DinhMucThucTeSP_ID],
	[STT],
	[MaSanPham],
	[TenSanPham],
	[DVT_SP],
	[MaNPL],
	[TenNPL],
	[DVT_NPL],
	[MaHS],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[NPL_TuCungUng],
	[GhiChu]
FROM
	[dbo].[t_KDT_SXXK_DinhMucThucTe_DinhMuc]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_SelectBy_DinhMucThucTeSP_ID]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_SelectBy_DinhMucThucTeSP_ID]
	@DinhMucThucTeSP_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[DinhMucThucTeSP_ID],
	[STT],
	[MaSanPham],
	[TenSanPham],
	[DVT_SP],
	[MaNPL],
	[TenNPL],
	[DVT_NPL],
	[MaHS],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[NPL_TuCungUng],
	[GhiChu]
FROM
	[dbo].[t_KDT_SXXK_DinhMucThucTe_DinhMuc]
WHERE
	[DinhMucThucTeSP_ID] = @DinhMucThucTeSP_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[DinhMucThucTeSP_ID],
	[STT],
	[MaSanPham],
	[TenSanPham],
	[DVT_SP],
	[MaNPL],
	[TenNPL],
	[DVT_NPL],
	[MaHS],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[NPL_TuCungUng],
	[GhiChu]
FROM [dbo].[t_KDT_SXXK_DinhMucThucTe_DinhMuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucThucTe_DinhMuc_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[DinhMucThucTeSP_ID],
	[STT],
	[MaSanPham],
	[TenSanPham],
	[DVT_SP],
	[MaNPL],
	[TenNPL],
	[DVT_NPL],
	[MaHS],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[NPL_TuCungUng],
	[GhiChu]
FROM
	[dbo].[t_KDT_SXXK_DinhMucThucTe_DinhMuc]	

GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '42.3') = 0
	BEGIN
		INSERT INTO dbo.t_HaiQuan_Version VALUES('42.3',GETDATE(), N'CẬP NHẬT PROC t_KDT_SXXK_DinhMucThucTe_DinhMuc')
	END	