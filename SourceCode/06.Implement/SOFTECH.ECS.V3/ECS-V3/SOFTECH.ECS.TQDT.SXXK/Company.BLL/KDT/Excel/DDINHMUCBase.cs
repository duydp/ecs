using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.ExportToExcel
{
	public partial class DDINHMUC 
	{
		#region Private members.
		
		protected string _MA_SP = String.Empty;
		protected string _MA_NPL = String.Empty;
		protected string _MA_DV = String.Empty;
		protected string _MA_HQ = String.Empty;
		protected decimal _DM_SD;
		protected decimal _TL_HH;
		protected decimal _DM_CHUNG;
		protected string _GHI_CHU = String.Empty;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public string MA_SP
		{
			set {this._MA_SP = value;}
			get {return this._MA_SP;}
		}
		public string MA_NPL
		{
			set {this._MA_NPL = value;}
			get {return this._MA_NPL;}
		}
		public string MA_DV
		{
			set {this._MA_DV = value;}
			get {return this._MA_DV;}
		}
		public string MA_HQ
		{
			set {this._MA_HQ = value;}
			get {return this._MA_HQ;}
		}
		public decimal DM_SD
		{
			set {this._DM_SD = value;}
			get {return this._DM_SD;}
		}
		public decimal TL_HH
		{
			set {this._TL_HH = value;}
			get {return this._TL_HH;}
		}
		public decimal DM_CHUNG
		{
			set {this._DM_CHUNG = value;}
			get {return this._DM_CHUNG;}
		}
		public string GHI_CHU
		{
			set {this._GHI_CHU = value;}
			get {return this._GHI_CHU;}
		}
		
		//---------------------------------------------------------------------------------------------
        
		
		#endregion
		
		//---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string maHaiQuan, string maDoanhNghiep)
        {
            //string sql = "SELECT * FROM DDINHMUC  WHERE Ma_HQ = @MaHQ AND MA_DV =@MaDV";
            string sql = "SELECT * FROM t_SXXK_DinhMuc  WHERE MaHaiQuan = @MaHQ AND MaDoanhNghiep =@MaDV";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@MaDV", SqlDbType.VarChar, maDoanhNghiep);

            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
	}	
}