using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.GC.BLL.SXXK.ToKhai
{
	public partial class HangMauDichDieuChinhCT : EntityBase
	{
		#region Private members.
		
		protected int _SoToKhai;
		protected string _MaLoaiHinh = String.Empty;
		protected string _MaHaiQuan = String.Empty;
		protected short _NamDangKy;
		protected string _MaHang = String.Empty;
		protected int _LanDieuChinh;
		protected int _LanDieuChinhCT;
		protected double _THUE_XNK;
		protected double _THUE_TTDB;
		protected double _THUE_VAT;
		protected double _PHU_THU;
		protected byte _MIENTHUE;
		protected double _TRIGIA_THUKHAC;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public int SoToKhai
		{
			set {this._SoToKhai = value;}
			get {return this._SoToKhai;}
		}
		public string MaLoaiHinh
		{
			set {this._MaLoaiHinh = value;}
			get {return this._MaLoaiHinh;}
		}
		public string MaHaiQuan
		{
            set { this._MaHaiQuan = value.Trim(); }
            get { return this._MaHaiQuan.Trim(); }
		}
		public short NamDangKy
		{
			set {this._NamDangKy = value;}
			get {return this._NamDangKy;}
		}
		public string MaHang
		{
			set {this._MaHang = value;}
			get {return this._MaHang;}
		}
		public int LanDieuChinh
		{
			set {this._LanDieuChinh = value;}
			get {return this._LanDieuChinh;}
		}
		public int LanDieuChinhCT
		{
			set {this._LanDieuChinhCT = value;}
			get {return this._LanDieuChinhCT;}
		}
		public double THUE_XNK
		{
			set {this._THUE_XNK = value;}
			get {return this._THUE_XNK;}
		}
		public double THUE_TTDB
		{
			set {this._THUE_TTDB = value;}
			get {return this._THUE_TTDB;}
		}
		public double THUE_VAT
		{
			set {this._THUE_VAT = value;}
			get {return this._THUE_VAT;}
		}
		public double PHU_THU
		{
			set {this._PHU_THU = value;}
			get {return this._PHU_THU;}
		}
		public byte MIENTHUE
		{
			set {this._MIENTHUE = value;}
			get {return this._MIENTHUE;}
		}
		public double TRIGIA_THUKHAC
		{
			set {this._TRIGIA_THUKHAC = value;}
			get {return this._TRIGIA_THUKHAC;}
		}
		
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public bool Load()
		{
			string spName = "p_SXXK_HangMauDichDieuChinhCT_Load";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
			this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);
			this.db.AddInParameter(dbCommand, "@LanDieuChinhCT", SqlDbType.Int, this._LanDieuChinhCT);
			
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) this._MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinh"))) this._LanDieuChinh = reader.GetInt32(reader.GetOrdinal("LanDieuChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinhCT"))) this._LanDieuChinhCT = reader.GetInt32(reader.GetOrdinal("LanDieuChinhCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("THUE_XNK"))) this._THUE_XNK = reader.GetDouble(reader.GetOrdinal("THUE_XNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("THUE_TTDB"))) this._THUE_TTDB = reader.GetDouble(reader.GetOrdinal("THUE_TTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("THUE_VAT"))) this._THUE_VAT = reader.GetDouble(reader.GetOrdinal("THUE_VAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PHU_THU"))) this._PHU_THU = reader.GetDouble(reader.GetOrdinal("PHU_THU"));
				if (!reader.IsDBNull(reader.GetOrdinal("MIENTHUE"))) this._MIENTHUE = reader.GetByte(reader.GetOrdinal("MIENTHUE"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_THUKHAC"))) this._TRIGIA_THUKHAC = reader.GetDouble(reader.GetOrdinal("TRIGIA_THUKHAC"));
				return true;
			}
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------
        public HangMauDichDieuChinhCTCollection SelectCollectionBy_SoToKhai_AND_MaLoaiHinh_AND_MaHaiQuan_AND_NamDangKy_AND_LanDieuChinh()
        {
            string spName = "p_SXXK_HangMauDichDieuChinhCT_SelectBy_SoToKhai_AND_MaLoaiHinh_AND_MaHaiQuan_AND_NamDangKy_AND_LanDieuChinh";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);

            HangMauDichDieuChinhCTCollection collection = new HangMauDichDieuChinhCTCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                HangMauDichDieuChinhCT entity = new HangMauDichDieuChinhCT();
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinh"))) entity.LanDieuChinh = reader.GetInt32(reader.GetOrdinal("LanDieuChinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinhCT"))) entity.LanDieuChinhCT = reader.GetInt32(reader.GetOrdinal("LanDieuChinhCT"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_XNK"))) entity.THUE_XNK = reader.GetDouble(reader.GetOrdinal("THUE_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_TTDB"))) entity.THUE_TTDB = reader.GetDouble(reader.GetOrdinal("THUE_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_VAT"))) entity.THUE_VAT = reader.GetDouble(reader.GetOrdinal("THUE_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PHU_THU"))) entity.PHU_THU = reader.GetDouble(reader.GetOrdinal("PHU_THU"));
                if (!reader.IsDBNull(reader.GetOrdinal("MIENTHUE"))) entity.MIENTHUE = reader.GetByte(reader.GetOrdinal("MIENTHUE"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_THUKHAC"))) entity.TRIGIA_THUKHAC = reader.GetDouble(reader.GetOrdinal("TRIGIA_THUKHAC"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------
        public HangMauDichDieuChinhCTCollection SelectCollectionBy_SoToKhai_AND_MaLoaiHinh_AND_MaHaiQuan_AND_NamDangKy_AND_MaHang_AND_LanDieuChinh()
        {
            string spName = "p_SXXK_HangMauDichDieuChinhCT_SelectBy_SoToKhai_AND_MaLoaiHinh_AND_MaHaiQuan_AND_NamDangKy_AND_MaHang_AND_LanDieuChinh";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
            this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);

            HangMauDichDieuChinhCTCollection collection = new HangMauDichDieuChinhCTCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                HangMauDichDieuChinhCT entity = new HangMauDichDieuChinhCT();
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinh"))) entity.LanDieuChinh = reader.GetInt32(reader.GetOrdinal("LanDieuChinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinhCT"))) entity.LanDieuChinhCT = reader.GetInt32(reader.GetOrdinal("LanDieuChinhCT"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_XNK"))) entity.THUE_XNK = reader.GetDouble(reader.GetOrdinal("THUE_XNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_TTDB"))) entity.THUE_TTDB = reader.GetDouble(reader.GetOrdinal("THUE_TTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("THUE_VAT"))) entity.THUE_VAT = reader.GetDouble(reader.GetOrdinal("THUE_VAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PHU_THU"))) entity.PHU_THU = reader.GetDouble(reader.GetOrdinal("PHU_THU"));
                if (!reader.IsDBNull(reader.GetOrdinal("MIENTHUE"))) entity.MIENTHUE = reader.GetByte(reader.GetOrdinal("MIENTHUE"));
                if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_THUKHAC"))) entity.TRIGIA_THUKHAC = reader.GetDouble(reader.GetOrdinal("TRIGIA_THUKHAC"));
                collection.Add(entity);
            }
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_SoToKhai_AND_MaLoaiHinh_AND_MaHaiQuan_AND_NamDangKy_AND_LanDieuChinh()
        {
            string spName = "p_SXXK_HangMauDichDieuChinhCT_SelectBy_SoToKhai_AND_MaLoaiHinh_AND_MaHaiQuan_AND_NamDangKy_AND_LanDieuChinh";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public DataSet SelectBy_SoToKhai_AND_MaLoaiHinh_AND_MaHaiQuan_AND_NamDangKy_AND_MaHang_AND_LanDieuChinh()
        {
            string spName = "p_SXXK_HangMauDichDieuChinhCT_SelectBy_SoToKhai_AND_MaLoaiHinh_AND_MaHaiQuan_AND_NamDangKy_AND_MaHang_AND_LanDieuChinh";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
            this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------



		public DataSet SelectAll()
        {
            string spName = "p_SXXK_HangMauDichDieuChinhCT_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_HangMauDichDieuChinhCT_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_SXXK_HangMauDichDieuChinhCT_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_SXXK_HangMauDichDieuChinhCT_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		public HangMauDichDieuChinhCTCollection SelectCollectionAll()
		{
			HangMauDichDieuChinhCTCollection collection = new HangMauDichDieuChinhCTCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				HangMauDichDieuChinhCT entity = new HangMauDichDieuChinhCT();
				
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinh"))) entity.LanDieuChinh = reader.GetInt32(reader.GetOrdinal("LanDieuChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinhCT"))) entity.LanDieuChinhCT = reader.GetInt32(reader.GetOrdinal("LanDieuChinhCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("THUE_XNK"))) entity.THUE_XNK = reader.GetDouble(reader.GetOrdinal("THUE_XNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("THUE_TTDB"))) entity.THUE_TTDB = reader.GetDouble(reader.GetOrdinal("THUE_TTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("THUE_VAT"))) entity.THUE_VAT = reader.GetDouble(reader.GetOrdinal("THUE_VAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PHU_THU"))) entity.PHU_THU = reader.GetDouble(reader.GetOrdinal("PHU_THU"));
				if (!reader.IsDBNull(reader.GetOrdinal("MIENTHUE"))) entity.MIENTHUE = reader.GetByte(reader.GetOrdinal("MIENTHUE"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_THUKHAC"))) entity.TRIGIA_THUKHAC = reader.GetDouble(reader.GetOrdinal("TRIGIA_THUKHAC"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public HangMauDichDieuChinhCTCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			HangMauDichDieuChinhCTCollection collection = new HangMauDichDieuChinhCTCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				HangMauDichDieuChinhCT entity = new HangMauDichDieuChinhCT();
				
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinh"))) entity.LanDieuChinh = reader.GetInt32(reader.GetOrdinal("LanDieuChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanDieuChinhCT"))) entity.LanDieuChinhCT = reader.GetInt32(reader.GetOrdinal("LanDieuChinhCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("THUE_XNK"))) entity.THUE_XNK = reader.GetDouble(reader.GetOrdinal("THUE_XNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("THUE_TTDB"))) entity.THUE_TTDB = reader.GetDouble(reader.GetOrdinal("THUE_TTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("THUE_VAT"))) entity.THUE_VAT = reader.GetDouble(reader.GetOrdinal("THUE_VAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("PHU_THU"))) entity.PHU_THU = reader.GetDouble(reader.GetOrdinal("PHU_THU"));
				if (!reader.IsDBNull(reader.GetOrdinal("MIENTHUE"))) entity.MIENTHUE = reader.GetByte(reader.GetOrdinal("MIENTHUE"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIA_THUKHAC"))) entity.TRIGIA_THUKHAC = reader.GetDouble(reader.GetOrdinal("TRIGIA_THUKHAC"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_SXXK_HangMauDichDieuChinhCT_Insert";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
			this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);
			this.db.AddInParameter(dbCommand, "@LanDieuChinhCT", SqlDbType.Int, this._LanDieuChinhCT);
			this.db.AddInParameter(dbCommand, "@THUE_XNK", SqlDbType.Float, this._THUE_XNK);
			this.db.AddInParameter(dbCommand, "@THUE_TTDB", SqlDbType.Float, this._THUE_TTDB);
			this.db.AddInParameter(dbCommand, "@THUE_VAT", SqlDbType.Float, this._THUE_VAT);
			this.db.AddInParameter(dbCommand, "@PHU_THU", SqlDbType.Float, this._PHU_THU);
			this.db.AddInParameter(dbCommand, "@MIENTHUE", SqlDbType.TinyInt, this._MIENTHUE);
			this.db.AddInParameter(dbCommand, "@TRIGIA_THUKHAC", SqlDbType.Float, this._TRIGIA_THUKHAC);
			
			if (transaction != null)
			{
				return this.db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return this.db.ExecuteNonQuery(dbCommand);
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(HangMauDichDieuChinhCTCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (HangMauDichDieuChinhCT item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, HangMauDichDieuChinhCTCollection collection)
        {
            foreach (HangMauDichDieuChinhCT item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_SXXK_HangMauDichDieuChinhCT_InsertUpdate";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
			this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);
			this.db.AddInParameter(dbCommand, "@LanDieuChinhCT", SqlDbType.Int, this._LanDieuChinhCT);
			this.db.AddInParameter(dbCommand, "@THUE_XNK", SqlDbType.Float, this._THUE_XNK);
			this.db.AddInParameter(dbCommand, "@THUE_TTDB", SqlDbType.Float, this._THUE_TTDB);
			this.db.AddInParameter(dbCommand, "@THUE_VAT", SqlDbType.Float, this._THUE_VAT);
			this.db.AddInParameter(dbCommand, "@PHU_THU", SqlDbType.Float, this._PHU_THU);
			this.db.AddInParameter(dbCommand, "@MIENTHUE", SqlDbType.TinyInt, this._MIENTHUE);
			this.db.AddInParameter(dbCommand, "@TRIGIA_THUKHAC", SqlDbType.Float, this._TRIGIA_THUKHAC);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(HangMauDichDieuChinhCTCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (HangMauDichDieuChinhCT item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_SXXK_HangMauDichDieuChinhCT_Update";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
			this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);
			this.db.AddInParameter(dbCommand, "@LanDieuChinhCT", SqlDbType.Int, this._LanDieuChinhCT);
			this.db.AddInParameter(dbCommand, "@THUE_XNK", SqlDbType.Float, this._THUE_XNK);
			this.db.AddInParameter(dbCommand, "@THUE_TTDB", SqlDbType.Float, this._THUE_TTDB);
			this.db.AddInParameter(dbCommand, "@THUE_VAT", SqlDbType.Float, this._THUE_VAT);
			this.db.AddInParameter(dbCommand, "@PHU_THU", SqlDbType.Float, this._PHU_THU);
			this.db.AddInParameter(dbCommand, "@MIENTHUE", SqlDbType.TinyInt, this._MIENTHUE);
			this.db.AddInParameter(dbCommand, "@TRIGIA_THUKHAC", SqlDbType.Float, this._TRIGIA_THUKHAC);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(HangMauDichDieuChinhCTCollection collection, SqlTransaction transaction)
        {
            foreach (HangMauDichDieuChinhCT item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_SXXK_HangMauDichDieuChinhCT_Delete";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
			this.db.AddInParameter(dbCommand, "@LanDieuChinh", SqlDbType.Int, this._LanDieuChinh);
			this.db.AddInParameter(dbCommand, "@LanDieuChinhCT", SqlDbType.Int, this._LanDieuChinhCT);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(HangMauDichDieuChinhCTCollection collection, SqlTransaction transaction)
        {
            foreach (HangMauDichDieuChinhCT item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(HangMauDichDieuChinhCTCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (HangMauDichDieuChinhCT item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		#endregion

        public static void updateDatabase(DataSet ds, SqlTransaction trangsaction)
        {
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                HangMauDichDieuChinhCT tt = new HangMauDichDieuChinhCT();
                tt.LanDieuChinh = Convert.ToInt32(row["LAN_DC"].ToString());
                tt.MaHaiQuan = (row["MA_HQ"].ToString());
                tt.MaLoaiHinh = (row["MA_LH"].ToString());
                tt.NamDangKy = Convert.ToInt16(row["NAMDK"].ToString());
                tt.SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                tt.MaHang = (row["MA_PHU"].ToString());
                tt.LanDieuChinh = Convert.ToInt32(row["LAN_DC"].ToString());
                tt.LanDieuChinhCT = Convert.ToInt32(row["LAN_DC_CT"].ToString());
                tt.MIENTHUE = Convert.ToByte(row["MIENTHUE"].ToString());
                if(row["THUE_TTDB"].ToString()!="")
                    tt.THUE_TTDB = Convert.ToDouble(row["THUE_TTDB"]);
                if (row["THUE_VAT"].ToString() != "")
                    tt.THUE_VAT = Convert.ToDouble(row["THUE_VAT"]);
                if (row["THUE_XNK"].ToString() != "")
                    tt.THUE_XNK = Convert.ToDouble(row["THUE_XNK"]);
                if (row["TRIGIA_THUKHAC"].ToString() != "")
                    tt.TRIGIA_THUKHAC = Convert.ToDouble(row["TRIGIA_THUKHAC"]);
                if (row["PHU_THU"].ToString() != "")
                    tt.PHU_THU = Convert.ToDouble(row["PHU_THU"]);
                tt.InsertUpdateTransaction(trangsaction);
            }
        }
	}	
}