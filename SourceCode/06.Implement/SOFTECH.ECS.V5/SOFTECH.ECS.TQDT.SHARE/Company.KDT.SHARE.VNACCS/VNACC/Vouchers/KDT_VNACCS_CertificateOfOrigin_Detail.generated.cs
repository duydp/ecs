﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_CertificateOfOrigin_Detail : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long CertificateOfOrigin_ID { set; get; }
		public string SoCO { set; get; }
		public string LoaiCO { set; get; }
		public string ToChucCapCO { set; get; }
		public DateTime NgayCapCO { set; get; }
		public string NuocCapCO { set; get; }
		public string NguoiCapCO { set; get; }
        public List<KDT_VNACCS_CertificateOfOrigin_Detail> ListCertificateOfOrigin = new List<KDT_VNACCS_CertificateOfOrigin_Detail>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_CertificateOfOrigin_Detail> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_CertificateOfOrigin_Detail> collection = new List<KDT_VNACCS_CertificateOfOrigin_Detail>();
			while (reader.Read())
			{
				KDT_VNACCS_CertificateOfOrigin_Detail entity = new KDT_VNACCS_CertificateOfOrigin_Detail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("CertificateOfOrigin_ID"))) entity.CertificateOfOrigin_ID = reader.GetInt64(reader.GetOrdinal("CertificateOfOrigin_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoCO"))) entity.SoCO = reader.GetString(reader.GetOrdinal("SoCO"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiCO"))) entity.LoaiCO = reader.GetString(reader.GetOrdinal("LoaiCO"));
				if (!reader.IsDBNull(reader.GetOrdinal("ToChucCapCO"))) entity.ToChucCapCO = reader.GetString(reader.GetOrdinal("ToChucCapCO"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCapCO"))) entity.NgayCapCO = reader.GetDateTime(reader.GetOrdinal("NgayCapCO"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocCapCO"))) entity.NuocCapCO = reader.GetString(reader.GetOrdinal("NuocCapCO"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiCapCO"))) entity.NguoiCapCO = reader.GetString(reader.GetOrdinal("NguoiCapCO"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_CertificateOfOrigin_Detail> collection, long id)
        {
            foreach (KDT_VNACCS_CertificateOfOrigin_Detail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_CertificateOfOrigin_Details VALUES(@CertificateOfOrigin_ID, @SoCO, @LoaiCO, @ToChucCapCO, @NgayCapCO, @NuocCapCO, @NguoiCapCO)";
            string update = "UPDATE t_KDT_VNACCS_CertificateOfOrigin_Details SET CertificateOfOrigin_ID = @CertificateOfOrigin_ID, SoCO = @SoCO, LoaiCO = @LoaiCO, ToChucCapCO = @ToChucCapCO, NgayCapCO = @NgayCapCO, NuocCapCO = @NuocCapCO, NguoiCapCO = @NguoiCapCO WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_CertificateOfOrigin_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CertificateOfOrigin_ID", SqlDbType.BigInt, "CertificateOfOrigin_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoCO", SqlDbType.NVarChar, "SoCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiCO", SqlDbType.NVarChar, "LoaiCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ToChucCapCO", SqlDbType.NVarChar, "ToChucCapCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapCO", SqlDbType.DateTime, "NgayCapCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocCapCO", SqlDbType.VarChar, "NuocCapCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiCapCO", SqlDbType.NVarChar, "NguoiCapCO", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CertificateOfOrigin_ID", SqlDbType.BigInt, "CertificateOfOrigin_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoCO", SqlDbType.NVarChar, "SoCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiCO", SqlDbType.NVarChar, "LoaiCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ToChucCapCO", SqlDbType.NVarChar, "ToChucCapCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapCO", SqlDbType.DateTime, "NgayCapCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocCapCO", SqlDbType.VarChar, "NuocCapCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiCapCO", SqlDbType.NVarChar, "NguoiCapCO", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_CertificateOfOrigin_Details VALUES(@CertificateOfOrigin_ID, @SoCO, @LoaiCO, @ToChucCapCO, @NgayCapCO, @NuocCapCO, @NguoiCapCO)";
            string update = "UPDATE t_KDT_VNACCS_CertificateOfOrigin_Details SET CertificateOfOrigin_ID = @CertificateOfOrigin_ID, SoCO = @SoCO, LoaiCO = @LoaiCO, ToChucCapCO = @ToChucCapCO, NgayCapCO = @NgayCapCO, NuocCapCO = @NuocCapCO, NguoiCapCO = @NguoiCapCO WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_CertificateOfOrigin_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CertificateOfOrigin_ID", SqlDbType.BigInt, "CertificateOfOrigin_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoCO", SqlDbType.NVarChar, "SoCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiCO", SqlDbType.NVarChar, "LoaiCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ToChucCapCO", SqlDbType.NVarChar, "ToChucCapCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapCO", SqlDbType.DateTime, "NgayCapCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocCapCO", SqlDbType.VarChar, "NuocCapCO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiCapCO", SqlDbType.NVarChar, "NguoiCapCO", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CertificateOfOrigin_ID", SqlDbType.BigInt, "CertificateOfOrigin_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoCO", SqlDbType.NVarChar, "SoCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiCO", SqlDbType.NVarChar, "LoaiCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ToChucCapCO", SqlDbType.NVarChar, "ToChucCapCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapCO", SqlDbType.DateTime, "NgayCapCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocCapCO", SqlDbType.VarChar, "NuocCapCO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiCapCO", SqlDbType.NVarChar, "NguoiCapCO", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_CertificateOfOrigin_Detail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_CertificateOfOrigin_Detail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_CertificateOfOrigin_Detail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_CertificateOfOrigin_Detail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_CertificateOfOrigin_Detail> SelectCollectionBy_CertificateOfOrigin_ID(long certificateOfOrigin_ID)
		{
            IDataReader reader = SelectReaderBy_CertificateOfOrigin_ID(certificateOfOrigin_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_CertificateOfOrigin_ID(long certificateOfOrigin_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectBy_CertificateOfOrigin_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@CertificateOfOrigin_ID", SqlDbType.BigInt, certificateOfOrigin_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_CertificateOfOrigin_ID(long certificateOfOrigin_ID)
		{
			const string spName = "p_KDT_VNACCS_CertificateOfOrigin_Detail_SelectBy_CertificateOfOrigin_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@CertificateOfOrigin_ID", SqlDbType.BigInt, certificateOfOrigin_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_CertificateOfOrigin_Detail(long certificateOfOrigin_ID, string soCO, string loaiCO, string toChucCapCO, DateTime ngayCapCO, string nuocCapCO, string nguoiCapCO)
		{
			KDT_VNACCS_CertificateOfOrigin_Detail entity = new KDT_VNACCS_CertificateOfOrigin_Detail();	
			entity.CertificateOfOrigin_ID = certificateOfOrigin_ID;
			entity.SoCO = soCO;
			entity.LoaiCO = loaiCO;
			entity.ToChucCapCO = toChucCapCO;
			entity.NgayCapCO = ngayCapCO;
			entity.NuocCapCO = nuocCapCO;
			entity.NguoiCapCO = nguoiCapCO;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@CertificateOfOrigin_ID", SqlDbType.BigInt, CertificateOfOrigin_ID);
			db.AddInParameter(dbCommand, "@SoCO", SqlDbType.NVarChar, SoCO);
			db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.NVarChar, LoaiCO);
			db.AddInParameter(dbCommand, "@ToChucCapCO", SqlDbType.NVarChar, ToChucCapCO);
			db.AddInParameter(dbCommand, "@NgayCapCO", SqlDbType.DateTime, NgayCapCO.Year <= 1753 ? DBNull.Value : (object) NgayCapCO);
			db.AddInParameter(dbCommand, "@NuocCapCO", SqlDbType.VarChar, NuocCapCO);
			db.AddInParameter(dbCommand, "@NguoiCapCO", SqlDbType.NVarChar, NguoiCapCO);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_CertificateOfOrigin_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_CertificateOfOrigin_Detail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_CertificateOfOrigin_Detail(long id, long certificateOfOrigin_ID, string soCO, string loaiCO, string toChucCapCO, DateTime ngayCapCO, string nuocCapCO, string nguoiCapCO)
		{
			KDT_VNACCS_CertificateOfOrigin_Detail entity = new KDT_VNACCS_CertificateOfOrigin_Detail();			
			entity.ID = id;
			entity.CertificateOfOrigin_ID = certificateOfOrigin_ID;
			entity.SoCO = soCO;
			entity.LoaiCO = loaiCO;
			entity.ToChucCapCO = toChucCapCO;
			entity.NgayCapCO = ngayCapCO;
			entity.NuocCapCO = nuocCapCO;
			entity.NguoiCapCO = nguoiCapCO;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_CertificateOfOrigin_Detail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@CertificateOfOrigin_ID", SqlDbType.BigInt, CertificateOfOrigin_ID);
			db.AddInParameter(dbCommand, "@SoCO", SqlDbType.NVarChar, SoCO);
			db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.NVarChar, LoaiCO);
			db.AddInParameter(dbCommand, "@ToChucCapCO", SqlDbType.NVarChar, ToChucCapCO);
			db.AddInParameter(dbCommand, "@NgayCapCO", SqlDbType.DateTime, NgayCapCO.Year <= 1753 ? DBNull.Value : (object) NgayCapCO);
			db.AddInParameter(dbCommand, "@NuocCapCO", SqlDbType.VarChar, NuocCapCO);
			db.AddInParameter(dbCommand, "@NguoiCapCO", SqlDbType.NVarChar, NguoiCapCO);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_CertificateOfOrigin_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_CertificateOfOrigin_Detail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_CertificateOfOrigin_Detail(long id, long certificateOfOrigin_ID, string soCO, string loaiCO, string toChucCapCO, DateTime ngayCapCO, string nuocCapCO, string nguoiCapCO)
		{
			KDT_VNACCS_CertificateOfOrigin_Detail entity = new KDT_VNACCS_CertificateOfOrigin_Detail();			
			entity.ID = id;
			entity.CertificateOfOrigin_ID = certificateOfOrigin_ID;
			entity.SoCO = soCO;
			entity.LoaiCO = loaiCO;
			entity.ToChucCapCO = toChucCapCO;
			entity.NgayCapCO = ngayCapCO;
			entity.NuocCapCO = nuocCapCO;
			entity.NguoiCapCO = nguoiCapCO;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@CertificateOfOrigin_ID", SqlDbType.BigInt, CertificateOfOrigin_ID);
			db.AddInParameter(dbCommand, "@SoCO", SqlDbType.NVarChar, SoCO);
			db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.NVarChar, LoaiCO);
			db.AddInParameter(dbCommand, "@ToChucCapCO", SqlDbType.NVarChar, ToChucCapCO);
			db.AddInParameter(dbCommand, "@NgayCapCO", SqlDbType.DateTime, NgayCapCO.Year <= 1753 ? DBNull.Value : (object) NgayCapCO);
			db.AddInParameter(dbCommand, "@NuocCapCO", SqlDbType.VarChar, NuocCapCO);
			db.AddInParameter(dbCommand, "@NguoiCapCO", SqlDbType.NVarChar, NguoiCapCO);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_CertificateOfOrigin_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_CertificateOfOrigin_Detail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_CertificateOfOrigin_Detail(long id)
		{
			KDT_VNACCS_CertificateOfOrigin_Detail entity = new KDT_VNACCS_CertificateOfOrigin_Detail();
			entity.ID = id;
			
			return entity.Delete();
		}

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_VNACC_CertificateOfOrigin_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public int DeleteBy_CertificateOfOrigin_ID(long certificateOfOrigin_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_DeleteBy_CertificateOfOrigin_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@CertificateOfOrigin_ID", SqlDbType.BigInt, certificateOfOrigin_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_CertificateOfOrigin_Detail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_CertificateOfOrigin_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_CertificateOfOrigin_Detail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}