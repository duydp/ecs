﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace TestVNACC
{
    public partial class FormTestControl : Form
    {
        public FormTestControl()
        {
            InitializeComponent();
        }

        private void ucMaLoaiHinhIDA1_EditValueChanged(object sender, EventArgs e)
        {
            //txtRefDB.Text = ucCategoryExtend1.ReferenceDB;
            //txtCode.Text = ucCategoryExtend1.Code;
            //txtName.Text = ucCategoryExtend1.Name_VN;
        }

        private void FormTestControl_Load(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;

                //Set value
                //ucCuaKhauXuatNhap1.CountryCode = "CN";

                ucCategoryExtend1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A016;
                ucCategoryExtend1.ReLoadData();

                CreateData();


                //SET COLUMN
                string whereCondition = "";
                string orderBy = "";
                string valueMember = "Code";
                string displayMember = "Name";
                string valueMemberCaption = "Mã";
                string displayMemberCaption = "Tên";

                whereCondition = string.Format("TableID like '{0}%'", ECategory.A016.ToString());
                orderBy = "CityCode asc";
                valueMember = "CityCode";
                displayMember = "CityNameOrStateName";
                valueMemberCaption = "Mã";
                displayMemberCaption = "Tên";

                //Create obj and load data            
                List<VNACC_Category_CityUNLOCODE> datas = VNACC_Category_CityUNLOCODE.SelectCollectionDynamic(whereCondition, orderBy);

                lookUpEdit1.Properties.DataSource = datas;

                lookUpEdit1.Properties.Columns.Clear();
                // The field providing the editor's display text.
                lookUpEdit1.Properties.DisplayMember = displayMember;
                lookUpEdit1.Properties.ValueMember = valueMember;

                // Add two columns to the dropdown.
                LookUpColumnInfoCollection coll = lookUpEdit1.Properties.Columns;
                // A column to display the Format field's values.
                coll.Add(new LookUpColumnInfo(valueMember, valueMemberCaption, 0));
                coll.Add(new LookUpColumnInfo(displayMember, displayMemberCaption, 0));

                //  Set column widths according to their contents and resize the popup, if required.
                lookUpEdit1.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                // Enable auto completion search mode.
                //lookUpEdit1.Properties.SearchMode = SearchMode.AutoComplete;
                // Specify the column against which to perform the search.
                lookUpEdit1.Properties.AutoSearchColumnIndex = 1;

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void ucCuaKhauXuatNhap1_EditValueChanged(object sender, EventArgs e)
        {
            textEdit1.Text = ucCuaKhauXuatNhap1.Code;
            textEdit2.Text = ucCuaKhauXuatNhap1.Name_VN;
        }

        private void ucCategoryExtend1_EditValueChanged(object sender, EventArgs e)
        {
            txtRefDB.Text = ucCategoryExtend1.ReferenceDB;
            txtCode.Text = ucCategoryExtend1.Code;
            txtName.Text = ucCategoryExtend1.Name_VN;
        }

        private void CreateData()
        {

            try
            {
                Cursor = Cursors.WaitCursor;

                List<Company.KDT.SHARE.VNACCS.VNACC_Category_CityUNLOCODE> list = Company.KDT.SHARE.VNACCS.VNACC_Category_CityUNLOCODE.SelectCollectionDynamic("TableID like 'A016%'", "");

                DevExpress.XtraEditors.Controls.ComboBoxItemCollection items = cboName.Properties.Items;

                items.Clear();

                items.BeginUpdate();

                try
                {
                    foreach (Company.KDT.SHARE.VNACCS.VNACC_Category_CityUNLOCODE item in list)
                    {
                        //items.Add(new Company.KDT.SHARE.VNACCS.Controls.Category(item.CityCode, item.CityNameOrStateName));
                        items.Add(item.CityNameOrStateName);
                    }
                }
                finally
                {
                    items.EndUpdate();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void txtMa_Leave(object sender, EventArgs e)
        {

        }

        private void cboName_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void ucCategoryAllowEmpty1_EditValueChanged(object sender, EventArgs e)
        {
            txtREFNull.Text = ucCategoryAllowEmpty1.ReferenceDB;
            txtCodeNull.Text = ucCategoryAllowEmpty1.Code;
            txtNameNull.Text = ucCategoryAllowEmpty1.Name_VN;
        }

        private void txtCodeInput_EditValueChanged(object sender, EventArgs e)
        {
            ucCategoryAllowEmpty1.Code = txtCodeInput.Text;
        }

        private void txtNameInput_EditValueChanged(object sender, EventArgs e)
        {
            ucCategoryAllowEmpty1.Name_VN = txtNameInput.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Helper.Controls.ErrorMessageBoxForm_VNACC.ShowMessage(EDeclaration_CustomsClearance.IDA.ToString(),
                new List<ErrorVNACCS>() { new ErrorVNACCS("A0002-0000-0000"), new ErrorVNACCS("S0900-VN1 -0001"), new ErrorVNACCS("S0276-BL_ -0000") }, 
                false, Helper.Controls.MessageType.Warning);
        }
    }
}
