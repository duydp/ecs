using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;

namespace Company.Interface.GC
{
    public partial class NPLCungUngForm : BaseForm
    {
        public long TKMD_ID = 0;
        private ToKhaiMauDich TKMD;
        private DataSet ds = new DataSet();
        public NPLCungUngForm()
        {
            InitializeComponent();
        }

        private void NPLCungUngForm_Load(object sender, EventArgs e)
        {          
            TKMD= new ToKhaiMauDich();  
            TKMD.ID = TKMD_ID;
            TKMD.Load(); 
            BindData();
        }

        private void BindData()
        {
            this.ds = new ToKhaiMauDich().GetNPLCungUngTK(this.TKMD_ID);
            dgList.DataSource = this.ds.Tables[0];
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                Company.GC.BLL.GC.NguyenPhuLieu NPL=new Company.GC.BLL.GC.NguyenPhuLieu();
                NPL.HopDong_ID=TKMD.IDHopDong;
                NPL.Ma= e.Row.Cells["MaNguyenPhuLieu"].Text.Trim();
                NPL.Load();
                e.Row.Cells["TenNPL"].Text = NPL.Ten;
            }
        }
    }
}