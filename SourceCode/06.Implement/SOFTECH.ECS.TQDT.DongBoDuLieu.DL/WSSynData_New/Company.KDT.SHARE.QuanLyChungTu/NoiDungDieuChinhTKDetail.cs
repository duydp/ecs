﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public enum ENoiDungDieuChinhTK
    {
        Auto,   //"Tự động tạo";
        Manual  //"Điều chỉnh tay";
    }

    public partial class NoiDungDieuChinhTKDetail
    {
        public List<NoiDungDieuChinhTKDetail> ListNoiDungDieuChinhTKDetail = new List<NoiDungDieuChinhTKDetail>();

        public void LoadListNoiDungDieuChinhTKDetail()
        {
            ListNoiDungDieuChinhTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(this.Id_DieuChinh);
        }

        //---------------------------------------------------------------------------------------------

        public int Insert(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
            db.AddInParameter(dbCommand, "@NoiDungTKChinh", SqlDbType.NVarChar, NoiDungTKChinh);
            db.AddInParameter(dbCommand, "@NoiDungTKSua", SqlDbType.NVarChar, NoiDungTKSua);
            db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, Id_DieuChinh);
            db.AddInParameter(dbCommand, "@NguoiTao", SqlDbType.NVarChar, NguoiTao);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (int)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (int)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        public int InsertUpdate(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
            db.AddInParameter(dbCommand, "@NoiDungTKChinh", SqlDbType.NVarChar, NoiDungTKChinh);
            db.AddInParameter(dbCommand, "@NoiDungTKSua", SqlDbType.NVarChar, NoiDungTKSua);
            db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, Id_DieuChinh);
            db.AddInParameter(dbCommand, "@NguoiTao", SqlDbType.NVarChar, NguoiTao);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateByThongTin(SqlTransaction transaction)
        {
            const string spName = "p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate_ByThongTin";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
            db.AddInParameter(dbCommand, "@NoiDungTKChinh", SqlDbType.NVarChar, NoiDungTKChinh);
            db.AddInParameter(dbCommand, "@NoiDungTKSua", SqlDbType.NVarChar, NoiDungTKSua);
            db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, Id_DieuChinh);
            db.AddInParameter(dbCommand, "@NguoiTao", SqlDbType.NVarChar, NguoiTao);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_Id_DieuChinh(int id_DieuChinh)
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteBy_Id_DieuChinh]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, id_DieuChinh);

            return db.ExecuteNonQuery(dbCommand);
        }

        public static int DeleteBy_Id_DieuChinh(SqlTransaction transaction, long TKMDID, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteDynamic]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            string whereCondition = "Id_DieuChinh IN (SELECT ID FROM dbo.t_KDT_NoiDungDieuChinhTK WHERE TKMD_ID = " + TKMDID + ")";

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }

        public static int DeleteBy_Id_DieuChinh(SqlTransaction transaction, long TKMDID, SqlDatabase db, ENoiDungDieuChinhTK nguoiTao)
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteDynamic]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            string whereCondition = "Id_DieuChinh IN (SELECT ID FROM dbo.t_KDT_NoiDungDieuChinhTK WHERE TKMD_ID = " + TKMDID + ") AND NguoiTao = '" + nguoiTao.ToString() + "'";

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }

        // Select by foreign key return collection		
        public static List<NoiDungDieuChinhTKDetail> SelectCollectionBy_Id_DieuChinh(int id_DieuChinh)
        {
            List<NoiDungDieuChinhTKDetail> collection = new List<NoiDungDieuChinhTKDetail>();
            SqlDataReader reader = (SqlDataReader)SelectReaderBy_Id_DieuChinh(id_DieuChinh);
            while (reader.Read())
            {
                NoiDungDieuChinhTKDetail entity = new NoiDungDieuChinhTKDetail();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDungTKChinh"))) entity.NoiDungTKChinh = reader.GetString(reader.GetOrdinal("NoiDungTKChinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDungTKSua"))) entity.NoiDungTKSua = reader.GetString(reader.GetOrdinal("NoiDungTKSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("Id_DieuChinh"))) entity.Id_DieuChinh = reader.GetInt32(reader.GetOrdinal("Id_DieuChinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiTao"))) entity.NguoiTao = reader.GetString(reader.GetOrdinal("NguoiTao"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static DataSet SelectBy_Id_DieuChinh(int id_DieuChinh)
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectBy_Id_DieuChinh]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, id_DieuChinh);

            return db.ExecuteDataSet(dbCommand);
        }

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_Id_DieuChinh(int id_DieuChinh)
        {
            const string spName = "p_KDT_NoiDungDieuChinhTKDetail_SelectBy_Id_DieuChinh";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, id_DieuChinh);

            return db.ExecuteReader(dbCommand);
        }

       
    }
    public class SortNoiDungDieuChinh : IComparer<NoiDungDieuChinhTKDetail>
    {
        public int Compare(NoiDungDieuChinhTKDetail x, NoiDungDieuChinhTKDetail y)
        {
            int Compare = string.Compare(x.NoiDungTKChinh, y.NoiDungTKChinh);
            if (Compare == 0)
            {
                return x.ID.CompareTo(y.ID);
            }
            return Compare;
        }
    }
}
