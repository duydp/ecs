using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class GiayDangKyKiemTraThucPhamNK_2 : DevExpress.XtraReports.UI.XtraReport
    {
        public GiayDangKyKiemTraThucPhamNK_2()
        {
            InitializeComponent();
        }
        public void BindingReport(VAGP010 vagp)
        {
            int HangHoa = vagp.HangHoa.Count;
            if (HangHoa <= 10)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(2).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 10, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            DataTable dt = new DataTable();
            if (vagp != null && vagp.HangHoa.Count > 0)
            {
                int STT = 0;
                dt.Columns.Add("STT", typeof(string));
                dt.Columns.Add("TenHang", typeof(string));
                dt.Columns.Add("MaSoHH", typeof(string));
                dt.Columns.Add("XuatXu", typeof(string));
                dt.Columns.Add("SoLuong", typeof(string));
                dt.Columns.Add("DVTSoLuong", typeof(string));
                dt.Columns.Add("KhoiLuong", typeof(string));
                dt.Columns.Add("DVTKhoiLuong", typeof(string));
                for (int i = 0; i < vagp.HangHoa.Count; i++)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    dr["STT"] = STT.ToString().Trim();
                    dr["TenHang"] = vagp.HangHoa[i].B01.GetValue().ToString().Trim();
                    dr["MaSoHH"] = vagp.HangHoa[i].B02.GetValue().ToString().Trim();
                    dr["XuatXu"] = vagp.HangHoa[i].B03.GetValue().ToString().Trim();
                    dr["SoLuong"] = vagp.HangHoa[i].B04.GetValue().ToString().Trim();
                    dr["DVTSoLuong"] = vagp.HangHoa[i].B05.GetValue().ToString().Trim();
                    dr["KhoiLuong"] = vagp.HangHoa[i].B06.GetValue().ToString().Trim();
                    dr["DVTKhoiLuong"] = vagp.HangHoa[i].B07.GetValue().ToString().Trim();
                    dt.Rows.Add(dr);
                }
                BindingReportHang(dt);
            }
        }
        public void BindingReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            lblTenHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "TenHang");
            lblMaSoHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MaSoHH");
            lblXuatXu.DataBindings.Add("Text", DetailReport.DataSource, "XuatXu");
            lblSoLuong.DataBindings.Add("Text", DetailReport.DataSource, "SoLuong");
            lblDVTSoLuong.DataBindings.Add("Text", DetailReport.DataSource, "DVTSoLuong");
            lblKhoiLuong.DataBindings.Add("Text", DetailReport.DataSource, "KhoiLuong");
            lblDVTKhoiLuong.DataBindings.Add("Text", DetailReport.DataSource, "DVTKhoiLuong");
        }

        private void lblSoTrang_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            cell.Text = (e.PageIndex + 2).ToString();
        }


        


    }
}
