GO
 IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDich_ThongKeToKhaiXNK]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_ThongKeToKhaiXNK]
GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_ThongKeToKhaiXNK]
-- Database: ECS_TQDT_GC_V5  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, January 23, 2014  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_ThongKeToKhaiXNK]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT DISTINCT TKMD.ID ,
                TKMD.SoTiepNhan ,
                TKMD.NgayTiepNhan ,
                TKMD.MaHaiQuan ,
                TKMD.SoToKhai ,
                TKMD.MaLoaiHinh ,
                TKMD.NgayDangKy ,
                TKMD.MaDoanhNghiep ,
                TKMD.TenDoanhNghiep ,
                TKMD.MaDaiLyTTHQ ,
                TKMD.TenDaiLyTTHQ ,
                TKMD.TenDonViDoiTac ,
                TKMD.ChiTietDonViDoiTac ,
                TKMD.SoGiayPhep ,
                TKMD.NgayGiayPhep ,
                TKMD.NgayHetHanGiayPhep ,
                TKMD.SoHopDong ,
                TKMD.NgayHopDong ,
                TKMD.NgayHetHanHopDong ,
                TKMD.SoHoaDonThuongMai ,
                TKMD.NgayHoaDonThuongMai ,
                TKMD.PTVT_ID ,
                TKMD.SoHieuPTVT ,
                TKMD.NgayDenPTVT ,
                TKMD.QuocTichPTVT_ID ,
                TKMD.LoaiVanDon ,
                TKMD.SoVanDon ,
                TKMD.NgayVanDon ,
                TKMD.NuocXK_ID ,
                TKMD.NuocNK_ID ,
                TKMD.DiaDiemXepHang ,
                TKMD.CuaKhau_ID ,
                TKMD.DKGH_ID ,
                TKMD.NguyenTe_ID ,
                TKMD.TyGiaTinhThue ,
                TKMD.TyGiaUSD ,
                TKMD.PTTT_ID ,
                TKMD.SoHang ,
                TKMD.SoLuongPLTK ,
                TKMD.TenChuHang ,
                TKMD.ChucVu ,
                TKMD.SoContainer20 ,
                TKMD.SoContainer40 ,
                TKMD.SoKien ,
                TKMD.TrongLuong ,
                TKMD.TongTriGiaKhaiBao ,
                TKMD.TongTriGiaTinhThue ,
                TKMD.LoaiToKhaiGiaCong ,
                TKMD.LePhiHaiQuan ,
                TKMD.PhiBaoHiem ,
                TKMD.PhiVanChuyen ,
                TKMD.PhiXepDoHang ,
                TKMD.PhiKhac ,
                TKMD.CanBoDangKy ,
                TKMD.QuanLyMay ,
				TKMD.TrangThaiXuLy,
				CASE TKMD.TrangThaiXuLy WHEN -1 THEN N''Chưa khai báo''
											   WHEN 0 THEN N''Chờ duyệt''
											   WHEN 1 THEN N''Đã duyệt''
											   WHEN 2 THEN N''Không phê duyệt''
											   WHEN 5 THEN N''Sửa tờ kha''
											   WHEN 10 THEN N''Đã hủy''
											   ELSE N''Chờ hủy'' END AS TrangThai,
                TKMD.LoaiHangHoa ,
                TKMD.GiayTo ,
				CASE TKMD.PhanLuong WHEN ''1'' THEN N''Luồng xanh''
											   WHEN ''2'' THEN N''Luồng vàng''
											   WHEN ''3'' THEN N''Luồng đỏ''
											   ELSE N''Chưa phân luồng'' END AS PhanLuong,
                TKMD.MaDonViUT ,
                TKMD.TenDonViUT ,
                TKMD.TrongLuongNet ,
                TKMD.SoTienKhoan ,
                TKMD.GUIDSTR ,
                TKMD.DeXuatKhac ,
                TKMD.LyDoSua ,
                TKMD.ActionStatus ,
                TKMD.GuidReference ,
                TKMD.NamDK ,
                TKMD.HUONGDAN  FROM dbo.t_KDT_ToKhaiMauDich TKMD 
INNER JOIN dbo.t_KDT_HangMauDich HMD ON HMD.TKMD_ID = TKMD.ID 
 WHERE ' +  @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  

GO

GO
 IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDich_BaoCaoChiTietHHXNK]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_BaoCaoChiTietHHXNK]
GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_BaoCaoChiTietHHXNK]
-- Database: ECS_TQDT_GC_V5  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, January 23, 2014  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_BaoCaoChiTietHHXNK]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT * FROM dbo.t_KDT_ToKhaiMauDich TKMD 
INNER JOIN dbo.t_KDT_HangMauDich HMD ON HMD.TKMD_ID = TKMD.ID WHERE ' +  @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  

GO

GO
 IF OBJECT_ID(N'[dbo].[p_KDT_ToKhaiMauDich_BaoCaoTongHopHHXNK]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_BaoCaoTongHopHHXNK]
GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_BaoCaoTongHopHHXNK]
-- Database: ECS_TQDT_GC_V5  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, January 23, 2014  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_BaoCaoTongHopHHXNK]  
 @WhereCondition NVARCHAR(500),  
 @GroupByExpression NVARCHAR(MAX) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT 
DISTINCT
TKMD.MaDoanhNghiep,
TKMD.TenDoanhNghiep,
TKMD.TenDonViDoiTac,
HMD.MaPhu,
HMD.TenHang,
HMD.MaHS,
HMD.DVT_ID,
TKMD.SoHopDong,
SUM(HMD.SoLuong) AS SoLuong,
SUM(HMD.TriGiaKB) AS TriGiaKB,
SUM(HMD.TriGiaTT) AS TriGiaTT,
SUM(HMD.TriGiaKB_VND) AS TriGiaKB_VND,
SUM(HMD.ThueXNK) AS ThueXNK ,
SUM(HMD.ThueTTDB) AS ThueTTDB,
SUM(HMD.ThueGTGT) AS ThueGTGT,
SUM(HMD.PhuThu) AS PhuThu,
SUM(HMD.TyLeThuKhac) AS TyLeThuKhac,
SUM(HMD.TriGiaThuKhac) AS TriGiaThuKhac,
SUM(HMD.MienThue) AS MienThue
FROM dbo.t_KDT_ToKhaiMauDich TKMD INNER JOIN dbo.t_KDT_HangMauDich HMD ON HMD.TKMD_ID = TKMD.ID 
WHERE ' +  @WhereCondition  

BEGIN  
 SET @SQL = @SQL + 'GROUP BY TKMD.MaDoanhNghiep,TKMD.TenDoanhNghiep,TKMD.TenDonViDoiTac,HMD.MaPhu,HMD.TenHang,HMD.MaHS,HMD.DVT_ID,TKMD.SoHopDong' + @GroupByExpression  
END  
  
EXEC sp_executesql @SQL  

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '37.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('37.3',GETDATE(), N'CẬP NHẬT PROCEDURE KẾT XUẤT DỮ LIỆU')
END