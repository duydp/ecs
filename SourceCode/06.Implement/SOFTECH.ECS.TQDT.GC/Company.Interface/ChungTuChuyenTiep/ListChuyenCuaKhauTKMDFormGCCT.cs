﻿using System;
using System.Drawing;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.KDT.GC;
using Company.Interface.GC;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu.GCCT;
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class ListChuyenCuaKhauTKMDFormGCCT : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public ToKhaiChuyenTiep TKCT;
        //-----------------------------------------------------------------------------------------

        public ListChuyenCuaKhauTKMDFormGCCT()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = TKCT.CTKDeNghiChuyenCuaKhau;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            //if (TKCT.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
            //{
            //    btnTaoMoi.Enabled = false;
            //    btnXoa.Enabled = false;
            //}

            //HUNGTQ, Uopdate 07/06/2010
            SetButtonStateCHUYENCUAKHAU(TKCT, isKhaiBoSung);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<DeNghiChuyenCuaKhau> listHopDong = new List<DeNghiChuyenCuaKhau>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DeNghiChuyenCuaKhau hd = (DeNghiChuyenCuaKhau)i.GetRow().DataRow;
                        listHopDong.Add(hd);
                    }
                }
            }

            foreach (DeNghiChuyenCuaKhau hd in listHopDong)
            {
                hd.Delete();
                TKCT.CTKDeNghiChuyenCuaKhau.Remove(hd);
            }

            dgList.DataSource = TKCT.CTKDeNghiChuyenCuaKhau;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            ChuyenCuaKhauFormGCCT f = new ChuyenCuaKhauFormGCCT();
            f.TKCT = TKCT;
            f.isKhaiBoSung = isKhaiBoSung;
            f.ShowDialog();
            dgList.DataSource = TKCT.CTKDeNghiChuyenCuaKhau;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ChuyenCuaKhauFormGCCT f = new ChuyenCuaKhauFormGCCT();
            f.TKCT = TKCT;
            f.deNghiChuyen = (DeNghiChuyenCuaKhau)e.Row.DataRow;
            f.isKhaiBoSung = isKhaiBoSung;
            f.ShowDialog();
            dgList.DataSource = TKCT.CTKDeNghiChuyenCuaKhau;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateCHUYENCUAKHAU(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkmd, bool isKhaiBoSung)
        {
            bool status = false;
            if (TKCT.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                //Khai bao moi
                if (isKhaiBoSung == false)
                {
                    //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                    status = (tkmd.SoTiepNhan == 0);

                    btnXoa.Enabled = status;
                    btnTaoMoi.Enabled = status;
                }
                //Khai bao bo sung
                else
                {
                    //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                    if (tkmd.SoToKhai == 0)
                    {
                        //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                        //Globals.ShowMessageTQDT(msg, false);

                        //return false;
                    }
                    else
                    {
                        status = true;

                        btnXoa.Enabled = status;
                        btnTaoMoi.Enabled = status;
                    }
                }
            }
            return true;
        }

        #endregion

    }
}
