﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.SXXK;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseNPLMaper : BaseFormHaveGuidPanel
    {
        public T_KHOKETOAN_NGUYENPHULIEU NPL = new T_KHOKETOAN_NGUYENPHULIEU();
        public T_KHOKETOAN_NGUYENPHULIEU_MAP NPLMap = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
        public bool isAdd = true;
        NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        public WareHouseNPLMaper()
        {
            InitializeComponent();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            txtDonViTinhKT.DataSource = this._DonViTinh;
            txtDonViTinhKT.DisplayMember = "Ten";
            txtDonViTinhKT.ValueMember = "Ten";
            txtDonViTinhKT.ReadOnly = false;

            txtDonViTinhNPL.DataSource = this._DonViTinh;
            txtDonViTinhNPL.DisplayMember = "Ten";
            txtDonViTinhNPL.ValueMember = "Ten";
            txtDonViTinhNPL.ReadOnly = false;

            cbbMaKho.DataSource = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
            cbbMaKho.DisplayMember = "MAKHO";
            cbbMaKho.ValueMember = "MAKHO";

            cbbTK.DataSource = T_KHOKETOAN_TAIKHOAN.SelectCollectionAll();
            cbbTK.DisplayMember = "MATK";
            cbbTK.ValueMember = "MATK";
        }

        private void WareHouseNPLMaper_Load(object sender, EventArgs e)
        {
            SetNPL();
        }
        private void BindData()
        {
            try
            {
                grList.Refetch();
                grList.DataSource = NPL.NPLMapCollection;
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetNPLMap()
        {
            try
            {

                NPLMap.MANPL = txtMaNPL.Text.ToString();
                NPLMap.TENNPL = txtTenNPL.Text.ToString();
                NPLMap.DVT = txtDonViTinhNPL.Text.ToString();
                NPLMap.MANPLMAP = txtMaNPLKT.Text.ToString();
                NPLMap.TENNPLMAP = txtTenNPLKT.Text.ToString();
                NPLMap.DVT = txtDonViTinhKT.Text.ToString();
                NPLMap.TYLEQD = Convert.ToDecimal(txtTLQD.Text);
                if (isAdd)
                    NPL.NPLMapCollection.Add(NPLMap);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetNPL()
        {
            NPL.MANPL = txtMaNPLKT.Text.ToString();
            NPL.TENNPL = txtTenNPLKT.Text.ToString();
            NPL.MAHS = ctrMaSoHang.Code;
            NPL.DVT = txtDonViTinhKT.Text.ToString();
            NPL.TENTIENGANH = txtTenTiengAnh.Text.ToString();
            NPL.GHICHU = txtGhiChu.Text.ToString();
            NPL.MAKHO = cbbMaKho.Text.ToString();
            NPL.TAIKHOAN = cbbTK.Text.ToString();
            NPL.DONHANG = txtDonHang.Text.ToString();
            NPL.NGUOIBAN = txtNguoiBan.Text.ToString();
            NPL.KICHTHUOC = txtKichThuoc.Text.ToString();
            NPL.MAUSAC = txtMauSac.Text.ToString();

            GetNPLMap();
        }
        private void SetNPL()
        {
            txtMaNPLKT.Text = NPL.MANPL;
            txtTenNPLKT.Text = NPL.TENNPL;
            ctrMaSoHang.Code = NPL.MAHS;
            txtDonViTinhKT.Text = NPL.DVT;
            txtTenTiengAnh.Text = NPL.TENTIENGANH;
            txtGhiChu.Text = NPL.GHICHU;
            cbbMaKho.Value = NPL.MAKHO;
            cbbTK.Value = NPL.TAIKHOAN;
            txtDonHang.Text = NPL.DONHANG;
            txtNguoiBan.Text = NPL.NGUOIBAN;
            txtMauSac.Text = NPL.MAUSAC;
            txtKichThuoc.Text = NPL.KICHTHUOC;

            BindData();
        }
        private void SetNPLMap()
        {
            txtMaNPL.Text = NPLMap.MANPL;
            txtTenNPL.Text = NPLMap.TENNPL;
            txtDonViTinhNPL.Text = NPLMap.DVT;
            txtTLQD.Text = NPLMap.TYLEQD.ToString();
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                ctrMaSoHang.SetValidate = !isOnlyWarning;
                ctrMaSoHang.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrMaSoHang.IsValidate;

                isValid &= ValidateControl.ValidateNull(txtMaNPLKT, errorProvider, " Mã Nguyên phụ liệu ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNPLKT, errorProvider, " Tên Nguyên phụ liệu ");
                isValid &= ValidateControl.ValidateNull(txtDonViTinhKT, errorProvider, " Đơn vị tính ", isOnlyWarning);

                isValid &= ValidateControl.ValidateNull(txtMaNPL, errorProvider, " Mã Nguyên phụ liệu ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNPL, errorProvider, " Tên Nguyên phụ liệu ");
                isValid &= ValidateControl.ValidateNull(txtMaHSNPL, errorProvider, " Mã HS ");
                isValid &= ValidateControl.ValidateNull(txtDonViTinhNPL, errorProvider, " Đơn vị tính ", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtTLQD, errorProvider, " Tỷ lệ quy đổi ");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetNPL();
                CheckExitNPL();
                NPL.InsertUpdateFull();
                isAdd = true;
                BindData();
                NPLMap = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
                txtMaNPL.Text = String.Empty;
                txtTenNPL.Text = String.Empty;
                txtMaHSNPL.Text = String.Empty;
                txtDonViTinhNPL.Text = String.Empty;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void CheckExitNPL()
        {
            //Kiểm tra NPL Map đã được khai báo .
            List<T_KHOKETOAN_NGUYENPHULIEU> NPLCollection = T_KHOKETOAN_NGUYENPHULIEU.SelectCollectionDynamic(" MANPL = '" + txtMaNPLKT.Text.ToString().Trim() + "'", "");
            if (NPLCollection.Count >= 1)
            {
                errorProvider.SetError(txtMaNPLKT, "MÃ NGUYÊN PHỤ LIỆU : ''" + txtMaNPLKT.Text.ToString() + "'' NÀY ĐÃ MAP TRƯỚC ĐÓ .");
                return;
            }
            //if (NPL.ID == 0)
            //{
            //}
            //else
            //{
            //    foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in NPL.NPLMapCollection)
            //    {
            //        if (item.MANPLMAP != txtMaNPLKT.Text.ToString().Trim())
            //        {
            //            errorProvider.SetError(txtMaNPLKT, "Mã NPL : ''" + txtMaNPLKT.Text.ToString() + "'' này khác với mã NPL đã có ở danh sách phía dưới .");
            //            return;
            //        }
            //    }
            //}
            if (isAdd)
            {
                //Kiểm tra NPL VNACCS đã có trên lưới .
                foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in NPL.NPLMapCollection)
                {
                    if (item.MANPLMAP == txtMaNPL.Text.ToString().Trim())
                    {
                        errorProvider.SetError(txtMaNPL, "MÃ NGUYÊN PHỤ LIỆU : ''" + txtMaNPL.Text.ToString() + "'' NÀY ĐÃ CÓ TRONG DANH SÁCH PHÍA DƯỚI .");
                        return;
                    }
                }
            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<T_KHOKETOAN_NGUYENPHULIEU_MAP> ItemColl = new List<T_KHOKETOAN_NGUYENPHULIEU_MAP>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((T_KHOKETOAN_NGUYENPHULIEU_MAP)i.GetRow().DataRow);
                        }

                    }
                    foreach (T_KHOKETOAN_NGUYENPHULIEU_MAP item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        NPL.NPLMapCollection.Remove(item);
                    }
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    NPLMap = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
                    NPLMap = (T_KHOKETOAN_NGUYENPHULIEU_MAP)e.Row.DataRow;
                    isAdd = false;
                    SetNPLMap();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {
            string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                if (this.NPLRegistedForm_CX == null)
                    this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
                this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm_CX.LoaiNPL = "3";
                this.NPLRegistedForm_CX.ShowDialog(this);
                if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                {
                    txtMaNPL.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                    txtTenNPL.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
                    txtMaHSNPL.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                    txtDonViTinhNPL.Text = DonViTinh_GetName(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID);

                }
            }
            else
            {
                if (this.NPLRegistedForm == null)
                    this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm.isBrowser = true;
                this.NPLRegistedForm.ShowDialog(this);
                if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
                {
                    txtMaNPL.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                    txtTenNPL.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                    txtMaHSNPL.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                    txtDonViTinhNPL.Text = DonViTinh_GetName(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID);

                }
            }
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtMaNPL.Text))
            {
                Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.Ma = txtMaNPL.Text;
                if (npl.Load())
                {
                    txtMaNPL.Text = npl.Ma;
                    ctrMaSoHang.Code = npl.MaHS;
                    txtTenNPL.Text = npl.Ten;
                    txtDonViTinhNPL.Text = DonViTinh_GetName(npl.DVT_ID);

                }
            }
        }
    }
}
