﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.VNACCS
{
    public partial class VNACC_ListToKhai : BaseForm
    {
        public List<KDT_VNACC_ToKhaiMauDich> ListTKMD = new List<KDT_VNACC_ToKhaiMauDich>();
        DataSet ds = new DataSet();
        //public enum SelectTion
        //{
        //    SingleSelect,
        //    MultiSelect
        //}
        //public long[] ListIDTKMD { get; set; }
        //public KDT_VNACC_ToKhaiMauDich HangMD { get; set; }
        //public List<KDT_VNACC_ToKhaiMauDich> listHangMD { get; set; }
        //public SelectTion SelectMode { set; get; }
        //public VNACC_ListHangHMD(SelectTion SelectMode, long[] idTKMD)
        //{
        //    //InitializeComponent();
        //    //gridEX1.SelectionMode = Janus.Windows.GridEX.SelectionMode.SingleSelection;
        //    //gridEX1.RootTable.Columns["Select"].Visible = SelectMode == SelectTion.MultiSelect;
        //    //this.SelectMode = SelectMode;
        //    //ListIDTKMD = idTKMD;
        //}

        private void VNACC_ListHangHMD_Load(object sender, EventArgs e)
        {
            //gridEX1.DataSource = KDT_VNACC_HangMauDich.LoadHangMauDichbyID(this.ListIDTKMD);
        }

        private void btnChonHang_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if (this.SelectMode == SelectTion.SingleSelect)
            //    {
            //        HangMD = (KDT_VNACC_HangMauDich)gridEX1.SelectedItems[0].GetRow().DataRow;
            //    }
            //    else if (this.SelectMode == SelectTion.MultiSelect)
            //    {
            //        listHangMD = new List<KDT_VNACC_HangMauDich>();
            //        foreach (Janus.Windows.GridEX.GridEXRow item in gridEX1.GetCheckedRows())
            //        {
            //            listHangMD.Add((KDT_VNACC_HangMauDich)item.DataRow);
            //        }
            //    }
            //    this.DialogResult = DialogResult.OK;
            //}   
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //    this.ShowMessage(ex.Message, false);
            //}

        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    SaveFileDialog sfNPL = new SaveFileDialog();
            //    sfNPL.FileName = "HangMauDich_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
            //    sfNPL.Filter = "Excel files| *.xls";
            //    sfNPL.ShowDialog(this);

            //    if (sfNPL.FileName != "")
            //    {
            //        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
            //        gridEXExporter1.GridEX = gridEX1;
            //        System.IO.Stream str = sfNPL.OpenFile();
            //        gridEXExporter1.Export(str);
            //        str.Close();

            //        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
            //        {
            //            System.Diagnostics.Process.Start(sfNPL.FileName);
            //        }
            //    }
            //}
            //catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                string soToKhai = txtSoToKhai.Text.Trim();
                               string trangthai = (cbbTrangThai.SelectedValue == null) ? "1" : cbbTrangThai.SelectedValue.ToString().Trim();
                DateTime ngayDangKy = clcNgayDangKy.Value;
                string haiquan = ctrMaHaiQuan.Code.Trim();

                string where = String.Empty;
                where = "MaDonVi='" + GlobalSettings.MA_DON_VI + "' ";
                if (trangthai != "")
                    where = where + " and TrangThaiXuLy='" + trangthai + "'";
                if (soToKhai != "")
                    where = where + " and SoToKhai like '%" + soToKhai + "%' ";
                if (haiquan != "")
                    where = where + " and CoQuanHaiQuan like '" + haiquan + "%' ";
              

                //if (ngayDangKy.ToString("dd/MM/yyyy") != "01/01/1900")
                //    where = where + " and NgayDangKy='" + ngayDangKy.ToString("yyyy/MM/dd") + "'";
                ListTKMD.Clear();

                LoadData(where);

                grList.DataSource = ListTKMD;
                grList.Refresh();


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }
        private void LoadData(string where)
        {
            ds = VNACC_Category_Common.SelectDynamic("ReferenceDB in ('E001','E002')", null);
            ListTKMD = KDT_VNACC_ToKhaiMauDich.SelectCollectionDynamic(where, null);
            grList.DropDowns["drdMaLoaiHinh"].DataSource = ds.Tables[0];
            grList.DataSource = ListTKMD;


        }
        
    }
}
