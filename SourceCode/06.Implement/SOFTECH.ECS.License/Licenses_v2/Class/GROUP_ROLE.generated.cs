using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Activation.Server
{
	public partial class Group_Role
	{
		#region Properties.
		
		public int ID_GROUP { set; get; }
		public int ID_ROLE { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static Group_Role Load(int iD_GROUP, int iD_ROLE)
		{
			const string spName = "[dbo].[p_LICENSE_Group_Role_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID_GROUP", SqlDbType.Int, iD_GROUP);
			db.AddInParameter(dbCommand, "@ID_ROLE", SqlDbType.Int, iD_ROLE);
			Group_Role entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new Group_Role();
				if (!reader.IsDBNull(reader.GetOrdinal("ID_GROUP"))) entity.ID_GROUP = reader.GetInt32(reader.GetOrdinal("ID_GROUP"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_ROLE"))) entity.ID_ROLE = reader.GetInt32(reader.GetOrdinal("ID_ROLE"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<Group_Role> SelectCollectionAll()
		{
			List<Group_Role> collection = new List<Group_Role>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				Group_Role entity = new Group_Role();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID_GROUP"))) entity.ID_GROUP = reader.GetInt32(reader.GetOrdinal("ID_GROUP"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_ROLE"))) entity.ID_ROLE = reader.GetInt32(reader.GetOrdinal("ID_ROLE"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<Group_Role> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<Group_Role> collection = new List<Group_Role>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				Group_Role entity = new Group_Role();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID_GROUP"))) entity.ID_GROUP = reader.GetInt32(reader.GetOrdinal("ID_GROUP"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_ROLE"))) entity.ID_ROLE = reader.GetInt32(reader.GetOrdinal("ID_ROLE"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<Group_Role> SelectCollectionBy_ID_GROUP(int iD_GROUP)
		{
			List<Group_Role> collection = new List<Group_Role>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_ID_GROUP(iD_GROUP);
			while (reader.Read())
			{
				Group_Role entity = new Group_Role();
				if (!reader.IsDBNull(reader.GetOrdinal("ID_GROUP"))) entity.ID_GROUP = reader.GetInt32(reader.GetOrdinal("ID_GROUP"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_ROLE"))) entity.ID_ROLE = reader.GetInt32(reader.GetOrdinal("ID_ROLE"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		public static IList<Group_Role> SelectCollectionBy_ID_ROLE(int iD_ROLE)
		{
			List<Group_Role> collection = new List<Group_Role>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_ID_ROLE(iD_ROLE);
			while (reader.Read())
			{
				Group_Role entity = new Group_Role();
				if (!reader.IsDBNull(reader.GetOrdinal("ID_GROUP"))) entity.ID_GROUP = reader.GetInt32(reader.GetOrdinal("ID_GROUP"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_ROLE"))) entity.ID_ROLE = reader.GetInt32(reader.GetOrdinal("ID_ROLE"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_ID_GROUP(int iD_GROUP)
		{
			const string spName = "[dbo].[p_LICENSE_Group_Role_SelectBy_ID_GROUP]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID_GROUP", SqlDbType.Int, iD_GROUP);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------
		public static DataSet SelectBy_ID_ROLE(int iD_ROLE)
		{
			const string spName = "[dbo].[p_LICENSE_Group_Role_SelectBy_ID_ROLE]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID_ROLE", SqlDbType.Int, iD_ROLE);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LICENSE_Group_Role_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LICENSE_Group_Role_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LICENSE_Group_Role_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LICENSE_Group_Role_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_ID_GROUP(int iD_GROUP)
		{
			const string spName = "p_LICENSE_Group_Role_SelectBy_ID_GROUP";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID_GROUP", SqlDbType.Int, iD_GROUP);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		public static IDataReader SelectReaderBy_ID_ROLE(int iD_ROLE)
		{
			const string spName = "p_LICENSE_Group_Role_SelectBy_ID_ROLE";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID_ROLE", SqlDbType.Int, iD_ROLE);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertRole(int iD_GROUP, int iD_ROLE)
		{
			Group_Role entity = new Group_Role();	
			entity.ID_GROUP = iD_GROUP;
			entity.ID_ROLE = iD_ROLE;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_LICENSE_Group_Role_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID_GROUP", SqlDbType.Int, ID_GROUP);
			db.AddInParameter(dbCommand, "@ID_ROLE", SqlDbType.Int, ID_ROLE);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<Group_Role> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Group_Role item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateRole(int iD_GROUP, int iD_ROLE)
		{
			Group_Role entity = new Group_Role();			
			entity.ID_GROUP = iD_GROUP;
			entity.ID_ROLE = iD_ROLE;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_LICENSE_Group_Role_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID_GROUP", SqlDbType.Int, ID_GROUP);
			db.AddInParameter(dbCommand, "@ID_ROLE", SqlDbType.Int, ID_ROLE);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<Group_Role> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Group_Role item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateRole(int iD_GROUP, int iD_ROLE)
		{
			Group_Role entity = new Group_Role();			
			entity.ID_GROUP = iD_GROUP;
			entity.ID_ROLE = iD_ROLE;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LICENSE_Group_Role_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID_GROUP", SqlDbType.Int, ID_GROUP);
			db.AddInParameter(dbCommand, "@ID_ROLE", SqlDbType.Int, ID_ROLE);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<Group_Role> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Group_Role item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteRole(int iD_GROUP, int iD_ROLE)
		{
			Group_Role entity = new Group_Role();
			entity.ID_GROUP = iD_GROUP;
			entity.ID_ROLE = iD_ROLE;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LICENSE_Group_Role_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID_GROUP", SqlDbType.Int, ID_GROUP);
			db.AddInParameter(dbCommand, "@ID_ROLE", SqlDbType.Int, ID_ROLE);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_ID_GROUP(int iD_GROUP)
		{
			const string spName = "[dbo].[p_LICENSE_Group_Role_DeleteBy_ID_GROUP]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID_GROUP", SqlDbType.Int, iD_GROUP);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		public static int DeleteBy_ID_ROLE(int iD_ROLE)
		{
			const string spName = "[dbo].[p_LICENSE_Group_Role_DeleteBy_ID_ROLE]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID_ROLE", SqlDbType.Int, iD_ROLE);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_LICENSE_Group_Role_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<Group_Role> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Group_Role item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}