﻿namespace VNACC_ImportExcelToGrid
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout gridMain_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.label1 = new System.Windows.Forms.Label();
            this.txtPathFile = new System.Windows.Forms.TextBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.numDongBatDau = new System.Windows.Forms.NumericUpDown();
            this.btnReadFile = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cboSheetName = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtElementNameJP = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtElementNameEN = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtElementNameVN = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAttribute = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDigit = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtRepetition1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCondition1 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCondition2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtCondition3 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtFormat = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtReferenceDB = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtInputConditionJP = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtInputConditionEN = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtColumn = new System.Windows.Forms.TextBox();
            this.btnGetData = new System.Windows.Forms.Button();
            this.gridMain = new Janus.Windows.GridEX.GridEX();
            this.lblMessage = new System.Windows.Forms.Label();
            this.janusSuperTip1 = new Janus.Windows.Common.JanusSuperTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.numDongBatDau)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMain)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Đường dẫn file:";
            // 
            // txtPathFile
            // 
            this.txtPathFile.Location = new System.Drawing.Point(113, 19);
            this.txtPathFile.Name = "txtPathFile";
            this.txtPathFile.Size = new System.Drawing.Size(317, 20);
            this.txtPathFile.TabIndex = 1;
            // 
            // btnOpen
            // 
            this.btnOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpen.Location = new System.Drawing.Point(436, 16);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(75, 23);
            this.btnOpen.TabIndex = 2;
            this.btnOpen.Text = "Chọn file";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.RestoreDirectory = true;
            this.openFileDialog.ShowHelp = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(244, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Dòng bắt đầu đọc dữ liệu:";
            // 
            // numDongBatDau
            // 
            this.numDongBatDau.Location = new System.Drawing.Point(382, 45);
            this.numDongBatDau.Name = "numDongBatDau";
            this.numDongBatDau.Size = new System.Drawing.Size(48, 20);
            this.numDongBatDau.TabIndex = 3;
            this.numDongBatDau.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // btnReadFile
            // 
            this.btnReadFile.Location = new System.Drawing.Point(436, 42);
            this.btnReadFile.Name = "btnReadFile";
            this.btnReadFile.Size = new System.Drawing.Size(75, 23);
            this.btnReadFile.TabIndex = 4;
            this.btnReadFile.Text = "Đọc file";
            this.btnReadFile.UseVisualStyleBackColor = true;
            this.btnReadFile.Click += new System.EventHandler(this.btnReadFile_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tên sheet:";
            // 
            // cboSheetName
            // 
            this.cboSheetName.FormattingEnabled = true;
            this.cboSheetName.Location = new System.Drawing.Point(113, 45);
            this.cboSheetName.Name = "cboSheetName";
            this.cboSheetName.Size = new System.Drawing.Size(121, 21);
            this.cboSheetName.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Cột No.";
            // 
            // txtNo
            // 
            this.txtNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNo.Location = new System.Drawing.Point(31, 99);
            this.txtNo.MaxLength = 1;
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(34, 20);
            this.txtNo.TabIndex = 6;
            this.txtNo.Text = "A";
            this.txtNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(144, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Cột Tiếng Nhật";
            // 
            // txtElementNameJP
            // 
            this.txtElementNameJP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtElementNameJP.Location = new System.Drawing.Point(163, 99);
            this.txtElementNameJP.MaxLength = 1;
            this.txtElementNameJP.Name = "txtElementNameJP";
            this.txtElementNameJP.Size = new System.Drawing.Size(34, 20);
            this.txtElementNameJP.TabIndex = 6;
            this.txtElementNameJP.Text = "C";
            this.txtElementNameJP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(229, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Cột Tiếng Anh";
            // 
            // txtElementNameEN
            // 
            this.txtElementNameEN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtElementNameEN.Location = new System.Drawing.Point(248, 99);
            this.txtElementNameEN.MaxLength = 1;
            this.txtElementNameEN.Name = "txtElementNameEN";
            this.txtElementNameEN.Size = new System.Drawing.Size(34, 20);
            this.txtElementNameEN.TabIndex = 6;
            this.txtElementNameEN.Text = "D";
            this.txtElementNameEN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(310, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Cột Tiếng Việt";
            // 
            // txtElementNameVN
            // 
            this.txtElementNameVN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtElementNameVN.Location = new System.Drawing.Point(329, 99);
            this.txtElementNameVN.MaxLength = 1;
            this.txtElementNameVN.Name = "txtElementNameVN";
            this.txtElementNameVN.Size = new System.Drawing.Size(34, 20);
            this.txtElementNameVN.TabIndex = 6;
            this.txtElementNameVN.Text = "E";
            this.txtElementNameVN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(390, 83);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Cột ID";
            // 
            // txtID
            // 
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.Location = new System.Drawing.Point(393, 99);
            this.txtID.MaxLength = 1;
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(34, 20);
            this.txtID.TabIndex = 6;
            this.txtID.Text = "F";
            this.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(433, 83);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Cột Attribute";
            // 
            // txtAttribute
            // 
            this.txtAttribute.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAttribute.Location = new System.Drawing.Point(449, 99);
            this.txtAttribute.MaxLength = 1;
            this.txtAttribute.Name = "txtAttribute";
            this.txtAttribute.Size = new System.Drawing.Size(34, 20);
            this.txtAttribute.TabIndex = 6;
            this.txtAttribute.Text = "G";
            this.txtAttribute.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(500, 83);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Cột Digit";
            // 
            // txtDigit
            // 
            this.txtDigit.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDigit.Location = new System.Drawing.Point(507, 99);
            this.txtDigit.MaxLength = 1;
            this.txtDigit.Name = "txtDigit";
            this.txtDigit.Size = new System.Drawing.Size(34, 20);
            this.txtDigit.TabIndex = 6;
            this.txtDigit.Text = "H";
            this.txtDigit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(553, 83);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Cột Repetition 1";
            // 
            // txtRepetition1
            // 
            this.txtRepetition1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRepetition1.Location = new System.Drawing.Point(577, 99);
            this.txtRepetition1.MaxLength = 1;
            this.txtRepetition1.Name = "txtRepetition1";
            this.txtRepetition1.Size = new System.Drawing.Size(34, 20);
            this.txtRepetition1.TabIndex = 6;
            this.txtRepetition1.Text = "I";
            this.txtRepetition1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 133);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Cột Condition 1";
            // 
            // txtCondition1
            // 
            this.txtCondition1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCondition1.Location = new System.Drawing.Point(31, 149);
            this.txtCondition1.MaxLength = 1;
            this.txtCondition1.Name = "txtCondition1";
            this.txtCondition1.Size = new System.Drawing.Size(34, 20);
            this.txtCondition1.TabIndex = 6;
            this.txtCondition1.Text = "K";
            this.txtCondition1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(92, 133);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Cột Condition 2";
            // 
            // txtCondition2
            // 
            this.txtCondition2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCondition2.Location = new System.Drawing.Point(116, 149);
            this.txtCondition2.MaxLength = 1;
            this.txtCondition2.Name = "txtCondition2";
            this.txtCondition2.Size = new System.Drawing.Size(34, 20);
            this.txtCondition2.TabIndex = 6;
            this.txtCondition2.Text = "L";
            this.txtCondition2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(177, 133);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Cột Condition 3";
            // 
            // txtCondition3
            // 
            this.txtCondition3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCondition3.Location = new System.Drawing.Point(201, 149);
            this.txtCondition3.MaxLength = 1;
            this.txtCondition3.Name = "txtCondition3";
            this.txtCondition3.Size = new System.Drawing.Size(34, 20);
            this.txtCondition3.TabIndex = 6;
            this.txtCondition3.Text = "M";
            this.txtCondition3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(262, 133);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Cột Code";
            // 
            // txtCode
            // 
            this.txtCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCode.Location = new System.Drawing.Point(269, 149);
            this.txtCode.MaxLength = 1;
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(34, 20);
            this.txtCode.TabIndex = 6;
            this.txtCode.Text = "V";
            this.txtCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(319, 133);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(58, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Cột Format";
            // 
            // txtFormat
            // 
            this.txtFormat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFormat.Location = new System.Drawing.Point(326, 149);
            this.txtFormat.MaxLength = 1;
            this.txtFormat.Name = "txtFormat";
            this.txtFormat.Size = new System.Drawing.Size(34, 20);
            this.txtFormat.TabIndex = 6;
            this.txtFormat.Text = "W";
            this.txtFormat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(379, 133);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Cột ReferenceDB";
            // 
            // txtReferenceDB
            // 
            this.txtReferenceDB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtReferenceDB.Location = new System.Drawing.Point(397, 149);
            this.txtReferenceDB.MaxLength = 1;
            this.txtReferenceDB.Name = "txtReferenceDB";
            this.txtReferenceDB.Size = new System.Drawing.Size(34, 20);
            this.txtReferenceDB.TabIndex = 6;
            this.txtReferenceDB.Text = "X";
            this.txtReferenceDB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(476, 133);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(123, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Cột Input Condition Nhật";
            // 
            // txtInputConditionJP
            // 
            this.txtInputConditionJP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInputConditionJP.Location = new System.Drawing.Point(510, 149);
            this.txtInputConditionJP.MaxLength = 1;
            this.txtInputConditionJP.Name = "txtInputConditionJP";
            this.txtInputConditionJP.Size = new System.Drawing.Size(34, 20);
            this.txtInputConditionJP.TabIndex = 6;
            this.txtInputConditionJP.Text = "Y";
            this.txtInputConditionJP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(605, 133);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(119, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Cột Input Condition Anh";
            // 
            // txtInputConditionEN
            // 
            this.txtInputConditionEN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInputConditionEN.Location = new System.Drawing.Point(639, 149);
            this.txtInputConditionEN.MaxLength = 1;
            this.txtInputConditionEN.Name = "txtInputConditionEN";
            this.txtInputConditionEN.Size = new System.Drawing.Size(34, 20);
            this.txtInputConditionEN.TabIndex = 6;
            this.txtInputConditionEN.Text = "Z";
            this.txtInputConditionEN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPathFile);
            this.groupBox1.Controls.Add(this.txtInputConditionEN);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtInputConditionJP);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtReferenceDB);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtFormat);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtCode);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtCondition3);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtCondition2);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtCondition1);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtRepetition1);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtDigit);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtAttribute);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtID);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtElementNameVN);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtElementNameEN);
            this.groupBox1.Controls.Add(this.txtColumn);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtElementNameJP);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txtNo);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.cboSheetName);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.btnGetData);
            this.groupBox1.Controls.Add(this.btnReadFile);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.numDongBatDau);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnOpen);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(878, 181);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin cấu hình";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(77, 83);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Cột Column";
            // 
            // txtColumn
            // 
            this.txtColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtColumn.Location = new System.Drawing.Point(96, 99);
            this.txtColumn.MaxLength = 1;
            this.txtColumn.Name = "txtColumn";
            this.txtColumn.Size = new System.Drawing.Size(34, 20);
            this.txtColumn.TabIndex = 6;
            this.txtColumn.Text = "B";
            this.txtColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnGetData
            // 
            this.btnGetData.Location = new System.Drawing.Point(791, 146);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(75, 23);
            this.btnGetData.TabIndex = 4;
            this.btnGetData.Text = "Lấy dữ liệu";
            this.btnGetData.UseVisualStyleBackColor = true;
            this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // gridMain
            // 
            this.gridMain.AlternatingColors = true;
            this.gridMain.AlternatingRowFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(236)))), ((int)(((byte)(252)))));
            gridMain_DesignTimeLayout.LayoutString = resources.GetString("gridMain_DesignTimeLayout.LayoutString");
            this.gridMain.DesignTimeLayout = gridMain_DesignTimeLayout;
            this.gridMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridMain.FilterMode = Janus.Windows.GridEX.FilterMode.Automatic;
            this.gridMain.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridMain.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridMain.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridMain.Location = new System.Drawing.Point(0, 204);
            this.gridMain.Name = "gridMain";
            this.gridMain.Size = new System.Drawing.Size(878, 306);
            this.gridMain.TabIndex = 8;
            this.gridMain.TotalRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridMain.TotalRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.gridMain.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.gridMain_FormattingRow);
            // 
            // lblMessage
            // 
            this.lblMessage.BackColor = System.Drawing.SystemColors.Info;
            this.lblMessage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMessage.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblMessage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblMessage.Location = new System.Drawing.Point(0, 181);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(878, 23);
            this.lblMessage.TabIndex = 9;
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // janusSuperTip1
            // 
            this.janusSuperTip1.AutoPopDelay = 2000;
            this.janusSuperTip1.ImageList = null;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 510);
            this.Controls.Add(this.gridMain);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmMain";
            this.Text = "VNACC Import Excel to Grid";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numDongBatDau)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMain)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPathFile;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numDongBatDau;
        private System.Windows.Forms.Button btnReadFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboSheetName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtElementNameJP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtElementNameEN;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtElementNameVN;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtAttribute;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtDigit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtRepetition1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCondition1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtCondition2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtCondition3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtFormat;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtReferenceDB;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtInputConditionJP;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtInputConditionEN;
        private System.Windows.Forms.GroupBox groupBox1;
        private Janus.Windows.GridEX.GridEX gridMain;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button btnGetData;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtColumn;
        private Janus.Windows.Common.JanusSuperTip janusSuperTip1;
    }
}

