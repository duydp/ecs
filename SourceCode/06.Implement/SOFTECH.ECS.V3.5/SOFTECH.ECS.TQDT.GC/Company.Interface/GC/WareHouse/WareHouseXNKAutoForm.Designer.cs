﻿namespace Company.Interface.GC.WareHouse
{
    partial class WareHouseXNKAutoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem11 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem12 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem13 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem14 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem15 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem16 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout cbbMaKho_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WareHouseXNKAutoForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout cbbTKCo_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout cbbTKNo_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbLoaiHang = new Janus.Windows.EditControls.UIComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.clcNgayHopDong = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbbMaNT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTyGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.clcNgayCT = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.cbbLoaiChungTu = new Janus.Windows.EditControls.UIComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtSoCT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbMaKho = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtNguoiGiao = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenKho = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDienGiai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTen = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiProgressBar1 = new Janus.Windows.EditControls.UIProgressBar();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnProcess = new Janus.Windows.EditControls.UIButton();
            this.lblError = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.ckbTimKiem = new Janus.Windows.EditControls.UICheckBox();
            this.grbtimkiem = new Janus.Windows.EditControls.UIGroupBox();
            this.clcTuNgay = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.ucCalendar1 = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ctrMaHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.btnTimKiem = new Janus.Windows.EditControls.UIButton();
            this.txtNamDangKy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoTKDauTien = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaLoaiHinh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.cbbTrangThai = new Janus.Windows.EditControls.UIComboBox();
            this.cbbPhanLuong = new Janus.Windows.EditControls.UIComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbTKCo = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.cbbTKNo = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaKho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbtimkiem)).BeginInit();
            this.grbtimkiem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTKCo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTKNo)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 733), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 733);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 709);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 709);
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox8);
            this.grbMain.Size = new System.Drawing.Size(1006, 733);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox7.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(1000, 309);
            this.uiGroupBox7.TabIndex = 1;
            this.uiGroupBox7.Text = "Thông tin cấu hình mặc định cho Chứng từ tự động ";
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.cbbLoaiHang);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.clcNgayHopDong);
            this.uiGroupBox4.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox4.Controls.Add(this.label19);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.cbbMaNT);
            this.uiGroupBox4.Controls.Add(this.txtTyGia);
            this.uiGroupBox4.Controls.Add(this.clcNgayCT);
            this.uiGroupBox4.Controls.Add(this.cbbLoaiChungTu);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Controls.Add(this.label26);
            this.uiGroupBox4.Controls.Add(this.label25);
            this.uiGroupBox4.Controls.Add(this.txtSoCT);
            this.uiGroupBox4.Controls.Add(this.label5);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(603, 17);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(394, 289);
            this.uiGroupBox4.TabIndex = 1;
            this.uiGroupBox4.Text = "Thông tin chứng từ";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbLoaiHang
            // 
            this.cbbLoaiHang.BackColor = System.Drawing.Color.White;
            this.cbbLoaiHang.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem11.FormatStyle.Alpha = 0;
            uiComboBoxItem11.IsSeparator = false;
            uiComboBoxItem11.Text = "Nguyên phụ liệu";
            uiComboBoxItem11.Value = "N";
            uiComboBoxItem12.FormatStyle.Alpha = 0;
            uiComboBoxItem12.IsSeparator = false;
            uiComboBoxItem12.Text = "Sản phẩm";
            uiComboBoxItem12.Value = "S";
            uiComboBoxItem13.FormatStyle.Alpha = 0;
            uiComboBoxItem13.IsSeparator = false;
            uiComboBoxItem13.Text = "Thiết bị";
            uiComboBoxItem13.Value = "T";
            uiComboBoxItem14.FormatStyle.Alpha = 0;
            uiComboBoxItem14.IsSeparator = false;
            uiComboBoxItem14.Text = "Hàng mẫu";
            uiComboBoxItem14.Value = "H";
            this.cbbLoaiHang.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem11,
            uiComboBoxItem12,
            uiComboBoxItem13,
            uiComboBoxItem14});
            this.cbbLoaiHang.Location = new System.Drawing.Point(125, 248);
            this.cbbLoaiHang.Name = "cbbLoaiHang";
            this.cbbLoaiHang.Size = new System.Drawing.Size(140, 21);
            this.cbbLoaiHang.TabIndex = 29;
            this.cbbLoaiHang.ValueMember = "ID";
            this.cbbLoaiHang.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbLoaiHang.SelectedIndexChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(20, 253);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Loại hàng :";
            // 
            // clcNgayHopDong
            // 
            this.clcNgayHopDong.CustomFormat = "dd/MM/yyyy";
            this.clcNgayHopDong.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayHopDong.DropDownCalendar.Name = "";
            this.clcNgayHopDong.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayHopDong.Location = new System.Drawing.Point(125, 215);
            this.clcNgayHopDong.Name = "clcNgayHopDong";
            this.clcNgayHopDong.ReadOnly = true;
            this.clcNgayHopDong.Size = new System.Drawing.Size(141, 21);
            this.clcNgayHopDong.TabIndex = 27;
            this.clcNgayHopDong.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoHopDong.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoHopDong.BackColor = System.Drawing.Color.White;
            this.txtSoHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDong.Location = new System.Drawing.Point(126, 180);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.ReadOnly = true;
            this.txtSoHopDong.Size = new System.Drawing.Size(250, 21);
            this.txtSoHopDong.TabIndex = 26;
            this.txtSoHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(20, 184);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 13);
            this.label19.TabIndex = 24;
            this.label19.Text = "Số hợp đồng :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(20, 219);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Ngày hợp đồng :";
            // 
            // cbbMaNT
            // 
            this.cbbMaNT.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cbbMaNT.Appearance.Options.UseBackColor = true;
            this.cbbMaNT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.cbbMaNT.Code = "";
            this.cbbMaNT.ColorControl = System.Drawing.Color.Empty;
            this.cbbMaNT.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbMaNT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.cbbMaNT.IsOnlyWarning = false;
            this.cbbMaNT.IsValidate = true;
            this.cbbMaNT.Location = new System.Drawing.Point(127, 112);
            this.cbbMaNT.Name = "cbbMaNT";
            this.cbbMaNT.Name_VN = "";
            this.cbbMaNT.SetOnlyWarning = false;
            this.cbbMaNT.SetValidate = false;
            this.cbbMaNT.ShowColumnCode = true;
            this.cbbMaNT.ShowColumnName = false;
            this.cbbMaNT.Size = new System.Drawing.Size(140, 21);
            this.cbbMaNT.TabIndex = 18;
            this.cbbMaNT.TagName = "";
            this.cbbMaNT.Where = null;
            this.cbbMaNT.WhereCondition = "";
            // 
            // txtTyGia
            // 
            this.txtTyGia.DecimalDigits = 4;
            this.txtTyGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGia.Location = new System.Drawing.Point(126, 147);
            this.txtTyGia.Name = "txtTyGia";
            this.txtTyGia.Size = new System.Drawing.Size(140, 21);
            this.txtTyGia.TabIndex = 19;
            this.txtTyGia.Text = "1.0000";
            this.txtTyGia.Value = new decimal(new int[] {
            10000,
            0,
            0,
            262144});
            this.txtTyGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // clcNgayCT
            // 
            this.clcNgayCT.CustomFormat = "dd/MM/yyyy";
            this.clcNgayCT.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayCT.DropDownCalendar.Name = "";
            this.clcNgayCT.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayCT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayCT.Location = new System.Drawing.Point(126, 47);
            this.clcNgayCT.Name = "clcNgayCT";
            this.clcNgayCT.Size = new System.Drawing.Size(140, 21);
            this.clcNgayCT.TabIndex = 15;
            this.clcNgayCT.Value = new System.DateTime(2018, 11, 11, 0, 0, 0, 0);
            this.clcNgayCT.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // cbbLoaiChungTu
            // 
            this.cbbLoaiChungTu.BackColor = System.Drawing.Color.White;
            this.cbbLoaiChungTu.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem15.FormatStyle.Alpha = 0;
            uiComboBoxItem15.IsSeparator = false;
            uiComboBoxItem15.Text = "Chứng từ Nhập kho";
            uiComboBoxItem15.Value = "N";
            uiComboBoxItem16.FormatStyle.Alpha = 0;
            uiComboBoxItem16.IsSeparator = false;
            uiComboBoxItem16.Text = "Chứng từ Xuất kho";
            uiComboBoxItem16.Value = "X";
            this.cbbLoaiChungTu.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem15,
            uiComboBoxItem16});
            this.cbbLoaiChungTu.Location = new System.Drawing.Point(126, 80);
            this.cbbLoaiChungTu.Name = "cbbLoaiChungTu";
            this.cbbLoaiChungTu.Size = new System.Drawing.Size(252, 21);
            this.cbbLoaiChungTu.TabIndex = 23;
            this.cbbLoaiChungTu.ValueMember = "ID";
            this.cbbLoaiChungTu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbLoaiChungTu.SelectedValueChanged += new System.EventHandler(this.cbbLoaiChungTu_SelectedValueChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(20, 84);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Loại chứng từ : ";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(20, 151);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(43, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Tỷ giá :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(20, 116);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Mã nguyên tệ :";
            // 
            // txtSoCT
            // 
            this.txtSoCT.BackColor = System.Drawing.Color.White;
            this.txtSoCT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCT.Location = new System.Drawing.Point(126, 18);
            this.txtSoCT.Name = "txtSoCT";
            this.txtSoCT.ReadOnly = true;
            this.txtSoCT.Size = new System.Drawing.Size(252, 21);
            this.txtSoCT.TabIndex = 14;
            this.txtSoCT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoCT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(20, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Số chứng từ :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(20, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Ngày chứng từ :";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.cbbMaKho);
            this.uiGroupBox2.Controls.Add(this.label15);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label24);
            this.uiGroupBox2.Controls.Add(this.label23);
            this.uiGroupBox2.Controls.Add(this.label22);
            this.uiGroupBox2.Controls.Add(this.label16);
            this.uiGroupBox2.Controls.Add(this.txtNguoiGiao);
            this.uiGroupBox2.Controls.Add(this.txtTenKho);
            this.uiGroupBox2.Controls.Add(this.txtDienGiai);
            this.uiGroupBox2.Controls.Add(this.txtDiaChi);
            this.uiGroupBox2.Controls.Add(this.txtMa);
            this.uiGroupBox2.Controls.Add(this.txtTen);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(600, 289);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbMaKho
            // 
            cbbMaKho_DesignTimeLayout.LayoutString = resources.GetString("cbbMaKho_DesignTimeLayout.LayoutString");
            this.cbbMaKho.DesignTimeLayout = cbbMaKho_DesignTimeLayout;
            this.cbbMaKho.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbMaKho.Location = new System.Drawing.Point(157, 182);
            this.cbbMaKho.Name = "cbbMaKho";
            this.cbbMaKho.SelectedIndex = -1;
            this.cbbMaKho.SelectedItem = null;
            this.cbbMaKho.Size = new System.Drawing.Size(410, 21);
            this.cbbMaKho.TabIndex = 12;
            this.cbbMaKho.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbbMaKho.ValueChanged += new System.EventHandler(this.cbbMaKho_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(21, 55);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(119, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Tên người giao / nhận :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Người giao / nhận hàng  :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Địa chỉ :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(21, 221);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(52, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Tên kho :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(21, 186);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(48, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Mã kho :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(22, 151);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Diễn giải :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(21, 120);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Người  nhận / giao :";
            // 
            // txtNguoiGiao
            // 
            this.txtNguoiGiao.BackColor = System.Drawing.Color.White;
            this.txtNguoiGiao.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtNguoiGiao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiGiao.Location = new System.Drawing.Point(157, 116);
            this.txtNguoiGiao.Name = "txtNguoiGiao";
            this.txtNguoiGiao.Size = new System.Drawing.Size(410, 21);
            this.txtNguoiGiao.TabIndex = 10;
            this.txtNguoiGiao.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNguoiGiao.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNguoiGiao.ButtonClick += new System.EventHandler(this.txtNguoiGiao_ButtonClick);
            // 
            // txtTenKho
            // 
            this.txtTenKho.BackColor = System.Drawing.Color.White;
            this.txtTenKho.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenKho.Location = new System.Drawing.Point(157, 217);
            this.txtTenKho.Name = "txtTenKho";
            this.txtTenKho.Size = new System.Drawing.Size(410, 21);
            this.txtTenKho.TabIndex = 13;
            this.txtTenKho.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenKho.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDienGiai
            // 
            this.txtDienGiai.BackColor = System.Drawing.Color.White;
            this.txtDienGiai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienGiai.Location = new System.Drawing.Point(158, 147);
            this.txtDienGiai.Name = "txtDienGiai";
            this.txtDienGiai.Size = new System.Drawing.Size(410, 21);
            this.txtDienGiai.TabIndex = 11;
            this.txtDienGiai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDienGiai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.BackColor = System.Drawing.Color.White;
            this.txtDiaChi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.Location = new System.Drawing.Point(158, 84);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(410, 21);
            this.txtDiaChi.TabIndex = 9;
            this.txtDiaChi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMa
            // 
            this.txtMa.BackColor = System.Drawing.Color.White;
            this.txtMa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMa.Location = new System.Drawing.Point(157, 21);
            this.txtMa.Name = "txtMa";
            this.txtMa.Size = new System.Drawing.Size(158, 21);
            this.txtMa.TabIndex = 7;
            this.txtMa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTen
            // 
            this.txtTen.BackColor = System.Drawing.Color.White;
            this.txtTen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTen.Location = new System.Drawing.Point(157, 51);
            this.txtTen.Name = "txtTen";
            this.txtTen.Size = new System.Drawing.Size(410, 21);
            this.txtTen.TabIndex = 8;
            this.txtTen.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.uiProgressBar1);
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Controls.Add(this.btnProcess);
            this.uiGroupBox3.Controls.Add(this.lblError);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 680);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1000, 50);
            this.uiGroupBox3.TabIndex = 4;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiProgressBar1
            // 
            this.uiProgressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiProgressBar1.Location = new System.Drawing.Point(12, 21);
            this.uiProgressBar1.Name = "uiProgressBar1";
            this.uiProgressBar1.Size = new System.Drawing.Size(540, 17);
            this.uiProgressBar1.TabIndex = 26;
            this.uiProgressBar1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(891, 18);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 23);
            this.btnClose.TabIndex = 25;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProcess.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcess.Image = ((System.Drawing.Image)(resources.GetObject("btnProcess.Image")));
            this.btnProcess.Location = new System.Drawing.Point(768, 18);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(117, 23);
            this.btnProcess.TabIndex = 24;
            this.btnProcess.Text = "Thực hiện";
            this.btnProcess.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // lblError
            // 
            this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(558, 23);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(65, 13);
            this.lblError.TabIndex = 9;
            this.lblError.Text = "{Process}";
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.AutoScroll = true;
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.uiGroupBox1);
            this.uiGroupBox8.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox8.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox8.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox8.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(1006, 733);
            this.uiGroupBox8.TabIndex = 6;
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.UseDefault;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.grList);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 363);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1000, 317);
            this.uiGroupBox1.TabIndex = 6;
            this.uiGroupBox1.Text = "Danh sách tờ khai";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grList
            // 
            this.grList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grList.CardViewGridlines = Janus.Windows.GridEX.CardViewGridlines.Both;
            this.grList.ColumnAutoResize = true;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grList.FrozenColumns = 5;
            this.grList.GroupByBoxVisible = false;
            this.grList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Hierarchical = true;
            this.grList.Location = new System.Drawing.Point(3, 135);
            this.grList.Name = "grList";
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.ScrollBars = Janus.Windows.GridEX.ScrollBars.Both;
            this.grList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelection;
            this.grList.Size = new System.Drawing.Size(994, 179);
            this.grList.TabIndex = 3;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grList_LoadingRow);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.ckbTimKiem);
            this.uiGroupBox5.Controls.Add(this.grbtimkiem);
            this.uiGroupBox5.Controls.Add(this.ctrMaHaiQuan);
            this.uiGroupBox5.Controls.Add(this.btnTimKiem);
            this.uiGroupBox5.Controls.Add(this.txtNamDangKy);
            this.uiGroupBox5.Controls.Add(this.txtSoTKDauTien);
            this.uiGroupBox5.Controls.Add(this.txtSoHoaDon);
            this.uiGroupBox5.Controls.Add(this.txtMaLoaiHinh);
            this.uiGroupBox5.Controls.Add(this.label8);
            this.uiGroupBox5.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox5.Controls.Add(this.label11);
            this.uiGroupBox5.Controls.Add(this.label17);
            this.uiGroupBox5.Controls.Add(this.label20);
            this.uiGroupBox5.Controls.Add(this.label21);
            this.uiGroupBox5.Controls.Add(this.label27);
            this.uiGroupBox5.Controls.Add(this.cbbTrangThai);
            this.uiGroupBox5.Controls.Add(this.cbbPhanLuong);
            this.uiGroupBox5.Controls.Add(this.label28);
            this.uiGroupBox5.Controls.Add(this.label29);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox5.Location = new System.Drawing.Point(3, 17);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(994, 118);
            this.uiGroupBox5.TabIndex = 2;
            this.uiGroupBox5.Text = "Tim kiếm";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // ckbTimKiem
            // 
            this.ckbTimKiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbTimKiem.Location = new System.Drawing.Point(684, 18);
            this.ckbTimKiem.Margin = new System.Windows.Forms.Padding(0);
            this.ckbTimKiem.Name = "ckbTimKiem";
            this.ckbTimKiem.Size = new System.Drawing.Size(15, 15);
            this.ckbTimKiem.TabIndex = 21;
            this.ckbTimKiem.VisualStyleManager = this.vsmMain;
            // 
            // grbtimkiem
            // 
            this.grbtimkiem.Controls.Add(this.clcTuNgay);
            this.grbtimkiem.Controls.Add(this.ucCalendar1);
            this.grbtimkiem.Controls.Add(this.label3);
            this.grbtimkiem.Controls.Add(this.label7);
            this.grbtimkiem.Enabled = false;
            this.grbtimkiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbtimkiem.Location = new System.Drawing.Point(689, 19);
            this.grbtimkiem.Name = "grbtimkiem";
            this.grbtimkiem.Size = new System.Drawing.Size(200, 84);
            this.grbtimkiem.TabIndex = 20;
            this.grbtimkiem.VisualStyleManager = this.vsmMain;
            // 
            // clcTuNgay
            // 
            this.clcTuNgay.Location = new System.Drawing.Point(87, 20);
            this.clcTuNgay.Name = "clcTuNgay";
            this.clcTuNgay.ReadOnly = false;
            this.clcTuNgay.Size = new System.Drawing.Size(90, 21);
            this.clcTuNgay.TabIndex = 19;
            this.clcTuNgay.TagName = "";
            this.clcTuNgay.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcTuNgay.WhereCondition = "";
            // 
            // ucCalendar1
            // 
            this.ucCalendar1.Location = new System.Drawing.Point(87, 50);
            this.ucCalendar1.Name = "ucCalendar1";
            this.ucCalendar1.ReadOnly = false;
            this.ucCalendar1.Size = new System.Drawing.Size(90, 21);
            this.ucCalendar1.TabIndex = 19;
            this.ucCalendar1.TagName = "";
            this.ucCalendar1.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ucCalendar1.WhereCondition = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Từ ngày";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Đến ngày";
            // 
            // ctrMaHaiQuan
            // 
            this.ctrMaHaiQuan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaHaiQuan.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrMaHaiQuan.Appearance.Options.UseBackColor = true;
            this.ctrMaHaiQuan.Appearance.Options.UseFont = true;
            this.ctrMaHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrMaHaiQuan.Code = "";
            this.ctrMaHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaHaiQuan.IsOnlyWarning = false;
            this.ctrMaHaiQuan.IsValidate = true;
            this.ctrMaHaiQuan.Location = new System.Drawing.Point(158, 19);
            this.ctrMaHaiQuan.Name = "ctrMaHaiQuan";
            this.ctrMaHaiQuan.Name_VN = "";
            this.ctrMaHaiQuan.SetOnlyWarning = false;
            this.ctrMaHaiQuan.SetValidate = false;
            this.ctrMaHaiQuan.ShowColumnCode = true;
            this.ctrMaHaiQuan.ShowColumnName = true;
            this.ctrMaHaiQuan.Size = new System.Drawing.Size(309, 26);
            this.ctrMaHaiQuan.TabIndex = 18;
            this.ctrMaHaiQuan.TagCode = "";
            this.ctrMaHaiQuan.TagName = "";
            this.ctrMaHaiQuan.Where = null;
            this.ctrMaHaiQuan.WhereCondition = "";
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiem.Image = ((System.Drawing.Image)(resources.GetObject("btnTimKiem.Image")));
            this.btnTimKiem.ImageIndex = 4;
            this.btnTimKiem.ImageSize = new System.Drawing.Size(20, 20);
            this.btnTimKiem.Location = new System.Drawing.Point(895, 73);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(90, 23);
            this.btnTimKiem.TabIndex = 17;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // txtNamDangKy
            // 
            this.txtNamDangKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamDangKy.Location = new System.Drawing.Point(552, 22);
            this.txtNamDangKy.Name = "txtNamDangKy";
            this.txtNamDangKy.Size = new System.Drawing.Size(112, 21);
            this.txtNamDangKy.TabIndex = 10;
            this.txtNamDangKy.Text = "2018";
            this.txtNamDangKy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtNamDangKy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNamDangKy.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // txtSoTKDauTien
            // 
            this.txtSoTKDauTien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTKDauTien.Location = new System.Drawing.Point(552, 84);
            this.txtSoTKDauTien.Name = "txtSoTKDauTien";
            this.txtSoTKDauTien.Size = new System.Drawing.Size(112, 21);
            this.txtSoTKDauTien.TabIndex = 10;
            this.txtSoTKDauTien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTKDauTien.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // txtSoHoaDon
            // 
            this.txtSoHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHoaDon.Location = new System.Drawing.Point(341, 84);
            this.txtSoHoaDon.Name = "txtSoHoaDon";
            this.txtSoHoaDon.Size = new System.Drawing.Size(126, 21);
            this.txtSoHoaDon.TabIndex = 10;
            this.txtSoHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHoaDon.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // txtMaLoaiHinh
            // 
            this.txtMaLoaiHinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaLoaiHinh.Location = new System.Drawing.Point(158, 84);
            this.txtMaLoaiHinh.Name = "txtMaLoaiHinh";
            this.txtMaLoaiHinh.Size = new System.Drawing.Size(109, 21);
            this.txtMaLoaiHinh.TabIndex = 10;
            this.txtMaLoaiHinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaLoaiHinh.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(484, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "TK đầu tiên";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.Location = new System.Drawing.Point(341, 52);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(126, 21);
            this.txtSoToKhai.TabIndex = 10;
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoToKhai.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(273, 88);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Số hóa đơn";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(484, 26);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 13);
            this.label17.TabIndex = 9;
            this.label17.Text = "Năm đăng ký";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(21, 88);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(63, 13);
            this.label20.TabIndex = 9;
            this.label20.Text = "Mã loại hình";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(484, 56);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 13);
            this.label21.TabIndex = 15;
            this.label21.Text = "Phân luồng";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(273, 56);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(54, 13);
            this.label27.TabIndex = 9;
            this.label27.Text = "Số tờ khai";
            // 
            // cbbTrangThai
            // 
            this.cbbTrangThai.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Chưa khai báo";
            uiComboBoxItem1.Value = "0";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Đã khai báo";
            uiComboBoxItem2.Value = "1";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Đã xác nhận khai báo";
            uiComboBoxItem3.Value = "2";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Thông quan";
            uiComboBoxItem4.Value = "3";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Đang sửa";
            uiComboBoxItem5.Value = "4";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Đã hủy";
            uiComboBoxItem6.Value = "6";
            this.cbbTrangThai.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6});
            this.cbbTrangThai.Location = new System.Drawing.Point(158, 52);
            this.cbbTrangThai.Name = "cbbTrangThai";
            this.cbbTrangThai.Size = new System.Drawing.Size(109, 21);
            this.cbbTrangThai.TabIndex = 16;
            this.cbbTrangThai.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbTrangThai.SelectedValueChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // cbbPhanLuong
            // 
            this.cbbPhanLuong.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbPhanLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "---Tất cả---";
            uiComboBoxItem7.Value = "0";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Luồng Xanh";
            uiComboBoxItem8.Value = "1";
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Luồng Vàng";
            uiComboBoxItem9.Value = "2";
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Luồng Đỏ";
            uiComboBoxItem10.Value = "3";
            this.cbbPhanLuong.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem7,
            uiComboBoxItem8,
            uiComboBoxItem9,
            uiComboBoxItem10});
            this.cbbPhanLuong.Location = new System.Drawing.Point(552, 52);
            this.cbbPhanLuong.Name = "cbbPhanLuong";
            this.cbbPhanLuong.Size = new System.Drawing.Size(112, 21);
            this.cbbPhanLuong.TabIndex = 16;
            this.cbbPhanLuong.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbPhanLuong.SelectedValueChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(21, 26);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(92, 13);
            this.label28.TabIndex = 13;
            this.label28.Text = "Cơ quan Hải quan";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(21, 56);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(56, 13);
            this.label29.TabIndex = 13;
            this.label29.Text = "Trạng thái";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.cbbTKCo);
            this.uiGroupBox6.Controls.Add(this.cbbTKNo);
            this.uiGroupBox6.Controls.Add(this.label9);
            this.uiGroupBox6.Controls.Add(this.label10);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 317);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(1000, 46);
            this.uiGroupBox6.TabIndex = 3;
            this.uiGroupBox6.Text = "Thông tin hàng hóa";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbTKCo
            // 
            cbbTKCo_DesignTimeLayout.LayoutString = resources.GetString("cbbTKCo_DesignTimeLayout.LayoutString");
            this.cbbTKCo.DesignTimeLayout = cbbTKCo_DesignTimeLayout;
            this.cbbTKCo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbTKCo.Location = new System.Drawing.Point(729, 17);
            this.cbbTKCo.Name = "cbbTKCo";
            this.cbbTKCo.SelectedIndex = -1;
            this.cbbTKCo.SelectedItem = null;
            this.cbbTKCo.Size = new System.Drawing.Size(252, 21);
            this.cbbTKCo.TabIndex = 14;
            this.cbbTKCo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // cbbTKNo
            // 
            cbbTKNo_DesignTimeLayout.LayoutString = resources.GetString("cbbTKNo_DesignTimeLayout.LayoutString");
            this.cbbTKNo.DesignTimeLayout = cbbTKNo_DesignTimeLayout;
            this.cbbTKNo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbTKNo.Location = new System.Drawing.Point(160, 17);
            this.cbbTKNo.Name = "cbbTKNo";
            this.cbbTKNo.SelectedIndex = -1;
            this.cbbTKNo.SelectedItem = null;
            this.cbbTKNo.Size = new System.Drawing.Size(410, 21);
            this.cbbTKNo.TabIndex = 13;
            this.cbbTKNo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(24, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Tài khoản Nợ :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(622, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Tài khoản Có :";
            // 
            // WareHouseXNKAutoForm
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1212, 739);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "WareHouseXNKAutoForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tạo chứng từ Xuất nhập kho tự động";
            this.Load += new System.EventHandler(this.WareHouseXNKAutoForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaKho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbtimkiem)).EndInit();
            this.grbtimkiem.ResumeLayout(false);
            this.grbtimkiem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTKCo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbTKNo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory cbbMaNT;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTyGia;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayCT;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiChungTu;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCT;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbMaKho;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiGiao;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenKho;
        private Janus.Windows.GridEX.EditControls.EditBox txtDienGiai;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMa;
        private Janus.Windows.GridEX.EditControls.EditBox txtTen;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIProgressBar uiProgressBar1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnProcess;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayHopDong;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblError;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.GridEX grList;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UICheckBox ckbTimKiem;
        private Janus.Windows.EditControls.UIGroupBox grbtimkiem;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcTuNgay;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar ucCalendar1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaHaiQuan;
        private Janus.Windows.EditControls.UIButton btnTimKiem;
        private Janus.Windows.GridEX.EditControls.EditBox txtNamDangKy;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTKDauTien;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHoaDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaLoaiHinh;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.EditControls.UIComboBox cbbTrangThai;
        private Janus.Windows.EditControls.UIComboBox cbbPhanLuong;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbTKCo;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbTKNo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiHang;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private System.Windows.Forms.Label label19;
    }
}
