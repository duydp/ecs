using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.GC.BLL.KDT.GC
{
	public partial class KDT_GC_IDHopDong
	{
        public int InsertUpdateTransactionNew(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_IDHopDong_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@ID_CU", SqlDbType.BigInt, this._ID_CU);
            this.db.AddInParameter(dbCommand, "@ID_MOI", SqlDbType.BigInt, this._ID_MOI);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdateNew(KDT_GC_IDHopDongCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (KDT_GC_IDHopDong item in collection)
                    {
                        if (item.InsertUpdateTransactionNew(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }		
	}	
}