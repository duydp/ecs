using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using System.Data;

namespace Company.KD.BLL.Utils
{
    public class KdtObj
    {
        public long SoLuong;
        public long TrangThai;

    }
    public partial class KDT
    {
        public static byte[] GetBytes(string s)
        {
            byte[] bs = new byte[s.Length];
            for (int i = 0; i < s.Length; i++)
                bs[i] = (byte)s[i];
            return bs;
        }
        //---
        public static string GetString(byte[] bs)
        {
            string s = "";
            for (int i = 0; i < bs.Length; i++)
                s += (char)bs[i];
            return s;
        }
        public static KdtObj getSoLuongDoanhNghiep()
        {            

            KdtObj obj = new KdtObj();
            List<KDT> listDN = KDT.SelectCollectionAll();
            string keyString = "ECSDaiLyKD12345abcde";
            foreach (KDT k in listDN)
            {
                byte[] key = GetBytes(keyString).Clone() as byte[];
				Blowfish bl = new Blowfish(key);
                byte[] soluong = GetBytes(k.SoLuong).Clone() as byte[];
                byte[] trangthai = GetBytes(k.TrangThai).Clone() as byte[];

                obj.SoLuong =Convert.ToInt64(GetString(bl.Decipher(soluong)));
                obj.TrangThai = Convert.ToInt64(GetString(bl.Decipher(trangthai)));                
            }
            return obj;
        }
    }
}
