﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Người khai HQ
    /// </summary>
    public class Agent:NameBase
    {
        /// <summary>
        /// 1. Đại lý làm thủ tục Hải quan 
        /// 2. Ủy thác 
        /// 3. Người khai hải quan 
        /// 4. Người chịu trách nhiệm nộp thuế
        /// 5. Mã MID
        /// n1
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }

    }
}
