using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class BanKhaiThongTinHoaDon : DevExpress.XtraReports.UI.XtraReport
    {
        public BanKhaiThongTinHoaDon()
        {
            InitializeComponent();
        }
        public void BindReport(VAL0870_IVA Val087)
        {
            lblSoTiepNhanHoaDon.Text = Val087.NIV.GetValue().ToString();
            lblPhanLoaiXuatNhapKhau.Text = Val087.YNK.GetValue().ToString();
            lblMaDaiLyHaiQuan.Text = Val087.TII.GetValue().ToString();
            lblSoHoaDon.Text = Val087.IVN.GetValue().ToString();
            if (Convert.ToDateTime(Val087.IVD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayLapHoaDon.Text = Convert.ToDateTime(Val087.IVD.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayLapHoaDon.Text = "";
            lblDiaDiemLapHoaDon.Text = Val087.IVB.GetValue().ToString();
            lblPhuongThucThanhToan.Text = Val087.HOS.GetValue().ToString();
            lblMaNguoiXuatNhapKhau.Text = Val087.IMC.GetValue().ToString();
            lblTenNguoiXuatNhapKhau.Text = Val087.IMN.GetValue().ToString();
            lblMaBuuChinhXuatNhapKhau.Text = Val087.IMY.GetValue().ToString();
            lblDiaChiXuatNhapKhau.Text = Val087.IMA.GetValue().ToString();
            lblSoDienThoaiXuatNhapKhau.Text = Val087.IMT.GetValue().ToString();
            lblNguoiLapHoaDon.Text = Val087.SAA.GetValue().ToString();
            lblMaNguoiNhanGui.Text = Val087.EPC.GetValue().ToString();
            lblTenNguoiNhanGui.Text = Val087.EPN.GetValue().ToString();
            lblMaBuuChinhNguoiNhanGui.Text = Val087.EP1.GetValue().ToString();
            lblDiaChiNguoiNhanGui1.Text = Val087.EPA.GetValue().ToString();
            lblDiaChiNguoiNhanGui2.Text = Val087.EP2.GetValue().ToString();
            lblDiaChiNguoiNhanGui3.Text = Val087.EP3.GetValue().ToString();
            lblDiaChiNguoiNhanGui4.Text = Val087.EP4.GetValue().ToString();
            lblSoDienThoaiNguoiNhanGui.Text = Val087.EP6.GetValue().ToString();
            lblNuocSoTaiNguoiNhanGui.Text = Val087.EEE.GetValue().ToString();
            lblMaKyHieu.Text = Val087.KNO.GetValue().ToString();
         //   lblPhanLoaiVanChuyen.Text = Val087.TTP.GetValue().ToString();
            lblTenPhuongTienVanChuyen.Text = Val087.VSS.GetValue().ToString();
            lblSoHieuChuyenDi.Text = Val087.VNO.GetValue().ToString();
            lblMaDiaDiemXepHang.Text = Val087.PSC.GetValue().ToString();
            lblTenDiaDiemXepHang.Text = Val087.PSN.GetValue().ToString();
            if (Convert.ToDateTime(Val087.PSD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblThoiKyXepHang.Text = Convert.ToDateTime(Val087.PSD.GetValue()).ToString("dd/MM/yyyy");
            else
                lblThoiKyXepHang.Text = "";
            lblMaDiaDiemDoHang.Text = Val087.DST.GetValue().ToString();
            lblTenDiaDiemDoHang.Text = Val087.DCN.GetValue().ToString();
            lblMaDiaDiemTrungChuyen.Text = Val087.KYC.GetValue().ToString();
            lblTenDiaDiemTrungChuyen.Text = Val087.KYN.GetValue().ToString();
            
            //lblTongTrongLuongHangGross.Text = Val087.GWJ.GetValue().ToString();
            //lblMaDonViTinhTongTrongLuong.Text = Val087.GWT.GetValue().ToString();
            //lblTrongLuongThuan.Text = Val087.JWJ.GetValue().ToString();
            //lblMaDonViTinhTrongLuongThuan.Text = Val087.JWT.GetValue().ToString();
            //lblTongTheTich.Text = Val087.STJ.GetValue().ToString();
            //lblMaDonViTinhTongTheTich.Text = Val087.STT.GetValue().ToString();
            //lblTongSoKienHang.Text = Val087.NOJ.GetValue().ToString();
            //lblMaDonViTinhTongSoKienHang.Text = Val087.NOT.GetValue().ToString();
            //lblGhiChuCuaChuHang.Text = Val087.NT3.GetValue().ToString();
            //lblSoPL.Text = Val087.PLN.GetValue().ToString();
            //lblNganHangLC.Text = Val087.LCB.GetValue().ToString();
            //lblTriGiaFOB.Text = Val087.FON.GetValue().ToString();
            //lblMaTienTeCuaFOB.Text = Val087.FOT.GetValue().ToString();
            //lblSoTienDieuChinhFOB.Text = Val087.FKK.GetValue().ToString();
            //lblMaTienTeSoTienDieuChinhFOB.Text = Val087.FKT.GetValue().ToString();
            //lblPhiVanChuyen.Text = Val087.FR3.GetValue().ToString();
            //lblMTTPhiVanChuyen.Text = Val087.FR2.GetValue().ToString();
            //lblNoiThanhToanPhiVanChuyen.Text = Val087.FR4.GetValue().ToString();
            //lblChiPhiXepHang1.Text = Val087.FS1.GetValue().ToString();
            //lblMTTChiPhiXepHang1.Text = Val087.FT1.GetValue().ToString();
            //lblLoaiChiPhiXepHang1.Text = Val087.FH1.GetValue().ToString();
            //lblChiPhiXepHang2.Text = Val087.FS2.GetValue().ToString();
            //lblMTTChiPhiXepHang2.Text = Val087.FT2.GetValue().ToString();
            //lblLoaiChiPhiXepHang2.Text = Val087.FH2.GetValue().ToString();
            //lblPhiVanChuyenDuongBoNoiDia.Text = Val087.NUH.GetValue().ToString();
            //lblMTTPhiVanChuyenDuongBoNoiDia.Text = Val087.NTK.GetValue().ToString();
            //lblPhiBaoHiem.Text = Val087.IN3.GetValue().ToString();
            //lblMTTCuaPhiBaoHiem.Text = Val087.IN2.GetValue().ToString();
            //lblSoTienDieuChinhPhiBaoHiem.Text = Val087.IK1.GetValue().ToString();
            //lblMTTSoTienDieuChinhPhiBH.Text = Val087.IK2.GetValue().ToString();
            //lblSoTienKhauTru.Text = Val087.NBG.GetValue().ToString();
            //lblMTTCuaSoTienKhauTru.Text = Val087.NBC.GetValue().ToString();
            //lblLoaiKhauTru.Text = Val087.NBS.GetValue().ToString();
            //lblSoTienDieuChinhKhac.Text = Val087.SKG.GetValue().ToString();
            //lblMTTSoTienDieuChinhKhac.Text = Val087.SKC.GetValue().ToString();
            //lblLoaiSoTienDieuChinhKhac.Text = Val087.SKS.GetValue().ToString();
            //lblTongTriGiaHoaDon.Text = Val087.IP4.GetValue().ToString();
            //lblMTTTongTriGiaHoaDon.Text = Val087.IP3.GetValue().ToString();
            //lblDieuKienGiaHoaDon.Text = Val087.IP2.GetValue().ToString();
            //lblDiaDiemGiaoHang.Text = Val087.HWT.GetValue().ToString();
            //lblGhiChuDacBiet.Text = Val087.NT1.GetValue().ToString();
            //lblTongSoDongHang.Text = Val087.SNM.GetValue().ToString();
            //lblSoDong.Text = Val087.RNO.GetValue().ToString();
            //lblMaHangHoa.Text = Val087.SNO.GetValue().ToString();
            //lblMaSoHangHoa.Text = Val087.CMD.GetValue().ToString();
            //lblMoTaHangHoa.Text = Val087.CMN.GetValue().ToString();
            //lblMaNuocXuatXu.Text = Val087.ORC.GetValue().ToString();
            //lblTenNuocXuatXu.Text = Val087.ORN.GetValue().ToString();
            //lblSoKienHang.Text = Val087.KBN.GetValue().ToString();
            //lblSoLuong1.Text = Val087.QN1.GetValue().ToString();
            //lblMaDVTSoLuong1.Text = Val087.QT1.GetValue().ToString();
            //lblSoLuong2.Text = Val087.QN2.GetValue().ToString();
            //lblMaDVTSoLuong2.Text = Val087.QT2.GetValue().ToString();
            //lblDonGiaHoaDon.Text = Val087.TNK.GetValue().ToString();
            //lblMTTDonGiaHoaDon.Text = Val087.TNC.GetValue().ToString();
            //lblDonViDonGiaHoaDon.Text = Val087.TSC.GetValue().ToString();
            //lblTriGiaHoaDon.Text = Val087.KKT.GetValue().ToString();
            //lblMTTGiaTien.Text = Val087.KKC.GetValue().ToString();
            //lblLoaiKhauTru_Hang.Text = Val087.NSR.GetValue().ToString();
            //lblSoTienKhauTru_Hang.Text = Val087.NBG.GetValue().ToString();
            //lblMTTSoTienKhauTru_Hang.Text = Val087.NBC.GetValue().ToString();
            BindReportPhanLoai(Val087);
        }
       
        public void BindReportPhanLoai(VAL0870_IVA Val087)
        {
            try
            {

                DataTable dt = new DataTable();
                dt.Columns.Add("STT", typeof(string));
                dt.Columns.Add("MaPhanLoai", typeof(string));
                dt.Columns.Add("PhanLoaiSo", typeof(string));
                dt.Columns.Add("NgayThangNam", typeof(string));

                for (int i = 0; i < Val087.NOM.listAttribute[0].ListValue.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["STT"] = "("+(i + 1).ToString()+")";                   
                    dr["MaPhanLoai"] = Val087.NOM.listAttribute[0].GetValueCollection(i).ToString().Trim();               
                    dr["PhanLoaiSo"] = Val087.NOM.listAttribute[1].GetValueCollection(i).ToString().Trim();
                    if (Convert.ToDateTime(Val087.NOM.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                    {
                        dr["NgayThangNam"] = Val087.NOM.listAttribute[2].GetValueCollection(i).ToString().Trim();
                    }
                    else
                    {
                        dr["NgayThangNam"] = "";
                    }
                    dt.Rows.Add(dr);
                }

                //while (dt.Rows.Count < 4)
                //{


                //    DataRow dr = dt.NewRow();
                //    dr["SacThue"] = "";
                //    dr["Chuong"] = chuong[dt.Rows.Count];
                //    dr["TieuMuc"] = "";
                //    dr["SoTienThue"] = "";
                //    dr["SoTienMien"] = "";
                //    dr["SoTienGiam"] = "";
                //    dr["SoThuePhaiNop"] = "";
                //    dt.Rows.Add(dr);
                //}
                DetailReport.DataSource = dt;
                lblSTTChiThiHQ.DataBindings.Add("Text", DetailReport.DataSource, "STT");
                lblPhanBietSo.DataBindings.Add("Text", DetailReport.DataSource, "MaPhanLoai");
                lblSo.DataBindings.Add("Text", DetailReport.DataSource, "PhanLoaiSo");
                lblNgayThangNam.DataBindings.Add("Text", DetailReport.DataSource, "NgayThangNam");

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0")
                lbl.Text = "";
        }
    }
}
