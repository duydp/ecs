﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class EDA
    {

        public List<EDA_HANGHOA> ListHangHoa { get; set; }

        public StringBuilder BuilEdiMessagesEDA(StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<EDA>(StrBuild, true, GlobalVNACC.PathConfig, "EDA");
            foreach (EDA_HANGHOA item in ListHangHoa)
            {
                item.BuildEdiMessages<EDA_HANGHOA>(StrBuild, true, GlobalVNACC.PathConfig, "EDA_HANGHOA");
            }
            return str;
        }
        public StringBuilder BuilEdiMessagesEDA01(StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<EDA>(StrBuild, true, GlobalVNACC.PathConfig, "EDA01");
            foreach (EDA_HANGHOA item in ListHangHoa)
            {
                item.BuildEdiMessages<EDA_HANGHOA>(StrBuild, true, GlobalVNACC.PathConfig, "EDA_HANGHOA");
            }
            return str;
        }
    }
}
