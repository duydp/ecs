/*
Run this script on:

        192.168.72.100.ECS_TQDT_SXXK_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_SXXK_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 08/09/2012 4:14:07 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[t_KDT_ToKhaiMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_ToKhaiMauDich] ADD
[ChucVu] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_ToKhaiMauDich_LoadBy]'
GO
  
  
  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_Load]  
-- Database: ECS_SXXKTQ  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, January 07, 2010  
------------------------------------------------------------------------------------------------------------------------  
  
ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_LoadBy]  
 @MaHaiQuan char(6),  
 @MaDoanhNghiep varchar(14),  
 @SoToKhai int,  
 @NamDK int,  
 @MaLoaiHinh char(5)  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
SELECT  
 [ID],  
 [SoTiepNhan],  
 [NgayTiepNhan],  
 [MaHaiQuan],  
 [SoToKhai],  
 [MaLoaiHinh],  
 [NgayDangKy],  
 [MaDoanhNghiep],  
 [TenDoanhNghiep],  
 [MaDaiLyTTHQ],  
 [TenDaiLyTTHQ],  
 [TenDonViDoiTac],  
 [ChiTietDonViDoiTac],  
 [SoGiayPhep],  
 [NgayGiayPhep],  
 [NgayHetHanGiayPhep],  
 [SoHopDong],  
 [NgayHopDong],  
 [NgayHetHanHopDong],  
 [SoHoaDonThuongMai],  
 [NgayHoaDonThuongMai],  
 [PTVT_ID],  
 [SoHieuPTVT],  
 [NgayDenPTVT],  
 [QuocTichPTVT_ID],  
 [LoaiVanDon],  
 [SoVanDon],  
 [NgayVanDon],  
 [NuocXK_ID],  
 [NuocNK_ID],  
 [DiaDiemXepHang],  
 [CuaKhau_ID],  
 [DKGH_ID],  
 [NguyenTe_ID],  
 [TyGiaTinhThue],  
 [TyGiaUSD],  
 [PTTT_ID],  
 [SoHang],  
 [SoLuongPLTK],  
 [TenChuHang],  
 [ChucVu],  
 [SoContainer20],  
 [SoContainer40],  
 [SoKien],  
 [TrongLuong],  
 [TongTriGiaKhaiBao],  
 [TongTriGiaTinhThue],  
 [LoaiToKhaiGiaCong],  
 [LePhiHaiQuan],  
 [PhiBaoHiem],  
 [PhiVanChuyen],  
 [PhiXepDoHang],  
 [PhiKhac],  
 [CanBoDangKy],  
 [QuanLyMay],  
 [TrangThaiXuLy],  
 [LoaiHangHoa],  
 [GiayTo],  
 [PhanLuong],  
 [MaDonViUT],  
 [TenDonViUT],  
 [TrongLuongNet],  
 [SoTienKhoan],  
 [HeSoNhan],  
 [GUIDSTR],  
 [DeXuatKhac],  
 [LyDoSua],  
 [ActionStatus],  
 [GuidReference],  
 [NamDK],  
 [HUONGDAN]  
FROM  
 [dbo].[t_KDT_ToKhaiMauDich]  
WHERE  
 [t_KDT_ToKhaiMauDich].MaHaiQuan = @MaHaiQuan  
 AND [t_KDT_ToKhaiMauDich].MaDoanhNghiep = @MaDoanhNghiep  
 AND [t_KDT_ToKhaiMauDich].SoToKhai = @SoToKhai  
 AND ISNULL([t_KDT_ToKhaiMauDich].NamDK, YEAR([t_KDT_ToKhaiMauDich].NgayDangKy)) = @NamDK  
 AND [t_KDT_ToKhaiMauDich].MaLoaiHinh= @MaLoaiHinh  
  
  
  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_ToKhaiMauDich_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_Insert]
-- Database: ECS_SXXKTQ
-- Author: Ngo Thanh Tung
-- Time created: Thursday, January 07, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(255),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(500),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(4),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(10),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@ChucVu nvarchar(20),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@CanBoDangKy varchar(100),
	@QuanLyMay bit,
	@TrangThaiXuLy int,
	@LoaiHangHoa char(1),
	@GiayTo nvarchar(40),
	@PhanLuong varchar(2),
	@MaDonViUT varchar(14),
	@TenDonViUT nvarchar(50),
	@TrongLuongNet float,
	@SoTienKhoan money,
	@HeSoNhan float,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference nvarchar(500),
	@NamDK int,
	@HUONGDAN nvarchar(max),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_ToKhaiMauDich]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[ChucVu],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[CanBoDangKy],
	[QuanLyMay],
	[TrangThaiXuLy],
	[LoaiHangHoa],
	[GiayTo],
	[PhanLuong],
	[MaDonViUT],
	[TenDonViUT],
	[TrongLuongNet],
	[SoTienKhoan],
	[HeSoNhan],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@MaHaiQuan,
	@SoToKhai,
	@MaLoaiHinh,
	@NgayDangKy,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@MaDaiLyTTHQ,
	@TenDaiLyTTHQ,
	@TenDonViDoiTac,
	@ChiTietDonViDoiTac,
	@SoGiayPhep,
	@NgayGiayPhep,
	@NgayHetHanGiayPhep,
	@SoHopDong,
	@NgayHopDong,
	@NgayHetHanHopDong,
	@SoHoaDonThuongMai,
	@NgayHoaDonThuongMai,
	@PTVT_ID,
	@SoHieuPTVT,
	@NgayDenPTVT,
	@QuocTichPTVT_ID,
	@LoaiVanDon,
	@SoVanDon,
	@NgayVanDon,
	@NuocXK_ID,
	@NuocNK_ID,
	@DiaDiemXepHang,
	@CuaKhau_ID,
	@DKGH_ID,
	@NguyenTe_ID,
	@TyGiaTinhThue,
	@TyGiaUSD,
	@PTTT_ID,
	@SoHang,
	@SoLuongPLTK,
	@TenChuHang,
	@ChucVu,
	@SoContainer20,
	@SoContainer40,
	@SoKien,
	@TrongLuong,
	@TongTriGiaKhaiBao,
	@TongTriGiaTinhThue,
	@LoaiToKhaiGiaCong,
	@LePhiHaiQuan,
	@PhiBaoHiem,
	@PhiVanChuyen,
	@PhiXepDoHang,
	@PhiKhac,
	@CanBoDangKy,
	@QuanLyMay,
	@TrangThaiXuLy,
	@LoaiHangHoa,
	@GiayTo,
	@PhanLuong,
	@MaDonViUT,
	@TenDonViUT,
	@TrongLuongNet,
	@SoTienKhoan,
	@HeSoNhan,
	@GUIDSTR,
	@DeXuatKhac,
	@LyDoSua,
	@ActionStatus,
	@GuidReference,
	@NamDK,
	@HUONGDAN
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_ToKhaiMauDich_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_Update]
-- Database: ECS_SXXKTQ
-- Author: Ngo Thanh Tung
-- Time created: Thursday, January 07, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(255),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(500),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(4),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(10),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@ChucVu nvarchar(20),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@CanBoDangKy varchar(100),
	@QuanLyMay bit,
	@TrangThaiXuLy int,
	@LoaiHangHoa char(1),
	@GiayTo nvarchar(40),
	@PhanLuong varchar(2),
	@MaDonViUT varchar(14),
	@TenDonViUT nvarchar(50),
	@TrongLuongNet float,
	@SoTienKhoan money,
	@HeSoNhan float,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference nvarchar(500),
	@NamDK int,
	@HUONGDAN nvarchar(max)
AS
UPDATE
	[dbo].[t_KDT_ToKhaiMauDich]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaHaiQuan] = @MaHaiQuan,
	[SoToKhai] = @SoToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaDaiLyTTHQ] = @MaDaiLyTTHQ,
	[TenDaiLyTTHQ] = @TenDaiLyTTHQ,
	[TenDonViDoiTac] = @TenDonViDoiTac,
	[ChiTietDonViDoiTac] = @ChiTietDonViDoiTac,
	[SoGiayPhep] = @SoGiayPhep,
	[NgayGiayPhep] = @NgayGiayPhep,
	[NgayHetHanGiayPhep] = @NgayHetHanGiayPhep,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[NgayHetHanHopDong] = @NgayHetHanHopDong,
	[SoHoaDonThuongMai] = @SoHoaDonThuongMai,
	[NgayHoaDonThuongMai] = @NgayHoaDonThuongMai,
	[PTVT_ID] = @PTVT_ID,
	[SoHieuPTVT] = @SoHieuPTVT,
	[NgayDenPTVT] = @NgayDenPTVT,
	[QuocTichPTVT_ID] = @QuocTichPTVT_ID,
	[LoaiVanDon] = @LoaiVanDon,
	[SoVanDon] = @SoVanDon,
	[NgayVanDon] = @NgayVanDon,
	[NuocXK_ID] = @NuocXK_ID,
	[NuocNK_ID] = @NuocNK_ID,
	[DiaDiemXepHang] = @DiaDiemXepHang,
	[CuaKhau_ID] = @CuaKhau_ID,
	[DKGH_ID] = @DKGH_ID,
	[NguyenTe_ID] = @NguyenTe_ID,
	[TyGiaTinhThue] = @TyGiaTinhThue,
	[TyGiaUSD] = @TyGiaUSD,
	[PTTT_ID] = @PTTT_ID,
	[SoHang] = @SoHang,
	[SoLuongPLTK] = @SoLuongPLTK,
	[TenChuHang] = @TenChuHang,
	[ChucVu] = @ChucVu,
	[SoContainer20] = @SoContainer20,
	[SoContainer40] = @SoContainer40,
	[SoKien] = @SoKien,
	[TrongLuong] = @TrongLuong,
	[TongTriGiaKhaiBao] = @TongTriGiaKhaiBao,
	[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
	[LoaiToKhaiGiaCong] = @LoaiToKhaiGiaCong,
	[LePhiHaiQuan] = @LePhiHaiQuan,
	[PhiBaoHiem] = @PhiBaoHiem,
	[PhiVanChuyen] = @PhiVanChuyen,
	[PhiXepDoHang] = @PhiXepDoHang,
	[PhiKhac] = @PhiKhac,
	[CanBoDangKy] = @CanBoDangKy,
	[QuanLyMay] = @QuanLyMay,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[LoaiHangHoa] = @LoaiHangHoa,
	[GiayTo] = @GiayTo,
	[PhanLuong] = @PhanLuong,
	[MaDonViUT] = @MaDonViUT,
	[TenDonViUT] = @TenDonViUT,
	[TrongLuongNet] = @TrongLuongNet,
	[SoTienKhoan] = @SoTienKhoan,
	[HeSoNhan] = @HeSoNhan,
	[GUIDSTR] = @GUIDSTR,
	[DeXuatKhac] = @DeXuatKhac,
	[LyDoSua] = @LyDoSua,
	[ActionStatus] = @ActionStatus,
	[GuidReference] = @GuidReference,
	[NamDK] = @NamDK,
	[HUONGDAN] = @HUONGDAN
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_ToKhaiMauDich_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_InsertUpdate]
-- Database: ECS_SXXKTQ
-- Author: Ngo Thanh Tung
-- Time created: Thursday, January 07, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(255),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(500),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(4),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(10),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@ChucVu nvarchar(20),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 2),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@CanBoDangKy varchar(100),
	@QuanLyMay bit,
	@TrangThaiXuLy int,
	@LoaiHangHoa char(1),
	@GiayTo nvarchar(40),
	@PhanLuong varchar(2),
	@MaDonViUT varchar(14),
	@TenDonViUT nvarchar(50),
	@TrongLuongNet float,
	@SoTienKhoan money,
	@HeSoNhan float,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference nvarchar(500),
	@NamDK int,
	@HUONGDAN nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ToKhaiMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ToKhaiMauDich] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaHaiQuan] = @MaHaiQuan,
			[SoToKhai] = @SoToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaDaiLyTTHQ] = @MaDaiLyTTHQ,
			[TenDaiLyTTHQ] = @TenDaiLyTTHQ,
			[TenDonViDoiTac] = @TenDonViDoiTac,
			[ChiTietDonViDoiTac] = @ChiTietDonViDoiTac,
			[SoGiayPhep] = @SoGiayPhep,
			[NgayGiayPhep] = @NgayGiayPhep,
			[NgayHetHanGiayPhep] = @NgayHetHanGiayPhep,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[NgayHetHanHopDong] = @NgayHetHanHopDong,
			[SoHoaDonThuongMai] = @SoHoaDonThuongMai,
			[NgayHoaDonThuongMai] = @NgayHoaDonThuongMai,
			[PTVT_ID] = @PTVT_ID,
			[SoHieuPTVT] = @SoHieuPTVT,
			[NgayDenPTVT] = @NgayDenPTVT,
			[QuocTichPTVT_ID] = @QuocTichPTVT_ID,
			[LoaiVanDon] = @LoaiVanDon,
			[SoVanDon] = @SoVanDon,
			[NgayVanDon] = @NgayVanDon,
			[NuocXK_ID] = @NuocXK_ID,
			[NuocNK_ID] = @NuocNK_ID,
			[DiaDiemXepHang] = @DiaDiemXepHang,
			[CuaKhau_ID] = @CuaKhau_ID,
			[DKGH_ID] = @DKGH_ID,
			[NguyenTe_ID] = @NguyenTe_ID,
			[TyGiaTinhThue] = @TyGiaTinhThue,
			[TyGiaUSD] = @TyGiaUSD,
			[PTTT_ID] = @PTTT_ID,
			[SoHang] = @SoHang,
			[SoLuongPLTK] = @SoLuongPLTK,
			[TenChuHang] = @TenChuHang,
			[ChucVu] = @ChucVu,
			[SoContainer20] = @SoContainer20,
			[SoContainer40] = @SoContainer40,
			[SoKien] = @SoKien,
			[TrongLuong] = @TrongLuong,
			[TongTriGiaKhaiBao] = @TongTriGiaKhaiBao,
			[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
			[LoaiToKhaiGiaCong] = @LoaiToKhaiGiaCong,
			[LePhiHaiQuan] = @LePhiHaiQuan,
			[PhiBaoHiem] = @PhiBaoHiem,
			[PhiVanChuyen] = @PhiVanChuyen,
			[PhiXepDoHang] = @PhiXepDoHang,
			[PhiKhac] = @PhiKhac,
			[CanBoDangKy] = @CanBoDangKy,
			[QuanLyMay] = @QuanLyMay,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[LoaiHangHoa] = @LoaiHangHoa,
			[GiayTo] = @GiayTo,
			[PhanLuong] = @PhanLuong,
			[MaDonViUT] = @MaDonViUT,
			[TenDonViUT] = @TenDonViUT,
			[TrongLuongNet] = @TrongLuongNet,
			[SoTienKhoan] = @SoTienKhoan,
			[HeSoNhan] = @HeSoNhan,
			[GUIDSTR] = @GUIDSTR,
			[DeXuatKhac] = @DeXuatKhac,
			[LyDoSua] = @LyDoSua,
			[ActionStatus] = @ActionStatus,
			[GuidReference] = @GuidReference,
			[NamDK] = @NamDK,
			[HUONGDAN] = @HUONGDAN
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ToKhaiMauDich]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaHaiQuan],
			[SoToKhai],
			[MaLoaiHinh],
			[NgayDangKy],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[MaDaiLyTTHQ],
			[TenDaiLyTTHQ],
			[TenDonViDoiTac],
			[ChiTietDonViDoiTac],
			[SoGiayPhep],
			[NgayGiayPhep],
			[NgayHetHanGiayPhep],
			[SoHopDong],
			[NgayHopDong],
			[NgayHetHanHopDong],
			[SoHoaDonThuongMai],
			[NgayHoaDonThuongMai],
			[PTVT_ID],
			[SoHieuPTVT],
			[NgayDenPTVT],
			[QuocTichPTVT_ID],
			[LoaiVanDon],
			[SoVanDon],
			[NgayVanDon],
			[NuocXK_ID],
			[NuocNK_ID],
			[DiaDiemXepHang],
			[CuaKhau_ID],
			[DKGH_ID],
			[NguyenTe_ID],
			[TyGiaTinhThue],
			[TyGiaUSD],
			[PTTT_ID],
			[SoHang],
			[SoLuongPLTK],
			[TenChuHang],
			[ChucVu],
			[SoContainer20],
			[SoContainer40],
			[SoKien],
			[TrongLuong],
			[TongTriGiaKhaiBao],
			[TongTriGiaTinhThue],
			[LoaiToKhaiGiaCong],
			[LePhiHaiQuan],
			[PhiBaoHiem],
			[PhiVanChuyen],
			[PhiXepDoHang],
			[PhiKhac],
			[CanBoDangKy],
			[QuanLyMay],
			[TrangThaiXuLy],
			[LoaiHangHoa],
			[GiayTo],
			[PhanLuong],
			[MaDonViUT],
			[TenDonViUT],
			[TrongLuongNet],
			[SoTienKhoan],
			[HeSoNhan],
			[GUIDSTR],
			[DeXuatKhac],
			[LyDoSua],
			[ActionStatus],
			[GuidReference],
			[NamDK],
			[HUONGDAN]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaHaiQuan,
			@SoToKhai,
			@MaLoaiHinh,
			@NgayDangKy,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@MaDaiLyTTHQ,
			@TenDaiLyTTHQ,
			@TenDonViDoiTac,
			@ChiTietDonViDoiTac,
			@SoGiayPhep,
			@NgayGiayPhep,
			@NgayHetHanGiayPhep,
			@SoHopDong,
			@NgayHopDong,
			@NgayHetHanHopDong,
			@SoHoaDonThuongMai,
			@NgayHoaDonThuongMai,
			@PTVT_ID,
			@SoHieuPTVT,
			@NgayDenPTVT,
			@QuocTichPTVT_ID,
			@LoaiVanDon,
			@SoVanDon,
			@NgayVanDon,
			@NuocXK_ID,
			@NuocNK_ID,
			@DiaDiemXepHang,
			@CuaKhau_ID,
			@DKGH_ID,
			@NguyenTe_ID,
			@TyGiaTinhThue,
			@TyGiaUSD,
			@PTTT_ID,
			@SoHang,
			@SoLuongPLTK,
			@TenChuHang,
			@ChucVu,
			@SoContainer20,
			@SoContainer40,
			@SoKien,
			@TrongLuong,
			@TongTriGiaKhaiBao,
			@TongTriGiaTinhThue,
			@LoaiToKhaiGiaCong,
			@LePhiHaiQuan,
			@PhiBaoHiem,
			@PhiVanChuyen,
			@PhiXepDoHang,
			@PhiKhac,
			@CanBoDangKy,
			@QuanLyMay,
			@TrangThaiXuLy,
			@LoaiHangHoa,
			@GiayTo,
			@PhanLuong,
			@MaDonViUT,
			@TenDonViUT,
			@TrongLuongNet,
			@SoTienKhoan,
			@HeSoNhan,
			@GUIDSTR,
			@DeXuatKhac,
			@LyDoSua,
			@ActionStatus,
			@GuidReference,
			@NamDK,
			@HUONGDAN
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_ToKhaiMauDich_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_Load]
-- Database: ECS_SXXKTQ
-- Author: Ngo Thanh Tung
-- Time created: Thursday, January 07, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[ChucVu],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[CanBoDangKy],
	[QuanLyMay],
	[TrangThaiXuLy],
	[LoaiHangHoa],
	[GiayTo],
	[PhanLuong],
	[MaDonViUT],
	[TenDonViUT],
	[TrongLuongNet],
	[SoTienKhoan],
	[HeSoNhan],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN]
FROM
	[dbo].[t_KDT_ToKhaiMauDich]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_ToKhaiMauDich_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectAll]
-- Database: ECS_SXXKTQ
-- Author: Ngo Thanh Tung
-- Time created: Thursday, January 07, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[ChucVu],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[CanBoDangKy],
	[QuanLyMay],
	[TrangThaiXuLy],
	[LoaiHangHoa],
	[GiayTo],
	[PhanLuong],
	[MaDonViUT],
	[TenDonViUT],
	[TrongLuongNet],
	[SoTienKhoan],
	[HeSoNhan],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN]
FROM
	[dbo].[t_KDT_ToKhaiMauDich]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_ToKhaiMauDich_Load_ByDaiLy]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_Load]
-- Database: ECS_SXXKTQ
-- Author: Ngo Thanh Tung
-- Time created: Thursday, January 07, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_Load_ByDaiLy]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@SoToKhai int,
	@NamDK int,
	@MaLoaiHinh char(5)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[t_KDT_ToKhaiMauDich].[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[t_KDT_ToKhaiMauDich].[MaHaiQuan],
	[t_KDT_ToKhaiMauDich].[SoToKhai],
	[t_KDT_ToKhaiMauDich].[MaLoaiHinh],
	[NgayDangKy],
	[t_KDT_ToKhaiMauDich].[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[ChucVu],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[CanBoDangKy],
	[QuanLyMay],
	[TrangThaiXuLy],
	[LoaiHangHoa],
	[GiayTo],
	[PhanLuong],
	[MaDonViUT],
	[TenDonViUT],
	[TrongLuongNet],
	[SoTienKhoan],
	[HeSoNhan],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN],
	dbo.t_User_DaiLy.MO_TA
FROM
	[dbo].[t_KDT_ToKhaiMauDich] LEFT JOIN dbo.t_DongBoDuLieu_Track 
	ON dbo.t_KDT_ToKhaiMauDich.SoToKhai = dbo.t_DongBoDuLieu_Track.SoToKhai
	AND dbo.t_KDT_ToKhaiMauDich.NamDK = dbo.t_DongBoDuLieu_Track.NamDangKy
	AND dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh = dbo.t_DongBoDuLieu_Track.MaLoaiHinh
	AND dbo.t_KDT_ToKhaiMauDich.MaHaiQuan = dbo.t_DongBoDuLieu_Track.MaHaiQuan
	AND dbo.t_KDT_ToKhaiMauDich.MaDoanhNghiep = dbo.t_DongBoDuLieu_Track.MaDoanhNghiep
	LEFT JOIN dbo.t_User_DaiLy 
	ON dbo.t_DongBoDuLieu_Track.IDUserDaiLy = dbo.t_User_DaiLy.ID
WHERE
	[t_KDT_ToKhaiMauDich].MaHaiQuan = @MaHaiQuan
	AND [t_KDT_ToKhaiMauDich].MaDoanhNghiep = @MaDoanhNghiep
	AND [t_KDT_ToKhaiMauDich].SoToKhai = @SoToKhai
	AND [t_KDT_ToKhaiMauDich].NamDK = @NamDK
	AND [t_KDT_ToKhaiMauDich].MaLoaiHinh= @MaLoaiHinh

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_ToKhaiMauDich_SelectDynamic_ByDaiLy]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectDynamic]
-- Database: ECS_SXXKTQ
-- Author: Ngo Thanh Tung
-- Time created: Thursday, January 07, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectDynamic_ByDaiLy]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT
		[t_KDT_ToKhaiMauDich].[ID],
		[SoTiepNhan],
		[NgayTiepNhan],
		[t_KDT_ToKhaiMauDich].[MaHaiQuan],
		[t_KDT_ToKhaiMauDich].[SoToKhai],
		[t_KDT_ToKhaiMauDich].[MaLoaiHinh],
		[NgayDangKy],
		[t_KDT_ToKhaiMauDich].[MaDoanhNghiep],
		[TenDoanhNghiep],
		[MaDaiLyTTHQ],
		[TenDaiLyTTHQ],
		[TenDonViDoiTac],
		[ChiTietDonViDoiTac],
		[SoGiayPhep],
		[NgayGiayPhep],
		[NgayHetHanGiayPhep],
		[SoHopDong],
		[NgayHopDong],
		[NgayHetHanHopDong],
		[SoHoaDonThuongMai],
		[NgayHoaDonThuongMai],
		[PTVT_ID],
		[SoHieuPTVT],
		[NgayDenPTVT],
		[QuocTichPTVT_ID],
		[LoaiVanDon],
		[SoVanDon],
		[NgayVanDon],
		[NuocXK_ID],
		[NuocNK_ID],
		[DiaDiemXepHang],
		[CuaKhau_ID],
		[DKGH_ID],
		[NguyenTe_ID],
		[TyGiaTinhThue],
		[TyGiaUSD],
		[PTTT_ID],
		[SoHang],
		[SoLuongPLTK],
		[TenChuHang],
		[ChucVu],
		[SoContainer20],
		[SoContainer40],
		[SoKien],
		[TrongLuong],
		[TongTriGiaKhaiBao],
		[TongTriGiaTinhThue],
		[LoaiToKhaiGiaCong],
		[LePhiHaiQuan],
		[PhiBaoHiem],
		[PhiVanChuyen],
		[PhiXepDoHang],
		[PhiKhac],
		[CanBoDangKy],
		[QuanLyMay],
		[TrangThaiXuLy],
		[LoaiHangHoa],
		[GiayTo],
		[PhanLuong],
		[MaDonViUT],
		[TenDonViUT],
		[TrongLuongNet],
		[SoTienKhoan],
		[HeSoNhan],
		[GUIDSTR],
		[DeXuatKhac],
		[LyDoSua],
		[ActionStatus],
		[GuidReference],
		[NamDK],
		[HUONGDAN],
		isnull(dbo.t_User_DaiLy.MO_TA, '''') as TenDaiLyKhaiBao,
		isnull(dbo.t_DongBoDuLieu_Track.TrangThaiDongBoTaiLen, 0) as TrangThaiDongBoTaiLen,
		isnull(dbo.t_DongBoDuLieu_Track.TrangThaiDongBoTaiXuong, 0) as TrangThaiDongBoTaiXuong
	FROM
		[dbo].[t_KDT_ToKhaiMauDich] LEFT JOIN dbo.t_DongBoDuLieu_Track 
		ON dbo.t_KDT_ToKhaiMauDich.SoToKhai = dbo.t_DongBoDuLieu_Track.SoToKhai
		AND dbo.t_KDT_ToKhaiMauDich.NamDK = dbo.t_DongBoDuLieu_Track.NamDangKy
		AND dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh = dbo.t_DongBoDuLieu_Track.MaLoaiHinh
		AND dbo.t_KDT_ToKhaiMauDich.MaHaiQuan = dbo.t_DongBoDuLieu_Track.MaHaiQuan
		AND dbo.t_KDT_ToKhaiMauDich.MaDoanhNghiep = dbo.t_DongBoDuLieu_Track.MaDoanhNghiep
		LEFT JOIN dbo.t_User_DaiLy 
		ON dbo.t_DongBoDuLieu_Track.IDUserDaiLy = dbo.t_User_DaiLy.ID
	WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_ToKhaiMauDich_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectDynamic]
-- Database: ECS_SXXKTQ
-- Author: Ngo Thanh Tung
-- Time created: Thursday, January 07, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaDaiLyTTHQ],
	[TenDaiLyTTHQ],
	[TenDonViDoiTac],
	[ChiTietDonViDoiTac],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHanGiayPhep],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHanHopDong],
	[SoHoaDonThuongMai],
	[NgayHoaDonThuongMai],
	[PTVT_ID],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[QuocTichPTVT_ID],
	[LoaiVanDon],
	[SoVanDon],
	[NgayVanDon],
	[NuocXK_ID],
	[NuocNK_ID],
	[DiaDiemXepHang],
	[CuaKhau_ID],
	[DKGH_ID],
	[NguyenTe_ID],
	[TyGiaTinhThue],
	[TyGiaUSD],
	[PTTT_ID],
	[SoHang],
	[SoLuongPLTK],
	[TenChuHang],
	[ChucVu],
	[SoContainer20],
	[SoContainer40],
	[SoKien],
	[TrongLuong],
	[TongTriGiaKhaiBao],
	[TongTriGiaTinhThue],
	[LoaiToKhaiGiaCong],
	[LePhiHaiQuan],
	[PhiBaoHiem],
	[PhiVanChuyen],
	[PhiXepDoHang],
	[PhiKhac],
	[CanBoDangKy],
	[QuanLyMay],
	[TrangThaiXuLy],
	[LoaiHangHoa],
	[GiayTo],
	[PhanLuong],
	[MaDonViUT],
	[TenDonViUT],
	[TrongLuongNet],
	[SoTienKhoan],
	[HeSoNhan],
	[GUIDSTR],
	[DeXuatKhac],
	[LyDoSua],
	[ActionStatus],
	[GuidReference],
	[NamDK],
	[HUONGDAN]
 FROM [dbo].[t_KDT_ToKhaiMauDich] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='3.8', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('3.8', getdate(), null)
	end 