using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Activation.Server;

namespace Licenses_v2.Module
{
    public partial class FrmEmployeeManager : DevExpress.XtraEditors.XtraForm
    {
        public FrmEmployeeManager()
        {
            InitializeComponent();
        }

        #region Adding Method

        private void OpenForm(string id, bool isEdit)
        {
            if (!ClassLib.IsFormShowed("Empl_" + id))
            {
                FrmEmployeeInfo obj = new FrmEmployeeInfo(id);
                obj.MdiParent = this.MdiParent;
                obj.isEdit = isEdit;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Empl_" + id].Activate();
            }
        }

        public void RefreshData()
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(this.tblEmployee());
            gridEmployee.DataSource = ds.Tables["tblEmployee"];
        }

        private DataTable tblEmployee()
        {
            DataTable dt = Employee.SelectAll().Tables[0].Copy();
            dt.Columns.Remove("PassWord");
            dt.Columns["ID"].Caption = "Mã nhân viên";
            dt.Columns["FirstName"].Caption = "Tên";
            dt.Columns["LastName"].Caption = "Họ";
            dt.Columns["UserName"].Caption = "Tên đăng nhập";
            dt.Columns["Department"].Caption = "Bộ phận";
            dt.Columns["Title"].Caption = "Chức danh";
            dt.Columns["Email"].Caption = "Thư điện tử";
            dt.TableName = "tblEmployee";

            return dt;
        }

        #endregion

        private void FrmEmployeeManager_Load(object sender, EventArgs e)
        {
            this.RefreshData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    string Message = "Bạn có muốn xóa nhân viên: " + ID;
                    if (MessageBox.Show(Message, "Xóa nhân viên", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Employee.DeleteEmployee(ID);
                    }
                }
            }
            this.RefreshData();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    this.OpenForm(ID, true);
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!ClassLib.IsFormShowed("Empl_New"))
            {
                FrmEmployeeInfo obj = new FrmEmployeeInfo(null);
                obj.MdiParent = this.MdiParent;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Empl_New"].Activate();
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            gridData.ClearColumnsFilter();
            gridData.ClearGrouping();
            gridData.ClearSorting();
            this.RefreshData();
        }

        private void gridEmployee_DoubleClick(object sender, EventArgs e)
        {
            foreach (int i in gridData.GetSelectedRows())
            {
                if (i >= 0)
                {
                    string ID = gridData.GetRowCellValue(i, "ID").ToString();
                    this.OpenForm(ID, false);
                }
            }
        }

    }
}