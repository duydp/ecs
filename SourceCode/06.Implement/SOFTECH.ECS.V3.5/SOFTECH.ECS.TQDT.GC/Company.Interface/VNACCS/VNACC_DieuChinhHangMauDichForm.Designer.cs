﻿namespace Company.Interface.VNACCS
{
    partial class VNACC_DieuChinhHangMauDichForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference grList_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_DieuChinhHangMauDichForm));
            this.ctrDVTLuong1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaSoHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoLuong1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMaHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblMaHang = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNuocXuatXu = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.label18 = new System.Windows.Forms.Label();
            this.ctrMaTTTriGiaTinhThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTriGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtSoTienThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDonGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ctrDV_SL_TrongDonGiaTinhThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaDVTDanhThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoLuongTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTriGiaTinhThueS = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.ctrMaTTDonGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTriGiaHoaDon = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDonGiaHoaDon = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.grList = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 587), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 587);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 563);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 563);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grList);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(1011, 587);
            // 
            // ctrDVTLuong1
            // 
            this.ctrDVTLuong1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrDVTLuong1.Appearance.Options.UseBackColor = true;
            this.ctrDVTLuong1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTLuong1.Code = "";
            this.ctrDVTLuong1.ColorControl = System.Drawing.Color.Empty;
            this.ctrDVTLuong1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTLuong1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTLuong1.IsOnlyWarning = false;
            this.ctrDVTLuong1.IsValidate = true;
            this.ctrDVTLuong1.Location = new System.Drawing.Point(358, 91);
            this.ctrDVTLuong1.Name = "ctrDVTLuong1";
            this.ctrDVTLuong1.Name_VN = "";
            this.ctrDVTLuong1.SetOnlyWarning = false;
            this.ctrDVTLuong1.SetValidate = false;
            this.ctrDVTLuong1.ShowColumnCode = true;
            this.ctrDVTLuong1.ShowColumnName = false;
            this.ctrDVTLuong1.Size = new System.Drawing.Size(153, 21);
            this.ctrDVTLuong1.TabIndex = 81;
            this.ctrDVTLuong1.TagName = "";
            this.ctrDVTLuong1.Where = null;
            this.ctrDVTLuong1.WhereCondition = "";
            // 
            // ctrMaSoHang
            // 
            this.ctrMaSoHang.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaSoHang.Appearance.Options.UseBackColor = true;
            this.ctrMaSoHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A506;
            this.ctrMaSoHang.Code = "";
            this.ctrMaSoHang.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaSoHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaSoHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaSoHang.IsOnlyWarning = false;
            this.ctrMaSoHang.IsValidate = true;
            this.ctrMaSoHang.Location = new System.Drawing.Point(114, 55);
            this.ctrMaSoHang.Name = "ctrMaSoHang";
            this.ctrMaSoHang.Name_VN = "";
            this.ctrMaSoHang.SetOnlyWarning = false;
            this.ctrMaSoHang.SetValidate = false;
            this.ctrMaSoHang.ShowColumnCode = true;
            this.ctrMaSoHang.ShowColumnName = false;
            this.ctrMaSoHang.Size = new System.Drawing.Size(160, 21);
            this.ctrMaSoHang.TabIndex = 79;
            this.ctrMaSoHang.TagName = "";
            this.ctrMaSoHang.Where = null;
            this.ctrMaSoHang.WhereCondition = "";
            // 
            // txtSoLuong1
            // 
            this.txtSoLuong1.BackColor = System.Drawing.SystemColors.Info;
            this.txtSoLuong1.DecimalDigits = 20;
            this.txtSoLuong1.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong1.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong1.Location = new System.Drawing.Point(114, 91);
            this.txtSoLuong1.MaxLength = 20;
            this.txtSoLuong1.Name = "txtSoLuong1";
            this.txtSoLuong1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong1.Size = new System.Drawing.Size(160, 21);
            this.txtSoLuong1.TabIndex = 80;
            this.txtSoLuong1.Text = "0";
            this.txtSoLuong1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(296, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 88;
            this.label3.Text = "Đơn vị tính";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 95);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 86;
            this.label6.Text = "Số lượng";
            // 
            // txtMaHangHoa
            // 
            this.txtMaHangHoa.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaHangHoa.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaHangHoa.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangHoa.Location = new System.Drawing.Point(115, 20);
            this.txtMaHangHoa.MaxLength = 50;
            this.txtMaHangHoa.Name = "txtMaHangHoa";
            this.txtMaHangHoa.Size = new System.Drawing.Size(159, 21);
            this.txtMaHangHoa.TabIndex = 77;
            this.txtMaHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHangHoa.ButtonClick += new System.EventHandler(this.txtMaHangHoa_ButtonClick);
            this.txtMaHangHoa.Leave += new System.EventHandler(this.txtMaHangHoa_Leave);
            // 
            // txtTenHang
            // 
            this.txtTenHang.BackColor = System.Drawing.SystemColors.Info;
            this.txtTenHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHang.Location = new System.Drawing.Point(358, 20);
            this.txtTenHang.MaxLength = 255;
            this.txtTenHang.Multiline = true;
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(333, 56);
            this.txtTenHang.TabIndex = 78;
            this.txtTenHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(300, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 83;
            this.label2.Text = "Tên hàng";
            // 
            // lblMaHang
            // 
            this.lblMaHang.AutoSize = true;
            this.lblMaHang.BackColor = System.Drawing.Color.Transparent;
            this.lblMaHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaHang.Location = new System.Drawing.Point(12, 28);
            this.lblMaHang.Name = "lblMaHang";
            this.lblMaHang.Size = new System.Drawing.Size(48, 13);
            this.lblMaHang.TabIndex = 85;
            this.lblMaHang.Text = "Mã hàng";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(12, 63);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 84;
            this.label11.Text = "Mã số hàng hóa";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ctrNuocXuatXu);
            this.uiGroupBox1.Controls.Add(this.label18);
            this.uiGroupBox1.Controls.Add(this.ctrMaTTTriGiaTinhThue);
            this.uiGroupBox1.Controls.Add(this.txtTriGiaTinhThue);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.label24);
            this.uiGroupBox1.Controls.Add(this.label23);
            this.uiGroupBox1.Controls.Add(this.txtSoTienThue);
            this.uiGroupBox1.Controls.Add(this.txtDonGiaTinhThue);
            this.uiGroupBox1.Controls.Add(this.ctrDV_SL_TrongDonGiaTinhThue);
            this.uiGroupBox1.Controls.Add(this.ctrMaDVTDanhThue);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongTinhThue);
            this.uiGroupBox1.Controls.Add(this.txtTriGiaTinhThueS);
            this.uiGroupBox1.Controls.Add(this.label26);
            this.uiGroupBox1.Controls.Add(this.label22);
            this.uiGroupBox1.Controls.Add(this.label25);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.ctrMaTTDonGia);
            this.uiGroupBox1.Controls.Add(this.txtTriGiaHoaDon);
            this.uiGroupBox1.Controls.Add(this.txtDonGiaHoaDon);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Controls.Add(this.txtTenHang);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.ctrDVTLuong1);
            this.uiGroupBox1.Controls.Add(this.lblMaHang);
            this.uiGroupBox1.Controls.Add(this.ctrMaSoHang);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.txtMaHangHoa);
            this.uiGroupBox1.Controls.Add(this.txtSoLuong1);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1011, 252);
            this.uiGroupBox1.TabIndex = 90;
            this.uiGroupBox1.Text = "Thông tin hàng hóa";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // ctrNuocXuatXu
            // 
            this.ctrNuocXuatXu.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocXuatXu.Appearance.Options.UseBackColor = true;
            this.ctrNuocXuatXu.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ctrNuocXuatXu.Code = "";
            this.ctrNuocXuatXu.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrNuocXuatXu.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrNuocXuatXu.IsOnlyWarning = false;
            this.ctrNuocXuatXu.IsValidate = true;
            this.ctrNuocXuatXu.Location = new System.Drawing.Point(849, 91);
            this.ctrNuocXuatXu.Name = "ctrNuocXuatXu";
            this.ctrNuocXuatXu.Name_VN = "";
            this.ctrNuocXuatXu.SetOnlyWarning = false;
            this.ctrNuocXuatXu.SetValidate = false;
            this.ctrNuocXuatXu.ShowColumnCode = true;
            this.ctrNuocXuatXu.ShowColumnName = true;
            this.ctrNuocXuatXu.Size = new System.Drawing.Size(159, 21);
            this.ctrNuocXuatXu.TabIndex = 111;
            this.ctrNuocXuatXu.TagCode = "";
            this.ctrNuocXuatXu.TagName = "";
            this.ctrNuocXuatXu.Where = null;
            this.ctrNuocXuatXu.WhereCondition = "";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(763, 95);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 112;
            this.label18.Text = "Nước xuất xứ";
            // 
            // ctrMaTTTriGiaTinhThue
            // 
            this.ctrMaTTTriGiaTinhThue.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaTTTriGiaTinhThue.Appearance.Options.UseBackColor = true;
            this.ctrMaTTTriGiaTinhThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTTriGiaTinhThue.Code = "";
            this.ctrMaTTTriGiaTinhThue.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTTriGiaTinhThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTTriGiaTinhThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTTriGiaTinhThue.IsOnlyWarning = false;
            this.ctrMaTTTriGiaTinhThue.IsValidate = true;
            this.ctrMaTTTriGiaTinhThue.Location = new System.Drawing.Point(280, 185);
            this.ctrMaTTTriGiaTinhThue.Name = "ctrMaTTTriGiaTinhThue";
            this.ctrMaTTTriGiaTinhThue.Name_VN = "";
            this.ctrMaTTTriGiaTinhThue.SetOnlyWarning = false;
            this.ctrMaTTTriGiaTinhThue.SetValidate = false;
            this.ctrMaTTTriGiaTinhThue.ShowColumnCode = true;
            this.ctrMaTTTriGiaTinhThue.ShowColumnName = false;
            this.ctrMaTTTriGiaTinhThue.Size = new System.Drawing.Size(72, 21);
            this.ctrMaTTTriGiaTinhThue.TabIndex = 109;
            this.ctrMaTTTriGiaTinhThue.TagName = "";
            this.ctrMaTTTriGiaTinhThue.Where = null;
            this.ctrMaTTTriGiaTinhThue.WhereCondition = "";
            // 
            // txtTriGiaTinhThue
            // 
            this.txtTriGiaTinhThue.DecimalDigits = 20;
            this.txtTriGiaTinhThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTriGiaTinhThue.Location = new System.Drawing.Point(114, 185);
            this.txtTriGiaTinhThue.MaxLength = 15;
            this.txtTriGiaTinhThue.Name = "txtTriGiaTinhThue";
            this.txtTriGiaTinhThue.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            this.txtTriGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThue.Size = new System.Drawing.Size(159, 21);
            this.txtTriGiaTinhThue.TabIndex = 108;
            this.txtTriGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThue.Value = null;
            this.txtTriGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 189);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 13);
            this.label10.TabIndex = 110;
            this.label10.Text = "Trị giá tính thuế(M)";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(283, 159);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(28, 13);
            this.label24.TabIndex = 106;
            this.label24.Text = "VNĐ";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(12, 159);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(65, 13);
            this.label23.TabIndex = 107;
            this.label23.Text = "Số tiền thuế";
            // 
            // txtSoTienThue
            // 
            this.txtSoTienThue.DecimalDigits = 20;
            this.txtSoTienThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTienThue.Location = new System.Drawing.Point(115, 155);
            this.txtSoTienThue.MaxLength = 15;
            this.txtSoTienThue.Name = "txtSoTienThue";
            this.txtSoTienThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienThue.Size = new System.Drawing.Size(159, 21);
            this.txtSoTienThue.TabIndex = 105;
            this.txtSoTienThue.Text = "0";
            this.txtSoTienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDonGiaTinhThue
            // 
            this.txtDonGiaTinhThue.DecimalDigits = 20;
            this.txtDonGiaTinhThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDonGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaTinhThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDonGiaTinhThue.Location = new System.Drawing.Point(630, 155);
            this.txtDonGiaTinhThue.MaxLength = 15;
            this.txtDonGiaTinhThue.Name = "txtDonGiaTinhThue";
            this.txtDonGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDonGiaTinhThue.Size = new System.Drawing.Size(128, 21);
            this.txtDonGiaTinhThue.TabIndex = 96;
            this.txtDonGiaTinhThue.Text = "0";
            this.txtDonGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDonGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDonGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ctrDV_SL_TrongDonGiaTinhThue
            // 
            this.ctrDV_SL_TrongDonGiaTinhThue.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrDV_SL_TrongDonGiaTinhThue.Appearance.Options.UseBackColor = true;
            this.ctrDV_SL_TrongDonGiaTinhThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDV_SL_TrongDonGiaTinhThue.Code = "";
            this.ctrDV_SL_TrongDonGiaTinhThue.ColorControl = System.Drawing.Color.Empty;
            this.ctrDV_SL_TrongDonGiaTinhThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDV_SL_TrongDonGiaTinhThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDV_SL_TrongDonGiaTinhThue.IsOnlyWarning = false;
            this.ctrDV_SL_TrongDonGiaTinhThue.IsValidate = true;
            this.ctrDV_SL_TrongDonGiaTinhThue.Location = new System.Drawing.Point(789, 155);
            this.ctrDV_SL_TrongDonGiaTinhThue.Name = "ctrDV_SL_TrongDonGiaTinhThue";
            this.ctrDV_SL_TrongDonGiaTinhThue.Name_VN = "";
            this.ctrDV_SL_TrongDonGiaTinhThue.SetOnlyWarning = false;
            this.ctrDV_SL_TrongDonGiaTinhThue.SetValidate = false;
            this.ctrDV_SL_TrongDonGiaTinhThue.ShowColumnCode = true;
            this.ctrDV_SL_TrongDonGiaTinhThue.ShowColumnName = false;
            this.ctrDV_SL_TrongDonGiaTinhThue.Size = new System.Drawing.Size(72, 20);
            this.ctrDV_SL_TrongDonGiaTinhThue.TabIndex = 97;
            this.ctrDV_SL_TrongDonGiaTinhThue.TagName = "";
            this.ctrDV_SL_TrongDonGiaTinhThue.Where = null;
            this.ctrDV_SL_TrongDonGiaTinhThue.WhereCondition = "";
            // 
            // ctrMaDVTDanhThue
            // 
            this.ctrMaDVTDanhThue.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaDVTDanhThue.Appearance.Options.UseBackColor = true;
            this.ctrMaDVTDanhThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrMaDVTDanhThue.Code = "";
            this.ctrMaDVTDanhThue.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaDVTDanhThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaDVTDanhThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaDVTDanhThue.IsOnlyWarning = false;
            this.ctrMaDVTDanhThue.IsValidate = true;
            this.ctrMaDVTDanhThue.Location = new System.Drawing.Point(280, 125);
            this.ctrMaDVTDanhThue.Name = "ctrMaDVTDanhThue";
            this.ctrMaDVTDanhThue.Name_VN = "";
            this.ctrMaDVTDanhThue.SetOnlyWarning = false;
            this.ctrMaDVTDanhThue.SetValidate = false;
            this.ctrMaDVTDanhThue.ShowColumnCode = true;
            this.ctrMaDVTDanhThue.ShowColumnName = false;
            this.ctrMaDVTDanhThue.Size = new System.Drawing.Size(72, 20);
            this.ctrMaDVTDanhThue.TabIndex = 100;
            this.ctrMaDVTDanhThue.TagName = "";
            this.ctrMaDVTDanhThue.Where = null;
            this.ctrMaDVTDanhThue.WhereCondition = "";
            // 
            // txtSoLuongTinhThue
            // 
            this.txtSoLuongTinhThue.DecimalDigits = 20;
            this.txtSoLuongTinhThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongTinhThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongTinhThue.Location = new System.Drawing.Point(115, 125);
            this.txtSoLuongTinhThue.MaxLength = 15;
            this.txtSoLuongTinhThue.Name = "txtSoLuongTinhThue";
            this.txtSoLuongTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongTinhThue.Size = new System.Drawing.Size(159, 21);
            this.txtSoLuongTinhThue.TabIndex = 99;
            this.txtSoLuongTinhThue.Text = "0";
            this.txtSoLuongTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGiaTinhThueS
            // 
            this.txtTriGiaTinhThueS.DecimalDigits = 20;
            this.txtTriGiaTinhThueS.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaTinhThueS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThueS.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTriGiaTinhThueS.Location = new System.Drawing.Point(630, 185);
            this.txtTriGiaTinhThueS.MaxLength = 15;
            this.txtTriGiaTinhThueS.Name = "txtTriGiaTinhThueS";
            this.txtTriGiaTinhThueS.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThueS.Size = new System.Drawing.Size(128, 21);
            this.txtTriGiaTinhThueS.TabIndex = 98;
            this.txtTriGiaTinhThueS.Text = "0";
            this.txtTriGiaTinhThueS.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThueS.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaTinhThueS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(12, 129);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(95, 13);
            this.label26.TabIndex = 103;
            this.label26.Text = "Số lượng tính thuế";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(761, 159);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(28, 13);
            this.label22.TabIndex = 101;
            this.label22.Text = "VNĐ";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(529, 189);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(96, 13);
            this.label25.TabIndex = 104;
            this.label25.Text = "Trị giá tính thuế(S)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(529, 159);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 13);
            this.label14.TabIndex = 102;
            this.label14.Text = "Đơn giá tính thuế";
            // 
            // ctrMaTTDonGia
            // 
            this.ctrMaTTDonGia.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrMaTTDonGia.Appearance.Options.UseBackColor = true;
            this.ctrMaTTDonGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTDonGia.Code = "";
            this.ctrMaTTDonGia.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaTTDonGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTDonGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTDonGia.IsOnlyWarning = false;
            this.ctrMaTTDonGia.IsValidate = true;
            this.ctrMaTTDonGia.Location = new System.Drawing.Point(789, 125);
            this.ctrMaTTDonGia.Name = "ctrMaTTDonGia";
            this.ctrMaTTDonGia.Name_VN = "";
            this.ctrMaTTDonGia.SetOnlyWarning = false;
            this.ctrMaTTDonGia.SetValidate = false;
            this.ctrMaTTDonGia.ShowColumnCode = true;
            this.ctrMaTTDonGia.ShowColumnName = false;
            this.ctrMaTTDonGia.Size = new System.Drawing.Size(72, 21);
            this.ctrMaTTDonGia.TabIndex = 93;
            this.ctrMaTTDonGia.TagName = "";
            this.ctrMaTTDonGia.Where = null;
            this.ctrMaTTDonGia.WhereCondition = "";
            // 
            // txtTriGiaHoaDon
            // 
            this.txtTriGiaHoaDon.BackColor = System.Drawing.SystemColors.Info;
            this.txtTriGiaHoaDon.DecimalDigits = 20;
            this.txtTriGiaHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaHoaDon.FormatString = "G20";
            this.txtTriGiaHoaDon.Location = new System.Drawing.Point(630, 91);
            this.txtTriGiaHoaDon.Name = "txtTriGiaHoaDon";
            this.txtTriGiaHoaDon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaHoaDon.Size = new System.Drawing.Size(128, 21);
            this.txtTriGiaHoaDon.TabIndex = 91;
            this.txtTriGiaHoaDon.Text = "0";
            this.txtTriGiaHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaHoaDon.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDonGiaHoaDon
            // 
            this.txtDonGiaHoaDon.BackColor = System.Drawing.SystemColors.Info;
            this.txtDonGiaHoaDon.DecimalDigits = 20;
            this.txtDonGiaHoaDon.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDonGiaHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaHoaDon.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDonGiaHoaDon.Location = new System.Drawing.Point(630, 125);
            this.txtDonGiaHoaDon.MaxLength = 15;
            this.txtDonGiaHoaDon.Name = "txtDonGiaHoaDon";
            this.txtDonGiaHoaDon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDonGiaHoaDon.Size = new System.Drawing.Size(128, 21);
            this.txtDonGiaHoaDon.TabIndex = 92;
            this.txtDonGiaHoaDon.Text = "0";
            this.txtDonGiaHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDonGiaHoaDon.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDonGiaHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(529, 95);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 95;
            this.label8.Text = "Trị giá hóa đơn";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(529, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 94;
            this.label7.Text = "Đơn giá hóa đơn";
            // 
            // btnGhi
            // 
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Image = ((System.Drawing.Image)(resources.GetObject("btnGhi.Image")));
            this.btnGhi.ImageSize = new System.Drawing.Size(20, 20);
            this.btnGhi.Location = new System.Drawing.Point(114, 217);
            this.btnGhi.Name = "btnGhi";
            this.helpProvider1.SetShowHelp(this.btnGhi, true);
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 90;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Controls.Add(this.btnXoa);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 540);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1011, 47);
            this.uiGroupBox2.TabIndex = 91;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(926, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 91;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(836, 15);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 92;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // grList
            // 
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            grList_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("grList_DesignTimeLayout_Reference_0.Instance")));
            grList_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            grList_DesignTimeLayout_Reference_0});
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grList.FrozenColumns = 5;
            this.grList.GroupByBoxVisible = false;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Location = new System.Drawing.Point(0, 252);
            this.grList.Name = "grList";
            this.grList.NewRowPosition = Janus.Windows.GridEX.NewRowPosition.BottomRow;
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelection;
            this.grList.Size = new System.Drawing.Size(1011, 288);
            this.grList.TabIndex = 92;
            this.grList.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.TotalRowPosition = Janus.Windows.GridEX.TotalRowPosition.BottomFixed;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grList_RowDoubleClick);
            // 
            // VNACC_DieuChinhHangMauDichForm
            // 
            this.ClientSize = new System.Drawing.Size(1217, 593);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_DieuChinhHangMauDichForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thay đổi Thông tin hàng hóa tờ khai";
            this.Load += new System.EventHandler(this.VNACC_DieuChinhHangMauDichForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        public Janus.Windows.GridEX.EditControls.EditBox txtTenHang;
        public System.Windows.Forms.Label label11;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTLuong1;
        public System.Windows.Forms.Label lblMaHang;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaSoHang;
        public System.Windows.Forms.Label label2;
        public Janus.Windows.GridEX.EditControls.EditBox txtMaHangHoa;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong1;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        public Janus.Windows.GridEX.GridEX grList;
        public Janus.Windows.EditControls.UIButton btnClose;
        public Janus.Windows.EditControls.UIButton btnXoa;
        public Janus.Windows.EditControls.UIButton btnGhi;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTDonGia;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaHoaDon;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaHoaDon;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label7;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaTinhThue;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDV_SL_TrongDonGiaTinhThue;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaDVTDanhThue;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongTinhThue;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThueS;
        public System.Windows.Forms.Label label26;
        public System.Windows.Forms.Label label22;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.Label label23;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienThue;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTTriGiaTinhThue;
        public Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThue;
        public System.Windows.Forms.Label label10;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrNuocXuatXu;
        public System.Windows.Forms.Label label18;
    }
}
