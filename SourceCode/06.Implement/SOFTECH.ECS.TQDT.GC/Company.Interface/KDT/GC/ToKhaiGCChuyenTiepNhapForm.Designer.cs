﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Company.Controls.CustomValidation;
using Janus.Windows.CalendarCombo;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Dock;
using CuaKhauControl=Company.Interface.Controls.CuaKhauControl;
using DonViHaiQuanControl=Company.Interface.Controls.DonViHaiQuanControl;
using LoaiHinhMauDichControl01=Company.Interface.Controls.LoaiHinhMauDichVControl;
using NguyenTeControl=Company.Interface.Controls.NguyenTeControl;
using NuocControl=Company.Interface.Controls.NuocControl;

namespace Company.Interface.KDT.GC
{
    partial class ToKhaiGCChuyenTiepNhapForm
    {
        private Panel pnlToKhaiMauDich;
        private UIGroupBox grbNguyenTe;
        private Label label9;
        private Label label30;
        private NumericEditBox txtTyGiaTinhThue;
        private NumericEditBox txtTyGiaUSD;
        private DataSet ds;
        private DataTable dtHangMauDich;
        private DataColumn dataColumn1;
        private DataColumn dataColumn2;
        private DataColumn dataColumn3;
        private DataColumn dataColumn4;
        private DataColumn dataColumn5;
        private DataColumn dataColumn6;
        private DataColumn dataColumn7;
        private DataColumn dataColumn8;
        private DataColumn dataColumn9;
        private DataTable dtLoaiHinhMauDich;
        private DataColumn dataColumn10;
        private DataColumn dataColumn11;
        private DataTable dtPTTT;
        private DataColumn dataColumn12;
        private DataColumn dataColumn13;
        private DataTable dtCuaKhau;
        private DataColumn dataColumn14;
        private DataColumn dataColumn15;
        private DataColumn dataColumn16;
        private DataTable dtNguyenTe;
        private DataColumn dataColumn17;
        private DataColumn dataColumn18;
        private DataTable dtCompanyNuoc;
        private DataColumn dataColumn19;
        private DataColumn dataColumn20;
        private ErrorProvider epError;
        private ContainerValidator cvError;
        private ListValidationSummary lvsError;
        private RequiredFieldValidator rfvSoHopDongGiao;
        private UIGroupBox uiGroupBox2;
        private DataTable dtDonViHaiQuan;
        private DataColumn dataColumn21;
        private DataColumn dataColumn22;
        private UIGroupBox uiGroupBox1;
        private UIPanelManager uiPanelManager1;
        private UIPanelGroup uiPanel0;
        private UIPanel uiPanel1;
        private UIPanelInnerContainer uiPanel1Container;
        private RequiredFieldValidator rfvNgayHDGiao;
        private RangeValidator rvTyGiaTT;
        private RangeValidator rvTyGiaUSD;
        private UICommandManager cmMain;
        private UICommand cmdSave;
        private UIRebar TopRebar1;
        private UICommandBar cmbToolBar;
        private UIRebar BottomRebar1;
        private UIRebar LeftRebar1;
        private UIRebar RightRebar1;
        private IContainer components;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiGCChuyenTiepNhapForm));
            this.pnlToKhaiMauDich = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.uiKhachHang = new Janus.Windows.EditControls.UIGroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.donViHaiQuanControl2 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.txtNguoiChiDinhKH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblNguoiChiDinhKH = new System.Windows.Forms.Label();
            this.grbNguoiGiao = new Janus.Windows.EditControls.UIGroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTenDVGiao = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDVGiao = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grbHopDongGiao = new Janus.Windows.EditControls.UIGroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.ccNgayHHHopDongGiao = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoHongDonggiao = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblHDKH = new System.Windows.Forms.Label();
            this.lblNgayHDKH = new System.Windows.Forms.Label();
            this.lblNgayHHKH = new System.Windows.Forms.Label();
            this.ccNgayHopDongGiao = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrLoaiHinhMauDich = new Company.Interface.Controls.LoaiHinhMauDichVControl();
            this.uiDV = new Janus.Windows.EditControls.UIGroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.numTrongLuongTinh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numTrongLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numSoKien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtDiaDiemGiaohang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ccThoiGianGiaoHang = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label5 = new System.Windows.Forms.Label();
            this.uiNguoiChiDinhDV = new Janus.Windows.EditControls.UIGroupBox();
            this.txtNguoiChiDinhDV = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grbNguoiNhan = new Janus.Windows.EditControls.UIGroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTenDonVinhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDonVinhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grbHopDongNhan = new Janus.Windows.EditControls.UIGroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ccNgayHHHopDongNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoHopDongNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblSoHDDV = new System.Windows.Forms.Label();
            this.lblNgayHDDV = new System.Windows.Forms.Label();
            this.lblNgayHHDV = new System.Windows.Forms.Label();
            this.ccNgayHopDongNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbLoaiHinh = new Company.Interface.Controls.LoaiHinhChuyenTiepControl();
            this.label16 = new System.Windows.Forms.Label();
            this.txtToKhaiSo = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtCanbodangky = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.ccNgayDangKy = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.lblPhanLuong = new System.Windows.Forms.Label();
            this.lbltrangthai = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtsotiepnhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label31 = new System.Windows.Forms.Label();
            this.grbNguyenTe = new Janus.Windows.EditControls.UIGroupBox();
            this.cbPTTT = new Janus.Windows.EditControls.UIComboBox();
            this.cbDKGH = new Janus.Windows.EditControls.UIComboBox();
            this.txtMaDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSoBienLai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtChungTu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ccNgayBienLai = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLePhiHQ = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nguyenTeControl1 = new Company.Interface.Controls.NguyenTeControl();
            this.txtTyGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtTyGiaUSD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ds = new System.Data.DataSet();
            this.dtHangMauDich = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dtLoaiHinhMauDich = new System.Data.DataTable();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dtPTTT = new System.Data.DataTable();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dtCuaKhau = new System.Data.DataTable();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dtNguyenTe = new System.Data.DataTable();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dtCompanyNuoc = new System.Data.DataTable();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dtDonViHaiQuan = new System.Data.DataTable();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvSoHopDongGiao = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.rfvNgayHDGiao = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rvTyGiaTT = new Company.Controls.CustomValidation.RangeValidator();
            this.rvTyGiaUSD = new Company.Controls.CustomValidation.RangeValidator();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbToolBar = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdChungTuKem1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuKem");
            this.cmdTruyenDuLieuTuXa1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTruyenDuLieuTuXa");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.Separator4 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdThemHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdThemMotHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemMotHang");
            this.cmdThemNhieuHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemNhieuHang");
            this.cmdReadExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdReadExcel");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.ToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiViet");
            this.InPhieuTN1 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdInTKDTSuaDoiBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKDTSuaDoiBoSung");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.NhanDuLieu = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieu");
            this.Huy = new Janus.Windows.UI.CommandBars.UICommand("Huy");
            this.cmdThemMotHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemMotHang");
            this.cmdThemNhieuHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemNhieuHang");
            this.ToKhaiViet = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiViet");
            this.InPhieuTN2 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdReadExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdReadExcel");
            this.cmdChungTuKem = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuKem");
            this.cmdGiayPhep1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhep");
            this.cmdHopDongThuongMai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHopDongThuongMai");
            this.cmdHoaDonThuongMai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHoaDonThuongMai");
            this.cmdDeNghiChuyenCuaKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDeNghiChuyenCuaKhau");
            this.cmdChungTuDangAnh1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDangAnh");
            this.cmdGiayPhep = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhep");
            this.cmdHopDongThuongMai = new Janus.Windows.UI.CommandBars.UICommand("cmdHopDongThuongMai");
            this.cmdHoaDonThuongMai = new Janus.Windows.UI.CommandBars.UICommand("cmdHoaDonThuongMai");
            this.cmdDeNghiChuyenCuaKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdDeNghiChuyenCuaKhau");
            this.cmdChungTuDangAnh = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDangAnh");
            this.cmdTruyenDuLieuTuXa = new Janus.Windows.UI.CommandBars.UICommand("cmdTruyenDuLieuTuXa");
            this.cmdSend2 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.NhanDuLieu2 = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieu");
            this.Huy2 = new Janus.Windows.UI.CommandBars.UICommand("Huy");
            this.Separator5 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdSuaToKhaiDaDuyet1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaToKhaiDaDuyet");
            this.cmdHuyToKhaiDaDuyet1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyToKhaiDaDuyet");
            this.Separator6 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdKetQuaXuLy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdSuaToKhaiDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaToKhaiDaDuyet");
            this.cmdHuyToKhaiDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyToKhaiDaDuyet");
            this.cmdKetQuaXuLy = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdInTKDTSuaDoiBoSung = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKDTSuaDoiBoSung");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.ilLarge = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.rfvNgayHHHopDonggiao = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaDVGiao = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDVGiao = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDiaDiemXephang = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoHopDongNhan = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rvLPHQ = new Company.Controls.CustomValidation.RangeValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.pnlToKhaiMauDich.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiKhachHang)).BeginInit();
            this.uiKhachHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiGiao)).BeginInit();
            this.grbNguoiGiao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDongGiao)).BeginInit();
            this.grbHopDongGiao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiDV)).BeginInit();
            this.uiDV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiNguoiChiDinhDV)).BeginInit();
            this.uiNguoiChiDinhDV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiNhan)).BeginInit();
            this.grbNguoiNhan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDongNhan)).BeginInit();
            this.grbHopDongNhan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguyenTe)).BeginInit();
            this.grbNguyenTe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHangMauDich)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiHinhMauDich)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPTTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCuaKhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompanyNuoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHopDongGiao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayHDGiao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaUSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToolBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayHHHopDonggiao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaDVGiao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDVGiao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaDiemXephang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHopDongNhan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvLPHQ)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(3, 37);
            this.grbMain.Size = new System.Drawing.Size(776, 504);
            // 
            // pnlToKhaiMauDich
            // 
            this.pnlToKhaiMauDich.AutoScroll = true;
            this.pnlToKhaiMauDich.AutoScrollMargin = new System.Drawing.Size(4, 4);
            this.pnlToKhaiMauDich.BackColor = System.Drawing.Color.Transparent;
            this.pnlToKhaiMauDich.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlToKhaiMauDich.Controls.Add(this.label21);
            this.pnlToKhaiMauDich.Controls.Add(this.uiKhachHang);
            this.pnlToKhaiMauDich.Controls.Add(this.uiDV);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox10);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNguyenTe);
            this.pnlToKhaiMauDich.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlToKhaiMauDich.Location = new System.Drawing.Point(0, 0);
            this.pnlToKhaiMauDich.Name = "pnlToKhaiMauDich";
            this.pnlToKhaiMauDich.Size = new System.Drawing.Size(776, 504);
            this.pnlToKhaiMauDich.TabIndex = 0;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(695, 629);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(0, 13);
            this.label21.TabIndex = 5;
            this.label21.Visible = false;
            // 
            // uiKhachHang
            // 
            this.uiKhachHang.Controls.Add(this.label18);
            this.uiKhachHang.Controls.Add(this.label13);
            this.uiKhachHang.Controls.Add(this.label17);
            this.uiKhachHang.Controls.Add(this.donViHaiQuanControl2);
            this.uiKhachHang.Controls.Add(this.txtNguoiChiDinhKH);
            this.uiKhachHang.Controls.Add(this.lblNguoiChiDinhKH);
            this.uiKhachHang.Controls.Add(this.grbNguoiGiao);
            this.uiKhachHang.Controls.Add(this.grbHopDongGiao);
            this.uiKhachHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiKhachHang.Location = new System.Drawing.Point(32, 309);
            this.uiKhachHang.Name = "uiKhachHang";
            this.uiKhachHang.Size = new System.Drawing.Size(706, 151);
            this.uiKhachHang.TabIndex = 3;
            this.uiKhachHang.Text = "Bên giao hàng";
            this.uiKhachHang.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiKhachHang.VisualStyleManager = this.vsmMain;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(298, 77);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 13);
            this.label18.TabIndex = 19;
            this.label18.Text = "* ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(298, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 13);
            this.label13.TabIndex = 18;
            this.label13.Text = "* ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(318, 117);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Mã hải quan";
            // 
            // donViHaiQuanControl2
            // 
            this.donViHaiQuanControl2.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl2.Location = new System.Drawing.Point(409, 108);
            this.donViHaiQuanControl2.Ma = "";
            this.donViHaiQuanControl2.MaCuc = "";
            this.donViHaiQuanControl2.Name = "donViHaiQuanControl2";
            this.donViHaiQuanControl2.ReadOnly = false;
            this.donViHaiQuanControl2.Size = new System.Drawing.Size(295, 22);
            this.donViHaiQuanControl2.TabIndex = 5;
            this.donViHaiQuanControl2.VisualStyleManager = this.vsmMain;
            // 
            // txtNguoiChiDinhKH
            // 
            this.txtNguoiChiDinhKH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiChiDinhKH.Location = new System.Drawing.Point(138, 109);
            this.txtNguoiChiDinhKH.Name = "txtNguoiChiDinhKH";
            this.txtNguoiChiDinhKH.Size = new System.Drawing.Size(156, 21);
            this.txtNguoiChiDinhKH.TabIndex = 3;
            this.txtNguoiChiDinhKH.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNguoiChiDinhKH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtNguoiChiDinhKH.VisualStyleManager = this.vsmMain;
            // 
            // lblNguoiChiDinhKH
            // 
            this.lblNguoiChiDinhKH.AutoSize = true;
            this.lblNguoiChiDinhKH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguoiChiDinhKH.Location = new System.Drawing.Point(8, 113);
            this.lblNguoiChiDinhKH.Name = "lblNguoiChiDinhKH";
            this.lblNguoiChiDinhKH.Size = new System.Drawing.Size(124, 13);
            this.lblNguoiChiDinhKH.TabIndex = 2;
            this.lblNguoiChiDinhKH.Text = "Người chỉ định giao hàng";
            // 
            // grbNguoiGiao
            // 
            this.grbNguoiGiao.BorderColor = System.Drawing.Color.Transparent;
            this.grbNguoiGiao.Controls.Add(this.label19);
            this.grbNguoiGiao.Controls.Add(this.label15);
            this.grbNguoiGiao.Controls.Add(this.txtTenDVGiao);
            this.grbNguoiGiao.Controls.Add(this.txtMaDVGiao);
            this.grbNguoiGiao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguoiGiao.Location = new System.Drawing.Point(0, 24);
            this.grbNguoiGiao.Name = "grbNguoiGiao";
            this.grbNguoiGiao.Size = new System.Drawing.Size(300, 79);
            this.grbNguoiGiao.TabIndex = 0;
            this.grbNguoiGiao.Text = "Người giao hàng";
            this.grbNguoiGiao.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbNguoiGiao.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(8, 52);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(57, 13);
            this.label19.TabIndex = 3;
            this.label19.Text = "Tên đơn vị";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(10, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Mã đơn vị";
            // 
            // txtTenDVGiao
            // 
            this.txtTenDVGiao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDVGiao.Location = new System.Drawing.Point(69, 48);
            this.txtTenDVGiao.Name = "txtTenDVGiao";
            this.txtTenDVGiao.Size = new System.Drawing.Size(225, 21);
            this.txtTenDVGiao.TabIndex = 2;
            this.txtTenDVGiao.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDVGiao.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDVGiao.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDVGiao
            // 
            this.txtMaDVGiao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDVGiao.Location = new System.Drawing.Point(69, 20);
            this.txtMaDVGiao.Name = "txtMaDVGiao";
            this.txtMaDVGiao.Size = new System.Drawing.Size(225, 21);
            this.txtMaDVGiao.TabIndex = 1;
            this.txtMaDVGiao.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDVGiao.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaDVGiao.VisualStyleManager = this.vsmMain;
            // 
            // grbHopDongGiao
            // 
            this.grbHopDongGiao.BorderColor = System.Drawing.Color.Transparent;
            this.grbHopDongGiao.Controls.Add(this.label14);
            this.grbHopDongGiao.Controls.Add(this.ccNgayHHHopDongGiao);
            this.grbHopDongGiao.Controls.Add(this.txtSoHongDonggiao);
            this.grbHopDongGiao.Controls.Add(this.lblHDKH);
            this.grbHopDongGiao.Controls.Add(this.lblNgayHDKH);
            this.grbHopDongGiao.Controls.Add(this.lblNgayHHKH);
            this.grbHopDongGiao.Controls.Add(this.ccNgayHopDongGiao);
            this.grbHopDongGiao.Controls.Add(this.uiGroupBox7);
            this.grbHopDongGiao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbHopDongGiao.Location = new System.Drawing.Point(306, 24);
            this.grbHopDongGiao.Name = "grbHopDongGiao";
            this.grbHopDongGiao.Size = new System.Drawing.Size(400, 79);
            this.grbHopDongGiao.TabIndex = 1;
            this.grbHopDongGiao.Text = "Hợp đồng giao";
            this.grbHopDongGiao.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbHopDongGiao.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(156, 53);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 13);
            this.label14.TabIndex = 8;
            this.label14.Text = "* ";
            // 
            // ccNgayHHHopDongGiao
            // 
            // 
            // 
            // 
            this.ccNgayHHHopDongGiao.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHHHopDongGiao.DropDownCalendar.Name = "";
            this.ccNgayHHHopDongGiao.DropDownCalendar.Visible = false;
            this.ccNgayHHHopDongGiao.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHHHopDongGiao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHHHopDongGiao.Location = new System.Drawing.Point(298, 48);
            this.ccNgayHHHopDongGiao.Name = "ccNgayHHHopDongGiao";
            this.ccNgayHHHopDongGiao.Nullable = true;
            this.ccNgayHHHopDongGiao.NullButtonText = "Xóa";
            this.ccNgayHHHopDongGiao.ShowNullButton = true;
            this.ccNgayHHHopDongGiao.Size = new System.Drawing.Size(85, 21);
            this.ccNgayHHHopDongGiao.TabIndex = 5;
            this.ccNgayHHHopDongGiao.TodayButtonText = "Hôm nay";
            this.ccNgayHHHopDongGiao.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHHHopDongGiao.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHongDonggiao
            // 
            this.txtSoHongDonggiao.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoHongDonggiao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHongDonggiao.Location = new System.Drawing.Point(15, 48);
            this.txtSoHongDonggiao.Name = "txtSoHongDonggiao";
            this.txtSoHongDonggiao.Size = new System.Drawing.Size(139, 21);
            this.txtSoHongDonggiao.TabIndex = 1;
            this.txtSoHongDonggiao.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHongDonggiao.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoHongDonggiao.VisualStyleManager = this.vsmMain;
            this.txtSoHongDonggiao.ButtonClick += new System.EventHandler(this.txtSoHongDonggiao_ButtonClick);
            // 
            // lblHDKH
            // 
            this.lblHDKH.AutoSize = true;
            this.lblHDKH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHDKH.Location = new System.Drawing.Point(12, 28);
            this.lblHDKH.Name = "lblHDKH";
            this.lblHDKH.Size = new System.Drawing.Size(60, 13);
            this.lblHDKH.TabIndex = 0;
            this.lblHDKH.Text = "Số HĐ giao";
            // 
            // lblNgayHDKH
            // 
            this.lblNgayHDKH.AutoSize = true;
            this.lblNgayHDKH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHDKH.Location = new System.Drawing.Point(177, 28);
            this.lblNgayHDKH.Name = "lblNgayHDKH";
            this.lblNgayHDKH.Size = new System.Drawing.Size(73, 13);
            this.lblNgayHDKH.TabIndex = 2;
            this.lblNgayHDKH.Text = "Ngày HĐ giao";
            // 
            // lblNgayHHKH
            // 
            this.lblNgayHHKH.AutoSize = true;
            this.lblNgayHHKH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHHKH.Location = new System.Drawing.Point(297, 29);
            this.lblNgayHHKH.Name = "lblNgayHHKH";
            this.lblNgayHHKH.Size = new System.Drawing.Size(72, 13);
            this.lblNgayHHKH.TabIndex = 4;
            this.lblNgayHHKH.Text = "Ngày hết hạn";
            // 
            // ccNgayHopDongGiao
            // 
            // 
            // 
            // 
            this.ccNgayHopDongGiao.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHopDongGiao.DropDownCalendar.Name = "";
            this.ccNgayHopDongGiao.DropDownCalendar.Visible = false;
            this.ccNgayHopDongGiao.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHopDongGiao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHopDongGiao.Location = new System.Drawing.Point(177, 48);
            this.ccNgayHopDongGiao.Name = "ccNgayHopDongGiao";
            this.ccNgayHopDongGiao.Nullable = true;
            this.ccNgayHopDongGiao.NullButtonText = "Xóa";
            this.ccNgayHopDongGiao.ShowNullButton = true;
            this.ccNgayHopDongGiao.Size = new System.Drawing.Size(114, 21);
            this.ccNgayHopDongGiao.TabIndex = 3;
            this.ccNgayHopDongGiao.TodayButtonText = "Hôm nay";
            this.ccNgayHopDongGiao.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHopDongGiao.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.Controls.Add(this.ctrLoaiHinhMauDich);
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.Location = new System.Drawing.Point(374, 79);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(168, 104);
            this.uiGroupBox7.TabIndex = 2;
            this.uiGroupBox7.Text = "Loại hình mậu dịch";
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // ctrLoaiHinhMauDich
            // 
            this.ctrLoaiHinhMauDich.BackColor = System.Drawing.Color.Transparent;
            this.ctrLoaiHinhMauDich.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrLoaiHinhMauDich.Location = new System.Drawing.Point(7, 24);
            this.ctrLoaiHinhMauDich.Ma = "";
            this.ctrLoaiHinhMauDich.Name = "ctrLoaiHinhMauDich";
            this.ctrLoaiHinhMauDich.Nhom = "";
            this.ctrLoaiHinhMauDich.ReadOnly = false;
            this.ctrLoaiHinhMauDich.Size = new System.Drawing.Size(169, 50);
            this.ctrLoaiHinhMauDich.TabIndex = 0;
            this.ctrLoaiHinhMauDich.VisualStyleManager = this.vsmMain;
            // 
            // uiDV
            // 
            this.uiDV.Controls.Add(this.label33);
            this.uiDV.Controls.Add(this.numTrongLuongTinh);
            this.uiDV.Controls.Add(this.numTrongLuong);
            this.uiDV.Controls.Add(this.numSoKien);
            this.uiDV.Controls.Add(this.label32);
            this.uiDV.Controls.Add(this.label28);
            this.uiDV.Controls.Add(this.uiGroupBox6);
            this.uiDV.Controls.Add(this.uiNguoiChiDinhDV);
            this.uiDV.Controls.Add(this.grbNguoiNhan);
            this.uiDV.Controls.Add(this.grbHopDongNhan);
            this.uiDV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiDV.Location = new System.Drawing.Point(32, 108);
            this.uiDV.Name = "uiDV";
            this.uiDV.Size = new System.Drawing.Size(705, 195);
            this.uiDV.TabIndex = 1;
            this.uiDV.Text = "Bên nhận hàng";
            this.uiDV.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiDV.VisualStyleManager = this.vsmMain;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(579, 170);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(43, 13);
            this.label33.TabIndex = 6;
            this.label33.Text = "TL tịnh:";
            // 
            // numTrongLuongTinh
            // 
            this.numTrongLuongTinh.DecimalDigits = 3;
            this.numTrongLuongTinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numTrongLuongTinh.Location = new System.Drawing.Point(628, 165);
            this.numTrongLuongTinh.Name = "numTrongLuongTinh";
            this.numTrongLuongTinh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numTrongLuongTinh.Size = new System.Drawing.Size(60, 21);
            this.numTrongLuongTinh.TabIndex = 6;
            this.numTrongLuongTinh.Text = "0.000";
            this.numTrongLuongTinh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numTrongLuongTinh.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.numTrongLuongTinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // numTrongLuong
            // 
            this.numTrongLuong.DecimalDigits = 3;
            this.numTrongLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numTrongLuong.Location = new System.Drawing.Point(504, 165);
            this.numTrongLuong.Name = "numTrongLuong";
            this.numTrongLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numTrongLuong.Size = new System.Drawing.Size(60, 21);
            this.numTrongLuong.TabIndex = 5;
            this.numTrongLuong.Text = "0.000";
            this.numTrongLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numTrongLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.numTrongLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // numSoKien
            // 
            this.numSoKien.DecimalDigits = 3;
            this.numSoKien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numSoKien.Location = new System.Drawing.Point(369, 165);
            this.numSoKien.Name = "numSoKien";
            this.numSoKien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numSoKien.Size = new System.Drawing.Size(55, 21);
            this.numSoKien.TabIndex = 4;
            this.numSoKien.Text = "0.000";
            this.numSoKien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numSoKien.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.numSoKien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.numSoKien.VisualStyleManager = this.vsmMain;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(437, 170);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(69, 13);
            this.label32.TabIndex = 4;
            this.label32.Text = "Trọng lượng:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(318, 170);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(45, 13);
            this.label28.TabIndex = 4;
            this.label28.Text = "Số kiện:";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BorderColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.label27);
            this.uiGroupBox6.Controls.Add(this.txtDiaDiemGiaohang);
            this.uiGroupBox6.Controls.Add(this.ccThoiGianGiaoHang);
            this.uiGroupBox6.Controls.Add(this.label5);
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(306, 121);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(407, 46);
            this.uiGroupBox6.TabIndex = 3;
            this.uiGroupBox6.Text = "Địa điểm giao hàng";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(277, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(116, 13);
            this.label27.TabIndex = 2;
            this.label27.Text = "Thời gian giao hàng";
            // 
            // txtDiaDiemGiaohang
            // 
            this.txtDiaDiemGiaohang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiemGiaohang.Location = new System.Drawing.Point(10, 17);
            this.txtDiaDiemGiaohang.Name = "txtDiaDiemGiaohang";
            this.txtDiaDiemGiaohang.Size = new System.Drawing.Size(267, 21);
            this.txtDiaDiemGiaohang.TabIndex = 0;
            this.txtDiaDiemGiaohang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaDiemGiaohang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDiaDiemGiaohang.VisualStyleManager = this.vsmMain;
            // 
            // ccThoiGianGiaoHang
            // 
            // 
            // 
            // 
            this.ccThoiGianGiaoHang.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccThoiGianGiaoHang.DropDownCalendar.Name = "";
            this.ccThoiGianGiaoHang.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccThoiGianGiaoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccThoiGianGiaoHang.Location = new System.Drawing.Point(297, 17);
            this.ccThoiGianGiaoHang.Name = "ccThoiGianGiaoHang";
            this.ccThoiGianGiaoHang.NullButtonText = "Xóa";
            this.ccThoiGianGiaoHang.ShowNullButton = true;
            this.ccThoiGianGiaoHang.Size = new System.Drawing.Size(85, 21);
            this.ccThoiGianGiaoHang.TabIndex = 1;
            this.ccThoiGianGiaoHang.TodayButtonText = "Hôm nay";
            this.ccThoiGianGiaoHang.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(278, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "* ";
            // 
            // uiNguoiChiDinhDV
            // 
            this.uiNguoiChiDinhDV.BorderColor = System.Drawing.Color.Transparent;
            this.uiNguoiChiDinhDV.Controls.Add(this.txtNguoiChiDinhDV);
            this.uiNguoiChiDinhDV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiNguoiChiDinhDV.Location = new System.Drawing.Point(5, 121);
            this.uiNguoiChiDinhDV.Name = "uiNguoiChiDinhDV";
            this.uiNguoiChiDinhDV.Size = new System.Drawing.Size(294, 46);
            this.uiNguoiChiDinhDV.TabIndex = 2;
            this.uiNguoiChiDinhDV.Text = "Người chỉ định nhận hàng";
            this.uiNguoiChiDinhDV.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiNguoiChiDinhDV.VisualStyleManager = this.vsmMain;
            // 
            // txtNguoiChiDinhDV
            // 
            this.txtNguoiChiDinhDV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiChiDinhDV.Location = new System.Drawing.Point(2, 17);
            this.txtNguoiChiDinhDV.Name = "txtNguoiChiDinhDV";
            this.txtNguoiChiDinhDV.Size = new System.Drawing.Size(286, 21);
            this.txtNguoiChiDinhDV.TabIndex = 0;
            this.txtNguoiChiDinhDV.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNguoiChiDinhDV.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtNguoiChiDinhDV.VisualStyleManager = this.vsmMain;
            // 
            // grbNguoiNhan
            // 
            this.grbNguoiNhan.BorderColor = System.Drawing.Color.Transparent;
            this.grbNguoiNhan.Controls.Add(this.label20);
            this.grbNguoiNhan.Controls.Add(this.label3);
            this.grbNguoiNhan.Controls.Add(this.txtTenDonVinhan);
            this.grbNguoiNhan.Controls.Add(this.txtMaDonVinhan);
            this.grbNguoiNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguoiNhan.Location = new System.Drawing.Point(0, 30);
            this.grbNguoiNhan.Name = "grbNguoiNhan";
            this.grbNguoiNhan.Size = new System.Drawing.Size(300, 79);
            this.grbNguoiNhan.TabIndex = 0;
            this.grbNguoiNhan.Text = "Người nhận hàng";
            this.grbNguoiNhan.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbNguoiNhan.VisualStyleManager = this.vsmMain;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(9, 53);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 13);
            this.label20.TabIndex = 4;
            this.label20.Text = "Tên đơn vị";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Mã đơn vị";
            // 
            // txtTenDonVinhan
            // 
            this.txtTenDonVinhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonVinhan.Location = new System.Drawing.Point(68, 48);
            this.txtTenDonVinhan.Name = "txtTenDonVinhan";
            this.txtTenDonVinhan.ReadOnly = true;
            this.txtTenDonVinhan.Size = new System.Drawing.Size(226, 21);
            this.txtTenDonVinhan.TabIndex = 2;
            this.txtTenDonVinhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonVinhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenDonVinhan.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDonVinhan
            // 
            this.txtMaDonVinhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonVinhan.Location = new System.Drawing.Point(69, 20);
            this.txtMaDonVinhan.Name = "txtMaDonVinhan";
            this.txtMaDonVinhan.ReadOnly = true;
            this.txtMaDonVinhan.Size = new System.Drawing.Size(225, 21);
            this.txtMaDonVinhan.TabIndex = 1;
            this.txtMaDonVinhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonVinhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaDonVinhan.VisualStyleManager = this.vsmMain;
            // 
            // grbHopDongNhan
            // 
            this.grbHopDongNhan.BorderColor = System.Drawing.Color.Transparent;
            this.grbHopDongNhan.Controls.Add(this.label10);
            this.grbHopDongNhan.Controls.Add(this.ccNgayHHHopDongNhan);
            this.grbHopDongNhan.Controls.Add(this.txtSoHopDongNhan);
            this.grbHopDongNhan.Controls.Add(this.lblSoHDDV);
            this.grbHopDongNhan.Controls.Add(this.lblNgayHDDV);
            this.grbHopDongNhan.Controls.Add(this.lblNgayHHDV);
            this.grbHopDongNhan.Controls.Add(this.ccNgayHopDongNhan);
            this.grbHopDongNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbHopDongNhan.Location = new System.Drawing.Point(306, 31);
            this.grbHopDongNhan.Name = "grbHopDongNhan";
            this.grbHopDongNhan.Size = new System.Drawing.Size(399, 78);
            this.grbHopDongNhan.TabIndex = 1;
            this.grbHopDongNhan.Text = "Hợp đồng nhận";
            this.grbHopDongNhan.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbHopDongNhan.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(156, 52);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "* ";
            // 
            // ccNgayHHHopDongNhan
            // 
            // 
            // 
            // 
            this.ccNgayHHHopDongNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHHHopDongNhan.DropDownCalendar.Name = "";
            this.ccNgayHHHopDongNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHHHopDongNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHHHopDongNhan.Location = new System.Drawing.Point(297, 47);
            this.ccNgayHHHopDongNhan.Name = "ccNgayHHHopDongNhan";
            this.ccNgayHHHopDongNhan.Nullable = true;
            this.ccNgayHHHopDongNhan.NullButtonText = "Xóa";
            this.ccNgayHHHopDongNhan.ReadOnly = true;
            this.ccNgayHHHopDongNhan.ShowNullButton = true;
            this.ccNgayHHHopDongNhan.Size = new System.Drawing.Size(85, 21);
            this.ccNgayHHHopDongNhan.TabIndex = 5;
            this.ccNgayHHHopDongNhan.TodayButtonText = "Hôm nay";
            this.ccNgayHHHopDongNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHHHopDongNhan.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHopDongNhan
            // 
            this.txtSoHopDongNhan.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoHopDongNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDongNhan.Location = new System.Drawing.Point(15, 47);
            this.txtSoHopDongNhan.Name = "txtSoHopDongNhan";
            this.txtSoHopDongNhan.ReadOnly = true;
            this.txtSoHopDongNhan.Size = new System.Drawing.Size(139, 21);
            this.txtSoHopDongNhan.TabIndex = 1;
            this.txtSoHopDongNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDongNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoHopDongNhan.VisualStyleManager = this.vsmMain;
            this.txtSoHopDongNhan.ButtonClick += new System.EventHandler(this.txtSoHopDongNhan_ButtonClick);
            // 
            // lblSoHDDV
            // 
            this.lblSoHDDV.AutoSize = true;
            this.lblSoHDDV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHDDV.Location = new System.Drawing.Point(12, 27);
            this.lblSoHDDV.Name = "lblSoHDDV";
            this.lblSoHDDV.Size = new System.Drawing.Size(64, 13);
            this.lblSoHDDV.TabIndex = 0;
            this.lblSoHDDV.Text = "Số HĐ nhận";
            // 
            // lblNgayHDDV
            // 
            this.lblNgayHDDV.AutoSize = true;
            this.lblNgayHDDV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHDDV.Location = new System.Drawing.Point(174, 25);
            this.lblNgayHDDV.Name = "lblNgayHDDV";
            this.lblNgayHDDV.Size = new System.Drawing.Size(77, 13);
            this.lblNgayHDDV.TabIndex = 2;
            this.lblNgayHDDV.Text = "Ngày HĐ nhận";
            // 
            // lblNgayHHDV
            // 
            this.lblNgayHHDV.AutoSize = true;
            this.lblNgayHHDV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHHDV.Location = new System.Drawing.Point(296, 25);
            this.lblNgayHHDV.Name = "lblNgayHHDV";
            this.lblNgayHHDV.Size = new System.Drawing.Size(72, 13);
            this.lblNgayHHDV.TabIndex = 4;
            this.lblNgayHHDV.Text = "Ngày hết hạn";
            // 
            // ccNgayHopDongNhan
            // 
            // 
            // 
            // 
            this.ccNgayHopDongNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHopDongNhan.DropDownCalendar.Name = "";
            this.ccNgayHopDongNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHopDongNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHopDongNhan.Location = new System.Drawing.Point(176, 47);
            this.ccNgayHopDongNhan.Name = "ccNgayHopDongNhan";
            this.ccNgayHopDongNhan.Nullable = true;
            this.ccNgayHopDongNhan.NullButtonText = "Xóa";
            this.ccNgayHopDongNhan.ReadOnly = true;
            this.ccNgayHopDongNhan.ShowNullButton = true;
            this.ccNgayHopDongNhan.Size = new System.Drawing.Size(114, 21);
            this.ccNgayHopDongNhan.TabIndex = 3;
            this.ccNgayHopDongNhan.TodayButtonText = "Hôm nay";
            this.ccNgayHopDongNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayHopDongNhan.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.Controls.Add(this.cbLoaiHinh);
            this.uiGroupBox10.Controls.Add(this.label16);
            this.uiGroupBox10.Controls.Add(this.txtToKhaiSo);
            this.uiGroupBox10.Controls.Add(this.txtCanbodangky);
            this.uiGroupBox10.Controls.Add(this.label22);
            this.uiGroupBox10.Controls.Add(this.label24);
            this.uiGroupBox10.Controls.Add(this.label25);
            this.uiGroupBox10.Controls.Add(this.ccNgayDangKy);
            this.uiGroupBox10.Controls.Add(this.lblPhanLuong);
            this.uiGroupBox10.Controls.Add(this.lbltrangthai);
            this.uiGroupBox10.Controls.Add(this.label29);
            this.uiGroupBox10.Controls.Add(this.txtsotiepnhan);
            this.uiGroupBox10.Controls.Add(this.label31);
            this.uiGroupBox10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox10.Location = new System.Drawing.Point(31, 3);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(704, 99);
            this.uiGroupBox10.TabIndex = 0;
            this.uiGroupBox10.Text = "Thông tin chung";
            this.uiGroupBox10.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox10.VisualStyleManager = this.vsmMain;
            // 
            // cbLoaiHinh
            // 
            this.cbLoaiHinh.BackColor = System.Drawing.Color.Transparent;
            this.cbLoaiHinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLoaiHinh.Location = new System.Drawing.Point(154, 68);
            this.cbLoaiHinh.Ma = "";
            this.cbLoaiHinh.Name = "cbLoaiHinh";
            this.cbLoaiHinh.Nhom = "";
            this.cbLoaiHinh.ReadOnly = false;
            this.cbLoaiHinh.Size = new System.Drawing.Size(288, 22);
            this.cbLoaiHinh.TabIndex = 9;
            this.cbLoaiHinh.VisualStyleManager = null;
            this.cbLoaiHinh.ValueChanged += new Company.Interface.Controls.LoaiHinhChuyenTiepControl.ValueChangedEventHandler(this.cbLoaiHinh_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(28, 77);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(126, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Loại hình chuyển tiếp";
            // 
            // txtToKhaiSo
            // 
            this.txtToKhaiSo.BackColor = System.Drawing.Color.White;
            this.txtToKhaiSo.Location = new System.Drawing.Point(154, 38);
            this.txtToKhaiSo.Name = "txtToKhaiSo";
            this.txtToKhaiSo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtToKhaiSo.Size = new System.Drawing.Size(46, 21);
            this.txtToKhaiSo.TabIndex = 7;
            this.txtToKhaiSo.Text = "0";
            this.txtToKhaiSo.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtToKhaiSo.Value = 0;
            this.txtToKhaiSo.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtToKhaiSo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtToKhaiSo.VisualStyleManager = this.vsmMain;
            // 
            // txtCanbodangky
            // 
            this.txtCanbodangky.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCanbodangky.Location = new System.Drawing.Point(444, 68);
            this.txtCanbodangky.Name = "txtCanbodangky";
            this.txtCanbodangky.Size = new System.Drawing.Size(250, 21);
            this.txtCanbodangky.TabIndex = 13;
            this.txtCanbodangky.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtCanbodangky.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtCanbodangky.VisualStyleManager = this.vsmMain;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(441, 46);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(93, 13);
            this.label22.TabIndex = 12;
            this.label22.Text = "Cán bộ đăng ký";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(211, 46);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 13);
            this.label24.TabIndex = 10;
            this.label24.Text = "Ngày đăng ký";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(28, 46);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(63, 13);
            this.label25.TabIndex = 6;
            this.label25.Text = "Số tờ khai";
            // 
            // ccNgayDangKy
            // 
            this.ccNgayDangKy.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccNgayDangKy.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayDangKy.DropDownCalendar.Name = "";
            this.ccNgayDangKy.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayDangKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayDangKy.Location = new System.Drawing.Point(332, 41);
            this.ccNgayDangKy.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ccNgayDangKy.Name = "ccNgayDangKy";
            this.ccNgayDangKy.NullButtonText = "Xóa";
            this.ccNgayDangKy.ShowNullButton = true;
            this.ccNgayDangKy.Size = new System.Drawing.Size(96, 21);
            this.ccNgayDangKy.TabIndex = 11;
            this.ccNgayDangKy.TodayButtonText = "Hôm nay";
            this.ccNgayDangKy.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayDangKy.VisualStyleManager = this.vsmMain;
            // 
            // lblPhanLuong
            // 
            this.lblPhanLuong.AutoSize = true;
            this.lblPhanLuong.BackColor = System.Drawing.Color.Transparent;
            this.lblPhanLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLuong.ForeColor = System.Drawing.Color.Red;
            this.lblPhanLuong.Location = new System.Drawing.Point(425, 23);
            this.lblPhanLuong.Name = "lblPhanLuong";
            this.lblPhanLuong.Size = new System.Drawing.Size(0, 13);
            this.lblPhanLuong.TabIndex = 3;
            // 
            // lbltrangthai
            // 
            this.lbltrangthai.AutoSize = true;
            this.lbltrangthai.BackColor = System.Drawing.Color.Transparent;
            this.lbltrangthai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltrangthai.ForeColor = System.Drawing.Color.Red;
            this.lbltrangthai.Location = new System.Drawing.Point(283, 23);
            this.lbltrangthai.Name = "lbltrangthai";
            this.lbltrangthai.Size = new System.Drawing.Size(133, 13);
            this.lbltrangthai.TabIndex = 3;
            this.lbltrangthai.Text = "Chưa gửi đến Hải quan";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(212, 23);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(65, 13);
            this.label29.TabIndex = 2;
            this.label29.Text = "Trạng thái";
            // 
            // txtsotiepnhan
            // 
            this.txtsotiepnhan.Location = new System.Drawing.Point(154, 15);
            this.txtsotiepnhan.Name = "txtsotiepnhan";
            this.txtsotiepnhan.ReadOnly = true;
            this.txtsotiepnhan.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtsotiepnhan.Size = new System.Drawing.Size(46, 21);
            this.txtsotiepnhan.TabIndex = 1;
            this.txtsotiepnhan.Text = "0";
            this.txtsotiepnhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtsotiepnhan.Value = 0;
            this.txtsotiepnhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtsotiepnhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtsotiepnhan.VisualStyleManager = this.vsmMain;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(28, 23);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(77, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "Số tiếp nhận";
            // 
            // grbNguyenTe
            // 
            this.grbNguyenTe.Controls.Add(this.cbPTTT);
            this.grbNguyenTe.Controls.Add(this.cbDKGH);
            this.grbNguyenTe.Controls.Add(this.txtMaDonViUyThac);
            this.grbNguyenTe.Controls.Add(this.label12);
            this.grbNguyenTe.Controls.Add(this.label11);
            this.grbNguyenTe.Controls.Add(this.label6);
            this.grbNguyenTe.Controls.Add(this.txtSoBienLai);
            this.grbNguyenTe.Controls.Add(this.txtChungTu);
            this.grbNguyenTe.Controls.Add(this.label8);
            this.grbNguyenTe.Controls.Add(this.label7);
            this.grbNguyenTe.Controls.Add(this.ccNgayBienLai);
            this.grbNguyenTe.Controls.Add(this.label4);
            this.grbNguyenTe.Controls.Add(this.txtLePhiHQ);
            this.grbNguyenTe.Controls.Add(this.label2);
            this.grbNguyenTe.Controls.Add(this.label23);
            this.grbNguyenTe.Controls.Add(this.label26);
            this.grbNguyenTe.Controls.Add(this.label1);
            this.grbNguyenTe.Controls.Add(this.nguyenTeControl1);
            this.grbNguyenTe.Controls.Add(this.txtTyGiaTinhThue);
            this.grbNguyenTe.Controls.Add(this.label9);
            this.grbNguyenTe.Controls.Add(this.label30);
            this.grbNguyenTe.Controls.Add(this.txtTyGiaUSD);
            this.grbNguyenTe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguyenTe.Location = new System.Drawing.Point(31, 466);
            this.grbNguyenTe.Name = "grbNguyenTe";
            this.grbNguyenTe.Size = new System.Drawing.Size(706, 159);
            this.grbNguyenTe.TabIndex = 4;
            this.grbNguyenTe.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbNguyenTe.VisualStyleManager = this.vsmMain;
            // 
            // cbPTTT
            // 
            this.cbPTTT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTTT.DisplayMember = "Name";
            this.cbPTTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTTT.Location = new System.Drawing.Point(123, 73);
            this.cbPTTT.Name = "cbPTTT";
            this.cbPTTT.Size = new System.Drawing.Size(193, 21);
            this.cbPTTT.TabIndex = 3;
            this.cbPTTT.ValueMember = "ID";
            this.cbPTTT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbPTTT.VisualStyleManager = this.vsmMain;
            // 
            // cbDKGH
            // 
            this.cbDKGH.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDKGH.DisplayMember = "ID";
            this.cbDKGH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDKGH.Location = new System.Drawing.Point(122, 46);
            this.cbDKGH.Name = "cbDKGH";
            this.cbDKGH.Size = new System.Drawing.Size(194, 21);
            this.cbDKGH.TabIndex = 2;
            this.cbDKGH.ValueMember = "ID";
            this.cbDKGH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbDKGH.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDonViUyThac
            // 
            this.txtMaDonViUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonViUyThac.Location = new System.Drawing.Point(138, 129);
            this.txtMaDonViUyThac.Name = "txtMaDonViUyThac";
            this.txtMaDonViUyThac.Size = new System.Drawing.Size(551, 21);
            this.txtMaDonViUyThac.TabIndex = 10;
            this.txtMaDonViUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonViUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaDonViUyThac.VisualStyleManager = this.vsmMain;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(691, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "* ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(502, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "* ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(5, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Đại lý làm thủ tục hải quan";
            // 
            // txtSoBienLai
            // 
            this.txtSoBienLai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoBienLai.Location = new System.Drawing.Point(415, 73);
            this.txtSoBienLai.Name = "txtSoBienLai";
            this.txtSoBienLai.Size = new System.Drawing.Size(81, 21);
            this.txtSoBienLai.TabIndex = 7;
            this.txtSoBienLai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoBienLai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoBienLai.VisualStyleManager = this.vsmMain;
            // 
            // txtChungTu
            // 
            this.txtChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChungTu.Location = new System.Drawing.Point(207, 102);
            this.txtChungTu.Name = "txtChungTu";
            this.txtChungTu.Size = new System.Drawing.Size(482, 21);
            this.txtChungTu.TabIndex = 9;
            this.txtChungTu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtChungTu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtChungTu.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(197, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Chứng từ kèm theo (chỉ định giao hàng)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(534, 78);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Ngày biên lai";
            // 
            // ccNgayBienLai
            // 
            this.ccNgayBienLai.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccNgayBienLai.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayBienLai.DropDownCalendar.Name = "";
            this.ccNgayBienLai.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayBienLai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayBienLai.Location = new System.Drawing.Point(608, 73);
            this.ccNgayBienLai.Name = "ccNgayBienLai";
            this.ccNgayBienLai.Nullable = true;
            this.ccNgayBienLai.NullButtonText = "Xóa";
            this.ccNgayBienLai.ShowNullButton = true;
            this.ccNgayBienLai.Size = new System.Drawing.Size(81, 21);
            this.ccNgayBienLai.TabIndex = 8;
            this.ccNgayBienLai.TodayButtonText = "Hôm nay";
            this.ccNgayBienLai.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayBienLai.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(354, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Số biên lai";
            // 
            // txtLePhiHQ
            // 
            this.txtLePhiHQ.DecimalDigits = 3;
            this.txtLePhiHQ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLePhiHQ.Location = new System.Drawing.Point(415, 45);
            this.txtLePhiHQ.Name = "txtLePhiHQ";
            this.txtLePhiHQ.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtLePhiHQ.Size = new System.Drawing.Size(275, 21);
            this.txtLePhiHQ.TabIndex = 6;
            this.txtLePhiHQ.Text = "0.000";
            this.txtLePhiHQ.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLePhiHQ.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtLePhiHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtLePhiHQ.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(330, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Lệ phí hải quan";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(1, 78);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(125, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Phương thức thanh toán";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(5, 51);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(101, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Điều kiện giao hàng";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nguyên tệ thanh toán";
            // 
            // nguyenTeControl1
            // 
            this.nguyenTeControl1.BackColor = System.Drawing.Color.Transparent;
            this.nguyenTeControl1.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.nguyenTeControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nguyenTeControl1.Location = new System.Drawing.Point(122, 18);
            this.nguyenTeControl1.Ma = "";
            this.nguyenTeControl1.Name = "nguyenTeControl1";
            this.nguyenTeControl1.ReadOnly = false;
            this.nguyenTeControl1.Size = new System.Drawing.Size(207, 22);
            this.nguyenTeControl1.TabIndex = 1;
            this.nguyenTeControl1.VisualStyleManager = this.vsmMain;
            this.nguyenTeControl1.ValueChanged += new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(this.nguyenTeControl1_ValueChanged);
            // 
            // txtTyGiaTinhThue
            // 
            this.txtTyGiaTinhThue.DecimalDigits = 3;
            this.txtTyGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGiaTinhThue.Location = new System.Drawing.Point(415, 19);
            this.txtTyGiaTinhThue.Name = "txtTyGiaTinhThue";
            this.txtTyGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyGiaTinhThue.Size = new System.Drawing.Size(81, 21);
            this.txtTyGiaTinhThue.TabIndex = 4;
            this.txtTyGiaTinhThue.Text = "0.000";
            this.txtTyGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTyGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTyGiaTinhThue.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(535, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Tỷ giá USD";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(347, 24);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(59, 13);
            this.label30.TabIndex = 4;
            this.label30.Text = "Tỷ giá VND";
            // 
            // txtTyGiaUSD
            // 
            this.txtTyGiaUSD.DecimalDigits = 3;
            this.txtTyGiaUSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGiaUSD.Location = new System.Drawing.Point(608, 18);
            this.txtTyGiaUSD.Name = "txtTyGiaUSD";
            this.txtTyGiaUSD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyGiaUSD.Size = new System.Drawing.Size(81, 21);
            this.txtTyGiaUSD.TabIndex = 5;
            this.txtTyGiaUSD.Text = "0.000";
            this.txtTyGiaUSD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyGiaUSD.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTyGiaUSD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTyGiaUSD.VisualStyleManager = this.vsmMain;
            // 
            // ds
            // 
            this.ds.DataSetName = "NewDataSet";
            this.ds.Locale = new System.Globalization.CultureInfo("en-US");
            this.ds.Tables.AddRange(new System.Data.DataTable[] {
            this.dtHangMauDich,
            this.dtLoaiHinhMauDich,
            this.dtPTTT,
            this.dtCuaKhau,
            this.dtNguyenTe,
            this.dtCompanyNuoc,
            this.dtDonViHaiQuan});
            // 
            // dtHangMauDich
            // 
            this.dtHangMauDich.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9});
            this.dtHangMauDich.TableName = "HangMauDich";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "hmd_SoThuTuHang";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "hmd_MaHS";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "hmd_MaPhu";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "hmd_TenHang";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "nuoc_Ten";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "donvitinh_Ten";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "hmd_Luong";
            this.dataColumn7.DataType = typeof(decimal);
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "hmd_DonGiaKB";
            this.dataColumn8.DataType = typeof(decimal);
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "hmd_TriGiaKB";
            this.dataColumn9.DataType = typeof(decimal);
            // 
            // dtLoaiHinhMauDich
            // 
            this.dtLoaiHinhMauDich.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn10,
            this.dataColumn11});
            this.dtLoaiHinhMauDich.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "loaihinh_Ma"}, true)});
            this.dtLoaiHinhMauDich.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn10};
            this.dtLoaiHinhMauDich.TableName = "LoaiHinhMauDich";
            // 
            // dataColumn10
            // 
            this.dataColumn10.AllowDBNull = false;
            this.dataColumn10.ColumnName = "loaihinh_Ma";
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "loaihinh_Ten";
            // 
            // dtPTTT
            // 
            this.dtPTTT.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn12,
            this.dataColumn13});
            this.dtPTTT.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "pttt_Ma"}, true)});
            this.dtPTTT.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn12};
            this.dtPTTT.TableName = "PhuongThucThanhToan";
            // 
            // dataColumn12
            // 
            this.dataColumn12.AllowDBNull = false;
            this.dataColumn12.ColumnName = "pttt_Ma";
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnName = "pttt_GhiChu";
            // 
            // dtCuaKhau
            // 
            this.dtCuaKhau.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn16});
            this.dtCuaKhau.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "cuakhau_Ma"}, true)});
            this.dtCuaKhau.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn14};
            this.dtCuaKhau.TableName = "CuaKhau";
            // 
            // dataColumn14
            // 
            this.dataColumn14.AllowDBNull = false;
            this.dataColumn14.ColumnName = "cuakhau_Ma";
            // 
            // dataColumn15
            // 
            this.dataColumn15.ColumnName = "cuakhau_Ten";
            // 
            // dataColumn16
            // 
            this.dataColumn16.ColumnName = "cuc_ma";
            // 
            // dtNguyenTe
            // 
            this.dtNguyenTe.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn17,
            this.dataColumn18});
            this.dtNguyenTe.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nguyente_Ma"}, true)});
            this.dtNguyenTe.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn17};
            this.dtNguyenTe.TableName = "NguyenTe";
            // 
            // dataColumn17
            // 
            this.dataColumn17.AllowDBNull = false;
            this.dataColumn17.ColumnName = "nguyente_Ma";
            // 
            // dataColumn18
            // 
            this.dataColumn18.ColumnName = "nguyente_Ten";
            // 
            // dtCompanyNuoc
            // 
            this.dtCompanyNuoc.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn19,
            this.dataColumn20});
            this.dtCompanyNuoc.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nuoc_Ma"}, true)});
            this.dtCompanyNuoc.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn19};
            this.dtCompanyNuoc.TableName = "CompanyNuoc";
            // 
            // dataColumn19
            // 
            this.dataColumn19.AllowDBNull = false;
            this.dataColumn19.ColumnName = "nuoc_Ma";
            // 
            // dataColumn20
            // 
            this.dataColumn20.ColumnName = "nuoc_Ten";
            // 
            // dtDonViHaiQuan
            // 
            this.dtDonViHaiQuan.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn21,
            this.dataColumn22});
            this.dtDonViHaiQuan.TableName = "DonViHaiQuan";
            // 
            // dataColumn21
            // 
            this.dataColumn21.ColumnName = "donvihaiquan_Ma";
            // 
            // dataColumn22
            // 
            this.dataColumn22.ColumnName = "donvihaiquan_Ten";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvSoHopDongGiao
            // 
            this.rfvSoHopDongGiao.ControlToValidate = this.txtSoHongDonggiao;
            this.rfvSoHopDongGiao.ErrorMessage = "\"Số Hợp Đồng Giao\" không được để trống.";
            this.rfvSoHopDongGiao.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoHopDongGiao.Icon")));
            this.rfvSoHopDongGiao.Tag = "rfvSoHopDongGiao";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(754, 139);
            this.uiGroupBox2.TabIndex = 258;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.pnlToKhaiMauDich);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 37);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(776, 504);
            this.uiGroupBox1.TabIndex = 5;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(241)))), ((int)(((byte)(228)))));
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.uiPanelManager1.DefaultPanelSettings.CloseButtonVisible = false;
            this.uiPanelManager1.Tag = null;
            this.uiPanelManager1.VisualStyleManager = this.vsmMain;
            this.uiPanel0.Id = new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2");
            this.uiPanel0.StaticGroup = true;
            this.uiPanel1.Id = new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8");
            this.uiPanel0.Panels.Add(this.uiPanel1);
            this.uiPanelManager1.Panels.Add(this.uiPanel0);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), Janus.Windows.UI.Dock.PanelGroupStyle.HorizontalTiles, Janus.Windows.UI.Dock.PanelDockStyle.Bottom, true, new System.Drawing.Size(970, 182), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8"), new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), 174, true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), Janus.Windows.UI.Dock.PanelGroupStyle.HorizontalTiles, true, new System.Drawing.Point(22, 29), new System.Drawing.Size(56, 56), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.AutoHide = true;
            this.uiPanel0.CaptionFormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.uiPanel0.CloseButtonVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel0.FloatingLocation = new System.Drawing.Point(22, 29);
            this.uiPanel0.FloatingSize = new System.Drawing.Size(56, 56);
            this.uiPanel0.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiPanel0.Location = new System.Drawing.Point(3, 263);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.Size = new System.Drawing.Size(970, 182);
            this.uiPanel0.TabIndex = 4;
            this.uiPanel0.Text = "Thông tin hàng hóa";
            // 
            // uiPanel1
            // 
            this.uiPanel1.CaptionVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(0, 26);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(970, 156);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Panel 1";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.dgList);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 1);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(968, 154);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(968, 154);
            this.dgList.TabIndex = 183;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "attached.ico");
            this.ImageList1.Images.SetKeyName(5, "internet.ico");
            this.ImageList1.Images.SetKeyName(6, "explorerBarItem25.Icon.ico");
            this.ImageList1.Images.SetKeyName(7, "page_edit.png");
            this.ImageList1.Images.SetKeyName(8, "page_red.png");
            this.ImageList1.Images.SetKeyName(9, "printer.png");
            // 
            // rfvNgayHDGiao
            // 
            this.rfvNgayHDGiao.ControlToValidate = this.ccNgayHopDongGiao;
            this.rfvNgayHDGiao.ErrorMessage = "\"Ngày hợp đồng giao\" không được để trống.";
            this.rfvNgayHDGiao.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayHDGiao.Icon")));
            this.rfvNgayHDGiao.Tag = "rfvNgayHDGiao";
            // 
            // rvTyGiaTT
            // 
            this.rvTyGiaTT.ControlToValidate = this.txtTyGiaTinhThue;
            this.rvTyGiaTT.ErrorMessage = "\"Tỷ giá VND\" không hợp lệ.";
            this.rvTyGiaTT.Icon = ((System.Drawing.Icon)(resources.GetObject("rvTyGiaTT.Icon")));
            this.rvTyGiaTT.MaximumValue = "50000";
            this.rvTyGiaTT.MinimumValue = "1";
            this.rvTyGiaTT.Tag = "rvTyGiaTT";
            this.rvTyGiaTT.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rvTyGiaUSD
            // 
            this.rvTyGiaUSD.ControlToValidate = this.txtTyGiaUSD;
            this.rvTyGiaUSD.ErrorMessage = "\"Tỷ giá USD\" không hợp lệ.";
            this.rvTyGiaUSD.Icon = ((System.Drawing.Icon)(resources.GetObject("rvTyGiaUSD.Icon")));
            this.rvTyGiaUSD.MaximumValue = "50000";
            this.rvTyGiaUSD.MinimumValue = "1";
            this.rvTyGiaUSD.Tag = "rvTyGiaUSD";
            this.rvTyGiaUSD.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbToolBar});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang,
            this.cmdSave,
            this.cmdSend,
            this.cmdPrint,
            this.XacNhan,
            this.NhanDuLieu,
            this.Huy,
            this.cmdThemMotHang,
            this.cmdThemNhieuHang,
            this.ToKhaiViet,
            this.InPhieuTN2,
            this.cmdReadExcel,
            this.cmdChungTuKem,
            this.cmdGiayPhep,
            this.cmdHopDongThuongMai,
            this.cmdHoaDonThuongMai,
            this.cmdDeNghiChuyenCuaKhau,
            this.cmdChungTuDangAnh,
            this.cmdTruyenDuLieuTuXa,
            this.cmdSuaToKhaiDaDuyet,
            this.cmdHuyToKhaiDaDuyet,
            this.cmdKetQuaXuLy,
            this.cmdInTKDTSuaDoiBoSung});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.ImageList = this.ImageList1;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmbToolBar
            // 
            this.cmbToolBar.CommandManager = this.cmMain;
            this.cmbToolBar.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang1,
            this.cmdSave1,
            this.Separator1,
            this.cmdChungTuKem1,
            this.cmdTruyenDuLieuTuXa1,
            this.Separator2,
            this.cmdPrint1,
            this.Separator4});
            this.cmbToolBar.FullRow = true;
            this.cmbToolBar.Key = "cmdToolBar";
            this.cmbToolBar.Location = new System.Drawing.Point(0, 0);
            this.cmbToolBar.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.cmbToolBar.MergeRowOrder = 1;
            this.cmbToolBar.Name = "cmbToolBar";
            this.cmbToolBar.RowIndex = 0;
            this.cmbToolBar.Size = new System.Drawing.Size(782, 34);
            this.cmbToolBar.Text = "cmdToolBar";
            this.cmbToolBar.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmbToolBar.Wrappable = Janus.Windows.UI.InheritableBoolean.True;
            // 
            // cmdThemHang1
            // 
            this.cmdThemHang1.Key = "cmdThemHang";
            this.cmdThemHang1.Name = "cmdThemHang1";
            this.cmdThemHang1.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            this.cmdThemHang1.Text = "&Thêm hàng";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            this.cmdSave1.Text = "&Lưu";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdChungTuKem1
            // 
            this.cmdChungTuKem1.Key = "cmdChungTuKem";
            this.cmdChungTuKem1.Name = "cmdChungTuKem1";
            // 
            // cmdTruyenDuLieuTuXa1
            // 
            this.cmdTruyenDuLieuTuXa1.Key = "cmdTruyenDuLieuTuXa";
            this.cmdTruyenDuLieuTuXa1.Name = "cmdTruyenDuLieuTuXa1";
            this.cmdTruyenDuLieuTuXa1.Text = "Thông quan điện tử";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            this.cmdPrint1.Text = "&In ";
            // 
            // Separator4
            // 
            this.Separator4.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator4.Key = "Separator";
            this.Separator4.Name = "Separator4";
            // 
            // cmdThemHang
            // 
            this.cmdThemHang.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemMotHang1,
            this.cmdThemNhieuHang1,
            this.cmdReadExcel1});
            this.cmdThemHang.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThemHang.Icon")));
            this.cmdThemHang.Key = "cmdThemHang";
            this.cmdThemHang.Name = "cmdThemHang";
            this.cmdThemHang.Text = "Thêm hàng";
            // 
            // cmdThemMotHang1
            // 
            this.cmdThemMotHang1.Key = "cmdThemMotHang";
            this.cmdThemMotHang1.Name = "cmdThemMotHang1";
            // 
            // cmdThemNhieuHang1
            // 
            this.cmdThemNhieuHang1.Key = "cmdThemNhieuHang";
            this.cmdThemNhieuHang1.Name = "cmdThemNhieuHang1";
            // 
            // cmdReadExcel1
            // 
            this.cmdReadExcel1.Key = "cmdReadExcel";
            this.cmdReadExcel1.Name = "cmdReadExcel1";
            // 
            // cmdSave
            // 
            this.cmdSave.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSave.Icon")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.MergeOrder = 3;
            this.cmdSave.MergeType = Janus.Windows.UI.CommandBars.CommandMergeType.MergeItems;
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSave.Text = "Lưu";
            // 
            // cmdSend
            // 
            this.cmdSend.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSend.Icon")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Shortcut = System.Windows.Forms.Shortcut.CtrlG;
            this.cmdSend.Text = "Khai báo";
            this.cmdSend.ToolTipText = "Khai báo (Ctrl + G)";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ToKhai1,
            this.InPhieuTN1,
            this.cmdInTKDTSuaDoiBoSung1});
            this.cmdPrint.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdPrint.Icon")));
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.cmdPrint.Text = "In ";
            // 
            // ToKhai1
            // 
            this.ToKhai1.ImageIndex = 9;
            this.ToKhai1.Key = "ToKhaiViet";
            this.ToKhai1.Name = "ToKhai1";
            // 
            // InPhieuTN1
            // 
            this.InPhieuTN1.ImageIndex = 9;
            this.InPhieuTN1.Key = "InPhieuTN";
            this.InPhieuTN1.Name = "InPhieuTN1";
            // 
            // cmdInTKDTSuaDoiBoSung1
            // 
            this.cmdInTKDTSuaDoiBoSung1.ImageIndex = 9;
            this.cmdInTKDTSuaDoiBoSung1.Key = "cmdInTKDTSuaDoiBoSung";
            this.cmdInTKDTSuaDoiBoSung1.Name = "cmdInTKDTSuaDoiBoSung1";
            // 
            // XacNhan
            // 
            this.XacNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan.Icon")));
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận";
            // 
            // NhanDuLieu
            // 
            this.NhanDuLieu.Icon = ((System.Drawing.Icon)(resources.GetObject("NhanDuLieu.Icon")));
            this.NhanDuLieu.Key = "NhanDuLieu";
            this.NhanDuLieu.Name = "NhanDuLieu";
            this.NhanDuLieu.Text = "Nhận dữ liệu";
            // 
            // Huy
            // 
            this.Huy.Icon = ((System.Drawing.Icon)(resources.GetObject("Huy.Icon")));
            this.Huy.Key = "Huy";
            this.Huy.Name = "Huy";
            this.Huy.Text = "Hủy khai báo";
            // 
            // cmdThemMotHang
            // 
            this.cmdThemMotHang.Key = "cmdThemMotHang";
            this.cmdThemMotHang.Name = "cmdThemMotHang";
            this.cmdThemMotHang.Text = "Thêm Hàng";
            // 
            // cmdThemNhieuHang
            // 
            this.cmdThemNhieuHang.Key = "cmdThemNhieuHang";
            this.cmdThemNhieuHang.Name = "cmdThemNhieuHang";
            this.cmdThemNhieuHang.Text = "Thêm nhiều hàng";
            // 
            // ToKhaiViet
            // 
            this.ToKhaiViet.Key = "ToKhaiViet";
            this.ToKhaiViet.Name = "ToKhaiViet";
            this.ToKhaiViet.Text = "Tờ khai";
            // 
            // InPhieuTN2
            // 
            this.InPhieuTN2.Key = "InPhieuTN";
            this.InPhieuTN2.Name = "InPhieuTN2";
            this.InPhieuTN2.Text = "Phiếu tiếp nhận";
            // 
            // cmdReadExcel
            // 
            this.cmdReadExcel.Key = "cmdReadExcel";
            this.cmdReadExcel.Name = "cmdReadExcel";
            this.cmdReadExcel.Text = "Thêm hàng từ Excel";
            // 
            // cmdChungTuKem
            // 
            this.cmdChungTuKem.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdGiayPhep1,
            this.cmdHopDongThuongMai1,
            this.cmdHoaDonThuongMai1,
            this.cmdDeNghiChuyenCuaKhau1,
            this.cmdChungTuDangAnh1});
            this.cmdChungTuKem.ImageIndex = 4;
            this.cmdChungTuKem.Key = "cmdChungTuKem";
            this.cmdChungTuKem.Name = "cmdChungTuKem";
            this.cmdChungTuKem.Text = "Chứng từ đính kèm";
            // 
            // cmdGiayPhep1
            // 
            this.cmdGiayPhep1.Key = "cmdGiayPhep";
            this.cmdGiayPhep1.Name = "cmdGiayPhep1";
            // 
            // cmdHopDongThuongMai1
            // 
            this.cmdHopDongThuongMai1.Key = "cmdHopDongThuongMai";
            this.cmdHopDongThuongMai1.Name = "cmdHopDongThuongMai1";
            // 
            // cmdHoaDonThuongMai1
            // 
            this.cmdHoaDonThuongMai1.Key = "cmdHoaDonThuongMai";
            this.cmdHoaDonThuongMai1.Name = "cmdHoaDonThuongMai1";
            // 
            // cmdDeNghiChuyenCuaKhau1
            // 
            this.cmdDeNghiChuyenCuaKhau1.Key = "cmdDeNghiChuyenCuaKhau";
            this.cmdDeNghiChuyenCuaKhau1.Name = "cmdDeNghiChuyenCuaKhau1";
            // 
            // cmdChungTuDangAnh1
            // 
            this.cmdChungTuDangAnh1.Key = "cmdChungTuDangAnh";
            this.cmdChungTuDangAnh1.Name = "cmdChungTuDangAnh1";
            // 
            // cmdGiayPhep
            // 
            this.cmdGiayPhep.ImageIndex = 0;
            this.cmdGiayPhep.Key = "cmdGiayPhep";
            this.cmdGiayPhep.Name = "cmdGiayPhep";
            this.cmdGiayPhep.Text = "Giấy phép";
            // 
            // cmdHopDongThuongMai
            // 
            this.cmdHopDongThuongMai.ImageIndex = 0;
            this.cmdHopDongThuongMai.Key = "cmdHopDongThuongMai";
            this.cmdHopDongThuongMai.Name = "cmdHopDongThuongMai";
            this.cmdHopDongThuongMai.Text = "Hợp đồng thương mại";
            // 
            // cmdHoaDonThuongMai
            // 
            this.cmdHoaDonThuongMai.ImageIndex = 0;
            this.cmdHoaDonThuongMai.Key = "cmdHoaDonThuongMai";
            this.cmdHoaDonThuongMai.Name = "cmdHoaDonThuongMai";
            this.cmdHoaDonThuongMai.Text = "Hóa đơn thương mại";
            // 
            // cmdDeNghiChuyenCuaKhau
            // 
            this.cmdDeNghiChuyenCuaKhau.ImageIndex = 0;
            this.cmdDeNghiChuyenCuaKhau.Key = "cmdDeNghiChuyenCuaKhau";
            this.cmdDeNghiChuyenCuaKhau.Name = "cmdDeNghiChuyenCuaKhau";
            this.cmdDeNghiChuyenCuaKhau.Text = "Đề nghị chuyển cửa khẩu";
            // 
            // cmdChungTuDangAnh
            // 
            this.cmdChungTuDangAnh.ImageIndex = 0;
            this.cmdChungTuDangAnh.Key = "cmdChungTuDangAnh";
            this.cmdChungTuDangAnh.Name = "cmdChungTuDangAnh";
            this.cmdChungTuDangAnh.Text = "Chứng từ dạng ảnh";
            // 
            // cmdTruyenDuLieuTuXa
            // 
            this.cmdTruyenDuLieuTuXa.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend2,
            this.NhanDuLieu2,
            this.Huy2,
            this.Separator5,
            this.cmdSuaToKhaiDaDuyet1,
            this.cmdHuyToKhaiDaDuyet1,
            this.Separator6,
            this.cmdKetQuaXuLy1});
            this.cmdTruyenDuLieuTuXa.ImageIndex = 5;
            this.cmdTruyenDuLieuTuXa.Key = "cmdTruyenDuLieuTuXa";
            this.cmdTruyenDuLieuTuXa.Name = "cmdTruyenDuLieuTuXa";
            this.cmdTruyenDuLieuTuXa.Text = "Truyền dữ liệu từ xa";
            // 
            // cmdSend2
            // 
            this.cmdSend2.Key = "cmdSend";
            this.cmdSend2.Name = "cmdSend2";
            // 
            // NhanDuLieu2
            // 
            this.NhanDuLieu2.Key = "NhanDuLieu";
            this.NhanDuLieu2.Name = "NhanDuLieu2";
            // 
            // Huy2
            // 
            this.Huy2.Key = "Huy";
            this.Huy2.Name = "Huy2";
            // 
            // Separator5
            // 
            this.Separator5.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator5.Key = "Separator";
            this.Separator5.Name = "Separator5";
            // 
            // cmdSuaToKhaiDaDuyet1
            // 
            this.cmdSuaToKhaiDaDuyet1.Key = "cmdSuaToKhaiDaDuyet";
            this.cmdSuaToKhaiDaDuyet1.Name = "cmdSuaToKhaiDaDuyet1";
            // 
            // cmdHuyToKhaiDaDuyet1
            // 
            this.cmdHuyToKhaiDaDuyet1.Key = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet1.Name = "cmdHuyToKhaiDaDuyet1";
            // 
            // Separator6
            // 
            this.Separator6.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator6.Key = "Separator";
            this.Separator6.Name = "Separator6";
            // 
            // cmdKetQuaXuLy1
            // 
            this.cmdKetQuaXuLy1.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy1.Name = "cmdKetQuaXuLy1";
            // 
            // cmdSuaToKhaiDaDuyet
            // 
            this.cmdSuaToKhaiDaDuyet.ImageIndex = 7;
            this.cmdSuaToKhaiDaDuyet.Key = "cmdSuaToKhaiDaDuyet";
            this.cmdSuaToKhaiDaDuyet.Name = "cmdSuaToKhaiDaDuyet";
            this.cmdSuaToKhaiDaDuyet.Text = "Sửa tờ khai đã duyệt";
            // 
            // cmdHuyToKhaiDaDuyet
            // 
            this.cmdHuyToKhaiDaDuyet.ImageIndex = 8;
            this.cmdHuyToKhaiDaDuyet.Key = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet.Name = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet.Text = "Hủy tờ khai đã duyệt";
            // 
            // cmdKetQuaXuLy
            // 
            this.cmdKetQuaXuLy.ImageIndex = 6;
            this.cmdKetQuaXuLy.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Name = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Text = "Kết quả xử lý";
            // 
            // cmdInTKDTSuaDoiBoSung
            // 
            this.cmdInTKDTSuaDoiBoSung.Key = "cmdInTKDTSuaDoiBoSung";
            this.cmdInTKDTSuaDoiBoSung.Name = "cmdInTKDTSuaDoiBoSung";
            this.cmdInTKDTSuaDoiBoSung.Text = "In Tờ khai sửa đổi";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbToolBar});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.cmbToolBar);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(782, 34);
            // 
            // ilLarge
            // 
            this.ilLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilLarge.ImageStream")));
            this.ilLarge.TransparentColor = System.Drawing.Color.Transparent;
            this.ilLarge.Images.SetKeyName(0, "");
            this.ilLarge.Images.SetKeyName(1, "");
            this.ilLarge.Images.SetKeyName(2, "");
            this.ilLarge.Images.SetKeyName(3, "");
            this.ilLarge.Images.SetKeyName(4, "Add.gif");
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "Add.gif");
            // 
            // rfvNgayHHHopDonggiao
            // 
            this.rfvNgayHHHopDonggiao.ControlToValidate = this.ccNgayHHHopDongGiao;
            this.rfvNgayHHHopDonggiao.ErrorMessage = "\"Ngày hết hạn hợp đồng giao\" không được để trống.";
            this.rfvNgayHHHopDonggiao.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayHHHopDonggiao.Icon")));
            this.rfvNgayHHHopDonggiao.Tag = "rfvNgayHHHopDonggiao";
            // 
            // rfvMaDVGiao
            // 
            this.rfvMaDVGiao.ControlToValidate = this.txtMaDVGiao;
            this.rfvMaDVGiao.ErrorMessage = "\"Mã đơn vị giao\" không được để trống.";
            this.rfvMaDVGiao.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaDVGiao.Icon")));
            // 
            // rfvTenDVGiao
            // 
            this.rfvTenDVGiao.ControlToValidate = this.txtTenDVGiao;
            this.rfvTenDVGiao.ErrorMessage = "\"Tên đơn vị giao\" không được để trống.";
            this.rfvTenDVGiao.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDVGiao.Icon")));
            this.rfvTenDVGiao.Tag = "rfvTenDVGiao";
            // 
            // rfvDiaDiemXephang
            // 
            this.rfvDiaDiemXephang.ControlToValidate = this.txtDiaDiemGiaohang;
            this.rfvDiaDiemXephang.ErrorMessage = "\"Địa điểm giao hàng\" không được để trống.";
            this.rfvDiaDiemXephang.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDiaDiemXephang.Icon")));
            this.rfvDiaDiemXephang.Tag = "rfvDiaDiemXephang";
            // 
            // rfvSoHopDongNhan
            // 
            this.rfvSoHopDongNhan.ControlToValidate = this.txtSoHopDongNhan;
            this.rfvSoHopDongNhan.ErrorMessage = "\"Số Hợp Đồng Nhận\" không được để trống.";
            this.rfvSoHopDongNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoHopDongNhan.Icon")));
            this.rfvSoHopDongNhan.Tag = "rfvSoHopDongNhan";
            // 
            // rvLPHQ
            // 
            this.rvLPHQ.ControlToValidate = this.txtLePhiHQ;
            this.rvLPHQ.ErrorMessage = "\"Lệ phí HQ\" không hợp lệ.";
            this.rvLPHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("rvLPHQ.Icon")));
            this.rvLPHQ.MaximumValue = "1000000000";
            this.rvLPHQ.MinimumValue = "0";
            this.rvLPHQ.Tag = "rvLPHQ";
            this.rvLPHQ.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // ToKhaiGCChuyenTiepNhapForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(782, 562);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ToKhaiGCChuyenTiepNhapForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tờ khai gia công chuyển tiếp";
            this.Load += new System.EventHandler(this.ToKhaiGCChuyenTiepNhapForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.pnlToKhaiMauDich.ResumeLayout(false);
            this.pnlToKhaiMauDich.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiKhachHang)).EndInit();
            this.uiKhachHang.ResumeLayout(false);
            this.uiKhachHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiGiao)).EndInit();
            this.grbNguoiGiao.ResumeLayout(false);
            this.grbNguoiGiao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDongGiao)).EndInit();
            this.grbHopDongGiao.ResumeLayout(false);
            this.grbHopDongGiao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiDV)).EndInit();
            this.uiDV.ResumeLayout(false);
            this.uiDV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiNguoiChiDinhDV)).EndInit();
            this.uiNguoiChiDinhDV.ResumeLayout(false);
            this.uiNguoiChiDinhDV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiNhan)).EndInit();
            this.grbNguoiNhan.ResumeLayout(false);
            this.grbNguoiNhan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDongNhan)).EndInit();
            this.grbHopDongNhan.ResumeLayout(false);
            this.grbHopDongNhan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            this.uiGroupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguyenTe)).EndInit();
            this.grbNguyenTe.ResumeLayout(false);
            this.grbNguyenTe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHangMauDich)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiHinhMauDich)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPTTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCuaKhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompanyNuoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHopDongGiao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayHDGiao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaUSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToolBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayHHHopDonggiao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaDVGiao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDVGiao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaDiemXephang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHopDongNhan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvLPHQ)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion		        

        private ImageList ImageList1;
        private UICommand cmdThemHang;
        internal ImageList ilLarge;
        private ImageList imageList2;
        private UICommand cmdPrint;
        private GridEX dgList;
        private Label label7;
        private CalendarCombo ccNgayBienLai;
        private Label label4;
        private NumericEditBox txtLePhiHQ;
        private Label label2;
        private Label label1;
        private NguyenTeControl nguyenTeControl1;
        private Label label8;
        private UIGroupBox uiGroupBox10;
        private EditBox txtCanbodangky;
        private Label label22;
        private Label label24;
        private Label label25;
        private CalendarCombo ccNgayDangKy;
        private Label lbltrangthai;
        private Label label29;
        private NumericEditBox txtsotiepnhan;
        private Label label31;
        private RequiredFieldValidator rfvNgayHHHopDonggiao;
        private RequiredFieldValidator rfvMaDVGiao;
        private RequiredFieldValidator rfvTenDVGiao;
        private RequiredFieldValidator rfvDiaDiemXephang;
        private EditBox txtChungTu;
        private RequiredFieldValidator rfvSoHopDongNhan;
        private EditBox txtSoBienLai;
        private NumericEditBox txtToKhaiSo;
        private UICommand cmdSend;
        private UICommand cmdSave1;
        private UICommand cmdThemHang1;
        private UICommand cmdPrint1;
        private UICommand XacNhan;
        private Label label16;
        private UIGroupBox uiKhachHang;
        private UIGroupBox uiNguoiChiDinhDV;
        private EditBox txtNguoiChiDinhDV;
        private UIGroupBox uiDV;
        private UIGroupBox uiGroupBox6;
        private EditBox txtDiaDiemGiaohang;
        private UIGroupBox grbNguoiNhan;
        private Label label3;
        private EditBox txtTenDonVinhan;
        private EditBox txtMaDonVinhan;
        private UIGroupBox grbHopDongNhan;
        private CalendarCombo ccNgayHHHopDongNhan;
        private EditBox txtSoHopDongNhan;
        private Label lblSoHDDV;
        private Label lblNgayHDDV;
        private Label lblNgayHHDV;
        private CalendarCombo ccNgayHopDongNhan;
        private UIGroupBox grbNguoiGiao;
        private Label label15;
        private EditBox txtTenDVGiao;
        private EditBox txtMaDVGiao;
        private UIGroupBox grbHopDongGiao;
        private CalendarCombo ccNgayHHHopDongGiao;
        private EditBox txtSoHongDonggiao;
        private Label lblHDKH;
        private Label lblNgayHDKH;
        private Label lblNgayHHKH;
        private CalendarCombo ccNgayHopDongGiao;
        private UIGroupBox uiGroupBox7;
        private Company.Interface.Controls.LoaiHinhMauDichVControl ctrLoaiHinhMauDich;
        private Label label17;
        private DonViHaiQuanControl donViHaiQuanControl2;
        private EditBox txtNguoiChiDinhKH;
        private Label lblNguoiChiDinhKH;
        private Label label6;
        private EditBox txtMaDonViUyThac;
        private Company.Interface.Controls.LoaiHinhChuyenTiepControl cbLoaiHinh;
        private UICommand Separator1;
        private UICommand NhanDuLieu;
        private UICommand Huy;
        private UICommand Separator2;
        private Label label14;
        private Label label5;
        private Label label10;
        private Label label11;
        private Label label18;
        private Label label13;
        private Label label12;
        private UICommand cmdThemMotHang1;
        private UICommand cmdThemNhieuHang1;
        private UICommand cmdThemMotHang;
        private UICommand cmdThemNhieuHang;
        private RangeValidator rvLPHQ;
        private Label label19;
        private Label label20;
        private UICommand ToKhai1;
        private UICommand InPhieuTN1;
        private UICommand ToKhai;
        private UICommand InPhieuTN2;
        private UICommand cmdReadExcel1;
        private UICommand cmdReadExcel;
        private UICommand cmdChungTuKem;
        private UICommand cmdGiayPhep;
        private UICommand cmdHopDongThuongMai;
        private UICommand cmdHoaDonThuongMai;
        private UICommand cmdDeNghiChuyenCuaKhau;
        private UICommand cmdChungTuDangAnh;
        private UICommand cmdTruyenDuLieuTuXa;
        private UICommand cmdSuaToKhaiDaDuyet;
        private UICommand cmdHuyToKhaiDaDuyet;
        private UICommand cmdKetQuaXuLy;
        private UICommand cmdChungTuKem1;
        private UICommand cmdGiayPhep1;
        private UICommand cmdHopDongThuongMai1;
        private UICommand cmdHoaDonThuongMai1;
        private UICommand cmdDeNghiChuyenCuaKhau1;
        private UICommand cmdChungTuDangAnh1;
        private UICommand cmdSend2;
        private UICommand NhanDuLieu2;
        private UICommand Huy2;
        private UICommand Separator5;
        private UICommand cmdSuaToKhaiDaDuyet1;
        private UICommand cmdHuyToKhaiDaDuyet1;
        private UICommand Separator6;
        private UICommand cmdKetQuaXuLy1;
        private UICommand cmdTruyenDuLieuTuXa1;
        private UICommand Separator4;
        private UICommand cmdChungTuDangAnhBS1;
        private Label label21;
        private UIComboBox cbPTTT;
        private Label label23;
        private UIComboBox cbDKGH;
        private Label label26;
        private Label label27;
        private CalendarCombo ccThoiGianGiaoHang;
        private Label lblPhanLuong;
        private UICommand cmdInTKDTSuaDoiBoSung;
        private UICommand cmdInTKDTSuaDoiBoSung1;
        private UICommand ToKhaiViet;
        private Label label28;
        private Label label33;
        private NumericEditBox numTrongLuongTinh;
        private NumericEditBox numTrongLuong;
        private NumericEditBox numSoKien;
        private Label label32;
    }
}
