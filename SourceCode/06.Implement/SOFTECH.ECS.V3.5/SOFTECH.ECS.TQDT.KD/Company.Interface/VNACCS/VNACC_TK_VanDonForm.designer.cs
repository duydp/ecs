﻿namespace Company.Interface
{
    partial class VNACC_TK_VanDonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_TK_VanDonForm));
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.chk2722 = new System.Windows.Forms.CheckBox();
            this.chk2061 = new System.Windows.Forms.CheckBox();
            this.chkMasterBill = new System.Windows.Forms.CheckBox();
            this.chk1593 = new System.Windows.Forms.CheckBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.clcNamVD = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcNgayVD = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label5 = new System.Windows.Forms.Label();
            this.lblNgayVD = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblHAWB = new System.Windows.Forms.Label();
            this.lblMAWB = new System.Windows.Forms.Label();
            this.txtSoDinhDanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoVanDonTC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoVanDonChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.grList = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 558), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 558);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 534);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 534);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(841, 558);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.label2);
            this.uiGroupBox5.Controls.Add(this.linkLabel1);
            this.uiGroupBox5.Controls.Add(this.label1);
            this.uiGroupBox5.Controls.Add(this.chk2722);
            this.uiGroupBox5.Controls.Add(this.chk2061);
            this.uiGroupBox5.Controls.Add(this.chkMasterBill);
            this.uiGroupBox5.Controls.Add(this.chk1593);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(841, 166);
            this.uiGroupBox5.TabIndex = 0;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(199, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(633, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Sau khi khai báo lấy số định danh hàng hóa thì Doanh nghiệp nhập số Định danh hàn" +
                "g hóa vào ô Số vận đơn chủ ";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(13, 121);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(180, 13);
            this.linkLabel1.TabIndex = 3;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Khai báo lấy số định danh hàng hóa ";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(11, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(639, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // chk2722
            // 
            this.chk2722.AutoSize = true;
            this.chk2722.Location = new System.Drawing.Point(12, 65);
            this.chk2722.Name = "chk2722";
            this.chk2722.Size = new System.Drawing.Size(593, 17);
            this.chk2722.TabIndex = 2;
            this.chk2722.Text = "Khai báo số định danh theo quyết định 2722/QĐ-BTC( Cảng biển do Hải quan Hồ Chí M" +
                "inh, Hải quan Vũng Tàu quản lý)";
            this.chk2722.UseVisualStyleBackColor = true;
            this.chk2722.Visible = false;
            this.chk2722.CheckedChanged += new System.EventHandler(this.chk2722_CheckedChanged);
            // 
            // chk2061
            // 
            this.chk2061.AutoSize = true;
            this.chk2061.Location = new System.Drawing.Point(12, 42);
            this.chk2061.Name = "chk2061";
            this.chk2061.Size = new System.Drawing.Size(460, 17);
            this.chk2061.TabIndex = 1;
            this.chk2061.Text = "Khai báo số định danh theo chuẩn quản lý giám sát Hải quan tự động tại Cảng hàng " +
                "không ";
            this.chk2061.UseVisualStyleBackColor = true;
            this.chk2061.CheckedChanged += new System.EventHandler(this.chk2061_CheckedChanged);
            // 
            // chkMasterBill
            // 
            this.chkMasterBill.AutoSize = true;
            this.chkMasterBill.Location = new System.Drawing.Point(14, 141);
            this.chkMasterBill.Name = "chkMasterBill";
            this.chkMasterBill.Size = new System.Drawing.Size(106, 17);
            this.chkMasterBill.TabIndex = 4;
            this.chkMasterBill.Text = "Chỉ có Master Bill";
            this.chkMasterBill.UseVisualStyleBackColor = true;
            this.chkMasterBill.CheckedChanged += new System.EventHandler(this.chkMasterBill_CheckedChanged);
            // 
            // chk1593
            // 
            this.chk1593.AutoSize = true;
            this.chk1593.Location = new System.Drawing.Point(12, 19);
            this.chk1593.Name = "chk1593";
            this.chk1593.Size = new System.Drawing.Size(421, 17);
            this.chk1593.TabIndex = 0;
            this.chk1593.Text = "Khai báo số định danh theo chuẩn quản lý giám sát Hải quan tự động tại Cảng biển";
            this.chk1593.UseVisualStyleBackColor = true;
            this.chk1593.CheckedChanged += new System.EventHandler(this.chk1593_CheckedChanged);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnDelete);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 518);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(841, 40);
            this.uiGroupBox1.TabIndex = 33;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(760, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDelete.Location = new System.Drawing.Point(679, 12);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.clcNamVD);
            this.uiGroupBox3.Controls.Add(this.clcNgayVD);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.lblNgayVD);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.lblHAWB);
            this.uiGroupBox3.Controls.Add(this.lblMAWB);
            this.uiGroupBox3.Controls.Add(this.txtSoDinhDanh);
            this.uiGroupBox3.Controls.Add(this.txtSoVanDonTC);
            this.uiGroupBox3.Controls.Add(this.txtSoVanDonChu);
            this.uiGroupBox3.Controls.Add(this.btnAdd);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 166);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(841, 174);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.Text = "Thông tin vận đơn";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // clcNamVD
            // 
            this.clcNamVD.CustomFormat = "yyyy";
            this.clcNamVD.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNamVD.DropDownCalendar.Name = "";
            this.clcNamVD.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNamVD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNamVD.Location = new System.Drawing.Point(722, 50);
            this.clcNamVD.Name = "clcNamVD";
            this.clcNamVD.Size = new System.Drawing.Size(71, 21);
            this.clcNamVD.TabIndex = 7;
            this.clcNamVD.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNamVD.TextChanged += new System.EventHandler(this.clcNamVD_TextChanged);
            // 
            // clcNgayVD
            // 
            this.clcNgayVD.CustomFormat = "dd/MM/yyyy";
            this.clcNgayVD.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayVD.DropDownCalendar.Name = "";
            this.clcNgayVD.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayVD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayVD.Location = new System.Drawing.Point(527, 49);
            this.clcNgayVD.Name = "clcNgayVD";
            this.clcNgayVD.Size = new System.Drawing.Size(158, 21);
            this.clcNgayVD.TabIndex = 6;
            this.clcNgayVD.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayVD.TextChanged += new System.EventHandler(this.clcNgayVD_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(722, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 14);
            this.label5.TabIndex = 0;
            this.label5.Text = "Năm MAWB";
            // 
            // lblNgayVD
            // 
            this.lblNgayVD.AutoSize = true;
            this.lblNgayVD.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayVD.Location = new System.Drawing.Point(524, 25);
            this.lblNgayVD.Name = "lblNgayVD";
            this.lblNgayVD.Size = new System.Drawing.Size(161, 14);
            this.lblNgayVD.TabIndex = 0;
            this.lblNgayVD.Text = "Ngày vận đơn / Ngày HAWB";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(61, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "Số định danh hàng hóa : ";
            // 
            // lblHAWB
            // 
            this.lblHAWB.AutoSize = true;
            this.lblHAWB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHAWB.Location = new System.Drawing.Point(67, 25);
            this.lblHAWB.Name = "lblHAWB";
            this.lblHAWB.Size = new System.Drawing.Size(135, 14);
            this.lblHAWB.TabIndex = 0;
            this.lblHAWB.Text = "Số vận đơn / Số HAWB";
            // 
            // lblMAWB
            // 
            this.lblMAWB.AutoSize = true;
            this.lblMAWB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMAWB.Location = new System.Drawing.Point(357, 25);
            this.lblMAWB.Name = "lblMAWB";
            this.lblMAWB.Size = new System.Drawing.Size(61, 14);
            this.lblMAWB.TabIndex = 0;
            this.lblMAWB.Text = "Số MAWB";
            // 
            // txtSoDinhDanh
            // 
            this.txtSoDinhDanh.BackColor = System.Drawing.Color.White;
            this.txtSoDinhDanh.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDinhDanh.Location = new System.Drawing.Point(20, 105);
            this.txtSoDinhDanh.Name = "txtSoDinhDanh";
            this.txtSoDinhDanh.Size = new System.Drawing.Size(229, 22);
            this.txtSoDinhDanh.TabIndex = 9;
            this.txtSoDinhDanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDinhDanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoVanDonTC
            // 
            this.txtSoVanDonTC.BackColor = System.Drawing.Color.White;
            this.txtSoVanDonTC.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDonTC.Location = new System.Drawing.Point(20, 49);
            this.txtSoVanDonTC.MaxLength = 12;
            this.txtSoVanDonTC.Name = "txtSoVanDonTC";
            this.txtSoVanDonTC.Size = new System.Drawing.Size(229, 22);
            this.txtSoVanDonTC.TabIndex = 8;
            this.txtSoVanDonTC.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanDonTC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoVanDonTC.TextChanged += new System.EventHandler(this.txtSoVanDonTC_TextChanged);
            // 
            // txtSoVanDonChu
            // 
            this.txtSoVanDonChu.BackColor = System.Drawing.Color.White;
            this.txtSoVanDonChu.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoVanDonChu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDonChu.Location = new System.Drawing.Point(273, 49);
            this.txtSoVanDonChu.MaxLength = 11;
            this.txtSoVanDonChu.Name = "txtSoVanDonChu";
            this.txtSoVanDonChu.Size = new System.Drawing.Size(229, 22);
            this.txtSoVanDonChu.TabIndex = 5;
            this.txtSoVanDonChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanDonChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoVanDonChu.TextChanged += new System.EventHandler(this.txtSoVanDonChu_TextChanged);
            this.txtSoVanDonChu.ButtonClick += new System.EventHandler(this.txtSoVanDonChu_ButtonClick);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAdd.Location = new System.Drawing.Point(20, 139);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(90, 23);
            this.btnAdd.TabIndex = 10;
            this.btnAdd.Text = "Ghi";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.grList);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 340);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(841, 178);
            this.uiGroupBox2.TabIndex = 35;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // grList
            // 
            this.grList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grList.ColumnAutoResize = true;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grList.GroupByBoxVisible = false;
            this.grList.Location = new System.Drawing.Point(3, 8);
            this.grList.Name = "grList";
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelection;
            this.grList.Size = new System.Drawing.Size(835, 167);
            this.grList.TabIndex = 0;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.grList_DeletingRecord);
            this.grList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grList_RowDoubleClick);
            // 
            // VNACC_TK_VanDonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1047, 564);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_TK_VanDonForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin vận đơn";
            this.Load += new System.EventHandler(this.VNACC_VanDonForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.CheckBox chk2061;
        private System.Windows.Forms.CheckBox chk1593;
        private System.Windows.Forms.CheckBox chk2722;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private System.Windows.Forms.CheckBox chkMasterBill;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblNgayVD;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblHAWB;
        private System.Windows.Forms.Label lblMAWB;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDonChu;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNamVD;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayVD;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDinhDanh;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDonTC;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX grList;
    }
}