﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using System.Text.RegularExpressions;
using System.Security.Cryptography.X509Certificates;
namespace Company.Interface
{
    public partial class FrmCauHinhThoiGian : BaseForm
    {
        public FrmCauHinhThoiGian()
        {
            InitializeComponent();
        }

        private void FrmCauHinhThoiGian_Load(object sender, EventArgs e)
        {
            try
            {

                tbarCounter.Value = Company.KDT.SHARE.Components.Globals.Delay;
                tbarConnected.Value = Company.KDT.SHARE.Components.Globals.TimeConnect;
                tbarCountSend.Value = Company.KDT.SHARE.Components.Globals.CountSend;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.Delay = tbarCounter.Value;
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TimeDelay",
                    tbarCounter.Value);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TimeConnect",
                                    tbarConnected.Value);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("CountSend",
                    tbarCountSend.Value);
                ShowMessage("Lưu cấu hình thành công.", false);
                this.Close();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

        }
    }
}
