﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class Content
    {
        private string _base64;
        [XmlText()]
        public string Text { get; set; }
        /// <summary>
        /// Bắt buộc phải là bin.base64       
        /// </summary>
        [XmlAttribute("dt", Namespace = "urn:schemas-microsoft-com:datatypes")]
        public string Base64
        {
            get { return _base64; }
            set
            {
                if (value != "bin.base64")
                {
                    throw new Exception("Giá trị bắt buộc phải là bin.base64");
                }
                _base64 = value;
            }
        }
        /// <summary>
        /// Phụ kiện gia công
        /// </summary>
        [XmlElement("Declaration")]
        public DeclarationPhuKien Declaration { get; set; }

        #region Nội dung lỗi từ hải quan trả về
        [XmlElement("XMLERRORINFORMATION")]
        public List<ErrorInformation> ErrorInformations { get; set; }
        #endregion
    }
}
