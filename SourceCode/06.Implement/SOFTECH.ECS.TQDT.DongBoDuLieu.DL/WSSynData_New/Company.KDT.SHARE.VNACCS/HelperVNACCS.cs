﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
using Company.KDT.SHARE.Components;
using System.Net.Mail;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using System.Collections;
using System.Xml;
using GetPKCS7;



namespace Company.KDT.SHARE.VNACCS
{
    public class HelperVNACCS
    {

        public static DataTable GeneratorTableValidate()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Name", typeof(string)); // Tên của chỉ tiêu
            dt.Columns.Add("Length", typeof(int)); // Độ dài chỉ tiêu
            dt.Columns.Add("Index", typeof(int)); // Thứ tự chỉ tiêu
            dt.Columns.Add("Required", typeof(bool)); // Có bắt buộc hay không
            dt.Columns.Add("Type", typeof(string)); // Nghiệp vụ
            dt.Columns.Add("Repetition", typeof(int));// Số lần lặp
            dt.Columns.Add("GroupRepetition", typeof(string)); // Nhóm lặp
            return dt;
        }
        public static void AddRowValidate(DataTable dt, string Name, int Length, int index, bool required, string type, int Repetition, string GroupRepetition)
        {
            if (dt == null || dt.Columns.Count == 0)
            {
                dt = GeneratorTableValidate();
            }
            DataRow[] listdr = dt.Select("Name = '" + Name + "'");
            if (listdr.Length > 0)
            {
                dt.Rows.Remove(listdr[0]);
            }
            DataRow dr = dt.NewRow();
            dr["Name"] = Name;
            dr["Length"] = Length;
            dr["Index"] = index;
            dr["Required"] = required;
            dr["Type"] = type;
            dr["Repetition"] = Repetition;
            dr["GroupRepetition"] = GroupRepetition;
            dt.Rows.Add(dr);
        }

        public static DataTable LoadDataValidate(string FileName)
        {
            try
            {
                string _fileName = Application.StartupPath + "\\Validate\\" + FileName;
                FileInfo xmlValidate = new FileInfo(_fileName);
                if (!xmlValidate.Exists)
                    return null;
                DataSet ds = new DataSet();
                ds.ReadXml(_fileName);
                if (ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                    return null;
                return ds.Tables[0];
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        public static void SaveDataValidate(string FileName, DataTable dt)
        {
            try
            {
                string _fileName = Application.StartupPath + "\\Validate\\" + FileName;
                if (dt != null)
                    dt.WriteXml(_fileName, XmlWriteMode.WriteSchema);
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        public static string FormatNumber(object value, int digit, int decimalFormat, bool isRequired)
        {
            try
            {
                string ret = string.Empty;
                System.Globalization.CultureInfo cltInfo = new System.Globalization.CultureInfo("en-us");
                decimal round;
                string sFormat;
                if (value == null || string.IsNullOrEmpty(value.ToString().Trim()))
                {
                    value = "0";
                    sFormat = string.Empty;
                }
                else
                {
                    //round = Math.Round(System.Convert.ToDecimal(value), decimalFormat);
                    round = System.Convert.ToDecimal(value);
                    if (round == 0 && !isRequired) sFormat = string.Empty;
                    else
                        sFormat = round.ToString("0.#######", cltInfo.NumberFormat);
                }
                if (sFormat.Length < digit)
                {

                    sFormat = sFormat.PadLeft(digit);
                }

                return sFormat;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return value.ToString();
            }
        }
        public static string FormatString(object value, int digit)
        {
            if (value == null) value = "";
            string temp = string.Empty;
            //byte[] bytevalue = UTF8Encoding.UTF8.GetBytes(value.ToString());
            //if (bytevalue.Length > digit)
            //{
            //    MemoryStream memory = new MemoryStream();
            //    memory.Write(bytevalue, 0, digit);
            //    return UTF8Encoding.UTF8.GetString(memory.ToArray());
            //}
            //else
            //{ 
            //    MemoryStream memory = new MemoryStream();
            //    memory.Write(bytevalue, 0, bytevalue.Length);
            //    for (int i = 0; i < digit - bytevalue.Length; i++)
            //    {
            //        memory.Write(UTF8Encoding.UTF8.GetBytes(" "), 0, 1);
            //    } 
            //    return UTF8Encoding.UTF8.GetString(memory.ToArray());
            //}
            if (value == null || string.IsNullOrEmpty(value.ToString()))
                return temp.PadRight(digit);
            temp = value.ToString().TrimEnd().Replace("\r", string.Empty).Replace("\n", string.Empty);

            int lengStr = UTF8Encoding.UTF8.GetBytes(temp).Length;
            if (lengStr > digit)
            {
                string ex = temp + ": Có độ dài quá lớn";
                Logger.LocalLogger.Instance().WriteMessage(ex, new Exception());
                throw new Exception(ex);
            }
            else if (lengStr < digit)
            {
                int numberDigit = lengStr - temp.Length;
                return temp.PadRight(digit - numberDigit);
            }
            else
                return temp;
        }
        public static string FormatDateTime(object date)
        {
            if (date == null)
                date = "";
            string temp = date.ToString();
            if (string.IsNullOrEmpty(date.ToString()))
                temp = temp.PadRight(8);
            else
            {
                DateTime tempdate = Convert.ToDateTime(date);
                if (tempdate.Year < 2000)
                    temp = string.Empty.PadRight(8);
                else
                    temp = tempdate.ToString("ddMMyyyy");
            }
            return temp;
        }
        public static DateTime AddHoursToDateTime(DateTime date, string house)
        {
            if (string.IsNullOrEmpty(house) || string.IsNullOrEmpty(house.Trim()))
                return date;
            else
            {
                if (house.Length == 6)
                {
                    int HH = System.Convert.ToInt16(house.Substring(0, 2));
                    int MM = System.Convert.ToInt16(house.Substring(2, 2));
                    int SS = System.Convert.ToInt16(house.Substring(4, 2));
                    return new DateTime(date.Year, date.Month, date.Day, HH, MM, SS);
                }
                else
                    return date;
            }
        }
#region Code OLD
        //public static string GetMIMEMessages(string content1, ref string boundary, bool signedNotpass, X509Certificate2 X509cert)
        //{
        //    //X509Certificate2 X509cert = Cryptography.GetStoreX509Certificate2(Globals.GetX509CertificatedName);
        //    //MailMessage mail = new MailMessage();
        //    //mail.Headers.Add("Host", "xxx.xxx.xxx.xxx:xxxx");
        //    //mail.Headers.Add("Cache-Control", "no-store,no-cache");
        //    //mail.Headers.Add("Pragma", "no-cache");
        //    //mail.Headers.Add("Content-Length", "46706");
        //    //mail.Headers.Add("Expect", "100-continue");
        //    //mail.Headers.Add("Connection", "Close");
        //    //mail.Body = strbody;
        //    //byte[] data = Encoding.UTF8.GetBytes(content1);
        //    //ContentInfo content = new ContentInfo(data);
        //    //SignedCms signedCms = new SignedCms(content, true);
        //    //CspParameters csp = new Company.KDT.SHARE.Components.RSACryptographyHelper(X509cert).GetCspParameters(Globals.PasswordSign);


        //    //Compute hash using SHA1

        //    //SHA1Managed sha1 = new SHA1Managed();
        //    //byte[] dataHash = sha1.ComputeHash(data);

        //    //ContentInfo ci = new ContentInfo(dataHash);
        //    //SignedCms cms = new SignedCms(ci);
        //    //CmsSigner signer = new CmsSigner(csp);
        //    //signer.IncludeOption = X509IncludeOption.EndCertOnly;

        //    //X509Chain chain = new X509Chain();
        //    //chain.ChainPolicy.RevocationMode = X509RevocationMode.NoCheck;
        //    //chain.Build(X509cert);

        //    //if (chain != null)
        //    //{
        //    //    signer.IncludeOption = X509IncludeOption.None;
        //    //    X509ChainElementEnumerator enumerator = chain.ChainElements.GetEnumerator();
        //    //    while (enumerator.MoveNext())
        //    //    {
        //    //        X509ChainElement current = enumerator.Current;
        //    //        signer.Certificates.Add(current.Certificate);
        //    //    }
        //    //}

        //    //signer.DigestAlgorithm = new Oid("SHA1");
        //    //cms.ComputeSignature(signer, false);
        //    //byte[] signedData = cms.Encode();




        //    #region ký số bắt buộc dùng mật khẩu
        //    //CmsSigner signer = new CmsSigner(csp);
        //    //signedCms.Certificates.Add(X509cert);
        //    //signer.SignerIdentifierType = SubjectIdentifierType.IssuerAndSerialNumber;
        //    //signer.IncludeOption = X509IncludeOption.WholeChain;
        //    //signer.DigestAlgorithm = new Oid(System.Security.Cryptography.CryptoConfig.MapNameToOID(new Company.KDT.SHARE.Components.RSACryptographyHelper(X509cert).GetHashAlgorithmName()));
        //    //signedCms.ComputeSignature(signer, false);
        //    //byte[] signedbytes = signedCms.Encode();

        //    //MIME mime = new MIME(signedbytes, "xxxx.xxxx.xxxx.xxxx:xxxx", "45678");
        //    //mime.ContentEdi = new StringBuilder().Append(content1);
        //    #endregion

        //    #region Ký số V3
        //    // Company.KDT.SHARE.Components.RSACryptographyHelper digitalsign = new Company.KDT.SHARE.Components.RSACryptographyHelper(X509cert);

        //    //RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider(csp);
        //    //string strAlgorithmOID = System.Security.Cryptography.CryptoConfig.MapNameToOID(digitalsign.GetHashAlgorithmName());
        //    //byte[] signedData = cryptoServiceProvider.SignData(data, strAlgorithmOID);
        //    ////MIME mime = new MIME(bArr, "xxxx.xxxx.xxxx.xxxx:xxxx", "45678");
        //    MIME mime = new MIME(signedNotpass ? signEdiNotPass(content1, X509cert) : signEdi(content1, X509cert), "xxxx.xxxx.xxxx.xxxx:xxxx", "45678");
        //    boundary = mime.boundary;
        //    mime.ContentEdi = new StringBuilder().Append(content1);

        //    #endregion
        //    boundary = mime.boundary;
        //    return mime.GetMessages().ToString();
        //}
#endregion
        
        public static MIME GetMIME(string content1, X509Certificate2 X509cert)
        {
            try
            {
                MIME mime = new MIME(signEdi(content1, X509cert), "xxxx.xxxx.xxxx.xxxx:xxxx", "45678");
                mime.ContentEdi = new StringBuilder().Append(content1);
                return mime;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
        public static MIME GetMIME(string content, string strSign)
        {
            try
            {
                MIME mime = new MIME(strSign, "xxxx.xxxx.xxxx.xxxx:xxxx", "45678");
                mime.ContentEdi = new StringBuilder().Append(content);
                return mime;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
        public static string GetMIMEMessagesByEmail(string content1)
        {
            X509Certificate2 X509cert = Cryptography.GetStoreX509Certificate2(Globals.GetX509CertificatedName);
            MailMessage mail = new MailMessage();
            mail.Headers.Add("Host", "xxx.xxx.xxx.xxx:xxxx");
            mail.Headers.Add("Cache-Control", "no-store,no-cache");
            mail.Headers.Add("Pragma", "no-cache");
            mail.Headers.Add("Content-Length", "46706");
            mail.Headers.Add("Expect", "100-continue");
            mail.Headers.Add("Connection", "Close");
            mail.Body = content1;
            byte[] data = Encoding.UTF8.GetBytes(content1);
            ContentInfo content = new ContentInfo(data);
            SignedCms signedCms = new SignedCms(content, true);
            CspParameters csp = new Company.KDT.SHARE.Components.RSACryptographyHelper(X509cert).GetCspParameters(Globals.PasswordSign);

            #region //Compute hash using SHA1


            //SHA1Managed sha1 = new SHA1Managed();
            //byte[] dataHash = sha1.ComputeHash(data);

            //ContentInfo ci = new ContentInfo(dataHash);
            //SignedCms cms = new SignedCms(ci);
            //CmsSigner signer = new CmsSigner(csp);
            //signer.IncludeOption = X509IncludeOption.EndCertOnly;

            //X509Chain chain = new X509Chain();
            //chain.ChainPolicy.RevocationMode = X509RevocationMode.NoCheck;
            //chain.Build(X509cert);

            //if (chain != null)
            //{
            //    signer.IncludeOption = X509IncludeOption.None;
            //    X509ChainElementEnumerator enumerator = chain.ChainElements.GetEnumerator();
            //    while (enumerator.MoveNext())
            //    {
            //        X509ChainElement current = enumerator.Current;
            //        signer.Certificates.Add(current.Certificate);
            //    }
            //}

            //signer.DigestAlgorithm = new Oid("SHA1");
            //cms.ComputeSignature(signer,false);
            //byte[] signedData = cms.Encode();

            #endregion


            #region ký số bắt buộc dùng mật khẩu
            //CmsSigner signer = new CmsSigner(csp);
            //signedCms.Certificates.Add(X509cert);
            ////signer.SignerIdentifierType = SubjectIdentifierType.IssuerAndSerialNumber;
            //signer.IncludeOption = X509IncludeOption.WholeChain;
            //signer.DigestAlgorithm = new Oid(System.Security.Cryptography.CryptoConfig.MapNameToOID(new Company.KDT.SHARE.Components.RSACryptographyHelper(X509cert).GetHashAlgorithmName()));
            //signedCms.ComputeSignature(signer, false);
            //byte[] signedbytes = signedCms.Encode();
            //MemoryStream ms2 = new MemoryStream(signedbytes);
            //AlternateView av2 = new AlternateView(ms2, "application/pkcs7-mime; smime-type=signed-data;name=smime.p7m");
            //mail.AlternateViews.Add(av2);
            #endregion

            #region Ký số V3
            Company.KDT.SHARE.Components.RSACryptographyHelper digitalsign = new Company.KDT.SHARE.Components.RSACryptographyHelper(X509cert);

            RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider(csp);
            string strAlgorithmOID = System.Security.Cryptography.CryptoConfig.MapNameToOID(digitalsign.GetHashAlgorithmName());
            byte[] bArr = cryptoServiceProvider.SignData(data, strAlgorithmOID);
            MemoryStream ms = new MemoryStream(bArr);
            AlternateView av = new AlternateView(ms, "application/pkcs7-mime; smime-type=signed-data;name=smime.p7m");
            mail.AlternateViews.Add(av);
            #endregion
            mail.To.Add("ToToTo@ToTo.ToTo");
            mail.From = new System.Net.Mail.MailAddress("from@from.from");

            MemoryStream memory = ConvertMailMessageToMemoryStream(mail);

            return Encoding.ASCII.GetString(memory.ToArray());
        }

        private static MemoryStream ConvertMailMessageToMemoryStream(MailMessage message)
        {
            BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            Assembly assembly = typeof(SmtpClient).Assembly;
            MemoryStream stream = new MemoryStream();
            Type mailWriterType = assembly.GetType("System.Net.Mail.MailWriter");
            ConstructorInfo mailWriterContructor = mailWriterType.GetConstructor(flags, null, new[] { typeof(Stream) }, null);
            object mailWriter = mailWriterContructor.Invoke(new object[] { stream });
            MethodInfo sendMethod = typeof(MailMessage).GetMethod("Send", flags);
            sendMethod.Invoke(message, flags, null, new[] { mailWriter, true }, null);
            MethodInfo closeMethod = mailWriter.GetType().GetMethod("Close", flags);
            closeMethod.Invoke(mailWriter, flags, null, new object[] { }, null);
            return stream;
        }
        public static StringBuilder BuildEdiMessages<T>(object obj, string InputMsgID)
        {
            return BuildEdiMessages<T>(obj, string.Empty, InputMsgID);
        }
        public static StringBuilder BuildEdiMessages<T>(object obj, string MaNghiepVu, string InputMSGid)
        {
            StringBuilder strBody = GetStrBody<T>(obj, MaNghiepVu);
            if (strBody != null)
            {
                StringBuilder strHeader = new StringBuilder();
                VNACCHeader header = new VNACCHeader(string.IsNullOrEmpty(MaNghiepVu) ? GetMaNghiepVu<T>() : MaNghiepVu, strBody.Length, InputMSGid);
                strHeader = header.BuildEdiMessages<VNACCHeader>(strHeader, false, GlobalVNACC.PathConfig, GetMaNghiepVu<VNACCHeader>());
                if (strHeader != null && strHeader.Length > 0)
                {
                    strHeader.AppendLine();
                    strHeader.Append(strBody);
                    return strHeader;
                }
            }
            return new StringBuilder();
        }

        public static StringBuilder BuildEdiMessages(MessagesSend obj)
        {

            if (obj.Body != null && !string.IsNullOrEmpty(obj.Body.ToString()))
            {
                StringBuilder strHeader = new StringBuilder();
                strHeader = obj.Header.BuildEdiMessages<VNACCHeader>(strHeader, false, GlobalVNACC.PathConfig, GetMaNghiepVu<VNACCHeader>());
                if (strHeader != null && strHeader.Length > 0)
                {
                    strHeader.AppendLine();
                    strHeader.Append(obj.Body);
                    return strHeader;
                }
            }
            return new StringBuilder();
        }

        public static string GetMaNghiepVu<T>()
        {
            string MaNghiepVu = string.Empty;
            MaNghiepVu = typeof(T).ToString();
            string[] temp = MaNghiepVu.Split('.');
            MaNghiepVu = temp[temp.Length - 1];
            return MaNghiepVu;
        }
        public static StringBuilder GetStrBody<T>(object obj, string MaNghiepVuSua)
        {
            try
            {
                if (typeof(T) == typeof(IVA))
                {
                    IVA iva = obj as IVA;
                    return iva.BuilEdiMessagesIVA(new StringBuilder());
                }
                else if (typeof(T) == typeof(IDA))
                {
                    IDA ida = obj as IDA;
                    if (MaNghiepVuSua == EnumNghiepVu.IDA01)
                        return ida.BuilEdiMessagesIDA01(new StringBuilder());
                    return ida.BuilEdiMessagesIDA(new StringBuilder());
                }
                else if (typeof(T) == typeof(EDA))
                {
                    EDA eda = obj as EDA;
                    if (MaNghiepVuSua == EnumNghiepVu.EDA01)
                        return eda.BuilEdiMessagesEDA01(new StringBuilder());
                    return eda.BuilEdiMessagesEDA(new StringBuilder());
                }
                else if (typeof(T) == typeof(MIC))
                {
                    MIC mic = obj as MIC;
                    if (MaNghiepVuSua == EnumNghiepVu.MIE)
                        return mic.BuidMessagesEdiMIE(new StringBuilder());
                    return mic.BuildEdiMessages<MIC>(new StringBuilder(), true, GlobalVNACC.PathConfig, EnumNghiepVu.MIC);
                }
                else if (typeof(T) == typeof(MEC))
                {
                    MEC mec = obj as MEC;
                    if (MaNghiepVuSua == EnumNghiepVu.MEE)
                        return mec.BuidMessagesEdiMEE(new StringBuilder());
                    return mec.BuildEdiMessages<MEC>(new StringBuilder(), true, GlobalVNACC.PathConfig, EnumNghiepVu.MEC);
                }
                else if (typeof(T) == typeof(SAA))
                {
                    SAA saa = obj as SAA;
                    return saa.BuilEdiMessagesSAA(new StringBuilder());
                }
                else if (typeof(T) == typeof(SEA))
                {
                    SEA saa = obj as SEA;
                    return saa.BuilEdiMessagesSEA(new StringBuilder());
                }
                else if (typeof(T) == typeof(SFA))
                {
                    SFA saa = obj as SFA;
                    return saa.BuilEdiMessagesSFA(new StringBuilder());
                }
                else if (typeof(T) == typeof(SMA))
                {
                    SMA saa = obj as SMA;
                    return saa.BuilEdiMessagesSMA(new StringBuilder());
                }
                else if (typeof(T) == typeof(OLA))
                {
                    OLA ola = obj as OLA;
                    return ola.BuilEdiMessagesOLA(new StringBuilder());
                }
                else if (typeof(T) == typeof(AMA))
                {
                    AMA ama = obj as AMA;
                    return ama.BuilEdiMessagesSFA(new StringBuilder());
                }
                else if (typeof(T) == typeof(TEA))
                {
                    TEA tea = obj as TEA;
                    return tea.BuilEdiMessagesTEA(new StringBuilder());
                }
                else if (typeof(T) == typeof(TIA))
                {
                    TIA tia = obj as TIA;
                    return tia.BuilEdiMessagesTIA(new StringBuilder());
                }
                else if (typeof(T) == typeof(COT))
                {
                    COT cot = obj as COT;
                    return cot.BuilEdiMessagesOLA(new StringBuilder());
                }
                else
                {
                    BasicVNACC bVnacc = obj as BasicVNACC;
                    return bVnacc.BuildEdiMessages<T>(new StringBuilder(), true, GlobalVNACC.PathConfig, GetMaNghiepVu<T>());

                }

            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex;

            }
        }
        public static void SaveFileEdi(string path, StringBuilder messages, string name)
        {
            try
            {
                string pathedi = path + "\\" + name + DateTime.Now.ToString("yyyyMMddhhmmss") + ".txt";
                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);
                if (!File.Exists(pathedi))
                {
                    TextWriter tw = new StreamWriter(pathedi, true);
                    tw.Write(messages);
                    tw.Close();
                }
                else if (File.Exists(pathedi))
                {
                    TextWriter tw = new StreamWriter(pathedi, true);
                    tw.WriteLine("The next line!");
                    tw.Close();
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        public static void SaveFileEdi(string path, StringBuilder messages)
        {
            try
            {
                string pathedi = path;
                //if (!System.IO.Directory.Exists(path))
                //    System.IO.Directory.CreateDirectory(path);
                if (!File.Exists(pathedi))
                {
                    //File.Create(pathedi,500000,FileOptions.RandomAccess);

                    TextWriter tw = new StreamWriter(pathedi, true);
                    tw.Write(messages);
                    tw.Close();
                }
                else if (File.Exists(pathedi))
                {
                    TextWriter tw = new StreamWriter(pathedi, true);
                    tw.WriteLine("The next line!");
                    tw.Close();
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        public static string signEdi(string dataToSign, X509Certificate2 cert)
        {
            try
            {
                // var store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
                // store.Open(OpenFlags.ReadOnly);
                if (cert != null)
                {
                    Helpers.GetSignature("Test");
                    if (GetNameSigner(cert.IssuerName.Name) == "VNPT Certification Authority")
                    {

                        byte[] data = Encoding.UTF8.GetBytes(dataToSign);
                        return Convert.ToBase64String(GetPKCS7Signed.CreatePKCS7(data, cert, Globals.PasswordSign));
                    }
                    else
                    {

                        // var cert = store.Certificates[2];

                        // var csp = (RSACryptoServiceProvider)cert.PrivateKey;
                        //var sha1 = new SHA1Managed();
                        //var encoding = new UnicodeEncoding();
                        //var data = encoding.GetBytes(dataToSign);
                        var data = Encoding.UTF8.GetBytes(dataToSign);
                        //var hash = sha1.ComputeHash(data);
                        var digestOid = new Oid("1.2.840.113549.1.7.2"); //pkcs7 signed

                        var content = new ContentInfo(digestOid, data);

                        var signedCms = new SignedCms(SubjectIdentifierType.IssuerAndSerialNumber, content, true); //detached=true
                        var singer = new CmsSigner(cert) { DigestAlgorithm = new Oid("1.3.14.3.2.26") };
                        signedCms.ComputeSignature(singer,false);
                        var signEnv = signedCms.Encode();
                        return Convert.ToBase64String(signEnv);
                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw e;

            }
            return null;
        }
        public static byte[] signEdiNotPass(string dataToSign, X509Certificate2 cert)
        {

            if (cert != null)
            {
                CspParameters csp = new Company.KDT.SHARE.Components.RSACryptographyHelper(cert).GetCspParameters(Globals.PasswordSign);
                Company.KDT.SHARE.Components.RSACryptographyHelper digitalsign = new Company.KDT.SHARE.Components.RSACryptographyHelper(cert);
                var data = Encoding.UTF8.GetBytes(dataToSign);
                //var hash = sha1.ComputeHash(data);
                RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider(csp);

                string strAlgorithmOID = System.Security.Cryptography.CryptoConfig.MapNameToOID("SHA1");
                byte[] signedData = cryptoServiceProvider.SignData(data, strAlgorithmOID);
                return signedData;
                //var digestOid = new Oid("1.2.840.113549.1.7.2"); //pkcs7 signed

                //var content = new ContentInfo(digestOid, data);
                //try
                //{
                //    var signedCms = new SignedCms(SubjectIdentifierType.IssuerAndSerialNumber, content, true); //detached=true
                //    var singer = new CmsSigner(SubjectIdentifierType.IssuerAndSerialNumber, cert) { DigestAlgorithm = new Oid("1.3.14.3.2.26") };
                //    signedCms.ComputeSignature(singer, false);
                //    var signEnv = signedCms.Encode();
                //    return signEnv;
                //}
                //catch (Exception e)
                //{
                //    MessageBox.Show(e.Message);
                //}

            }
            return null;
        }
        /// <summary>
        /// Lấy chuỗi từ số byte cho  trước
        /// </summary>
        /// <param name="str">Chuỗi nhập vào</param>
        /// <param name="SoByte">Số lượng byte muốn cắt</param>
        /// <returns>Chuỗi mới bắt đầu từ offset 0 của chuỗi cũ đến offset thứ "SoByte" - 1</returns>
        public static string GetStringByBytes(string str, int SoByte)
        {
            string strResult = string.Empty;
            byte[] temp1 = UTF8Encoding.UTF8.GetBytes(str);
            byte[] temp2 = new byte[SoByte];
            if (temp1.Length > temp2.Length)
            {
                Buffer.BlockCopy(temp1, 0, temp2, 0, SoByte);
                strResult = UTF8Encoding.UTF8.GetString(temp2);
            }
            else
                strResult = UTF8Encoding.UTF8.GetString(temp1);

            return strResult;
        }
        /// <summary>
        /// Cắt chuỗi theo theo số bytes
        /// </summary>
        /// <param name="str">Chuỗi đầu vào</param>
        /// <param name="SoByte">Số lượng byte muốn cắt</param>
        /// <returns>Trả về là chuỗi mới đã được cắt "soByte" đầu tiên từ chuỗi cũ </returns>
        public static string SubStringByBytes(string str, int SoByte)
        {
            //SoByte = SoByte;
            string strResult = string.Empty;
            byte[] temp1 = UTF8Encoding.UTF8.GetBytes(str);
            byte[] temp2 = new byte[temp1.Length - SoByte];

            Buffer.BlockCopy(temp1, SoByte, temp2, 0, temp1.Length - SoByte);
            strResult = UTF8Encoding.UTF8.GetString(temp2);

            return strResult;
        }

        public static T UpdateObject<T>(object DoiTuongCanUpdate, object DoiTuongChuaDuLieuUpdate, bool UpdateIDandMasterID)
        {
            if (DoiTuongCanUpdate == null) return (T)DoiTuongChuaDuLieuUpdate;
            else if (DoiTuongChuaDuLieuUpdate == null) return (T)DoiTuongCanUpdate;
            else
            {
                PropertyInfo[] listProperties = typeof(T).GetProperties();
                foreach (PropertyInfo property in listProperties)
                {
                    bool isUpdate = true;
                    object value = property.GetValue(DoiTuongChuaDuLieuUpdate, null);
                    if (property.PropertyType.Namespace != "System.Collections.Generic")
                    {
                        if (value != null)
                        {
                            //if (property.GetType() == typeof(string))
                            //{
                            //    if (!string.IsNullOrEmpty(value.ToString().Trim())) isUpdate = true;
                            //}
                            //else
                            //    isUpdate = true;
                            if (property.Name.ToUpper() == "ID" || property.Name.ToUpper() == "MASTERID"
                                || property.Name.ToUpper() == "TKMDID" || property.Name.ToUpper() == "MASTER_ID"
                                || property.Name.ToUpper() == "TKMD_ID" || property.Name.ToUpper() == "ID_TKMD" || property.Name.ToUpper() == "ID_MASTER"
                                || property.Name.ToUpper() == "IDTKMD" || property.Name.ToUpper() == "IDMASTER" || property.Name.ToUpper() == "GIAYPHEP_ID")
                            {
                               /* if (Convert.ToInt64(value) == 0)*/ isUpdate = UpdateIDandMasterID;
                            }
                        }
                    }
                    if (isUpdate)
                    {
                        property.SetValue(DoiTuongCanUpdate, value, null);
                    }
                    if (property.PropertyType.Namespace == "System.Collections.Generic")
                    {
                        Type typelist = property.PropertyType.GetGenericArguments()[0];
                        object valueAfterUpdate = property.GetValue(DoiTuongCanUpdate, null);
                        IList listValue = valueAfterUpdate as IList;
                        if (listValue != null)
                            foreach (var item in listValue)
                            {
                                UpdateIdOffList(item, typelist);
                            }
                        //for (var i = 0; i < length; i++)
                        //{
                            
                        //}
                    }
                }
                return (T)DoiTuongCanUpdate;
            }
        }
        public static object UpdateIdOffList(object value, Type typeGeneric)
        {
            PropertyInfo[] listProperties = typeGeneric.GetProperties();
            foreach (PropertyInfo property in listProperties)
            {
                //object value = property.GetValue(value, null);
                if (property.Name.ToUpper() == "ID" || property.Name.ToUpper() == "MASTERID"
                               || property.Name.ToUpper() == "TKMDID" || property.Name.ToUpper() == "MASTER_ID"
                               || property.Name.ToUpper() == "TKMD_ID" || property.Name.ToUpper() == "ID_TKMD" || property.Name.ToUpper() == "ID_MASTER"
                               || property.Name.ToUpper() == "IDTKMD" || property.Name.ToUpper() == "IDMASTER")
                {
                    property.SetValue(value, 0, null);
                }
                if (property.PropertyType.Namespace == "System.Collections.Generic")
                {
                    Type typelist = property.PropertyType.GetGenericArguments()[0];
                    object valueAfterUpdate = property.GetValue(value, null);
                    IList listValue = valueAfterUpdate as IList;
                    if(listValue!= null)
                        foreach (var item in listValue)
                        {
                            UpdateIdOffList(item, typelist);
                        }
                   
                }
            }
            return value;
        }
        public static List<T> UpdateList<T>(List<T> DoiTuongCanUpdate, List<T> DoiTuongChuaDuLieuUpdate)
        {
            return UpdateList<T>(DoiTuongCanUpdate, DoiTuongChuaDuLieuUpdate, false);
        }
        public static List<T> UpdateList<T>(List<T> DoiTuongCanUpdate, List<T> DoiTuongChuaDuLieuUpdate, bool UpdateID)
        {
            if (DoiTuongChuaDuLieuUpdate == null || DoiTuongChuaDuLieuUpdate.Count == 0)
                DoiTuongCanUpdate = DoiTuongChuaDuLieuUpdate;
            else if (DoiTuongCanUpdate.Count > DoiTuongChuaDuLieuUpdate.Count)
                DoiTuongCanUpdate.RemoveRange(DoiTuongChuaDuLieuUpdate.Count - 1, DoiTuongCanUpdate.Count - DoiTuongChuaDuLieuUpdate.Count);
            for (int i = 0; i < DoiTuongChuaDuLieuUpdate.Count; i++)
            {
                if (DoiTuongCanUpdate == null) DoiTuongCanUpdate = new List<T>();
                if (i >= DoiTuongCanUpdate.Count)
                    DoiTuongCanUpdate.Add(DoiTuongChuaDuLieuUpdate[i]);
                else
                {
                    DoiTuongCanUpdate[i] = UpdateObject<T>(DoiTuongCanUpdate[i], DoiTuongChuaDuLieuUpdate[i], UpdateID);
                }
            }
            return DoiTuongCanUpdate;
        }
        public static IList<T> UpdateIList<T>(IList<T> DoiTuongCanUpdate, IList<T> DoiTuongChuaDuLieuUpdate)
        {
            return UpdateIList<T>(DoiTuongCanUpdate, DoiTuongChuaDuLieuUpdate, false);
        }
        public static IList<T> UpdateIList<T>(IList<T> DoiTuongCanUpdate, IList<T> DoiTuongChuaDuLieuUpdate, bool UpdateID)
        {
            for (int i = 0; i < DoiTuongChuaDuLieuUpdate.Count; i++)
            {
                //if (DoiTuongCanUpdate == null) DoiTuongCanUpdate = new List<T>();
                if (i >= DoiTuongCanUpdate.Count)
                    DoiTuongCanUpdate.Add(DoiTuongChuaDuLieuUpdate[i]);
                else
                {
                    DoiTuongCanUpdate[i] = UpdateObject<T>(DoiTuongCanUpdate[i], DoiTuongChuaDuLieuUpdate[i],UpdateID);
                }
            }
            return DoiTuongCanUpdate;
        }

        #region GUIDE and ERROR GUIDE

        public static string GuidePath = AppDomain.CurrentDomain.BaseDirectory + "Help\\guide";
        public static string GuideErrorPath = AppDomain.CurrentDomain.BaseDirectory + "Help\\gym_err";

        /// <summary>
        /// Doc file thong tin XML Guide theo Code cua Nghiep vu khai bao
        /// </summary>
        /// <param name="guideCode"></param>
        public static XmlDocument ReadGuideFile(string guideCode)
        {
            XmlDocument docGuide = new XmlDocument();
            try
            {
                string guideFile = GuidePath + string.Format("\\{0}_guide.xml", guideCode);

                if (System.IO.File.Exists(guideFile))
                    docGuide.Load(guideFile);
                else
                    docGuide = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                docGuide = null;
            }

            return docGuide;
        }

        /// <summary>
        /// Tim lay thong tin guide theo ID control trong danh sach
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetGuide(XmlDocument docGuide, string id)
        {
            return GetGuideString(docGuide, "id", id);
        }

        public static string GetGuideName(XmlDocument docGuide, string val)
        {
            XmlNode node = GetGuideNode(docGuide, "id", val);

            return node != null ? node.Attributes["name"].InnerXml : "";
        }

        private static string GetGuideString(XmlDocument docGuide, string key, string id)
        {
            if (docGuide != null)
            {
                int length = id.Length;
                StringBuilder builder = new StringBuilder(id);
                while (length > 0)
                {
                    XmlNode node = docGuide.DocumentElement.SelectSingleNode("item[@" + key + "='" + builder.ToString() + "']");
                    if (node != null)
                    {
                        return node.InnerText;
                    }
                    builder[--length] = '_';
                }
            }
            return "";
        }

        private static XmlNode GetGuideNode(XmlDocument docGuide, string key, string id)
        {
            if (docGuide != null)
            {
                int length = id.Length;
                StringBuilder builder = new StringBuilder(id);
                while (length > 0)
                {
                    XmlNode node = docGuide.DocumentElement.SelectSingleNode("item[@" + key + "='" + builder.ToString() + "']");
                    if (node != null)
                    {
                        return node;
                    }
                    builder[--length] = '_';
                }
            }
            return null;
        }

        /// <summary>
        /// Doc file thong tin XML Guide theo Code cua Nghiep vu khai bao
        /// </summary>
        /// <param name="guideCode"></param>
        public static XmlDocument ReadGuideErrorFile(string guideCode)
        {
            XmlDocument docGuideError = new XmlDocument();
            try
            {
                string guideFile = Company.KDT.SHARE.VNACCS.HelperVNACCS.GuideErrorPath + string.Format("\\{0}_err.xml", guideCode);

                if (System.IO.File.Exists(guideFile))
                    docGuideError.Load(guideFile);
                else
                    docGuideError = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                docGuideError = null;
            }

            return docGuideError;
        }

        /// <summary>
        /// Tim lay thong tin guide theo ID control trong danh sach
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetGuideError(XmlDocument docGuideError, string id)
        {
            XmlNode node = GetGuideErrorNode(docGuideError, "code", id);

            return node != null ? node.SelectSingleNode("description").InnerXml : "";
        }

        public static string GetGuideErrorDescription(XmlDocument docGuideError, string id)
        {
            XmlNode node = GetGuideErrorNode(docGuideError, "code", id);

            return node != null ? node.SelectSingleNode("description").InnerXml : "";
        }

        public static string GetGuideErrorDisposition(XmlDocument docGuideError, string id)
        {
            XmlNode node = GetGuideErrorNode(docGuideError, "code", id);

            return node != null ? node.SelectSingleNode("disposition").InnerXml : "";
        }

        private static string GetGuideErrorString(XmlDocument docGuideError, string key, string id)
        {
            if (docGuideError != null)
            {
                int length = id.Length;
                StringBuilder builder = new StringBuilder(id);
                while (length > 0)
                {
                    XmlNode node = docGuideError.DocumentElement.SelectSingleNode("response[@" + key + "='" + builder.ToString().Trim() + "']");
                    if (node != null)
                    {
                        return node.InnerText;
                    }
                    builder[--length] = '_';
                }
            }
            return "";
        }

        private static XmlNode GetGuideErrorNode(XmlDocument docGuideError, string key, string id)
        {
            if (docGuideError != null)
            {
                int length = id.Length;
                StringBuilder builder = new StringBuilder(id);
                while (length > 0)
                {
                    XmlNode node = docGuideError.DocumentElement.SelectSingleNode("response[@" + key + "='" + builder.ToString().Trim() + "']");
                    if (node != null)
                    {
                        return node;
                    }
                    builder[--length] = '_';
                }
            }
            return null;
        }

        #endregion
        private static string GetNameSigner(string signName)
        {
            string[] infors = signName.Split(',');
            string name = string.Empty;
            string mst = string.Empty;
            foreach (string info in infors)
            {
                int index = info.IndexOf("CN=");
                if (index > -1)
                {
                    name = info.Substring(index + 3);
                }
                index = info.IndexOf("OID");

                if (index > -1)
                {
                    index = info.IndexOf("=");
                    if (index > 0)
                        mst = info.Substring(index + 1);
                }
            }
            if (!string.IsNullOrEmpty(mst)) return string.Format("{0} - [{1}]", name, mst);
            else return string.Format("{0}", name);
        }

        public static string NewInputMSGID()
        {
            //return "123456789";
            return DateTime.Now.ToString("ddMMyyhhmm");
        }
    }
}
