-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_Insert]
	@ReferenceDB varchar(5),
	@Code varchar(5),
	@Name_VN nvarchar(max),
	@Name_EN varchar(max),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_Common]
(
	[ReferenceDB],
	[Code],
	[Name_VN],
	[Name_EN],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@ReferenceDB,
	@Code,
	@Name_VN,
	@Name_EN,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_Update]
	@ID int,
	@ReferenceDB varchar(5),
	@Code varchar(5),
	@Name_VN nvarchar(max),
	@Name_EN varchar(max),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_Common]
SET
	[ReferenceDB] = @ReferenceDB,
	[Code] = @Code,
	[Name_VN] = @Name_VN,
	[Name_EN] = @Name_EN,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_InsertUpdate]
	@ID int,
	@ReferenceDB varchar(5),
	@Code varchar(5),
	@Name_VN nvarchar(max),
	@Name_EN varchar(max),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_Common] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Common] 
		SET
			[ReferenceDB] = @ReferenceDB,
			[Code] = @Code,
			[Name_VN] = @Name_VN,
			[Name_EN] = @Name_EN,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_Common]
		(
			[ReferenceDB],
			[Code],
			[Name_VN],
			[Name_EN],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@ReferenceDB,
			@Code,
			@Name_VN,
			@Name_EN,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Common]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Common] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceDB],
	[Code],
	[Name_VN],
	[Name_EN],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Common]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ReferenceDB],
	[Code],
	[Name_VN],
	[Name_EN],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_Common] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceDB],
	[Code],
	[Name_VN],
	[Name_EN],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Common]	

GO

