-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_DoiTac_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_Insert]

IF OBJECT_ID(N'[dbo].[p_DoiTac_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_Update]

IF OBJECT_ID(N'[dbo].[p_DoiTac_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_DoiTac_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_Delete]

IF OBJECT_ID(N'[dbo].[p_DoiTac_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_DoiTac_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_Load]

IF OBJECT_ID(N'[dbo].[p_DoiTac_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_DoiTac_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_DoiTac_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_Insert]
	@MaCongTy varchar(100),
	@TenCongTy varchar(500),
	@DiaChi nvarchar(500),
	@DienThoai varchar(50),
	@Email varchar(50),
	@Fax varchar(50),
	@GhiChu nvarchar(1000),
	@MaDoanhNghiep varchar(50),
	@DateCreated datetime,
	@DateModified datetime,
	@DiaChi2 nvarchar(50),
	@BuuChinh varchar(7),
	@TinhThanh nvarchar(50),
	@QuocGia nvarchar(50),
	@MaNuoc varchar(2),
	@MaDaiLyHQ varchar(5),
	@TenDaiLyHQ nvarchar(500),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_HaiQuan_DoiTac]
(
	[MaCongTy],
	[TenCongTy],
	[DiaChi],
	[DienThoai],
	[Email],
	[Fax],
	[GhiChu],
	[MaDoanhNghiep],
	[DateCreated],
	[DateModified],
	[DiaChi2],
	[BuuChinh],
	[TinhThanh],
	[QuocGia],
	[MaNuoc],
	[MaDaiLyHQ],
	[TenDaiLyHQ]
)
VALUES 
(
	@MaCongTy,
	@TenCongTy,
	@DiaChi,
	@DienThoai,
	@Email,
	@Fax,
	@GhiChu,
	@MaDoanhNghiep,
	@DateCreated,
	@DateModified,
	@DiaChi2,
	@BuuChinh,
	@TinhThanh,
	@QuocGia,
	@MaNuoc,
	@MaDaiLyHQ,
	@TenDaiLyHQ
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_Update]
	@ID bigint,
	@MaCongTy varchar(100),
	@TenCongTy varchar(500),
	@DiaChi nvarchar(500),
	@DienThoai varchar(50),
	@Email varchar(50),
	@Fax varchar(50),
	@GhiChu nvarchar(1000),
	@MaDoanhNghiep varchar(50),
	@DateCreated datetime,
	@DateModified datetime,
	@DiaChi2 nvarchar(50),
	@BuuChinh varchar(7),
	@TinhThanh nvarchar(50),
	@QuocGia nvarchar(50),
	@MaNuoc varchar(2),
	@MaDaiLyHQ varchar(5),
	@TenDaiLyHQ nvarchar(500)
AS

UPDATE
	[dbo].[t_HaiQuan_DoiTac]
SET
	[MaCongTy] = @MaCongTy,
	[TenCongTy] = @TenCongTy,
	[DiaChi] = @DiaChi,
	[DienThoai] = @DienThoai,
	[Email] = @Email,
	[Fax] = @Fax,
	[GhiChu] = @GhiChu,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified,
	[DiaChi2] = @DiaChi2,
	[BuuChinh] = @BuuChinh,
	[TinhThanh] = @TinhThanh,
	[QuocGia] = @QuocGia,
	[MaNuoc] = @MaNuoc,
	[MaDaiLyHQ] = @MaDaiLyHQ,
	[TenDaiLyHQ] = @TenDaiLyHQ
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_InsertUpdate]
	@ID bigint,
	@MaCongTy varchar(100),
	@TenCongTy varchar(500),
	@DiaChi nvarchar(500),
	@DienThoai varchar(50),
	@Email varchar(50),
	@Fax varchar(50),
	@GhiChu nvarchar(1000),
	@MaDoanhNghiep varchar(50),
	@DateCreated datetime,
	@DateModified datetime,
	@DiaChi2 nvarchar(50),
	@BuuChinh varchar(7),
	@TinhThanh nvarchar(50),
	@QuocGia nvarchar(50),
	@MaNuoc varchar(2),
	@MaDaiLyHQ varchar(5),
	@TenDaiLyHQ nvarchar(500)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_DoiTac] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_DoiTac] 
		SET
			[MaCongTy] = @MaCongTy,
			[TenCongTy] = @TenCongTy,
			[DiaChi] = @DiaChi,
			[DienThoai] = @DienThoai,
			[Email] = @Email,
			[Fax] = @Fax,
			[GhiChu] = @GhiChu,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified,
			[DiaChi2] = @DiaChi2,
			[BuuChinh] = @BuuChinh,
			[TinhThanh] = @TinhThanh,
			[QuocGia] = @QuocGia,
			[MaNuoc] = @MaNuoc,
			[MaDaiLyHQ] = @MaDaiLyHQ,
			[TenDaiLyHQ] = @TenDaiLyHQ
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_HaiQuan_DoiTac]
		(
			[MaCongTy],
			[TenCongTy],
			[DiaChi],
			[DienThoai],
			[Email],
			[Fax],
			[GhiChu],
			[MaDoanhNghiep],
			[DateCreated],
			[DateModified],
			[DiaChi2],
			[BuuChinh],
			[TinhThanh],
			[QuocGia],
			[MaNuoc],
			[MaDaiLyHQ],
			[TenDaiLyHQ]
		)
		VALUES 
		(
			@MaCongTy,
			@TenCongTy,
			@DiaChi,
			@DienThoai,
			@Email,
			@Fax,
			@GhiChu,
			@MaDoanhNghiep,
			@DateCreated,
			@DateModified,
			@DiaChi2,
			@BuuChinh,
			@TinhThanh,
			@QuocGia,
			@MaNuoc,
			@MaDaiLyHQ,
			@TenDaiLyHQ
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_HaiQuan_DoiTac]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_DoiTac] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaCongTy],
	[TenCongTy],
	[DiaChi],
	[DienThoai],
	[Email],
	[Fax],
	[GhiChu],
	[MaDoanhNghiep],
	[DateCreated],
	[DateModified],
	[DiaChi2],
	[BuuChinh],
	[TinhThanh],
	[QuocGia],
	[MaNuoc],
	[MaDaiLyHQ],
	[TenDaiLyHQ]
FROM
	[dbo].[t_HaiQuan_DoiTac]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaCongTy],
	[TenCongTy],
	[DiaChi],
	[DienThoai],
	[Email],
	[Fax],
	[GhiChu],
	[MaDoanhNghiep],
	[DateCreated],
	[DateModified],
	[DiaChi2],
	[BuuChinh],
	[TinhThanh],
	[QuocGia],
	[MaNuoc],
	[MaDaiLyHQ],
	[TenDaiLyHQ]
FROM [dbo].[t_HaiQuan_DoiTac] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DoiTac_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: 19 February, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DoiTac_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaCongTy],
	[TenCongTy],
	[DiaChi],
	[DienThoai],
	[Email],
	[Fax],
	[GhiChu],
	[MaDoanhNghiep],
	[DateCreated],
	[DateModified],
	[DiaChi2],
	[BuuChinh],
	[TinhThanh],
	[QuocGia],
	[MaNuoc],
	[MaDaiLyHQ],
	[TenDaiLyHQ]
FROM
	[dbo].[t_HaiQuan_DoiTac]	

GO

