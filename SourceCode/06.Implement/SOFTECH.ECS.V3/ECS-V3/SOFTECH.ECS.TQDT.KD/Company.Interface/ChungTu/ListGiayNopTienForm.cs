﻿using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
#elif SXXK_V3
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
#elif GC_V3
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
#endif

namespace Company.Interface
{
    public partial class ListGiayNopTienForm : BaseForm
    {
        public ToKhaiMauDich TKMD;

        public ListGiayNopTienForm()
        {
            InitializeComponent();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ListChungTuNoForm_Load(object sender, EventArgs e)
        {            
            if (TKMD.ID > 0)
            {
                List<GiayNopTien> items = GiayNopTien.SelectCollectionDynamic("TKMD_ID=" + TKMD.ID, "");
                foreach (GiayNopTien item in items)
                {
                    item.LoadChiTiet();
                    item.LoadChungTu();
                }
                dgList.DataSource = items;
            }
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }

            SetButtonState(TKMD);

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<GiayNopTien> listGiayNopTien = new List<GiayNopTien>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa thông tin giấy nộp tiền không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        GiayNopTien gp = (GiayNopTien)i.GetRow().DataRow;
                        listGiayNopTien.Add(gp);
                    }
                }
            }



            try
            {
                foreach (GiayNopTien item in listGiayNopTien)
                {
                    item.DeleteFull();
                    TKMD.GiayNopTiens.Remove(item);
                }
                dgList.Refetch();
                dgList.DataSource = TKMD.GiayNopTiens;
            }
            catch { dgList.Refresh(); }
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            GiayNopTienForm giayNopTienForm = new GiayNopTienForm();
            giayNopTienForm.TKMD = TKMD;
            giayNopTienForm.ShowDialog(this);

            dgList.DataSource = TKMD.GiayNopTiens;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GiayNopTienForm f = new GiayNopTienForm();
            f.TKMD = TKMD;
            f.giayNopTien = (GiayNopTien)e.Row.DataRow;
            f.ShowDialog(this);
            dgList.DataSource = TKMD.GiayNopTiens;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        #region Begin Set Button State


        private bool SetButtonState(ToKhaiMauDich tkmd)
        {
            bool status = false;

            status = (tkmd.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || tkmd.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO
                || tkmd.TrangThaiXuLy == TrangThaiXuLy.DA_HUY);

            btnXoa.Enabled =btnTaoMoi.Enabled= !status;

            return true;
        }

        #endregion
    }
}
