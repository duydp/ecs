﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace DongBoDuLieu.BLL.GC
{
  public  class HopDongInfo
    {
        public long ID;
        public long SoTiepNhan;
        public int TrangThaiXuLy = -1;
        public string SoHopDong = String.Empty;
        public string MaHaiQuan = String.Empty;
        public string MaDoanhNghiep = String.Empty;
        public string MaDaiLy = String.Empty;
        public DateTime NgayKy = new DateTime(1900, 01, 01);
        public DateTime NgayDangKy = new DateTime(1900, 01, 01);
        public DateTime NgayHetHan = new DateTime(1900, 01, 01);
        public DateTime NgayGiaHan = new DateTime(1900, 01, 01);
        public string NuocThue_ID = String.Empty;
        public string NguyenTe_ID = String.Empty;
        public DateTime NgayTiepNhan = new DateTime(1900, 01, 01);
        public string DonViDoiTac = String.Empty;
        public string DiaChiDoiTac = String.Empty;
        public string CanBoTheoDoi = String.Empty;
        public string CanBoDuyet = String.Empty;
        public int TrangThaiThanhKhoan;
        public string GUIDSTR = String.Empty;
        public string DeXuatKhac = String.Empty;
        public string LyDoSua = String.Empty;
        public short ActionStatus;
        public string GuidReference = String.Empty;
        public int NamTN;
        public string HUONGDAN = String.Empty;
        public string PhanLuong = String.Empty;
        public string HuongdanPL = String.Empty;
    }
}
