﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using Company.Interface.GC;
//using Company.GC.BLL.KDT.GC;
namespace Company.Interface.KDT.GC
{
    public partial class CungUngDetailForm : BaseForm
    {
        public KDT_GC_CungUng_Detail CU_Detail = new KDT_GC_CungUng_Detail();
        private KDT_GC_CungUng_ChungTu ChungTu = new KDT_GC_CungUng_ChungTu();
        public int tragThai;
        public long HopDong_ID;
        public string MaNPL;
        public CungUngDetailForm()
        {
            InitializeComponent();
        }

        private void CungUngDetailForm_Load(object sender, EventArgs e)
        {
            if (tragThai == 1 && tragThai == 0)
            {
                btnGhi.Enabled = btnXoa.Enabled = true;
            }
            cbbLoaiChungTu.SelectedIndex = 0;
            LoadData();
            if (CU_Detail.CungUngCT_List.Count > 0)
            {
                ChungTu = new KDT_GC_CungUng_ChungTu();
                ChungTu = CU_Detail.CungUngCT_List[0];
                SetChungTu();
                //Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                //dm.HopDong_ID = HopDong_ID;
                //dm.MaSanPham = CU_Detail.MaSanPham;
                //dm.MaNguyenPhuLieu = MaNPL;
                //dm.Load();
                //txtSoLuong.Text = Convert.ToString(CU_Detail.SoLuong * (dm.DinhMucSuDung + (dm.DinhMucSuDung * dm.TyLeHaoHut / 100)));
            }
        }
        private void LoadData()
        {
            if(CU_Detail.CungUngCT_List.Count == 0 )
            {
                Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                dm.HopDong_ID = HopDong_ID;
                dm.MaSanPham = CU_Detail.MaSanPham;
                dm.MaNguyenPhuLieu = MaNPL;
                dm.Load();
                txtSoLuong.Text = Convert.ToString(CU_Detail.SoLuong * (dm.DinhMucSuDung + (dm.DinhMucSuDung * dm.TyLeHaoHut / 100)));
                //txtDonGia.Text = CU_Detail.
            }
            gdListChungTu.DataSource = CU_Detail.CungUngCT_List;
            gdListChungTu.Refetch();            
        }

      
        private void SetChungTu()
        {
            txtSoChungTu.Text = ChungTu.SoChungTu;
            clcNgayChungTu.Value = ChungTu.NgayChungTu;
            txtNoiCap.Text = ChungTu.NoiCap;
            txtSoLuong.Value = ChungTu.SoLuong;
            cbbLoaiChungTu.SelectedValue = ChungTu.LoaiChungTu;
        }
        private void GetChungTu()
        {
            ChungTu.SoChungTu = txtSoChungTu.Text;
            ChungTu.NgayChungTu = clcNgayChungTu.Value;
            ChungTu.NoiCap = txtNoiCap.Text;
            ChungTu.SoLuong =Convert.ToDecimal(txtSoLuong.Value);
            ChungTu.DonGia = Convert.ToDecimal(txtDonGia.Value);
            ChungTu.TriGia = Convert.ToDecimal(txtTriGia.Value);
            ChungTu.TyGia = 0;
            ChungTu.LoaiChungTu = cbbLoaiChungTu.SelectedValue != null ? 1 : Convert.ToInt16(cbbLoaiChungTu.SelectedValue.ToString());
        }
        bool isAddNew = false;
        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (txtSoChungTu.Text.Trim() == "")
            {
                ShowMessage("Nhập số chứng từ", false);
                txtSoChungTu.Focus();
                return;
            }
            if (CU_Detail == null)
                CU_Detail = new KDT_GC_CungUng_Detail();
            if (ChungTu == null || ChungTu.SoChungTu ==null)
            {
                ChungTu = new KDT_GC_CungUng_ChungTu();
                isAddNew = true;
            }
            GetChungTu();
            if (isAddNew)
                CU_Detail.CungUngCT_List.Add(ChungTu);
            LoadData();
            ChungTu = null;
            txtSoChungTu.Text = "";


        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = gdListChungTu.SelectedItems;
                List<KDT_GC_CungUng_ChungTu> ItemColl = new List<KDT_GC_CungUng_ChungTu>();
                if (gdListChungTu.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa chứng từ này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                            ItemColl.Add((KDT_GC_CungUng_ChungTu)i.GetRow().DataRow);
                    }
                    foreach (KDT_GC_CungUng_ChungTu item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        CU_Detail.CungUngCT_List.Remove(item);
                    }
                    LoadData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void gdListChungTu_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                ChungTu = (KDT_GC_CungUng_ChungTu)gdListChungTu.CurrentRow.DataRow;
                SetChungTu();
            }
        }
       
    }
}
