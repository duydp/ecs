﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components;
using Company.Interface.VNACCS;
using System.Threading;

namespace Company.Interface
{
    public partial class VNACC_TKBoSung_ThueHangHoa_HangForm : BaseFormHaveGuidPanel
    {
        private KDT_VNACC_HangMauDich_KhaiBoSung_Thue HangThue = null;
        private KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac HangThueThuKhac = null;
        public KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung;
        public long IDTKMD;
        private bool isAddNew = true;
        private bool isAddNewThuKhac = true;

        private DataTable dtHS = new DataTable();

        public VNACC_TKBoSung_ThueHangHoa_HangForm()
        {
            InitializeComponent();

            base.SetHandler(this);

            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.AMA.ToString());
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (!ValidateForm(false))
                    return;

                if (isAddNew)
                {
                    if (HangThue == null)
                        HangThue = new KDT_VNACC_HangMauDich_KhaiBoSung_Thue();

                    GetHangThue(HangThue);

                    TKBoSung.HangCollection.Add(HangThue);
                }
                else
                {
                    GetHangThue(HangThue);
                }

                grdHang.DataSource = TKBoSung.HangCollection;
                grdHang.Refetch();
                HangThue = new KDT_VNACC_HangMauDich_KhaiBoSung_Thue();
                SetHangThue(HangThue);
                isAddNew = true;

                gridThueThuKhac.DataSource = HangThue.HangThuKhacCollection;
                gridThueThuKhac.Refetch();
                HangThueThuKhac = new KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac();
                SetHangThueThuKhac(HangThueThuKhac);
                isAddNewThuKhac = true;
                hmd = new KDT_VNACC_HangMauDich();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetHangThue(KDT_VNACC_HangMauDich_KhaiBoSung_Thue HangThue)
        {
            try
            {
                errorProvider.Clear();

                HangThue.SoThuTuDongHangTrenToKhaiGoc = txtSoThuTuDongHangTrenToKhaiGoc.Value.ToString();

                HangThue.MoTaHangHoaTruocKhiKhaiBoSung = txtMoTaHangHoaTruocKhiKhaiBoSung.Text;
                HangThue.MoTaHangHoaSauKhiKhaiBoSung = txtMoTaHangHoaSauKhiKhaiBoSung.Text;

                HangThue.MaNuocXuatXuTruocKhiKhaiBoSung = ucMaNuocXuatXuTruocKhiKhaiBoSung.Code;
                HangThue.MaNuocXuatXuSauKhiKhaiBoSung = ucMaNuocXuatXuSauKhiKhaiBoSung.Code;

                HangThue.TriGiaTinhThueTruocKhiKhaiBoSung = Convert.ToDecimal(txtTriGiaTinhThueTruocKhiKhaiBoSung.Value);
                HangThue.TriGiaTinhThueSauKhiKhaiBoSung = Convert.ToDecimal(txtTriGiaTinhThueSauKhiKhaiBoSung.Value);

                HangThue.SoLuongTinhThueTruocKhiKhaiBoSung = Convert.ToDecimal(txtSoLuongTinhThueTruocKhiKhaiBoSung.Value);
                HangThue.SoLuongTinhThueSauKhiKhaiBoSung = Convert.ToDecimal(txtSoLuongTinhThueSauKhiKhaiBoSung.Value);

                HangThue.MaDonViTinhSoLuongTinhThueTruocKhaiBoSung = ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.Code;
                HangThue.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung = ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.Code;

                HangThue.MaSoHangHoaTruocKhiKhaiBoSung = txtMaSoHangHoaTruocKhiKhaiBoSung.Text;
                HangThue.MaSoHangHoaSauKhiKhaiBoSung = txtMaSoHangHoaSauKhiKhaiBoSung.Text;

                HangThue.ThueSuatTruocKhiKhaiBoSung = txtThueSuatTruocKhiKhaiBoSung.Text;
                HangThue.ThueSuatSauKhiKhaiBoSung = txtThueSuatSauKhiKhaiBoSung.Text;

                HangThue.SoTienThueTruocKhiKhaiBoSung = txtSoTienThueTruocKhiKhaiBoSung.Text;
                HangThue.SoTienThueSauKhiKhaiBoSung = txtSoTienThueSauKhiKhaiBoSung.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void SetHangThue(KDT_VNACC_HangMauDich_KhaiBoSung_Thue HangThue)
        {
            try
            {
                errorProvider.Clear();

                txtSoThuTuDongHangTrenToKhaiGoc.Value = HangThue.SoThuTuDongHangTrenToKhaiGoc;

                txtMoTaHangHoaTruocKhiKhaiBoSung.Text = HangThue.MoTaHangHoaTruocKhiKhaiBoSung;
                txtMoTaHangHoaSauKhiKhaiBoSung.Text = HangThue.MoTaHangHoaSauKhiKhaiBoSung;

                ucMaNuocXuatXuTruocKhiKhaiBoSung.Code = HangThue.MaNuocXuatXuTruocKhiKhaiBoSung;
                ucMaNuocXuatXuSauKhiKhaiBoSung.Code = HangThue.MaNuocXuatXuSauKhiKhaiBoSung;

                txtTriGiaTinhThueTruocKhiKhaiBoSung.Value = HangThue.TriGiaTinhThueTruocKhiKhaiBoSung;
                txtTriGiaTinhThueSauKhiKhaiBoSung.Value = HangThue.TriGiaTinhThueSauKhiKhaiBoSung;

                txtSoLuongTinhThueTruocKhiKhaiBoSung.Value = HangThue.SoLuongTinhThueTruocKhiKhaiBoSung;
                txtSoLuongTinhThueSauKhiKhaiBoSung.Value = HangThue.SoLuongTinhThueSauKhiKhaiBoSung;

                ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.Code = HangThue.MaDonViTinhSoLuongTinhThueTruocKhaiBoSung;
                ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.Code = HangThue.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung;

                txtMaSoHangHoaTruocKhiKhaiBoSung.Text = HangThue.MaSoHangHoaTruocKhiKhaiBoSung;
                txtMaSoHangHoaSauKhiKhaiBoSung.Text = HangThue.MaSoHangHoaSauKhiKhaiBoSung;

                txtThueSuatTruocKhiKhaiBoSung.Text = HangThue.ThueSuatTruocKhiKhaiBoSung;
                txtThueSuatSauKhiKhaiBoSung.Text = HangThue.ThueSuatSauKhiKhaiBoSung;

                txtSoTienThueTruocKhiKhaiBoSung.Text = HangThue.SoTienThueTruocKhiKhaiBoSung;
                txtSoTienThueSauKhiKhaiBoSung.Text = HangThue.SoTienThueSauKhiKhaiBoSung;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void SetHangThueThuKhac(KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac HangThueThuKhac)
        {
            try
            {
                errorProvider.Clear();

                txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.Value = HangThueThuKhac.TriGiaTinhThueTruocKhiKhaiBoSungThuKhac;
                txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.Value = HangThueThuKhac.TriGiaTinhThueSauKhiKhaiBoSungThuKhac;

                txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Value = HangThueThuKhac.SoLuongTinhThueTruocKhiKhaiBoSungThuKhac;
                txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Value = HangThueThuKhac.SoLuongTinhThueSauKhiKhaiBoSungThuKhac;

                ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Code = HangThueThuKhac.MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac;
                ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Code = HangThueThuKhac.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac;

                txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.Code = HangThueThuKhac.MaApDungThueSuatTruocKhiKhaiBoSungThuKhac;
                txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.Code = HangThueThuKhac.MaApDungThueSuatSauKhiKhaiBoSungThuKhac;

                txtThueSuatTruocKhiKhaiBoSungThuKhac.Text = HangThueThuKhac.ThueSuatTruocKhiKhaiBoSungThuKhac;
                txtThueSuatSauKhiKhaiBoSungThuKhac.Text = HangThueThuKhac.ThueSuatSauKhiKhaiBoSungThuKhac;

                txtSoTienThueTruocKhiKhaiBoSungThuKhac.Text = HangThueThuKhac.SoTienThueTruocKhiKhaiBoSungThuKhac;
                txtSoTienThueSauKhiKhaiBoSungThuKhac.Text = HangThueThuKhac.SoTienThueSauKhiKhaiBoSungThuKhac;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void GetHangThueThuKhac(KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac HangThueThuKhac)
        {
            try
            {
                errorProvider.Clear();

                HangThueThuKhac.SoDong = txtSoThuTuDongHangTrenToKhaiGoc.Value.ToString();

                HangThueThuKhac.TriGiaTinhThueTruocKhiKhaiBoSungThuKhac = Convert.ToDecimal(txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.Value);
                HangThueThuKhac.TriGiaTinhThueSauKhiKhaiBoSungThuKhac = Convert.ToDecimal(txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.Value);

                HangThueThuKhac.SoLuongTinhThueTruocKhiKhaiBoSungThuKhac = Convert.ToDecimal(txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Value);
                HangThueThuKhac.SoLuongTinhThueSauKhiKhaiBoSungThuKhac = Convert.ToDecimal(txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Value);

                HangThueThuKhac.MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac = ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Code;
                HangThueThuKhac.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac = ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Code;

                HangThueThuKhac.MaApDungThueSuatTruocKhiKhaiBoSungThuKhac = txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.Code;
                HangThueThuKhac.MaApDungThueSuatSauKhiKhaiBoSungThuKhac = txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.Code;

                HangThueThuKhac.ThueSuatTruocKhiKhaiBoSungThuKhac = txtThueSuatTruocKhiKhaiBoSungThuKhac.Text.ToString();
                HangThueThuKhac.ThueSuatSauKhiKhaiBoSungThuKhac = txtThueSuatSauKhiKhaiBoSungThuKhac.Text.ToString();

                HangThueThuKhac.SoTienThueTruocKhiKhaiBoSungThuKhac = txtSoTienThueTruocKhiKhaiBoSungThuKhac.Text;
                HangThueThuKhac.SoTienThueSauKhiKhaiBoSungThuKhac = txtSoTienThueSauKhiKhaiBoSungThuKhac.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void VNACC_TKBoSung_ThueHangHoa_HangForm_Load(object sender, EventArgs e)
        {
            try
            {
                SetIDControl();

                SetMaxLengthControl();

                Khoitao_DuLieuChuan();

                grdHang.DataSource = TKBoSung.HangCollection;

                btnAddThuKhac.Click += new EventHandler(btnAddThuKhac_Click);
                btnDelThuKhac.Click += new EventHandler(btnDelThuKhac_Click);

                ValidateForm(true);

                SetAutoRemoveUnicodeAndUpperCaseControl();
                if (this.OpenType == OpenFormType.View)
                {
                    btnGhi.Enabled = false;
                    btnXoa.Enabled = false;
                    btnAddThuKhac.Enabled = false;
                    btnDelThuKhac.Enabled = false;
                }
                else
                {
                    btnGhi.Enabled = true;
                    btnXoa.Enabled = true;
                    btnAddThuKhac.Enabled = true;
                    btnDelThuKhac.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void Khoitao_DuLieuChuan()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
                dtHS = MaHS.SelectAll();
                foreach (DataRow dr in dtHS.Rows)
                    col.Add(dr["HS10So"].ToString());

                txtMaSoHangHoaTruocKhiKhaiBoSung.AutoCompleteCustomSource = col;
                txtMaSoHangHoaSauKhiKhaiBoSung.AutoCompleteCustomSource = col;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grdHang.SelectedItems;
                List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue> hangColl = new List<KDT_VNACC_HangMauDich_KhaiBoSung_Thue>();
                if (grdHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            hangColl.Add((KDT_VNACC_HangMauDich_KhaiBoSung_Thue)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_HangMauDich_KhaiBoSung_Thue hmd in hangColl)
                    {
                        if (hmd.ID > 0)
                            hmd.Delete();
                        TKBoSung.HangCollection.Remove(hmd);
                    }

                    grdHang.DataSource = TKBoSung.HangCollection;
                    try
                    {
                        grdHang.Refetch();
                    }
                    catch { }
                    HangThue = new KDT_VNACC_HangMauDich_KhaiBoSung_Thue();
                    isAddNew = true;
                    SetHangThue(HangThue);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void grdHang_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grdHang.SelectedItems;
                if (grdHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                HangThue = (KDT_VNACC_HangMauDich_KhaiBoSung_Thue)items[0].GetRow().DataRow;
                SetHangThue(HangThue);
                isAddNew = false;

                gridThueThuKhac.DataSource = HangThue.HangThuKhacCollection;
                try
                {
                    gridThueThuKhac.Refetch();
                }
                catch { }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void txtMaSoHangHoa_Leave(object sender, EventArgs e)
        {
            try
            {
                string MoTa = MaHS.CheckExist(txtMaSoHangHoaTruocKhiKhaiBoSung.Text);
                if (MoTa == "")
                {
                    txtMaSoHangHoaTruocKhiKhaiBoSung.Focus();
                    errorProvider.SetIconPadding(txtMaSoHangHoaTruocKhiKhaiBoSung, -8);
                    errorProvider.SetError(txtMaSoHangHoaTruocKhiKhaiBoSung, setText("Mã HS không có trong danh mục mã HS.", "This HS is not exist"));
                }
                else
                {
                    errorProvider.SetError(txtMaSoHangHoaTruocKhiKhaiBoSung, string.Empty);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtSoThuTuDongHangTrenToKhaiGoc.Tag = "RAN"; //Số thứ tự dòng/hàng trên tờ khai gốc
                txtMoTaHangHoaTruocKhiKhaiBoSung.Tag = "CMB"; //Mô tả hàng hóa trước khi khai bổ sung
                txtMoTaHangHoaSauKhiKhaiBoSung.Tag = "CMA"; //Mô tả hàng hóa sau khi khai bổ sung
                ucMaNuocXuatXuTruocKhiKhaiBoSung.TagCode = "ORB"; //Mã nước xuất xứ trước khi khai bổ sung
                ucMaNuocXuatXuSauKhiKhaiBoSung.TagCode = "ORA"; //Mã nước xuất xứ sau khi khai bổ sung
                txtTriGiaTinhThueTruocKhiKhaiBoSung.Tag = "MKB"; //Trị giá tính thuế trước khi khai bổ sung
                txtTriGiaTinhThueSauKhiKhaiBoSung.Tag = "AKB"; //Trị giá tính thuế sau khi khai bổ sung
                txtSoLuongTinhThueTruocKhiKhaiBoSung.Tag = "MKQ"; //Số lượng tính thuế trước khi khai bổ sung
                txtSoLuongTinhThueSauKhiKhaiBoSung.Tag = "AKQ"; //Số lượng tính thuế sau khi khai bổ sung
                ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.TagName = "MKC"; //Mã đơn vị tính của số lượng tính thuếtrước khai bổ sung
                ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.TagName = "AKC"; //Mã đơn vị tính số lượng tính thuế sau khi khai bổ sung
                txtMaSoHangHoaTruocKhiKhaiBoSung.Tag = "MKT"; //Mã số hàng hóa trước khi khai bổ sung
                txtMaSoHangHoaSauKhiKhaiBoSung.Tag = "AKT"; //Mã số hàng hóa sau khi khai bổ sung
                txtThueSuatTruocKhiKhaiBoSung.Tag = "MKR"; //Thuế suất trước khi khai bổ sung
                txtThueSuatSauKhiKhaiBoSung.Tag = "AKR"; //Thuế suất sau khi khai bổ sung
                txtSoTienThueTruocKhiKhaiBoSung.Tag = "MKA"; //Số tiền thuế trước khi khai bổ sung
                txtSoTienThueSauKhiKhaiBoSung.Tag = "AKA"; //Số tiền thuế sau khi khai bổ sung

                txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.Tag = "MB_"; //Trị giá tính thuế trước khi khai bổ sung (thuế và thu khác)
                txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Tag = "MQ_"; //Số lượng tính thuế trước khi khai bổ sung (thuế và thu khác)
                ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.TagName = "MC_"; //Mã đơn vị tính số lượng tính thuế trước khi khai bổ sung (thuế và thu khác) 
                txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.Tag = "MK_"; //Mã áp dụng thuế suất trước khi khai bổ sung (thuế và thu khác)
                txtThueSuatTruocKhiKhaiBoSungThuKhac.Tag = "MR_"; //Thuế suất trước khi khai bổ sung (thuế và thu khác)
                txtSoTienThueTruocKhiKhaiBoSungThuKhac.Tag = "MA_"; //Số tiền thuế trước khi khai bổ sung (thuế và thu khác)
                txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.Tag = "AB_"; //Trị giá tính thuế sau khi khai bổ sung (thuế và thu khác)
                txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Tag = "AQ_"; //Số lượng tính thuế sau khi khai bổ sung ( thuế và thu khác)
                ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.TagName = "AC_"; //Mã đơn vị tính số lượng tính thuế sau khi khai bổ sung (thuế và thu khác)
                txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.Tag = "AK_"; //Mã áp dụng thuế suất sau khi khai bổ sung (thuế và thu khác)
                txtThueSuatSauKhiKhaiBoSungThuKhac.Tag = "AR_"; //Thuế suất sau khi khai bổ sung (thuế và thu khác)
                txtSoTienThueSauKhiKhaiBoSungThuKhac.Tag = "AA_"; //Số tiền thuế sau khi khai bổ sung (thuế và thu khác)

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtSoThuTuDongHangTrenToKhaiGoc.MaxLength = 2;
                txtMoTaHangHoaTruocKhiKhaiBoSung.MaxLength = 600;
                txtMoTaHangHoaSauKhiKhaiBoSung.MaxLength = 600;
                //ucMaNuocXuatXuTruocKhiKhaiBoSung.MaxLength = 2;
                //ucMaNuocXuatXuSauKhiKhaiBoSung.MaxLength = 2;
                txtTriGiaTinhThueTruocKhiKhaiBoSung.MaxLength = 17;
                txtSoLuongTinhThueTruocKhiKhaiBoSung.MaxLength = 12;
                //ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.MaxLength = 4;
                txtMaSoHangHoaTruocKhiKhaiBoSung.MaxLength = 12;
                txtThueSuatTruocKhiKhaiBoSung.MaxLength = 30;
                txtSoTienThueTruocKhiKhaiBoSung.MaxLength = 16;
                txtTriGiaTinhThueSauKhiKhaiBoSung.MaxLength = 17;
                txtSoLuongTinhThueSauKhiKhaiBoSung.MaxLength = 12;
                //ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.MaxLength = 4;
                txtMaSoHangHoaSauKhiKhaiBoSung.MaxLength = 12;
                txtThueSuatSauKhiKhaiBoSung.MaxLength = 30;
                txtSoTienThueSauKhiKhaiBoSung.MaxLength = 16;
                txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.MaxLength = 17;
                txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.MaxLength = 12;
                //ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.MaxLength = 4;
                //txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.MaxLength = 10;
                txtThueSuatTruocKhiKhaiBoSungThuKhac.MaxLength = 25;
                txtSoTienThueTruocKhiKhaiBoSungThuKhac.MaxLength = 16;
                txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.MaxLength = 17;
                txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.MaxLength = 12;
                //ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.MaxLength = 4;
                //txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.MaxLength = 10;
                txtThueSuatSauKhiKhaiBoSungThuKhac.MaxLength = 25;
                txtSoTienThueSauKhiKhaiBoSungThuKhac.MaxLength = 16;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            //try
            //{
            //    Cursor = Cursors.WaitCursor;

            //    isValid &= ValidateControl.ValidateNull(txtSoThuTuDongHangTrenToKhaiGoc, errorProvider, "Số thứ tự dòng trên tờ khai gốc", isOnlyWarning);
            //    isValid &= ValidateControl.ValidateNull(txtMoTaHangHoaTruocKhiKhaiBoSung, errorProvider, "Mô tả hàng hóa trước khi khai bổ sung", isOnlyWarning);
            //    ucMaNuocXuatXuTruocKhiKhaiBoSung.SetValidate = !isOnlyWarning; ucMaNuocXuatXuTruocKhiKhaiBoSung.IsOnlyWarning = isOnlyWarning;
            //    isValid &= ucMaNuocXuatXuTruocKhiKhaiBoSung.IsValidate;
            //    isValid &= ValidateControl.ValidateNull(txtMaSoHangHoaTruocKhiKhaiBoSung, errorProvider, "Mã số hàng hóa trước khi khai bổ sung", isOnlyWarning);
            //    isValid &= ValidateControl.ValidateNull(txtThueSuatTruocKhiKhaiBoSung, errorProvider, "Thuế suất trước khi khai bổ sung", isOnlyWarning);
            //    isValid &= ValidateControl.ValidateNull(txtSoTienThueTruocKhiKhaiBoSung, errorProvider, "Số tiền thuế trước khi khai bổ sung", isOnlyWarning);
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //    Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            //}
            //finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateControlThuKhac()
        {
            bool isValid = true;

            //try
            //{
            //    Cursor = Cursors.WaitCursor;

            //    isValid &= ValidateControl.ValidateNull(txtSoTienThueTruocKhiKhaiBoSungThuKhac, errorProvider, "Số tiền thuế trước khi khai bổ sung (thuế và thu khác)");
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //    Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            //}
            //finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void btnAddThuKhac_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (!ValidateControlThuKhac())
                    return;

                if (isAddNewThuKhac)
                {
                    HangThueThuKhac = new KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac();

                    GetHangThueThuKhac(HangThueThuKhac);

                    if (HangThue == null)
                        HangThue = new KDT_VNACC_HangMauDich_KhaiBoSung_Thue();
                    HangThue.HangThuKhacCollection.Add(HangThueThuKhac);
                }
                else
                {
                    GetHangThueThuKhac(HangThueThuKhac);
                }

                gridThueThuKhac.DataSource = HangThue.HangThuKhacCollection;
                gridThueThuKhac.Refetch();

                HangThueThuKhac = new KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac();
                SetHangThueThuKhac(HangThueThuKhac);
                isAddNewThuKhac = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnDelThuKhac_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = gridThueThuKhac.SelectedItems;
                List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac> hangColl = new List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac>();
                if (gridThueThuKhac.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            hangColl.Add((KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac hmd in hangColl)
                    {
                        if (hmd.ID > 0)
                            hmd.Delete();

                        HangThue.HangThuKhacCollection.Remove(hmd);
                    }

                    gridThueThuKhac.DataSource = HangThue.HangThuKhacCollection;
                    try
                    {
                        gridThueThuKhac.Refetch();
                    }
                    catch { }

                    HangThueThuKhac = new KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac();
                    isAddNewThuKhac = true;
                    //SetHangThueThuKhac(HangThueThuKhac);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void grdThueThuKhac_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = gridThueThuKhac.SelectedItems;
                if (gridThueThuKhac.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                HangThueThuKhac = (KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac)items[0].GetRow().DataRow;
                SetHangThueThuKhac(HangThueThuKhac);
                isAddNewThuKhac = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            txtSoThuTuDongHangTrenToKhaiGoc.TextChanged += new EventHandler(SetTextChanged_Handler);
           // txtMoTaHangHoaTruocKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
           // txtMoTaHangHoaSauKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucMaNuocXuatXuTruocKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaNuocXuatXuSauKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTriGiaTinhThueTruocKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoLuongTinhThueTruocKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaSoHangHoaTruocKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtThueSuatTruocKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoTienThueTruocKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTriGiaTinhThueSauKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoLuongTinhThueSauKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaSoHangHoaSauKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtThueSuatSauKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoTienThueSauKhiKhaiBoSung.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtThueSuatTruocKhiKhaiBoSungThuKhac.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoTienThueTruocKhiKhaiBoSungThuKhac.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtThueSuatSauKhiKhaiBoSungThuKhac.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoTienThueSauKhiKhaiBoSungThuKhac.TextChanged += new EventHandler(SetTextChanged_Handler);

            txtSoThuTuDongHangTrenToKhaiGoc.CharacterCasing = CharacterCasing.Upper;
            //txtMoTaHangHoaTruocKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            //txtMoTaHangHoaSauKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            ucMaNuocXuatXuTruocKhiKhaiBoSung.IsUpperCase = true;
            ucMaNuocXuatXuSauKhiKhaiBoSung.IsUpperCase = true;
            txtTriGiaTinhThueTruocKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            txtSoLuongTinhThueTruocKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.IsUpperCase = true;
            txtMaSoHangHoaTruocKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            txtThueSuatTruocKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            txtSoTienThueTruocKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            txtTriGiaTinhThueSauKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            txtSoLuongTinhThueSauKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.IsUpperCase = true;
            txtMaSoHangHoaSauKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            txtThueSuatSauKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            txtSoTienThueSauKhiKhaiBoSung.CharacterCasing = CharacterCasing.Upper;
            txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.CharacterCasing = CharacterCasing.Upper;
            txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.CharacterCasing = CharacterCasing.Upper;
            ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.IsUpperCase = true;
            //txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.CharacterCasing = CharacterCasing.Upper;
            txtThueSuatTruocKhiKhaiBoSungThuKhac.CharacterCasing = CharacterCasing.Upper;
            txtSoTienThueTruocKhiKhaiBoSungThuKhac.CharacterCasing = CharacterCasing.Upper;
            txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.CharacterCasing = CharacterCasing.Upper;
            txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.CharacterCasing = CharacterCasing.Upper;
            ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.IsUpperCase = true;
            //txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.CharacterCasing = CharacterCasing.Upper;
            txtThueSuatSauKhiKhaiBoSungThuKhac.CharacterCasing = CharacterCasing.Upper;
            txtSoTienThueSauKhiKhaiBoSungThuKhac.CharacterCasing = CharacterCasing.Upper;

        }
        private KDT_VNACC_HangMauDich hmd;
        private void txtSoThuTuDongHangTrenToKhaiGoc_Click(object sender, EventArgs e)
        {
            try
            {
                VNACC_ListHangHMD fhmd = new VNACC_ListHangHMD(VNACC_ListHangHMD.SelectTion.SingleSelect, new long[] { this.IDTKMD });
                fhmd.ShowDialog(this);
                if (fhmd.DialogResult == DialogResult.OK)
                {
                    hmd = fhmd.HangMD;
                    setHangFromHMD(hmd);
                    cmbNoiDungSuaChua.SelectedValue = 0;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void setHangFromHMD(KDT_VNACC_HangMauDich hmd)
        {
            try
            {
                errorProvider.Clear();
                txtMaHangTruocKhiKhaiBao.Text = hmd.MaHangHoa;
                txtMaHangSauKhiKhaiBao.Text = hmd.MaHangHoa;

                txtSoThuTuDongHangTrenToKhaiGoc.Value = hmd.SoDong;

                txtMoTaHangHoaTruocKhiKhaiBoSung.Text = string.IsNullOrEmpty(hmd.MaHangHoa) ? hmd.TenHang : hmd.MaHangHoa + "#&" + hmd.TenHang;
                txtMoTaHangHoaSauKhiKhaiBoSung.Text = string.IsNullOrEmpty(hmd.MaHangHoa) ? hmd.TenHang : hmd.MaHangHoa + "#&" + hmd.TenHang;

                ucMaNuocXuatXuTruocKhiKhaiBoSung.Code = hmd.NuocXuatXu =="" ? "VN" : hmd.NuocXuatXu;
                ucMaNuocXuatXuSauKhiKhaiBoSung.Code = hmd.NuocXuatXu == "" ? "VN" : hmd.NuocXuatXu;

                txtTriGiaTinhThueTruocKhiKhaiBoSung.Value = hmd.TriGiaTinhThueS;
                txtTriGiaTinhThueSauKhiKhaiBoSung.Value = hmd.TriGiaTinhThueS;

                txtSoLuongTinhThueTruocKhiKhaiBoSung.Value = hmd.SoLuong1;
                txtSoLuongTinhThueSauKhiKhaiBoSung.Value = hmd.SoLuong1;

                ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.Code = hmd.DVTLuong1;
                ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.Code = hmd.DVTLuong1;

                txtMaSoHangHoaTruocKhiKhaiBoSung.Text = hmd.MaSoHang;
                txtMaSoHangHoaSauKhiKhaiBoSung.Text = hmd.MaSoHang;
                //duydp 30/12/2015
                txtThueSuatTruocKhiKhaiBoSung.Text = (string.IsNullOrEmpty(hmd.ThueSuatThue) || hmd.ThueSuatThue == "0") ? "KCT" : hmd.ThueSuatThue.ToString();
                txtThueSuatSauKhiKhaiBoSung.Text = (string.IsNullOrEmpty(hmd.ThueSuatThue) || hmd.ThueSuatThue == "0") ? "KCT" : hmd.ThueSuatThue.ToString();
                //duydp 30/12/2015
                txtSoTienThueTruocKhiKhaiBoSung.Text = hmd.SoTienThue.ToString();
                txtSoTienThueSauKhiKhaiBoSung.Text = hmd.SoTienThue.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }

        private void cmbNoiDungSuaChua_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (System.Convert.ToInt16(cmbNoiDungSuaChua.SelectedItem.Value) == 0) // Sửa hàng
                {
                    setHangFromHMD(hmd);
                }
                else if (System.Convert.ToInt16(cmbNoiDungSuaChua.SelectedItem.Value) == 1) // Thêm hàng
                {
                    KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(TKBoSung.TKMD_ID);
                    TKMD.LoadFull();
                    if (TKMD.HangCollection.Count==0)
                    {
                        TKMD.HangCollection = KDT_VNACC_HangMauDich.SelectCollectionDynamic("TKMD_ID =" + TKMD.ID + "","");
                    }
                    errorProvider.Clear();
                    txtSoThuTuDongHangTrenToKhaiGoc.Value = (TKMD.HangCollection.Count + 1);

                    txtMoTaHangHoaTruocKhiKhaiBoSung.Text = "N/A";

                    ucMaNuocXuatXuTruocKhiKhaiBoSung.Code = "VN";

                    txtTriGiaTinhThueTruocKhiKhaiBoSung.Value = 0;

                    txtSoLuongTinhThueTruocKhiKhaiBoSung.Value = 0;

                    ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.Code = string.Empty;

                    txtMaSoHangHoaTruocKhiKhaiBoSung.Text = "0";

                    txtThueSuatTruocKhiKhaiBoSung.Text = "0";

                    txtSoTienThueTruocKhiKhaiBoSung.Text = "0";

                    txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.Value = 0;
                    txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Value = 0;
                    txtThueSuatTruocKhiKhaiBoSungThuKhac.Text = "0";
                    txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.Text = "0";
                    ucMaDonViTinhSoLuongTinhThueTruocKhaiBoSung.Code = String.Empty;
                    txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.Code = String.Empty;

                }
                else // Xóa hàng
                {
                    errorProvider.Clear();

                    txtMoTaHangHoaSauKhiKhaiBoSung.Text = "N/A";

                    ucMaNuocXuatXuSauKhiKhaiBoSung.Code = "VN";

                    txtTriGiaTinhThueSauKhiKhaiBoSung.Value = 0;

                    txtSoLuongTinhThueSauKhiKhaiBoSung.Value = 0;

                    ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.Code = string.Empty;

                    txtMaSoHangHoaSauKhiKhaiBoSung.Text = "0";

                    txtThueSuatSauKhiKhaiBoSung.Text = "0";

                    txtSoTienThueSauKhiKhaiBoSung.Text = "0";


                    txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.Value = 0;
                    txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Value = 0;
                    txtThueSuatSauKhiKhaiBoSungThuKhac.Text = "0";
                    txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.Text = "0";
                    ucMaDonViTinhSoLuongTinhThueSauKhaiBoSung.Code = String.Empty;
                    txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.Code = String.Empty;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                VNACC_ListThueVaThuKhac f = new VNACC_ListThueVaThuKhac();
                f.HMD = this.hmd;
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.OK)
                {
                    setThueThuKhacTruocBy(f.thuethukhac);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void setThueThuKhacTruocBy( KDT_VNACC_HangMauDich_ThueThuKhac thuethukhac)
        {
            try
            {
                if (thuethukhac != null)
                {
                    errorProvider.Clear();

                    txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.Value = thuethukhac.TriGiaTinhThueVaThuKhac; //HangThueThuKhac.TriGiaTinhThueTruocKhiKhaiBoSungThuKhac;
                    txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.Value = thuethukhac.TriGiaTinhThueVaThuKhac;

                    txtSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Value = (thuethukhac.SoLuongTinhThueVaThuKhac == 0) ? hmd.SoLuong1 : thuethukhac.SoLuongTinhThueVaThuKhac; //HangThueThuKhac.SoLuongTinhThueTruocKhiKhaiBoSungThuKhac;
                    txtSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Value = (thuethukhac.SoLuongTinhThueVaThuKhac == 0) ? hmd.SoLuong1 : thuethukhac.SoLuongTinhThueVaThuKhac;

                    ucMaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac.Code = (string.IsNullOrEmpty(thuethukhac.MaDVTDanhThueVaThuKhac)) ? hmd.DVTLuong1 : thuethukhac.MaDVTDanhThueVaThuKhac; //HangThueThuKhac.MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac;
                    ucMaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac.Code = (string.IsNullOrEmpty(thuethukhac.MaDVTDanhThueVaThuKhac)) ? hmd.DVTLuong1 : thuethukhac.MaDVTDanhThueVaThuKhac;

                    txtMaApDungThueSuatTruocKhiKhaiBoSungThuKhac.Code = thuethukhac.MaTSThueThuKhac; //HangThueThuKhac.MaApDungThueSuatTruocKhiKhaiBoSungThuKhac;
                    txtMaApDungThueSuatSauKhiKhaiBoSungThuKhac.Code = thuethukhac.MaTSThueThuKhac;

                    txtThueSuatTruocKhiKhaiBoSungThuKhac.Text = String.IsNullOrEmpty(thuethukhac.ThueSuatThueVaThuKhac) ? String.Empty : thuethukhac.ThueSuatThueVaThuKhac.ToString().Replace("%", ""); //HangThueThuKhac.ThueSuatTruocKhiKhaiBoSungThuKhac;
                    txtThueSuatSauKhiKhaiBoSungThuKhac.Text = String.IsNullOrEmpty(thuethukhac.ThueSuatThueVaThuKhac) ? String.Empty : thuethukhac.ThueSuatThueVaThuKhac.ToString().Replace("%", "");

                    txtSoTienThueTruocKhiKhaiBoSungThuKhac.Text = thuethukhac.SoTienThueVaThuKhac.ToString();// HangThueThuKhac.SoTienThueTruocKhiKhaiBoSungThuKhac;
                    txtSoTienThueTruocKhiKhaiBoSungThuKhac.Text = thuethukhac.SoTienThueVaThuKhac.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private double triGiaHD;
        private double thuesuat;
        private void txtThueSuatTruocKhiKhaiBoSung_Leave(object sender, EventArgs e)
        {
            try
            {
                this.triGiaHD = Convert.ToDouble(txtTriGiaTinhThueTruocKhiKhaiBoSung.Value);
                if (txtThueSuatTruocKhiKhaiBoSung.Text != "0")
                {
                    if (txtThueSuatTruocKhiKhaiBoSung.Text.Contains("%"))
                    {
                        this.thuesuat = Convert.ToDouble(txtThueSuatTruocKhiKhaiBoSung.Text.Replace("%", ""));
#if KD_V4
                    txtSoTienThueTruocKhiKhaiBoSung.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.SoThapPhan.TriGiaNT).Replace(".", ",");
#elif GC_V4
                        txtSoTienThueTruocKhiKhaiBoSung.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.SoThapPhan.TGNT).Replace(".", ",");
#elif SXXK_V4
                    txtSoTienThueTruocKhiKhaiBoSung.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.TriGiaNT).Replace(".", ",");
#endif
                    }
                    else if (txtThueSuatTruocKhiKhaiBoSung.Text == "KCT")
                    {
                        txtSoTienThueTruocKhiKhaiBoSung.Text = "0";
                    }
                    else
                    {
                        try
                        {
                            this.thuesuat = Convert.ToDouble(txtThueSuatTruocKhiKhaiBoSung.Text);
#if KD_V4
                        txtSoTienThueTruocKhiKhaiBoSung.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100,GlobalSettings.SoThapPhan.TriGiaNT).Replace(".",",");
#elif GC_V4
                            txtSoTienThueTruocKhiKhaiBoSung.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.SoThapPhan.TGNT).Replace(".", ",");
#elif SXXK_V4
                        txtSoTienThueTruocKhiKhaiBoSung.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.TriGiaNT).Replace(".", ",");
#endif
                        }
                        catch (Exception ex)
                        {
                            //ShowMessage("Nhập thuế suất trước khi khai bổ sung\nNhập KCT trong trường hợp không chịu thuế \n(Thuế suất 0%)", false);
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }

                    }
                }
                else
                {
                    txtSoTienThueTruocKhiKhaiBoSung.Text = "0";
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtThueSuatSauKhiKhaiBoSung_Leave(object sender, EventArgs e)
        {
            try
            {
                this.triGiaHD = Convert.ToDouble(txtTriGiaTinhThueSauKhiKhaiBoSung.Value);
                if (txtThueSuatSauKhiKhaiBoSung.Text != "0")
                {
                    if (txtThueSuatSauKhiKhaiBoSung.Text.Contains("%"))
                    {
                        this.thuesuat = Convert.ToDouble(txtThueSuatSauKhiKhaiBoSung.Text.Replace("%", ""));
#if KD_V4
                    txtSoTienThueSauKhiKhaiBoSung.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.SoThapPhan.TriGiaNT).Replace(".", ",");
#elif GC_V4
                        txtSoTienThueSauKhiKhaiBoSung.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.SoThapPhan.TGNT).Replace(".", ",");
#elif SXXK_V4
                    txtSoTienThueSauKhiKhaiBoSung.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.TriGiaNT).Replace(".", ",");
#endif

                    }
                    else if (txtThueSuatSauKhiKhaiBoSung.Text == "KCT")
                    {
                        txtSoTienThueSauKhiKhaiBoSung.Text = "0";
                    }
                    else
                    {
                        try
                        {
                            this.thuesuat = Convert.ToDouble(txtThueSuatSauKhiKhaiBoSung.Text);
                            //Cấu hình số thập phân sau dấy phẩy dựa theo trị giá NT
#if KD_V4
                        txtSoTienThueSauKhiKhaiBoSung.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.SoThapPhan.TriGiaNT).Replace(".", ",");
#elif GC_V4
                            txtSoTienThueSauKhiKhaiBoSung.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.SoThapPhan.TGNT).Replace(".", ",");
#elif SXXK_V4
                        txtSoTienThueSauKhiKhaiBoSung.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.TriGiaNT).Replace(".", ",");
#endif
                        }
                        catch (Exception ex)
                        {
                            //ShowMessage("Nhập thuế suất sau khi khai bổ sung\nNhập KCT trong trường hợp không chịu thuế \n(Thuế suất 0%)", false);
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }

                    }
                }
                else
                {
                    txtSoTienThueSauKhiKhaiBoSung.Text = "0";
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtThueSuatTruocKhiKhaiBoSungThuKhac_Leave(object sender, EventArgs e)
        {
            try
            {
                this.triGiaHD = Convert.ToDouble(txtTriGiaTinhThueTruocKhiKhaiBoSungThuKhac.Value);
                if (txtThueSuatTruocKhiKhaiBoSungThuKhac.Text != "0")
                {
                    if (txtThueSuatTruocKhiKhaiBoSungThuKhac.Text.Contains("%"))
                    {
                        this.thuesuat = Convert.ToDouble(txtThueSuatTruocKhiKhaiBoSungThuKhac.Text.Replace("%", ""));
#if KD_V4
                    txtSoTienThueTruocKhiKhaiBoSungThuKhac.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.SoThapPhan.TriGiaNT).Replace(".", ",");
#elif GC_V4
                        txtSoTienThueTruocKhiKhaiBoSungThuKhac.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.SoThapPhan.TGNT).Replace(".", ",");
#elif SXXK_V4
                    txtSoTienThueTruocKhiKhaiBoSungThuKhac.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.TriGiaNT).Replace(".", ",");
#endif
                    }
                    else if (txtThueSuatTruocKhiKhaiBoSungThuKhac.Text == "KCT")
                    {
                        txtSoTienThueTruocKhiKhaiBoSungThuKhac.Text = "0";
                    }
                    else
                    {
                        try
                        {
                            this.thuesuat = Convert.ToDouble(txtThueSuatTruocKhiKhaiBoSungThuKhac.Text);
#if KD_V4
                        txtSoTienThueTruocKhiKhaiBoSungThuKhac.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.SoThapPhan.TriGiaNT).Replace(".", ",");
#elif GC_V4
                            txtSoTienThueTruocKhiKhaiBoSungThuKhac.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.SoThapPhan.TGNT).Replace(".", ",");
#elif SXXK_V4
                        txtSoTienThueTruocKhiKhaiBoSungThuKhac.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.TriGiaNT).Replace(".", ",");
#endif
                        }
                        catch (Exception ex)
                        {
                            //ShowMessage("Nhập thuế suất trước khi khai bổ sung và thu khác", false);
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }

                    }
                }
                else
                {
                    txtSoTienThueTruocKhiKhaiBoSungThuKhac.Text = "0";
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtThueSuatSauKhiKhaiBoSungThuKhac_Leave(object sender, EventArgs e)
        {
            try
            {
                this.triGiaHD = Convert.ToDouble(txtTriGiaTinhThueSauKhiKhaiBoSungThuKhac.Value);
                if (txtThueSuatSauKhiKhaiBoSungThuKhac.Text != "0")
                {
                    if (txtThueSuatSauKhiKhaiBoSungThuKhac.Text.Contains("%"))
                    {
                        this.thuesuat = Convert.ToDouble(txtThueSuatSauKhiKhaiBoSungThuKhac.Text.Replace("%", ""));
#if KD_V4 
                    txtSoTienThueSauKhiKhaiBoSungThuKhac.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.SoThapPhan.TriGiaNT).Replace(".", ",");
#elif GC_V4
                        txtSoTienThueSauKhiKhaiBoSungThuKhac.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.SoThapPhan.TGNT).Replace(".", ",");
#elif SXXK_V4
                    txtSoTienThueSauKhiKhaiBoSungThuKhac.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.TriGiaNT).Replace(".", ",");
#endif
                    }
                    else if (txtThueSuatSauKhiKhaiBoSungThuKhac.Text == "KCT")
                    {
                        txtSoTienThueSauKhiKhaiBoSungThuKhac.Text = "0";
                    }
                    else
                    {
                        try
                        {
                            this.thuesuat = Convert.ToDouble(txtThueSuatSauKhiKhaiBoSungThuKhac.Text);
#if KD_V4
                        txtSoTienThueSauKhiKhaiBoSungThuKhac.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.SoThapPhan.TriGiaNT).Replace(".", ",");
#elif GC_V4
                            txtSoTienThueSauKhiKhaiBoSungThuKhac.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.SoThapPhan.TGNT).Replace(".", ",");
#elif SXXK_V4
                        txtSoTienThueSauKhiKhaiBoSungThuKhac.Text = Helpers.FormatNumeric(this.triGiaHD * thuesuat / 100, GlobalSettings.TriGiaNT).Replace(".", ",");
#endif
                        }
                        catch (Exception ex)
                        {
                            //ShowMessage("Nhập thuế suất sau khi khai bổ sung và thu khác", false);
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                }
                else
                {
                    txtSoTienThueSauKhiKhaiBoSungThuKhac.Text = "0";
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }

    }
}
