﻿namespace Company.Interface.GC
{
    partial class ReadExcelFormMMTB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadExcelFormMMTB));
            this.linkFileExcel = new System.Windows.Forms.LinkLabel();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnSelectFile = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.btnReadFile = new Janus.Windows.EditControls.UIButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtRowIndex = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtLuongConlai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSTTHH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHS = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChuHH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHDCT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtLuongTN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtLuongTX = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtLuongCT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSheet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.chkMutilHD = new Janus.Windows.EditControls.UICheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Size = new System.Drawing.Size(730, 308);
            // 
            // linkFileExcel
            // 
            this.linkFileExcel.AutoSize = true;
            this.linkFileExcel.BackColor = System.Drawing.Color.Transparent;
            this.linkFileExcel.Location = new System.Drawing.Point(8, 17);
            this.linkFileExcel.Name = "linkFileExcel";
            this.linkFileExcel.Size = new System.Drawing.Size(171, 13);
            this.linkFileExcel.TabIndex = 110;
            this.linkFileExcel.TabStop = true;
            this.linkFileExcel.Text = "File Excel báo cáo quyết toán mẫu";
            this.linkFileExcel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkFileExcel_LinkClicked);
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(11, 20);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(603, 21);
            this.txtFilePath.TabIndex = 0;
            this.txtFilePath.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectFile.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectFile.Image")));
            this.btnSelectFile.ImageIndex = 4;
            this.btnSelectFile.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSelectFile.Location = new System.Drawing.Point(626, 18);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(90, 23);
            this.btnSelectFile.TabIndex = 310;
            this.btnSelectFile.Text = "Chọn File";
            this.btnSelectFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtFilePath);
            this.uiGroupBox2.Controls.Add(this.btnSelectFile);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 215);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(730, 48);
            this.uiGroupBox2.TabIndex = 317;
            this.uiGroupBox2.Text = "Đường dẫn file Excel cần nhập";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.linkFileExcel);
            this.uiGroupBox4.Controls.Add(this.uiButton1);
            this.uiGroupBox4.Controls.Add(this.btnReadFile);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 263);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(730, 45);
            this.uiGroupBox4.TabIndex = 318;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiButton1
            // 
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Image = ((System.Drawing.Image)(resources.GetObject("uiButton1.Image")));
            this.uiButton1.ImageIndex = 4;
            this.uiButton1.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton1.Location = new System.Drawing.Point(332, 13);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(90, 23);
            this.uiButton1.TabIndex = 310;
            this.uiButton1.Text = "Đóng";
            this.uiButton1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnReadFile
            // 
            this.btnReadFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadFile.Image = ((System.Drawing.Image)(resources.GetObject("btnReadFile.Image")));
            this.btnReadFile.ImageIndex = 4;
            this.btnReadFile.ImageSize = new System.Drawing.Size(20, 20);
            this.btnReadFile.Location = new System.Drawing.Point(236, 13);
            this.btnReadFile.Name = "btnReadFile";
            this.btnReadFile.Size = new System.Drawing.Size(90, 23);
            this.btnReadFile.TabIndex = 310;
            this.btnReadFile.Text = "Đọc File";
            this.btnReadFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnReadFile.Click += new System.EventHandler(this.btnReadFile_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 80;
            this.label5.Text = "Tên Sheet";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(344, 162);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(96, 13);
            this.label21.TabIndex = 87;
            this.label21.Text = "Số HĐ chuyển tiếp";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(17, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 87;
            this.label7.Text = "Số HĐ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(135, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 105;
            this.label1.Text = "Tên hàng hóa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(239, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 106;
            this.label2.Text = "Mã hàng hóa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(345, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 103;
            this.label3.Text = "Mã HS";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(544, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 104;
            this.label4.Text = "Ghi chú";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(236, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 13);
            this.label10.TabIndex = 84;
            this.label10.Text = "Dòng bắt đầu";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(21, 160);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 13);
            this.label11.TabIndex = 104;
            this.label11.Text = "Lượng tạm nhập";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(141, 160);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 13);
            this.label15.TabIndex = 104;
            this.label15.Text = "Lượng tái xuất";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(441, 162);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 101;
            this.label17.Text = "Lượng còn lại";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(441, 112);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(27, 13);
            this.label22.TabIndex = 104;
            this.label22.Text = "ĐVT";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(242, 160);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 13);
            this.label16.TabIndex = 104;
            this.label16.Text = "Lượng chuyển tiếp";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(17, 113);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(25, 13);
            this.label14.TabIndex = 102;
            this.label14.Text = "STT";
            // 
            // txtRowIndex
            // 
            this.txtRowIndex.DecimalDigits = 0;
            this.txtRowIndex.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRowIndex.Location = new System.Drawing.Point(239, 37);
            this.txtRowIndex.MaxLength = 6;
            this.txtRowIndex.Name = "txtRowIndex";
            this.txtRowIndex.Size = new System.Drawing.Size(90, 21);
            this.txtRowIndex.TabIndex = 85;
            this.txtRowIndex.Text = "2";
            this.txtRowIndex.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.txtRowIndex.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.chkMutilHD);
            this.uiGroupBox3.Controls.Add(this.txtRowIndex);
            this.uiGroupBox3.Controls.Add(this.label14);
            this.uiGroupBox3.Controls.Add(this.label16);
            this.uiGroupBox3.Controls.Add(this.label22);
            this.uiGroupBox3.Controls.Add(this.label17);
            this.uiGroupBox3.Controls.Add(this.label15);
            this.uiGroupBox3.Controls.Add(this.label11);
            this.uiGroupBox3.Controls.Add(this.label10);
            this.uiGroupBox3.Controls.Add(this.label4);
            this.uiGroupBox3.Controls.Add(this.label3);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.txtSheet);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.label21);
            this.uiGroupBox3.Controls.Add(this.txtLuongCT);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.txtLuongTX);
            this.uiGroupBox3.Controls.Add(this.txtSoHD);
            this.uiGroupBox3.Controls.Add(this.txtLuongTN);
            this.uiGroupBox3.Controls.Add(this.txtSoHDCT);
            this.uiGroupBox3.Controls.Add(this.txtGhiChuHH);
            this.uiGroupBox3.Controls.Add(this.txtMaHangHoa);
            this.uiGroupBox3.Controls.Add(this.txtTenHangHoa);
            this.uiGroupBox3.Controls.Add(this.txtMaHS);
            this.uiGroupBox3.Controls.Add(this.txtSTTHH);
            this.uiGroupBox3.Controls.Add(this.txtLuongConlai);
            this.uiGroupBox3.Controls.Add(this.txtDVT);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(730, 215);
            this.uiGroupBox3.TabIndex = 316;
            this.uiGroupBox3.Text = "Thông số cấu hình";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtDVT
            // 
            this.txtDVT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVT.Location = new System.Drawing.Point(443, 131);
            this.txtDVT.MaxLength = 1;
            this.txtDVT.Name = "txtDVT";
            this.txtDVT.Size = new System.Drawing.Size(90, 21);
            this.txtDVT.TabIndex = 90;
            this.txtDVT.Text = "E";
            this.txtDVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtLuongConlai
            // 
            this.txtLuongConlai.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLuongConlai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongConlai.Location = new System.Drawing.Point(444, 178);
            this.txtLuongConlai.MaxLength = 1;
            this.txtLuongConlai.Name = "txtLuongConlai";
            this.txtLuongConlai.Size = new System.Drawing.Size(90, 21);
            this.txtLuongConlai.TabIndex = 90;
            this.txtLuongConlai.Text = "J";
            this.txtLuongConlai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSTTHH
            // 
            this.txtSTTHH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSTTHH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSTTHH.Location = new System.Drawing.Point(20, 129);
            this.txtSTTHH.MaxLength = 1;
            this.txtSTTHH.Name = "txtSTTHH";
            this.txtSTTHH.Size = new System.Drawing.Size(90, 21);
            this.txtSTTHH.TabIndex = 91;
            this.txtSTTHH.Text = "A";
            this.txtSTTHH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHS
            // 
            this.txtMaHS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHS.Location = new System.Drawing.Point(347, 131);
            this.txtMaHS.MaxLength = 1;
            this.txtMaHS.Name = "txtMaHS";
            this.txtMaHS.Size = new System.Drawing.Size(90, 21);
            this.txtMaHS.TabIndex = 98;
            this.txtMaHS.Text = "D";
            this.txtMaHS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenHangHoa
            // 
            this.txtTenHangHoa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangHoa.Location = new System.Drawing.Point(138, 131);
            this.txtTenHangHoa.MaxLength = 1;
            this.txtTenHangHoa.Name = "txtTenHangHoa";
            this.txtTenHangHoa.Size = new System.Drawing.Size(90, 21);
            this.txtTenHangHoa.TabIndex = 94;
            this.txtTenHangHoa.Text = "B";
            this.txtTenHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHangHoa
            // 
            this.txtMaHangHoa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangHoa.Location = new System.Drawing.Point(239, 131);
            this.txtMaHangHoa.MaxLength = 1;
            this.txtMaHangHoa.Name = "txtMaHangHoa";
            this.txtMaHangHoa.Size = new System.Drawing.Size(90, 21);
            this.txtMaHangHoa.TabIndex = 93;
            this.txtMaHangHoa.Text = "C";
            this.txtMaHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChuHH
            // 
            this.txtGhiChuHH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGhiChuHH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChuHH.Location = new System.Drawing.Point(544, 177);
            this.txtGhiChuHH.MaxLength = 1;
            this.txtGhiChuHH.Name = "txtGhiChuHH";
            this.txtGhiChuHH.Size = new System.Drawing.Size(90, 21);
            this.txtGhiChuHH.TabIndex = 95;
            this.txtGhiChuHH.Text = "K";
            this.txtGhiChuHH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoHDCT
            // 
            this.txtSoHDCT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoHDCT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHDCT.Location = new System.Drawing.Point(347, 178);
            this.txtSoHDCT.MaxLength = 1;
            this.txtSoHDCT.Name = "txtSoHDCT";
            this.txtSoHDCT.Size = new System.Drawing.Size(90, 21);
            this.txtSoHDCT.TabIndex = 92;
            this.txtSoHDCT.Text = "I";
            this.txtSoHDCT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtLuongTN
            // 
            this.txtLuongTN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLuongTN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongTN.Location = new System.Drawing.Point(21, 178);
            this.txtLuongTN.MaxLength = 1;
            this.txtLuongTN.Name = "txtLuongTN";
            this.txtLuongTN.Size = new System.Drawing.Size(90, 21);
            this.txtLuongTN.TabIndex = 95;
            this.txtLuongTN.Text = "F";
            this.txtLuongTN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoHD
            // 
            this.txtSoHD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHD.Location = new System.Drawing.Point(20, 82);
            this.txtSoHD.MaxLength = 1;
            this.txtSoHD.Name = "txtSoHD";
            this.txtSoHD.Size = new System.Drawing.Size(90, 21);
            this.txtSoHD.TabIndex = 92;
            this.txtSoHD.Text = "M";
            this.txtSoHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtLuongTX
            // 
            this.txtLuongTX.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLuongTX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongTX.Location = new System.Drawing.Point(138, 178);
            this.txtLuongTX.MaxLength = 1;
            this.txtLuongTX.Name = "txtLuongTX";
            this.txtLuongTX.Size = new System.Drawing.Size(90, 21);
            this.txtLuongTX.TabIndex = 95;
            this.txtLuongTX.Text = "H";
            this.txtLuongTX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtLuongCT
            // 
            this.txtLuongCT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLuongCT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuongCT.Location = new System.Drawing.Point(242, 178);
            this.txtLuongCT.MaxLength = 1;
            this.txtLuongCT.Name = "txtLuongCT";
            this.txtLuongCT.Size = new System.Drawing.Size(90, 21);
            this.txtLuongCT.TabIndex = 95;
            this.txtLuongCT.Text = "G";
            this.txtLuongCT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSheet
            // 
            this.txtSheet.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSheet.Location = new System.Drawing.Point(21, 37);
            this.txtSheet.Name = "txtSheet";
            this.txtSheet.Size = new System.Drawing.Size(184, 21);
            this.txtSheet.TabIndex = 81;
            this.txtSheet.Text = "Sheet1";
            this.txtSheet.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // chkMutilHD
            // 
            this.chkMutilHD.AutoSize = true;
            this.chkMutilHD.Location = new System.Drawing.Point(138, 85);
            this.chkMutilHD.Name = "chkMutilHD";
            this.chkMutilHD.Size = new System.Drawing.Size(123, 18);
            this.chkMutilHD.TabIndex = 107;
            this.chkMutilHD.Text = "  Nhập Excel nhiều HĐ";
            // 
            // ReadExcelFormMMTB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 308);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximumSize = new System.Drawing.Size(746, 347);
            this.MinimumSize = new System.Drawing.Size(746, 347);
            this.Name = "ReadExcelFormMMTB";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Đọc báo cáo quyết toán MMTB từ File Excel";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.LinkLabel linkFileExcel;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private Janus.Windows.EditControls.UIButton btnSelectFile;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIButton btnReadFile;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRowIndex;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSheet;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.GridEX.EditControls.EditBox txtLuongCT;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtLuongTX;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHD;
        private Janus.Windows.GridEX.EditControls.EditBox txtLuongTN;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHDCT;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuHH;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangHoa;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangHoa;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHS;
        private Janus.Windows.GridEX.EditControls.EditBox txtSTTHH;
        private Janus.Windows.GridEX.EditControls.EditBox txtLuongConlai;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVT;
        private Janus.Windows.EditControls.UICheckBox chkMutilHD;

    }
}