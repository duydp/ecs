using System;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using Company.BLL;
using Company.Interface.Report.SXXK;
using Company.Interface.Report;

namespace Company.Interface.SXXK
{
    public partial class NguyenPhuLieuMappingForm : BaseForm
    {
        public DataTable dtNguyenPhuLieu = new DataTable();
        public NguyenPhuLieuMappingForm()
        {
            InitializeComponent();
        }

        private void NguyenPhuLieuRegistedForm_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["MaNPLMoi"].EditType = EditType.MultiColumnCombo;

            BindHopDong();
            BindGrid();
        }

        private void BindHopDong()
        {
            cbHopDong.DataSource = new NguyenPhuLieu().GetSoHopDongGiaCong();
            cbHopDong.ValueMember = "ID";
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.SelectedIndex = cbHopDong.Items.Count - 1;
        }
        private void BindGrid()
        {
            this.dtNguyenPhuLieu = new NguyenPhuLieu().GetDanhSachNPL(cbHopDong.SelectedValue.ToString());
            dgList.DataSource = this.dtNguyenPhuLieu;
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        private void cbHopDong_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new NguyenPhuLieu().GetDanhSachNPLCuaHopDong((long)cbHopDong.SelectedValue);
            dgList.Tables[0].Columns["MaNPLMoi"].DropDown.DataSource = dt;
            BindGrid();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                new NguyenPhuLieu().InsertNPLMapping(this.dtNguyenPhuLieu, (long) cbHopDong.SelectedValue);
                ShowMessage("Lưu thành công.", false);
            }
            catch(Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
        }
        public bool CheckExits(string maNPL)
        {
            if (maNPL.Trim().Length == 0) return false;
            foreach (DataRow dr in this.dtNguyenPhuLieu.Rows)
            {
                if (dr["MaNPLMoi"].ToString().Trim().ToUpper() == maNPL.Trim().ToUpper()) return true;

            }
            return false;
        }
        private void dgList_UpdatingCell(object sender, UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "Ma")
            {
                NguyenPhuLieu NPL = new NguyenPhuLieu();
                NPL.Ma = e.Value.ToString();
                NPL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                NPL.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                if (!NPL.Load())
                {
                    ShowMessage("Mã NPL này không tồn tại.", false);
                    e.Cancel = true;
                }
                else
                {
                    dgList.CurrentRow.Cells["Ten"].Text =  NPL.Ten;
                    dgList.CurrentRow.Cells["Ten"].Value = NPL.Ten;
                    dgList.CurrentRow.Cells["MaHS"].Text = NPL.MaHS;
                    dgList.CurrentRow.Cells["MaHS"].Value = NPL.MaHS;
                    dgList.CurrentRow.Cells["DVT_ID"].Value = NPL.DVT_ID;
                    dgList.CurrentRow.Cells["DVT_ID"].Text = DonViTinh.GetName(NPL.DVT_ID);
                }
            }
        }



    }
}