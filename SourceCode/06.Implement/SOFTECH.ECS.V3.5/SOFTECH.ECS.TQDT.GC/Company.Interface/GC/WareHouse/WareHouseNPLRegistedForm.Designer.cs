﻿namespace Company.Interface.GC.WareHouse
{
    partial class WareHouseNPLRegistedForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grListNPL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference grListNPL_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WareHouseNPLRegistedForm));
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.grListNPL = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchNPL = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListNPL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Size = new System.Drawing.Size(912, 454);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox3.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(912, 454);
            this.uiGroupBox3.TabIndex = 8;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.grListNPL);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox8.Location = new System.Drawing.Point(3, 54);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(906, 397);
            this.uiGroupBox8.TabIndex = 5;
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // grListNPL
            // 
            this.grListNPL.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grListNPL.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grListNPL.ColumnAutoResize = true;
            grListNPL_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("grListNPL_DesignTimeLayout_Reference_0.Instance")));
            grListNPL_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            grListNPL_DesignTimeLayout_Reference_0});
            grListNPL_DesignTimeLayout.LayoutString = resources.GetString("grListNPL_DesignTimeLayout.LayoutString");
            this.grListNPL.DesignTimeLayout = grListNPL_DesignTimeLayout;
            this.grListNPL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListNPL.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grListNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListNPL.FrozenColumns = 5;
            this.grListNPL.GroupByBoxVisible = false;
            this.grListNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListNPL.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grListNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListNPL.Hierarchical = true;
            this.grListNPL.Location = new System.Drawing.Point(3, 8);
            this.grListNPL.Name = "grListNPL";
            this.grListNPL.RecordNavigator = true;
            this.grListNPL.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListNPL.Size = new System.Drawing.Size(900, 386);
            this.grListNPL.TabIndex = 85;
            this.grListNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grListNPL.VisualStyleManager = this.vsmMain;
            this.grListNPL.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListNPL_RowDoubleClick);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnSearchNPL);
            this.uiGroupBox6.Controls.Add(this.label1);
            this.uiGroupBox6.Controls.Add(this.txtNPL);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(906, 46);
            this.uiGroupBox6.TabIndex = 4;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchNPL
            // 
            this.btnSearchNPL.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchNPL.Image")));
            this.btnSearchNPL.Location = new System.Drawing.Point(399, 16);
            this.btnSearchNPL.Name = "btnSearchNPL";
            this.btnSearchNPL.Size = new System.Drawing.Size(75, 23);
            this.btnSearchNPL.TabIndex = 87;
            this.btnSearchNPL.Text = "Tìm kiếm";
            this.btnSearchNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchNPL.Click += new System.EventHandler(this.btnSearchNPL_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 86;
            this.label1.Text = "Mã NPL";
            // 
            // txtNPL
            // 
            this.txtNPL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtNPL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtNPL.BackColor = System.Drawing.Color.White;
            this.txtNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNPL.Location = new System.Drawing.Point(69, 17);
            this.txtNPL.Name = "txtNPL";
            this.txtNPL.Size = new System.Drawing.Size(315, 21);
            this.txtNPL.TabIndex = 85;
            this.txtNPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNPL.TextChanged += new System.EventHandler(this.btnSearchNPL_Click);
            // 
            // WareHouseNPLRegistedForm
            // 
            this.ClientSize = new System.Drawing.Size(912, 454);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "WareHouseNPLRegistedForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Danh sách Nguyên phụ liệu kho kế toán đã đăng ký";
            this.Load += new System.EventHandler(this.WareHouseNPLRegistedForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grListNPL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.GridEX.GridEX grListNPL;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton btnSearchNPL;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtNPL;
    }
}
