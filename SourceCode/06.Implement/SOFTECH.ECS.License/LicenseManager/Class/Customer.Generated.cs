using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Licenses_v2.Class
{
	public partial class Customer
	{
		#region Properties.
		
		public string ID { set; get; }
		public string Name { set; get; }
		public string ContactFirstName { set; get; }
		public string ContactLastName { set; get; }
		public string CompanyOrDepartment { set; get; }
		public string BillingAddress { set; get; }
		public string City { set; get; }
		public string StateOrProvince { set; get; }
		public string PostalCode { set; get; }
		public string ContactTitle { set; get; }
		public string PhoneNumber { set; get; }
		public string FaxNumber { set; get; }
		public string Email { set; get; }
		public string Notes { set; get; }
		public string MaDN { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static Customer Load(string id)
		{
			const string spName = "[dbo].[p_LICENSE_Customer_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, id);
			Customer entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new Customer();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetString(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Name"))) entity.Name = reader.GetString(reader.GetOrdinal("Name"));
				if (!reader.IsDBNull(reader.GetOrdinal("ContactFirstName"))) entity.ContactFirstName = reader.GetString(reader.GetOrdinal("ContactFirstName"));
				if (!reader.IsDBNull(reader.GetOrdinal("ContactLastName"))) entity.ContactLastName = reader.GetString(reader.GetOrdinal("ContactLastName"));
				if (!reader.IsDBNull(reader.GetOrdinal("CompanyOrDepartment"))) entity.CompanyOrDepartment = reader.GetString(reader.GetOrdinal("CompanyOrDepartment"));
				if (!reader.IsDBNull(reader.GetOrdinal("BillingAddress"))) entity.BillingAddress = reader.GetString(reader.GetOrdinal("BillingAddress"));
				if (!reader.IsDBNull(reader.GetOrdinal("City"))) entity.City = reader.GetString(reader.GetOrdinal("City"));
				if (!reader.IsDBNull(reader.GetOrdinal("StateOrProvince"))) entity.StateOrProvince = reader.GetString(reader.GetOrdinal("StateOrProvince"));
				if (!reader.IsDBNull(reader.GetOrdinal("PostalCode"))) entity.PostalCode = reader.GetString(reader.GetOrdinal("PostalCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("ContactTitle"))) entity.ContactTitle = reader.GetString(reader.GetOrdinal("ContactTitle"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhoneNumber"))) entity.PhoneNumber = reader.GetString(reader.GetOrdinal("PhoneNumber"));
				if (!reader.IsDBNull(reader.GetOrdinal("FaxNumber"))) entity.FaxNumber = reader.GetString(reader.GetOrdinal("FaxNumber"));
				if (!reader.IsDBNull(reader.GetOrdinal("Email"))) entity.Email = reader.GetString(reader.GetOrdinal("Email"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDN"))) entity.MaDN = reader.GetString(reader.GetOrdinal("MaDN"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<Customer> SelectCollectionAll()
		{
			List<Customer> collection = new List<Customer>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				Customer entity = new Customer();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetString(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Name"))) entity.Name = reader.GetString(reader.GetOrdinal("Name"));
				if (!reader.IsDBNull(reader.GetOrdinal("ContactFirstName"))) entity.ContactFirstName = reader.GetString(reader.GetOrdinal("ContactFirstName"));
				if (!reader.IsDBNull(reader.GetOrdinal("ContactLastName"))) entity.ContactLastName = reader.GetString(reader.GetOrdinal("ContactLastName"));
				if (!reader.IsDBNull(reader.GetOrdinal("CompanyOrDepartment"))) entity.CompanyOrDepartment = reader.GetString(reader.GetOrdinal("CompanyOrDepartment"));
				if (!reader.IsDBNull(reader.GetOrdinal("BillingAddress"))) entity.BillingAddress = reader.GetString(reader.GetOrdinal("BillingAddress"));
				if (!reader.IsDBNull(reader.GetOrdinal("City"))) entity.City = reader.GetString(reader.GetOrdinal("City"));
				if (!reader.IsDBNull(reader.GetOrdinal("StateOrProvince"))) entity.StateOrProvince = reader.GetString(reader.GetOrdinal("StateOrProvince"));
				if (!reader.IsDBNull(reader.GetOrdinal("PostalCode"))) entity.PostalCode = reader.GetString(reader.GetOrdinal("PostalCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("ContactTitle"))) entity.ContactTitle = reader.GetString(reader.GetOrdinal("ContactTitle"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhoneNumber"))) entity.PhoneNumber = reader.GetString(reader.GetOrdinal("PhoneNumber"));
				if (!reader.IsDBNull(reader.GetOrdinal("FaxNumber"))) entity.FaxNumber = reader.GetString(reader.GetOrdinal("FaxNumber"));
				if (!reader.IsDBNull(reader.GetOrdinal("Email"))) entity.Email = reader.GetString(reader.GetOrdinal("Email"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDN"))) entity.MaDN = reader.GetString(reader.GetOrdinal("MaDN"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<Customer> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<Customer> collection = new List<Customer>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				Customer entity = new Customer();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetString(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Name"))) entity.Name = reader.GetString(reader.GetOrdinal("Name"));
				if (!reader.IsDBNull(reader.GetOrdinal("ContactFirstName"))) entity.ContactFirstName = reader.GetString(reader.GetOrdinal("ContactFirstName"));
				if (!reader.IsDBNull(reader.GetOrdinal("ContactLastName"))) entity.ContactLastName = reader.GetString(reader.GetOrdinal("ContactLastName"));
				if (!reader.IsDBNull(reader.GetOrdinal("CompanyOrDepartment"))) entity.CompanyOrDepartment = reader.GetString(reader.GetOrdinal("CompanyOrDepartment"));
				if (!reader.IsDBNull(reader.GetOrdinal("BillingAddress"))) entity.BillingAddress = reader.GetString(reader.GetOrdinal("BillingAddress"));
				if (!reader.IsDBNull(reader.GetOrdinal("City"))) entity.City = reader.GetString(reader.GetOrdinal("City"));
				if (!reader.IsDBNull(reader.GetOrdinal("StateOrProvince"))) entity.StateOrProvince = reader.GetString(reader.GetOrdinal("StateOrProvince"));
				if (!reader.IsDBNull(reader.GetOrdinal("PostalCode"))) entity.PostalCode = reader.GetString(reader.GetOrdinal("PostalCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("ContactTitle"))) entity.ContactTitle = reader.GetString(reader.GetOrdinal("ContactTitle"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhoneNumber"))) entity.PhoneNumber = reader.GetString(reader.GetOrdinal("PhoneNumber"));
				if (!reader.IsDBNull(reader.GetOrdinal("FaxNumber"))) entity.FaxNumber = reader.GetString(reader.GetOrdinal("FaxNumber"));
				if (!reader.IsDBNull(reader.GetOrdinal("Email"))) entity.Email = reader.GetString(reader.GetOrdinal("Email"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDN"))) entity.MaDN = reader.GetString(reader.GetOrdinal("MaDN"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LICENSE_Customer_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LICENSE_Customer_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LICENSE_Customer_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LICENSE_Customer_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertCustomer(string name, string contactFirstName, string contactLastName, string companyOrDepartment, string billingAddress, string city, string stateOrProvince, string postalCode, string contactTitle, string phoneNumber, string faxNumber, string email, string notes, string maDN)
		{
			Customer entity = new Customer();	
			entity.Name = name;
			entity.ContactFirstName = contactFirstName;
			entity.ContactLastName = contactLastName;
			entity.CompanyOrDepartment = companyOrDepartment;
			entity.BillingAddress = billingAddress;
			entity.City = city;
			entity.StateOrProvince = stateOrProvince;
			entity.PostalCode = postalCode;
			entity.ContactTitle = contactTitle;
			entity.PhoneNumber = phoneNumber;
			entity.FaxNumber = faxNumber;
			entity.Email = email;
			entity.Notes = notes;
			entity.MaDN = maDN;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_LICENSE_Customer_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
			db.AddInParameter(dbCommand, "@Name", SqlDbType.NVarChar, Name);
			db.AddInParameter(dbCommand, "@ContactFirstName", SqlDbType.NVarChar, ContactFirstName);
			db.AddInParameter(dbCommand, "@ContactLastName", SqlDbType.NVarChar, ContactLastName);
			db.AddInParameter(dbCommand, "@CompanyOrDepartment", SqlDbType.NVarChar, CompanyOrDepartment);
			db.AddInParameter(dbCommand, "@BillingAddress", SqlDbType.NVarChar, BillingAddress);
			db.AddInParameter(dbCommand, "@City", SqlDbType.NVarChar, City);
			db.AddInParameter(dbCommand, "@StateOrProvince", SqlDbType.NVarChar, StateOrProvince);
			db.AddInParameter(dbCommand, "@PostalCode", SqlDbType.VarChar, PostalCode);
			db.AddInParameter(dbCommand, "@ContactTitle", SqlDbType.NVarChar, ContactTitle);
			db.AddInParameter(dbCommand, "@PhoneNumber", SqlDbType.VarChar, PhoneNumber);
			db.AddInParameter(dbCommand, "@FaxNumber", SqlDbType.VarChar, FaxNumber);
			db.AddInParameter(dbCommand, "@Email", SqlDbType.VarChar, Email);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@MaDN", SqlDbType.NVarChar, MaDN);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<Customer> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Customer item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateCustomer(string id, string name, string contactFirstName, string contactLastName, string companyOrDepartment, string billingAddress, string city, string stateOrProvince, string postalCode, string contactTitle, string phoneNumber, string faxNumber, string email, string notes, string maDN)
		{
			Customer entity = new Customer();			
			entity.ID = id;
			entity.Name = name;
			entity.ContactFirstName = contactFirstName;
			entity.ContactLastName = contactLastName;
			entity.CompanyOrDepartment = companyOrDepartment;
			entity.BillingAddress = billingAddress;
			entity.City = city;
			entity.StateOrProvince = stateOrProvince;
			entity.PostalCode = postalCode;
			entity.ContactTitle = contactTitle;
			entity.PhoneNumber = phoneNumber;
			entity.FaxNumber = faxNumber;
			entity.Email = email;
			entity.Notes = notes;
			entity.MaDN = maDN;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_LICENSE_Customer_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
			db.AddInParameter(dbCommand, "@Name", SqlDbType.NVarChar, Name);
			db.AddInParameter(dbCommand, "@ContactFirstName", SqlDbType.NVarChar, ContactFirstName);
			db.AddInParameter(dbCommand, "@ContactLastName", SqlDbType.NVarChar, ContactLastName);
			db.AddInParameter(dbCommand, "@CompanyOrDepartment", SqlDbType.NVarChar, CompanyOrDepartment);
			db.AddInParameter(dbCommand, "@BillingAddress", SqlDbType.NVarChar, BillingAddress);
			db.AddInParameter(dbCommand, "@City", SqlDbType.NVarChar, City);
			db.AddInParameter(dbCommand, "@StateOrProvince", SqlDbType.NVarChar, StateOrProvince);
			db.AddInParameter(dbCommand, "@PostalCode", SqlDbType.VarChar, PostalCode);
			db.AddInParameter(dbCommand, "@ContactTitle", SqlDbType.NVarChar, ContactTitle);
			db.AddInParameter(dbCommand, "@PhoneNumber", SqlDbType.VarChar, PhoneNumber);
			db.AddInParameter(dbCommand, "@FaxNumber", SqlDbType.VarChar, FaxNumber);
			db.AddInParameter(dbCommand, "@Email", SqlDbType.VarChar, Email);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@MaDN", SqlDbType.NVarChar, MaDN);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<Customer> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Customer item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateCustomer(string id, string name, string contactFirstName, string contactLastName, string companyOrDepartment, string billingAddress, string city, string stateOrProvince, string postalCode, string contactTitle, string phoneNumber, string faxNumber, string email, string notes, string maDN)
		{
			Customer entity = new Customer();			
			entity.ID = id;
			entity.Name = name;
			entity.ContactFirstName = contactFirstName;
			entity.ContactLastName = contactLastName;
			entity.CompanyOrDepartment = companyOrDepartment;
			entity.BillingAddress = billingAddress;
			entity.City = city;
			entity.StateOrProvince = stateOrProvince;
			entity.PostalCode = postalCode;
			entity.ContactTitle = contactTitle;
			entity.PhoneNumber = phoneNumber;
			entity.FaxNumber = faxNumber;
			entity.Email = email;
			entity.Notes = notes;
			entity.MaDN = maDN;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LICENSE_Customer_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
			db.AddInParameter(dbCommand, "@Name", SqlDbType.NVarChar, Name);
			db.AddInParameter(dbCommand, "@ContactFirstName", SqlDbType.NVarChar, ContactFirstName);
			db.AddInParameter(dbCommand, "@ContactLastName", SqlDbType.NVarChar, ContactLastName);
			db.AddInParameter(dbCommand, "@CompanyOrDepartment", SqlDbType.NVarChar, CompanyOrDepartment);
			db.AddInParameter(dbCommand, "@BillingAddress", SqlDbType.NVarChar, BillingAddress);
			db.AddInParameter(dbCommand, "@City", SqlDbType.NVarChar, City);
			db.AddInParameter(dbCommand, "@StateOrProvince", SqlDbType.NVarChar, StateOrProvince);
			db.AddInParameter(dbCommand, "@PostalCode", SqlDbType.VarChar, PostalCode);
			db.AddInParameter(dbCommand, "@ContactTitle", SqlDbType.NVarChar, ContactTitle);
			db.AddInParameter(dbCommand, "@PhoneNumber", SqlDbType.VarChar, PhoneNumber);
			db.AddInParameter(dbCommand, "@FaxNumber", SqlDbType.VarChar, FaxNumber);
			db.AddInParameter(dbCommand, "@Email", SqlDbType.VarChar, Email);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@MaDN", SqlDbType.NVarChar, MaDN);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<Customer> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Customer item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteCustomer(string id)
		{
			Customer entity = new Customer();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LICENSE_Customer_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_LICENSE_Customer_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<Customer> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Customer item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}