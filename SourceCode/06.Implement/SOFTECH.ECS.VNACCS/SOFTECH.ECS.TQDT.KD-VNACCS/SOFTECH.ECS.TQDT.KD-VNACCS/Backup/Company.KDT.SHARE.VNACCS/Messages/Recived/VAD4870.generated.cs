using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAD4870 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute K01 { get; set; }
public PropertiesAttribute K02 { get; set; }
public PropertiesAttribute K04 { get; set; }
public PropertiesAttribute ADA { get; set; }
public PropertiesAttribute ADB { get; set; }
public PropertiesAttribute K06 { get; set; }
public PropertiesAttribute K07 { get; set; }
public PropertiesAttribute K08 { get; set; }
public PropertiesAttribute K09 { get; set; }
public PropertiesAttribute K10 { get; set; }
public PropertiesAttribute K11 { get; set; }
public PropertiesAttribute K12 { get; set; }
public PropertiesAttribute K13 { get; set; }
public PropertiesAttribute K14 { get; set; }
public PropertiesAttribute K15 { get; set; }
public PropertiesAttribute K16 { get; set; }
public PropertiesAttribute K20 { get; set; }
public PropertiesAttribute K21 { get; set; }
public PropertiesAttribute K25 { get; set; }
public PropertiesAttribute K26 { get; set; }
public PropertiesAttribute K29 { get; set; }
public PropertiesAttribute K30 { get; set; }
public PropertiesAttribute A1 { get; set; }
public PropertiesAttribute A2 { get; set; }
public PropertiesAttribute A3 { get; set; }
public PropertiesAttribute A4 { get; set; }
public PropertiesAttribute A5 { get; set; }
public PropertiesAttribute K31 { get; set; }
public PropertiesAttribute K37 { get; set; }
public PropertiesAttribute K38 { get; set; }
public PropertiesAttribute K39 { get; set; }
public PropertiesAttribute K40 { get; set; }
public PropertiesAttribute K41 { get; set; }
public PropertiesAttribute K42 { get; set; }
public PropertiesAttribute K43 { get; set; }
public PropertiesAttribute K44 { get; set; }
public PropertiesAttribute K45 { get; set; }
public PropertiesAttribute K46 { get; set; }
public PropertiesAttribute K47 { get; set; }
public PropertiesAttribute K50 { get; set; }
public PropertiesAttribute K51 { get; set; }
public PropertiesAttribute K52 { get; set; }
public PropertiesAttribute K53 { get; set; }

public VAD4870()
        {
K01 = new PropertiesAttribute(114, typeof(string));
K02 = new PropertiesAttribute(12, typeof(int));
K04 = new PropertiesAttribute(3, typeof(string));
ADA = new PropertiesAttribute(1, typeof(string));
ADB = new PropertiesAttribute(1, typeof(string));
K06 = new PropertiesAttribute(5, typeof(string));
K07 = new PropertiesAttribute(6, typeof(string));
K08 = new PropertiesAttribute(2, typeof(string));
K09 = new PropertiesAttribute(35, typeof(string));
K10 = new PropertiesAttribute(20, typeof(string));
K11 = new PropertiesAttribute(7, typeof(string));
K12 = new PropertiesAttribute(20, typeof(string));
K13 = new PropertiesAttribute(8, typeof(int));
K14 = new PropertiesAttribute(3, typeof(string));
K15 = new PropertiesAttribute(10, typeof(int));
K16 = new PropertiesAttribute(3, typeof(string));
K20 = new PropertiesAttribute(9, typeof(string));
K21 = new PropertiesAttribute(35, typeof(string));
K25 = new PropertiesAttribute(13, typeof(string));
K26 = new PropertiesAttribute(300, typeof(string));
K29 = new PropertiesAttribute(140, typeof(string));
K30 = new PropertiesAttribute(3, typeof(int));
A1 = new PropertiesAttribute(12, typeof(string));
A2 = new PropertiesAttribute(12, typeof(string));
A3 = new PropertiesAttribute(12, typeof(string));
A4 = new PropertiesAttribute(12, typeof(string));
A5 = new PropertiesAttribute(12, typeof(string));
K31 = new PropertiesAttribute(1, typeof(string));
K37 = new PropertiesAttribute(5, typeof(string));
K38 = new PropertiesAttribute(20, typeof(string));
K39 = new PropertiesAttribute(1, typeof(string));
K40 = new PropertiesAttribute(42, typeof(string));
K41 = new PropertiesAttribute(3, typeof(string));
K42 = new PropertiesAttribute(1, typeof(string));
K43 = new PropertiesAttribute(114, typeof(string));
K44 = new PropertiesAttribute(1, typeof(string));
K45 = new PropertiesAttribute(8, typeof(string));
K46 = new PropertiesAttribute(7, typeof(string));
K47 = new PropertiesAttribute(20, typeof(string));
K50 = new PropertiesAttribute(840, typeof(string));
K51 = new PropertiesAttribute(1, typeof(string));
K52 = new PropertiesAttribute(96, typeof(string));
K53 = new PropertiesAttribute(1, typeof(string));
TongSoByte = 2057;
}

}public partial class EnumGroupID
    {
}

}
