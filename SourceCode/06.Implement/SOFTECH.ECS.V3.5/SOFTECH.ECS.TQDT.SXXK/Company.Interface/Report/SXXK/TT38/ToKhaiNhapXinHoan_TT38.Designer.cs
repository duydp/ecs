﻿namespace Company.Interface.Report.SXXK.TT38
{
    partial class ToKhaiNhapXinHoan_TT38
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout gridTKNK_Hoan_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiNhapXinHoan_TT38));
            Janus.Windows.GridEX.GridEXLayout gridTKNK_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.numTKNKH = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_FindTKKH = new Janus.Windows.EditControls.UIButton();
            this.gridTKNK_Hoan = new Janus.Windows.GridEX.GridEX();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.numTKNK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnFindTKNK = new Janus.Windows.EditControls.UIButton();
            this.gridTKNK = new Janus.Windows.GridEX.GridEX();
            this.btnExport = new Janus.Windows.EditControls.UIButton();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTKNK_Hoan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTKNK)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.btnExport);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.btnAdd);
            this.grbMain.Controls.Add(this.btnDelete);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Controls.Add(this.label4);
            this.grbMain.Size = new System.Drawing.Size(784, 427);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAdd.Icon")));
            this.btnAdd.Location = new System.Drawing.Point(371, 91);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(26, 23);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.VisualStyleManager = this.vsmMain;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDelete.Icon")));
            this.btnDelete.Location = new System.Drawing.Point(371, 120);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(26, 23);
            this.btnDelete.TabIndex = 10;
            this.btnDelete.VisualStyleManager = this.vsmMain;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.numTKNKH);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.btn_FindTKKH);
            this.uiGroupBox2.Controls.Add(this.gridTKNK_Hoan);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(426, 33);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(343, 382);
            this.uiGroupBox2.TabIndex = 8;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // numTKNKH
            // 
            this.numTKNKH.Location = new System.Drawing.Point(128, 12);
            this.numTKNKH.Name = "numTKNKH";
            this.numTKNKH.Size = new System.Drawing.Size(100, 21);
            this.numTKNKH.TabIndex = 1;
            this.numTKNKH.Text = "0";
            this.numTKNKH.Value = 0;
            this.numTKNKH.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.numTKNKH.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tìm đến tờ khai số:";
            // 
            // btn_FindTKKH
            // 
            this.btn_FindTKKH.Icon = ((System.Drawing.Icon)(resources.GetObject("btn_FindTKKH.Icon")));
            this.btn_FindTKKH.Location = new System.Drawing.Point(234, 11);
            this.btn_FindTKKH.Name = "btn_FindTKKH";
            this.btn_FindTKKH.Size = new System.Drawing.Size(89, 23);
            this.btn_FindTKKH.TabIndex = 2;
            this.btn_FindTKKH.Text = "Tìm";
            this.btn_FindTKKH.VisualStyleManager = this.vsmMain;
            // 
            // gridTKNK_Hoan
            // 
            this.gridTKNK_Hoan.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridTKNK_Hoan.AutomaticSort = false;
            this.gridTKNK_Hoan.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.gridTKNK_Hoan.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.gridTKNK_Hoan.ColumnAutoResize = true;
            gridTKNK_Hoan_DesignTimeLayout.LayoutString = resources.GetString("gridTKNK_Hoan_DesignTimeLayout.LayoutString");
            this.gridTKNK_Hoan.DesignTimeLayout = gridTKNK_Hoan_DesignTimeLayout;
            this.gridTKNK_Hoan.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridTKNK_Hoan.GroupByBoxVisible = false;
            this.gridTKNK_Hoan.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridTKNK_Hoan.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridTKNK_Hoan.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridTKNK_Hoan.Location = new System.Drawing.Point(2, 38);
            this.gridTKNK_Hoan.Name = "gridTKNK_Hoan";
            this.gridTKNK_Hoan.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridTKNK_Hoan.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridTKNK_Hoan.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridTKNK_Hoan.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridTKNK_Hoan.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.gridTKNK_Hoan.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.gridTKNK_Hoan.Size = new System.Drawing.Size(340, 343);
            this.gridTKNK_Hoan.TabIndex = 3;
            this.gridTKNK_Hoan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.gridTKNK_Hoan.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(432, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(212, 14);
            this.label5.TabIndex = 11;
            this.label5.Text = "Danh Sách Tờ Khai Nhập Đã Chọn";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(18, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(297, 14);
            this.label4.TabIndex = 7;
            this.label4.Text = "Danh Sách Tờ Khai Nhập Đưa Vào Thanh Khoản";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.numTKNK);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.btnFindTKNK);
            this.uiGroupBox1.Controls.Add(this.gridTKNK);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 33);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(343, 382);
            this.uiGroupBox1.TabIndex = 12;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // numTKNK
            // 
            this.numTKNK.Location = new System.Drawing.Point(128, 12);
            this.numTKNK.Name = "numTKNK";
            this.numTKNK.Size = new System.Drawing.Size(100, 21);
            this.numTKNK.TabIndex = 1;
            this.numTKNK.Text = "0";
            this.numTKNK.Value = 0;
            this.numTKNK.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.numTKNK.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tìm đến tờ khai số:";
            // 
            // btnFindTKNK
            // 
            this.btnFindTKNK.Icon = ((System.Drawing.Icon)(resources.GetObject("btnFindTKNK.Icon")));
            this.btnFindTKNK.Location = new System.Drawing.Point(234, 11);
            this.btnFindTKNK.Name = "btnFindTKNK";
            this.btnFindTKNK.Size = new System.Drawing.Size(89, 23);
            this.btnFindTKNK.TabIndex = 2;
            this.btnFindTKNK.Text = "Tìm";
            this.btnFindTKNK.VisualStyleManager = this.vsmMain;
            this.btnFindTKNK.Click += new System.EventHandler(this.btnFindTKNK_Click);
            // 
            // gridTKNK
            // 
            this.gridTKNK.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gridTKNK.AutomaticSort = false;
            this.gridTKNK.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.gridTKNK.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.gridTKNK.ColumnAutoResize = true;
            gridTKNK_DesignTimeLayout.LayoutString = resources.GetString("gridTKNK_DesignTimeLayout.LayoutString");
            this.gridTKNK.DesignTimeLayout = gridTKNK_DesignTimeLayout;
            this.gridTKNK.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridTKNK.GroupByBoxVisible = false;
            this.gridTKNK.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridTKNK.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridTKNK.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridTKNK.Location = new System.Drawing.Point(2, 38);
            this.gridTKNK.Name = "gridTKNK";
            this.gridTKNK.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.gridTKNK.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridTKNK.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gridTKNK.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridTKNK.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.gridTKNK.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.gridTKNK.Size = new System.Drawing.Size(340, 343);
            this.gridTKNK.TabIndex = 3;
            this.gridTKNK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.gridTKNK.VisualStyleManager = this.vsmMain;
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(360, 391);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(59, 23);
            this.btnExport.TabIndex = 13;
            this.btnExport.Text = "Xuất BC";
            this.btnExport.VisualStyleManager = this.vsmMain;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(361, 362);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(59, 23);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ToKhaiNhapXinHoan_TT38
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 427);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ToKhaiNhapXinHoan_TT38";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Bảng kê tờ khai nhập khẩu xin hoàn";
            this.Load += new System.EventHandler(this.ToKhaiNhapXinHoan_TT38_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTKNK_Hoan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTKNK)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numTKNKH;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIButton btn_FindTKKH;
        private Janus.Windows.GridEX.GridEX gridTKNK_Hoan;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numTKNK;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton btnFindTKNK;
        private Janus.Windows.GridEX.GridEX gridTKNK;
        private Janus.Windows.EditControls.UIButton btnExport;
        private Janus.Windows.EditControls.UIButton btnSave;
    }
}