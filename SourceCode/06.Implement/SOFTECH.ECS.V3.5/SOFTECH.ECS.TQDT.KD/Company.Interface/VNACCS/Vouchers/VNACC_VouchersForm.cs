﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components;
using System.IO;
using Janus.Windows.GridEX;
#if KD_V4
using Company.KD.BLL.KDT.SXXK;
using Company.KD.BLL.DataTransferObjectMapper;
#endif
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.KDT.SHARE.Components.Messages.Send;
using System.Diagnostics;
using iTextSharp.text.pdf;
using System.Security.Cryptography.X509Certificates;
using SignRemote;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Threading;
using Company.KDT.SHARE.Components.Messages.SmartCA;
#if SXXK_V4
using Company.BLL.KDT.SXXK;
using Company.BLL.DataTransferObjectMapper;
using System.Diagnostics;
#endif
#if GC_V4
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.DataTransferObjectMapper;
using System.Diagnostics;
using iTextSharp.text.pdf;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using SignRemote;
using Company.KDT.SHARE.QuanLyChungTu;
#endif

namespace Company.Interface.VNACCS.Vouchers
{
    public partial class VNACC_VouchersForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_License license = new KDT_VNACCS_License();
        public KDT_VNACCS_License_Detail licenseDetail = new KDT_VNACCS_License_Detail();
        public KDT_VNACCS_ContractDocument contractDocument = new KDT_VNACCS_ContractDocument();
        public KDT_VNACCS_ContractDocument_Detail contractDocumentDetail = new KDT_VNACCS_ContractDocument_Detail();
        public KDT_VNACCS_CommercialInvoice commercialInvoice = new KDT_VNACCS_CommercialInvoice();
        public KDT_VNACCS_CommercialInvoice_Detail commercialInvoiceDetail = new KDT_VNACCS_CommercialInvoice_Detail();
        public KDT_VNACCS_CertificateOfOrigin certificateOfOrigin = new KDT_VNACCS_CertificateOfOrigin();
        public KDT_VNACCS_CertificateOfOrigin_Detail certificateOfOriginDetail = new KDT_VNACCS_CertificateOfOrigin_Detail();
        public KDT_VNACCS_BillOfLading billOfLading = new KDT_VNACCS_BillOfLading();
        public KDT_VNACCS_BillOfLading_Detail billOfLadingDetail = new KDT_VNACCS_BillOfLading_Detail();
        public KDT_VNACCS_Container_Detail container = new KDT_VNACCS_Container_Detail();
        public KDT_VNACCS_AdditionalDocument additionalDocument = new KDT_VNACCS_AdditionalDocument();
        public KDT_VNACCS_AdditionalDocument_Detail additionalDocumentDetail = new KDT_VNACCS_AdditionalDocument_Detail();
        public KDT_VNACCS_OverTime overTime = new KDT_VNACCS_OverTime();
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        public KDT_VNACC_ToKhaiMauDich_KhaiBoSung TKBoSung = new KDT_VNACC_ToKhaiMauDich_KhaiBoSung();
        public System.IO.FileInfo fin;
        public System.IO.FileStream fs;
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        string filebase64 = "";
        long filesize = 0;
        public long size;
        public byte[] data;
        public byte[] dataSigned;
        public IntPtr parentFormHandler;
        public string FileName = "";
        public bool isAddGiayPhep = true;
        public bool isAddHopDong = true;
        public bool isAddHoaDon = true;
        public bool isAddCO = true;
        public bool isAddVanDon = true;
        public bool isAddContainer = true;
        public bool isAddCTkhac = true;
        public bool isAddOverTime = true;
        public MSG_FILE_OUTBOX MSG_FILE_OUTBOX;
        public SignRemote.MessageFile MessageFile;
        bool isSignSuccess = false;
        public String FormType = "TKMD";

        public VNACC_VouchersForm()
        {
            InitializeComponent();
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend":
                    SendVNACCSAll("Send");
                    break;
                case "cmdSave":
                    SaveAll();
                    break;
                case "cmdFeedback":
                    FeedbackAll();
                    break;
                case "cmdResult":
                    ResultAll();
                    break;
                case "cmdEdit":
                    SendVNACCSAll("Edit");
                    break;
                case "cmdCancel":
                    SendVNACCSAll("Cancel");
                    break;
                case "cmdUpdateGuidString":
                    UpdateGUIDSTR();
                    break;
                default:
                    break;
            }
        }
        private void SetCommandStatusAll()
        {
            switch (tbCtrVouchers.SelectedTab.Name)
            {
                case "tpGiayPhep":
                    SetCommandStatusLicense();
                    break;
                case "tpHopDong":
                    SetCommandStatusContractDocument();
                    break;
                case "tpHoaDon":
                    SetCommandStatusCommercialInvoice();
                    break;
                case "tpCO":
                    SetCommandStatusCertificateOfOrigin();
                    break;
                case "tpVanDon":
                    SetCommandStatusBillOfLading();
                    break;
                case "tpContainer":
                    SetCommandStatusContainer();
                    break;
                case "tpCTKhac":
                    SetCommandStatusAdditionalDocumentn();
                    break;
                case "tpLamNgoaiGio":
                    SetCommandStatusOverTime();
                    break;
                default:
                    break;
            }


        }
        private void SetCommandStatusLicense()
        {
            if (license.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTNGiayPhep.Text = license.SoTN.ToString();
                clcNgayTNGiayPhep.Value = license.NgayTN;
                this.OpenType = OpenFormType.View;
            }
            else if (license.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || license.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                clcNgayTNGiayPhep.Value = DateTime.Now;
                this.OpenType = OpenFormType.Edit;
            }
            else if (license.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || license.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                this.OpenType = OpenFormType.View;
            }
            else if (license.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTNGiayPhep.Text = license.SoTN.ToString();
                clcNgayTNGiayPhep.Value = license.NgayTN;
                this.OpenType = OpenFormType.View;
            }
        }
        private void SetCommandStatusContractDocument()
        {
            if (contractDocument.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTNHopDong.Text = contractDocument.SoTN.ToString();
                clcNgayTNHopDong.Value = contractDocument.NgayTN;
                this.OpenType = OpenFormType.View;
            }
            else if (contractDocument.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || contractDocument.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                clcNgayTNHopDong.Value = DateTime.Now;
                this.OpenType = OpenFormType.Edit;
            }
            else if (contractDocument.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || contractDocument.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                this.OpenType = OpenFormType.View;
            }
            else if (contractDocument.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTNHopDong.Text = contractDocument.SoTN.ToString();
                clcNgayTNHopDong.Value = contractDocument.NgayTN;
                this.OpenType = OpenFormType.View;
            }
        }
        private void SetCommandStatusCommercialInvoice()
        {
            if (commercialInvoice.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTNHoaDon.Text = commercialInvoice.SoTN.ToString();
                clcNgayTNHoaDon.Value = commercialInvoice.NgayTN;
                this.OpenType = OpenFormType.View;
            }
            else if (commercialInvoice.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || commercialInvoice.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                clcNgayTNHoaDon.Value = DateTime.Now;
                this.OpenType = OpenFormType.Edit;
            }
            else if (commercialInvoice.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || commercialInvoice.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                this.OpenType = OpenFormType.View;
            }
            else if (commercialInvoice.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTNHoaDon.Text = commercialInvoice.SoTN.ToString();
                clcNgayTNHoaDon.Value = commercialInvoice.NgayTN;
                this.OpenType = OpenFormType.View;
            }
        }
        private void SetCommandStatusCertificateOfOrigin()
        {
            if (certificateOfOrigin.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhanCO.Text = certificateOfOrigin.SoTN.ToString();
                clcNgayTiepNhanCO.Value = certificateOfOrigin.NgayTN;
                this.OpenType = OpenFormType.View;
            }
            else if (certificateOfOrigin.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || certificateOfOrigin.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                clcNgayTiepNhanCO.Value = DateTime.Now;
                this.OpenType = OpenFormType.Edit;
            }
            else if (certificateOfOrigin.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || certificateOfOrigin.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                this.OpenType = OpenFormType.View;
            }
            else if (certificateOfOrigin.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhanCO.Text = certificateOfOrigin.SoTN.ToString();
                clcNgayTiepNhanCO.Value = certificateOfOrigin.NgayTN;
                this.OpenType = OpenFormType.View;
            }
        }
        private void SetCommandStatusBillOfLading()
        {
            if (billOfLading.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhanVanDon.Text = billOfLading.SoTN.ToString();
                clcNgayTNVanDon.Value = billOfLading.NgayTN;
                this.OpenType = OpenFormType.View;
            }
            else if (billOfLading.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || billOfLading.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                clcNgayTNVanDon.Value = DateTime.Now;
                this.OpenType = OpenFormType.Edit;
            }
            else if (billOfLading.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || billOfLading.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                this.OpenType = OpenFormType.View;
            }
            else if (billOfLading.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhanVanDon.Text = billOfLading.SoTN.ToString();
                clcNgayTNVanDon.Value = billOfLading.NgayTN;
                this.OpenType = OpenFormType.View;
            }
        }
        private void SetCommandStatusContainer()
        {
            if (container.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTNContainer.Text = container.SoTN.ToString();
                clcNgayTNContainer.Value = container.NgayTN;
                this.OpenType = OpenFormType.View;
            }
            else if (container.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || container.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                clcNgayTNContainer.Value = DateTime.Now;
                this.OpenType = OpenFormType.Edit;
            }
            else if (container.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || container.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                this.OpenType = OpenFormType.View;
            }
            else if (container.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTNContainer.Text = container.SoTN.ToString();
                clcNgayTNContainer.Value = container.NgayTN;
                this.OpenType = OpenFormType.View;
            }
        }
        private void SetCommandStatusAdditionalDocumentn()
        {
            if (additionalDocument.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTNCTKhac.Text = additionalDocument.SoTN.ToString();
                clcNgayTNCTKhac.Value = additionalDocument.NgayTN;
                this.OpenType = OpenFormType.View;
            }
            else if (additionalDocument.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || additionalDocument.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                clcNgayTNCTKhac.Value = DateTime.Now;
                this.OpenType = OpenFormType.Edit;
            }
            else if (additionalDocument.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || additionalDocument.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                this.OpenType = OpenFormType.View;
            }
            else if (additionalDocument.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTNCTKhac.Text = additionalDocument.SoTN.ToString();
                clcNgayTNCTKhac.Value = additionalDocument.NgayTN;
                this.OpenType = OpenFormType.View;
            }
        }
        private void SetCommandStatusOverTime()
        {
            if (overTime.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTNDKLNG.Text = overTime.SoTN.ToString();
                clcNgayTNDKLNG.Value = overTime.NgayTN;
                this.OpenType = OpenFormType.View;
            }
            else if (overTime.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || overTime.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                clcNgayTNDKLNG.Value = DateTime.Now;
                this.OpenType = OpenFormType.Edit;
            }
            else if (overTime.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || overTime.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                this.OpenType = OpenFormType.View;
            }
            else if (overTime.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTNDKLNG.Text = overTime.SoTN.ToString();
                clcNgayTNDKLNG.Value = overTime.NgayTN;
                this.OpenType = OpenFormType.View;
            }
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                switch (tbCtrVouchers.SelectedTab.Name)
                {
                    case "tpGiayPhep":
                        frmUpdateGUIDSTR tpGiayPhep = new frmUpdateGUIDSTR("t_KDT_VNACCS_License", "", Convert.ToInt32(license.ID));
                        tpGiayPhep.ShowDialog(this);
                        break;
                    case "tpHopDong":
                        frmUpdateGUIDSTR tpHopDong = new frmUpdateGUIDSTR("t_KDT_VNACCS_ContractDocument", "", Convert.ToInt32(contractDocument.ID));
                        tpHopDong.ShowDialog(this);
                        break;
                    case "tpHoaDon":
                        frmUpdateGUIDSTR tpHoaDon = new frmUpdateGUIDSTR("t_KDT_VNACCS_CommercialInvoice", "", Convert.ToInt32(commercialInvoice.ID));
                        tpHoaDon.ShowDialog(this);
                        break;
                    case "tpCO":
                        frmUpdateGUIDSTR tpCO = new frmUpdateGUIDSTR("t_KDT_VNACCS_CertificateOfOrigin", "", Convert.ToInt32(certificateOfOrigin.ID));
                        tpCO.ShowDialog(this);
                        break;
                    case "tpVanDon":
                        frmUpdateGUIDSTR tpVanDon = new frmUpdateGUIDSTR("t_KDT_VNACCS_BillOfLading", "", Convert.ToInt32(billOfLading.ID));
                        tpVanDon.ShowDialog(this);
                        break;
                    case "tpContainer":
                        frmUpdateGUIDSTR tpContainer = new frmUpdateGUIDSTR("t_KDT_VNACCS_Container_Detail", "", Convert.ToInt32(container.ID));
                        tpContainer.ShowDialog(this);
                        break;
                    case "tpCTKhac":
                        frmUpdateGUIDSTR tpCTKhac = new frmUpdateGUIDSTR("t_KDT_VNACCS_AdditionalDocument", "", Convert.ToInt32(additionalDocument.ID));
                        tpCTKhac.ShowDialog(this);
                        break;
                    case "tpLamNgoaiGio":
                        frmUpdateGUIDSTR tpLamNgoaiGio = new frmUpdateGUIDSTR("t_KDT_VNACCS_OverTime", "", Convert.ToInt32(overTime.ID));
                        tpLamNgoaiGio.ShowDialog(this);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        //Giấy phép
        public void BindDataLicense()
        {
            try
            {
                cbbLoaiGiayPhep.DataSource = VNACC_Category_Common.SelectDynamic("ReferenceDB='A528'", "").Tables[0];
                cbbLoaiGiayPhep.DisplayMember = "Code";
                cbbLoaiGiayPhep.ValueMember = "Code";
                if (FormType == "TKMD")
                {
                    grListGiayPhep.Refresh();
                    txtMaHQGiayPhep.Text = TKMD.CoQuanHaiQuan;
                    grListGiayPhep.DataSource = KDT_VNACCS_License.SelectDynamicBy_TKMD("TKMD_ID =" + TKMD.ID, "").Tables[0];
                }
                else
                {
                    grListGiayPhep.Refresh();
                    txtMaHQGiayPhep.Text = TKBoSung.CoQuanHaiQuan;
                    grListGiayPhep.DataSource = KDT_VNACCS_License.SelectDynamicBy_TKMD("TKMD_ID =" + TKBoSung.ID, "").Tables[0];
                }
                grListGiayPhep.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }
        //Hợp đồng
        public void BindDataContractDocument()
        {
            try
            {
                if (FormType == "TKMD")
                {
                    grListHopDong.Refresh();
                    txtMaHQHopDong.Text = TKMD.CoQuanHaiQuan;
                    grListHopDong.DataSource = KDT_VNACCS_ContractDocument.SelectDynamicBy_TKMD("TKMD_ID =" + TKMD.ID, "").Tables[0];
                    grListHopDong.Refetch();
                }
                else
                {
                    grListHopDong.Refresh();
                    txtMaHQHopDong.Text = TKBoSung.CoQuanHaiQuan;
                    grListHopDong.DataSource = KDT_VNACCS_ContractDocument.SelectDynamicBy_TKMD("TKMD_ID =" + TKBoSung.ID, "").Tables[0];
                    grListHopDong.Refetch();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        //Hóa đơn
        public void BindDataCommercialInvoice()
        {
            try
            {
                if (FormType == "TKMD")
                {
                    grListHoaDon.Refresh();
                    txtMaHQHoaDon.Text = TKMD.CoQuanHaiQuan;
                    grListHoaDon.DataSource = KDT_VNACCS_CommercialInvoice.SelectDynamicBy_TKMD("TKMD_ID =" + TKMD.ID, "").Tables[0];
                    grListHoaDon.Refetch();
                }
                else
                {
                    grListHoaDon.Refresh();
                    txtMaHQHoaDon.Text = TKBoSung.CoQuanHaiQuan;
                    grListHoaDon.DataSource = KDT_VNACCS_CommercialInvoice.SelectDynamicBy_TKMD("TKMD_ID =" + TKBoSung.ID, "").Tables[0];
                    grListHoaDon.Refetch();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        // CO
        public void BindDataCertificateOfOrigin()
        {
            try
            {
                if (FormType == "TKMD")
                {
                    grListCO.Refresh();
                    txtMaHQCO.Text = TKMD.CoQuanHaiQuan;
                    grListCO.DataSource = KDT_VNACCS_CertificateOfOrigin.SelectDynamicBy_TKMD("TKMD_ID =" + TKMD.ID, "").Tables[0];
                    grListCO.Refetch();
                }
                else
                {
                    grListCO.Refresh();
                    txtMaHQCO.Text = TKBoSung.CoQuanHaiQuan;
                    grListCO.DataSource = KDT_VNACCS_CertificateOfOrigin.SelectDynamicBy_TKMD("TKMD_ID =" + TKBoSung.ID, "").Tables[0];
                    grListCO.Refetch();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        // Vận đơn
        public void BindDataBillOfLading()
        {
            try
            {
                if (FormType == "TKMD")
                {
                    grListVanDon.Refresh();
                    txtMaHQVanDon.Text = TKMD.CoQuanHaiQuan;
                    grListVanDon.DataSource = KDT_VNACCS_BillOfLading.SelectDynamicBy_TKMD("TKMD_ID =" + TKMD.ID, "").Tables[0];
                    grListVanDon.Refetch();
                }
                else
                {
                    grListVanDon.Refresh();
                    txtMaHQVanDon.Text = TKBoSung.CoQuanHaiQuan;
                    grListVanDon.DataSource = KDT_VNACCS_BillOfLading.SelectDynamicBy_TKMD("TKMD_ID =" + TKBoSung.ID, "").Tables[0];
                    grListVanDon.Refetch();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //                
            }
        }
        // Container
        public void BindDataContainer()
        {
            try
            {
                if (FormType == "TKMD")
                {
                    grListContainer.Refresh();
                    txtMaHQContainer.Text = TKMD.CoQuanHaiQuan;
                    txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                    dtpNgayDangKyToKhai.Value = TKMD.NgayDangKy;
                    txtMaHQTK.Text = TKMD.CoQuanHaiQuan;
                    txtMaLoaiHinh.Text = TKMD.MaLoaiHinh;
                    grListContainer.DataSource = KDT_VNACCS_Container_Detail.SelectDynamic("TKMD_ID =" + TKMD.ID, "").Tables[0];
                    grListContainer.Refetch();
                }
                else
                {
                    grListContainer.Refresh();
                    txtMaHQContainer.Text = TKBoSung.CoQuanHaiQuan;
                    txtSoToKhai.Text = TKBoSung.SoToKhai.ToString();
                    dtpNgayDangKyToKhai.Value = TKBoSung.NgayKhaiBao;
                    txtMaHQTK.Text = TKBoSung.CoQuanHaiQuan;
                    txtMaLoaiHinh.Text = TKBoSung.MaLoaiHinh;
                    grListContainer.DataSource = KDT_VNACCS_Container_Detail.SelectDynamic("TKMD_ID =" + TKBoSung.ID, "").Tables[0];
                    grListContainer.Refetch();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //   
            }
        }
        // Chứng từ khác
        public void BindDataAdditionalDocument()
        {
            try
            {
                if (FormType == "TKMD")
                {
                    grListCTKhac.Refresh();
                    txtMaHQCTKhac.Text = TKMD.CoQuanHaiQuan;
                    grListCTKhac.DataSource = KDT_VNACCS_AdditionalDocument.SelectDynamicBy_TKMD("TKMD_ID =" + TKMD.ID, "").Tables[0];
                    grListCTKhac.Refetch();
                }
                else
                {
                    grListCTKhac.Refresh();
                    txtMaHQCTKhac.Text = TKBoSung.CoQuanHaiQuan;
                    grListCTKhac.DataSource = KDT_VNACCS_AdditionalDocument.SelectDynamicBy_TKMD("TKMD_ID =" + TKBoSung.ID, "").Tables[0];
                    grListCTKhac.Refetch();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        // Đăng ký làm ngoài giờ
        public void BindDataOvertime()
        {
            try
            {
                grListDKLNG.Refresh();
                txtMaHQDKLNG.Text = TKMD.CoQuanHaiQuan;
                grListDKLNG.DataSource = KDT_VNACCS_OverTime.SelectAll().Tables[0];
                grListDKLNG.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        public void SendVNACCSAll(string Status)
        {
            switch (tbCtrVouchers.SelectedTab.Name)
            {
                case "tpGiayPhep":
                    SendVNACCS_License(Status);
                    break;
                case "tpHopDong":
                    SendVNACCS_ContractDocument(Status);
                    break;
                case "tpHoaDon":
                    SendVNACCS_CommercialInvoice(Status);
                    break;
                case "tpCO":
                    SendVNACCS_CertificateOfOrigin(Status);
                    break;
                case "tpVanDon":
                    SendVNACCS_BillOfLading(Status);
                    break;
                case "tpContainer":
                    SendVNACCS_Container(Status);
                    break;
                case "tpCTKhac":
                    SendVNACCS_AdditionalDocumentn(Status);
                    break;
                case "tpLamNgoaiGio":
                    SendVNACCS_OverTime(Status);
                    break;
                default:
                    break;
            }
        }
        public void SendVNACCS_License(string Status)
        {
            if (ShowMessageTQDT("Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (license.ID == 0)
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ", " Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    license = KDT_VNACCS_License.Load(license.ID);
                    license.LicenseCollection = KDT_VNACCS_License_Detail.SelectCollectionBy_License_ID(license.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.License;
                sendXML.master_id = license.ID;

                if (sendXML.Load())
                {
                    if (license.TrangThaiXuLy != TrangThaiXuLy.CHO_DUYET && Status == "Send")
                    {
                        ShowMessageTQDT(" Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    license.GuidStr = Guid.NewGuid().ToString();
                    Licenses_VNACCS licenses_VNACCS = new Licenses_VNACCS();
                    licenses_VNACCS = Mapper_V4.ToDataTransferLicense(license, licenseDetail, TKMD, TKBoSung, GlobalSettings.TEN_DON_VI, Status, FormType);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = license.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(license.MaHQ)),
                              Identity = license.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = licenses_VNACCS.Issuer,
                              Function = licenses_VNACCS.Function,
                              Reference = license.GuidStr,
                          },
                          licenses_VNACCS
                        );
                    //license.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        license.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else if (Status == "Edit")
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        license.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                        license.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(license.ID, MessageTitle.RegisterLicense);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.License;
                        sendXML.master_id = license.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = Convert.ToInt32(licenses_VNACCS.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            license.Update();
                            txtSoTNGiayPhep.Text = license.SoTN.ToString();
                            clcNgayTNGiayPhep.Value = license.NgayTN;
                            BindDataLicense();
                            ShowMessageTQDT(msgInfor, false);
                            SetCommandStatusAll();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            FeedbackAll();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        public void SendVNACCS_ContractDocument(string Status)
        {
            if (ShowMessageTQDT(" Thông báo từ hệ thống ", " Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (contractDocument.ID == 0)
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ", " Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    contractDocument = KDT_VNACCS_ContractDocument.Load(contractDocument.ID);
                    contractDocument.ContractDocumentCollection = KDT_VNACCS_ContractDocument_Detail.SelectCollectionBy_ContractDocument_ID(contractDocument.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ContractDocument;
                sendXML.master_id = contractDocument.ID;

                if (sendXML.Load())
                {
                    if (contractDocument.TrangThaiXuLy != TrangThaiXuLy.CHO_DUYET && Status == "Send")
                    {
                        ShowMessageTQDT(" Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    contractDocument.GuidStr = Guid.NewGuid().ToString();
                    Contract_VNACCS contract_VNACCS = new Contract_VNACCS();
                    contract_VNACCS = Mapper_V4.ToDataTransferContract(contractDocument, contractDocumentDetail, TKMD, TKBoSung, GlobalSettings.TEN_DON_VI, Status, FormType);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = contractDocument.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(contractDocument.MaHQ)),
                              Identity = contractDocument.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = contract_VNACCS.Issuer,
                              Function = contract_VNACCS.Function,
                              Reference = contractDocument.GuidStr,
                          },
                          contract_VNACCS
                        );
                    //contractDocument.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        contractDocument.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else if (Status == "Edit")
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        contractDocument.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                        contractDocument.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(contractDocument.ID, MessageTitle.RegisterContractDocument);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.ContractDocument;
                        sendXML.master_id = contractDocument.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = Convert.ToInt32(contract_VNACCS.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            contractDocument.Update();
                            txtSoTNHopDong.Text = contractDocument.SoTN.ToString();
                            clcNgayTNHopDong.Value = contractDocument.NgayTN;
                            BindDataContractDocument();
                            ShowMessageTQDT(msgInfor, false);
                            SetCommandStatusAll();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            FeedbackAll();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        public void SendVNACCS_CommercialInvoice(string Status)
        {
            if (ShowMessageTQDT(" Thông báo từ hệ thống ", " Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (commercialInvoice.ID == 0)
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ", " Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    commercialInvoice = KDT_VNACCS_CommercialInvoice.Load(commercialInvoice.ID);
                    commercialInvoice.CommercialInvoiceCollection = KDT_VNACCS_CommercialInvoice_Detail.SelectCollectionBy_CommercialInvoice_ID(commercialInvoice.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.CommercialInvoice;
                sendXML.master_id = commercialInvoice.ID;

                if (sendXML.Load())
                {
                    if (commercialInvoice.TrangThaiXuLy != TrangThaiXuLy.CHO_DUYET && Status == "Send")
                    {
                        ShowMessageTQDT(" Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    commercialInvoice.GuidStr = Guid.NewGuid().ToString();
                    CommercialInvoice_VNACCS commercialInvoice_VNACCS = new CommercialInvoice_VNACCS();
                    commercialInvoice_VNACCS = Mapper_V4.ToDataTransferCommercialInvoice(commercialInvoice, commercialInvoiceDetail, TKMD, TKBoSung, GlobalSettings.TEN_DON_VI, Status, FormType);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = commercialInvoice.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(commercialInvoice.MaHQ)),
                              Identity = commercialInvoice.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = commercialInvoice_VNACCS.Issuer,
                              Function = commercialInvoice_VNACCS.Function,
                              Reference = commercialInvoice.GuidStr,
                          },
                          commercialInvoice_VNACCS
                        );
                    //commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else if (Status == "Edit")
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                        commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(commercialInvoice.ID, MessageTitle.RegisterCommercialInvoicer);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.CommercialInvoice;
                        sendXML.master_id = commercialInvoice.ID;
                        String xml = Helpers.Serializer(msgSend.Content, true, true);
                        sendXML.msg = xml;
                        sendXML.func = Convert.ToInt32(commercialInvoice_VNACCS.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            commercialInvoice.Update();
                            txtSoTNHoaDon.Text = commercialInvoice.SoTN.ToString();
                            clcNgayTNHoaDon.Value = commercialInvoice.NgayTN;
                            BindDataCommercialInvoice();
                            ShowMessageTQDT(msgInfor, false);
                            SetCommandStatusAll();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            FeedbackAll();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        public void SendVNACCS_CertificateOfOrigin(string Status)
        {
            if (ShowMessageTQDT(" Thông báo từ hệ thống ", " Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (certificateOfOrigin.ID == 0)
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ", " Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    certificateOfOrigin = KDT_VNACCS_CertificateOfOrigin.Load(certificateOfOrigin.ID);
                    certificateOfOrigin.CertificateOfOriginCollection = KDT_VNACCS_CertificateOfOrigin_Detail.SelectCollectionBy_CertificateOfOrigin_ID(certificateOfOrigin.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.CertificateOfOrigin;
                sendXML.master_id = certificateOfOrigin.ID;

                if (sendXML.Load())
                {
                    if (certificateOfOrigin.TrangThaiXuLy != TrangThaiXuLy.CHO_DUYET && Status == "Send")
                    {
                        ShowMessageTQDT(" Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    certificateOfOrigin.GuidStr = Guid.NewGuid().ToString();
                    CertificateOfOrigins_VNACCS certificateOfOrigins_VNACCS = new CertificateOfOrigins_VNACCS();
                    certificateOfOrigins_VNACCS = Mapper_V4.ToDataTransferCertificateOfOrigin(certificateOfOrigin, certificateOfOriginDetail, TKMD, TKBoSung, GlobalSettings.TEN_DON_VI, Status, FormType);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = certificateOfOrigin.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(certificateOfOrigin.MaHQ)),
                              Identity = certificateOfOrigin.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = certificateOfOrigins_VNACCS.Issuer,
                              Function = certificateOfOrigins_VNACCS.Function,
                              Reference = certificateOfOrigin.GuidStr,
                          },
                          certificateOfOrigins_VNACCS
                        );
                    //certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else if (Status == "Edit")
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                        certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(certificateOfOrigin.ID, MessageTitle.RegisterCertificateOfOrigin);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.CertificateOfOrigin;
                        sendXML.master_id = certificateOfOrigin.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = Convert.ToInt32(certificateOfOrigins_VNACCS.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            certificateOfOrigin.Update();
                            txtSoTiepNhanCO.Text = certificateOfOrigin.SoTN.ToString();
                            clcNgayTiepNhanCO.Value = certificateOfOrigin.NgayTN;
                            BindDataCertificateOfOrigin();
                            ShowMessageTQDT(msgInfor, false);
                            SetCommandStatusAll();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            FeedbackAll();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        public void SendVNACCS_BillOfLading(string Status)
        {
            if (ShowMessageTQDT(" Thông báo từ hệ thống ", " Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (billOfLading.ID == 0)
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ", " Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    billOfLading = KDT_VNACCS_BillOfLading.Load(billOfLading.ID);
                    billOfLading.BillOfLadingCollection = KDT_VNACCS_BillOfLading_Detail.SelectCollectionBy_BillOfLading_ID(billOfLading.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.BillOfLading;
                sendXML.master_id = billOfLading.ID;

                if (sendXML.Load())
                {
                    if (billOfLading.TrangThaiXuLy != TrangThaiXuLy.CHO_DUYET && Status == "Send")
                    {

                        ShowMessageTQDT(" Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    billOfLading.GuidStr = Guid.NewGuid().ToString();
                    BillOfLading_VNACCS billOfLading_VNACCS = new BillOfLading_VNACCS();
                    billOfLading_VNACCS = Mapper_V4.ToDataTransferBillOfLading(billOfLading, billOfLadingDetail, TKMD, TKBoSung, GlobalSettings.TEN_DON_VI, Status, FormType);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = billOfLading.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(billOfLading.MaHQ)),
                              Identity = billOfLading.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = billOfLading_VNACCS.Issuer,
                              Function = billOfLading_VNACCS.Function,
                              Reference = billOfLading.GuidStr,
                          },
                          billOfLading_VNACCS
                        );
                    //billOfLading.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        billOfLading.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else if (Status == "Edit")
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        billOfLading.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                        billOfLading.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(billOfLading.ID, MessageTitle.RegisterBillOfLading);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.BillOfLading;
                        sendXML.master_id = billOfLading.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = Convert.ToInt32(billOfLading_VNACCS.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            billOfLading.Update();
                            txtSoTiepNhanVanDon.Text = billOfLading.SoTN.ToString();
                            clcNgayTNVanDon.Value = billOfLading.NgayTN;
                            BindDataBillOfLading();
                            ShowMessageTQDT(msgInfor, false);
                            SetCommandStatusAll();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            FeedbackAll();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        public void SendVNACCS_Container(string Status)
        {
            if (ShowMessageTQDT(" Thông báo từ hệ thống ", " Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (container.ID == 0)
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ", " Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    container = KDT_VNACCS_Container_Detail.Load(container.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.Containers;
                sendXML.master_id = container.ID;

                if (sendXML.Load())
                {
                    if (container.TrangThaiXuLy != TrangThaiXuLy.CHO_DUYET && Status == "Send")
                    {
                        ShowMessageTQDT(" Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    container.GuidStr = Guid.NewGuid().ToString();
                    Company.KDT.SHARE.Components.Messages.Vouchers.Container_VNACCS container_VNACCS = new Company.KDT.SHARE.Components.Messages.Vouchers.Container_VNACCS();
                    container_VNACCS = Mapper_V4.ToDataTransferContainer(container, TKMD, TKBoSung, GlobalSettings.TEN_DON_VI, Status, FormType);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = container.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(container.MaHQ)),
                              Identity = container.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = container_VNACCS.Issuer,
                              Function = container_VNACCS.Function,
                              Reference = container.GuidStr,
                          },
                          container_VNACCS
                        );
                    //container.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        container.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else if (Status == "Edit")
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        container.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                        container.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(container.ID, MessageTitle.RegisterContainer);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.Containers;
                        sendXML.master_id = container.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = Convert.ToInt32(container_VNACCS.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            container.Update();
                            txtSoTNContainer.Text = container.SoTN.ToString();
                            clcNgayTNContainer.Value = container.NgayTN;
                            BindDataContainer();
                            ShowMessageTQDT(msgInfor, false);
                            SetCommandStatusAll();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            FeedbackAll();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        public void SendVNACCS_AdditionalDocumentn(string Status)
        {
            if (ShowMessageTQDT(" Thông báo từ hệ thống ", " Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (additionalDocument.ID == 0)
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ", " Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    additionalDocument = KDT_VNACCS_AdditionalDocument.Load(additionalDocument.ID);
                    additionalDocument.AdditionalDocumentCollection = KDT_VNACCS_AdditionalDocument_Detail.SelectCollectionBy_AdditionalDocument_ID(additionalDocument.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.AdditionalDocument;
                sendXML.master_id = additionalDocument.ID;

                if (sendXML.Load())
                {
                    if (additionalDocument.TrangThaiXuLy != TrangThaiXuLy.CHO_DUYET && Status == "Send")
                    {
                        ShowMessageTQDT(" Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    additionalDocument.GuidStr = Guid.NewGuid().ToString();
                    AdditionalDocument_VNACCS additionalDocument_VNACCS = new AdditionalDocument_VNACCS();
                    additionalDocument_VNACCS = Mapper_V4.ToDataTransferAdditionalDocument(additionalDocument, additionalDocumentDetail, TKMD, TKBoSung, GlobalSettings.TEN_DON_VI, Status, FormType);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = additionalDocument.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(additionalDocument.MaHQ)),
                              Identity = additionalDocument.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = additionalDocument_VNACCS.Issuer,
                              Function = additionalDocument_VNACCS.Function,
                              Reference = additionalDocument.GuidStr,
                          },
                          additionalDocument_VNACCS
                        );
                    //additionalDocument.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        additionalDocument.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else if (Status == "Edit")
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        additionalDocument.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                        additionalDocument.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(additionalDocument.ID, MessageTitle.RegisterAdditionalDocumentr);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.AdditionalDocument;
                        sendXML.master_id = additionalDocument.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = Convert.ToInt32(additionalDocument_VNACCS.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            additionalDocument.Update();
                            txtSoTNCTKhac.Text = additionalDocument.SoTN.ToString();
                            clcNgayTNCTKhac.Value = additionalDocument.NgayTN;
                            BindDataAdditionalDocument();
                            ShowMessageTQDT(msgInfor, false);
                            SetCommandStatusAll();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            FeedbackAll();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        public void SendVNACCS_OverTime(string Status)
        {
            if (ShowMessageTQDT(" Thông báo từ hệ thống ", " Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (overTime.ID == 0)
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ", " Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    overTime = KDT_VNACCS_OverTime.Load(overTime.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.OverTime;
                sendXML.master_id = overTime.ID;
                if (sendXML.Load())
                {
                    if (overTime.TrangThaiXuLy != TrangThaiXuLy.CHO_DUYET && Status == "Send")
                    {
                        ShowMessageTQDT(" Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    overTime.GuidStr = Guid.NewGuid().ToString();
                    Overtime_VNACCS overtime_VNACCS = new Overtime_VNACCS();
                    overtime_VNACCS = Mapper_V4.ToDataTransferOvertime(overTime, TKMD, GlobalSettings.TEN_DON_VI, Status);
                    ObjectSend msgSend = new ObjectSend(

                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = overTime.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(overTime.MaHQ)),
                              Identity = overTime.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = overtime_VNACCS.Issuer,
                              Function = overtime_VNACCS.Function,
                              Reference = overTime.GuidStr,
                          },
                          overtime_VNACCS
                        );
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                        overTime.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    else if (Status == "Edit")
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                        overTime.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                        overTime.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    }

                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(overTime.ID, MessageTitle.RegisterOverTime);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.OverTime;
                        sendXML.master_id = overTime.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = Convert.ToInt32(overtime_VNACCS.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            overTime.Update();
                            txtSoTNDKLNG.Text = overTime.SoTN.ToString();
                            clcNgayTNDKLNG.Value = overTime.NgayTN;
                            BindDataOvertime();
                            ShowMessageTQDT(msgInfor, false);
                            SetCommandStatusAll();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            FeedbackAll();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        public void Feedback_License()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.License;
            sendXML.master_id = license.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                return;
            }
            while (isFeedBack)
            {
                string reference = license.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.License,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.License,
                };
                subjectBase.Type = DeclarationIssuer.License;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = license.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(license.MaHQ.Trim())),
                                                  Identity = license.MaHQ
                                              }, subjectBase, null);
                if (license.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (chkAutoFeedbackGP.Checked)
                    {
                        Stopwatch sw;
                        if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi ...") || msgInfor.Contains("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin") || msgInfor.Contains("The operation has timed out"))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 2000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi sau [10] giây để hỏi lại phản hồi..."))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 10000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else
                        {
                            isFeedBack = false;
                        }
                    }
                    else
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        //isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        BindDataLicense();
                    }
                }
            }

        }
        public void Feedback_ContractDocument()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;

            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.ContractDocument;
            sendXML.master_id = contractDocument.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                return;
            }
            while (isFeedBack)
            {
                string reference = contractDocument.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.ContractDocument,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.ContractDocument,
                };
                subjectBase.Type = DeclarationIssuer.ContractDocument;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = contractDocument.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(contractDocument.MaHQ.Trim())),
                                                  Identity = contractDocument.MaHQ
                                              }, subjectBase, null);
                if (contractDocument.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (chkAutoFeedbackHD.Checked)
                    {
                        Stopwatch sw;
                        if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi ...") || msgInfor.Contains("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin") || msgInfor.Contains("The operation has timed out"))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 2000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi sau [10] giây để hỏi lại phản hồi..."))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 10000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else
                        {
                            isFeedBack = false;
                        }
                    }
                    else
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        BindDataContractDocument();
                    }
                }
            }

        }
        public void Feedback_CommercialInvoice()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.CommercialInvoice;
            sendXML.master_id = commercialInvoice.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                return;
            }
            while (isFeedBack)
            {
                string reference = commercialInvoice.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.CommercialInvoice,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.CommercialInvoice,
                };
                subjectBase.Type = DeclarationIssuer.CommercialInvoice;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = commercialInvoice.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(commercialInvoice.MaHQ.Trim())),
                                                  Identity = commercialInvoice.MaHQ
                                              }, subjectBase, null);
                if (commercialInvoice.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (chkAutoFeedbackHoaDon.Checked)
                    {
                        Stopwatch sw;
                        if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi ...") || msgInfor.Contains("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin") || msgInfor.Contains("The operation has timed out"))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 2000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi sau [10] giây để hỏi lại phản hồi..."))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 10000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else
                        {
                            isFeedBack = false;
                        }
                    }
                    else
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        BindDataCommercialInvoice();
                    }

                }
            }

        }
        public void Feedback_CertificateOfOrigin()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.CertificateOfOrigin;
            sendXML.master_id = certificateOfOrigin.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                return;
            }
            while (isFeedBack)
            {
                string reference = certificateOfOrigin.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.CertificateOfOrigin,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.CertificateOfOrigin,
                };
                subjectBase.Type = DeclarationIssuer.CertificateOfOrigin;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = certificateOfOrigin.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(certificateOfOrigin.MaHQ.Trim())),
                                                  Identity = certificateOfOrigin.MaHQ
                                              }, subjectBase, null);
                if (certificateOfOrigin.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (chkAutoFeedbackCO.Checked)
                    {
                        Stopwatch sw;
                        if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi ...") || msgInfor.Contains("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin") || msgInfor.Contains("The operation has timed out"))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 2000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi sau [10] giây để hỏi lại phản hồi..."))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 10000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else
                        {
                            isFeedBack = false;
                        }
                    }
                    else
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        BindDataCertificateOfOrigin();
                    }
                }
            }

        }
        public void Feedback_BillOfLading()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.BillOfLading;
            sendXML.master_id = billOfLading.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                return;
            }
            while (isFeedBack)
            {
                string reference = billOfLading.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.BillOfLading,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.BillOfLading,
                };
                subjectBase.Type = DeclarationIssuer.BillOfLading;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = billOfLading.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(billOfLading.MaHQ.Trim())),
                                                  Identity = billOfLading.MaHQ
                                              }, subjectBase, null);
                if (billOfLading.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (chkAutoFeedbackVD.Checked)
                    {
                        Stopwatch sw;
                        if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi ...") || msgInfor.Contains("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin") || msgInfor.Contains("The operation has timed out"))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 2000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi sau [10] giây để hỏi lại phản hồi..."))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 10000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else
                        {
                            isFeedBack = false;
                        }
                    }
                    else
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        BindDataBillOfLading();
                    }

                }
            }

        }
        public void Feedback_Container()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.Containers;
            sendXML.master_id = container.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                return;
            }
            while (isFeedBack)
            {
                string reference = container.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.Containers,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.Containers,
                };
                subjectBase.Type = DeclarationIssuer.Containers;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = container.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(container.MaHQ.Trim())),
                                                  Identity = container.MaHQ
                                              }, subjectBase, null);
                if (container.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (chkAutoFeedbackCT.Checked)
                    {
                        Stopwatch sw;
                        if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi ...") || msgInfor.Contains("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin") || msgInfor.Contains("The operation has timed out"))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 2000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi sau [10] giây để hỏi lại phản hồi..."))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 10000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else
                        {
                            isFeedBack = false;
                        }
                    }
                    else
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        BindDataContainer();
                    }

                }
            }

        }
        public void Feedback_AdditionalDocumentn()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.AdditionalDocument;
            sendXML.master_id = additionalDocument.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                return;
            }
            while (isFeedBack)
            {
                string reference = additionalDocument.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.AdditionalDocument,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.AdditionalDocument,
                };
                subjectBase.Type = DeclarationIssuer.AdditionalDocument;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = additionalDocument.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(additionalDocument.MaHQ.Trim())),
                                                  Identity = additionalDocument.MaHQ
                                              }, subjectBase, null);
                if (additionalDocument.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (chkAutoFeedbackCTK.Checked)
                    {
                        Stopwatch sw;
                        if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi ...") || msgInfor.Contains("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin") || msgInfor.Contains("The operation has timed out"))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 2000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi sau [10] giây để hỏi lại phản hồi..."))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 10000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else
                        {
                            isFeedBack = false;
                        }
                    }
                    else
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        BindDataAdditionalDocument();
                    }

                }
            }

        }
        private void Feedback_OverTime()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.OverTime;
            sendXML.master_id = overTime.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                return;
            }
            while (isFeedBack)
            {
                string reference = overTime.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.OverTime,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.OverTime,
                };
                subjectBase.Type = DeclarationIssuer.OverTime;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = overTime.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(overTime.MaHQ.Trim())),
                                                  Identity = overTime.MaHQ
                                              }, subjectBase, null);
                if (overTime.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (chkAutoFeedbackOT.Checked)
                    {
                        Stopwatch sw;
                        if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi ...") || msgInfor.Contains("Thiết bị ký số đã bị tháo ra\r\nVui lòng gắn vào để ký thông tin") || msgInfor.Contains("The operation has timed out"))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 2000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else if (msgInfor.Contains("đã được hệ thống tiếp nhận nhưng chưa xử lý xong, vui lòng đợi sau [10] giây để hỏi lại phản hồi..."))
                        {
                            sw = new Stopwatch();
                            sw.Start();
                            bool flag = false;
                            while (!flag)
                            {
                                if (sw.ElapsedMilliseconds > 10000)
                                {
                                    flag = true;
                                }
                            }
                            sw.Stop();
                            isFeedBack = true;
                        }
                        else
                        {
                            isFeedBack = false;
                        }
                    }
                    else
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                            FeedbackAll();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            isFeedBack = false;
                            SetCommandStatusAll();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        }
                        BindDataOvertime();
                    }
                }
            }
        }
        private void FeedbackAll()
        {
            switch (tbCtrVouchers.SelectedTab.Name)
            {
                case "tpGiayPhep":
                    Feedback_License();
                    break;
                case "tpHopDong":
                    Feedback_ContractDocument();
                    break;
                case "tpHoaDon":
                    Feedback_CommercialInvoice();
                    break;
                case "tpCO":
                    Feedback_CertificateOfOrigin();
                    break;
                case "tpVanDon":
                    Feedback_BillOfLading();
                    break;
                case "tpContainer":
                    Feedback_Container();
                    break;
                case "tpCTKhac":
                    Feedback_AdditionalDocumentn();
                    break;
                case "tpLamNgoaiGio":
                    Feedback_OverTime();
                    break;
                default:
                    break;
            }
        }
        private void Reset()
        {
            switch (tbCtrVouchers.SelectedTab.Name)
            {
                case "tpGiayPhep":
                    license.ID = 0;
                    licenseDetail.License_ID = 0;
                    txtNguoiCapGiayPhep.Text = String.Empty;
                    txtSoGiayPhep.Text = String.Empty;
                    txtNoiCapGiayPhep.Text = String.Empty;
                    txtGhiChuGiayPhep.Text = String.Empty;
                    cbbLoaiGiayPhep.Text = String.Empty;
                    lblFileNameGiayPhep.Text = String.Empty;
                    txtFileSizeGiayPhep.Text = String.Empty;
                    txtSoTNGiayPhep.Text = "0";
                    clcNgayTNGiayPhep.Value = DateTime.Now;
                    license = new KDT_VNACCS_License();
                    isAddGiayPhep = true;
                    break;
                case "tpHopDong":
                    contractDocument.ID = 0;
                    contractDocumentDetail.ContractDocument_ID = 0;
                    txtSoHopDong.Text = String.Empty;
                    txtTongTriGiaHopDong.Text = String.Empty;
                    txtGhiChuHopDong.Text = String.Empty;
                    txtGhiChuHopDong.Text = String.Empty;
                    lblFileNameHopDong.Text = String.Empty;
                    txtFileSizeHopDong.Text = String.Empty;
                    txtSoTNHopDong.Text = "0";
                    clcNgayTNHopDong.Value = DateTime.Now;
                    contractDocument = new KDT_VNACCS_ContractDocument();
                    isAddHopDong = true;
                    break;
                case "tpHoaDon":
                    commercialInvoice.ID = 0;
                    commercialInvoiceDetail.CommercialInvoice_ID = 0;
                    txtSoHoaDonTM.Text = String.Empty;
                    txtGhiChuHoaDon.Text = String.Empty;
                    lblFileNameHoaDon.Text = String.Empty;
                    txtFileSizeHoaDon.Text = String.Empty;
                    txtSoTNHoaDon.Text = "0";
                    clcNgayTNHoaDon.Value = DateTime.Now;
                    commercialInvoice = new KDT_VNACCS_CommercialInvoice();
                    isAddHoaDon = true;
                    break;
                case "tpCO":
                    certificateOfOrigin.ID = 0;
                    certificateOfOriginDetail.CertificateOfOrigin_ID = 0;
                    txtSoCO.Text = String.Empty;
                    txtToChucCapCO.Text = String.Empty;
                    txtNguoiCapCO.Text = String.Empty;
                    txtGhiChuCO.Text = String.Empty;
                    lblFileNameCO.Text = String.Empty;
                    txtFileSizeCO.Text = String.Empty;
                    txtSoTiepNhanCO.Text = "0";
                    clcNgayTiepNhanCO.Value = DateTime.Now;
                    certificateOfOrigin = new KDT_VNACCS_CertificateOfOrigin();
                    isAddCO = true;
                    break;
                case "tpVanDon":
                    billOfLading.ID = 0;
                    billOfLadingDetail.BillOfLading_ID = 0;
                    txtSoVanDon.Text = String.Empty;
                    txtDiaDiemCTQC.Text = String.Empty;
                    txtGhiChuVanDon.Text = String.Empty;
                    lblFileNameVanDon.Text = String.Empty;
                    txtFileSizeVanDon.Text = String.Empty;
                    txtSoTiepNhanVanDon.Text = "0";
                    clcNgayTNVanDon.Value = DateTime.Now;
                    billOfLading = new KDT_VNACCS_BillOfLading();
                    isAddVanDon = true;
                    break;
                case "tpContainer":
                    container.ID = 0;
                    txtGhiChuContainer.Text = String.Empty;
                    lblFileNamContainer.Text = String.Empty;
                    txtFileSizeContainer.Text = String.Empty;
                    txtSoTNContainer.Text = "0";
                    clcNgayTNContainer.Value = DateTime.Now;
                    container = new KDT_VNACCS_Container_Detail();
                    isAddContainer = true;
                    break;
                case "tpCTKhac":
                    additionalDocument.ID = 0;
                    additionalDocumentDetail.AdditionalDocument_ID = 0;
                    txtSoChungTuKhac.Text = String.Empty;
                    txtTenChungTuKhac.Text = String.Empty;
                    txtNoiPhatHanhCTKhac.Text = String.Empty;
                    txtGhiChuCTKhac.Text = String.Empty;
                    lblFileNameCTKhac.Text = String.Empty;
                    txtFileSizeChungTuKhac.Text = String.Empty;
                    txtSoTNCTKhac.Text = "0";
                    clcNgayTNCTKhac.Value = DateTime.Now;
                    additionalDocument = new KDT_VNACCS_AdditionalDocument();
                    isAddCTkhac = true;
                    break;
                case "tpLamNgoaiGio":
                    overTime.ID = 0;
                    txtNoiDung.Text = String.Empty;
                    txtSoTNDKLNG.Text = "0";
                    clcNgayTNDKLNG.Value = DateTime.Now;
                    overTime = new KDT_VNACCS_OverTime();
                    isAddOverTime = true;
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// Tính dung lượng File
        /// </summary>
        /// <param name="FileInBytes"></param>
        /// <returns></returns>
        private string CalculateFileSize(decimal FileInBytes)
        {
            string strSize = "00";
            if (FileInBytes < 1024)
                strSize = FileInBytes + " B";//Byte
            else if (FileInBytes >= 1024 & FileInBytes < 1048576)
                strSize = Math.Round((FileInBytes / 1024), 2) + " KB";//Kilobyte
            else if (FileInBytes >= 1048576 & FileInBytes < 107341824)
                strSize = Math.Round((FileInBytes / 1024) / 1024, 2) + " MB";//Megabyte
            else if (FileInBytes >= 107341824 & FileInBytes < 1099511627776)
                strSize = Math.Round(((FileInBytes / 1024) / 1024) / 1024, 2) + " GB";//Gigabyte
            else
                strSize = Math.Round((((FileInBytes / 1024) / 1024) / 1024) / 1024, 2) + " TB";//Terabyte
            return strSize;
        }
        public void SaveAll()
        {
            try
            {
                switch (tbCtrVouchers.SelectedTab.Name)
                {
                    case "tpGiayPhep":
                        if (String.IsNullOrEmpty(lblFileNameGiayPhep.Text))
                        {
                            ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CHƯA THÊM TỆP TIN CHỨNG TỪ ĐÍNH KÈM", false);
                            return;
                        }
                        if (!ValidateForm(false))
                            return;
                        GetVouchers("License");
                        if (size > GlobalSettings.FileSize / 2)
                        {
                            ShowMessage(string.Format("TỔNG DUNG LƯỢNG FILE ĐÍNH KÈM CHO PHÉP {0}\r\nDUNG LƯỢNG DOANH NGHIỆP NHẬP {1} ĐÃ VƯỢT MỨC CHO PHÉP.", CalculateFileSize(GlobalSettings.FileSize / 2), CalculateFileSize(size)), false);
                            return;
                        }
                        license.InsertUpdateFull();
                        ShowMessage("Lưu thành công", false);
                        BindDataLicense();
                        Reset();
                        break;
                    case "tpHopDong":
                        if (String.IsNullOrEmpty(lblFileNameHopDong.Text))
                        {
                            ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CHƯA THÊM TỆP TIN CHỨNG TỪ ĐÍNH KÈM", false);
                            return;
                        }
                        if (!ValidateForm(false))
                            return;
                        GetVouchers("ContractDocument");
                        if (size > GlobalSettings.FileSize / 2)
                        {
                            ShowMessage(string.Format("TỔNG DUNG LƯỢNG FILE ĐÍNH KÈM CHO PHÉP {0}\r\nDUNG LƯỢNG DOANH NGHIỆP NHẬP {1} ĐÃ VƯỢT MỨC CHO PHÉP.", CalculateFileSize(GlobalSettings.FileSize / 2), CalculateFileSize(size)), false);
                            return;
                        }
                        contractDocument.InsertUpdateFull();
                        ShowMessage("Lưu thành công", false);
                        BindDataContractDocument();
                        Reset();
                        break;
                    case "tpHoaDon":
                        if (String.IsNullOrEmpty(lblFileNameHoaDon.Text))
                        {
                            ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CHƯA THÊM TỆP TIN CHỨNG TỪ ĐÍNH KÈM", false);
                            return;
                        }
                        if (!ValidateForm(false))
                            return;
                        GetVouchers("CommercialInvoice");
                        if (size > GlobalSettings.FileSize / 2)
                        {
                            ShowMessage(string.Format("TỔNG DUNG LƯỢNG FILE ĐÍNH KÈM CHO PHÉP {0}\r\nDUNG LƯỢNG DOANH NGHIỆP NHẬP {1} ĐÃ VƯỢT MỨC CHO PHÉP.", CalculateFileSize(GlobalSettings.FileSize / 2), CalculateFileSize(size)), false);
                            return;
                        }
                        commercialInvoice.InsertUpdateFull();
                        ShowMessage("Lưu thành công", false);
                        BindDataCommercialInvoice();
                        Reset();
                        break;
                    case "tpCO":
                        if (String.IsNullOrEmpty(lblFileNameCO.Text))
                        {
                            ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CHƯA THÊM TỆP TIN CHỨNG TỪ ĐÍNH KÈM", false);
                            return;
                        }
                        if (!ValidateForm(false))
                            return;
                        GetVouchers("CertificateOfOrigin");
                        if (size > GlobalSettings.FileSize / 2)
                        {
                            ShowMessage(string.Format("TỔNG DUNG LƯỢNG FILE ĐÍNH KÈM CHO PHÉP {0}\r\nDUNG LƯỢNG DOANH NGHIỆP NHẬP {1} ĐÃ VƯỢT MỨC CHO PHÉP.", CalculateFileSize(GlobalSettings.FileSize / 2), CalculateFileSize(size)), false);
                            return;
                        }
                        certificateOfOrigin.InsertUpdateFull();
                        ShowMessage("Lưu thành công", false);
                        BindDataCertificateOfOrigin();
                        Reset();
                        break;
                    case "tpVanDon":
                        if (String.IsNullOrEmpty(lblFileNameVanDon.Text))
                        {
                            ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CHƯA THÊM TỆP TIN CHỨNG TỪ ĐÍNH KÈM", false);
                            return;
                        }
                        if (!ValidateForm(false))
                            return;
                        GetVouchers("BillOfLading");
                        if (size > GlobalSettings.FileSize / 2)
                        {
                            ShowMessage(string.Format("TỔNG DUNG LƯỢNG FILE ĐÍNH KÈM CHO PHÉP {0}\r\nDUNG LƯỢNG DOANH NGHIỆP NHẬP {1} ĐÃ VƯỢT MỨC CHO PHÉP.", CalculateFileSize(GlobalSettings.FileSize / 2), CalculateFileSize(size)), false);
                            return;
                        }
                        billOfLading.InsertUpdateFull();
                        ShowMessage("Lưu thành công", false);
                        BindDataBillOfLading();
                        Reset();
                        break;
                    case "tpContainer":
                        if (String.IsNullOrEmpty(lblFileNamContainer.Text))
                        {
                            ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CHƯA THÊM TỆP TIN CHỨNG TỪ ĐÍNH KÈM", false);
                            return;
                        }
                        GetVouchers("Container");
                        if (size > GlobalSettings.FileSize / 2)
                        {
                            ShowMessage(string.Format("TỔNG DUNG LƯỢNG FILE ĐÍNH KÈM CHO PHÉP {0}\r\nDUNG LƯỢNG DOANH NGHIỆP NHẬP {1} ĐÃ VƯỢT MỨC CHO PHÉP.", CalculateFileSize(GlobalSettings.FileSize / 2), CalculateFileSize(size)), false);
                            return;
                        }
                        container.InsertUpdate();
                        ShowMessage("Lưu thành công", false);
                        BindDataContainer();
                        Reset();
                        break;
                    case "tpCTKhac":
                        if (String.IsNullOrEmpty(lblFileNameCTKhac.Text))
                        {
                            ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CHƯA THÊM TỆP TIN CHỨNG TỪ ĐÍNH KÈM", false);
                            return;
                        }
                        if (!ValidateForm(false))
                            return;
                        GetVouchers("AdditionalDocument");
                        if (size > GlobalSettings.FileSize / 2)
                        {
                            ShowMessage(string.Format("TỔNG DUNG LƯỢNG FILE ĐÍNH KÈM CHO PHÉP {0}\r\nDUNG LƯỢNG DOANH NGHIỆP NHẬP {1} ĐÃ VƯỢT MỨC CHO PHÉP.", CalculateFileSize(GlobalSettings.FileSize / 2), CalculateFileSize(size)), false);
                            return;
                        }
                        additionalDocument.InsertUpdateFull();
                        ShowMessage("Lưu thành công", false);
                        BindDataAdditionalDocument();
                        Reset();
                        break;
                    case "tpLamNgoaiGio":
                        if (!ValidateForm(false))
                            return;
                        GetVouchers("OverTime");
                        overTime.InsertUpdate();
                        ShowMessage("Lưu thành công", false);
                        BindDataOvertime();
                        Reset();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //               
            }

        }
        public void GetVouchers(string Vouchers)
        {
            try
            {
                switch (Vouchers)
                {
                    case "License":
                        if (FormType == "TKMD")
                        {
                            license.TKMD_ID = TKMD.ID;
                        }
                        else
                        {
                            license.TKMD_ID = TKBoSung.ID;
                        }
                        if (license.ID == 0)
                            license.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        license.SoTN = Convert.ToInt64(txtSoTNGiayPhep.Text);
                        license.NgayTN = clcNgayTNGiayPhep.Value;
                        license.MaHQ = txtMaHQGiayPhep.Text;
                        license.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        license.GhiChuKhac = txtGhiChuGiayPhep.Text.ToString();
                        license.FileName = lblFileNameGiayPhep.Text;
                        if (license.LicenseCollection.Count > 0)
                        {
                            foreach (KDT_VNACCS_License_Detail item in license.LicenseCollection)
                            {
                                if (item.License_ID == license.ID)
                                {
                                    item.NguoiCapGP = txtNguoiCapGiayPhep.Text.ToString();
                                    item.SoGP = txtSoGiayPhep.Text.ToString();
                                    item.NgayCapGP = dtpNgayCapGiayPhep.Value;
                                    item.NoiCapGP = txtNoiCapGiayPhep.Text.ToString();
                                    item.LoaiGP = cbbLoaiGiayPhep.Text.ToString();
                                    item.NgayHetHanGP = dtpNgayHetHanGiayPhep.Value;
                                }
                            }
                        }
                        else
                        {
                            licenseDetail.NguoiCapGP = txtNguoiCapGiayPhep.Text.ToString();
                            licenseDetail.SoGP = txtSoGiayPhep.Text.ToString();
                            licenseDetail.NgayCapGP = dtpNgayCapGiayPhep.Value;
                            licenseDetail.NoiCapGP = txtNoiCapGiayPhep.Text.ToString();
                            licenseDetail.LoaiGP = cbbLoaiGiayPhep.Text.ToString();
                            licenseDetail.NgayHetHanGP = dtpNgayHetHanGiayPhep.Value;
                        }
                        if (isAddGiayPhep)
                            license.LicenseCollection.Add(licenseDetail);
                        break;
                    case "ContractDocument":
                        if (FormType == "TKMD")
                        {
                            contractDocument.TKMD_ID = TKMD.ID;
                        }
                        else
                        {
                            contractDocument.TKMD_ID = TKBoSung.ID;
                        }
                        if (contractDocument.ID == 0)
                            contractDocument.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        contractDocument.SoTN = Convert.ToInt64(txtSoTNHopDong.Text);
                        contractDocument.NgayTN = clcNgayTNHopDong.Value;
                        contractDocument.MaHQ = txtMaHQHopDong.Text;
                        contractDocument.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        contractDocument.GhiChuKhac = txtGhiChuHopDong.Text.ToString();
                        contractDocument.FileName = lblFileNameHopDong.Text;
                        if (contractDocument.ContractDocumentCollection.Count > 0)
                        {
                            foreach (KDT_VNACCS_ContractDocument_Detail item in contractDocument.ContractDocumentCollection)
                            {
                                if (item.ContractDocument_ID == contractDocument.ID)
                                {
                                    item.SoHopDong = txtSoHopDong.Text.ToString();
                                    item.NgayHopDong = dtpNgayHopDong.Value;
                                    item.ThoiHanThanhToan = dtpThoiHanThanhToan.Value;
                                    item.TongTriGia = Convert.ToDecimal(txtTongTriGiaHopDong.Text.ToString());
                                }
                            }
                        }
                        else
                        {
                            contractDocumentDetail.SoHopDong = txtSoHopDong.Text.ToString();
                            contractDocumentDetail.NgayHopDong = dtpNgayHopDong.Value;
                            contractDocumentDetail.ThoiHanThanhToan = dtpThoiHanThanhToan.Value;
                            contractDocumentDetail.TongTriGia = Convert.ToDecimal(txtTongTriGiaHopDong.Text.ToString());
                        }
                        if (isAddHopDong)
                            contractDocument.ContractDocumentCollection.Add(contractDocumentDetail);
                        break;
                    case "CommercialInvoice":
                        if (FormType == "TKMD")
                        {
                            commercialInvoice.TKMD_ID = TKMD.ID;
                        }
                        else
                        {
                            commercialInvoice.TKMD_ID = TKBoSung.ID;
                        }
                        if (commercialInvoice.ID == 0)
                            commercialInvoice.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        commercialInvoice.SoTN = Convert.ToInt64(txtSoTNHoaDon.Text);
                        commercialInvoice.NgayTN = clcNgayTNHoaDon.Value;
                        commercialInvoice.MaHQ = txtMaHQHoaDon.Text;
                        commercialInvoice.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        commercialInvoice.GhiChuKhac = txtGhiChuHoaDon.Text.ToString();
                        commercialInvoice.FileName = lblFileNameHoaDon.Text;
                        if (commercialInvoice.CommercialInvoiceCollection.Count > 0)
                        {
                            foreach (KDT_VNACCS_CommercialInvoice_Detail item in commercialInvoice.CommercialInvoiceCollection)
                            {
                                if (item.CommercialInvoice_ID == commercialInvoice.ID)
                                {
                                    item.SoHoaDonTM = txtSoHoaDonTM.Text.ToString();
                                    item.NgayPhatHanhHDTM = dtpNgayPhatHanhHoaDonTM.Value;
                                }
                            }
                        }
                        else
                        {
                            commercialInvoiceDetail.SoHoaDonTM = txtSoHoaDonTM.Text.ToString();
                            commercialInvoiceDetail.NgayPhatHanhHDTM = dtpNgayPhatHanhHoaDonTM.Value;
                        }
                        if (isAddHoaDon)
                            commercialInvoice.CommercialInvoiceCollection.Add(commercialInvoiceDetail);
                        break;
                    case "CertificateOfOrigin":
                        if (FormType == "TKMD")
                        {
                            certificateOfOrigin.TKMD_ID = TKMD.ID;
                        }
                        else
                        {
                            certificateOfOrigin.TKMD_ID = TKBoSung.ID;
                        }
                        if (certificateOfOrigin.ID == 0)
                            certificateOfOrigin.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        certificateOfOrigin.SoTN = Convert.ToInt64(txtSoTiepNhanCO.Text);
                        certificateOfOrigin.NgayTN = clcNgayTiepNhanCO.Value;
                        certificateOfOrigin.MaHQ = txtMaHQCO.Text;
                        certificateOfOrigin.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        certificateOfOrigin.GhiChuKhac = txtGhiChuCO.Text.ToString();
                        certificateOfOrigin.FileName = lblFileNameCO.Text;
                        if (certificateOfOrigin.CertificateOfOriginCollection.Count > 0)
                        {
                            foreach (KDT_VNACCS_CertificateOfOrigin_Detail item in certificateOfOrigin.CertificateOfOriginCollection)
                            {
                                if (item.CertificateOfOrigin_ID == certificateOfOrigin.ID)
                                {
                                    item.SoCO = txtSoCO.Text.ToString();
                                    item.LoaiCO = cbbLoaiCO.SelectedValue.ToString();
                                    item.NgayCapCO = dateNgayCapCO.Value;
                                    item.NuocCapCO = txtNuocCapCO.Ma.ToString();
                                    item.ToChucCapCO = txtToChucCapCO.Text.ToString();
                                    item.NguoiCapCO = txtNguoiCapCO.Text.ToString();
                                }
                            }
                        }
                        else
                        {
                            certificateOfOriginDetail.SoCO = txtSoCO.Text.ToString();
                            certificateOfOriginDetail.LoaiCO = cbbLoaiCO.SelectedValue.ToString();
                            certificateOfOriginDetail.NgayCapCO = dateNgayCapCO.Value;
                            certificateOfOriginDetail.NuocCapCO = txtNuocCapCO.Ma.ToString();
                            certificateOfOriginDetail.ToChucCapCO = txtToChucCapCO.Text.ToString();
                            certificateOfOriginDetail.NguoiCapCO = txtNguoiCapCO.Text.ToString();
                        }
                        if (isAddCO)
                            certificateOfOrigin.CertificateOfOriginCollection.Add(certificateOfOriginDetail);
                        break;
                    case "BillOfLading":
                        if (FormType == "TKMD")
                        {
                            billOfLading.TKMD_ID = TKMD.ID;
                        }
                        else
                        {
                            billOfLading.TKMD_ID = TKBoSung.ID;
                        }
                        if (billOfLading.ID == 0)
                            billOfLading.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        billOfLading.SoTN = Convert.ToInt64(txtSoTiepNhanVanDon.Text);
                        billOfLading.NgayTN = clcNgayTNVanDon.Value;
                        billOfLading.MaHQ = txtMaHQVanDon.Text;
                        billOfLading.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        billOfLading.GhiChuKhac = txtGhiChuVanDon.Text.ToString();
                        billOfLading.FileName = lblFileNameVanDon.Text;
                        if (billOfLading.BillOfLadingCollection.Count > 0)
                        {
                            foreach (KDT_VNACCS_BillOfLading_Detail item in billOfLading.BillOfLadingCollection)
                            {
                                if (item.BillOfLading_ID == billOfLading.ID)
                                {
                                    item.SoVanDon = txtSoVanDon.Text.ToString();
                                    item.NgayVanDon = dtpNgayVanDon.Value;
                                    item.NuocPhatHanh = ctrNuocPHVanDon.Ma.ToString();
                                    item.DiaDiemCTQC = txtDiaDiemCTQC.Text.ToString();
                                    item.LoaiVanDon = Convert.ToDecimal(cbbLoaiVanDon.SelectedValue.ToString());
                                }
                            }
                        }
                        else
                        {
                            billOfLadingDetail.SoVanDon = txtSoVanDon.Text.ToString();
                            billOfLadingDetail.NgayVanDon = dtpNgayVanDon.Value;
                            billOfLadingDetail.NuocPhatHanh = ctrNuocPHVanDon.Ma.ToString();
                            billOfLadingDetail.DiaDiemCTQC = txtDiaDiemCTQC.Text.ToString();
                            billOfLadingDetail.LoaiVanDon = Convert.ToDecimal(cbbLoaiVanDon.SelectedValue.ToString());
                        }
                        if (isAddVanDon)
                            billOfLading.BillOfLadingCollection.Add(billOfLadingDetail);
                        break;
                    case "Container":
                        if (FormType == "TKMD")
                        {
                            container.TKMD_ID = TKMD.ID;
                        }
                        else
                        {
                            container.TKMD_ID = TKBoSung.ID;
                        }
                        if (container.ID == 0)
                            container.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        container.SoTN = Convert.ToInt64(txtSoTNContainer.Text);
                        container.NgayTN = clcNgayTNContainer.Value;
                        container.MaHQ = txtMaHQContainer.Text;
                        container.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        container.GhiChuKhac = txtGhiChuContainer.Text.ToString();
                        container.FileName = lblFileNamContainer.Text;
                        break;
                    case "AdditionalDocument":
                        if (FormType == "TKMD")
                        {
                            additionalDocument.TKMD_ID = TKMD.ID;
                        }
                        else
                        {
                            additionalDocument.TKMD_ID = TKBoSung.ID;
                        }
                        if (additionalDocument.ID == 0)
                            additionalDocument.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        additionalDocument.SoTN = Convert.ToInt64(txtSoTNCTKhac.Text);
                        additionalDocument.NgayTN = clcNgayTNCTKhac.Value;
                        additionalDocument.MaHQ = txtMaHQCTKhac.Text;
                        additionalDocument.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        additionalDocument.GhiChuKhac = txtGhiChuCTKhac.Text.ToString();
                        additionalDocument.FileName = lblFileNameCTKhac.Text;
                        additionalDocument.LoaiChungTu = Convert.ToInt32(cbbLoaiChungTu.SelectedValue.ToString());

                        additionalDocumentDetail.NoiPhatHanh = txtNoiPhatHanhCTKhac.Text.ToString();
                        if (additionalDocument.AdditionalDocumentCollection.Count > 0)
                        {
                            foreach (KDT_VNACCS_AdditionalDocument_Detail item in additionalDocument.AdditionalDocumentCollection)
                            {
                                if (item.AdditionalDocument_ID == additionalDocument.ID)
                                {
                                    item.SoChungTu = txtSoChungTuKhac.Text.ToString();
                                    item.TenChungTu = txtTenChungTuKhac.Text.ToString();
                                    item.NgayPhatHanh = dtpNgayChungTuKhac.Value;
                                }
                            }
                        }
                        else
                        {
                            additionalDocumentDetail.SoChungTu = txtSoChungTuKhac.Text.ToString();
                            additionalDocumentDetail.TenChungTu = txtTenChungTuKhac.Text.ToString();
                            additionalDocumentDetail.NgayPhatHanh = dtpNgayChungTuKhac.Value;
                        }
                        if (isAddCTkhac)
                            additionalDocument.AdditionalDocumentCollection.Add(additionalDocumentDetail);
                        break;
                    case "OverTime":
                        if (overTime.ID == 0)
                            overTime.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        overTime.SoTN = Convert.ToInt64(txtSoTNDKLNG.Text);
                        overTime.NgayTN = clcNgayTNDKLNG.Value;
                        overTime.MaHQ = txtMaHQDKLNG.Text;
                        overTime.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        overTime.NgayDangKy = dtpNgayDangKyThuTuc.Value;
                        overTime.GioLamThuTuc = dtpGioLamTT.Value.Hour + ":" + dtpGioLamTT.Value.Minute;
                        overTime.NoiDung = txtNoiDung.Text.ToString();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        public void ResultAll()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                switch (tbCtrVouchers.SelectedTab.Name)
                {
                    case "tpGiayPhep":
                        form.ItemID = license.ID;
                        form.DeclarationIssuer = DeclarationIssuer.License;
                        form.ShowDialog(this);
                        break;
                    case "tpHopDong":
                        form.ItemID = contractDocument.ID;
                        form.DeclarationIssuer = DeclarationIssuer.ContractDocument;
                        form.ShowDialog(this);
                        break;
                    case "tpHoaDon":
                        form.ItemID = commercialInvoice.ID;
                        form.DeclarationIssuer = DeclarationIssuer.CommercialInvoice;
                        form.ShowDialog(this);
                        break;
                    case "tpCO":
                        form.ItemID = certificateOfOrigin.ID;
                        form.DeclarationIssuer = DeclarationIssuer.CertificateOfOrigin;
                        form.ShowDialog(this);
                        break;
                    case "tpVanDon":
                        form.ItemID = billOfLading.ID;
                        form.DeclarationIssuer = DeclarationIssuer.BillOfLading;
                        form.ShowDialog(this);
                        break;
                    case "tpContainer":
                        form.ItemID = container.ID;
                        form.DeclarationIssuer = DeclarationIssuer.Containers;
                        form.ShowDialog(this);
                        break;
                    case "tpCTKhac":
                        form.ItemID = additionalDocument.ID;
                        form.DeclarationIssuer = DeclarationIssuer.AdditionalDocument;
                        form.ShowDialog(this);
                        break;
                    case "tpLamNgoaiGio":
                        form.ItemID = overTime.ID;
                        form.DeclarationIssuer = DeclarationIssuer.OverTime;
                        form.ShowDialog(this);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                switch (tbCtrVouchers.SelectedTab.Name)
                {
                    case "tpGiayPhep":
                        isValid &= ValidateControl.ValidateNull(txtNguoiCapGiayPhep, errorProvider, "NGƯỜI CẤP GIẤY PHÉP", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtSoGiayPhep, errorProvider, "SỐ GIẤY PHÉP", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(dtpNgayCapGiayPhep, errorProvider, "NGÀY CẤP GIẤY PHÉP", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtNoiCapGiayPhep, errorProvider, "NƠI CẤP GIẤY PHÉP", isOnlyWarning);
                        isValid &= ValidateControl.ValidateChoose(cbbLoaiGiayPhep, errorProvider, "LOẠI GIẤY PHÉP ", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(dtpNgayHetHanGiayPhep, errorProvider, "NGÀY HẾT HẠN GIẤY PHÉP ", isOnlyWarning);
                        break;
                    case "tpHopDong":
                        isValid &= ValidateControl.ValidateNull(txtSoHopDong, errorProvider, "SỐ HỢP ĐỒNG", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(dtpNgayHopDong, errorProvider, "NGÀY HỢP ĐỒNG", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(dtpThoiHanThanhToan, errorProvider, "THỜI HẠN THANH TOÁN", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtTongTriGiaHopDong, errorProvider, "TỔNG TRỊ GIÁ ", isOnlyWarning);
                        break;
                    case "tpHoaDon":
                        isValid &= ValidateControl.ValidateNull(txtSoHoaDonTM, errorProvider, "SỐ HÓA ĐƠN THƯƠNG MẠI", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(dtpNgayPhatHanhHoaDonTM, errorProvider, "NGÀY PHÁT HÀNH HÓA ĐƠN", isOnlyWarning);
                        break;
                    case "tpCO":
                        isValid &= ValidateControl.ValidateNull(txtSoCO, errorProvider, "SỐ CO", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(cbbLoaiCO, errorProvider, "LOẠI CO", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtToChucCapCO, errorProvider, "TỔ CHỨC CẤP", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtNguoiCapCO, errorProvider, "NGƯỜI CẤP", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(dateNgayCapCO, errorProvider, "NGÀY CẤP", isOnlyWarning);
                        //isValid &= ValidateControl.ValidateNull(txtNuocCapCO, "Người cấp", isOnlyWarning);
                        //isValid &= ValidateControl.ValidateNull(txtGhiChuCO, errorProvider, "Ghi chú khác ", isOnlyWarning);
                        break;
                    case "tpVanDon":
                        isValid &= ValidateControl.ValidateNull(txtSoVanDon, errorProvider, "SỐ VẬN ĐƠN", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(dtpNgayVanDon, errorProvider, "NGÀY VẬN ĐƠN", isOnlyWarning);
                        //isValid &= ValidateControl.ValidateNull(ctrNuocPHVanDon, errorProvider, "Nước phát hành", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtDiaDiemCTQC, errorProvider, "ĐỊA ĐIỂM CHUYỂN TẢI QUÁ CẢNH", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(cbbLoaiVanDon, errorProvider, "LOẠI VẬN ĐƠN ", isOnlyWarning);
                        break;
                    case "tpCTKhac":
                        isValid &= ValidateControl.ValidateNull(txtSoChungTuKhac, errorProvider, "SỐ CHỨNG TỪ", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtTenChungTuKhac, errorProvider, "TÊN CHỨNG TỪ", isOnlyWarning);
                        isValid &= ValidateControl.ValidateDate(dtpNgayChungTuKhac, errorProvider, "NGÀY CHỨNG TỪ", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtNoiPhatHanhCTKhac, errorProvider, "NƠI PHÁT HÀNH", isOnlyWarning);
                        isValid &= ValidateControl.ValidateChoose(cbbLoaiChungTu, errorProvider, "LOẠI CHỨNG TỪ", isOnlyWarning);
                        break;
                    case "tpLamNgoaiGio":
                        isValid &= ValidateControl.ValidateNull(txtNoiDung, errorProvider, "NỘI DUNG LÀM NGOÀI GIỜ", isOnlyWarning);
                        //isValid &= ValidateControl.ValidateNull(txtTenChungTuKhac, errorProvider, "Tên chứng từ", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(dtpGioLamTT, errorProvider, "GIỜ LÀM THỦ TỤC", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(dtpNgayDangKyThuTuc, errorProvider, "NGÀY ĐĂNG KÝ THỦ TỤC", isOnlyWarning);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void tbCtrVouchers_Selected(object sender, TabControlEventArgs e)
        {
            switch (tbCtrVouchers.SelectedTab.Name)
            {
                case "tpGiayPhep":
                    BindDataLicense();
                    break;
                case "tpHopDong":
                    BindDataContractDocument();
                    break;
                case "tpHoaDon":
                    BindDataCommercialInvoice();
                    break;
                case "tpCO":
                    BindDataCertificateOfOrigin();
                    break;
                case "tpVanDon":
                    BindDataBillOfLading();
                    break;
                case "tpContainer":
                    BindDataContainer();
                    break;
                case "tpCTKhac":
                    BindDataAdditionalDocument();
                    break;
                case "tpLamNgoaiGio":
                    BindDataOvertime();
                    break;
                default:
                    break;
            }
        }
        public static bool CheckExitsSignature(string source)
        {
            try
            {
                PdfReader reader = new PdfReader(source);
                List<string> blanks = reader.AcroFields.GetSignatureNames();
                if (blanks.Count == 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }
        public static void ConvertImageToPdf(string srcFilename, string dstFilename)
        {
            iTextSharp.text.Rectangle pageSize = null;

            using (var srcImage = new Bitmap(srcFilename))
            {
                pageSize = new iTextSharp.text.Rectangle(0, 0, srcImage.Width, srcImage.Height);
            }
            using (var ms = new MemoryStream())
            {
                var document = new iTextSharp.text.Document(pageSize, 0, 0, 0, 0);
                iTextSharp.text.pdf.PdfWriter.GetInstance(document, ms).SetFullCompression();
                document.Open();
                var image = iTextSharp.text.Image.GetInstance(srcFilename);
                document.Add(image);
                document.Close();

                File.WriteAllBytes(dstFilename, ms.ToArray());
            }
        }
        public void AttachFile(string Vouchers)
        {
            try
            {
                OpenFileDialog OpenFileDialog = new OpenFileDialog();

                OpenFileDialog.FileName = "";
                OpenFileDialog.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.bmp;*.tiff)|*.jpg;*.jpeg;*.gif;*.bmp;*.tiff;*.jpe;*.;*.jfif;*.rle;*.dib;*.png|"
                                        + "Text files (*.txt;*.xml;*xsl)|*.txt;*.xml;*xsl|"
                                        + "Application File (*.csv;*.doc;*.docx;*.mdb;*.pdf;*.ppt;*.xls;*.xlsx;)|*.csv;*.doc;*.docx;*.mdb;*.pdf;*.ppt;*.xls;*.xlsx";

                OpenFileDialog.Multiselect = false;
                if (OpenFileDialog.ShowDialog() == DialogResult.OK)
                {
                    System.IO.FileInfo fin = new System.IO.FileInfo(OpenFileDialog.FileName);
                    if (fin.Extension.ToUpper() != ".jpg".ToUpper()
                      && fin.Extension.ToUpper() != ".jpeg".ToUpper()
                      && fin.Extension.ToUpper() != ".gif".ToUpper()
                      && fin.Extension.ToUpper() != ".tiff".ToUpper()
                      && fin.Extension.ToUpper() != ".jpe".ToUpper()
                      && fin.Extension.ToUpper() != ".jfif".ToUpper()
                      && fin.Extension.ToUpper() != ".rle".ToUpper()
                      && fin.Extension.ToUpper() != ".dib".ToUpper()
                      && fin.Extension.ToUpper() != ".png".ToUpper()
                      && fin.Extension.ToUpper() != ".txt".ToUpper()
                      && fin.Extension.ToUpper() != ".xml".ToUpper()
                      && fin.Extension.ToUpper() != ".xsl".ToUpper()
                      && fin.Extension.ToUpper() != ".csv".ToUpper()
                      && fin.Extension.ToUpper() != ".doc".ToUpper()
                      && fin.Extension.ToUpper() != ".mdb".ToUpper()
                      && fin.Extension.ToUpper() != ".pdf".ToUpper()
                      && fin.Extension.ToUpper() != ".ppt".ToUpper()
                      && fin.Extension.ToUpper() != ".xls".ToUpper()
                      && fin.Extension.ToUpper() != ".xlsx".ToUpper()
                      && fin.Extension.ToUpper() != ".docx".ToUpper())
                    {
                        ShowMessageTQDT("TỆP TIN " + fin.Name + "KHÔNG ĐÚNG ĐỊNH DẠNG TIẾP NHẬN CỦA HẢI QUAN", false);
                    }
                    else
                    {
                        System.IO.FileStream fs = new System.IO.FileStream(OpenFileDialog.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                        size = 0;
                        size = fs.Length;
                        data = new byte[fs.Length];
                        fs.Read(data, 0, data.Length);
                        filebase64 = System.Convert.ToBase64String(data);
                        FileName = fin.Name;
                        fs.Flush();
                        fs.Close();
                        // Kiểm tra cấu hình ký trực tiếp hay qua LAN
                        if (Company.KDT.SHARE.Components.Globals.IsSignature)
                        {
                            // Kiểm tra định dạng File
                            if (fin.Extension.ToLower() == ".pdf")
                            {
                                // Kiểm tra dung lượng File
                                if (size > GlobalSettings.FileSize / 2)
                                {
                                    if (ShowMessageTQDT("\rĐỂ ĐẢM BẢO ĐƯỜNG TRUYỀN TỚI HỆ THỐNG TNTT KHI KHAI BÁO CHỨNG TỪ BỔ SUNG KHÔNG BỊ CHẬM, NGHẼN MẠNG.\n\rCƠ QUAN HẢI QUAN TẠM THỜI GIẢM DUNG LƯỢNG TỐI ĐA ĐƯỢC PHÉP KHAI BÁO CỦA THÔNG ĐIỆP XUỐNG CÒN 1.3MB (ĐÂY LÀ CHUNG DUNG LƯỢNG TOÀN BỘ NỘI DUNG KHAI BÁO ĐƯỢC GỬI LÊN).\n\rDUNG LƯỢNG FILE ĐÍNH KÈM CỦA DOANH NGHIỆP ĐANG NHẬP LÀ : " + CalculateFileSize(size) + " VƯỢT QUÁ DUNG LƯỢNG CHO PHÉP .\n\rPHẦN MỀM SẼ TỰ ĐỘNG NÉN LẠI FILE ĐÍNH KÈM ĐỂ GIẢM BỚT DUNG LƯỢNG .\n\rDOANH NGHIỆP NHẤN ĐỒNG Ý ĐỂ THỰC HIỆN NÉN . ", true) == "Yes")
                                    {
                                        //Thực hiện việc Nén File
                                        string dstFilename = Application.StartupPath + "\\Temp\\" + fin.Name.Replace(fin.Extension.ToString(), "_Compress.pdf");
                                        if (File.Exists(dstFilename))
                                        {
                                            File.Delete(dstFilename);
                                        }
                                        PdfReader reader = new PdfReader(fin.FullName);
                                        PdfStamper stamper = new PdfStamper(reader, new FileStream(dstFilename, FileMode.Create), PdfWriter.VERSION_1_5);
                                        PdfWriter writer = stamper.Writer;
                                        writer.SetPdfVersion(PdfWriter.PDF_VERSION_1_7);
                                        writer.CompressionLevel = PdfStream.BEST_COMPRESSION;

                                        int pageNum = reader.NumberOfPages;
                                        for (int i = 1; i <= pageNum; i++)
                                        {
                                            reader.SetPageContent(i, reader.GetPageContent(i));
                                        }
                                        stamper.SetFullCompression();
                                        stamper.Close();

                                        fs = new System.IO.FileStream(dstFilename, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                                        size = 0;
                                        size = fs.Length;
                                        data = new byte[fs.Length];
                                        fs.Read(data, 0, data.Length);
                                        filebase64 = System.Convert.ToBase64String(data);
                                        FileName = Path.GetFileName(dstFilename);
                                        fs.Flush();
                                        fs.Close();
                                        // Thực hiện Ký File
                                        X509Certificate2 x509Certificate2 = SoftechSignLibrary.GetCertificate(parentFormHandler);
                                        if (SoftechSignLibrary.SoftechSign(x509Certificate2, data, ref dataSigned))
                                        {
                                            filebase64 = System.Convert.ToBase64String(dataSigned);
                                            ShowMessageTQDT("NÉN VÀ KÝ FILE PDF THÀNH CÔNG ", false);
                                        }
                                        else
                                        {
                                            ShowMessageTQDT("KÝ KHÔNG THÀNH CÔNG . DOANH NGHIỆP KIỂM TRA LẠI CHỮ KÝ SỐ .", false);
                                        }

                                    }
                                    else
                                    {
                                        if (CheckExitsSignature(fin.FullName))
                                        {
                                            X509Certificate2 x509Certificate2 = SoftechSignLibrary.GetCertificate(parentFormHandler);
                                            if (SoftechSignLibrary.SoftechSign(x509Certificate2, data, ref dataSigned))
                                            {
                                                fs = new System.IO.FileStream(fin.FullName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                                                size = 0;
                                                size = fs.Length;
                                                data = new byte[fs.Length];
                                                fs.Read(data, 0, data.Length);
                                                filebase64 = System.Convert.ToBase64String(data);
                                                fs.Flush();
                                                fs.Close();
                                                ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                            }
                                            else
                                            {
                                                ShowMessageTQDT("KÝ KHÔNG THÀNH CÔNG . DOANH NGHIỆP KIỂM TRA LẠI CHỮ KÝ SỐ .", false);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (CheckExitsSignature(fin.FullName))
                                    {
                                        X509Certificate2 x509Certificate2 = SoftechSignLibrary.GetCertificate(parentFormHandler);
                                        if (SoftechSignLibrary.SoftechSign(x509Certificate2, data, ref dataSigned))
                                        {
                                            filebase64 = System.Convert.ToBase64String(dataSigned);
                                            ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        }
                                        else
                                        {
                                            ShowMessageTQDT("KÝ KHÔNG THÀNH CÔNG . DOANH NGHIỆP KIỂM TRA LẠI CHỮ KÝ SỐ .", false);
                                        }
                                    }
                                }

                            }
                            else if (fin.Extension.ToLower() == ".xls" || fin.Extension.ToLower() == ".xlsx" || fin.Extension.ToLower() == ".docx" || fin.Extension.ToLower() == ".doc")
                            {
                                if (!SoftechSignLibrary.isSignedExcel(fin.FullName))
                                {
                                    X509Certificate2 x509Certificate2 = SoftechSignLibrary.GetCertificate(parentFormHandler);
                                    if (SoftechSignLibrary.SignOfficeDocument(fin.FullName, x509Certificate2))
                                    {
                                        if (SoftechSignLibrary.isVerifySignedExcel(fin.FullName))
                                        {
                                            fs = new System.IO.FileStream(fin.FullName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                                            size = 0;
                                            size = fs.Length;
                                            data = new byte[fs.Length];
                                            fs.Read(data, 0, data.Length);
                                            filebase64 = System.Convert.ToBase64String(data);
                                            fs.Flush();
                                            fs.Close();
                                            ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        }
                                        else
                                        {
                                            ShowMessageTQDT("KÝ KHÔNG THÀNH CÔNG . DOANH NGHIỆP KIỂM TRA LẠI CHỮ KÝ SỐ .", false);
                                        }
                                    }
                                }
                            }
                            else if ((fin.Extension.ToLower() == ".jpg" || fin.Extension.ToLower() == ".jpeg" || fin.Extension.ToLower() == ".jpe" || fin.Extension.ToLower() == ".gif" || fin.Extension.ToLower() == ".jfif" || fin.Extension.ToLower() == ".bmp" || fin.Extension.ToLower() == ".dib" || fin.Extension.ToLower() == ".rle" || fin.Extension.ToLower() == ".png" || fin.Extension.ToLower() == ".tiff"))
                            {
                                if (ShowMessageTQDT("FILE DOANH NGHIỆP ĐANG CHỌN LÀ ĐỊNH DẠNG FILE HÌNH ẢNH .\rPHẦN MỀM SẼ TỰ ĐỘNG CONVERT FILE HÌNH ẢNH THÀNH PDF .\rSAU KHI CHUYỂN THÀNH FILE PDF PHẦN MỀM SẼ TỰ ĐỘNG KÝ CHỮ KÝ SỐ CHO FILE NÀY .\rDOANH NGHIỆP NHẤN ĐỒNG Ý ĐỂ THỰC HIỆN ", true) == "Yes")
                                {
                                    string dstFilename = Application.StartupPath + "\\Temp\\" + fin.Name.Replace(fin.Extension.ToString(), ".pdf");
                                    if (File.Exists(dstFilename))
                                    {
                                        File.Delete(dstFilename);
                                    }
                                    ConvertImageToPdf(fin.FullName, dstFilename);

                                    fs = new System.IO.FileStream(dstFilename, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                                    size = 0;
                                    size = fs.Length;
                                    data = new byte[fs.Length];
                                    fs.Read(data, 0, data.Length);
                                    filebase64 = System.Convert.ToBase64String(data);
                                    FileName = Path.GetFileName(dstFilename);
                                    fs.Flush();
                                    fs.Close();
                                    X509Certificate2 x509Certificate2 = SoftechSignLibrary.GetCertificate(parentFormHandler);
                                    if (SoftechSignLibrary.SoftechSign(x509Certificate2, data, ref dataSigned))
                                    {
                                        filebase64 = System.Convert.ToBase64String(dataSigned);
                                        ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                    }
                                    else
                                    {
                                        ShowMessageTQDT("KÝ KHÔNG THÀNH CÔNG . DOANH NGHIỆP KIỂM TRA LẠI CHỮ KÝ SỐ . .", false);
                                    }
                                }
                            }
                        }
                        else if (Company.KDT.SHARE.Components.Globals.IsSignOnLan)
                        {
                            if (fin.Extension.ToLower() == ".pdf")
                            {
                                if (size > GlobalSettings.FileSize / 2)
                                {
                                    if (ShowMessageTQDT("\rĐỂ ĐẢM BẢO ĐƯỜNG TRUYỀN TỚI HỆ THỐNG TNTT KHI KHAI BÁO CHỨNG TỪ BỔ SUNG KHÔNG BỊ CHẬM, NGHẼN MẠNG.\n\rCƠ QUAN HẢI QUAN TẠM THỜI GIẢM DUNG LƯỢNG TỐI ĐA ĐƯỢC PHÉP KHAI BÁO CỦA THÔNG ĐIỆP XUỐNG CÒN 1.3MB (ĐÂY LÀ CHUNG DUNG LƯỢNG TOÀN BỘ NỘI DUNG KHAI BÁO ĐƯỢC GỬI LÊN).\n\rDUNG LƯỢNG FILE ĐÍNH KÈM CỦA DOANH NGHIỆP ĐANG NHẬP LÀ : " + CalculateFileSize(size) + " VƯỢT QUÁ DUNG LƯỢNG CHO PHÉP .\n\rPHẦN MỀM SẼ TỰ ĐỘNG NÉN LẠI FILE ĐÍNH KÈM ĐỂ GIẢM BỚT DUNG LƯỢNG .\n\rDOANH NGHIỆP NHẤN ĐỒNG Ý ĐỂ THỰC HIỆN NÉN . ", true) == "Yes")
                                    {
                                        //Thực hiện việc Nén File
                                        string dstFilename = Application.StartupPath + "\\Temp\\" + fin.Name.Replace(fin.Extension.ToString(), "_Compress.pdf");
                                        if (File.Exists(dstFilename))
                                        {
                                            File.Delete(dstFilename);
                                        }
                                        PdfReader reader = new PdfReader(fin.FullName);
                                        PdfStamper stamper = new PdfStamper(reader, new FileStream(dstFilename, FileMode.Create), PdfWriter.VERSION_1_5);
                                        PdfWriter writer = stamper.Writer;
                                        writer.SetPdfVersion(PdfWriter.PDF_VERSION_1_7);
                                        writer.CompressionLevel = PdfStream.BEST_COMPRESSION;

                                        int pageNum = reader.NumberOfPages;
                                        for (int i = 1; i <= pageNum; i++)
                                        {
                                            reader.SetPageContent(i, reader.GetPageContent(i));
                                        }
                                        stamper.SetFullCompression();
                                        stamper.Close();

                                        fs = new System.IO.FileStream(dstFilename, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                                        size = 0;
                                        size = fs.Length;
                                        data = new byte[fs.Length];
                                        fs.Read(data, 0, data.Length);
                                        filebase64 = System.Convert.ToBase64String(data);
                                        FileName = Path.GetFileName(dstFilename);
                                        fs.Flush();
                                        fs.Close();
                                        // Thực hiện Ký File
                                        SendMessageForm sendForm = new SendMessageForm();
                                        sendForm.FileName = FileName;
                                        sendForm.filebase64 = filebase64;
                                        sendForm.Extends = fin.Extension.ToLower();
                                        sendForm.Send += SendMessage;
                                        bool isSend = sendForm.DoSendFile(null);
                                        if (isSend && !String.IsNullOrEmpty(sendForm.FileData))
                                        {
                                            FileName = sendForm.FileName;
                                            filebase64 = sendForm.FileData;
                                            ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        }
                                        else
                                        {
                                            ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                        }
                                        //int count = 0;
                                        //int timeWait = 15;
                                        //bool isSuccess = false;
                                        //while (count < timeWait)
                                        //{
                                        //    MSG_FILE_OUTBOX MSG_FILE_OUTBOX = GetFileSignatureOnLan(FileName, filebase64);
                                        //    if (MSG_FILE_OUTBOX != null)
                                        //    {
                                        //        FileName = MSG_FILE_OUTBOX.FILE_NAME;
                                        //        filebase64 = MSG_FILE_OUTBOX.FILE_DATA;
                                        //        ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        //        isSuccess = true;
                                        //        break;
                                        //    }
                                        //    else
                                        //    {
                                        //        count++;
                                        //        Thread.Sleep(1000);
                                        //    }
                                        //}
                                        //if (!isSuccess)
                                        //{
                                        //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                        //}
                                    }
                                    else
                                    {
                                        if (CheckExitsSignature(fin.FullName))
                                        {
                                            SendMessageForm sendForm = new SendMessageForm();
                                            sendForm.FileName = FileName;
                                            sendForm.filebase64 = filebase64;
                                            sendForm.Extends = fin.Extension.ToLower();
                                            sendForm.Send += SendMessage;
                                            bool isSend = sendForm.DoSendFile(null);
                                            if (isSend && !String.IsNullOrEmpty(sendForm.FileData))
                                            {
                                                FileName = sendForm.FileName;
                                                filebase64 = sendForm.FileData;
                                                ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                            }
                                            else
                                            {
                                                ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                            }
                                            //bool isSuccess = false;
                                            //MSG_FILE_OUTBOX MSG_FILE_OUTBOX = null;
                                            //Thread t = new Thread(() => MSG_FILE_OUTBOX = GetFileSignatureOnLan(FileName, filebase64));
                                            //t.Start();
                                            //t.Join();
                                            //if (MSG_FILE_OUTBOX != null)
                                            //{
                                            //    FileName = MSG_FILE_OUTBOX.FILE_NAME;
                                            //    filebase64 = MSG_FILE_OUTBOX.FILE_DATA;
                                            //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                            //    isSuccess = true;
                                            //}
                                            //int count = 0;
                                            //int timeWait = 15;
                                            //bool isSuccess = false;
                                            //while (count < timeWait)
                                            //{
                                            //    MSG_FILE_OUTBOX MSG_FILE_OUTBOX = GetFileSignatureOnLan(FileName, filebase64);
                                            //    if (MSG_FILE_OUTBOX != null)
                                            //    {
                                            //        FileName = MSG_FILE_OUTBOX.FILE_NAME;
                                            //        filebase64 = MSG_FILE_OUTBOX.FILE_DATA;
                                            //        ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                            //        isSuccess = true;
                                            //        break;
                                            //    }
                                            //    else
                                            //    {
                                            //        count++;
                                            //        Thread.Sleep(1000);
                                            //    }
                                            //}
                                            //if (!isSuccess)
                                            //{
                                            //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                            //}
                                        }
                                    }
                                }
                                else
                                {
                                    if (CheckExitsSignature(fin.FullName))
                                    {
                                        //int count = 0;
                                        //int timeWait = 15;
                                        //while (count < timeWait)
                                        //{
                                        //    //MSG_FILE_OUTBOX MSG_FILE_OUTBOX = GetFileSignatureOnLan(FileName, filebase64);
                                        //    DoSend();
                                        //    if (isSignSuccess)
                                        //    {
                                        //        FileName = MSG_FILE_OUTBOX.FILE_NAME;
                                        //        filebase64 = MSG_FILE_OUTBOX.FILE_DATA;
                                        //        ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        //        break;
                                        //    }
                                        //    else
                                        //    {
                                        //        count++;
                                        //        Thread.Sleep(1000);
                                        //    }
                                        //}
                                        //if (!isSignSuccess)
                                        //{
                                        //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                        //}

                                        SendMessageForm sendForm = new SendMessageForm();
                                        sendForm.FileName = FileName;
                                        sendForm.filebase64 = filebase64;
                                        sendForm.Extends = fin.Extension.ToLower();
                                        sendForm.Send += SendMessage;
                                        bool isSend = sendForm.DoSendFile(null);
                                        if (isSend && !String.IsNullOrEmpty(sendForm.FileData))
                                        {
                                            FileName = sendForm.FileName;
                                            filebase64 = sendForm.FileData;
                                            ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        }
                                        else
                                        {
                                            ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                        }

                                        //bool isSuccess = false;
                                        //MSG_FILE_OUTBOX MSG_FILE_OUTBOX = null;
                                        //Thread t = new Thread(() => MSG_FILE_OUTBOX = GetFileSignatureOnLan(FileName, filebase64));
                                        //t.Start();
                                        //t.Join();
                                        //if (MSG_FILE_OUTBOX != null)
                                        //{
                                        //    FileName = MSG_FILE_OUTBOX.FILE_NAME;
                                        //    filebase64 = MSG_FILE_OUTBOX.FILE_DATA;
                                        //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        //    isSuccess = true;
                                        //}
                                        //int count = 0;
                                        //int timeWait = 15;
                                        //bool isSuccess = false;
                                        //while (count < timeWait)
                                        //{
                                        //    MSG_FILE_OUTBOX MSG_FILE_OUTBOX = GetFileSignatureOnLan(FileName, filebase64);
                                        //    if (MSG_FILE_OUTBOX != null)
                                        //    {
                                        //        FileName = MSG_FILE_OUTBOX.FILE_NAME;
                                        //        filebase64 = MSG_FILE_OUTBOX.FILE_DATA;
                                        //        ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        //        isSuccess = true;
                                        //        break;
                                        //    }
                                        //    else
                                        //    {
                                        //        count++;
                                        //        Thread.Sleep(1000);
                                        //    }
                                        //}

                                        //if (!isSuccess)
                                        //{
                                        //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                        //}
                                    }
                                }
                            }
                            else if ((fin.Extension.ToLower() == ".jpg" || fin.Extension.ToLower() == ".jpeg" || fin.Extension.ToLower() == ".jpe" || fin.Extension.ToLower() == ".gif" || fin.Extension.ToLower() == ".jfif" || fin.Extension.ToLower() == ".bmp" || fin.Extension.ToLower() == ".dib" || fin.Extension.ToLower() == ".rle" || fin.Extension.ToLower() == ".png" || fin.Extension.ToLower() == ".tiff"))
                            {
                                if (ShowMessageTQDT("FILE DOANH NGHIỆP ĐANG CHỌN LÀ ĐỊNH DẠNG FILE HÌNH ẢNH .\rPHẦN MỀM SẼ TỰ ĐỘNG CONVERT FILE HÌNH ẢNH THÀNH PDF .\rSAU KHI CHUYỂN THÀNH FILE PDF PHẦN MỀM SẼ TỰ ĐỘNG KÝ CHỮ KÝ SỐ CHO FILE NÀY .\rDOANH NGHIỆP NHẤN ĐỒNG Ý ĐỂ THỰC HIỆN ", true) == "Yes")
                                {
                                    string dstFilename = Application.StartupPath + "\\Temp\\" + fin.Name.Replace(fin.Extension.ToString(), ".pdf");
                                    if (File.Exists(dstFilename))
                                    {
                                        File.Delete(dstFilename);
                                    }
                                    ConvertImageToPdf(fin.FullName, dstFilename);

                                    fs = new System.IO.FileStream(dstFilename, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                                    size = 0;
                                    size = fs.Length;
                                    data = new byte[fs.Length];
                                    fs.Read(data, 0, data.Length);
                                    filebase64 = System.Convert.ToBase64String(data);
                                    FileName = Path.GetFileName(dstFilename);
                                    fs.Flush();
                                    fs.Close();
                                    // Ký qua mạng Lan
                                    SendMessageForm sendForm = new SendMessageForm();
                                    sendForm.FileName = FileName;
                                    sendForm.filebase64 = filebase64;
                                    sendForm.Extends = fin.Extension.ToLower();
                                    sendForm.Send += SendMessage;
                                    bool isSend = sendForm.DoSendFile(null);
                                    if (isSend && !String.IsNullOrEmpty(sendForm.FileData))
                                    {
                                        FileName = sendForm.FileName;
                                        filebase64 = sendForm.FileData;
                                        ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                    }
                                    else
                                    {
                                        ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                    }
                                    //bool isSuccess = false;
                                    //MSG_FILE_OUTBOX MSG_FILE_OUTBOX = null;
                                    //Thread t = new Thread(() => MSG_FILE_OUTBOX = GetFileSignatureOnLan(FileName, filebase64));
                                    //t.Start();
                                    //t.Join();
                                    //if (MSG_FILE_OUTBOX != null)
                                    //{
                                    //    FileName = MSG_FILE_OUTBOX.FILE_NAME;
                                    //    filebase64 = MSG_FILE_OUTBOX.FILE_DATA;
                                    //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                    //    isSuccess = true;
                                    //}
                                    //int count = 0;
                                    //int timeWait = 15;
                                    //bool isSuccess = false;
                                    //while (count < timeWait)
                                    //{
                                    //    MSG_FILE_OUTBOX MSG_FILE_OUTBOX = GetFileSignatureOnLan(FileName, filebase64);
                                    //    if (MSG_FILE_OUTBOX != null)
                                    //    {
                                    //        FileName = MSG_FILE_OUTBOX.FILE_NAME;
                                    //        filebase64 = MSG_FILE_OUTBOX.FILE_DATA;
                                    //        ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                    //        isSuccess = true;
                                    //        break;
                                    //    }
                                    //    else
                                    //    {
                                    //        count++;
                                    //        Thread.Sleep(1000);
                                    //    }
                                    //}
                                    //if (!isSuccess)
                                    //{
                                    //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                    //}
                                }
                            }
                        }
                        else if (Company.KDT.SHARE.Components.Globals.IsSignRemote)
                        {
                            if (fin.Extension.ToLower() == ".pdf")
                            {
                                if (size > GlobalSettings.FileSize / 2)
                                {
                                    if (ShowMessageTQDT("\rĐỂ ĐẢM BẢO ĐƯỜNG TRUYỀN TỚI HỆ THỐNG TNTT KHI KHAI BÁO CHỨNG TỪ BỔ SUNG KHÔNG BỊ CHẬM, NGHẼN MẠNG.\n\rCƠ QUAN HẢI QUAN TẠM THỜI GIẢM DUNG LƯỢNG TỐI ĐA ĐƯỢC PHÉP KHAI BÁO CỦA THÔNG ĐIỆP XUỐNG CÒN 1.3MB (ĐÂY LÀ CHUNG DUNG LƯỢNG TOÀN BỘ NỘI DUNG KHAI BÁO ĐƯỢC GỬI LÊN).\n\rDUNG LƯỢNG FILE ĐÍNH KÈM CỦA DOANH NGHIỆP ĐANG NHẬP LÀ : " + CalculateFileSize(size) + " VƯỢT QUÁ DUNG LƯỢNG CHO PHÉP .\n\rPHẦN MỀM SẼ TỰ ĐỘNG NÉN LẠI FILE ĐÍNH KÈM ĐỂ GIẢM BỚT DUNG LƯỢNG .\n\rDOANH NGHIỆP NHẤN ĐỒNG Ý ĐỂ THỰC HIỆN NÉN . ", true) == "Yes")
                                    {
                                        //Thực hiện việc Nén File
                                        string dstFilename = Application.StartupPath + "\\Temp\\" + fin.Name.Replace(fin.Extension.ToString(), "_Compress.pdf");
                                        if (File.Exists(dstFilename))
                                        {
                                            File.Delete(dstFilename);
                                        }
                                        PdfReader reader = new PdfReader(fin.FullName);
                                        PdfStamper stamper = new PdfStamper(reader, new FileStream(dstFilename, FileMode.Create), PdfWriter.VERSION_1_5);
                                        PdfWriter writer = stamper.Writer;
                                        writer.SetPdfVersion(PdfWriter.PDF_VERSION_1_7);
                                        writer.CompressionLevel = PdfStream.BEST_COMPRESSION;

                                        int pageNum = reader.NumberOfPages;
                                        for (int i = 1; i <= pageNum; i++)
                                        {
                                            reader.SetPageContent(i, reader.GetPageContent(i));
                                        }
                                        stamper.SetFullCompression();
                                        stamper.Close();

                                        fs = new System.IO.FileStream(dstFilename, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                                        size = 0;
                                        size = fs.Length;
                                        data = new byte[fs.Length];
                                        fs.Read(data, 0, data.Length);
                                        filebase64 = System.Convert.ToBase64String(data);
                                        FileName = Path.GetFileName(dstFilename);
                                        fs.Flush();
                                        fs.Close();
                                        // Thực hiện Ký File

                                        SendMessageForm sendForm = new SendMessageForm();
                                        sendForm.FileName = FileName;
                                        sendForm.filebase64 = filebase64;
                                        sendForm.Extends = fin.Extension.ToLower();
                                        sendForm.Send += SendMessage;
                                        bool isSend = sendForm.DoSendFile(null);
                                        if (isSend && !String.IsNullOrEmpty(sendForm.FileData))
                                        {
                                            FileName = sendForm.FileName;
                                            filebase64 = sendForm.FileData;
                                            ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        }
                                        else
                                        {
                                            ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                        }

                                        //SignRemote.MessageFile MessageFile = null;
                                        //Thread t = new Thread(() => MessageFile = GetSignatureRemote(FileName, filebase64));
                                        //t.Start();
                                        //t.Join();
                                        //if (MessageFile != null)
                                        //{
                                        //    FileName = MessageFile.FileName;
                                        //    filebase64 = MessageFile.FileData;
                                        //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        //}
                                        //else
                                        //{
                                        //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                        //}
                                        //SignRemote.MessageFile MessageFile = GetSignatureRemote(FileName, filebase64);
                                        //if (MessageFile != null)
                                        //{
                                        //    FileName = MessageFile.FileName;
                                        //    filebase64 = MessageFile.FileData;
                                        //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        //}
                                        //else
                                        //{
                                        //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                        //}
                                    }
                                    else
                                    {
                                        if (CheckExitsSignature(fin.FullName))
                                        {
                                            SendMessageForm sendForm = new SendMessageForm();
                                            sendForm.FileName = FileName;
                                            sendForm.filebase64 = filebase64;
                                            sendForm.Extends = fin.Extension.ToLower();
                                            sendForm.Send += SendMessage;
                                            bool isSend = sendForm.DoSendFile(null);
                                            if (isSend && !String.IsNullOrEmpty(sendForm.FileData))
                                            {
                                                FileName = sendForm.FileName;
                                                filebase64 = sendForm.FileData;
                                                ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                            }
                                            else
                                            {
                                                ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                            }

                                            //SignRemote.MessageFile MessageFile = null;
                                            //Thread t = new Thread(() => MessageFile = GetSignatureRemote(FileName, filebase64));
                                            //t.Start();
                                            //t.Join();
                                            //if (MessageFile != null)
                                            //{
                                            //    FileName = MessageFile.FileName;
                                            //    filebase64 = MessageFile.FileData;
                                            //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                            //}
                                            //else
                                            //{
                                            //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                            //}
                                            //SignRemote.MessageFile MessageFile = GetSignatureRemote(FileName, filebase64);
                                            //if (MessageFile != null)
                                            //{
                                            //    FileName = MessageFile.FileName;
                                            //    filebase64 = MessageFile.FileData;
                                            //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                            //}
                                            //else
                                            //{
                                            //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                            //}
                                        }
                                    }
                                }
                                else
                                {
                                    if (CheckExitsSignature(fin.FullName))
                                    {
                                        SendMessageForm sendForm = new SendMessageForm();
                                        sendForm.FileName = FileName;
                                        sendForm.filebase64 = filebase64;
                                        sendForm.Extends = fin.Extension.ToLower();
                                        sendForm.Send += SendMessage;
                                        bool isSend = sendForm.DoSendFile(null);
                                        if (isSend && !String.IsNullOrEmpty(sendForm.FileData))
                                        {
                                            FileName = sendForm.FileName;
                                            filebase64 = sendForm.FileData;
                                            ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        }
                                        else
                                        {
                                            ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                        }

                                        //SignRemote.MessageFile MessageFile = null;
                                        //Thread t = new Thread(() => MessageFile = GetSignatureRemote(FileName, filebase64));
                                        //t.Start();
                                        //t.Join();
                                        //if (MessageFile != null)
                                        //{
                                        //    FileName = MessageFile.FileName;
                                        //    filebase64 = MessageFile.FileData;
                                        //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        //}
                                        //else
                                        //{
                                        //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                        //}

                                        //SignRemote.MessageFile MessageFile = GetSignatureRemote(FileName, filebase64);
                                        //if (MessageFile != null)
                                        //{
                                        //    FileName = MessageFile.FileName;
                                        //    filebase64 = MessageFile.FileData;
                                        //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        //}
                                        //else
                                        //{
                                        //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                        //}
                                    }
                                }
                            }
                            else if ((fin.Extension.ToLower() == ".jpg" || fin.Extension.ToLower() == ".jpeg" || fin.Extension.ToLower() == ".jpe" || fin.Extension.ToLower() == ".gif" || fin.Extension.ToLower() == ".jfif" || fin.Extension.ToLower() == ".bmp" || fin.Extension.ToLower() == ".dib" || fin.Extension.ToLower() == ".rle" || fin.Extension.ToLower() == ".png" || fin.Extension.ToLower() == ".tiff"))
                            {
                                if (ShowMessageTQDT("FILE DOANH NGHIỆP ĐANG CHỌN LÀ ĐỊNH DẠNG FILE HÌNH ẢNH .\rPHẦN MỀM SẼ TỰ ĐỘNG CONVERT FILE HÌNH ẢNH THÀNH PDF .\rSAU KHI CHUYỂN THÀNH FILE PDF PHẦN MỀM SẼ TỰ ĐỘNG KÝ CHỮ KÝ SỐ CHO FILE NÀY .\rDOANH NGHIỆP NHẤN ĐỒNG Ý ĐỂ THỰC HIỆN ", true) == "Yes")
                                {
                                    string dstFilename = Application.StartupPath + "\\Temp\\" + fin.Name.Replace(fin.Extension.ToString(), ".pdf");
                                    if (File.Exists(dstFilename))
                                    {
                                        File.Delete(dstFilename);
                                    }
                                    ConvertImageToPdf(fin.FullName, dstFilename);

                                    fs = new System.IO.FileStream(dstFilename, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                                    size = 0;
                                    size = fs.Length;
                                    data = new byte[fs.Length];
                                    fs.Read(data, 0, data.Length);
                                    filebase64 = System.Convert.ToBase64String(data);
                                    FileName = Path.GetFileName(dstFilename);
                                    fs.Flush();
                                    fs.Close();
                                    // Ký từ xa
                                    SendMessageForm sendForm = new SendMessageForm();
                                    sendForm.FileName = FileName;
                                    sendForm.filebase64 = filebase64;
                                    sendForm.Extends = fin.Extension.ToLower();
                                    sendForm.Send += SendMessage;
                                    bool isSend = sendForm.DoSendFile(null);
                                    if (isSend && !String.IsNullOrEmpty(sendForm.FileData))
                                    {
                                        FileName = sendForm.FileName;
                                        filebase64 = sendForm.FileData;
                                        ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                    }
                                    else
                                    {
                                        ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);

                                    }
                                    //SignRemote.MessageFile MessageFile = null;
                                    //Thread t = new Thread(() => MessageFile = GetSignatureRemote(FileName, filebase64));
                                    //t.Start();
                                    //t.Join();
                                    //if (MessageFile != null)
                                    //{
                                    //    FileName = MessageFile.FileName;
                                    //    filebase64 = MessageFile.FileData;
                                    //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                    //}
                                    //else
                                    //{
                                    //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                    //}

                                    //SignRemote.MessageFile MessageFile = GetSignatureRemote(FileName, filebase64);
                                    //if (MessageFile != null)
                                    //{
                                    //    FileName = MessageFile.FileName;
                                    //    filebase64 = MessageFile.FileData;
                                    //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                    //}
                                    //else
                                    //{
                                    //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ", false);
                                    //}
                                }
                            }
                        }
                        else if (Company.KDT.SHARE.Components.Globals.IsSignSmartCA)
                        {
                            if (fin.Extension.ToLower() == ".pdf")
                            {
                                if (size > GlobalSettings.FileSize / 2)
                                {
                                    if (ShowMessageTQDT("\rĐỂ ĐẢM BẢO ĐƯỜNG TRUYỀN TỚI HỆ THỐNG TNTT KHI KHAI BÁO CHỨNG TỪ BỔ SUNG KHÔNG BỊ CHẬM, NGHẼN MẠNG.\n\rCƠ QUAN HẢI QUAN TẠM THỜI GIẢM DUNG LƯỢNG TỐI ĐA ĐƯỢC PHÉP KHAI BÁO CỦA THÔNG ĐIỆP XUỐNG CÒN 1.3MB (ĐÂY LÀ CHUNG DUNG LƯỢNG TOÀN BỘ NỘI DUNG KHAI BÁO ĐƯỢC GỬI LÊN).\n\rDUNG LƯỢNG FILE ĐÍNH KÈM CỦA DOANH NGHIỆP ĐANG NHẬP LÀ : " + CalculateFileSize(size) + " VƯỢT QUÁ DUNG LƯỢNG CHO PHÉP .\n\rPHẦN MỀM SẼ TỰ ĐỘNG NÉN LẠI FILE ĐÍNH KÈM ĐỂ GIẢM BỚT DUNG LƯỢNG .\n\rDOANH NGHIỆP NHẤN ĐỒNG Ý ĐỂ THỰC HIỆN NÉN . ", true) == "Yes")
                                    {
                                        //Thực hiện việc Nén File
                                        string dstFilename = Application.StartupPath + "\\Temp\\" + fin.Name.Replace(fin.Extension.ToString(), "_Compress.pdf");
                                        if (File.Exists(dstFilename))
                                        {
                                            File.Delete(dstFilename);
                                        }
                                        PdfReader reader = new PdfReader(fin.FullName);
                                        PdfStamper stamper = new PdfStamper(reader, new FileStream(dstFilename, FileMode.Create), PdfWriter.VERSION_1_5);
                                        PdfWriter writer = stamper.Writer;
                                        writer.SetPdfVersion(PdfWriter.PDF_VERSION_1_7);
                                        writer.CompressionLevel = PdfStream.BEST_COMPRESSION;

                                        int pageNum = reader.NumberOfPages;
                                        for (int i = 1; i <= pageNum; i++)
                                        {
                                            reader.SetPageContent(i, reader.GetPageContent(i));
                                        }
                                        stamper.SetFullCompression();
                                        stamper.Close();

                                        fs = new System.IO.FileStream(dstFilename, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                                        size = 0;
                                        size = fs.Length;
                                        data = new byte[fs.Length];
                                        fs.Read(data, 0, data.Length);
                                        filebase64 = System.Convert.ToBase64String(data);
                                        FileName = Path.GetFileName(dstFilename);
                                        fs.Flush();
                                        fs.Close();
                                        // Thực hiện Ký File
                                        SendMessageForm sendForm = new SendMessageForm();
                                        sendForm.FileName = FileName;
                                        sendForm.filebase64 = filebase64;
                                        sendForm.Extends = fin.Extension.ToLower();
                                        sendForm.Send += SendMessage;
                                        bool isSend = sendForm.DoSendFile(null);
                                        if (isSend && !String.IsNullOrEmpty(sendForm.FileData))
                                        {
                                            filebase64 = sendForm.FileData;
                                            ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        }
                                        else
                                        {
                                            ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN NGƯỜI DÙNG ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU", false);

                                        }
                                        //string Filebase64 = "";
                                        //Thread t = new Thread(() => Filebase64 = GetFileSignatureSmartCA(FileName, filebase64, fin.Extension.ToLower()));
                                        //t.Start();
                                        //t.Join();
                                        //if (Filebase64 != null)
                                        //{
                                        //    filebase64 = Filebase64;
                                        //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        //}
                                        //else
                                        //{
                                        //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN NGƯỜI DÙNG ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU", false);
                                        //}
                                    }
                                    else
                                    {
                                        if (CheckExitsSignature(fin.FullName))
                                        {
                                            SendMessageForm sendForm = new SendMessageForm();
                                            sendForm.FileName = FileName;
                                            sendForm.filebase64 = filebase64;
                                            sendForm.Extends = fin.Extension.ToLower();
                                            sendForm.Send += SendMessage;
                                            bool isSend = sendForm.DoSendFile(null);
                                            if (isSend && !String.IsNullOrEmpty(sendForm.FileData))
                                            {
                                                filebase64 = sendForm.FileData;
                                                ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                            }
                                            else
                                            {
                                                ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN NGƯỜI DÙNG ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU", false);

                                            }
                                            //string Filebase64 = "";
                                            //Thread t = new Thread(() => Filebase64 = GetFileSignatureSmartCA(FileName, filebase64, fin.Extension.ToLower()));
                                            //t.Start();
                                            //t.Join();
                                            //if (Filebase64 != null)
                                            //{
                                            //    filebase64 = Filebase64;
                                            //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                            //}
                                            //else
                                            //{
                                            //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN NGƯỜI DÙNG ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU", false);
                                            //}
                                        }
                                    }
                                }
                                else
                                {
                                    if (CheckExitsSignature(fin.FullName))
                                    {
                                        SendMessageForm sendForm = new SendMessageForm();
                                        sendForm.FileName = FileName;
                                        sendForm.filebase64 = filebase64;
                                        sendForm.Extends = fin.Extension.ToLower();
                                        sendForm.Send += SendMessage;
                                        bool isSend = sendForm.DoSendFile(null);
                                        if (isSend && !String.IsNullOrEmpty(sendForm.FileData))
                                        {
                                            filebase64 = sendForm.FileData;
                                            ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        }
                                        else
                                        {
                                            ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN NGƯỜI DÙNG ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU", false);

                                        }
                                        //string Filebase64 = "";
                                        //Thread t = new Thread(() => Filebase64 = GetFileSignatureSmartCA(FileName, filebase64, fin.Extension.ToLower()));
                                        //t.Start();
                                        //t.Join();
                                        //if (Filebase64 != null)
                                        //{
                                        //    filebase64 = Filebase64;
                                        //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                        //}
                                        //else
                                        //{
                                        //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN NGƯỜI DÙNG ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU", false);
                                        //}
                                    }
                                }
                            }
                            else if ((fin.Extension.ToLower() == ".jpg" || fin.Extension.ToLower() == ".jpeg" || fin.Extension.ToLower() == ".jpe" || fin.Extension.ToLower() == ".gif" || fin.Extension.ToLower() == ".jfif" || fin.Extension.ToLower() == ".bmp" || fin.Extension.ToLower() == ".dib" || fin.Extension.ToLower() == ".rle" || fin.Extension.ToLower() == ".png" || fin.Extension.ToLower() == ".tiff"))
                            {
                                if (ShowMessageTQDT("FILE DOANH NGHIỆP ĐANG CHỌN LÀ ĐỊNH DẠNG FILE HÌNH ẢNH .\rPHẦN MỀM SẼ TỰ ĐỘNG CONVERT FILE HÌNH ẢNH THÀNH PDF .\rSAU KHI CHUYỂN THÀNH FILE PDF PHẦN MỀM SẼ TỰ ĐỘNG KÝ CHỮ KÝ SỐ CHO FILE NÀY .\rDOANH NGHIỆP NHẤN ĐỒNG Ý ĐỂ THỰC HIỆN ", true) == "Yes")
                                {
                                    string dstFilename = Application.StartupPath + "\\Temp\\" + fin.Name.Replace(fin.Extension.ToString(), ".pdf");
                                    if (File.Exists(dstFilename))
                                    {
                                        File.Delete(dstFilename);
                                    }
                                    ConvertImageToPdf(fin.FullName, dstFilename);

                                    fs = new System.IO.FileStream(dstFilename, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                                    size = 0;
                                    size = fs.Length;
                                    data = new byte[fs.Length];
                                    fs.Read(data, 0, data.Length);
                                    filebase64 = System.Convert.ToBase64String(data);
                                    FileName = Path.GetFileName(dstFilename);
                                    fs.Flush();
                                    fs.Close();
                                    // Ký từ xa
                                    SendMessageForm sendForm = new SendMessageForm();
                                    sendForm.FileName = FileName;
                                    sendForm.filebase64 = filebase64;
                                    sendForm.Extends = ".pdf";
                                    sendForm.Send += SendMessage;
                                    bool isSend = sendForm.DoSendFile(null);
                                    if (isSend && !String.IsNullOrEmpty(sendForm.FileData))
                                    {
                                        filebase64 = sendForm.FileData;
                                        ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                    }
                                    else
                                    {
                                        ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN NGƯỜI DÙNG ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU", false);

                                    }
                                    //string Filebase64 = "";
                                    //Thread t = new Thread(() => Filebase64 = GetFileSignatureSmartCA(FileName, filebase64, fin.Extension.ToLower()));
                                    //t.Start();
                                    //t.Join();
                                    //if (Filebase64 != null)
                                    //{
                                    //    filebase64 = Filebase64;
                                    //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                    //}
                                    //else
                                    //{
                                    //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN NGƯỜI DÙNG ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU", false);
                                    //}
                                }
                            }
                            else if (fin.Extension.ToLower() == ".docx" || fin.Extension.ToLower() == ".xlsx")
                            {
                                SendMessageForm sendForm = new SendMessageForm();
                                sendForm.FileName = FileName;
                                sendForm.filebase64 = filebase64;
                                sendForm.Extends = fin.Extension.ToLower();
                                sendForm.Send += SendMessage;
                                bool isSend = sendForm.DoSendFile(null);
                                if (isSend && !String.IsNullOrEmpty(sendForm.FileData))
                                {
                                    filebase64 = sendForm.FileData;
                                    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                }
                                else
                                {
                                    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN NGƯỜI DÙNG ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU", false);

                                }
                                //string Filebase64 = "";
                                //Thread t = new Thread(() => Filebase64 = GetFileSignatureSmartCA(FileName, filebase64, fin.Extension.ToLower()));
                                //t.Start();
                                //t.Join();
                                //if (Filebase64 != null)
                                //{
                                //    filebase64 = Filebase64;
                                //    ShowMessageTQDT("KÝ SỐ FILE THÀNH CÔNG ", false);
                                //}
                                //else
                                //{
                                //    ShowMessageTQDT("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN NGƯỜI DÙNG ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU", false);
                                //}
                            }
                        }

                        switch (tbCtrVouchers.SelectedTab.Name)
                        {
                            case "tpGiayPhep":
                                lblFileNameGiayPhep.Text = FileName;
                                license.Content = filebase64;
                                txtFileSizeGiayPhep.Text = CalculateFileSize(size);
                                break;
                            case "tpHopDong":
                                lblFileNameHopDong.Text = FileName;
                                contractDocument.Content = filebase64;
                                txtFileSizeHopDong.Text = CalculateFileSize(size);
                                break;
                            case "tpHoaDon":
                                lblFileNameHoaDon.Text = FileName;
                                commercialInvoice.Content = filebase64;
                                txtFileSizeHoaDon.Text = CalculateFileSize(size);
                                break;
                            case "tpCO":
                                lblFileNameCO.Text = FileName;
                                certificateOfOrigin.Content = filebase64;
                                txtFileSizeCO.Text = CalculateFileSize(size);
                                break;
                            case "tpVanDon":
                                lblFileNameVanDon.Text = FileName;
                                billOfLading.Content = filebase64;
                                txtFileSizeVanDon.Text = CalculateFileSize(size);
                                break;
                            case "tpContainer":
                                lblFileNamContainer.Text = FileName;
                                container.Content = filebase64;
                                txtFileSizeContainer.Text = CalculateFileSize(size);
                                break;
                            case "tpCTKhac":
                                lblFileNameCTKhac.Text = FileName;
                                additionalDocument.Content = filebase64;
                                txtFileSizeChungTuKhac.Text = CalculateFileSize(size);
                                break;
                            default:
                                break;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private String IsCompress(string CompressData)
        {
            try
            {
                String DecompressedData = String.Empty;
                DecompressedData = Compression.DeCompress(CompressData);
                return DecompressedData;
            }
            catch
            {
                return null;
            }
        }
        public void View(string Vouchers)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                String DecompressedData = String.Empty;
                string path = Application.StartupPath + "\\Temp";

                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                string fileName = "";
                System.IO.FileStream fs;
                byte[] bytes;
                switch (Vouchers)
                {
                    case "License":
                        if (String.IsNullOrEmpty(license.FileName))
                        {
                            ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CHƯA CHỌN FILE ĐỂ XEM.", false);
                            return;
                        }
                        fileName = path + "\\" + license.FileName;
                        if (System.IO.File.Exists(fileName))
                        {
                            System.IO.File.Delete(fileName);
                        }
                        fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);

                        DecompressedData = IsCompress(license.Content);
                        if (!String.IsNullOrEmpty(DecompressedData))
                        {
                            bytes = System.Convert.FromBase64String(DecompressedData);
                        }
                        else
                        {
                            bytes = System.Convert.FromBase64String(license.Content);
                        }
                        fs.Write(bytes, 0, bytes.Length);
                        fs.Close();
                        System.Diagnostics.Process.Start(fileName);
                        break;
                    case "ContractDocument":
                        if (String.IsNullOrEmpty(contractDocument.FileName))
                        {
                            ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CHƯA CHỌN FILE ĐỂ XEM.", false);
                            return;
                        }
                        fileName = path + "\\" + contractDocument.FileName;
                        if (System.IO.File.Exists(fileName))
                        {
                            System.IO.File.Delete(fileName);
                        }
                        fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);

                        DecompressedData = IsCompress(contractDocument.Content);
                        if (!String.IsNullOrEmpty(DecompressedData))
                        {
                            bytes = System.Convert.FromBase64String(DecompressedData);
                        }
                        else
                        {
                            bytes = System.Convert.FromBase64String(contractDocument.Content);
                        }
                        fs.Write(bytes, 0, bytes.Length);
                        fs.Close();
                        System.Diagnostics.Process.Start(fileName);
                        break;
                    case "CommercialInvoice":
                        if (String.IsNullOrEmpty(commercialInvoice.FileName))
                        {
                            ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CHƯA CHỌN FILE ĐỂ XEM.", false);
                            return;
                        }
                        fileName = path + "\\" + commercialInvoice.FileName;
                        if (System.IO.File.Exists(fileName))
                        {
                            System.IO.File.Delete(fileName);
                        }
                        fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                        DecompressedData = IsCompress(commercialInvoice.Content);
                        if (!String.IsNullOrEmpty(DecompressedData))
                        {
                            bytes = System.Convert.FromBase64String(DecompressedData);
                        }
                        else
                        {
                            bytes = System.Convert.FromBase64String(commercialInvoice.Content);
                        }
                        fs.Write(bytes, 0, bytes.Length);
                        fs.Close();
                        System.Diagnostics.Process.Start(fileName);
                        break;
                    case "CertificateOfOrigin":
                        certificateOfOrigin = KDT_VNACCS_CertificateOfOrigin.Load(certificateOfOrigin.ID);
                        if (String.IsNullOrEmpty(certificateOfOrigin.FileName))
                        {
                            ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CHƯA CHỌN FILE ĐỂ XEM.", false);
                            return;
                        }
                        fileName = path + "\\" + certificateOfOrigin.FileName;
                        if (System.IO.File.Exists(fileName))
                        {
                            System.IO.File.Delete(fileName);
                        }
                        fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);

                        DecompressedData = IsCompress(certificateOfOrigin.Content);
                        if (!String.IsNullOrEmpty(DecompressedData))
                        {
                            bytes = System.Convert.FromBase64String(DecompressedData);
                        }
                        else
                        {
                            bytes = System.Convert.FromBase64String(certificateOfOrigin.Content);
                        }
                        fs.Write(bytes, 0, bytes.Length);
                        fs.Close();
                        System.Diagnostics.Process.Start(fileName);
                        break;
                    case "BillOfLading":
                        if (String.IsNullOrEmpty(billOfLading.FileName))
                        {
                            ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CHƯA CHỌN FILE ĐỂ XEM.", false);
                            return;
                        }
                        fileName = path + "\\" + billOfLading.FileName;
                        if (System.IO.File.Exists(fileName))
                        {
                            System.IO.File.Delete(fileName);
                        }
                        fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);

                        DecompressedData = IsCompress(billOfLading.Content);
                        if (!String.IsNullOrEmpty(DecompressedData))
                        {
                            bytes = System.Convert.FromBase64String(DecompressedData);
                        }
                        else
                        {
                            bytes = System.Convert.FromBase64String(billOfLading.Content);
                        }
                        fs.Write(bytes, 0, bytes.Length);
                        fs.Close();
                        System.Diagnostics.Process.Start(fileName);
                        break;
                    case "Container":
                        if (String.IsNullOrEmpty(container.FileName))
                        {
                            ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CHƯA CHỌN FILE ĐỂ XEM.", false);
                            return;
                        }
                        fileName = path + "\\" + container.FileName;
                        if (System.IO.File.Exists(fileName))
                        {
                            System.IO.File.Delete(fileName);
                        }
                        fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                        DecompressedData = IsCompress(container.Content);
                        if (!String.IsNullOrEmpty(DecompressedData))
                        {
                            bytes = System.Convert.FromBase64String(DecompressedData);
                        }
                        else
                        {
                            bytes = System.Convert.FromBase64String(container.Content);
                        }
                        fs.Write(bytes, 0, bytes.Length);
                        fs.Close();
                        System.Diagnostics.Process.Start(fileName);
                        break;
                    case "AdditionalDocument":
                        if (String.IsNullOrEmpty(additionalDocument.FileName))
                        {
                            ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CHƯA CHỌN FILE ĐỂ XEM.", false);
                            return;
                        }
                        fileName = path + "\\" + additionalDocument.FileName;
                        if (System.IO.File.Exists(fileName))
                        {
                            System.IO.File.Delete(fileName);
                        }
                        fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);

                        DecompressedData = IsCompress(additionalDocument.Content);
                        if (!String.IsNullOrEmpty(DecompressedData))
                        {
                            bytes = System.Convert.FromBase64String(DecompressedData);
                        }
                        else
                        {
                            bytes = System.Convert.FromBase64String(additionalDocument.Content);
                        }
                        fs.Write(bytes, 0, bytes.Length);
                        fs.Close();
                        System.Diagnostics.Process.Start(fileName);
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }
        private void btnAddCO_Click(object sender, EventArgs e)
        {
            AttachFile("CertificateOfOrigin");
        }
        private void btnAddVanDon_Click(object sender, EventArgs e)
        {
            AttachFile("BillOfLading");
        }

        private void btnAddHopDong_Click(object sender, EventArgs e)
        {
            AttachFile("ContractDocument");
        }

        private void btnAddHoaDon_Click(object sender, EventArgs e)
        {
            AttachFile("CommercialInvoice");
        }

        private void btnAddGiayPhep_Click(object sender, EventArgs e)
        {
            AttachFile("License");
        }

        private void btnAddContainer_Click(object sender, EventArgs e)
        {
            AttachFile("Container");
        }

        private void btnAddCTKhac_Click(object sender, EventArgs e)
        {
            AttachFile("AdditionalDocument");
        }

        private void btnViewCO_Click(object sender, EventArgs e)
        {
            View("CertificateOfOrigin");
        }

        private void btnViewVanDon_Click(object sender, EventArgs e)
        {
            View("BillOfLading");
        }

        private void btnViewHopDong_Click(object sender, EventArgs e)
        {
            View("ContractDocument");
        }

        private void btnViewHoaDon_Click(object sender, EventArgs e)
        {
            View("CommercialInvoice");
        }

        private void btnViewGiayPhep_Click(object sender, EventArgs e)
        {
            View("License");
        }

        private void btnViewContainer_Click(object sender, EventArgs e)
        {
            View("Container");
        }

        private void btnViewCTKhac_Click(object sender, EventArgs e)
        {
            View("AdditionalDocument");
        }

        private void btnDeleteCO_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListCO.SelectedItems;
                if (grListCO.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CÓ MUỐN XÓA CO NÀY KHÔNG?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        certificateOfOrigin.ID = Convert.ToInt64(grListCO.GetRow().Cells["ID"].Value.ToString());
                        MsgSend sendXML = new MsgSend();
                        sendXML.LoaiHS = LoaiKhaiBao.CertificateOfOrigin;
                        sendXML.master_id = certificateOfOrigin.ID;
                        if (sendXML.Load())
                        {
                            if (ShowMessage("CHỨNG TỪ NÀY ĐÃ GỬI ĐẾN HQ .DOANH NGHIỆP CÓ MUỐN XÓA CHỨNG TỪ NÀY KHÔNG ", true) == "Yes")
                            {
                                if (certificateOfOrigin.ID == 0)
                                {
                                    continue;
                                }
                                if (certificateOfOrigin.ID > 0)
                                {
                                    certificateOfOrigin.CertificateOfOriginCollection = KDT_VNACCS_CertificateOfOrigin_Detail.SelectCollectionBy_CertificateOfOrigin_ID(certificateOfOrigin.ID);
                                    certificateOfOrigin.DeleteFull();
                                }
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            if (certificateOfOrigin.ID == 0)
                            {
                                continue;
                            }
                            if (certificateOfOrigin.ID > 0)
                            {
                                certificateOfOrigin.CertificateOfOriginCollection = KDT_VNACCS_CertificateOfOrigin_Detail.SelectCollectionBy_CertificateOfOrigin_ID(certificateOfOrigin.ID);
                                certificateOfOrigin.DeleteFull();
                            }
                        }
                    }
                    certificateOfOrigin.ID = 0;
                    certificateOfOriginDetail.ID = 0;
                    lblFileNameCO.Text = String.Empty;
                    Reset();
                }
                BindDataCertificateOfOrigin();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }

        private void btnDeleteVanDon_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListVanDon.SelectedItems;
                if (grListVanDon.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CÓ MUỐN XÓA VẬN ĐƠN NÀY KHÔNG?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        billOfLading.ID = Convert.ToInt64(grListVanDon.GetRow().Cells["ID"].Value.ToString());
                        MsgSend sendXML = new MsgSend();
                        sendXML.LoaiHS = LoaiKhaiBao.BillOfLading;
                        sendXML.master_id = billOfLading.ID;
                        if (sendXML.Load())
                        {
                            if (ShowMessage("CHỨNG TỪ NÀY ĐÃ GỬI ĐẾN HQ .DOANH NGHIỆP CÓ MUỐN XÓA CHỨNG TỪ NÀY KHÔNG ", true) == "Yes")
                            {
                                if (billOfLading.ID == 0)
                                {
                                    continue;
                                }
                                if (billOfLading.ID > 0)
                                {
                                    billOfLading.BillOfLadingCollection = KDT_VNACCS_BillOfLading_Detail.SelectCollectionBy_BillOfLading_ID(billOfLading.ID);
                                    billOfLading.DeleteFull();
                                }
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            if (billOfLading.ID == 0)
                            {
                                continue;
                            }
                            if (billOfLading.ID > 0)
                            {
                                billOfLading.BillOfLadingCollection = KDT_VNACCS_BillOfLading_Detail.SelectCollectionBy_BillOfLading_ID(billOfLading.ID);
                                billOfLading.DeleteFull();
                            }
                        }
                    }
                    billOfLading.ID = 0;
                    billOfLadingDetail.BillOfLading_ID = 0;
                    lblFileNameVanDon.Text = String.Empty;
                    Reset();
                }
                BindDataBillOfLading();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }

        private void btnDeleteHopDong_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListHopDong.SelectedItems;
                if (grListHopDong.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CÓ MUỐN HỢP ĐỒNG NÀY KHÔNG?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        contractDocument.ID = Convert.ToInt64(grListHopDong.GetRow().Cells["ID"].Value.ToString());
                        MsgSend sendXML = new MsgSend();
                        sendXML.LoaiHS = LoaiKhaiBao.ContractDocument;
                        sendXML.master_id = contractDocument.ID;
                        if (sendXML.Load())
                        {
                            if (ShowMessage("CHỨNG TỪ NÀY ĐÃ GỬI ĐẾN HQ .DOANH NGHIỆP CÓ MUỐN XÓA CHỨNG TỪ NÀY KHÔNG ", true) == "Yes")
                            {
                                if (contractDocument.ID == 0)
                                {
                                    continue;
                                }
                                if (contractDocument.ID > 0)
                                {
                                    contractDocument.ContractDocumentCollection = KDT_VNACCS_ContractDocument_Detail.SelectCollectionBy_ContractDocument_ID(contractDocument.ID);
                                    contractDocument.DeleteFull();
                                }
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            if (contractDocument.ID == 0)
                            {
                                continue;
                            }
                            if (contractDocument.ID > 0)
                            {
                                contractDocument.ContractDocumentCollection = KDT_VNACCS_ContractDocument_Detail.SelectCollectionBy_ContractDocument_ID(contractDocument.ID);
                                contractDocument.DeleteFull();
                            }
                        }
                    }
                    contractDocument.ID = 0;
                    contractDocumentDetail.ID = 0;
                    lblFileNameHopDong.Text = String.Empty;
                    Reset();
                }
                BindDataContractDocument();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }

        private void btnDeleteHoaDon_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListHoaDon.SelectedItems;
                if (grListHoaDon.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CÓ MUỐN XÓA HÓA ĐƠN NÀY KHÔNG?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        commercialInvoice.ID = Convert.ToInt64(grListHoaDon.GetRow().Cells["ID"].Value.ToString());
                        MsgSend sendXML = new MsgSend();
                        sendXML.LoaiHS = LoaiKhaiBao.CommercialInvoice;
                        sendXML.master_id = commercialInvoice.ID;
                        if (sendXML.Load())
                        {
                            if (ShowMessage("CHỨNG TỪ NÀY ĐÃ GỬI ĐẾN HQ .DOANH NGHIỆP CÓ MUỐN XÓA CHỨNG TỪ NÀY KHÔNG ", true) == "Yes")
                            {
                                if (commercialInvoice.ID == 0)
                                {
                                    continue;
                                }
                                if (commercialInvoice.ID > 0)
                                {
                                    commercialInvoice.CommercialInvoiceCollection = KDT_VNACCS_CommercialInvoice_Detail.SelectCollectionBy_CommercialInvoice_ID(commercialInvoice.ID);
                                    commercialInvoice.DeleteFull();
                                }
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            if (commercialInvoice.ID == 0)
                            {
                                continue;
                            }
                            if (commercialInvoice.ID > 0)
                            {
                                commercialInvoice.CommercialInvoiceCollection = KDT_VNACCS_CommercialInvoice_Detail.SelectCollectionBy_CommercialInvoice_ID(commercialInvoice.ID);
                                commercialInvoice.DeleteFull();
                            }
                        }
                    }
                    commercialInvoice.ID = 0;
                    commercialInvoiceDetail.ID = 0;
                    lblFileNameHoaDon.Text = String.Empty;
                    Reset();
                }
                BindDataCommercialInvoice();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }

        private void btnDeleteGiayPhep_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListGiayPhep.SelectedItems;
                if (grListGiayPhep.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CÓ MUỐN XÓA GIẤY PHÉP NÀY KHÔNG?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        license.ID = Convert.ToInt64(grListGiayPhep.GetRow().Cells["ID"].Value.ToString());
                        MsgSend sendXML = new MsgSend();
                        sendXML.LoaiHS = LoaiKhaiBao.License;
                        sendXML.master_id = license.ID;
                        if (sendXML.Load())
                        {
                            if (ShowMessage("CHỨNG TỪ NÀY ĐÃ GỬI ĐẾN HQ .DOANH NGHIỆP CÓ MUỐN XÓA CHỨNG TỪ NÀY KHÔNG ", true) == "Yes")
                            {
                                if (license.ID == 0)
                                {
                                    continue;
                                }
                                if (license.ID > 0)
                                {
                                    license.LicenseCollection = KDT_VNACCS_License_Detail.SelectCollectionBy_License_ID(license.ID);
                                    license.DeleteFull();
                                }
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            if (license.ID == 0)
                            {
                                continue;
                            }
                            if (license.ID > 0)
                            {
                                license.LicenseCollection = KDT_VNACCS_License_Detail.SelectCollectionBy_License_ID(license.ID);
                                license.DeleteFull();
                            }
                        }
                    }
                    license.ID = 0;
                    licenseDetail.ID = 0;
                    lblFileNameGiayPhep.Text = String.Empty;
                    Reset();
                }
                BindDataLicense();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }

        private void btnDeleteContainer_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListContainer.SelectedItems;
                if (grListContainer.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CÓ MUỐN XÓA CONTAINER NÀY KHÔNG?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        container.ID = Convert.ToInt64(grListContainer.GetRow().Cells["ID"].Value.ToString());
                        MsgSend sendXML = new MsgSend();
                        sendXML.LoaiHS = LoaiKhaiBao.Containers;
                        sendXML.master_id = container.ID;
                        if (sendXML.Load())
                        {
                            if (ShowMessage("CHỨNG TỪ NÀY ĐÃ GỬI ĐẾN HQ .DOANH NGHIỆP CÓ MUỐN XÓA CHỨNG TỪ NÀY KHÔNG ", true) == "Yes")
                            {
                                if (container.ID == 0)
                                {
                                    continue;
                                }
                                if (container.ID > 0)
                                {
                                    container.Delete();
                                }
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            if (container.ID == 0)
                            {
                                continue;
                            }
                            if (container.ID > 0)
                            {
                                container.Delete();
                            }
                        }
                    }
                    container.ID = 0;
                    Reset();
                }
                BindDataContainer();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }

        private void btnDeleteCTKhac_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListCTKhac.SelectedItems;
                if (grListCTKhac.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CÓ MUỐN XÓA CHỨNG TỪ NÀY KHÔNG?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        additionalDocument.ID = Convert.ToInt64(grListCTKhac.GetRow().Cells["ID"].Value.ToString());
                        MsgSend sendXML = new MsgSend();
                        sendXML.LoaiHS = LoaiKhaiBao.AdditionalDocument;
                        sendXML.master_id = additionalDocument.ID;
                        if (sendXML.Load())
                        {
                            if (ShowMessage("CHỨNG TỪ NÀY ĐÃ GỬI ĐẾN HQ .DOANH NGHIỆP CÓ MUỐN XÓA CHỨNG TỪ NÀY KHÔNG ", true) == "Yes")
                            {
                                if (additionalDocument.ID == 0)
                                {
                                    continue;
                                }
                                if (additionalDocument.ID > 0)
                                {
                                    additionalDocument.AdditionalDocumentCollection = KDT_VNACCS_AdditionalDocument_Detail.SelectCollectionBy_AdditionalDocument_ID(additionalDocument.ID);
                                    additionalDocument.DeleteFull();
                                }
                            }
                            else
                            {
                                return;
                            }
                        }
                        if (additionalDocument.ID == 0)
                        {
                            continue;
                        }
                        if (additionalDocument.ID > 0)
                        {
                            additionalDocument.AdditionalDocumentCollection = KDT_VNACCS_AdditionalDocument_Detail.SelectCollectionBy_AdditionalDocument_ID(additionalDocument.ID);
                            additionalDocument.DeleteFull();
                        }
                    }
                    additionalDocument.ID = 0;
                    additionalDocumentDetail.ID = 0;
                    lblFileNameCTKhac.Text = String.Empty;
                    Reset();
                }
                BindDataAdditionalDocument();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }

        private void btnDeleteOverTime_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListDKLNG.SelectedItems;
                if (grListDKLNG.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessageTQDT(" Thông báo từ hệ thống ", " DOANH NGHIỆP CÓ MUỐN XÓA ĐĂNG KÝ LÀM NGOÀI GIỜ NÀY KHÔNG?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        overTime.ID = Convert.ToInt64(grListDKLNG.GetRow().Cells["ID"].Value.ToString());
                        MsgSend sendXML = new MsgSend();
                        sendXML.LoaiHS = LoaiKhaiBao.OverTime;
                        sendXML.master_id = overTime.ID;
                        if (sendXML.Load())
                        {
                            if (ShowMessage("CHỨNG TỪ NÀY ĐÃ GỬI ĐẾN HQ .DOANH NGHIỆP CÓ MUỐN XÓA CHỨNG TỪ NÀY KHÔNG ", true) == "Yes")
                            {
                                if (overTime.ID == 0)
                                {
                                    continue;
                                }
                                if (overTime.ID > 0)
                                {
                                    overTime.Delete();
                                }
                            }
                            else
                            {
                                return;
                            }
                        }
                        if (overTime.ID == 0)
                        {
                            continue;
                        }
                        if (overTime.ID > 0)
                        {
                            overTime.Delete();
                        }
                    }
                    overTime.ID = 0;
                    Reset();
                }
                BindDataOvertime();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private Decimal calcBase64Size(String base64String)
        {
            decimal result = 1;
            if (!String.IsNullOrEmpty(base64String))
            {
                Int32 padding = 0;
                if (base64String.EndsWith("=="))
                {
                    padding = 2;
                }
                else
                {
                    if (base64String.EndsWith("=")) padding = 1;
                }
                result = Convert.ToDecimal((Math.Ceiling(Convert.ToDouble(base64String.Length) / 4) * 3) - padding);
            }
            return result;
        }

        private void grListCO_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    certificateOfOrigin.ID = Convert.ToInt64(grListCO.GetRow().Cells["ID"].Value.ToString());
                    certificateOfOrigin = KDT_VNACCS_CertificateOfOrigin.Load(certificateOfOrigin.ID);
                    certificateOfOrigin.CertificateOfOriginCollection = KDT_VNACCS_CertificateOfOrigin_Detail.SelectCollectionBy_CertificateOfOrigin_ID(certificateOfOrigin.ID);
                    txtSoCO.Text = grListCO.GetRow().Cells["SoCO"].Value.ToString();
                    cbbLoaiCO.SelectedValue = grListCO.GetRow().Cells["LoaiCO"].Value.ToString();
                    txtToChucCapCO.Text = grListCO.GetRow().Cells["ToChucCapCO"].Value.ToString();
                    dateNgayCapCO.Value = Convert.ToDateTime(grListCO.GetRow().Cells["NgayCapCO"].Value.ToString());
                    txtNuocCapCO.Ma = grListCO.GetRow().Cells["NuocCapCO"].Value.ToString();
                    txtNguoiCapCO.Text = grListCO.GetRow().Cells["NguoiCapCO"].Value.ToString();
                    txtGhiChuCO.Text = grListCO.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    lblFileNameCO.Text = grListCO.GetRow().Cells["FileName"].Value.ToString();

                    //decimal size = System.Text.Encoding.Unicode.GetByteCount(certificateOfOrigin.Content);
                    size = Convert.ToInt64(calcBase64Size(certificateOfOrigin.Content));
                    txtFileSizeCO.Text = CalculateFileSize(size);

                    txtMaHQCO.Text = grListCO.GetRow().Cells["MaHQ"].Value.ToString();
                    txtSoTiepNhanCO.Text = grListCO.GetRow().Cells["SoTN"].Value.ToString();
                    clcNgayTiepNhanCO.Value = Convert.ToDateTime(grListCO.GetRow().Cells["NgayTN"].Value.ToString());
                    isAddCO = false;
                    SetCommandStatusAll();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }


        }
        private void grListVanDon_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    billOfLading.ID = Convert.ToInt64(grListVanDon.GetRow().Cells["ID"].Value.ToString());
                    billOfLading = KDT_VNACCS_BillOfLading.Load(billOfLading.ID);
                    billOfLading.BillOfLadingCollection = KDT_VNACCS_BillOfLading_Detail.SelectCollectionBy_BillOfLading_ID(billOfLading.ID);
                    txtSoVanDon.Text = grListVanDon.GetRow().Cells["SoVanDon"].Value.ToString();
                    dtpNgayVanDon.Value = Convert.ToDateTime(grListVanDon.GetRow().Cells["NgayVanDon"].Value.ToString());
                    ctrNuocPHVanDon.Ma = grListVanDon.GetRow().Cells["NuocPhatHanh"].Value.ToString();
                    txtDiaDiemCTQC.Text = grListVanDon.GetRow().Cells["DiaDiemCTQC"].Value.ToString();
                    cbbLoaiVanDon.SelectedValue = grListVanDon.GetRow().Cells["LoaiVanDon"].Value.ToString();
                    txtGhiChuVanDon.Text = grListVanDon.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    lblFileNameVanDon.Text = grListVanDon.GetRow().Cells["FileName"].Value.ToString();

                    //decimal size = System.Text.Encoding.Unicode.GetByteCount(Helpers.ConvertFromBase64(billOfLading.Content));
                    size = Convert.ToInt64(calcBase64Size(billOfLading.Content));
                    txtFileSizeVanDon.Text = CalculateFileSize(size);

                    txtMaHQVanDon.Text = grListVanDon.GetRow().Cells["MaHQ"].Value.ToString();
                    txtSoTiepNhanVanDon.Text = grListVanDon.GetRow().Cells["SoTN"].Value.ToString();
                    clcNgayTNVanDon.Value = Convert.ToDateTime(grListVanDon.GetRow().Cells["NgayTN"].Value.ToString());
                    isAddVanDon = false;
                    SetCommandStatusAll();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }

        private void grListHopDong_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    contractDocument.ID = Convert.ToInt64(grListHopDong.GetRow().Cells["ID"].Value.ToString());
                    contractDocument = KDT_VNACCS_ContractDocument.Load(contractDocument.ID);
                    contractDocument.ContractDocumentCollection = KDT_VNACCS_ContractDocument_Detail.SelectCollectionBy_ContractDocument_ID(contractDocument.ID);
                    txtSoHopDong.Text = grListHopDong.GetRow().Cells["SoHopDong"].Value.ToString();
                    dtpNgayHopDong.Value = Convert.ToDateTime(grListHopDong.GetRow().Cells["NgayHopDong"].Value.ToString());
                    dtpThoiHanThanhToan.Value = Convert.ToDateTime(grListHopDong.GetRow().Cells["ThoiHanThanhToan"].Value.ToString());
                    txtTongTriGiaHopDong.Text = grListHopDong.GetRow().Cells["TongTriGia"].Value.ToString();
                    txtGhiChuHopDong.Text = grListHopDong.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    lblFileNameHopDong.Text = grListHopDong.GetRow().Cells["FileName"].Value.ToString();

                    //decimal size = System.Text.Encoding.Unicode.GetByteCount(Helpers.ConvertFromBase64(contractDocument.Content));
                    size = Convert.ToInt64(calcBase64Size(contractDocument.Content));
                    txtFileSizeHopDong.Text = CalculateFileSize(size);

                    txtMaHQHopDong.Text = grListHopDong.GetRow().Cells["MaHQ"].Value.ToString();
                    txtSoTNHopDong.Text = grListHopDong.GetRow().Cells["SoTN"].Value.ToString();
                    clcNgayTNHopDong.Value = Convert.ToDateTime(grListHopDong.GetRow().Cells["NgayTN"].Value.ToString());
                    isAddHopDong = false;
                    SetCommandStatusAll();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void grListHoaDon_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    commercialInvoice.ID = Convert.ToInt64(grListHoaDon.GetRow().Cells["ID"].Value.ToString());
                    commercialInvoice = KDT_VNACCS_CommercialInvoice.Load(commercialInvoice.ID);
                    commercialInvoice.CommercialInvoiceCollection = KDT_VNACCS_CommercialInvoice_Detail.SelectCollectionBy_CommercialInvoice_ID(commercialInvoice.ID);
                    txtSoHoaDonTM.Text = grListHoaDon.GetRow().Cells["SoHoaDonTM"].Value.ToString();
                    dtpNgayPhatHanhHoaDonTM.Value = Convert.ToDateTime(grListHoaDon.GetRow().Cells["NgayPhatHanhHDTM"].Value.ToString());
                    txtGhiChuHoaDon.Text = grListHoaDon.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    lblFileNameHoaDon.Text = grListHoaDon.GetRow().Cells["FileName"].Value.ToString();

                    //decimal size = System.Text.Encoding.Unicode.GetByteCount(Helpers.ConvertFromBase64(commercialInvoice.Content));
                    size = Convert.ToInt64(calcBase64Size(commercialInvoice.Content));
                    txtFileSizeHoaDon.Text = CalculateFileSize(size);

                    txtMaHQHoaDon.Text = grListHoaDon.GetRow().Cells["MaHQ"].Value.ToString();
                    txtSoTNHoaDon.Text = grListHoaDon.GetRow().Cells["SoTN"].Value.ToString();
                    clcNgayTNHoaDon.Value = Convert.ToDateTime(grListHoaDon.GetRow().Cells["NgayTN"].Value.ToString());
                    isAddHoaDon = false;
                    SetCommandStatusAll();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void grListGiayPhep_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    license.ID = Convert.ToInt64(grListGiayPhep.GetRow().Cells["ID"].Value.ToString());
                    license = KDT_VNACCS_License.Load(license.ID);
                    license.LicenseCollection = KDT_VNACCS_License_Detail.SelectCollectionBy_License_ID(license.ID);
                    txtNguoiCapGiayPhep.Text = grListGiayPhep.GetRow().Cells["NguoiCapGP"].Value.ToString();
                    txtSoGiayPhep.Text = grListGiayPhep.GetRow().Cells["SoGP"].Value.ToString();
                    dtpNgayCapGiayPhep.Value = Convert.ToDateTime(grListGiayPhep.GetRow().Cells["NgayCapGP"].Value.ToString());
                    txtNoiCapGiayPhep.Text = grListGiayPhep.GetRow().Cells["NoiCapGP"].Value.ToString();
                    cbbLoaiGiayPhep.Text = grListGiayPhep.GetRow().Cells["LoaiGP"].Value.ToString();
                    dtpNgayHetHanGiayPhep.Value = Convert.ToDateTime(grListGiayPhep.GetRow().Cells["NgayHetHanGP"].Value.ToString());
                    txtGhiChuGiayPhep.Text = grListGiayPhep.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    lblFileNameGiayPhep.Text = grListGiayPhep.GetRow().Cells["FileName"].Value.ToString();

                    //decimal size = System.Text.Encoding.Unicode.GetByteCount(Helpers.ConvertFromBase64(license.Content));
                    size = Convert.ToInt64(calcBase64Size(license.Content));
                    txtFileSizeGiayPhep.Text = CalculateFileSize(size);

                    txtMaHQGiayPhep.Text = grListGiayPhep.GetRow().Cells["MaHQ"].Value.ToString();
                    txtSoTNGiayPhep.Text = grListGiayPhep.GetRow().Cells["SoTN"].Value.ToString();
                    clcNgayTNGiayPhep.Value = Convert.ToDateTime(grListGiayPhep.GetRow().Cells["NgayTN"].Value.ToString());
                    isAddGiayPhep = false;
                    SetCommandStatusAll();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void grListContainer_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    container.ID = Convert.ToInt64(grListContainer.GetRow().Cells["ID"].Value.ToString());
                    container = KDT_VNACCS_Container_Detail.Load(container.ID);
                    txtGhiChuContainer.Text = grListContainer.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    lblFileNamContainer.Text = grListContainer.GetRow().Cells["FileName"].Value.ToString();

                    //decimal size = System.Text.Encoding.Unicode.GetByteCount(Helpers.ConvertFromBase64(container.Content));
                    size = Convert.ToInt64(calcBase64Size(container.Content));
                    txtFileSizeContainer.Text = CalculateFileSize(size);

                    txtMaHQContainer.Text = grListContainer.GetRow().Cells["MaHQ"].Value.ToString();
                    txtSoTNContainer.Text = grListContainer.GetRow().Cells["SoTN"].Value.ToString();
                    clcNgayTNContainer.Value = Convert.ToDateTime(grListContainer.GetRow().Cells["NgayTN"].Value.ToString());
                    isAddContainer = false;
                    SetCommandStatusAll();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void grListCTKhac_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    additionalDocument.ID = Convert.ToInt64(grListCTKhac.GetRow().Cells["ID"].Value.ToString());
                    additionalDocument = KDT_VNACCS_AdditionalDocument.Load(additionalDocument.ID);
                    additionalDocument.AdditionalDocumentCollection = KDT_VNACCS_AdditionalDocument_Detail.SelectCollectionBy_AdditionalDocument_ID(additionalDocument.ID);
                    txtSoChungTuKhac.Text = grListCTKhac.GetRow().Cells["SoChungTu"].Value.ToString();
                    txtTenChungTuKhac.Text = grListCTKhac.GetRow().Cells["TenChungTu"].Value.ToString();
                    dtpNgayChungTuKhac.Value = Convert.ToDateTime(grListCTKhac.GetRow().Cells["NgayPhatHanh"].Value.ToString());
                    txtNoiPhatHanhCTKhac.Text = grListCTKhac.GetRow().Cells["NoiPhatHanh"].Value.ToString();
                    txtGhiChuCTKhac.Text = grListCTKhac.GetRow().Cells["GhiChuKhac"].Value.ToString();
                    lblFileNameCTKhac.Text = additionalDocument.FileName;
                    cbbLoaiChungTu.SelectedValue = grListCTKhac.GetRow().Cells["LoaiChungTu"].Value.ToString();

                    //decimal size = System.Text.Encoding.Unicode.GetByteCount(Helpers.ConvertFromBase64(additionalDocument.Content));
                    size = Convert.ToInt64(calcBase64Size(additionalDocument.Content));
                    txtFileSizeChungTuKhac.Text = CalculateFileSize(size);


                    txtMaHQCTKhac.Text = grListCTKhac.GetRow().Cells["MaHQ"].Value.ToString();
                    txtSoTNCTKhac.Text = grListCTKhac.GetRow().Cells["SoTN"].Value.ToString();
                    clcNgayTNCTKhac.Value = Convert.ToDateTime(grListCTKhac.GetRow().Cells["NgayTN"].Value.ToString());
                    isAddCTkhac = false;
                    SetCommandStatusAll();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void grListDKLNG_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    overTime.ID = Convert.ToInt64(grListDKLNG.GetRow().Cells["ID"].Value.ToString());
                    overTime = KDT_VNACCS_OverTime.Load(overTime.ID);
                    txtNoiDung.Text = grListDKLNG.GetRow().Cells["NoiDung"].Value.ToString();
                    dtpNgayDangKyThuTuc.Value = Convert.ToDateTime(grListDKLNG.GetRow().Cells["NgayDangKy"].Value.ToString());
                    dtpGioLamTT.Value = Convert.ToDateTime(grListDKLNG.GetRow().Cells["GioLamThuTuc"].Value.ToString());

                    txtMaHQDKLNG.Text = grListDKLNG.GetRow().Cells["MaHQ"].Value.ToString();
                    txtSoTNDKLNG.Text = grListDKLNG.GetRow().Cells["SoTN"].Value.ToString();
                    clcNgayTNDKLNG.Value = Convert.ToDateTime(grListDKLNG.GetRow().Cells["NgayTN"].Value.ToString());
                    isAddOverTime = false;
                    SetCommandStatusAll();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            switch (tbCtrVouchers.SelectedTab.Name)
            {
                case "tpGiayPhep":
                    feedbackContent = SingleMessage.LicenseSendHandler(license, ref msgInfor, e);
                    break;
                case "tpHopDong":
                    feedbackContent = SingleMessage.ContractDocumentSendHandler(contractDocument, ref msgInfor, e);
                    break;
                case "tpHoaDon":
                    feedbackContent = SingleMessage.CommercialInvoiceSendHandler(commercialInvoice, ref msgInfor, e);
                    break;
                case "tpCO":
                    feedbackContent = SingleMessage.CertificateOfOriginSendHandler(certificateOfOrigin, ref msgInfor, e);
                    break;
                case "tpVanDon":
                    feedbackContent = SingleMessage.BillOfLadingSendHandler(billOfLading, ref msgInfor, e);
                    break;
                case "tpContainer":
                    feedbackContent = SingleMessage.ContainersSendHandler(container, ref msgInfor, e);
                    break;
                case "tpCTKhac":
                    feedbackContent = SingleMessage.AdditionalDocumentSendHandler(additionalDocument, ref msgInfor, e);
                    break;
                case "tpLamNgoaiGio":
                    feedbackContent = SingleMessage.OverTimeSendHandler(overTime, ref msgInfor, e);
                    break;
                default:
                    break;
            }
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

        private void grListCO_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                    switch (trangthai)
                    {
                        case "0":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                            break;
                        case "-1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                            break;
                        case "1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                            break;
                        case "2":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                            break;
                        case "-3":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                            break;
                    }
                    string LoaiCo = e.Row.Cells["LoaiCO"].Value.ToString();
                    switch (LoaiCo)
                    {
                        case "D":
                            e.Row.Cells["LoaiCO"].Text = "Form D";
                            break;
                        case "E":
                            e.Row.Cells["LoaiCO"].Text = "Form E";
                            break;
                        case "AK":
                            e.Row.Cells["LoaiCO"].Text = "Form AK";
                            break;
                        case "AJ":
                            e.Row.Cells["LoaiCO"].Text = "Form AJ";
                            break;
                        case "S":
                            e.Row.Cells["LoaiCO"].Text = "Form S";
                            break;
                        case "JV":
                            e.Row.Cells["LoaiCO"].Text = "Form JV";
                            break;
                        case "AANZ":
                            e.Row.Cells["LoaiCO"].Text = "Form AANZ";
                            break;
                        case "AI":
                            e.Row.Cells["LoaiCO"].Text = "Form AI";
                            break;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void grListVanDon_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                    switch (trangthai)
                    {
                        case "0":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                            break;
                        case "-1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                            break;
                        case "1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                            break;
                        case "2":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                            break;
                        case "-3":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                            break;
                    }
                    string loaivandon = e.Row.Cells["LoaiVanDon"].Value.ToString();
                    switch (loaivandon)
                    {
                        case "1":
                            e.Row.Cells["LoaiVanDon"].Text = "Đường biển";
                            break;
                        case "2":
                            e.Row.Cells["LoaiVanDon"].Text = "Đường không";
                            break;
                        case "3":
                            e.Row.Cells["LoaiVanDon"].Text = "Đường bộ";
                            break;
                        case "4":
                            e.Row.Cells["LoaiVanDon"].Text = "Đường sắt";
                            break;
                        case "5":
                            e.Row.Cells["LoaiVanDon"].Text = "Đường sông";
                            break;
                        case "9":
                            e.Row.Cells["LoaiVanDon"].Text = "Loại khác";
                            break;

                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void grListHopDong_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                    switch (trangthai)
                    {
                        case "0":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                            break;
                        case "-1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                            break;
                        case "1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                            break;
                        case "2":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                            break;
                        case "-3":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                            break;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void grListHoaDon_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                    switch (trangthai)
                    {
                        case "0":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                            break;
                        case "-1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                            break;
                        case "1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                            break;
                        case "2":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                            break;
                        case "-3":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                            break;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void grListGiayPhep_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                    switch (trangthai)
                    {
                        case "0":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                            break;
                        case "-1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                            break;
                        case "1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                            break;
                        case "2":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                            break;
                        case "-3":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                            break;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void grListContainer_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                    switch (trangthai)
                    {
                        case "0":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                            break;
                        case "-1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                            break;
                        case "1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                            break;
                        case "2":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                            break;
                        case "-3":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                            break;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void grListCTKhac_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string loaiChungTu = e.Row.Cells["LoaiChungTu"].Value.ToString();
                    switch (loaiChungTu)
                    {
                        case "1":
                            e.Row.Cells["LoaiChungTu"].Text = "Bảng kê chi tiết hàng hóa";
                            break;
                        case "2":
                            e.Row.Cells["LoaiChungTu"].Text = "Thông tin giấy chứng nhận kiểm tra chuyên ngành";
                            break;
                        case "3":
                            e.Row.Cells["LoaiChungTu"].Text = "Chứng từ chứng minh tổ chức, cá nhân đủ điều kiện xuất khẩu, nhập khẩu hàng hóa theo quy định của pháp luật về đầu tư";
                            break;
                        case "4":
                            e.Row.Cells["LoaiChungTu"].Text = "Hợp đồng ủy thác";
                            break;
                        case "5":
                            e.Row.Cells["LoaiChungTu"].Text = "Chứng từ xác định hàng hóa nhập khẩu được áp dụng thuế suất thuế GTGT 5%";
                            break;
                        case "9":
                            e.Row.Cells["LoaiChungTu"].Text = "Chứng từ khác";
                            break;
                    }
                    string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                    switch (trangthai)
                    {
                        case "0":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                            break;
                        case "-1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                            break;
                        case "1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                            break;
                        case "2":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                            break;
                        case "-3":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                            break;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void grListDKLNG_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                    switch (trangthai)
                    {
                        case "0":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                            break;
                        case "-1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                            break;
                        case "1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                            break;
                        case "2":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                            break;
                        case "-4":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Hủy khai báo";
                            break;
                        case "-3":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                            break;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void tbCtrVouchers_SelectedTabChanged(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {
            cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            cmdCancel.Visible = cmdCancel1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            cmdEdit.Visible = cmdEdit1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            switch (tbCtrVouchers.SelectedTab.Name)
            {
                case "tpGiayPhep":
                    BindDataLicense();
                    break;
                case "tpHopDong":
                    BindDataContractDocument();
                    break;
                case "tpHoaDon":
                    BindDataCommercialInvoice();
                    break;
                case "tpCO":
                    BindDataCertificateOfOrigin();
                    break;
                case "tpVanDon":
                    BindDataBillOfLading();
                    break;
                case "tpContainer":
                    BindDataContainer();
                    break;
                case "tpCTKhac":
                    BindDataAdditionalDocument();
                    break;
                case "tpLamNgoaiGio":
                    cmdCancel.Visible = cmdCancel1.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    cmdEdit.Visible = cmdEdit1.Visible = Janus.Windows.UI.InheritableBoolean.True;
                    BindDataOvertime();
                    break;
                default:
                    break;
            }
        }

        private void linkHuongDanKB_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=2D3z1aFY0QA");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void linkHuongDanKySo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=1ZShUo3fBI4&list=PLFhBUYQraey7iikiKodZ9B3NoB5plC25c");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void chkAutoFeedback_CheckedChanged(object sender, EventArgs e)
        {
            switch (tbCtrVouchers.SelectedTab.Name)
            {
                case "tpGiayPhep":
                    if (chkAutoFeedbackGP.CheckState == CheckState.Checked)
                    {
                        Feedback_License();
                    }
                    break;
                case "tpHopDong":
                    if (chkAutoFeedbackHD.CheckState == CheckState.Checked)
                    {
                        Feedback_ContractDocument();
                    }
                    break;
                case "tpHoaDon":
                    if (chkAutoFeedbackHoaDon.CheckState == CheckState.Checked)
                    {
                        Feedback_CommercialInvoice();
                    }
                    break;
                case "tpCO":
                    if (chkAutoFeedbackCO.CheckState == CheckState.Checked)
                    {
                        Feedback_CertificateOfOrigin();
                    }
                    break;
                case "tpVanDon":
                    if (chkAutoFeedbackVD.CheckState == CheckState.Checked)
                    {
                        Feedback_BillOfLading();
                    }
                    break;
                case "tpContainer":
                    if (chkAutoFeedbackCT.CheckState == CheckState.Checked)
                    {
                        Feedback_Container();
                    }
                    break;
                case "tpCTKhac":
                    if (chkAutoFeedbackCTK.CheckState == CheckState.Checked)
                    {
                        Feedback_AdditionalDocumentn();
                    }
                    break;
                case "tpLamNgoaiGio":
                    if (chkAutoFeedbackOT.CheckState == CheckState.Checked)
                    {
                        Feedback_OverTime();
                    }
                    break;
                default:
                    break;
            }
        }

        private void timerAutoFeedBack_Tick(object sender, EventArgs e)
        {

        }

        private void VNACC_VouchersForm_Load(object sender, EventArgs e)
        {
            BindDataCertificateOfOrigin();
        }
        public bool DoSend()
        {
            ThreadPool.QueueUserWorkItem(DoWork);
            return isSignSuccess;
        }
        private void DoWork(object obj)
        {
            if (Company.KDT.SHARE.Components.Globals.IsSignOnLan)
            {
                MSG_FILE_OUTBOX = GetFileSignatureOnLan(FileName, filebase64);
                if (MSG_FILE_OUTBOX != null && MSG_FILE_OUTBOX.FILE_NAME.Contains("Signed"))
                {
                    isSignSuccess = true;
                }
            }
            else if (Company.KDT.SHARE.Components.Globals.IsSignRemote)
            {
                MessageFile = GetSignatureRemote(FileName, filebase64);
                if (MessageFile != null)
                {
                    isSignSuccess = true;
                }
            }
        }
        private MSG_FILE_OUTBOX GetFileSignatureOnLan(string FileName, string FileData)
        {
            try
            {
                string guidStr = Guid.NewGuid().ToString();
                string cnnString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { GlobalSettings.SERVER_NAME, GlobalSettings.DataSignLan, GlobalSettings.USER, GlobalSettings.PASS });
                string MSG_FROM = System.Environment.MachineName.ToString();
                MSG_FILE_INBOX.InsertSignFileContent(cnnString, guidStr, FileName, FileData, -1, MSG_FROM);
                int count = 0;
                int timeWait = 15;
                while (count < timeWait)
                {
                    MSG_FILE_OUTBOX outbox = MSG_FILE_OUTBOX.GetContentSignLocal(cnnString, guidStr);
                    if (outbox != null && outbox.FILE_DATA != null)
                    {
                        return outbox;
                    }
                    else
                    {
                        count++;
                        Thread.Sleep(1000);
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
            //throw new Exception("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ");
        }
        //public const string URL_CREDENTIALSLIST = "https://rmgateway.vnptit.vn/csc/credentials/list";
        //public const string URL_CREDENTIALSINFO = "https://rmgateway.vnptit.vn/csc/credentials/info";
        //public const string URL_SIGNHASH = "https://rmgateway.vnptit.vn/csc/signature/signhash";
        //public const string URL_SIGN = "https://rmgateway.vnptit.vn/csc/signature/sign";
        //public const string URL_GETTRANINFO = "https://rmgateway.vnptit.vn/csc/credentials/gettraninfo";
        private string GetFileSignatureSmartCA(string FileName, string Content, string Extends)
        {
            try
            {
                //var customerEmail = "0400392263123";
                //var customerPass = "Tchq@12345";
                var refresh_token = Company.KDT.SHARE.Components.Globals.REFRESH_TOKEN;
                var access_token = Company.KDT.SHARE.Components.Globals.ACCESS_TOKEN;
                DateTime expires_in = Convert.ToDateTime(Company.KDT.SHARE.Components.Globals.EXPIRES_TOKEN);
                if (!String.IsNullOrEmpty(access_token))
                {
                    if (DateTime.Now > expires_in)
                    {
                        access_token = HelperVNACCS.GetAccessToken(Company.KDT.SHARE.Components.Globals.UserNameSmartCA, Company.KDT.SHARE.Components.Globals.PaswordSmartCA, refresh_token);
                        if (access_token == null)
                        {
                            throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được chuỗi Access Token"));
                        }
                    }
                }
                else
                {
                    access_token = HelperVNACCS.GetAccessToken(Company.KDT.SHARE.Components.Globals.UserNameSmartCA, Company.KDT.SHARE.Components.Globals.PaswordSmartCA, refresh_token);
                    if (access_token == null)
                    {
                        throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được chuỗi Access Token"));
                    }
                }
                String credential = HelperVNACCS.GetCredentialSmartCA(access_token, Company.KDT.SHARE.Components.Globals.URL_CREDENTIALSLIST);
                if (credential == null)
                {
                    throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được danh sách Chữ ký số"));
                }
                String certBase64 = HelperVNACCS.GetAccoutSmartCACert(access_token, Company.KDT.SHARE.Components.Globals.URL_CREDENTIALSINFO, credential);
                if (certBase64 == null)
                {
                    throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được Chữ ký số"));
                }
                var tranId = HelperVNACCS.SignDocument(access_token, Company.KDT.SHARE.Components.Globals.URL_SIGN, Content, credential, FileName, Content);
                if (tranId == null)
                {
                    throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được Thông tin giao dịch Chữ ký số"));
                }
                bool isWait = true;
                int countSend = 1;
                var datasigned = "";
                int countRequest = 60;
                while (isWait && countSend < 6)
                {
                    int count = countRequest;
                    string title = string.Format("Chờ người dùng xác nhận {0}", countSend);
                    while (count > 0)
                    {
                        //SetCaption(string.Format(title + ": {0} giây", count));
                        var getTranInfo = HelperVNACCS.GetTranInfo(access_token, Company.KDT.SHARE.Components.Globals.URL_GETTRANINFO, tranId);
                        if (getTranInfo != null)
                        {
                            if (getTranInfo.Content.TranStatus == TranStatus.WAITING_FOR_SIGNER_CONFIRM.ToString())
                            {
                                Thread.Sleep(1000);
                                count--;
                            }
                            else
                            {
                                switch (getTranInfo.Content.TranStatus)
                                {
                                    case "1":
                                        return datasigned = getTranInfo.Content.Documents[0].DataSigned;
                                        break;
                                    case "4001":
                                        throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.EXPIRED));
                                        break;
                                    case "4002":
                                        throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.SIGNER_REJECTED));
                                        break;
                                    case "4003":
                                        throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.AUTHORIZE_KEY_FAILED));
                                        break;
                                    case "4004":
                                        throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.SIGN_FAILED));
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        else
                        {
                        }
                    }
                    countSend++;
                }
                //var SignedData = "";
                //if (Extends.ToLower() == ".pdf")
                //{
                //    SignedData = HelperVNACCS.SignPdf(certBase64, access_token, FileName, Content);
                //    if (SignedData != null)
                //    {
                //        return SignedData;
                //    }
                //    else
                //    {
                //        return null;
                //    }
                //}
                //else if (Extends.ToLower() == ".docx" || Extends.ToLower() == ".xlsx")
                //{
                //    SignedData = HelperVNACCS.SignOffice(certBase64, access_token, FileName, Content);
                //    if (SignedData != null)
                //    {
                //        return SignedData;
                //    }
                //    else
                //    {
                //        return null;
                //    }
                //}
                //else if (Extends.ToLower() == ".xml")
                //{
                //    SignedData = HelperVNACCS.SignXml(certBase64, access_token, FileName, Content);
                //    if (SignedData != null)
                //    {
                //        return SignedData;
                //    }
                //    else
                //    {
                //        return null;
                //    }
                //}
                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private int CountRequest(decimal len)
        {
            int timeRequest = 0;
            Random rnd1 = new Random();

            if (len < 1024)
                timeRequest = rnd1.Next(3, 7);
            else if (len > 1024 & len < 1048576)
                timeRequest = rnd1.Next(5, 9);
            else if (len > 1048576 & len < 107341824)
                timeRequest = rnd1.Next(10, 20);
            else timeRequest = rnd1.Next(30, 60);
            return timeRequest;
        }
        private SignRemote.MessageFile GetSignatureRemote(string FileName, string FileData)
        {
            try
            {
                ISignMessage signRemote = WebService.SignMessage();
                string guidStr = Guid.NewGuid().ToString();
                MessageSendFile msgSend = new MessageSendFile()
                {
                    From = Company.KDT.SHARE.Components.Globals.UserNameSignRemote,
                    Password = Company.KDT.SHARE.Components.Globals.PasswordSignRemote,
                    SendStatus = SendStatusInfo.Send,
                    Message = new SignRemote.MessageFile()
                    {
                        FileName = FileName,
                        FileData = FileData,
                        GuidStr = guidStr,
                    },
                    To = GlobalSettings.MA_DON_VI.Trim()
                };

                string msg = Helpers.Serializer(msgSend, true, true);

                string msgRet = signRemote.ClientSendFile(msg);
                msgSend = Helpers.Deserialize<MessageSendFile>(msgRet);
                if (msgSend.SendStatus == SendStatusInfo.Error)
                {
                    throw new Exception("Thông báo từ hệ thống: " + msgSend.Warrning);
                }
                MessageSendFile msgFeedBack = new MessageSendFile()
                {
                    From = Company.KDT.SHARE.Components.Globals.UserNameSignRemote,
                    Password = Company.KDT.SHARE.Components.Globals.PasswordSignRemote,
                    SendStatus = SendStatusInfo.Request,
                    Message = new SignRemote.MessageFile()
                    {
                        GuidStr = guidStr
                    },
                    To = GlobalSettings.MA_DON_VI.Trim()
                };
                msg = Helpers.Serializer(msgFeedBack, true, true);

                bool isWait = true;
                int countSend = 1;
                int countRequest = CountRequest(msg.Length);
                while (isWait && countSend < 5)
                {
                    int count = countRequest;
                    while (count > 0)
                    {
                        Thread.Sleep(1000);
                        count--;
                    }
                    msgRet = signRemote.ClientSendFile(msg);
                    msgSend = Helpers.Deserialize<MessageSendFile>(msgRet);
                    if (msgSend.SendStatus == SendStatusInfo.Error)
                    {
                        throw new Exception("Thông báo từ hệ thống: " + msgSend.Warrning);
                    }
                    else if (msgSend.SendStatus == SendStatusInfo.NoSign)
                    {
                        return new SignRemote.MessageFile
                        {
                            FileName = msgSend.Message.FileName,
                            FileData = msgSend.Message.FileData
                        };

                    }
                    else if (msgSend.SendStatus == SendStatusInfo.Wait)
                        countSend++;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            //throw new Exception(string.Format("ID={0}. Ký số từ xa thất bại\nMáy chủ đã không phản hồi thông tin trong thời gian giao dịch", guidStr));
        }
    }
}
