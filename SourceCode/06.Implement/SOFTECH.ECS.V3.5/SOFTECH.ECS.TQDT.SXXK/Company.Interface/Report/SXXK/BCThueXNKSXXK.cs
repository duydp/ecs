using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using System.Globalization;

namespace Company.Interface.Report.SXXK
{
    public partial class BCThueXNKSXXK : DevExpress.XtraReports.UI.XtraReport
    {
        public int LanThanhLy;
        private int STT = 0;
        public BCThueXNKSXXK()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("MaNPL", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
                new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            }
            else
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
                new DevExpress.XtraReports.UI.GroupField("MaNPL", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            }
            DataTable dt = new Company.BLL.SXXK.ThanhKhoan.BCThueXNK().SelectDynamic("LanThanhLy = " + this.LanThanhLy , "").Tables[0];
            dt.TableName = "BCThueXNK";
            this.DataSource = dt;
            lblTongLuongNPLSuDung.Summary.Running = SummaryRunning.Group;
            lblTongLuongNPLSuDung.Summary.Func = SummaryFunc.Sum;
            lblTongLuongNPLSuDung.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            //lblSHSTK.Text = "Lần " + LanThanhLyBase.getLanThanhLyMoiNhat(GlobalSettings.MA_DON_VI, maHaiQuan) + "/" + DateTime.Today.Year;
            lblMaNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaNPL");
            lblSoToKhaiNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiNhap");
            lblNgayDangKyNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyNhap", "{0:dd/MM/yy}");
            lblNgayThucNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayThucNhap", "{0:dd/MM/yy}");
            lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblDonGiaTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DonGiaTT", "{0:g20}");
            lblTyGiaTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TyGiaTT", "{0:n3}");
            lblThueSuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueSuat", "{0:n0}");
            lblThueNKNop.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueNKNop", "{0:n0}");
            lblTenDVT_NPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            lblSoToKhaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiXuat");
            lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            lblNgayThucXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayThucXuat", "{0:dd/MM/yy}");
            lblLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTon", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongTienThueHoan.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TienThueHoan", "{0:n0}");
            lblTongTienThueHoan.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TienThueHoan", "{0:n0}");
            lblTongTienThueTKTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TienThueTKTiep", "{0:n0}");
            xrTableCell21.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaNPL");
            xrTableCell22.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            xrTableCell23.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            xrTableCell24.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DonGiaTT", "{0:n3}");
            xrTableCell25.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TyGiaTT", "{0:n3}");
            xrTableCell26.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueSuat", "{0:n0}");
            xrTableCell27.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueNKNop", "{0:n0}");
            lblTongThueKhongThu.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TienThueHoan", "{0:n0}");
            xrLabel76.Text = xrLabel5.Text = GlobalSettings.TieudeNgay;
            
        }
        private decimal total1 = 0;
        private decimal total2 = 0;
        private void lblTongThueKhongThu_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {


        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            //total1 += Convert.ToDecimal(lblTongTienThueHoan.Text);
            total2 += Convert.ToDecimal(lblTongTienThueTKTiep.Text);
        }

        private void lblTongThueLanSauTK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ((XRLabel)sender).Text = total2.ToString("N0"); 
        }

        private void lblThueSuat_AfterPrint(object sender, EventArgs e)
        {
            ((XRLabel)sender).Text = ((XRLabel)sender).Text + "%";
        }

        private void xrTableCell26_AfterPrint(object sender, EventArgs e)
        {
            ((XRLabel)sender).Text = ((XRLabel)sender).Text + "%";
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblSTT.Text = "" + this.STT;
        }

        private void BCThueXNKSXXK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
        }

        private void lblMaNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblMaNPL.Text = GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString();

        }

        private void xrTableCell21_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrTableCell21.Text = GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString();
        }
    }
}
