﻿using System;
using System.Drawing;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;

namespace Company.Interface.KDT.GC
{
    public partial class HangChuyenTiepForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT;
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        private bool IsExit = true;
        public string MaLoaiHinh;
        public string MaNguyenTe;

        //-----------------------------------------------------------------------------------------

        public HangChuyenTiepForm()
        {
            InitializeComponent();
        }

        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            switch (this.MaLoaiHinh.Substring(2, 2))
            {
                case "PL":
                    dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                    break;
                case "SP":
                    if (this.MaLoaiHinh.EndsWith("X"))
                    {
                        dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
                        txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
                    }
                    else
                    {
                        dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                        txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                    }
                    break;
                case "TB":
                    dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                    break;
            }

            khoitao_DuLieuChuanUpdate();
        }
        private void khoitao_DuLieuChuanUpdate()
        {
            //TODO: Hungtq update 22/02/2011
            switch (MaLoaiHinh.Substring(3, 2))
            {
                case "18"://"PL":
                    dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    break;
                case "19"://"SP":
                    if (MaLoaiHinh.Substring(0,1).Equals("N"))
                    {
                        dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
                    }
                    else
                    {
                        dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    }
                    break;
                case "20"://"TB":
                    dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    break;
            }
        }

        //-----------------------------------------------------------------------------------------
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangChuyenTiep hct in this.TKCT.HCTCollection)
            {
                if (hct.MaHang.Trim() == maHang) return true;
            }
            return false;
        }
        private void btnAddNew_Click(object sender, EventArgs e)
        {
            if (TKCT.ID > 0)
            {
                if (TKCT.MaLoaiHinh.Contains("X"))
                {
                    if (Company.GC.BLL.GC.PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(TKCT.ID))
                    {
                        showMsg("MSG_ALL01");
                        //ShowMessage("Tờ khai chuyển tiếp có id = " + TKCT.ID + " này đã được phân bổ nên không thể xóa được.", false);
                        return;
                    }
                }
            }
            KiemTraTonTaiHang();
            cvError.Validate();
            if (!cvError.IsValid) return;
            if (!IsExit) return;
            txtTGNT.Value = (decimal)txtLuong.Value * (decimal)txtDGNT.Value;
            if (checkMaHangExit(txtMaHang.Text))
            {
                showMsg("MSG_2702007");
                //ShowMessage("Mặt hàng này đã tồn tại trên lưới, vui lòng chọn mặt hàng khác.", false);
                return;
            }

            if (!KiemTraSoLuong())
                return;
            HangChuyenTiep hct = new HangChuyenTiep();
            hct.SoThuTuHang = 0;
            hct.MaHS = txtMaHS.Text;
            hct.MaHang = txtMaHang.Text;
            hct.TenHang = txtTenHang.Text;
            hct.ID_NuocXX = ctrNuocXX.Ma;
            hct.ID_DVT = cbDonViTinh.SelectedValue.ToString();
            hct.SoLuong = Convert.ToDecimal(txtLuong.Value);
            hct.DonGia = Convert.ToDecimal(txtDGNT.Value);
            hct.TriGia = Convert.ToDecimal(txtTGNT.Value);
            this.TKCT.HCTCollection.Add(hct);
            this.Clear();
            //dgList.DataSource = this.HCTCollection;
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
        }

        //-----------------------------------------------------------------------------------------
        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            //this.fillNguyenPhuLieu(txtMaNPL.Text);
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["ID_NuocXX"].Text = this.Nuoc_GetName(e.Row.Cells["ID_NuocXX"].Value.ToString());
            e.Row.Cells["ID_DVT"].Text = this.DonViTinh_GetName(e.Row.Cells["ID_DVT"].Text.ToString());
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            switch (this.MaLoaiHinh.Substring(2, 2))
            {
                case "PL":

                    Company.Interface.GC.NguyenPhuLieuRegistedForm fNPL = new Company.Interface.GC.NguyenPhuLieuRegistedForm();
                    fNPL.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                    fNPL.isBrower = true;
                    fNPL.ShowDialog();
                    if (fNPL.NguyenPhuLieuSelected.Ten != "" && fNPL.NguyenPhuLieuSelected.Ma != "")
                    {
                        txtMaHang.Text = fNPL.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = fNPL.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = fNPL.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = fNPL.NguyenPhuLieuSelected.DVT_ID;
                        if (this.MaLoaiHinh.EndsWith("N"))
                        {
                            decimal SoLuongConDuocNhap = fNPL.NguyenPhuLieuSelected.SoLuongDangKy - fNPL.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL.NguyenPhuLieuSelected.SoLuongCungUng;
                            txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = fNPL.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL.NguyenPhuLieuSelected.SoLuongDaDung; //+ fNPL.NguyenPhuLieuSelected.SoLuongCungUng;
                            txtLuongCon.Text = SoLuongConDuocGiao.ToString();
                        }

                        txtLuong.Focus();
                    }
                    break;
                case "SP":
                    if (this.MaLoaiHinh.EndsWith("N"))
                    {
                        Company.Interface.GC.NguyenPhuLieuRegistedForm fNPL1 = new Company.Interface.GC.NguyenPhuLieuRegistedForm();
                        fNPL1.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                        fNPL1.isBrower = true;
                        fNPL1.ShowDialog();
                        if (fNPL1.NguyenPhuLieuSelected.Ten != "" && fNPL1.NguyenPhuLieuSelected.Ma != "")
                        {
                            txtMaHang.Text = fNPL1.NguyenPhuLieuSelected.Ma;
                            txtTenHang.Text = fNPL1.NguyenPhuLieuSelected.Ten;
                            txtMaHS.Text = fNPL1.NguyenPhuLieuSelected.MaHS;
                            cbDonViTinh.SelectedValue = fNPL1.NguyenPhuLieuSelected.DVT_ID;
                            if (this.MaLoaiHinh.EndsWith("N"))
                            {
                                decimal SoLuongConDuocNhap = fNPL1.NguyenPhuLieuSelected.SoLuongDangKy - fNPL1.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL1.NguyenPhuLieuSelected.SoLuongCungUng;
                                txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                            }
                            txtLuong.Focus();
                        }
                    }
                    else
                    {
                        Company.Interface.GC.SanPhamRegistedForm fsp = new Company.Interface.GC.SanPhamRegistedForm();
                        fsp.SanPhamSelected.HopDong_ID = HD.ID;
                        fsp.isBrower = true;
                        fsp.ShowDialog();
                        if (fsp.SanPhamSelected.Ma != "")
                        {
                            txtMaHang.Text = fsp.SanPhamSelected.Ma;
                            txtTenHang.Text = fsp.SanPhamSelected.Ten;
                            txtMaHS.Text = fsp.SanPhamSelected.MaHS;
                            cbDonViTinh.SelectedValue = fsp.SanPhamSelected.DVT_ID;
                            txtLuong.Focus();
                        }
                    }
                    break;
                case "TB":
                    Company.Interface.GC.ThietBiRegistedForm ftb = new Company.Interface.GC.ThietBiRegistedForm();
                    ftb.ThietBiSelected.HopDong_ID = HD.ID;
                    ftb.isBrower = true;
                    ftb.ShowDialog();
                    if (ftb.ThietBiSelected.Ma != "")
                    {
                        txtMaHang.Text = ftb.ThietBiSelected.Ma;
                        txtTenHang.Text = ftb.ThietBiSelected.Ten;
                        txtMaHS.Text = ftb.ThietBiSelected.MaHS;
                        cbDonViTinh.SelectedValue = ftb.ThietBiSelected.DVT_ID;
                        if (this.MaLoaiHinh.EndsWith("N"))
                        {
                            decimal SoLuongConDuocNhap = ftb.ThietBiSelected.SoLuongDangKy - ftb.ThietBiSelected.SoLuongDaNhap;
                            txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = ftb.ThietBiSelected.SoLuongDaNhap;
                            txtLuongCon.Text = SoLuongConDuocGiao.ToString();
                        }

                        txtLuong.Focus();
                    }
                    break;
            }

            txtMaHang_ButtonClickUpdate();
        }
        private void txtMaHang_ButtonClickUpdate()
        {
            //TODO: Update by Hungtq 22/02/2011
            switch (this.MaLoaiHinh.Substring(3, 2))
            {
                case "18"://"PL":

                    Company.Interface.GC.NguyenPhuLieuRegistedForm fNPL = new Company.Interface.GC.NguyenPhuLieuRegistedForm();
                    fNPL.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                    fNPL.isBrower = true;
                    fNPL.ShowDialog();
                    if (fNPL.NguyenPhuLieuSelected.Ten != "" && fNPL.NguyenPhuLieuSelected.Ma != "")
                    {
                        txtMaHang.Text = fNPL.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = fNPL.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = fNPL.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = fNPL.NguyenPhuLieuSelected.DVT_ID;
                        if (this.MaLoaiHinh.Substring(0,1).Equals("N"))
                        {
                            decimal SoLuongConDuocNhap = fNPL.NguyenPhuLieuSelected.SoLuongDangKy - fNPL.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL.NguyenPhuLieuSelected.SoLuongCungUng;
                            txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = fNPL.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL.NguyenPhuLieuSelected.SoLuongDaDung; //+ fNPL.NguyenPhuLieuSelected.SoLuongCungUng;
                            txtLuongCon.Text = SoLuongConDuocGiao.ToString();
                        }

                        txtLuong.Focus();
                    }
                    break;
                case "19"://"SP":
                    if (this.MaLoaiHinh.Substring(0,1).Equals("N"))
                    {
                        Company.Interface.GC.NguyenPhuLieuRegistedForm fNPL1 = new Company.Interface.GC.NguyenPhuLieuRegistedForm();
                        fNPL1.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                        fNPL1.isBrower = true;
                        fNPL1.ShowDialog();
                        if (fNPL1.NguyenPhuLieuSelected.Ten != "" && fNPL1.NguyenPhuLieuSelected.Ma != "")
                        {
                            txtMaHang.Text = fNPL1.NguyenPhuLieuSelected.Ma;
                            txtTenHang.Text = fNPL1.NguyenPhuLieuSelected.Ten;
                            txtMaHS.Text = fNPL1.NguyenPhuLieuSelected.MaHS;
                            cbDonViTinh.SelectedValue = fNPL1.NguyenPhuLieuSelected.DVT_ID;
                            if (this.MaLoaiHinh.Substring(0,1).Equals("N"))
                            {
                                decimal SoLuongConDuocNhap = fNPL1.NguyenPhuLieuSelected.SoLuongDangKy - fNPL1.NguyenPhuLieuSelected.SoLuongDaNhap - fNPL1.NguyenPhuLieuSelected.SoLuongCungUng;
                                txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                            }
                            txtLuong.Focus();
                        }
                    }
                    else
                    {
                        Company.Interface.GC.SanPhamRegistedForm fsp = new Company.Interface.GC.SanPhamRegistedForm();
                        fsp.SanPhamSelected.HopDong_ID = HD.ID;
                        fsp.isBrower = true;
                        fsp.ShowDialog();
                        if (fsp.SanPhamSelected.Ma != "")
                        {
                            txtMaHang.Text = fsp.SanPhamSelected.Ma;
                            txtTenHang.Text = fsp.SanPhamSelected.Ten;
                            txtMaHS.Text = fsp.SanPhamSelected.MaHS;
                            cbDonViTinh.SelectedValue = fsp.SanPhamSelected.DVT_ID;
                            txtLuong.Focus();
                        }
                    }
                    break;
                case "20"://"TB":
                    Company.Interface.GC.ThietBiRegistedForm ftb = new Company.Interface.GC.ThietBiRegistedForm();
                    ftb.ThietBiSelected.HopDong_ID = HD.ID;
                    ftb.isBrower = true;
                    ftb.ShowDialog();
                    if (ftb.ThietBiSelected.Ma != "")
                    {
                        txtMaHang.Text = ftb.ThietBiSelected.Ma;
                        txtTenHang.Text = ftb.ThietBiSelected.Ten;
                        txtMaHS.Text = ftb.ThietBiSelected.MaHS;
                        cbDonViTinh.SelectedValue = ftb.ThietBiSelected.DVT_ID;
                        if (this.MaLoaiHinh.Substring(0,1).Equals("N"))
                        {
                            decimal SoLuongConDuocNhap = ftb.ThietBiSelected.SoLuongDangKy - ftb.ThietBiSelected.SoLuongDaNhap;
                            txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = ftb.ThietBiSelected.SoLuongDaNhap;
                            txtLuongCon.Text = SoLuongConDuocGiao.ToString();
                        }

                        txtLuong.Focus();
                    }
                    break;
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                HangChuyenTiepEditForm f = new HangChuyenTiepEditForm();
                f.HCT = (HangChuyenTiep)e.Row.DataRow;
                f.TKCT = this.TKCT;
                f.HD = this.HD;
                f.MaLoaiHinh = this.MaLoaiHinh;
                f.MaHaiQuan = this.MaHaiQuan;
                f.MaNguyenTe = this.MaNguyenTe;
                if (TKCT.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                    f.OpenType = OpenFormType.View;
                f.ShowDialog();
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
        }

        private void HangChuyenTiepForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            lblMaNT1.Text = lblMaNT2.Text = "(" + this.MaNguyenTe + ")";
            dgList.DataSource = this.TKCT.HCTCollection;
            if (!MaLoaiHinh.EndsWith("N") || !MaLoaiHinh.Substring(0,1).Equals("N"))
            {
                lblLuongCon.Text = setText("Lượng còn được giao", "Quantity can be exported");
            }
            if (this.TKCT.TrangThaiXuLy != -1)
            {
                btnAddNew.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }
        }

        private void txtLuong_Leave(object sender, EventArgs e)
        {
            txtTGNT.Value = (decimal)txtLuong.Value * (decimal)txtDGNT.Value;
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            txtTGNT.Value = (decimal)txtLuong.Value * (decimal)txtDGNT.Value;
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (TKCT.ID > 0)
            {
                if (TKCT.MaLoaiHinh.Contains("X"))
                {
                    if (Company.GC.BLL.GC.PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(TKCT.ID))
                    {
                        showMsg("MSG_ALL01");
                        //ShowMessage("Tờ khai chuyển tiếp có id = " + TKCT.ID + " này đã được phân bổ nên không thể xóa được.", false);
                        e.Cancel = true;
                        return;
                    }
                }
            }
            if (e.Row.RowType == RowType.Record)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                {
                    HangChuyenTiep hct = (HangChuyenTiep)e.Row.DataRow;
                    if (hct.ID > 0)
                    {
                        string LoaiHangHoa = "N";
                        if (TKCT.MaLoaiHinh.IndexOf("SP") > 0 || TKCT.MaLoaiHinh.IndexOf("19") > 0)
                            LoaiHangHoa = "S";
                        else if (TKCT.MaLoaiHinh.IndexOf("TB") > 0 || TKCT.MaLoaiHinh.IndexOf("20") > 0)
                            LoaiHangHoa = "T";
                        if (hct.ID > 0)
                        {
                            hct.Delete(TKCT.IDHopDong, LoaiHangHoa, TKCT.MaLoaiHinh);
                        }
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void txtMaHang_Leave(object sender, EventArgs e)
        {
            switch (this.MaLoaiHinh.Substring(2, 2))
            {
                case "PL":
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.HopDong_ID = HD.ID;
                    npl.Ma = txtMaHang.Text.Trim();
                    if (npl.Load())
                    {
                        this.IsExit = true;
                        txtMaHang.Text = npl.Ma;
                        txtTenHang.Text = npl.Ten;
                        txtMaHS.Text = npl.MaHS;
                        cbDonViTinh.SelectedValue = npl.DVT_ID;
                        if (this.MaLoaiHinh.EndsWith("N"))
                        {
                            decimal SoLuongConDuocNhap = npl.SoLuongDangKy - npl.SoLuongDaNhap - npl.SoLuongCungUng;
                            txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = npl.SoLuongDaNhap - npl.SoLuongDaDung;//+ npl.SoLuongCungUng;
                            txtLuongCon.Text = SoLuongConDuocGiao.ToString();
                        }
                        epError.SetError(txtMaHang, null);
                        txtLuong.Focus();
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại mã hàng này.", "This value is not exist"));

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
                case "SP":
                    if (this.MaLoaiHinh.EndsWith("N"))
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl1 = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl1.HopDong_ID = HD.ID;
                        npl1.Ma = txtMaHang.Text.Trim();
                        if (npl1.Load())
                        {
                            this.IsExit = true;
                            txtMaHang.Text = npl1.Ma;
                            txtTenHang.Text = npl1.Ten;
                            txtMaHS.Text = npl1.MaHS;
                            cbDonViTinh.SelectedValue = npl1.DVT_ID;
                            if (this.MaLoaiHinh.EndsWith("N"))
                            {
                                decimal SoLuongConDuocNhap = npl1.SoLuongDangKy - npl1.SoLuongDaNhap - npl1.SoLuongCungUng;
                                txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                            }
                            epError.SetError(txtMaHang, null);
                            txtLuong.Focus();
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại mã hàng này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    else
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.Ma = txtMaHang.Text;
                        if (sp.Load())
                        {
                            this.IsExit = true;
                            txtMaHang.Text = sp.Ma;
                            txtTenHang.Text = sp.Ten;
                            txtMaHS.Text = sp.MaHS;
                            cbDonViTinh.SelectedValue = sp.DVT_ID;
                            txtLuong.Focus();
                            if (this.MaLoaiHinh.EndsWith("X"))
                            {
                                decimal SoLuongConDuocNhap = sp.SoLuongDangKy - sp.SoLuongDaXuat;
                                txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                            }
                            epError.SetError(txtMaHang, null);
                            txtLuong.Focus();
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại sản phầm này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    break;
                case "TB":
                    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                    tb.HopDong_ID = HD.ID;
                    tb.Ma = txtMaHang.Text;
                    if (tb.Load())
                    {
                        this.IsExit = true;
                        txtMaHang.Text = tb.Ma;
                        txtTenHang.Text = tb.Ten;
                        txtMaHS.Text = tb.MaHS;
                        cbDonViTinh.SelectedValue = tb.DVT_ID;
                        epError.SetError(txtMaHang, string.Empty);
                        if (this.MaLoaiHinh.EndsWith("N"))
                        {
                            decimal SoLuongConDuocNhap = tb.SoLuongDangKy - tb.SoLuongDaNhap;
                            txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = tb.SoLuongDaNhap;
                            txtLuongCon.Text = SoLuongConDuocGiao.ToString();
                        }
                        epError.SetError(txtMaHang, null);
                        txtLuong.Focus();
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
            }

            txtMaHang_LeaveUpdate();
        }
        private void txtMaHang_LeaveUpdate()
        {
            //TODO: Update by Hungtq 22/02/2011
            switch (this.MaLoaiHinh.Substring(3, 2))
            {
                case "18"://"PL":
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.HopDong_ID = HD.ID;
                    npl.Ma = txtMaHang.Text.Trim();
                    if (npl.Load())
                    {
                        this.IsExit = true;
                        txtMaHang.Text = npl.Ma;
                        txtTenHang.Text = npl.Ten;
                        txtMaHS.Text = npl.MaHS;
                        cbDonViTinh.SelectedValue = npl.DVT_ID;
                        if (this.MaLoaiHinh.Substring(0,1).Equals("N"))
                        {
                            decimal SoLuongConDuocNhap = npl.SoLuongDangKy - npl.SoLuongDaNhap - npl.SoLuongCungUng;
                            txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = npl.SoLuongDaNhap - npl.SoLuongDaDung;//+ npl.SoLuongCungUng;
                            txtLuongCon.Text = SoLuongConDuocGiao.ToString();
                        }
                        epError.SetError(txtMaHang, null);
                        txtLuong.Focus();
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại mã hàng này.", "This value is not exist"));

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
                case "19"://"SP":
                    if (this.MaLoaiHinh.Substring(0,1).Equals("N"))
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl1 = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl1.HopDong_ID = HD.ID;
                        npl1.Ma = txtMaHang.Text.Trim();
                        if (npl1.Load())
                        {
                            this.IsExit = true;
                            txtMaHang.Text = npl1.Ma;
                            txtTenHang.Text = npl1.Ten;
                            txtMaHS.Text = npl1.MaHS;
                            cbDonViTinh.SelectedValue = npl1.DVT_ID;
                            if (this.MaLoaiHinh.Substring(0,1).Equals("N"))
                            {
                                decimal SoLuongConDuocNhap = npl1.SoLuongDangKy - npl1.SoLuongDaNhap - npl1.SoLuongCungUng;
                                txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                            }
                            epError.SetError(txtMaHang, null);
                            txtLuong.Focus();
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại mã hàng này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    else
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.Ma = txtMaHang.Text;
                        if (sp.Load())
                        {
                            this.IsExit = true;
                            txtMaHang.Text = sp.Ma;
                            txtTenHang.Text = sp.Ten;
                            txtMaHS.Text = sp.MaHS;
                            cbDonViTinh.SelectedValue = sp.DVT_ID;
                            txtLuong.Focus();
                            if (this.MaLoaiHinh.Substring(0,1).Equals("X"))
                            {
                                decimal SoLuongConDuocNhap = sp.SoLuongDangKy - sp.SoLuongDaXuat;
                                txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                            }
                            epError.SetError(txtMaHang, null);
                            txtLuong.Focus();
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại sản phầm này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    break;
                case "20"://"TB":
                    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                    tb.HopDong_ID = HD.ID;
                    tb.Ma = txtMaHang.Text;
                    if (tb.Load())
                    {
                        this.IsExit = true;
                        txtMaHang.Text = tb.Ma;
                        txtTenHang.Text = tb.Ten;
                        txtMaHS.Text = tb.MaHS;
                        cbDonViTinh.SelectedValue = tb.DVT_ID;
                        epError.SetError(txtMaHang, string.Empty);
                        if (this.MaLoaiHinh.Substring(0,1).Equals("N"))
                        {
                            decimal SoLuongConDuocNhap = tb.SoLuongDangKy - tb.SoLuongDaNhap;
                            txtLuongCon.Text = SoLuongConDuocNhap.ToString();
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = tb.SoLuongDaNhap;
                            txtLuongCon.Text = SoLuongConDuocGiao.ToString();
                        }
                        epError.SetError(txtMaHang, null);
                        txtLuong.Focus();
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
            }
        }
        private bool KiemTraSoLuong()
        {
            decimal SoLuongHienTai = Convert.ToDecimal(txtLuong.Text);
            switch (this.MaLoaiHinh.Substring(2, 2))
            {
                case "PL":
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.HopDong_ID = HD.ID;
                    npl.Ma = txtMaHang.Text;
                    if (npl.Load())
                    {
                        if (this.MaLoaiHinh.EndsWith("N"))
                        {
                            decimal SoLuongConDuocNhap = npl.SoLuongDangKy - npl.SoLuongDaNhap - npl.SoLuongCungUng;
                            if (SoLuongHienTai > SoLuongConDuocNhap)
                            {
                                if (showMsg("MSG_240237", SoLuongConDuocNhap, true) == "Yes")
                                //if (ShowMessage("Bạn đã nhập quá số lượng còn được nhập là : " + SoLuongConDuocNhap.ToString() + ". Bạn có muốn tiếp tục không ?", true) == "Yes")
                                {
                                    return true;
                                }
                                else
                                    return false;
                            }
                            else
                                return true;
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = npl.SoLuongDaNhap - npl.SoLuongDaDung + npl.SoLuongCungUng;
                            if (SoLuongConDuocGiao < SoLuongHienTai)
                            {
                                if (showMsg("MSG_2702008", SoLuongConDuocGiao, true) == "Yes")
                                //if (ShowMessage("Bạn đã giao quá số lượng còn được giao là : " + SoLuongConDuocGiao.ToString() + ". Bạn có muốn tiếp tục không ?", true) == "Yes")
                                {
                                    return true;
                                }
                                else
                                    return false;
                            }
                            else
                                return true;
                        }

                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
                case "SP":
                    if (this.MaLoaiHinh.EndsWith("N"))
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl1 = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl1.HopDong_ID = HD.ID;
                        npl1.Ma = txtMaHang.Text;
                        if (npl1.Load())
                        {
                            if (this.MaLoaiHinh.EndsWith("N"))
                            {
                                decimal SoLuongConDuocNhap = npl1.SoLuongDangKy - npl1.SoLuongDaNhap - npl1.SoLuongCungUng;
                                if (SoLuongHienTai > SoLuongConDuocNhap)
                                {
                                    if (showMsg("MSG_240237", SoLuongConDuocNhap, true) == "Yes")
                                    //if (ShowMessage("Bạn đã nhập quá số lượng còn được nhập là : " + SoLuongConDuocNhap.ToString() + ". Bạn có muốn tiếp tục không ?", true) == "Yes")
                                    {
                                        return true;
                                    }
                                    else
                                        return false;
                                }
                                else
                                    return true;
                            }
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    else
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.Ma = txtMaHang.Text.Trim();
                        if (sp.Load())
                        {
                            if (this.MaLoaiHinh.EndsWith("X"))
                            {
                                decimal SoLuongConDuocNhap = sp.SoLuongDangKy - sp.SoLuongDaXuat;
                                if (SoLuongConDuocNhap < SoLuongHienTai)
                                {
                                    if (showMsg("MSG_240237", SoLuongConDuocNhap, true) == "Yes")
                                    //if (ShowMessage("Bạn đã nhập quá số lượng còn được nhập là : " + SoLuongConDuocNhap.ToString() + ". Bạn có muốn tiếp tục không ?", true) == "Yes")
                                    {
                                        return true;
                                    }
                                    else
                                        return false;
                                }
                                else
                                    return true;
                            }
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại sản phẩm này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    break;
                case "TB":
                    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                    tb.HopDong_ID = HD.ID;
                    tb.Ma = txtMaHang.Text.Trim();
                    if (tb.Load())
                    {
                        if (this.MaLoaiHinh.EndsWith("N"))
                        {
                            decimal SoLuongConDuocNhap = tb.SoLuongDangKy - tb.SoLuongDaNhap;
                            if (SoLuongConDuocNhap < SoLuongHienTai)
                            {
                                if (showMsg("MSG_240237", SoLuongConDuocNhap, true) == "Yes")
                                //if (ShowMessage("Bạn đã nhập quá số lượng còn được nhập là : " + SoLuongConDuocNhap.ToString() + ". Bạn có muốn tiếp tục không ?", true) == "Yes")
                                {
                                    return true;
                                }
                                else
                                    return false;
                            }
                            else return true;
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = tb.SoLuongDaNhap;
                            if (SoLuongConDuocGiao < SoLuongHienTai)
                            {
                                if (showMsg("MSG_240238", SoLuongConDuocGiao, true) == "Yes")
                                //if (ShowMessage("Bạn đã nhập quá số lượng còn được giao là : " + SoLuongConDuocGiao.ToString() + ". Bạn có muốn tiếp tục không ?", true) == "Yes")
                                {
                                    return true;
                                }
                                else
                                    return false;
                            }
                            else return true;
                        }
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
            }

            return KiemTraSoLuongUpdate();

        }
        private bool KiemTraSoLuongUpdate()
        {
            decimal SoLuongHienTai = Convert.ToDecimal(txtLuong.Text);

            //TODO: Update by Hungtq 22/02/2011
            switch (this.MaLoaiHinh.Substring(3, 2))
            {
                case "18"://"PL":
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.HopDong_ID = HD.ID;
                    npl.Ma = txtMaHang.Text;
                    if (npl.Load())
                    {
                        if (this.MaLoaiHinh.Substring(0,1).Equals("N"))
                        {
                            decimal SoLuongConDuocNhap = npl.SoLuongDangKy - npl.SoLuongDaNhap - npl.SoLuongCungUng;
                            if (SoLuongHienTai > SoLuongConDuocNhap)
                            {
                                if (showMsg("MSG_240237", SoLuongConDuocNhap, true) == "Yes")
                                //if (ShowMessage("Bạn đã nhập quá số lượng còn được nhập là : " + SoLuongConDuocNhap.ToString() + ". Bạn có muốn tiếp tục không ?", true) == "Yes")
                                {
                                    return true;
                                }
                                else
                                    return false;
                            }
                            else
                                return true;
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = npl.SoLuongDaNhap - npl.SoLuongDaDung + npl.SoLuongCungUng;
                            if (SoLuongConDuocGiao < SoLuongHienTai)
                            {
                                if (showMsg("MSG_2702008", SoLuongConDuocGiao, true) == "Yes")
                                //if (ShowMessage("Bạn đã giao quá số lượng còn được giao là : " + SoLuongConDuocGiao.ToString() + ". Bạn có muốn tiếp tục không ?", true) == "Yes")
                                {
                                    return true;
                                }
                                else
                                    return false;
                            }
                            else
                                return true;
                        }

                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
                case "19"://"SP":
                    if (this.MaLoaiHinh.Substring(0,1).Equals("N"))
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl1 = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl1.HopDong_ID = HD.ID;
                        npl1.Ma = txtMaHang.Text;
                        if (npl1.Load())
                        {
                            if (this.MaLoaiHinh.Substring(0,1).Equals("N"))
                            {
                                decimal SoLuongConDuocNhap = npl1.SoLuongDangKy - npl1.SoLuongDaNhap - npl1.SoLuongCungUng;
                                if (SoLuongHienTai > SoLuongConDuocNhap)
                                {
                                    if (showMsg("MSG_240237", SoLuongConDuocNhap, true) == "Yes")
                                    //if (ShowMessage("Bạn đã nhập quá số lượng còn được nhập là : " + SoLuongConDuocNhap.ToString() + ". Bạn có muốn tiếp tục không ?", true) == "Yes")
                                    {
                                        return true;
                                    }
                                    else
                                        return false;
                                }
                                else
                                    return true;
                            }
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    else
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.Ma = txtMaHang.Text.Trim();
                        if (sp.Load())
                        {
                            if (this.MaLoaiHinh.Substring(0,1).Equals("X"))
                            {
                                decimal SoLuongConDuocNhap = sp.SoLuongDangKy - sp.SoLuongDaXuat;
                                if (SoLuongConDuocNhap < SoLuongHienTai)
                                {
                                    if (showMsg("MSG_240237", SoLuongConDuocNhap, true) == "Yes")
                                    //if (ShowMessage("Bạn đã nhập quá số lượng còn được nhập là : " + SoLuongConDuocNhap.ToString() + ". Bạn có muốn tiếp tục không ?", true) == "Yes")
                                    {
                                        return true;
                                    }
                                    else
                                        return false;
                                }
                                else
                                    return true;
                            }
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại sản phẩm này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    break;
                case "20"://"TB":
                    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                    tb.HopDong_ID = HD.ID;
                    tb.Ma = txtMaHang.Text.Trim();
                    if (tb.Load())
                    {
                        if (this.MaLoaiHinh.Substring(0,1).Equals("N"))
                        {
                            decimal SoLuongConDuocNhap = tb.SoLuongDangKy - tb.SoLuongDaNhap;
                            if (SoLuongConDuocNhap < SoLuongHienTai)
                            {
                                if (showMsg("MSG_240237", SoLuongConDuocNhap, true) == "Yes")
                                //if (ShowMessage("Bạn đã nhập quá số lượng còn được nhập là : " + SoLuongConDuocNhap.ToString() + ". Bạn có muốn tiếp tục không ?", true) == "Yes")
                                {
                                    return true;
                                }
                                else
                                    return false;
                            }
                            else return true;
                        }
                        else
                        {
                            decimal SoLuongConDuocGiao = tb.SoLuongDaNhap;
                            if (SoLuongConDuocGiao < SoLuongHienTai)
                            {
                                if (showMsg("MSG_240238", SoLuongConDuocGiao, true) == "Yes")
                                //if (ShowMessage("Bạn đã nhập quá số lượng còn được giao là : " + SoLuongConDuocGiao.ToString() + ". Bạn có muốn tiếp tục không ?", true) == "Yes")
                                {
                                    return true;
                                }
                                else
                                    return false;
                            }
                            else return true;
                        }
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
            }

            return false;
        }

        private void KiemTraTonTaiHang()
        {
            switch (this.MaLoaiHinh.Substring(2, 2))
            {
                case "PL":
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.HopDong_ID = HD.ID;
                    npl.Ma = txtMaHang.Text;
                    if (npl.Load())
                    {
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
                case "SP":
                    if (this.MaLoaiHinh.EndsWith("N"))
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl1 = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl1.HopDong_ID = HD.ID;
                        npl1.Ma = txtMaHang.Text;
                        if (npl1.Load())
                        {
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    else
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.Ma = txtMaHang.Text;
                        if (sp.Load())
                        {
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại sản phầm này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    break;
                case "TB":
                    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                    tb.HopDong_ID = HD.ID;
                    tb.Ma = txtMaHang.Text;
                    if (tb.Load())
                    {
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
            }

            KiemTraTonTaiHangUpdate();
        }
        private void KiemTraTonTaiHangUpdate()
        {
            switch (this.MaLoaiHinh.Substring(3, 2))
            {
                case "18"://"PL":
                    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                    npl.HopDong_ID = HD.ID;
                    npl.Ma = txtMaHang.Text;
                    if (npl.Load())
                    {
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
                case "19"://"SP":
                    if (this.MaLoaiHinh.Substring(0,1).Equals("N"))
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl1 = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl1.HopDong_ID = HD.ID;
                        npl1.Ma = txtMaHang.Text;
                        if (npl1.Load())
                        {
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    else
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.Ma = txtMaHang.Text;
                        if (sp.Load())
                        {
                            epError.SetError(txtMaHang, null);
                            epError.SetError(txtTenHang, null);
                            epError.SetError(txtMaHS, null);
                        }
                        else
                        {
                            this.IsExit = false;
                            epError.SetIconPadding(txtMaHang, -8);
                            epError.SetError(txtMaHang, setText("Không tồn tại sản phầm này.", "This value is not exist"));

                            txtTenHang.Text = txtMaHS.Text = string.Empty;
                        }
                    }
                    break;
                case "20"://"TB":
                    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                    tb.HopDong_ID = HD.ID;
                    tb.Ma = txtMaHang.Text;
                    if (tb.Load())
                    {
                        epError.SetError(txtMaHang, null);
                        epError.SetError(txtTenHang, null);
                        epError.SetError(txtMaHS, null);
                    }
                    else
                    {
                        this.IsExit = false;
                        epError.SetIconPadding(txtMaHang, -8);
                        epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                    }
                    break;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (TKCT.ID > 0)
            {
                if (TKCT.MaLoaiHinh.Contains("X"))
                {
                    if (Company.GC.BLL.GC.PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(TKCT.ID))
                    {
                        showMsg("MSG_ALL01");
                        //ShowMessage("Tờ khai chuyển tiếp có id = " + TKCT.ID + " này đã được phân bổ nên không thể xóa được.", false);
                        return;
                    }
                }
            }
            List<HangChuyenTiep> hctColl = new List<HangChuyenTiep>();
            if (TKCT.HCTCollection.Count <= 0) return;
            GridEXSelectedItemCollection gritemColl = dgList.SelectedItems;

            if (gritemColl.Count < 0) return;
            //string LoaiHangHoa = "N";
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem item in gritemColl)
                {
                    if (item.RowType == RowType.Record)
                    {
                        HangChuyenTiep hct = (HangChuyenTiep)item.GetRow().DataRow;
                        if (hct.ID > 0)
                        {
                            string LoaiHangHoa = "N";
                            if (TKCT.MaLoaiHinh.IndexOf("SP") > 0 || TKCT.MaLoaiHinh.IndexOf("19") > 0)
                                LoaiHangHoa = "S";
                            else if (TKCT.MaLoaiHinh.IndexOf("TB") > 0 || TKCT.MaLoaiHinh.IndexOf("20") > 0)
                                LoaiHangHoa = "T";
                            if (hct.ID > 0)
                            {
                                hct.Delete(TKCT.IDHopDong, LoaiHangHoa, TKCT.MaLoaiHinh);
                            }
                        }
                        hctColl.Add(hct);
                    }

                }
                foreach (HangChuyenTiep hctt in hctColl)
                {
                    this.TKCT.HCTCollection.Remove(hctt);
                }
                dgList.DataSource = this.TKCT.HCTCollection;
                this.Clear();
                try
                {

                    dgList.Refetch();
                }
                catch { dgList.Refresh(); }
            }
        }
        private void Clear()
        {
            txtMaHang.Text = "";
            txtTenHang.Text = "";
            txtMaHS.Text = "";
            txtLuong.Text = "0.000";
            txtLuongCon.Text = "0.000";
            txtDGNT.Text = "0";
            txtTGNT.Text = "0.00";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            ctrNuocXX.Ma = GlobalSettings.NUOC;
        }

        private void txtTenHang_TextChanged(object sender, EventArgs e)
        {
            if (txtTenHang.Text != "")
            {
                epError.Clear();

            }
        }
        
    }
}