using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_TaxClassificationCode : ICloneable
	{
		#region Properties.
		
		public string ResultCode { set; get; }
		public int PGNumber { set; get; }
		public string TableID { set; get; }
		public string ProcessClassification { set; get; }
		public int CreatorClassification { set; get; }
		public string NumberOfKeyItems { set; get; }
		public string TaxCode { set; get; }
		public string TaxName { set; get; }
		public decimal IndicationOfCommonTax { set; get; }
		public decimal IndicationOfPreferantialTax { set; get; }
		public decimal IndicationOfMFNTax { set; get; }
		public decimal IndicationOfFTATax { set; get; }
		public decimal IndicationOfOutOfQuota { set; get; }
		public decimal IndicationOfSpecificDuty { set; get; }
		public decimal IndicationOfSpecificDutyAndAdValoremDuty { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<VNACC_Category_TaxClassificationCode> ConvertToCollection(IDataReader reader)
		{
			List<VNACC_Category_TaxClassificationCode> collection = new List<VNACC_Category_TaxClassificationCode>();
			while (reader.Read())
			{
				VNACC_Category_TaxClassificationCode entity = new VNACC_Category_TaxClassificationCode();
				if (!reader.IsDBNull(reader.GetOrdinal("ResultCode"))) entity.ResultCode = reader.GetString(reader.GetOrdinal("ResultCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("PGNumber"))) entity.PGNumber = reader.GetInt32(reader.GetOrdinal("PGNumber"));
				if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ProcessClassification"))) entity.ProcessClassification = reader.GetString(reader.GetOrdinal("ProcessClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatorClassification"))) entity.CreatorClassification = reader.GetInt32(reader.GetOrdinal("CreatorClassification"));
				if (!reader.IsDBNull(reader.GetOrdinal("NumberOfKeyItems"))) entity.NumberOfKeyItems = reader.GetString(reader.GetOrdinal("NumberOfKeyItems"));
				if (!reader.IsDBNull(reader.GetOrdinal("TaxCode"))) entity.TaxCode = reader.GetString(reader.GetOrdinal("TaxCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("TaxName"))) entity.TaxName = reader.GetString(reader.GetOrdinal("TaxName"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndicationOfCommonTax"))) entity.IndicationOfCommonTax = reader.GetDecimal(reader.GetOrdinal("IndicationOfCommonTax"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndicationOfPreferantialTax"))) entity.IndicationOfPreferantialTax = reader.GetDecimal(reader.GetOrdinal("IndicationOfPreferantialTax"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndicationOfMFNTax"))) entity.IndicationOfMFNTax = reader.GetDecimal(reader.GetOrdinal("IndicationOfMFNTax"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndicationOfFTATax"))) entity.IndicationOfFTATax = reader.GetDecimal(reader.GetOrdinal("IndicationOfFTATax"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndicationOfOutOfQuota"))) entity.IndicationOfOutOfQuota = reader.GetDecimal(reader.GetOrdinal("IndicationOfOutOfQuota"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndicationOfSpecificDuty"))) entity.IndicationOfSpecificDuty = reader.GetDecimal(reader.GetOrdinal("IndicationOfSpecificDuty"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndicationOfSpecificDutyAndAdValoremDuty"))) entity.IndicationOfSpecificDutyAndAdValoremDuty = reader.GetDecimal(reader.GetOrdinal("IndicationOfSpecificDutyAndAdValoremDuty"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<VNACC_Category_TaxClassificationCode> collection, string taxCode)
        {
            foreach (VNACC_Category_TaxClassificationCode item in collection)
            {
                if (item.TaxCode == taxCode)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_VNACC_Category_TaxClassificationCode VALUES(@ResultCode, @PGNumber, @TableID, @ProcessClassification, @CreatorClassification, @NumberOfKeyItems, @TaxCode, @TaxName, @IndicationOfCommonTax, @IndicationOfPreferantialTax, @IndicationOfMFNTax, @IndicationOfFTATax, @IndicationOfOutOfQuota, @IndicationOfSpecificDuty, @IndicationOfSpecificDutyAndAdValoremDuty, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_TaxClassificationCode SET ResultCode = @ResultCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, CreatorClassification = @CreatorClassification, NumberOfKeyItems = @NumberOfKeyItems, TaxName = @TaxName, IndicationOfCommonTax = @IndicationOfCommonTax, IndicationOfPreferantialTax = @IndicationOfPreferantialTax, IndicationOfMFNTax = @IndicationOfMFNTax, IndicationOfFTATax = @IndicationOfFTATax, IndicationOfOutOfQuota = @IndicationOfOutOfQuota, IndicationOfSpecificDuty = @IndicationOfSpecificDuty, IndicationOfSpecificDutyAndAdValoremDuty = @IndicationOfSpecificDutyAndAdValoremDuty, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE TaxCode = @TaxCode";
            string delete = "DELETE FROM t_VNACC_Category_TaxClassificationCode WHERE TaxCode = @TaxCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TaxCode", SqlDbType.VarChar, "TaxCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TaxName", SqlDbType.NVarChar, "TaxName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfCommonTax", SqlDbType.Decimal, "IndicationOfCommonTax", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfPreferantialTax", SqlDbType.Decimal, "IndicationOfPreferantialTax", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfMFNTax", SqlDbType.Decimal, "IndicationOfMFNTax", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfFTATax", SqlDbType.Decimal, "IndicationOfFTATax", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfOutOfQuota", SqlDbType.Decimal, "IndicationOfOutOfQuota", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfSpecificDuty", SqlDbType.Decimal, "IndicationOfSpecificDuty", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfSpecificDutyAndAdValoremDuty", SqlDbType.Decimal, "IndicationOfSpecificDutyAndAdValoremDuty", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TaxCode", SqlDbType.VarChar, "TaxCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TaxName", SqlDbType.NVarChar, "TaxName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfCommonTax", SqlDbType.Decimal, "IndicationOfCommonTax", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfPreferantialTax", SqlDbType.Decimal, "IndicationOfPreferantialTax", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfMFNTax", SqlDbType.Decimal, "IndicationOfMFNTax", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfFTATax", SqlDbType.Decimal, "IndicationOfFTATax", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfOutOfQuota", SqlDbType.Decimal, "IndicationOfOutOfQuota", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfSpecificDuty", SqlDbType.Decimal, "IndicationOfSpecificDuty", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfSpecificDutyAndAdValoremDuty", SqlDbType.Decimal, "IndicationOfSpecificDutyAndAdValoremDuty", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@TaxCode", SqlDbType.VarChar, "TaxCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_VNACC_Category_TaxClassificationCode VALUES(@ResultCode, @PGNumber, @TableID, @ProcessClassification, @CreatorClassification, @NumberOfKeyItems, @TaxCode, @TaxName, @IndicationOfCommonTax, @IndicationOfPreferantialTax, @IndicationOfMFNTax, @IndicationOfFTATax, @IndicationOfOutOfQuota, @IndicationOfSpecificDuty, @IndicationOfSpecificDutyAndAdValoremDuty, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_VNACC_Category_TaxClassificationCode SET ResultCode = @ResultCode, PGNumber = @PGNumber, TableID = @TableID, ProcessClassification = @ProcessClassification, CreatorClassification = @CreatorClassification, NumberOfKeyItems = @NumberOfKeyItems, TaxName = @TaxName, IndicationOfCommonTax = @IndicationOfCommonTax, IndicationOfPreferantialTax = @IndicationOfPreferantialTax, IndicationOfMFNTax = @IndicationOfMFNTax, IndicationOfFTATax = @IndicationOfFTATax, IndicationOfOutOfQuota = @IndicationOfOutOfQuota, IndicationOfSpecificDuty = @IndicationOfSpecificDuty, IndicationOfSpecificDutyAndAdValoremDuty = @IndicationOfSpecificDutyAndAdValoremDuty, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE TaxCode = @TaxCode";
            string delete = "DELETE FROM t_VNACC_Category_TaxClassificationCode WHERE TaxCode = @TaxCode";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TaxCode", SqlDbType.VarChar, "TaxCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TaxName", SqlDbType.NVarChar, "TaxName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfCommonTax", SqlDbType.Decimal, "IndicationOfCommonTax", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfPreferantialTax", SqlDbType.Decimal, "IndicationOfPreferantialTax", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfMFNTax", SqlDbType.Decimal, "IndicationOfMFNTax", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfFTATax", SqlDbType.Decimal, "IndicationOfFTATax", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfOutOfQuota", SqlDbType.Decimal, "IndicationOfOutOfQuota", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfSpecificDuty", SqlDbType.Decimal, "IndicationOfSpecificDuty", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndicationOfSpecificDutyAndAdValoremDuty", SqlDbType.Decimal, "IndicationOfSpecificDutyAndAdValoremDuty", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ResultCode", SqlDbType.VarChar, "ResultCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PGNumber", SqlDbType.Int, "PGNumber", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TableID", SqlDbType.VarChar, "TableID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ProcessClassification", SqlDbType.VarChar, "ProcessClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatorClassification", SqlDbType.Int, "CreatorClassification", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NumberOfKeyItems", SqlDbType.VarChar, "NumberOfKeyItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TaxCode", SqlDbType.VarChar, "TaxCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TaxName", SqlDbType.NVarChar, "TaxName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfCommonTax", SqlDbType.Decimal, "IndicationOfCommonTax", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfPreferantialTax", SqlDbType.Decimal, "IndicationOfPreferantialTax", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfMFNTax", SqlDbType.Decimal, "IndicationOfMFNTax", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfFTATax", SqlDbType.Decimal, "IndicationOfFTATax", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfOutOfQuota", SqlDbType.Decimal, "IndicationOfOutOfQuota", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfSpecificDuty", SqlDbType.Decimal, "IndicationOfSpecificDuty", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndicationOfSpecificDutyAndAdValoremDuty", SqlDbType.Decimal, "IndicationOfSpecificDutyAndAdValoremDuty", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@TaxCode", SqlDbType.VarChar, "TaxCode", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static VNACC_Category_TaxClassificationCode Load(string taxCode)
		{
			const string spName = "[dbo].[p_VNACC_Category_TaxClassificationCode_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TaxCode", SqlDbType.VarChar, taxCode);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<VNACC_Category_TaxClassificationCode> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<VNACC_Category_TaxClassificationCode> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<VNACC_Category_TaxClassificationCode> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertVNACC_Category_TaxClassificationCode(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string taxName, decimal indicationOfCommonTax, decimal indicationOfPreferantialTax, decimal indicationOfMFNTax, decimal indicationOfFTATax, decimal indicationOfOutOfQuota, decimal indicationOfSpecificDuty, decimal indicationOfSpecificDutyAndAdValoremDuty, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_TaxClassificationCode entity = new VNACC_Category_TaxClassificationCode();	
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.TaxName = taxName;
			entity.IndicationOfCommonTax = indicationOfCommonTax;
			entity.IndicationOfPreferantialTax = indicationOfPreferantialTax;
			entity.IndicationOfMFNTax = indicationOfMFNTax;
			entity.IndicationOfFTATax = indicationOfFTATax;
			entity.IndicationOfOutOfQuota = indicationOfOutOfQuota;
			entity.IndicationOfSpecificDuty = indicationOfSpecificDuty;
			entity.IndicationOfSpecificDutyAndAdValoremDuty = indicationOfSpecificDutyAndAdValoremDuty;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_VNACC_Category_TaxClassificationCode_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@TaxCode", SqlDbType.VarChar, TaxCode);
			db.AddInParameter(dbCommand, "@TaxName", SqlDbType.NVarChar, TaxName);
			db.AddInParameter(dbCommand, "@IndicationOfCommonTax", SqlDbType.Decimal, IndicationOfCommonTax);
			db.AddInParameter(dbCommand, "@IndicationOfPreferantialTax", SqlDbType.Decimal, IndicationOfPreferantialTax);
			db.AddInParameter(dbCommand, "@IndicationOfMFNTax", SqlDbType.Decimal, IndicationOfMFNTax);
			db.AddInParameter(dbCommand, "@IndicationOfFTATax", SqlDbType.Decimal, IndicationOfFTATax);
			db.AddInParameter(dbCommand, "@IndicationOfOutOfQuota", SqlDbType.Decimal, IndicationOfOutOfQuota);
			db.AddInParameter(dbCommand, "@IndicationOfSpecificDuty", SqlDbType.Decimal, IndicationOfSpecificDuty);
			db.AddInParameter(dbCommand, "@IndicationOfSpecificDutyAndAdValoremDuty", SqlDbType.Decimal, IndicationOfSpecificDutyAndAdValoremDuty);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<VNACC_Category_TaxClassificationCode> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_TaxClassificationCode item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateVNACC_Category_TaxClassificationCode(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string taxCode, string taxName, decimal indicationOfCommonTax, decimal indicationOfPreferantialTax, decimal indicationOfMFNTax, decimal indicationOfFTATax, decimal indicationOfOutOfQuota, decimal indicationOfSpecificDuty, decimal indicationOfSpecificDutyAndAdValoremDuty, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_TaxClassificationCode entity = new VNACC_Category_TaxClassificationCode();			
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.TaxCode = taxCode;
			entity.TaxName = taxName;
			entity.IndicationOfCommonTax = indicationOfCommonTax;
			entity.IndicationOfPreferantialTax = indicationOfPreferantialTax;
			entity.IndicationOfMFNTax = indicationOfMFNTax;
			entity.IndicationOfFTATax = indicationOfFTATax;
			entity.IndicationOfOutOfQuota = indicationOfOutOfQuota;
			entity.IndicationOfSpecificDuty = indicationOfSpecificDuty;
			entity.IndicationOfSpecificDutyAndAdValoremDuty = indicationOfSpecificDutyAndAdValoremDuty;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_VNACC_Category_TaxClassificationCode_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@TaxCode", SqlDbType.VarChar, TaxCode);
			db.AddInParameter(dbCommand, "@TaxName", SqlDbType.NVarChar, TaxName);
			db.AddInParameter(dbCommand, "@IndicationOfCommonTax", SqlDbType.Decimal, IndicationOfCommonTax);
			db.AddInParameter(dbCommand, "@IndicationOfPreferantialTax", SqlDbType.Decimal, IndicationOfPreferantialTax);
			db.AddInParameter(dbCommand, "@IndicationOfMFNTax", SqlDbType.Decimal, IndicationOfMFNTax);
			db.AddInParameter(dbCommand, "@IndicationOfFTATax", SqlDbType.Decimal, IndicationOfFTATax);
			db.AddInParameter(dbCommand, "@IndicationOfOutOfQuota", SqlDbType.Decimal, IndicationOfOutOfQuota);
			db.AddInParameter(dbCommand, "@IndicationOfSpecificDuty", SqlDbType.Decimal, IndicationOfSpecificDuty);
			db.AddInParameter(dbCommand, "@IndicationOfSpecificDutyAndAdValoremDuty", SqlDbType.Decimal, IndicationOfSpecificDutyAndAdValoremDuty);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<VNACC_Category_TaxClassificationCode> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_TaxClassificationCode item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateVNACC_Category_TaxClassificationCode(string resultCode, int pGNumber, string tableID, string processClassification, int creatorClassification, string numberOfKeyItems, string taxCode, string taxName, decimal indicationOfCommonTax, decimal indicationOfPreferantialTax, decimal indicationOfMFNTax, decimal indicationOfFTATax, decimal indicationOfOutOfQuota, decimal indicationOfSpecificDuty, decimal indicationOfSpecificDutyAndAdValoremDuty, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			VNACC_Category_TaxClassificationCode entity = new VNACC_Category_TaxClassificationCode();			
			entity.ResultCode = resultCode;
			entity.PGNumber = pGNumber;
			entity.TableID = tableID;
			entity.ProcessClassification = processClassification;
			entity.CreatorClassification = creatorClassification;
			entity.NumberOfKeyItems = numberOfKeyItems;
			entity.TaxCode = taxCode;
			entity.TaxName = taxName;
			entity.IndicationOfCommonTax = indicationOfCommonTax;
			entity.IndicationOfPreferantialTax = indicationOfPreferantialTax;
			entity.IndicationOfMFNTax = indicationOfMFNTax;
			entity.IndicationOfFTATax = indicationOfFTATax;
			entity.IndicationOfOutOfQuota = indicationOfOutOfQuota;
			entity.IndicationOfSpecificDuty = indicationOfSpecificDuty;
			entity.IndicationOfSpecificDutyAndAdValoremDuty = indicationOfSpecificDutyAndAdValoremDuty;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_TaxClassificationCode_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ResultCode", SqlDbType.VarChar, ResultCode);
			db.AddInParameter(dbCommand, "@PGNumber", SqlDbType.Int, PGNumber);
			db.AddInParameter(dbCommand, "@TableID", SqlDbType.VarChar, TableID);
			db.AddInParameter(dbCommand, "@ProcessClassification", SqlDbType.VarChar, ProcessClassification);
			db.AddInParameter(dbCommand, "@CreatorClassification", SqlDbType.Int, CreatorClassification);
			db.AddInParameter(dbCommand, "@NumberOfKeyItems", SqlDbType.VarChar, NumberOfKeyItems);
			db.AddInParameter(dbCommand, "@TaxCode", SqlDbType.VarChar, TaxCode);
			db.AddInParameter(dbCommand, "@TaxName", SqlDbType.NVarChar, TaxName);
			db.AddInParameter(dbCommand, "@IndicationOfCommonTax", SqlDbType.Decimal, IndicationOfCommonTax);
			db.AddInParameter(dbCommand, "@IndicationOfPreferantialTax", SqlDbType.Decimal, IndicationOfPreferantialTax);
			db.AddInParameter(dbCommand, "@IndicationOfMFNTax", SqlDbType.Decimal, IndicationOfMFNTax);
			db.AddInParameter(dbCommand, "@IndicationOfFTATax", SqlDbType.Decimal, IndicationOfFTATax);
			db.AddInParameter(dbCommand, "@IndicationOfOutOfQuota", SqlDbType.Decimal, IndicationOfOutOfQuota);
			db.AddInParameter(dbCommand, "@IndicationOfSpecificDuty", SqlDbType.Decimal, IndicationOfSpecificDuty);
			db.AddInParameter(dbCommand, "@IndicationOfSpecificDutyAndAdValoremDuty", SqlDbType.Decimal, IndicationOfSpecificDutyAndAdValoremDuty);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<VNACC_Category_TaxClassificationCode> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_TaxClassificationCode item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteVNACC_Category_TaxClassificationCode(string taxCode)
		{
			VNACC_Category_TaxClassificationCode entity = new VNACC_Category_TaxClassificationCode();
			entity.TaxCode = taxCode;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACC_Category_TaxClassificationCode_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TaxCode", SqlDbType.VarChar, TaxCode);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_VNACC_Category_TaxClassificationCode_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<VNACC_Category_TaxClassificationCode> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACC_Category_TaxClassificationCode item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}