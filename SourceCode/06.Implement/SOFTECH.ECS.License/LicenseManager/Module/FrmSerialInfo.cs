using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Activation.Server;

namespace Licenses_v2.Module
{
    public partial class FrmSerialInfo : DevExpress.XtraEditors.XtraForm
    {
        private Serial Ser;
        public Boolean isEdit = false;
        public FrmSerialInfo(string Code)
        {
            InitializeComponent();
            if (Code == null)
            {
                this.Text = "Thêm Serial";
                lblCaption.Text = "Thêm Serial";
                Ser = new Serial();
                this.Name = "Ser_New";
                this.isEdit = true;
                txtSerCreateDate.Text = DateTime.Now.ToString();
                txtSerEmployeeID.Text = Program.LoginID;
            }
            else
            {
                Ser = Serial.Load(Code);
                this.Text = "Serial: " + Code;
                lblCaption.Text = "Serial: " + Code;
                this.Name = "Ser_" + Code;
                this.FillData();
            }
        }

        private void FrmSerialInfo_Load(object sender, EventArgs e)
        {
            cboxSerProduct.Properties.DataSource = Product.SelectAll().Tables[0];
            cboxSerProduct.Properties.DisplayMember = "Name";
            cboxSerProduct.Properties.ValueMember = "ID";

            cboxSerLicense.Properties.DataSource = Activation.Server.License.SelectAll().Tables[0];
            cboxSerLicense.Properties.DisplayMember = "Name";
            cboxSerLicense.Properties.ValueMember = "ID";
            
            this.DisableButton(isEdit);
        }

        private void cboxSerProduct_EditValueChanged(object sender, EventArgs e)
        {
            txtSerProduct.Text = cboxSerProduct.EditValue.ToString();
        }

        private void cboxSerLicense_EditValueChanged(object sender, EventArgs e)
        {
            txtSerLicense.Text = cboxSerLicense.EditValue.ToString();
        }

        private void btnGenCode_Click(object sender, EventArgs e)
        {
            if (txtSerLicense.Text != "" && txtSerProduct.Text != "" && txtSerCreateDate.Text != "")
            {
                string str = txtSerLicense.Text + txtSerProduct.Text + txtSerCreateDate.Text;
                txtSerSerial.Text = Activation.Server.ClassLib.encryptString(str);
            }
            else MessageBox.Show("Thông tin chưa đầy đủ");
        }

        #region Add Methods.
        private void GetData()
        {
            Ser.Code = txtSerSerial.Text;
            Ser.ProductID = txtSerProduct.Text;
            Ser.LicenseID = txtSerLicense.Text;
            Ser.CreateDate = DateTime.Parse(txtSerCreateDate.Text);
            Ser.EmployeeID = txtSerEmployeeID.Text;
            Ser.Status = Boolean.Parse(checkSerStatus.EditValue.ToString());
        }

        private void FillData()
        {
            txtSerSerial.Text = Ser.Code;
            txtSerProduct.Text = Ser.ProductID;
            cboxSerProduct.EditValue = Ser.ProductID;
            txtSerLicense.Text = Ser.LicenseID;
            cboxSerLicense.EditValue = Ser.LicenseID;
            txtSerCreateDate.Text = Ser.CreateDate.ToShortDateString();
            txtSerEmployeeID.Text = Ser.EmployeeID;
            checkSerStatus.EditValue = Ser.Status;
        }

        private void DisableButton(Boolean val)
        {
            btnCancel.Enabled = val;
            btnSave.Enabled = val;
            btnGenCode.Enabled = val;
            groupSerialInfo.Enabled = val;
            btnEdit.Enabled = !val;
        }
        #endregion

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.GetData();
            if (this.Name == "Ser_New")
            {
                if (txtSerSerial.Text != "")
                {
                    Ser.Insert();
                    this.Text = "Serial: " + Ser.Code;
                    this.Name = "Ser_" + Ser.Code;
                    this.DisableButton(false);
                    lblCaption.Text = "Serial: " + Ser.Code;
                }
                else MessageBox.Show("Chưa khởi tạo Serial");
            }
            else
            {
                Ser.Update();
                this.DisableButton(false);
            }
            if (ClassLib.IsFormShowed("FrmSerialManager"))
            {
                FrmSerialManager obj = (FrmSerialManager)Application.OpenForms["FrmSerialManager"];
                obj.RefreshData();
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (this.Name == "Ser_New")
            {
                this.Close();
            }
            else
            {
                this.FillData();
                this.DisableButton(false);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            this.isEdit = true;
            this.DisableButton(true);
        }

    }
}