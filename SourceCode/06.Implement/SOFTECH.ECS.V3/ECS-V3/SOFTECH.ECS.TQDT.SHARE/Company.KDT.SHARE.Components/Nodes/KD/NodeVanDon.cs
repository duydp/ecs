﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Nodes.KD
{
    public class NodeVanDon
    {
        public const string Declaration = "Declaration";
        
        /// <summary>
        /// Loại chứng từ 	1	an..3	Danh mục chuẩn
        /// </summary>
        public const string issuer = "issuer";

        /// <summary>
        /// Số tham chiếu chứng từ 	1	an..35 || Số TK || Số vận đơn
        /// </summary>
        public const string reference = "reference";

        /// <summary>
        ///Ngày khai chứng từ 	1	an19	YYYY-MM-DD HH:mm:ss || Ngày TK || Ngày vận đơn
        /// </summary>
        public const string issue = "issue";

        /// <summary>
        /// Chức năng của chứng từ(=8) 1 n..2 Danh mục chuẩn || Ngày TK || Ngày phát hành hoá đơn thương mại
        /// </summary>
        public const string function = "function";

        /// <summary>
        /// Nơi khai báo	0	an..60 || Mã nước cấp C/O
        /// </summary>
        public const string issueLocation = "issueLocation";

        /// <summary>
        /// Trạng thái của chứng từ	1 an..3 Danh mục chuẩn || Trạng thái đại lý
        /// </summary>
        public const string status = "status";

        /// <summary>
        /// Số đăng ký chứng từ  	
        /// </summary>
        public const string customsReference = "customsReference";

        /// <summary>
        /// Ngày đăng ký chứng từ  0  an 19	YYYY-MM-DD HH:mm:ss
        /// </summary>
        public const string acceptance = "acceptance";

        /// <summary>
        /// Mã hải quan 	1	an..6	Danh mục chuẩn || Mã hải quan 
        /// </summary>
        public const string declarationOffice = "declarationOffice";

        /// <summary>
        /// Thông tin người khai hải quan
        /// </summary>
        public const string Agent = "Agent";

        /// <summary>
        ///Tên người khai hải quan||Tên doanh nghiệp||Tên hãng vận tải||Tên người gửi hàng||Tên người nhận hàng||Người được thông báo||Cảng xếp hàng||Cảng dỡ hàng
        /// </summary>
        
        /// <summary>
        ///Mã người khai hải quan||Mã doanh nghiệp||Số hiệu PTVT||Mã hãng vận tải||Mã người gửi hàng||Mã người nhận hàng||Người được thông báo
        /// </summary>
        public const string identity = "identity";

        /// <summary>
        ///Tên người khai hải quan||Tên doanh nghiệp||Tên hãng vận tải||Tên người gửi hàng||Tên người nhận hàng||Người được thông báo||Cảng xếp hàng||Cảng dỡ hàng
        /// </summary>
        public const string name = "name";
        
        /// <summary>
        /// Forwarder
        /// </summary>
        public const string Importer = "Importer";

        /// <summary>
        /// Thông tin tham chiếu đến Tờ khai
        /// </summary>
        public const string DeclarationDocument = "DeclarationDocument";
        
        /// <summary>
        /// Mã LH	1	an..10	Danh mục chuẩn
        /// </summary>
        public const string natureOfTransaction = "natureOfTransaction";

        /// <summary>
        /// Phương thức thanh toán	1	a..10	Danh mục chuẩn
        /// </summary>

        /// <summary>
        /// 2	Vận đơn
        /// </summary>
        public const string BillOfLadings = "BillOfLadings";
        public const string BillOfLading = "BillOfLading";

        public const string BorderTransportMeans = "BorderTransportMeans";

        /// <summary>
        /// Tên PTVT	1	an..35 ||Số hiệu container || Mã hàng do nhà sản xuất hoặc doanh nghiệp quy định
        /// </summary>
        public const string identification = "identification";								

        /// <summary>
        ///Số hiệu chuyến đi	1
        /// </summary>
        public const string journey = "journey";								

        /// <summary>
        /// Kiểu PTVT	0
        /// </summary>
        public const string modeAndType = "modeAndType";								

        /// <summary>
        ///Ngày khởi hành	1	an..10	YYYY-MM-DD
        /// </summary>
        public const string departure = "departure";								

        /// <summary>
        /// Quốc tịch PTVT	1	a2
        /// </summary>
        public const string registrationNationality = "registrationNationality"; 									

        public const string Carrier = "Carrier";
        public const string Consignment = "Consignment";
        public const string Consignor = "Consignor";
        public const string Consignee = "Consignee";
        public const string NotifyParty = "NotifyParty";
        public const string LoadingLocation = "LoadingLocation";
        public const string code = "code";

        /// <summary>
        ///Ngày phương tiện vận tải khởi hành	1	an10	YYYY-MM-DD
        /// </summary>
        public const string loading = "loading";

        public const string UnloadingLocation = "UnloadingLocation";

        /// <summary>
        ///Ngày đến của phương tiện vận tải	1	an10	YYYY-MM-DD
        /// </summary>
        public const string arrival = "arrival";							

        public const string DeliveryDestination = "DeliveryDestination";
		
		/// <summary>
        /// Cảng giao hàng/Cảng đích	1	an..35	an..4 nếu có mã
		/// </summary>
	    public const string line = "line";

        /// <summary>
        /// Packaging
        /// </summary>
        public const string ConsignmentItemPackaging = "ConsignmentItemPackaging";

        /// <summary>
        ///Tổng số kiện	1	n..8 
        /// </summary>
        public const string quantity = "quantity";	
		
		/// <summary>
        ///Loại kiện	1	an..2
		/// </summary>
        public const string type = "type";

        /// <summary>
        /// Container
        /// </summary>
        public const string TransportEquipment = "TransportEquipment";
        public const string characteristic = "characteristic";
        public const string fullness = "fullness";

        /// <summary>
        ///Số seal container	1	an..35
        /// </summary>
        public const string seal = "seal";							
        public const string EquipmentIdentification = "EquipmentIdentification";
        public const string ConsignmentItem = "ConsignmentItem";
        public const string sequence = "sequence";
        public const string markNumber = "markNumber";
	    
        /// <summary>
        /// Hàng hoá
        /// </summary>
        public const string Commodity = "Commodity";							

        /// <summary>
        ///Tên hàng/Mô tả hàng hoá 	1 an..256	
        /// </summary>
        public const string description = "description";							
		
        /// <summary>
        ///Mã hàng	1	an..12	Tối thiểu 6 số
        /// </summary>
        public const string tariffClassification = "tariffClassification";
        public const string GoodMeasure = "GoodMeasure";

        /// <summary>
        ///Tổng trọng lượng	1	n..11,3 	
        /// </summary>
        public const string grossMass = "grossMass";	
		
		/// <summary>
        ///Mã đơn vị tính	1	an..3	Danh mục chuẩn
		/// </summary>
        public const string measureUnit = "measureUnit";
    }
}
