﻿namespace Company.Interface
{
    partial class FrmCauHinhThoiGian
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCauHinhThoiGian));
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbarConnected = new Janus.Windows.EditControls.UITrackBar();
            this.tbarCounter = new Janus.Windows.EditControls.UITrackBar();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.tbarCountSend = new Janus.Windows.EditControls.UITrackBar();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox6);
            this.grbMain.Size = new System.Drawing.Size(335, 195);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.label1);
            this.uiGroupBox6.Controls.Add(this.label2);
            this.uiGroupBox6.Controls.Add(this.label3);
            this.uiGroupBox6.Controls.Add(this.tbarConnected);
            this.uiGroupBox6.Controls.Add(this.tbarCountSend);
            this.uiGroupBox6.Controls.Add(this.tbarCounter);
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 3);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(329, 139);
            this.uiGroupBox6.TabIndex = 0;
            this.uiGroupBox6.Text = "Thông tin";
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Thời gian kết nối";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Thời gian đếm";
            // 
            // tbarConnected
            // 
            this.tbarConnected.Location = new System.Drawing.Point(114, 56);
            this.tbarConnected.Maximum = 3;
            this.tbarConnected.Minimum = 1;
            this.tbarConnected.Name = "tbarConnected";
            this.tbarConnected.Size = new System.Drawing.Size(206, 30);
            this.tbarConnected.SmallChange = 2;
            this.tbarConnected.TabIndex = 0;
            this.tbarConnected.ThumbColor = System.Drawing.SystemColors.Control;
            this.tbarConnected.Value = 1;
            // 
            // tbarCounter
            // 
            this.tbarCounter.Location = new System.Drawing.Point(114, 20);
            this.tbarCounter.Maximum = 20;
            this.tbarCounter.Name = "tbarCounter";
            this.tbarCounter.Size = new System.Drawing.Size(206, 30);
            this.tbarCounter.SmallChange = 2;
            this.tbarCounter.TabIndex = 0;
            this.tbarCounter.ThumbColor = System.Drawing.SystemColors.Control;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(102, 160);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(183, 160);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "connect.png");
            this.imageList1.Images.SetKeyName(1, "disconnect.png");
            // 
            // tbarCountSend
            // 
            this.tbarCountSend.Location = new System.Drawing.Point(114, 92);
            this.tbarCountSend.Name = "tbarCountSend";
            this.tbarCountSend.Size = new System.Drawing.Size(206, 30);
            this.tbarCountSend.SmallChange = 2;
            this.tbarCountSend.TabIndex = 0;
            this.tbarCountSend.ThumbColor = System.Drawing.SystemColors.Control;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Số lần lấy phản hồi";
            // 
            // FrmCauHinhThoiGian
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 195);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(309, 163);
            this.Name = "FrmCauHinhThoiGian";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thiết lập thông tin";
            this.Load += new System.EventHandler(this.FrmCauHinhThoiGian_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.ImageList imageList1;
        private Janus.Windows.EditControls.UITrackBar tbarCounter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UITrackBar tbarConnected;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UITrackBar tbarCountSend;
    }
}