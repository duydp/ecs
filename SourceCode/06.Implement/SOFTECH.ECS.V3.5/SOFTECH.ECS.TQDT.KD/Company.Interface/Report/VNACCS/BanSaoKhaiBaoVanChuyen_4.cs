using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.Report.VNACCS
{
    public partial class BanSaoKhaiBaoVanChuyen_4 : DevExpress.XtraReports.UI.XtraReport
    {
        public BanSaoKhaiBaoVanChuyen_4()
        {
            InitializeComponent();
        }
        public void BindingReport(VAS501 vas504)
        {
            int SoCont = vas504.HangHoa.Count;
            if (SoCont <= 20)
                TongSoTrang.Text = System.Convert.ToDecimal(5).ToString();
            else
            {
                TongSoTrang.Text = System.Convert.ToDecimal(5 + Math.Round((decimal)SoCont / 20, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblTenThongTinXuat.Text = vas504.AB.GetValue().ToString();
            //lblMaCoBaoYeuCauXN.Text = vas504.KA.GetValue().ToString().ToUpper();
            //lblMaCoBaoYeuCauXN.Text = vas504.AC.GetValue().ToString();
            lblCoQuanHQ.Text = vas504.AD.GetValue().ToString().ToUpper();
            lblSoToKhaiVC.Text = vas504.AE.GetValue().ToString().ToUpper();
            lblCoBaoXNK.Text = vas504.ED.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vas504.AF.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayLapTK.Text = Convert.ToDateTime(vas504.AF.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayLapTK.Text = "";
            }
            for (int i = 0; i < vas504.DG.listAttribute[0].ListValue.Count; i++)
            {

                //lblSoTTDongHang.Text = vas504.DG.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                lblSoHangHoa.Text = vas504.DG.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                if (Convert.ToDateTime(vas504.DG.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblNgayPhatHanhVanDon.Text = Convert.ToDateTime(vas504.DG.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayPhatHanhVanDon.Text = "";
                }
                lblMoTaHangHoa.Text = vas504.DG.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                //                 if (System.Convert.ToDecimal(vas504.DG.listAttribute[4].GetValueCollection(i)) != 0)
                //                 {
                //                     lblMaHS.Text = vas504.DG.listAttribute[4].GetValueCollection(i).ToString();
                //                 }
                //                 else
                //                 {
                lblMaHS.Text = vas504.DG.listAttribute[4].GetValueCollection(i).ToString().ToUpper();
                //}
                //lblMaHS.Text = vas504.DG.listAttribute[4].GetValueCollection(i).ToString().ToUpper();
                lblKyHieuSoHieu.Text = vas504.DG.listAttribute[5].GetValueCollection(i).ToString().ToUpper();
                if (Convert.ToDateTime(vas504.DG.listAttribute[6].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblNgayNhapKhoHQ.Text = Convert.ToDateTime(vas504.DG.listAttribute[6].GetValueCollection(i)).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayNhapKhoHQ.Text = "";
                }
                lblPhanLoaiSP.Text = vas504.DG.listAttribute[7].GetValueCollection(i).ToString().ToUpper();
                lblMaNuocSX.Text = vas504.DG.listAttribute[8].GetValueCollection(i).ToString().ToUpper();
                lblTenNuocSX.Text = vas504.DG.listAttribute[9].GetValueCollection(i).ToString().ToUpper();
                lblMaDiaDiemXuatPhat.Text = vas504.DG.listAttribute[10].GetValueCollection(i).ToString().ToUpper();
                lblTenDiaDiemXuatPhat.Text = vas504.DG.listAttribute[11].GetValueCollection(i).ToString().ToUpper();
                lblMaDiaDiemDich.Text = vas504.DG.listAttribute[12].GetValueCollection(i).ToString().ToUpper();
                lblTenDiaDiemDich.Text = vas504.DG.listAttribute[13].GetValueCollection(i).ToString().ToUpper();
                lblLoaiManifest.Text = vas504.DG.listAttribute[14].GetValueCollection(i).ToString().ToUpper();
                lblMaPhuongTienVC2.Text = vas504.DG.listAttribute[15].GetValueCollection(i).ToString().ToUpper();
                lblTenPhuongTienVC2.Text = vas504.DG.listAttribute[16].GetValueCollection(i).ToString().ToUpper();
                if (Convert.ToDateTime(vas504.DG.listAttribute[17].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblNgayDuKienDenDi.Text = Convert.ToDateTime(vas504.DG.listAttribute[17].GetValueCollection(i)).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayDuKienDenDi.Text = "";
                }
                lblMaNguoiNK.Text = vas504.DG.listAttribute[18].GetValueCollection(i).ToString().ToUpper();
                lblTenNguoiNK.Text = vas504.DG.listAttribute[19].GetValueCollection(i).ToString().ToUpper();
                lblDiaChiNguoiNK.Text = vas504.DG.listAttribute[20].GetValueCollection(i).ToString().ToUpper();
                lblMaNguoiXK.Text = vas504.DG.listAttribute[21].GetValueCollection(i).ToString().ToUpper();
                lblTenNguoiXK.Text = vas504.DG.listAttribute[22].GetValueCollection(i).ToString().ToUpper();
                lblDiaChiNguoiXK.Text = vas504.DG.listAttribute[23].GetValueCollection(i).ToString().ToUpper();
                lblMaNguoiUyThac.Text = vas504.DG.listAttribute[24].GetValueCollection(i).ToString().ToUpper();
                lblTenNguoiUyThac.Text = vas504.DG.listAttribute[25].GetValueCollection(i).ToString().ToUpper();
                lblDiaChiNguoiUyThac.Text = vas504.DG.listAttribute[26].GetValueCollection(i).ToString().ToUpper();
                lblMaVBPL1.Text = vas504.DG.listAttribute[27].GetValueCollection(i).ToString().ToUpper();
                lblMaVBPL2.Text = vas504.DG.listAttribute[28].GetValueCollection(i).ToString().ToUpper();
                lblMaVBPL3.Text = vas504.DG.listAttribute[29].GetValueCollection(i).ToString().ToUpper();
                lblMaVBPL4.Text = vas504.DG.listAttribute[30].GetValueCollection(i).ToString().ToUpper();
                lblMaVBPL5.Text = vas504.DG.listAttribute[31].GetValueCollection(i).ToString().ToUpper();
                lblMaDVTTriGia.Text = vas504.DG.listAttribute[32].GetValueCollection(i).ToString().ToUpper();
                if (System.Convert.ToDecimal(vas504.DG.listAttribute[33].GetValueCollection(i)) != 0)
                {
                    lblTriGia.Text = vas504.DG.listAttribute[33].GetValueCollection(i).ToString();
                }
                else
                {
                    lblTriGia.Text = "";
                }
                //lblTriGia.Text = vas504.DG.listAttribute[33].GetValueCollection(i).ToString().ToUpper();
                if (System.Convert.ToDecimal(vas504.DG.listAttribute[34].GetValueCollection(i)) != 0)
                {
                    lblSoLuong.Text = vas504.DG.listAttribute[34].GetValueCollection(i).ToString();
                }
                else
                {
                    lblSoLuong.Text = "";
                }
                //lblSoLuong.Text = vas504.DG.listAttribute[34].GetValueCollection(i).ToString().ToUpper();
                lblMaDVTSoLuong.Text = vas504.DG.listAttribute[35].GetValueCollection(i).ToString().ToUpper();
                if (System.Convert.ToDecimal(vas504.DG.listAttribute[36].GetValueCollection(i)) != 0)
                {
                    lblTongTrongLuong.Text = vas504.DG.listAttribute[36].GetValueCollection(i).ToString();
                }
                else
                {
                    lblTongTrongLuong.Text = "";
                }
                //lblTongTrongLuong.Text = vas504.DG.listAttribute[36].GetValueCollection(i).ToString().ToUpper();
                lblMaDVTTrongLuong.Text = vas504.DG.listAttribute[37].GetValueCollection(i).ToString().ToUpper();
                if (System.Convert.ToDecimal(vas504.DG.listAttribute[38].GetValueCollection(i)) != 0)
                {
                    lblTheTich.Text = vas504.DG.listAttribute[38].GetValueCollection(i).ToString();
                }
                else
                {
                    lblTheTich.Text = "";
                }
                //lblTheTich.Text = vas504.DG.listAttribute[38].GetValueCollection(i).ToString().ToUpper();
                lblMaDVTTheTich.Text = vas504.DG.listAttribute[39].GetValueCollection(i).ToString().ToUpper();
                lblMaDanhDauDDKhoiHanh1.Text = vas504.DG.listAttribute[40].GetValueCollection(i).ToString().ToUpper();
                lblMaDanhDauDDKhoiHanh2.Text = vas504.DG.listAttribute[41].GetValueCollection(i).ToString().ToUpper();
                lblMaDanhDauDDKhoiHanh3.Text = vas504.DG.listAttribute[42].GetValueCollection(i).ToString().ToUpper();
                lblMaDanhDauDDKhoiHanh4.Text = vas504.DG.listAttribute[43].GetValueCollection(i).ToString().ToUpper();
                lblMaDanhDauDDKhoiHanh5.Text = vas504.DG.listAttribute[44].GetValueCollection(i).ToString().ToUpper();
                lblSoGiayPhep.Text = vas504.DG.listAttribute[45].GetValueCollection(i).ToString().ToUpper().ToUpper();
                if (Convert.ToDateTime(vas504.DG.listAttribute[46].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblNgayCapPhep.Text = Convert.ToDateTime(vas504.DG.listAttribute[46].GetValueCollection(i)).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayCapPhep.Text = "";
                }
                if (Convert.ToDateTime(vas504.DG.listAttribute[47].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblNgayHetHanCapPhep.Text = Convert.ToDateTime(vas504.DG.listAttribute[47].GetValueCollection(i)).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayHetHanCapPhep.Text = "";
                }
                lblGhiChu2.Text = vas504.DG.listAttribute[48].GetValueCollection(i).ToString().ToUpper();

            }

            if (Convert.ToDateTime(vas504.CW.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDuKienBatDauVC.Text = Convert.ToDateTime(vas504.CW.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDuKienBatDauVC.Text = "";
            }
            lblGioDuKienBatDauVC.Text = vas504.CX.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vas504.CY.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDuKienKetThucVC.Text = Convert.ToDateTime(vas504.CY.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDuKienKetThucVC.Text = "";
            }
            lblGioDuKienKetThucVC.Text = vas504.CZ.GetValue().ToString().ToUpper();
            //             lblMaBuuChinh.Text = vas504.KL.GetValue().ToString().ToUpper();
            //             lblDiaChiBuuChinh.Text = vas504.KM.GetValue().ToString().ToUpper();
            //             lblTenBuuChinh.Text = vas504.KN.GetValue().ToString().ToUpper();
            for (int i = 0; i < vas504.Z01.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblSoToKhaiXK1.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblSoToKhaiXK2.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblSoToKhaiXK3.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblSoToKhaiXK4.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblSoToKhaiXK5.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 5:
                        lblSoToKhaiXK6.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 6:
                        lblSoToKhaiXK7.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 7:
                        lblSoToKhaiXK8.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 8:
                        lblSoToKhaiXK9.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 9:
                        lblSoToKhaiXK10.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 10:
                        lblSoToKhaiXK11.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 11:
                        lblSoToKhaiXK12.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 12:
                        lblSoToKhaiXK13.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 13:
                        lblSoToKhaiXK14.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 14:
                        lblSoToKhaiXK15.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 15:
                        lblSoToKhaiXK16.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 16:
                        lblSoToKhaiXK17.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 17:
                        lblSoToKhaiXK18.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 18:
                        lblSoToKhaiXK19.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 19:
                        lblSoToKhaiXK20.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 20:
                        lblSoToKhaiXK21.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 21:
                        lblSoToKhaiXK22.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 22:
                        lblSoToKhaiXK23.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 23:
                        lblSoToKhaiXK24.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 24:
                        lblSoToKhaiXK25.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 25:
                        lblSoToKhaiXK26.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 26:
                        lblSoToKhaiXK27.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 27:
                        lblSoToKhaiXK28.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 28:
                        lblSoToKhaiXK29.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 29:
                        lblSoToKhaiXK30.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 30:
                        lblSoToKhaiXK31.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 31:
                        lblSoToKhaiXK32.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 32:
                        lblSoToKhaiXK33.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 33:
                        lblSoToKhaiXK34.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 34:
                        lblSoToKhaiXK35.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 35:
                        lblSoToKhaiXK36.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 36:
                        lblSoToKhaiXK37.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 37:
                        lblSoToKhaiXK38.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 38:
                        lblSoToKhaiXK39.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 39:
                        lblSoToKhaiXK40.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 40:
                        lblSoToKhaiXK41.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 41:
                        lblSoToKhaiXK42.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 42:
                        lblSoToKhaiXK43.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 43:
                        lblSoToKhaiXK44.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 44:
                        lblSoToKhaiXK45.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 45:
                        lblSoToKhaiXK46.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 46:
                        lblSoToKhaiXK47.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 47:
                        lblSoToKhaiXK48.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 48:
                        lblSoToKhaiXK49.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 49:
                        lblSoToKhaiXK50.Text = vas504.Z01.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }

        }
        public void BindingReport(VAS5030 vas504)
        {
            int SoCont = vas504.HangHoa.Count;
            if (SoCont <= 20)
                TongSoTrang.Text = System.Convert.ToDecimal(5).ToString();
            else
            {
                TongSoTrang.Text = System.Convert.ToDecimal(5 + Math.Round((decimal)SoCont / 20, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblTenThongTinXuat.Text = vas504.AB.GetValue().ToString();
            //lblMaCoBaoYeuCauXN.Text = vas504.KA.GetValue().ToString().ToUpper();
            lblMaCoBaoYeuCauXN.Text = vas504.AC.GetValue().ToString();
            lblCoQuanHQ.Text = vas504.AD.GetValue().ToString().ToUpper();
            lblSoToKhaiVC.Text = vas504.AE.GetValue().ToString().ToUpper();
            lblCoBaoXNK.Text = vas504.ED.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vas504.AF.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayLapTK.Text = Convert.ToDateTime(vas504.AF.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayLapTK.Text = "";
            }
            for (int i = 0; i < vas504.DG.listAttribute[0].ListValue.Count; i++)
            {

                //lblSoTTDongHang.Text = vas504.DG.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                lblSoHangHoa.Text = vas504.DG.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                if (Convert.ToDateTime(vas504.DG.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblNgayPhatHanhVanDon.Text = Convert.ToDateTime(vas504.DG.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayPhatHanhVanDon.Text = "";
                }
                lblMoTaHangHoa.Text = vas504.DG.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
//                 if (System.Convert.ToDecimal(vas504.DG.listAttribute[4].GetValueCollection(i)) != 0)
//                 {
//                     lblMaHS.Text = vas504.DG.listAttribute[4].GetValueCollection(i).ToString();
//                 }
//                 else
//                 {
                lblMaHS.Text = vas504.DG.listAttribute[4].GetValueCollection(i).ToString().ToUpper();
                //}
                //lblMaHS.Text = vas504.DG.listAttribute[4].GetValueCollection(i).ToString().ToUpper();
                lblKyHieuSoHieu.Text = vas504.DG.listAttribute[5].GetValueCollection(i).ToString().ToUpper();
                if (Convert.ToDateTime(vas504.DG.listAttribute[6].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblNgayNhapKhoHQ.Text = Convert.ToDateTime(vas504.DG.listAttribute[6].GetValueCollection(i)).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayNhapKhoHQ.Text = "";
                }
                lblPhanLoaiSP.Text = vas504.DG.listAttribute[7].GetValueCollection(i).ToString().ToUpper();
                lblMaNuocSX.Text = vas504.DG.listAttribute[8].GetValueCollection(i).ToString().ToUpper();
                lblTenNuocSX.Text = vas504.DG.listAttribute[9].GetValueCollection(i).ToString().ToUpper();
                lblMaDiaDiemXuatPhat.Text = vas504.DG.listAttribute[10].GetValueCollection(i).ToString().ToUpper();
                lblTenDiaDiemXuatPhat.Text = vas504.DG.listAttribute[11].GetValueCollection(i).ToString().ToUpper();
                lblMaDiaDiemDich.Text = vas504.DG.listAttribute[12].GetValueCollection(i).ToString().ToUpper();
                lblTenDiaDiemDich.Text = vas504.DG.listAttribute[13].GetValueCollection(i).ToString().ToUpper();
                lblLoaiManifest.Text = vas504.DG.listAttribute[14].GetValueCollection(i).ToString().ToUpper();
                lblMaPhuongTienVC2.Text = vas504.DG.listAttribute[15].GetValueCollection(i).ToString().ToUpper();
                lblTenPhuongTienVC2.Text = vas504.DG.listAttribute[16].GetValueCollection(i).ToString().ToUpper();
                if (Convert.ToDateTime(vas504.DG.listAttribute[17].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblNgayDuKienDenDi.Text = Convert.ToDateTime(vas504.DG.listAttribute[17].GetValueCollection(i)).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayDuKienDenDi.Text = "";
                }
                lblMaNguoiNK.Text = vas504.DG.listAttribute[18].GetValueCollection(i).ToString().ToUpper();
                lblTenNguoiNK.Text = vas504.DG.listAttribute[19].GetValueCollection(i).ToString().ToUpper();
                lblDiaChiNguoiNK.Text = vas504.DG.listAttribute[20].GetValueCollection(i).ToString().ToUpper();
                lblMaNguoiXK.Text = vas504.DG.listAttribute[21].GetValueCollection(i).ToString().ToUpper();
                lblTenNguoiXK.Text = vas504.DG.listAttribute[22].GetValueCollection(i).ToString().ToUpper();
                lblDiaChiNguoiXK.Text = vas504.DG.listAttribute[23].GetValueCollection(i).ToString().ToUpper();
                lblMaNguoiUyThac.Text = vas504.DG.listAttribute[24].GetValueCollection(i).ToString().ToUpper();
                lblTenNguoiUyThac.Text = vas504.DG.listAttribute[25].GetValueCollection(i).ToString().ToUpper();
                lblDiaChiNguoiUyThac.Text = vas504.DG.listAttribute[26].GetValueCollection(i).ToString().ToUpper();
                lblMaVBPL1.Text = vas504.DG.listAttribute[27].GetValueCollection(i).ToString().ToUpper();
                lblMaVBPL2.Text = vas504.DG.listAttribute[28].GetValueCollection(i).ToString().ToUpper();
                lblMaVBPL3.Text = vas504.DG.listAttribute[29].GetValueCollection(i).ToString().ToUpper();
                lblMaVBPL4.Text = vas504.DG.listAttribute[30].GetValueCollection(i).ToString().ToUpper();
                lblMaVBPL5.Text = vas504.DG.listAttribute[31].GetValueCollection(i).ToString().ToUpper();
                lblMaDVTTriGia.Text = vas504.DG.listAttribute[32].GetValueCollection(i).ToString().ToUpper();
                if (System.Convert.ToDecimal(vas504.DG.listAttribute[33].GetValueCollection(i))!=0)
                {
                    lblTriGia.Text = vas504.DG.listAttribute[33].GetValueCollection(i).ToString();
                }
                else
                {
                    lblTriGia.Text = "";
                }
                //lblTriGia.Text = vas504.DG.listAttribute[33].GetValueCollection(i).ToString().ToUpper();
                if (System.Convert.ToDecimal(vas504.DG.listAttribute[34].GetValueCollection(i)) != 0)
                {
                    lblSoLuong.Text = vas504.DG.listAttribute[34].GetValueCollection(i).ToString();
                }
                else
                {
                    lblSoLuong.Text = "";
                }
                //lblSoLuong.Text = vas504.DG.listAttribute[34].GetValueCollection(i).ToString().ToUpper();
                lblMaDVTSoLuong.Text = vas504.DG.listAttribute[35].GetValueCollection(i).ToString().ToUpper();
                if (System.Convert.ToDecimal(vas504.DG.listAttribute[36].GetValueCollection(i)) != 0)
                {
                    lblTongTrongLuong.Text = vas504.DG.listAttribute[36].GetValueCollection(i).ToString();
                }
                else
                {
                    lblTongTrongLuong.Text = "";
                }
                //lblTongTrongLuong.Text = vas504.DG.listAttribute[36].GetValueCollection(i).ToString().ToUpper();
                lblMaDVTTrongLuong.Text = vas504.DG.listAttribute[37].GetValueCollection(i).ToString().ToUpper();
                if (System.Convert.ToDecimal(vas504.DG.listAttribute[38].GetValueCollection(i)) != 0)
                {
                    lblTheTich.Text = vas504.DG.listAttribute[38].GetValueCollection(i).ToString();
                }
                else
                {
                    lblTheTich.Text = "";
                }
                //lblTheTich.Text = vas504.DG.listAttribute[38].GetValueCollection(i).ToString().ToUpper();
                lblMaDVTTheTich.Text = vas504.DG.listAttribute[39].GetValueCollection(i).ToString().ToUpper();
                lblMaDanhDauDDKhoiHanh1.Text = vas504.DG.listAttribute[40].GetValueCollection(i).ToString().ToUpper();
                lblMaDanhDauDDKhoiHanh2.Text = vas504.DG.listAttribute[41].GetValueCollection(i).ToString().ToUpper();
                lblMaDanhDauDDKhoiHanh3.Text = vas504.DG.listAttribute[42].GetValueCollection(i).ToString().ToUpper();
                lblMaDanhDauDDKhoiHanh4.Text = vas504.DG.listAttribute[43].GetValueCollection(i).ToString().ToUpper();
                lblMaDanhDauDDKhoiHanh5.Text = vas504.DG.listAttribute[44].GetValueCollection(i).ToString().ToUpper();
                lblSoGiayPhep.Text = vas504.DG.listAttribute[45].GetValueCollection(i).ToString().ToUpper().ToUpper();
                if (Convert.ToDateTime(vas504.DG.listAttribute[46].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblNgayCapPhep.Text = Convert.ToDateTime(vas504.DG.listAttribute[46].GetValueCollection(i)).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayCapPhep.Text = "";
                }
                if (Convert.ToDateTime(vas504.DG.listAttribute[47].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                {
                    lblNgayHetHanCapPhep.Text = Convert.ToDateTime(vas504.DG.listAttribute[47].GetValueCollection(i)).ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgayHetHanCapPhep.Text = "";
                }
                lblGhiChu2.Text = vas504.DG.listAttribute[48].GetValueCollection(i).ToString().ToUpper();

            }
         
            if (Convert.ToDateTime(vas504.CW.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDuKienBatDauVC.Text = Convert.ToDateTime(vas504.CW.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDuKienBatDauVC.Text = "";
            }
            lblGioDuKienBatDauVC.Text = vas504.CX.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vas504.CY.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDuKienKetThucVC.Text = Convert.ToDateTime(vas504.CY.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDuKienKetThucVC.Text = "";
            }
            lblGioDuKienKetThucVC.Text = vas504.CZ.GetValue().ToString().ToUpper();
//             lblMaBuuChinh.Text = vas504.KL.GetValue().ToString().ToUpper();
//             lblDiaChiBuuChinh.Text = vas504.KM.GetValue().ToString().ToUpper();
//             lblTenBuuChinh.Text = vas504.KN.GetValue().ToString().ToUpper();
            for (int i = 0; i < vas504.ZO1.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblSoToKhaiXK1.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblSoToKhaiXK2.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblSoToKhaiXK3.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblSoToKhaiXK4.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblSoToKhaiXK5.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 5:
                        lblSoToKhaiXK6.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 6:
                        lblSoToKhaiXK7.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 7:
                        lblSoToKhaiXK8.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 8:
                        lblSoToKhaiXK9.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 9:
                        lblSoToKhaiXK10.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 10:
                        lblSoToKhaiXK11.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 11:
                        lblSoToKhaiXK12.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 12:
                        lblSoToKhaiXK13.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 13:
                        lblSoToKhaiXK14.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 14:
                        lblSoToKhaiXK15.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 15:
                        lblSoToKhaiXK16.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 16:
                        lblSoToKhaiXK17.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 17:
                        lblSoToKhaiXK18.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 18:
                        lblSoToKhaiXK19.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 19:
                        lblSoToKhaiXK20.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 20:
                        lblSoToKhaiXK21.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 21:
                        lblSoToKhaiXK22.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 22:
                        lblSoToKhaiXK23.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 23:
                        lblSoToKhaiXK24.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 24:
                        lblSoToKhaiXK25.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 25:
                        lblSoToKhaiXK26.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 26:
                        lblSoToKhaiXK27.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 27:
                        lblSoToKhaiXK28.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 28:
                        lblSoToKhaiXK29.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 29:
                        lblSoToKhaiXK30.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 30:
                        lblSoToKhaiXK31.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 31:
                        lblSoToKhaiXK32.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 32:
                        lblSoToKhaiXK33.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 33:
                        lblSoToKhaiXK34.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 34:
                        lblSoToKhaiXK35.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 35:
                        lblSoToKhaiXK36.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 36:
                        lblSoToKhaiXK37.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 37:
                        lblSoToKhaiXK38.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 38:
                        lblSoToKhaiXK39.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 39:
                        lblSoToKhaiXK40.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 40:
                        lblSoToKhaiXK41.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 41:
                        lblSoToKhaiXK42.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 42:
                        lblSoToKhaiXK43.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 43:
                        lblSoToKhaiXK44.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 44:
                        lblSoToKhaiXK45.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 45:
                        lblSoToKhaiXK46.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 46:
                        lblSoToKhaiXK47.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 47:
                        lblSoToKhaiXK48.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 48:
                        lblSoToKhaiXK49.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 49:
                        lblSoToKhaiXK50.Text = vas504.ZO1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            

        }
        

      
      

    }
}
