﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
namespace Company.Interface.VNACCS.Vouchers
{
    public partial class TransportEquipmentManagerForm : BaseForm
    {
        public KDT_VNACCS_TransportEquipment transportEquipment;
        public TransportEquipmentManagerForm()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //string where = "";
                //if (txtSoToKhai.Text.Length > 0)
                //{
                //    where += " TKMD_ID = (SELECT TOP 1 ID FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE SoToKhai = '" + txtSoToKhai.Text.Trim() + "')";
                //}
                dgList.Refetch();
                dgList.DataSource = KDT_VNACCS_TransportEquipment.SelectCollectionDynamic(GetSearchWhere(), "");

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }
        private string GetSearchWhere()
        {
            try
            {
                string where = " 1=1";
                if (!String.IsNullOrEmpty(txtSoTiepNhan.Text))
                    where += " AND SoTN LIKE '%" + txtSoTiepNhan.Text +"%'";
                if (!String.IsNullOrEmpty(txtNamTiepNhan.Text))
                    where += " AND YEAR(NgayTN) = " + txtNamTiepNhan.Text;
                if (!String.IsNullOrEmpty(cbStatus.SelectedValue.ToString()))
                    where += " AND TrangThaiXuLy =" + cbStatus.SelectedValue;
                if (!String.IsNullOrEmpty(ctrCoQuanHaiQuan.Code))
                    where += " AND MaHQ = '" + ctrCoQuanHaiQuan.Code + "'";
                if (txtSoToKhai.Text.Length > 0)
                    where += " AND TKMD_ID = (SELECT TOP 1 ID FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE SoToKhai LIKE '%" + txtSoToKhai.Text.Trim() + "%')";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
                return null;
            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (dgList.GetRows().Length < 0)
                {
                    return;
                }
                if (items.Count < 0)
                {
                    return;
                }
                if (ShowMessage("Bạn có muốn xóa tờ khai này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        transportEquipment.ID = Convert.ToInt64(dgList.GetRow().Cells["ID"].Value.ToString());
                        transportEquipment.Delete();
                    }
                }
                ShowMessage("Xóa thành công .", false);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                KDT_VNACCS_TransportEquipment transportEquipment = new KDT_VNACCS_TransportEquipment();
                transportEquipment = (KDT_VNACCS_TransportEquipment)e.Row.DataRow;
                VNACC_TransportEquipmentForm f = new VNACC_TransportEquipmentForm();
                f.transportEquipment = transportEquipment;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void BindData()
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = KDT_VNACCS_TransportEquipment.SelectCollectionAll();
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void TransportEquipmentManagerForm_Load(object sender, EventArgs e)
        {
            try
            {
                ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
                ctrCoQuanHaiQuan.BackColor = Color.Transparent;
                cbStatus.SelectedValue = "0";
                btnSearch_Click(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        public void ExportExcel(GridEX gridList)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách _" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                string TrangThai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (TrangThai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcel(dgList);
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }
    }
}
