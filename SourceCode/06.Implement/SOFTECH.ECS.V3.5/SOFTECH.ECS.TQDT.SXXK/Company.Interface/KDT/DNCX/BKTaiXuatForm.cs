using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.Interface.SXXK.BangKeThanhLy;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.KDT.SXXK
{
    public partial class BKTaiXuatForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public KDT_CX_TaiXuatDangKy TaiXuatDK = new KDT_CX_TaiXuatDangKy();
        private KDT_CX_TaiXuat TaiXuat = new KDT_CX_TaiXuat();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
      
        public BKTaiXuatForm()
        {
            InitializeComponent();
        }
        bool isnew = true;
        private void cmdAdd_Click(object sender, EventArgs e)
        {
            if (txtSoToKhai.Text == string.Empty || txtMaNPL.Text == string.Empty)
                ShowMessage("Chưa chọn số tờ khai", false);
            else
            {
                cvError.Validate();
                if (TaiXuat == null)
                    TaiXuat = new KDT_CX_TaiXuat();
                getTX();
                if (isnew)
                    TaiXuatDK.TaiXuatList.Add(TaiXuat);
                grdList.DataSource = TaiXuatDK.TaiXuatList;
                grdList.Refetch();
                TaiXuat = new KDT_CX_TaiXuat();
                txtSoToKhai.Text = "";
                isnew = true;
            }
        }
       
        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                TaiXuatDK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                TaiXuatDK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                TaiXuatDK.LanThanhLy = HSTL.LanThanhLy;
                TaiXuatDK.InsertUpdatFull();
                LoadData();
                ShowMessage("Lưu bảng kê thành công",false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi lưu bảng kê. " +ex, false);
            }
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case"cmdKhaiBao":
                    SendV5();
                    break;
                case "cmdNhanDulieu":
                    FeedBackV5();
                    break;
                case "cmdKetQua":
                    KetQuaXuLyTX();
                    break;
                    
            }
        }

        private void txtSoToKhai_ButtonClick(object sender, EventArgs e)
        {
            int id = this.HSTL.getBKToKhaiXuat();
            DataSet ds = new BKToKhaiXuat().getBKToKhaiXuatNPL(this.HSTL.BKCollection[id].ID);
            DSToKhaiXuatNPLForm f = new DSToKhaiXuatNPLForm();
            f.BangKeHSTL_ID = this.HSTL.BKCollection[id].ID;
            f.ds = ds;
            f.ShowDialog(this);
           
            if (TaiXuat == null)
                TaiXuat = new KDT_CX_TaiXuat();
            if (f.SoToKhai > 0)
            {
                //TaiXuat.TKMD_ID = f.TKMD_ID;
                TaiXuat.SoToKhai = f.SoToKhaiVNACCS;
                TaiXuat.MaLoaiHinh = f.MaLoaiHinh;
                TaiXuat.NgayDangKy = f.NgayDangKy;
                TaiXuat.MaHaiQuan = f.MaHaiQuan;
                TaiXuat.MaHang = f.MaNPL;
                TaiXuat.TenHang = f.TenNPL;
                TaiXuat.DVT = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(f.DVT_ID.Trim());
                TaiXuat.LoaiHang = 1;
                TaiXuat.LuongTaiXuat = f.SoLuong;
                setTX();
            }

            
        }

        private void BKTaiXuatForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            if (TaiXuatDK.ID != 0)
            {
                txtSoTiepNhan.Text = TaiXuatDK.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = TaiXuatDK.NgayTiepNhan;
                switch (TaiXuatDK.TrangThaiXuLy)
                {
                    case 1:
                        lblTrangThaiXuLy.Text = "Đã duyệt";
                        break;
                    case 2:
                        lblTrangThaiXuLy.Text = "Không phê duyệt";
                        break;
                    case 0:
                        lblTrangThaiXuLy.Text = "Chờ duyệt";
                        break;
                    case -1:
                        lblTrangThaiXuLy.Text = "Chưa khai báo";
                        break;
                }
                grdList.DataSource = TaiXuatDK.TaiXuatList;
                grdList.Refetch();
            }
            else
            {
                TaiXuatDK.TrangThaiXuLy = -1;
            }
        }
        private void getTX()
        {
            TaiXuat.SoToKhai = Convert.ToDecimal(txtSoToKhai.Text);
            TaiXuat.NgayDangKy = clcNgayDangKy.Value;
            TaiXuat.MaLoaiHinh = txtLoaiHinhXuat.Text;
            TaiXuat.MaHang = txtMaNPL.Text;
            TaiXuat.TenHang = txtTenNPL.Text;
            TaiXuat.DVT = ctrDVT.Code;
            TaiXuat.LuongTaiXuat = Convert.ToDecimal(txtSoLuongXuat.Value);
            TaiXuat.Temp = txtGhichu.Text;
        }
        private void setTX()
        {
            txtSoToKhai.Text = TaiXuat.SoToKhai.ToString();
            txtLoaiHinhXuat.Text = TaiXuat.MaLoaiHinh;
            clcNgayDangKy.Value = TaiXuat.NgayDangKy;
            txtMaNPL.Text = TaiXuat.MaHang;
            txtTenNPL.Text = TaiXuat.TenHang;
            txtSoLuongXuat.Value = TaiXuat.LuongTaiXuat;
            ctrDVT.Code = TaiXuat.DVT;
            txtGhichu.Text = TaiXuat.Temp;
        }

        private void grdList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if(e.Row.RowType  == RowType.Record)
            {
                TaiXuat = new KDT_CX_TaiXuat();
                TaiXuat = (KDT_CX_TaiXuat)grdList.CurrentRow.DataRow;
                setTX();
                isnew = false;
            }
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {

        }


        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.TaiXuatSendHandler(TaiXuatDK, ref msgInfor, e);
        }

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

        private void SendV5()
        {
            if (ShowMessage("Bạn có muốn khai báo bảng kê tái xuất này đến Hải quan?", true) == "Yes")
            {
                bool isKhaiSua = false;
                if (TaiXuatDK.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    TaiXuatDK = KDT_CX_TaiXuatDangKy.LoadFull(TaiXuatDK.ID);
                    if (TaiXuatDK.TrangThaiXuLy == 5)
                        isKhaiSua = true;
                    else
                        isKhaiSua = false;
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.TaiXuat;
                sendXML.master_id = TaiXuatDK.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [lấy phản hồi] để lấy thông tin", false);
                    cmdAdd.Enabled = cmdSave.Enabled = false;
                    cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdNhanDulieu.Enabled = cmdNhanDulieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    return;
                }
                try
                {
                    if (this.TaiXuatDK.TaiXuatList.Count > 0)
                    {
                        string returnMessage = string.Empty;
                        TaiXuatDK.GUIDSTR = Guid.NewGuid().ToString();
                        SXXK_HosoThanhKhoan TaiXuat_Object = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferBKTaiXuat(TaiXuatDK,HSTL.SoQuyetDinh, GlobalSettings.TEN_DON_VI, isKhaiSua);
                        //PhuKienToKhai phukien = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferPhuKienToKhai(TaiXuatDK, GlobalSettings.TEN_DON_VI);

                        ObjectSend msgSend = new ObjectSend(
                             new NameBase()
                             {
                                 Name = GlobalSettings.TEN_DON_VI,
                                 Identity = TaiXuatDK.MaDoanhNghiep
                             },
                              new NameBase()
                              {
                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TaiXuatDK.MaHaiQuan)),
                                  Identity = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TaiXuatDK.MaHaiQuan).Trim() 
                                  //Identity = TaiXuatDK.MaHaiQuan
                              },
                              new SubjectBase()
                              {

                                  Type = DeclarationIssuer.BKTaiXuatKCX,
                                  Function = isKhaiSua ? DeclarationFunction.SUA : DeclarationFunction.KHAI_BAO,
                                  Reference = TaiXuatDK.GUIDSTR,
                              },
                              TaiXuat_Object
                            );
                        //TaiXuatDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(TaiXuatDK.ID, isKhaiSua ? MessageTitle.KhaiBaoSuaTaiXuat : MessageTitle.KhaiBaoTaiXuat);
                            cmdNhanDulieu.Enabled = cmdNhanDulieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdAdd.Enabled = cmdSave.Enabled = false;
                            cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //btnDelete.Enabled = false;
                            sendXML.LoaiHS = LoaiKhaiBao.TaiXuat;
                            sendXML.master_id = TaiXuatDK.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();
                            
                            TaiXuatDK.Update();
                            FeedBackV5();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }

        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            //MsgSend sendXML = new MsgSend();
            //sendXML.LoaiHS = LoaiKhaiBao.TaiXuat;
            //sendXML.master_id = TaiXuatDK.ID;
            //if (!sendXML.Load())
            //{
            //    ShowMessage("THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN. XIN KIỂM TRA LẠI", false);
            //    cmdNhanDulieu.Enabled = cmdNhanDulieu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdAdd.Enabled = cmdSave.Enabled = false;
            //    cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    return;
            //}
            while (isFeedBack)
            {
                string reference = TaiXuatDK.GUIDSTR;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.BKTaiXuatKCX,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.BKTaiXuatKCX,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = TaiXuatDK.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TaiXuatDK.MaHaiQuan.Trim())),
                                                  Identity =VNACCS_Mapper.GetCodeVNACCMaHaiQuan(TaiXuatDK.MaHaiQuan).Trim()
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }

            }
        }

        private void KetQuaXuLyTX()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.TaiXuatDK.ID;
            form.DeclarationIssuer = DeclarationIssuer.BKTaiXuatKCX;
            form.ShowDialog(this);
        }
       

    }
}