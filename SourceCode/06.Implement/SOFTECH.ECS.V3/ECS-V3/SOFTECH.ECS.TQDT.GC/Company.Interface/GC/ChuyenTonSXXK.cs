using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;
using Company.GC.BLL.GC;

namespace Company.Interface
{
    public partial class ChuyenTonSXXK : BaseForm
    {
        public HopDong HD = new HopDong();
        public ChuyenTonSXXK()
        {
            InitializeComponent();
        }

        

        private void btnHuybo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChuyenTonSXXK_Load(object sender, EventArgs e)
        {
            BindHopDong();
            BindToKhaiSXXKTon();
        }

        private void btnThuchien_Click(object sender, EventArgs e)
        {
            GridEXRow[] rows = dgListNPLTK.GetCheckedRows();
            this.Cursor = Cursors.WaitCursor;
            foreach (GridEXRow row in rows)
            {
                try
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    NPLNhapTonThucTe NPL = new NPLNhapTonThucTe();
                    TKMD.SoToKhai = NPL.SoToKhai = Convert.ToInt32(row.Cells["SoToKhai"].Value);
                    TKMD.MaLoaiHinh = NPL.MaLoaiHinh = Convert.ToString(row.Cells["MaLoaiHinh"].Value);
                    //TKMD.NgayDangKy = Convert.ToDateTime(row.Cells["NgayDangKy"].Value);
                    NPL.NamDangKy = Convert.ToInt16(row.Cells["NamDangKy"].Value);
                    TKMD.MaHaiQuan = NPL.MaHaiQuan = Convert.ToString(row.Cells["MaHaiQuan"].Value);
                    TKMD.LoadBySoToKhai(NPL.NamDangKy);
                    TKMD.ChuyenHopDong(Convert.ToInt64(cbHopDong.Value));
                    NPL.ChuyenHopDong(Convert.ToInt64(cbHopDong.Value));
                }
                catch
                {
                    ShowMessage("Lỗi khi chuyển tờ khai số " + row.Cells["SoToKhai"].Value, false);
                    this.Cursor = Cursors.Default;
                    return;
                }

            }
            ShowMessage("Đã chuyển thành công sang hợp đồng mới.", false);
            this.Close();
            this.Cursor = Cursors.Default;
        }
        private void BindHopDong()
        {
            string where = string.Format("MaDoanhNghiep='{0}' AND ID !={1}",  GlobalSettings.MA_DON_VI,this,HD.ID);
            List<HopDong>  collection = HopDong.SelectCollectionDynamic(where, "SoHopDong");
            cbHopDong.DataSource = collection;
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
            cbHopDong.SelectedIndex = collection.Count - 1;
        }
        private void BindToKhaiSXXKTon()
        {
            dgListNPLTK.DataSource = this.HD.GetToKhaiSXXKTon().Tables[0];
            dgListNPLTK.Refetch();
        }
    }
}