using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using System.Linq;

namespace Company.Interface.Report.SXXK
{
    public partial class BCTongHopNhapXuatNLVT : DevExpress.XtraReports.UI.XtraReport
    {
        public int LanThanhLy;
        private int STT = 0;
        private int STTBanLuu = 0;
        public int SoHoSo;
        public bool isHoanThue = false;
        private DataTable DtGhiChu = new DataTable();
        private DataTable dt = new DataTable();
        private decimal SoToKhaiNhap = 0;
        private decimal SoToKhaiNhapBanLuu = 0;
        private decimal SoToKhaiXuat = 0;
        private decimal SoToKhaiXuatBanLuu = 0;
        private decimal temp = 0;

        private string MaNPLTemp = "";
        private string MaNPLBanLuu = "";
        private int cnt = 1; //Gia tri phai thiet lap mac dinh ban dau = 1.
        private DataTable dtMaNPLTemp;

        private string MaSPTemp = "";
        private string MaSPBanLuu = "";

        private decimal _TongLuongNhap = 0;
        private decimal _TongLuongTonDau = 0;
        private decimal _TongLuongNPLSuDung = 0;
        private decimal _TongLuongTonCuoi = 0;
        private decimal _TongLuongNPLTaiXuat = 0;

        private int _ChuyenTiepTK = 0;

        public BCTongHopNhapXuatNLVT()
        {
            InitializeComponent();
        }

        public void BindReport(string where)
        {
        } 

    }
}
