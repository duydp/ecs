﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseNPLRegistedForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public HopDong HD;
        public T_KHOKETOAN_NGUYENPHULIEU NPLSelected;
        public T_KHOKETOAN_NGUYENPHULIEU_MAP NPLMapSelected;
        public string NPLMap;
        public WareHouseNPLRegistedForm()
        {
            InitializeComponent();
        }

        private void WareHouseNPLRegistedForm_Load(object sender, EventArgs e)
        {
            btnSearchNPL_Click(null,null);
        }

        private void btnSearchNPL_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(NPLMap))
                {
                    grListNPL.Refetch();
                    grListNPL.DataSource = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionDynamic("MANPL ='" + NPLMap + "' AND HOPDONG_ID = " + HD.ID, "");
                    grListNPL.Refresh();
                }
                else
                {
                    grListNPL.Refetch();
                    grListNPL.DataSource = T_KHOKETOAN_NGUYENPHULIEU.SelectCollectionDynamic("MANPL LIKE '%" + txtNPL.Text.ToString() + "%' AND HOPDONG_ID = " + HD.ID, "");
                    grListNPL.Refresh();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListNPL_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(NPLMap))
                {
                    NPLMapSelected = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
                    NPLMapSelected = (T_KHOKETOAN_NGUYENPHULIEU_MAP)e.Row.DataRow;
                }
                else
                {
                    NPLSelected = new T_KHOKETOAN_NGUYENPHULIEU();
                    NPLSelected = (T_KHOKETOAN_NGUYENPHULIEU)e.Row.DataRow;
                    NPLSelected.NPLMapCollection = T_KHOKETOAN_NGUYENPHULIEU_MAP.SelectCollectionBy_KHONPL_ID(NPLSelected.ID);
                }
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
