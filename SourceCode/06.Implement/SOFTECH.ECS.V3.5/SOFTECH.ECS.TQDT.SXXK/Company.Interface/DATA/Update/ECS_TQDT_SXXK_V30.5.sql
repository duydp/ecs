/****** Object:  View [dbo].[v_HangTon]    Script Date: 01/19/2015 11:28:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER VIEW [dbo].[v_HangTon]
AS
SELECT     CASE WHEN t .MaLoaiHinh LIKE '%V%' THEN
                          (SELECT     TOP 1 SoTKVNACCS
                            FROM          t_VNACCS_CapSoToKhai
                            WHERE      SoTK = t .SoToKhai) ELSE t .SoToKhai END AS SoToKhaiVNACCS, t.MaDoanhNghiep, t.MaHaiQuan, t.SoToKhai, t.MaLoaiHinh, t.NamDangKy, t.MaNPL, 
                      hmd.TenHang AS TenNPL, hmd.DVT_ID, dvt.Ten AS TenDVT, hmd.NuocXX_ID, hmd.TriGiaKB, t.Luong, t.Ton, t.ThueXNK, t.ThueXNKTon, t.ThueTTDB, t.ThueVAT, 
                      t.ThueCLGia, t.PhuThu,
                          (SELECT     NgayDangKy
                            FROM          dbo.t_SXXK_ToKhaiMauDich
                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaHaiQuan = t.MaHaiQuan) AND 
                                                   (MaDoanhNghiep = t.MaDoanhNghiep)) AS NgayDangKy,
                          (SELECT     COUNT(*) AS Expr1
                            FROM          dbo.t_KDT_SXXK_NPLNhapTon
                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaNPL = t.MaNPL) AND (TonCuoi < TonDau) AND 
                                                   (LanThanhLy IN
                                                       (SELECT     LanThanhLy
                                                         FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy)) AND (MaDoanhNghiep = t.MaDoanhNghiep)) AS SoLanThanhLy,
                          (SELECT     MAX(LanThanhLy) AS Expr1
                            FROM          dbo.t_KDT_SXXK_NPLNhapTon AS t_KDT_SXXK_NPLNhapTon_2
                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaNPL = t.MaNPL) AND (TonCuoi < TonDau) AND 
                                                   (LanThanhLy IN
                                                       (SELECT     LanThanhLy
                                                         FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy AS t_KDT_SXXK_HoSoThanhLyDangKy_3)) AND (MaDoanhNghiep = t.MaDoanhNghiep)) 
                      AS LanThanhLy,
                          (SELECT     TrangThaiThanhKhoan
                            FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy AS t_KDT_SXXK_HoSoThanhLyDangKy_2
                            WHERE      (LanThanhLy =
                                                       (SELECT     MAX(LanThanhLy) AS Expr1
                                                         FROM          dbo.t_KDT_SXXK_NPLNhapTon AS t_KDT_SXXK_NPLNhapTon_1
                                                         WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaNPL = t.MaNPL) AND 
                                                                                (TonCuoi < TonDau) AND (LanThanhLy IN
                                                                                    (SELECT     LanThanhLy
                                                                                      FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy AS t_KDT_SXXK_HoSoThanhLyDangKy_1)) AND 
                                                                                (MaDoanhNghiep = t.MaDoanhNghiep)))) AS TrangThaiThanhKhoan,
                          (SELECT     COUNT(*) AS Expr1
                            FROM          dbo.t_SXXK_ThanhLy_NPLNhapTon AS nt INNER JOIN
                                                   dbo.t_SXXK_HangMauDich AS h ON nt.MaNPL = h.MaPhu AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND 
                                                   nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan AND nt.Luong <> h.SoLuong
                            WHERE      (nt.SoToKhai = t.SoToKhai) AND (nt.MaLoaiHinh = t.MaLoaiHinh) AND (nt.NamDangKy = t.NamDangKy) AND (nt.MaNPL = t.MaNPL) AND 
                                                   (nt.MaHaiQuan = t.MaHaiQuan) AND (nt.MaDoanhNghiep = t.MaDoanhNghiep)) AS SaiSoLuong,
                          (SELECT     h.SoLuong
                            FROM          dbo.t_SXXK_ThanhLy_NPLNhapTon AS nt INNER JOIN
                                                   dbo.t_SXXK_HangMauDich AS h ON nt.MaNPL = h.MaPhu AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND 
                                                   nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan AND nt.Luong <> h.SoLuong
                            WHERE      (nt.SoToKhai = t.SoToKhai) AND (nt.MaLoaiHinh = t.MaLoaiHinh) AND (nt.NamDangKy = t.NamDangKy) AND (nt.MaNPL = t.MaNPL) AND 
                                                   (nt.MaHaiQuan = t.MaHaiQuan) AND (nt.MaDoanhNghiep = t.MaDoanhNghiep) 
                                                   and (nt.Luong = h.SoLuong)-- phi kiem tra to khai cung ma khac don gia
                                                   ) AS SoLuongDangKy, dbo.f_KiemTra_TienTrinh_ThanhLy(t.MaHaiQuan, 
                      t.MaDoanhNghiep, t.SoToKhai, t.MaLoaiHinh, t.NamDangKy, t.MaNPL, t.Ton) AS TienTrinhChayThanhLy,
                          (SELECT     ThanhLy
                            FROM          dbo.t_SXXK_ToKhaiMauDich AS tk
                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaHaiQuan = t.MaHaiQuan) AND 
                                                   (MaDoanhNghiep = t.MaDoanhNghiep)) AS ThanhLy, CASE WHEN
                          ((SELECT     COUNT(*)
                              FROM         [dbo].t_SXXK_ThanhLy_NPLNhapTon nt INNER JOIN
                                                    dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND 
                                                    nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan
                              WHERE     nt.SoToKhai = t .SoToKhai AND nt.MaLoaiHinh = t .MaLoaiHinh AND nt.NamDangKy = t .NamDangKy AND nt.MaNPL = t .MaNPL AND 
                                                    nt.MaHaiQuan = t .MaHaiQuan AND nt.MaDoanhNghiep = t .MaDoanhNghiep AND nt.Luong <> h.SoLuong) = 0 AND
                          (SELECT     COUNT(*)
                            FROM          [dbo].[t_KDT_SXXK_NPLNhapTon]
                            WHERE      SoToKhai = t .SoToKhai AND MaLoaiHinh = t .MaLoaiHinh AND NamDangKy = t .NamDangKy AND MaNPL = t .MaNPL AND TonCuoi < TonDau AND 
                                                   LanThanhLy IN
                                                       (SELECT     LanThanhLy
                                                         FROM          t_KDT_SXXK_HoSoThanhLyDangKy) AND MaDoanhNghiep = t .MaDoanhNghiep) = 0 AND (t .Luong <> t .Ton)) 
                      THEN 1 ELSE 0 END AS LechTon,
                          (SELECT     TenChuHang
                            FROM          dbo.t_SXXK_ToKhaiMauDich AS t_SXXK_ToKhaiMauDich_2
                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaHaiQuan = t.MaHaiQuan) AND 
                                                   (MaDoanhNghiep = t.MaDoanhNghiep)) AS TenChuHang,
                            Case when t.MaLoaiHinh like '%V%' then (select top 1 GhiChu from t_KDT_VNACC_ToKhaiMauDich where SoToKhai = (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t.SoToKhai)) else (select top 1 SoHopDong from t_SXXK_ToKhaiMauDich where SoToKhai = t.SoToKhai)  end AS SoHopDong
FROM         dbo.t_SXXK_ThanhLy_NPLNhapTon AS t LEFT OUTER JOIN
                      dbo.t_SXXK_HangMauDich AS hmd ON t.MaHaiQuan = hmd.MaHaiQuan AND t.MaNPL = hmd.MaPhu AND t.SoToKhai = hmd.SoToKhai AND 
                      t.MaLoaiHinh = hmd.MaLoaiHinh AND t.NamDangKy = hmd.NamDangKy AND t.Luong = hmd.SoLuong LEFT OUTER JOIN
                      dbo.t_HaiQuan_DonViTinh AS dvt ON hmd.DVT_ID = dvt.ID
WHERE     ((CAST(t.SoToKhai AS VARCHAR(10)) + t.MaLoaiHinh + CAST(t.NamDangKy AS VARCHAR(4)) + t.MaHaiQuan) IN
                          (SELECT     CAST(SoToKhai AS VARCHAR(10)) + MaLoaiHinh + CAST(NamDangKy AS VARCHAR(4)) + MaHaiQuan AS Expr1
                            FROM          dbo.t_SXXK_ToKhaiMauDich AS t_SXXK_ToKhaiMauDich_1
                            WHERE      (MaLoaiHinh LIKE 'N%')))

  Go
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '30.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('30.5',GETDATE(), N' Cập nhật các V_HangTon')
END	
