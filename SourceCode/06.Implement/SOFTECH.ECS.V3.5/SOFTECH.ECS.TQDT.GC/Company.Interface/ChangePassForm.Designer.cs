﻿namespace Company.Interface
{
    partial class ChangePassForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangePassForm));
            this.label5 = new System.Windows.Forms.Label();
            this.txtMatKhauMoi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMatKhauCu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMatKhauMoiNhapLai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnUpdate = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.compareValidator1 = new Company.Controls.CustomValidation.CompareValidator();
            this.requiredFieldValidator1 = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compareValidator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.requiredFieldValidator1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.label6);
            this.grbMain.Controls.Add(this.label4);
            this.grbMain.Controls.Add(this.label3);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.btnUpdate);
            this.grbMain.Controls.Add(this.txtMatKhauMoiNhapLai);
            this.grbMain.Controls.Add(this.label1);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Controls.Add(this.txtMatKhauMoi);
            this.grbMain.Controls.Add(this.label2);
            this.grbMain.Controls.Add(this.txtMatKhauCu);
            this.grbMain.Size = new System.Drawing.Size(413, 136);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(27, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Mật khẩu mới";
            // 
            // txtMatKhauMoi
            // 
            this.txtMatKhauMoi.Location = new System.Drawing.Point(157, 46);
            this.txtMatKhauMoi.MaxLength = 255;
            this.txtMatKhauMoi.Name = "txtMatKhauMoi";
            this.txtMatKhauMoi.PasswordChar = '*';
            this.txtMatKhauMoi.Size = new System.Drawing.Size(227, 21);
            this.txtMatKhauMoi.TabIndex = 9;
            this.txtMatKhauMoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMatKhauMoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMatKhauMoi.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(27, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Mật khẩu cũ";
            // 
            // txtMatKhauCu
            // 
            this.txtMatKhauCu.Location = new System.Drawing.Point(157, 20);
            this.txtMatKhauCu.MaxLength = 255;
            this.txtMatKhauCu.Name = "txtMatKhauCu";
            this.txtMatKhauCu.PasswordChar = '*';
            this.txtMatKhauCu.Size = new System.Drawing.Size(227, 21);
            this.txtMatKhauCu.TabIndex = 7;
            this.txtMatKhauCu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMatKhauCu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMatKhauCu.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Nhập lại mật khẩu";
            // 
            // txtMatKhauMoiNhapLai
            // 
            this.txtMatKhauMoiNhapLai.Location = new System.Drawing.Point(157, 73);
            this.txtMatKhauMoiNhapLai.MaxLength = 255;
            this.txtMatKhauMoiNhapLai.Name = "txtMatKhauMoiNhapLai";
            this.txtMatKhauMoiNhapLai.PasswordChar = '*';
            this.txtMatKhauMoiNhapLai.Size = new System.Drawing.Size(227, 21);
            this.txtMatKhauMoiNhapLai.TabIndex = 11;
            this.txtMatKhauMoiNhapLai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMatKhauMoiNhapLai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMatKhauMoiNhapLai.VisualStyleManager = this.vsmMain;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUpdate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.ImageSize = new System.Drawing.Size(20, 20);
            this.btnUpdate.Location = new System.Drawing.Point(182, 101);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(92, 23);
            this.btnUpdate.TabIndex = 12;
            this.btnUpdate.Text = "&Chấp nhận";
            this.btnUpdate.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnUpdate.VisualStyleManager = this.vsmMain;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(280, 101);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "Đón&g";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtMatKhauCu;
            this.rfvMa.ErrorMessage = "\"Nhập lại mật khẩu\" không được để trống";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // compareValidator1
            // 
            this.compareValidator1.ControlToCompare = this.txtMatKhauMoi;
            this.compareValidator1.ControlToValidate = this.txtMatKhauMoiNhapLai;
            this.compareValidator1.ErrorMessage = "\"Mật khẩu mới\" và \"nhập lại mật khẩu\" không giống nhau";
            this.compareValidator1.Icon = ((System.Drawing.Icon)(resources.GetObject("compareValidator1.Icon")));
            this.compareValidator1.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.Equal;
            this.compareValidator1.Tag = "compareValidator1";
            // 
            // requiredFieldValidator1
            // 
            this.requiredFieldValidator1.ControlToValidate = this.txtMatKhauMoi;
            this.requiredFieldValidator1.ErrorMessage = "\"Mật khẩu mới\" không được để trống";
            this.requiredFieldValidator1.Icon = ((System.Drawing.Icon)(resources.GetObject("requiredFieldValidator1.Icon")));
            this.requiredFieldValidator1.Tag = "requiredFieldValidator1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(391, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(391, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "*";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(390, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "*";
            // 
            // ChangePassForm
            // 
            this.AcceptButton = this.btnUpdate;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(413, 136);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangePassForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Đổi mật khẩu";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compareValidator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.requiredFieldValidator1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.EditControls.EditBox txtMatKhauMoiNhapLai;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtMatKhauMoi;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtMatKhauCu;
        private Janus.Windows.EditControls.UIButton btnUpdate;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.CompareValidator compareValidator1;
        private Company.Controls.CustomValidation.RequiredFieldValidator requiredFieldValidator1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
    }
}
