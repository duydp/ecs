﻿using System;
using System.Drawing;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.SXXK.ToKhai;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.KD.BLL;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;

namespace Company.Interface.SXXK
{
    public partial class HangMauDichRegisterForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        private double clg;
        private double dgnt;
        public HangMauDichCollection HMDCollection = new HangMauDichCollection();
        public HangMauDich HMD = null;
        public string LoaiHangHoa;
        private decimal luong;

        public string MaNguyenTe;
        public string MaNuocXX;
        public string NhomLoaiHinh = string.Empty;
        private double st_clg;

        //-----------------------------------------------------------------------------------------				
        private double tgnt;
        private double tgtt_gtgt;

        private double tgtt_nk;
        private double tgtt_ttdb;
        private double tl_clg;
        private double tongTGKB;
        private double ts_gtgt;

        private double ts_nk;
        private double ts_ttdb;
        private double tt_gtgt;

        private double tt_nk;
        private double tt_ttdb;
        public double TyGiaTT;
        public ToKhaiMauDich TKMD;
        public bool create = false;
        private NguyenPhuLieuRegistedForm NPLRegistedForm;
        private SanPhamRegistedForm SPRegistedForm;
        //-----------------------------------------------------------------------------------------

        public HangMauDichRegisterForm()
        {
            InitializeComponent();
        }

        private void khoitao_GiaoDien()
        {
            if (this.NhomLoaiHinh.Substring(1, 2) == "GC" || this.NhomLoaiHinh.Substring(1, 2) == "SX")
            {
                txtMaHang.ButtonStyle = EditButtonStyle.Ellipsis;
                txtTenHang.ReadOnly = cbDonViTinh.ReadOnly = true;
                txtMaHang.BackColor = txtTenHang.BackColor = cbDonViTinh.BackColor = Color.FromArgb(255, 255, 192);
            }
        }

        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
        }

        private double tinhthue()
        {
            this.tgnt = this.dgnt * Convert.ToDouble(this.luong);
            this.tgtt_nk = this.tgnt * this.TyGiaTT;
            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt);
            txtTien_CLG.Value = Math.Round(this.st_clg);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg);
        }

        private double tinhthue2()
        {
            this.tgnt = this.dgnt * Convert.ToDouble(this.luong);
            this.tgtt_nk = Convert.ToDouble(txtTGTT_NK.Value);
            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt);
            txtTien_CLG.Value = Math.Round(this.st_clg);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg);
        }

        //-----------------------------------------------------------------------------------------
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangMauDich hmd in HMDCollection)
            {
                if (hmd.MaPhu.Trim() == maHang) return true;
            }
            return false;
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            this.HMDCollection.Remove(HMD);
            if (checkMaHangExit(txtMaHang.Text.Trim()))
            {
                MLMessages("Mặt hàng này đã được chọn.","MSG_SAV10","", false);
                if(HMD.MaPhu.Trim()!="")
                    this.HMDCollection.Add(HMD);
                return;
            }
            
            HMD.MaHS = txtMaHS.Text;
            HMD.MaPhu = txtMaHang.Text.Trim();
            HMD.TenHang = txtTenHang.Text.Trim();
            HMD.NuocXX_ID = ctrNuocXX.Ma;
            HMD.MaHaiQuan=GlobalSettings.MA_HAI_QUAN;
            HMD.SoToKhai = TKMD.SoToKhai;
            HMD.MaLoaiHinh = TKMD.MaLoaiHinh;
            HMD.NamDangKy = TKMD.NamDangKy;
            HMD.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            HMD.SoLuong = Convert.ToDecimal(txtLuong.Value);
            HMD.DonGiaKB = Convert.ToDouble(txtDGNT.Value);
            HMD.TriGiaKB = Convert.ToDouble(txtTGNT.Value);
            HMD.TriGiaTT = Convert.ToDouble(txtTGTT_NK.Value);
            HMD.ThueSuatXNK = Convert.ToDouble(txtTS_NK.Value);
            HMD.ThueSuatTTDB = Convert.ToDouble(txtTS_TTDB.Value);
            HMD.ThueSuatGTGT = Convert.ToDouble(txtTS_GTGT.Value);
            HMD.TyLeThuKhac = Convert.ToDouble(txtTL_CLG.Value);
            HMD.ThueXNK = Math.Round(Convert.ToDouble(txtTienThue_NK.Value), 0);
            HMD.ThueTTDB = Math.Round(Convert.ToDouble(txtTienThue_TTDB.Value), 0);
            HMD.ThueGTGT = Math.Round(Convert.ToDouble(txtTienThue_GTGT.Value), 0);
            HMD.DonGiaTT = HMD.TriGiaTT / Convert.ToDouble(HMD.SoLuong);
            if (chkMienThue.Checked)
                HMD.MienThue = 1;
            else
                HMD.MienThue = 0;
            this.HMDCollection.Add(HMD);
            // Tính thuế.
            //hmd.TinhThue(this.TyGiaTT);

            //this.HMDCollection.Add(hmd);

            this.tongTGKB += Convert.ToDouble(txtTGNT.Value);
            lblTongTGKB.Text += " : " + this.tongTGKB.ToString("N");
            if (Properties.Settings.Default.NgonNgu == "1") lblTongTGKB.Text = "Total declared value of : " + this.tongTGKB.ToString("N");
            if (create)
            {
                int k = 0;
                foreach (HangMauDich hang in this.HMDCollection)
                {
                    if (k < hang.SoThuTuHang)
                        k = hang.SoThuTuHang;
                }
                HMD.SoThuTuHang = (short)(k + 1);
            }
            this.TKMD.InsertUpdateFullHang(HMD,create);

            //dgList.DataSource = this.HMDCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            txtMaHang.Text = "";
            txtTenHang.Text = "";
            txtMaHS.Text = "";
            txtLuong.Value = 0;
            txtDGNT.Value = 0;
            HMD = new HangMauDich();
            create = true;
        }

        //-----------------------------------------------------------------------------------------
        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            if (LoaiHangHoa == "N")
            {
                Company.KD.BLL.SXXK.HangHoaNhap npl = new Company.KD.BLL.SXXK.HangHoaNhap();
                npl.Ma = txtMaHang.Text.Trim();
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ;
                if (npl.Load())
                {
                    txtMaHang.Text = npl.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = npl.MaHS;
                    txtTenHang.Text = npl.Ten;
                    cbDonViTinh.SelectedValue = npl.DVT_ID;
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.","This value is not exist"));

                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                }
            }
            else
            {
                Company.KD.BLL.SXXK.HangHoaXuat sp = new Company.KD.BLL.SXXK.HangHoaXuat();
                sp.Ma = txtMaHang.Text.Trim();
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ; ;
                if (sp.Load())
                {
                    txtMaHang.Text = sp.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = sp.MaHS;
                    txtTenHang.Text = sp.Ten;
                    cbDonViTinh.SelectedValue = sp.DVT_ID;
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang,setText( "Không tồn tại sản phẩm này.","This value is not exist"));
                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                }
            }
        }

        private void HangMauDichForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDien();

            lblNguyenTe_DGNT.Text = lblNguyenTe_TGNT.Text = "(" + this.MaNguyenTe + ")";
            toolTip1.SetToolTip(lblNguyenTe_DGNT, this.MaNguyenTe);
            toolTip1.SetToolTip(lblNguyenTe_TGNT, this.MaNguyenTe);

            this.tongTGKB = 0;
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                // Tính lại thuế.
                //hmd.TinhThue(this.TyGiaTT);
                // Tổng trị giá khai báo.
                this.tongTGKB += hmd.TriGiaKB;
            }
            dgList.DataSource = this.HMDCollection;

            lblTongTGKB.Text += string.Format(" : {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
            lblTyGiaTT.Text += " : " + this.TyGiaTT.ToString("N");
            if (HMD != null && HMD.SoToKhai > 0)
            {
                txtMaHang.Text = this.HMD.MaPhu;
                txtTenHang.Text = this.HMD.TenHang;
                txtMaHS.Text = this.HMD.MaHS;
                cbDonViTinh.SelectedValue = this.HMD.DVT_ID;
                txtDGNT.Value = this.dgnt = this.HMD.DonGiaKB;
                txtLuong.Value = this.luong = this.HMD.SoLuong;
                txtTGNT.Value = this.HMD.TriGiaKB;
                txtTS_NK.Value = this.ts_nk = this.HMD.ThueSuatXNK;
                txtTS_TTDB.Value = this.ts_ttdb = this.HMD.ThueSuatTTDB;
                txtTS_GTGT.Value = this.ts_gtgt = this.HMD.ThueSuatGTGT;
                txtTL_CLG.Value = this.tl_clg = this.HMD.TyLeThuKhac;
                txtTGTT_NK.Value = HMD.TriGiaTT;
                this.ts_nk = this.HMD.ThueSuatXNK / 100;
                this.ts_ttdb = this.HMD.ThueSuatTTDB / 100;
                this.ts_gtgt = this.HMD.ThueSuatGTGT / 100;
                this.tl_clg = this.HMD.TyLeThuKhac / 100;
                this.tinhthue2();
            }
            if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.CapNhatDuLieu)))
            {
                uiButton1.Visible = false;
            }  
        }

        private void txtLuong_Leave(object sender, EventArgs e)
        {
            this.luong = Convert.ToDecimal(txtLuong.Value);
            this.tinhthue();
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            this.dgnt = Convert.ToDouble(txtDGNT.Value);
            this.tinhthue();
        }

        private void txtTS_NK_Leave(object sender, EventArgs e)
        {
            this.ts_nk = Convert.ToDouble(txtTS_NK.Value) / 100;
            this.tinhthue2();
        }

        private void txtTS_TTDB_Leave(object sender, EventArgs e)
        {
            this.ts_ttdb = Convert.ToDouble(txtTS_TTDB.Value) / 100;
            this.tinhthue2();
        }

        private void txtTS_GTGT_Leave(object sender, EventArgs e)
        {
            this.ts_gtgt = Convert.ToDouble(txtTS_GTGT.Value) / 100;
            this.tinhthue2();
        }

        private void txtTS_CLG_Leave(object sender, EventArgs e)
        {
            this.tl_clg = Convert.ToDouble(txtTL_CLG.Value) / 100;
            this.tinhthue2();
        }

        //-----------------------------------------------------------------------------------------------
      

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);
        }

        private void chkMienThue_CheckedChanged(object sender, EventArgs e)
        {
            grbThue.Enabled = !chkMienThue.Checked;
            txtTS_NK.Value = 0;
            txtTS_TTDB.Value = 0;
            txtTS_GTGT.Value = 0;
            txtTL_CLG.Value = 0;
            this.tinhthue();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            switch (this.NhomLoaiHinh[0].ToString())
            {
                case "N":
                    if (this.NPLRegistedForm == null)
                        this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                    this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                    this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ;
                    this.NPLRegistedForm.ShowDialog(this);
                    if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "")
                    {
                        txtMaHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                    }
                    break;
                case "X":
                    if (this.LoaiHangHoa == "N")
                    {
                        if (this.NPLRegistedForm == null)
                            this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                        this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                        this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ;
                        this.NPLRegistedForm.ShowDialog(this);
                        if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "")
                        {
                            txtMaHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                            txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                            txtMaHS.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                            cbDonViTinh.SelectedValue = this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                        }
                    }
                    else
                    {
                        if (this.SPRegistedForm == null)
                            this.SPRegistedForm = new SanPhamRegistedForm();
                        this.SPRegistedForm.CalledForm = "HangMauDichForm";
                        this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        this.SPRegistedForm.ShowDialog(this);
                        if (this.SPRegistedForm.SanPhamSelected.Ma != "")
                        {
                            txtMaHang.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                            txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                            txtMaHS.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
                            cbDonViTinh.SelectedValue = this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3);
                        }
                    }
                    break;
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    this.HMD = (HangMauDich)e.Row.DataRow;
                    if (HMD != null && HMD.SoToKhai > 0)
                    {
                        txtMaHang.Text = this.HMD.MaPhu;
                        txtTenHang.Text = this.HMD.TenHang;
                        txtMaHS.Text = this.HMD.MaHS;
                        cbDonViTinh.SelectedValue = this.HMD.DVT_ID;


                        txtDGNT.Value = this.dgnt = this.HMD.DonGiaKB;
                        txtLuong.Value = this.luong = this.HMD.SoLuong;
                        txtTGNT.Value = this.HMD.TriGiaKB;
                        txtTS_NK.Value = this.ts_nk = this.HMD.ThueSuatXNK;
                        txtTS_TTDB.Value = this.ts_ttdb = this.HMD.ThueSuatTTDB;
                        txtTS_GTGT.Value = this.ts_gtgt = this.HMD.ThueSuatGTGT;
                        txtTL_CLG.Value = this.tl_clg = this.HMD.TyLeThuKhac;
                        txtTGTT_NK.Value = this.HMD.TriGiaTT;
                        this.ts_nk = this.HMD.ThueSuatXNK / 100;
                        this.ts_ttdb = this.HMD.ThueSuatTTDB / 100;
                        this.ts_gtgt = this.HMD.ThueSuatGTGT / 100;
                        this.tl_clg = this.HMD.TyLeThuKhac / 100;
                        create = false;
                        this.tinhthue2();
                    }
                    
                }
                break;
            }
        }

        private void txtTGTT_NK_Leave(object sender, EventArgs e)
        {
            this.tinhthue2();
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
