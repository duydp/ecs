﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Thông báo thuế
    /// </summary>
    public class AdditionalDocument : IssueBase
    {
        /// <summary>
        /// Mã thông báo thuế (= 006)
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }
        
        /// <summary>
        /// Hợp đồng - Giấy phép - Vận đơn
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }

        /// <summary>
        /// Ngày hết hạn thông báo thuế 
        /// / hết hạn hợp đồng
        /// </summary>
        [XmlElement("expire")]
        public string Expire { get; set; }
        
        [XmlElement("isDebt")]
        public string IsDebt { get; set; }

        [XmlElement("submit")]
        public string Submit { get; set; }

        [XmlElement("DutyTaxFee")]
        public List<DutyTaxFee> DutyTaxFees { get; set; }        
        /// <summary>
        /// Chương, loại, khoản, mục
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }
        /// <summary>
        ///  ....  
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }
        /// <summary>
        /// Ma xac dinh loai chung tu thanh khoan (tieu huy, bieu tang, to khai tham chieu ...) 
        /// </summary>
        [XmlElement("documentType")]
        public string DocumentType { get; set; }
        /// <summary>
        /// Vi du: tieu huy
        /// </summary>
        [XmlElement("documentReference")]
        public string DocumentReference { get; set; }
        /// <summary>
        /// <!--So chung tu (co the la bo so to khai .... -->
        /// </summary>
        [XmlElement("description")]
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("GoodItems")]
        public List<GoodsItem> GoodItems { get; set; }



    }
}
