﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;
using Company.Interface.GC;
 

namespace Company.Interface.Report.GC
{
    public partial class BangKe09_HQGC_TT117 : DevExpress.XtraReports.UI.XtraReport
    {
        public Report.ReportViewBC06Form report = new ReportViewBC06Form();
        public HopDong HD = new HopDong();
        public DataSet dsBK = new DataSet();
        public BangKe09_HQGC_TT117()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
           // DataTable dt = new Company.GC.BLL.GC.ThanhKhoanHDGC().getDuLieuThanhKhoan(this.HD.ID).Tables[0];

           DataTable dt = dsBK.Tables[0];

           xrLabel11.Text =string.Format("{0}, ngày {1} tháng {2} năm {3}","",DateTime.Now.Day,DateTime.Now.Month,DateTime.Now.Year);           
            lblSTT.DataBindings.Add("Text", this.DataSource, "STT");
            lblToKhai.DataBindings.Add("Text", this.DataSource, "SoNgayToKhai");

            lblSoLuong.DataBindings.Add("Text", this.DataSource, "SoLuong", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTriGia.DataBindings.Add("Text", this.DataSource, "TriGia", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblHdPl.DataBindings.Add("Text", this.DataSource, "HdPhuLuc");
            lblCkXuat.DataBindings.Add("Text", this.DataSource,"CuaKhau");
            lblSoNgayBL.DataBindings.Add("Text", this.DataSource, "SoNgayBL");
            lblNplSp.DataBindings.Add("Text", this.DataSource, "TenHang");
            //xrLabel19.Text  = xrLabel11.Text = xrLabel13.Text= GlobalSettings.TieudeNgay;
            
        }

        private void xrLabel40_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void lblTenNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //string ten = GetCurrentColumnValue("Ten").ToString();
            
            //lblToKhai.Text = ten.Substring(0,ten.LastIndexOf("(") - 1);
        }

        private void lblMaNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //string ten = GetCurrentColumnValue("Ten").ToString();
            //string[] arr = ten.Split(new char[1] {'('});
            //lblNplSp.Text = arr[arr.Length - 1].Replace(")","");
        }

        private void lblhopsoHDGC_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRLabel label = (XRLabel)sender;
            report.Label = label;
            report.txtName.Text = label.Text;
            report.lblName.Text = label.Tag.ToString();
        }
        public void setText(XRLabel label, string text)
        {
            label.Text = text;
        }
    }
}
