using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class SFA_HANGHOA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute GDS { get; set; }
public PropertiesAttribute HSC { get; set; }
public PropertiesAttribute CO { get; set; }
public PropertiesAttribute QT { get; set; }
public PropertiesAttribute QTU { get; set; }
public PropertiesAttribute WG { get; set; }
public PropertiesAttribute WGU { get; set; }

public SFA_HANGHOA()
        {
GDS = new PropertiesAttribute(768, typeof(string));
HSC = new PropertiesAttribute(12, typeof(string));
CO = new PropertiesAttribute(2, typeof(string));
QT = new PropertiesAttribute(8, typeof(int));
QTU = new PropertiesAttribute(3, typeof(string));
WG = new PropertiesAttribute(10, typeof(int));
WGU = new PropertiesAttribute(3, typeof(string));
TongSoByte = 820;
}

}public partial class EnumGroupID
    {
}

}
