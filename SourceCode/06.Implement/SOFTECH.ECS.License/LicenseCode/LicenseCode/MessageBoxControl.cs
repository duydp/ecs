﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace LicenseCode
{
    public partial class MessageBoxControl : DevExpress.XtraEditors.XtraForm
    {
        public MessageBoxControl()
        {
            InitializeComponent();
        }
        public  MessageBoxControl _MsgBox;
        public string ReturnValue = "Yes";
        public string MessageString
        {
            set { this.txtMessage.Text = value; }
            get { return this.txtMessage.Text; }
        }
        internal static string ShowMessage(string message)
        {
           MessageBoxControl _MsgBox = new MessageBoxControl();
            _MsgBox.btnYes.Visible = false;
            _MsgBox.btnNo.Visible = false;
            _MsgBox.MessageString = message;
            _MsgBox.ShowDialog();
            string st = _MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        internal static string ShowMessageConfirm(string message)
        {
            MessageBoxControl _MsgBox = new MessageBoxControl();
            _MsgBox.btnCancel.Visible = false;
            _MsgBox.btnError.Visible = false;
            _MsgBox.btnYes.Visible = true;
            _MsgBox.btnNo.Visible = true;
            _MsgBox.MessageString = message;
            _MsgBox.ShowDialog();
            string st = _MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            this.ReturnValue = "Yes";
            this.Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            this.ReturnValue = "No";
            this.Close();
        }
    }
}