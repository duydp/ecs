using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.GC.BLL.KDT.GC
{
    public partial class HangPhuKien
    {
        //public void Delete(string MaPhuKien, long HopDong_ID)
        //{
        //    using (SqlConnection connection = (SqlConnection)db.CreateConnection())
        //    {
        //        connection.Open();
        //        SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
        //        try
        //        {
        //            if (MaPhuKien == "N01")
        //            {
        //                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //                npl.HopDong_ID = HopDong_ID;
        //                npl.Ma = this.MaHang;
        //                npl.Load(transaction);
        //                npl.DeleteTransaction(transaction);
        //            }
        //            //else if (MaPhuKien == "N05")//chua
        //            //{
        //            //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //            //    npl.Ma = this.MaHang;
        //            //    npl.HopDong_ID = HopDong_ID;
        //            //    npl.Load(transaction);
        //            //    npl.SoLuongDangKy = npl.SoLuongDangKy - (this.SoLuong - Convert.ToDecimal(this.ThongTinCu));
        //            //    npl.UpdateTransaction(transaction);
        //            //}
        //            //else if (MaPhuKien == "N06")
        //            //{
        //            //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //            //    npl.Ma = this.MaHang;
        //            //    npl.HopDong_ID = HopDong_ID;
        //            //    npl.Load(transaction);
        //            //    npl.DVT_ID = DuLieuChuan.DonViTinh.GetID(this.ThongTinCu);
        //            //    npl.UpdateTransaction(transaction);
        //            //}
        //            //else if (MaPhuKien == "N11")
        //            //{
        //            //    Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
        //            //    npl.Ma = this.MaHang;
        //            //    npl.HopDong_ID = HopDong_ID;
        //            //    npl.Load(transaction);
        //            //    npl.SoLuongCungUng -= this.SoLuong;
        //            //    npl.UpdateTransaction(transaction);
        //            //}
        //            //else if (MaPhuKien == "S06")
        //            //{
        //            //    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
        //            //    sp.Ma = this.MaHang;
        //            //    sp.HopDong_ID = HopDong_ID;
        //            //    sp.Load(transaction);
        //            //    sp.DVT_ID = DuLieuChuan.DonViTinh.GetID(this.ThongTinCu);
        //            //    sp.UpdateTransaction(transaction);
        //            //}
        //            //else if (MaPhuKien == "S10")
        //            //{
        //            //    Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
        //            //    sp.Ma = this.MaHang;
        //            //    sp.HopDong_ID = HopDong_ID;
        //            //    sp.Load(transaction);
        //            //    sp.Ma = this.ThongTinCu;
        //            //    sp.UpdateMaSPTransaction(transaction, this.MaHang);
        //            //    //cap nhat lai dinh muc neu co
        //            //    Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
        //            //    dm.HopDong_ID = HopDong_ID;
        //            //    dm.MaSanPham = sp.Ma;
        //            //    dm.UpdateMaDinhMucTransaction(transaction, this.MaHang);
        //            //}
        //            else if (MaPhuKien == "S13")
        //            {
        //                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
        //                sp.Ma = this.MaHang;
        //                sp.HopDong_ID = HopDong_ID;
        //                sp.DeleteTransaction(transaction);
        //            }
        //            else if (MaPhuKien == "S15")
        //            {
        //                Company.GC.BLL.GC.NhomSanPham LoaiSP = new Company.GC.BLL.GC.NhomSanPham();
        //                LoaiSP.MaSanPham = this.MaHang;
        //                LoaiSP.HopDong_ID = HopDong_ID;
        //                LoaiSP.DeleteTransaction(transaction);
        //            }
        //            //else if (MaPhuKien == "T01")
        //            //{
        //            //    Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
        //            //    tb.Ma = this.MaHang;
        //            //    tb.HopDong_ID = HopDong_ID;
        //            //    tb.TrangThai = 2;
        //            //    tb.Load(transaction);
        //            //    tb.SoLuongDangKy = Convert.ToDecimal(this.ThongTinCu); ;
        //            //    tb.UpdateTransaction(transaction);
        //            //}
        //            else if (MaPhuKien == "T07")
        //            {
        //                BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
        //                entity.HopDong_ID = HopDong_ID;
        //                entity.Ma = this.MaHang;
        //                entity.DeleteTransaction(transaction);
        //            }
        //            this.DeleteTransaction(transaction);
        //            transaction.Commit();
        //        }
        //        catch(Exception ex)
        //        {                    
        //            transaction.Rollback();
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }            
        //}
        public bool Load(SqlTransaction trans)
        {
            string spName = "p_KDT_GC_HangPhuKien_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand, trans);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) this._MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) this._TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinCu"))) this._ThongTinCu = reader.GetString(reader.GetOrdinal("ThongTinCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) this._SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) this._DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) this._NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) this._TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) this._TinhTrang = reader.GetString(reader.GetOrdinal("TinhTrang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) this._Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSP"))) this._NhomSP = reader.GetString(reader.GetOrdinal("NhomSP"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public void Delete(string MaPhuKien, long HopDong_ID)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    if (MaPhuKien.Trim() == "N01")
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl.HopDong_ID = HopDong_ID;
                        npl.Ma = this.MaHang;
                        npl.DeleteTransaction(transaction);
                    }
                    else if (MaPhuKien.Trim() == "N05")//chua
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl.Ma = this.MaHang;
                        npl.HopDong_ID = HopDong_ID;
                        npl.Load(transaction);
                        npl.TrangThai = 0;
                        npl.SoLuongDangKy = npl.SoLuongDangKy - (this.SoLuong - Convert.ToDecimal(this.ThongTinCu));
                        npl.UpdateTransaction(transaction);
                    }
                    else if (MaPhuKien.Trim() == "N06")
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl.Ma = this.MaHang;
                        npl.HopDong_ID = HopDong_ID;
                        npl.Load(transaction);
                        npl.TrangThai = 0;
                        npl.DVT_ID = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetID(this.ThongTinCu);
                        npl.UpdateTransaction(transaction);
                    }
                    else if (MaPhuKien.Trim() == "N11")
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl.Ma = this.MaHang;
                        npl.HopDong_ID = HopDong_ID;
                        npl.Load(transaction);
                        npl.TrangThai = 0;
                        npl.SoLuongCungUng -= this.SoLuong;
                        npl.UpdateTransaction(transaction);
                    }
                    else if (MaPhuKien.Trim() == "S06")
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.Ma = this.MaHang;
                        sp.HopDong_ID = HopDong_ID;
                        sp.Load(transaction);
                        sp.DVT_ID = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetID(this.ThongTinCu);
                        sp.TrangThai = 0;
                        sp.UpdateTransaction(transaction);
                    }
                    else if (MaPhuKien.Trim() == "S10")
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.Ma = this.MaHang;
                        sp.HopDong_ID = HopDong_ID;
                        sp.Load(transaction);
                        sp.Ma = this.ThongTinCu;
                        sp.TrangThai = 0;
                        sp.UpdateMaSPTransaction(transaction, this.MaHang);
                        //cap nhat lai dinh muc neu co
                        Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
                        dm.HopDong_ID = HopDong_ID;
                        dm.MaSanPham = sp.Ma;
                        dm.UpdateMaDinhMucTransaction(transaction, this.MaHang);
                    }
                    else if (MaPhuKien.Trim() == "S13")
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.Ma = this.MaHang;
                        sp.HopDong_ID = HopDong_ID;
                        sp.DeleteTransaction(transaction);
                    }
                    else if (MaPhuKien.Trim() == "S15")
                    {
                        Company.GC.BLL.GC.NhomSanPham LoaiSP = new Company.GC.BLL.GC.NhomSanPham();
                        LoaiSP.MaSanPham = this.MaHang;
                        LoaiSP.HopDong_ID = HopDong_ID;
                        LoaiSP.DeleteTransaction(transaction);
                    }
                    else if (MaPhuKien.Trim() == "S09")//chua
                    {
                        Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                        sp.Ma = this.MaHang;
                        sp.HopDong_ID = HopDong_ID;
                        sp.Load(transaction);
                        sp.TrangThai = 0;
                        sp.SoLuongDangKy = sp.SoLuongDangKy - (this.SoLuong - Convert.ToDecimal(this.ThongTinCu));
                        sp.UpdateTransaction(transaction);
                    }
                    else if (MaPhuKien.Trim() == "T01")
                    {
                        Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                        tb.Ma = this.MaHang;
                        tb.HopDong_ID = HopDong_ID;
                        tb.Load(transaction);
                        tb.TrangThai = 0;
                        tb.SoLuongDangKy = Convert.ToDecimal(this.ThongTinCu); ;
                        tb.UpdateTransaction(transaction);
                    }
                    else if (MaPhuKien.Trim() == "T07")
                    {
                        BLL.GC.ThietBi entity = new Company.GC.BLL.GC.ThietBi();
                        entity.HopDong_ID = HopDong_ID;
                        entity.Ma = this.MaHang;
                        entity.DeleteTransaction(transaction);
                    }
                    this.DeleteTransaction(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public long InsertTransaction(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_HangPhuKien_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.NVarChar, this._ThongTinCu);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, this._DonGia);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, this._TriGia);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, this._TinhTrang);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@NhomSP", SqlDbType.VarChar, this._NhomSP);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        public int UpdateTransaction(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_HangPhuKien_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.NVarChar, this._ThongTinCu);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, this._DonGia);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, this._TriGia);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, this._TinhTrang);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@NhomSP", SqlDbType.VarChar, this._NhomSP);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_HangPhuKien_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.NVarChar, this._ThongTinCu);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, this._DonGia);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, this._TriGia);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, this._TinhTrang);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@NhomSP", SqlDbType.VarChar, this._NhomSP);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public HangPhuKienCollection SelectCollectionBy_Master_ID(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_HangPhuKien_SelectBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            HangPhuKienCollection collection = new HangPhuKienCollection();
            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                HangPhuKien entity = new HangPhuKien();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinCu"))) entity.ThongTinCu = reader.GetString(reader.GetOrdinal("ThongTinCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) entity.TinhTrang = reader.GetString(reader.GetOrdinal("TinhTrang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSP"))) entity.NhomSP = reader.GetString(reader.GetOrdinal("NhomSP"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

    }
}