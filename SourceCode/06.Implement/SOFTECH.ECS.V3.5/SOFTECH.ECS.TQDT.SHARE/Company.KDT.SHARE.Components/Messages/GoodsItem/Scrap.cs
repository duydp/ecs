﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GoodsItem
{
   public class Scrap
    {
        /// <summary>
        /// Hàng hóa
        /// </summary>
       [XmlElement("Commodity")]
       public Commodity Commodity  { get; set; }

       /// <summary>
       /// Hàng hóa
       /// </summary>
       [XmlElement("GoodsMeasure")]
       public GoodsMeasure GoodsMeasure { get; set; }
    }
}
