﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components;
using System.Threading;
using Company.Interface.VNACCS;
using Company.Interface.DanhMucChuan;
using System.IO;
using System.Diagnostics;
#if SXXK_V4
using Company.Interface.SXXK;
using System.IO;
using System.Diagnostics;
#elif GC_V4
using Company.Interface.GC;
using Company.GC.BLL.GC;
//using System.IO;
//using System.Diagnostics;
#endif


namespace Company.Interface
{
    public partial class VNACC_HangMauDichXuatForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        public KDT_VNACC_HangMauDich HMD = null;
        public List<KDT_VNACC_HangMauDich> ListHMD = new List<KDT_VNACC_HangMauDich>();
        DataTable dtHS = new DataTable();
        public string MaTienTe = "";
        private double soLuong;
        private double triGiaHD;
        private double donGiaHD;
        public String Caption;
        public bool IsChange;
#if SXXK_V4
        NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
#elif GC_V4
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
#endif
        public string loaiHangHoa = "SP";

        public VNACC_HangMauDichXuatForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.EDA.ToString());
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoSua)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void GetHangMauDich()
        {
            HMD.MaSoHang = ctrMaSoHang.Code;
            HMD.MaQuanLy = txtMaQuanLy.Text;
            HMD.MaHangHoa = txtMaHangHoa.Text;
            HMD.TenHang = txtTenHang.Text;
            if (grList.SelectedItems.Count == 1)
            {
                HMD.SoDong = grList.RowCount >= 10 ? (grList.GetRow().RowIndex + 1).ToString() : "0" + (grList.GetRow().RowIndex + 1).ToString();
            }
            else
            {
                HMD.SoDong = grList.RowCount >= 10 ? (grList.RowCount + 1).ToString() : "0" + (grList.RowCount + 1).ToString();
            }
            HMD.ThueSuat = Convert.ToDecimal(txtThueSuat.Value);
            HMD.ThueSuatTuyetDoi = Convert.ToDecimal(txtThueSuatTuyetDoi.Value);
            HMD.MaDVTTuyetDoi = ctrMaDVTTuyetDoi.Code;
            HMD.MaTTTuyetDoi = ctrMaTTTuyetDoi.Code;
            HMD.MaMienGiamThue = ctrMaMienGiamThue.Code;
            HMD.DieuKhoanMienGiam = txtDieuKhoanMienGiam.Text;
            HMD.SoTienGiamThue = Convert.ToDecimal(txtSoTienMienGiam.Value);
            HMD.SoLuong1 = Convert.ToDecimal(txtSoLuong1.Value);
            HMD.DVTLuong1 = ctrDVTLuong1.Code;
            HMD.SoLuong2 = Convert.ToDecimal(txtSoLuong2.Value);
            HMD.DVTLuong2 = ctrDVTLuong2.Code;
            HMD.TriGiaHoaDon = Convert.ToDecimal(txtTriGiaHoaDon.Value);
            HMD.DonGiaHoaDon = Convert.ToDecimal(txtDonGiaHoaDon.Value);
            HMD.DV_SL_TrongDonGiaTinhThue = ctrDVTDonGia.Code;
            // duydp 09/11/2015 Fix khai báo hàng FOC không có đơn giá và trị giá
            if (HMD.DonGiaHoaDon == 0)
            {
                HMD.DVTDonGia = String.Empty;
                HMD.MaTTDonGia = String.Empty;
            }
            else
            {
                HMD.DVTDonGia = ctrDVTDonGia.Code;
                HMD.MaTTDonGia = ctrMaTTDonGia.Code;
            }
            // duydp 09/11/2015
            //minhnd09/04/2015 add nước xuất xứ
            HMD.NuocXuatXu = ctrNuocXuatXu.Code;
            //minhnd09/04/2015
            HMD.TriGiaTinhThue = Convert.ToDecimal(txtTriGiaTinhThue.Value);
            HMD.MaTTTriGiaTinhThue = ctrMaTTTriGiaTinhThue.Code;
            HMD.SoTTDongHangTKTNTX = txtSoTTDongHangTKTNTX.Text;
            HMD.SoDMMienThue = txtSoDMMienThue.Text;
            HMD.SoDongDMMienThue = txtSoDongDMMienThue.Text;
            HMD.MaVanBanPhapQuyKhac1 = ctrMaVanBan1.Code;
            HMD.MaVanBanPhapQuyKhac2 = ctrMaVanBan2.Code;
            HMD.MaVanBanPhapQuyKhac3 = ctrMaVanBan3.Code;
            HMD.MaVanBanPhapQuyKhac4 = ctrMaVanBan4.Code;
            HMD.MaVanBanPhapQuyKhac5 = ctrMaVanBan5.Code;

        }

        private void SetHangMauDich()
        {
            ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaSoHang.Code = HMD.MaSoHang;
            ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaHangHoa.Text = HMD.MaHangHoa;
            txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

            txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaQuanLy.Text = HMD.MaQuanLy;
            txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

            txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
            txtTenHang.Text = HMD.TenHang;
            txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

            txtThueSuat.TextChanged -= new EventHandler(txt_TextChanged);
            txtThueSuat.Text = HMD.ThueSuat.ToString();
            txtThueSuat.TextChanged += new EventHandler(txt_TextChanged);

            txtThueSuatTuyetDoi.TextChanged -= new EventHandler(txt_TextChanged);
            txtThueSuatTuyetDoi.Text = HMD.ThueSuatTuyetDoi.ToString();
            txtThueSuatTuyetDoi.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaDVTTuyetDoi.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaDVTTuyetDoi.Code = HMD.MaDVTTuyetDoi;
            ctrMaDVTTuyetDoi.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaTTTuyetDoi.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTTuyetDoi.Code = HMD.MaTTTuyetDoi;
            ctrMaTTTuyetDoi.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaMienGiamThue.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaMienGiamThue.Code = HMD.MaMienGiamThue;
            ctrMaMienGiamThue.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtSoTienMienGiam.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoTienMienGiam.Text = HMD.SoTienGiamThue.ToString();
            txtSoTienMienGiam.TextChanged += new EventHandler(txt_TextChanged);


            txtSoLuong1.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoLuong1.Text = HMD.SoLuong1.ToString();
            txtSoLuong1.TextChanged += new EventHandler(txt_TextChanged);

            ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrDVTLuong1.Code = HMD.DVTLuong1;
            ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtSoLuong2.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoLuong2.Text = HMD.SoLuong2.ToString();
            txtSoLuong2.TextChanged += new EventHandler(txt_TextChanged);

            ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrDVTLuong2.Code = HMD.DVTLuong2;
            ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtTriGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
            txtTriGiaHoaDon.Text = HMD.TriGiaHoaDon.ToString();
            txtTriGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

            txtDonGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
            txtDonGiaHoaDon.Text = HMD.DonGiaHoaDon.ToString();
            txtDonGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

            ctrDVTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrDVTDonGia.Code = HMD.DVTDonGia;
            ctrDVTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaTTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTDonGia.Code = HMD.MaTTDonGia;
            ctrMaTTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //ctrMaSoHang.Code = HMD.MaSoHang;
            //txtMaQuanLy.Text = HMD.MaQuanLy;
            //txtMaHangHoa.Text = HMD.MaHangHoa;
            //txtTenHang.Text = HMD.TenHang;
            //txtThueSuat.Text = HMD.ThueSuat.ToString();
            //txtThueSuatTuyetDoi.Text = HMD.ThueSuatTuyetDoi.ToString();
            //ctrMaDVTTuyetDoi.Code = HMD.MaDVTTuyetDoi;
            //ctrMaTTTuyetDoi.Code = HMD.MaTTTuyetDoi;
            //ctrMaMienGiamThue.Code = HMD.MaMienGiamThue;
            //txtSoTienMienGiam.Text = HMD.SoTienGiamThue.ToString();
            //txtSoLuong1.Text = HMD.SoLuong1.ToString();
            //ctrDVTLuong1.Code = HMD.DVTLuong1;
            //txtSoLuong2.Text = HMD.SoLuong2.ToString();
            //ctrDVTLuong2.Code = HMD.DVTLuong2;
            //txtTriGiaHoaDon.Text = HMD.TriGiaHoaDon.ToString();
            //txtDonGiaHoaDon.Text = HMD.DonGiaHoaDon.ToString();
            //ctrDVTDonGia.Code = HMD.DVTDonGia;
            //ctrMaTTDonGia.Code = HMD.MaTTDonGia;
            //minhnd 09/04/2015

            ctrNuocXuatXu.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrNuocXuatXu.Code = HMD.NuocXuatXu;
            ctrNuocXuatXu.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            //ctrNuocXuatXu.Code = HMD.NuocXuatXu;

            //minhnd 09/04/2015
            txtTriGiaTinhThue.TextChanged -= new EventHandler(txt_TextChanged);
            txtTriGiaTinhThue.Text = HMD.TriGiaTinhThue.ToString();
            txtTriGiaTinhThue.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaTTTriGiaTinhThue.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTTriGiaTinhThue.Code = HMD.MaTTTriGiaTinhThue;
            ctrMaTTTriGiaTinhThue.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //txtTriGiaTinhThue.Text = HMD.TriGiaTinhThue.ToString();
            //ctrMaTTTriGiaTinhThue.Code = HMD.MaTTTriGiaTinhThue;
            //duydp 03/12/2015
            if (HMD.SoTTDongHangTKTNTX == "0")
            {
                txtSoTTDongHangTKTNTX.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoTTDongHangTKTNTX.Text = String.Empty;
                txtSoTTDongHangTKTNTX.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoTTDongHangTKTNTX.Text = String.Empty;
            }
            else
            {
                txtSoTTDongHangTKTNTX.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoTTDongHangTKTNTX.Text = HMD.SoTTDongHangTKTNTX;
                txtSoTTDongHangTKTNTX.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoTTDongHangTKTNTX.Text = HMD.SoTTDongHangTKTNTX;
            }
            //duydp 03/12/2015
            txtSoDMMienThue.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoDMMienThue.Text = HMD.SoDMMienThue;
            txtSoDMMienThue.TextChanged += new EventHandler(txt_TextChanged);

            txtSoDongDMMienThue.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoDongDMMienThue.Text = HMD.SoDongDMMienThue;
            txtSoDongDMMienThue.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaVanBan1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaVanBan1.Code = HMD.MaVanBanPhapQuyKhac1;
            ctrMaVanBan1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaVanBan2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaVanBan2.Code = HMD.MaVanBanPhapQuyKhac2;
            ctrMaVanBan2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaVanBan3.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaVanBan3.Code = HMD.MaVanBanPhapQuyKhac3;
            ctrMaVanBan3.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaVanBan4.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaVanBan4.Code = HMD.MaVanBanPhapQuyKhac4;
            ctrMaVanBan4.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaVanBan5.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaVanBan5.Code = HMD.MaVanBanPhapQuyKhac5;
            ctrMaVanBan5.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //txtSoDongDMMienThue.Text = HMD.SoDMMienThue;
            //txtSoDMMienThue.Text = HMD.SoDongDMMienThue;
            //ctrMaVanBan1.Code = HMD.MaVanBanPhapQuyKhac1;
            //ctrMaVanBan2.Code = HMD.MaVanBanPhapQuyKhac2;
            //ctrMaVanBan3.Code = HMD.MaVanBanPhapQuyKhac3;
            //ctrMaVanBan4.Code = HMD.MaVanBanPhapQuyKhac4;
            //ctrMaVanBan5.Code = HMD.MaVanBanPhapQuyKhac5;


            txtTriGiaTinhThueS.TextChanged -= new EventHandler(txt_TextChanged);
            txtTriGiaTinhThueS.Text = HMD.TriGiaTinhThueS.ToString();
            txtTriGiaTinhThueS.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaTTTriGiaTinhThueS.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTTriGiaTinhThueS.Code = HMD.MaTTTriGiaTinhThueS;
            ctrMaTTTriGiaTinhThueS.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtDonGiaTinhThue.TextChanged -= new EventHandler(txt_TextChanged);
            txtDonGiaTinhThue.Text = HMD.DonGiaTinhThue.ToString();
            txtDonGiaTinhThue.TextChanged += new EventHandler(txt_TextChanged);

            txtDV_SL_TrongDonGiaTinhThue.TextChanged -= new EventHandler(txt_TextChanged);
            txtDV_SL_TrongDonGiaTinhThue.Text = HMD.DV_SL_TrongDonGiaTinhThue;
            txtDV_SL_TrongDonGiaTinhThue.TextChanged += new EventHandler(txt_TextChanged);

            //txtTriGiaTinhThueS.Text = HMD.TriGiaTinhThueS.ToString();
            //ctrMaTTTriGiaTinhThueS.Code = HMD.MaTTTriGiaTinhThueS;
            //txtDonGiaTinhThue.Text = HMD.DonGiaTinhThue.ToString();
            //txtDV_SL_TrongDonGiaTinhThue.Text = HMD.DV_SL_TrongDonGiaTinhThue;

            ctrMaTTSoTienThueXuatKhau.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTSoTienThueXuatKhau.Code = HMD.MaTTSoTienThueXuatKhau;
            ctrMaTTSoTienThueXuatKhau.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //txtSoTienThue.TextChanged -= new EventHandler(txt_TextChanged);
            //txtSoTienThue.Text = HMD.SoTienThue.ToString();
            //txtSoTienThue.TextChanged += new EventHandler(txt_TextChanged);

            //ctrMaTTSoTienThueXuatKhau.Code = HMD.MaTTSoTienThueXuatKhau;
            //txtSoTienThue.Text = HMD.SoTienThue.ToString();

            //txtThueSuatThueXuatKhau.Text = HMD.ThueSuatThue;

            txtPhanLoai_TS_ThueXuatKhau.TextChanged -= new EventHandler(txt_TextChanged);
            txtPhanLoai_TS_ThueXuatKhau.Text = HMD.PhanLoaiThueSuatThue;
            txtPhanLoai_TS_ThueXuatKhau.TextChanged += new EventHandler(txt_TextChanged);

            txtSoDongDMMienThue.TextChanged -= new EventHandler(txt_TextChanged);
            txtDieuKhoanMienGiam.Text = HMD.DieuKhoanMienGiam;
            txtDieuKhoanMienGiam.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaTTCuaDonGiaTinhThue.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTCuaDonGiaTinhThue.Code = HMD.MaTTDonGiaTinhThue;
            ctrMaTTCuaDonGiaTinhThue.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //txtPhanLoai_TS_ThueXuatKhau.Text = HMD.PhanLoaiThueSuatThue;
            //txtDieuKhoanMienGiam.Text = HMD.DieuKhoanMienGiam;
            //ctrMaTTCuaDonGiaTinhThue.Code = HMD.MaTTDonGiaTinhThue;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_VNACC_HangMauDich> ItemColl = new List<KDT_VNACC_HangMauDich>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACC_HangMauDich)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_HangMauDich item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TKMD.HangCollection.Remove(item);
                    }

                    grList.DataSource = TKMD.HangCollection;
                    try { grList.Refetch(); }
                    catch { grList.Refresh(); }

                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {

                if (!ValidateForm(false))
                    return;
                bool isAddNew = false;
                if (HMD == null)
                {
                    HMD = new KDT_VNACC_HangMauDich();
                    isAddNew = true;
                }
                GetHangMauDich();
#if GC_V4 ||SXXK_V4
                try
                {
                    if (!checkMaHang(HMD.MaHangHoa, loaiHangHoa))
                    {
                        MLMessages("Sản phẩm này chưa được khai báo định mức. Xin kiểm tra lại thông tin ", "MSG_PUB06", "", false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }



#endif
#if GC_V4
                //ctrMaMienGiamThue
                if (string.IsNullOrEmpty(HMD.MaMienGiamThue) || HMD.MaMienGiamThue.Trim() == "")
                {
                    this.ShowMessage("Dòng hàng chưa nhập Mã Miễn Giảm Thuế.", false);
                }
#endif


                if (isAddNew)
                    TKMD.HangCollection.Add(HMD);
                grList.DataSource = TKMD.HangCollection;
                grList.Refetch();
                HMD = new KDT_VNACC_HangMauDich();
                //SetHangMD();
                HMD = null;
                txtSoLuong1.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong1.Value = 0;
                txtSoLuong1.TextChanged += new EventHandler(txt_TextChanged);

                txtSoLuong2.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong2.Value = 0;
                txtSoLuong2.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoLuong1.Value = 0;

                this.SetChange(true);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }
        private bool checkMaHang(string MaHang, string loai)
        {
            bool ketqua = true;
            switch (loai)
            {
#if SXXK_V4
                case "NPL":
                    Company.BLL.KDT.ExportToExcel.NguyenPhuLieu Npl = new Company.BLL.KDT.ExportToExcel.NguyenPhuLieu();
                    Npl.Ma = MaHang;
                    Npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    Npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    if (Npl.Load())
                    {
                        ketqua = true;
                        //    Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy nplDangKy = new Company.BLL.KDT.SXXK.NguyenPhuLieuDangKy();
                        //    nplDangKy.ID = Npl.Master_ID;
                        //    nplDangKy.Load1();
                        //    if (nplDangKy.TrangThaiXuLy == 1)
                        //        ketqua = true;
                        //    else
                        //        ketqua = false;
                    }
                    else
                        ketqua = false;
                    break;
                case "SP":
                    Company.BLL.KDT.ExportToExcel.DinhMuc dinhmuc = new Company.BLL.KDT.ExportToExcel.DinhMuc();
                    //dinhmuc.MaSanPHam = MaHang;

                    if (dinhmuc.checkExitSanPham(MaHang, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI))
                    {
                        ketqua = true;
                        //Company.BLL.KDT.SXXK.DinhMucDangKy dmDangKy = new Company.BLL.KDT.SXXK.DinhMucDangKy();
                        //dmDangKy.ID = dinhmuc.Master_ID;
                        //dmDangKy.Load();
                        //if (dmDangKy.TrangThaiXuLy == 1)
                        //    ketqua = true;
                        //else
                        //    ketqua = false;
                    }
                    else
                        ketqua = false;
                    break;
#elif GC_V4
                case "SP":
                    Company.GC.BLL.GC.DinhMuc dinhmuc = new Company.GC.BLL.GC.DinhMuc();
                    dinhmuc.MaSanPham = MaHang;
                    dinhmuc.HopDong_ID = TKMD.HopDong_ID;
                    if (dinhmuc.CheckExitsDinhMucSanPham())
                    {
                        ketqua = true;
                    }
                    else
                        ketqua = false;
                    break;
               
#endif
            }
            return ketqua;
        }
        private void VNACC_HangMauDichXuatForm_Load(object sender, EventArgs e)
        {
            Caption = this.Text;

            grList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelection;
            SetIDControl();
            grList.DataSource = TKMD.HangCollection;

            ctrMaTTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTDonGia.Code = MaTienTe;
            ctrMaTTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //ctrMaTTDonGia.Code = MaTienTe;
#if GC_V4
            ctrMaMienGiamThue.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaMienGiamThue.Code = "XNG82";
            ctrMaMienGiamThue.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //ctrMaMienGiamThue.Code = "XNG82";
#endif
#if SXXK_V4
            //ctrMaMienGiamThue.Code = "XN210";
#endif

#if KD_V4

            txtMaHangHoa.Visible = false;
            lblMaHang.Visible = false;
            grList.RootTable.Columns["MaHangHoa"].Visible = false;
            grList.RootTable.Columns["MaQuanLyRieng"].Visible = true;
#else
            grList.RootTable.Columns["MaHangHoa"].Visible = true;
            grList.RootTable.Columns["MaQuanLyRieng"].Visible = false;
#endif
            if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
            {
                btnGhi.Enabled = false;
                btnXoa.Enabled = false;
            }
            SetMaxLengthControl();

            ValidateForm(true);
            txtThueSuat.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtThueSuat.Text == "0")
            {
                txtThueSuat.TextChanged -= new EventHandler(txt_TextChanged);
                txtThueSuat.Text = "";
                txtThueSuat.TextChanged += new EventHandler(txt_TextChanged);

                //txtThueSuat.Text = "";
            }
            txtThueSuatTuyetDoi.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtThueSuatTuyetDoi.Text == "0")
            {
                txtThueSuatTuyetDoi.TextChanged -= new EventHandler(txt_TextChanged);
                txtThueSuatTuyetDoi.Text = "";
                txtThueSuatTuyetDoi.TextChanged += new EventHandler(txt_TextChanged);

                //txtThueSuatTuyetDoi.Text = "";
            }
            txtTriGiaHoaDon.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtTriGiaHoaDon.Text == "0")
            {
                txtTriGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                txtTriGiaHoaDon.Text = "";
                txtTriGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                //txtTriGiaHoaDon.Text = "";
            }
            txtDonGiaHoaDon.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtDonGiaHoaDon.Text == "0")
            {
                txtDonGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                txtDonGiaHoaDon.Text = "";
                txtDonGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                //txtDonGiaHoaDon.Text = "";
            }

            SetAutoRemoveUnicodeAndUpperCaseControl();
        }

        private void grList_DoubleClick(object sender, EventArgs e)
        {

            if (grList.GetRows().Length < 0) return;
            HMD = (KDT_VNACC_HangMauDich)grList.CurrentRow.DataRow;
            SetHangMauDich();
            check();
        }
        private void check()
        {
            txtThueSuat.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtThueSuat.Text == "0")
            {
                txtThueSuat.TextChanged -= new EventHandler(txt_TextChanged);
                txtThueSuat.Text = "";
                txtThueSuat.TextChanged += new EventHandler(txt_TextChanged);

                //txtThueSuat.Text = "";
            }
            txtThueSuatTuyetDoi.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtThueSuatTuyetDoi.Text == "0")
            {
                txtThueSuatTuyetDoi.TextChanged -= new EventHandler(txt_TextChanged);
                txtThueSuatTuyetDoi.Text = "";
                txtThueSuatTuyetDoi.TextChanged += new EventHandler(txt_TextChanged);

                //txtThueSuatTuyetDoi.Text = "";
            }
            txtTriGiaHoaDon.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtTriGiaHoaDon.Text == "0")
            {
                txtTriGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                txtTriGiaHoaDon.Text = "";
                txtTriGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                //txtTriGiaHoaDon.Text = "";
            }
            txtDonGiaHoaDon.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtDonGiaHoaDon.Text == "0")
            {
                txtDonGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                txtDonGiaHoaDon.Text = "";
                txtDonGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                //txtDonGiaHoaDon.Text = "";
            }
            //txtSoDongDMMienThue. = NumericEditNullBehavior.AllowNull;
            if (txtSoDongDMMienThue.Text == "0")
            {
                txtSoDongDMMienThue.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoDongDMMienThue.Text = "";
                txtSoDongDMMienThue.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoDongDMMienThue.Text = "";
            }
            txtSoLuong2.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtSoLuong2.Text == "0")
            {
                txtSoLuong2.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong2.Text = "";
                txtSoLuong2.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoLuong2.Text = "";
            }
            txtSoTienMienGiam.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtSoTienMienGiam.Text == "0")
            {
                txtSoTienMienGiam.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoTienMienGiam.Text = "";
                txtSoTienMienGiam.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoTienMienGiam.Text = "";
            }
            txtTriGiaTinhThue.NullBehavior = NumericEditNullBehavior.AllowNull;
            if (txtTriGiaTinhThue.Text == "0")
            {
                txtTriGiaTinhThue.TextChanged -= new EventHandler(txt_TextChanged);
                txtTriGiaTinhThue.Text = "";
                txtTriGiaTinhThue.TextChanged += new EventHandler(txt_TextChanged);

                //txtTriGiaTinhThue.Text = "";
            }

        }
        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                ctrMaSoHang.TagName = "CMD"; //Mã số hàng hóa
                txtMaQuanLy.Tag = "COC"; //Mã quản lý riêng
                txtThueSuat.Tag = "TXA"; //Thuế suất
                ctrNuocXuatXu.TagCode = "OR"; //Mã nước xuất xứ
                txtThueSuatTuyetDoi.Tag = "TXB"; //Mức thuế tuyệt đối
                ctrMaDVTTuyetDoi.TagName = "TXC"; //Mã đơn vị tính thuế tuyệt đối
                ctrMaTTTuyetDoi.TagName = "TXD"; //Mã đồng tiền của mức thuế tuyệt đối
                txtTenHang.Tag = "CMN"; //Mô tả hàng hóa
                ctrMaMienGiamThue.TagName = "RE"; //Mã miễn / Giảm / Không chịu thuế xuất khẩu
                txtSoTienMienGiam.Tag = "REG"; //Số tiền giảm thuế xuất khẩu
                txtSoLuong1.Tag = "QN1"; //Số lượng (1)
                ctrDVTLuong1.TagName = "QT1"; //Mã đơn vị tính (1)
                txtSoLuong2.Tag = "QN2"; //Số lượng (2)
                ctrDVTLuong2.TagName = "QT2"; //Mã đơn vị tính (2)
                txtTriGiaHoaDon.Tag = "BPR"; //Trị giá hóa đơn
                ctrMaTTTriGiaTinhThue.TagName = "BPC"; //Mã đồng tiền của giá tính thuế
                txtTriGiaTinhThue.Tag = "DPR"; //Trị giá tính thuế
                txtDonGiaHoaDon.Tag = "UPR"; //Đơn giá hóa đơn
                ctrMaTTDonGia.TagName = "UPC"; //Mã đồng tiền của đơn giá hóa đơn
                ctrDVTDonGia.TagName = "TSC"; //Đơn vị của đơn giá hóa đơn và số lượng
                txtSoTTDongHangTKTNTX.Tag = "TDL"; //Số thứ tự của dòng hàng trên tờ khai tạm nhập tái xuất tương ứng
                txtSoDMMienThue.Tag = "TXN"; //Danh mục miễn thuế xuất khẩu
                txtSoDongDMMienThue.Tag = "TXR"; //Số dòng tương ứng trong danh mục miễn thuế
                ctrMaVanBan1.TagName = "OL_"; //Mã văn bản pháp quy khác
                ctrMaVanBan2.TagName = "OL_"; //Mã văn bản pháp quy khác
                ctrMaVanBan3.TagName = "OL_"; //Mã văn bản pháp quy khác
                ctrMaVanBan4.TagName = "OL_"; //Mã văn bản pháp quy khác
                ctrMaVanBan5.TagName = "OL_"; //Mã văn bản pháp quy khác


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                //ctrMaSoHang.MaxLength = 12; //Mã số hàng hóa
                txtMaQuanLy.MaxLength = 7; //Mã quản lý riêng
                //txtThueSuat.MaxLength = 10; //Thuế suất
                txtThueSuatTuyetDoi.MaxLength = 10; //Mức thuế tuyệt đối
                //ctrMaDVTTuyetDoi.MaxLength = 4; //Mã đơn vị tính thuế tuyệt đối
                //ctrMaTTTuyetDoi.MaxLength = 3; //Mã đồng tiền của mức thuế tuyệt đối
                txtTenHang.MaxLength = 200; //Mô tả hàng hóa
                //ctrMaMienGiamThue.MaxLength = 5; //Mã miễn / Giảm / Không chịu thuế xuất khẩu
                txtSoTienMienGiam.MaxLength = 16; //Số tiền giảm thuế xuất khẩu
                txtSoLuong1.MaxLength = 12; //Số lượng (1)
                //ctrDVTLuong1.MaxLength = 4; //Mã đơn vị tính (1)
                txtSoLuong2.MaxLength = 12; //Số lượng (2)
                //ctrDVTLuong2.MaxLength = 4; //Mã đơn vị tính (2)
                txtTriGiaHoaDon.MaxLength = 20; //Trị giá hóa đơn
                //ctrMaTTTriGiaTinhThue.MaxLength = 3; //Mã đồng tiền của giá tính thuế
                txtTriGiaTinhThue.MaxLength = 20; //Trị giá tính thuế
                txtDonGiaHoaDon.MaxLength = 9; //Đơn giá hóa đơn
                //ctrMaTTDonGia.MaxLength = 3; //Mã đồng tiền của đơn giá hóa đơn
                //ctrDVTDonGia.MaxLength = 4; //Đơn vị của đơn giá hóa đơn và số lượng
                txtSoTTDongHangTKTNTX.MaxLength = 2; //Số thứ tự của dòng hàng trên tờ khai tạm nhập tái xuất tương ứng
                txtSoDMMienThue.MaxLength = 12; //Danh mục miễn thuế xuất khẩu
                txtSoDongDMMienThue.MaxLength = 3; //Số dòng tương ứng trong danh mục miễn thuế
                //ctrSoMucKhaiKhoanDC.MaxLength = 2; //Mã văn bản pháp quy khác
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                //Đơn vị tính số lượng
                ctrDVTLuong1.SetValidate = !isOnlyWarning; ctrDVTLuong1.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrDVTLuong1.IsValidate;
                //Đồng tiền giá hóa đơn
                //ctrMaTTDonGia.SetValidate = !isOnlyWarning; ctrMaTTDonGia.IsOnlyWarning = isOnlyWarning;
                //isValid &= ctrMaTTDonGia.IsValidate;
                //Đơn vị tính đơn giá HD
                //ctrDVTDonGia.SetValidate = !isOnlyWarning; ctrDVTDonGia.IsOnlyWarning = isOnlyWarning;
                //isValid &= ctrDVTDonGia.IsValidate;
#if GC_V4 || SXXK_V4
                isValid &= ValidateControl.ValidateNull(txtMaHangHoa, errorProvider, "Mã hàng hoá");
#endif
                //isValid &= ValidateControl.ValidateNull(txtMaHangHoa, errorProvider, "Mã hàng hoá");
                isValid &= ValidateControl.ValidateNull(txtTenHang, errorProvider, "Tên hàng");
                isValid &= ValidateControl.ValidateNull(txtSoLuong1, errorProvider, "số lượng ", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            //ctrMaSoHang.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã số hàng hóa
            //txtMaQuanLy.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã quản lý riêng
            //txtThueSuat.TextChanged += new EventHandler(SetTextChanged_Handler); //Thuế suất
            //txtThueSuatTuyetDoi.TextChanged += new EventHandler(SetTextChanged_Handler); //Mức thuế tuyệt đối
            //txtMaDVTTuyetDoi.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đơn vị tính thuế tuyệt đối
            //ctrMaTTTuyetDoi.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đồng tiền của mức thuế tuyệt đối
            //txtTenHang.TextChanged += new EventHandler(SetTextChanged_Handler); //Mô tả hàng hóa
            //txtMaMienGiamThue.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã miễn / Giảm / Không chịu thuế xuất khẩu
            //txtSoTienMienGiam.TextChanged += new EventHandler(SetTextChanged_Handler); //Số tiền giảm thuế xuất khẩu
            //txtSoLuong1.TextChanged += new EventHandler(SetTextChanged_Handler); //Số lượng (1)
            //ctrDVTLuong1.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đơn vị tính (1)
            //txtSoLuong2.TextChanged += new EventHandler(SetTextChanged_Handler); //Số lượng (2)
            //ctrDVTLuong2.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đơn vị tính (2)
            //txtTriGiaHoaDon.TextChanged += new EventHandler(SetTextChanged_Handler); //Trị giá hóa đơn
            //ctrMaTTTriGiaTinhThue.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đồng tiền của giá tính thuế
            //txtTriGiaTinhThue.TextChanged += new EventHandler(SetTextChanged_Handler); //Trị giá tính thuế
            //txtDonGiaHoaDon.TextChanged += new EventHandler(SetTextChanged_Handler); //Đơn giá hóa đơn
            //ctrMaTTDonGia.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đồng tiền của đơn giá hóa đơn
            //txtDVTDonGia.TextChanged += new EventHandler(SetTextChanged_Handler); //Đơn vị của đơn giá hóa đơn và số lượng
            //txtSoTTDongHangTKTNTX.TextChanged += new EventHandler(SetTextChanged_Handler); //Số thứ tự của dòng hàng trên tờ khai tạm nhập tái xuất tương ứng
            //txtSoDMMienThue.TextChanged += new EventHandler(SetTextChanged_Handler); //Danh mục miễn thuế xuất khẩu
            //txtSoDongDMMienThue.TextChanged += new EventHandler(SetTextChanged_Handler); //Số dòng tương ứng trong danh mục miễn thuế
            ////txtSoMucKhaiKhoanDC.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã văn bản pháp quy khác

            //ctrMaSoHang.IsUpperCase = true; //Mã số hàng hóa
            //txtMaQuanLy.CharacterCasing = CharacterCasing.Upper; //Mã quản lý riêng
            //txtThueSuat.CharacterCasing = CharacterCasing.Upper; //Thuế suất
            //txtThueSuatTuyetDoi.CharacterCasing = CharacterCasing.Upper; //Mức thuế tuyệt đối
            //txtMaDVTTuyetDoi.CharacterCasing = CharacterCasing.Upper; //Mã đơn vị tính thuế tuyệt đối
            //ctrMaTTTuyetDoi.IsUpperCase = true; //Mã đồng tiền của mức thuế tuyệt đối
            //txtTenHang.CharacterCasing = CharacterCasing.Upper; //Mô tả hàng hóa
            //txtMaMienGiamThue.CharacterCasing = CharacterCasing.Upper; //Mã miễn / Giảm / Không chịu thuế xuất khẩu
            //txtSoTienMienGiam.CharacterCasing = CharacterCasing.Upper; //Số tiền giảm thuế xuất khẩu
            //txtSoLuong1.CharacterCasing = CharacterCasing.Upper; //Số lượng (1)
            //ctrDVTLuong1.IsUpperCase = true; //Mã đơn vị tính (1)
            //txtSoLuong2.CharacterCasing = CharacterCasing.Upper; //Số lượng (2)
            //ctrDVTLuong2.IsUpperCase = true; //Mã đơn vị tính (2)
            //txtTriGiaHoaDon.CharacterCasing = CharacterCasing.Upper; //Trị giá hóa đơn
            //ctrMaTTTriGiaTinhThue.IsUpperCase = true; //Mã đồng tiền của giá tính thuế
            //txtTriGiaTinhThue.CharacterCasing = CharacterCasing.Upper; //Trị giá tính thuế
            //txtDonGiaHoaDon.CharacterCasing = CharacterCasing.Upper; //Đơn giá hóa đơn
            //ctrMaTTDonGia.IsUpperCase = true; //Mã đồng tiền của đơn giá hóa đơn
            //txtDVTDonGia.CharacterCasing = CharacterCasing.Upper; //Đơn vị của đơn giá hóa đơn và số lượng
            //txtSoTTDongHangTKTNTX.CharacterCasing = CharacterCasing.Upper; //Số thứ tự của dòng hàng trên tờ khai tạm nhập tái xuất tương ứng
            //txtSoDMMienThue.CharacterCasing = CharacterCasing.Upper; //Danh mục miễn thuế xuất khẩu
            //txtSoDongDMMienThue.CharacterCasing = CharacterCasing.Upper; //Số dòng tương ứng trong danh mục miễn thuế
            ////txtSoMucKhaiKhoanDC.CharacterCasing = CharacterCasing.Upper; //Mã văn bản pháp quy khác
        }

        private void txtTriGiaHoaDon_Leave(object sender, EventArgs e)
        {
            if (GlobalSettings.TinhThueTGNT == "1")
            {
                this.soLuong = Convert.ToDouble(txtSoLuong1.Value);
                if (soLuong != 0)
                {
                    this.triGiaHD = Convert.ToDouble(txtTriGiaHoaDon.Value);
#if KD_V4
                    txtDonGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.SoThapPhan.DonGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtDonGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.SoThapPhan.DonGiaNT, Thread.CurrentThread.CurrentCulture);
#elif SXXK_V4||GC_V4
                    txtDonGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.DonGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtDonGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.DonGiaNT, Thread.CurrentThread.CurrentCulture);
#endif
                }
                ctrMaTTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaTTDonGia.Code = this.MaTienTe;
                ctrMaTTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                //ctrMaTTDonGia.Code = this.MaTienTe;
            }
        }

        private void txtDonGiaHoaDon_Leave(object sender, EventArgs e)
        {
            if (GlobalSettings.TinhThueTGNT == "0")
            {
                this.soLuong = Convert.ToDouble(txtSoLuong1.Value);
                if (soLuong != 0)
                {
                    this.donGiaHD = Convert.ToDouble(txtDonGiaHoaDon.Value);
#if KD_V4
                    txtTriGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.SoThapPhan.TriGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtTriGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.SoThapPhan.TriGiaNT, Thread.CurrentThread.CurrentCulture);
#elif SXXK_V4||GC_V4
                    txtTriGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.TriGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtTriGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.TriGiaNT, Thread.CurrentThread.CurrentCulture);
#endif
                }
                ctrMaTTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaTTDonGia.Code = this.MaTienTe;
                ctrMaTTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                //ctrMaTTDonGia.Code = this.MaTienTe;
            }
        }

        private void txtSoLuong1_Leave(object sender, EventArgs e)
        {
            this.soLuong = Convert.ToDouble(txtSoLuong1.Value);
            if (soLuong != 0)
            {
                if (GlobalSettings.TinhThueTGNT == "1")
                {
                    this.triGiaHD = Convert.ToDouble(txtTriGiaHoaDon.Value);
#if KD_V4
                    txtDonGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.SoThapPhan.DonGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtDonGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.SoThapPhan.DonGiaNT, Thread.CurrentThread.CurrentCulture);
#elif SXXK_V4||GC_V4
                    txtDonGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.DonGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtDonGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtDonGiaHoaDon.Value = Helpers.FormatNumeric(this.triGiaHD / soLuong, GlobalSettings.DonGiaNT, Thread.CurrentThread.CurrentCulture);
#endif
                }
                if (GlobalSettings.TinhThueTGNT == "0")
                {
                    this.donGiaHD = Convert.ToDouble(txtDonGiaHoaDon.Value);
#if KD_V4
                    txtTriGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.SoThapPhan.TriGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtTriGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.SoThapPhan.TriGiaNT, Thread.CurrentThread.CurrentCulture);
#elif SXXK_V4||GC_V4
                    txtTriGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.TriGiaNT, Thread.CurrentThread.CurrentCulture);
                    txtTriGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                    //txtTriGiaHoaDon.Value = Helpers.FormatNumeric(this.donGiaHD * soLuong, GlobalSettings.TriGiaNT, Thread.CurrentThread.CurrentCulture);
#endif
                }
                ctrMaTTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaTTDonGia.Code = this.MaTienTe;
                ctrMaTTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                txtSoLuong2.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong2.Text = txtSoLuong1.Text;
                txtSoLuong2.TextChanged += new EventHandler(txt_TextChanged);
                //ctrMaTTDonGia.Code = this.MaTienTe;
            }
            else
            {
                ShowMessage("Số lượng phải lớn 0", false);
                txtSoLuong1.Focus();
            }
        }

        private void ctrDVTLuong1_Leave(object sender, EventArgs e)
        {
            ctrDVTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrDVTDonGia.Code = ctrDVTLuong1.Code;
            ctrDVTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrDVTLuong2.Code = ctrDVTLuong1.Code;
            ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            //ctrDVTDonGia.Code = ctrDVTLuong1.Code;
        }

        private void ctrDVTDonGia_Leave(object sender, EventArgs e)
        {
            ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrDVTLuong1.Code = ctrDVTDonGia.Code;
            ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //ctrDVTLuong1.Code = ctrDVTDonGia.Code;
        }

        private void txtMaQuanLy_ButtonClick(object sender, EventArgs e)
        {
            string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;
            //Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(MaHaiQuan, "MaHQ"); //MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
#if SXXK_V4

            switch (this.loaiHangHoa)
            {
                case "N":
                    if (this.NPLRegistedForm == null)
                        this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                    this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                    this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; ;
                    this.NPLRegistedForm.isBrowser = true;
                    this.NPLRegistedForm.ShowDialog(this);
                    if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
                    {
                        txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                        txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                        txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                        ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrMaSoHang.Code = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong2.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        //txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        //txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        //ctrMaSoHang.Code = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        //ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                    }
                    txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaQuanLy.Text = "1" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

                    //txtMaQuanLy.Text = "1" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    break;
                case "T":
                    if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                    {
                        if (this.NPLRegistedForm_CX == null)
                            this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
                        this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                        this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        this.NPLRegistedForm_CX.LoaiNPL = "3";
                        this.NPLRegistedForm_CX.ShowDialog(this);
                        if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                        {
                            txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                            txtMaHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                            txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                            txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                            txtTenHang.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
                            txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                            ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                            ctrMaSoHang.Code = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                            ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                            ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                            ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                            ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                            ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                            ctrDVTLuong2.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                            ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                            //txtMaHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                            //txtTenHang.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
                            //ctrMaSoHang.Code = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                            //ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                        }
                    }
                    txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaQuanLy.Text = "3" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

                    //txtMaQuanLy.Text = "3" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    break;
                case "S":

                    if (this.SPRegistedForm == null)
                        this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.CalledForm = "HangMauDichForm";
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SPRegistedForm.ShowDialog(this);
                    if (this.SPRegistedForm.SanPhamSelected.Ma != "" && this.SPRegistedForm.SanPhamSelected != null)
                    {
                        txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                        txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                        txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                        ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrMaSoHang.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                        ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong1.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong2.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        //txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        //txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        //ctrMaSoHang.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                        //ctrDVTLuong1.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                    }
                    txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaQuanLy.Text = "2" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

                    //txtMaQuanLy.Text = "2" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    break;
            }
#elif GC_V4

            switch (this.loaiHangHoa)
            {
                case "N":
                    //if (this.NPLRegistedForm == null)
                    this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                    this.NPLRegistedForm.isBrower = true;
                    this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.NPLRegistedForm.NguyenPhuLieuSelected.HopDong_ID = TKMD.HopDong_ID;
                    this.NPLRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.NPLRegistedForm.NguyenPhuLieuSelected.Ma))
                    {
                        txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                        txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                        txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                        ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrMaSoHang.Code = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong2.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        //txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        //txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        //ctrMaSoHang.Code = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        //ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                    }
                    txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaQuanLy.Text = "1" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

                    //txtMaQuanLy.Text = "1" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    break;
                case "S":
                    //if (this.SPRegistedForm == null)
                    this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SPRegistedForm.SanPhamSelected.HopDong_ID = TKMD.HopDong_ID;
                    this.SPRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                    {
                        txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                        txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                        txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                        ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrMaSoHang.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                        ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong1.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong2.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        //txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        //txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        //ctrMaSoHang.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                        //ctrDVTLuong1.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                    }
                    txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaQuanLy.Text = "2" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

                    //txtMaQuanLy.Text = "2" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    break;
                case "T":
                    ThietBiRegistedForm ftb = new ThietBiRegistedForm();
                    ftb.ThietBiSelected.HopDong_ID = TKMD.HopDong_ID;
                    ftb.isBrower = true;
                    ftb.ShowDialog();
                    if (!string.IsNullOrEmpty(ftb.ThietBiSelected.Ma))
                    {
                        txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMaHangHoa.Text = ftb.ThietBiSelected.Ma;
                        txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                        txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                        txtTenHang.Text = ftb.ThietBiSelected.Ten;
                        txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                        ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrMaSoHang.Code = ftb.ThietBiSelected.MaHS;
                        ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong1.Code = this.DVT_VNACC(ftb.ThietBiSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong2.Code = this.DVT_VNACC(ftb.ThietBiSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        //txtMaHangHoa.Text = ftb.ThietBiSelected.Ma;
                        //txtTenHang.Text = ftb.ThietBiSelected.Ten;
                        //ctrMaSoHang.Code = ftb.ThietBiSelected.MaHS;
                        //ctrDVTLuong1.Code = this.DVT_VNACC(ftb.ThietBiSelected.DVT_ID.PadRight(3));
                    }
                    txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaQuanLy.Text = "3" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

                    //txtMaQuanLy.Text = "3" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    break;
                case "H":
                    HangMauRegistedForm hangmauForm = new HangMauRegistedForm();
                    hangmauForm.isBrower = true;
                    hangmauForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    hangmauForm.HangMauSelected.HopDong_ID = TKMD.HopDong_ID;
                    hangmauForm.ShowDialog();
                    if (!string.IsNullOrEmpty(hangmauForm.HangMauSelected.Ma))
                    {
                        txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                        txtMaHangHoa.Text = hangmauForm.HangMauSelected.Ma;
                        txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                        txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                        txtTenHang.Text = hangmauForm.HangMauSelected.Ten;
                        txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                        ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrMaSoHang.Code = hangmauForm.HangMauSelected.MaHS;
                        ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong1.Code = this.DVT_VNACC(hangmauForm.HangMauSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                        ctrDVTLuong2.Code = this.DVT_VNACC(hangmauForm.HangMauSelected.DVT_ID.PadRight(3));
                        ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                        //txtMaHangHoa.Text = hangmauForm.HangMauSelected.Ma;
                        //txtTenHang.Text = hangmauForm.HangMauSelected.Ten;
                        //ctrMaSoHang.Code = hangmauForm.HangMauSelected.MaHS;
                        //ctrDVTLuong1.Code = this.DVT_VNACC(hangmauForm.HangMauSelected.DVT_ID.PadRight(3));
                    }
                    txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaQuanLy.Text = "4" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

                    //txtMaQuanLy.Text = "4" + ((TKMD != null && TKMD.CoQuanHaiQuan != null) ? TKMD.CoQuanHaiQuan.Trim() : MaHQVNACC);
                    break;
            }
#elif KD_V4
#endif

        }

        private void txtMaHangHoa_Leave(object sender, EventArgs e)
        {
#if GC_V4
            // reSetError();
            bool isNotFound = false;
            if (this.loaiHangHoa == "N")
            {

                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.HopDong_ID = TKMD.HopDong_ID;
                npl.Ma = txtMaHangHoa.Text.Trim();
                if (npl.Load())
                {
                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = npl.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = npl.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = npl.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    //txtMaHangHoa.Text = npl.Ma;
                    ////if (ctrMaSoHang.Code.Trim().Length == 0)
                    //ctrMaSoHang.Code = npl.MaHS;
                    //txtTenHang.Text = npl.Ten;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));
                    //cbDonViTinh.SelectedValue = npl.DVT_ID;
                    //epError.SetError(txtMaHang, null);
                    //epError.SetError(txtTenHang, null);
                    //epError.SetError(txtMaHS, null);

                }
                else
                {
                    isNotFound = true;
                }
            }
            else if (this.loaiHangHoa == "S")
            {
                SanPham sp = new SanPham();
                sp.HopDong_ID = TKMD.HopDong_ID;
                sp.Ma = txtMaHangHoa.Text.Trim();
                if (sp.Load())
                {
                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = sp.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = sp.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = sp.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    //txtMaHangHoa.Text = sp.Ma;
                    ////if (ctrMaSoHang.Code.Trim().Length == 0)
                    //    ctrMaSoHang.Code = sp.MaHS;
                    //txtTenHang.Text = sp.Ten;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));
                }
                else
                {
                    isNotFound = true;
                }
            }
            else if (this.loaiHangHoa == "T")
            {
                ThietBi tb = new ThietBi();
                tb.HopDong_ID = TKMD.HopDong_ID;
                tb.Ma = txtMaHangHoa.Text.Trim();
                if (tb.Load())
                {
                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = tb.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = tb.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = tb.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(tb.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(tb.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    //txtMaHangHoa.Text = tb.Ma;
                    ////if (ctrMaSoHang.Code.Trim().Length == 0)
                    //    ctrMaSoHang.Code = tb.MaHS;
                    //txtTenHang.Text = tb.Ten;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(tb.DVT_ID.PadRight(3));
                }
                else
                {
                    isNotFound = true;
                }
            }
            else if (this.loaiHangHoa == "H")
            {
                HangMau hangmau = HangMau.Load(TKMD.HopDong_ID, txtMaHangHoa.Text.Trim());
                if (hangmau != null)
                {
                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = hangmau.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = hangmau.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = hangmau.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(hangmau.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(hangmau.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    //txtMaHangHoa.Text = hangmau.Ma;
                    ////if (ctrMaSoHang.Code.Trim().Length == 0)
                    //    ctrMaSoHang.Code = hangmau.MaHS;
                    //txtTenHang.Text = hangmau.Ten;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(hangmau.DVT_ID.PadRight(3));
                }
                else isNotFound = true;

            }
            if (isNotFound)
            {

                txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtTenHang.Text = string.Empty;
                txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrMaSoHang.Code = string.Empty;
                ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                //txtTenHang.Text = ctrMaSoHang.Code = string.Empty;
                return;
            }
#elif SXXK_V4
            if (loaiHangHoa == "N")
            {
                Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.Ma = txtMaHangHoa.Text;
                if (npl.Load())
                {
                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = npl.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = npl.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = npl.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    //txtMaHangHoa.Text = npl.Ma;
                    ////if (ctrMaSoHang.Code.Trim().Length == 0)
                    //ctrMaSoHang.Code = npl.MaHS;
                    //txtTenHang.Text = npl.Ten;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));

                }


            }
            else if (loaiHangHoa == "S")
            {
                Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = txtMaHangHoa.Text;
                sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (sp.Load())
                {
                    txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaHangHoa.Text = sp.Ma;
                    txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenHang.Text = sp.Ten;
                    txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                    ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrMaSoHang.Code = sp.MaHS;
                    ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong1.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));
                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                    ctrDVTLuong2.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));
                    ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                    //txtMaHangHoa.Text = sp.Ma;
                    ////if (ctrMaSoHang.Code.Trim().Length == 0)
                    //ctrMaSoHang.Code = sp.MaHS;
                    //txtTenHang.Text = sp.Ten;
                    //ctrDVTLuong1.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));

                }
            }
#endif
        }
        public long IdTKTamNhapTX;
        private void txtSoTTDongHangTKTNTX_ButtonClick(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;

                if (IdTKTamNhapTX > 0)
                {
                    VNACC_ListHangHMD listForm = new VNACC_ListHangHMD(Company.Interface.VNACCS.VNACC_ListHangHMD.SelectTion.SingleSelect, new long[] { IdTKTamNhapTX });
                    listForm.ShowDialog();
                    if (listForm.DialogResult == DialogResult.OK)
                    {
                        if (listForm.HangMD != null)
                        {
                            KDT_VNACC_HangMauDich hmdTNTX = listForm.HangMD;

                            ctrMaSoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                            ctrMaSoHang.Code = hmdTNTX.MaSoHang;
                            ctrMaSoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                            txtMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                            txtMaHangHoa.Text = hmdTNTX.MaHangHoa;
                            txtMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                            txtMaQuanLy.TextChanged -= new EventHandler(txt_TextChanged);
                            txtMaQuanLy.Text = hmdTNTX.MaQuanLy;
                            txtMaQuanLy.TextChanged += new EventHandler(txt_TextChanged);

                            txtTenHang.TextChanged -= new EventHandler(txt_TextChanged);
                            txtTenHang.Text = hmdTNTX.TenHang;
                            txtTenHang.TextChanged += new EventHandler(txt_TextChanged);

                            txtSoLuong1.TextChanged -= new EventHandler(txt_TextChanged);
                            txtSoLuong1.Text = hmdTNTX.SoLuong1.ToString();
                            txtSoLuong1.TextChanged += new EventHandler(txt_TextChanged);

                            ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                            ctrDVTLuong1.Code = hmdTNTX.DVTLuong1;
                            ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                            txtSoLuong2.TextChanged -= new EventHandler(txt_TextChanged);
                            txtSoLuong2.Text = hmdTNTX.SoLuong2.ToString();
                            txtSoLuong2.TextChanged += new EventHandler(txt_TextChanged);

                            ctrDVTLuong2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                            ctrDVTLuong2.Code = hmdTNTX.DVTLuong2;
                            ctrDVTLuong2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                            txtTriGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                            txtTriGiaHoaDon.Text = hmdTNTX.TriGiaHoaDon.ToString();
                            txtTriGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                            txtDonGiaHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                            txtDonGiaHoaDon.Text = hmdTNTX.DonGiaHoaDon.ToString();
                            txtDonGiaHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                            ctrDVTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                            ctrDVTDonGia.Code = hmdTNTX.DVTDonGia;
                            ctrDVTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                            ctrMaTTDonGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                            ctrMaTTDonGia.Code = hmdTNTX.MaTTDonGia;
                            ctrMaTTDonGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                            txtSoTTDongHangTKTNTX.TextChanged -= new EventHandler(txt_TextChanged);
                            txtSoTTDongHangTKTNTX.Text = hmdTNTX.SoDong.Length == 1 ? "0" + hmdTNTX.SoDong : hmdTNTX.SoDong;
                            txtSoTTDongHangTKTNTX.TextChanged += new EventHandler(txt_TextChanged);

                            ctrNuocXuatXu.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
                            ctrNuocXuatXu.Code = hmdTNTX.NuocXuatXu;
                            ctrNuocXuatXu.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

                            //ctrMaSoHang.Code = hmdTNTX.MaSoHang;
                            //txtMaQuanLy.Text = hmdTNTX.MaQuanLy;
                            //txtMaHangHoa.Text = hmdTNTX.MaHangHoa;
                            //txtTenHang.Text = hmdTNTX.TenHang;
                            //txtSoLuong1.Text = hmdTNTX.SoLuong1.ToString();
                            //ctrDVTLuong1.Code = hmdTNTX.DVTLuong1;
                            //txtSoLuong2.Text = hmdTNTX.SoLuong2.ToString();
                            //ctrDVTLuong2.Code = hmdTNTX.DVTLuong2;
                            //txtTriGiaHoaDon.Text = hmdTNTX.TriGiaHoaDon.ToString();
                            //txtDonGiaHoaDon.Text = hmdTNTX.DonGiaHoaDon.ToString();
                            //ctrDVTDonGia.Code = hmdTNTX.DVTDonGia;
                            //ctrMaTTDonGia.Code = hmdTNTX.MaTTDonGia;
                            //txtSoTTDongHangTKTNTX.Text = hmdTNTX.SoDong.Length == 1 ? "0" + hmdTNTX.SoDong : hmdTNTX.SoDong;

                        }
                    }



                }
                else
                {
                    this.ShowMessage("Bạn chưa chọn tờ khai tạm nhập hoặc tờ khai tạm nhập không tồn tại", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void grList_DeletingRecords(object sender, CancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }
        private void CopyDanhMuc_LinkkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (KDT_VNACC_HangMauDich item in this.TKMD.HangCollection)
            {
                item.SoDMMienThue = txtSoDMMienThue.Text;
            }
            this.SetChange(true);
            this.ShowMessage("Copy thành công", false);
        }
        private void CopyMienGiam_LinkkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (KDT_VNACC_HangMauDich item in this.TKMD.HangCollection)
            {
                item.MaMienGiamThue = ctrMaMienGiamThue.Code;
                item.DieuKhoanMienGiam = txtDieuKhoanMienGiam.Text;
            }
            this.SetChange(true);
            this.ShowMessage("Copy thành công", false);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (KDT_VNACC_HangMauDich item in this.TKMD.HangCollection)
            {
                item.MaQuanLy = txtMaQuanLy.Text.Trim();
            }
            this.SetChange(true);
            this.ShowMessage("Copy thành công", false);
        }
        private void btnDVT1_Click(object sender, EventArgs e)
        {
            MapperForm f = new MapperForm();
            f.TKMD = this.TKMD;
            f.LoaiMapper = "DVT";
            f.ShowDialog();
            if (f.DialogResult == DialogResult.OK)
            {
                ctrDVTLuong1.Code = f.mapper.CodeV5.Trim();
            }
        }

        private void btnDVT2_Click(object sender, EventArgs e)
        {
            MapperForm f = new MapperForm();
            f.TKMD = this.TKMD;
            f.LoaiMapper = "DVT";
            f.ShowDialog();
            if (f.DialogResult == DialogResult.OK)
            {
                ctrDVTLuong2.Code = f.mapper.CodeV5.Trim();
            }
        }

        private void linkMaTTCopy_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (KDT_VNACC_HangMauDich item in this.TKMD.HangCollection)
            {
                item.MaTTDonGia = ctrMaTTDonGia.Code;
            }
            this.SetChange(true);
            this.ShowMessage("Copy thành công", false);
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {

                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.RestoreDirectory = true;
                sfNPL.InitialDirectory = Application.StartupPath;
                sfNPL.Filter = "Excel files| *.xls";
                sfNPL.FileName = "DanhSachHangHoaTKHQXK_" + TKMD.SoToKhai + "_" + TKMD.MaLoaiHinh + ".xls";
                sfNPL.ShowDialog();
                if (sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporterNPL = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporterNPL.GridEX = grList;
                    Stream str = sfNPL.OpenFile();
                    gridEXExporterNPL.Export(str);
                    str.Close();
                    //if (showMsg("MSG_MAL08", true) == "Yes")
                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void linkLabel2_Click(object sender, EventArgs e)
        {
            Company.Interface.CauHinhToKhaiForm f = new Company.Interface.CauHinhToKhaiForm();
            f.Listlable.Add(f.lblPhuongThucTT);
            f.ShowDialog();
            this.txtDonGiaHoaDon.Focus();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (KDT_VNACC_HangMauDich item in this.TKMD.HangCollection)
            {
                item.NuocXuatXu = ctrNuocXuatXu.Code;
            }
            this.SetChange(true);
            this.ShowMessage("Copy thành công", false);
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (KDT_VNACC_HangMauDich item in this.TKMD.HangCollection)
            {
                item.DVTLuong2 = string.Empty;
                item.SoLuong2 = 0;
            }
            this.ShowMessage("Xoá thành công", false);
        }

        private void ctrMaMienGiamThue_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ctrMaMienGiamThue.Code))
            {
                try
                {
                    List<VNACC_Category_Common> result = VNACC_Category_Common.SelectCollectionDynamic("ReferenceDB='A520' AND Code='" + ctrMaMienGiamThue.Code.Trim() + "'", "ID");
                    if (result.Count >= 1)
                    {
                        txtDieuKhoanMienGiam.Text = result[0].Name_VN;
                    }
                    else
                    {
                        txtDieuKhoanMienGiam.Text = String.Empty;
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }

        private void linkSuaSoLuong_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            VNACC_DieuChinhHangMauDichForm f = new VNACC_DieuChinhHangMauDichForm();
            f.TKMD = TKMD;
            f.ShowDialog(this);
            grList.DataSource = TKMD.HangCollection;
            grList.Refetch();
        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (KDT_VNACC_HangMauDich item in this.TKMD.HangCollection)
            {
                item.SoLuong2 = item.SoLuong1;
                item.DVTLuong2 = item.DVTLuong1;
            }
            this.SetChange(true);
            this.ShowMessage("Coppy thành công", false);
        }

        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(ctrMaSoHang.Code.ToString()))
                {
                    System.Diagnostics.Process.Start("https://caselaw.vn/chi-tiet-ma-hs/" + ctrMaSoHang.Code.ToString() + "");
                }
                else
                {
                    ShowMessageTQDT("Doanh nghiệp chưa chọn Mã HS .", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
