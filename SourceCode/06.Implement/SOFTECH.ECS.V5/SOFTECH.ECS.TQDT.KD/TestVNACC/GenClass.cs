﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestVNACC
{
    public class GenClass
    {
        public StringBuilder VnaccsClass { get; set; }
        public StringBuilder EnumClass { get; set; }
        public string ClassName { get; set; }
        private List<string> listGroup;
        private int TongSoByte = 0;
        public GenClass(string className, string[] strLines)
        {
            this.TongSoByte = 0;
            this.listGroup = new List<string>();
            this.ClassName = className.ToUpper().Trim();
            this.VnaccsClass = new StringBuilder();
            this.VnaccsClass.AppendLine(@"using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
");
            this.VnaccsClass.AppendLine(@"public partial class " + this.ClassName + @" : BasicVNACC
    {");
            this.VnaccsClass.Append(AddProperties(strLines)).AppendLine();
            this.VnaccsClass.Append(BuildClass(strLines)).AppendLine();
            this.VnaccsClass.Append("}");
            this.VnaccsClass.Append(this.EnumClass).AppendLine();
            this.VnaccsClass.Append("}");
        }
        public StringBuilder AddProperties(string[] strlines)
        {
            StringBuilder PropertiesVnaccs = new StringBuilder();
            string groupID = string.Empty;
            PropertiesVnaccs.AppendLine("public static int TongSoByte { get; set; }");
            for (int i = 0; i < strlines.Length; i++)
            {
                string str = strlines[i];
                if (string.IsNullOrEmpty(str))
                {
                    continue;
                }
                else
                {
                    string[] pts = str.Split('-');

                    try
                    {

                        if (pts.Length == 3)
                        {
                            int byteProperty = 0;
                            if (pts[2].ToString().Contains(','))
                            {
                                if (pts[1].Contains("v"))
                                    byteProperty = System.Convert.ToInt32(pts[2].Split(',')[0]) * 3;
                                else
                                    byteProperty = (System.Convert.ToInt32(pts[2].Split(',')[0]));
                            }
                            else
                            {
                                if (pts[1].Contains("v"))
                                    byteProperty = System.Convert.ToInt32(pts[2]) * 3;
                                else
                                    byteProperty = System.Convert.ToInt32(pts[2]);
                            }
                            TongSoByte = TongSoByte + byteProperty + 2;
                            PropertiesVnaccs.AppendLine("public PropertiesAttribute " + pts[0].Trim().ToUpper() + " { get; set; }");
                        }
                        else if (pts.Length == 5)
                        {
                            int byteProperty = 0;

                            if (pts[4].ToString().Contains(','))
                            {
                                if (pts[3].Contains("v"))
                                    byteProperty = ((System.Convert.ToInt32(pts[4].Split(',')[0])) * 3 + 2) * (System.Convert.ToInt32(pts[1]));
                                else
                                    byteProperty = (System.Convert.ToInt32(pts[4].Split(',')[0]) + 2) * System.Convert.ToInt32(pts[1]);
                                //TongSoByte = TongSoByte + ((System.Convert.ToInt32(pts[4].Split(',')[0]) + 2) * System.Convert.ToInt32(pts[1]));
                            }
                            else
                            {
                                if (pts[3].Contains("v"))
                                    byteProperty = ((System.Convert.ToInt32(pts[4]) * 3) + 2) * (System.Convert.ToInt32(pts[1]));
                                else
                                    byteProperty = (System.Convert.ToInt32(pts[4]) + 2) * System.Convert.ToInt32(pts[1]);
                                //TongSoByte = TongSoByte + ((System.Convert.ToInt32(pts[4]) + 2) * System.Convert.ToInt32(pts[1]));
                            }
                            TongSoByte = TongSoByte + byteProperty;
                            if (groupID == pts[0].Trim())
                                continue;
                            else
                            {

                                groupID = pts[0].Trim();
                                listGroup.Add(groupID);
                                PropertiesVnaccs.AppendLine("public GroupAttribute " + pts[0].Trim().ToUpper() + " { get; set; }");
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                    catch (System.Exception ex)
                    {
                        string error = string.Empty;
                        if (pts.Length == 5)
                            error = pts[2];
                        else
                            error = pts[0];

                        Logger.LocalLogger.Instance().WriteMessage(ex.Message + error.ToString().ToUpper(), ex);
                        throw ex;
                    }
                }
            }
            return PropertiesVnaccs;
        }
        public StringBuilder BuildClass(string[] strlines)
        {
            StringBuilder BuildClass = new StringBuilder();
            BuildEnum();
            BuildClass.AppendLine(@"public "+ this.ClassName+@"()
        {");

            for (int i = 0; i < strlines.Length; i++)
            {
                string str = strlines[i];
                if (string.IsNullOrEmpty(str))
                {
                    continue;
                }
                else
                {
                    string[] pts = str.Split('-');
                    if (pts.Length == 3)
                    {
                        if (pts[1].Trim().ToUpper() == "N")
                        {
                            if (pts[2].Contains(','))
                            {
                                string[] formatnumber = pts[2].Split(',');
                                BuildClass.AppendLine(string.Format(@"{0} = new PropertiesAttribute({1}, typeof(decimal),{2});", pts[0].Trim().ToUpper(), formatnumber[0].Trim(), formatnumber[1].Trim()));
                            }
                            else
                                BuildClass.AppendLine(string.Format(@"{0} = new PropertiesAttribute({1}, typeof(decimal));", pts[0].Trim().ToUpper(), pts[2].Trim()));
                        }
                        else if (pts[1].Trim().ToUpper() == "D")
                        {
                            BuildClass.AppendLine(string.Format(@"{0} = new PropertiesAttribute({1}, typeof(DateTime));", pts[0].Trim().ToUpper(), pts[2].Trim()));
                        }
                        else if (pts[1].Trim().ToUpper() == "AN")
                        {
                            BuildClass.AppendLine(string.Format(@"{0} = new PropertiesAttribute({1}, typeof(string));", pts[0].Trim().ToUpper(), pts[2].Trim()));
                        }
                        else if (pts[1].Trim().ToUpper() == "V")
                        {
                            BuildClass.AppendLine(string.Format(@"{0} = new PropertiesAttribute({1}, typeof(string));", pts[0].Trim().ToUpper(), (Convert.ToInt16(pts[2].Trim()) * 3)));
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            foreach (var group in listGroup)
            {
                string loop = string.Empty;
                BuildClass.AppendLine(string.Format("#region {0}", group));
                BuildClass.AppendLine(string.Format(@"List<PropertiesAttribute> list{0} = new List<PropertiesAttribute>();",group));
                foreach (string item in strlines)
                {
                    string[] pts = item.Split('-');
                    //loop = pts[1].Trim();
                    if (pts.Length == 5 && pts[0].Trim().ToUpper() == group.Trim().ToUpper())
                    {
                        loop = pts[1].Trim();
                        AddEnum(this.ClassName + "_" + pts[2].Trim());
                        if (pts[3].Trim().ToUpper() == "N")
                        {
                            if (pts[4].Contains(','))
                            {
                                string[] formatnumber = pts[4].Split(',');
                                BuildClass.AppendLine(@"list" + group + ".Add(new PropertiesAttribute(" + formatnumber[0].Trim() + ", " + loop + ", EnumGroupID." + this.ClassName + "_" + pts[2].Trim() + ", typeof(decimal)," + formatnumber[1].Trim() + "));");
                            }
                            else
                                BuildClass.AppendLine(@"list" + group + ".Add(new PropertiesAttribute(" + pts[4].Trim() + ", " + loop + ", EnumGroupID." + this.ClassName + "_" + pts[2].Trim() + ", typeof(int)));");
                        }
                        else if (pts[3].Trim().ToUpper() == "D")
                        {
                            BuildClass.AppendLine(@"list" + group + ".Add(new PropertiesAttribute(" + pts[4].Trim() + ", " + loop + ", EnumGroupID." + this.ClassName + "_" + pts[2].Trim() + ", typeof(DateTime)));");
                        }
                        else if (pts[3].Trim().ToUpper() == "AN")
                        {
                            BuildClass.AppendLine(@"list" + group + ".Add(new PropertiesAttribute(" + pts[4].Trim() + ", " + loop + ", EnumGroupID." + this.ClassName + "_" + pts[2].Trim() + ", typeof(string)));");
                        }
                        else if (pts[3].Trim().ToUpper() == "V")
                        {
                            BuildClass.AppendLine(@"list" + group + ".Add(new PropertiesAttribute(" + (Convert.ToInt16(pts[4].Trim()) * 3) + ", " + loop + ", EnumGroupID." + this.ClassName + "_" + pts[2].Trim() + ", typeof(string)));");
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                        continue;
                }
                BuildClass.AppendLine(string.Format(@"{0} = new GroupAttribute(""{0}"", {1}, list{0});", group, loop));
                BuildClass.AppendLine(string.Format("#endregion {0}", group));
            }
            BuildClass.AppendLine(string.Format("TongSoByte = {0};",TongSoByte));
            BuildClass.AppendLine("}");
            EndEnum();
            return BuildClass;
        }

        #region create Enum
        public void BuildEnum()
        {
            this.EnumClass = new StringBuilder();
            this.EnumClass.AppendLine(@"public partial class EnumGroupID
    {");
          
        }
        public void AddEnum(string name)
        {
            EnumClass.AppendLine(string.Format(@"public static readonly string {0} = ""{0}"";",name));
        }
        public void EndEnum()
        {
            this.EnumClass.AppendLine("}");
        }
        #endregion
    }
}
