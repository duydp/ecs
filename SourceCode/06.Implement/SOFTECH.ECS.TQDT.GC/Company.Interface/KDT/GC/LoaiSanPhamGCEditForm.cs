﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.GC.BLL.DuLieuChuan;
using System.Data;
using Janus.Windows.GridEX;
using System.Windows.Forms;

namespace Company.Interface.KDT.GC
{
    public partial class LoaiSanPhamGCEditForm : BaseForm
    {
        public Company.GC.BLL.KDT.GC.NhomSanPham nhom = new Company.GC.BLL.KDT.GC.NhomSanPham();
        public HopDong HD = new HopDong();
        public LoaiSanPhamGCEditForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            //this.Close();
        }

        private void LoaiSanPhamGCEditForm_Load(object sender, EventArgs e)
        {
            dgNhomSanPham.DataSource = HD.NhomSPCollection;
            cbTen.Ma = nhom.MaSanPham;
            txtSoLuong.Text = nhom.SoLuong.ToString();
            txtGia.Text = nhom.GiaGiaCong.ToString();
            if (this.OpenType == OpenFormType.Edit)
            {
                ;
            }
            else
            {
                dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                btnAdd.Enabled = false;
                btnXoa.Enabled = false;
            }

            txtSoLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
            txtGia.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
        }
        private void reset()
        {
            txtGia.Text = "0.000";
            txtSoLuong.Text = "0.000";
            dgNhomSanPham.Refetch();
            //cbTen.SelectIndex = 0;

            nhom = new Company.GC.BLL.KDT.GC.NhomSanPham();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            {
                cbTen.Focus();
                cvError.Validate();
                if (!cvError.IsValid) return;
                {
                    checkExitsNhomSanPhamAndSTTHang();
                }
            }


        }
        private void checkExitsNhomSanPhamAndSTTHang()
        {
            HD.NhomSPCollection.Remove(nhom);
            foreach (Company.GC.BLL.KDT.GC.NhomSanPham nhomSP in HD.NhomSPCollection)
            {
                if (nhomSP.MaSanPham == cbTen.Ma.ToString())
                {
                    showMsg("MSG_2702055");
                    //MLMessages("Đã có sản phẩm này trong danh sách.","MSG_WRN06","product", false);
                    if (nhom.MaSanPham != "")
                        HD.NhomSPCollection.Add(nhom);
                    return;
                }
            }
            foreach (SanPham sp in HD.SPCollection)
            {
                if (sp.NhomSanPham_ID.ToUpper() == nhom.MaSanPham.ToUpper())
                {
                    sp.NhomSanPham_ID = cbTen.Ma.Trim();
                }
            }
            nhom.SoLuong = Convert.ToDecimal(txtSoLuong.Text);
            nhom.GiaGiaCong = Convert.ToDecimal(txtGia.Text);
            nhom.TenSanPham = cbTen.Ten.Trim();
            nhom.MaSanPham = cbTen.Ma.Trim();
            nhom.HopDong_ID = HD.ID;
            HD.NhomSPCollection.Add(nhom);
            reset();
        }
        private void dgNhomSanPham_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)e.Row.DataRow;
            cbTen.Ma = nhom.MaSanPham;
            txtSoLuong.Text = nhom.SoLuong.ToString();
            txtGia.Text = nhom.GiaGiaCong.ToString();
        }

        private void dgNhomSanPham_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string st = "";
            foreach (GridEXSelectedItem row in dgNhomSanPham.SelectedItems)
            {
                Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                foreach (SanPham sp in HD.SPCollection)
                {
                    if (sp.NhomSanPham_ID == nhom.MaSanPham)
                    {
                        if (st.IndexOf(sp.Ma) < 0)
                            st += sp.Ma + ";";
                    }
                }
            }
            bool ok = false;
            if (st == "")
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa nhóm sản phẩm này không?", true) == "Yes")
                {
                    ok = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                if (showMsg("MSG_240239", st, true) == "Yes")
                //if (MLMessages("Nhóm sản phẩm này có liên quan đến các sản phẩm " + st + " Bạn có muốn xoá không", "MSG_DEL05", st, true) == "Yes")
                {
                    ok = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
            if (ok)
            {
                foreach (GridEXSelectedItem row in dgNhomSanPham.SelectedItems)
                {
                    Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                    if (HD.SPCollection.Count > 0)
                    {
                        SanPham sp = new SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.DeleteByNhomSanPhamTransaction(null, nhom.MaSanPham);
                        SanPhamCollection SPCollectionTMP = new SanPhamCollection();
                        foreach (SanPham spDelete in HD.SPCollection)
                        {
                            if (spDelete.NhomSanPham_ID == nhom.MaSanPham)
                            {
                                SPCollectionTMP.Add(spDelete);
                            }
                        }
                        foreach (SanPham spDelete in SPCollectionTMP)
                        {
                            foreach (SanPham SPHopDong in HD.SPCollection)
                            {
                                if (SPHopDong.Ma == spDelete.Ma)
                                {
                                    HD.SPCollection.Remove(SPHopDong);
                                    break;
                                }
                            }
                        }
                    }
                    nhom.Delete();
                }
            }
            else e.Cancel = true;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string st = "";
            GridEXSelectedItemCollection items = dgNhomSanPham.SelectedItems;
            if (items.Count < 0) return;
            foreach (GridEXSelectedItem row in items)
            {
                Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                foreach (SanPham sp in HD.SPCollection)
                {
                    if (sp.NhomSanPham_ID == nhom.MaSanPham)
                    {
                        if (st.IndexOf(sp.Ma) < 0)
                            st += sp.Ma + ";";
                    }
                }
            }
            bool ok = false;
            if (st == "")
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                //if (ShowMessage("Bạn có muốn xóa nhóm sản phẩm này không?", true) == "Yes")
                {
                    ok = true;
                }
            }
            else
            {
                if (showMsg("MSG_240239", st, true) == "Yes")
                //if (MLMessages("Nhóm sản phẩm này có liên quan đến các sản phẩm " + st + " Bạn có muốn xoá không", "MSG_DEL05", st, true) == "Yes")
                {
                    ok = true;
                }

            }
            if (ok)
            {
                NhomSanPhamCollection NhomSPCollection = new NhomSanPhamCollection();
                foreach (GridEXSelectedItem row in items)
                {
                    Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                    NhomSPCollection.Add(nhom);
                    if (HD.SPCollection.Count > 0)
                    {
                        SanPham sp = new SanPham();
                        sp.HopDong_ID = HD.ID;
                        sp.DeleteByNhomSanPhamTransaction(null, nhom.MaSanPham);
                        SanPhamCollection SPCollectionTMP = new SanPhamCollection();
                        foreach (SanPham spDelete in HD.SPCollection)
                        {
                            if (spDelete.NhomSanPham_ID == nhom.MaSanPham)
                            {
                                SPCollectionTMP.Add(spDelete);
                            }
                        }
                        foreach (SanPham spDelete in SPCollectionTMP)
                        {
                            foreach (SanPham SPHopDong in HD.SPCollection)
                            {
                                if (SPHopDong.Ma == spDelete.Ma)
                                {
                                    HD.SPCollection.Remove(SPHopDong);
                                    break;
                                }
                            }
                        }
                    }
                    nhom.Delete();
                }
                if (NhomSPCollection.Count > 0)
                {
                    foreach (Company.GC.BLL.KDT.GC.NhomSanPham nhomSP in NhomSPCollection)
                    {
                        foreach (Company.GC.BLL.KDT.GC.NhomSanPham nhomSPHopDong in HD.NhomSPCollection)
                        {
                            if (nhomSPHopDong.MaSanPham == nhomSP.MaSanPham)
                            {
                                HD.NhomSPCollection.Remove(nhomSPHopDong);
                                break;
                            }
                        }

                    }
                }
                dgNhomSanPham.DataSource = HD.NhomSPCollection;
                try
                {
                    dgNhomSanPham.Refetch();
                }
                catch { dgNhomSanPham.Refresh(); }

            }

        }

        //private void txtSoLuong_TextChanged(object sender, EventArgs e)
        //{
        //    error.SetError(txtSoLuong,null);
        //}
        //private void txtGia_TextChanged(object sender, EventArgs e)
        //{
        //    error.SetError(txtGia, null);
        //}
    }
}