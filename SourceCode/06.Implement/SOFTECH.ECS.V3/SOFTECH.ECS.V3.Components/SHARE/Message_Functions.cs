﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.SHARE
{
    public class Message_Functions
    {
        public const int Huy = 1;
        public const int Sua = 5;
        public const int Khai_bao = 8;
        public const int Chua_xu_ly = 12;
        public const int Hoi_trang_thai = 13;
        public const int De_nghi = 16;
        public const int Khong_chap_nhan = 27;
        public const int Cap_so_tiep_nhan = 29;
        public const int Cap_so_to_khai = 30;
        public const int Duyet_luong_chung_tu = 31;
        public const int Thong_bao_thong_quan = 32;
        public const int Thong_bao_thuc_xuat = 33;
        public const int Thong_bao_xac_nhan_hang_ra_khoi_khu_vuc_giam_sat = 34;
        
    }
}
