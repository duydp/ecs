﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.Maper;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.Interface.VNACCS;
using Company.Interface.DanhMucChuan;
#if SXXK_V4
using Company.Interface.VNACCS.Vouchers;
using Company.KDT.SHARE.VNACCS.Controls;
#endif
#if KD_V4
using Company.Interface.VNACCS.Vouchers;
using Company.KDT.SHARE.VNACCS.Controls;
#endif
#if GC_V4
using Company.GC.BLL.KDT.GC;
using Company.Interface.VNACCS.Vouchers;
using Company.KDT.SHARE.VNACCS.Controls;
#endif



namespace Company.Interface
{
    public partial class VNACC_ToKhaiMauDichNhapForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
        public KDT_VNACC_TK_SoVanDon vd = new KDT_VNACC_TK_SoVanDon();
        public VNACC_TK_TKTriGiaForm f = new VNACC_TK_TKTriGiaForm();
        public VNACC_HangMauDichNhapForm hmdForm = new VNACC_HangMauDichNhapForm();
        public VNACC_TK_GiayPhepForm gpForm = new VNACC_TK_GiayPhepForm();
        public VNACC_TK_DinhKiemDTForm dkForm = new VNACC_TK_DinhKiemDTForm();
        public VNACC_TK_TrungChuyenForm trungchuyenForm = new VNACC_TK_TrungChuyenForm();
        public string soHD = "";
        public DateTime ngayHD;
        private DateTime dayMin = new DateTime(1900, 1, 1);
        private string MaTienTe_TK = "";
        public bool IsCopy;
        public bool Is2061 = false;
        public string KyHieuVaSoHieu = "";
        public decimal TongSoLuongHang = 0;
        public decimal TongSoLuongHangTK = 0;
        public decimal TongTriGiaHD = 0;
        public decimal TongTriGiaTinhThue = 0;
        public decimal TongHeSoPhanBo = 0;
        public List<KDT_VNACC_ToKhaiMauDich> ToKhaiMauDichCollection = new List<KDT_VNACC_ToKhaiMauDich>();
        public String Caption;
        public bool IsChange;
#if GC_V4
        public HopDong HD;
#endif
        public long HopDong_ID;
        //public static bool flag = true;
        public VNACC_ToKhaiMauDichNhapForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.IDA.ToString());
            BoSungNghiepVu();
        }
        private string loaiHangHoa = "";
        private void txtSoVanDon_ButtonClick(object sender, EventArgs e)
        {
            VNACC_TK_VanDonForm f = new VNACC_TK_VanDonForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
            Is2061 = f.IsCheck2061;
            if (TKMD.VanDonCollection.Count >0)
            {
                txtSoVanDon.Text = TKMD.VanDonCollection[0].SoDinhDanh;
            }
        }

        private void check() 
        {
            if (txtSoToKhai.Text == "0")
            {
                txtSoToKhai.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoToKhai.Text = "";
                txtSoToKhai.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoToKhai.Text = "";
            }
            if (txtTongSoTKChiaNho.Text == "0")
            {
                txtTongSoTKChiaNho.TextChanged -= new EventHandler(txt_TextChanged);
                txtTongSoTKChiaNho.Text = "";
                txtTongSoTKChiaNho.TextChanged += new EventHandler(txt_TextChanged);

                //txtTongSoTKChiaNho.Text = "";
            }
            txtSoToKhaiTNTX.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            if (txtSoToKhaiTNTX.Text == "0")
            {
                txtSoToKhaiTNTX.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoToKhaiTNTX.Text = "";
                txtSoToKhaiTNTX.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoToKhaiTNTX.Text = "";
            }
            if (txtSoNhanhToKhai.Text == "0")
            {
                txtSoNhanhToKhai.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoNhanhToKhai.Text = "";
                txtSoNhanhToKhai.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoNhanhToKhai.Text = "";
            }
            if (txtNamPhatHanhBL.Text == "0")
            {
                txtNamPhatHanhBL.TextChanged -= new EventHandler(txt_TextChanged);
                txtNamPhatHanhBL.Text = "";
                txtNamPhatHanhBL.TextChanged += new EventHandler(txt_TextChanged);

                //txtNamPhatHanhBL.Text = "";
            }
            if (txtNamPhatHanhHM.Text == "0")
            {
                txtNamPhatHanhHM.TextChanged -= new EventHandler(txt_TextChanged);
                txtNamPhatHanhHM.Text = "";
                txtNamPhatHanhHM.TextChanged += new EventHandler(txt_TextChanged);

                //txtNamPhatHanhHM.Text = "";
            }
            if (txtTongHeSoPhanBo.Text == "0")
            {
                txtTongHeSoPhanBo.TextChanged -= new EventHandler(txt_TextChanged);
                txtTongHeSoPhanBo.Text = "";
                txtTongHeSoPhanBo.TextChanged += new EventHandler(txt_TextChanged);

                //txtTongHeSoPhanBo.Text = "";
            }
            txtSoTiepNhanHD.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            if (txtSoTiepNhanHD.Text == "0")
            {
                txtSoTiepNhanHD.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoTiepNhanHD.Text = "";
                txtSoTiepNhanHD.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoTiepNhanHD.Text = "";
            }
            txtSoLuongCont.NullBehavior = txtSoLuong.NullBehavior = txtTrongLuong.NullBehavior = txtTongTriGiaHD.NullBehavior = Janus.Windows.GridEX.NumericEditNullBehavior.AllowNull;
            if (txtSoLuongCont.Text == "0")
            {
                txtSoLuongCont.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuongCont.Text = "";
                txtSoLuongCont.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoLuongCont.Text = "";
            }
            if (txtSoLuong.Text == "0")
            {
                txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong.Text = "";
                txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoLuong.Text = "";
            }
            if (txtTrongLuong.Text == "0")
            {
                txtTrongLuong.TextChanged -= new EventHandler(txt_TextChanged);
                txtTrongLuong.Text = "";
                txtTrongLuong.TextChanged += new EventHandler(txt_TextChanged);

                //txtTrongLuong.Text = "";
            }
            if (txtTongTriGiaHD.Text == "0")
            {
                txtTongTriGiaHD.TextChanged -= new EventHandler(txt_TextChanged);
                txtTongTriGiaHD.Text = "";
                txtTongTriGiaHD.TextChanged += new EventHandler(txt_TextChanged);

                //txtTongTriGiaHD.Text = "";
            }

        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoSua)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void VNACC_ToKhaiMauDichXuatForm_Load(object sender, EventArgs e)
        {
            
            try
            {
                Cursor = Cursors.WaitCursor;
                this.SetIDControl();

                Caption = this.Text;

                if (TKMD.ID != null)
                {
                    //SetToKhai();
                    TuDongCapNhatThongTin();
                    cmbMain.Commands["cmdChiThiHQ"].Visible = Janus.Windows.UI.InheritableBoolean.True;

                }
                

                SetToKhai();
                if (TKMD.ID == 0)
                {
                    ctrPhanLoaiToChuc.Code = "4";
#if GC_V4
                    if (HD!=null)
                    {
                        SetAutomaticGiayPhep();
                    }
#endif
                }

                check();
                LoadDuLieuChuan();



                LoadGrid();

                setCommandStatus();
                if (TKMD.SoLuongCont == 0)
                {
                    cmdBoSungCont.Text = cmdBoSungCont1.Text = "Lấy phản hồi danh sách hàng hóa đủ điều kiện qua KVGS";
                }
                SetMaxLengthControl();

                ValidateForm(true);

                SetAutoRemoveUnicodeAndUpperCaseControl();
                //ctrMaNuocDoiTac.Leave += new EventHandler(ctrMaNuocDoiTac_Leave);

                //ctrCoQuanHaiQuan_Leave(null, null);
                //ctrMaPhuongThucVT_Leave(null, null);
                //ctrMaNuocDoiTac_Leave(null, null);


                //thông tư 38 loại hàng hóa được sử dụng
                ////Minhnd fix mã phân loại hàng hóa
                //if (Company.KDT.SHARE.Components.Globals.IsMaPhanLoaiHangHoa)
                //    ctrMaPhanLoaiHH.Enabled = true;
                //else
                //    ctrMaPhanLoaiHH.Enabled = true;
                ////Minhnd fix mã phân loại hàng hóa



                
                getLoaiHangHoa();
                
                
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

        }
        private void SetAutomaticGiayPhep()
        {
#if GC_V4
            KDT_VNACC_TK_GiayPhep gp = new KDT_VNACC_TK_GiayPhep();
            gp.SoTT = 1;
            gp.PhanLoai = "HDGC";
            gp.SoGiayPhep = HD.SoTiepNhan.ToString();
            TKMD.GiayPhepCollection.Add(gp);
#endif
        }
        private void LoadGrid()
        {
            grListTyGia.DataSource = TKMD.TyGiaCollection;
            grListSacThue.DataSource = TKMD.SacThueCollection;
            grListTyGia.Refetch();
            grListSacThue.Refetch();
        }
        private void GetToKhai()
        {
            errorProvider.Clear();
            
            //TKMD.SoToKhai = Convert.ToDecimal(txtSoToKhai.Value);
            TKMD.SoToKhaiDauTien = txtSoToKhaiDauTien.Text.Trim();
            TKMD.SoNhanhToKhai = Convert.ToDecimal(txtSoNhanhToKhai.Value);
            TKMD.TongSoTKChiaNho = Convert.ToDecimal(txtTongSoTKChiaNho.Value);
            TKMD.SoToKhaiTNTX = Convert.ToDecimal(txtSoToKhaiTNTX.Value);
            TKMD.MaLoaiHinh = ctrMaLoaiHinh.Code;//(cbbMaLoaiHinh.SelectedValue != null) ? cbbMaLoaiHinh.SelectedValue.ToString() : string.Empty;
            TKMD.MaPhanLoaiHH = ctrMaPhanLoaiHH.Code;//(cbbMaPhanLoaiHH.SelectedValue != null) ? cbbMaPhanLoaiHH.SelectedValue.ToString() : string.Empty;
            TKMD.PhanLoaiToChuc = ctrPhanLoaiToChuc.Code;
            TKMD.CoQuanHaiQuan = ctrCoQuanHaiQuan.Code;
            TKMD.NhomXuLyHS = ctrNhomXuLyHS.Code; //"01";
            TKMD.ThoiHanTaiNhapTaiXuat = clcThoiHanTaiNhapTaiXuat.Value;
            TKMD.NgayDangKy = clcNgayDangKy.Value;
            TKMD.MaPhuongThucVT = ctrMaPhuongThucVT.Code;
            TKMD.NgayNhapKhoDau = clcNgayNhapKhoDauTien.Value;
            //don vi
            TKMD.MaDonVi = txtMaDonVi.Text;
            TKMD.TenDonVi = txtTenDonVi.Text;
            TKMD.MaBuuChinhDonVi = txtMaBuuChinhDonVi.Text;
            TKMD.DiaChiDonVi = txtDiaChiDonVi.Text;
            TKMD.SoDienThoaiDonVi = txtSoDienThoaiDonVi.Text;
            
            TKMD.MaUyThac = txtMaUyThac.Text;
            TKMD.TenUyThac = txtTenUyThac.Text;

            TKMD.NguoiUyThacXK = txtUyThacXK.Text;

            // doi tac
            TKMD.MaDoiTac = txtMaDoiTac.Text;
            TKMD.TenDoiTac = txtTenDoiTac.Text;
            TKMD.MaBuuChinhDoiTac = txtMaBuuChinhDoiTac.Text;
            TKMD.DiaChiDoiTac1 = txtDiaChiDoiTac1.Text;
            TKMD.DiaChiDoiTac2 = txtDiaChiDoiTac2.Text;
            TKMD.DiaChiDoiTac3 = txtDiaChiDoiTac3.Text;
            TKMD.DiaChiDoiTac4 = txtDiaChiDoiTac4.Text;
            TKMD.MaNuocDoiTac = ctrMaNuocDoiTac.Code;
            TKMD.MaDaiLyHQ = txtMaDaiLyHQ.Text;
            //so kien va trong luong
            TKMD.SoLuong = Convert.ToDecimal(txtSoLuong.Value);
            TKMD.MaDVTSoLuong = ctrMaDVTSoLuong.Code;
            TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Value);
            TKMD.MaDVTTrongLuong = ctrMaDVTTrongLuong.Code;
            // van don
            TKMD.MaDDLuuKho = ctrMaDDLuuKho.Code;
            TKMD.SoHieuKyHieu = txtSoHieuKyHieu.Text;
            TKMD.MaPTVC = txtMaPTVC.Text;
            TKMD.TenPTVC = txtTenPTVC.Text;
            TKMD.NgayHangDen = clcNgayHangDen.Value;
            TKMD.MaDiaDiemDoHang = ctrDiaDiemDoHang.Code;
            TKMD.TenDiaDiemDohang = ctrDiaDiemDoHang.Name_VN;
            TKMD.MaDiaDiemXepHang = ctrDiaDiemXepHang.Code;
            TKMD.TenDiaDiemXepHang = ctrDiaDiemXepHang.Name_VN;
            if (String.IsNullOrEmpty(txtSoLuongCont.Text))
            {
                TKMD.SoLuongCont = 0;
            }
            else
            {
                TKMD.SoLuongCont = Convert.ToDecimal(txtSoLuongCont.Text);
            }
            //hoa don
            TKMD.PhanLoaiHD = ctrPhanLoaiHD.Code;//(cbbPhanLoaiHD.SelectedValue != null) ? cbbPhanLoaiHD.SelectedValue.ToString() : string.Empty;
            TKMD.SoTiepNhanHD = Convert.ToDecimal(txtSoTiepNhanHD.Value);
            TKMD.SoHoaDon = txtSoHoaDon.Text;
            TKMD.NgayPhatHanhHD = clcNgayPhatHanhHD.Value;
            TKMD.PhuongThucTT = ctrPhuongThucTT.Code;
            TKMD.PhanLoaiGiaHD = ctrMaPhanLoaiTriGia.Code;
            TKMD.MaDieuKienGiaHD = ctrMaDieuKienGiaHD.Code;

            TKMD.TongTriGiaHD = Convert.ToDecimal(txtTongTriGiaHD.Value);

            //minhnd 17/06/2015 TongHeSoPhanBoTG = TongTriGiaHD
            //if (int.Parse(txtTongSoTrangCuaToKhai.Text) > 0)
            //{
            //    TKMD.TongHeSoPhanBoTG = Convert.ToDecimal(txtTongTriGiaHD.Value);
            //}
            TKMD.TongHeSoPhanBoTG = Convert.ToDecimal(txtTongHeSoPhanBo.Value);
            //else 
            //{
            //    decimal triGiaHang = 0;
            //    foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
            //    {
            //        triGiaHang += hmd.TriGiaHoaDon;
            //    }
            //    TKMD.TongHeSoPhanBoTG = triGiaHang;
            //}
            

            TKMD.MaTTHoaDon = ctrMaTTHoaDon.Code;
            //tri gia tinh thue
            //TKMD.TriGiaTinhThue = Convert.ToDecimal(txtTriGiaTinhThue.Text);
            //TKMD.MaTTTriGiaTinhThue = ctrMaTTTriGiaTinhThue.Ma.Trim();
            //TKMD.PhanLoaiKhongQDVND = txtPhanLoaiKhongQDVND.Text;
            TKMD.MaKetQuaKiemTra = ctrMaKetQuaKiemTra.Code;
            TKMD.MaVanbanPhapQuy1 = ctrMaVanBanPhapQuy1.Code;
            TKMD.MaVanbanPhapQuy2 = ctrMaVanBanPhapQuy2.Code;
            TKMD.MaVanbanPhapQuy3 = ctrMaVanBanPhapQuy3.Code;
            TKMD.MaVanbanPhapQuy4 = ctrMaVanBanPhapQuy4.Code;
            TKMD.MaVanbanPhapQuy5 = ctrMaVanBanPhapQuy5.Code;

            //han muc
            TKMD.NguoiNopThue = ctrNguoiNopThue.Code;//(cbbNguoiNopThue.SelectedValue != null) ? cbbNguoiNopThue.SelectedValue.ToString() : string.Empty;
            TKMD.MaLyDoDeNghiBP = ctrMaLyDoDeNghi.Code;
            TKMD.MaNHTraThueThay = ctrMaNHTraThueThay.Code;
            TKMD.NamPhatHanhHM = Convert.ToDecimal(txtNamPhatHanhHM.Value);
            TKMD.KyHieuCTHanMuc = txtKyHieuCTHanMuc.Text;
            TKMD.SoCTHanMuc = txtSoCTHanMuc.Text;
            //bao lanh thue
            TKMD.MaXDThoiHanNopThue = ctrMaXDThoiHanNopThue.Code;
            TKMD.MaNHBaoLanh = ctrMaNHBaoLanh.Code;
            TKMD.NamPhatHanhBL = Convert.ToDecimal(txtNamPhatHanhBL.Value);
            TKMD.KyHieuCTBaoLanh = txtKyHieuCTBaoLanh.Text;
            TKMD.SoCTBaoLanh = txtSoCTBaoLanh.Text;
            //
            TKMD.NgayKhoiHanhVC = clcNgayKhoiHanhVC.Value;
            TKMD.DiaDiemDichVC = ctrDiaDiemDichVC.Code;
            TKMD.NgayDen = clcNgayDen.Value;
            
            
#if KD_V4 || SXXK_V4
            TKMD.GhiChu = txtGhiChu.Text;
            
#elif GC_V4
            //if (TKMD.MaDonVi == "4000395355" || TKMD.MaDonVi == "0400101556")
            //{
            if (HD == null)
            {
                try
                {
                    HopDong hopDong = new HopDong();
                    hopDong = HopDong.Load(HopDong.GetID(txtSoHD.Text));
                    DateTime NgayHetHan = hopDong.NgayGiaHan.Year == 1900 ? hopDong.NgayHetHan : hopDong.NgayGiaHan;

                    TKMD.GhiChu = "#&" + txtSoHD.Text + "#&" + lblNgayHD1.Text.Replace("/", "") + "#&" + NgayHetHan.ToString("dd/MM/yyyy").Replace("/", "") + "#&" + hopDong.TenDonViDoiTac + "#&" + txtGhiChu.Text;
                    // Cảnh báo hết hạn hợp đồng gia công
                    if (NgayHetHan < DateTime.Now)
                    {
                        ShowMessageTQDT("Cảnh báo từ Hệ thống", "Hợp đồng gia công : " + hopDong.SoHopDong + " \nNgày hết hạn hợp đồng : " + NgayHetHan.ToString("dd/MM/yyyy") + " đã hết hạn hợp đồng .\nDoanh nghiệp kiểm tra thông tin Hợp đồng trước khi khai báo tờ khai", false);
                    }
                    else if (NgayHetHan <= DateTime.Now.AddDays(30))
                    {
                        ShowMessageTQDT("Cảnh báo từ Hệ thống", "Hợp đồng gia công : " + hopDong.SoHopDong + " \nNgày hết hạn hợp đồng : " + NgayHetHan.ToString("dd/MM/yyyy") + " chỉ còn :  " + (NgayHetHan - DateTime.Now).TotalDays.ToString("N0") + " ngày là hết hạn .\nDoanh nghiệp thực hiện Khai báo phụ kiện Gia hạn hợp đồng trước khi Hợp đồng hết hạn", false);
                    }
                }
                catch (Exception ex)
                {
                    TKMD.GhiChu = txtGhiChu.Text.ToString();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
            else
            {
                DateTime NgayHetHan = HD.NgayGiaHan.Year == 1900 ? HD.NgayHetHan : HD.NgayGiaHan;
                TKMD.GhiChu = "#&" + txtSoHD.Text + "#&" + lblNgayHD1.Text.Replace("/", "") + "#&" + NgayHetHan.ToString("dd/MM/yyyy").Replace("/", "") + "#&" + HD.TenDonViDoiTac + "#&" + txtGhiChu.Text;
                // Cảnh báo hết hạn hợp đồng gia công
                if (NgayHetHan < DateTime.Now)
                {
                    ShowMessageTQDT("Cảnh báo từ Hệ thống", "Hợp đồng gia công : " + HD.SoHopDong + " \nNgày hết hạn hợp đồng : " + NgayHetHan.ToString("dd/MM/yyyy") + " đã hết hạn hợp đồng .\nDoanh nghiệp kiểm tra thông tin Hợp đồng trước khi khai báo tờ khai", false);
                }
                else if (NgayHetHan <= DateTime.Now.AddDays(30))
                {
                    ShowMessageTQDT("Cảnh báo từ Hệ thống", "Hợp đồng gia công : " + HD.SoHopDong + " \nNgày hết hạn hợp đồng : " + NgayHetHan.ToString("dd/MM/yyyy") + " chỉ còn :  " + (NgayHetHan - DateTime.Now).TotalDays.ToString("N0") + " ngày là hết hạn .\nDoanh nghiệp thực hiện Khai báo phụ kiện Gia hạn hợp đồng trước khi Hợp đồng hết hạn", false);
                }
            }                
            //}
            //else 
            //{
            //    if (HD == null)
            //    {
            //        DateTime NgayHetHan = HopDong.GetDate(txtSoHD.Text);
            //        TKMD.GhiChu = "#&" + txtSoHD.Text + "#&" + lblNgayHD1.Text.Replace("/", "") + "#&" + NgayHetHan.ToString("dd/MM/yyyy").Replace("/", "") + "#&" + txtGhiChu.Text;
            //    }
            //    else
            //    {
            //        TKMD.GhiChu = "#&" + txtSoHD.Text + "#&" + lblNgayHD1.Text.Replace("/", "") + "#&" + HD.NgayHetHan.ToString("dd/MM/yyyy").Replace("/", "") + "#&" + txtGhiChu.Text;
            //    }
            //    //TKMD.GhiChu = txtGhiChu.Text;
            //}
            //TKMD.GhiChu = txtGhiChu.Text;
#endif
            TKMD.SoQuanLyNoiBoDN = txtSoQuanLyNoiBoDN.Text;
            //Van don
            if (txtSoVanDon.Text.Trim() != "")
            {
                if (TKMD.VanDonCollection.Count == 0)
                    TKMD.VanDonCollection.Add(vd);
                TKMD.VanDonCollection[0].SoTT = 1;
                TKMD.VanDonCollection[0].SoDinhDanh = txtSoVanDon.Text;
            }
            else
            {
                if (TKMD.VanDonCollection.Count > 0)
                {
                    TKMD.VanDonCollection[0].Delete();
                    TKMD.VanDonCollection.Clear();
                }
            }
            //TKMD.SoLuongCont = Convert.ToDecimal(numericEditBox1.Value);

            #region get loai hang cho to khai GC và SXXK
#if SXXK_V4 || GC_V4
            if (rdbNPL.Checked)
                TKMD.LoaiHang = "N";
            else if (rdbThietBi.Checked)
                TKMD.LoaiHang = "T";
            else if (rdbHangMau.Checked)
                TKMD.LoaiHang = "H";
            else if (rdbSanPham.Checked)
                TKMD.LoaiHang = "S";
#endif
            #endregion

            if(ctrMaPhuongThucVT.Code == "9")
            {
                TKMD.TenDiaDiemDohang = txtTenDiaDiemDoHang.Text.Trim();
                TKMD.MaDiaDiemDoHang = txtMaDiaDiemDoHang.Text.Trim();
                TKMD.TenDiaDiemXepHang = txtTenDiaDiemXepHang.Text.Trim();
                TKMD.MaDiaDiemXepHang = txtMaDiaDiemXepHang.Text.Trim();
            }

        }
        private void SetToKhai()
        {
            errorProvider.Clear();

            txtSoToKhai.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoToKhai.Text = Convert.ToString(TKMD.SoToKhai);
            txtSoToKhai.TextChanged += new EventHandler(txt_TextChanged);

            txtSoToKhaiDauTien.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoToKhaiDauTien.Text = TKMD.SoToKhaiDauTien;
            txtSoToKhaiDauTien.TextChanged += new EventHandler(txt_TextChanged);

            txtSoNhanhToKhai.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoNhanhToKhai.Text = Convert.ToString(TKMD.SoNhanhToKhai);
            txtSoNhanhToKhai.TextChanged += new EventHandler(txt_TextChanged);

            txtTongSoTKChiaNho.TextChanged -= new EventHandler(txt_TextChanged);
            txtTongSoTKChiaNho.Text = Convert.ToString(TKMD.TongSoTKChiaNho);
            txtTongSoTKChiaNho.TextChanged += new EventHandler(txt_TextChanged);

            txtSoToKhaiTNTX.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoToKhaiTNTX.Text = Convert.ToString(TKMD.SoToKhaiTNTX);
            txtSoToKhaiTNTX.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaLoaiHinh.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaLoaiHinh.Code = TKMD.MaLoaiHinh;
            ctrMaLoaiHinh.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaPhanLoaiHH.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaPhanLoaiHH.Code = TKMD.MaPhanLoaiHH;
            ctrMaPhanLoaiHH.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrPhanLoaiToChuc.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrPhanLoaiToChuc.Code = TKMD.PhanLoaiToChuc; ;
            ctrPhanLoaiToChuc.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrCoQuanHaiQuan.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrCoQuanHaiQuan.Code = TKMD.CoQuanHaiQuan;
            ctrCoQuanHaiQuan.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            ctrCoQuanHaiQuan_Leave(null, null);

            ctrNhomXuLyHS.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy.EditValueChangedHandle(txt_TextChanged);
            ctrNhomXuLyHS.Code = TKMD.NhomXuLyHS;
            ctrNhomXuLyHS.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy.EditValueChangedHandle(txt_TextChanged);

            clcThoiHanTaiNhapTaiXuat.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);
            clcThoiHanTaiNhapTaiXuat.Value = TKMD.ThoiHanTaiNhapTaiXuat;
            clcThoiHanTaiNhapTaiXuat.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);

            clcNgayDangKy.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);
            clcNgayDangKy.Value = TKMD.NgayDangKy;
            clcNgayDangKy.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);

            ctrMaPhuongThucVT.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaPhuongThucVT.Code = TKMD.MaPhuongThucVT;
            ctrMaPhuongThucVT.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            clcNgayNhapKhoDauTien.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);
            clcNgayNhapKhoDauTien.Value = TKMD.NgayNhapKhoDau;
            clcNgayNhapKhoDauTien.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);

            //txtSoToKhai.Text = Convert.ToString(TKMD.SoToKhai);
            //txtSoToKhaiDauTien.Text = TKMD.SoToKhaiDauTien;
            //txtSoNhanhToKhai.Text = Convert.ToString(TKMD.SoNhanhToKhai);
            //txtTongSoTKChiaNho.Text = Convert.ToString(TKMD.TongSoTKChiaNho);
            //txtSoToKhaiTNTX.Text = Convert.ToString(TKMD.SoToKhaiTNTX);
            //ctrMaLoaiHinh.Code = TKMD.MaLoaiHinh;
            //ctrMaPhanLoaiHH.Code = TKMD.MaPhanLoaiHH;
            //ctrPhanLoaiToChuc.Code = TKMD.PhanLoaiToChuc;
            //ctrCoQuanHaiQuan.Code = TKMD.CoQuanHaiQuan;
            //ctrCoQuanHaiQuan_Leave(null, null);
            //ctrNhomXuLyHS.Code = TKMD.NhomXuLyHS;
            //clcThoiHanTaiNhapTaiXuat.Value = TKMD.ThoiHanTaiNhapTaiXuat;
            //clcNgayDangKy.Value = TKMD.NgayDangKy;
            //ctrMaPhuongThucVT.Code = TKMD.MaPhuongThucVT;
            //clcNgayNhapKhoDauTien.Value = TKMD.NgayNhapKhoDau;
            //Phan luong
            string pl = TKMD.MaPhanLoaiKiemTra;
            if (pl == "1")
            {
                lblPhanLuong.Text = "Luồng Xanh";
                this.lblPhanLuong.ForeColor = System.Drawing.Color.Green;
            }
            if (pl == "2")
            {
                lblPhanLuong.Text = "Luồng Vàng";
                this.lblPhanLuong.ForeColor = System.Drawing.Color.Yellow;
            }
            if (pl == "3")
            {
                lblPhanLuong.Text = "Luồng Đỏ";
                this.lblPhanLuong.ForeColor = System.Drawing.Color.Red;
            }
            //Don vi
            txtMaDonVi.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaDonVi.Text = TKMD.MaDonVi;
            txtMaDonVi.TextChanged += new EventHandler(txt_TextChanged);

            txtTenDonVi.TextChanged -= new EventHandler(txt_TextChanged);
            txtTenDonVi.Text = TKMD.TenDonVi;
            txtTenDonVi.TextChanged += new EventHandler(txt_TextChanged);

            txtMaBuuChinhDonVi.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaBuuChinhDonVi.Text = TKMD.MaBuuChinhDonVi;
            txtMaBuuChinhDonVi.TextChanged += new EventHandler(txt_TextChanged);

            txtDiaChiDonVi.TextChanged -= new EventHandler(txt_TextChanged);
            txtDiaChiDonVi.Text = TKMD.DiaChiDonVi;
            txtDiaChiDonVi.TextChanged += new EventHandler(txt_TextChanged);

            txtSoDienThoaiDonVi.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoDienThoaiDonVi.Text = TKMD.SoDienThoaiDonVi;
            txtSoDienThoaiDonVi.TextChanged += new EventHandler(txt_TextChanged);

            //txtMaDonVi.Text = TKMD.MaDonVi;
            //txtTenDonVi.Text = TKMD.TenDonVi;
            //txtMaBuuChinhDonVi.Text = TKMD.MaBuuChinhDonVi;
            //txtDiaChiDonVi.Text = TKMD.DiaChiDonVi;
            //txtSoDienThoaiDonVi.Text = TKMD.SoDienThoaiDonVi;
            //uy thac
            txtMaUyThac.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaUyThac.Text = TKMD.MaUyThac;
            txtMaUyThac.TextChanged += new EventHandler(txt_TextChanged);

            txtTenUyThac.TextChanged -= new EventHandler(txt_TextChanged);
            txtTenUyThac.Text = TKMD.TenUyThac;
            txtTenUyThac.TextChanged += new EventHandler(txt_TextChanged);

            //txtMaUyThac.Text = TKMD.MaUyThac;
            //txtTenUyThac.Text = TKMD.TenUyThac;

            //uy thac xuat khau
            txtUyThacXK.TextChanged -= new EventHandler(txt_TextChanged);
            txtUyThacXK.Text = TKMD.NguoiUyThacXK;
            txtUyThacXK.TextChanged += new EventHandler(txt_TextChanged);

            //txtUyThacXK.Text = TKMD.NguoiUyThacXK;

            //doi tac
            txtMaDoiTac.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaDoiTac.Text = TKMD.MaDoiTac;
            txtMaDoiTac.TextChanged += new EventHandler(txt_TextChanged);

            txtTenDoiTac.TextChanged -= new EventHandler(txt_TextChanged);
            txtTenDoiTac.Text = TKMD.TenDoiTac;
            txtTenDoiTac.TextChanged += new EventHandler(txt_TextChanged);

            txtMaBuuChinhDoiTac.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaBuuChinhDoiTac.Text = TKMD.MaBuuChinhDoiTac;
            txtMaBuuChinhDoiTac.TextChanged += new EventHandler(txt_TextChanged);

            txtDiaChiDoiTac1.TextChanged -= new EventHandler(txt_TextChanged);
            txtDiaChiDoiTac1.Text = TKMD.DiaChiDoiTac1;
            txtDiaChiDoiTac1.TextChanged += new EventHandler(txt_TextChanged);

            txtDiaChiDoiTac2.TextChanged -= new EventHandler(txt_TextChanged);
            txtDiaChiDoiTac2.Text = TKMD.DiaChiDoiTac2;
            txtDiaChiDoiTac2.TextChanged += new EventHandler(txt_TextChanged);

            txtDiaChiDoiTac3.TextChanged -= new EventHandler(txt_TextChanged);
            txtDiaChiDoiTac3.Text = TKMD.DiaChiDoiTac3;
            txtDiaChiDoiTac3.TextChanged += new EventHandler(txt_TextChanged);

            txtDiaChiDoiTac4.TextChanged -= new EventHandler(txt_TextChanged);
            txtDiaChiDoiTac4.Text = TKMD.DiaChiDoiTac4;
            txtDiaChiDoiTac4.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaNuocDoiTac.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrMaNuocDoiTac.Code = TKMD.MaNuocDoiTac;
            ctrMaNuocDoiTac.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            txtMaDaiLyHQ.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaDaiLyHQ.Text = TKMD.MaDaiLyHQ;
            txtMaDaiLyHQ.TextChanged += new EventHandler(txt_TextChanged);

            //txtMaDoiTac.Text = TKMD.MaDoiTac;
            //txtTenDoiTac.Text = TKMD.TenDoiTac;
            //txtMaBuuChinhDoiTac.Text = TKMD.MaBuuChinhDoiTac;
            //txtDiaChiDoiTac1.Text = TKMD.DiaChiDoiTac1;
            //txtDiaChiDoiTac2.Text = TKMD.DiaChiDoiTac2;
            //txtDiaChiDoiTac3.Text = TKMD.DiaChiDoiTac3;
            //txtDiaChiDoiTac4.Text = TKMD.DiaChiDoiTac4;
            //ctrMaNuocDoiTac.Code = TKMD.MaNuocDoiTac;
            //txtMaDaiLyHQ.Text = TKMD.MaDaiLyHQ;
            //so kien va trong luong
            txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoLuong.Text = Convert.ToString(TKMD.SoLuong);
            txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaDVTSoLuong.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaDVTSoLuong.Code = TKMD.MaDVTSoLuong;
            ctrMaDVTSoLuong.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtTrongLuong.TextChanged -= new EventHandler(txt_TextChanged);
            txtTrongLuong.Text = Convert.ToString(TKMD.TrongLuong);
            txtTrongLuong.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaDVTTrongLuong.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaDVTTrongLuong.Code = TKMD.MaDVTTrongLuong;
            ctrMaDVTTrongLuong.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //txtSoLuong.Text = Convert.ToString(TKMD.SoLuong);
            //ctrMaDVTSoLuong.Code = TKMD.MaDVTSoLuong;
            //txtTrongLuong.Text = Convert.ToString(TKMD.TrongLuong);
            //ctrMaDVTTrongLuong.Code = TKMD.MaDVTTrongLuong;
            //van don
            ctrMaDDLuuKho.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaDDLuuKho.Code = TKMD.MaDDLuuKho;
            ctrMaDDLuuKho.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtSoHieuKyHieu.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoHieuKyHieu.Text = TKMD.SoHieuKyHieu;
            txtSoHieuKyHieu.TextChanged += new EventHandler(txt_TextChanged);

            txtMaPTVC.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaPTVC.Text = TKMD.MaPTVC;
            txtMaPTVC.TextChanged += new EventHandler(txt_TextChanged);

            txtTenPTVC.TextChanged -= new EventHandler(txt_TextChanged);
            txtTenPTVC.Text = TKMD.TenPTVC;
            txtTenPTVC.TextChanged += new EventHandler(txt_TextChanged);

            clcNgayHangDen.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);
            clcNgayHangDen.Value = TKMD.NgayHangDen;
            clcNgayHangDen.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);

            ctrDiaDiemDoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrDiaDiemDoHang.Code = (TKMD.MaDiaDiemDoHang != null && TKMD.MaDiaDiemDoHang.Length > 3 && TKMD.MaPhuongThucVT != "4") ? TKMD.MaDiaDiemDoHang.Substring(2, TKMD.MaDiaDiemDoHang.Length - 2) : TKMD.MaDiaDiemDoHang;
            ctrDiaDiemDoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            ctrDiaDiemDoHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrDiaDiemDoHang.Ten = TKMD.TenDiaDiemDohang;
            ctrDiaDiemDoHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            ctrDiaDiemXepHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrDiaDiemXepHang.Code = TKMD.MaDiaDiemXepHang;
            ctrDiaDiemXepHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            ctrDiaDiemXepHang.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);
            ctrDiaDiemXepHang.Ten = TKMD.TenDiaDiemXepHang;
            ctrDiaDiemXepHang.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(txt_TextChanged);

            txtSoLuongCont.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoLuongCont.Text = TKMD.SoLuongCont.ToString();
            txtSoLuongCont.TextChanged += new EventHandler(txt_TextChanged);

            //ctrMaDDLuuKho.Code = TKMD.MaDDLuuKho;
            //txtSoHieuKyHieu.Text = TKMD.SoHieuKyHieu;
            //txtMaPTVC.Text = TKMD.MaPTVC;
            //txtTenPTVC.Text = TKMD.TenPTVC;
            //clcNgayHangDen.Value = TKMD.NgayHangDen;

            ////ctrDiaDiemDoHang.Code = TKMD.MaDiaDiemDoHang;

            //ctrDiaDiemDoHang.Code = (TKMD.MaDiaDiemDoHang != null && TKMD.MaDiaDiemDoHang.Length > 3 && TKMD.MaPhuongThucVT != "4") ? TKMD.MaDiaDiemDoHang.Substring(2, TKMD.MaDiaDiemDoHang.Length - 2) : TKMD.MaDiaDiemDoHang;
            //ctrDiaDiemDoHang.Ten = TKMD.TenDiaDiemDohang;
            //ctrDiaDiemXepHang.Code = TKMD.MaDiaDiemXepHang;
            //ctrDiaDiemXepHang.Ten = TKMD.TenDiaDiemXepHang;
            //txtSoLuongCont.Text = TKMD.SoLuongCont.ToString();
            //hoa don

            ctrPhanLoaiHD.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrPhanLoaiHD.Code = TKMD.PhanLoaiHD;
            ctrPhanLoaiHD.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtSoTiepNhanHD.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoTiepNhanHD.Text = Convert.ToString(TKMD.SoTiepNhanHD);
            txtSoTiepNhanHD.TextChanged += new EventHandler(txt_TextChanged);

            txtSoHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoHoaDon.Text = TKMD.SoHoaDon;
            txtSoHoaDon.TextChanged += new EventHandler(txt_TextChanged);

            clcNgayPhatHanhHD.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);
            clcNgayPhatHanhHD.Value = TKMD.NgayPhatHanhHD;
            clcNgayPhatHanhHD.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);

            ctrPhuongThucTT.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrPhuongThucTT.Code = TKMD.PhuongThucTT;
            ctrPhuongThucTT.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaPhanLoaiTriGia.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaPhanLoaiTriGia.Code = TKMD.PhanLoaiGiaHD;
            ctrMaPhanLoaiTriGia.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaDieuKienGiaHD.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaDieuKienGiaHD.Code = TKMD.MaDieuKienGiaHD;
            ctrMaDieuKienGiaHD.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaTTHoaDon.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaTTHoaDon.Code = TKMD.MaTTHoaDon;
            ctrMaTTHoaDon.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtTongTriGiaHD.TextChanged -= new EventHandler(txt_TextChanged);
            txtTongTriGiaHD.Text = Convert.ToString(TKMD.TongTriGiaHD);
            txtTongTriGiaHD.TextChanged += new EventHandler(txt_TextChanged);

            txtTongHeSoPhanBo.TextChanged -= new EventHandler(txt_TextChanged);
            txtTongHeSoPhanBo.Text = Convert.ToString(TKMD.TongHeSoPhanBoTG);
            txtTongHeSoPhanBo.TextChanged += new EventHandler(txt_TextChanged);

            ctrMaKetQuaKiemTra.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaKetQuaKiemTra.Code = TKMD.MaKetQuaKiemTra;
            ctrMaKetQuaKiemTra.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaVanBanPhapQuy1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaVanBanPhapQuy1.Code = TKMD.MaVanbanPhapQuy1;
            ctrMaVanBanPhapQuy1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaVanBanPhapQuy2.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaVanBanPhapQuy2.Code = TKMD.MaVanbanPhapQuy2;
            ctrMaVanBanPhapQuy2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaVanBanPhapQuy3.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaVanBanPhapQuy3.Code = TKMD.MaVanbanPhapQuy3;
            ctrMaVanBanPhapQuy3.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaVanBanPhapQuy4.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaVanBanPhapQuy4.Code = TKMD.MaVanbanPhapQuy4;
            ctrMaVanBanPhapQuy4.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaVanBanPhapQuy5.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaVanBanPhapQuy5.Code = TKMD.MaVanbanPhapQuy5;
            ctrMaVanBanPhapQuy5.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            //ctrPhanLoaiHD.Code = TKMD.PhanLoaiHD;
            //txtSoTiepNhanHD.Text = Convert.ToString(TKMD.SoTiepNhanHD);
            //txtSoHoaDon.Text = TKMD.SoHoaDon;
            //clcNgayPhatHanhHD.Value = TKMD.NgayPhatHanhHD;
            //ctrPhuongThucTT.Code = TKMD.PhuongThucTT;
            //ctrMaPhanLoaiTriGia.Code = TKMD.PhanLoaiGiaHD;
            //ctrMaDieuKienGiaHD.Code = TKMD.MaDieuKienGiaHD;
            //ctrMaTTHoaDon.Code = TKMD.MaTTHoaDon;
            //txtTongTriGiaHD.Text = Convert.ToString(TKMD.TongTriGiaHD);

            ////minhnd thêm Hệ số phân bổ TG

            //txtTongHeSoPhanBo.Text = Convert.ToString(TKMD.TongHeSoPhanBoTG);

            //Tinh thuế

            ////txtTriGiaTinhThue.Text = Convert.ToString(TKMD.TriGiaTinhThue);
            ////txtPhanLoaiKhongQDVND.Text = TKMD.PhanLoaiKhongQDVND;
            ////ctrMaTTTriGiaTinhThue.Ma = TKMD.MaTTTriGiaTinhThue;

            //ctrMaKetQuaKiemTra.Code = TKMD.MaKetQuaKiemTra;
            //ctrMaVanBanPhapQuy1.Code = TKMD.MaVanbanPhapQuy1;
            //ctrMaVanBanPhapQuy2.Code = TKMD.MaVanbanPhapQuy2;
            //ctrMaVanBanPhapQuy3.Code = TKMD.MaVanbanPhapQuy3;
            //ctrMaVanBanPhapQuy4.Code = TKMD.MaVanbanPhapQuy4;
            //ctrMaVanBanPhapQuy5.Code = TKMD.MaVanbanPhapQuy5;

            //han muc
            ctrNguoiNopThue.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrNguoiNopThue.Code = TKMD.NguoiNopThue;
            ctrNguoiNopThue.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaLyDoDeNghi.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaLyDoDeNghi.Code = TKMD.MaLyDoDeNghiBP;
            ctrMaLyDoDeNghi.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaNHTraThueThay.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaNHTraThueThay.Code = TKMD.MaNHTraThueThay;
            ctrMaNHTraThueThay.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtNamPhatHanhHM.TextChanged -= new EventHandler(txt_TextChanged);
            txtNamPhatHanhHM.Text = Convert.ToString(TKMD.NamPhatHanhHM);
            txtNamPhatHanhHM.TextChanged += new EventHandler(txt_TextChanged);

            txtKyHieuCTHanMuc.TextChanged -= new EventHandler(txt_TextChanged);
            txtKyHieuCTHanMuc.Text = TKMD.KyHieuCTHanMuc;
            txtKyHieuCTHanMuc.TextChanged += new EventHandler(txt_TextChanged);

            txtSoCTHanMuc.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoCTHanMuc.Text = TKMD.SoCTHanMuc;
            txtSoCTHanMuc.TextChanged += new EventHandler(txt_TextChanged);

            //ctrNguoiNopThue.Code = TKMD.NguoiNopThue;
            //ctrMaLyDoDeNghi.Code = TKMD.MaLyDoDeNghiBP;
            //ctrMaNHTraThueThay.Code = TKMD.MaNHTraThueThay;
            //txtNamPhatHanhHM.Text = Convert.ToString(TKMD.NamPhatHanhHM);
            //txtKyHieuCTHanMuc.Text = TKMD.KyHieuCTHanMuc;
            //txtSoCTHanMuc.Text = TKMD.SoCTHanMuc;

            //bao lanh thue

            ctrMaXDThoiHanNopThue.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaXDThoiHanNopThue.Code = TKMD.MaXDThoiHanNopThue;
            ctrMaXDThoiHanNopThue.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            ctrMaNHBaoLanh.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrMaNHBaoLanh.Code = TKMD.MaNHBaoLanh;
            ctrMaNHBaoLanh.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            txtNamPhatHanhBL.TextChanged -= new EventHandler(txt_TextChanged);
            txtNamPhatHanhBL.Text = Convert.ToString(TKMD.NamPhatHanhBL);
            txtNamPhatHanhBL.TextChanged += new EventHandler(txt_TextChanged);

            txtKyHieuCTBaoLanh.TextChanged -= new EventHandler(txt_TextChanged);
            txtKyHieuCTBaoLanh.Text = TKMD.KyHieuCTBaoLanh;
            txtKyHieuCTBaoLanh.TextChanged += new EventHandler(txt_TextChanged);

            txtSoCTBaoLanh.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoCTBaoLanh.Text = TKMD.SoCTBaoLanh;
            txtSoCTBaoLanh.TextChanged += new EventHandler(txt_TextChanged);

            //ctrMaXDThoiHanNopThue.Code = TKMD.MaXDThoiHanNopThue;
            //ctrMaNHBaoLanh.Code = TKMD.MaNHBaoLanh;
            //txtNamPhatHanhBL.Text = Convert.ToString(TKMD.NamPhatHanhBL);
            //txtKyHieuCTBaoLanh.Text = TKMD.KyHieuCTBaoLanh;
            //txtSoCTBaoLanh.Text = TKMD.SoCTBaoLanh;
            //----
            clcNgayKhoiHanhVC.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);
            clcNgayKhoiHanhVC.Value = TKMD.NgayKhoiHanhVC;
            clcNgayKhoiHanhVC.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);

            ctrDiaDiemDichVC.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
            ctrDiaDiemDichVC.Code = TKMD.DiaDiemDichVC;
            ctrDiaDiemDichVC.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

            clcNgayDen.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);
            clcNgayDen.Value = TKMD.NgayDen;
            clcNgayDen.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCalendar.EditValueChangedHandle(txt_TextChanged);

            //clcNgayKhoiHanhVC.Value = TKMD.NgayKhoiHanhVC;
            //ctrDiaDiemDichVC.Code = TKMD.DiaDiemDichVC;
            //clcNgayDen.Value = TKMD.NgayDen;
#if KD_V4 || SXXK_V4
            txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
            txtGhiChu.Text = TKMD.GhiChu;
            txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

            //txtGhiChu.Text = TKMD.GhiChu;
#elif GC_V4
            //if (TKMD.MaDonVi == "4000395355" || TKMD.MaDonVi == "0400101556")
            //{
            //    string ghichu = TKMD.GhiChu;
            //    if (ghichu != null)
            //    {
            //        int a = ghichu.LastIndexOf("#&");
            //        if (a > 1)
            //            txtGhiChu.Text = TKMD.GhiChu.Substring(a + 2);
            //    }
            //}
            //else
            //{
            //    txtGhiChu.Text = TKMD.GhiChu;
            //}

            string ghichu = TKMD.GhiChu;
            if (ghichu != null)
            {
                int a = ghichu.LastIndexOf("#&");
                if (a > 1)
                {
                    txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                    txtGhiChu.Text = TKMD.GhiChu.Substring(a + 2);
                    txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

                    //txtGhiChu.Text = TKMD.GhiChu.Substring(a + 2);
                }
                else
                {
                    txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                    txtGhiChu.Text = TKMD.GhiChu;
                    txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

                    //txtGhiChu.Text = TKMD.GhiChu;
                }
            }
            //txtGhiChu.Text = TKMD.GhiChu;
#endif
            txtSoQuanLyNoiBoDN.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoQuanLyNoiBoDN.Text = TKMD.SoQuanLyNoiBoDN;
            txtSoQuanLyNoiBoDN.TextChanged += new EventHandler(txt_TextChanged);

            //txtSoQuanLyNoiBoDN.Text = TKMD.SoQuanLyNoiBoDN;
            //van don

            if (TKMD.VanDonCollection.Count >= 1)
            {
                txtSoVanDon.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoVanDon.Text = TKMD.VanDonCollection[0].SoDinhDanh;
                txtSoVanDon.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoVanDon.Text = TKMD.VanDonCollection[0].SoDinhDanh;
            }

            txtSoHieuKyHieu.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoHieuKyHieu.Text = TKMD.SoHieuKyHieu;
            txtSoHieuKyHieu.TextChanged += new EventHandler(txt_TextChanged);

            //txtSoHieuKyHieu.Text = TKMD.SoHieuKyHieu;
            // thue tra ve
            txtTriGiaTinhThue.TextChanged -= new EventHandler(txt_TextChanged);
            txtTriGiaTinhThue.Text = TKMD.TriGiaTinhThue.ToString();
            txtTriGiaTinhThue.TextChanged += new EventHandler(txt_TextChanged);

            txtTongTienThuePhaiNop.TextChanged -= new EventHandler(txt_TextChanged);
            txtTongTienThuePhaiNop.Text = TKMD.TongTienThuePhaiNop.ToString();
            txtTongTienThuePhaiNop.TextChanged += new EventHandler(txt_TextChanged);

            txtSoTienBaoLanh.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoTienBaoLanh.Text = TKMD.SoTienBaoLanh.ToString();
            txtSoTienBaoLanh.TextChanged += new EventHandler(txt_TextChanged);

            txtTongSoTrangCuaToKhai.TextChanged -= new EventHandler(txt_TextChanged);
            txtTongSoTrangCuaToKhai.Text = TKMD.TongSoTrangCuaToKhai.ToString();
            txtTongSoTrangCuaToKhai.TextChanged += new EventHandler(txt_TextChanged);

            txtTongSoDongHangCuaToKhai.TextChanged -= new EventHandler(txt_TextChanged);
            txtTongSoDongHangCuaToKhai.Text = TKMD.TongSoDongHangCuaToKhai.ToString();
            txtTongSoDongHangCuaToKhai.TextChanged += new EventHandler(txt_TextChanged);


            //txtTriGiaTinhThue.Text = TKMD.TriGiaTinhThue.ToString();
            //txtTongTienThuePhaiNop.Text = TKMD.TongTienThuePhaiNop.ToString();
            //txtSoTienBaoLanh.Text = TKMD.SoTienBaoLanh.ToString();
            //txtTongSoTrangCuaToKhai.Text = TKMD.TongSoTrangCuaToKhai.ToString();
            //txtTongSoDongHangCuaToKhai.Text = TKMD.TongSoDongHangCuaToKhai.ToString();
            // Load Tỷ giá và sác thuế
            LoadGrid();
            //Load hop dong
#if GC_V4
            if (TKMD.HopDong_ID > 0)
            {
                HopDong hd = HopDong.Load(TKMD.HopDong_ID);
                txtSoHD.Text = hd.SoHopDong;
                lblNgayHD1.Text = hd.NgayKy.ToString("dd/MM/yyyy");

            }

#endif
            #region get loai hang cho to khai GC và SXXK
#if SXXK_V4 || GC_V4
            if (TKMD.LoaiHang == "N")
            {
                rdbNPL.CheckedChanged -= new EventHandler(txt_TextChanged);
                rdbNPL.Checked = true;
                rdbNPL.CheckedChanged += new EventHandler(txt_TextChanged);

                //rdbNPL.Checked = true;
            }
            else if (TKMD.LoaiHang == "T")
            {
                rdbThietBi.CheckedChanged -= new EventHandler(txt_TextChanged);
                rdbThietBi.Checked = true;
                rdbThietBi.CheckedChanged += new EventHandler(txt_TextChanged);

                //rdbThietBi.Checked = true;
            }
            else if (TKMD.LoaiHang == "H")
            {
                rdbHangMau.CheckedChanged -= new EventHandler(txt_TextChanged);
                rdbHangMau.Checked = true;
                rdbHangMau.CheckedChanged += new EventHandler(txt_TextChanged);

                //rdbHangMau.Checked = true;
            }
            else if (TKMD.LoaiHang == "S")
            {
                rdbSanPham.CheckedChanged -= new EventHandler(txt_TextChanged);
                rdbSanPham.Checked = true;
                rdbSanPham.CheckedChanged += new EventHandler(txt_TextChanged);

                //rdbSanPham.Checked = true;
            }
#endif
            #endregion
            if (TKMD.MaPhuongThucVT == "9")
            {
                ctrDiaDiemXepHang.Visible = ctrDiaDiemDoHang.Visible = false;
                txtMaDiaDiemDoHang.Visible = txtTenDiaDiemDoHang.Visible = txtTenDiaDiemXepHang.Visible = txtMaDiaDiemXepHang.Visible = true;

                txtMaDiaDiemXepHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaDiaDiemXepHang.Text = TKMD.MaDiaDiemXepHang;
                txtMaDiaDiemXepHang.TextChanged += new EventHandler(txt_TextChanged);

                txtMaDiaDiemDoHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaDiaDiemDoHang.Text = TKMD.MaDiaDiemDoHang;
                txtMaDiaDiemDoHang.TextChanged += new EventHandler(txt_TextChanged);

                txtTenDiaDiemXepHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtTenDiaDiemXepHang.Text = TKMD.TenDiaDiemXepHang;
                txtTenDiaDiemXepHang.TextChanged += new EventHandler(txt_TextChanged);

                txtTenDiaDiemDoHang.TextChanged -= new EventHandler(txt_TextChanged);
                txtTenDiaDiemDoHang.Text = TKMD.TenDiaDiemDohang;
                txtTenDiaDiemDoHang.TextChanged += new EventHandler(txt_TextChanged);

                //txtMaDiaDiemXepHang.Text = TKMD.MaDiaDiemXepHang;
                //txtMaDiaDiemDoHang.Text = TKMD.MaDiaDiemDoHang;
                //txtTenDiaDiemXepHang.Text = TKMD.TenDiaDiemXepHang;
                //txtTenDiaDiemDoHang.Text = TKMD.TenDiaDiemDohang;
            }

        }

        private void LoadDuLieuChuan()
        {
#if KD_V4
            lblNgayHD.Visible = false;
            lblNgayHD1.Visible = false;
            lblSoHD.Visible = false;
            txtSoHD.Visible = false;
#elif SXXK_V4
            rdbSanPham.Visible = true;
            rdbNPL.Visible = true;
            rdbThietBi.Visible = true;
            rdbHangMau.Visible = false;
            lblNgayHD.Visible = false;
            lblNgayHD1.Visible = false;
            lblSoHD.Visible = false;
            txtSoHD.Visible = false;
            
#elif GC_V4
            rdbSanPham.Visible = true;
            rdbNPL.Visible = true;
            rdbThietBi.Visible = true;
            rdbHangMau.Visible = true;
            lblNgayHD.Visible = true;
            lblNgayHD1.Visible = true;
            lblSoHD.Visible = true;
            txtSoHD.Visible = true;
            txtSoHD.ReadOnly = true;


#endif

            if (TKMD.ID == 0 && !IsCopy)
            {

                ctrMaNuocDoiTac.TextChanged -= new EventHandler(txt_TextChanged);
                ctrMaNuocDoiTac.Code = GlobalSettings.NUOC;
                ctrMaNuocDoiTac.TextChanged += new EventHandler(txt_TextChanged);


                //ctrMaNuocDoiTac.Code = GlobalSettings.NUOC;
                ctrDiaDiemDoHang.Where = string.Format("CountryCode = '{0}'", "VN");
                ctrDiaDiemDoHang.ReLoadData();
                // Người nhập khẩu
                txtMaDonVi.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaDonVi.Text = GlobalSettings.MA_DON_VI;
                txtMaDonVi.TextChanged += new EventHandler(txt_TextChanged);

                txtTenDonVi.TextChanged -= new EventHandler(txt_TextChanged);
                txtTenDonVi.Text = GlobalSettings.TEN_DON_VI.ToUpper();
                txtTenDonVi.TextChanged += new EventHandler(txt_TextChanged);

                txtDiaChiDonVi.TextChanged -= new EventHandler(txt_TextChanged);
                txtDiaChiDonVi.Text = GlobalSettings.DIA_CHI.ToUpper();
                txtDiaChiDonVi.TextChanged += new EventHandler(txt_TextChanged);

                txtSoDienThoaiDonVi.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoDienThoaiDonVi.Text = GlobalSettings.SoDienThoaiDN;
                txtSoDienThoaiDonVi.TextChanged += new EventHandler(txt_TextChanged);

                //txtMaDonVi.Text = GlobalSettings.MA_DON_VI;
                //txtTenDonVi.Text = GlobalSettings.TEN_DON_VI.ToUpper();
                //txtDiaChiDonVi.Text = GlobalSettings.DIA_CHI.ToUpper();
                //txtSoDienThoaiDonVi.Text = GlobalSettings.SoDienThoaiDN;
                // Hai quan
                string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;

                ctrCoQuanHaiQuan.TextChanged -= new EventHandler(txt_TextChanged);
                ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
                ctrCoQuanHaiQuan.TextChanged += new EventHandler(txt_TextChanged);

                //ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS; //Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(MaHaiQuan, "MaHQ"); //MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
                //Ma tien te 
                MaTienTe_TK = GlobalSettings.NGUYEN_TE_MAC_DINH;

                ctrMaTTHoaDon.TextChanged -= new EventHandler(txt_TextChanged);
                ctrMaTTHoaDon.Code = MaTienTe_TK;
                ctrMaTTHoaDon.TextChanged += new EventHandler(txt_TextChanged);

                //ctrMaTTHoaDon.Code = MaTienTe_TK;

#if KD_V4
                ctrMaLoaiHinh.TextChanged -= new EventHandler(txt_TextChanged);
                ctrMaLoaiHinh.Code = "A11";
                ctrMaLoaiHinh.TextChanged += new EventHandler(txt_TextChanged);

                //ctrMaLoaiHinh.Code = "A11";
#elif SXXK_V4
                if(Company.KDT.SHARE.Components.Globals.LaDNCX)
                {
                    ctrMaLoaiHinh.TextChanged -= new EventHandler(txt_TextChanged);
                    ctrMaLoaiHinh.Code = "E11";
                    ctrMaLoaiHinh.TextChanged += new EventHandler(txt_TextChanged);

                    //ctrMaLoaiHinh.Code = "E11";
                }
                else
                {
                    ctrMaLoaiHinh.TextChanged -= new EventHandler(txt_TextChanged);
                    ctrMaLoaiHinh.Code = "E31";
                    ctrMaLoaiHinh.TextChanged += new EventHandler(txt_TextChanged);

                    //ctrMaLoaiHinh.Code = "E31";
                }
#elif GC_V4
                ctrMaLoaiHinh.TextChanged -= new EventHandler(txt_TextChanged);
                ctrMaLoaiHinh.Code = "E21";
                ctrMaLoaiHinh.TextChanged += new EventHandler(txt_TextChanged);

                //ctrMaLoaiHinh.Code = "E21";
#endif
#if SXXK_V4 || GC_V4

                rdbNPL.CheckedChanged -= new EventHandler(txt_TextChanged);
                rdbNPL.Checked = true;
                rdbNPL.CheckedChanged += new EventHandler(txt_TextChanged);

                ////rdbNPL.Checked = true;
#endif

            }
            else
            {
                MaTienTe_TK = ctrMaTTHoaDon.Code;

            }

        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThemHang":
                    showThemHang();
                    break;
                case "cmdThemHangExcel":
                    showThemHangExcel();
                    break;
                case "cmdThemHangDon":
                    showThemHang();
                    break;
                case "cmdChiThiHQ":
                    showChiThiHQ();
                    break;
                case "cmdGiayPhep":
                    showGiayPhep();
                    break;
                case "cmdDinhKemDT":
                    showDinhKem();
                    break;
                case "cmdTrungChuyen":
                    showTrungChuyen();
                    break;
                case "cmdLuu":
                    saveToKhai();
                    break;
                case "cmdIDA":
                    SendVnaccs(false);
                    break;
                case "cmdIDA01":
                    SendVnaccs(true);
                    break;
                case "cmdIID":
                    GetIID(true);
                    break;
                case "cmdIDC":
                    SendVnaccsIDC(false);
                    break;
                case "cmdIDE":
                    SendVnaccsIDC(true);
                    break;
                case "cmdToKhaiTriGia":
                    showToKhaiTriGia();
                    break;
                case "cmdKetQuaTraVe":
                    ShowKetQuaTraVe();
                    break;
                case "cmdKhaiBaoSua":
                    if (this.ShowMessage("Bạn muốn chuyển sang trạng thái sửa tờ khai", true) == "Yes")
                    {
                        TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoSua;
                        TKMD.InsertUpdateFull();
                        SetToKhai();
                        setCommandStatus();
                    }
                    break;
                case "cmdIDB":
                    GetIID(false);
                    break;
                case "cmdIDD":
                    GetIDD(true);
                    break;
                case "cmdKetQuaXuLy":
                    ShowKetQuaXuLy();
                    break;
                case "cmdInAn":
                    InToKhaiTam();
                    break;
                case "cmdReloadData":
                    ReloadData();
                    break;
                case "cmdBoSungCont":
                    showDanhSachCont();
                    break;
                case "cmdMau30":
                    Mau30();
                    break;
                case "cmdPrint":
                    ReportViewVNACCSToKhaiNhap();
                    break;
                case "cmdSendVoucher":
                    SendVNACCS_Vouchers();
                    break;
                case "cmdToKhaiNhanh":
                    ShowDanhSachTKNhanh();
                    break;

            }
        }

        private void ShowDanhSachTKNhanh()
        {
            VNACC_ToKhaiMauDichNhanhManagerForm f = new VNACC_ToKhaiMauDichNhanhManagerForm();
            if (TKMD.SoToKhaiDauTien.ToString()=="F")
            {
                if (ToKhaiMauDichCollection.Count > 0)
                {
                    f.TKMDCollection = ToKhaiMauDichCollection;
                }
                else
                {
                    if (TKMD.SoToKhai !=0)
                    {
                        f.TKMDCollection = KDT_VNACC_ToKhaiMauDich.SelectCollectionDynamic("SoToKhaiDauTien='" + TKMD.SoToKhai + "'", "ID");
                    }
                    else
                    {
                        f.TKMDCollection = KDT_VNACC_ToKhaiMauDich.SelectCollectionDynamic("SoHoaDon='" + TKMD.SoHoaDon.ToString() + "' AND SoToKhaiDauTien NOT IN ('F')", "ID");
                    }
                }   
            }
            f.ShowDialog(this);
        }

        private void SendVNACCS_Vouchers()
        {
            VNACC_VouchersForm f = new VNACC_VouchersForm();
            f.FormType = "TKMD";
            f.TKMD = TKMD;
            f.ShowDialog(this);
        }

        private void ReportViewVNACCSToKhaiNhap()
        {
            ProcessReport.ShowReport(TKMD, "ReportViewVNACCSToKhaiNhap");
        }
        private void Mau30()
        {
            if (TKMD.SoLuongCont == 0 && (TKMD.MaPhuongThucVT == "3" || TKMD.MaPhuongThucVT == "1"))
            {
                Company.Interface.Report.VNACCS.BangKeHangHoaQuaXNKVGS_M30_TT38 bangkeCont = new Company.Interface.Report.VNACCS.BangKeHangHoaQuaXNKVGS_M30_TT38();
                bangkeCont.BindReport(TKMD.ID);
                bangkeCont.ShowRibbonPreview();
                
            }
            else
            {
                ShowMessage("Mẫu 30 không sử dụng cho tờ khai này", false);
                //MessageBox.Show("Mẫu 30 không sử dụng cho tờ khai này", "Thông báo", MessageBoxButtons.OK);
            }
            
        }
        private void getLoaiHangHoa()
        {
#if SXXK_V4 ||GC_V4
            if (rdbThietBi.Checked)
            {
                loaiHangHoa = "T";
            }
            else if (rdbSanPham.Checked)
            {
                loaiHangHoa = "S";
            }
            else if (rdbNPL.Checked)
            {
                loaiHangHoa = "N";
            }
            else if (rdbHangMau.Checked)
            {
                loaiHangHoa = "H";
            }
#endif
        }
        private void showThemHang()
        {

            VNACC_HangMauDichNhapForm f = new VNACC_HangMauDichNhapForm();
            f.TKMD = this.TKMD;
            f.MaTienTe = this.MaTienTe_TK;
            f.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
            getLoaiHangHoa();
            f.loaiHangHoa = loaiHangHoa;
#endif
            f.ShowDialog();
            this.SetChange(f.IsChange);
            LoadGrid();
        }
        private void showChiThiHQ()
        {
            VNACC_ChiThiHaiQuanForm f = new VNACC_ChiThiHaiQuanForm();
            f.Master_ID = this.TKMD.ID;
            f.LoaiThongTin = ELoaiThongTin.TK;
            f.ShowDialog();
        }
        private void showGiayPhep()
        {
            //Company.Interface.VNACCS.CO.VNACCS_COForm f = new Company.Interface.VNACCS.CO.VNACCS_COForm();
            VNACC_TK_GiayPhepForm f = new VNACC_TK_GiayPhepForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
            this.SetChange(f.IsChange);
        }
        private void showDinhKem()
        {
            VNACC_TK_DinhKiemDTForm f = new VNACC_TK_DinhKiemDTForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
            this.SetChange(f.IsChange);
        }
        private void showTrungChuyen()
        {
            VNACC_TK_TrungChuyenForm f = new VNACC_TK_TrungChuyenForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
            this.SetChange(f.IsChange);

        }
        private void sovandon()
        {
            KDT_VNACC_TK_SoVanDon cd = new KDT_VNACC_TK_SoVanDon();

        }
        private void showToKhaiTriGia()
        {
            VNACC_TK_TKTriGiaForm f = new VNACC_TK_TKTriGiaForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
            this.SetChange(f.IsChange);
        }
        private void showDanhSachCont()
        {
            ContainerBSManagerForm frm = new ContainerBSManagerForm();
            frm.MaDoanhNghiep = TKMD.MaDonVi;
            frm.TKMD = TKMD;
            if (TKMD.MaDDLuuKho.Trim() != "")
                frm.MaHaiQuan = TKMD.MaDDLuuKho.Substring(0, 4);
            else
                frm.MaHaiQuan = TKMD.CoQuanHaiQuan;
            frm.ShowDialog();
       
        }
        private void saveToKhai()
        {
            try
            {
                
                this.Cursor = Cursors.WaitCursor;
                //if (!Company.KDT.SHARE.Components.Globals.IsMaPhanLoaiHangHoa)
                //    ctrMaPhanLoaiHH.Code = "";
                //flag = false;
                if (!ValidateForm(false))
                    return;
#if SXXK_V4 ||GC_V4
                List<string> mahang = new List<string>();
                string mahangloi = "";
                string checkMaQuanLy = string.Empty;
                foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
                {
                    hmd.tkxuat = false;
                    if (!checkMaHang(hmd.MaHangHoa, loaiHangHoa))
                    {
                        mahang.Add(hmd.MaHangHoa);
                    }
                    if (hmd.MaQuanLy == "" || hmd.MaQuanLy == null)
                    {
                        checkMaQuanLy = checkMaQuanLy + " - " + hmd.MaHangHoa;
                    }
                }

                if (!string.IsNullOrEmpty(checkMaQuanLy))
                    if (ShowMessage("THÔNG TIN HÀNG [" + checkMaQuanLy + " ] KHÔNG CÓ MÃ QUẢN LÝ RIÊNG. DOANH NGHIỆP CÓ MUỐN KIỂM TRA LẠI THÔNG TIN HÀNG.", true) == "Yes")
                        return;
                if (loaiHangHoa == "N" && mahang.Count > 0)
                {
                    if (mahang.Count > 0)
                    {
                        mahangloi = "[STT] - [MÃ NGUYÊN PHỤ LIỆU]\n\n";
                        for (int i = 0; i < mahang.Count; i++)
                        {
                            mahangloi += "[" + ( i + 1) + "] - [" + mahang[i].ToString() + "]\n";
                        }
                        MLMessages(mahangloi + "\nCHƯA ĐƯỢC KHAI BÁO ĐẾN HQ. DOANH NGHIỆP HÃY KHAI BÁO NPL TRƯỚC KHI KHAI TỜ KHAI.", "MSG_PUB06", "", false);
                        
                    }

                }
                else if (loaiHangHoa == "S")
                {
                    if (mahang.Count > 0)
                    {
                        mahangloi = "[STT] - [MÃ SẢN PHẨM]\n\n";
                        for (int i = 0; i < mahang.Count; i++)
                        {
                            mahangloi += "[" + (i + 1) + "] - [" + mahang[i].ToString() + "]\n";
                        }
                        MLMessages(mahangloi + "\nCHƯA ĐƯỢC KHAI BÁO ĐỊNH MỨC. DOANH NGHIỆP HÃY KHAI BÁO ĐỊNH MỨC TRƯỚC KHI KHAI TỜ KHAI. ", "MSG_PUB06", "", false);
                        
                    }
                }
                else if (loaiHangHoa=="T")
                {
                    if (mahang.Count > 0)
                    {
                        mahangloi = "[STT] - [MÃ THIẾT BỊ]\n\n";
                        for (int i = 0; i < mahang.Count; i++)
                        {
                            mahangloi += "[" + (i + 1) + "] - [" + mahang[i].ToString() + "]\n";
                        }
                        MLMessages(mahangloi + "\nCHƯA ĐƯỢC KHAI BÁO ĐẾN HQ. DOANH NGHIỆP HÃY KHAI BÁO THIẾT BỊ NÀY TRƯỚC KHI KHAI TỜ KHAI. ", "MSG_PUB06", "", false);
                        return;
                    }                    
                }
#endif

                //minhnd fix tờ khai nhập KD
                foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
                {
                    //if (hmd.tkxuat== false)
                    hmd.tkxuat = false;
                }
                //

                string valueTemp = "";
                if (TKMD.VanDonCollection.Count > 0)
                {
                    if (TKMD.VanDonCollection[0].LoaiDinhDanh == "2061")
                    {
                        if (ShowMessageTQDT("Thực hiện Quyết định số  2061/QĐ-BTC ngày 13/10/2017 của Bộ tài chính về việc áp dụng thí điểm khai số quản lý đối với hàng hóa xuât khẩu, nhập khẩu, quá cảng, quản lý, giám sát Hải quan tự động tại Cảng hàng không quốc tế Nội Bài. \r\n -Đối với tờ khai nhập khẩu: \r\n1.Trường hợp gửi hàng qua forwarde (Có phát hàng HAWB)\r\nDoanh nghiệp nhập đầy đủ thông tin về Số vận đơn chủ MAWB, Số  vận đơn thứ HAWB vànăm phát hành số vận đơn chủ  MAWB.\r\nChương trình  sẽ tự động  chuyển số vận đơn chủ MAWB và năm vận đơn chủ MAWB xuống chỉ tiêu Ký hiệu và số hiệu bao bì để khai báo tới hải quan theo cấu trúc như sau : ##XYYYYNNNNNNNNNNN##.\r\n2. Trường hợp gừi hàng trực tiếp hãng hàng không (không phát hành HAWB)\r\nDoanh nghiệp tích chọn vào tiệu Chí Chỉ có số MAWB trện phần mềm. Thực hiện nhập đầy đù Số vận đơn MAWB và năm phát hành vận đơn MAWB. Chương trình sẽ tự động Chuyển dữ liệu về năm phát hành vận đơn MAWB xuông chỉ tiệu Ký hiệu và số hiệu bao bì để khai báo tới hệ thống hải quan theo cấu trúc như sau: ##XYYYY##.", true) == "Yes")
                        {
                            if (txtSoHieuKyHieu.Text.Length > 0)
                            {
                                if (txtSoHieuKyHieu.Text.ToString().Contains("##1"))
                                {
                                    string[] temp = txtSoHieuKyHieu.Text.Split(new string[] { "##" }, StringSplitOptions.None);
                                    int count = 0;
                                    foreach (String item in temp)
                                    {
                                        count++;
                                    }
                                    if (temp.Length > 0)
                                    {
                                        valueTemp = temp[count - 1].ToString();
                                    }
                                    int i = 1;
                                    if (TKMD.VanDonCollection.Count > 0)
                                    {
                                        KyHieuVaSoHieu = "";
                                        foreach (KDT_VNACC_TK_SoVanDon item in TKMD.VanDonCollection)
                                        {
                                            if (TKMD.VanDonCollection[0].LoaiDinhDanh == "2061")
                                            {
                                                if (!String.IsNullOrEmpty(TKMD.VanDonCollection[0].SoVanDon.ToString()))
                                                {
                                                    KyHieuVaSoHieu += "##" + item.SoTT + "" + item.NamVanDonChu + "" + item.SoVanDonChu + ";";
                                                }
                                                else
                                                {
                                                    KyHieuVaSoHieu += "##" + item.SoTT + "" + item.NamVanDonChu + ";";
                                                }
                                            }
                                            else
                                            {
                                                KyHieuVaSoHieu += "##" + item.SoTT + "" + item.NamVanDonChu + ";";
                                            }
                                        }
                                        KyHieuVaSoHieu = KyHieuVaSoHieu.Substring(0, KyHieuVaSoHieu.Length - 1);
                                        txtSoHieuKyHieu.Text = KyHieuVaSoHieu + "##" + valueTemp;
                                    }
                                }
                                else
                                {
                                    valueTemp = txtSoHieuKyHieu.Text;
                                    int i = 1;
                                    if (TKMD.VanDonCollection.Count > 0)
                                    {
                                        KyHieuVaSoHieu = "";
                                        foreach (KDT_VNACC_TK_SoVanDon item in TKMD.VanDonCollection)
                                        {
                                            if (TKMD.VanDonCollection[0].LoaiDinhDanh == "2061")
                                            {
                                                KyHieuVaSoHieu += "##" + item.SoTT + "" + item.NamVanDonChu + "" + item.SoVanDonChu + ";";
                                            }
                                            else
                                            {
                                                KyHieuVaSoHieu += "##" + item.SoTT + "" + item.NamVanDonChu + ";";
                                            }
                                        }
                                        KyHieuVaSoHieu = KyHieuVaSoHieu.Substring(0, KyHieuVaSoHieu.Length - 1);
                                        txtSoHieuKyHieu.Text = KyHieuVaSoHieu + "##" + valueTemp;
                                    }
                                }
                            }
                            else
                            {
                                valueTemp = txtSoHieuKyHieu.Text;
                                int i = 1;
                                if (TKMD.VanDonCollection.Count > 0)
                                {
                                    KyHieuVaSoHieu = "";
                                    foreach (KDT_VNACC_TK_SoVanDon item in TKMD.VanDonCollection)
                                    {
                                        if (TKMD.VanDonCollection[0].LoaiDinhDanh == "2061")
                                        {
                                            KyHieuVaSoHieu += "##" + item.SoTT + "" + item.NamVanDonChu + "" + item.SoVanDonChu + ";";
                                        }
                                        else
                                        {
                                            KyHieuVaSoHieu += "##" + item.SoTT + "" + item.NamVanDonChu + ";";
                                        }
                                    }
                                    KyHieuVaSoHieu = KyHieuVaSoHieu.Substring(0, KyHieuVaSoHieu.Length - 1);
                                    txtSoHieuKyHieu.Text = KyHieuVaSoHieu + "##" + valueTemp;
                                }
                            }
                        }
                    }
                    else if (TKMD.VanDonCollection[0].LoaiDinhDanh == "1563")
                    {
                       if (ShowMessageTQDT("Thực hiện Quyết định của Bộ tài chính về việc thí điểm khai số vận đơn trên tờ khai hải quan đối với hàng hóa xuất khẩu , nhập khẩu  (đối với hàng hóa đường biển) cần thực hiện đăng ký số quản lý hàng hóa (Số định danh) khi thực hiện khai báo tờ khai .\n\nVới tờ khai nhập khẩu : Hệ thống Hải quan sẽ tự động cấp số định danh dựa vào Thông tin Số vận đơn và Ngày vận đơn mà doanh nghiệp khai báo trên tờ khai theo nguyên tắc : DDMMYYSOVANDON (Trong đó DDMMYY là Ngày vận đơn  , SOVANDON là Số vận đơn ).\n\n Trong phần mềm Softech sau khi Nhập các Thông tin về Số vận đơn và Ngày vận đơn phần mềm sẽ tự động cập nhật lại Số vận đơn theo yêu cầu của HQ .\n\nBạn chọn Đồng ý để phần mềm Tự động cập nhật lại Số vận đơn .", true) == "Yes")
                        {
                            if (txtSoHieuKyHieu.Text.Length > 0)
                            {
                                if (txtSoHieuKyHieu.Text.ToString().Contains("##1"))
                                {
                                    string[] temp = txtSoHieuKyHieu.Text.Split(new string[] { "##" }, StringSplitOptions.None);
                                    int count = 0;
                                    foreach (String item in temp)
                                    {
                                        count++;
                                    }
                                    if (temp.Length > 0)
                                    {
                                        valueTemp = temp[count - 1].ToString();
                                    }
                                    int i = 1;
                                    if (TKMD.VanDonCollection.Count > 0)
                                    {
                                        KyHieuVaSoHieu = "";
                                        foreach (KDT_VNACC_TK_SoVanDon item in TKMD.VanDonCollection)
                                        {
                                            if (TKMD.VanDonCollection[0].LoaiDinhDanh == "2061")
                                            {
                                                KyHieuVaSoHieu += "##" + item.SoTT + "" + item.NamVanDonChu + "" + item.SoVanDonChu + ";";
                                            }
                                            else
                                            {
                                                KyHieuVaSoHieu += "##" + item.SoTT + "" + item.NamVanDonChu + ";";
                                            }
                                        }
                                        KyHieuVaSoHieu = KyHieuVaSoHieu.Substring(0, KyHieuVaSoHieu.Length - 1);
                                        txtSoHieuKyHieu.Text = KyHieuVaSoHieu + "##" + valueTemp;
                                    }
                                }
                                else
                                {
                                    valueTemp = txtSoHieuKyHieu.Text;
                                    int i = 1;
                                    if (TKMD.VanDonCollection.Count > 0)
                                    {
                                        KyHieuVaSoHieu = "";
                                        foreach (KDT_VNACC_TK_SoVanDon item in TKMD.VanDonCollection)
                                        {
                                            if (TKMD.VanDonCollection[0].LoaiDinhDanh == "2061")
                                            {
                                                KyHieuVaSoHieu += "##" + item.SoTT + "" + item.NamVanDonChu + "" + item.SoVanDonChu + ";";
                                            }
                                            else
                                            {
                                                KyHieuVaSoHieu += "##" + item.SoTT + "" + item.NamVanDonChu + ";";
                                            }
                                        }
                                        KyHieuVaSoHieu = KyHieuVaSoHieu.Substring(0, KyHieuVaSoHieu.Length - 1);
                                        txtSoHieuKyHieu.Text = KyHieuVaSoHieu + "##" + valueTemp;
                                    }
                                }
                            }
                            else
                            {
                                valueTemp = txtSoHieuKyHieu.Text;
                                int i = 1;
                                if (TKMD.VanDonCollection.Count > 0)
                                {
                                    KyHieuVaSoHieu = "";
                                    foreach (KDT_VNACC_TK_SoVanDon item in TKMD.VanDonCollection)
                                    {
                                        if (TKMD.VanDonCollection[0].LoaiDinhDanh == "2061")
                                        {
                                            KyHieuVaSoHieu += "##" + item.SoTT + "" + item.NamVanDonChu + "" + item.SoVanDonChu + ";";
                                        }
                                        else
                                        {
                                            KyHieuVaSoHieu += "##" + item.SoTT + "" + item.NamVanDonChu + ";";
                                        }
                                    }
                                    KyHieuVaSoHieu = KyHieuVaSoHieu.Substring(0, KyHieuVaSoHieu.Length - 1);
                                    txtSoHieuKyHieu.Text = KyHieuVaSoHieu + "##" + valueTemp;
                                }
                            }
                        }
                    }                    
                }
                //    if (TKMD.VanDonCollection[0].LoaiDinhDanh == "1563")
                //    {
                //        valueTemp = "";
                //        foreach (KDT_VNACC_TK_SoVanDon item in TKMD.VanDonCollection)
                //        {
                //            valueTemp += item.NgayVanDon.ToString("dd/MM/yyyy").Replace("/", "") + "" + item.SoVanDon + ";";
                //        }
                //        valueTemp = valueTemp.Substring(0, valueTemp.Length - 1);
                //        txtSoVanDon.Text = valueTemp;
                //    }
                //    else if (TKMD.VanDonCollection[0].LoaiDinhDanh == "2061" || TKMD.VanDonCollection[0].LoaiDinhDanh == "2722")
                //    {
                //        if (TKMD.VanDonCollection[0].SoVanDon.ToString() != txtSoVanDon.Text)
                //        {
                //            foreach (KDT_VNACC_TK_SoVanDon item in TKMD.VanDonCollection)
                //            {
                //                valueTemp += item.NamVanDonChu + item.SoVanDonChu.ToString().Trim() + item.SoVanDon + ";";
                //            }
                //            valueTemp = valueTemp.Substring(0, valueTemp.Length - 1);
                //            string[] split = valueTemp.Split(' ');
                //            string valueTempNew = "";
                //            foreach (string word in split)
                //            {
                //                valueTempNew += word;
                //            }
                //            txtSoVanDon.Text = valueTempNew;
                //        }
                //    }
                //}
                GetToKhai();

                if (TKMD.HangCollection.Count == 0)
                {
                    ShowMessage("Chưa nhập thông tin hàng", false);
                }
                else
                {
                    // Check Bill and Invoice
                    try
                    {
                        if (!String.IsNullOrEmpty(TKMD.SoHoaDon) || TKMD.VanDonCollection.Count > 0)
                        {
                            string listVanDon = "";
                            string SoHoaDon = "";
                            for (int i = 0; i < TKMD.VanDonCollection.Count; i++)
                            {
                                   listVanDon +="'"+ TKMD.VanDonCollection[i].SoDinhDanh.ToString() +"',";
                            }
                            if (!String.IsNullOrEmpty(listVanDon))
                            {
                                listVanDon = listVanDon.Substring(0, listVanDon.Length - 1);
                            }
                            else
                            {
                                listVanDon = "''";
                            }
                            if (!String.IsNullOrEmpty(TKMD.SoHoaDon))
                            {
                                 SoHoaDon = TKMD.SoHoaDon;
                            }
                            else
                            {
                                SoHoaDon = "''";
                            }
                            string result = KDT_VNACC_ToKhaiMauDich.CheckBillAndInvoice(SoHoaDon, listVanDon, TKMD.SoToKhai);
                            if (!String.IsNullOrEmpty(result))
                            {
                                if (result != "0")
                                {
                                    if (ShowMessage("Thông tin Bill và Invoce của tờ khai trùng với tờ khai có số tờ khai : " + result + "\nBạn có muốn kiểm tra lại không ?", true) == "Yes")
                                    {
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    if (TKMD.HangCollection.Count > 0)
                    {
                        // Tách thành các tờ khai nhánh .
                        if (TKMD.HangCollection.Count >= 51)
                        {
                            foreach (KDT_VNACC_HangMauDich item in TKMD.HangCollection)
                            {
                                TongSoLuongHang += item.SoLuong1;
                                TongTriGiaHD += item.TriGiaHoaDon;
                            }
                            int SoToKhaiNhanh = TKMD.HangCollection.Count / 50;
                            if (TKMD.HangCollection.Count % 50 != 0)
                                SoToKhaiNhanh += 1;
                            if (ShowMessage("Tổng số dòng hàng nhập vào là : " + TKMD.HangCollection.Count + " > 50 dòng hàng . Phần mềm sẽ tự động tách ra thành  : " + SoToKhaiNhanh + " tờ khai nhánh", true) == "Yes")
                            {
                                ToKhaiMauDichCollection = new List<KDT_VNACC_ToKhaiMauDich>();
                                TKMD.SoToKhaiDauTien = "F";
                                TKMD.SoNhanhToKhai = 1;
                                TKMD.TongSoTKChiaNho = SoToKhaiNhanh;
                                ToKhaiMauDichCollection = CopyToKhai(TKMD, true, SoToKhaiNhanh);

                                TKMD.HangCollection = TKMD.HangCollection.GetRange(0, 50);
                                foreach (KDT_VNACC_HangMauDich item in TKMD.HangCollection)
                                {
                                    TongSoLuongHangTK += item.SoLuong1;
                                    TongHeSoPhanBo += item.TriGiaHoaDon;
                                }
                                TKMD.TongTriGiaHD = TongTriGiaHD;
                                TKMD.TongHeSoPhanBoTG = TongHeSoPhanBo;
                                TKMD.TriGiaTinhThue = TongHeSoPhanBo;
                                TKMD.MaTTTriGiaTinhThue = TKMD.MaTTHoaDon;
                                //Phân bổ Phí và các khoản điều chỉnh .
                                if (TKMD.PhiVanChuyen != 0)
                                    TKMD.PhiVanChuyen = (TKMD.PhiVanChuyen / TongSoLuongHang) * TongSoLuongHangTK;
                                if (TKMD.PhiBaoHiem != 0)
                                    TKMD.PhiBaoHiem = (TKMD.PhiBaoHiem / TongSoLuongHang) * TongSoLuongHangTK;

                                if (TKMD.KhoanDCCollection.Count > 0)
                                {
                                    foreach (KDT_VNACC_TK_KhoanDieuChinh item in TKMD.KhoanDCCollection)
                                    {
                                        item.TriGiaKhoanDieuChinh = (item.TriGiaKhoanDieuChinh / TongSoLuongHang) * TongSoLuongHangTK;
                                        item.TongHeSoPhanBo = TongHeSoPhanBo;
                                    }
                                }
                                VNACC_ToKhaiMauDichNhanhManagerForm f = new VNACC_ToKhaiMauDichNhanhManagerForm();
                                f.TKMDCollection = ToKhaiMauDichCollection;
                                f.ShowDialog(this);
                            }
                        }
                    }
                    // Kiểm tra Số lượng 2 và ĐVT số lượng 2
                    int count = 0;
                    foreach (KDT_VNACC_HangMauDich item in TKMD.HangCollection)
                    {
                        if (item.SoLuong2 == 0 || String.IsNullOrEmpty(item.DVTLuong2))
                        {
                            count++;
                        }
                    }
                    if (count > 0)
                    {
                        if (this.ShowMessage("Tờ khai này có : " + count + " dòng hàng chưa nhập Số lượng 2 hoặc ĐVT Số lượng 2 .Doanh nghiệp có muốn kiểm tra lại tờ khai không ?", true) == "Yes")
                        {
                            return;
                        }
                    }
                    if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam)
                    {
                        if (this.ShowMessage("Tờ khai này đã được khai báo, bạn muốn thay đổi thông tin và khai báo lại ?", true) == "Yes")
                        {
                            TKMD.TrangThaiXuLy = EnumTrangThaiXuLy.ChuaKhaiBao;
                            //TKMD.SoToKhai = 0;
                            TKMD.InsertUpdateFull();
                            ShowMessage("Lưu tờ khai thành công", false);
                            LoadGrid();
                            SetToKhai();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        TKMD.InsertUpdateFull();
                        ShowMessage("Lưu tờ khai thành công", false);
                        LoadGrid();
                        SetToKhai();
                    }
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

        }
        private List<KDT_VNACC_ToKhaiMauDich> CopyToKhai(KDT_VNACC_ToKhaiMauDich tkmd, bool isCopyHangHoa, int number)
        {
            try
            {
                List<KDT_VNACC_ToKhaiMauDich> ToKhaiMauDichCollection = new List<KDT_VNACC_ToKhaiMauDich>();
                int index = 50;
                int count = 50;
                for (int i = 1; i < number; i++)
                {
                    KDT_VNACC_ToKhaiMauDich tkmdcopy = new KDT_VNACC_ToKhaiMauDich();
                    HelperVNACCS.UpdateObject<KDT_VNACC_ToKhaiMauDich>(tkmdcopy, tkmd, false);
                    tkmdcopy.NgayDangKy = new DateTime(1900, 1, 1);
                    tkmdcopy.SoToKhai = 0;
                    tkmdcopy.SoNhanhToKhai = 0;
                    tkmdcopy.SoToKhaiDauTien = "";
                    tkmdcopy.SoNhanhToKhai = i + 1;
                    tkmdcopy.TongSoTKChiaNho = number;
                    tkmdcopy.MaPhanLoaiKiemTra = string.Empty;
                    tkmdcopy.AnHangCollection = new List<KDT_VNACC_TK_PhanHoi_AnHan>();
                    tkmdcopy.ChiThiHQCollection = new List<KDT_VNACC_ChiThiHaiQuan>();
                    tkmdcopy.SacThueCollection = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
                    tkmdcopy.TrangThaiXuLy = "0";
                    tkmdcopy.TyGiaCollection = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
                    tkmdcopy.InputMessageID = string.Empty;
                    tkmdcopy.MessageTag = string.Empty;
                    tkmdcopy.IndexTag = string.Empty;
                    tkmdcopy.HangCollection = new List<KDT_VNACC_HangMauDich>();
                    if (tkmd.HangCollection.Count >= 50 + count)
                    {
                        tkmdcopy.HangCollection = tkmd.HangCollection.GetRange(index, count);
                    }
                    else
                    {
                        tkmdcopy.HangCollection = tkmd.HangCollection.GetRange(index, tkmd.HangCollection.Count - index);
                    }
                    //Phân bổ Phí và các khoản điều chỉnh .
                    TongSoLuongHangTK = 0;
                    TongHeSoPhanBo = 0;
                    foreach (KDT_VNACC_HangMauDich item in tkmdcopy.HangCollection)
                    {
                        TongSoLuongHangTK += item.SoLuong1;
                        TongHeSoPhanBo += item.TriGiaHoaDon;
                    }
                    tkmdcopy.TongTriGiaHD = TongTriGiaHD;
                    tkmdcopy.TongHeSoPhanBoTG = TongHeSoPhanBo;
                    tkmdcopy.TriGiaTinhThue = TongHeSoPhanBo;
                    tkmdcopy.MaTTTriGiaTinhThue = tkmd.MaTTHoaDon;
                    if (TKMD.PhiVanChuyen != 0)
                        tkmdcopy.PhiVanChuyen = (TKMD.PhiVanChuyen / TongSoLuongHang) * TongSoLuongHangTK;
                    if (TKMD.PhiBaoHiem != 0)
                        tkmdcopy.PhiBaoHiem = (TKMD.PhiBaoHiem / TongSoLuongHang) * TongSoLuongHangTK;
                    tkmdcopy.KhoanDCCollection = new List<KDT_VNACC_TK_KhoanDieuChinh>();
                    if (TKMD.KhoanDCCollection.Count > 0)
                    {
                        tkmdcopy.KhoanDCCollection = TKMD.KhoanDCCollection;
                        foreach (KDT_VNACC_TK_KhoanDieuChinh item in tkmdcopy.KhoanDCCollection)
                        {
                            item.TriGiaKhoanDieuChinh = (item.TriGiaKhoanDieuChinh / TongSoLuongHang) * TongSoLuongHangTK;
                            item.TongHeSoPhanBo = TongHeSoPhanBo;
                        }
                    }

                    index += 50;
                    count += 49;
                    tkmdcopy.InsertUpdateFull();
                    tkmdcopy.LoadFull();
                    ToKhaiMauDichCollection.Add(tkmdcopy);
                }
                return ToKhaiMauDichCollection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void autoFixTenHang() 
        {
            try
            {
                if (TKMD.LoaiHang == "N") 
                {
                    
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
        }
        private void showThemHangExcel()
        {
            ReadExcelFormVNACCS f = new ReadExcelFormVNACCS();
            f.TKMD = TKMD;
            f.MaTienTen_HMD = MaTienTe_TK;
            f.LoaiHinh = "N";
            f.ShowDialog();
            this.SetChange(f.IsSuccess);
            LoadGrid();

        }

        private void grListHang_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            VNACC_HangMauDichNhapForm f = new VNACC_HangMauDichNhapForm();
            f.TKMD = TKMD;
            f.ShowDialog();
        }

        #region Bổ sung nghiệp vụ

        private Janus.Windows.UI.CommandBars.UICommand cmdIDA;
        private Janus.Windows.UI.CommandBars.UICommand cmdIDB;
        private Janus.Windows.UI.CommandBars.UICommand cmdIDC;
        private Janus.Windows.UI.CommandBars.UICommand cmdIID;
        private Janus.Windows.UI.CommandBars.UICommand cmdIDA01;
        private Janus.Windows.UI.CommandBars.UICommand cmdIDD;
        private Janus.Windows.UI.CommandBars.UICommand cmdIDE;
        //private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaTraVe;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBaoSua;
        private Janus.Windows.UI.CommandBars.UICommand Separator;
        private Janus.Windows.UI.CommandBars.UICommand Separator2;
        private Janus.Windows.UI.CommandBars.UICommand Separator3;
        private void BoSungNghiepVu()
        {


            this.cmdIDA = new Janus.Windows.UI.CommandBars.UICommand("cmdIDA");
            this.cmdIDA.Key = "cmdIDA";
            this.cmdIDA.Name = "cmdIDA";
            this.cmdIDA.Text = "nghiệp vụ IDA (Khai báo tờ khai tạm)";

            this.cmdIDB = new Janus.Windows.UI.CommandBars.UICommand("cmdIDB");
            this.cmdIDB.Key = "cmdIDB";
            this.cmdIDB.Name = "cmdIDB";
            this.cmdIDB.Text = "nghiệp vụ IDB (Gọi tờ khai tạm) ";

            this.cmdIDC = new Janus.Windows.UI.CommandBars.UICommand("cmdIDC");
            this.cmdIDC.Key = "cmdIDC";
            this.cmdIDC.Name = "cmdIDC";
            this.cmdIDC.Text = "Nghiệp vụ IDC (Khai báo chính thức tờ khai)";

            this.cmdIDA01 = new Janus.Windows.UI.CommandBars.UICommand("cmdIDA01");
            this.cmdIDA01.Key = "cmdIDA01";
            this.cmdIDA01.Name = "cmdIDA01";
            this.cmdIDA01.Text = "nghiệp vụ IDA01 (khai báo sửa tạm)";

            this.cmdIDD = new Janus.Windows.UI.CommandBars.UICommand("cmdIDD");
            this.cmdIDD.Key = "cmdIDD";
            this.cmdIDD.Name = "cmdIDD";
            this.cmdIDD.Text = "nghiệp vụ IDD (Gọi tờ khai sửa) ";

            this.cmdIID = new Janus.Windows.UI.CommandBars.UICommand("cmdIID");
            this.cmdIID.Key = "cmdIID";
            this.cmdIID.Name = "cmdIID";
            this.cmdIID.Text = "nghiệp vụ IID (tham chiếu tờ khai) ";

            this.cmdIDE = new Janus.Windows.UI.CommandBars.UICommand("cmdIDE");
            this.cmdIDE.Key = "cmdIDE";
            this.cmdIDE.Name = "cmdIDE";
            this.cmdIDE.Text = "nghiệp vụ IDE (Xác nhận sửa tờ khai) ";

            //             this.cmdKetQuaTraVe = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaTraVe");
            //             this.cmdKetQuaTraVe.Key = "cmdKetQuaTraVe";
            //             this.cmdKetQuaTraVe.Name = "cmdKetQuaTraVe";
            //             this.cmdKetQuaTraVe.Text = "Thông tin từ HQ";
            // 
            //             this.cmbMain.Commands.Add(cmdKetQuaTraVe);

            this.cmdKhaiBaoSua = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBaoSua");
            this.cmdKhaiBaoSua.Key = "cmdKhaiBaoSua";
            this.cmdKhaiBaoSua.Name = "cmdKhaiBaoSua";
            this.cmdKhaiBaoSua.Text = "Sửa TK Trong thông quan";

            this.Separator = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            Separator.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;

            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator2");
            Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;

            this.Separator3 = new Janus.Windows.UI.CommandBars.UICommand("Separator3");
            Separator3.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.cmdKhaiBaoSua.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] 
            {
                this.cmdIDA01,
                this.cmdIDE,
                this.cmdIDD
                ,});


            /*this.cmd.Text = "Cập nhật thông tin ";*/
            this.cmdKhaiBao1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] 
            {
                this.cmdIDA,
                this.cmdIDB,
                this.Separator,
                this.cmdIDC,
                Separator2,
                this.cmdKhaiBaoSua,
                Separator3,
                this.cmdIID
            });
        }

        #endregion Bổ sung nghiệp vụ

        #region Send VNACCS
        private void SendVnaccs(bool KhaiBaoSua)
        {
            ////minnd 02/04/2015
            //if (TKMD.MaDonVi == "4000395355" || TKMD.MaDonVi == "0400101556")
            //{
            //}
            //else 
            //{
            //    //minhnd 02/04/2015 không khai số hợp đồng
            //    TKMD.HopDong_So = "";
            //    //minhnd 02/04/2015
            //}
            ////minnd 02/04/2015
            
            saveToKhai();

            try
            {
                if (TKMD.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                if (string.IsNullOrEmpty(TKMD.DiaDiemDichVC) || TKMD.DiaDiemDichVC.Trim() == "")
                {
                    if (this.ShowMessage("Tờ khai chưa nhập ĐỊA ĐIỂM ĐÍCH CHO VẬN CHUYỂN BẢO THUẾ. Bạn có muốn tiếp tục?", true) == "No")
                        return;
                }
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {
                    if(TKMD.HangCollection != null && TKMD.HangCollection.Count > 0)
                    {
                        if(string.IsNullOrEmpty(TKMD.SoToKhaiDauTien))
                        {
                            decimal triGiaHang = 0;
                            foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
                            {
                                triGiaHang += hmd.TriGiaHoaDon;
                            }
                            if(triGiaHang != TKMD.TongTriGiaHD)
                            {
                                if (this.ShowMessage("Tổng trị giá hàng là : " + triGiaHang.ToString("###.###.###.###,####") + " Trong khi đó tổng trị giá hóa đơn là :" + TKMD.TongTriGiaHD.ToString("###.###.###.###,####") + " . Bạn có chắc chắn muốn khai báo  ? ", true) != "Yes")
                                    return;
                            }
                        }
                    }
                    IDA ida = VNACCMaperFromObject.IDAMapper(TKMD);
                    if (ida == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    MessagesSend msg;
                    if (!KhaiBaoSua)
                    {
                        TKMD.InputMessageID = HelperVNACCS.NewInputMSGID();
                        TKMD.InsertUpdateFull();
                        msg = MessagesSend.Load<IDA>(ida, TKMD.InputMessageID);

                    }
                    else
                    {
                        msg = MessagesSend.Load<IDA>(ida, true, EnumNghiepVu.IDA01, TKMD.InputMessageID);
                    }

                    MsgLog.SaveMessages(msg, TKMD.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = false;
                    f.inputMSGID = TKMD.InputMessageID;
                    f.ShowDialog(this);
                    if (f.DialogResult == DialogResult.Cancel)
                    {
                        this.ShowMessage(f.msgFeedBack, false);
                    }
                    else if (f.DialogResult == DialogResult.No)
                    {
                        if (f.columnErrors != null)
                        {
                            ShowValidateFormErrorSend(f.columnErrors);
                        }
                        //if (Helper.Controls.ErrorMessageBoxForm_VNACC.columnErrors!=null)
                        //{
                        //    ShowValidateFormErrorSend(Helper.Controls.ErrorMessageBoxForm_VNACC.columnErrors);
                        //}
                    }
                    else
                    {
                        string ketqua = "Khai báo thông tin thành công";
                        CapNhatThongTin(f.feedback.ResponseData);
                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                        {
                            try
                            {
                                decimal SoToKhai = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
                                ketqua = ketqua + Environment.NewLine;
                                ketqua += "Số tờ khai: " + SoToKhai;
                            }
                            catch (System.Exception ex)
                            {
                                ketqua += Environment.NewLine + "Lỗi cập nhật số tờ khai: " + Environment.NewLine + ex.Message;
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                        this.ShowMessageTQDT(ketqua, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public bool CheckVouchers()
        {
            bool isExits = true;
            List<KDT_VNACCS_License> LicenseCollection = new List<KDT_VNACCS_License>();
            List<KDT_VNACCS_ContractDocument> ContractDocumentCollection = new List<KDT_VNACCS_ContractDocument>();
            List<KDT_VNACCS_CommercialInvoice> CommercialInvoiceCollection = new List<KDT_VNACCS_CommercialInvoice>();
            List<KDT_VNACCS_CertificateOfOrigin> CertificateOfOriginCollection = new List<KDT_VNACCS_CertificateOfOrigin>();
            List<KDT_VNACCS_BillOfLading> BillOfLadingCollection = new List<KDT_VNACCS_BillOfLading>();
            List<KDT_VNACCS_Container_Detail> ContainerCollection = new List<KDT_VNACCS_Container_Detail>();
            List<KDT_VNACCS_AdditionalDocument> AdditionalDocumentCollection = new List<KDT_VNACCS_AdditionalDocument>();
            int CountAll = 0;

            LicenseCollection = KDT_VNACCS_License.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID,"");
            ContractDocumentCollection = KDT_VNACCS_ContractDocument.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID, "");
            CommercialInvoiceCollection = KDT_VNACCS_CommercialInvoice.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID, "");
            CertificateOfOriginCollection = KDT_VNACCS_CertificateOfOrigin.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID, "");
            BillOfLadingCollection = KDT_VNACCS_BillOfLading.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID, "");
            ContainerCollection = KDT_VNACCS_Container_Detail.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID, "");
            AdditionalDocumentCollection = KDT_VNACCS_AdditionalDocument.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID, "");
            if (LicenseCollection.Count >=1)
            {
                foreach (KDT_VNACCS_License item in LicenseCollection)
                {
                    if (item.SoTN != 0 && item.TrangThaiXuLy.ToString()== "0" )
                    {
                        CountAll += 1;
                    }
                }
            }
            if (ContractDocumentCollection.Count >= 1)
            {
                foreach (KDT_VNACCS_ContractDocument item in ContractDocumentCollection)
                {
                    if (item.SoTN != 0 && item.TrangThaiXuLy.ToString() == "0")
                    {
                        CountAll += 1;
                    }
                }
            }
            if (CommercialInvoiceCollection.Count >= 1)
            {
                foreach (KDT_VNACCS_CommercialInvoice item in CommercialInvoiceCollection)
                {
                    if (item.SoTN != 0 && item.TrangThaiXuLy.ToString() == "0")
                    {
                        CountAll += 1;
                    }
                }
            }
            if (CertificateOfOriginCollection.Count >= 1)
            {
                foreach (KDT_VNACCS_CertificateOfOrigin item in CertificateOfOriginCollection)
                {
                    if (item.SoTN != 0 && item.TrangThaiXuLy.ToString() == "0")
                    {
                        CountAll += 1;
                    }
                }
            }
            if (BillOfLadingCollection.Count >= 1)
            {
                foreach (KDT_VNACCS_BillOfLading item in BillOfLadingCollection)
                {
                    if (item.SoTN != 0 && item.TrangThaiXuLy.ToString() == "0")
                    {
                        CountAll += 1;
                    }
                }
            }
            if (ContainerCollection.Count >= 1)
            {
                foreach (KDT_VNACCS_Container_Detail item in ContainerCollection)
                {
                    if (item.SoTN != 0 && item.TrangThaiXuLy.ToString() == "0")
                    {
                        CountAll += 1;
                    }
                }
            }
            if (AdditionalDocumentCollection.Count >= 1)
            {
                foreach (KDT_VNACCS_AdditionalDocument item in AdditionalDocumentCollection)
                {
                    if (item.SoTN != 0 && item.TrangThaiXuLy.ToString() == "0")
                    {
                        CountAll += 1;
                    }
                }
            }
            if (CountAll==0)
            {
                isExits = false;
            }
            return isExits;
        }
        private void SendVnaccsIDC(bool KhaiBaoSua)
        {
            //if (TKMD.MaDonVi == "4000395355" ||TKMD.MaDonVi == "0400101556")
            //{
            //}
            //else
            //{
            //    //minhnd 02/04/2015 không khai số hợp đồng
            //    TKMD.HopDong_So = "";
            //    //minhnd 02/04/2015
            //}

            try
            {
                if (TKMD.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                #region Kiểm tra Chứng từ đã khai báo trước khi khai chính thức tờ khai
                //if (!CheckVouchers())
                //{
                //    if (this.ShowMessageTQDT("\nThực hiện ý kiến chỉ đạo của Lãnh đạo Tổng cục, Cục CNTT & Thống kê Hải quan sẽ thực hiện nâng cấp chức năng kiểm tra tờ khai qua khu vực giám sát có đính kèm chứng từ điện tử lên Hệ thống hải quan với nội dung như sau:\n\n- Đối với những tờ khai có ngày đăng ký từ ngày 05/06/2018 khi Thông tư số 39/2018/TT-BTC ngày 20/04/2018 có hiệu lực sẽ bổ sung điều kiện kiểm tra chỉ cho phép xác nhận hàng qua khu vực giám sát khi tờ khai có chứng từ điện tử khai lên hệ thống Hải quan (áp dụng đối với tờ khai xuất khẩu và tờ khai nhập khẩu)\n\n- Thời gian thực hiện nâng cấp: từ ngày 17/09/2018.\n\nCho phép và yêu cầu khai chứng từ điện tử ngay sau khi IDA/EDA (ko cần chờ khai IDC/EDC như trước) --> Hải quan sẽ kiểm tra điều kiện xác nhận hàng qua KVGS nếu có khai chứng từ theo CV 1318/CNTT-PTUD. Về các loại chứng từ đính kèm các bạn tham khảo Điều 16 TT39 sửa đổi .\n\nTờ khai chưa khai báo Thành công bất kỳ chứng từ đính kèm nào ! Tờ khai sẽ không được cấp phép qua KVGS .Doanh nghiệp chắc chắn muốn khai báo tờ khai này không !", true) == "Yes")
                //    {
                //    }
                //    else
                //    {
                //        return;
                //    }
                //}
                #endregion
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {

                    MessagesSend msg;
                    if (!KhaiBaoSua)
                    {
                        IDC IDC = VNACCMaperFromObject.IDCMapper(TKMD.SoToKhai, string.Empty);
                        if (IDC == null)
                        {
                            this.ShowMessage("Lỗi khi tạo messages !", false);
                            return;
                        }
                        msg = MessagesSend.Load<IDC>(IDC, TKMD.InputMessageID);
                    }
                    else
                    {
                        IDE IDE = VNACCMaperFromObject.IDEMapper(TKMD.SoToKhai, string.Empty);
                        if (IDE == null)
                        {
                            this.ShowMessage("Lỗi khi tạo messages !", false);
                            return;
                        }
                        msg = MessagesSend.Load<IDE>(IDE, TKMD.InputMessageID);
                    }

                    MsgLog.SaveMessages(msg, TKMD.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = true;
                    f.inputMSGID = TKMD.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result)
                    {
                        TuDongCapNhatThongTin();
                    }
                    else if (f.DialogResult == DialogResult.Cancel)
                    {
                        ShowMessage(f.msgFeedBack, false);
                    }

                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }
        private void GetIDD(bool idd)
        {
            try
            {

                VNACC_TK_Reference f1 = new VNACC_TK_Reference();
                f1.NghiepVu = EnumNghiepVu.IDD ;
                if (TKMD.ID != 0)
                {
                    f1.SoToKhai = TKMD.SoToKhai;
                    f1.SoHoaDon = TKMD.SoTiepNhanHD;
                }
                f1.ShowDialog(this);
                if (f1.DialogResult == DialogResult.OK)
                {

                    if (TKMD.ID > 0 && this.ShowMessage("Bạn chắc chắn muốn nhận thông tin tờ khai này ? Tờ khai bạn vừa nhập sẽ bị thay đổi thông tin theo tờ khai bạn nhận về từ HQ", true) != "Yes")
                    {
                        return;
                    }
                    TKMD.InputMessageID = HelperVNACCS.NewInputMSGID();
                    MessagesSend msg;
                    IDD IDD;
                        IDD = VNACCMaperFromObject.IDDMapper(f1.SoToKhai);
                        if (IDD == null)
                        {
                            this.ShowMessage("Lỗi khi tạo messages !", false);
                            return;
                        }
                    msg = MessagesSend.Load<IDD>(IDD, TKMD.InputMessageID);


                    MsgLog.SaveMessages(msg, TKMD.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = false;
                    f.inputMSGID = TKMD.InputMessageID;
                    f.ShowDialog(this);
                    if (f.DialogResult == DialogResult.Cancel)
                    {
                        this.ShowMessage(f.msgFeedBack, false);
                    }
                    else if (f.DialogResult == DialogResult.No)
                    {
                    }
                    else
                    {
                        TKMD.InputMessageID = msg.Header.InputMessagesID.GetValue().ToString();
                        if (f.feedback.Error == null || f.feedback.Error.Count == 0)
                        {
                            if (f.feedback.ResponseData != null && f.feedback.ResponseData.Body != null)
                            {
                                string maNV = string.Empty;
                                if (f.feedback.ResponseData.header == null)
                                {
                                    maNV = f.feedback.header.VungDuTru_1.GetValue().ToString().Substring(0, 7);
                                }
                                CapNhatThongTin(f.feedback.ResponseData, maNV);
                                f.msgFeedBack += Environment.NewLine + "Thông tin đã được cập nhật theo phản hồi hải quan. Vui lòng kiểm tra lại";
                            }
                        }
                        this.ShowMessageTQDT(f.msgFeedBack, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }
        private void GetIID(bool iid)
        {
            try
            {

                VNACC_TK_Reference f1 = new VNACC_TK_Reference();
                f1.NghiepVu = iid ? EnumNghiepVu.IID : EnumNghiepVu.IDB;
                if (TKMD.ID != 0)
                {
                    f1.SoToKhai = TKMD.SoToKhai;
                    f1.SoHoaDon = TKMD.SoTiepNhanHD;
                }
                f1.ShowDialog(this);
                if (f1.DialogResult == DialogResult.OK)
                {

                    if (TKMD.ID > 0 && this.ShowMessage("Bạn chắc chắn muốn nhận thông tin tờ khai này ? Tờ khai bạn vừa nhập sẽ bị thay đổi thông tin theo tờ khai bạn nhận về từ HQ", true) != "Yes")
                    {
                        return;
                    }
                    TKMD.InputMessageID = HelperVNACCS.NewInputMSGID();
                    MessagesSend msg;
                    IID IID;
                    IDB IDB;
                    if (iid)
                    {
                        IID = VNACCMaperFromObject.IIDMapper(f1.SoToKhai);
                        if (IID == null)
                        {
                            this.ShowMessage("Lỗi khi tạo messages !", false);
                            return;
                        }
                        msg = MessagesSend.Load<IID>(IID, TKMD.InputMessageID);
                    }
                    else
                    {
                        IDB = VNACCMaperFromObject.IDBMapper(f1.SoToKhai, f1.LoaiVanDon, f1.SoVanDon, f1.SoHoaDon);
                        if (IDB == null)
                        {
                            this.ShowMessage("Lỗi khi tạo messages !", false);
                            return;
                        }
                        msg = MessagesSend.Load<IDB>(IDB, TKMD.InputMessageID);
                    }

                    MsgLog.SaveMessages(msg, TKMD.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = false;
                    f.inputMSGID = TKMD.InputMessageID;
                    f.ShowDialog(this);
                    if (f.DialogResult == DialogResult.Cancel)
                    {
                        this.ShowMessage(f.msgFeedBack, false);
                    }
                    else if (f.DialogResult == DialogResult.No)
                    {
                    }
                    else
                    {
                        TKMD.InputMessageID = msg.Header.InputMessagesID.GetValue().ToString();
                        if (f.feedback.Error == null || f.feedback.Error.Count == 0)
                        {
                            if (f.feedback.ResponseData != null && f.feedback.ResponseData.Body != null)
                            {
                                string maNV = string.Empty;
                                if (f.feedback.ResponseData.header == null)
                                {
                                    maNV = f.feedback.header.VungDuTru_1.GetValue().ToString().Substring(0, 7);
                                }
                                CapNhatThongTin(f.feedback.ResponseData, maNV);
                                f.msgFeedBack += Environment.NewLine + "Thông tin đã được cập nhật theo phản hồi hải quan. Vui lòng kiểm tra lại";
                            }
                        }
                        this.ShowMessageTQDT(f.msgFeedBack, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        #endregion

        #region Cập nhật thông tin
        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            CapNhatThongTin(msgResult, string.Empty);
        }
        private void CapNhatThongTin(ReturnMessages msgResult, string MaNV)
        {
            ProcessMessages.GetDataResult_TKMD(msgResult, MaNV, TKMD);
            //TKMD.CoQuanHaiQuan = GlobalSettings.MA_HAI_QUAN.Substring(1, 2).ToUpper() + GlobalSettings.MA_HAI_QUAN.Substring(0, 1).ToUpper() + GlobalSettings.MA_HAI_QUAN.Substring(3, 1).ToUpper();
            
            TKMD.InsertUpdateFull();
            SetToKhai();
            setCommandStatus();
        }
        private void TuDongCapNhatThongTin()
        {
            if (TKMD != null && TKMD.SoToKhai > 0 && TKMD.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan like '{0}%' and TrangThai in ('C','E')", TKMD.SoToKhai.ToString().Substring(0,11)), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        if (msgPb.TrangThai == EnumTrangThaiXuLyMessage.ChuaXuLy || msgPb.TrangThai == EnumTrangThaiXuLyMessage.XuLyLoi)
                        {
                            ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                            CapNhatThongTin(msgReturn);
                            //msgPb.Delete();
                            msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                            msgPb.InsertUpdate();
                        }
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
            }
        }
        #endregion

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtSoToKhai.Tag = "ICN"; //Số tờ khai
                txtSoToKhaiDauTien.Tag = "FIC"; //Số tờ khai đầu tiên
                txtSoNhanhToKhai.Tag = "BNO"; //Số nhánh của tờ khai chia nhỏ
                txtTongSoTKChiaNho.Tag = "DNO"; //Tổng số tờ khai chia nhỏ
                txtSoToKhaiTNTX.Tag = "TDN"; //Số tờ khai tạm nhập tái xuất tương ứng
                // duydp Ngày nhập kho đầu tiên
                clcNgayNhapKhoDauTien.TagName = "ISD";//Ngày nhập kho đầu tiên
                // Ngày nhập kho đầu tiên
                ctrMaLoaiHinh.TagName = "ICB"; //Mã loại hình
                ctrMaPhanLoaiHH.TagName = "CCC"; //Mã phân loại hàng hóa
                ctrMaPhuongThucVT.TagName = "MTC"; //Mã hiệu phương thức vận chuyển
                ctrPhanLoaiToChuc.TagName = "SKB"; //Phân loại cá nhân/tổ chức
                ctrCoQuanHaiQuan.TagCode = "CH"; //Cơ quan Hải quan
                ctrNhomXuLyHS.TagName = "CHB"; //Nhóm xử lý hồ sơ
                clcThoiHanTaiNhapTaiXuat.TagName = "RED"; //Thời hạn tái xuất
                clcNgayDangKy.TagName = "ICD"; //Ngày đăng ký tờ khai (Dự kiến)
                txtMaDonVi.Tag = "IMC"; //Mã người nhập khẩu
                txtTenDonVi.Tag = "IMN"; //Tên người nhập khẩu
                txtMaBuuChinhDonVi.Tag = "IMY"; //Mã bưu chính
                txtDiaChiDonVi.Tag = "IMA"; //Địa chỉ người nhập khẩu
                txtSoDienThoaiDonVi.Tag = "IMT"; //Số điện thoại người nhập khẩu
                txtMaUyThac.Tag = "NMC"; //Mã người ủy thác nhập khẩu
                txtTenUyThac.Tag = "NMN"; //Tên người ủy thác nhập khẩu
                txtMaDoiTac.Tag = "EPC"; //Mã người xuất khẩu
                txtTenDoiTac.Tag = "EPN"; //Tên người xuất khẩu
                txtMaBuuChinhDoiTac.Tag = "EPY"; //Mã bưu chính
                txtDiaChiDoiTac1.Tag = "EPA"; //Địa chỉ 1(Street and number/P.O.BOX)
                txtDiaChiDoiTac2.Tag = "EP2"; //Địa chỉ 2(Street and number/P.O.BOX)
                txtDiaChiDoiTac3.Tag = "EP3"; //Địa chỉ 3(City name)
                txtDiaChiDoiTac4.Tag = "EP4"; //Địa chỉ 4(Country sub-entity, name)
                ctrMaNuocDoiTac.TagCode = "EPO"; //Mã nước(Country, coded)
                txtMaUyThac.Tag = "ENM";
                txtTenUyThac.Tag = "ENM"; //Người ủy thác xuất khẩu
                txtUyThacXK.Tag = "ENM"; //Tên người ủy thác xuất khẩu
                txtMaDaiLyHQ.Tag = "ICC"; //Mã đại lý Hải quan
                txtSoVanDon.Tag = "BL_"; //Số vận đơn (Số B/L, số AWB v.v. …)
                txtSoLuong.Tag = "NO"; //Số lượng
                ctrMaDVTSoLuong.TagName = "NOT"; //Mã đơn vị tính
                txtTrongLuong.Tag = "GW"; //Tổng trọng lượng hàng (Gross)
                ctrMaDVTTrongLuong.TagName = "GWT"; //Mã đơn vị tính trọng lượng (Gross)
                ctrMaDDLuuKho.TagName = "ST"; //Mã địa điểm lưu kho hàng chờ thông quan dự kiến
                txtSoHieuKyHieu.Tag = "MRK"; //Ký hiệu và số hiệu
                txtMaPTVC.Tag = "VSC"; //Mã phương tiện vận chuyển
                txtTenPTVC.Tag = "VSN"; //Tên tầu (máy bay) chở hàng
                clcNgayHangDen.TagName = "ARR"; //Ngày hàng đến
                ctrDiaDiemDoHang.TagCode = "DST"; //Mã địa điểm dỡ hàng
                ctrDiaDiemDoHang.TagName = "DSN"; //Tên địa điểm dỡ hàng
                ctrDiaDiemXepHang.TagCode = "PSC"; //Mã địa điểm xếp hàng
                ctrDiaDiemXepHang.TagName = "PSN"; //Tên địa điểm xếp hàng
                txtSoLuongCont.Tag = "COC"; //Số lượng container
                ctrMaKetQuaKiemTra.TagName = "N4"; //Mã kết quả kiểm tra nội dung
                ctrMaVanBanPhapQuy1.TagName = "OL_"; //Mã văn bản pháp quy khác
                ctrMaVanBanPhapQuy2.TagName = "OL_"; //Mã văn bản pháp quy khác
                ctrMaVanBanPhapQuy3.TagName = "OL_"; //Mã văn bản pháp quy khác
                ctrMaVanBanPhapQuy4.TagName = "OL_"; //Mã văn bản pháp quy khác
                ctrMaVanBanPhapQuy5.TagName = "OL_"; //Mã văn bản pháp quy khác


                txtSoTiepNhanHD.Tag = "IV2"; //Số tiếp nhận hóa đơn điện tử
                txtSoHoaDon.Tag = "IV3"; //Số hóa đơn
                clcNgayPhatHanhHD.TagName = "IVD"; //Ngày phát hành
                ctrPhuongThucTT.TagName = "IVP"; //Phương thức thanh toán
                ctrMaPhanLoaiTriGia.TagName = "IP1"; //Mã phân loại giá hóa đơn
                ctrMaDieuKienGiaHD.TagName = "IP2"; //Mã điều kiện giá hóa đơn
                ctrMaTTHoaDon.TagName = "IP3"; //Mã đồng tiền của hóa đơn
                txtTongTriGiaHD.Tag = "IP4"; //Tổng trị giá  hóa đơn
                
                ctrPhanLoaiHD.TagName = "IV1"; // Phân loại hình thức hóa đơn
                //minhnd
                txtTongHeSoPhanBo.Tag = "TP";// Tổng hệ số phân bổ trị giá
                //minhnd
                ctrNguoiNopThue.TagName = "TPM"; //Người nộp thuế 
                ctrMaLyDoDeNghi.TagName = "BP"; //Mã lý do đề nghị BP (release before permit)(Phát hành trước giấy phép)
                ctrMaNHTraThueThay.TagName = "BRC"; //Mã ngân hàng trả thuế thay
                txtNamPhatHanhHM.Tag = "BYA"; //Năm phát hành hạn mức
                txtKyHieuCTHanMuc.Tag = "BCM"; //Kí hiệu chứng từ hạn mức
                txtSoCTHanMuc.Tag = "BCN"; //Số chứng từ hạn mức
                ctrMaXDThoiHanNopThue.TagName = "ENC"; //Mã xác định thời hạn nộp thuế
                ctrMaNHBaoLanh.TagName = "SBC"; //Mã ngân hàng bảo lãnh
                txtNamPhatHanhBL.Tag = "RYA"; //Năm phát hành bảo lãnh
                txtKyHieuCTBaoLanh.Tag = "SCM"; //Kí hiệu chứng từ bảo lãnh
                txtSoCTBaoLanh.Tag = "SCN"; //Số chứng từ bảo lãnh

                ctrDiaDiemDichVC.TagName = "ARP"; //Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
                clcNgayDen.TagName = "ADT"; //Ngày dự kiến đến
                clcNgayKhoiHanhVC.TagName = "DPD"; // Ngày khởi hành vận chuyển
                txtGhiChu.Tag = "NT2"; //Phần ghi chú
                txtSoQuanLyNoiBoDN.Tag = "REF"; //Số quản lý của nội bộ doanh nghiệp

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private void ctrMaPhuongThucVT_Leave(object sender, EventArgs e)
        {
            ctrDiaDiemDoHang.Visible = ctrDiaDiemXepHang.Visible = true;
            txtMaDiaDiemDoHang.Visible = txtTenDiaDiemDoHang.Visible = txtTenDiaDiemXepHang.Visible = txtMaDiaDiemXepHang.Visible = false;
            if (ctrMaPhuongThucVT.Code == "4")
                ctrDiaDiemDoHang.CategoryType = ECategory.A620;
            else if (ctrMaPhuongThucVT.Code == "5")
                ctrDiaDiemDoHang.CategoryType = ECategory.A601;
            else if(ctrMaPhuongThucVT.Code == "9")
            {
               // ctrDiaDiemDoHang.CategoryType = ECategory.A016T;
               // ctrDiaDiemDoHang.IsXNKTaiCho = true;
               //ctrDiaDiemDoHang.IsRequimentMa = true;
               // //ctrDiaDiemDoHang.Code = "VNZZZ";
               // ctrDiaDiemXepHang.CategoryType = ECategory.A016T;
               // ctrDiaDiemXepHang.IsXNKTaiCho = true;
                ctrDiaDiemDoHang.Visible = ctrDiaDiemXepHang.Visible = false;
                txtMaDiaDiemDoHang.Visible = txtTenDiaDiemDoHang.Visible = txtTenDiaDiemXepHang.Visible = txtMaDiaDiemXepHang.Visible = true;
            }
            else
                ctrDiaDiemDoHang.CategoryType = ECategory.A016;
            ctrDiaDiemDoHang.Where = "I";
           
            ctrDiaDiemDoHang.ReLoadData();
            ctrDiaDiemXepHang.ReLoadData();
           
        }

        private void ctrCoQuanHaiQuan_Leave(object sender, EventArgs e)
        {
            ctrNhomXuLyHS.CustomsCode = ctrCoQuanHaiQuan.Code;
            ctrNhomXuLyHS.ReLoadData();
            if (TKMD != null && !string.IsNullOrEmpty(TKMD.NhomXuLyHS))
                ctrNhomXuLyHS.Code = TKMD.NhomXuLyHS;
            if (ctrCoQuanHaiQuan.Code == "33PD")
                ctrNhomXuLyHS.Code = "00";
        }

        private void ShowKetQuaTraVe()
        {
            SendmsgVNACCFrm f = new SendmsgVNACCFrm(null);
            f.isSend = false;
            f.isRep = true;
            f.inputMSGID = TKMD.InputMessageID;
            f.SoToKhai =  this.TKMD.SoToKhai.ToString().Length > 11 ? this.TKMD.SoToKhai.ToString().Substring(0, 11) : string.Empty;
            f.ShowDialog(this);
        }

        private void setCommandStatus()
        {
            if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao || TKMD.TrangThaiXuLy == null)
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdKhaiBaoSua.Visible = cmdIDC.Visible = cmdIDE.Visible = cmdIDA01.Visible = cmdIDD.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdIDA.Visible = cmdIDB.Visible = Janus.Windows.UI.InheritableBoolean.True;
                cmdBoSungCont.Visible = cmdBoSungCont1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam)
            {
                cmdInAn.Visible = cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdKhaiBaoSua.Visible = cmdIDA.Visible = cmdIDE.Visible = cmdIDA01.Visible = cmdIDD.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdIDC.Visible = cmdIDB.Visible = Janus.Windows.UI.InheritableBoolean.True;
                cmdBoSungCont.Visible = cmdBoSungCont1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoSua)
            {
                cmdInAn.Visible = cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdIDA.Visible = cmdIDB.Visible = cmdIDC.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBaoSua.Visible = cmdIDA01.Visible = cmdIDE.Visible = cmdIDD.Visible = Janus.Windows.UI.InheritableBoolean.True; cmdKhaiBaoSua.Commands.Clear();
                cmdBoSungCont.Visible = cmdBoSungCont1.Visible = Janus.Windows.UI.InheritableBoolean.False;
                this.cmdKhaiBaoSua.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] 
                
            {
                this.cmdIDA01,
                this.cmdIDE,
                this.cmdIDD
                ,});
            }
            else if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc)
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdIDA01.Visible = cmdIDE.Visible = cmdIDD.Visible = cmdIDA.Visible = cmdIDB.Visible = cmdIDC.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBaoSua.Visible = cmdIID.Visible = Janus.Windows.UI.InheritableBoolean.True;
                cmdKhaiBaoSua.Commands.Clear();
                cmdBoSungCont.Visible = cmdBoSungCont1.Visible = Janus.Windows.UI.InheritableBoolean.True;
            }
            else if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
            {
                cmdKhaiBaoSua.Visible =  Janus.Windows.UI.InheritableBoolean.False;
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBao.Enabled = cmdKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdIDA01.Visible = cmdIDE.Visible = cmdIDD.Visible = cmdIDA.Visible = cmdIDB.Visible = cmdIDC.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBaoSua.Visible = cmdIID.Visible = Janus.Windows.UI.InheritableBoolean.False;
                //cmdDSContainer.Visible  = Janus.Windows.UI.InheritableBoolean.True;
                cmdBoSungCont.Visible = cmdBoSungCont1.Visible = Janus.Windows.UI.InheritableBoolean.True;
            }
            if (string.IsNullOrEmpty(TKMD.InputMessageID))
                cmdKetQuaTraVe.Visible = Janus.Windows.UI.InheritableBoolean.False;
            else
                cmdKetQuaTraVe.Visible = Janus.Windows.UI.InheritableBoolean.True;

        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtSoToKhai.MaxLength = 12;
                txtSoToKhaiDauTien.MaxLength = 12;
                txtSoNhanhToKhai.MaxLength = 2;
                txtTongSoTKChiaNho.MaxLength = 2;
                txtSoToKhaiTNTX.MaxLength = 12;
                //txtMaLoaiHinh.MaxLength = 3;
                //txtMaPhanLoaiHH.MaxLength = 1;
                //txtMaPhuongThucVT.MaxLength = 1;
                //txtPhanLoaiToChuc.MaxLength = 1;
                //txtCoQuanHaiQuan.MaxLength = 6;
                //txtNhomXuLyHS.MaxLength = 2;
                //txtThoiHanTaiNhapTaiXuat.MaxLength = 8;
                //txtNgayDangKy.MaxLength = 8;
                txtMaDonVi.MaxLength = 13;
                txtTenDonVi.MaxLength = 300;
                txtMaBuuChinhDonVi.MaxLength = 7;
                txtDiaChiDonVi.MaxLength = 300;
                txtSoDienThoaiDonVi.MaxLength = 20;
                txtMaUyThac.MaxLength = 13;
                txtTenUyThac.MaxLength = 300;
                txtMaDoiTac.MaxLength = 13;
                txtTenDoiTac.MaxLength = 70;
                txtMaBuuChinhDoiTac.MaxLength = 9;
                txtDiaChiDoiTac1.MaxLength = 35;
                txtDiaChiDoiTac2.MaxLength = 35;
                txtDiaChiDoiTac3.MaxLength = 35;
                txtDiaChiDoiTac4.MaxLength = 35;
                //txtMaNuocDoiTac.MaxLength = 2;
                //txtNguoiUyThacXK.MaxLength = 70;
                txtMaDaiLyHQ.MaxLength = 5;
                txtSoVanDon.MaxLength = 35;
                txtSoLuong.MaxLength = 8;
                //txtMaDVTSoLuong.MaxLength = 3;
                txtTrongLuong.MaxLength = 10;
                //txtMaDVTTrongLuong.MaxLength = 3;
                //txtMaDDLuuKho.MaxLength = 7;
                txtSoHieuKyHieu.MaxLength = 140;
                txtMaPTVC.MaxLength = 9;
                txtTenPTVC.MaxLength = 35;
                //txtNgayHangDen.MaxLength = 8;
                //txtMaDiaDiemDoHang.MaxLength = 6;
                //txtTenDiaDiemDohang.MaxLength = 35;
                //txtMaDiaDiemXepHang.MaxLength = 5;
                //txtTenDiaDiemXepHang.MaxLength = 35;
                txtSoLuongCont.MaxLength = 3;
                //txtMaKetQuaKiemTra.MaxLength = 1;
                //txtMaVanbanPhapQuy.MaxLength = 2;
                //txtPhanLoai.MaxLength = 4;
                //txtSoGiayPhep.MaxLength = 20;
                //txtPhanLoaiHD.MaxLength = 1;
                txtSoTiepNhanHD.MaxLength = 12;
                txtSoHoaDon.MaxLength = 35;
                //txtNgayPhatHanhHD.MaxLength = 8;
                //txtPhuongThucTT.MaxLength = 7;
                //txtPhanLoaiGiaHD.MaxLength = 1;
                //txtMaDieuKienGiaHD.MaxLength = 3;
                //txtMaTTHoaDon.MaxLength = 3;
                txtTongTriGiaHD.MaxLength = 20;
                //txtMaPhanLoaiTriGia.MaxLength = 1;
                //txtSoTiepNhanTKTriGia.MaxLength = 9;
                //txtMaTTHieuChinhTriGia.MaxLength = 3;
                //txtGiaHieuChinhTriGia.MaxLength = 20;
                //txtMaPhanLoaiPhiVC.MaxLength = 1;
                //txtMaTTPhiVC.MaxLength = 3;
                //txtPhiVanChuyen.MaxLength = 18;
                //txtMaPhanLoaiPhiBH.MaxLength = 1;
                //txtMaTTPhiBH.MaxLength = 3;
                //txtPhiBaoHiem.MaxLength = 16;
                //txtSoDangKyBH.MaxLength = 6;
                //txtMaTenDieuChinh.MaxLength = 1;
                //txtMaPhanLoaiDieuChinh.MaxLength = 3;
                //txtMaTTDieuChinhTriGia.MaxLength = 3;
                //txtTriGiaKhoanDieuChinh.MaxLength = 20;
                //txtTongHeSoPhanBo.MaxLength = 20;
                //txtChiTietKhaiTriGia.MaxLength = 840;
                //txtTongHeSoPhanBoTG.MaxLength = 20;
                //txtNguoiNopThue.MaxLength = 1;
                //txtMaLyDoDeNghiBP.MaxLength = 1;
                //ctrMaNHTraThueThay.MaxLength = 11;
                txtNamPhatHanhHM.MaxLength = 4;
                txtKyHieuCTHanMuc.MaxLength = 10;
                txtSoCTHanMuc.MaxLength = 10;
                //txtMaXDThoiHanNopThue.MaxLength = 1;
                //txtMaNHBaoLanh.MaxLength = 11;
                txtNamPhatHanhBL.MaxLength = 4;
                txtKyHieuCTBaoLanh.MaxLength = 10;
                txtSoCTBaoLanh.MaxLength = 10;
                //txtPhanLoai.MaxLength = 3;
                //txtSoDinhKemKhaiBaoDT.MaxLength = 12;
                //txtNgayNhapKhoDau.MaxLength = 8;
                //txtNgayKhoiHanhVC.MaxLength = 8;
                //txtMaDiaDiem.MaxLength = 7;
                //txtNgayDen.MaxLength = 8;
                //txtNgayKhoiHanh.MaxLength = 8;
                //txtDiaDiemDichVC.MaxLength = 7;
                //txtNgayDen.MaxLength = 8;
                txtGhiChu.MaxLength = 300;
                txtSoQuanLyNoiBoDN.MaxLength = 20;
                //txtPhanLoai.MaxLength = 1;
                //txtNgay.MaxLength = 8;
                //txtTen.MaxLength = 120;
                //txtNoiDung.MaxLength = 420;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                ctrDiaDiemXepHang.ShowColumnCode = true;
                ctrDiaDiemXepHang.ShowColumnName = false;
                //ctrDiaDiemXepHang.SetValidate = !isOnlyWarning; ctrDiaDiemXepHang.IsOnlyWarning = isOnlyWarning;
                //isValid &= ctrDiaDiemXepHang.IsValidate; //"Mã địa điểm xếp hàng");
                ctrMaDVTSoLuong.SetValidate = !isOnlyWarning; ctrMaDVTSoLuong.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrMaDVTSoLuong.IsValidate; //"Mã đơn vị tính");
                ctrMaLoaiHinh.SetValidate = !isOnlyWarning; ctrMaLoaiHinh.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrMaLoaiHinh.IsValidate; //"Mã loại hình");
                ctrMaPhuongThucVT.SetValidate = !isOnlyWarning; ctrMaPhuongThucVT.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrMaPhuongThucVT.IsValidate; //"Mã hiệu phương thức vận chuyển");

                isValid &= ValidateControl.ValidateNull(txtSoLuong, errorProvider, "Số lượng", isOnlyWarning);

                ctrPhanLoaiHD.SetValidate = !isOnlyWarning; ctrPhanLoaiHD.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrPhanLoaiHD.IsValidate; //"Phân loại hình thức hóa đơn");
                ctrPhanLoaiToChuc.SetValidate = !isOnlyWarning; ctrPhanLoaiToChuc.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrPhanLoaiToChuc.IsValidate; //"Phân loại cá nhân/tổ chức");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private bool ShowValidateFormErrorSend(List<ColumnError> columnErrors)
        {
            
            bool isValid = true;
            foreach (ColumnError item in columnErrors)
            {                
                string TagName = item.ErrorColumnCode.ToString();
                switch (TagName)
                {
                    case "ICN":
                        //Số tờ khai
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateZero(txtSoToKhai, errorProvider, "", item.ErrorString);
                        break;
                    case "FIC":
                        //Số tờ khai đầu tiên
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoToKhaiDauTien, errorProvider, "", false, item.ErrorString);
                        break;
                    case "BNO":
                        //Số nhánh của tờ khai chia nhỏ
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoNhanhToKhai, errorProvider, "", false, item.ErrorString);
                        break;
                    case "DNO":
                        //Tổng số tờ khai chia nhỏ
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTongSoTKChiaNho, errorProvider, "", false, item.ErrorString);
                        break;
                    case "TDN":
                        //Số tờ khai tạm nhập tái xuất tương ứng
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoToKhaiTNTX, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ISD":
                        //Ngày nhập kho đầu tiên
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateCalendar(clcNgayNhapKhoDauTien, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ICB":
                        //Mã loại hình
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaLoaiHinh, errorProvider, "", false, item.ErrorString);
                        break;
                    case "CCC":
                        //Mã phân loại hàng hóa
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaPhanLoaiHH, errorProvider, "", false, item.ErrorString);
                        break;
                    case "MTC":
                        //Mã hiệu phương thức vận chuyển
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaPhuongThucVT, errorProvider, "", false, item.ErrorString);
                        break;
                    case "SKB":
                        //Phân loại cá nhân/tổ chức
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrPhanLoaiToChuc, errorProvider, "", false, item.ErrorString);
                        break;
                    case "CH":
                        //Cơ quan Hải quan
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(ctrCoQuanHaiQuan, errorProvider, "", false, item.ErrorString);
                        break;
                    case "CHB":
                        //Nhóm xử lý hồ sơ
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseNhomXuLy(ctrNhomXuLyHS, errorProvider, "", false, item.ErrorString);
                        break;
                    case "RED":
                        //Thời hạn tái xuất
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateCalendar(clcThoiHanTaiNhapTaiXuat, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ICD":
                        //Ngày đăng ký tờ khai (Dự kiến)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateCalendar(clcNgayDangKy, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IMC":
                        //Mã người nhập khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtMaDonVi, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IMN":
                        //Tên người nhập khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTenDonVi, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IMY":
                        //Mã bưu chính
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtMaBuuChinhDonVi, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IMA":
                        //Địa chỉ người nhập khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtDiaChiDonVi, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IMT":
                        //Số điện thoại người nhập khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoDienThoaiDonVi, errorProvider, "", false, item.ErrorString);
                        break;
                    case "NMC":
                        //Mã người ủy thác nhập khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtMaUyThac, errorProvider, "", false, item.ErrorString);
                        break;
                    case "NMN":
                        //Tên người ủy thác nhập khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTenUyThac, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EPC":
                        //Mã người xuất khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtMaDoiTac, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EPN":
                        //Tên người xuất khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTenDoiTac, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EPY":
                        //Mã bưu chính
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtMaBuuChinhDoiTac, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EPA":
                        //Địa chỉ 1(Street and number/P.O.BOX)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtDiaChiDoiTac1, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EP2":
                        //Địa chỉ 2(Street and number/P.O.BOX)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtDiaChiDoiTac2, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EP3":
                        //Địa chỉ 3(City name)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtDiaChiDoiTac3, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EP4":
                        //Địa chỉ 4(Country sub-entity, name)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtDiaChiDoiTac4, errorProvider, "", false, item.ErrorString);
                        break;
                    case "EPO":
                        //Mã nước(Country, coded)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(ctrMaNuocDoiTac, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ENM":
                        //Người ủy thác xuất khẩu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtUyThacXK, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ICC":
                        //Mã đại lý Hải quan
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtMaDaiLyHQ, errorProvider, "", false, item.ErrorString);
                        break;
                    case "BL1":
                        //Số vận đơn (Số B/L, số AWB v.v. …)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoVanDon, errorProvider, "", false, item.ErrorString);
                        break;
                    //case "BL1":
                    //    //Số vận đơn (Số B/L, số AWB v.v. …)
                    //    isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoVanDon, errorProvider, "", false, item.ErrorString);
                    //    break;
                    case "BL2":
                        //Số vận đơn (Số B/L, số AWB v.v. …)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoVanDon, errorProvider, "", false, item.ErrorString);
                        break;
                    case "BL3":
                        //Số vận đơn (Số B/L, số AWB v.v. …)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoVanDon, errorProvider, "", false, item.ErrorString);
                        break;
                    case "BL4":
                        //Số vận đơn (Số B/L, số AWB v.v. …)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoVanDon, errorProvider, "", false, item.ErrorString);
                        break;
                    case "BL5":
                        //Số vận đơn (Số B/L, số AWB v.v. …)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoVanDon, errorProvider, "", false, item.ErrorString);
                        break;
                    case "NO":
                        //Số lượng
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoLuong, errorProvider, "", false, item.ErrorString);
                        break;
                    case "NOT":
                        //Mã đơn vị tính
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaDVTSoLuong, errorProvider, "", false, item.ErrorString);
                        break;
                    case "GW":
                        //Tổng trọng lượng hàng (Gross)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTrongLuong, errorProvider, "", false, item.ErrorString);
                        break;
                    case "GWT":
                        //Mã đơn vị tính trọng lượng (Gross)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaDVTTrongLuong, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ST":
                        //Mã địa điểm lưu kho hàng chờ thông quan dự kiến
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaDDLuuKho, errorProvider, "", false, item.ErrorString);
                        break;
                    case "MRK":
                        //Ký hiệu và số hiệu
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoHieuKyHieu, errorProvider, "", false, item.ErrorString);
                        break;
                    case "VSC":
                        //Mã phương tiện vận chuyển
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtMaPTVC, errorProvider, "", false, item.ErrorString);
                        break;
                    case "VSN":
                        //Tên tầu (máy bay) chở hàng
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTenPTVC, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ARR":
                        //Ngày hàng đến
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateCalendar(clcNgayHangDen, errorProvider, "", false, item.ErrorString);
                        break;
                    case "DST":
                        //Mã địa điểm dỡ hàng
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(ctrDiaDiemDoHang, errorProvider, "", false, item.ErrorString);
                        break;
                    case "DSN":
                        //Tên địa điểm dỡ hàng
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTenDiaDiemDoHang, errorProvider, "", false, item.ErrorString);
                        break;
                    case "PSC":
                        //Mã địa điểm xếp hàng
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(ctrDiaDiemXepHang, errorProvider, "", false, item.ErrorString);
                        break;
                    case "PSN":
                        //Tên địa điểm xếp hàng
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTenDiaDiemXepHang, errorProvider, "", false, item.ErrorString);
                        break;
                    case "COC":
                        //Số lượng container
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoLuongCont, errorProvider, "", false, item.ErrorString);
                        break;
                    case "N4":
                        //Mã kết quả kiểm tra nội dung
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaKetQuaKiemTra, errorProvider, "", true, item.ErrorString);
                        break;
                    case "OL1":
                        //Mã văn bản pháp quy khác
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaVanBanPhapQuy1, errorProvider, "", false, item.ErrorString);
                        break;
                    case "OL2":
                        //Mã văn bản pháp quy khác
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaVanBanPhapQuy2, errorProvider, "", false, item.ErrorString);
                        break;
                    case "OL3":
                        //Mã văn bản pháp quy khác
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaVanBanPhapQuy3, errorProvider, "", false, item.ErrorString);
                        break;
                    case "OL4":
                        //Mã văn bản pháp quy khác
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaVanBanPhapQuy4, errorProvider, "", false, item.ErrorString);
                        break;
                    case "OL5":
                        //Mã văn bản pháp quy khác
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaVanBanPhapQuy5, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IV2":
                        //Số tiếp nhận hóa đơn điện tử
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoTiepNhanHD, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IV3":
                        //Số hóa đơn
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoHoaDon, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IVD":
                        //Ngày phát hành
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateCalendar(clcNgayPhatHanhHD, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IVP":
                        //Phương thức thanh toán
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrPhuongThucTT, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IP1":
                        //Mã phân loại giá hóa đơn
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaPhanLoaiTriGia, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IP2":
                        //Mã điều kiện giá hóa đơn
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaDieuKienGiaHD, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IP3":
                        //Mã đồng tiền của hóa đơn
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaTTHoaDon, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IP4":
                        //Tổng trị giá  hóa đơn
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTongTriGiaHD, errorProvider, "", false, item.ErrorString);
                        break;
                    case "IV1":
                        // Phân loại hình thức hóa đơn
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrPhanLoaiHD, errorProvider, "", false, item.ErrorString);
                        break;
                    case "TP":
                        // Tổng hệ số phân bổ trị giá
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtTongHeSoPhanBo, errorProvider, "", false, item.ErrorString);
                        break;
                    case "TPM":
                        //Người nộp thuế 
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrNguoiNopThue, errorProvider, "", false, item.ErrorString);
                        break;
                    case "BP":
                        //Mã lý do đề nghị BP (release before permit)(Phát hành trước giấy phép)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaLyDoDeNghi, errorProvider, "", false, item.ErrorString);
                        break;
                    case "BRC":
                        //Mã ngân hàng trả thuế thay
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaNHTraThueThay, errorProvider, "", false, item.ErrorString);
                        break;
                    case "BYA":
                        //Năm phát hành hạn mức
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtNamPhatHanhHM, errorProvider, "", false, item.ErrorString);
                        break;
                    case "BCM":
                        //Kí hiệu chứng từ hạn mức
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtKyHieuCTHanMuc, errorProvider, "", false, item.ErrorString);
                        break;
                    case "BCN":
                        //Số chứng từ hạn mức
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoCTHanMuc, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ENC":
                        //Mã xác định thời hạn nộp thuế
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaXDThoiHanNopThue, errorProvider, "", false, item.ErrorString);
                        break;
                    case "SBC":
                        //Mã ngân hàng bảo lãnh
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrMaNHBaoLanh, errorProvider, "", false, item.ErrorString);
                        break;
                    case "RYA":
                        //Năm phát hành bảo lãnh
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtNamPhatHanhBL, errorProvider, "", false, item.ErrorString);
                        break;
                    case "SCM":
                        //Kí hiệu chứng từ bảo lãnh
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtKyHieuCTBaoLanh, errorProvider, "", false, item.ErrorString);
                        break;
                    case "SCN":
                        //Số chứng từ bảo lãnh
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoCTBaoLanh, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ARP":
                        //Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(ctrDiaDiemDichVC, errorProvider, "", false, item.ErrorString);
                        break;
                    case "ADT":
                        //Ngày dự kiến đến
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateCalendar(clcNgayDen, errorProvider, "", false, item.ErrorString);
                        break;
                    case "DPD":
                        // Ngày khởi hành vận chuyển
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateCalendar(clcNgayKhoiHanhVC, errorProvider, "", false, item.ErrorString);
                        break;
                    case "NT2":
                        //Phần ghi chú
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtGhiChu, errorProvider, "", false, item.ErrorString);
                        break;
                    case "REF":
                        //Số quản lý của nội bộ doanh nghiệp
                        isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(txtSoQuanLyNoiBoDN, errorProvider, "", false, item.ErrorString);
                        break;


                        //Form VNACC Tờ khai trị giá
                    case "VD1":
                        //Mã phân loại khai trị giá
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaPhanLoaiTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaPhanLoaiTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                            f.Activate();
                        }

                        break;
                    case "VD2":
                        //Số tiếp nhận tờ khai trị giá tổng hợp 
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(f.txtSoTiepNhanTKTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(f.txtSoTiepNhanTKTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        break;
                    case "VCC":
                        //Mã tiền tệ của giá cơ sở hiệu chỉnh trị giá
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaTTHieuChinhTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaTTHieuChinhTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }                        
                        break;
                    case "VPC":
                        //Giá cơ sở để hiệu chỉnh trị giá
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(f.txtGiaHieuChinhTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(f.txtGiaHieuChinhTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }                        
                        break;
                    case "FR1":
                        //Mã phân loại phí vận chuyển
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaPhanLoaiPhiVC, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaPhanLoaiPhiVC, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }  
                        
                        break;
                    case "FR2":
                         //Mã tiền tệ phí vận chuyển
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaTTPhiVC, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaTTPhiVC, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        } 
                        
                        break;
                    case "FR3":
                         //Phí vận chuyển
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(f.txtPhiVanChuyen, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(f.txtPhiVanChuyen, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        } 
                        
                        break;
                    case "IN1":
                         //Mã phân loại bảo hiểm
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaPhanLoaiPhiBH, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaPhanLoaiPhiBH, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        break;
                    case "IN2":
                        //Mã tiền tệ của tiền bảo hiểm
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaTTPhiBH, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaTTPhiBH, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        } 
                        
                        break;
                    case "IN3":
                        //Phí bảo hiểm
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(f.txtPhiBaoHiem, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(f.txtPhiBaoHiem, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        } 
                        
                        break;
                    case "IN4":
                        //Số đăng ký bảo hiểm tổng hợp
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(f.txtSoDangKyBH, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(f.txtSoDangKyBH, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        } 
                        
                        break;
                    case "VR1":
                        //Mã tên khoản điều chỉnh
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(f.grList, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(f.grList, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        break;
                    case "VI1":
                        //Mã phân loại điều chỉnh trị giá
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaPhanLoaiTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaPhanLoaiTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        } 
                        
                        break;
                    case "VC1":
                        //Mã đồng tiền của khoản điều chỉnh trị giá
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaPhanLoaiTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaPhanLoaiTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        } 
                        
                        break;
                    case "VP1":
                        //Trị giá khoản điều chỉnh
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaPhanLoaiTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaPhanLoaiTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        } 
                        
                        break;
                    case "VT1":
                        //Tổng hệ số phân bổ trị giá khoản điều chỉnh
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaPhanLoaiTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(f.ctrMaPhanLoaiTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        } 
                        
                        break;
                    case "VLD":
                        //Chi tiết khai trị giá
                        if (!f.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(f.txtChiTietKhaiTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        }
                        else
                        {
                            f.Dispose();
                            f = new VNACC_TK_TKTriGiaForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(f.txtChiTietKhaiTriGia, errorProvider, "", false, item.ErrorString);
                            f.TKMD = this.TKMD;
                            f.Show();
                        } 
                        
                        break;

                        // Hàng mậu dịch VNACCS
                    case "CMD":
                        //Mã số hàng hóa
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaSoHang, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaSoHang, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "GZC":
                        //Mã quản lý riêng
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtMaQuanLy, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtMaQuanLy, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "TXA":
                        //Thuế suất
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtThueSuat, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtThueSuat, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "TXB":
                        //Mức thuế tuyệt đối
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtThueSuatTuyetDoi, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtThueSuatTuyetDoi, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "TXC":
                        //Mã đơn vị tính thuế tuyệt đối
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaDVTTuyetDoi, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaDVTTuyetDoi, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "TXD":
                        //Mã đồng tiền của mức thuế tuyệt đối
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaTTTuyetDoi, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaTTTuyetDoi, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "CMN":
                        //Mô tả hàng hóa
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtTenHang, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtTenHang, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "OR":
                        //Mã nước xuất xứ
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(hmdForm.ctrNuocXuatXu, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(hmdForm.ctrNuocXuatXu, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "ORS":
                        //Mã biểu thuế nhập khẩu 
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(hmdForm.ctrMaBieuThueNK, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(hmdForm.ctrMaBieuThueNK, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "KWS":
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtMaHanNgach, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtMaHanNgach, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        //Mã ngoài hạn ngạch
                        
                        break;
                    case "SPD":
                        //Mã xác định mức thuế nhập khẩu theo lượng
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(hmdForm.ctrMaThueNKTheoLuong, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategoryAllowEmpty(hmdForm.ctrMaThueNKTheoLuong, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "QN1":
                        //Số lượng (1)
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoLuong1, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoLuong1, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                       
                        break;
                    case "QT1":
                        //Mã đơn vị tính (1)
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrDVTLuong1, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrDVTLuong1, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "QN2":
                        //Số lượng (2)
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoLuong2, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoLuong2, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "QT2":
                        //Mã đơn vị tính (2)
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrDVTLuong2, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrDVTLuong2, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "BPR":
                        //Trị giá hóa đơn
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtTriGiaHoaDon, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtTriGiaHoaDon, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "UPR":
                        //Đơn giá hóa đơn
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtDonGiaHoaDon, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtDonGiaHoaDon, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "UPC":
                        //Mã đồng tiền của đơn giá hóa đơn
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaTTDonGia, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaTTDonGia, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "TSC":
                        //Đơn vị của đơn giá hóa đơn và số lượng
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrDVTDonGia, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrDVTDonGia, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "BPC":
                        //Mã đồng tiền trị giá tính thuế
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaTTTriGiaTinhThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaTTTriGiaTinhThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "DPR":
                        //Trị giá tính thuế
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtTriGiaTinhThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtTriGiaTinhThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "VN1":
                        //Số của mục khai khoản điều chỉnh
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNumber(hmdForm.txtSoMucKhaiKhoanDC1, errorProvider,"", item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNumber(hmdForm.txtSoMucKhaiKhoanDC1, errorProvider,"", item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }                         
                        break;
                    case "VN2":
                        //Số của mục khai khoản điều chỉnh
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNumber(hmdForm.txtSoMucKhaiKhoanDC2, errorProvider,"", item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNumber(hmdForm.txtSoMucKhaiKhoanDC2, errorProvider, "", item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        break;
                    case "VN3":
                        //Số của mục khai khoản điều chỉnh
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNumber(hmdForm.txtSoMucKhaiKhoanDC3, errorProvider, "", item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNumber(hmdForm.txtSoMucKhaiKhoanDC3, errorProvider, "", item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        break;
                    case "VN4":
                        //Số của mục khai khoản điều chỉnh
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNumber(hmdForm.txtSoMucKhaiKhoanDC4, errorProvider, "", item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNumber(hmdForm.txtSoMucKhaiKhoanDC4, errorProvider, "", item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        break;
                    case "VN5":
                        //Số của mục khai khoản điều chỉnh
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNumber(hmdForm.txtSoMucKhaiKhoanDC5, errorProvider, "", item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNumber(hmdForm.txtSoMucKhaiKhoanDC5, errorProvider, "", item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        break;
                    case "TDL":
                        //Số thứ tự của dòng hàng trên tờ khai tạm nhập tái xuất tương ứng
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoTTDongHangTKTNTX, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoTTDongHangTKTNTX, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "TXN":
                        //Số đăng ký danh mục miễn thuế nhập khẩu
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoDMMienThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoDMMienThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "TXR":
                        //Số dòng tương ứng trong danh mục miễn thuế nhập khẩu
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoDongDMMienThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoDongDMMienThue, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "RE":
                        //Mã miễn / Giảm / Không chịu thuế nhập khẩu
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaMienGiam, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateChooseCategory(hmdForm.ctrMaMienGiam, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "REG":
                        //Số tiền giảm thuế nhập khẩu
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoTienMienGiam, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNull(hmdForm.txtSoTienMienGiam, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "TX":
                        //Mã áp dụng thuế suất / Mức thuế và thu khác
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(hmdForm.grListThueThuKhac, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(hmdForm.grListThueThuKhac, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        } 
                        
                        break;
                    case "TR":
                        //Mã miễn / Giảm / Không chịu thuế và thu khác
                        if (!hmdForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(hmdForm.grListThueThuKhac, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }
                        else
                        {
                            hmdForm.Dispose();
                            hmdForm = new VNACC_HangMauDichNhapForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(hmdForm.grListThueThuKhac, errorProvider, "", false, item.ErrorString);
                            hmdForm.TKMD = this.TKMD;
                            hmdForm.MaTienTe = this.MaTienTe_TK;
                            hmdForm.IdTKTamNhapTX = IDTKTamNhapTaiXuat;
#if SXXK_V4 ||GC_V4
                            getLoaiHangHoa();
                            hmdForm.loaiHangHoa = loaiHangHoa;
#endif
                            hmdForm.ShowDialog();
                        }                         
                        break;
                        

                        //Đính kèm điện tử khai báo
                    case "EA1":
                        //Phân loại đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EA2":
                        //Phân loại đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EA3":
                        //Phân loại đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EA4":
                        //Phân loại đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EA5":
                        //Phân loại đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EB1":
                        //Số đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EB2":
                        //Số đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EB3":
                        //Số đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EB4":
                        //Số đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                    case "EB5":
                        //Số đính kèm khai báo điện tử
                        if (!dkForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        else
                        {
                            dkForm.Dispose();
                            dkForm = new VNACC_TK_DinhKiemDTForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(dkForm.grList, errorProvider, "", false, item.ErrorString);
                            dkForm.TKMD = this.TKMD;
                            dkForm.Show();
                        }
                        break;
                        // Giấy phép
                    case "SS1":
                        //Phân loại giấy phép nhập khẩu
                        if (!gpForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        else
                        {
                            gpForm.Dispose();
                            gpForm = new VNACC_TK_GiayPhepForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        break;
                    case "SS2":
                        //Phân loại giấy phép nhập khẩu
                        if (!gpForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        else
                        {
                            gpForm.Dispose();
                            gpForm = new VNACC_TK_GiayPhepForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        break;
                    case "SS3":
                        //Phân loại giấy phép nhập khẩu
                        if (!gpForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        else
                        {
                            gpForm.Dispose();
                            gpForm = new VNACC_TK_GiayPhepForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        break;
                    case "SN1":
                        //Phân loại giấy phép nhập khẩu
                        if (!gpForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        else
                        {
                            gpForm.Dispose();
                            gpForm = new VNACC_TK_GiayPhepForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        break;
                    case "SN2":
                        //Phân loại giấy phép nhập khẩu
                        if (!gpForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        else
                        {
                            gpForm.Dispose();
                            gpForm = new VNACC_TK_GiayPhepForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        break;
                    case "SN3":
                        //Phân loại giấy phép nhập khẩu
                        if (!gpForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        else
                        {
                            gpForm.Dispose();
                            gpForm = new VNACC_TK_GiayPhepForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(gpForm.grList, errorProvider, "", false, item.ErrorString);
                            gpForm.TKMD = this.TKMD;
                            gpForm.Show();
                        }
                        break;
                        // Trung chuyển
                    case "ST1":
                        //Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "ST2":
                        //Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "ST3":
                        //Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "ST4":
                        //Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "ST5":
                        //Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "AD1":
                        //Ngày dự kiến đến(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "AD2":
                        //Ngày dự kiến đến(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "AD3":
                        //Ngày dự kiến đến(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "AD4":
                        //Ngày dự kiến đến(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "AD5":
                        //Ngày dự kiến đến(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "SD1":
                        //Ngày khởi hành vận chuyển(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "SD2":
                        //Ngày khởi hành vận chuyển(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "SD3":
                        //Ngày khởi hành vận chuyển(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "SD4":
                        //Ngày khởi hành vận chuyển(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                    case "SD5":
                        //Ngày khởi hành vận chuyển(Địa điểm trung chuyển)
                        if (!trungchuyenForm.IsDisposed)
                        {
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        else
                        {
                            trungchuyenForm.Dispose();
                            trungchuyenForm = new VNACC_TK_TrungChuyenForm();
                            isValid &= Company.Interface.ValidateControlErrorMessage.ValidateNullGridExDropDowns(trungchuyenForm.grList, errorProvider, "", false, item.ErrorString);
                            trungchuyenForm.TKMD = this.TKMD;
                            trungchuyenForm.Show();
                        }
                        break;
                }
            }
            return isValid;
        }
        

        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            //txtSoToKhaiDauTien.TextChanged += new EventHandler(SetTextChanged_Handler); //Số tờ khai đầu tiên
            ////ctrMaLoaiHinh.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã loại hình
            ////ctrMaPhanLoaiHH.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại hàng hóa
            ////ctrMaPhuongThucVT.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã hiệu phương thức vận chuyển
            ////ctrPhanLoaiToChuc.TextChanged += new EventHandler(SetTextChanged_Handler); //Phân loại cá nhân/tổ chức
            ////ctrCoQuanHaiQuan.TextChanged += new EventHandler(SetTextChanged_Handler); //Cơ quan Hải quan
            ////ctrNhomXuLyHS.TextChanged += new EventHandler(SetTextChanged_Handler); //Nhóm xử lý hồ sơ
            //txtMaDonVi.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã người nhập khẩu
            //txtMaBuuChinhDonVi.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã bưu chính
            //txtSoDienThoaiDonVi.TextChanged += new EventHandler(SetTextChanged_Handler); //Số điện thoại người nhập khẩu
            txtMaUyThac.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã người ủy thác nhập khẩu
            txtMaDoiTac.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã người xuất khẩu
            txtTenDoiTac.TextChanged += new EventHandler(SetTextChanged_Handler); //Tên người xuất khẩu
            //txtMaBuuChinhDoiTac.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã bưu chính
            txtDiaChiDoiTac1.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa chỉ 1(Street and number/P.O.BOX)
            txtDiaChiDoiTac2.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa chỉ 2(Street and number/P.O.BOX)
            txtDiaChiDoiTac3.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa chỉ 3(City name)
            txtDiaChiDoiTac4.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa chỉ 4(Country sub-entity, name)
            ////ctrMaNuocDoiTac.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã nước(Country, coded)
            ////txtNguoiUyThacXK.TextChanged += new EventHandler(SetTextChanged_Handler); //Người ủy thác xuất khẩu
            //txtMaDaiLyHQ.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đại lý Hải quan
            txtSoVanDon.TextChanged += new EventHandler(SetTextChanged_Handler); //Số vận đơn (Số B/L, số AWB v.v. …)
            ////ctrMaDVTSoLuong.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đơn vị tính
            ////ctrMaDVTTrongLuong.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đơn vị tính trọng lượng (Gross)
            ////ctrMaDDLuuKho.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã địa điểm lưu kho hàng chờ thông quan dự kiến
            txtSoHieuKyHieu.TextChanged += new EventHandler(SetTextChanged_Handler); //Ký hiệu và số hiệu
            txtMaPTVC.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phương tiện vận chuyển
            txtTenPTVC.TextChanged += new EventHandler(SetTextChanged_Handler); //Tên tầu (máy bay) chở hàng
            ////ctrDiaDiemDoHang.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã địa điểm dỡ hàng
            ////txtTenDiaDiemDohang.TextChanged += new EventHandler(SetTextChanged_Handler); //Tên địa điểm dỡ hàng
            ////ctrDiaDiemXepHang.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã địa điểm xếp hàng
            ////txtTenDiaDiemXepHang.TextChanged += new EventHandler(SetTextChanged_Handler); //Tên địa điểm xếp hàng
            ////ctrMaKetQuaKiemTra.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã kết quả kiểm tra nội dung
            ////ctrMaVanbanPhapQuy.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã văn bản pháp quy khác
            ////ctrPhanLoai.TextChanged += new EventHandler(SetTextChanged_Handler); //Phân loại giấy phép nhập khẩu
            ////ctrPhanLoaiHD.TextChanged += new EventHandler(SetTextChanged_Handler); //Phân loại hình thức hóa đơn
            txtSoHoaDon.TextChanged += new EventHandler(SetTextChanged_Handler); //Số hóa đơn
            ////ctrPhuongThucTT.TextChanged += new EventHandler(SetTextChanged_Handler); //Phương thức thanh toán
            ////ctrPhanLoaiGiaHD.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại giá hóa đơn
            //ctrMaDieuKienGiaHD.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã điều kiện giá hóa đơn
            //ctrMaTTHoaDon.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đồng tiền của hóa đơn
            //ctrMaPhanLoaiTriGia.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại khai trị giá
            ////txtSoTiepNhanTKTriGia.TextChanged += new EventHandler(SetTextChanged_Handler); //Số tiếp nhận tờ khai trị giá tổng hợp
            ////txtMaTTHieuChinhTriGia.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã tiền tệ của giá cơ sở hiệu chỉnh trị giá
            ////ctrMaPhanLoaiPhiVC.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại phí vận chuyển
            ////txtMaTTPhiVC.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã tiền tệ phí vận chuyển
            ////txtMaPhanLoaiPhiBH.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại bảo hiểm
            ////txtMaTTPhiBH.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã tiền tệ của tiền bảo hiểm
            ////txtSoDangKyBH.TextChanged += new EventHandler(SetTextChanged_Handler); //Số đăng ký bảo hiểm tổng hợp
            ////txtMaTenDieuChinh.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã tên khoản điều chỉnh
            ////ctrMaPhanLoaiDieuChinh.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại điều chỉnh trị giá
            ////txtMaTTDieuChinhTriGia.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã đồng tiền của khoản điều chỉnh trị giá
            ////txtNguoiNopThue.TextChanged += new EventHandler(SetTextChanged_Handler); //Người nộp thuế 
            ////ctrtMaLyDoDeNghiBP.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã lý do đề nghị BP (release before permit)(Phát hành trước giấy phép)
            //txtMaNHTraThueThay.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã ngân hàng trả thuế thay
            //txtKyHieuCTHanMuc.TextChanged += new EventHandler(SetTextChanged_Handler); //Kí hiệu chứng từ hạn mức
            //txtSoCTHanMuc.TextChanged += new EventHandler(SetTextChanged_Handler); //Số chứng từ hạn mức
            ////ctrMaXDThoiHanNopThue.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã xác định thời hạn nộp thuế
            //txtMaNHBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã ngân hàng bảo lãnh
            //txtKyHieuCTBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler); //Kí hiệu chứng từ bảo lãnh
            //txtSoCTBaoLanh.TextChanged += new EventHandler(SetTextChanged_Handler); //Số chứng từ bảo lãnh
            ////ctrDiaDiemDichVC.TextChanged += new EventHandler(SetTextChanged_Handler); //Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
            //txtSoQuanLyNoiBoDN.TextChanged += new EventHandler(SetTextChanged_Handler); //Số quản lý của nội bộ doanh nghiệp

            ////UpperCase
            //txtSoToKhaiDauTien.CharacterCasing = CharacterCasing.Upper; //Số tờ khai đầu tiên
            //ctrMaLoaiHinh.IsUpperCase = true; //Mã loại hình
            //ctrMaPhanLoaiHH.IsUpperCase = true; //Mã phân loại hàng hóa
            //ctrMaPhuongThucVT.IsUpperCase = true; //Mã hiệu phương thức vận chuyển
            //ctrPhanLoaiToChuc.IsUpperCase = true; //Phân loại cá nhân/tổ chức
            //ctrCoQuanHaiQuan.IsUpperCase = true; //Cơ quan Hải quan
            ////ctrNhomXuLyHS.IsUpperCase = true; //Nhóm xử lý hồ sơ
            //txtMaDonVi.CharacterCasing = CharacterCasing.Upper; //Mã người nhập khẩu
            //txtMaBuuChinhDonVi.CharacterCasing = CharacterCasing.Upper; //Mã bưu chính
            //txtSoDienThoaiDonVi.CharacterCasing = CharacterCasing.Upper; //Số điện thoại người nhập khẩu
            //txtMaUyThac.CharacterCasing = CharacterCasing.Upper; //Mã người ủy thác nhập khẩu
            //txtMaDoiTac.CharacterCasing = CharacterCasing.Upper; //Mã người xuất khẩu
            //txtTenDoiTac.CharacterCasing = CharacterCasing.Upper; //Tên người xuất khẩu
            //txtMaBuuChinhDoiTac.CharacterCasing = CharacterCasing.Upper; //Mã bưu chính
            //txtDiaChiDoiTac1.CharacterCasing = CharacterCasing.Upper; //Địa chỉ 1(Street and number/P.O.BOX)
            //txtDiaChiDoiTac2.CharacterCasing = CharacterCasing.Upper; //Địa chỉ 2(Street and number/P.O.BOX)
            //txtDiaChiDoiTac3.CharacterCasing = CharacterCasing.Upper; //Địa chỉ 3(City name)
            //txtDiaChiDoiTac4.CharacterCasing = CharacterCasing.Upper; //Địa chỉ 4(Country sub-entity, name)
            //ctrMaNuocDoiTac.IsUpperCase = true; //Mã nước(Country, coded)
            ////txtNguoiUyThacXK.CharacterCasing = CharacterCasing.Upper; //Người ủy thác xuất khẩu
            //txtMaDaiLyHQ.CharacterCasing = CharacterCasing.Upper; //Mã đại lý Hải quan
            //txtSoVanDon.CharacterCasing = CharacterCasing.Upper; //Số vận đơn (Số B/L, số AWB v.v. …)
            //ctrMaDVTSoLuong.IsUpperCase = true; //Mã đơn vị tính
            //ctrMaDVTTrongLuong.IsUpperCase = true; //Mã đơn vị tính trọng lượng (Gross)
            //ctrMaDDLuuKho.IsUpperCase = true; //Mã địa điểm lưu kho hàng chờ thông quan dự kiến
            //txtSoHieuKyHieu.CharacterCasing = CharacterCasing.Upper; //Ký hiệu và số hiệu
            //txtMaPTVC.CharacterCasing = CharacterCasing.Upper; //Mã phương tiện vận chuyển
            //txtTenPTVC.CharacterCasing = CharacterCasing.Upper; //Tên tầu (máy bay) chở hàng
            //ctrDiaDiemDoHang.IsUpperCase = true; //Mã địa điểm dỡ hàng
            ////txtTenDiaDiemDohang.CharacterCasing = CharacterCasing.Upper; //Tên địa điểm dỡ hàng
            //ctrDiaDiemXepHang.IsUpperCase = true; //Mã địa điểm xếp hàng
            ////txtTenDiaDiemXepHang.CharacterCasing = CharacterCasing.Upper; //Tên địa điểm xếp hàng
            //ctrMaKetQuaKiemTra.IsUpperCase = true; //Mã kết quả kiểm tra nội dung
            ////ctrMaVanbanPhapQuy.CharacterCasing = CharacterCasing.Upper; //Mã văn bản pháp quy khác
            ////txtPhanLoai.CharacterCasing = CharacterCasing.Upper; //Phân loại giấy phép nhập khẩu
            ////txtSoGiayPhep.CharacterCasing = CharacterCasing.Upper; //Số của giấy phép
            //ctrPhanLoaiHD.IsUpperCase = true; //Phân loại hình thức hóa đơn
            //txtSoHoaDon.CharacterCasing = CharacterCasing.Upper; //Số hóa đơn
            //ctrPhuongThucTT.IsUpperCase = true; //Phương thức thanh toán
            ////txtPhanLoaiGiaHD.CharacterCasing = CharacterCasing.Upper; //Mã phân loại giá hóa đơn
            ////txtMaDieuKienGiaHD.CharacterCasing = CharacterCasing.Upper; //Mã điều kiện giá hóa đơn
            ////txtMaTTHoaDon.CharacterCasing = CharacterCasing.Upper; //Mã đồng tiền của hóa đơn
            //ctrMaPhanLoaiTriGia.IsUpperCase = true; //Mã phân loại khai trị giá
            ////txtSoTiepNhanTKTriGia.CharacterCasing = CharacterCasing.Upper; //Số tiếp nhận tờ khai trị giá tổng hợp
            ////txtMaTTHieuChinhTriGia.CharacterCasing = CharacterCasing.Upper; //Mã tiền tệ của giá cơ sở hiệu chỉnh trị giá
            ////txtMaPhanLoaiPhiVC.CharacterCasing = CharacterCasing.Upper; //Mã phân loại phí vận chuyển
            ////txtMaTTPhiVC.CharacterCasing = CharacterCasing.Upper; //Mã tiền tệ phí vận chuyển
            ////txtMaPhanLoaiPhiBH.CharacterCasing = CharacterCasing.Upper; //Mã phân loại bảo hiểm
            ////txtMaTTPhiBH.CharacterCasing = CharacterCasing.Upper; //Mã tiền tệ của tiền bảo hiểm
            ////txtSoDangKyBH.CharacterCasing = CharacterCasing.Upper; //Số đăng ký bảo hiểm tổng hợp
            ////txtMaTenDieuChinh.CharacterCasing = CharacterCasing.Upper; //Mã tên khoản điều chỉnh
            ////txtMaPhanLoaiDieuChinh.CharacterCasing = CharacterCasing.Upper; //Mã phân loại điều chỉnh trị giá
            ////txtMaTTDieuChinhTriGia.CharacterCasing = CharacterCasing.Upper; //Mã đồng tiền của khoản điều chỉnh trị giá
            ////txtNguoiNopThue.CharacterCasing = CharacterCasing.Upper; //Người nộp thuế 
            ////txtMaLyDoDeNghiBP.CharacterCasing = CharacterCasing.Upper; //Mã lý do đề nghị BP (release before permit)(Phát hành trước giấy phép)"
            //txtMaNHTraThueThay.CharacterCasing = CharacterCasing.Upper; //Mã ngân hàng trả thuế thay
            //txtKyHieuCTHanMuc.CharacterCasing = CharacterCasing.Upper; //Kí hiệu chứng từ hạn mức
            //txtSoCTHanMuc.CharacterCasing = CharacterCasing.Upper; //Số chứng từ hạn mức
            //ctrMaXDThoiHanNopThue.IsUpperCase = true; //Mã xác định thời hạn nộp thuế
            //txtMaNHBaoLanh.CharacterCasing = CharacterCasing.Upper; //Mã ngân hàng bảo lãnh
            //txtKyHieuCTBaoLanh.CharacterCasing = CharacterCasing.Upper; //Kí hiệu chứng từ bảo lãnh
            //txtSoCTBaoLanh.CharacterCasing = CharacterCasing.Upper; //Số chứng từ bảo lãnh
            ////txtPhanLoai.CharacterCasing = CharacterCasing.Upper; //Phân loại đính kèm khai báo điện tử
            ////txtMaDiaDiem.CharacterCasing = CharacterCasing.Upper; //Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp)
            ////txtDiaDiemDichVC.CharacterCasing = CharacterCasing.Upper; //Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
            //txtSoQuanLyNoiBoDN.CharacterCasing = CharacterCasing.Upper; //Số quản lý của nội bộ doanh nghiệp
            ////txtPhanLoai.CharacterCasing = CharacterCasing.Upper; //Phân loại chỉ thị của Hải quan

        }

        private void ctrMaNuocDoiTac_Leave(object sender, EventArgs e)
        {
            //             if (!string.IsNullOrEmpty(ctrMaNuocDoiTac.Code))
            //             {
            //                 ctrDiaDiemXepHang.Where = string.Format(ctrMaNuocDoiTac.Code);
            //                 ctrDiaDiemXepHang.ReLoadData();
            //             }
        }

        private void txtTenDoiTac_ButtonClick(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                DonViDoiTacForm f = new DonViDoiTacForm();
                f.isBrower = true;
                f.ShowDialog(this);

                ////if (f.doiTac != null && f.doiTac.TenCongTy != "")
                if (f.DialogResult == DialogResult.OK)
                {
                    txtMaDoiTac.Text = string.IsNullOrEmpty(f.doiTac.MaCongTy) ? "" : f.doiTac.MaCongTy.Trim().ToUpper();
                    txtTenDoiTac.Text = string.IsNullOrEmpty(f.doiTac.TenCongTy) ? "" : f.doiTac.TenCongTy.Trim().ToUpper();
                    txtDiaChiDoiTac1.Text = string.IsNullOrEmpty(f.doiTac.DiaChi) ? "" : f.doiTac.DiaChi.Trim().ToUpper();
                    txtDiaChiDoiTac2.Text = string.IsNullOrEmpty(f.doiTac.DiaChi2) ? "" : f.doiTac.DiaChi2.ToUpper();
                    txtMaBuuChinhDoiTac.Text = string.IsNullOrEmpty(f.doiTac.BuuChinh) ? "" : f.doiTac.BuuChinh.Trim().ToUpper();
                    txtDiaChiDoiTac3.Text = string.IsNullOrEmpty(f.doiTac.TinhThanh) ? "" : f.doiTac.TinhThanh.Trim().ToUpper();
                    txtDiaChiDoiTac4.Text = string.IsNullOrEmpty(f.doiTac.QuocGia) ? "" : f.doiTac.QuocGia.Trim().ToUpper();
                    ctrMaNuocDoiTac.Code = string.IsNullOrEmpty(f.doiTac.MaNuoc) ? "" : f.doiTac.MaNuoc.Trim().ToUpper();
                    txtMaDaiLyHQ.Text = string.IsNullOrEmpty(f.doiTac.MaDaiLyHQ) ? "" : f.doiTac.MaDaiLyHQ.Trim().ToUpper();
                    txtTenDaiLyHaiQuan.Text = string.IsNullOrEmpty(f.doiTac.TenDaiLyHQ) ? "" : f.doiTac.TenDaiLyHQ.Trim().ToUpper();


                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

        }
        private void ctrMaTTHoaDon_Load(object sender, EventArgs e)
        {
            MaTienTe_TK = ctrMaTTHoaDon.Code;
        }

        private void ctrMaTTHoaDon_Leave(object sender, EventArgs e)
        {
            MaTienTe_TK = ctrMaTTHoaDon.Code;
        }


        private void txt_Leave(object sender, EventArgs e)
        {
            Control ctr = (Control)sender;
            ctr.Text = ctr.Text.ToString().Trim().ToUpper();

        }

        private void ShowKetQuaXuLy()
        {
            VNACC_KetQuaXuLy f = new VNACC_KetQuaXuLy(TKMD.ID, "TKMD");
            //f.master_id = TKMD.ID;
            f.ShowDialog(this);
        }
        private void InToKhaiTam()
        {
            MsgLog msg = MsgLog.GetThongTinToKhaiTam(TKMD.InputMessageID, "IDA");
            if (msg != null && msg.ID > 0)
                ProcessReport.ShowReport(msg.ID, "VAD1AC0", new ResponseVNACCS(msg.Log_Messages).ResponseData);
            else
                ShowMessage("Không tìm thấy bản khai tạm", false);
        }

        private void txtSoHD_ButtonClick(object sender, EventArgs e)
        {
#if GC_V4
            if (TKMD.HangCollection.Count == 0)
            {
                Company.Interface.KDT.GC.HopDongManageForm f = new Company.Interface.KDT.GC.HopDongManageForm();
                f.IsBrowseForm = true;
                f.IsDaDuyet = true;
                f.ShowDialog();
                if (!string.IsNullOrEmpty(f.HopDongSelected.SoHopDong))
                {

                    TKMD.HopDong_ID = f.HopDongSelected.ID;
                    string sohopdong = string.Empty;
                    if (f.HopDongSelected.ActionStatus == 999)
                        sohopdong = "1";
                    else
                        sohopdong = "2";
                    if (f.HopDongSelected.MaHaiQuan != "" && f.HopDongSelected.MaHaiQuan.Length >= 4)
                        sohopdong = sohopdong + f.HopDongSelected.MaHaiQuan.Substring(1, 2);
                    if (f.HopDongSelected.NgayDangKy.Year > 1900)
                        sohopdong = sohopdong + f.HopDongSelected.NgayDangKy.Year.ToString().Trim();
                    if (f.HopDongSelected.SoTiepNhan != 0)
                        sohopdong = sohopdong + f.HopDongSelected.SoTiepNhan.ToString().Trim();
                    TKMD.HopDong_So = sohopdong;
                    txtSoHD.Text = f.HopDongSelected.SoHopDong;
                    lblNgayHD1.Text = f.HopDongSelected.NgayKy.ToString("dd/MM/yyyy");
                }
            }
             else
            {
            ShowMessage("Xóa thông tin hàng trước khi thay đổi hợp đồng", false);
            }
#endif
        }
        private long IDTKTamNhapTaiXuat = 0;
        private void txtSoToKhaiTNTX_ButtonClick(object sender, EventArgs e)
        {
            VNACC_ToKhaiManager f = new VNACC_ToKhaiManager();
            f.IsShowChonToKhai = true;
            f.ShowDialog(this);
            if (f.DialogResult == DialogResult.OK)
            {
                IDTKTamNhapTaiXuat = f.TKMDDuocChon.ID;
                txtSoToKhaiTNTX.Text = f.TKMDDuocChon.SoToKhai.ToString();

            }
        }

        private void txtSoToKhaiTNTX_ValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSoToKhaiTNTX.Text) && txtSoToKhaiTNTX.Text != "0")
            {
                List<KDT_VNACC_ToKhaiMauDich> listtkmd = KDT_VNACC_ToKhaiMauDich.SelectCollectionDynamic("SoToKhai = " + txtSoToKhaiTNTX.Text, null);
                if (listtkmd.Count == 1)
                {
                    IDTKTamNhapTaiXuat = listtkmd[0].ID;
                }

            }
        }

        private void txtTenPTVC_ButtonClick(object sender, EventArgs e)
        {
            //MaPTVTFrm frm = new MaPTVTFrm();
            //frm.ShowDialog();
        }

        private bool checkMaHang(string MaHang, string loai)
        {
            bool ketqua = true;
            switch (loai)
            {
#if SXXK_V4
                case "N":
                    Company.BLL.SXXK.NguyenPhuLieu Npl = new Company.BLL.SXXK.NguyenPhuLieu();
                    Npl.Ma = MaHang;
                    Npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    Npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    ketqua = Npl.Load();
                    break;

                case "S":
                    ketqua = Company.BLL.SXXK.DinhMuc.checkIsExist(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, MaHang);
                    break;
#elif GC_V4
                case "N":

                    Company.GC.BLL.GC.NguyenPhuLieu nplGC = new Company.GC.BLL.GC.NguyenPhuLieu();
                    nplGC.HopDong_ID = TKMD.HopDong_ID;
                    nplGC.Ma = MaHang;
                    ketqua = nplGC.Load();
                    break;

                case "S":
                    Company.GC.BLL.GC.DinhMuc dinhmuc = new Company.GC.BLL.GC.DinhMuc();
                    dinhmuc.MaSanPham = MaHang;
                    dinhmuc.HopDong_ID = TKMD.HopDong_ID;
                    ketqua= dinhmuc.CheckExitsDinhMucSanPham();
                    break;

                case "T":
                    Company.GC.BLL.GC.ThietBi tbGC = new Company.GC.BLL.GC.ThietBi();
                    tbGC.HopDong_ID = TKMD.HopDong_ID;
                    tbGC.Ma = MaHang;
                    ketqua = tbGC.Load();
                    break;
#endif
                case "":
                    break;
            }
            return ketqua;
        }
        private void btnMaLoaiHinh_Click(object sender, EventArgs e)
        {
            MapperForm f = new MapperForm();
            f.TKMD = this.TKMD;
            f.LoaiMapper = "MaLoaiHinh";
            f.ShowDialog();
            if (f.DialogResult == DialogResult.OK)
            {
                ctrMaLoaiHinh.Code = f.mapper.CodeV5.Trim();
            }
        }

        private void btnMaHQ_Click(object sender, EventArgs e)
        {
            MapperForm f = new MapperForm();
            f.TKMD = this.TKMD;
            f.LoaiMapper = "MaHQ";
            f.ShowDialog();
            if (f.DialogResult == DialogResult.OK)
            {
                ctrCoQuanHaiQuan.Code = f.mapper.CodeV5.Trim();
            }
        }
        private void ReloadData()
        {
            try
            {
                this.UseWaitCursor = true;
                MsgPhanBo.UpdateMsgPhanBo(this.TKMD.SoToKhai.ToString());
                TuDongCapNhatThongTin();
                this.UseWaitCursor = false;
                ShowMessage("Cập nhật thành công tờ khai", false);
            }
            catch (System.Exception ex)
            {
                ShowMessage("Lỗi khi cập nhật tờ khai " + ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void VNACC_ToKhaiMauDichNhapForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Tờ khai Nhập khẩu  có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    this.saveToKhai();
                    IsChange = false;
                    this.Close();
                }
            }
        }
    }
}
