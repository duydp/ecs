﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SOFTECH.ECS.V3.Components.KD
{
    public partial class HangMauDich_GiayPhep
    {
        //HangMauDich_GiayPhep
        #region Properties.

        public long HangMauDich_ID { set; get; }
        public long GiayPhep_ID { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static IList<HangMauDich_GiayPhep> ConvertToCollection(IDataReader reader)
        {
            IList<HangMauDich_GiayPhep> collection = new List<HangMauDich_GiayPhep>();
            while (reader.Read())
            {
                HangMauDich_GiayPhep entity = new HangMauDich_GiayPhep();
                if (!reader.IsDBNull(reader.GetOrdinal("HangMauDich_ID"))) entity.HangMauDich_ID = reader.GetInt64(reader.GetOrdinal("HangMauDich_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayPhep_ID"))) entity.GiayPhep_ID = reader.GetInt64(reader.GetOrdinal("GiayPhep_ID"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static HangMauDich_GiayPhep Load(long hangMauDich_ID)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_GiayPhep_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HangMauDich_ID", SqlDbType.BigInt, hangMauDich_ID);
            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<HangMauDich_GiayPhep> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------
        public static IList<HangMauDich_GiayPhep> SelectCollectionAll()
        {
            IDataReader reader = SelectReaderAll();
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        public static IList<HangMauDich_GiayPhep> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IList<HangMauDich_GiayPhep> SelectCollectionBy_GiayPhep_ID(long giayPhep_ID)
        {
            IDataReader reader = SelectReaderBy_GiayPhep_ID(giayPhep_ID);
            return ConvertToCollection(reader);
        }
        //---------------------------------------------------------------------------------------------
        public static IList<HangMauDich_GiayPhep> SelectCollectionBy_HangMauDich_ID(long hangMauDich_ID)
        {
            IDataReader reader = SelectReaderBy_HangMauDich_ID(hangMauDich_ID);
            return ConvertToCollection(reader);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_GiayPhep_ID(long giayPhep_ID)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_GiayPhep_SelectBy_GiayPhep_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, giayPhep_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static DataSet SelectBy_HangMauDich_ID(long hangMauDich_ID)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_GiayPhep_SelectBy_HangMauDich_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HangMauDich_ID", SqlDbType.BigInt, hangMauDich_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_GiayPhep_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_GiayPhep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_GiayPhep_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_GiayPhep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static IDataReader SelectReaderBy_GiayPhep_ID(long giayPhep_ID)
        {
            const string spName = "p_KDT_HangMauDich_GiayPhep_SelectBy_GiayPhep_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, giayPhep_ID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------
        public static IDataReader SelectReaderBy_HangMauDich_ID(long hangMauDich_ID)
        {
            const string spName = "p_KDT_HangMauDich_GiayPhep_SelectBy_HangMauDich_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HangMauDich_ID", SqlDbType.BigInt, hangMauDich_ID);

            return db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertGiayPhep(long giayPhep_ID)
        {
            HangMauDich_GiayPhep entity = new HangMauDich_GiayPhep();
            entity.GiayPhep_ID = giayPhep_ID;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_GiayPhep_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@HangMauDich_ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, GiayPhep_ID);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                HangMauDich_ID = (long)db.GetParameterValue(dbCommand, "@HangMauDich_ID");
                return HangMauDich_ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                HangMauDich_ID = (long)db.GetParameterValue(dbCommand, "@HangMauDich_ID");
                return HangMauDich_ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<HangMauDich_GiayPhep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangMauDich_GiayPhep item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateGiayPhep(long hangMauDich_ID, long giayPhep_ID)
        {
            HangMauDich_GiayPhep entity = new HangMauDich_GiayPhep();
            entity.HangMauDich_ID = hangMauDich_ID;
            entity.GiayPhep_ID = giayPhep_ID;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_HangMauDich_GiayPhep_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HangMauDich_ID", SqlDbType.BigInt, HangMauDich_ID);
            db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, GiayPhep_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<HangMauDich_GiayPhep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangMauDich_GiayPhep item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateGiayPhep(long hangMauDich_ID, long giayPhep_ID)
        {
            HangMauDich_GiayPhep entity = new HangMauDich_GiayPhep();
            entity.HangMauDich_ID = hangMauDich_ID;
            entity.GiayPhep_ID = giayPhep_ID;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_GiayPhep_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HangMauDich_ID", SqlDbType.BigInt, HangMauDich_ID);
            db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, GiayPhep_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<HangMauDich_GiayPhep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangMauDich_GiayPhep item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteGiayPhep(long hangMauDich_ID)
        {
            HangMauDich_GiayPhep entity = new HangMauDich_GiayPhep();
            entity.HangMauDich_ID = hangMauDich_ID;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_GiayPhep_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HangMauDich_ID", SqlDbType.BigInt, HangMauDich_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_GiayPhep_ID(long giayPhep_ID)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_GiayPhep_DeleteBy_GiayPhep_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, giayPhep_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_HangMauDich_ID(long hangMauDich_ID)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_GiayPhep_DeleteBy_HangMauDich_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HangMauDich_ID", SqlDbType.BigInt, hangMauDich_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_HangMauDich_GiayPhep_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<HangMauDich_GiayPhep> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (HangMauDich_GiayPhep item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}