﻿using System;
using System.Drawing;
using Company.BLL.DuLieuChuan;
using Company.BLL.SXXK.ToKhai ;
using Company.Interface.SXXK;
using Janus.Windows.GridEX.EditControls;
using Company.BLL.Utils;

namespace Company.Interface
{
    public partial class LoaiHinhKhacModifyForm : BaseForm
    {
        public bool IsEdited = false;
        public bool IsDeleted = false;
        //-----------------------------------------------------------------------------------------
        public HangMauDich HMD = new HangMauDich();
        public HangMauDichCollection collection=new HangMauDichCollection();
        public string LoaiHangHoa = "";
        public string NhomLoaiHinh = string.Empty;
        
        public decimal TyGiaTT;
        public string MaNguyenTe;
        public string MaNuocXX;
        public string MaLoaiHinh = "";
        //-----------------------------------------------------------------------------------------				
        // Tính thuế
        private decimal luong;
        private decimal dgnt;
        private decimal tgnt;

        private decimal tgtt_nk;
        private decimal tgtt_ttdb;
        private decimal tgtt_gtgt;
        private decimal clg;

        private decimal ts_nk;
        private decimal ts_ttdb;
        private decimal ts_gtgt;
        private decimal tl_clg;

        private decimal tt_nk;
        private decimal tt_ttdb;
        private decimal tt_gtgt;
        private decimal st_clg;
        private bool edit = false;
        //-----------------------------------------------------------------------------------------
        private string MaHangOld = "";
        public LoaiHinhKhacModifyForm()
        {
            InitializeComponent();
        }

        private void khoitao_GiaoDien()
        {
            //if (this.NhomLoaiHinh.Substring(1, 2) == "GC" || this.NhomLoaiHinh.Substring(1, 2) == "SX")
            //{
              txtMaHang.ButtonStyle = EditButtonStyle.Ellipsis;
             //   txtTenHang.ReadOnly = cbDonViTinh.ReadOnly = true;
                txtMaHang.BackColor = txtTenHang.BackColor = cbDonViTinh.BackColor = Color.FromArgb(255, 255, 192);
            //}
            
        }
        
        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
        }
        
        private decimal tinhthue()
        {
            this.tgnt = this.dgnt * this.luong;
            if (edit) this.tgtt_nk = Convert.ToDecimal(txtTGTT_NK.Value);
            else this.tgtt_nk = this.tgnt * this.TyGiaTT;

            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk,MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = this.tt_ttdb;
            txtTienThue_GTGT.Value = this.tt_gtgt;
            txtTien_CLG.Value = this.st_clg;

            txtTongSoTienThue.Value = this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg;

            return this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg;
        }

        //-----------------------------------------------------------------------------------------

        private void HangMauDichEditForm_Load(object sender, EventArgs e)
        {
            ctrNuocXX.Ma = this.HMD.NuocXX_ID;
            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDien();
            lblNguyenTe_DGNT.Text = lblNguyenTe_TGNT.Text = "(" + this.MaNguyenTe + ")";
            toolTip1.SetToolTip(lblNguyenTe_DGNT, this.MaNguyenTe);
            toolTip1.SetToolTip(lblNguyenTe_TGNT, this.MaNguyenTe);

            // Tính lại thuế.
            //this.HMD.TinhThue(this.TyGiaTT);
            if (GlobalSettings.NGON_NGU == "0")
            {
                lblTyGiaTT.Text = "Tỷ giá VNĐ: " + this.TyGiaTT.ToString("N");
            }
            else
            {
                lblTyGiaTT.Text = "VND rate: " + this.TyGiaTT.ToString("N");
            }
            
            
            // Bind data.
            txtMaHang.Text = this.HMD.MaPhu;
            txtTenHang.Text = this.HMD.TenHang;
            txtMaHS.Text = this.HMD.MaHS;
            cbDonViTinh.SelectedValue = this.HMD.DVT_ID ; //
            
            //luu lai ma hang
            MaHangOld = this.HMD.MaPhu;
            
            txtDGNT.Value = this.dgnt = this.HMD.DonGiaKB;
            txtLuong.Value = this.luong = this.HMD.SoLuong;
            txtTGNT.Value = this.HMD.TriGiaKB;
            txtTriGiaKB.Value = this.HMD.TriGiaKB_VND;
            txtTGTT_NK.Value = this.HMD.TriGiaTT;
            txtTS_NK.Value = this.ts_nk = this.HMD.ThueSuatXNK;
            txtTS_TTDB.Value = this.ts_ttdb = this.HMD.ThueSuatTTDB;
            txtTS_GTGT.Value = this.ts_gtgt = this.HMD.ThueSuatGTGT;
            txtTL_CLG.Value = this.tl_clg = this.HMD.TyLeThuKhac;
            ctrNuocXX.Ma = this.HMD.NuocXX_ID;
            this.ts_nk = this.HMD.ThueSuatXNK / 100;
            this.ts_ttdb = this.HMD.ThueSuatTTDB / 100;
            this.ts_gtgt = this.HMD.ThueSuatGTGT / 100;
            this.tl_clg = this.HMD.TyLeThuKhac / 100;            
            
            this.tinhthue();
        }

        private void txtLuong_Leave(object sender, EventArgs e)
        {
            try{
                this.luong = Convert.ToDecimal(txtLuong.Value);
                this.tinhthue();
                epError.SetError(txtDGNT, null);
                epError.SetError(txtLuong, null);
            }catch
            {
                epError.SetError(txtDGNT, setText("Dữ liệu không hợp lệ", "Invalid input value"));
                epError.SetError(txtLuong, setText("Dữ liệu không hợp lệ", "Invalid input value"));
            }
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            try{
                this.dgnt = Convert.ToDecimal(txtDGNT.Value);
                this.tinhthue();
                epError.SetError(txtDGNT, null);
                epError.SetError(txtLuong, null);
            }
            catch
            {
                epError.SetError(txtDGNT, setText("Dữ liệu không hợp lệ", "Invalid input value"));
                epError.SetError(txtLuong, setText("Dữ liệu không hợp lệ", "Invalid input value"));
            
            }
        }

        private void txtTS_NK_Leave(object sender, EventArgs e)
        {
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            this.tinhthue();
        }

        private void txtTS_TTDB_Leave(object sender, EventArgs e)
        {
            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
            this.tinhthue();
        }

        private void txtTS_GTGT_Leave(object sender, EventArgs e)
        {
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
            this.tinhthue();
        }

        private void txtTS_CLG_Leave(object sender, EventArgs e)
        {
            this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;
            this.tinhthue();
        }

        //-----------------------------------------------------------------------------------------------

        private void chkMienThue_CheckedChanged(object sender, EventArgs e)
        {
            grbThue.Enabled = !chkMienThue.Checked;
            txtTS_NK.Value = 0;
            txtTS_TTDB.Value = 0;
            txtTS_GTGT.Value = 0;
            txtTL_CLG.Value = 0;
            this.tinhthue();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            switch (this.NhomLoaiHinh)
            {
                case "N":
                    NguyenPhuLieuRegistedForm f = new NguyenPhuLieuRegistedForm();
                    f.CalledForm = "HangMauDichForm";
                    f.MaHaiQuan = this.MaHaiQuan.Trim();
                    f.ShowDialog();
                    if (f.NguyenPhuLieuSelected.Ma != "")
                    {
                        txtMaHang.Text = f.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = f.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = f.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = f.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                    }
                    break;
                case "X":
                    if (this.LoaiHangHoa == "N")
                    {
                        NguyenPhuLieuRegistedForm fNPLX = new NguyenPhuLieuRegistedForm();
                        fNPLX.CalledForm = "HangMauDichForm";
                        fNPLX.MaHaiQuan = this.MaHaiQuan.Trim();
                        fNPLX.ShowDialog();
                        if (fNPLX.NguyenPhuLieuSelected.Ma != "")
                        {
                            txtMaHang.Text = fNPLX.NguyenPhuLieuSelected.Ma;
                            txtTenHang.Text = fNPLX.NguyenPhuLieuSelected.Ten;
                            txtMaHS.Text = fNPLX.NguyenPhuLieuSelected.MaHS;
                            cbDonViTinh.SelectedValue = fNPLX.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                        }
                    }
                    else
                    {
                        SanPhamRegistedForm fSPX = new SanPhamRegistedForm();
                        fSPX.CalledForm = "HangMauDichForm";
                        fSPX.MaHaiQuan = this.MaHaiQuan.Trim();
                        fSPX.ShowDialog();
                        if (fSPX.SanPhamSelected.Ma != "")
                        {
                            txtMaHang.Text = fSPX.SanPhamSelected.Ma;
                            txtTenHang.Text = fSPX.SanPhamSelected.Ten;
                            txtMaHS.Text = fSPX.SanPhamSelected.MaHS;
                            cbDonViTinh.SelectedValue = fSPX.SanPhamSelected.DVT_ID.PadRight(3);
                        }
                    }
                    break;               
            }
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                epError.SetIconPadding(txtMaHS, -8);
               // epError.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    epError.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                }
                else
                {
                    epError.SetError(txtMaHS, "HS code is invalid");
                }
                return;
            }
           
            this.HMD.MaHS = txtMaHS.Text;
            this.HMD.MaPhu = txtMaHang.Text;
            this.HMD.TenHang = txtTenHang.Text;
            this.HMD.NuocXX_ID = ctrNuocXX.Ma;
            this.HMD.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            this.HMD.SoLuong = Convert.ToDecimal(txtLuong.Text);
            this.HMD.DonGiaKB = Convert.ToDecimal(txtDGNT.Text);
            this.HMD.TriGiaKB = Convert.ToDecimal(txtTGNT.Text);
            this.HMD.TriGiaTT = Convert.ToDecimal(txtTGTT_NK.Text);
            this.HMD.TriGiaKB_VND = Convert.ToDecimal(txtTriGiaKB.Value);
            this.HMD.ThueSuatXNK = Convert.ToDecimal(txtTS_NK.Text);
            this.HMD.ThueSuatTTDB = Convert.ToDecimal(txtTS_TTDB.Text);
            this.HMD.ThueSuatGTGT = Convert.ToDecimal(txtTS_GTGT.Text);
            this.HMD.ThueXNK = Convert.ToDecimal(txtTienThue_NK.Text);
            this.HMD.ThueTTDB = Convert.ToDecimal(txtTienThue_TTDB.Text);
            this.HMD.ThueGTGT = Convert.ToDecimal(txtTienThue_GTGT.Text);
            this.HMD.Ton = Convert.ToDecimal(txtLuong.Text);
            if (chkMienThue.Checked)
                this.HMD.MienThue = 1;
            else
                this.HMD.MienThue = 0;

            // Tính thuế.
            this.HMD.TinhThue(this.TyGiaTT);
            this.IsEdited = true;
            txtMaHang.Text = "";
            txtTenHang.Text = "";
            txtMaHS.Text = "";
            txtLuong.Value = 0;
            txtDGNT.Value = 0;
            txtTGTT_NK.Value = 0;
            this.tinhthue();
            txtTGNT.Value = 0;
            txtTriGiaKB.Value = 0;
            this.edit = false;
            epError.Clear();
            //txtMaHang.Focus();
            this.Close();
        }
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangMauDich hmd in collection)
            {
                if (hmd.MaPhu.Trim() == maHang && maHang!=MaHangOld) return true;               
            }
            return false;
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
           // if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa hàng này không?", "MSG_DEL01", "", true) == "Yes")
            {
                this.IsDeleted = true;
                this.Close();
            }
        }

        private void txtMaHang_Leave(object sender, EventArgs e)
        {
            if (this.MaLoaiHinh.Contains("SX"))
            {
                if (LoaiHangHoa == "N")
                {
                    Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                    npl.Ma = txtMaHang.Text.Trim();
                    npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    if (npl.Load())
                    {
                        txtMaHang.Text = npl.Ma;
                        txtMaHS.Text = npl.MaHS;
                        txtTenHang.Text = npl.Ten;
                        cbDonViTinh.SelectedValue = npl.DVT_ID;
                    }
                    else
                    {
                        epError.SetIconPadding(txtMaHang, -8);
                        //epError.SetError(txtMaHang, "Không tồn tại nguyên phụ liệu này.");
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            epError.SetError(txtMaHang, "Không tồn tại nguyên phụ liệu này.");
                        }
                        else
                        {
                            epError.SetError(txtMaHang, "This material haven't exist.");
                        }

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                        cbDonViTinh.SelectedValue = "";
                    }
                }
                else
                {
                    Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                    sp.Ma = txtMaHang.Text.Trim();
                    sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    sp.MaHaiQuan = MaHaiQuan;
                    if (sp.Load())
                    {
                        txtMaHang.Text = sp.Ma;
                        txtMaHS.Text = sp.MaHS;
                        txtTenHang.Text = sp.Ten;
                        cbDonViTinh.SelectedValue = sp.DVT_ID;
                    }
                    else
                    {
                        epError.SetIconPadding(txtMaHang, -8);
                       // epError.SetError(txtMaHang, "Không tồn tại sản phẩm này.");
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            epError.SetError(txtMaHang, "Không tồn tại sản phẩm này.");
                        }
                        else
                        {
                            epError.SetError(txtMaHang, "This product haven't exist.");
                        }
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                        cbDonViTinh.SelectedValue = "";
                    }
                }
            }
        }

        private void txtDGNT_Click(object sender, EventArgs e)
        {

        }

        private void txtTGTT_NK_Leave(object sender, EventArgs e)
        {
            this.tinhthue();
        }

        private void txtTGTT_NK_Click(object sender, EventArgs e)
        {
            this.edit = true;
        }

        

    }
}
