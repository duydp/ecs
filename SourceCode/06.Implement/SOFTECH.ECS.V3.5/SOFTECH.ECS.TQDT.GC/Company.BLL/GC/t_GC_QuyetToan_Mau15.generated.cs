﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.GC
{
	public partial class t_GC_QuyetToan_Mau15 : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public int SoTaiKhoan { set; get; }
		public int HopDong_ID { set; get; }
		public string SoHopDong { set; get; }
		public long SoToKhai { set; get; }
		public decimal SoToKhaiVnacc { set; get; }
		public DateTime NgayDangKy { set; get; }
		public string MaLoaiHinh { set; get; }
		public string Ma { set; get; }
		public string Ten { set; get; }
		public string MaHS { set; get; }
		public string DVT_ID { set; get; }
		public decimal SoLuongDangKy { set; get; }
		public decimal SoLuongDaNhap { set; get; }
		public decimal SoLuongDaDung { set; get; }
		public decimal SoLuongCungUng { set; get; }
		public decimal LuongTon { set; get; }
		public decimal SoLuong { set; get; }
		public decimal LuongSD_TK { set; get; }
		public decimal LuongTon_TK { set; get; }
		public decimal DonGiaKB { set; get; }
		public decimal DonGiaTT { set; get; }
		public decimal TriGiaTT { set; get; }
		public decimal TriGiaSuDung { set; get; }
		public decimal TonTriGia { set; get; }
		public decimal TyGiaTT { set; get; }
		public string MaDVT { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<t_GC_QuyetToan_Mau15> ConvertToCollection(IDataReader reader)
		{
			List<t_GC_QuyetToan_Mau15> collection = new List<t_GC_QuyetToan_Mau15>();
			while (reader.Read())
			{
				t_GC_QuyetToan_Mau15 entity = new t_GC_QuyetToan_Mau15();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTaiKhoan"))) entity.SoTaiKhoan = reader.GetInt32(reader.GetOrdinal("SoTaiKhoan"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt32(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiVnacc"))) entity.SoToKhaiVnacc = reader.GetDecimal(reader.GetOrdinal("SoToKhaiVnacc"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaDung"))) entity.SoLuongDaDung = reader.GetDecimal(reader.GetOrdinal("SoLuongDaDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCungUng"))) entity.SoLuongCungUng = reader.GetDecimal(reader.GetOrdinal("SoLuongCungUng"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTon"))) entity.LuongTon = reader.GetDecimal(reader.GetOrdinal("LuongTon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongSD_TK"))) entity.LuongSD_TK = reader.GetDecimal(reader.GetOrdinal("LuongSD_TK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTon_TK"))) entity.LuongTon_TK = reader.GetDecimal(reader.GetOrdinal("LuongTon_TK"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaSuDung"))) entity.TriGiaSuDung = reader.GetDecimal(reader.GetOrdinal("TriGiaSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TonTriGia"))) entity.TonTriGia = reader.GetDecimal(reader.GetOrdinal("TonTriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTT"))) entity.TyGiaTT = reader.GetDecimal(reader.GetOrdinal("TyGiaTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVT"))) entity.MaDVT = reader.GetString(reader.GetOrdinal("MaDVT"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<t_GC_QuyetToan_Mau15> collection, long id)
        {
            foreach (t_GC_QuyetToan_Mau15 item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_GC_QuyetToan_Mau15 VALUES(@SoTaiKhoan, @HopDong_ID, @SoHopDong, @SoToKhai, @SoToKhaiVnacc, @NgayDangKy, @MaLoaiHinh, @Ma, @Ten, @MaHS, @DVT_ID, @SoLuongDangKy, @SoLuongDaNhap, @SoLuongDaDung, @SoLuongCungUng, @LuongTon, @SoLuong, @LuongSD_TK, @LuongTon_TK, @DonGiaKB, @DonGiaTT, @TriGiaTT, @TriGiaSuDung, @TonTriGia, @TyGiaTT, @MaDVT)";
            string update = "UPDATE t_GC_QuyetToan_Mau15 SET SoTaiKhoan = @SoTaiKhoan, HopDong_ID = @HopDong_ID, SoHopDong = @SoHopDong, SoToKhai = @SoToKhai, SoToKhaiVnacc = @SoToKhaiVnacc, NgayDangKy = @NgayDangKy, MaLoaiHinh = @MaLoaiHinh, Ma = @Ma, Ten = @Ten, MaHS = @MaHS, DVT_ID = @DVT_ID, SoLuongDangKy = @SoLuongDangKy, SoLuongDaNhap = @SoLuongDaNhap, SoLuongDaDung = @SoLuongDaDung, SoLuongCungUng = @SoLuongCungUng, LuongTon = @LuongTon, SoLuong = @SoLuong, LuongSD_TK = @LuongSD_TK, LuongTon_TK = @LuongTon_TK, DonGiaKB = @DonGiaKB, DonGiaTT = @DonGiaTT, TriGiaTT = @TriGiaTT, TriGiaSuDung = @TriGiaSuDung, TonTriGia = @TonTriGia, TyGiaTT = @TyGiaTT, MaDVT = @MaDVT WHERE ID = @ID";
            string delete = "DELETE FROM t_GC_QuyetToan_Mau15 WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTaiKhoan", SqlDbType.Int, "SoTaiKhoan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.Int, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.BigInt, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiVnacc", SqlDbType.Decimal, "SoToKhaiVnacc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongDangKy", SqlDbType.Decimal, "SoLuongDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongDaNhap", SqlDbType.Decimal, "SoLuongDaNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongDaDung", SqlDbType.Decimal, "SoLuongDaDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongCungUng", SqlDbType.Decimal, "SoLuongCungUng", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTon", SqlDbType.Decimal, "LuongTon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSD_TK", SqlDbType.Decimal, "LuongSD_TK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTon_TK", SqlDbType.Decimal, "LuongTon_TK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaKB", SqlDbType.Decimal, "DonGiaKB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTT", SqlDbType.Decimal, "TriGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaSuDung", SqlDbType.Decimal, "TriGiaSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TonTriGia", SqlDbType.Decimal, "TonTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyGiaTT", SqlDbType.Decimal, "TyGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT", SqlDbType.VarChar, "MaDVT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTaiKhoan", SqlDbType.Int, "SoTaiKhoan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.Int, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.BigInt, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiVnacc", SqlDbType.Decimal, "SoToKhaiVnacc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongDangKy", SqlDbType.Decimal, "SoLuongDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongDaNhap", SqlDbType.Decimal, "SoLuongDaNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongDaDung", SqlDbType.Decimal, "SoLuongDaDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongCungUng", SqlDbType.Decimal, "SoLuongCungUng", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTon", SqlDbType.Decimal, "LuongTon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSD_TK", SqlDbType.Decimal, "LuongSD_TK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTon_TK", SqlDbType.Decimal, "LuongTon_TK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaKB", SqlDbType.Decimal, "DonGiaKB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTT", SqlDbType.Decimal, "TriGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaSuDung", SqlDbType.Decimal, "TriGiaSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TonTriGia", SqlDbType.Decimal, "TonTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyGiaTT", SqlDbType.Decimal, "TyGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT", SqlDbType.VarChar, "MaDVT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_GC_QuyetToan_Mau15 VALUES(@SoTaiKhoan, @HopDong_ID, @SoHopDong, @SoToKhai, @SoToKhaiVnacc, @NgayDangKy, @MaLoaiHinh, @Ma, @Ten, @MaHS, @DVT_ID, @SoLuongDangKy, @SoLuongDaNhap, @SoLuongDaDung, @SoLuongCungUng, @LuongTon, @SoLuong, @LuongSD_TK, @LuongTon_TK, @DonGiaKB, @DonGiaTT, @TriGiaTT, @TriGiaSuDung, @TonTriGia, @TyGiaTT, @MaDVT)";
            string update = "UPDATE t_GC_QuyetToan_Mau15 SET SoTaiKhoan = @SoTaiKhoan, HopDong_ID = @HopDong_ID, SoHopDong = @SoHopDong, SoToKhai = @SoToKhai, SoToKhaiVnacc = @SoToKhaiVnacc, NgayDangKy = @NgayDangKy, MaLoaiHinh = @MaLoaiHinh, Ma = @Ma, Ten = @Ten, MaHS = @MaHS, DVT_ID = @DVT_ID, SoLuongDangKy = @SoLuongDangKy, SoLuongDaNhap = @SoLuongDaNhap, SoLuongDaDung = @SoLuongDaDung, SoLuongCungUng = @SoLuongCungUng, LuongTon = @LuongTon, SoLuong = @SoLuong, LuongSD_TK = @LuongSD_TK, LuongTon_TK = @LuongTon_TK, DonGiaKB = @DonGiaKB, DonGiaTT = @DonGiaTT, TriGiaTT = @TriGiaTT, TriGiaSuDung = @TriGiaSuDung, TonTriGia = @TonTriGia, TyGiaTT = @TyGiaTT, MaDVT = @MaDVT WHERE ID = @ID";
            string delete = "DELETE FROM t_GC_QuyetToan_Mau15 WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTaiKhoan", SqlDbType.Int, "SoTaiKhoan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.Int, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.BigInt, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhaiVnacc", SqlDbType.Decimal, "SoToKhaiVnacc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongDangKy", SqlDbType.Decimal, "SoLuongDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongDaNhap", SqlDbType.Decimal, "SoLuongDaNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongDaDung", SqlDbType.Decimal, "SoLuongDaDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongCungUng", SqlDbType.Decimal, "SoLuongCungUng", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTon", SqlDbType.Decimal, "LuongTon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSD_TK", SqlDbType.Decimal, "LuongSD_TK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTon_TK", SqlDbType.Decimal, "LuongTon_TK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaKB", SqlDbType.Decimal, "DonGiaKB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTT", SqlDbType.Decimal, "TriGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaSuDung", SqlDbType.Decimal, "TriGiaSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TonTriGia", SqlDbType.Decimal, "TonTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyGiaTT", SqlDbType.Decimal, "TyGiaTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT", SqlDbType.VarChar, "MaDVT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTaiKhoan", SqlDbType.Int, "SoTaiKhoan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.Int, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.BigInt, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhaiVnacc", SqlDbType.Decimal, "SoToKhaiVnacc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ma", SqlDbType.VarChar, "Ma", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongDangKy", SqlDbType.Decimal, "SoLuongDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongDaNhap", SqlDbType.Decimal, "SoLuongDaNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongDaDung", SqlDbType.Decimal, "SoLuongDaDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongCungUng", SqlDbType.Decimal, "SoLuongCungUng", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTon", SqlDbType.Decimal, "LuongTon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSD_TK", SqlDbType.Decimal, "LuongSD_TK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTon_TK", SqlDbType.Decimal, "LuongTon_TK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaKB", SqlDbType.Decimal, "DonGiaKB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonGiaTT", SqlDbType.Decimal, "DonGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTT", SqlDbType.Decimal, "TriGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaSuDung", SqlDbType.Decimal, "TriGiaSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TonTriGia", SqlDbType.Decimal, "TonTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyGiaTT", SqlDbType.Decimal, "TyGiaTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT", SqlDbType.VarChar, "MaDVT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static t_GC_QuyetToan_Mau15 Load(long id)
		{
			const string spName = "[dbo].[p_t_GC_QuyetToan_Mau15_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<t_GC_QuyetToan_Mau15> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_t_GC_QuyetToan_Mau15_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_t_GC_QuyetToan_Mau15_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_t_GC_QuyetToan_Mau15_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_t_GC_QuyetToan_Mau15_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long Insertt_GC_QuyetToan_Mau15(int soTaiKhoan, int hopDong_ID, string soHopDong, long soToKhai, decimal soToKhaiVnacc, DateTime ngayDangKy, string maLoaiHinh, string ma, string ten, string maHS, string dVT_ID, decimal soLuongDangKy, decimal soLuongDaNhap, decimal soLuongDaDung, decimal soLuongCungUng, decimal luongTon, decimal soLuong, decimal luongSD_TK, decimal luongTon_TK, decimal donGiaKB, decimal donGiaTT, decimal triGiaTT, decimal triGiaSuDung, decimal tonTriGia, decimal tyGiaTT, string maDVT)
		{
			t_GC_QuyetToan_Mau15 entity = new t_GC_QuyetToan_Mau15();	
			entity.SoTaiKhoan = soTaiKhoan;
			entity.HopDong_ID = hopDong_ID;
			entity.SoHopDong = soHopDong;
			entity.SoToKhai = soToKhai;
			entity.SoToKhaiVnacc = soToKhaiVnacc;
			entity.NgayDangKy = ngayDangKy;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.Ma = ma;
			entity.Ten = ten;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.SoLuongDangKy = soLuongDangKy;
			entity.SoLuongDaNhap = soLuongDaNhap;
			entity.SoLuongDaDung = soLuongDaDung;
			entity.SoLuongCungUng = soLuongCungUng;
			entity.LuongTon = luongTon;
			entity.SoLuong = soLuong;
			entity.LuongSD_TK = luongSD_TK;
			entity.LuongTon_TK = luongTon_TK;
			entity.DonGiaKB = donGiaKB;
			entity.DonGiaTT = donGiaTT;
			entity.TriGiaTT = triGiaTT;
			entity.TriGiaSuDung = triGiaSuDung;
			entity.TonTriGia = tonTriGia;
			entity.TyGiaTT = tyGiaTT;
			entity.MaDVT = maDVT;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_t_GC_QuyetToan_Mau15_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoTaiKhoan", SqlDbType.Int, SoTaiKhoan);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.Int, HopDong_ID);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
			db.AddInParameter(dbCommand, "@SoToKhaiVnacc", SqlDbType.Decimal, SoToKhaiVnacc);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, SoLuongDangKy);
			db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, SoLuongDaNhap);
			db.AddInParameter(dbCommand, "@SoLuongDaDung", SqlDbType.Decimal, SoLuongDaDung);
			db.AddInParameter(dbCommand, "@SoLuongCungUng", SqlDbType.Decimal, SoLuongCungUng);
			db.AddInParameter(dbCommand, "@LuongTon", SqlDbType.Decimal, LuongTon);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@LuongSD_TK", SqlDbType.Decimal, LuongSD_TK);
			db.AddInParameter(dbCommand, "@LuongTon_TK", SqlDbType.Decimal, LuongTon_TK);
			db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, DonGiaKB);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaSuDung", SqlDbType.Decimal, TriGiaSuDung);
			db.AddInParameter(dbCommand, "@TonTriGia", SqlDbType.Decimal, TonTriGia);
			db.AddInParameter(dbCommand, "@TyGiaTT", SqlDbType.Decimal, TyGiaTT);
			db.AddInParameter(dbCommand, "@MaDVT", SqlDbType.VarChar, MaDVT);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdatet_GC_QuyetToan_Mau15(long id, int soTaiKhoan, int hopDong_ID, string soHopDong, long soToKhai, decimal soToKhaiVnacc, DateTime ngayDangKy, string maLoaiHinh, string ma, string ten, string maHS, string dVT_ID, decimal soLuongDangKy, decimal soLuongDaNhap, decimal soLuongDaDung, decimal soLuongCungUng, decimal luongTon, decimal soLuong, decimal luongSD_TK, decimal luongTon_TK, decimal donGiaKB, decimal donGiaTT, decimal triGiaTT, decimal triGiaSuDung, decimal tonTriGia, decimal tyGiaTT, string maDVT)
		{
			t_GC_QuyetToan_Mau15 entity = new t_GC_QuyetToan_Mau15();			
			entity.ID = id;
			entity.SoTaiKhoan = soTaiKhoan;
			entity.HopDong_ID = hopDong_ID;
			entity.SoHopDong = soHopDong;
			entity.SoToKhai = soToKhai;
			entity.SoToKhaiVnacc = soToKhaiVnacc;
			entity.NgayDangKy = ngayDangKy;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.Ma = ma;
			entity.Ten = ten;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.SoLuongDangKy = soLuongDangKy;
			entity.SoLuongDaNhap = soLuongDaNhap;
			entity.SoLuongDaDung = soLuongDaDung;
			entity.SoLuongCungUng = soLuongCungUng;
			entity.LuongTon = luongTon;
			entity.SoLuong = soLuong;
			entity.LuongSD_TK = luongSD_TK;
			entity.LuongTon_TK = luongTon_TK;
			entity.DonGiaKB = donGiaKB;
			entity.DonGiaTT = donGiaTT;
			entity.TriGiaTT = triGiaTT;
			entity.TriGiaSuDung = triGiaSuDung;
			entity.TonTriGia = tonTriGia;
			entity.TyGiaTT = tyGiaTT;
			entity.MaDVT = maDVT;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_t_GC_QuyetToan_Mau15_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTaiKhoan", SqlDbType.Int, SoTaiKhoan);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.Int, HopDong_ID);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
			db.AddInParameter(dbCommand, "@SoToKhaiVnacc", SqlDbType.Decimal, SoToKhaiVnacc);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, SoLuongDangKy);
			db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, SoLuongDaNhap);
			db.AddInParameter(dbCommand, "@SoLuongDaDung", SqlDbType.Decimal, SoLuongDaDung);
			db.AddInParameter(dbCommand, "@SoLuongCungUng", SqlDbType.Decimal, SoLuongCungUng);
			db.AddInParameter(dbCommand, "@LuongTon", SqlDbType.Decimal, LuongTon);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@LuongSD_TK", SqlDbType.Decimal, LuongSD_TK);
			db.AddInParameter(dbCommand, "@LuongTon_TK", SqlDbType.Decimal, LuongTon_TK);
			db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, DonGiaKB);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaSuDung", SqlDbType.Decimal, TriGiaSuDung);
			db.AddInParameter(dbCommand, "@TonTriGia", SqlDbType.Decimal, TonTriGia);
			db.AddInParameter(dbCommand, "@TyGiaTT", SqlDbType.Decimal, TyGiaTT);
			db.AddInParameter(dbCommand, "@MaDVT", SqlDbType.VarChar, MaDVT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int Updatet_GC_QuyetToan_Mau15(long id, int soTaiKhoan, int hopDong_ID, string soHopDong, long soToKhai, decimal soToKhaiVnacc, DateTime ngayDangKy, string maLoaiHinh, string ma, string ten, string maHS, string dVT_ID, decimal soLuongDangKy, decimal soLuongDaNhap, decimal soLuongDaDung, decimal soLuongCungUng, decimal luongTon, decimal soLuong, decimal luongSD_TK, decimal luongTon_TK, decimal donGiaKB, decimal donGiaTT, decimal triGiaTT, decimal triGiaSuDung, decimal tonTriGia, decimal tyGiaTT, string maDVT)
		{
			t_GC_QuyetToan_Mau15 entity = new t_GC_QuyetToan_Mau15();			
			entity.ID = id;
			entity.SoTaiKhoan = soTaiKhoan;
			entity.HopDong_ID = hopDong_ID;
			entity.SoHopDong = soHopDong;
			entity.SoToKhai = soToKhai;
			entity.SoToKhaiVnacc = soToKhaiVnacc;
			entity.NgayDangKy = ngayDangKy;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.Ma = ma;
			entity.Ten = ten;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.SoLuongDangKy = soLuongDangKy;
			entity.SoLuongDaNhap = soLuongDaNhap;
			entity.SoLuongDaDung = soLuongDaDung;
			entity.SoLuongCungUng = soLuongCungUng;
			entity.LuongTon = luongTon;
			entity.SoLuong = soLuong;
			entity.LuongSD_TK = luongSD_TK;
			entity.LuongTon_TK = luongTon_TK;
			entity.DonGiaKB = donGiaKB;
			entity.DonGiaTT = donGiaTT;
			entity.TriGiaTT = triGiaTT;
			entity.TriGiaSuDung = triGiaSuDung;
			entity.TonTriGia = tonTriGia;
			entity.TyGiaTT = tyGiaTT;
			entity.MaDVT = maDVT;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_t_GC_QuyetToan_Mau15_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTaiKhoan", SqlDbType.Int, SoTaiKhoan);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.Int, HopDong_ID);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
			db.AddInParameter(dbCommand, "@SoToKhaiVnacc", SqlDbType.Decimal, SoToKhaiVnacc);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, SoLuongDangKy);
			db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, SoLuongDaNhap);
			db.AddInParameter(dbCommand, "@SoLuongDaDung", SqlDbType.Decimal, SoLuongDaDung);
			db.AddInParameter(dbCommand, "@SoLuongCungUng", SqlDbType.Decimal, SoLuongCungUng);
			db.AddInParameter(dbCommand, "@LuongTon", SqlDbType.Decimal, LuongTon);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@LuongSD_TK", SqlDbType.Decimal, LuongSD_TK);
			db.AddInParameter(dbCommand, "@LuongTon_TK", SqlDbType.Decimal, LuongTon_TK);
			db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, DonGiaKB);
			db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
			db.AddInParameter(dbCommand, "@TriGiaSuDung", SqlDbType.Decimal, TriGiaSuDung);
			db.AddInParameter(dbCommand, "@TonTriGia", SqlDbType.Decimal, TonTriGia);
			db.AddInParameter(dbCommand, "@TyGiaTT", SqlDbType.Decimal, TyGiaTT);
			db.AddInParameter(dbCommand, "@MaDVT", SqlDbType.VarChar, MaDVT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int Deletet_GC_QuyetToan_Mau15(long id)
		{
			t_GC_QuyetToan_Mau15 entity = new t_GC_QuyetToan_Mau15();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_t_GC_QuyetToan_Mau15_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_t_GC_QuyetToan_Mau15_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}