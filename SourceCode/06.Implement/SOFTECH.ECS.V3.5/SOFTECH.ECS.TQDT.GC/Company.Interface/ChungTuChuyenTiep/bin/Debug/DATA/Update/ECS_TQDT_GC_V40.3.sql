GO
IF NOT EXISTS (SELECT 1 FROM sys.columns 
          WHERE Name = N'SoTiepNhan'
          AND Object_ID = Object_ID(N't_GC_DinhMuc'))
BEGIN
	ALTER TABLE dbo.t_GC_DinhMuc
	ADD 
		SoTiepNhan NUMERIC(18,0) ,
		NgayTiepNhan DATETIME ,
		TrangThaiXuLy INT ,
		LenhSanXuat_ID BIGINT DEFAULT(0),
		GuidString NVARCHAR(50)
END
GO
UPDATE t_GC_DinhMuc SET LenhSanXuat_ID = 0 WHERE LenhSanXuat_ID IS NULL
GO
ALTER TABLE dbo.t_GC_DinhMuc 
ALTER COLUMN LenhSanXuat_ID BIGINT NOT NULL
GO
ALTER TABLE [dbo].[t_GC_DinhMuc] DROP CONSTRAINT [PK_t_GC_DinhMuc]
GO
ALTER TABLE [dbo].[t_GC_DinhMuc] ADD CONSTRAINT [PK_t_GC_DinhMuc] PRIMARY KEY CLUSTERED ([HopDong_ID], [MaSanPham], [MaNguyenPhuLieu],LenhSanXuat_ID) ON [PRIMARY]
GO
IF OBJECT_ID('t_GC_DinhMuc_Log') IS NOT NULL
DROP TABLE dbo.t_GC_DinhMuc_Log
GO
CREATE TABLE [dbo].[t_GC_DinhMuc_Log]
(
[HopDong_ID] [bigint] NOT NULL,
[MaSanPham] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaNguyenPhuLieu] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DinhMucSuDung] [numeric] (18, 8) NOT NULL,
[TyLeHaoHut] [numeric] (18, 8) NOT NULL,
[STTHang] [int] NOT NULL,
[NPL_TuCungUng] [numeric] (18, 6) NOT NULL,
[TenSanPham] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNPL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
SoTiepNhan NUMERIC(18,0) ,
NgayTiepNhan DATETIME ,
TrangThaiXuLy INT ,
LenhSanXuat_ID BIGINT DEFAULT(0),
GuidString NVARCHAR(50),
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_GC_DinhMuc_Log') IS NOT NULL
DROP TRIGGER trg_t_GC_DinhMuc_Log
GO
CREATE TRIGGER trg_t_GC_DinhMuc_Log
ON dbo.t_GC_DinhMuc
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_GC_DinhMuc_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_GC_DinhMuc_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_GC_DinhMuc_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO
IF OBJECT_ID('t_KDT_GC_DinhMucThucTe_DinhMuc') IS NOT NULL
DROP TABLE t_KDT_GC_DinhMucThucTe_DinhMuc
GO
IF OBJECT_ID('t_KDT_GC_DinhMucThucTe_SP') IS NOT NULL
DROP TABLE t_KDT_GC_DinhMucThucTe_SP
GO
IF OBJECT_ID('t_KDT_GC_DinhMucThucTeDangKy') IS NOT NULL
DROP TABLE t_KDT_GC_DinhMucThucTeDangKy
GO
CREATE TABLE t_KDT_GC_DinhMucThucTeDangKy
(
	ID BIGINT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[HopDong_ID] [bigint] NOT NULL,
	SoTiepNhan NUMERIC(18,0) ,
	NgayTiepNhan DATETIME,
	TrangThaiXuLy INT NOT NULL,
	GuidString NVARCHAR(50) NULL,
	MaDoanhNghiep VARCHAR(20) NOT NULL,
	TenDoanhNghiep NVARCHAR(255) NOT NULL,
	MaHaiQuan VARCHAR(6) NOT NULL,
	LenhSanXuat_ID BIGINT FOREIGN KEY REFERENCES t_KDT_LenhSanXuat(ID),
	GhiChu NVARCHAR(255),
)
CREATE TABLE t_KDT_GC_DinhMucThucTe_SP
(
	ID BIGINT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	DinhMucThucTe_ID BIGINT NOT NULL FOREIGN KEY REFERENCES t_KDT_GC_DinhMucThucTeDangKy(ID),
	[MaSanPham] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TenSanPham] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DVT_ID] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	GhiChu NVARCHAR(255),
	TrangThai INT NOT NULL DEFAULT (0)
)
CREATE TABLE t_KDT_GC_DinhMucThucTe_DinhMuc
(
	ID BIGINT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	DinhMucThucTeSP_ID BIGINT NOT NULL FOREIGN KEY REFERENCES dbo.t_KDT_GC_DinhMucThucTe_SP(ID),
	[STT] [int] NOT NULL,
	[MaSanPham] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TenSanPham] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DVT_SP] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MaNPL] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TenNPL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DVT_NPL] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MaHS] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DinhMucSuDung] [numeric] (18, 8) NOT NULL,
	[TyLeHaoHut] [numeric] (18, 8) DEFAULT(0),
	[NPL_TuCungUng] [numeric] (18, 6) NULL,
	GhiChu NVARCHAR(255),
)
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_Insert]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_Update]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_Delete]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_Load]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_SelectAll]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_SelectBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_SelectBy_HopDong_ID]

IF OBJECT_ID(N'[dbo].[p_GC_DinhMuc_DeleteBy_HopDong_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GC_DinhMuc_DeleteBy_HopDong_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_Insert]
	@HopDong_ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@STTHang int,
	@NPL_TuCungUng numeric(18, 6),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255),
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@GuidString nvarchar(50),
	@LenhSanXuat_ID bigint
AS
INSERT INTO [dbo].[t_GC_DinhMuc]
(
	[HopDong_ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[STTHang],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[LenhSanXuat_ID]
)
VALUES
(
	@HopDong_ID,
	@MaSanPham,
	@MaNguyenPhuLieu,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@STTHang,
	@NPL_TuCungUng,
	@TenSanPham,
	@TenNPL,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThaiXuLy,
	@GuidString,
	@LenhSanXuat_ID
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_Update]
	@HopDong_ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@STTHang int,
	@NPL_TuCungUng numeric(18, 6),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255),
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@GuidString nvarchar(50),
	@LenhSanXuat_ID bigint
AS

UPDATE
	[dbo].[t_GC_DinhMuc]
SET
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[STTHang] = @STTHang,
	[NPL_TuCungUng] = @NPL_TuCungUng,
	[TenSanPham] = @TenSanPham,
	[TenNPL] = @TenNPL,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[GuidString] = @GuidString,
	[LenhSanXuat_ID] = @LenhSanXuat_ID
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_InsertUpdate]
	@HopDong_ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@STTHang int,
	@NPL_TuCungUng numeric(18, 6),
	@TenSanPham nvarchar(255),
	@TenNPL nvarchar(255),
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@GuidString nvarchar(50),
	@LenhSanXuat_ID bigint
AS
IF EXISTS(SELECT [HopDong_ID], [MaSanPham], [MaNguyenPhuLieu] FROM [dbo].[t_GC_DinhMuc] WHERE [HopDong_ID] = @HopDong_ID AND [MaSanPham] = @MaSanPham AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu)
	BEGIN
		UPDATE
			[dbo].[t_GC_DinhMuc] 
		SET
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[STTHang] = @STTHang,
			[NPL_TuCungUng] = @NPL_TuCungUng,
			[TenSanPham] = @TenSanPham,
			[TenNPL] = @TenNPL,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[GuidString] = @GuidString,
			[LenhSanXuat_ID] = @LenhSanXuat_ID
		WHERE
			[HopDong_ID] = @HopDong_ID
			AND [MaSanPham] = @MaSanPham
			AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_GC_DinhMuc]
	(
			[HopDong_ID],
			[MaSanPham],
			[MaNguyenPhuLieu],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[STTHang],
			[NPL_TuCungUng],
			[TenSanPham],
			[TenNPL],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThaiXuLy],
			[GuidString],
			[LenhSanXuat_ID]
	)
	VALUES
	(
			@HopDong_ID,
			@MaSanPham,
			@MaNguyenPhuLieu,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@STTHang,
			@NPL_TuCungUng,
			@TenSanPham,
			@TenNPL,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThaiXuLy,
			@GuidString,
			@LenhSanXuat_ID
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_Delete]
	@HopDong_ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50)
AS

DELETE FROM 
	[dbo].[t_GC_DinhMuc]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_DeleteBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_DeleteBy_HopDong_ID]
	@HopDong_ID bigint
AS

DELETE FROM [dbo].[t_GC_DinhMuc]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_GC_DinhMuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_Load]
	@HopDong_ID bigint,
	@MaSanPham varchar(50),
	@MaNguyenPhuLieu nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[STTHang],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[LenhSanXuat_ID]
FROM
	[dbo].[t_GC_DinhMuc]
WHERE
	[HopDong_ID] = @HopDong_ID
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_SelectBy_HopDong_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_SelectBy_HopDong_ID]
	@HopDong_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[STTHang],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[LenhSanXuat_ID]
FROM
	[dbo].[t_GC_DinhMuc]
WHERE
	[HopDong_ID] = @HopDong_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[HopDong_ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[STTHang],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[LenhSanXuat_ID]
FROM [dbo].[t_GC_DinhMuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_GC_DinhMuc_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_GC_DinhMuc_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[HopDong_ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[STTHang],
	[NPL_TuCungUng],
	[TenSanPham],
	[TenNPL],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[LenhSanXuat_ID]
FROM
	[dbo].[t_GC_DinhMuc]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTeDangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTeDangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTeDangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTeDangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTeDangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTeDangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectBy_LenhSanXuat_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectBy_LenhSanXuat_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTeDangKy_DeleteBy_LenhSanXuat_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_DeleteBy_LenhSanXuat_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTeDangKy_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_Insert]
	@HopDong_ID bigint,
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@GuidString nvarchar(50),
	@MaDoanhNghiep varchar(20),
	@TenDoanhNghiep nvarchar(255),
	@MaHaiQuan varchar(6),
	@LenhSanXuat_ID bigint,
	@GhiChu nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_DinhMucThucTeDangKy]
(
	[HopDong_ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LenhSanXuat_ID],
	[GhiChu]
)
VALUES 
(
	@HopDong_ID,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThaiXuLy,
	@GuidString,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@MaHaiQuan,
	@LenhSanXuat_ID,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTeDangKy_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_Update]
	@ID bigint,
	@HopDong_ID bigint,
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@GuidString nvarchar(50),
	@MaDoanhNghiep varchar(20),
	@TenDoanhNghiep nvarchar(255),
	@MaHaiQuan varchar(6),
	@LenhSanXuat_ID bigint,
	@GhiChu nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_GC_DinhMucThucTeDangKy]
SET
	[HopDong_ID] = @HopDong_ID,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[GuidString] = @GuidString,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaHaiQuan] = @MaHaiQuan,
	[LenhSanXuat_ID] = @LenhSanXuat_ID,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTeDangKy_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_InsertUpdate]
	@ID bigint,
	@HopDong_ID bigint,
	@SoTiepNhan numeric(18, 0),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@GuidString nvarchar(50),
	@MaDoanhNghiep varchar(20),
	@TenDoanhNghiep nvarchar(255),
	@MaHaiQuan varchar(6),
	@LenhSanXuat_ID bigint,
	@GhiChu nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_DinhMucThucTeDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_DinhMucThucTeDangKy] 
		SET
			[HopDong_ID] = @HopDong_ID,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[GuidString] = @GuidString,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaHaiQuan] = @MaHaiQuan,
			[LenhSanXuat_ID] = @LenhSanXuat_ID,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_DinhMucThucTeDangKy]
		(
			[HopDong_ID],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThaiXuLy],
			[GuidString],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[MaHaiQuan],
			[LenhSanXuat_ID],
			[GhiChu]
		)
		VALUES 
		(
			@HopDong_ID,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThaiXuLy,
			@GuidString,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@MaHaiQuan,
			@LenhSanXuat_ID,
			@GhiChu
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTeDangKy_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_DinhMucThucTeDangKy]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTeDangKy_DeleteBy_LenhSanXuat_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_DeleteBy_LenhSanXuat_ID]
	@LenhSanXuat_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_DinhMucThucTeDangKy]
WHERE
	[LenhSanXuat_ID] = @LenhSanXuat_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTeDangKy_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_DinhMucThucTeDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTeDangKy_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LenhSanXuat_ID],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_DinhMucThucTeDangKy]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectBy_LenhSanXuat_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectBy_LenhSanXuat_ID]
	@LenhSanXuat_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LenhSanXuat_ID],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_DinhMucThucTeDangKy]
WHERE
	[LenhSanXuat_ID] = @LenhSanXuat_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HopDong_ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LenhSanXuat_ID],
	[GhiChu]
FROM [dbo].[t_KDT_GC_DinhMucThucTeDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTeDangKy_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HopDong_ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[GuidString],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[MaHaiQuan],
	[LenhSanXuat_ID],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_DinhMucThucTeDangKy]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_SP_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_SP_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_SP_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_SP_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_SP_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_SP_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_SP_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_SP_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_SP_SelectBy_DinhMucThucTe_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_SelectBy_DinhMucThucTe_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_SP_DeleteBy_DinhMucThucTe_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_DeleteBy_DinhMucThucTe_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_SP_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_Insert]
	@DinhMucThucTe_ID bigint,
	@MaSanPham nvarchar(50),
	@TenSanPham nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@GhiChu nvarchar(255),
	@TrangThai int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_DinhMucThucTe_SP]
(
	[DinhMucThucTe_ID],
	[MaSanPham],
	[TenSanPham],
	[MaHS],
	[DVT_ID],
	[GhiChu],
	[TrangThai]
)
VALUES 
(
	@DinhMucThucTe_ID,
	@MaSanPham,
	@TenSanPham,
	@MaHS,
	@DVT_ID,
	@GhiChu,
	@TrangThai
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_SP_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_Update]
	@ID bigint,
	@DinhMucThucTe_ID bigint,
	@MaSanPham nvarchar(50),
	@TenSanPham nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@GhiChu nvarchar(255),
	@TrangThai int
AS

UPDATE
	[dbo].[t_KDT_GC_DinhMucThucTe_SP]
SET
	[DinhMucThucTe_ID] = @DinhMucThucTe_ID,
	[MaSanPham] = @MaSanPham,
	[TenSanPham] = @TenSanPham,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[GhiChu] = @GhiChu,
	[TrangThai] = @TrangThai
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_SP_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_InsertUpdate]
	@ID bigint,
	@DinhMucThucTe_ID bigint,
	@MaSanPham nvarchar(50),
	@TenSanPham nvarchar(255),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@GhiChu nvarchar(255),
	@TrangThai int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_DinhMucThucTe_SP] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_DinhMucThucTe_SP] 
		SET
			[DinhMucThucTe_ID] = @DinhMucThucTe_ID,
			[MaSanPham] = @MaSanPham,
			[TenSanPham] = @TenSanPham,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[GhiChu] = @GhiChu,
			[TrangThai] = @TrangThai
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_DinhMucThucTe_SP]
		(
			[DinhMucThucTe_ID],
			[MaSanPham],
			[TenSanPham],
			[MaHS],
			[DVT_ID],
			[GhiChu],
			[TrangThai]
		)
		VALUES 
		(
			@DinhMucThucTe_ID,
			@MaSanPham,
			@TenSanPham,
			@MaHS,
			@DVT_ID,
			@GhiChu,
			@TrangThai
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_SP_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_DinhMucThucTe_SP]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_SP_DeleteBy_DinhMucThucTe_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_DeleteBy_DinhMucThucTe_ID]
	@DinhMucThucTe_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_DinhMucThucTe_SP]
WHERE
	[DinhMucThucTe_ID] = @DinhMucThucTe_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_SP_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_DinhMucThucTe_SP] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_SP_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[DinhMucThucTe_ID],
	[MaSanPham],
	[TenSanPham],
	[MaHS],
	[DVT_ID],
	[GhiChu],
	[TrangThai]
FROM
	[dbo].[t_KDT_GC_DinhMucThucTe_SP]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_SP_SelectBy_DinhMucThucTe_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_SelectBy_DinhMucThucTe_ID]
	@DinhMucThucTe_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[DinhMucThucTe_ID],
	[MaSanPham],
	[TenSanPham],
	[MaHS],
	[DVT_ID],
	[GhiChu],
	[TrangThai]
FROM
	[dbo].[t_KDT_GC_DinhMucThucTe_SP]
WHERE
	[DinhMucThucTe_ID] = @DinhMucThucTe_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_SP_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[DinhMucThucTe_ID],
	[MaSanPham],
	[TenSanPham],
	[MaHS],
	[DVT_ID],
	[GhiChu],
	[TrangThai]
FROM [dbo].[t_KDT_GC_DinhMucThucTe_SP] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_SP_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_SP_SelectAll]








AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[DinhMucThucTe_ID],
	[MaSanPham],
	[TenSanPham],
	[MaHS],
	[DVT_ID],
	[GhiChu],
	[TrangThai]
FROM
	[dbo].[t_KDT_GC_DinhMucThucTe_SP]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_SelectBy_DinhMucThucTeSP_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_SelectBy_DinhMucThucTeSP_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_DeleteBy_DinhMucThucTeSP_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_DeleteBy_DinhMucThucTeSP_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Insert]
	@DinhMucThucTeSP_ID bigint,
	@STT int,
	@MaSanPham varchar(50),
	@TenSanPham nvarchar(255),
	@DVT_SP char(3),
	@MaNPL nvarchar(50),
	@TenNPL nvarchar(255),
	@DVT_NPL char(3),
	@MaHS varchar(12),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@NPL_TuCungUng numeric(18, 6),
	@GhiChu nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_DinhMucThucTe_DinhMuc]
(
	[DinhMucThucTeSP_ID],
	[STT],
	[MaSanPham],
	[TenSanPham],
	[DVT_SP],
	[MaNPL],
	[TenNPL],
	[DVT_NPL],
	[MaHS],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[NPL_TuCungUng],
	[GhiChu]
)
VALUES 
(
	@DinhMucThucTeSP_ID,
	@STT,
	@MaSanPham,
	@TenSanPham,
	@DVT_SP,
	@MaNPL,
	@TenNPL,
	@DVT_NPL,
	@MaHS,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@NPL_TuCungUng,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Update]
	@ID bigint,
	@DinhMucThucTeSP_ID bigint,
	@STT int,
	@MaSanPham varchar(50),
	@TenSanPham nvarchar(255),
	@DVT_SP char(3),
	@MaNPL nvarchar(50),
	@TenNPL nvarchar(255),
	@DVT_NPL char(3),
	@MaHS varchar(12),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@NPL_TuCungUng numeric(18, 6),
	@GhiChu nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_GC_DinhMucThucTe_DinhMuc]
SET
	[DinhMucThucTeSP_ID] = @DinhMucThucTeSP_ID,
	[STT] = @STT,
	[MaSanPham] = @MaSanPham,
	[TenSanPham] = @TenSanPham,
	[DVT_SP] = @DVT_SP,
	[MaNPL] = @MaNPL,
	[TenNPL] = @TenNPL,
	[DVT_NPL] = @DVT_NPL,
	[MaHS] = @MaHS,
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[NPL_TuCungUng] = @NPL_TuCungUng,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_InsertUpdate]
	@ID bigint,
	@DinhMucThucTeSP_ID bigint,
	@STT int,
	@MaSanPham varchar(50),
	@TenSanPham nvarchar(255),
	@DVT_SP char(3),
	@MaNPL nvarchar(50),
	@TenNPL nvarchar(255),
	@DVT_NPL char(3),
	@MaHS varchar(12),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 8),
	@NPL_TuCungUng numeric(18, 6),
	@GhiChu nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_DinhMucThucTe_DinhMuc] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_DinhMucThucTe_DinhMuc] 
		SET
			[DinhMucThucTeSP_ID] = @DinhMucThucTeSP_ID,
			[STT] = @STT,
			[MaSanPham] = @MaSanPham,
			[TenSanPham] = @TenSanPham,
			[DVT_SP] = @DVT_SP,
			[MaNPL] = @MaNPL,
			[TenNPL] = @TenNPL,
			[DVT_NPL] = @DVT_NPL,
			[MaHS] = @MaHS,
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[NPL_TuCungUng] = @NPL_TuCungUng,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_DinhMucThucTe_DinhMuc]
		(
			[DinhMucThucTeSP_ID],
			[STT],
			[MaSanPham],
			[TenSanPham],
			[DVT_SP],
			[MaNPL],
			[TenNPL],
			[DVT_NPL],
			[MaHS],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[NPL_TuCungUng],
			[GhiChu]
		)
		VALUES 
		(
			@DinhMucThucTeSP_ID,
			@STT,
			@MaSanPham,
			@TenSanPham,
			@DVT_SP,
			@MaNPL,
			@TenNPL,
			@DVT_NPL,
			@MaHS,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@NPL_TuCungUng,
			@GhiChu
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_DinhMucThucTe_DinhMuc]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_DeleteBy_DinhMucThucTeSP_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_DeleteBy_DinhMucThucTeSP_ID]
	@DinhMucThucTeSP_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GC_DinhMucThucTe_DinhMuc]
WHERE
	[DinhMucThucTeSP_ID] = @DinhMucThucTeSP_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_DinhMucThucTe_DinhMuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[DinhMucThucTeSP_ID],
	[STT],
	[MaSanPham],
	[TenSanPham],
	[DVT_SP],
	[MaNPL],
	[TenNPL],
	[DVT_NPL],
	[MaHS],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[NPL_TuCungUng],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_DinhMucThucTe_DinhMuc]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_SelectBy_DinhMucThucTeSP_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_SelectBy_DinhMucThucTeSP_ID]
	@DinhMucThucTeSP_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[DinhMucThucTeSP_ID],
	[STT],
	[MaSanPham],
	[TenSanPham],
	[DVT_SP],
	[MaNPL],
	[TenNPL],
	[DVT_NPL],
	[MaHS],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[NPL_TuCungUng],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_DinhMucThucTe_DinhMuc]
WHERE
	[DinhMucThucTeSP_ID] = @DinhMucThucTeSP_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[DinhMucThucTeSP_ID],
	[STT],
	[MaSanPham],
	[TenSanPham],
	[DVT_SP],
	[MaNPL],
	[TenNPL],
	[DVT_NPL],
	[MaHS],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[NPL_TuCungUng],
	[GhiChu]
FROM [dbo].[t_KDT_GC_DinhMucThucTe_DinhMuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_GC_DinhMucThucTe_DinhMuc_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[DinhMucThucTeSP_ID],
	[STT],
	[MaSanPham],
	[TenSanPham],
	[DVT_SP],
	[MaNPL],
	[TenNPL],
	[DVT_NPL],
	[MaHS],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[NPL_TuCungUng],
	[GhiChu]
FROM
	[dbo].[t_KDT_GC_DinhMucThucTe_DinhMuc]	

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '40.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('40.3',GETDATE(), N'CẬP NHẬT TABLE ĐỊNH MỨC THỰC TẾ')
END
