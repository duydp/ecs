using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.Report.VNACCS
{
    public partial class QuyetDinhThongQuanHangHoaXuatKhau_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public QuyetDinhThongQuanHangHoaXuatKhau_1()
        {
            InitializeComponent();
        }


        public void BindReport(VAE1LF0 vae1lf)
        {
            //lblSoTrang.Text = "1/" + vae1lf.K69.GetValue().ToString();
            int HangHoa = vae1lf.HangMD.Count;
            if (HangHoa <= 2)
            {
                lblSoTrang.Text = "1/" + System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblSoTrang.Text = "1/" + System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 2, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblSotokhai.Text = vae1lf.ECN.GetValue().ToString();
            lblSotokhaidautien.Text = vae1lf.FIC.GetValue().ToString();
            lblSonhanhtokhaichianho.Text = vae1lf.BNO.GetValue().ToString();
            lblTongsotokhaichianho.Text = vae1lf.DNO.GetValue().ToString();
            lblSotokhaitamnhaptaixuat.Text = vae1lf.TDN.GetValue().ToString();
            lblMaphanloaikiemtra.Text = vae1lf.K07.GetValue().ToString();
            lblMaloaihinh.Text = vae1lf.ECB.GetValue().ToString();
            lblMaphanloaihanghoa.Text = vae1lf.CCC.GetValue().ToString();
            lblMahieuphuongthucvanchuyen.Text = vae1lf.MTC.GetValue().ToString();
            lblMasothuedaidien.Text = vae1lf.K01.GetValue().ToString();
            lblTencoquanhaiquantiepnhantokhai.Text = vae1lf.K08.GetValue().ToString();
            lblNhomxulyhoso.Text = vae1lf.CHB.GetValue().ToString();
            if
             (Convert.ToDateTime(vae1lf.K10.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
              {
                  lblNgaydangky.Text = Convert.ToDateTime(vae1lf.K10.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaydangky.Text = "";
            }

            if (vae1lf.AD1.GetValue().ToString() != "" && vae1lf.AD1.GetValue().ToString() != "0")
            {
                lblGiodangky.Text = vae1lf.AD1.GetValue().ToString().Substring(0, 2) + ":" + vae1lf.AD1.GetValue().ToString().Substring(2, 2) + ":" + vae1lf.AD1.GetValue().ToString().Substring(4, 2);
            }
            else
            {
                lblGiodangky.Text = "";
            }
            if
                (Convert.ToDateTime(vae1lf.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaythaydoidangky.Text = Convert.ToDateTime(vae1lf.AD2.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaythaydoidangky.Text = "";
            }
             
            if (vae1lf.AD3.GetValue().ToString() != "" && vae1lf.AD3.GetValue().ToString() != "0")
            {
                lblGiothaydoidangky.Text = vae1lf.AD3.GetValue().ToString().Substring(0, 2) + ":" + vae1lf.AD3.GetValue().ToString().Substring(2, 2) + ":" + vae1lf.AD3.GetValue().ToString().Substring(4, 2);
            }
            else
            {
                lblGiothaydoidangky.Text = "";
            }
            if
                (Convert.ToDateTime(vae1lf.RID.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoihantainhapxuat.Text = Convert.ToDateTime(vae1lf.RID.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoihantainhapxuat.Text = "";
            }
            lblBieuthitruonghophethan.Text = vae1lf.AAA.GetValue().ToString();
            lblMaxuatkhau.Text = vae1lf.EPC.GetValue().ToString();
            lblTenxuatkhau.Text = vae1lf.EPN.GetValue().ToString();
            lblMaBuuChinhNXK.Text = vae1lf.EPP.GetValue().ToString();
            lblDiachinguoixuat.Text = vae1lf.EPA.GetValue().ToString();
            lblSodtnguoixuat.Text = vae1lf.EPT.GetValue().ToString();
            lblManguoiuythacxuatkhau.Text = vae1lf.EXC.GetValue().ToString();
            lblTennguoiuythacxuatkhau.Text = vae1lf.EXN.GetValue().ToString();
            lblManguoinhapkhau.Text = vae1lf.CGC.GetValue().ToString();
            lblTennguoinhapkhau.Text = vae1lf.CGN.GetValue().ToString();
            lblMabuuchinh.Text = vae1lf.CGP.GetValue().ToString();
            lblDiachi1.Text = vae1lf.CGA.GetValue().ToString();
            lblDiachi2.Text = vae1lf.CAT.GetValue().ToString();
            lblDiachi3.Text = vae1lf.CAC.GetValue().ToString();
            lblDiachi4.Text = vae1lf.CAS.GetValue().ToString();
            lblManuoc.Text = vae1lf.CGK.GetValue().ToString();
            lblMadailyhaiquan.Text = vae1lf.K32.GetValue().ToString();
            lblTendailyhaiquan.Text = vae1lf.K33.GetValue().ToString();
            lblManhanvienhaiquan.Text = vae1lf.K34.GetValue().ToString();
            lblSovandon.Text = vae1lf.EKN.GetValue().ToString();
            lblSoluong.Text = vae1lf.NO.GetValue().ToString();
            lblMadonvitinh.Text = vae1lf.NOT.GetValue().ToString();
            lblTongtrongluonghang.Text = vae1lf.GW.GetValue().ToString();
            lblMadonvitinhtrongluong.Text = vae1lf.GWT.GetValue().ToString();
            lblMadiadiemluukho.Text = vae1lf.ST.GetValue().ToString();
            lblTendiadiemluukho.Text = vae1lf.K42.GetValue().ToString();
            lblMadiadiemnhanhangcuoi.Text = vae1lf.DSC.GetValue().ToString();
            lblTendiadiemnhanhangcuoi.Text = vae1lf.DSN.GetValue().ToString();
            lblMadiadiemxephang.Text = vae1lf.PSC.GetValue().ToString();
            lblTendiadiemxephang.Text = vae1lf.PSN.GetValue().ToString();
            lblMaphuongtienvanchuyen.Text = vae1lf.VSC.GetValue().ToString();
            lblTenphuongtienvanchuyen.Text = vae1lf.VSN.GetValue().ToString();
            if
            (Convert.ToDateTime(vae1lf.SYM.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayhangdidukien.Text = Convert.ToDateTime(vae1lf.SYM.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayhangdidukien.Text = "";
            }
            lblKyhieusohieu.Text = vae1lf.MRK.GetValue().ToString();
            for (int i = 0; i < vae1lf.SS_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:

                        lblPhanloaigiayphepxuatkhau1.Text = vae1lf.SS_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSogiayphep1.Text = vae1lf.SS_.listAttribute[1].GetValueCollection(i).ToString();
                       
                        break;
                    case 1:

                        lblPhanloaigiayphepxuatkhau2.Text = vae1lf.SS_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSogiayphep2.Text = vae1lf.SS_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 2:

                        lblPhanloaigiayphepxuatkhau3.Text = vae1lf.SS_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSogiayphep3.Text = vae1lf.SS_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 3:

                        lblPhanloaigiayphepxuatkhau4.Text = vae1lf.SS_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSogiayphep4.Text = vae1lf.SS_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 4:

                        lblPhanloaigiayphepxuatkhau5.Text = vae1lf.SS_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSogiayphep5.Text = vae1lf.SS_.listAttribute[1].GetValueCollection(i).ToString();
                        break;

                }

            }
            lblPhanloaihinhthuchoadon.Text = vae1lf.IV1.GetValue().ToString();
           
            lblSohoadon.Text = vae1lf.IV3.GetValue().ToString();
            lblSotiepnhanhoadondientu.Text = vae1lf.IV2.GetValue().ToString();
            if
                (Convert.ToDateTime(vae1lf.IVD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayphathanh.Text = Convert.ToDateTime(vae1lf.IVD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayphathanh.Text = "";
            }
            lblPhuongthucthanhtoan.Text = vae1lf.IVP.GetValue().ToString();
            lblMadkgiahoadon.Text = vae1lf.IP2.GetValue().ToString();
            lblMadongtienhoadon.Text = vae1lf.IP3.GetValue().ToString();
            lblTongtrigiahoadon.Text = vae1lf.IP4.GetValue().ToString();
            lblMaphanloaigiahoadon.Text = vae1lf.IP1.GetValue().ToString();
            lblMadongtientongthue.Text = vae1lf.FCD.GetValue().ToString();
            lblTongthue.Text = vae1lf.FKK.GetValue().ToString();
            for (int i = 0; i < vae1lf.RC_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMadongtientygiathue1.Text = vae1lf.RC_.listAttribute[0].GetValueCollection(i).ToString();
                        lblTygiatinhthue1.Text = vae1lf.RC_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 1:
                        lblMadongtientygiathue2.Text = vae1lf.RC_.listAttribute[0].GetValueCollection(i).ToString();
                        lblTygiatinhthue2.Text = vae1lf.RC_.listAttribute[1].GetValueCollection(i).ToString();
                        break;


                }

            }

            //minhnd Fix Trị giá Tờ khai
            KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.LoadBySoTK(lblSotokhai.Text);
            decimal triGiaHang = 0;
            TKMD.LoadFull();
            foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
            {
                triGiaHang += hmd.TriGiaHoaDon;
            }
            lblTonghesophanbothue.Text = triGiaHang.ToString("#,#.0000#;(#,#.0000#)");
            //minhnd Fix Trị giá Tờ khai

            //lblTonghesophanbothue.Text = vae1lf.TP.GetValue().ToString();

            lblMaphanloainhaplieutonggia.Text = vae1lf.K68.GetValue().ToString();
            lblPhanloaikhongcanquydoi.Text = vae1lf.CNV.GetValue().ToString();

            for (int i = 0; i < vae1lf.EA_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblPhanloaidinhkemkhaibaodientu1.Text = vae1lf.EA_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSodinhkemkhaibao1.Text = vae1lf.EA_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 1:
                        lblPhanloaidinhkemkhaibaodientu2.Text = vae1lf.EA_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSodinhkemkhaibao2.Text = vae1lf.EA_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 2:
                        lblPhanloaidinhkemkhaibaodientu3.Text = vae1lf.EA_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSodinhkemkhaibao3.Text = vae1lf.EA_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                }

            }

            lblNguoinopthue.Text = vae1lf.TPM.GetValue().ToString();
            lblMaxacdinhthoihannopthue.Text = vae1lf.ENC.GetValue().ToString();
            lblPhanloainopthue.Text = vae1lf.PAY.GetValue().ToString();
            lblTongsotienthuexuatkhau.Text = vae1lf.ETA.GetValue().ToString();
            lblMatientetongtienthuexuat.Text = vae1lf.AD7.GetValue().ToString();
            lblTongsotienlephi.Text = vae1lf.TCO.GetValue().ToString();
            lblTongsotienbaolanh.Text = vae1lf.SAM.GetValue().ToString();
            lblMatientetienbaolanh.Text = vae1lf.AD8.GetValue().ToString();
            lblTongsotokhaicuatrang.Text = vae1lf.K69.GetValue().ToString();
            lblTongsodonghangtokhai.Text = vae1lf.K70.GetValue().ToString();
            lblPhanghichu.Text = vae1lf.NT2.GetValue().ToString();
            lblSoquanlynoibodoanhnghiep.Text = vae1lf.REF.GetValue().ToString();
            lblSoquanlynguoisudung.Text = vae1lf.K82.GetValue().ToString();

            lblTentruongdvHQ.Text = vae1lf.K87.GetValue().ToString();
            if (Convert.ToDateTime(vae1lf.CCD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgayhoanthanhktra.Text = Convert.ToDateTime(vae1lf.CCD.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgayhoanthanhktra.Text = "";
            if (vae1lf.AD4.GetValue().ToString() != "" && vae1lf.AD4.GetValue().ToString().ToUpper() != "0")
                lblGiohoanthanhktra.Text = vae1lf.AD4.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vae1lf.AD4.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vae1lf.AD4.GetValue().ToString().ToUpper().Substring(4, 2);
            else
                lblGiohoanthanhktra.Text = "";
            if (Convert.ToDateTime(vae1lf.K86.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                lblNgaycapphep.Text = Convert.ToDateTime(vae1lf.K86.GetValue()).ToString("dd/MM/yyyy");
            else
                lblNgaycapphep.Text = "";
            if (vae1lf.AD5.GetValue().ToString() != "" && vae1lf.AD5.GetValue().ToString().ToUpper() != "0")
                lblGiocapphep.Text = vae1lf.AD5.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vae1lf.AD5.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vae1lf.AD5.GetValue().ToString().ToUpper().Substring(4, 2);
            else
                lblGiocapphep.Text = "";


            if
                (Convert.ToDateTime(vae1lf.DPD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaykhoihanh.Text = Convert.ToDateTime(vae1lf.DPD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaykhoihanh.Text = "";
            }
            for (int i = 0; i < vae1lf.ST_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblDiadiem.Text = vae1lf.ST_.listAttribute[0].GetValueCollection(i).ToString();
                        if(Convert.ToDateTime(vae1lf.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy")!= "01/01/1900")
                        {
                            lblNgaydukiendentrungchuyen.Text = Convert.ToDateTime(vae1lf.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaydukiendentrungchuyen.Text = "";
                        }

                        if(Convert.ToDateTime(vae1lf.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaykhoihanhvanchuyen.Text = Convert.ToDateTime(vae1lf.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaykhoihanhvanchuyen.Text = "";
                        }
                        break;
                    case 1:
                        lblDiadiem2.Text = vae1lf.ST_.listAttribute[0].GetValueCollection(i).ToString();
                        if(Convert.ToDateTime(vae1lf.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaydukiendentrungchuyen2.Text = Convert.ToDateTime(vae1lf.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaydukiendentrungchuyen2.Text = "";
                        }
                        if(Convert.ToDateTime(vae1lf.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaykhoihanhvanchuyen2.Text = Convert.ToDateTime(vae1lf.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaykhoihanhvanchuyen2.Text = "";
                        }
                        break;
                    case 2:
                        lblDiadiem3.Text = vae1lf.ST_.listAttribute[0].GetValueCollection(i).ToString();
                        if(Convert.ToDateTime(vae1lf.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy")!= "01/01/1900")
                        {
                            lblNgaydukiendentrungchuyen3.Text = Convert.ToDateTime(vae1lf.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaydukiendentrungchuyen3.Text = "";
                        }
                        if(Convert.ToDateTime(vae1lf.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaykhoihanhvanchuyen3.Text = Convert.ToDateTime(vae1lf.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaykhoihanhvanchuyen3.Text = "";
                        }
                        break;


                }

            }

            lblDiadiemdich.Text = vae1lf.ARP.GetValue().ToString();
            if
                (Convert.ToDateTime(vae1lf.ADT.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaydukienden.Text = Convert.ToDateTime(vae1lf.ADT.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaydukienden.Text = "";
            }





        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0")
                lbl.Text = "";
        }


    }
}
