﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAS5050 : BasicVNACC
    {
   
        public List<VAS5050_HANG> HangHoa{ get; set; }

        //public StringBuilder BuilEdiMessagesIVA(StringBuilder StrBuild)
        //{
        //    StringBuilder str = StrBuild;
        //    str = BuildEdiMessages<VAS5050>(StrBuild, true, GlobalVNACC.PathConfig, "VAS5050");
        //    foreach (VAS5050_HANG item in HangHoa)
        //    {
        //        item.BuildEdiMessages<VAS5050_HANG>(StrBuild, true, GlobalVNACC.PathConfig, "VAS5050_HANG");

        //    }
        //    return str;
        //}
        public void LoadVAS5050(string strResult)
        {
            try
            {
                this.GetObject<VAS5050>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAS5050, false, VAS5050.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAS5050.TongSoByte);
                while (true)
                {
                    VAS5050_HANG ivaHang = new VAS5050_HANG();
                    ivaHang.GetObject<VAS5050_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAS5050_HANG", false, VAS5050_HANG.TongSoByte);
                    if (this.HangHoa == null) this.HangHoa = new List<VAS5050_HANG>();
                    this.HangHoa.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAS5050_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAS5050_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAS5050_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
