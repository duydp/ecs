﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;
using Company.GC.BLL.Utils;
using Company.GC.BLL.GC;
using System.Threading;
using System.Collections;
using System.Text;
using Company.GC.BLL.KDT.SXXK;
using System.Data.Common;
using System.Collections.Generic;
using Company.GC.BLL.KDT.GC;
namespace Company.GC.BLL.KDT
{
    public partial class ToKhaiMauDich
    {
        public static DataTable SelectToKhaiChuaPhanBo(long IDHopDong)
        {
            string sql = "SELECT ID,SoTiepNhan,NgayTiepNhan,SoToKhai,NgayDangKy,MaLoaiHinh FROM t_KDT_ToKhaiMauDich WHERE  " +
                         " MaLoaiHinh LIKE 'X%' AND IDHopDong=@IDHopDong AND ID NOT IN " +
                         " ( SELECT DISTINCT ID_TKMD FROM T_GC_PhanBoToKhaiXuat WHERE MaDoanhNghiep LIKE 'XGC%')" +
                         " UNION " +
                         " SELECT ID,SoTiepNhan,NgayTiepNhan,SoToKhai,NgayDangKy,MaLoaiHinh FROM T_KDT_GC_ToKhaiChuyenTiep WHERE IDHopDong=@IDHopDong AND (MaLoaiHinh LIKE 'PHPLX%' OR MaLoaiHinh LIKE 'PHSPX%') AND ID NOT IN " +
                         " ( SELECT DISTINCT ID_TKMD FROM T_GC_PhanBoToKhaiXuat WHERE MaDoanhNghiep LIKE 'PH%')" +
                         "ORDER BY NgayDangKy,SoToKhai,MaLoaiHinh";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);

            return db.ExecuteDataSet(dbCommand).Tables[0];


        }
        public static DataTable SelectToKhaiDaPhanBo(long IDHopDong)
        {
            string sql = "SELECT * FROM v_GC_ToKhaiXuatDaPhanBo where IDHopDong = " + IDHopDong + "ORDER BY STT";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            return db.ExecuteDataSet(dbCommand).Tables[0];

        }

        public static ToKhaiMauDich SelectToKhaiBySoToKhaiAndMaLoaiHinhAndNamDKAndMaHaiQuan(int SoToKhai, string MaLoaiHinh, string MaHaiQuan, short NamDangKy)
        {
            string sql = "select * from t_KDT_ToKhaiMauDich where SoToKhai=@SoToKhai " +
                         " and MaLoaiHinh=@MaLoaiHinh and MaHaiQuan=@MaHaiQuan and year(NgayDangKy)=@NamDangKy " +
                         "order by ngaydangky,mahaiquan,sotokhai,maloaihinh";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, NamDangKy);

            IDataReader reader = db.ExecuteReader(dbCommand);

            ToKhaiMauDich entity = null;
            if (reader.Read())
            {
                entity = new ToKhaiMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) entity.MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay_THN_THX"))) entity.Ngay_THN_THX = reader.GetDateTime(reader.GetOrdinal("Ngay_THN_THX"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiPhanBo"))) entity.TrangThaiPhanBo = reader.GetInt32(reader.GetOrdinal("TrangThaiPhanBo"));
            }
            return entity;

        }
        public void LoadPhanBo()
        {
            PhanBoToKhaiXuatCollection = (List<PhanBoToKhaiXuat>)PhanBoToKhaiXuat.SelectPhanBoToKhaiXuatOfTKMD(this.ID);
        }
        public void LoadPhanBo(SqlTransaction transaction)
        {
            PhanBoToKhaiXuatCollection = (List<PhanBoToKhaiXuat>)PhanBoToKhaiXuat.SelectPhanBoToKhaiXuatOfTKMD(this.ID, transaction);
        }
        public void DeletePhanBoVaFixData(DataTable dtNPLToKhaiNhap)
        {
            this.LoadPhanBo();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    string sql = "select * from  v_GC_ToKhaiXuatDaPhanBo WHERE STT > (SELECT STT FROM v_GC_ToKhaiXuatDaPhanBo WHERE ID = " + this.ID + ") AND IDHopDong = " + this.IDHopDong;
                    SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

                    DataSet ds = db.ExecuteDataSet(dbCommand, transaction);
                    foreach (PhanBoToKhaiXuat item in this.PhanBoToKhaiXuatCollection)
                    {
                        item.DeletePhanBo(transaction);
                    }
                    this.TrangThaiPhanBo = 0;
                    this.UpdateTransaction(transaction);
                    UpdateTrangThaiChuaPhanBo(transaction, ds);
                    FixData(dtNPLToKhaiNhap, transaction);
                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
        public void DeletePhanBo()
        {
            this.LoadPhanBo();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    string sql = "select * from  v_GC_ToKhaiXuatDaPhanBo WHERE STT > (SELECT STT FROM v_GC_ToKhaiXuatDaPhanBo WHERE ID = " + this.ID + ") AND IDHopDong = " + this.IDHopDong;
                    SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

                    DataSet ds = db.ExecuteDataSet(dbCommand, transaction);
                    foreach (PhanBoToKhaiXuat item in this.PhanBoToKhaiXuatCollection)
                    {
                        item.DeletePhanBo(transaction);
                    }
                    this.TrangThaiPhanBo = 0;
                    this.UpdateTransaction(transaction);
                    UpdateTrangThaiChuaPhanBo(transaction, ds);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
        private void FixData(DataTable dtNPLToKhaiNhap, SqlTransaction transaction)
        {
            //using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            //{
            //    connection.Open();
            //    SqlTransaction transaction = connection.BeginTransaction();
            try
            {
                decimal temp = 0;
                foreach (DataRow dr in dtNPLToKhaiNhap.Rows)
                {
                    NPLNhapTonThucTe npl = NPLNhapTonThucTe.Load(SoToKhai = Convert.ToInt32(dr["SoToKhaiNhap"]), dr["MaLoaiHinhNhap"].ToString(), Convert.ToInt16(dr["NamDangKyNhap"]), Convert.ToString(dr["MaHaiQuanNhap"]), Convert.ToString(dr["MaNPL"]), transaction);
                    temp = npl.GetLuongTonDung(transaction);
                    if (temp == 0)
                        npl.Ton = npl.Luong;
                    else
                        npl.Ton = temp;
                    npl.Update(transaction);
                }

            }
            catch (Exception ex)
            {
                //transaction.Rollback();
                //throw ex;
            }
            finally
            {
                //connection.Close();
            }
            //}
        }

        public void UpdateTrangThaiChuaPhanBo(SqlTransaction transaction, DataSet ds)
        {


            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string maLoaiHinh = row["MaLoaiHinh"].ToString();
                int id = Convert.ToInt32(row["ID"]);
                if (maLoaiHinh.Contains("XGC"))
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.ID = id;
                    TKMD.Load(transaction);
                    bool ok = true;

                    if (ok)
                    {

                        TKMD.LoadPhanBo(transaction);
                        TKMD.DeletePhanBo(transaction);
                        TKMD.TrangThaiPhanBo = 0;
                        TKMD.UpdateTransaction(transaction);
                    }
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    TKCT.ID = id;
                    TKCT.Load(transaction);
                    bool ok = true;

                    if (ok)
                    {

                        TKCT.LoadPhanBo(transaction);
                        TKCT.DeletePhanBo(transaction);
                        //TKCT.UpdateTransaction(transaction);
                    }
                }
            }
        }
        public void DeletePhanBo(SqlTransaction transaction)
        {
            if (this.MaLoaiHinh.StartsWith("XGC"))
            {
                foreach (PhanBoToKhaiXuat item in PhanBoToKhaiXuatCollection)
                {
                    item.DeletePhanBo(transaction);
                }
            }
        }
        public decimal SelectDonGia(string maNPL)
        {
            string sql = "SELECT DonGiaKB FROM v_GC_HangToKhai WHERE MaPhu = '" + maNPL + "' AND SoToKhai = " + this.SoToKhai +
                         " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' AND NgayDangKy = @NgayDangKy";
            DbCommand command = db.GetSqlStringCommand(sql);
            db.AddInParameter(command, "@NgayDangKy", DbType.DateTime, this.NgayDangKy);
            return Convert.ToDecimal(db.ExecuteScalar(command));
        }
        public decimal SelectTriGia(string maNPL)
        {
            string sql = "SELECT TriGiaKB FROM v_GC_HangToKhai WHERE MaPhu = '" + maNPL + "' AND SoToKhai = " + this.SoToKhai +
                         " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' AND NgayDangKy = @NgayDangKy";
            DbCommand command = db.GetSqlStringCommand(sql);
            db.AddInParameter(command, "@NgayDangKy", DbType.DateTime, this.NgayDangKy);
            return Convert.ToDecimal(db.ExecuteScalar(command));
        }


        public void TransferToNPLTon()
        {
            if (this.HMDCollection.Count == 0)
                this.LoadHMDCollection();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.UpdateTransaction(transaction);
                    if (this.MaLoaiHinh.StartsWith("NGC") && this.LoaiHangHoa == "N")
                    {
                        foreach (HangMauDich hmd in this.HMDCollection)
                        {

                            //CAP NHAT VAO LUONG TON THUC TE
                            NPLNhapTonThucTe nplNhapTonThucTe = new NPLNhapTonThucTe();
                            nplNhapTonThucTe.SoToKhai = this.SoToKhai;
                            nplNhapTonThucTe.MaLoaiHinh = this.MaLoaiHinh;
                            nplNhapTonThucTe.NamDangKy = (short)this.NgayDangKy.Year;
                            nplNhapTonThucTe.MaHaiQuan = this.MaHaiQuan;
                            nplNhapTonThucTe.MaNPL = hmd.MaPhu;
                            nplNhapTonThucTe.MaDoanhNghiep = this.MaDoanhNghiep;
                            nplNhapTonThucTe.Luong = hmd.SoLuong;
                            nplNhapTonThucTe.Ton = hmd.SoLuong;
                            nplNhapTonThucTe.NgayDangKy = this.NgayDangKy;
                            nplNhapTonThucTe.ID_HopDong = this.IDHopDong;
                            nplNhapTonThucTe.Insert(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public void ChuyenHopDong(long idHopDong)
        {
            string sql = "UPDATE t_KDT_ToKhaiMauDich SET IDHopDong = " + idHopDong + " WHERE SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' AND NgayDangKy = @NgayDangKy";
            DbCommand command = db.GetSqlStringCommand(sql);
            db.AddInParameter(command, "@NgayDangKy", DbType.DateTime, this.NgayDangKy);
            db.ExecuteNonQuery(command);
        }
        public DataTable GetDanhSachToKhaiNhapPhanBo()
        {
            string sql = "SELECT DISTINCT SoToKhaiNhap, MaLoaiHinhNhap, NamDangKyNhap, MaNPL, MaHaiQuanNhap FROM v_GC_PhanBo WHERE ID_TKMD = " + this.ID;
            DbCommand command = db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(command).Tables[0];
        }

        public int DeleteBy_IDHopDong(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_ToKhaiMauDich_DeleteBy_IDHopDong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, this._IDHopDong);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }
    }
}