﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_TEA_NguoiXNK : BaseForm
    {
        public KDT_VNACCS_TEA TEA = new KDT_VNACCS_TEA();
        private DataTable dt = new DataTable();
        public VNACC_TEA_NguoiXNK()
        {
            InitializeComponent();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                int sott = 1;
                TEA.NguoiXNKCollection.Clear();
                if (dt.Rows.Count == 0) return;
                foreach (DataRow i in dt.Rows)
                {
                    KDT_VNACCS_TEANguoiXNK NguoiXNK = new KDT_VNACCS_TEANguoiXNK();
                    if (i["ID"].ToString().Length != 0)
                    {
                        NguoiXNK.ID = Convert.ToInt32(i["ID"].ToString());
                        NguoiXNK.Master_ID = Convert.ToInt32(i["Master_ID"].ToString());
                    }
                    NguoiXNK.MaNguoiXNK = i["MaNguoiXNK"].ToString();
                    NguoiXNK.TenNguoiXNK = i["TenNguoiXNK"].ToString();
                    TEA.NguoiXNKCollection.Add(NguoiXNK);
                    sott++;
                }
                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void VNACC_TEA_NguoiXNK_Load(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(TEA.NguoiXNKCollection);
                grList.DataSource = dt;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_VNACCS_TEANguoiXNK> ItemColl = new List<KDT_VNACCS_TEANguoiXNK>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ItemColl.Add((KDT_VNACCS_TEANguoiXNK)i.GetRow().DataRow);
                    }

                }
                foreach (KDT_VNACCS_TEANguoiXNK item in ItemColl)
                {
                    if (item.ID > 0)
                    {
                        if (ShowMessage("Bạn có muốn xóa người XNK " + item.MaNguoiXNK + " này không?", true) == "Yes")
                        {
                            item.Delete();
                            TEA.NguoiXNKCollection.Remove(item);
                        }
                    }
                }
                grList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
    }
}
