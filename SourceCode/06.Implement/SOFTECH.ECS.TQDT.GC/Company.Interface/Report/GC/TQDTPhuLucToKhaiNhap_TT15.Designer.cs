﻿namespace Company.Interface.Report
{
    partial class TQDTPhuLucToKhaiNhap_TT15
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTableHang3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHSHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXuatXuHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCheDoUuDaiHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVTHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGNTHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter1 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTableThue3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTNKHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatNKHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueNKHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTTTDBHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatTTDBHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueTTDBHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTBVMTHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatBVMTHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueBVMTHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTVATHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatVATHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueVATHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongThueHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTableHang2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHSHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXuatXuHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCheDoUuDaiHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVTHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGNTHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader2 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter2 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTableThue2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTNKHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatNKHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueNKHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTTTDBHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatTTDBHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueTTDBHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTBVMTHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatBVMTHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueBVMTHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTVATHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatVATHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueVATHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongThueHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport4 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTableCont = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongKienCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongCont1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongKienCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongCont2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongKienCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongCont3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTCont4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHieuCont4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongKienCont4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuongCont4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongContainer = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader4 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport3 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTableHang1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHSHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXuatXuHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCheDoUuDaiHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLuongHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGNTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader3 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter3 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTableThue1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTNKHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatNKHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueNKHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTTTDBHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatTTDBHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueTTDBHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTBVMTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatBVMTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueBVMTHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTGTTVATHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatVATHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueVATHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongThueHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChiCucHQDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoPhuLucToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChiCucHQCuaKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblLoaiHinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableCont)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Height = 0;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportFooter1,
            this.ReportHeader1});
            this.DetailReport1.Level = 2;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableHang3});
            this.Detail1.Height = 25;
            this.Detail1.Name = "Detail1";
            // 
            // xrTableHang3
            // 
            this.xrTableHang3.Location = new System.Drawing.Point(17, 0);
            this.xrTableHang3.Name = "xrTableHang3";
            this.xrTableHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableHang3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTableHang3.Size = new System.Drawing.Size(758, 25);
            this.xrTableHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTHang3,
            this.lblMoTaHang3,
            this.lblMaHSHang3,
            this.lblXuatXuHang3,
            this.lblCheDoUuDaiHang3,
            this.lblLuongHang3,
            this.lblDVTHang3,
            this.lblDonGiaHang3,
            this.lblTGNTHang3});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow4.Weight = 0.75757575757575757;
            // 
            // lblSTTHang3
            // 
            this.lblSTTHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTTHang3.Name = "lblSTTHang3";
            this.lblSTTHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTTHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTHang3.Weight = 0.052770448548812666;
            // 
            // lblMoTaHang3
            // 
            this.lblMoTaHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMoTaHang3.Multiline = true;
            this.lblMoTaHang3.Name = "lblMoTaHang3";
            this.lblMoTaHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMoTaHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMoTaHang3.Weight = 0.22163588390501327;
            // 
            // lblMaHSHang3
            // 
            this.lblMaHSHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMaHSHang3.Name = "lblMaHSHang3";
            this.lblMaHSHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHSHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaHSHang3.Weight = 0.13192612137203172;
            // 
            // lblXuatXuHang3
            // 
            this.lblXuatXuHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblXuatXuHang3.Multiline = true;
            this.lblXuatXuHang3.Name = "lblXuatXuHang3";
            this.lblXuatXuHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblXuatXuHang3.StylePriority.UseTextAlignment = false;
            this.lblXuatXuHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblXuatXuHang3.Weight = 0.088390501319261225;
            // 
            // lblCheDoUuDaiHang3
            // 
            this.lblCheDoUuDaiHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCheDoUuDaiHang3.Name = "lblCheDoUuDaiHang3";
            this.lblCheDoUuDaiHang3.StylePriority.UseBorders = false;
            this.lblCheDoUuDaiHang3.Weight = 0.086411609498680764;
            // 
            // lblLuongHang3
            // 
            this.lblLuongHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongHang3.Multiline = true;
            this.lblLuongHang3.Name = "lblLuongHang3";
            this.lblLuongHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongHang3.StylePriority.UseBorders = false;
            this.lblLuongHang3.StylePriority.UseTextAlignment = false;
            this.lblLuongHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuongHang3.Weight = 0.10883905013192619;
            // 
            // lblDVTHang3
            // 
            this.lblDVTHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDVTHang3.Name = "lblDVTHang3";
            this.lblDVTHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDVTHang3.StylePriority.UseBorders = false;
            this.lblDVTHang3.StylePriority.UseTextAlignment = false;
            this.lblDVTHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDVTHang3.Weight = 0.077836411609498668;
            // 
            // lblDonGiaHang3
            // 
            this.lblDonGiaHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDonGiaHang3.Name = "lblDonGiaHang3";
            this.lblDonGiaHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonGiaHang3.StylePriority.UseBorders = false;
            this.lblDonGiaHang3.StylePriority.UsePadding = false;
            this.lblDonGiaHang3.Weight = 0.10949868073878634;
            // 
            // lblTGNTHang3
            // 
            this.lblTGNTHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTGNTHang3.Name = "lblTGNTHang3";
            this.lblTGNTHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGNTHang3.StylePriority.UsePadding = false;
            this.lblTGNTHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGNTHang3.Weight = 0.1226912928759895;
            // 
            // ReportFooter1
            // 
            this.ReportFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableThue3});
            this.ReportFooter1.Height = 154;
            this.ReportFooter1.Name = "ReportFooter1";
            // 
            // xrTableThue3
            // 
            this.xrTableThue3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableThue3.Location = new System.Drawing.Point(17, 0);
            this.xrTableThue3.Name = "xrTableThue3";
            this.xrTableThue3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableThue3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18});
            this.xrTableThue3.Size = new System.Drawing.Size(758, 154);
            this.xrTableThue3.StylePriority.UseBorders = false;
            this.xrTableThue3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36,
            this.xrTableCell37});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow5.StylePriority.UseTextAlignment = false;
            this.xrTableRow5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow5.Weight = 0.51489361702127678;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Multiline = true;
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell34.StylePriority.UseBorders = false;
            this.xrTableCell34.Text = "Loại thuế";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell34.Weight = 0.2744063324538259;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell35.Multiline = true;
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell35.StylePriority.UseBorders = false;
            this.xrTableCell35.Text = "Trị giá tính thuế/Số lượng chịu thuế";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell35.Weight = 0.30738786279683378;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell36.Multiline = true;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell36.StylePriority.UseBorders = false;
            this.xrTableCell36.Text = "Thuế suất(%)/ Mức thuế";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell36.Weight = 0.1094986807387863;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.Text = "Tiền thuế";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell37.Weight = 0.30870712401055411;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell53,
            this.lblTGTTNKHang3,
            this.lblThueSuatNKHang3,
            this.lblTienThueNKHang3});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.36271570291084732;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Text = "26. Thuế nhập khẩu";
            this.xrTableCell53.Weight = 0.2744063324538259;
            // 
            // lblTGTTNKHang3
            // 
            this.lblTGTTNKHang3.Name = "lblTGTTNKHang3";
            this.lblTGTTNKHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTNKHang3.StylePriority.UsePadding = false;
            this.lblTGTTNKHang3.StylePriority.UseTextAlignment = false;
            this.lblTGTTNKHang3.Text = "0";
            this.lblTGTTNKHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTNKHang3.Weight = 0.30738786279683378;
            // 
            // lblThueSuatNKHang3
            // 
            this.lblThueSuatNKHang3.Name = "lblThueSuatNKHang3";
            this.lblThueSuatNKHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatNKHang3.StylePriority.UsePadding = false;
            this.lblThueSuatNKHang3.StylePriority.UseTextAlignment = false;
            this.lblThueSuatNKHang3.Text = "0";
            this.lblThueSuatNKHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatNKHang3.Weight = 0.1094986807387863;
            // 
            // lblTienThueNKHang3
            // 
            this.lblTienThueNKHang3.Name = "lblTienThueNKHang3";
            this.lblTienThueNKHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueNKHang3.StylePriority.UseBorders = false;
            this.lblTienThueNKHang3.StylePriority.UsePadding = false;
            this.lblTienThueNKHang3.StylePriority.UseTextAlignment = false;
            this.lblTienThueNKHang3.Text = "0";
            this.lblTienThueNKHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueNKHang3.Weight = 0.30870712401055411;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell50,
            this.lblTGTTTTDBHang3,
            this.lblThueSuatTTDBHang3,
            this.lblTienThueTTDBHang3});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.36413718835661135;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Text = "27.Thuế TTĐB";
            this.xrTableCell50.Weight = 0.2744063324538259;
            // 
            // lblTGTTTTDBHang3
            // 
            this.lblTGTTTTDBHang3.Name = "lblTGTTTTDBHang3";
            this.lblTGTTTTDBHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTTTDBHang3.StylePriority.UsePadding = false;
            this.lblTGTTTTDBHang3.StylePriority.UseTextAlignment = false;
            this.lblTGTTTTDBHang3.Text = "0";
            this.lblTGTTTTDBHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTTTDBHang3.Weight = 0.30738786279683378;
            // 
            // lblThueSuatTTDBHang3
            // 
            this.lblThueSuatTTDBHang3.Name = "lblThueSuatTTDBHang3";
            this.lblThueSuatTTDBHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatTTDBHang3.StylePriority.UsePadding = false;
            this.lblThueSuatTTDBHang3.StylePriority.UseTextAlignment = false;
            this.lblThueSuatTTDBHang3.Text = "0";
            this.lblThueSuatTTDBHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatTTDBHang3.Weight = 0.1094986807387863;
            // 
            // lblTienThueTTDBHang3
            // 
            this.lblTienThueTTDBHang3.Name = "lblTienThueTTDBHang3";
            this.lblTienThueTTDBHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueTTDBHang3.StylePriority.UseBorders = false;
            this.lblTienThueTTDBHang3.StylePriority.UsePadding = false;
            this.lblTienThueTTDBHang3.StylePriority.UseTextAlignment = false;
            this.lblTienThueTTDBHang3.Text = "0";
            this.lblTienThueTTDBHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueTTDBHang3.Weight = 0.30870712401055411;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell72,
            this.lblTGTTBVMTHang3,
            this.lblThueSuatBVMTHang3,
            this.lblTienThueBVMTHang3});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.36413718835661169;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Text = "28.Thuế  BVMT";
            this.xrTableCell72.Weight = 0.2744063324538259;
            // 
            // lblTGTTBVMTHang3
            // 
            this.lblTGTTBVMTHang3.Name = "lblTGTTBVMTHang3";
            this.lblTGTTBVMTHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTBVMTHang3.StylePriority.UsePadding = false;
            this.lblTGTTBVMTHang3.StylePriority.UseTextAlignment = false;
            this.lblTGTTBVMTHang3.Text = "0";
            this.lblTGTTBVMTHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTBVMTHang3.Weight = 0.30738786279683378;
            // 
            // lblThueSuatBVMTHang3
            // 
            this.lblThueSuatBVMTHang3.Name = "lblThueSuatBVMTHang3";
            this.lblThueSuatBVMTHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatBVMTHang3.StylePriority.UsePadding = false;
            this.lblThueSuatBVMTHang3.StylePriority.UseTextAlignment = false;
            this.lblThueSuatBVMTHang3.Text = "0";
            this.lblThueSuatBVMTHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatBVMTHang3.Weight = 0.1094986807387863;
            // 
            // lblTienThueBVMTHang3
            // 
            this.lblTienThueBVMTHang3.Name = "lblTienThueBVMTHang3";
            this.lblTienThueBVMTHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueBVMTHang3.StylePriority.UseBorders = false;
            this.lblTienThueBVMTHang3.StylePriority.UsePadding = false;
            this.lblTienThueBVMTHang3.StylePriority.UseTextAlignment = false;
            this.lblTienThueBVMTHang3.Text = "0";
            this.lblTienThueBVMTHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueBVMTHang3.Weight = 0.30870712401055411;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell76,
            this.lblTGTTVATHang3,
            this.lblThueSuatVATHang3,
            this.lblTienThueVATHang3});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.36413718835661141;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Text = "29.Thuế GTGT";
            this.xrTableCell76.Weight = 0.2744063324538259;
            // 
            // lblTGTTVATHang3
            // 
            this.lblTGTTVATHang3.Name = "lblTGTTVATHang3";
            this.lblTGTTVATHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTVATHang3.StylePriority.UsePadding = false;
            this.lblTGTTVATHang3.StylePriority.UseTextAlignment = false;
            this.lblTGTTVATHang3.Text = "0";
            this.lblTGTTVATHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTVATHang3.Weight = 0.30738786279683383;
            // 
            // lblThueSuatVATHang3
            // 
            this.lblThueSuatVATHang3.Name = "lblThueSuatVATHang3";
            this.lblThueSuatVATHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatVATHang3.StylePriority.UsePadding = false;
            this.lblThueSuatVATHang3.StylePriority.UseTextAlignment = false;
            this.lblThueSuatVATHang3.Text = "0";
            this.lblThueSuatVATHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatVATHang3.Weight = 0.1094986807387863;
            // 
            // lblTienThueVATHang3
            // 
            this.lblTienThueVATHang3.Name = "lblTienThueVATHang3";
            this.lblTienThueVATHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueVATHang3.StylePriority.UsePadding = false;
            this.lblTienThueVATHang3.StylePriority.UseTextAlignment = false;
            this.lblTienThueVATHang3.Text = "0";
            this.lblTienThueVATHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueVATHang3.Weight = 0.30870712401055411;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.lblTongThueHang3});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.36500587390680089;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell81.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.StylePriority.UseBorders = false;
            this.xrTableCell81.StylePriority.UseFont = false;
            this.xrTableCell81.StylePriority.UseTextAlignment = false;
            this.xrTableCell81.Text = "Cộng:";
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell81.Weight = 0.691292875989446;
            // 
            // lblTongThueHang3
            // 
            this.lblTongThueHang3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongThueHang3.Name = "lblTongThueHang3";
            this.lblTongThueHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueHang3.StylePriority.UseBorders = false;
            this.lblTongThueHang3.StylePriority.UsePadding = false;
            this.lblTongThueHang3.StylePriority.UseTextAlignment = false;
            this.lblTongThueHang3.Text = "0";
            this.lblTongThueHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongThueHang3.Weight = 0.30870712401055411;
            // 
            // ReportHeader1
            // 
            this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11});
            this.ReportHeader1.Height = 40;
            this.ReportHeader1.Name = "ReportHeader1";
            // 
            // xrTable11
            // 
            this.xrTable11.Location = new System.Drawing.Point(17, 0);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable11.Size = new System.Drawing.Size(758, 40);
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell22,
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow14.StylePriority.UseTextAlignment = false;
            this.xrTableRow14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow14.Weight = 0.51489361702127678;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell61.Text = "Số TT";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell61.Weight = 0.052770448548812666;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell62.Multiline = true;
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell62.Text = "18.Mô tả hàng hóa";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell62.Weight = 0.22163588390501329;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell63.Multiline = true;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell63.Text = "19.Mã số hàng hóa";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell63.Weight = 0.13192612137203166;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseBorders = false;
            this.xrTableCell64.StylePriority.UseTextAlignment = false;
            this.xrTableCell64.Text = "20.Xuất xứ";
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell64.Weight = 0.088390501319261225;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.Text = "21. Chế độ ưu đãi";
            this.xrTableCell22.Weight = 0.0870712401055409;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell65.Multiline = true;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell65.StylePriority.UseTextAlignment = false;
            this.xrTableCell65.Text = "22.Lượng";
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell65.Weight = 0.10949868073878628;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell66.Multiline = true;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell66.Text = "23.Đơn vị tính";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell66.Weight = 0.0778364116094987;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell67.Text = "24.Đơn giá nguyên tệ";
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell67.Weight = 0.10949868073878628;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell68.Text = "25.Trị giá nguyên tệ";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell68.Weight = 0.12137203166226915;
            // 
            // DetailReport2
            // 
            this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.ReportHeader2,
            this.ReportFooter2});
            this.DetailReport2.Level = 1;
            this.DetailReport2.Name = "DetailReport2";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableHang2});
            this.Detail2.Height = 25;
            this.Detail2.Name = "Detail2";
            // 
            // xrTableHang2
            // 
            this.xrTableHang2.Location = new System.Drawing.Point(17, 0);
            this.xrTableHang2.Name = "xrTableHang2";
            this.xrTableHang2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTableHang2.Size = new System.Drawing.Size(758, 25);
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTHang2,
            this.lblMoTaHang2,
            this.lblMaHSHang2,
            this.lblXuatXuHang2,
            this.lblCheDoUuDaiHang2,
            this.lblLuongHang2,
            this.lblDVTHang2,
            this.lblDonGiaHang2,
            this.lblTGNTHang2});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.StylePriority.UseBorders = false;
            this.xrTableRow9.Weight = 1;
            // 
            // lblSTTHang2
            // 
            this.lblSTTHang2.Name = "lblSTTHang2";
            this.lblSTTHang2.StylePriority.UseTextAlignment = false;
            this.lblSTTHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSTTHang2.Weight = 0.16604221635883906;
            // 
            // lblMoTaHang2
            // 
            this.lblMoTaHang2.Name = "lblMoTaHang2";
            this.lblMoTaHang2.StylePriority.UseTextAlignment = false;
            this.lblMoTaHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMoTaHang2.Weight = 0.65849604221635882;
            // 
            // lblMaHSHang2
            // 
            this.lblMaHSHang2.Name = "lblMaHSHang2";
            this.lblMaHSHang2.StylePriority.UseTextAlignment = false;
            this.lblMaHSHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaHSHang2.Weight = 0.3970976253298153;
            // 
            // lblXuatXuHang2
            // 
            this.lblXuatXuHang2.Name = "lblXuatXuHang2";
            this.lblXuatXuHang2.StylePriority.UseTextAlignment = false;
            this.lblXuatXuHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblXuatXuHang2.Weight = 0.26649076517150394;
            // 
            // lblCheDoUuDaiHang2
            // 
            this.lblCheDoUuDaiHang2.Name = "lblCheDoUuDaiHang2";
            this.lblCheDoUuDaiHang2.StylePriority.UseTextAlignment = false;
            this.lblCheDoUuDaiHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblCheDoUuDaiHang2.Weight = 0.26187335092348285;
            // 
            // lblLuongHang2
            // 
            this.lblLuongHang2.Name = "lblLuongHang2";
            this.lblLuongHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongHang2.StylePriority.UsePadding = false;
            this.lblLuongHang2.StylePriority.UseTextAlignment = false;
            this.lblLuongHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuongHang2.Weight = 0.329155672823219;
            // 
            // lblDVTHang2
            // 
            this.lblDVTHang2.Name = "lblDVTHang2";
            this.lblDVTHang2.StylePriority.UseTextAlignment = false;
            this.lblDVTHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDVTHang2.Weight = 0.23021108179419525;
            // 
            // lblDonGiaHang2
            // 
            this.lblDonGiaHang2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDonGiaHang2.Name = "lblDonGiaHang2";
            this.lblDonGiaHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonGiaHang2.StylePriority.UseBorders = false;
            this.lblDonGiaHang2.StylePriority.UsePadding = false;
            this.lblDonGiaHang2.StylePriority.UseTextAlignment = false;
            this.lblDonGiaHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblDonGiaHang2.Weight = 0.33311345646437995;
            // 
            // lblTGNTHang2
            // 
            this.lblTGNTHang2.Name = "lblTGNTHang2";
            this.lblTGNTHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGNTHang2.StylePriority.UsePadding = false;
            this.lblTGNTHang2.StylePriority.UseTextAlignment = false;
            this.lblTGNTHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGNTHang2.Weight = 0.35751978891820579;
            // 
            // ReportHeader2
            // 
            this.ReportHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.ReportHeader2.Height = 40;
            this.ReportHeader2.Name = "ReportHeader2";
            // 
            // xrTable3
            // 
            this.xrTable3.Location = new System.Drawing.Point(17, 0);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.Size = new System.Drawing.Size(758, 40);
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell69});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow3.StylePriority.UseTextAlignment = false;
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow3.Weight = 0.51489361702127678;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.Text = "Số\r\nTT";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.0554089709762533;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.Text = "18.Mô tả hàng hóa";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 0.21899736147757265;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.Multiline = true;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.Text = "19.Mã số hàng hóa";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell6.Weight = 0.13192612137203164;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "20.Xuất xứ";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell7.Weight = 0.088390501319261225;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Multiline = true;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.Text = "21.Chế độ ưu đãi";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.087071240105540876;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.Multiline = true;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.Text = "22.Lượng hàng";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell16.Weight = 0.10949868073878628;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.Text = "23.Đơn vị tính";
            this.xrTableCell17.Weight = 0.077176781002638514;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.Text = "24.Đơn giá nguyên tệ";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell18.Weight = 0.11147757255936674;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell69.StylePriority.UseBorders = false;
            this.xrTableCell69.Text = "21.Trị giá nguyên tệ";
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell69.Weight = 0.12005277044854885;
            // 
            // ReportFooter2
            // 
            this.ReportFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableThue2});
            this.ReportFooter2.Height = 149;
            this.ReportFooter2.Name = "ReportFooter2";
            // 
            // xrTableThue2
            // 
            this.xrTableThue2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableThue2.Location = new System.Drawing.Point(17, 0);
            this.xrTableThue2.Name = "xrTableThue2";
            this.xrTableThue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableThue2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow15,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22});
            this.xrTableThue2.Size = new System.Drawing.Size(758, 149);
            this.xrTableThue2.StylePriority.UseBorders = false;
            this.xrTableThue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell84});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableRow1.StylePriority.UseTextAlignment = false;
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow1.Weight = 0.51489361702127678;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Multiline = true;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.Text = "Loại thuế";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 0.2744063324538259;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Multiline = true;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.Text = "Trị giá tính thuế/Số lượng chịu thuế";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 0.30738786279683378;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell12.Multiline = true;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.Text = "Thuế suất(%)/ Mức thuế";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell12.Weight = 0.10949868073878628;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell84.StylePriority.UseBorders = false;
            this.xrTableCell84.Text = "Tiền thuế";
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell84.Weight = 0.30870712401055411;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.lblTGTTNKHang2,
            this.lblThueSuatNKHang2,
            this.lblTienThueNKHang2});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 0.34755319148936187;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Text = "26. Thuế nhập khẩu";
            this.xrTableCell85.Weight = 0.2744063324538259;
            // 
            // lblTGTTNKHang2
            // 
            this.lblTGTTNKHang2.Name = "lblTGTTNKHang2";
            this.lblTGTTNKHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTNKHang2.StylePriority.UsePadding = false;
            this.lblTGTTNKHang2.StylePriority.UseTextAlignment = false;
            this.lblTGTTNKHang2.Text = "0";
            this.lblTGTTNKHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTNKHang2.Weight = 0.30738786279683378;
            // 
            // lblThueSuatNKHang2
            // 
            this.lblThueSuatNKHang2.Name = "lblThueSuatNKHang2";
            this.lblThueSuatNKHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatNKHang2.StylePriority.UsePadding = false;
            this.lblThueSuatNKHang2.StylePriority.UseTextAlignment = false;
            this.lblThueSuatNKHang2.Text = "0";
            this.lblThueSuatNKHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatNKHang2.Weight = 0.10949868073878628;
            // 
            // lblTienThueNKHang2
            // 
            this.lblTienThueNKHang2.Name = "lblTienThueNKHang2";
            this.lblTienThueNKHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueNKHang2.StylePriority.UseBorders = false;
            this.lblTienThueNKHang2.StylePriority.UsePadding = false;
            this.lblTienThueNKHang2.StylePriority.UseTextAlignment = false;
            this.lblTienThueNKHang2.Text = "0";
            this.lblTienThueNKHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueNKHang2.Weight = 0.30870712401055411;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell89,
            this.lblTGTTTTDBHang2,
            this.lblThueSuatTTDBHang2,
            this.lblTienThueTTDBHang2});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.34894479585968952;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Text = "27.Thuế TTĐB";
            this.xrTableCell89.Weight = 0.2744063324538259;
            // 
            // lblTGTTTTDBHang2
            // 
            this.lblTGTTTTDBHang2.Name = "lblTGTTTTDBHang2";
            this.lblTGTTTTDBHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTTTDBHang2.StylePriority.UsePadding = false;
            this.lblTGTTTTDBHang2.StylePriority.UseTextAlignment = false;
            this.lblTGTTTTDBHang2.Text = "0";
            this.lblTGTTTTDBHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTTTDBHang2.Weight = 0.30738786279683378;
            // 
            // lblThueSuatTTDBHang2
            // 
            this.lblThueSuatTTDBHang2.Name = "lblThueSuatTTDBHang2";
            this.lblThueSuatTTDBHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatTTDBHang2.StylePriority.UsePadding = false;
            this.lblThueSuatTTDBHang2.StylePriority.UseTextAlignment = false;
            this.lblThueSuatTTDBHang2.Text = "0";
            this.lblThueSuatTTDBHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatTTDBHang2.Weight = 0.10949868073878628;
            // 
            // lblTienThueTTDBHang2
            // 
            this.lblTienThueTTDBHang2.Name = "lblTienThueTTDBHang2";
            this.lblTienThueTTDBHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueTTDBHang2.StylePriority.UseBorders = false;
            this.lblTienThueTTDBHang2.StylePriority.UsePadding = false;
            this.lblTienThueTTDBHang2.StylePriority.UseTextAlignment = false;
            this.lblTienThueTTDBHang2.Text = "0";
            this.lblTienThueTTDBHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueTTDBHang2.Weight = 0.30870712401055411;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.lblTGTTBVMTHang2,
            this.lblThueSuatBVMTHang2,
            this.lblTienThueBVMTHang2});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.34894479585968963;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Text = "28.Thuế  BVMT";
            this.xrTableCell93.Weight = 0.2744063324538259;
            // 
            // lblTGTTBVMTHang2
            // 
            this.lblTGTTBVMTHang2.Name = "lblTGTTBVMTHang2";
            this.lblTGTTBVMTHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTBVMTHang2.StylePriority.UsePadding = false;
            this.lblTGTTBVMTHang2.StylePriority.UseTextAlignment = false;
            this.lblTGTTBVMTHang2.Text = "0";
            this.lblTGTTBVMTHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTBVMTHang2.Weight = 0.30738786279683378;
            // 
            // lblThueSuatBVMTHang2
            // 
            this.lblThueSuatBVMTHang2.Name = "lblThueSuatBVMTHang2";
            this.lblThueSuatBVMTHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatBVMTHang2.StylePriority.UsePadding = false;
            this.lblThueSuatBVMTHang2.StylePriority.UseTextAlignment = false;
            this.lblThueSuatBVMTHang2.Text = "0";
            this.lblThueSuatBVMTHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatBVMTHang2.Weight = 0.10949868073878628;
            // 
            // lblTienThueBVMTHang2
            // 
            this.lblTienThueBVMTHang2.Name = "lblTienThueBVMTHang2";
            this.lblTienThueBVMTHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueBVMTHang2.StylePriority.UseBorders = false;
            this.lblTienThueBVMTHang2.StylePriority.UsePadding = false;
            this.lblTienThueBVMTHang2.StylePriority.UseTextAlignment = false;
            this.lblTienThueBVMTHang2.Text = "0";
            this.lblTienThueBVMTHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueBVMTHang2.Weight = 0.30870712401055411;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell97,
            this.lblTGTTVATHang2,
            this.lblThueSuatVATHang2,
            this.lblTienThueVATHang2});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.34894479585968952;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Text = "29.Thuế GTGT";
            this.xrTableCell97.Weight = 0.2744063324538259;
            // 
            // lblTGTTVATHang2
            // 
            this.lblTGTTVATHang2.Name = "lblTGTTVATHang2";
            this.lblTGTTVATHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTVATHang2.StylePriority.UsePadding = false;
            this.lblTGTTVATHang2.StylePriority.UseTextAlignment = false;
            this.lblTGTTVATHang2.Text = "0";
            this.lblTGTTVATHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTVATHang2.Weight = 0.30738786279683378;
            // 
            // lblThueSuatVATHang2
            // 
            this.lblThueSuatVATHang2.Name = "lblThueSuatVATHang2";
            this.lblThueSuatVATHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatVATHang2.StylePriority.UsePadding = false;
            this.lblThueSuatVATHang2.StylePriority.UseTextAlignment = false;
            this.lblThueSuatVATHang2.Text = "0";
            this.lblThueSuatVATHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatVATHang2.Weight = 0.10949868073878628;
            // 
            // lblTienThueVATHang2
            // 
            this.lblTienThueVATHang2.Name = "lblTienThueVATHang2";
            this.lblTienThueVATHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueVATHang2.StylePriority.UsePadding = false;
            this.lblTienThueVATHang2.StylePriority.UseTextAlignment = false;
            this.lblTienThueVATHang2.Text = "0";
            this.lblTienThueVATHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueVATHang2.Weight = 0.30870712401055411;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell103,
            this.lblTongThueHang2});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 0.34984933870040269;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell103.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseBorders = false;
            this.xrTableCell103.StylePriority.UseFont = false;
            this.xrTableCell103.StylePriority.UseTextAlignment = false;
            this.xrTableCell103.Text = "Cộng:";
            this.xrTableCell103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell103.Weight = 0.69129287598944589;
            // 
            // lblTongThueHang2
            // 
            this.lblTongThueHang2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblTongThueHang2.Name = "lblTongThueHang2";
            this.lblTongThueHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueHang2.StylePriority.UseBorders = false;
            this.lblTongThueHang2.StylePriority.UsePadding = false;
            this.lblTongThueHang2.StylePriority.UseTextAlignment = false;
            this.lblTongThueHang2.Text = "0";
            this.lblTongThueHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongThueHang2.Weight = 0.30870712401055411;
            // 
            // DetailReport4
            // 
            this.DetailReport4.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.ReportHeader4});
            this.DetailReport4.Level = 3;
            this.DetailReport4.Name = "DetailReport4";
            // 
            // Detail4
            // 
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableCont});
            this.Detail4.Height = 115;
            this.Detail4.Name = "Detail4";
            // 
            // xrTableCont
            // 
            this.xrTableCont.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCont.Location = new System.Drawing.Point(17, 0);
            this.xrTableCont.Name = "xrTableCont";
            this.xrTableCont.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8,
            this.xrTableRow12,
            this.xrTableRow7,
            this.xrTableRow13,
            this.xrTableRow23});
            this.xrTableCont.Size = new System.Drawing.Size(758, 115);
            this.xrTableCont.StylePriority.UseBorders = false;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTCont1,
            this.lblSoHieuCont1,
            this.lblSoLuongKienCont1,
            this.lblTrongLuongCont1});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1;
            // 
            // lblSTTCont1
            // 
            this.lblSTTCont1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSTTCont1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSTTCont1.Name = "lblSTTCont1";
            this.lblSTTCont1.StylePriority.UseBorders = false;
            this.lblSTTCont1.StylePriority.UseFont = false;
            this.lblSTTCont1.StylePriority.UseTextAlignment = false;
            this.lblSTTCont1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTCont1.Weight = 0.16754617414248019;
            // 
            // lblSoHieuCont1
            // 
            this.lblSoHieuCont1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSoHieuCont1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoHieuCont1.Multiline = true;
            this.lblSoHieuCont1.Name = "lblSoHieuCont1";
            this.lblSoHieuCont1.StylePriority.UseBorders = false;
            this.lblSoHieuCont1.StylePriority.UseFont = false;
            this.lblSoHieuCont1.StylePriority.UseTextAlignment = false;
            this.lblSoHieuCont1.Text = "\r\n";
            this.lblSoHieuCont1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoHieuCont1.Weight = 0.820580474934037;
            // 
            // lblSoLuongKienCont1
            // 
            this.lblSoLuongKienCont1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSoLuongKienCont1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoLuongKienCont1.Name = "lblSoLuongKienCont1";
            this.lblSoLuongKienCont1.StylePriority.UseBorders = false;
            this.lblSoLuongKienCont1.StylePriority.UseFont = false;
            this.lblSoLuongKienCont1.StylePriority.UseTextAlignment = false;
            this.lblSoLuongKienCont1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongKienCont1.Weight = 0.95646437994722922;
            // 
            // lblTrongLuongCont1
            // 
            this.lblTrongLuongCont1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblTrongLuongCont1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTrongLuongCont1.Name = "lblTrongLuongCont1";
            this.lblTrongLuongCont1.StylePriority.UseBorders = false;
            this.lblTrongLuongCont1.StylePriority.UseFont = false;
            this.lblTrongLuongCont1.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongCont1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongCont1.Weight = 1.0554089709762533;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTCont2,
            this.lblSoHieuCont2,
            this.lblSoLuongKienCont2,
            this.lblTrongLuongCont2});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.99999999999999989;
            // 
            // lblSTTCont2
            // 
            this.lblSTTCont2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSTTCont2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSTTCont2.Name = "lblSTTCont2";
            this.lblSTTCont2.StylePriority.UseBorders = false;
            this.lblSTTCont2.StylePriority.UseFont = false;
            this.lblSTTCont2.StylePriority.UseTextAlignment = false;
            this.lblSTTCont2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTCont2.Weight = 0.16754617414248019;
            // 
            // lblSoHieuCont2
            // 
            this.lblSoHieuCont2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSoHieuCont2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoHieuCont2.Name = "lblSoHieuCont2";
            this.lblSoHieuCont2.StylePriority.UseBorders = false;
            this.lblSoHieuCont2.StylePriority.UseFont = false;
            this.lblSoHieuCont2.StylePriority.UseTextAlignment = false;
            this.lblSoHieuCont2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoHieuCont2.Weight = 0.820580474934037;
            // 
            // lblSoLuongKienCont2
            // 
            this.lblSoLuongKienCont2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSoLuongKienCont2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoLuongKienCont2.Name = "lblSoLuongKienCont2";
            this.lblSoLuongKienCont2.StylePriority.UseBorders = false;
            this.lblSoLuongKienCont2.StylePriority.UseFont = false;
            this.lblSoLuongKienCont2.StylePriority.UseTextAlignment = false;
            this.lblSoLuongKienCont2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongKienCont2.Weight = 0.95646437994722922;
            // 
            // lblTrongLuongCont2
            // 
            this.lblTrongLuongCont2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblTrongLuongCont2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTrongLuongCont2.Name = "lblTrongLuongCont2";
            this.lblTrongLuongCont2.StylePriority.UseBorders = false;
            this.lblTrongLuongCont2.StylePriority.UseFont = false;
            this.lblTrongLuongCont2.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongCont2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongCont2.Weight = 1.0554089709762533;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTCont3,
            this.lblSoHieuCont3,
            this.lblSoLuongKienCont3,
            this.lblTrongLuongCont3});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1;
            // 
            // lblSTTCont3
            // 
            this.lblSTTCont3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSTTCont3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSTTCont3.Name = "lblSTTCont3";
            this.lblSTTCont3.StylePriority.UseBorders = false;
            this.lblSTTCont3.StylePriority.UseFont = false;
            this.lblSTTCont3.StylePriority.UseTextAlignment = false;
            this.lblSTTCont3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTCont3.Weight = 0.16754617414248019;
            // 
            // lblSoHieuCont3
            // 
            this.lblSoHieuCont3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSoHieuCont3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoHieuCont3.Name = "lblSoHieuCont3";
            this.lblSoHieuCont3.StylePriority.UseBorders = false;
            this.lblSoHieuCont3.StylePriority.UseFont = false;
            this.lblSoHieuCont3.StylePriority.UseTextAlignment = false;
            this.lblSoHieuCont3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoHieuCont3.Weight = 0.820580474934037;
            // 
            // lblSoLuongKienCont3
            // 
            this.lblSoLuongKienCont3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSoLuongKienCont3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoLuongKienCont3.Name = "lblSoLuongKienCont3";
            this.lblSoLuongKienCont3.StylePriority.UseBorders = false;
            this.lblSoLuongKienCont3.StylePriority.UseFont = false;
            this.lblSoLuongKienCont3.StylePriority.UseTextAlignment = false;
            this.lblSoLuongKienCont3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongKienCont3.Weight = 0.95646437994722922;
            // 
            // lblTrongLuongCont3
            // 
            this.lblTrongLuongCont3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblTrongLuongCont3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTrongLuongCont3.Name = "lblTrongLuongCont3";
            this.lblTrongLuongCont3.StylePriority.UseBorders = false;
            this.lblTrongLuongCont3.StylePriority.UseFont = false;
            this.lblTrongLuongCont3.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongCont3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongCont3.Weight = 1.0554089709762533;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTCont4,
            this.lblSoHieuCont4,
            this.lblSoLuongKienCont4,
            this.lblTrongLuongCont4});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.StylePriority.UseBorders = false;
            this.xrTableRow13.StylePriority.UseTextAlignment = false;
            this.xrTableRow13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow13.Weight = 1.0000000000000002;
            // 
            // lblSTTCont4
            // 
            this.lblSTTCont4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSTTCont4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSTTCont4.Multiline = true;
            this.lblSTTCont4.Name = "lblSTTCont4";
            this.lblSTTCont4.StylePriority.UseBorders = false;
            this.lblSTTCont4.StylePriority.UseFont = false;
            this.lblSTTCont4.StylePriority.UseTextAlignment = false;
            this.lblSTTCont4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTCont4.Weight = 0.16754617414248019;
            // 
            // lblSoHieuCont4
            // 
            this.lblSoHieuCont4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSoHieuCont4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoHieuCont4.Name = "lblSoHieuCont4";
            this.lblSoHieuCont4.StylePriority.UseBorders = false;
            this.lblSoHieuCont4.StylePriority.UseFont = false;
            this.lblSoHieuCont4.StylePriority.UseTextAlignment = false;
            this.lblSoHieuCont4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoHieuCont4.Weight = 0.820580474934037;
            // 
            // lblSoLuongKienCont4
            // 
            this.lblSoLuongKienCont4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblSoLuongKienCont4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoLuongKienCont4.Name = "lblSoLuongKienCont4";
            this.lblSoLuongKienCont4.StylePriority.UseBorders = false;
            this.lblSoLuongKienCont4.StylePriority.UseFont = false;
            this.lblSoLuongKienCont4.StylePriority.UseTextAlignment = false;
            this.lblSoLuongKienCont4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongKienCont4.Weight = 0.95646437994722922;
            // 
            // lblTrongLuongCont4
            // 
            this.lblTrongLuongCont4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblTrongLuongCont4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTrongLuongCont4.Name = "lblTrongLuongCont4";
            this.lblTrongLuongCont4.StylePriority.UseBorders = false;
            this.lblTrongLuongCont4.StylePriority.UseFont = false;
            this.lblTrongLuongCont4.StylePriority.UseTextAlignment = false;
            this.lblTrongLuongCont4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTrongLuongCont4.Weight = 1.0554089709762533;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell54,
            this.lblTongContainer});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1.0000000000000002;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StylePriority.UseBorders = false;
            this.xrTableCell41.Weight = 0.16754617414248019;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseBorders = false;
            this.xrTableCell42.Weight = 0.820580474934037;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseBorders = false;
            this.xrTableCell43.Weight = 0.9564643799472291;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell54.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StylePriority.UseBorders = false;
            this.xrTableCell54.StylePriority.UseFont = false;
            this.xrTableCell54.StylePriority.UseTextAlignment = false;
            this.xrTableCell54.Text = "Cộng:";
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell54.Weight = 0.23482849604221634;
            // 
            // lblTongContainer
            // 
            this.lblTongContainer.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongContainer.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTongContainer.Name = "lblTongContainer";
            this.lblTongContainer.StylePriority.UseBorders = false;
            this.lblTongContainer.StylePriority.UseFont = false;
            this.lblTongContainer.StylePriority.UseTextAlignment = false;
            this.lblTongContainer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongContainer.Weight = 0.82058047493403685;
            // 
            // ReportHeader4
            // 
            this.ReportHeader4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.ReportHeader4.Height = 50;
            this.ReportHeader4.Name = "ReportHeader4";
            // 
            // xrTable6
            // 
            this.xrTable6.Location = new System.Drawing.Point(17, 0);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow6});
            this.xrTable6.Size = new System.Drawing.Size(758, 50);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseBorders = false;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "31.Lượng hàng, số hiệu container";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 3;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell9,
            this.xrTableCell13,
            this.xrTableCell14});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.StylePriority.UseBorders = false;
            this.xrTableRow6.StylePriority.UseTextAlignment = false;
            this.xrTableRow6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow6.Weight = 1.0000000000000002;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Multiline = true;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "Số TT";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell5.Weight = 0.16754617414248019;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "a.Số hiệu container";
            this.xrTableCell9.Weight = 0.820580474934037;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "b.Số lượng kiện trong container";
            this.xrTableCell13.Weight = 0.95646437994722922;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Text = "c.Trọng lượng hàng trong container";
            this.xrTableCell14.Weight = 1.0554089709762533;
            // 
            // DetailReport3
            // 
            this.DetailReport3.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.ReportHeader3,
            this.ReportFooter3});
            this.DetailReport3.Level = 0;
            this.DetailReport3.Name = "DetailReport3";
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableHang1});
            this.Detail3.Height = 25;
            this.Detail3.Name = "Detail3";
            // 
            // xrTableHang1
            // 
            this.xrTableHang1.Location = new System.Drawing.Point(17, 0);
            this.xrTableHang1.Name = "xrTableHang1";
            this.xrTableHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableHang1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.xrTableHang1.Size = new System.Drawing.Size(758, 25);
            this.xrTableHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTHang1,
            this.lblMoTaHang1,
            this.lblMaHSHang1,
            this.lblXuatXuHang1,
            this.lblCheDoUuDaiHang1,
            this.lblLuongHang1,
            this.lblDVTHang1,
            this.lblDonGiaHang1,
            this.lblTGNTHang1});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableRow25.Weight = 0.75757575757575757;
            // 
            // lblSTTHang1
            // 
            this.lblSTTHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTTHang1.Name = "lblSTTHang1";
            this.lblSTTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTTHang1.Weight = 0.052770448548812666;
            // 
            // lblMoTaHang1
            // 
            this.lblMoTaHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMoTaHang1.Multiline = true;
            this.lblMoTaHang1.Name = "lblMoTaHang1";
            this.lblMoTaHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMoTaHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMoTaHang1.Weight = 0.22163588390501327;
            // 
            // lblMaHSHang1
            // 
            this.lblMaHSHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMaHSHang1.Name = "lblMaHSHang1";
            this.lblMaHSHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHSHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaHSHang1.Weight = 0.13192612137203172;
            // 
            // lblXuatXuHang1
            // 
            this.lblXuatXuHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblXuatXuHang1.Multiline = true;
            this.lblXuatXuHang1.Name = "lblXuatXuHang1";
            this.lblXuatXuHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblXuatXuHang1.StylePriority.UseTextAlignment = false;
            this.lblXuatXuHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblXuatXuHang1.Weight = 0.088390501319261225;
            // 
            // lblCheDoUuDaiHang1
            // 
            this.lblCheDoUuDaiHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCheDoUuDaiHang1.Name = "lblCheDoUuDaiHang1";
            this.lblCheDoUuDaiHang1.StylePriority.UseBorders = false;
            this.lblCheDoUuDaiHang1.Weight = 0.086411609498680764;
            // 
            // lblLuongHang1
            // 
            this.lblLuongHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLuongHang1.Multiline = true;
            this.lblLuongHang1.Name = "lblLuongHang1";
            this.lblLuongHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLuongHang1.StylePriority.UseBorders = false;
            this.lblLuongHang1.StylePriority.UseTextAlignment = false;
            this.lblLuongHang1.Text = "0";
            this.lblLuongHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblLuongHang1.Weight = 0.10883905013192619;
            // 
            // lblDVTHang1
            // 
            this.lblDVTHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDVTHang1.Name = "lblDVTHang1";
            this.lblDVTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDVTHang1.StylePriority.UseBorders = false;
            this.lblDVTHang1.StylePriority.UseTextAlignment = false;
            this.lblDVTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDVTHang1.Weight = 0.077836411609498668;
            // 
            // lblDonGiaHang1
            // 
            this.lblDonGiaHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDonGiaHang1.Name = "lblDonGiaHang1";
            this.lblDonGiaHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonGiaHang1.StylePriority.UseBorders = false;
            this.lblDonGiaHang1.StylePriority.UsePadding = false;
            this.lblDonGiaHang1.StylePriority.UseTextAlignment = false;
            this.lblDonGiaHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblDonGiaHang1.Weight = 0.10949868073878634;
            // 
            // lblTGNTHang1
            // 
            this.lblTGNTHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTGNTHang1.Name = "lblTGNTHang1";
            this.lblTGNTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGNTHang1.StylePriority.UsePadding = false;
            this.lblTGNTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGNTHang1.Weight = 0.1226912928759895;
            // 
            // ReportHeader3
            // 
            this.ReportHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
            this.ReportHeader3.Height = 40;
            this.ReportHeader3.Name = "ReportHeader3";
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable8.Location = new System.Drawing.Point(17, 0);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
            this.xrTable8.Size = new System.Drawing.Size(758, 40);
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell104,
            this.xrTableCell105,
            this.xrTableCell106,
            this.xrTableCell107,
            this.xrTableCell108,
            this.xrTableCell109,
            this.xrTableCell110,
            this.xrTableCell111,
            this.xrTableCell112});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow24.StylePriority.UseTextAlignment = false;
            this.xrTableRow24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow24.Weight = 0.51489361702127678;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell104.Text = "Số TT";
            this.xrTableCell104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell104.Weight = 0.052770448548812666;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell105.Multiline = true;
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell105.Text = "18.Mô tả hàng hóa";
            this.xrTableCell105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell105.Weight = 0.22163588390501329;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell106.Multiline = true;
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell106.Text = "19.Mã số hàng hóa";
            this.xrTableCell106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell106.Weight = 0.13192612137203166;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.StylePriority.UseBorders = false;
            this.xrTableCell107.StylePriority.UseTextAlignment = false;
            this.xrTableCell107.Text = "20.Xuất xứ";
            this.xrTableCell107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell107.Weight = 0.088390501319261225;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.StylePriority.UseBorders = false;
            this.xrTableCell108.Text = "21. Chế độ ưu đãi";
            this.xrTableCell108.Weight = 0.0870712401055409;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell109.Multiline = true;
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell109.StylePriority.UseTextAlignment = false;
            this.xrTableCell109.Text = "22.Lượng";
            this.xrTableCell109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell109.Weight = 0.10949868073878628;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell110.Multiline = true;
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell110.Text = "23.Đơn vị tính";
            this.xrTableCell110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell110.Weight = 0.0778364116094987;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell111.Text = "24.Đơn giá nguyên tệ";
            this.xrTableCell111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell111.Weight = 0.10949868073878628;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell112.Text = "25.Trị giá nguyên tệ";
            this.xrTableCell112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell112.Weight = 0.12137203166226915;
            // 
            // ReportFooter3
            // 
            this.ReportFooter3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableThue1});
            this.ReportFooter3.Height = 154;
            this.ReportFooter3.Name = "ReportFooter3";
            // 
            // xrTableThue1
            // 
            this.xrTableThue1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableThue1.Location = new System.Drawing.Point(17, 0);
            this.xrTableThue1.Name = "xrTableThue1";
            this.xrTableThue1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableThue1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow30,
            this.xrTableRow31});
            this.xrTableThue1.Size = new System.Drawing.Size(758, 154);
            this.xrTableThue1.StylePriority.UseBorders = false;
            this.xrTableThue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell122,
            this.xrTableCell123,
            this.xrTableCell124,
            this.xrTableCell125});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTableRow26.StylePriority.UseTextAlignment = false;
            this.xrTableRow26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableRow26.Weight = 0.51489361702127678;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell122.Multiline = true;
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell122.StylePriority.UseBorders = false;
            this.xrTableCell122.Text = "Loại thuế";
            this.xrTableCell122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell122.Weight = 0.2744063324538259;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell123.Multiline = true;
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell123.StylePriority.UseBorders = false;
            this.xrTableCell123.Text = "Trị giá tính thuế/Số lượng chịu thuế";
            this.xrTableCell123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell123.Weight = 0.30738786279683378;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell124.Multiline = true;
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell124.StylePriority.UseBorders = false;
            this.xrTableCell124.Text = "Thuế suất(%)/ Mức thuế";
            this.xrTableCell124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell124.Weight = 0.1094986807387863;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell125.StylePriority.UseBorders = false;
            this.xrTableCell125.Text = "Tiền thuế";
            this.xrTableCell125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell125.Weight = 0.30870712401055411;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell126,
            this.lblTGTTNKHang1,
            this.lblThueSuatNKHang1,
            this.lblTienThueNKHang1});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 0.36271570291084732;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Text = "26. Thuế nhập khẩu";
            this.xrTableCell126.Weight = 0.2744063324538259;
            // 
            // lblTGTTNKHang1
            // 
            this.lblTGTTNKHang1.Name = "lblTGTTNKHang1";
            this.lblTGTTNKHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTNKHang1.StylePriority.UsePadding = false;
            this.lblTGTTNKHang1.StylePriority.UseTextAlignment = false;
            this.lblTGTTNKHang1.Text = "0";
            this.lblTGTTNKHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTNKHang1.Weight = 0.30738786279683378;
            // 
            // lblThueSuatNKHang1
            // 
            this.lblThueSuatNKHang1.Name = "lblThueSuatNKHang1";
            this.lblThueSuatNKHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatNKHang1.StylePriority.UsePadding = false;
            this.lblThueSuatNKHang1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatNKHang1.Text = "0";
            this.lblThueSuatNKHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatNKHang1.Weight = 0.1094986807387863;
            // 
            // lblTienThueNKHang1
            // 
            this.lblTienThueNKHang1.Name = "lblTienThueNKHang1";
            this.lblTienThueNKHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueNKHang1.StylePriority.UseBorders = false;
            this.lblTienThueNKHang1.StylePriority.UsePadding = false;
            this.lblTienThueNKHang1.StylePriority.UseTextAlignment = false;
            this.lblTienThueNKHang1.Text = "0";
            this.lblTienThueNKHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueNKHang1.Weight = 0.30870712401055411;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell130,
            this.lblTGTTTTDBHang1,
            this.lblThueSuatTTDBHang1,
            this.lblTienThueTTDBHang1});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 0.36413718835661135;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Text = "27.Thuế TTĐB";
            this.xrTableCell130.Weight = 0.2744063324538259;
            // 
            // lblTGTTTTDBHang1
            // 
            this.lblTGTTTTDBHang1.Name = "lblTGTTTTDBHang1";
            this.lblTGTTTTDBHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTTTDBHang1.StylePriority.UsePadding = false;
            this.lblTGTTTTDBHang1.StylePriority.UseTextAlignment = false;
            this.lblTGTTTTDBHang1.Text = "0";
            this.lblTGTTTTDBHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTTTDBHang1.Weight = 0.30738786279683378;
            // 
            // lblThueSuatTTDBHang1
            // 
            this.lblThueSuatTTDBHang1.Name = "lblThueSuatTTDBHang1";
            this.lblThueSuatTTDBHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatTTDBHang1.StylePriority.UsePadding = false;
            this.lblThueSuatTTDBHang1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatTTDBHang1.Text = "0";
            this.lblThueSuatTTDBHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatTTDBHang1.Weight = 0.1094986807387863;
            // 
            // lblTienThueTTDBHang1
            // 
            this.lblTienThueTTDBHang1.Name = "lblTienThueTTDBHang1";
            this.lblTienThueTTDBHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueTTDBHang1.StylePriority.UseBorders = false;
            this.lblTienThueTTDBHang1.StylePriority.UsePadding = false;
            this.lblTienThueTTDBHang1.StylePriority.UseTextAlignment = false;
            this.lblTienThueTTDBHang1.Text = "0";
            this.lblTienThueTTDBHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueTTDBHang1.Weight = 0.30870712401055411;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell134,
            this.lblTGTTBVMTHang1,
            this.lblThueSuatBVMTHang1,
            this.lblTienThueBVMTHang1});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 0.36413718835661169;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Text = "28.Thuế  BVMT";
            this.xrTableCell134.Weight = 0.2744063324538259;
            // 
            // lblTGTTBVMTHang1
            // 
            this.lblTGTTBVMTHang1.Name = "lblTGTTBVMTHang1";
            this.lblTGTTBVMTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTBVMTHang1.StylePriority.UsePadding = false;
            this.lblTGTTBVMTHang1.StylePriority.UseTextAlignment = false;
            this.lblTGTTBVMTHang1.Text = "0";
            this.lblTGTTBVMTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTBVMTHang1.Weight = 0.30738786279683378;
            // 
            // lblThueSuatBVMTHang1
            // 
            this.lblThueSuatBVMTHang1.Name = "lblThueSuatBVMTHang1";
            this.lblThueSuatBVMTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatBVMTHang1.StylePriority.UsePadding = false;
            this.lblThueSuatBVMTHang1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatBVMTHang1.Text = "0";
            this.lblThueSuatBVMTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatBVMTHang1.Weight = 0.1094986807387863;
            // 
            // lblTienThueBVMTHang1
            // 
            this.lblTienThueBVMTHang1.Name = "lblTienThueBVMTHang1";
            this.lblTienThueBVMTHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueBVMTHang1.StylePriority.UseBorders = false;
            this.lblTienThueBVMTHang1.StylePriority.UsePadding = false;
            this.lblTienThueBVMTHang1.StylePriority.UseTextAlignment = false;
            this.lblTienThueBVMTHang1.Text = "0";
            this.lblTienThueBVMTHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueBVMTHang1.Weight = 0.30870712401055411;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell138,
            this.lblTGTTVATHang1,
            this.lblThueSuatVATHang1,
            this.lblTienThueVATHang1});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 0.36413718835661141;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Text = "29.Thuế GTGT";
            this.xrTableCell138.Weight = 0.2744063324538259;
            // 
            // lblTGTTVATHang1
            // 
            this.lblTGTTVATHang1.Name = "lblTGTTVATHang1";
            this.lblTGTTVATHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTGTTVATHang1.StylePriority.UsePadding = false;
            this.lblTGTTVATHang1.StylePriority.UseTextAlignment = false;
            this.lblTGTTVATHang1.Text = "0";
            this.lblTGTTVATHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTGTTVATHang1.Weight = 0.30738786279683383;
            // 
            // lblThueSuatVATHang1
            // 
            this.lblThueSuatVATHang1.Name = "lblThueSuatVATHang1";
            this.lblThueSuatVATHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatVATHang1.StylePriority.UsePadding = false;
            this.lblThueSuatVATHang1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatVATHang1.Text = "0";
            this.lblThueSuatVATHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatVATHang1.Weight = 0.1094986807387863;
            // 
            // lblTienThueVATHang1
            // 
            this.lblTienThueVATHang1.Name = "lblTienThueVATHang1";
            this.lblTienThueVATHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTienThueVATHang1.StylePriority.UsePadding = false;
            this.lblTienThueVATHang1.StylePriority.UseTextAlignment = false;
            this.lblTienThueVATHang1.Text = "0";
            this.lblTienThueVATHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueVATHang1.Weight = 0.30870712401055411;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell142,
            this.lblTongThueHang1});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 0.36500587390680089;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell142.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.StylePriority.UseBorders = false;
            this.xrTableCell142.StylePriority.UseFont = false;
            this.xrTableCell142.StylePriority.UseTextAlignment = false;
            this.xrTableCell142.Text = "Cộng:";
            this.xrTableCell142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell142.Weight = 0.691292875989446;
            // 
            // lblTongThueHang1
            // 
            this.lblTongThueHang1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongThueHang1.Name = "lblTongThueHang1";
            this.lblTongThueHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueHang1.StylePriority.UseBorders = false;
            this.lblTongThueHang1.StylePriority.UsePadding = false;
            this.lblTongThueHang1.StylePriority.UseTextAlignment = false;
            this.lblTongThueHang1.Text = "0";
            this.lblTongThueHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongThueHang1.Weight = 0.30870712401055411;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable14,
            this.xrLabel2,
            this.xrLabel3,
            this.xrLabel4});
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrTable14
            // 
            this.xrTable14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable14.Location = new System.Drawing.Point(17, 50);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 96F);
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32,
            this.xrTableRow33});
            this.xrTable14.Size = new System.Drawing.Size(758, 50);
            this.xrTable14.StylePriority.UseBorders = false;
            this.xrTable14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31,
            this.lblChiCucHQDangKy,
            this.xrTableCell21,
            this.lblSoPhuLucToKhai,
            this.xrTableCell4,
            this.lblSoToKhai});
            this.xrTableRow32.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.StylePriority.UseFont = false;
            this.xrTableRow32.Weight = 0.39523658304223558;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.StylePriority.UsePadding = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.Text = "Chi cục Hải quan đăng ký tờ khai:";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell31.Weight = 0.25131926121372039;
            // 
            // lblChiCucHQDangKy
            // 
            this.lblChiCucHQDangKy.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblChiCucHQDangKy.Name = "lblChiCucHQDangKy";
            this.lblChiCucHQDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblChiCucHQDangKy.StylePriority.UseFont = false;
            this.lblChiCucHQDangKy.StylePriority.UsePadding = false;
            this.lblChiCucHQDangKy.StylePriority.UseTextAlignment = false;
            this.lblChiCucHQDangKy.Text = "hai quan DN";
            this.lblChiCucHQDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblChiCucHQDangKy.Weight = 0.26451187335092352;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.StylePriority.UsePadding = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.Text = "Phụ lục số:";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell21.Weight = 0.087071240105540862;
            // 
            // lblSoPhuLucToKhai
            // 
            this.lblSoPhuLucToKhai.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoPhuLucToKhai.Name = "lblSoPhuLucToKhai";
            this.lblSoPhuLucToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoPhuLucToKhai.StylePriority.UseFont = false;
            this.lblSoPhuLucToKhai.StylePriority.UsePadding = false;
            this.lblSoPhuLucToKhai.StylePriority.UseTextAlignment = false;
            this.lblSoPhuLucToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoPhuLucToKhai.Weight = 0.17546174142480209;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UsePadding = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Số tờ khai:";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell4.Weight = 0.079815303430079143;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoToKhai.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoToKhai.StylePriority.UseBorders = false;
            this.lblSoToKhai.StylePriority.UseFont = false;
            this.lblSoToKhai.StylePriority.UsePadding = false;
            this.lblSoToKhai.StylePriority.UseTextAlignment = false;
            this.lblSoToKhai.Text = "312";
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoToKhai.Weight = 0.14182058047493404;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.lblChiCucHQCuaKhau,
            this.xrTableCell23,
            this.lblNgayDangKy,
            this.xrTableCell29,
            this.lblLoaiHinh});
            this.xrTableRow33.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.StylePriority.UseFont = false;
            this.xrTableRow33.Weight = 0.41136868847253083;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell27.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.xrTableCell27.StylePriority.UseBorders = false;
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UsePadding = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "Chi cục Hải quan cửa khẩu nhập:";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell27.Weight = 0.25395778364116089;
            // 
            // lblChiCucHQCuaKhau
            // 
            this.lblChiCucHQCuaKhau.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblChiCucHQCuaKhau.Name = "lblChiCucHQCuaKhau";
            this.lblChiCucHQCuaKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblChiCucHQCuaKhau.StylePriority.UseFont = false;
            this.lblChiCucHQCuaKhau.StylePriority.UsePadding = false;
            this.lblChiCucHQCuaKhau.StylePriority.UseTextAlignment = false;
            this.lblChiCucHQCuaKhau.Text = "c34c";
            this.lblChiCucHQCuaKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblChiCucHQCuaKhau.Weight = 0.26319261213720324;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UsePadding = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "Ngày, giờ đăng ký:";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell23.Weight = 0.14248021108179418;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblNgayDangKy.Multiline = true;
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblNgayDangKy.StylePriority.UseFont = false;
            this.lblNgayDangKy.StylePriority.UsePadding = false;
            this.lblNgayDangKy.StylePriority.UseTextAlignment = false;
            this.lblNgayDangKy.Text = "22/22/2012 10:00\r\n";
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgayDangKy.Weight = 0.12137203166226915;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 0, 0, 0, 100F);
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.StylePriority.UsePadding = false;
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.Text = "Loại hình:";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell29.Weight = 0.0758575197889182;
            // 
            // lblLoaiHinh
            // 
            this.lblLoaiHinh.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblLoaiHinh.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblLoaiHinh.Name = "lblLoaiHinh";
            this.lblLoaiHinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblLoaiHinh.StylePriority.UseBorders = false;
            this.lblLoaiHinh.StylePriority.UseFont = false;
            this.lblLoaiHinh.StylePriority.UsePadding = false;
            this.lblLoaiHinh.StylePriority.UseTextAlignment = false;
            this.lblLoaiHinh.Text = "KD";
            this.lblLoaiHinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblLoaiHinh.Weight = 0.14313984168865437;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.Location = new System.Drawing.Point(25, 8);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.Size = new System.Drawing.Size(208, 23);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "HẢI QUAN VIỆT NAM";
            // 
            // xrLabel3
            // 
            this.xrLabel3.Location = new System.Drawing.Point(692, 8);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.Size = new System.Drawing.Size(100, 25);
            this.xrLabel3.Text = "HQ/2012-PLXK";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.Location = new System.Drawing.Point(50, 8);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.Size = new System.Drawing.Size(750, 25);
            this.xrLabel4.Text = "             PHỤ LỤC TỜ KHAI HÀNG HÓA NHẬP KHẨU";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrLabel8,
            this.xrLabel9});
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Location = new System.Drawing.Point(508, 0);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.Size = new System.Drawing.Size(258, 33);
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "33.Tôi xin cam đoan, chịu trách nhiệm\r\n trước pháp luật về nội dung khai trên tờ " +
                "khai";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Location = new System.Drawing.Point(558, 33);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.Size = new System.Drawing.Size(175, 17);
            this.xrLabel8.Text = "Ngày      tháng      năm";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Location = new System.Drawing.Point(508, 50);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.Size = new System.Drawing.Size(233, 23);
            this.xrLabel9.Text = "(Người khai ký, ghi rõ họ tên, đóng dấu)";
            // 
            // TQDTPhuLucToKhaiNhap_TT15
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.DetailReport1,
            this.DetailReport2,
            this.DetailReport4,
            this.DetailReport3,
            this.GroupHeader1,
            this.GroupFooter1});
            this.Margins = new System.Drawing.Printing.Margins(5, 15, 4, 18);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableCont)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableHang1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableThue1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTableHang3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHSHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblXuatXuHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblDVTHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblTGNTHang3;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport2;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRTable xrTableHang2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHSHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblXuatXuHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblTGNTHang2;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport4;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.XRTable xrTableCont;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTCont4;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuCont4;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongKienCont4;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongCont4;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader2;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter2;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader4;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell lblCheDoUuDaiHang3;
        private DevExpress.XtraReports.UI.XRTable xrTableThue3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTNKHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatNKHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueNKHang3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTTTDBHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatTTDBHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueTTDBHang3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTBVMTHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatBVMTHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueBVMTHang3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTVATHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatVATHang3;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueVATHang3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell lblTongThueHang3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell lblCheDoUuDaiHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblDVTHang2;
        private DevExpress.XtraReports.UI.XRTable xrTableThue2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTNKHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatNKHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueNKHang2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTTTDBHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatTTDBHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueTTDBHang2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTBVMTHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatBVMTHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueBVMTHang2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTVATHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatVATHang2;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueVATHang2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell lblTongThueHang2;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport3;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.XRTable xrTableHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHSHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblXuatXuHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblCheDoUuDaiHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblLuongHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblDVTHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTGNTHang1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader3;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter3;
        private DevExpress.XtraReports.UI.XRTable xrTableThue1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTNKHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatNKHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueNKHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTTTDBHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatTTDBHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueTTDBHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTBVMTHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatBVMTHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueBVMTHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell lblTGTTVATHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatVATHang1;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueVATHang1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell lblTongThueHang1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell lblChiCucHQDangKy;
        private DevExpress.XtraReports.UI.XRTableCell lblSoPhuLucToKhai;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell lblChiCucHQCuaKhau;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRTableCell lblLoaiHinh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTCont3;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuCont3;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongKienCont3;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongCont3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTCont1;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuCont1;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongKienCont1;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongCont1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTCont2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHieuCont2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongKienCont2;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuongCont2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell lblTongContainer;
    }
}
