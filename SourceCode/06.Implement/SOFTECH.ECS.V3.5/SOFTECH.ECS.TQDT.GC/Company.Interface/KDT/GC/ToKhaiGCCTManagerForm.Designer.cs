﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.GC
{
    partial class ToKhaiGCCTManagerForm
    {
        private UIGroupBox uiGroupBox1; 
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgList_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiGCCTManagerForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageListSmall = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.label5 = new System.Windows.Forms.Label();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnResultHistory = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.donViHaiQuanControl1 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label2 = new System.Windows.Forms.Label();
            this.timToKhai1 = new Company.Interface.Controls.TimToKhai();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.uiContextMenu1 = new Janus.Windows.UI.CommandBars.UIContextMenu();
            this.cmdPrint2 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdCSDaDuyet2 = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.Copy2 = new Janus.Windows.UI.CommandBars.UICommand("Copy");
            this.cmdExportExcel2 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExcel");
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdCSDaDuyet1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.Copy1 = new Janus.Windows.UI.CommandBars.UICommand("Copy");
            this.cmdExportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExcel");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSingleDownload = new Janus.Windows.UI.CommandBars.UICommand("cmdSingleDownload");
            this.cmdCancel = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.ToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiViet");
            this.InPhieuTN1 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdInTKGCCT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKGCCT");
            this.cmdInGCCTTQ1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInGCCTTQ");
            this.inTKGCCT_1961 = new Janus.Windows.UI.CommandBars.UICommand("inTKGCCT_196");
            this.cmdInSuaDoiBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInSuaDoiBoSung");
            this.cmdInBangHangHoa_HD1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInBangHangHoa_HD");
            this.cmdXuatToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhai");
            this.cmdCSDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.CopyTK = new Janus.Windows.UI.CommandBars.UICommand("CopyTK");
            this.CopyHang = new Janus.Windows.UI.CommandBars.UICommand("CopyHang");
            this.Copy = new Janus.Windows.UI.CommandBars.UICommand("Copy");
            this.CopyTK1 = new Janus.Windows.UI.CommandBars.UICommand("CopyTK");
            this.CopyHang1 = new Janus.Windows.UI.CommandBars.UICommand("CopyHang");
            this.InPhieuTN = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.ToKhaiViet = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiViet");
            this.cmdInTKGCCT = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKGCCT");
            this.cmdTKGCCTA4 = new Janus.Windows.UI.CommandBars.UICommand("cmdTKGCCTA4");
            this.cmdInGCCTTQ = new Janus.Windows.UI.CommandBars.UICommand("cmdInGCCTTQ");
            this.inTKGCCT_196 = new Janus.Windows.UI.CommandBars.UICommand("inTKGCCT_196");
            this.cmdInBangHangHoa_HD = new Janus.Windows.UI.CommandBars.UICommand("cmdInBangHangHoa_HD");
            this.cmdInSuaDoiBoSung = new Janus.Windows.UI.CommandBars.UICommand("cmdInSuaDoiBoSung");
            this.cmdExportExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExcel");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiContextMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(0, 32);
            this.grbMain.Size = new System.Drawing.Size(989, 299);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 32);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(989, 299);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            this.cmMain.SetContextMenu(this.dgList, this.uiContextMenu1);
            dgList_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgList_DesignTimeLayout_Reference_0.Instance")));
            dgList_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgList_DesignTimeLayout_Reference_0});
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.ImageList = this.ImageListSmall;
            this.dgList.Location = new System.Drawing.Point(0, 88);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(989, 168);
            this.dgList.TabIndex = 304;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // ImageListSmall
            // 
            this.ImageListSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageListSmall.ImageStream")));
            this.ImageListSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageListSmall.Images.SetKeyName(0, "");
            this.ImageListSmall.Images.SetKeyName(1, "");
            this.ImageListSmall.Images.SetKeyName(2, "");
            this.ImageListSmall.Images.SetKeyName(3, "");
            this.ImageListSmall.Images.SetKeyName(4, "app_large_icons.png");
            this.ImageListSmall.Images.SetKeyName(5, "page_excel.png");
            this.ImageListSmall.Images.SetKeyName(6, "edit32.png");
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.btnXoa);
            this.uiGroupBox3.Controls.Add(this.btnResultHistory);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 256);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(989, 43);
            this.uiGroupBox3.TabIndex = 303;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(910, 13);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(72, 23);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(5, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(205, 14);
            this.label5.TabIndex = 10;
            this.label5.Text = "Hướng dẫn: Kích đôi để xem chi tiết";
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(826, 13);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 11;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnResultHistory
            // 
            this.btnResultHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResultHistory.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResultHistory.Image = ((System.Drawing.Image)(resources.GetObject("btnResultHistory.Image")));
            this.btnResultHistory.ImageList = this.ImageListSmall;
            this.btnResultHistory.ImageSize = new System.Drawing.Size(20, 20);
            this.btnResultHistory.Location = new System.Drawing.Point(713, 13);
            this.btnResultHistory.Name = "btnResultHistory";
            this.btnResultHistory.Size = new System.Drawing.Size(107, 23);
            this.btnResultHistory.TabIndex = 13;
            this.btnResultHistory.Text = "Kết quả xử lý";
            this.btnResultHistory.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnResultHistory.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.donViHaiQuanControl1);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.timToKhai1);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(989, 88);
            this.uiGroupBox2.TabIndex = 302;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(122, 15);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = false;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(433, 22);
            this.donViHaiQuanControl1.TabIndex = 303;
            this.donViHaiQuanControl1.VisualStyleManager = null;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 14);
            this.label2.TabIndex = 300;
            this.label2.Text = "Hải quan tiếp nhận";
            // 
            // timToKhai1
            // 
            this.timToKhai1.AutoSize = true;
            this.timToKhai1.BackColor = System.Drawing.Color.Transparent;
            this.timToKhai1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timToKhai1.Location = new System.Drawing.Point(28, 47);
            this.timToKhai1.Name = "timToKhai1";
            this.timToKhai1.Size = new System.Drawing.Size(925, 28);
            this.timToKhai1.TabIndex = 299;
            this.timToKhai1.Search += new System.EventHandler<Company.Interface.Controls.StringEventArgs>(this.timToKhai1_Search);
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSingleDownload,
            this.cmdCancel,
            this.XacNhan,
            this.cmdPrint,
            this.cmdXuatToKhai,
            this.cmdCSDaDuyet,
            this.CopyTK,
            this.CopyHang,
            this.Copy,
            this.InPhieuTN,
            this.ToKhaiViet,
            this.cmdInTKGCCT,
            this.cmdTKGCCTA4,
            this.cmdInGCCTTQ,
            this.inTKGCCT_196,
            this.cmdInBangHangHoa_HD,
            this.cmdInSuaDoiBoSung,
            this.cmdExportExcel});
            this.cmMain.ContainerControl = this;
            this.cmMain.ContextMenus.AddRange(new Janus.Windows.UI.CommandBars.UIContextMenu[] {
            this.uiContextMenu1});
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.ShowShortcutInToolTips = true;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick_1);
            // 
            // uiContextMenu1
            // 
            this.uiContextMenu1.CommandManager = this.cmMain;
            this.uiContextMenu1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdPrint2,
            this.cmdCSDaDuyet2,
            this.Copy2,
            this.cmdExportExcel2});
            this.uiContextMenu1.Key = "ContextMenu1";
            // 
            // cmdPrint2
            // 
            this.cmdPrint2.Key = "cmdPrint";
            this.cmdPrint2.Name = "cmdPrint2";
            // 
            // cmdCSDaDuyet2
            // 
            this.cmdCSDaDuyet2.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet2.Name = "cmdCSDaDuyet2";
            // 
            // Copy2
            // 
            this.Copy2.Key = "Copy";
            this.Copy2.Name = "Copy2";
            // 
            // cmdExportExcel2
            // 
            this.cmdExportExcel2.Key = "cmdExportExcel";
            this.cmdExportExcel2.Name = "cmdExportExcel2";
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCSDaDuyet1,
            this.cmdPrint1,
            this.Copy1,
            this.cmdExportExcel1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(989, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdCSDaDuyet1
            // 
            this.cmdCSDaDuyet1.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet1.Name = "cmdCSDaDuyet1";
            this.cmdCSDaDuyet1.Text = "&Chuyển Trạng Thái";
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            this.cmdPrint1.Text = "&In ";
            // 
            // Copy1
            // 
            this.Copy1.Key = "Copy";
            this.Copy1.Name = "Copy1";
            // 
            // cmdExportExcel1
            // 
            this.cmdExportExcel1.Key = "cmdExportExcel";
            this.cmdExportExcel1.Name = "cmdExportExcel1";
            // 
            // cmdSend
            // 
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSend.Text = "Khai báo";
            this.cmdSend.ToolTipText = "Khai báo (Ctrl + S)";
            // 
            // cmdSingleDownload
            // 
            this.cmdSingleDownload.Image = ((System.Drawing.Image)(resources.GetObject("cmdSingleDownload.Image")));
            this.cmdSingleDownload.Key = "cmdSingleDownload";
            this.cmdSingleDownload.Name = "cmdSingleDownload";
            this.cmdSingleDownload.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            this.cmdSingleDownload.Text = "Nhận dữ liệu";
            this.cmdSingleDownload.ToolTipText = "Nhận dữ liệu của tờ khai đang chọn  (Ctrl + D)";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Image = ((System.Drawing.Image)(resources.GetObject("cmdCancel.Image")));
            this.cmdCancel.Key = "cmdCancel";
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Shortcut = System.Windows.Forms.Shortcut.CtrlC;
            this.cmdCancel.Text = "Hủy khai báo";
            this.cmdCancel.ToolTipText = "Hủy khai báo (Ctrl + C)";
            // 
            // XacNhan
            // 
            this.XacNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan.Icon")));
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận thông tin";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ToKhai1,
            this.InPhieuTN1,
            this.cmdInTKGCCT1,
            this.cmdInGCCTTQ1,
            this.inTKGCCT_1961,
            this.cmdInSuaDoiBoSung1,
            this.cmdInBangHangHoa_HD1});
            this.cmdPrint.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrint.Image")));
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.cmdPrint.Text = "In ";
            // 
            // ToKhai1
            // 
            this.ToKhai1.Image = ((System.Drawing.Image)(resources.GetObject("ToKhai1.Image")));
            this.ToKhai1.Key = "ToKhaiViet";
            this.ToKhai1.Name = "ToKhai1";
            this.ToKhai1.Text = "Tờ khai";
            this.ToKhai1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // InPhieuTN1
            // 
            this.InPhieuTN1.Key = "InPhieuTN";
            this.InPhieuTN1.Name = "InPhieuTN1";
            this.InPhieuTN1.Text = "Phiếu tiếp nhận";
            this.InPhieuTN1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdInTKGCCT1
            // 
            this.cmdInTKGCCT1.Key = "cmdInTKGCCT";
            this.cmdInTKGCCT1.Name = "cmdInTKGCCT1";
            this.cmdInTKGCCT1.Text = "Tờ khai gia công chuyển tiếp A4";
            this.cmdInTKGCCT1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdInGCCTTQ1
            // 
            this.cmdInGCCTTQ1.Key = "cmdInGCCTTQ";
            this.cmdInGCCTTQ1.Name = "cmdInGCCTTQ1";
            this.cmdInGCCTTQ1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // inTKGCCT_1961
            // 
            this.inTKGCCT_1961.Image = ((System.Drawing.Image)(resources.GetObject("inTKGCCT_1961.Image")));
            this.inTKGCCT_1961.Key = "inTKGCCT_196";
            this.inTKGCCT_1961.Name = "inTKGCCT_1961";
            this.inTKGCCT_1961.Text = "In tờ khai thông quan chuyển tiếp (Thông tư 196)";
            // 
            // cmdInSuaDoiBoSung1
            // 
            this.cmdInSuaDoiBoSung1.Image = ((System.Drawing.Image)(resources.GetObject("cmdInSuaDoiBoSung1.Image")));
            this.cmdInSuaDoiBoSung1.Key = "cmdInSuaDoiBoSung";
            this.cmdInSuaDoiBoSung1.Name = "cmdInSuaDoiBoSung1";
            // 
            // cmdInBangHangHoa_HD1
            // 
            this.cmdInBangHangHoa_HD1.Image = ((System.Drawing.Image)(resources.GetObject("cmdInBangHangHoa_HD1.Image")));
            this.cmdInBangHangHoa_HD1.Key = "cmdInBangHangHoa_HD";
            this.cmdInBangHangHoa_HD1.Name = "cmdInBangHangHoa_HD1";
            // 
            // cmdXuatToKhai
            // 
            this.cmdXuatToKhai.Key = "cmdXuatToKhai";
            this.cmdXuatToKhai.Name = "cmdXuatToKhai";
            this.cmdXuatToKhai.Text = "Xuất dữ liệu cho phòng khai";
            // 
            // cmdCSDaDuyet
            // 
            this.cmdCSDaDuyet.Image = ((System.Drawing.Image)(resources.GetObject("cmdCSDaDuyet.Image")));
            this.cmdCSDaDuyet.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Name = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            this.cmdCSDaDuyet.Text = "Chuyển Trạng Thái";
            // 
            // CopyTK
            // 
            this.CopyTK.Icon = ((System.Drawing.Icon)(resources.GetObject("CopyTK.Icon")));
            this.CopyTK.Key = "CopyTK";
            this.CopyTK.Name = "CopyTK";
            this.CopyTK.Text = "Sao chép tờ khai";
            // 
            // CopyHang
            // 
            this.CopyHang.Icon = ((System.Drawing.Icon)(resources.GetObject("CopyHang.Icon")));
            this.CopyHang.Key = "CopyHang";
            this.CopyHang.Name = "CopyHang";
            this.CopyHang.Text = "Sao chép cả hàng hóa";
            // 
            // Copy
            // 
            this.Copy.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.CopyTK1,
            this.CopyHang1});
            this.Copy.Image = ((System.Drawing.Image)(resources.GetObject("Copy.Image")));
            this.Copy.Key = "Copy";
            this.Copy.Name = "Copy";
            this.Copy.Text = "Sao chép";
            // 
            // CopyTK1
            // 
            this.CopyTK1.Image = ((System.Drawing.Image)(resources.GetObject("CopyTK1.Image")));
            this.CopyTK1.Key = "CopyTK";
            this.CopyTK1.Name = "CopyTK1";
            // 
            // CopyHang1
            // 
            this.CopyHang1.Image = ((System.Drawing.Image)(resources.GetObject("CopyHang1.Image")));
            this.CopyHang1.Key = "CopyHang";
            this.CopyHang1.Name = "CopyHang1";
            // 
            // InPhieuTN
            // 
            this.InPhieuTN.Image = ((System.Drawing.Image)(resources.GetObject("InPhieuTN.Image")));
            this.InPhieuTN.Key = "InPhieuTN";
            this.InPhieuTN.Name = "InPhieuTN";
            this.InPhieuTN.Text = "Phiếu tiếp nhận";
            // 
            // ToKhaiViet
            // 
            this.ToKhaiViet.Key = "ToKhaiViet";
            this.ToKhaiViet.Name = "ToKhaiViet";
            this.ToKhaiViet.Text = "Tờ khai";
            // 
            // cmdInTKGCCT
            // 
            this.cmdInTKGCCT.Image = ((System.Drawing.Image)(resources.GetObject("cmdInTKGCCT.Image")));
            this.cmdInTKGCCT.Key = "cmdInTKGCCT";
            this.cmdInTKGCCT.Name = "cmdInTKGCCT";
            this.cmdInTKGCCT.Text = "Tờ khai gia công chuyển tiếp A4";
            // 
            // cmdTKGCCTA4
            // 
            this.cmdTKGCCTA4.Image = ((System.Drawing.Image)(resources.GetObject("cmdTKGCCTA4.Image")));
            this.cmdTKGCCTA4.Key = "cmdTKGCCTA4";
            this.cmdTKGCCTA4.Name = "cmdTKGCCTA4";
            this.cmdTKGCCTA4.Text = "In TK GCCT A4";
            // 
            // cmdInGCCTTQ
            // 
            this.cmdInGCCTTQ.Image = ((System.Drawing.Image)(resources.GetObject("cmdInGCCTTQ.Image")));
            this.cmdInGCCTTQ.Key = "cmdInGCCTTQ";
            this.cmdInGCCTTQ.Name = "cmdInGCCTTQ";
            this.cmdInGCCTTQ.Text = "Tờ khai gia công chuyển tiếp thông quan";
            // 
            // inTKGCCT_196
            // 
            this.inTKGCCT_196.Key = "inTKGCCT_196";
            this.inTKGCCT_196.Name = "inTKGCCT_196";
            this.inTKGCCT_196.Text = "In tờ khai thông quan gia công chuyển tiếp TT196";
            // 
            // cmdInBangHangHoa_HD
            // 
            this.cmdInBangHangHoa_HD.Key = "cmdInBangHangHoa_HD";
            this.cmdInBangHangHoa_HD.Name = "cmdInBangHangHoa_HD";
            this.cmdInBangHangHoa_HD.Text = "In bảng kê hợp đồng/ đơn hàng";
            // 
            // cmdInSuaDoiBoSung
            // 
            this.cmdInSuaDoiBoSung.Key = "cmdInSuaDoiBoSung";
            this.cmdInSuaDoiBoSung.Name = "cmdInSuaDoiBoSung";
            this.cmdInSuaDoiBoSung.Text = "In tờ khai sửa đổi bổ sung";
            // 
            // cmdExportExcel
            // 
            this.cmdExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdExportExcel.Image")));
            this.cmdExportExcel.Key = "cmdExportExcel";
            this.cmdExportExcel.Name = "cmdExportExcel";
            this.cmdExportExcel.Text = "Xuất Excel";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(989, 32);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            this.saveFileDialog1.RestoreDirectory = true;
            // 
            // ToKhaiGCCTManagerForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.ClientSize = new System.Drawing.Size(989, 331);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ToKhaiGCCTManagerForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Tag = "ToKhaiGCCTManagerForm";
            this.Text = "Theo dõi tờ khai GCCT";
            this.Load += new System.EventHandler(this.ToKhaiGCCTManagerForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiContextMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private UIButton btnClose;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdSingleDownload;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel;
        private Janus.Windows.UI.CommandBars.UICommand XacNhan;
        private ImageList ImageListSmall;
        private Janus.Windows.UI.CommandBars.UIContextMenu uiContextMenu1;
        private Label label5;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint1;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint2;
        private UIButton btnXoa;
        private SaveFileDialog saveFileDialog1;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuatToKhai;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet2;
        private Janus.Windows.UI.CommandBars.UICommand Copy1;
        private Janus.Windows.UI.CommandBars.UICommand CopyTK;
        private Janus.Windows.UI.CommandBars.UICommand CopyHang;
        private Janus.Windows.UI.CommandBars.UICommand Copy;
        private Janus.Windows.UI.CommandBars.UICommand CopyTK1;
        private Janus.Windows.UI.CommandBars.UICommand CopyHang1;
        private Janus.Windows.UI.CommandBars.UICommand Copy2;
        private Janus.Windows.UI.CommandBars.UICommand ToKhai;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN;
        private Janus.Windows.UI.CommandBars.UICommand ToKhai1;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN1;
        private Janus.Windows.UI.CommandBars.UICommand cmdInTKGCCT1;
        private Janus.Windows.UI.CommandBars.UICommand cmdInTKGCCT;
        private Janus.Windows.UI.CommandBars.UICommand cmdTKGCCTA4;
        private UIButton btnResultHistory;
        private Janus.Windows.UI.CommandBars.UICommand ToKhaiViet;
        private Janus.Windows.UI.CommandBars.UICommand cmdInGCCTTQ1;
        private Janus.Windows.UI.CommandBars.UICommand cmdInGCCTTQ;
        private Label label2;
        private Janus.Windows.UI.CommandBars.UICommand inTKGCCT_1961;
        private Janus.Windows.UI.CommandBars.UICommand inTKGCCT_196;
        private Janus.Windows.UI.CommandBars.UICommand cmdInSuaDoiBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdInBangHangHoa_HD1;
        private Janus.Windows.UI.CommandBars.UICommand cmdInBangHangHoa_HD;
        private Janus.Windows.UI.CommandBars.UICommand cmdInSuaDoiBoSung;
        private UIGroupBox uiGroupBox2;
        private GridEX dgList;
        private UIGroupBox uiGroupBox3;
        private Janus.Windows.UI.CommandBars.UICommand cmdExportExcel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdExportExcel;
        private Janus.Windows.UI.CommandBars.UICommand cmdExportExcel2;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl1;
        private Company.Interface.Controls.TimToKhai timToKhai1;
    }
}
