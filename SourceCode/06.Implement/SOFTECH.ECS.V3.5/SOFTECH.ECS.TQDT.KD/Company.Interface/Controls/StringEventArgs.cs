﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.Interface.Controls
{
    public class StringEventArgs : EventArgs
    {
        public StringEventArgs(string str, int status)
        {
            _string = str;
            _status = status;
        }

        string _string;
        int _status;
        public int Status
        {
            get
            {
                return _status;
            }
        }
        public string String
        {
            get { return _string; }
        }
    }
}
