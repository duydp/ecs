﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
   public class CommercialInvoice
    {
        /// <summary>
        /// Số hóa đơn thương mại
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Ngày phát hành hóa đơn thương mại
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// AttachedFile
        /// </summary>
        [XmlElement("AttachedFile")]
        public Company.KDT.SHARE.Components.AttachedFile AttachedFile { get; set; }
    }
}
