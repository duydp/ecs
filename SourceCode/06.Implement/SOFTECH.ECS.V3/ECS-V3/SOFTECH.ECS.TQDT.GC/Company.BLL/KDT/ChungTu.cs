
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace Company.GC.BLL.KDT
{
	public partial class ChungTu : EntityBase
	{
		#region Private members.
		
		protected long _ID;
		protected string _TenChungTu = String.Empty;
		protected short _SoBanChinh;
		protected short _SoBanSao;
		protected string _FileUpLoad = String.Empty;
		protected long _Master_ID;
		protected int _STTHang;
		protected int _LoaiCT;
		protected byte[] _NoiDung;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public long ID
		{
			set {this._ID = value;}
			get {return this._ID;}
		}
		public string TenChungTu
		{
			set {this._TenChungTu = value;}
			get {return this._TenChungTu;}
		}
		public short SoBanChinh
		{
			set {this._SoBanChinh = value;}
			get {return this._SoBanChinh;}
		}
		public short SoBanSao
		{
			set {this._SoBanSao = value;}
			get {return this._SoBanSao;}
		}
		public string FileUpLoad
		{
			set {this._FileUpLoad = value;}
			get {return this._FileUpLoad;}
		}
		public long Master_ID
		{
			set {this._Master_ID = value;}
			get {return this._Master_ID;}
		}
		public int STTHang
		{
			set {this._STTHang = value;}
			get {return this._STTHang;}
		}
		public int LoaiCT
		{
			set {this._LoaiCT = value;}
			get {return this._LoaiCT;}
		}
		public byte[] NoiDung
		{
			set {this._NoiDung = value;}
			get {return this._NoiDung;}
		}
		
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
       
       
		public bool Load()
		{
			string spName = "p_KDT_SXXK_ChungTuKemTheo_Load";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenChungTu"))) this._TenChungTu = reader.GetString(reader.GetOrdinal("TenChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoBanChinh"))) this._SoBanChinh = reader.GetInt16(reader.GetOrdinal("SoBanChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoBanSao"))) this._SoBanSao = reader.GetInt16(reader.GetOrdinal("SoBanSao"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileUpLoad"))) this._FileUpLoad = reader.GetString(reader.GetOrdinal("FileUpLoad"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) this._Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiCT"))) this._LoaiCT = reader.GetInt32(reader.GetOrdinal("LoaiCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) this._NoiDung = (byte[])reader.GetValue(reader.GetOrdinal("NoiDung"));
				return true;
			}
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public ChungTuCollection SelectCollectionBy_Master_ID()
		{
			string spName = "p_KDT_SXXK_ChungTuKemTheo_SelectBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
			
			ChungTuCollection collection = new ChungTuCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			while (reader.Read())
			{
				ChungTu entity = new ChungTu();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenChungTu"))) entity.TenChungTu = reader.GetString(reader.GetOrdinal("TenChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoBanChinh"))) entity.SoBanChinh = reader.GetInt16(reader.GetOrdinal("SoBanChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoBanSao"))) entity.SoBanSao = reader.GetInt16(reader.GetOrdinal("SoBanSao"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileUpLoad"))) entity.FileUpLoad = reader.GetString(reader.GetOrdinal("FileUpLoad"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiCT"))) entity.LoaiCT = reader.GetInt32(reader.GetOrdinal("LoaiCT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = (byte[])reader.GetValue(reader.GetOrdinal("NoiDung"));
				collection.Add(entity);
			}
			return collection;
		}

		//---------------------------------------------------------------------------------------------

		public DataSet SelectBy_Master_ID()
		{
			string spName = "p_KDT_SXXK_ChungTuKemTheo_SelectBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
						
            return this.db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public DataSet SelectAll()
        {
            string spName = "p_KDT_SXXK_ChungTuKemTheo_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_SXXK_ChungTuKemTheo_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_SXXK_ChungTuKemTheo_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_SXXK_ChungTuKemTheo_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		public ChungTuCollection SelectCollectionAll()
		{
			ChungTuCollection collection = new ChungTuCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				ChungTu entity = new ChungTu();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenChungTu"))) entity.TenChungTu = reader.GetString(reader.GetOrdinal("TenChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoBanChinh"))) entity.SoBanChinh = reader.GetInt16(reader.GetOrdinal("SoBanChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoBanSao"))) entity.SoBanSao = reader.GetInt16(reader.GetOrdinal("SoBanSao"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileUpLoad"))) entity.FileUpLoad = reader.GetString(reader.GetOrdinal("FileUpLoad"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiCT"))) entity.LoaiCT = reader.GetInt32(reader.GetOrdinal("LoaiCT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = (byte[])reader.GetValue(reader.GetOrdinal("NoiDung"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public ChungTuCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			ChungTuCollection collection = new ChungTuCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				ChungTu entity = new ChungTu();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenChungTu"))) entity.TenChungTu = reader.GetString(reader.GetOrdinal("TenChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoBanChinh"))) entity.SoBanChinh = reader.GetInt16(reader.GetOrdinal("SoBanChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoBanSao"))) entity.SoBanSao = reader.GetInt16(reader.GetOrdinal("SoBanSao"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileUpLoad"))) entity.FileUpLoad = reader.GetString(reader.GetOrdinal("FileUpLoad"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiCT"))) entity.LoaiCT = reader.GetInt32(reader.GetOrdinal("LoaiCT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = (byte[])reader.GetValue(reader.GetOrdinal("NoiDung"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_SXXK_ChungTuKemTheo_Insert";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			this.db.AddInParameter(dbCommand, "@TenChungTu", SqlDbType.NVarChar, this._TenChungTu);
			this.db.AddInParameter(dbCommand, "@SoBanChinh", SqlDbType.SmallInt, this._SoBanChinh);
			this.db.AddInParameter(dbCommand, "@SoBanSao", SqlDbType.SmallInt, this._SoBanSao);
			this.db.AddInParameter(dbCommand, "@FileUpLoad", SqlDbType.VarChar, this._FileUpLoad);
			this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
			this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			this.db.AddInParameter(dbCommand, "@LoaiCT", SqlDbType.Int, this._LoaiCT);
			this.db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.Image, this._NoiDung);
			
			if (transaction != null)
			{
				this.db.ExecuteNonQuery(dbCommand, transaction);
				this._ID = (long) this.db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}
            else
			{
				this.db.ExecuteNonQuery(dbCommand);
				this._ID = (long) this.db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(ChungTuCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (ChungTu item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, ChungTuCollection collection)
        {
            foreach (ChungTu item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_SXXK_ChungTuKemTheo_InsertUpdate";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			this.db.AddInParameter(dbCommand, "@TenChungTu", SqlDbType.NVarChar, this._TenChungTu);
			this.db.AddInParameter(dbCommand, "@SoBanChinh", SqlDbType.SmallInt, this._SoBanChinh);
			this.db.AddInParameter(dbCommand, "@SoBanSao", SqlDbType.SmallInt, this._SoBanSao);
			this.db.AddInParameter(dbCommand, "@FileUpLoad", SqlDbType.VarChar, this._FileUpLoad);
			this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
			this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			this.db.AddInParameter(dbCommand, "@LoaiCT", SqlDbType.Int, this._LoaiCT);
			this.db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.Image, this._NoiDung);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(ChungTuCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (ChungTu item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_SXXK_ChungTuKemTheo_Update";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			this.db.AddInParameter(dbCommand, "@TenChungTu", SqlDbType.NVarChar, this._TenChungTu);
			this.db.AddInParameter(dbCommand, "@SoBanChinh", SqlDbType.SmallInt, this._SoBanChinh);
			this.db.AddInParameter(dbCommand, "@SoBanSao", SqlDbType.SmallInt, this._SoBanSao);
			this.db.AddInParameter(dbCommand, "@FileUpLoad", SqlDbType.VarChar, this._FileUpLoad);
			this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
			this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
			this.db.AddInParameter(dbCommand, "@LoaiCT", SqlDbType.Int, this._LoaiCT);
			this.db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.Image, this._NoiDung);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(ChungTuCollection collection, SqlTransaction transaction)
        {
            foreach (ChungTu item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_SXXK_ChungTuKemTheo_Delete";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(ChungTuCollection collection, SqlTransaction transaction)
        {
            foreach (ChungTu item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(ChungTuCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (ChungTu item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		#endregion
	}	
}