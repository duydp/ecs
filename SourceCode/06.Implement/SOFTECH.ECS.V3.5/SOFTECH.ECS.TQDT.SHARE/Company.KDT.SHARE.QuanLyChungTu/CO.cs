﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class CO
    {
        public string TenCangDoHang { set; get; }
        public string TenCangXepHang { set; get; }

        public List<HangCoDetail> ListHMDofCo = new List<HangCoDetail>();

        public static Company.KDT.SHARE.Components.Common.COBoSung LoadCoBoSung(long coID)
        {
            List<Company.KDT.SHARE.Components.Common.COBoSung> COBoSungCollection = new List<Company.KDT.SHARE.Components.Common.COBoSung>();
            COBoSungCollection = (List<Company.KDT.SHARE.Components.Common.COBoSung>)Company.KDT.SHARE.Components.Common.COBoSung.SelectCollectionDynamic("CoID=" + coID, "");

            return COBoSungCollection.Count != 0 ? COBoSungCollection[0] : null;
        }
        public void LoadListHMDofCo()
        {
            ListHMDofCo = HangCoDetail.SelectCollectionDynamic("Co_ID=" + this.ID, "");
        }
        public void DeleteCoFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (HangCoDetail hangCo in ListHMDofCo)
                        {
                            hangCo.Delete(transaction);
                        }

                        //Xoa CO Bo sung
                        Company.KDT.SHARE.Components.Common.COBoSung.DeleteBy_COID(this.ID);

                        this.Delete(transaction);
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }
        public bool InsertUpdateFull()
        {
            bool ret = false;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    long id = this.ID;
                    try
                    {
                        if (this.ID == 0)
                            this.Insert(transaction);
                        else
                            this.Update(transaction);

                        //Them CO bo sung
                        Company.KDT.SHARE.Components.Common.COBoSung coBS = LoadCoBoSung(this.ID);
                        if (coBS == null)
                        {
                            Company.KDT.SHARE.Components.Common.COBoSung.InsertCOBoSung(this.ID, this.TenCangXepHang, this.TenCangDoHang);
                        }
                        else
                        {
                            Company.KDT.SHARE.Components.Common.COBoSung.UpdateCOBoSung(coBS.ID, this.ID, this.TenCangXepHang, this.TenCangDoHang);
                        }

                        foreach (HangCoDetail hangCo in this.ListHMDofCo)
                        {
                            hangCo.Co_ID = this.ID;
                            if (id == 0)
                                hangCo.ID = 0;

                            if (hangCo.ID == 0)
                                hangCo.Insert(transaction);
                            else
                                hangCo.Update(transaction);
                        }
                        transaction.Commit();
                        ret = true;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Lỗi thực hiện lưu dữ liệu: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        public DataTable ConvertListToDataSet(DataTable dsHMD)
        {
            if (ListHMDofCo != null && ListHMDofCo.Count == 0)
            {
                LoadListHMDofCo();
            }

            DataTable tmp = dsHMD.Copy();
            tmp.Rows.Clear();
            tmp.Columns.Add("GhiChu");
            dsHMD.Columns.Add("GhiChu");
            dsHMD.Columns.Add("HMD_ID");
            dsHMD.Columns.Add("MaChuyenNganh");
            dsHMD.Columns.Add("MaNguyenTe");

            dsHMD.Columns.Add("LoaiKien");
            dsHMD.Columns.Add("SoHoaDon");
            dsHMD.Columns.Add("SoHieuKien");
            dsHMD.Columns.Add("NgayHoaDon");


            tmp.Columns["ID"].ColumnName = "HMD_ID";
            tmp.Columns["HMD_ID"].Caption = "HMD_ID";
            tmp.Columns.Add("ID");
            tmp.Columns.Add("MaChuyenNganh");
            tmp.Columns.Add("MaNguyenTe");
            tmp.Columns.Add("LoaiKien");
            tmp.Columns.Add("SoHoaDon");
            tmp.Columns.Add("SoHieuKien");
            tmp.Columns.Add("NgayHoaDon");
            foreach (HangCoDetail item in ListHMDofCo)
            {
                DataRow[] row = dsHMD.Select("ID=" + item.HMD_ID);
                if (row != null && row.Length != 0)
                {
                    row[0]["HMD_ID"] = row[0]["ID"];
                    row[0]["MaChuyenNganh"] = item.MaChuyenNganh;
                    row[0]["MaNguyenTe"] = item.MaNguyenTe;
                    row[0]["ID"] = item.ID.ToString();

                    row[0]["SoThuTuHang"] = item.SoThuTuHang;
                    row[0]["MaHS"] = item.MaHS;
                    row[0]["MaPhu"] = item.MaPhu;
                    row[0]["TenHang"] = item.TenHang;
                    row[0]["NuocXX_ID"] = item.NuocXX_ID;
                    row[0]["DVT_ID"] = item.DVT_ID;
                    row[0]["SoLuong"] = item.SoLuong;
                    row[0]["DonGiaKB"] = item.DonGiaKB;
                    row[0]["TriGiaKB"] = item.TriGiaKB;
                    #region V3
                    row[0]["TrongLuong"] = item.TrongLuong;
                    row[0]["LoaiKien"] = item.LoaiKien;
                    row[0]["SoHieuKien"] = item.SoHieuKien;
                    row[0]["SoHoaDon"] = item.SoHoaDon;
                    row[0]["NgayHoaDon"] = item.NgayHoaDon.ToString("dd/MM/yyyy");
                    #endregion V3
                    tmp.ImportRow(row[0]);
                }
            }
            return tmp;

        }

        //         public DataTable ConvertListToDataSet(DataTable dsHMD)
        //         {
        //             DataTable tmp = dsHMD.Copy();
        //             tmp.Rows.Clear();
        //             tmp.Columns.Add("GhiChu");
        //             dsHMD.Columns.Add("GhiChu");
        //             dsHMD.Columns.Add("HMD_ID");
        //             dsHMD.Columns.Add("MaChuyenNganh");
        //             dsHMD.Columns.Add("MaNguyenTe");
        // 
        //             dsHMD.Columns.Add("LoaiKien");
        //             dsHMD.Columns.Add("SoHoaDon");
        //             dsHMD.Columns.Add("SoHieuKien");
        //             dsHMD.Columns.Add("NgayHoaDon");
        // 
        // 
        //             tmp.Columns["ID"].ColumnName = "HMD_ID";
        //             tmp.Columns["HMD_ID"].Caption = "HMD_ID";
        //             tmp.Columns.Add("ID");
        //             tmp.Columns.Add("MaChuyenNganh");
        //             tmp.Columns.Add("MaNguyenTe");
        //             tmp.Columns.Add("LoaiKien");
        //             tmp.Columns.Add("SoHoaDon");
        //             tmp.Columns.Add("SoHieuKien");
        //             tmp.Columns.Add("NgayHoaDon");
        //             foreach (HangCoDetail item in ListHMDofCo)
        //             {
        //                 DataRow[] row = dsHMD.Select("ID=" + item.HMD_ID);
        //                 if (row != null && row.Length != 0)
        //                 {
        //                     row[0]["HMD_ID"] = row[0]["ID"];
        //                     row[0]["MaChuyenNganh"] = item.MaChuyenNganh;
        //                     row[0]["MaNguyenTe"] = item.MaNguyenTe;
        //                     row[0]["ID"] = item.ID.ToString();
        // 
        //                     row[0]["SoThuTuHang"] = item.SoThuTuHang;
        //                     row[0]["MaHS"] = item.MaHS;
        //                     row[0]["MaPhu"] = item.MaPhu;
        //                     row[0]["TenHang"] = item.TenHang;
        //                     row[0]["NuocXX_ID"] = item.NuocXX_ID;
        //                     row[0]["DVT_ID"] = item.DVT_ID;
        //                     row[0]["SoLuong"] = item.SoLuong;
        //                     row[0]["DonGiaKB"] = item.DonGiaKB;
        //                     row[0]["TriGiaKB"] = item.TriGiaKB;
        //                     #region V3
        //                     row[0]["TrongLuong"] = item.TrongLuong;
        //                     row[0]["LoaiKien"] = item.LoaiKien;
        //                     row[0]["SoHieuKien"] = item.SoHieuKien;
        //                     row[0]["SoHoaDon"] = item.SoHoaDon;
        //                     row[0]["NgayHoaDon"] = item.NgayHoaDon.ToString("dd/MM/yyyy");
        //                     #endregion V3
        //                     tmp.ImportRow(row[0]);
        //                 }
        //             }
        //             return tmp;
        // 
        //         }
        //         public CO LoadCO(long iD)
        //         {
        //             const string spName = "[dbo].[p_KDT_CO_Load]";
        //             SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //             SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
        // 
        //             db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, iD);
        //             CO entity = null;
        //             SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
        //             if (reader.Read())
        //             {
        //                 entity = new CO();
        //                 if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("SoCO"))) entity.SoCO = reader.GetString(reader.GetOrdinal("SoCO"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("NgayCO"))) entity.NgayCO = reader.GetDateTime(reader.GetOrdinal("NgayCO"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("ToChucCap"))) entity.ToChucCap = reader.GetString(reader.GetOrdinal("ToChucCap"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("NuocCapCO"))) entity.NuocCapCO = reader.GetString(reader.GetOrdinal("NuocCapCO"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("MaNuocXKTrenCO"))) entity.MaNuocXKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocXKTrenCO"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("MaNuocNKTrenCO"))) entity.MaNuocNKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocNKTrenCO"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiXK"))) entity.TenDiaChiNguoiXK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiXK"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiNK"))) entity.TenDiaChiNguoiNK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiNK"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("LoaiCO"))) entity.LoaiCO = reader.GetString(reader.GetOrdinal("LoaiCO"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoTaChiTiet"))) entity.ThongTinMoTaChiTiet = reader.GetString(reader.GetOrdinal("ThongTinMoTaChiTiet"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("NguoiKy"))) entity.NguoiKy = reader.GetString(reader.GetOrdinal("NguoiKy"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("NoCo"))) entity.NoCo = reader.GetInt32(reader.GetOrdinal("NoCo"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanNop"))) entity.ThoiHanNop = reader.GetDateTime(reader.GetOrdinal("ThoiHanNop"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
        // 
        //                 if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("NgayKhoiHanh"))) entity.NgayKhoiHanh = reader.GetDateTime(reader.GetOrdinal("NgayKhoiHanh"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("CangXepHang"))) entity.CangXepHang = reader.GetString(reader.GetOrdinal("CangXepHang"));
        //                 if (!reader.IsDBNull(reader.GetOrdinal("CangDoHang"))) entity.CangDoHang = reader.GetString(reader.GetOrdinal("CangDoHang"));
        //             }
        //             reader.Close();
        //             return entity;
        //         }

        public static CO Load(string maDoanhNghiep, string soCO, DateTime ngayCO, string loaiCO, long idTKMD, SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_CO_LoadBy]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoCO", SqlDbType.VarChar, soCO);
            db.AddInParameter(dbCommand, "@NgayCO", SqlDbType.DateTime, ngayCO);
            db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.VarChar, loaiCO);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, idTKMD);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);

            CO entity = null;
            SqlDataReader reader = null;

            if (transaction != null)
                reader = (SqlDataReader)db.ExecuteReader(dbCommand, transaction);
            else
                reader = (SqlDataReader)db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                entity = new CO();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoCO"))) entity.SoCO = reader.GetString(reader.GetOrdinal("SoCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayCO"))) entity.NgayCO = reader.GetDateTime(reader.GetOrdinal("NgayCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToChucCap"))) entity.ToChucCap = reader.GetString(reader.GetOrdinal("ToChucCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocCapCO"))) entity.NuocCapCO = reader.GetString(reader.GetOrdinal("NuocCapCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocXKTrenCO"))) entity.MaNuocXKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocXKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocNKTrenCO"))) entity.MaNuocNKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocNKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiXK"))) entity.TenDiaChiNguoiXK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiNK"))) entity.TenDiaChiNguoiNK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiCO"))) entity.LoaiCO = reader.GetString(reader.GetOrdinal("LoaiCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoTaChiTiet"))) entity.ThongTinMoTaChiTiet = reader.GetString(reader.GetOrdinal("ThongTinMoTaChiTiet"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiKy"))) entity.NguoiKy = reader.GetString(reader.GetOrdinal("NguoiKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoCo"))) entity.NoCo = reader.GetInt32(reader.GetOrdinal("NoCo"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanNop"))) entity.ThoiHanNop = reader.GetDateTime(reader.GetOrdinal("ThoiHanNop"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKhoiHanh"))) entity.NgayKhoiHanh = reader.GetDateTime(reader.GetOrdinal("NgayKhoiHanh"));
                if (!reader.IsDBNull(reader.GetOrdinal("CangXepHang"))) entity.CangXepHang = reader.GetString(reader.GetOrdinal("CangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CangDoHang"))) entity.CangDoHang = reader.GetString(reader.GetOrdinal("CangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiXK"))) entity.MaNguoiXK = reader.GetString(reader.GetOrdinal("MaNguoiXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNK"))) entity.MaNguoiNK = reader.GetString(reader.GetOrdinal("MaNguoiNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HamLuongXuatXu"))) entity.HamLuongXuatXu = reader.GetDouble(reader.GetOrdinal("HamLuongXuatXu"));
                if (!reader.IsDBNull(reader.GetOrdinal("MoTaHangHoa"))) entity.MoTaHangHoa = reader.GetString(reader.GetOrdinal("MoTaHangHoa"));

            }
            reader.Close();

            Company.KDT.SHARE.Components.Common.COBoSung coBS = LoadCoBoSung(entity.ID);
            if (coBS != null)
            {
                entity.TenCangDoHang = coBS.TenCangDoHang;
                entity.TenCangXepHang = coBS.TenCangXepHang;
            }

            return entity;
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_CO_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoCO", SqlDbType.VarChar, SoCO);
            db.AddInParameter(dbCommand, "@NgayCO", SqlDbType.DateTime, NgayCO.Year <= 1753 ? DBNull.Value : (object)NgayCO);
            db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
            db.AddInParameter(dbCommand, "@NuocCapCO", SqlDbType.VarChar, NuocCapCO);
            db.AddInParameter(dbCommand, "@MaNuocXKTrenCO", SqlDbType.VarChar, MaNuocXKTrenCO);
            db.AddInParameter(dbCommand, "@MaNuocNKTrenCO", SqlDbType.VarChar, MaNuocNKTrenCO);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiXK", SqlDbType.NVarChar, TenDiaChiNguoiXK);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiNK", SqlDbType.NVarChar, TenDiaChiNguoiNK);
            db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.VarChar, LoaiCO);
            db.AddInParameter(dbCommand, "@ThongTinMoTaChiTiet", SqlDbType.NVarChar, ThongTinMoTaChiTiet);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@NguoiKy", SqlDbType.NVarChar, NguoiKy);
            db.AddInParameter(dbCommand, "@NoCo", SqlDbType.Int, NoCo);
            db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year <= 1753 ? DBNull.Value : (object)ThoiHanNop);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
            db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object)NgayHetHan);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year <= 1753 ? DBNull.Value : (object)NgayKhoiHanh);
            db.AddInParameter(dbCommand, "@CangXepHang", SqlDbType.NVarChar, CangXepHang);
            db.AddInParameter(dbCommand, "@CangDoHang", SqlDbType.NVarChar, CangDoHang);
            db.AddInParameter(dbCommand, "@MaNguoiXK", SqlDbType.VarChar, MaNguoiXK);
            db.AddInParameter(dbCommand, "@MaNguoiNK", SqlDbType.VarChar, MaNguoiNK);
            db.AddInParameter(dbCommand, "@HamLuongXuatXu", SqlDbType.Float, HamLuongXuatXu);
            db.AddInParameter(dbCommand, "@MoTaHangHoa", SqlDbType.NVarChar, MoTaHangHoa);
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_CO_Update]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoCO", SqlDbType.VarChar, SoCO);
            db.AddInParameter(dbCommand, "@NgayCO", SqlDbType.DateTime, NgayCO.Year == 1753 ? DBNull.Value : (object)NgayCO);
            db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
            db.AddInParameter(dbCommand, "@NuocCapCO", SqlDbType.VarChar, NuocCapCO);
            db.AddInParameter(dbCommand, "@MaNuocXKTrenCO", SqlDbType.VarChar, MaNuocXKTrenCO);
            db.AddInParameter(dbCommand, "@MaNuocNKTrenCO", SqlDbType.VarChar, MaNuocNKTrenCO);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiXK", SqlDbType.NVarChar, TenDiaChiNguoiXK);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiNK", SqlDbType.NVarChar, TenDiaChiNguoiNK);
            db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.VarChar, LoaiCO);
            db.AddInParameter(dbCommand, "@ThongTinMoTaChiTiet", SqlDbType.NVarChar, ThongTinMoTaChiTiet);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@NguoiKy", SqlDbType.NVarChar, NguoiKy);
            db.AddInParameter(dbCommand, "@NoCo", SqlDbType.Int, NoCo);
            db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year == 1753 ? DBNull.Value : (object)ThoiHanNop);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
            db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year == 1753 ? DBNull.Value : (object)NgayHetHan);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year == 1753 ? DBNull.Value : (object)NgayKhoiHanh);
            db.AddInParameter(dbCommand, "@CangXepHang", SqlDbType.NVarChar, CangXepHang);
            db.AddInParameter(dbCommand, "@CangDoHang", SqlDbType.NVarChar, CangDoHang);
            db.AddInParameter(dbCommand, "@MaNguoiXK", SqlDbType.VarChar, MaNguoiXK);
            db.AddInParameter(dbCommand, "@MaNguoiNK", SqlDbType.VarChar, MaNguoiNK);
            db.AddInParameter(dbCommand, "@HamLuongXuatXu", SqlDbType.Float, HamLuongXuatXu);
            db.AddInParameter(dbCommand, "@MoTaHangHoa", SqlDbType.NVarChar, MoTaHangHoa);
            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public static bool checkSoCOExit(string soCO, string MaDoanhNghiep, long TKMD_ID, long coID)
        {
            string sql = "select SoCO from t_KDT_CO where SoCO=@SoCO and MaDoanhNghiep=@MaDoanhNghiep and TKMD_ID=@TKMD_ID and ID!=" + coID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbcommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbcommand, "@SoCO", SqlDbType.VarChar, soCO);
            db.AddInParameter(dbcommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbcommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            object o = db.ExecuteScalar(dbcommand);
            return o != null;
        }

        public static List<CO> SelectListCOByMaDanhNghiepAndKhacTKMD(long TKMD_ID, string MaDoanhNghiep)
        {
            List<CO> collection = new List<CO>();
            string sql = "select * from t_KDT_CO where TKMD_ID<>@TKMD_ID and MaDoanhNghiep=@MaDoanhNghiep";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbcommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbcommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbcommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbcommand);
            while (reader.Read())
            {
                CO entity = new CO();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoCO"))) entity.SoCO = reader.GetString(reader.GetOrdinal("SoCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayCO"))) entity.NgayCO = reader.GetDateTime(reader.GetOrdinal("NgayCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToChucCap"))) entity.ToChucCap = reader.GetString(reader.GetOrdinal("ToChucCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocCapCO"))) entity.NuocCapCO = reader.GetString(reader.GetOrdinal("NuocCapCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocXKTrenCO"))) entity.MaNuocXKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocXKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocNKTrenCO"))) entity.MaNuocNKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocNKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiXK"))) entity.TenDiaChiNguoiXK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiNK"))) entity.TenDiaChiNguoiNK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiCO"))) entity.LoaiCO = reader.GetString(reader.GetOrdinal("LoaiCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoTaChiTiet"))) entity.ThongTinMoTaChiTiet = reader.GetString(reader.GetOrdinal("ThongTinMoTaChiTiet"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiKy"))) entity.NguoiKy = reader.GetString(reader.GetOrdinal("NguoiKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoCo"))) entity.NoCo = reader.GetInt32(reader.GetOrdinal("NoCo"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanNop"))) entity.ThoiHanNop = reader.GetDateTime(reader.GetOrdinal("ThoiHanNop"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKhoiHanh"))) entity.NgayKhoiHanh = reader.GetDateTime(reader.GetOrdinal("NgayKhoiHanh"));
                if (!reader.IsDBNull(reader.GetOrdinal("CangXepHang"))) entity.CangXepHang = reader.GetString(reader.GetOrdinal("CangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CangDoHang"))) entity.CangDoHang = reader.GetString(reader.GetOrdinal("CangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiXK"))) entity.MaNguoiXK = reader.GetString(reader.GetOrdinal("MaNguoiXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNK"))) entity.MaNguoiNK = reader.GetString(reader.GetOrdinal("MaNguoiNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HamLuongXuatXu"))) entity.HamLuongXuatXu = reader.GetDouble(reader.GetOrdinal("HamLuongXuatXu"));
                if (!reader.IsDBNull(reader.GetOrdinal("MoTaHangHoa"))) entity.MoTaHangHoa = reader.GetString(reader.GetOrdinal("MoTaHangHoa"));
                collection.Add(entity);
            }
            reader.Close();

            foreach (CO item in collection)
            {
                Company.KDT.SHARE.Components.Common.COBoSung coBS = LoadCoBoSung(item.ID);
                if (coBS != null)
                {
                    item.TenCangDoHang = coBS.TenCangDoHang;
                    item.TenCangXepHang = coBS.TenCangXepHang;
                }
            }

            return collection;
        }

        public static bool KiemTraChungTuCoBoSung(List<CO> collections)
        {
            foreach (CO item in collections)
            {
                if (item.LoaiKB == 1)
                {
                    return true;
                }
            }

            return false;
        }


        public static XmlNode ConvertCollectionCOToXML(XmlDocument doc, List<CO> list, long p)
        {
            throw new NotImplementedException();
        }
    }
}