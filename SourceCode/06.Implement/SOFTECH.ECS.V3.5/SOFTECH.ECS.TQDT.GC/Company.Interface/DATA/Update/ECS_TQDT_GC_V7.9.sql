/****** Object:  Table [dbo].[t_KDT_ToKhaiMauDichBoSung]    Script Date: 02/06/2013 11:32:22 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_ToKhaiChuyenTiepBoSung]') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[t_KDT_ToKhaiChuyenTiepBoSung](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKCT_ID] [bigint] NOT NULL,
	[PhienBan] [int] NOT NULL,
	[ChuKySo] [bit] NOT NULL,
	[GhiChu] [nvarchar](250) NULL,
 CONSTRAINT [PK_t_KDT_ToKhaiChuyentiep_BoSung] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END

GO


/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Delete]    Script Date: 03/05/2013 16:24:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_DeleteBy_TKCT_ID]    Script Date: 03/05/2013 16:24:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiChuyenTiepBoSung_DeleteBy_TKCT_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_DeleteBy_TKCT_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_DeleteDynamic]    Script Date: 03/05/2013 16:24:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiChuyenTiepBoSung_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Insert]    Script Date: 03/05/2013 16:24:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_InsertUpdate]    Script Date: 03/05/2013 16:24:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiChuyenTiepBoSung_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_InsertUpdate_ByTKCT]    Script Date: 03/05/2013 16:24:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiChuyenTiepBoSung_InsertUpdate_ByTKCT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_InsertUpdate_ByTKCT]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Load]    Script Date: 03/05/2013 16:24:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectAll]    Script Date: 03/05/2013 16:24:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectBy_TKCT_ID]    Script Date: 03/05/2013 16:24:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectBy_TKCT_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectBy_TKCT_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectDynamic]    Script Date: 03/05/2013 16:24:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Update]    Script Date: 03/05/2013 16:24:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Delete]    Script Date: 03/05/2013 16:24:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ToKhaiChuyenTiepBoSung]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_DeleteBy_TKCT_ID]    Script Date: 03/05/2013 16:24:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_DeleteBy_TKCT_ID]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_DeleteBy_TKCT_ID]
	@TKCT_ID bigint
AS

DELETE FROM [dbo].[t_KDT_ToKhaiChuyenTiepBoSung]
WHERE
	[TKCT_ID] = @TKCT_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_DeleteDynamic]    Script Date: 03/05/2013 16:24:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ToKhaiChuyenTiepBoSung] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Insert]    Script Date: 03/05/2013 16:24:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Insert]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Insert]
	@TKCT_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ToKhaiChuyenTiepBoSung]
(
	[TKCT_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu]
)
VALUES 
(
	@TKCT_ID,
	@PhienBan,
	@ChuKySo,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_InsertUpdate]    Script Date: 03/05/2013 16:24:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_InsertUpdate]
	@ID bigint,
	@TKCT_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ToKhaiChuyenTiepBoSung] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ToKhaiChuyenTiepBoSung] 
		SET
			[TKCT_ID] = @TKCT_ID,
			[PhienBan] = @PhienBan,
			[ChuKySo] = @ChuKySo,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ToKhaiChuyenTiepBoSung]
		(
			[TKCT_ID],
			[PhienBan],
			[ChuKySo],
			[GhiChu]
		)
		VALUES 
		(
			@TKCT_ID,
			@PhienBan,
			@ChuKySo,
			@GhiChu
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_InsertUpdate_ByTKCT]    Script Date: 03/05/2013 16:24:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_InsertUpdate_ByTKCT]  
 @TKCT_ID bigint,  
 @PhienBan int,  
 @ChuKySo bit,  
 @GhiChu nvarchar(250)  
AS  
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ToKhaiChuyenTiepBoSung] WHERE [TKCT_ID] = @TKCT_ID)  
 BEGIN  
  UPDATE  
   [dbo].[t_KDT_ToKhaiChuyenTiepBoSung]   
  SET  
   [TKCT_ID] = @TKCT_ID,  
   [PhienBan] = @PhienBan,  
   [ChuKySo] = @ChuKySo,  
   [GhiChu] = @GhiChu  
  WHERE  
   [TKCT_ID] = @TKCT_ID 
 END  
ELSE  
 BEGIN  
    
  INSERT INTO [dbo].[t_KDT_ToKhaiChuyenTiepBoSung]  
  (  
   [TKCT_ID],  
   [PhienBan],  
   [ChuKySo],  
   [GhiChu]  
  )  
  VALUES   
  (  
   @TKCT_ID,  
   @PhienBan,  
   @ChuKySo,  
   @GhiChu  
  )    
 END  


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Load]    Script Date: 03/05/2013 16:24:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Load]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKCT_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu]
FROM
	[dbo].[t_KDT_ToKhaiChuyenTiepBoSung]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectAll]    Script Date: 03/05/2013 16:24:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectAll]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKCT_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu]
FROM
	[dbo].[t_KDT_ToKhaiChuyenTiepBoSung]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectBy_TKCT_ID]    Script Date: 03/05/2013 16:24:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectBy_TKCT_ID]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectBy_TKCT_ID]
	@TKCT_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKCT_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu]
FROM
	[dbo].[t_KDT_ToKhaiChuyenTiepBoSung]
WHERE
	[TKCT_ID] = @TKCT_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectDynamic]    Script Date: 03/05/2013 16:24:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKCT_ID],
	[PhienBan],
	[ChuKySo],
	[GhiChu]
FROM [dbo].[t_KDT_ToKhaiChuyenTiepBoSung] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Update]    Script Date: 03/05/2013 16:24:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Update]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiChuyenTiepBoSung_Update]
	@ID bigint,
	@TKCT_ID bigint,
	@PhienBan int,
	@ChuKySo bit,
	@GhiChu nvarchar(250)
AS

UPDATE
	[dbo].[t_KDT_ToKhaiChuyenTiepBoSung]
SET
	[TKCT_ID] = @TKCT_ID,
	[PhienBan] = @PhienBan,
	[ChuKySo] = @ChuKySo,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID


GO



UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '7.9'
      ,[Date] = getdate()
      ,[Notes] = ''
      GO	


