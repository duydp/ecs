﻿using System;
using System.Xml;
using System.Windows.Forms;
#if KD_V3 || KD_V4
using Company.KD.BLL.SXXK;
using Company.KD.BLL.SXXK.ToKhai;
#elif SXXK_V3
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
#elif GV_V3
using Company.GC.BLL.SXXK;
using Company.GC.BLL.SXXK.ToKhai;
#endif
using System.Threading;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Net.Sockets;

namespace Company.Interface
{
    public partial class ThongTinDNAndHQForm : BaseForm
    {
        string user = "";
        string pw = "";

        public static int versionHD = 0;

        public ThongTinDNAndHQForm()
        {
            InitializeComponent();
        }

        private void SendForm_Load(object sender, EventArgs e)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(Application.StartupPath + "\\Config.xml");
                XmlNode node = doc.SelectSingleNode("Root/Type");
                versionHD = Convert.ToInt32(node.InnerText);
                //Chi hien thi Tab DBDL cho phia Doanh nghiep - Server





                uiTabDBDL.TabVisible = !GlobalSettings.IsDaiLy;
                cbFont.SelectedValue = Company.KDT.SHARE.Components.Globals.FontName;
                string MaDN = GlobalSettings.MA_DON_VI;
                Company.KDT.SHARE.Components.HeThongPhongKhai htpk = new Company.KDT.SHARE.Components.HeThongPhongKhai();

                //Tam go bo su kien tren control
                //chkCKS.CheckedChanged -= new EventHandler(chkCKS_CheckedChanged);

                #region Cau hinh Dai ly

                if (versionHD == 1)
                {
                    try

                    {
                        //Thông tin doanh nghiệp
                        txtMaDN.ReadOnly = true;
                        txtMaDN.Text = MaDN;
                        txtTenDN.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "CauHinh").TenDoanhNghiep.ToString();
                        txtDiaChi.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "DIA_CHI").Value_Config.ToString();
                        txtDienThoaiDN.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "ĐIEN_THOAI").Value_Config.ToString();
                        txtSoFaxDN.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "SO_FAX").Value_Config.ToString();
                        txtNguoiLienHeDN.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "NGUOI_LIEN_HE").Value_Config.ToString();
                        txtChucVu.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "CHUC_VU").Value_Config.ToString();
                        txtMailDoanhNghiep.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MailDoanhNghiep").Value_Config.ToString();
                        txtMaMid.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MaMID").Value_Config.ToString();

                        //Thông tin hải quan
                        txtMaCuc.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MA_CUC_HAI_QUAN").Value_Config.ToString();
                        donViHaiQuanControl1.Ma = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MA_HAI_QUAN").Value_Config.ToString();
                        txtTenCucHQ.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "TEN_HAI_QUAN").Value_Config.ToString();
                        txtTenNganHQ.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "TEN_HAI_QUAN_NGAN").Value_Config.ToString();
                        txtMailHaiQuan.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "MailHaiQuan").Value_Config.ToString();

                        //Thông tin kết nối service hải quan
                        txtDiaChiHQ.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "DIA_CHI_HQ").Value_Config.ToString();
                        txtTenDichVu.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "TEN_DICH_VU").Value_Config.ToString();
                       // chkCKS.Checked = Convert.ToBoolean(Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "CHU_KY_SO").Value_Config.ToString());
                        try
                        {

                            chkSendV4.Checked = Convert.ToBoolean(Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "SendV4").Value_Config.ToString());
                            chkKhaiBaoV5.Checked = Convert.ToBoolean(Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "SendV5").Value_Config.ToString());
                            chkSendV4_CheckedChanged(null, null);

                            //Terminal
                            txtVNACCTerminalID.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "VNACC_TerminalID").Value_Config.ToString();
                            txtVNACCTerminalPass.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "VNACC_TerminalPass").Value_Config.ToString();
                            txtVNACCUserID.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "VNACC_UserID").Value_Config.ToString();
                            txtVNACCUserCode.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "VNACC_UserCode").Value_Config.ToString();
                            txtVNACCUserPass.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "VNACC_UserPass").Value_Config.ToString();
                            txtVNACCAddress.Text = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "VNACCAddress").Value_Config.ToString();

                            //Cap nhat bien toan cuc VNACC cho chuong trinh
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalID = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "VNACC_TerminalID").Value_Config.ToString();
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalAccessKey = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "VNACC_TerminalPass").Value_Config.ToString();
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_ID = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "VNACC_UserID").Value_Config.ToString();
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Ma = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "VNACC_UserCode").Value_Config.ToString();
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Pass = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "VNACC_UserPass").Value_Config.ToString();
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.Url = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDN, "VNACCAddress").Value_Config.ToString();
                        }
                        catch (System.Exception ex)
                        {
                            chkSendV4.Checked = false;
                            chkKhaiBaoV5.Checked = false;
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }

                #endregion

                #region Cau hinh Doanh nghiep

                else
                {
                    //Thông tin doanh nghiệp
                    txtMaDN.Text = GlobalSettings.MA_DON_VI;
                    txtTenDN.Text = GlobalSettings.TEN_DON_VI;
                    txtDiaChi.Text = GlobalSettings.DIA_CHI;
                    txtDienThoaiDN.Text = GlobalSettings.SoDienThoaiDN;
                    txtSoFaxDN.Text = GlobalSettings.SoFaxDN;
                    txtNguoiLienHeDN.Text = GlobalSettings.NguoiLienHe;
                    txtChucVu.Text = GlobalSettings.ChucVu;
                    txtMailDoanhNghiep.Text = GlobalSettings.MailDoanhNghiep;
                    txtMaMid.Text = GlobalSettings.MaMID;
                    //thông tin Hải quan
                    txtMaCuc.Text = GlobalSettings.MA_CUC_HAI_QUAN.Trim();
                    donViHaiQuanControl1.Ma = GlobalSettings.MA_HAI_QUAN.Trim();
                    donViHaiQuanControl1.ReadOnly = false;
                    txtTenCucHQ.Text = GlobalSettings.TEN_CUC_HAI_QUAN;
                    txtTenNganHQ.Text = GlobalSettings.TEN_HAI_QUAN_NGAN;
                    txtMailHaiQuan.Text = GlobalSettings.MailHaiQuan;
                    //Thông tin kết nối service hải quan
                    txtDiaChiHQ.Text = GlobalSettings.DiaChiWS_Host;
                    txtTenDichVu.Text = GlobalSettings.DiaChiWS_Name;
                    //chkCKS.Checked = Company.KDT.SHARE.Components.Globals.SuDungHttps;
                    chkSendV4.Checked = GlobalSettings.SendV4;
                    chkKhaiBaoV5.Checked = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS;
                    rbtIsKTX.Checked = Company.KDT.SHARE.Components.Globals.IsKTX;
                }

                #endregion

                txtThongBaoHetHan.Text = GlobalSettings.ThongBaoHetHan.ToString();
                txtSoTienKhoanTKN.Text = GlobalSettings.SoTienKhoanTKN.ToString();
                txtSoTienKhoanTKX.Text = GlobalSettings.SoTienKhoanTKX.ToString();

                //chkOnlyMe.Enabled = GlobalSettings.IsDaiLy;
                //chkOnlyMe.Checked = GlobalSettings.IsOnlyMe;

                CultureInfo cultureVN = new CultureInfo("vi-VN");
                if (Thread.CurrentThread.CurrentCulture.Equals(cultureVN))
                {
                    opVietNam.Checked = true;
                }
                else
                    opTiengAnh.Checked = true;

#if (KD_V3 || GC_V3)
                txtSoTienKhoanTKN.Enabled = false;
                txtSoTienKhoanTKX.Enabled = false;
                txtThongBaoHetHan.Enabled = false;
#elif (KD_V3 || GC_V3 || KD_V4 || GC_V4)
                chkLaDNCX.Enabled = false;
#elif (SXXK_V3 || SXXK_V4)
                chkLaDNCX.Enabled = true;
#endif
                chkLaDNCX.Checked = Company.KDT.SHARE.Components.Globals.LaDNCX;

                //if (!GlobalSettings.IsDaiLy)
                //{
                //    //Cau hinh dai ly
                //    Company.KDT.SHARE.Components.ISyncData myService = Company.KDT.SHARE.Components.WebService.SyncService();
                //    string check = myService.CheckCauHinhDL(GlobalSettings.MA_DON_VI);
                //    GlobalSettings.SOTOKHAI_DONGBO = 0;

                //    if (int.TryParse(check, out GlobalSettings.SOTOKHAI_DONGBO))
                //    {
                //        GlobalSettings.SOTOKHAI_DONGBO = Convert.ToInt32(check);
                //    }
                //    else
                //    {
                //        if (check == "NULL")
                //            ShowMessage("Doanh nghiệp chưa cấu hình giới hạn số tờ khai cho đại lý", false);
                //        else
                //            ShowMessage("Lỗi kết nối kiểm tra cấu hình: " + check, false);
                //        GlobalSettings.ISKHAIBAO = false;
                //    }
                //    txtSoTKDBDL.Text = GlobalSettings.SOTOKHAI_DONGBO.ToString();
                //}

                //Bat lai su kien tren control
                //chkCKS.CheckedChanged += new EventHandler(chkCKS_CheckedChanged);

                rbtIsKTX.Checked = Company.KDT.SHARE.Components.Globals.IsKTX;
                rbtIsTQDT.Checked = !Company.KDT.SHARE.Components.Globals.IsKTX;

                donViHaiQuanControl1.Leave += new EventHandler(donViHaiQuanControl1_Leave);

                //Dong bo du lieu
                chkSoTK.Checked = GlobalSettings.SuDungSoTK;
                txtSoTKDBDL.Text = GlobalSettings.SoTK.ToString();
                chkDongBoChungTuDinhkem.Checked = GlobalSettings.DongBoChungTuDinhKem;

                //Lay thong tin thiet lap Terminal
                GetTerminal();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Company.Interface.ValidateControl.ValidateNull(donViHaiQuanControl1, errorProvider1, "Mã chi cục Hải quan"))
                {
                    uiTab1.TabPages[0].Selected = true;
                    return;
                }                
                containerValidator1.Validate();
                if (!containerValidator1.IsValid)
                {
                    uiTab1.TabPages[1].Selected = true;
                    txtMaDN.Focus();
                    return;
                }

                //if (MainForm.isUseSettingVNACC)
                //{
                //    //TODO: Kiem tra thong tin nhap VNACC
                //    if (!Company.Interface.ValidateControl.ValidateNull(txtVNACCTerminalID, errorProvider1, "Terminal ID"))
                //    {
                //        uiTab1.TabPages[2].Selected = true;
                //        txtVNACCTerminalID.Focus();
                //        return;
                //    }
                //    else if (!Company.Interface.ValidateControl.ValidateNull(txtVNACCUserID, errorProvider1, "User ID"))
                //    {
                //        uiTab1.TabPages[2].Selected = true;
                //        txtVNACCUserID.Focus();
                //        return;
                //    }
                //    else if (!Company.Interface.ValidateControl.ValidateNull(txtVNACCUserCode, errorProvider1, "User Code"))
                //    {
                //        uiTab1.TabPages[2].Selected = true;
                //        txtVNACCUserCode.Focus();
                //        return;
                //    }
                //}

                if (!Company.Interface.ValidateControl.ValidateNull(txtDienThoaiDN, errorProvider1, "Điện thoại"))
                    return;
                if (!Company.Interface.ValidateControl.ValidateNull(txtMailDoanhNghiep, errorProvider1, "Email (Thư điện tử)"))
                    return;
                Company.KDT.SHARE.Components.Globals.FontName = cbFont.SelectedValue.ToString();
                #region OldCode


                //Hungtq 14/01/2011. Luu cau hinh
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DIA_CHI", txtDiaChi.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_DON_VI", txtMaDN.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_HAI_QUAN", donViHaiQuanControl1.Ma.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_HAI_QUAN", donViHaiQuanControl1.Ten);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_HAI_QUAN_NGAN", txtTenNganHQ.Text);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_DON_VI", txtTenDN.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_CUC_HAI_QUAN", txtMaCuc.Text);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_CUC_HAI_QUAN", txtTenCucHQ.Text);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MailHaiQuan", txtMailHaiQuan.Text);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MaMID", txtMaMid.Text);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ThongBaoHetHan", txtThongBaoHetHan.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SoTienKhoanTKN", txtSoTienKhoanTKN.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SoTienKhoanTKX", txtSoTienKhoanTKX.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MailDoanhNghiep", txtMailDoanhNghiep.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FromMail", txtMailDoanhNghiep.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FontName", Company.KDT.SHARE.Components.Globals.FontName);

                #endregion
                if (versionHD == 0)
                {
                    /*DATLMQ update Lưu cấu hình vào file config 18/01/2011.*/
                    XmlDocument doc = new XmlDocument();
                    string path = Company.KDT.SHARE.Components.Globals.GetPathProgram() + "\\ConfigDoanhNghiep";
                    //Hungtq update 28/01/2011.
                    string fileName = Company.KDT.SHARE.Components.Globals.GetFileName(path);

                    doc.Load(fileName);

                    //HUNGTQ Updated 07/06/2011
                    //Set thông tin MaCucHQ
                    GlobalSettings.MA_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaCucHQ").InnerText = txtMaCuc.Text.Trim();
                    //Set thông tin TenCucHQ
                    GlobalSettings.TEN_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenCucHQ").InnerText = txtTenCucHQ.Text.Trim();
                    //Set thông tin MaChiCucHQ
                    GlobalSettings.MA_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaChiCucHQ").InnerText = donViHaiQuanControl1.Ma.Trim();
                    //Set thông tin TenChiCucHQ
                    GlobalSettings.TEN_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenChiCucHQ").InnerText = donViHaiQuanControl1.Ten.Trim();
                    //Set thông tin TenNganChiCucHQ
                    GlobalSettings.TEN_HAI_QUAN_NGAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenNganChiCucHQ").InnerText = txtTenNganHQ.Text.Trim();
                    //Set thông tin MailHQ
                    GlobalSettings.MailHaiQuan = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailHQ").InnerText = txtMailHaiQuan.Text.Trim();
                    //Set thông tin MaDoanhNghiep
                    GlobalSettings.MA_DON_VI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaDoanhNghiep").InnerText = txtMaDN.Text.Trim();
                    //Set thông tin TenDoanhNghiep
                    GlobalSettings.TEN_DON_VI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenDoanhNghiep").InnerText = txtTenDN.Text.Trim();
                    //Set thông tin DiaChiDoanhNghiep
                    GlobalSettings.DIA_CHI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DiaChiDoanhNghiep").InnerText = txtDiaChi.Text.Trim();
                    //Set thông tin MaMid
                    GlobalSettings.MaMID = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaMID").InnerText = txtMaMid.Text.Trim();
                    //Set thông tin Email doanh nghiep/ ca nhan
                    GlobalSettings.MailDoanhNghiep = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailDoanhNghiep").InnerText = txtMailDoanhNghiep.Text.Trim();
                    //Set thông tin Dien thoai
                    GlobalSettings.SoDienThoaiDN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DienThoai").InnerText = txtDienThoaiDN.Text.Trim();
                    //Set thông tin Fax
                    GlobalSettings.SoFaxDN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Fax").InnerText = txtSoFaxDN.Text.Trim();
                    //Set thông tin Nguoi lien he
                    GlobalSettings.NguoiLienHe = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "NguoiLienHe").InnerText = txtNguoiLienHeDN.Text.Trim();

                    GlobalSettings.ChucVu = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "ChucVu").InnerText = txtChucVu.Text.Trim();

                    GlobalSettings.MA_HAI_QUAN_VNACCS = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACCMaHaiQuan(donViHaiQuanControl1.Ma.ToString().Trim());
                    //GlobalSettings.IsOnlyMe = chkOnlyMe.Checked;
                    //Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "OnlyMe").InnerText = chkOnlyMe.Checked.ToString();

                    //-----------------------------------------------------------------------
                    //HungTQ Updated 05/11/2012

                    //if (!URI.Contains("http:"))
                    //URI = "http://" + lblWS.Text.Trim();
                    string URI = URI = lblWS.Text.Trim();
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DiaChiWS", URI);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlSetting("Company_BLL_WS_KhaiDienTu_KDTService", URI);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlSetting("QuanLyChungTu_WS_KDTService", URI);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("WS_URL", URI);

                    try
                    {
                        if (txtHost.Text.Trim().Equals("") || txtPort.Text.Trim().Equals(""))
                        {
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsUseProxy", "False");
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Host", "");
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Port", "");
                        }
                        else
                        {
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsUseProxy", "True");
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Host", txtHost.Text);
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Port", txtPort.Text);
                        }
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi thiết lập Proxy.\nChi tiết lỗi: " + ex.Message, false);
                        return;
                    }

                    //HUNGTQ Updated 07/06/2011
                    //Set thong tin WS_Host
                    XmlNode nodeWS_Host = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Host");
                    //if (txtDiaChiHQ.Text.Contains("http:") || txtDiaChiHQ.Text.Contains("https:"))
                    GlobalSettings.DiaChiWS_Host = nodeWS_Host.InnerText = txtDiaChiHQ.Text.Trim();
                    //else
                    //    GlobalSettings.DiaChiWS_Host = nodeWS_Host.InnerText = "http://" + txtDiaChiHQ.Text.Trim();
                    //Set thong tin WS_Name
                    XmlNode nodeWS_Name = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Name");
                    GlobalSettings.DiaChiWS_Name = nodeWS_Name.InnerText = txtTenDichVu.Text.Trim();
                    //Set thong tin WebService
                    XmlNode nodeWebService = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS");
                    GlobalSettings.DiaChiWS = nodeWebService.InnerText = URI;

                    //minhnd 21/03/2015 fix khai báo V4 + V5
                    chkKhaiBaoV5.Checked = true;
                    chkSendV4.Checked = true;
                    //end minhnd 21/03/2015

                    //Hungtq updated 03/04/2012.
                    //Company.KDT.SHARE.Components.Globals.SuDungHttps = chkCKS.Checked;
                    //Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SuDungHttps", chkCKS.Checked == true ? "true" : "false");
                    //Company.KDT.SHARE.Components.Globals.SuDungHttps = chkCKS.Checked;

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SendV4", chkSendV4.Checked == true ? "true" : "false");
                    GlobalSettings.SendV4 = chkSendV4.Checked;
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsKhaiVNACCS", chkKhaiBaoV5.Checked == true ? "true" : "false");
                    Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS = chkKhaiBaoV5.Checked;
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsKTX", rbtIsKTX.Checked == true ? "true" : "false");
                    Company.KDT.SHARE.Components.Globals.IsKTX = rbtIsKTX.Checked;
                    //Hungtq updated 05/01/2013.
#if (SXXK_V3 || SXXK_V4)
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("LaDNCX", chkLaDNCX.Checked == true ? "true" : "false");
                    Company.KDT.SHARE.Components.Globals.LaDNCX = chkLaDNCX.Checked;
#endif

                    //Lưu file cấu hình
                    doc.Save(fileName);
                }

                #region Luu cau hinh cho DAI LY

                // luu cau hinh danh rieng
                if (versionHD == 1)
                {
                    string maDN = txtMaDN.Text;
                    string tenDN = txtTenDN.Text;
                    string diaChi = txtDiaChi.Text;
                    string nguoiLH = txtNguoiLienHeDN.Text;
                    string eMail = txtMailDoanhNghiep.Text;
                    string maHQ = donViHaiQuanControl1.Ma.ToString();
                    string mailHQ = txtMailHaiQuan.Text;
                    string midDN = txtMaMid.Text;
                    string maCuc = txtMaCuc.Text;
                    string tenCuc = donViHaiQuanControl1.cbTen.Text;
                    string tenHQ = txtTenCucHQ.Text;
                    string tenNganHQ = txtTenNganHQ.Text;
                    string dienthoai = txtDienThoaiDN.Text;
                    string sofax = txtSoFaxDN.Text;
                    string diaChiHQ = txtDiaChiHQ.Text;
                    string tenDichVu = txtTenDichVu.Text;
                    string chucVu = txtChucVu.Text;
                    string SendV4 = chkSendV4.Checked ? "True" : "False";
                    string IsKhaiVNACCS = chkKhaiBaoV5.Checked ? "True" : "False";
                    //inser update du lieu
                    try
                    {
                        Company.KDT.SHARE.Components.HeThongPhongKhai htpk = new Company.KDT.SHARE.Components.HeThongPhongKhai();
                        htpk.MaDoanhNghiep = maDN;
                        htpk.TenDoanhNghiep = tenDN;
                        htpk.PassWord = "0";

                        htpk.Key_Config = "CauHinh";
                        htpk.Value_Config = "1";
                        htpk.InsertUpdate();
                        htpk.Key_Config = "DIA_CHI";
                        htpk.Value_Config = diaChi;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "ĐIEN_THOAI";
                        htpk.Value_Config = dienthoai;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "SO_FAX";
                        htpk.Value_Config = sofax;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "NGUOI_LIEN_HE";
                        htpk.Value_Config = nguoiLH;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "CHUC_VU";
                        htpk.Value_Config = chucVu;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "MailDoanhNghiep";
                        htpk.Value_Config = eMail;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "MaMID";
                        htpk.Value_Config = midDN;
                        htpk.InsertUpdate();

                        //Lưu thông tin hải quan
                        htpk.Key_Config = "MA_CUC_HAI_QUAN";
                        htpk.Value_Config = maCuc;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "MA_HAI_QUAN";
                        htpk.Value_Config = maHQ;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "MailHaiQuan";
                        htpk.Value_Config = mailHQ;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "TEN_CUC_HAI_QUAN";
                        htpk.Value_Config = tenCuc;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "TEN_HAI_QUAN_NGAN";
                        htpk.Value_Config = tenNganHQ;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "TEN_HAI_QUAN";
                        htpk.Value_Config = tenHQ;
                        htpk.InsertUpdate();

                        //Save Dai chi khai bao hai quan
                        htpk.Key_Config = "DIA_CHI_HQ";
                        htpk.Value_Config = diaChiHQ;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "TEN_DICH_VU";
                        htpk.Value_Config = tenDichVu;
                        htpk.InsertUpdate();
                        htpk.Key_Config = "CHU_KY_SO";
                        htpk.Value_Config = Convert.ToString(true);
                        htpk.InsertUpdate();
                        //Cấu hình sendV4
                        htpk.Key_Config = "SendV4";
                        htpk.Value_Config = SendV4;
                        htpk.InsertUpdate();

                        //Cấu hình sendV5
                        htpk.Key_Config = "SendV5";
                        htpk.Value_Config = IsKhaiVNACCS;
                        htpk.InsertUpdate();

                        if (MainForm.isUseSettingVNACC)
                        {
                            //Luu thong tin thiet lap Terminal khai bao VNACCS-VCIS
                            htpk.Key_Config = "VNACC_TerminalID";
                            htpk.Value_Config = txtVNACCTerminalID.Text.Trim().ToUpper();
                            htpk.InsertUpdate();

                            htpk.Key_Config = "VNACC_TerminalPass";
                            htpk.Value_Config = txtVNACCTerminalPass.Text;
                            htpk.InsertUpdate();

                            htpk.Key_Config = "VNACC_UserID";
                            htpk.Value_Config = txtVNACCUserID.Text.Length > 3 ? txtVNACCUserID.Text.Trim().Substring(txtVNACCUserID.Text.Length - 3, 3).ToUpper() : txtVNACCUserID.Text.ToUpper();
                            htpk.InsertUpdate();

                            htpk.Key_Config = "VNACC_UserCode";
                            htpk.Value_Config = txtVNACCUserCode.Text.Trim().ToUpper();
                            htpk.InsertUpdate();

                            htpk.Key_Config = "VNACC_UserPass";
                            htpk.Value_Config = txtVNACCUserPass.Text;
                            htpk.InsertUpdate();

                            htpk.Key_Config = "VNACCAddress";
                            htpk.Value_Config = txtVNACCAddress.Text;
                            htpk.InsertUpdate();
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsKhaiVNACCS", chkKhaiBaoV5.Checked == true ? "true" : "false");
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("VNACCAddress", txtVNACCAddress.Text);
                            //Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("VNACCService", txtVNACCService.Text);
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("VNACCHostProxy", txtVNACCHostProxy.Text);
                            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("VNACCPortProxy", txtVNACCPortProxy.Text);

                            //Cap nhat bien toan cuc VNACC cho chuong trinh
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalID = txtVNACCTerminalID.Text.Trim().ToUpper();
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalAccessKey = txtVNACCTerminalPass.Text;
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_ID = txtVNACCUserID.Text.Length > 3 ? txtVNACCUserID.Text.Trim().Substring(txtVNACCUserID.Text.Length - 3, 3).ToUpper() : txtVNACCUserID.Text.ToUpper();
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Ma = txtVNACCUserCode.Text.Trim().ToUpper();
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Pass = txtVNACCUserPass.Text;
                            Company.KDT.SHARE.VNACCS.GlobalVNACC.Url = txtVNACCAddress.Text;
                        }

                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi cập nhật thông tin vào hệ thống phồng khai xãy ra lỗi. /n/n" + ex, false);
                    }
                }
                #endregion

                //kiểm tra cấu hình Chữ ký số.Phi
               
                    bool KyTrucTiep = Company.KDT.SHARE.Components.Globals.IsSignature;
                    bool KyTuXa = Company.KDT.SHARE.Components.Globals.IsSignRemote;
                    bool KyMangLan = Company.KDT.SHARE.Components.Globals.IsSignOnLan;
                    if (KyTrucTiep == false && KyTuXa == false && KyMangLan == false)
                    {
                        ShowMessage("Chưa cấu hình chữ ký số. Xin vui lòng chọn cấu hình.", false);
                        FrmCauHinhChuKySo frm = new FrmCauHinhChuKySo();
                        frm.ShowDialog();
                    }
                    if (Company.KDT.SHARE.Components.Globals.IsSignature == true || Company.KDT.SHARE.Components.Globals.IsSignRemote == true || Company.KDT.SHARE.Components.Globals.IsSignOnLan == true)
                    {
                        this.Close();
                    }

              

                //Danh cho doanh nghiep
                if (versionHD == 0 && MainForm.isUseSettingVNACC)
                {
                    //TODO: Kiem tra co bi trung TerminalID?
                    if (Company.QuanTri.User.CheckVNACCTerminalCountOnline(txtVNACCTerminalID.Text.Trim(), ((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME) > 1)
                    {
                        Helper.Controls.MessageBoxControlV.ShowMessage("Phát hiện Terminal '" + txtVNACCTerminalID.Text.Trim() + "' này đang sử dụng. Bạn vui lòng thiết lập lại thông tin cho Terminal.", false);
                        DialogResult = DialogResult.None;
                        txtVNACCTerminalID.Focus();
                        uiTab1.TabPages[2].Selected = true;
                        return;
                    }
                    else if (Company.QuanTri.User.CheckVNACCUserCodeCountOnline(txtVNACCUserCode.Text.Trim(), ((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME) > 1)
                    {
                        Helper.Controls.MessageBoxControlV.ShowMessage("Phát hiện UserCode '" + txtVNACCUserCode.Text.Trim() + "' này đang sử dụng. Bạn vui lòng thiết lập lại thông tin cho Terminal.", false);
                        DialogResult = DialogResult.None;
                        txtVNACCUserCode.Focus();
                        uiTab1.TabPages[2].Selected = true;
                        return;
                    }

                    //Luu thong tin thiet lap Terminal khai bao VNACCS-VCIS
                    Company.QuanTri.User userVNACC = Company.QuanTri.User.Load_VNACC(((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.ID);
                    userVNACC.VNACC_TerminalID = txtVNACCTerminalID.Text.Trim().ToUpper();
                    userVNACC.VNACC_TerminalPass = txtVNACCTerminalPass.Text;
                    userVNACC.VNACC_UserID = txtVNACCUserID.Text.Length > 3 ? txtVNACCUserID.Text.Trim().Substring(txtVNACCUserID.Text.Length - 3, 3).ToUpper() : txtVNACCUserID.Text.ToUpper();
                    userVNACC.VNACC_UserCode = txtVNACCUserCode.Text.Trim().ToUpper();
                    userVNACC.VNACC_UserPass = txtVNACCUserPass.Text;
                    userVNACC.Update_VNACC();

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("VNACCAddress", txtVNACCAddress.Text);
                    //Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("VNACCService", txtVNACCService.Text);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("VNACCHostProxy", txtVNACCHostProxy.Text);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("VNACCPortProxy", txtVNACCPortProxy.Text);

                    //Cap nhat bien toan cuc VNACC cho chuong trinh
                    Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalID = userVNACC.VNACC_TerminalID;
                    Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalAccessKey = userVNACC.VNACC_TerminalPass;
                    Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_ID = userVNACC.VNACC_UserID;
                    Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Ma = userVNACC.VNACC_UserCode;
                    Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Pass = userVNACC.VNACC_UserPass;
                    Company.KDT.SHARE.VNACCS.GlobalVNACC.Url = txtVNACCAddress.Text;
                }

                //Luu thong tin dong bo du lieu
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SuDungSoTK", chkSoTK.Checked);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SoTK", txtSoTKDBDL.Text);
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("CHUNGTUDINHKEM", chkDongBoChungTuDinhkem.Checked);

                ShowMessage("Lưu file cấu hình Thông tin Doanh nghiệp và Hải quan thành công.", false);
                GlobalSettings.RefreshKey();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        void frm_HandlerConfig(object sender, EventArgs e)
        {

        }

        private void donViHaiQuanControl1_Leave(object sender, EventArgs e)
        {
            GetIP();

            GetIP_VNACC();
        }

        private void GetIP()
        {
            try
            {
                if (string.IsNullOrEmpty(donViHaiQuanControl1.Ma)) return;
                string maCuc = donViHaiQuanControl1.Ma.Substring(1, 2);
                txtMaCuc.Text = maCuc;

                Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan objDonViHQ = Company.KDT.SHARE.Components.DanhMucHaiQuan.DonViHaiQuan.Load("Z" + maCuc + "Z");

                txtTenCucHQ.Text = objDonViHQ != null ? objDonViHQ.Ten : "";
                txtTenNganHQ.Text = donViHaiQuanControl1.Ten;
                txtMailHaiQuan.Text = "";

                //Updated by Hungtq 2903/2012.
                Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc objCucHQ = Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc.Load(maCuc);
                if (chkKhaiBaoV5.Checked)
                {
                    //txtDiaChiHQ.Text = "103.248.160.22";
                    txtDiaChiHQ.Text = "tntt.customs.gov.vn";
                    txtTenDichVu.Text = "";
                }
                else
                {
                    if (objCucHQ != null)
                    {
                        txtDiaChiHQ.Text = (objCucHQ.IPServiceCKS != "" ? objCucHQ.IPServiceCKS : "?");
                        txtTenDichVu.Text = objCucHQ.ServicePathCKS;
                    }
                    else
                    {
                        txtDiaChiHQ.Text = "123.30.23.5";
                        txtTenCucHQ.Text = "KDTServiceCKS/Service.asmx";
                    }
                                       
                    //if (objCucHQ != null)
                    //{
                    //    txtDiaChiHQ.Text = (objCucHQ.IPService != "" ? objCucHQ.IPService : "?");

                    //    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00" && !GlobalSettings.SendV4)
                    //    {
                    //        if (GlobalSettings.SuDungChuKySo)
                    //            txtTenDichVu.Text = objCucHQ.ServicePathCKS;
                    //        else
                    //            txtTenDichVu.Text = objCucHQ.ServicePathV3;
                    //    }
                    //    else if (Company.KDT.SHARE.Components.Globals.VersionSend == "2.00")
                    //    {
                    //        txtTenDichVu.Text = objCucHQ.ServicePathV2;
                    //    }
                    //    else //Mac dinh V4
                    //    {
                    //        txtTenDichVu.Text = objCucHQ.ServicePathV4;
                    //    }
                    //}
                }


                //Test webservice
                string msgError = "";
                bool webServiceConnection = Company.KDT.SHARE.Components.Globals.ServiceExists(lblWS.Text.Trim(), false, out msgError);
                if (webServiceConnection)
                    lblWS.ForeColor = System.Drawing.Color.Blue;
                else
                    lblWS.ForeColor = System.Drawing.Color.Red;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void txtTenDichVu_TextChanged(object sender, EventArgs e)
        {
            txtDiaChiHQ_TextChanged(null, null);
        }

        private void txtDiaChiHQ_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string url = txtDiaChiHQ.Text.Trim() + "/" + txtTenDichVu.Text.Trim();
                if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                    url = string.Format("http://{0}", url); lblWS.Text = txtDiaChiHQ.Text.Trim() + "/" + txtTenDichVu.Text.Trim();
                lblWS.Text = url;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnOpenWS_Click(object sender, EventArgs e)
        {
            Company.KDT.SHARE.Components.Globals.OpenWebrowser(lblWS.Text.Trim());
        }

        private void chkCKS_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
            //{
            //    bool KyTrucTiep = Company.KDT.SHARE.Components.Globals.IsSignature;
            //    bool KyTuXa = Company.KDT.SHARE.Components.Globals.IsSignRemote;
            //    bool KyMangLan = Company.KDT.SHARE.Components.Globals.IsSignOnLan;
            //    if (chkCKS.Checked == false && (KyTrucTiep == true || KyTuXa == true || KyMangLan == true))
            //    {
            //        if (ShowMessage("Bạn không muốn sữ dụng chử ký sô? ", true) == "Yes")
            //        {
            //            Company.KDT.SHARE.Components.Globals.IsSignature = false;
            //            Company.KDT.SHARE.Components.Globals.IsSignRemote = false;
            //            Company.KDT.SHARE.Components.Globals.IsSignOnLan = false;
            //        }
            //        else
            //        {
            //            chkCKS.Checked = true;
            //        }
            //    }
            //    if (string.IsNullOrEmpty(donViHaiQuanControl1.Ma)) return;
            //    string maCuc = donViHaiQuanControl1.Ma.Substring(1, 2);
            //    txtMaCuc.Text = maCuc;

                //Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc objCucHQ = Company.KDT.SHARE.Components.DanhMucHaiQuan.Cuc.Load(maCuc);
                //if (objCucHQ == null)
                //{
                //    if (chkCKS.Checked)
                //    {
                //        txtDiaChiHQ.Text = "123.30.23.5";
                //        txtTenCucHQ.Text = "KDTService/Service.asmx";
                //    }
                //    return;
                //};

                //if (chkCKS.Checked)
                //{
                //    txtDiaChiHQ.Text = (objCucHQ.IPServiceCKS != "" ? objCucHQ.IPServiceCKS : "?");

                //    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                //    {
                //        txtTenDichVu.Text = objCucHQ.ServicePathCKS;
                //    }
                //    else if (Company.KDT.SHARE.Components.Globals.VersionSend == "2.00")
                //    {
                //        txtTenDichVu.Text = objCucHQ.ServicePathV2;
                //    }
                //}
                //else
                //{
                //    txtDiaChiHQ.Text = (objCucHQ.IPService != "" ? objCucHQ.IPService : "?");

                //    if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                //    {
                //        if (GlobalSettings.SuDungChuKySo)
                //            txtTenDichVu.Text = objCucHQ.ServicePathCKS;
                //        else
                //            txtTenDichVu.Text = objCucHQ.ServicePathV3;
                //    }
                //    else if (Company.KDT.SHARE.Components.Globals.VersionSend == "2.00")
                //    {
                //        txtTenDichVu.Text = objCucHQ.ServicePathV2;
                //    }
                //    else if (Company.KDT.SHARE.Components.Globals.VersionSend == "4.00")
                //    {
                //        //Chua co thong tin du lieu service cua V4 cac cuc HQ
                //    }
                //}
                GetIP();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnCapNhatDBDL_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if (string.IsNullOrEmpty(GlobalSettings.USERNAME_DONGBO) || string.IsNullOrEmpty(GlobalSettings.PASSWOR_DONGBO))
            //    {
            //        WSDBDLForm f = new WSDBDLForm();
            //        f.ShowDialog();
            //        if (!f.IsReady) return;
            //        user = f.txtMaDoanhNghiep.Text;
            //        pw = f.txtMatKhau.Text;
            //    }
            //    else
            //    {
            //        user = GlobalSettings.USERNAME_DONGBO;
            //        pw = GlobalSettings.PASSWOR_DONGBO;
            //    }

            //    //Luu server cau hinih dai ly
            //    Company.KDT.SHARE.Components.ISyncData myService = Company.KDT.SHARE.Components.WebService.SyncService();
            //    bool successfull = myService.InsertUpdateCauHinhDaiLy(user, pw, GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI, Convert.ToInt32(txtSoTKDBDL.Text), "", 1);

            //    if (successfull == false)
            //    {
            //        ShowMessage("Lưu cấu hình số tờ khai Đại lý phải đồng bộ dữ liệu cho Doanh nghiệp thất bại.", false);
            //    }
            //    else
            //    {
            //        ShowMessage("Lưu cấu hình số tờ khai Đại lý phải đồng bộ dữ liệu cho Doanh nghiệp thành công.", false);

            //        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SuDungSoTK", chkSoTK.Checked);
            //        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SoTK", txtSoTKDBDL.Text);
            //        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("CHUNGTUDINHKEM", chkDongBoChungTuDinhkem.Checked);
            //    }
            //}
            //catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void chkSendV4_CheckedChanged(object sender, EventArgs e)
        {
            rbtIsKTX.Enabled = rbtIsTQDT.Enabled = chkSendV4.Checked;
        }

        private void linkGetIP_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GetIP();
        }

        /*** VNACCs ***/

        private void GetTerminal()
        {
            //txtVNACCTerminalPass.PasswordChar = txtVNACCUserPass.PasswordChar = '*';

            txtVNACCTerminalID.Text = Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalID;
            txtVNACCTerminalPass.Text = Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalAccessKey;
            txtVNACCUserID.Text = Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Ma + Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_ID;
            txtVNACCUserCode.Text = Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Ma;
            txtVNACCUserPass.Text = Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Pass;

            txtVNACCAddress.Text = Company.KDT.SHARE.VNACCS.GlobalVNACC.Url;
            txtVNACCHostProxy.Text = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("VNACCHostProxy", "");
            txtVNACCPortProxy.Text = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("VNACCPortProxy", "");

            //txtVNACCUserCode.TextChanged += new EventHandler(txtVNACCUserCode_TextChanged);
        }

        private void GetIP_VNACC()
        {
            try
            {
                txtVNACCAddress.Text = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("VNACCAddress", @"https://ediconn.vnaccs.customs.gov.vn/test");
                if (string.IsNullOrEmpty(txtVNACCAddress.Text.Trim())) txtVNACCAddress.Text = @"https://ediconn.vnaccs.customs.gov.vn/test";
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void linkVNACCGetIP_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GetIP_VNACC();
        }

        private void btnOpenWSVNACC_Click(object sender, EventArgs e)
        {
            Company.KDT.SHARE.Components.Globals.OpenWebrowser(lblWSVNACC.Text.Trim());
        }

        private void txtVNACCAddress_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string url = txtVNACCAddress.Text.Trim(); // +"/" + txtVNACCService.Text.Trim();
                if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                    url = string.Format("http://{0}", url); lblWSVNACC.Text = txtVNACCAddress.Text.Trim(); // +"/" + txtVNACCService.Text.Trim();
                lblWSVNACC.Text = url;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void txtVNACCService_TextChanged(object sender, EventArgs e)
        {
            txtVNACCAddress_TextChanged(null, null);
        }

        private void txtVNACCUserCode_TextChanged(object sender, EventArgs e)
        {
            txtVNACCUserID.Text = txtVNACCUserCode.Text;
        }

        private void chkKhaiBaoV5_CheckedChanged(object sender, EventArgs e)
        {
            GetIP();
            
        }

    }
}