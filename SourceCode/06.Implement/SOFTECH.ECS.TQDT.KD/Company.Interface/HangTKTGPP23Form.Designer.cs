﻿namespace Company.Interface
{
    partial class HangTKTGPP23Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HangTKTGPP23Form));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.calendarCombo1 = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label11 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtToSo = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.ccNgayDangKyTT = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txtGiaiTrinh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtTriGiaTTHang = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.lblTriGiaTTHang = new System.Windows.Forms.Label();
            this.txtTriGiaNTHang = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtDCCBaoHiem = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDCTBaoHiem = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtDCCVanTai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDCTVanTai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtDCCKhoanKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDCTKhoanKhac = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtDCCSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDCTSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtDieuChinhCongCapDoTM = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDieuChinhTruCapDoTM = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTriGiaHangTT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.ctrDonViHaiQuan = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label14 = new System.Windows.Forms.Label();
            this.ctrLoaiHinhTT = new Company.Interface.Controls.LoaiHinhMauDichHControl();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSoToKhaiTT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ccNgayXuatTT = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtTenHangTT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSTTHangTT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ccNgayXuat = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtTenHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtLyDo = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSTTHang = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvTenHangTT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.cvSTTHangTT = new Company.Controls.CustomValidation.CompareValidator();
            this.cvSoToKhaiTT = new Company.Controls.CustomValidation.CompareValidator();
            this.rfvTenHang = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNgayXuat = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNgayXuatTT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNgayDangKy = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.cvTriGiaNTTT = new Company.Controls.CustomValidation.CompareValidator();
            this.rfvLydo = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvGiaitrinh = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenHangTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSTTHangTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoToKhaiTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayXuat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayXuatTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayDangKy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvTriGiaNTTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvLydo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvGiaitrinh)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(660, 594);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.calendarCombo1);
            this.uiGroupBox1.Controls.Add(this.btnGhi);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label25);
            this.uiGroupBox1.Controls.Add(this.txtToSo);
            this.uiGroupBox1.Controls.Add(this.txtTenHang);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(660, 594);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // calendarCombo1
            // 
            // 
            // 
            // 
            this.calendarCombo1.DropDownCalendar.Name = "";
            this.calendarCombo1.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.calendarCombo1.IsNullDate = true;
            this.calendarCombo1.Location = new System.Drawing.Point(534, 12);
            this.calendarCombo1.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.calendarCombo1.Name = "calendarCombo1";
            this.calendarCombo1.ReadOnly = true;
            this.calendarCombo1.Size = new System.Drawing.Size(99, 21);
            this.calendarCombo1.TabIndex = 5;
            this.calendarCombo1.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.calendarCombo1.VisualStyleManager = this.vsmMain;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(18, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Số tờ khai";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(445, 17);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(83, 13);
            this.label25.TabIndex = 4;
            this.label25.Text = "Ngày đăng ký";
            this.label25.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // txtToSo
            // 
            this.txtToSo.Location = new System.Drawing.Point(87, 12);
            this.txtToSo.Name = "txtToSo";
            this.txtToSo.Size = new System.Drawing.Size(49, 21);
            this.txtToSo.TabIndex = 1;
            this.txtToSo.Text = "0";
            this.txtToSo.Value = ((ulong)(0ul));
            this.txtToSo.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtToSo.VisualStyleManager = this.vsmMain;
            // 
            // txtTenHang
            // 
            this.txtTenHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHang.Location = new System.Drawing.Point(339, 12);
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.ReadOnly = true;
            this.txtTenHang.Size = new System.Drawing.Size(75, 21);
            this.txtTenHang.TabIndex = 3;
            this.txtTenHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHang.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(181, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(152, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Kèm tờ khai nhập khẩu số";
            this.label6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.ccNgayDangKyTT);
            this.uiGroupBox4.Controls.Add(this.label29);
            this.uiGroupBox4.Controls.Add(this.label28);
            this.uiGroupBox4.Controls.Add(this.txtGiaiTrinh);
            this.uiGroupBox4.Controls.Add(this.label27);
            this.uiGroupBox4.Controls.Add(this.txtTriGiaTTHang);
            this.uiGroupBox4.Controls.Add(this.lblTriGiaTTHang);
            this.uiGroupBox4.Controls.Add(this.txtTriGiaNTHang);
            this.uiGroupBox4.Controls.Add(this.label24);
            this.uiGroupBox4.Controls.Add(this.txtDCCBaoHiem);
            this.uiGroupBox4.Controls.Add(this.txtDCTBaoHiem);
            this.uiGroupBox4.Controls.Add(this.label23);
            this.uiGroupBox4.Controls.Add(this.txtDCCVanTai);
            this.uiGroupBox4.Controls.Add(this.txtDCTVanTai);
            this.uiGroupBox4.Controls.Add(this.label22);
            this.uiGroupBox4.Controls.Add(this.txtDCCKhoanKhac);
            this.uiGroupBox4.Controls.Add(this.txtDCTKhoanKhac);
            this.uiGroupBox4.Controls.Add(this.label21);
            this.uiGroupBox4.Controls.Add(this.txtDCCSoLuong);
            this.uiGroupBox4.Controls.Add(this.txtDCTSoLuong);
            this.uiGroupBox4.Controls.Add(this.label20);
            this.uiGroupBox4.Controls.Add(this.txtDieuChinhCongCapDoTM);
            this.uiGroupBox4.Controls.Add(this.txtDieuChinhTruCapDoTM);
            this.uiGroupBox4.Controls.Add(this.label19);
            this.uiGroupBox4.Controls.Add(this.label17);
            this.uiGroupBox4.Controls.Add(this.label16);
            this.uiGroupBox4.Controls.Add(this.txtTriGiaHangTT);
            this.uiGroupBox4.Controls.Add(this.label15);
            this.uiGroupBox4.Controls.Add(this.ctrDonViHaiQuan);
            this.uiGroupBox4.Controls.Add(this.label14);
            this.uiGroupBox4.Controls.Add(this.ctrLoaiHinhTT);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.label10);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.txtSoToKhaiTT);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.ccNgayXuatTT);
            this.uiGroupBox4.Controls.Add(this.txtTenHangTT);
            this.uiGroupBox4.Controls.Add(this.label5);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.txtSTTHangTT);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Controls.Add(this.ccNgayXuat);
            this.uiGroupBox4.Controls.Add(this.txtTenHangHoa);
            this.uiGroupBox4.Controls.Add(this.txtLyDo);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.txtSTTHang);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(12, 42);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(634, 511);
            this.uiGroupBox4.TabIndex = 6;
            this.uiGroupBox4.Text = "Chi tiết";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnClose.Location = new System.Drawing.Point(571, 559);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 52;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayDangKyTT
            // 
            // 
            // 
            // 
            this.ccNgayDangKyTT.DropDownCalendar.Name = "";
            this.ccNgayDangKyTT.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDangKyTT.Location = new System.Drawing.Point(522, 150);
            this.ccNgayDangKyTT.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ccNgayDangKyTT.Name = "ccNgayDangKyTT";
            this.ccNgayDangKyTT.Size = new System.Drawing.Size(99, 21);
            this.ccNgayDangKyTT.TabIndex = 21;
            this.ccNgayDangKyTT.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDangKyTT.VisualStyleManager = this.vsmMain;
            // 
            // btnGhi
            // 
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(490, 558);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 51;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(378, 256);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(19, 13);
            this.label29.TabIndex = 29;
            this.label29.Text = "(-)";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(295, 256);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(23, 13);
            this.label28.TabIndex = 28;
            this.label28.Text = "(+)";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGiaiTrinh
            // 
            this.txtGiaiTrinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaiTrinh.Location = new System.Drawing.Point(15, 480);
            this.txtGiaiTrinh.Name = "txtGiaiTrinh";
            this.txtGiaiTrinh.Size = new System.Drawing.Size(522, 21);
            this.txtGiaiTrinh.TabIndex = 50;
            this.txtGiaiTrinh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGiaiTrinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtGiaiTrinh.VisualStyleManager = this.vsmMain;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(12, 464);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(260, 13);
            this.label27.TabIndex = 49;
            this.label27.Text = "Giải trình các khoản điều chỉnh và chứng từ kèm theo";
            // 
            // txtTriGiaTTHang
            // 
            this.txtTriGiaTTHang.DecimalDigits = 8;
            this.txtTriGiaTTHang.FormatString = "G20";
            this.txtTriGiaTTHang.Location = new System.Drawing.Point(440, 435);
            this.txtTriGiaTTHang.Name = "txtTriGiaTTHang";
            this.txtTriGiaTTHang.ReadOnly = true;
            this.txtTriGiaTTHang.Size = new System.Drawing.Size(97, 21);
            this.txtTriGiaTTHang.TabIndex = 47;
            this.txtTriGiaTTHang.Text = "0";
            this.txtTriGiaTTHang.Value = 0;
            this.txtTriGiaTTHang.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTriGiaTTHang.VisualStyleManager = this.vsmMain;
            // 
            // lblTriGiaTTHang
            // 
            this.lblTriGiaTTHang.AutoSize = true;
            this.lblTriGiaTTHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTriGiaTTHang.Location = new System.Drawing.Point(12, 440);
            this.lblTriGiaTTHang.Name = "lblTriGiaTTHang";
            this.lblTriGiaTTHang.Size = new System.Drawing.Size(309, 13);
            this.lblTriGiaTTHang.TabIndex = 48;
            this.lblTriGiaTTHang.Text = "4. Trị giá tính thuế bằng đồng Việt Nam = (3) * Tỷ giá tính thuế";
            // 
            // txtTriGiaNTHang
            // 
            this.txtTriGiaNTHang.DecimalDigits = 8;
            this.txtTriGiaNTHang.FormatString = "G20";
            this.txtTriGiaNTHang.Location = new System.Drawing.Point(440, 409);
            this.txtTriGiaNTHang.Name = "txtTriGiaNTHang";
            this.txtTriGiaNTHang.ReadOnly = true;
            this.txtTriGiaNTHang.Size = new System.Drawing.Size(97, 21);
            this.txtTriGiaNTHang.TabIndex = 46;
            this.txtTriGiaNTHang.Text = "0";
            this.txtTriGiaNTHang.Value = 0;
            this.txtTriGiaNTHang.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTriGiaNTHang.VisualStyleManager = this.vsmMain;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(12, 414);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(410, 13);
            this.label24.TabIndex = 45;
            this.label24.Text = "3. Trị giá tính thuế nguyên tệ của hàng hóa cần xác định trị giá tính thuế = (1) " +
                "+ (2)";
            // 
            // txtDCCBaoHiem
            // 
            this.txtDCCBaoHiem.DecimalDigits = 8;
            this.txtDCCBaoHiem.FormatString = "G20";
            this.txtDCCBaoHiem.Location = new System.Drawing.Point(278, 380);
            this.txtDCCBaoHiem.Name = "txtDCCBaoHiem";
            this.txtDCCBaoHiem.Size = new System.Drawing.Size(61, 21);
            this.txtDCCBaoHiem.TabIndex = 43;
            this.txtDCCBaoHiem.Text = "0";
            this.txtDCCBaoHiem.Value = 0;
            this.txtDCCBaoHiem.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtDCCBaoHiem.VisualStyleManager = this.vsmMain;
            this.txtDCCBaoHiem.Leave += new System.EventHandler(this.txtTriGiaHangTT_Leave);
            // 
            // txtDCTBaoHiem
            // 
            this.txtDCTBaoHiem.DecimalDigits = 8;
            this.txtDCTBaoHiem.FormatString = "G20";
            this.txtDCTBaoHiem.Location = new System.Drawing.Point(361, 380);
            this.txtDCTBaoHiem.Name = "txtDCTBaoHiem";
            this.txtDCTBaoHiem.Size = new System.Drawing.Size(61, 21);
            this.txtDCTBaoHiem.TabIndex = 44;
            this.txtDCTBaoHiem.Text = "0";
            this.txtDCTBaoHiem.Value = 0;
            this.txtDCTBaoHiem.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtDCTBaoHiem.VisualStyleManager = this.vsmMain;
            this.txtDCTBaoHiem.Leave += new System.EventHandler(this.txtTriGiaHangTT_Leave);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(28, 385);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(164, 13);
            this.label23.TabIndex = 42;
            this.label23.Text = "e. Điều chỉnh về chi phí bảo hiểm";
            // 
            // txtDCCVanTai
            // 
            this.txtDCCVanTai.DecimalDigits = 8;
            this.txtDCCVanTai.FormatString = "G20";
            this.txtDCCVanTai.Location = new System.Drawing.Point(278, 353);
            this.txtDCCVanTai.Name = "txtDCCVanTai";
            this.txtDCCVanTai.Size = new System.Drawing.Size(61, 21);
            this.txtDCCVanTai.TabIndex = 40;
            this.txtDCCVanTai.Text = "0";
            this.txtDCCVanTai.Value = 0;
            this.txtDCCVanTai.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtDCCVanTai.VisualStyleManager = this.vsmMain;
            this.txtDCCVanTai.Leave += new System.EventHandler(this.txtTriGiaHangTT_Leave);
            // 
            // txtDCTVanTai
            // 
            this.txtDCTVanTai.DecimalDigits = 8;
            this.txtDCTVanTai.FormatString = "G20";
            this.txtDCTVanTai.Location = new System.Drawing.Point(361, 353);
            this.txtDCTVanTai.Name = "txtDCTVanTai";
            this.txtDCTVanTai.Size = new System.Drawing.Size(61, 21);
            this.txtDCTVanTai.TabIndex = 41;
            this.txtDCTVanTai.Text = "0";
            this.txtDCTVanTai.Value = 0;
            this.txtDCTVanTai.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtDCTVanTai.VisualStyleManager = this.vsmMain;
            this.txtDCTVanTai.Leave += new System.EventHandler(this.txtTriGiaHangTT_Leave);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(28, 358);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(154, 13);
            this.label22.TabIndex = 39;
            this.label22.Text = "d. Điều chỉnh về chi phí vận tải";
            // 
            // txtDCCKhoanKhac
            // 
            this.txtDCCKhoanKhac.DecimalDigits = 8;
            this.txtDCCKhoanKhac.FormatString = "G20";
            this.txtDCCKhoanKhac.Location = new System.Drawing.Point(278, 326);
            this.txtDCCKhoanKhac.Name = "txtDCCKhoanKhac";
            this.txtDCCKhoanKhac.Size = new System.Drawing.Size(61, 21);
            this.txtDCCKhoanKhac.TabIndex = 37;
            this.txtDCCKhoanKhac.Text = "0";
            this.txtDCCKhoanKhac.Value = 0;
            this.txtDCCKhoanKhac.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtDCCKhoanKhac.VisualStyleManager = this.vsmMain;
            this.txtDCCKhoanKhac.Leave += new System.EventHandler(this.txtTriGiaHangTT_Leave);
            // 
            // txtDCTKhoanKhac
            // 
            this.txtDCTKhoanKhac.DecimalDigits = 8;
            this.txtDCTKhoanKhac.FormatString = "G20";
            this.txtDCTKhoanKhac.Location = new System.Drawing.Point(361, 326);
            this.txtDCTKhoanKhac.Name = "txtDCTKhoanKhac";
            this.txtDCTKhoanKhac.Size = new System.Drawing.Size(61, 21);
            this.txtDCTKhoanKhac.TabIndex = 38;
            this.txtDCTKhoanKhac.Text = "0";
            this.txtDCTKhoanKhac.Value = 0;
            this.txtDCTKhoanKhac.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtDCTKhoanKhac.VisualStyleManager = this.vsmMain;
            this.txtDCTKhoanKhac.Leave += new System.EventHandler(this.txtTriGiaHangTT_Leave);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(28, 331);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(202, 13);
            this.label21.TabIndex = 36;
            this.label21.Text = "c. Điều chỉnh về các khoản giảm giá khác";
            // 
            // txtDCCSoLuong
            // 
            this.txtDCCSoLuong.DecimalDigits = 8;
            this.txtDCCSoLuong.FormatString = "G20";
            this.txtDCCSoLuong.Location = new System.Drawing.Point(278, 299);
            this.txtDCCSoLuong.Name = "txtDCCSoLuong";
            this.txtDCCSoLuong.Size = new System.Drawing.Size(61, 21);
            this.txtDCCSoLuong.TabIndex = 34;
            this.txtDCCSoLuong.Text = "0";
            this.txtDCCSoLuong.Value = 0;
            this.txtDCCSoLuong.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtDCCSoLuong.VisualStyleManager = this.vsmMain;
            this.txtDCCSoLuong.Leave += new System.EventHandler(this.txtTriGiaHangTT_Leave);
            // 
            // txtDCTSoLuong
            // 
            this.txtDCTSoLuong.DecimalDigits = 8;
            this.txtDCTSoLuong.FormatString = "G20";
            this.txtDCTSoLuong.Location = new System.Drawing.Point(361, 299);
            this.txtDCTSoLuong.Name = "txtDCTSoLuong";
            this.txtDCTSoLuong.Size = new System.Drawing.Size(61, 21);
            this.txtDCTSoLuong.TabIndex = 35;
            this.txtDCTSoLuong.Text = "0";
            this.txtDCTSoLuong.Value = 0;
            this.txtDCTSoLuong.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtDCTSoLuong.VisualStyleManager = this.vsmMain;
            this.txtDCTSoLuong.Leave += new System.EventHandler(this.txtTriGiaHangTT_Leave);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(28, 304);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(129, 13);
            this.label20.TabIndex = 33;
            this.label20.Text = "b. Điều chỉnh về số lượng";
            // 
            // txtDieuChinhCongCapDoTM
            // 
            this.txtDieuChinhCongCapDoTM.DecimalDigits = 8;
            this.txtDieuChinhCongCapDoTM.FormatString = "G20";
            this.txtDieuChinhCongCapDoTM.Location = new System.Drawing.Point(278, 272);
            this.txtDieuChinhCongCapDoTM.Name = "txtDieuChinhCongCapDoTM";
            this.txtDieuChinhCongCapDoTM.Size = new System.Drawing.Size(61, 21);
            this.txtDieuChinhCongCapDoTM.TabIndex = 31;
            this.txtDieuChinhCongCapDoTM.Text = "0";
            this.txtDieuChinhCongCapDoTM.Value = 0;
            this.txtDieuChinhCongCapDoTM.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtDieuChinhCongCapDoTM.VisualStyleManager = this.vsmMain;
            this.txtDieuChinhCongCapDoTM.Leave += new System.EventHandler(this.txtTriGiaHangTT_Leave);
            // 
            // txtDieuChinhTruCapDoTM
            // 
            this.txtDieuChinhTruCapDoTM.DecimalDigits = 8;
            this.txtDieuChinhTruCapDoTM.FormatString = "G20";
            this.txtDieuChinhTruCapDoTM.Location = new System.Drawing.Point(361, 272);
            this.txtDieuChinhTruCapDoTM.Name = "txtDieuChinhTruCapDoTM";
            this.txtDieuChinhTruCapDoTM.Size = new System.Drawing.Size(61, 21);
            this.txtDieuChinhTruCapDoTM.TabIndex = 32;
            this.txtDieuChinhTruCapDoTM.Text = "0";
            this.txtDieuChinhTruCapDoTM.Value = 0;
            this.txtDieuChinhTruCapDoTM.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtDieuChinhTruCapDoTM.VisualStyleManager = this.vsmMain;
            this.txtDieuChinhTruCapDoTM.Leave += new System.EventHandler(this.txtTriGiaHangTT_Leave);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(28, 277);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(177, 13);
            this.label19.TabIndex = 30;
            this.label19.Text = "a. Điều chỉnh về cấp độ thương mại";
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(12, 248);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(257, 17);
            this.label17.TabIndex = 27;
            this.label17.Text = "2. Các khoản điều chỉnh (+/-)";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(12, 226);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(263, 17);
            this.label16.TabIndex = 25;
            this.label16.Text = "1. Trị giá tính thuế của hàng hóa nhập khẩu tương tự";
            // 
            // txtTriGiaHangTT
            // 
            this.txtTriGiaHangTT.DecimalDigits = 8;
            this.txtTriGiaHangTT.FormatString = "G20";
            this.txtTriGiaHangTT.Location = new System.Drawing.Point(278, 222);
            this.txtTriGiaHangTT.Name = "txtTriGiaHangTT";
            this.txtTriGiaHangTT.Size = new System.Drawing.Size(61, 21);
            this.txtTriGiaHangTT.TabIndex = 26;
            this.txtTriGiaHangTT.Text = "0";
            this.txtTriGiaHangTT.Value = 0;
            this.txtTriGiaHangTT.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTriGiaHangTT.VisualStyleManager = this.vsmMain;
            this.txtTriGiaHangTT.Leave += new System.EventHandler(this.txtTriGiaHangTT_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(6, 206);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(232, 13);
            this.label15.TabIndex = 24;
            this.label15.Text = "IV. Xác định trị giá tính thuế và giải trình";
            // 
            // ctrDonViHaiQuan
            // 
            this.ctrDonViHaiQuan.BackColor = System.Drawing.Color.Transparent;
            this.ctrDonViHaiQuan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrDonViHaiQuan.Location = new System.Drawing.Point(229, 177);
            this.ctrDonViHaiQuan.Ma = "";
            this.ctrDonViHaiQuan.MaCuc = "";
            this.ctrDonViHaiQuan.Name = "ctrDonViHaiQuan";
            this.ctrDonViHaiQuan.ReadOnly = false;
            this.ctrDonViHaiQuan.Size = new System.Drawing.Size(392, 22);
            this.ctrDonViHaiQuan.TabIndex = 23;
            this.ctrDonViHaiQuan.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(12, 180);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "Chi cục hải quan";
            // 
            // ctrLoaiHinhTT
            // 
            this.ctrLoaiHinhTT.BackColor = System.Drawing.Color.Transparent;
            this.ctrLoaiHinhTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrLoaiHinhTT.Location = new System.Drawing.Point(229, 149);
            this.ctrLoaiHinhTT.Ma = "";
            this.ctrLoaiHinhTT.Name = "ctrLoaiHinhTT";
            this.ctrLoaiHinhTT.Nhom = "";
            this.ctrLoaiHinhTT.ReadOnly = false;
            this.ctrLoaiHinhTT.Size = new System.Drawing.Size(156, 22);
            this.ctrLoaiHinhTT.TabIndex = 19;
            this.ctrLoaiHinhTT.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 155);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Số tờ khai";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(169, 153);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 17);
            this.label10.TabIndex = 18;
            this.label10.Text = "Loại hình";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(443, 155);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "Ngày đăng ký";
            // 
            // txtSoToKhaiTT
            // 
            this.txtSoToKhaiTT.DecimalDigits = 0;
            this.txtSoToKhaiTT.Location = new System.Drawing.Point(89, 150);
            this.txtSoToKhaiTT.Name = "txtSoToKhaiTT";
            this.txtSoToKhaiTT.Size = new System.Drawing.Size(61, 21);
            this.txtSoToKhaiTT.TabIndex = 17;
            this.txtSoToKhaiTT.Text = "0";
            this.txtSoToKhaiTT.Value = ((ulong)(0ul));
            this.txtSoToKhaiTT.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoToKhaiTT.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "STT hàng";
            // 
            // ccNgayXuatTT
            // 
            // 
            // 
            // 
            this.ccNgayXuatTT.DropDownCalendar.Name = "";
            this.ccNgayXuatTT.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayXuatTT.Location = new System.Drawing.Point(522, 119);
            this.ccNgayXuatTT.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ccNgayXuatTT.Name = "ccNgayXuatTT";
            this.ccNgayXuatTT.Size = new System.Drawing.Size(99, 21);
            this.ccNgayXuatTT.TabIndex = 15;
            this.ccNgayXuatTT.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayXuatTT.VisualStyleManager = this.vsmMain;
            // 
            // txtTenHangTT
            // 
            this.txtTenHangTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangTT.Location = new System.Drawing.Point(229, 119);
            this.txtTenHangTT.Name = "txtTenHangTT";
            this.txtTenHangTT.Size = new System.Drawing.Size(156, 21);
            this.txtTenHangTT.TabIndex = 13;
            this.txtTenHangTT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHangTT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHangTT.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(443, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Ngày xuất";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(169, 123);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 17);
            this.label8.TabIndex = 12;
            this.label8.Text = "Tên hàng";
            // 
            // txtSTTHangTT
            // 
            this.txtSTTHangTT.DecimalDigits = 0;
            this.txtSTTHangTT.Location = new System.Drawing.Point(89, 121);
            this.txtSTTHangTT.Name = "txtSTTHangTT";
            this.txtSTTHangTT.Size = new System.Drawing.Size(61, 21);
            this.txtSTTHangTT.TabIndex = 11;
            this.txtSTTHangTT.Text = "0";
            this.txtSTTHangTT.Value = ((ulong)(0ul));
            this.txtSTTHangTT.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSTTHangTT.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(424, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "I. Lý do không áp dụng các phương pháp xác định trị giá tính thuế trước đó";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(219, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "III. Tên hàng hóa nhập khẩu tương tự";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "STT hàng";
            // 
            // ccNgayXuat
            // 
            // 
            // 
            // 
            this.ccNgayXuat.DropDownCalendar.Name = "";
            this.ccNgayXuat.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayXuat.Location = new System.Drawing.Point(522, 67);
            this.ccNgayXuat.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ccNgayXuat.Name = "ccNgayXuat";
            this.ccNgayXuat.Size = new System.Drawing.Size(99, 21);
            this.ccNgayXuat.TabIndex = 8;
            this.ccNgayXuat.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayXuat.VisualStyleManager = this.vsmMain;
            // 
            // txtTenHangHoa
            // 
            this.txtTenHangHoa.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtTenHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangHoa.Location = new System.Drawing.Point(229, 69);
            this.txtTenHangHoa.Name = "txtTenHangHoa";
            this.txtTenHangHoa.ReadOnly = true;
            this.txtTenHangHoa.Size = new System.Drawing.Size(156, 21);
            this.txtTenHangHoa.TabIndex = 6;
            this.txtTenHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHangHoa.VisualStyleManager = this.vsmMain;
            this.txtTenHangHoa.ButtonClick += new System.EventHandler(this.txtTenHangHoa_ButtonClick);
            // 
            // txtLyDo
            // 
            this.txtLyDo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLyDo.Location = new System.Drawing.Point(436, 14);
            this.txtLyDo.Multiline = true;
            this.txtLyDo.Name = "txtLyDo";
            this.txtLyDo.Size = new System.Drawing.Size(185, 44);
            this.txtLyDo.TabIndex = 1;
            this.txtLyDo.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLyDo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtLyDo.VisualStyleManager = this.vsmMain;
            this.txtLyDo.TextChanged += new System.EventHandler(this.txtLyDo_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(443, 72);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(57, 13);
            this.label18.TabIndex = 7;
            this.label18.Text = "Ngày xuất";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(169, 75);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Tên hàng";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(262, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "II. Tên hàng hóa cần xác định trị giá tính thuế";
            // 
            // txtSTTHang
            // 
            this.txtSTTHang.DecimalDigits = 0;
            this.txtSTTHang.Location = new System.Drawing.Point(89, 69);
            this.txtSTTHang.Name = "txtSTTHang";
            this.txtSTTHang.Size = new System.Drawing.Size(61, 21);
            this.txtSTTHang.TabIndex = 4;
            this.txtSTTHang.Text = "0";
            this.txtSTTHang.Value = ((ulong)(0ul));
            this.txtSTTHang.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSTTHang.VisualStyleManager = this.vsmMain;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // rfvTenHangTT
            // 
            this.rfvTenHangTT.ControlToValidate = this.txtTenHangTT;
            this.rfvTenHangTT.ErrorMessage = "\"Tên hàng tương tự\" không được để trống.";
            this.rfvTenHangTT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenHangTT.Icon")));
            this.rfvTenHangTT.Tag = "rfvTenHangTT";
            // 
            // cvSTTHangTT
            // 
            this.cvSTTHangTT.ControlToValidate = this.txtSTTHangTT;
            this.cvSTTHangTT.ErrorMessage = "\"Số tờ khai hàng TT\" không hợp lệ.";
            this.cvSTTHangTT.Icon = ((System.Drawing.Icon)(resources.GetObject("cvSTTHangTT.Icon")));
            this.cvSTTHangTT.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvSTTHangTT.Tag = "cvSTTHangTT";
            this.cvSTTHangTT.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvSTTHangTT.ValueToCompare = "0";
            // 
            // cvSoToKhaiTT
            // 
            this.cvSoToKhaiTT.ControlToValidate = this.txtSoToKhaiTT;
            this.cvSoToKhaiTT.ErrorMessage = "\"Số tờ khai hàng TT\" không hợp lệ.";
            this.cvSoToKhaiTT.Icon = ((System.Drawing.Icon)(resources.GetObject("cvSoToKhaiTT.Icon")));
            this.cvSoToKhaiTT.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvSoToKhaiTT.Tag = "cvSoToKhaiTT";
            this.cvSoToKhaiTT.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvSoToKhaiTT.ValueToCompare = "0";
            // 
            // rfvTenHang
            // 
            this.rfvTenHang.ControlToValidate = this.txtTenHangHoa;
            this.rfvTenHang.ErrorMessage = "\"Tên hàng\" không được để trống.";
            this.rfvTenHang.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenHang.Icon")));
            this.rfvTenHang.Tag = "rfvTenHang";
            // 
            // rfvNgayXuat
            // 
            this.rfvNgayXuat.ControlToValidate = this.ccNgayXuat;
            this.rfvNgayXuat.ErrorMessage = "\"Ngày xuất hàng\" không được để trống.";
            this.rfvNgayXuat.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayXuat.Icon")));
            this.rfvNgayXuat.Tag = "rfvNgayXuat";
            // 
            // rfvNgayXuatTT
            // 
            this.rfvNgayXuatTT.ControlToValidate = this.ccNgayXuatTT;
            this.rfvNgayXuatTT.ErrorMessage = "\"Ngày xuất hàng tương tự\" không được để trống.";
            this.rfvNgayXuatTT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayXuatTT.Icon")));
            this.rfvNgayXuatTT.Tag = "rfvNgayXuatTT";
            // 
            // rfvNgayDangKy
            // 
            this.rfvNgayDangKy.ControlToValidate = this.ccNgayDangKyTT;
            this.rfvNgayDangKy.ErrorMessage = "\"Ngày đăng ký  hàng tương tự\" không được để trống.";
            this.rfvNgayDangKy.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayDangKy.Icon")));
            this.rfvNgayDangKy.Tag = "rfvNgayDangKy";
            // 
            // cvTriGiaNTTT
            // 
            this.cvTriGiaNTTT.ControlToValidate = this.txtTriGiaHangTT;
            this.cvTriGiaNTTT.ErrorMessage = "\"Trị giá hàng TT\" không hợp lệ.";
            this.cvTriGiaNTTT.Icon = ((System.Drawing.Icon)(resources.GetObject("cvTriGiaNTTT.Icon")));
            this.cvTriGiaNTTT.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvTriGiaNTTT.Tag = "cvTriGiaNTTT";
            this.cvTriGiaNTTT.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvTriGiaNTTT.ValueToCompare = "0";
            // 
            // rfvLydo
            // 
            this.rfvLydo.ControlToValidate = this.txtLyDo;
            this.rfvLydo.ErrorMessage = "\"Lý do\" không được để trống.";
            this.rfvLydo.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvLydo.Icon")));
            this.rfvLydo.Tag = "rfvLydo";
            // 
            // rfvGiaitrinh
            // 
            this.rfvGiaitrinh.ControlToValidate = this.txtGiaiTrinh;
            this.rfvGiaitrinh.ErrorMessage = "\"Giải trình\" không được để trống.";
            this.rfvGiaitrinh.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvGiaitrinh.Icon")));
            this.rfvGiaitrinh.Tag = "rfvGiaitrinh";
            // 
            // HangTKTGPP23Form
            // 
            this.AcceptButton = this.btnGhi;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(660, 594);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "HangTKTGPP23Form";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Trị giá tính thuế của một dòng hàng";
            this.Load += new System.EventHandler(this.HangTKTGForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenHangTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSTTHangTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoToKhaiTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayXuat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayXuatTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayDangKy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvTriGiaNTTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvLydo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvGiaitrinh)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private System.Windows.Forms.ImageList ImageList1;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSTTHang;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayXuat;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangHoa;
        private Janus.Windows.GridEX.EditControls.EditBox txtLyDo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private Company.Interface.Controls.DonViHaiQuanControl ctrDonViHaiQuan;
        private System.Windows.Forms.Label label14;
        private Company.Interface.Controls.LoaiHinhMauDichHControl ctrLoaiHinhTT;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhaiTT;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayXuatTT;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangTT;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSTTHangTT;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDCCBaoHiem;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDCTBaoHiem;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDCCVanTai;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDCTVanTai;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDCCKhoanKhac;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDCTKhoanKhac;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDCCSoLuong;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDCTSoLuong;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDieuChinhCongCapDoTM;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDieuChinhTruCapDoTM;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private Janus.Windows.GridEX.EditControls.EditBox txtGiaiTrinh;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTTHang;
        private System.Windows.Forms.Label lblTriGiaTTHang;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaNTHang;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayDangKyTT;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaHangTT;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenHangTT;
        private Company.Controls.CustomValidation.CompareValidator cvSTTHangTT;
        private Company.Controls.CustomValidation.CompareValidator cvSoToKhaiTT;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenHang;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayXuat;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayXuatTT;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayDangKy;
        private Company.Controls.CustomValidation.CompareValidator cvTriGiaNTTT;
        private Janus.Windows.CalendarCombo.CalendarCombo calendarCombo1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtToSo;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHang;
        private System.Windows.Forms.Label label6;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvLydo;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvGiaitrinh;


    }
}