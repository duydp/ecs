﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CA_QuanLyGCN
{
    public partial class MaDoanhNghiemFrm : Form
    {
        public string MaDoanhNghiep { get { return txtMaDoanhNghiep.SelectedValue == null ? string.Empty : txtMaDoanhNghiep.SelectedValue.ToString().Trim(); } }

        public MaDoanhNghiemFrm()
        {
            InitializeComponent();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void MaDoanhNghiemFrm_Load(object sender, EventArgs e)
        {
            txtMaDoanhNghiep.AutoComplete = true;
            txtMaDoanhNghiep.DataSource = DoanhNghiep.SelectAllMaDoanhNghiep("Mst");
            //txtMaDoanhNghiep.DataSource = DoanhNghiep.ListAll();
            //txtMaDoanhNghiep.SetDataBinding(DoanhNghiep.ListAll(), "mst");
            txtMaDoanhNghiep.ValueMember = "Mst";
            txtMaDoanhNghiep.DisplayMember = "Mst";
            //txtMaDoanhNghiep.DisplayMember = "tendn";
        }
    }
}
