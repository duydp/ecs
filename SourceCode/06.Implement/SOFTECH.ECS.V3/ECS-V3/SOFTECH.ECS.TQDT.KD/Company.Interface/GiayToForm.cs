﻿using System;
using System.Drawing;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.KDT;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.KD.BLL;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
namespace Company.Interface
{
    public partial class GiayToForm : BaseForm
    {
        public ChungTuCollection collection=new ChungTuCollection();
        public string GiayTo = "";
        private Image img ; 
        public GiayToForm()
        {
            InitializeComponent();
            
        }
        private byte[] GetArrayFromImagen(Image imagen)
        {
            MemoryStream ms = new MemoryStream();            
            imagen.Save(ms, ImageFormat.Jpeg);
            byte[] matriz = ms.ToArray();

            return matriz;
        }
        private void btnAddNew_Click(object sender, EventArgs e)
        {
            FileStream file = null;
            Image img = null;           
            if (txtBCHopDong.Text != "0" || txtBSHopDong.Text != "0")
            {                
                GiayTo += "Hop Dong TM";
                if (txtBCHopDong.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCHopDong.Text + " ";
                if (txtBSHopDong.Text != "0")
                    GiayTo += " Ban Sao " + txtBSHopDong.Text+" ";
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 1)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT = 1;
                ctHD.TenChungTu = "Hợp đồng thương mại";
                ctHD.SoBanChinh = Convert.ToInt16(txtBCHopDong.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSHopDong.Text);
                if (FileHopDong.Text != "")
                {
                    img = Image.FromFile(FileHopDong.Text);
                    ctHD.NoiDung = GetArrayFromImagen(img);
                    ctHD.FileUpLoad = FileHopDong.Text;                 
                }
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 1)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }
            if (txtBCHoaDon.Text != "0" || txtBSHoaDon.Text != "0")
            {              
                GiayTo += "Hoa Don TM";
                if (txtBCHoaDon.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCHoaDon.Text + " "; ;
                if (txtBSHoaDon.Text != "0")
                    GiayTo += " Ban Sao " + txtBSHoaDon.Text + " "; ;
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 2)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT =2;
                ctHD.TenChungTu = "Hóa đơn thương mại";
                ctHD.SoBanChinh = Convert.ToInt16(txtBCHoaDon.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSHoaDon.Text);
                if (FileHoaDon.Text.Trim() != "")
                {
                    img = Image.FromFile(FileHoaDon.Text);
                    ctHD.NoiDung = GetArrayFromImagen(img);
                    ctHD.FileUpLoad = FileHoaDon.Text;                  
                }
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT ==2)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }
            if (txtBCBanKe.Text != "0" || txtBSBanKe.Text != "0")
            {              
                GiayTo += " Ban ke";
                if (txtBCBanKe.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCBanKe.Text + " "; ;
                if (txtBSBanKe.Text != "0")
                    GiayTo += " Ban Sao " + txtBSBanKe.Text + " "; ;
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 3)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT = 3;
                ctHD.TenChungTu = "Bản kê chi tiết";
                ctHD.SoBanChinh = Convert.ToInt16(txtBCHoaDon.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSHoaDon.Text);
                if (FileBanKe.Text.Trim() != "")
                {
                    img = Image.FromFile(FileHoaDon.Text);
                    ctHD.NoiDung = GetArrayFromImagen(img);
                    ctHD.FileUpLoad = FileBanKe.Text;                    
                }
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 3)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }
            if (txtBCVanTai.Text != "0" || txtBSVanTai.Text != "0")
            {                
                GiayTo += "Van tai don";
                if (txtBCVanTai.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCVanTai.Text + " "; ;
                if (txtBSVanTai.Text != "0")
                    GiayTo += " Ban Sao " + txtBSVanTai.Text + " "; ;
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 4)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT = 4;
                ctHD.TenChungTu = "Vận tải đơn";
                ctHD.SoBanChinh = Convert.ToInt16(txtBCVanTai.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSVanTai.Text);
                if (FileVanTai.Text.Trim() != "")
                {
                    img = Image.FromFile(FileVanTai.Text);
                    ctHD.NoiDung = GetArrayFromImagen(img);
                    ctHD.FileUpLoad = FileVanTai.Text;                    
                }
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 4)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }

            if (txtLoaiKhac1.Text.Trim() != "")
            {              
                GiayTo += txtLoaiKhac1.Text.Trim();
                if (txtBCLoaiKhac1.Text != "0")
                    GiayTo += " Ban Chinh " + txtBCLoaiKhac1.Text + " "; ;
                if (txtBSLoaiKhac1.Text != "0")
                    GiayTo += " Ban Sao " + txtBSLoaiKhac1.Text + " "; ;
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 5)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD == null)
                {
                    ctHD = new ChungTu();
                    collection.Add(ctHD);
                }
                ctHD.LoaiCT = 5;
                ctHD.TenChungTu = txtLoaiKhac1.Text.Trim();
                ctHD.SoBanChinh = Convert.ToInt16(txtBCLoaiKhac1.Text);
                ctHD.SoBanSao = Convert.ToInt16(txtBSLoaiKhac1.Text);
                if (FileLoaiKhac.Text.Trim() != "")
                {
                    img = Image.FromFile(FileLoaiKhac.Text);
                    ctHD.NoiDung = GetArrayFromImagen(img);
                    ctHD.FileUpLoad = FileLoaiKhac.Text;                    
                }
            }
            else
            {
                ChungTu ctHD = null;
                foreach (ChungTu ct in collection)
                {
                    if (ct.LoaiCT == 5)
                    {
                        ctHD = ct; break;
                    }
                }
                if (ctHD != null)
                {
                    collection.Remove(ctHD);
                    if (ctHD.ID > 0)
                        ctHD.Delete();
                }
            }
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {          
            this.Close();
        }

        private void ChungTuForm_Load(object sender, EventArgs e)
        {
            if (this.OpenType == Company.KDT.SHARE.Components.OpenFormType.View)
                {
                    btnAddNew.Visible = false;
                }
                foreach (ChungTu ct in collection)
                {
                    //hợp đồng
                    if (ct.LoaiCT == 1)
                    {
                        txtBCHopDong.Text = ct.SoBanChinh.ToString();
                        txtBSHopDong.Text = ct.SoBanSao.ToString();
                        FileHopDong.Text = ct.FileUpLoad;
                    }
                        //hóa đơn
                    else if (ct.LoaiCT == 2)
                    {
                        txtBCHoaDon.Text = ct.SoBanChinh.ToString();
                        txtBSHoaDon.Text = ct.SoBanSao.ToString();
                        FileHoaDon.Text = ct.FileUpLoad;
                    }
                    else if (ct.LoaiCT == 3)
                    {
                        txtBCBanKe.Text = ct.SoBanChinh.ToString();
                        txtBSBanKe.Text = ct.SoBanSao.ToString();
                        FileBanKe.Text = ct.FileUpLoad;
                    }
                    else if (ct.LoaiCT == 4)
                    {
                        txtBCVanTai.Text = ct.SoBanChinh.ToString();
                        txtBSVanTai.Text = ct.SoBanSao.ToString();
                        FileVanTai.Text = ct.FileUpLoad;
                    }
                    else 
                    {
                        txtBCLoaiKhac1.Text = ct.SoBanChinh.ToString();
                        txtBSLoaiKhac1.Text = ct.SoBanSao.ToString();
                        txtLoaiKhac1.Text = ct.TenChungTu;
                        FileLoaiKhac.Text = ct.FileUpLoad;
                    }
                }
                openFile.InitialDirectory = Application.StartupPath;
        }

        private void editBox1_ButtonClick(object sender, EventArgs e)
        {
            EditBox txtChungTu = (EditBox)sender;
            DialogResult kq = openFile.ShowDialog(this);
            if (kq == DialogResult.OK)
            {
                txtChungTu.Text = openFile.FileName;                
            }
        }
     

     
    }
}
