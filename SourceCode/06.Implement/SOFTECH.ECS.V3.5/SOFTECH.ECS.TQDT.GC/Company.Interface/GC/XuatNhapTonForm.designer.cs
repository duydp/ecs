﻿namespace Company.Interface
{
    partial class XuatNhapTonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgListHD_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XuatNhapTonForm));
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgDinhMuc_SanPham_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListTotal_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.mnuGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripXuatExcel_NPL = new System.Windows.Forms.ToolStripMenuItem();
            this.gridEXPrintDocument1 = new Janus.Windows.GridEX.GridEXPrintDocument();
            this.gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenChuHang2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnTimKiemNPL = new Janus.Windows.EditControls.UIButton();
            this.dgListNPL = new Janus.Windows.GridEX.GridEX();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnThemHD = new System.Windows.Forms.Button();
            this.btnDone = new System.Windows.Forms.Button();
            this.btnXuatMau15_TriGia = new System.Windows.Forms.Button();
            this.btnInBaoCao = new System.Windows.Forms.Button();
            this.btnMau15_Luong = new System.Windows.Forms.Button();
            this.btnGetXNTFromThanhKhoan = new System.Windows.Forms.Button();
            this.btnMau15_TongLuong = new System.Windows.Forms.Button();
            this.btnXuLyXNT_Mau15 = new System.Windows.Forms.Button();
            this.dtpNgayCuoiKy = new System.Windows.Forms.DateTimePicker();
            this.btnBCMau15_TongTriGia = new System.Windows.Forms.Button();
            this.dtpNgayDauKy = new System.Windows.Forms.DateTimePicker();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblXuLy = new System.Windows.Forms.Label();
            this.lblPercen = new System.Windows.Forms.Label();
            this.mnnGridDM = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripGridDM = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHopDong = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuXuatEXHopDong = new System.Windows.Forms.ToolStripMenuItem();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListHD = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgDinhMuc_SanPham = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListTotal = new Janus.Windows.GridEX.GridEX();
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSave2 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback2 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult2 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSendEdit1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSendEdit");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdSendEdit = new Janus.Windows.UI.CommandBars.UICommand("cmdSendEdit");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dateNgayTN = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.cbbDVT = new Janus.Windows.EditControls.UIComboBox();
            this.cbbLoaiHinh = new Janus.Windows.EditControls.UIComboBox();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamQT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lblFileName = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.ReadExcelFile = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.mnuGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.mnnGridDM.SuspendLayout();
            this.mnuHopDong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListHD)).BeginInit();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDinhMuc_SanPham)).BeginInit();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Controls.Add(this.groupBox1);
            this.grbMain.Location = new System.Drawing.Point(0, 28);
            this.grbMain.Size = new System.Drawing.Size(1060, 542);
            // 
            // mnuGrid
            // 
            this.mnuGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripXuatExcel_NPL});
            this.mnuGrid.Name = "mnuGrid";
            this.mnuGrid.Size = new System.Drawing.Size(128, 26);
            // 
            // toolStripXuatExcel_NPL
            // 
            this.toolStripXuatExcel_NPL.Name = "toolStripXuatExcel_NPL";
            this.toolStripXuatExcel_NPL.Size = new System.Drawing.Size(127, 22);
            this.toolStripXuatExcel_NPL.Text = "Xuất Excel";
            this.toolStripXuatExcel_NPL.Click += new System.EventHandler(this.btnXuatExcel_Click);
            // 
            // gridEXPrintDocument1
            // 
            this.gridEXPrintDocument1.CardColumnsPerPage = 1;
            this.gridEXPrintDocument1.FitColumns = Janus.Windows.GridEX.FitColumnsMode.SizingColumns;
            this.gridEXPrintDocument1.PageHeaderCenter = "DANH SÁCH NGUYÊN PHỤ LIỆU TỒN";
            this.gridEXPrintDocument1.PageHeaderFormatStyle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // gridEXExporter1
            // 
            this.gridEXExporter1.SheetName = "NPLTon";
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.txtTenChuHang2);
            this.uiGroupBox5.Controls.Add(this.label1);
            this.uiGroupBox5.Controls.Add(this.btnTimKiemNPL);
            this.uiGroupBox5.Location = new System.Drawing.Point(12, 19);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1035, 51);
            this.uiGroupBox5.TabIndex = 193;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // txtTenChuHang2
            // 
            this.txtTenChuHang2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtTenChuHang2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTenChuHang2.Location = new System.Drawing.Point(136, 18);
            this.txtTenChuHang2.Name = "txtTenChuHang2";
            this.txtTenChuHang2.Size = new System.Drawing.Size(220, 20);
            this.txtTenChuHang2.TabIndex = 17;
            this.txtTenChuHang2.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Tên chủ hàng";
            // 
            // btnTimKiemNPL
            // 
            this.btnTimKiemNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiemNPL.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnTimKiemNPL.Location = new System.Drawing.Point(362, 16);
            this.btnTimKiemNPL.Name = "btnTimKiemNPL";
            this.btnTimKiemNPL.Size = new System.Drawing.Size(84, 23);
            this.btnTimKiemNPL.TabIndex = 18;
            this.btnTimKiemNPL.Text = "Tìm kiếm";
            this.btnTimKiemNPL.WordWrap = false;
            // 
            // dgListNPL
            // 
            this.dgListNPL.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPL.AlternatingColors = true;
            this.dgListNPL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgListNPL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNPL.ColumnAutoResize = true;
            this.dgListNPL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPL.GroupByBoxVisible = false;
            this.dgListNPL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPL.Location = new System.Drawing.Point(12, 76);
            this.dgListNPL.Name = "dgListNPL";
            this.dgListNPL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListNPL.Size = new System.Drawing.Size(1035, 409);
            this.dgListNPL.TabIndex = 192;
            this.dgListNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.btnThemHD);
            this.groupBox1.Controls.Add(this.btnDone);
            this.groupBox1.Controls.Add(this.btnXuatMau15_TriGia);
            this.groupBox1.Controls.Add(this.btnInBaoCao);
            this.groupBox1.Controls.Add(this.btnMau15_Luong);
            this.groupBox1.Controls.Add(this.btnGetXNTFromThanhKhoan);
            this.groupBox1.Controls.Add(this.btnMau15_TongLuong);
            this.groupBox1.Controls.Add(this.btnXuLyXNT_Mau15);
            this.groupBox1.Controls.Add(this.dtpNgayCuoiKy);
            this.groupBox1.Controls.Add(this.btnBCMau15_TongTriGia);
            this.groupBox1.Controls.Add(this.dtpNgayDauKy);
            this.groupBox1.Controls.Add(this.progressBar1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lblXuLy);
            this.groupBox1.Controls.Add(this.lblPercen);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.DarkGreen;
            this.groupBox1.Location = new System.Drawing.Point(0, 435);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1060, 107);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Xử lý dữ liệu";
            // 
            // btnThemHD
            // 
            this.btnThemHD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnThemHD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThemHD.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnThemHD.ForeColor = System.Drawing.Color.White;
            this.btnThemHD.Location = new System.Drawing.Point(225, 53);
            this.btnThemHD.Name = "btnThemHD";
            this.btnThemHD.Size = new System.Drawing.Size(159, 25);
            this.btnThemHD.TabIndex = 9;
            this.btnThemHD.Text = "BƯỚC 1 : CHỌN HĐ     ";
            this.btnThemHD.UseVisualStyleBackColor = false;
            this.btnThemHD.Click += new System.EventHandler(this.btnThemHD_Click);
            // 
            // btnDone
            // 
            this.btnDone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.btnDone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDone.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnDone.ForeColor = System.Drawing.Color.White;
            this.btnDone.Location = new System.Drawing.Point(883, 51);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(174, 49);
            this.btnDone.TabIndex = 10;
            this.btnDone.Text = "BƯỚC 4 : HOÀN THÀNH";
            this.btnDone.UseVisualStyleBackColor = false;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // btnXuatMau15_TriGia
            // 
            this.btnXuatMau15_TriGia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.btnXuatMau15_TriGia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnXuatMau15_TriGia.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatMau15_TriGia.ForeColor = System.Drawing.Color.White;
            this.btnXuatMau15_TriGia.Location = new System.Drawing.Point(575, 76);
            this.btnXuatMau15_TriGia.Name = "btnXuatMau15_TriGia";
            this.btnXuatMau15_TriGia.Size = new System.Drawing.Size(136, 25);
            this.btnXuatMau15_TriGia.TabIndex = 11;
            this.btnXuatMau15_TriGia.Text = "MẪU 15 (TRỊ GIÁ)";
            this.btnXuatMau15_TriGia.UseVisualStyleBackColor = false;
            this.btnXuatMau15_TriGia.Click += new System.EventHandler(this.btnXuatMau15_Click);
            // 
            // btnInBaoCao
            // 
            this.btnInBaoCao.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.btnInBaoCao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInBaoCao.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnInBaoCao.ForeColor = System.Drawing.Color.White;
            this.btnInBaoCao.Location = new System.Drawing.Point(391, 76);
            this.btnInBaoCao.Name = "btnInBaoCao";
            this.btnInBaoCao.Size = new System.Drawing.Size(178, 25);
            this.btnInBaoCao.TabIndex = 13;
            this.btnInBaoCao.Text = "XUẤT BÁO CÁO";
            this.btnInBaoCao.UseVisualStyleBackColor = false;
            this.btnInBaoCao.Click += new System.EventHandler(this.btnInBaoCao_Click);
            // 
            // btnMau15_Luong
            // 
            this.btnMau15_Luong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.btnMau15_Luong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMau15_Luong.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMau15_Luong.ForeColor = System.Drawing.Color.White;
            this.btnMau15_Luong.Location = new System.Drawing.Point(575, 51);
            this.btnMau15_Luong.Name = "btnMau15_Luong";
            this.btnMau15_Luong.Size = new System.Drawing.Size(136, 25);
            this.btnMau15_Luong.TabIndex = 11;
            this.btnMau15_Luong.Text = "MẪU 15 (LƯỢNG)";
            this.btnMau15_Luong.UseVisualStyleBackColor = false;
            this.btnMau15_Luong.Click += new System.EventHandler(this.btnMau15_Luong_Click);
            // 
            // btnGetXNTFromThanhKhoan
            // 
            this.btnGetXNTFromThanhKhoan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnGetXNTFromThanhKhoan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGetXNTFromThanhKhoan.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnGetXNTFromThanhKhoan.ForeColor = System.Drawing.Color.White;
            this.btnGetXNTFromThanhKhoan.Location = new System.Drawing.Point(225, 76);
            this.btnGetXNTFromThanhKhoan.Name = "btnGetXNTFromThanhKhoan";
            this.btnGetXNTFromThanhKhoan.Size = new System.Drawing.Size(160, 25);
            this.btnGetXNTFromThanhKhoan.TabIndex = 9;
            this.btnGetXNTFromThanhKhoan.Text = "BƯỚC 2 : LẤY DỮ LIỆU";
            this.btnGetXNTFromThanhKhoan.UseVisualStyleBackColor = false;
            this.btnGetXNTFromThanhKhoan.Click += new System.EventHandler(this.btnGetXNTFromThanhKhoan_Click);
            // 
            // btnMau15_TongLuong
            // 
            this.btnMau15_TongLuong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnMau15_TongLuong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMau15_TongLuong.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnMau15_TongLuong.ForeColor = System.Drawing.Color.White;
            this.btnMau15_TongLuong.Location = new System.Drawing.Point(717, 51);
            this.btnMau15_TongLuong.Name = "btnMau15_TongLuong";
            this.btnMau15_TongLuong.Size = new System.Drawing.Size(160, 27);
            this.btnMau15_TongLuong.TabIndex = 9;
            this.btnMau15_TongLuong.Text = "MẪU 15 (TỔNG LƯỢNG)";
            this.btnMau15_TongLuong.UseVisualStyleBackColor = false;
            this.btnMau15_TongLuong.Click += new System.EventHandler(this.btnMau15_TongLuong_Click);
            // 
            // btnXuLyXNT_Mau15
            // 
            this.btnXuLyXNT_Mau15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.btnXuLyXNT_Mau15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnXuLyXNT_Mau15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuLyXNT_Mau15.ForeColor = System.Drawing.Color.White;
            this.btnXuLyXNT_Mau15.Location = new System.Drawing.Point(391, 53);
            this.btnXuLyXNT_Mau15.Name = "btnXuLyXNT_Mau15";
            this.btnXuLyXNT_Mau15.Size = new System.Drawing.Size(178, 25);
            this.btnXuLyXNT_Mau15.TabIndex = 12;
            this.btnXuLyXNT_Mau15.Text = "BƯỚC 3 : XỬ LÝ BÁO CÁO";
            this.btnXuLyXNT_Mau15.UseVisualStyleBackColor = false;
            this.btnXuLyXNT_Mau15.Click += new System.EventHandler(this.btnXuLyXNT_Mau15_Click);
            // 
            // dtpNgayCuoiKy
            // 
            this.dtpNgayCuoiKy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dtpNgayCuoiKy.CustomFormat = "dd/MM/yyyy";
            this.dtpNgayCuoiKy.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgayCuoiKy.Location = new System.Drawing.Point(114, 80);
            this.dtpNgayCuoiKy.Name = "dtpNgayCuoiKy";
            this.dtpNgayCuoiKy.Size = new System.Drawing.Size(102, 21);
            this.dtpNgayCuoiKy.TabIndex = 10;
            this.dtpNgayCuoiKy.Value = new System.DateTime(2015, 12, 31, 0, 0, 0, 0);
            // 
            // btnBCMau15_TongTriGia
            // 
            this.btnBCMau15_TongTriGia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnBCMau15_TongTriGia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBCMau15_TongTriGia.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnBCMau15_TongTriGia.ForeColor = System.Drawing.Color.White;
            this.btnBCMau15_TongTriGia.Location = new System.Drawing.Point(717, 76);
            this.btnBCMau15_TongTriGia.Name = "btnBCMau15_TongTriGia";
            this.btnBCMau15_TongTriGia.Size = new System.Drawing.Size(160, 27);
            this.btnBCMau15_TongTriGia.TabIndex = 9;
            this.btnBCMau15_TongTriGia.Text = "MẪU 15 (TỔNG TRỊ GIÁ)";
            this.btnBCMau15_TongTriGia.UseVisualStyleBackColor = false;
            this.btnBCMau15_TongTriGia.Click += new System.EventHandler(this.btnBCMau15Tong_Click);
            // 
            // dtpNgayDauKy
            // 
            this.dtpNgayDauKy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dtpNgayDauKy.CustomFormat = "dd/MM/yyyy";
            this.dtpNgayDauKy.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgayDauKy.Location = new System.Drawing.Point(114, 55);
            this.dtpNgayDauKy.Name = "dtpNgayDauKy";
            this.dtpNgayDauKy.Size = new System.Drawing.Size(102, 21);
            this.dtpNgayDauKy.TabIndex = 10;
            this.dtpNgayDauKy.Value = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.progressBar1.Location = new System.Drawing.Point(11, 16);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(956, 15);
            this.progressBar1.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.DarkGreen;
            this.label7.Location = new System.Drawing.Point(13, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 14);
            this.label7.TabIndex = 8;
            this.label7.Text = "Ngày Cuối Kỳ";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.DarkGreen;
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 14);
            this.label2.TabIndex = 8;
            this.label2.Text = "Ngày Đầu Kỳ";
            // 
            // lblXuLy
            // 
            this.lblXuLy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblXuLy.AutoSize = true;
            this.lblXuLy.BackColor = System.Drawing.Color.Transparent;
            this.lblXuLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblXuLy.ForeColor = System.Drawing.Color.Red;
            this.lblXuLy.Location = new System.Drawing.Point(13, 35);
            this.lblXuLy.Name = "lblXuLy";
            this.lblXuLy.Size = new System.Drawing.Size(149, 13);
            this.lblXuLy.TabIndex = 8;
            this.lblXuLy.Text = "Chưa xử lý xuất nhập tồn";
            // 
            // lblPercen
            // 
            this.lblPercen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblPercen.AutoSize = true;
            this.lblPercen.BackColor = System.Drawing.Color.Transparent;
            this.lblPercen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPercen.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblPercen.Location = new System.Drawing.Point(973, 16);
            this.lblPercen.Name = "lblPercen";
            this.lblPercen.Size = new System.Drawing.Size(30, 13);
            this.lblPercen.TabIndex = 8;
            this.lblPercen.Text = "0 %";
            // 
            // mnnGridDM
            // 
            this.mnnGridDM.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripGridDM});
            this.mnnGridDM.Name = "mnuGrid";
            this.mnnGridDM.Size = new System.Drawing.Size(128, 26);
            // 
            // toolStripGridDM
            // 
            this.toolStripGridDM.Name = "toolStripGridDM";
            this.toolStripGridDM.Size = new System.Drawing.Size(127, 22);
            this.toolStripGridDM.Text = "Xuất Excel";
            this.toolStripGridDM.Click += new System.EventHandler(this.toolStripGridDM_Click);
            // 
            // mnuHopDong
            // 
            this.mnuHopDong.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuXuatEXHopDong});
            this.mnuHopDong.Name = "mnuGrid";
            this.mnuHopDong.Size = new System.Drawing.Size(128, 26);
            // 
            // toolStripMenuXuatEXHopDong
            // 
            this.toolStripMenuXuatEXHopDong.Name = "toolStripMenuXuatEXHopDong";
            this.toolStripMenuXuatEXHopDong.Size = new System.Drawing.Size(127, 22);
            this.toolStripMenuXuatEXHopDong.Text = "Xuất Excel";
            this.toolStripMenuXuatEXHopDong.Click += new System.EventHandler(this.toolStripMenuXuatEXHopDong_Click);
            // 
            // uiTab1
            // 
            this.uiTab1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiTab1.Location = new System.Drawing.Point(-3, 213);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(1060, 216);
            this.uiTab1.TabIndex = 4;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage4,
            this.uiTabPage1,
            this.uiTabPage2,
            this.uiTabPage3});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            this.uiTab1.SelectedTabChanged += new Janus.Windows.UI.Tab.TabEventHandler(this.uiTab1_SelectedTabChanged);
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.Controls.Add(this.dgListHD);
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(1058, 194);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "DANH SÁCH HỢP ĐỒNG";
            // 
            // dgListHD
            // 
            this.dgListHD.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListHD.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListHD.ColumnAutoResize = true;
            this.dgListHD.ContextMenuStrip = this.mnuHopDong;
            dgListHD_DesignTimeLayout.LayoutString = resources.GetString("dgListHD_DesignTimeLayout.LayoutString");
            this.dgListHD.DesignTimeLayout = dgListHD_DesignTimeLayout;
            this.dgListHD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListHD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListHD.FrozenColumns = 5;
            this.dgListHD.GroupByBoxVisible = false;
            this.dgListHD.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHD.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHD.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListHD.Location = new System.Drawing.Point(0, 0);
            this.dgListHD.Name = "dgListHD";
            this.dgListHD.RecordNavigator = true;
            this.dgListHD.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListHD.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListHD.Size = new System.Drawing.Size(1058, 194);
            this.dgListHD.TabIndex = 5;
            this.dgListHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListHD.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.dgListHD_DeletingRecord);
            this.dgListHD.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgListHD_DeletingRecords);
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.dgList);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(1058, 194);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "TỔNG HỢP NPL NHẬP";
            // 
            // dgList
            // 
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ContextMenuStrip = this.mnuGrid;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 5;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(1058, 194);
            this.dgList.TabIndex = 1;
            this.dgList.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.dgDinhMuc_SanPham);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(1058, 194);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "TỔNG HỢP NPL TÍNH THEO ĐỊNH MỨC";
            // 
            // dgDinhMuc_SanPham
            // 
            this.dgDinhMuc_SanPham.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgDinhMuc_SanPham.ContextMenuStrip = this.mnnGridDM;
            dgDinhMuc_SanPham_DesignTimeLayout.LayoutString = resources.GetString("dgDinhMuc_SanPham_DesignTimeLayout.LayoutString");
            this.dgDinhMuc_SanPham.DesignTimeLayout = dgDinhMuc_SanPham_DesignTimeLayout;
            this.dgDinhMuc_SanPham.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgDinhMuc_SanPham.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgDinhMuc_SanPham.FrozenColumns = 5;
            this.dgDinhMuc_SanPham.GroupByBoxVisible = false;
            this.dgDinhMuc_SanPham.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgDinhMuc_SanPham.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgDinhMuc_SanPham.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgDinhMuc_SanPham.Location = new System.Drawing.Point(0, 0);
            this.dgDinhMuc_SanPham.Name = "dgDinhMuc_SanPham";
            this.dgDinhMuc_SanPham.RecordNavigator = true;
            this.dgDinhMuc_SanPham.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgDinhMuc_SanPham.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgDinhMuc_SanPham.Size = new System.Drawing.Size(1058, 194);
            this.dgDinhMuc_SanPham.TabIndex = 3;
            this.dgDinhMuc_SanPham.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgDinhMuc_SanPham.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.dgListTotal);
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(1058, 194);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "TỔNG HỢP SỐ LIỆU BÁO CÁO";
            // 
            // dgListTotal
            // 
            this.dgListTotal.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTotal.AlternatingColors = true;
            this.dgListTotal.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTotal.ColumnAutoResize = true;
            this.dgListTotal.ContextMenuStrip = this.contextMenuStrip1;
            dgListTotal_DesignTimeLayout.LayoutString = resources.GetString("dgListTotal_DesignTimeLayout.LayoutString");
            this.dgListTotal.DesignTimeLayout = dgListTotal_DesignTimeLayout;
            this.dgListTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTotal.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTotal.FrozenColumns = 3;
            this.dgListTotal.GroupByBoxVisible = false;
            this.dgListTotal.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTotal.Location = new System.Drawing.Point(0, 0);
            this.dgListTotal.Margin = new System.Windows.Forms.Padding(0);
            this.dgListTotal.Name = "dgListTotal";
            this.dgListTotal.RecordNavigator = true;
            this.dgListTotal.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTotal.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTotal.Size = new System.Drawing.Size(1058, 194);
            this.dgListTotal.TabIndex = 11;
            this.dgListTotal.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSave,
            this.cmdFeedback,
            this.cmdResult,
            this.cmdSendEdit});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.Id = new System.Guid("db3f14b4-f472-45b2-95e5-5df0dc360a22");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(100, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend1,
            this.cmdSave2,
            this.cmdFeedback2,
            this.cmdResult2});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(411, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdSave2
            // 
            this.cmdSave2.Key = "cmdSave";
            this.cmdSave2.Name = "cmdSave2";
            // 
            // cmdFeedback2
            // 
            this.cmdFeedback2.Key = "cmdFeedback";
            this.cmdFeedback2.Name = "cmdFeedback2";
            // 
            // cmdResult2
            // 
            this.cmdResult2.Key = "cmdResult";
            this.cmdResult2.Name = "cmdResult2";
            // 
            // cmdSend
            // 
            this.cmdSend.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSendEdit1});
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "KHAI BÁO";
            // 
            // cmdSendEdit1
            // 
            this.cmdSendEdit1.Image = ((System.Drawing.Image)(resources.GetObject("cmdSendEdit1.Image")));
            this.cmdSendEdit1.Key = "cmdSendEdit";
            this.cmdSendEdit1.Name = "cmdSendEdit1";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "LƯU LẠI";
            // 
            // cmdFeedback
            // 
            this.cmdFeedback.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedback.Image")));
            this.cmdFeedback.Key = "cmdFeedback";
            this.cmdFeedback.Name = "cmdFeedback";
            this.cmdFeedback.Text = "NHẬN PHẢN HỒI";
            // 
            // cmdResult
            // 
            this.cmdResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult.Image")));
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "KẾT QUẢ XỬ LÝ";
            // 
            // cmdSendEdit
            // 
            this.cmdSendEdit.Key = "cmdSendEdit";
            this.cmdSendEdit.Name = "cmdSendEdit";
            this.cmdSendEdit.Text = "KHAI BÁO SỬA";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1060, 28);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(103)))));
            this.uiGroupBox1.Controls.Add(this.dateNgayTN);
            this.uiGroupBox1.Controls.Add(this.cbbDVT);
            this.uiGroupBox1.Controls.Add(this.cbbLoaiHinh);
            this.uiGroupBox1.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.txtGhiChu);
            this.uiGroupBox1.Controls.Add(this.txtNamQT);
            this.uiGroupBox1.Controls.Add(this.lblTrangThai);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.ForeColor = System.Drawing.Color.DarkGreen;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1060, 163);
            this.uiGroupBox1.TabIndex = 5;
            this.uiGroupBox1.Text = "THÔNG TIN CHUNG";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dateNgayTN
            // 
            this.dateNgayTN.Enabled = false;
            this.dateNgayTN.Location = new System.Drawing.Point(405, 25);
            this.dateNgayTN.Name = "dateNgayTN";
            this.dateNgayTN.ReadOnly = false;
            this.dateNgayTN.Size = new System.Drawing.Size(127, 21);
            this.dateNgayTN.TabIndex = 26;
            this.dateNgayTN.TagName = "";
            this.dateNgayTN.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateNgayTN.WhereCondition = "";
            // 
            // cbbDVT
            // 
            this.cbbDVT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbDVT.FlatBorderColor = System.Drawing.SystemColors.Control;
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.FormatStyle.ForeColor = System.Drawing.Color.Green;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Trị giá";
            uiComboBoxItem1.Value = "1";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.FormatStyle.ForeColor = System.Drawing.Color.Blue;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Số lượng";
            uiComboBoxItem2.Value = "2";
            this.cbbDVT.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbbDVT.Location = new System.Drawing.Point(717, 90);
            this.cbbDVT.Name = "cbbDVT";
            this.cbbDVT.Size = new System.Drawing.Size(138, 21);
            this.cbbDVT.TabIndex = 25;
            this.cbbDVT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            // 
            // cbbLoaiHinh
            // 
            this.cbbLoaiHinh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiHinh.FlatBorderColor = System.Drawing.SystemColors.Control;
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.FormatStyle.ForeColor = System.Drawing.Color.Green;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Gia công";
            uiComboBoxItem3.Value = "1";
            this.cbbLoaiHinh.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3});
            this.cbbLoaiHinh.Location = new System.Drawing.Point(405, 90);
            this.cbbLoaiHinh.Name = "cbbLoaiHinh";
            this.cbbLoaiHinh.Size = new System.Drawing.Size(138, 21);
            this.cbbLoaiHinh.TabIndex = 25;
            this.cbbLoaiHinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Enabled = false;
            this.txtSoTiepNhan.Location = new System.Drawing.Point(130, 25);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(129, 21);
            this.txtSoTiepNhan.TabIndex = 24;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(130, 131);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(916, 21);
            this.txtGhiChu.TabIndex = 20;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // txtNamQT
            // 
            this.txtNamQT.Location = new System.Drawing.Point(130, 90);
            this.txtNamQT.Name = "txtNamQT";
            this.txtNamQT.Size = new System.Drawing.Size(129, 21);
            this.txtNamQT.TabIndex = 20;
            this.txtNamQT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(714, 33);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(87, 13);
            this.lblTrangThai.TabIndex = 19;
            this.lblTrangThai.Text = "Chưa khai báo";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(578, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Trạng thái: ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(288, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Ngày tiếp nhận";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(578, 94);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Đơn vị tính báo cáo :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(288, 94);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Loại hình báo cáo :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 94);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(103, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "Năm quyết toán :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(23, 135);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Ghi chú :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(22, 65);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "Hải quan";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(22, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "Số tiếp nhận";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(103)))));
            this.uiGroupBox2.Controls.Add(this.ReadExcelFile);
            this.uiGroupBox2.Controls.Add(this.btnAdd);
            this.uiGroupBox2.Controls.Add(this.lblFileName);
            this.uiGroupBox2.Controls.Add(this.label20);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.ForeColor = System.Drawing.Color.DarkGreen;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 163);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1060, 44);
            this.uiGroupBox2.TabIndex = 6;
            this.uiGroupBox2.Text = "THÔNG TIN CHUNG";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(638, 13);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(138, 25);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "THÊM FILE";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Location = new System.Drawing.Point(127, 20);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(0, 13);
            this.lblFileName.TabIndex = 17;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(23, 20);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 13);
            this.label20.TabIndex = 17;
            this.label20.Text = "File Name :";
            // 
            // ReadExcelFile
            // 
            this.ReadExcelFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.ReadExcelFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ReadExcelFile.Font = new System.Drawing.Font("Tahoma", 9F);
            this.ReadExcelFile.ForeColor = System.Drawing.Color.White;
            this.ReadExcelFile.Location = new System.Drawing.Point(782, 14);
            this.ReadExcelFile.Name = "ReadExcelFile";
            this.ReadExcelFile.Size = new System.Drawing.Size(265, 25);
            this.ReadExcelFile.TabIndex = 9;
            this.ReadExcelFile.Text = "ĐỌC BÁO CÁO QUYẾT TOÁN TỪ FILE EXCEL";
            this.ReadExcelFile.UseVisualStyleBackColor = false;
            this.ReadExcelFile.Click += new System.EventHandler(this.ReadExcelFile_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuExportExcel});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(141, 26);
            // 
            // mnuExportExcel
            // 
            this.mnuExportExcel.Name = "mnuExportExcel";
            this.mnuExportExcel.Size = new System.Drawing.Size(140, 22);
            this.mnuExportExcel.Text = "XUẤT EXCEL";
            this.mnuExportExcel.Click += new System.EventHandler(this.mnuExportExcel_Click);
            // 
            // XuatNhapTonForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1060, 570);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "XuatNhapTonForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "BÁO CÁO QUYẾT TOÁN HỢP ĐỒNG";
            this.Load += new System.EventHandler(this.XuatNhapTonForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.mnuGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPL)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.mnnGridDM.ResumeLayout(false);
            this.mnuHopDong.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListHD)).EndInit();
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgDinhMuc_SanPham)).EndInit();
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEXPrintDocument gridEXPrintDocument1;
        private System.Windows.Forms.ContextMenuStrip mnuGrid;
        private System.Windows.Forms.ToolStripMenuItem toolStripXuatExcel_NPL;
        private Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenChuHang2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton btnTimKiemNPL;
        private Janus.Windows.GridEX.GridEX dgListNPL;
        private System.Windows.Forms.ImageList imageList1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtpNgayDauKy;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblXuLy;
        private System.Windows.Forms.Label lblPercen;
        private System.Windows.Forms.ContextMenuStrip mnnGridDM;
        private System.Windows.Forms.ToolStripMenuItem toolStripGridDM;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.GridEX.GridEX dgDinhMuc_SanPham;
        private System.Windows.Forms.ContextMenuStrip mnuHopDong;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuXuatEXHopDong;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpNgayCuoiKy;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.GridEX.GridEX dgListTotal;
        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave2;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback2;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult2;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendEdit1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendEdit;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayTN;
        private Janus.Windows.EditControls.UIComboBox cbbDVT;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiHinh;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.GridEX.EditControls.EditBox txtNamQT;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Button btnMau15_TongLuong;
        private System.Windows.Forms.Button btnInBaoCao;
        private System.Windows.Forms.Button btnXuLyXNT_Mau15;
        private System.Windows.Forms.Button btnMau15_Luong;
        private System.Windows.Forms.Button btnXuatMau15_TriGia;
        private System.Windows.Forms.Button btnBCMau15_TongTriGia;
        private System.Windows.Forms.Button btnThemHD;
        private System.Windows.Forms.Button btnGetXNTFromThanhKhoan;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private Janus.Windows.GridEX.GridEX dgListHD;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button ReadExcelFile;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuExportExcel;

    }
}