﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.GC.BLL.KDT
{
    public partial class ThanhKhoan
    {

        public List<HangThanhKhoan> HangCollection { get; set; }
        public void LoadHangCollection()
        {

            HangCollection = (List<HangThanhKhoan>)HangThanhKhoan.SelectCollectionDynamic("Master_ID=" + this.ID, ""); 
        }
    }
}
