﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu.GCCT;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface
{
    public partial class ChungTuKemFormGCCT : BaseForm
    {
        public ToKhaiChuyenTiep TKCT;
        public ChungTuKemAnh CTK = new ChungTuKemAnh();
        public List<ChungTuKemAnhChiTiet> ListCTDK = new List<ChungTuKemAnhChiTiet>();
        public bool isKhaiBoSung = false;
        public bool isAddNew = true;

        string filebase64 = "";
        long filesize = 0;
        public ChungTuKemFormGCCT()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Lay tong dung luong cac file dinh kem
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private void HienThiTongDungLuong(List<ChungTuKemAnhChiTiet> list)
        {
            long size = 0;

            for (int i = 0; i < list.Count; i++)
            {
                size += Convert.ToInt64(list[i].FileSize);
            }


            //hien thi tong dung luong file
            lblTongDungLuong.Text = Convert.ToString(size / (1024 * 1024)) + " MB (" + size.ToString() + " Bytes)";

            lblLuuY.Text = Convert.ToString(GlobalSettings.FileSize / (1024 * 1024)) + " MB";
        }

        private void BindData()
        {
            dgList.DataSource = ListCTDK;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }

            //Cap nhat thong tin tong dung luong file.
            HienThiTongDungLuong(ListCTDK);

        }

        private void GiayPhepForm_Load(object sender, EventArgs e)
        {
            //Ngay chung tu mac dinh
            ccNgayChungTu.Text = DateTime.Today.ToShortDateString();

            //Thiet lap nut Khai bao, Phan hoi mac dinh
            btnKhaiBao.Enabled = true;
            btnLayPhanHoi.Enabled = false;

            //Load danh sach Loai chung tu
            DataView dvLCT = Company.KDT.SHARE.Components.DuLieuChuan.LoaiChungTu.SelectAll().Tables[0].DefaultView;
            dvLCT.Sort = "ID ASC";
            cbLoaiCT.Items.Clear();
            for (int i = 0; i < dvLCT.Count; i++)
            {
                cbLoaiCT.Items.Add(dvLCT[i]["Ten"].ToString(), (int)dvLCT[i]["ID"]);
            }
            cbLoaiCT.SelectedIndex = cbLoaiCT.Items.Count > 0 ? 0 : -1;

            if (CTK.ID > 0)
            {
                ccNgayChungTu.Text = CTK.NGAY_CT.ToShortDateString();
                txtThongTinKhac.Text = CTK.DIENGIAI;
                txtSoChungTu.Text = CTK.SO_CT;
                cbLoaiCT.SelectedValue = CTK.MA_LOAI_CT;

                ListCTDK = (List<ChungTuKemAnhChiTiet>)ChungTuKemAnhChiTiet.SelectCollectionBy_ChungTuKemAnhID(CTK.ID);

                BindData();
            }
            else
            {
                //Cap nhat thong tin tong dung luong file.
                HienThiTongDungLuong(ListCTDK);
            }

            //if (TKCT.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO && !isKhaiBoSung)
            //{
            if (TKCT.ID <= 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }
            else if (CTK.SOTN > 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                txtSoTiepNhan.Text = CTK.SOTN + "";
                ccNgayTiepNhan.Text = CTK.NGAYTN.ToShortDateString();

            }
            else if (TKCT.SoToKhai > 0 && int.Parse(TKCT.PhanLuong != "" ? TKCT.PhanLuong : "0") == 0)
            {
                btnKhaiBao.Enabled = true;
                btnLayPhanHoi.Enabled = true;
            }
            else if (TKCT.PhanLuong != "")
            {
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //}

            txtSoChungTu.Focus();

            // Thiết lập trạng thái các nút trên form.
            // HUNGTQ, Update 07/06/2010.
            SetButtonStateCHUNGTUKEM(TKCT, isKhaiBoSung, CTK);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<ChungTuKemAnhChiTiet> HangGiayPhepDetailCollection = new List<ChungTuKemAnhChiTiet>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ChungTuKemAnhChiTiet hgpDetail = new ChungTuKemAnhChiTiet();
                        hgpDetail = (ChungTuKemAnhChiTiet)i.GetRow().DataRow;

                        if (hgpDetail == null) continue;

                        //Detele from DB.
                        if (hgpDetail.ID > 0)
                            hgpDetail.Delete();

                        //Remove out Collction
                        ListCTDK.Remove(hgpDetail);
                    }
                }
            }

            BindData();
        }

        private bool checkSoCTK(string soCT)
        {
            int cnt = 0;

            foreach (ChungTuKemAnh gpTMP in TKCT.AnhCollection)
            {
                if (gpTMP.SO_CT.Trim().ToUpper() == soCT.Trim().ToUpper())
                    cnt += 1;
            }

            if (isAddNew == true)//Neu la them moi chung tu, co 1 ket qua tra ve
                return cnt > 0;
            else
                return cnt > 1;//Neu la chinh sua chung tu, co hon 1 ket qua tra ve la 2, tuc bo qua chung tu dang sua.
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;

            if (TKCT.ID <= 0)
            {
                ShowMessage("Vui lòng nhập tờ khai trước", false);
                return;
            }
            if (ListCTDK.Count == 0)
            {
                ShowMessage("Chưa chọn file đính kèm", false);
                btnAddNew.Focus();
                return;
            }
            //Cap nhat lai danh sach file dinh kem chi tiet
            CTK.ListChungTuKemAnhChiTiet = ListCTDK;

            //TKCT.AnhCollection.Remove(chungTuKem);

            if (checkSoCTK(txtSoChungTu.Text))
            {
                ShowMessage("Chung tu da ton tai", false);
                return;
            }

            CTK.SO_CT = txtSoChungTu.Text.Trim();
            CTK.DIENGIAI = txtThongTinKhac.Text;
            CTK.MA_LOAI_CT = cbLoaiCT.SelectedValue.ToString();
            CTK.NGAY_CT = ccNgayChungTu.Value;
            CTK.ID_TK = TKCT.ID;
            CTK.LoaiTK = "TKCT";
            //Khai bo sung
            if (isKhaiBoSung)
                CTK.LoaiKB = "1";
            else
                CTK.LoaiKB = "0";

            try
            {
                //CTK.InsertUpdateFull(ListCTDK);
                CTK.InsertUpdateFull();
                if (isAddNew == true)
                    TKCT.AnhCollection.Add(CTK);

                BindData();

                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
                return;
            }

        }

        static public string EncodeTo64(string toEncode)
        {

            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);

            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);

            return returnValue;

        }

        static public string DecodeFrom64(string encodedData)
        {

            byte[] encodedDataAsBytes

                = System.Convert.FromBase64String(encodedData);

            string returnValue =

               System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);

            return returnValue;

        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            openFileDialog1.Filter = "JPEG (*.jpg; *.jpeg)|*.jpg;*.jpeg|TIFF (*.tif; *.tiff)|*.tif;*.tiff";
            openFileDialog1.Multiselect = true;

            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    for (int k = 0; k < openFileDialog1.FileNames.Length; k++)
                    {
                        System.IO.FileInfo fin = new System.IO.FileInfo(openFileDialog1.FileNames[k]);
                        System.IO.FileStream fs = new System.IO.FileStream(openFileDialog1.FileNames[k], System.IO.FileMode.Open, System.IO.FileAccess.Read);

                        //Cap nhat tong dung luong file.
                        long size = 0;

                        for (int i = 0; i < ListCTDK.Count; i++)
                        {
                            size += Convert.ToInt64(ListCTDK[i].FileSize);
                        }

                        //+ them dung luong file moi chuan bi them vao danh sach
                        size += fs.Length;

                        //Kiem tra dung luong file
                        if (size > GlobalSettings.FileSize)
                        {
                            this.ShowMessage("Tổng dung lượng các file đính kèm vượt quá dung lượng cho phép.", false);
                            return;
                        }

                        byte[] data = new byte[fs.Length];
                        fs.Read(data, 0, data.Length);
                        filebase64 = System.Convert.ToBase64String(data);
                        filesize = fs.Length;

                        /*
                         * truoc khi them moi file dinh kem vao danh sach, phai kiem tra tong dung luong co hop len khong?.
                         * Neu > dung luong choh phep -> Hien thi thong bao va khong them vao danh sach.
                         */
                        ChungTuKemAnhChiTiet ctctiet = new ChungTuKemAnhChiTiet();
                        ctctiet.ChungTuKemAnhID = CTK.ID;
                        ctctiet.FileName = fin.Name;
                        ctctiet.FileSize = filesize;
                        ctctiet.NoiDung = data;

                        ListCTDK.Add(ctctiet);
                    }

                    dgList.DataSource = ListCTDK;
                    try
                    {
                        dgList.Refetch();
                    }
                    catch { dgList.Refresh(); }

                    HienThiTongDungLuong(ListCTDK);

                }
            }
            catch (Exception ex) { }
        }

        private void btnChonGP_Click(object sender, EventArgs e)
        {
            //kiem tra thu da co muc temp?. Neu chua co -> tao 1 thu muc temp de chua cac file tam thoi.
            string path = Application.StartupPath + "\\Temp";

            if (System.IO.Directory.Exists(path) == false)
            {
                System.IO.Directory.CreateDirectory(path);
            }
            if (dgList.RowCount > 0)
            {
                //Giai ma basecode64 -> file & luu tam vao thu muc Temp moi tao.
                ChungTuKemAnhChiTiet fileData = (ChungTuKemAnhChiTiet)dgList.GetRow().DataRow;
                string fileName = path + "\\" + fileData.FileName;

                //Ghi file
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }

                System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                fs.Write(fileData.NoiDung, 0, fileData.NoiDung.Length);
                fs.Close();

                System.Diagnostics.Process.Start(fileName);
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            try
            {
                if (CTK.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO.ToString() || CTK.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET.ToString())
                    CTK.GUIDSTR = Guid.NewGuid().ToString();
                else if (CTK.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI.ToString())
                {
                    ShowMessage("Bạn đã gửi yêu cầu khai báo bổ đến hải quan\r\nVui lòng nhận phản hồi thông tin", false);
                    return;
                }
                #region V3

                ObjectSend msgSend = SingleMessage.BoSungChungTuGCCT(TKCT, CTK, GlobalSettings.TEN_DON_VI);

                CTK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO.ToString();
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                bool isSend = sendMessageForm.DoSend(msgSend);
                if (isSend)
                {
                    SetButtonStateCHUNGTUKEM(TKCT, isKhaiBoSung, CTK);
                    sendMessageForm.Message.XmlSaveMessage(CTK.ID, MessageTitle.KhaiBaoBoSungChungTuDinhKem);
                }
                if ((CTK.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO.ToString() ||
                    CTK.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI.ToString()) && isSend)
                {
                    CTK.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI.ToString();
                    CTK.Update();
                    SetButtonStateCHUNGTUKEM(TKCT, isKhaiBoSung, CTK);
                }

                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKCT.MaHaiQuanTiepNhan, new SendEventArgs(ex));
            }
        }

      

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            ObjectSend msgSend = SingleMessage.FeedBackGCCT(TKCT, CTK.GUIDSTR);
            SendMessageForm sendMessageForm = new SendMessageForm();
            sendMessageForm.Send += SendMessage;
            sendMessageForm.DoSend(msgSend);
            SetButtonStateCHUNGTUKEM(TKCT, isKhaiBoSung, CTK);
            //LayThongTinPhanHoi();
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void btnXoaChungTu_Click(object sender, EventArgs e)
        {
            if (CTK.ID > 0)
            {
                if (ShowMessage("Bạn có muốn xóa chứng từ này không?", true) == "Yes")
                {

                    //Xoa chung tu chi tiet
                    CTK.LoadListChungTuKemAnhChiTiet();

                    for (int i = 0; i < CTK.ListChungTuKemAnhChiTiet.Count; i++)
                    {
                        CTK.ListChungTuKemAnhChiTiet[i].Delete();
                    }

                    //Xoa chung tu trong DB
                    CTK.Delete();

                    //Xoa chung tu trong Collection
                    TKCT.AnhCollection.Remove(CTK);

                    //Load lai danh sach chung tu kem cho TK
                    TKCT.LoadChungTuKem();
                }
            }

            //Dong form sau khi xoa chung tu.
            this.Close();
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form CHUNGTUKEM.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private void SetButtonStateCHUNGTUKEM(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh chungTuKem)
        {
            if (chungTuKem == null)
                return;

            bool status = false;
            btnXemFile.Enabled = true;
            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                    || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY);

                btnXoa.Enabled = status;
                btnXoaChungTu.Enabled = status;
                btnAddNew.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else
            {
                if (chungTuKem.SOTN > 0)
                {
                    txtSoTiepNhan.Text = chungTuKem.SOTN.ToString();
                    ccNgayTiepNhan.Value = chungTuKem.NGAYTN;
                }
                btnKetQuaXuLy.Enabled = true;
                bool khaiBaoEnable = (int.Parse(chungTuKem.TrangThaiXuLy) == TrangThaiXuLy.CHUA_KHAI_BAO ||
                                     int.Parse(chungTuKem.TrangThaiXuLy) == TrangThaiXuLy.KHONG_PHE_DUYET) && chungTuKem.SOTN == 0;

                btnKhaiBao.Enabled = btnXoa.Enabled = btnXoaChungTu.Enabled = btnAddNew.Enabled = btnGhi.Enabled = khaiBaoEnable;

                bool layPhanHoi = int.Parse(chungTuKem.TrangThaiXuLy) == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ||
                                   int.Parse(chungTuKem.TrangThaiXuLy) == TrangThaiXuLy.DA_DUYET ||
                                   int.Parse(chungTuKem.TrangThaiXuLy) == TrangThaiXuLy.CHO_DUYET;
                btnLayPhanHoi.Enabled = layPhanHoi;
            }
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (CTK.GUIDSTR != null && CTK.GUIDSTR != "")
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = CTK.ID;
                form.DeclarationIssuer = AdditionalDocumentType.CHUNG_TU_DANG_ANH;
                form.ShowDialog(this);
            }
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);

        }
        #region  V3

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    FeedBackContent feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function)
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                noidung = SingleMessage.GetErrorContent(feedbackContent);

                                e.FeedBackMessage.XmlSaveMessage(CTK.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                CTK.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET.ToString();
                                this.ShowMessageTQDT("Hải quan từ chối tiếp nhận", noidung, false);
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            noidung = noidung.Replace(string.Format("Message [{0}]", CTK.GUIDSTR), string.Empty);
                            this.ShowMessage(string.Format("Thông báo từ hệ thống hải quan : {0}", noidung), false);
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                string[] vals = noidung.Split('/');

                                CTK.SOTN = long.Parse(vals[0].Trim());
                                //CTK. = int.Parse(vals[1].Trim());
                                CTK.NGAYTN = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                                e.FeedBackMessage.XmlSaveMessage(CTK.ID, MessageTitle.KhaiBaoBoSungChungTuDinhKemCoSoTN, noidung);
                                CTK.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET.ToString();
                                txtSoTiepNhan.Text = CTK.SOTN.ToString();
                                ccNgayTiepNhan.Value = CTK.NGAYTN;
                                this.ShowMessageTQDT("Được cấp số tiếp nhận.\n" + CTK.SOTN.ToString() + "\n" + CTK.NGAYTN.ToString(), false);
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                e.FeedBackMessage.XmlSaveMessage(CTK.ID, MessageTitle.KhaiBaoBoSungChungTuDinhKemDuocChapNhan, noidung);
                                CTK.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET.ToString();
                                ShowMessageTQDT("Chứng từ bổ sung đã được duyệt", false);
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(CTK.ID, MessageTitle.Error, noidung);
                                break;
                            }
                    }
                    if (isUpdate)
                        CTK.Update();

                }
                else
                {

                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }

        #endregion
    }
}

