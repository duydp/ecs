﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.VNACCS
{
    public class MIME
    {
        public MIMEHeader Header { get; set; }
        public StringBuilder ContentEdi { get; set; }
        public MIMEPartSign SignPart { get; set; }
        public string boundary { get; set; }
        public MIME()
        {

        }
        public MIME(string Signbyte, string Host, string Length)
        {
            boundary = "------" + Guid.NewGuid().ToString().Replace("-","");
            Header = new MIMEHeader(Host, Length, this.boundary);
            SignPart = new MIMEPartSign(this.boundary, Signbyte);
        }
        public StringBuilder GetMessages()
        {
            StringBuilder str = new StringBuilder();
            //str.Append(Header.ToEDI(new StringBuilder()));
            //str.AppendLine();
            str.Append(this.boundary).AppendLine();
            str.Append(this.ContentEdi);
            str.AppendLine();
            str.Append(SignPart.ToEDI());
            return str;
        }

    }
}
