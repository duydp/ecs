﻿using System.Configuration;
using System.Windows.Forms;
using System;
using System.Xml;
using System.Collections.Generic;
using System.IO;

namespace SaoChepChuyenDaDuyetMaHQKhac
{
    public class GlobalSettings
    {
        public static string DATABASE_NAME;
        public static string USER;
        public static string PASS;
        public static string SERVER_NAME;

        public static string PassWordDT = "";
        public static string UserId = "";
        public static bool IsRemember = false;

        public static string MA_CUC_HAI_QUAN;
        public static string TEN_CUC_HAI_QUAN;

        public static string MA_HAI_QUAN;
        public static string TEN_HAI_QUAN;

        public static string TEN_DON_VI;
        public static string MA_DON_VI;
        public static string MailDoanhNghiep;

        public static string SoDienThoaiDN;
        public static string SoFaxDN;
        public static string NguoiLienHe;

        public static void RefreshKey()
        {
            try
            {
                /*DATLMQ update Đọc dữ liệu từ file Config 19/01/2011.*/
                XmlDocument doc = new XmlDocument();
                string path = AppDomain.CurrentDomain.BaseDirectory + "ConfigDoanhNghiep";
                //Hungtq update 28/01/2011.
                string fileName = GlobalSettings.GetFileName(path);

                if (fileName == "")
                    return;

                doc.Load(fileName);

                //HUNGTQ Updated 07/06/2011
                //Get thong tin Server
                SERVER_NAME = GlobalSettings.GetNodeConfigDN(doc, "Server").InnerText;
                //Get thong tin Database
                DATABASE_NAME = GlobalSettings.GetNodeConfigDN(doc, "Database").InnerText;
                //Get thong tin UserName
                USER = GlobalSettings.GetNodeConfigDN(doc, "UserName").InnerText;
                //Get thong tin PassWord
                PASS = GlobalSettings.GetNodeConfigDN(doc, "Password").InnerText;

                //Get thông tin MaCucHQ
                MA_CUC_HAI_QUAN = GlobalSettings.GetNodeConfigDN(doc, "MaCucHQ").InnerText;
                //Get thông tin TenCucHQ
                TEN_CUC_HAI_QUAN = GlobalSettings.GetNodeConfigDN(doc, "TenCucHQ").InnerText;
                //Get thông tin MaChiCucHQ
                MA_HAI_QUAN = GlobalSettings.GetNodeConfigDN(doc, "MaChiCucHQ").InnerText;
                //Get thông tin TenChiCucHQ
                TEN_HAI_QUAN = GlobalSettings.GetNodeConfigDN(doc, "TenChiCucHQ").InnerText;

                //Get thông tin MaDoanhNghiep
                MA_DON_VI = GlobalSettings.GetNodeConfigDN(doc, "MaDoanhNghiep").InnerText.Trim();
                //Get thông tin TenDoanhNghiep
                TEN_DON_VI = GlobalSettings.GetNodeConfigDN(doc, "TenDoanhNghiep").InnerText;

                MailDoanhNghiep = GlobalSettings.GetNodeConfigDN(doc, "MailDoanhNghiep").InnerText;
                SoDienThoaiDN = GlobalSettings.GetNodeConfigDN(doc, "DienThoai").InnerText;
                SoFaxDN = GlobalSettings.GetNodeConfigDN(doc, "Fax").InnerText;
                NguoiLienHe = GlobalSettings.GetNodeConfigDN(doc, "NguoiLienHe").InnerText;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        public static bool ValidateNull(DonViHaiQuanControl control, ErrorProvider err, string fieldName)
        {
            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' không được để trống.";

            err.SetError(control, "");

            if (control.Ma.Trim().Length == 0)
            {
                isValid = false;
                err.SetError(control, msgErr);
                control.Focus();
            }

            return isValid;
        }

        public static XmlNode GetNodeConfigDN(XmlDocument doc, string key)
        {
            try
            {
                foreach (XmlNode node in doc.GetElementsByTagName("Config"))
                {
                    if (node["Key"].InnerText.Trim().ToLower() == key.Trim().ToLower())
                    {
                        return node["Value"];
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(key, ex); }

            XmlDocument docx = new XmlDocument();
            return docx.CreateNode(XmlNodeType.Element, key, "");
        }

        public static string GetConfig(XmlDocument doc, string key, string defaultValue)
        {
            XmlNode node = GetNodeConfigDN(doc, key);
            if (node == null) return defaultValue;
            else if (string.IsNullOrEmpty(node.InnerText)) return defaultValue;
            else
                return node.InnerText;
        }

        /// <summary>
        /// Lay ten 1 file xml trong danh sach trong 1 folder.
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        public static string GetFileName(string folderPath)
        {
            string[] fileNames = null;
            try
            {

                if (System.IO.Directory.Exists(folderPath))
                {
                    fileNames = System.IO.Directory.GetFiles(folderPath, "*.xml");
                }

                if (fileNames.Length > 0)
                {
                    return fileNames[0]; //Lay file xml dau tien trong danh sach (Neu co nhieu hon 1).
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return "";
        }

        public static void SaveNodeXmlConnectionStrings(string connectionString)
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString = connectionString;
                config.Save(ConfigurationSaveMode.Modified);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        /// <summary>
        /// Đồng bộ lại thông tin từ file cấu hình XMl sang file cấu hình EXE của chương trình.
        /// </summary>
        /// <param name="phanHe">KD, GC, SXXK</param>
        /// <returns></returns>
        public static bool AupdateConfigXMLToExe(string phanHe)
        {
            try
            {
                string pathConfigXML = AppDomain.CurrentDomain.BaseDirectory + "ConfigDoanhNghiep\\ConfigDoanhNghiep.xml";
                string pathConfigEXE = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile.Replace(".vshost", "");

                if (File.Exists(pathConfigXML) && File.Exists(pathConfigEXE))
                {
                    XmlDocument configXML = new XmlDocument();
                    configXML.Load(pathConfigXML);

                    //Service khai bao

                    //Database
                    string S = GetConfig(configXML, "Server", ".\\ECSEXPRESS");
                    string D = GetConfig(configXML, "Database", string.Format("ECS_TQDT_{0}_V4", phanHe));
                    string U = GetConfig(configXML, "UserName", "sa");
                    string P = GetConfig(configXML, "Password", "123456");

                    //Database connectionstring
                    string concatString = "";
                    concatString += "Server=" + S + ";";
                    concatString += "Database=" + D + ";";
                    concatString += "uid=" + U + ";";
                    concatString += "pwd=" + P + ";";
                    SaveNodeXmlConnectionStrings(concatString);

                    //Thong tin Hai quan

                    //Thong tin Doanh nghiep
                }

                return true;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); return false; }
        }


    }
}