﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.Interface.KDT.SXXK;
namespace Company.Interface.KDT.GC
{
    public partial class DinhMucGCRegisterForm : Company.Interface.BaseForm
    {
        public KDT_LenhSanXuat_SP SP = new KDT_LenhSanXuat_SP();
        public Company.GC.BLL.GC.GC_DinhMuc DM = new Company.GC.BLL.GC.GC_DinhMuc();
        public GC_NguyenPhuLieu NPL = new GC_NguyenPhuLieu();
        public bool isAdd = true;
        public DinhMucGCRegisterForm()
        {
            InitializeComponent();
            cbHopDong.DataSource = HopDong.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "ID");
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void DinhMucGCRegisterForm_Load(object sender, EventArgs e)
        {

        }
        private void LoadData()
        {
            try
            {
                cbbMaNPL.DataSource = GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(Convert.ToInt64(cbHopDong.Value));
                cbbMaNPL.DisplayMember = "Ma";
                cbbMaNPL.ValueMember = "Ma";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataSP()
        {
            try
            {
                grListSP.Refetch();
                grListSP.DataSource = KDT_LenhSanXuat_SP.SelectCollectionDynamic("LenhSanXuat_ID = " + cbbLenhSanXuat.Value, "");
                grListSP.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataLenhSanXuat()
        {
            try
            {
                HopDong HD = new HopDong();
                HD = HopDong.Load(Convert.ToInt64(cbHopDong.Value));
                List<KDT_LenhSanXuat> LenhSanXuatCollection = new List<KDT_LenhSanXuat>();
                LenhSanXuatCollection = KDT_LenhSanXuat.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "' AND HopDong_ID = " + HD.ID + "", "ID");
                cbbLenhSanXuat.DataSource = LenhSanXuatCollection;
                cbbLenhSanXuat.DisplayMember = "SoLenhSanXuat";
                cbbLenhSanXuat.ValueMember = "ID";
                if (LenhSanXuatCollection.Count == 0)
                {
                    grListSP.Refetch();
                    grListSP.DataSource = null;
                    grListSP.Refresh();

                    dgListNPL.Refetch();
                    dgListNPL.DataSource = null;
                    dgListNPL.Refresh();

                }
                else
                {
                    cbbLenhSanXuat.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataNPL()
        {
            try
            {
                List<Company.GC.BLL.GC.GC_DinhMuc> DMCollection = new List<Company.GC.BLL.GC.GC_DinhMuc>();
                DMCollection = Company.GC.BLL.GC.GC_DinhMuc.SelectCollectionDynamic("HopDong_ID = " + cbHopDong.Value + " AND MaSanPham ='" + SP.MaSanPham + "' AND LenhSanXuat_ID =" + cbbLenhSanXuat.Value + "", "");
                foreach (Company.GC.BLL.GC.GC_DinhMuc item in DMCollection)
                {
                    if (item.TyLeHaoHut > 0)
                    {
                        item.DinhMucSuDung = item.DinhMucSuDung + item.DinhMucSuDung * item.TyLeHaoHut / 100;
                        item.TyLeHaoHut = 0;
                    }
                }
                dgListNPL.Refetch();
                dgListNPL.DataSource = DMCollection;
                dgListNPL.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                BindDataLenhSanXuat();
                HopDong HD = new HopDong();
                HD = HopDong.Load(Convert.ToInt64(cbHopDong.Value));
                clcNgayHĐ.Value = HD.NgayKy;
                clcNgayHH.Value = HD.NgayGiaHan.Year == 1900 ? HD.NgayHetHan : HD.NgayGiaHan;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(grListSP.RootTable.Columns["MaSanPham"], ConditionOperator.Contains, txtMaSP.Text);
                grListSP.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaNPL_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaNPL.Value != null)
                {
                    List<GC_NguyenPhuLieu> NPLCollection = GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(Convert.ToInt64(cbHopDong.Value));
                    foreach (GC_NguyenPhuLieu item in NPLCollection)
                    {
                        if (item.Ma == cbbMaNPL.Value.ToString())
                        {
                            txtTenNPL.Text = item.Ten.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnCoppy_Click(object sender, EventArgs e)
        {
            try
            {
                if (SP == null)
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN SẢN PHẨM ĐỂ SAO CHÉP ĐỊNH MỨC", false);
                    return;
                }
                if (cbbLenhSanXuat.Value.ToString().Trim() == "")
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN LỆNH SẢN XUẤT CỦA SẢN PHẨM .", false);
                    return;
                }
                SaoChepDinhMucDaDuyetGCForm f = new SaoChepDinhMucDaDuyetGCForm();
                f.SP = SP;
                f.HopDong_ID = Convert.ToInt64(cbHopDong.Value);
                f.ShowDialog(this);
                if (f.DMCollectionCopy.Count > 0)
                {
                    foreach (Company.GC.BLL.GC.DinhMuc item in f.DMCollectionCopy)
                    {
                        DM = new Company.GC.BLL.GC.GC_DinhMuc();
                        DM.HopDong_ID = Convert.ToInt64(cbHopDong.Value);
                        DM.MaSanPham = SP.MaSanPham;
                        DM.TenSanPham = SP.TenSanPham;
                        DM.MaNguyenPhuLieu = item.MaNguyenPhuLieu.ToString();
                        DM.TenNPL = item.TenNPL.ToString();
                        DM.DinhMucSuDung = item.DinhMucSuDung;
                        DM.TyLeHaoHut = item.TyLeHaoHut;
                        DM.LenhSanXuat_ID = Convert.ToInt64(cbbLenhSanXuat.Value);
                        DM.SoTiepNhan = Convert.ToDecimal(txtSoTiepNhan.Text.ToString());
                        DM.NgayTiepNhan = clcNgayTiepNhan.Value;
                        DM.GuidString = txtGuidString.Text.ToString();
                        DM.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        DM.InsertUpdate();
                    }
                    ShowMessageTQDT("Sao chép thành công", false);
                }
                BindDataSP();
                BindDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDeleteSP_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                List<KDT_LenhSanXuat_SP> ItemColl = new List<KDT_LenhSanXuat_SP>();
                if (grListSP.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_LenhSanXuat_SP)i.GetRow().DataRow);
                        }
                    }
                    foreach (KDT_LenhSanXuat_SP item in ItemColl)
                    {
                        List<Company.GC.BLL.GC.GC_DinhMuc> DMCollection = Company.GC.BLL.GC.GC_DinhMuc.SelectCollectionDynamic("HopDong_ID = " + cbHopDong.Value + "AND MaSanPham ='" + item.MaSanPham + "' AND LenhSanXuat_ID = " + cbbLenhSanXuat.Value + "", "");
                        foreach (Company.GC.BLL.GC.GC_DinhMuc itemss in DMCollection)
                        {
                            if (itemss.TrangThaiXuLy != TrangThaiXuLy.DA_DUYET)
                            {
                                itemss.Delete();
                            }
                            else
                            {
                                ShowMessageTQDT("ĐỊNH MỨC CỦA SẢN PHẨM NÀY ĐÃ KHAI BÁO ĐẾN HQ .DOANH NGHIỆP KHÔNG ĐƯỢC PHÉP XÓA", false);
                                return;
                            }
                        }
                    }
                    BindDataSP();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetData()
        {
            try
            {
                DM.HopDong_ID = Convert.ToInt64(cbHopDong.Value);
                DM.MaSanPham = SP.MaSanPham;
                DM.TenSanPham = SP.TenSanPham;
                DM.MaNguyenPhuLieu = cbbMaNPL.Value.ToString();
                DM.TenNPL = txtTenNPL.Text;
                DM.DinhMucSuDung = Convert.ToDecimal(txtDinhMuc.Text.ToString());
                DM.TyLeHaoHut = Convert.ToDecimal(txtTyLeHH.Text.ToString());
                DM.LenhSanXuat_ID = Convert.ToInt64(cbbLenhSanXuat.Value);
                DM.SoTiepNhan = Convert.ToDecimal(txtSoTiepNhan.Text.ToString());
                DM.NgayTiepNhan = clcNgayTiepNhan.Value;
                DM.GuidString = txtGuidString.Text.ToString();
                string Status = DM.TrangThaiXuLy.ToString();
                switch (Status)
                {
                    case "1":
                        DM.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void setCommandStatus()
        {
            if (DM.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || DM.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                txtSoTiepNhan.Text = DM.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = DM.NgayTiepNhan;
                txtGuidString.Text = DM.GuidString.ToString().ToUpper();
                lblTrangThai.Text = DM.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET ? setText("Đã duyệt", "Approved") : setText("Chờ duyệt", "Wait Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (DM.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || DM.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {

                clcNgayTiepNhan.Value = DateTime.Now;
                lblTrangThai.Text = DM.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                txtGuidString.Text = DM.GuidString.ToString().ToUpper();
                this.OpenType = OpenFormType.Edit;
            }
            else if (DM.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                txtSoTiepNhan.Text = DM.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = DM.NgayTiepNhan;
                txtGuidString.Text = DM.GuidString.ToString().ToUpper();
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
            }
            else if (DM.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || DM.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || DM.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {


                lblTrangThai.Text = setText("Đã khai báo", "Not declared");
                txtSoTiepNhan.Text = DM.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = DM.NgayTiepNhan;
                txtGuidString.Text = DM.GuidString.ToString().ToUpper();
                this.OpenType = OpenFormType.View;
            }
            else if (DM.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                txtSoTiepNhan.Text = DM.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = DM.NgayTiepNhan;
                txtGuidString.Text = DM.GuidString.ToString().ToUpper();
                lblTrangThai.Text = setText("Đang sửa ", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (DM.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {

                txtSoTiepNhan.Text = DM.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = DM.NgayTiepNhan;
                txtGuidString.Text = DM.GuidString.ToString().ToUpper();
                lblTrangThai.Text = setText("Đang hủy", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }

        }
        private void SetData()
        {
            try
            {
                cbbMaNPL.Value = DM.MaNguyenPhuLieu;
                txtTenNPL.Text = DM.TenNPL;
                txtDinhMuc.Text = DM.DinhMucSuDung.ToString();
                txtTyLeHH.Text = DM.TyLeHaoHut.ToString();
                cbbLenhSanXuat.Value = DM.LenhSanXuat_ID;
                //txtSoTiepNhan.Text = DM.SoTiepNhan.ToString();
                //clcNgayTiepNhan.Value = DM.NgayTiepNhan.Year == 1900 ? DateTime.Now : DM.NgayTiepNhan;
                //txtGuidString.Text = DM.GuidString.ToString().ToUpper();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateChoose(cbbMaNPL, errorProvider, "MÃ NGUYÊN PHỤ LIỆU", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNPL, errorProvider, "TÊN/MÔ TẢ NGUYÊN PHỤ LIỆU", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDinhMuc, errorProvider, "ĐỊNH MỨC SỬ DỤNG", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbbLenhSanXuat, errorProvider, "LỆNH SẢN XUẤT", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetData();
                DM.InsertUpdate();
                DM = new Company.GC.BLL.GC.GC_DinhMuc();
                cbbMaNPL.Text = String.Empty;
                txtTenNPL.Text = String.Empty;
                txtDinhMuc.Text = String.Empty;
                BindDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgListNPL.SelectedItems;
                List<Company.GC.BLL.GC.GC_DinhMuc> ItemColl = new List<Company.GC.BLL.GC.GC_DinhMuc>();
                if (dgListNPL.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((Company.GC.BLL.GC.GC_DinhMuc)i.GetRow().DataRow);
                        }
                    }
                    foreach (Company.GC.BLL.GC.GC_DinhMuc item in ItemColl)
                    {
                        if (item.TrangThaiXuLy != TrangThaiXuLy.DA_DUYET)
                        {
                            item.Delete();
                        }
                        else
                        {
                            ShowMessageTQDT("ĐỊNH MỨC NÀY ĐÃ KHAI BÁO ĐẾN HQ . DOANH NGHIỆP KHÔNG ĐƯỢC PHÉP XÓA ĐỊNH MỨC NÀY .", false);
                            return;
                        }
                    }
                    BindDataNPL();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void grListSP_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SP = new KDT_LenhSanXuat_SP();
                        SP = (KDT_LenhSanXuat_SP)i.GetRow().DataRow;
                        Janus.Windows.GridEX.GridEXFormatStyle style = new Janus.Windows.GridEX.GridEXFormatStyle();
                        List<Company.GC.BLL.GC.GC_DinhMuc> DMCollection = new List<Company.GC.BLL.GC.GC_DinhMuc>();
                        DMCollection = Company.GC.BLL.GC.GC_DinhMuc.SelectCollectionDynamic("HopDong_ID = " + cbHopDong.Value + " AND MaSanPham ='" + SP.MaSanPham + "' AND LenhSanXuat_ID =" + cbbLenhSanXuat.Value + "", "");
                        if (DMCollection.Count == 0)
                        {
                            style.BackColor = Color.GreenYellow;
                            i.GetRow().Cells["MaSanPham"].FormatStyle = style;
                        }
                        else
                        {
                            txtSoTiepNhan.Text = DMCollection[0].SoTiepNhan.ToString();
                            clcNgayTiepNhan.Value = DMCollection[0].NgayTiepNhan.Year == 1900 || DMCollection[0].NgayTiepNhan.Year == 1 ? DateTime.Now : DMCollection[0].NgayTiepNhan;
                            txtGuidString.Text = DMCollection[0].GuidString.ToString().ToUpper();

                            string TrangThai = DMCollection[0].TrangThaiXuLy.ToString();
                            switch (TrangThai)
                            {
                                case "-1":
                                    style.BackColor = Color.Yellow;
                                    i.GetRow().Cells["MaSanPham"].FormatStyle = style;
                                    lblTrangThai.Text = TrangThaiXuLy.strCHUAKHAIBAO;
                                    break;
                                case "0":
                                    style.BackColor = Color.Transparent;
                                    i.GetRow().Cells["MaSanPham"].FormatStyle = style;
                                    lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;
                                    break;
                                case "1":
                                    style.BackColor = Color.White;
                                    i.GetRow().Cells["MaSanPham"].FormatStyle = style;
                                    lblTrangThai.Text = TrangThaiXuLy.strDADUYET;
                                    break;
                                case "5":
                                    style.BackColor = Color.Violet;
                                    i.GetRow().Cells["MaSanPham"].FormatStyle = style;
                                    lblTrangThai.Text = TrangThaiXuLy.strSUATKDADUYET;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                BindDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListNPL_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgListNPL.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DM = new Company.GC.BLL.GC.GC_DinhMuc();
                        DM = (Company.GC.BLL.GC.GC_DinhMuc)i.GetRow().DataRow;
                    }
                }
                SetData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListSP_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                    SP = new KDT_LenhSanXuat_SP();
                    SP = (KDT_LenhSanXuat_SP)e.Row.DataRow;
                    Janus.Windows.GridEX.GridEXFormatStyle style = new Janus.Windows.GridEX.GridEXFormatStyle();
                    List<Company.GC.BLL.GC.GC_DinhMuc> DMCollection = new List<Company.GC.BLL.GC.GC_DinhMuc>();
                    DMCollection = Company.GC.BLL.GC.GC_DinhMuc.SelectCollectionDynamic("HopDong_ID = " + cbHopDong.Value + " AND MaSanPham ='" + SP.MaSanPham + "' AND LenhSanXuat_ID =" + cbbLenhSanXuat.Value + "", "");
                    if (DMCollection.Count == 0)
                    {
                        style.BackColor = Color.GreenYellow;
                        e.Row.Cells["MaSanPham"].FormatStyle = style;
                    }
                    else
                    {
                        string TrangThai = DMCollection[0].TrangThaiXuLy.ToString();
                        switch (TrangThai)
                        {
                            case "-1":
                                style.BackColor = Color.Yellow;
                                e.Row.Cells["MaSanPham"].FormatStyle = style;
                                break;
                            case "0":
                                style.BackColor = Color.Transparent;
                                e.Row.Cells["MaSanPham"].FormatStyle = style;
                                break;
                            case "1":
                                style.BackColor = Color.White;
                                e.Row.Cells["MaSanPham"].FormatStyle = style;
                                break;
                            case "5":
                                style.BackColor = Color.Violet;
                                e.Row.Cells["MaSanPham"].FormatStyle = style;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListNPL_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    DM = new Company.GC.BLL.GC.GC_DinhMuc();
                    DM = (Company.GC.BLL.GC.GC_DinhMuc)e.Row.DataRow;
                    SetData();
                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbLenhSanXuat_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                BindDataSP();
                LoadData();
                if (cbbLenhSanXuat.Value != null)
                {
                    KDT_LenhSanXuat lenhSX = new KDT_LenhSanXuat();
                    lenhSX = KDT_LenhSanXuat.Load(Convert.ToInt64(cbbLenhSanXuat.Value));
                    if (lenhSX.ID > 0)
                    {
                        clcTuNgay.Value = lenhSX.TuNgay;
                        clcDenNgay.Value = lenhSX.DenNgay;
                        txtPO.Text = lenhSX.SoDonHang;
                        cbbTinhTrang.SelectedValue = lenhSX.TinhTrang;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                KDT_LenhSanXuat lenhSX = new KDT_LenhSanXuat();
                lenhSX = KDT_LenhSanXuat.Load(Convert.ToInt64(cbbLenhSanXuat.Value));
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH CHI TIẾT ĐỊNH MỨC SẢN PHẨM CỦA LỆNH SẢN XUẤT (" + lenhSX.SoLenhSanXuat.ToString() + ").xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    grListSP.Tables[0].Columns.Add(new GridEXColumn("MaNguyenPhuLieu", ColumnType.Text, EditType.NoEdit) { Caption = "MÃ NPL" });
                    grListSP.Tables[0].Columns.Add(new GridEXColumn("TenNPL", ColumnType.Text, EditType.NoEdit) { Caption = "TÊN NPL" });
                    grListSP.Tables[0].Columns.Add(new GridEXColumn("DinhMucSuDung", ColumnType.Text, EditType.NoEdit) { Caption = "ĐỊNH MỨC SỬ DỤNG" });
                    grListSP.Tables[0].Columns.Add(new GridEXColumn("TyLeHaoHut", ColumnType.Text, EditType.NoEdit) { Caption = "TỶ LỆ HAO HỤT" });
                    grListSP.Tables[0].Columns.Add(new GridEXColumn("NPL_TuCungUng", ColumnType.Text, EditType.NoEdit) { Caption = "NPL TỰ CUNG ỨNG" });
                    grListSP.Refresh();
                    grListSP.DataSource = Company.GC.BLL.GC.GC_DinhMuc.SelectCollectionDynamic("HopDong_ID = " + cbHopDong.Value + " AND LenhSanXuat_ID =" + cbbLenhSanXuat.Value + "", "");
                    grListSP.Refetch();
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = grListSP;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                    grListSP.Tables[0].Columns.Remove("MaNguyenPhuLieu");
                    grListSP.Tables[0].Columns.Remove("TenNPL");
                    grListSP.Tables[0].Columns.Remove("DinhMucSuDung");
                    grListSP.Tables[0].Columns.Remove("TyLeHaoHut");
                    grListSP.Tables[0].Columns.Remove("NPL_TuCungUng");
                    BindDataSP();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH ĐỊNH MỨC CỦA SẢN PHẨM (" + SP.MaSanPham.ToString() + ").xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgListNPL;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbbLenhSanXuat.Value == null || cbbLenhSanXuat.Value.ToString().Trim() == "")
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN LỆNH SẢN XUẤT CỦA SẢN PHẨM .", false);
                    return;
                }
                List<KDT_LenhSanXuat_SP> SPCollection = new List<KDT_LenhSanXuat_SP>();
                SPCollection = KDT_LenhSanXuat_SP.SelectCollectionDynamic("LenhSanXuat_ID = " + cbbLenhSanXuat.Value, "");
                if (SPCollection.Count == 0)
                {
                    ShowMessage("LỆNH SẢN XUẤT NÀY CHƯA CÓ DANH SÁCH SẢN PHẨM . DOANH NGHIỆP HÃY CẬP NHẬT DANH SÁCH SẢN PHẨM CHO LỆNH SẢN XUẤT NÀY.", false);
                    return;
                }
                ImportKDTDMForm f = new ImportKDTDMForm();
                DinhMucDangKy dmDangKy = new DinhMucDangKy();
                dmDangKy.ID_HopDong = Convert.ToInt64(cbHopDong.Value);
                Company.GC.BLL.KDT.GC.HopDong hd = new Company.GC.BLL.KDT.GC.HopDong();
                hd = Company.GC.BLL.KDT.GC.HopDong.Load(dmDangKy.ID_HopDong);
                f.HD = hd;
                f.dmDangKy = dmDangKy;
                f.ShowDialog();
                if (f.dmDangKy.DMCollection.Count > 0)
                {
                    foreach (Company.GC.BLL.KDT.GC.DinhMuc item in f.dmDangKy.DMCollection)
                    {
                        foreach (KDT_LenhSanXuat_SP ite in SPCollection)
                        {
                            if (ite.MaSanPham == item.MaSanPham)
                            {
                                DM = new Company.GC.BLL.GC.GC_DinhMuc();
                                DM.HopDong_ID = Convert.ToInt64(cbHopDong.Value);
                                DM.MaSanPham = item.MaSanPham;
                                DM.TenSanPham = item.TenSanPham;
                                DM.MaNguyenPhuLieu = item.MaNguyenPhuLieu.ToString();
                                DM.TenNPL = item.TenNPL;
                                DM.DinhMucSuDung = item.DinhMucSuDung;
                                DM.TyLeHaoHut = item.TyLeHaoHut;
                                DM.LenhSanXuat_ID = Convert.ToInt64(cbbLenhSanXuat.Value);
                                DM.SoTiepNhan = Convert.ToDecimal(txtSoTiepNhan.Text.ToString());
                                DM.NgayTiepNhan = clcNgayTiepNhan.Value;
                                DM.GuidString = txtGuidString.Text.ToString();
                                DM.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                DM.InsertUpdate();
                            }
                        }
                    }
                    ShowMessageTQDT("NHẬP ĐỊNH MỨC TỪ EXCEL THÀNH CÔNG ", false);
                }
                BindDataSP();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnIpExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (SP == null || String.IsNullOrEmpty(SP.MaSanPham.ToString()))
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN SẢN PHẨM ĐỂ NHẬP ĐỊNH MỨC TỪ FILE EXCEL", false);
                    return;
                }
                ImportKDTDMForm f = new ImportKDTDMForm();
                DinhMucDangKy dmDangKy = new DinhMucDangKy();
                dmDangKy.ID_HopDong = Convert.ToInt64(cbHopDong.Value);
                Company.GC.BLL.KDT.GC.HopDong hd = new Company.GC.BLL.KDT.GC.HopDong();
                hd = Company.GC.BLL.KDT.GC.HopDong.Load(dmDangKy.ID_HopDong);
                f.HD = hd;
                f.dmDangKy = dmDangKy;
                f.ShowDialog();
                if (f.dmDangKy.DMCollection.Count > 0)
                {
                    foreach (Company.GC.BLL.KDT.GC.DinhMuc item in f.dmDangKy.DMCollection)
                    {
                        // KIỂM TRA CHỈ NHẬP EXCEL CHO MÃ SP ĐÃ CHỌN BỎ QUA CÁC MÃ KHÁC NẾU CÓ
                        if (item.MaSanPham == SP.MaSanPham)
                        {
                            DM = new Company.GC.BLL.GC.GC_DinhMuc();
                            DM.HopDong_ID = Convert.ToInt64(cbHopDong.Value);
                            DM.MaSanPham = item.MaSanPham;
                            DM.TenSanPham = item.TenSanPham;
                            DM.MaNguyenPhuLieu = item.MaNguyenPhuLieu.ToString();
                            DM.TenNPL = item.TenNPL;
                            DM.DinhMucSuDung = item.DinhMucSuDung;
                            DM.TyLeHaoHut = item.TyLeHaoHut;
                            DM.LenhSanXuat_ID = Convert.ToInt64(cbbLenhSanXuat.Value);
                            DM.SoTiepNhan = Convert.ToDecimal(txtSoTiepNhan.Text.ToString());
                            DM.NgayTiepNhan = clcNgayTiepNhan.Value;
                            DM.GuidString = txtGuidString.Text.ToString();
                            DM.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                            DM.InsertUpdate();
                        }
                    }
                    ShowMessageTQDT("NHẬP ĐỊNH MỨC TỪ EXCEL THÀNH CÔNG ", false);
                }
                BindDataSP();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
    }
}
