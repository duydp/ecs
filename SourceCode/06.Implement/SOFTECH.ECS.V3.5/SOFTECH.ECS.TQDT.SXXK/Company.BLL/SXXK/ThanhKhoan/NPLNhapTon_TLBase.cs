using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.BLL.SXXK.ThanhKhoan
{
	public partial class NPLNhapTon_TL : EntityBase
	{
		#region Private members.
		
		protected int _LanThanhLy;
		protected string _MaDoanhNghiep = String.Empty;
		protected int _SoToKhai;
		protected string _MaLoaiHinh = String.Empty;
		protected short _NamDangKy;
		protected string _MaHaiQuan = String.Empty;
		protected string _MaNPL = String.Empty;
		protected decimal _Luong;
		protected decimal _TonDau;
		protected decimal _TonCuoi;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public int LanThanhLy
		{
			set {this._LanThanhLy = value;}
			get {return this._LanThanhLy;}
		}
		public string MaDoanhNghiep
		{
			set {this._MaDoanhNghiep = value;}
			get {return this._MaDoanhNghiep;}
		}
		public int SoToKhai
		{
			set {this._SoToKhai = value;}
			get {return this._SoToKhai;}
		}
		public string MaLoaiHinh
		{
			set {this._MaLoaiHinh = value;}
			get {return this._MaLoaiHinh;}
		}
		public short NamDangKy
		{
			set {this._NamDangKy = value;}
			get {return this._NamDangKy;}
		}
		public string MaHaiQuan
		{
			set {this._MaHaiQuan = value;}
			get {return this._MaHaiQuan;}
		}
		public string MaNPL
		{
			set {this._MaNPL = value;}
			get {return this._MaNPL;}
		}
		public decimal Luong
		{
			set {this._Luong = value;}
			get {return this._Luong;}
		}
		public decimal TonDau
		{
			set {this._TonDau = value;}
			get {return this._TonDau;}
		}
		public decimal TonCuoi
		{
			set {this._TonCuoi = value;}
			get {return this._TonCuoi;}
		}
		
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public bool Load()
		{
			string spName = "p_SXXK_ThanhLy_NPLNhapTon_TL_Load";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) this._LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) this._Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TonDau"))) this._TonDau = reader.GetDecimal(reader.GetOrdinal("TonDau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TonCuoi"))) this._TonCuoi = reader.GetDecimal(reader.GetOrdinal("TonCuoi"));
				return true;
			}
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public NPLNhapTon_TLCollection SelectCollectionBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan()
		{
			string spName = "p_SXXK_ThanhLy_NPLNhapTon_TL_SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			
			NPLNhapTon_TLCollection collection = new NPLNhapTon_TLCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			while (reader.Read())
			{
				NPLNhapTon_TL entity = new NPLNhapTon_TL();
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TonDau"))) entity.TonDau = reader.GetDecimal(reader.GetOrdinal("TonDau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TonCuoi"))) entity.TonCuoi = reader.GetDecimal(reader.GetOrdinal("TonCuoi"));
				collection.Add(entity);
			}
			return collection;
		}

		//---------------------------------------------------------------------------------------------
		public NPLNhapTon_TLCollection SelectCollectionBy_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan_AND_MaNPL()
		{
			string spName = "p_SXXK_ThanhLy_NPLNhapTon_TL_SelectBy_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan_AND_MaNPL";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			
			NPLNhapTon_TLCollection collection = new NPLNhapTon_TLCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			while (reader.Read())
			{
				NPLNhapTon_TL entity = new NPLNhapTon_TL();
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TonDau"))) entity.TonDau = reader.GetDecimal(reader.GetOrdinal("TonDau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TonCuoi"))) entity.TonCuoi = reader.GetDecimal(reader.GetOrdinal("TonCuoi"));
				collection.Add(entity);
			}
			return collection;
		}

		//---------------------------------------------------------------------------------------------

		public DataSet SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan()
		{
			string spName = "p_SXXK_ThanhLy_NPLNhapTon_TL_SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
						
            return this.db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------
		public DataSet SelectBy_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan_AND_MaNPL()
		{
			string spName = "p_SXXK_ThanhLy_NPLNhapTon_TL_SelectBy_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan_AND_MaNPL";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
						
            return this.db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public DataSet SelectAll()
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TL_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TL_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TL_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TL_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		public NPLNhapTon_TLCollection SelectCollectionAll()
		{
			NPLNhapTon_TLCollection collection = new NPLNhapTon_TLCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				NPLNhapTon_TL entity = new NPLNhapTon_TL();
				
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TonDau"))) entity.TonDau = reader.GetDecimal(reader.GetOrdinal("TonDau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TonCuoi"))) entity.TonCuoi = reader.GetDecimal(reader.GetOrdinal("TonCuoi"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public NPLNhapTon_TLCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			NPLNhapTon_TLCollection collection = new NPLNhapTon_TLCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				NPLNhapTon_TL entity = new NPLNhapTon_TL();
				
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TonDau"))) entity.TonDau = reader.GetDecimal(reader.GetOrdinal("TonDau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TonCuoi"))) entity.TonCuoi = reader.GetDecimal(reader.GetOrdinal("TonCuoi"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_SXXK_ThanhLy_NPLNhapTon_TL_Insert";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			this.db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
			this.db.AddInParameter(dbCommand, "@TonDau", SqlDbType.Decimal, this._TonDau);
			this.db.AddInParameter(dbCommand, "@TonCuoi", SqlDbType.Decimal, this._TonCuoi);
			
			if (transaction != null)
			{
				return this.db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return this.db.ExecuteNonQuery(dbCommand);
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(NPLNhapTon_TLCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (NPLNhapTon_TL item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, NPLNhapTon_TLCollection collection)
        {
            foreach (NPLNhapTon_TL item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_SXXK_ThanhLy_NPLNhapTon_TL_InsertUpdate";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			this.db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
			this.db.AddInParameter(dbCommand, "@TonDau", SqlDbType.Decimal, this._TonDau);
			this.db.AddInParameter(dbCommand, "@TonCuoi", SqlDbType.Decimal, this._TonCuoi);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(NPLNhapTon_TLCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (NPLNhapTon_TL item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_SXXK_ThanhLy_NPLNhapTon_TL_Update";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			this.db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
			this.db.AddInParameter(dbCommand, "@TonDau", SqlDbType.Decimal, this._TonDau);
			this.db.AddInParameter(dbCommand, "@TonCuoi", SqlDbType.Decimal, this._TonCuoi);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(NPLNhapTon_TLCollection collection, SqlTransaction transaction)
        {
            foreach (NPLNhapTon_TL item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_SXXK_ThanhLy_NPLNhapTon_TL_Delete";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
        public int DeleteDynamicTransaction(SqlTransaction transaction, string whereCondition)
        {
            string spName = "p_SXXK_ThanhLy_NPLNhapTon_TL_DeleteDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.VarChar, whereCondition);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(NPLNhapTon_TLCollection collection, SqlTransaction transaction)
        {
            foreach (NPLNhapTon_TL item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(NPLNhapTon_TLCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (NPLNhapTon_TL item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		#endregion

        public static void Updatebase(DataSet ds, SqlTransaction transaction)
        {
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                NPLNhapTon_TL nplNhapTonTL = new NPLNhapTon_TL();
                nplNhapTonTL.LanThanhLy = Convert.ToInt32(row["LAN_TL"].ToString());
                nplNhapTonTL.Luong = Convert.ToDecimal(row["LUONG"]);
                nplNhapTonTL.MaDoanhNghiep = (row["MA_DV"].ToString());
                nplNhapTonTL.MaHaiQuan = (row["MA_HQ"].ToString());
                nplNhapTonTL.MaLoaiHinh = (row["MA_LH"].ToString());
                nplNhapTonTL.MaNPL = (row["MA_NPL"].ToString());
                nplNhapTonTL.NamDangKy = Convert.ToInt16(row["NAMDK"].ToString());
                nplNhapTonTL.SoToKhai = Convert.ToInt32(row["SOTK"]);
                nplNhapTonTL.TonCuoi = Convert.ToDecimal(row["TON"]);
                nplNhapTonTL.TonDau = Convert.ToDecimal(row["TON_DAU"]);
                nplNhapTonTL.InsertUpdateTransaction(transaction);
            }
        }
        public static void Updatebase(DataSet ds)
        {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                {
                    connection.Open();
                    SqlTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        Updatebase(ds, transaction);
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
        }
	}	
}