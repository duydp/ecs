﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
#endif

using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface
{
    public partial class GiayNopTienChungTuForm : BaseForm
    {
        public GiayNopTienChungTu ChungTu = new GiayNopTienChungTu();
        bool _isEdit = false;
        public GiayNopTienChungTuForm(bool isEdit)
        {
            _isEdit = isEdit;
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (ChungTu.ID > 0 && ShowMessage("Bạn có muốn xóa thông tin không?", true) != "Yes") return;

            try
            {
                ChungTu.Delete();
                DialogResult = DialogResult.No;
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(GlobalSettings.MA_HAI_QUAN, new SendEventArgs(ex));
            }
        }
        private void Set()
        {
            cbLoaiChungTu.SelectedValue = ChungTu.LoaiChungTu;
            txtSoChungTu.Text = ChungTu.SoChungTu;
            txtTenChungTu.Text = ChungTu.TenChungTu;
            ccNgayPhatHanh.Text = ChungTu.NgayPhatHanh.ToLongDateString();

        }
        private void Get()
        {
            ChungTu.LoaiChungTu = cbLoaiChungTu.SelectedValue.ToString();
            ChungTu.SoChungTu = txtSoChungTu.Text;
            ChungTu.TenChungTu = txtTenChungTu.Text;
            ChungTu.NgayPhatHanh = ccNgayPhatHanh.Value;

        }
        private void GiayNopTienChungTuForm_Load(object sender, EventArgs e)
        {
            cbLoaiChungTu.DataSource = LoaiChungTu.SelectAll().Tables[0];
            btnGhi.Enabled = btnXoa.Enabled = _isEdit;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            bool isValid = ValidateControl.ValidateNull(cbLoaiChungTu, error, "Loại chứng từ")
                && ValidateControl.ValidateNull(txtSoChungTu, error, "Số chứng từ")
                && ValidateControl.ValidateDate(ccNgayPhatHanh, error, "Ngày phát hành");
            if (!isValid) return;
            try
            {
                Get();
                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(GlobalSettings.MA_HAI_QUAN, new SendEventArgs(ex));
            }
        }


    }
}
