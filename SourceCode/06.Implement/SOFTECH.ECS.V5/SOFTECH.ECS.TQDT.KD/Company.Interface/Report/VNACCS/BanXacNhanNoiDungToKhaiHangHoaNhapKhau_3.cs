using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.Report.VNACCS
{
    public partial class BanXacNhanNoiDungToKhaiHangHoaNhapKhau_3 : DevExpress.XtraReports.UI.XtraReport
    {
        public BanXacNhanNoiDungToKhaiHangHoaNhapKhau_3()
        {
            InitializeComponent();
        }

        private void xrLabel2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }
        public void BindingReport(VAD1AC0 vad1ac)
        {
            lblSoToKhai.Text = vad1ac.ICN.GetValue().ToString().ToUpper();

            lblSoToKhaiDauTien.Text = vad1ac.FIC.GetValue().ToString().ToUpper();
            lblSoNhanhToKhaiChiaNho.Text = vad1ac.BNO.GetValue().ToString().ToUpper();
            lblTongSoToKhaiChiaNho.Text = vad1ac.DNO.GetValue().ToString().ToUpper();
            lblSoToKhaiTamNhapTaiXuat.Text = vad1ac.TDN.GetValue().ToString().ToUpper();
            lblMaPhanLoaiKiemTra.Text = vad1ac.A06.GetValue().ToString().ToUpper();
            lblMaLoaiHinh.Text = vad1ac.ICB.GetValue().ToString().ToUpper();
            lblMaPhanLoaiHangHoa.Text = vad1ac.CCC.GetValue().ToString().ToUpper();
            lblMaHieuPhuongThucVanChuyen.Text = vad1ac.MTC.GetValue().ToString().ToUpper();
            lblPhanLoaiCaNhanToChuc.Text = vad1ac.SKB.GetValue().ToString().ToUpper();
            lblMaSoHangHoaDaiDienToKhai.Text = vad1ac.A00.GetValue().ToString().ToUpper();
            lblTenCoQuanHaiQuanTiepNhanToKhai.Text = vad1ac.A07.GetValue().ToString().ToUpper();
            lblMaBoPhanXuLyToKhai.Text = vad1ac.CHB.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vad1ac.A09.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayDangKy.Text = Convert.ToDateTime(vad1ac.A09.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayDangKy.Text = "";
            }
            if (vad1ac.AD1.GetValue().ToString().ToUpper() != "" && vad1ac.AD1.GetValue().ToString().ToUpper() != "0")
            {
                lblGioDangKy.Text = vad1ac.AD1.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ac.AD1.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ac.AD1.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad1ac.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayThayDoiDangKy.Text = Convert.ToDateTime(vad1ac.AD2.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayThayDoiDangKy.Text = "";
            }
            if (vad1ac.AD3.GetValue().ToString().ToUpper() != "" && vad1ac.AD3.GetValue().ToString().ToUpper() != "0")
            {
                lblGioThayDoiDangKy.Text = vad1ac.AD3.GetValue().ToString().ToUpper().Substring(0, 2) + ":" + vad1ac.AD3.GetValue().ToString().ToUpper().Substring(2, 2) + ":" + vad1ac.AD3.GetValue().ToString().ToUpper().Substring(4, 2);
            }
            else
            {
                lblGioThayDoiDangKy.Text = "";
            }
            if (Convert.ToDateTime(vad1ac.RED.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoiHanTaiNhapTaiXuat.Text = Convert.ToDateTime(vad1ac.RED.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoiHanTaiNhapTaiXuat.Text = "";
            }
            lblBieuThiTHHetHan.Text = vad1ac.AAA.GetValue().ToString().ToUpper();
            BindingReportHang(vad1ac.HangMD[0]);
        }

        public void BindingReportHang(VAD1AC0_HANG vad1ac_Hang)
        {
            lblSoDong.Text = vad1ac_Hang.B25.GetValue().ToString().ToUpper();

            lblMaSoHangHoa.Text = vad1ac_Hang.CMD.GetValue().ToString().ToUpper();
            lblMaQuanLyRieng.Text = vad1ac_Hang.GZC.GetValue().ToString().ToUpper();
            lblMaPhanLoaiTaiXacNhanGia.Text = vad1ac_Hang.B29.GetValue().ToString().ToUpper();
            lblMoTaHangHoa.Text = vad1ac_Hang.CMN.GetValue().ToString().ToUpper();
            lblSoLuong1.Text = vad1ac_Hang.QN1.GetValue().ToString().ToUpper();
            lblMaDonViTinh1.Text = vad1ac_Hang.QT1.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad1ac_Hang.VN_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblSoMucKhaiKhoangDieuChinh1.Text = vad1ac_Hang.VN_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblSoMucKhaiKhoangDieuChinh2.Text = vad1ac_Hang.VN_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblSoMucKhaiKhoangDieuChinh3.Text = vad1ac_Hang.VN_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblSoMucKhaiKhoangDieuChinh4.Text = vad1ac_Hang.VN_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblSoMucKhaiKhoangDieuChinh5.Text = vad1ac_Hang.VN_.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
            lblSoLuong2.Text = vad1ac_Hang.QN2.GetValue().ToString().ToUpper();
            lblMaDonViTinh2.Text = vad1ac_Hang.QT2.GetValue().ToString().ToUpper();
            lblTriGiaHoaDon.Text = vad1ac_Hang.BPR.GetValue().ToString().ToUpper();
            lblDonGiaHoaDon.Text = vad1ac_Hang.UPR.GetValue().ToString().ToUpper();
            lblMaDongTienCuaDonGia.Text = vad1ac_Hang.UPC.GetValue().ToString().ToUpper();
            lblDonViCuaDonGiaVaSoLuong.Text = vad1ac_Hang.TSC.GetValue().ToString().ToUpper();
            lblTriGiaTinhThueS.Text = vad1ac_Hang.B36.GetValue().ToString().ToUpper();
            lblMaDongTienCuaGiaTinhThue.Text = vad1ac_Hang.B50.GetValue().ToString().ToUpper();
            lblTriGiaTinhThueM.Text = vad1ac_Hang.B51.GetValue().ToString().ToUpper();
            lblSoLuongTinhThue.Text = vad1ac_Hang.B37.GetValue().ToString().ToUpper();
            lblMaDonViTinhChuanDanhThue.Text = vad1ac_Hang.B38.GetValue().ToString().ToUpper();
            lblDonGiaTinhThue.Text = vad1ac_Hang.KKT.GetValue().ToString().ToUpper();
            lblDonViSoLuongTrongDonGiaTinhThue.Text = vad1ac_Hang.KKS.GetValue().ToString().ToUpper();
            lblMaPhanLoaiThueSuat.Text = vad1ac_Hang.B42.GetValue().ToString().ToUpper();
            lblThueSuatThueNhapKhau.Text = vad1ac_Hang.B43.GetValue().ToString().ToUpper();
            lblPhanLoaiNhapThueSuat.Text = vad1ac_Hang.SKB.GetValue().ToString().ToUpper();
            lblMaXacDinhMucThueNhapKhau.Text = vad1ac_Hang.SPD.GetValue().ToString().ToUpper();
            lblSoTienThueNK.Text = vad1ac_Hang.B47.GetValue().ToString().ToUpper();
            lblMaNuocXuatXu.Text = vad1ac_Hang.OR.GetValue().ToString().ToUpper();
            lblTenNoiXuatXu.Text = vad1ac_Hang.B56.GetValue().ToString().ToUpper();
            lblMaBieuThueNK.Text = vad1ac_Hang.ORS.GetValue().ToString().ToUpper();
            lblSoTienGiamThueNK.Text = vad1ac_Hang.B49.GetValue().ToString().ToUpper();
            lblMaNgoaiHanNgach.Text = vad1ac_Hang.KWS.GetValue().ToString().ToUpper();
            lblSoThuTuDongHangToKhai.Text = vad1ac_Hang.TDL.GetValue().ToString().ToUpper();
            lblSoDKDanhMucMienThue.Text = vad1ac_Hang.TXN.GetValue().ToString().ToUpper();
            lblSoDongTuongUngDMMienThue.Text = vad1ac_Hang.TXR.GetValue().ToString().ToUpper();
            lblMaMienGiamThueNK.Text = vad1ac_Hang.RE.GetValue().ToString().ToUpper();
            lblDieuKhoanMienGiam.Text = vad1ac_Hang.B59.GetValue().ToString().ToUpper();
            for (int i = 0; i < vad1ac_Hang.KQ1.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblTenKhoanMucThueVaThuKhac1.Text = vad1ac_Hang.KQ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaApDungThueSuatThueVaThuKhac1.Text = vad1ac_Hang.KQ1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaTinhThueVaThuKhac1.Text = vad1ac_Hang.KQ1.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblSoLuongTinhThueVaThuKhac1.Text = vad1ac_Hang.KQ1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        lblMaDonViTinhChuanDanhThueVaThuKhac1.Text = vad1ac_Hang.KQ1.listAttribute[4].GetValueCollection(i).ToString().ToUpper();
                        lblThueSuatThueVaThuKhac1.Text = vad1ac_Hang.KQ1.listAttribute[5].GetValueCollection(i).ToString().ToUpper();
                        lblSoTienThueVaThuKhac1.Text = vad1ac_Hang.KQ1.listAttribute[6].GetValueCollection(i).ToString().ToUpper();
                        lblMaMienGiamThueVaThuKhac1.Text = vad1ac_Hang.KQ1.listAttribute[7].GetValueCollection(i).ToString().ToUpper();
                        lblDieuKhoanMienGiamThueVaThuKhac1.Text = vad1ac_Hang.KQ1.listAttribute[8].GetValueCollection(i).ToString().ToUpper();
                        lblSoTienGiamThueVaThuKhac1.Text = vad1ac_Hang.KQ1.listAttribute[9].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 1:
                        lblTenKhoanMucThueVaThuKhac2.Text = vad1ac_Hang.KQ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaApDungThueSuatThueVaThuKhac2.Text = vad1ac_Hang.KQ1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaTinhThueVaThuKhac2.Text = vad1ac_Hang.KQ1.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblSoLuongTinhThueVaThuKhac2.Text = vad1ac_Hang.KQ1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        lblMaDonViTinhChuanDanhThueVaThuKhac2.Text = vad1ac_Hang.KQ1.listAttribute[4].GetValueCollection(i).ToString().ToUpper();
                        lblThueSuatThueVaThuKhac2.Text = vad1ac_Hang.KQ1.listAttribute[5].GetValueCollection(i).ToString().ToUpper();
                        lblSoTienThueVaThuKhac2.Text = vad1ac_Hang.KQ1.listAttribute[6].GetValueCollection(i).ToString().ToUpper();
                        lblMaMienGiamThueVaThuKhac2.Text = vad1ac_Hang.KQ1.listAttribute[7].GetValueCollection(i).ToString().ToUpper();
                        lblDieuKhoanMienGiamThueVaThuKhac2.Text = vad1ac_Hang.KQ1.listAttribute[8].GetValueCollection(i).ToString().ToUpper();
                        lblSoTienGiamThueVaThuKhac2.Text = vad1ac_Hang.KQ1.listAttribute[9].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 2:
                        lblTenKhoanMucThueVaThuKhac3.Text = vad1ac_Hang.KQ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaApDungThueSuatThueVaThuKhac3.Text = vad1ac_Hang.KQ1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaTinhThueVaThuKhac3.Text = vad1ac_Hang.KQ1.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblSoLuongTinhThueVaThuKhac3.Text = vad1ac_Hang.KQ1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        lblMaDonViTinhChuanDanhThueVaThuKhac3.Text = vad1ac_Hang.KQ1.listAttribute[4].GetValueCollection(i).ToString().ToUpper();
                        lblThueSuatThueVaThuKhac3.Text = vad1ac_Hang.KQ1.listAttribute[5].GetValueCollection(i).ToString().ToUpper();
                        lblSoTienThueVaThuKhac3.Text = vad1ac_Hang.KQ1.listAttribute[6].GetValueCollection(i).ToString().ToUpper();
                        lblMaMienGiamThueVaThuKhac3.Text = vad1ac_Hang.KQ1.listAttribute[7].GetValueCollection(i).ToString().ToUpper();
                        lblDieuKhoanMienGiamThueVaThuKhac3.Text = vad1ac_Hang.KQ1.listAttribute[8].GetValueCollection(i).ToString().ToUpper();
                        lblSoTienGiamThueVaThuKhac3.Text = vad1ac_Hang.KQ1.listAttribute[9].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 3:
                        lblTenKhoanMucThueVaThuKhac4.Text = vad1ac_Hang.KQ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaApDungThueSuatThueVaThuKhac4.Text = vad1ac_Hang.KQ1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaTinhThueVaThuKhac4.Text = vad1ac_Hang.KQ1.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblSoLuongTinhThueVaThuKhac4.Text = vad1ac_Hang.KQ1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        lblMaDonViTinhChuanDanhThueVaThuKhac4.Text = vad1ac_Hang.KQ1.listAttribute[4].GetValueCollection(i).ToString().ToUpper();
                        lblThueSuatThueVaThuKhac4.Text = vad1ac_Hang.KQ1.listAttribute[5].GetValueCollection(i).ToString().ToUpper();
                        lblSoTienThueVaThuKhac4.Text = vad1ac_Hang.KQ1.listAttribute[6].GetValueCollection(i).ToString().ToUpper();
                        lblMaMienGiamThueVaThuKhac4.Text = vad1ac_Hang.KQ1.listAttribute[7].GetValueCollection(i).ToString().ToUpper();
                        lblDieuKhoanMienGiamThueVaThuKhac4.Text = vad1ac_Hang.KQ1.listAttribute[8].GetValueCollection(i).ToString().ToUpper();
                        lblSoTienGiamThueVaThuKhac4.Text = vad1ac_Hang.KQ1.listAttribute[9].GetValueCollection(i).ToString().ToUpper();
                        break;
                    case 4:
                        lblTenKhoanMucThueVaThuKhac5.Text = vad1ac_Hang.KQ1.listAttribute[0].GetValueCollection(i).ToString().ToUpper();
                        lblMaApDungThueSuatThueVaThuKhac5.Text = vad1ac_Hang.KQ1.listAttribute[1].GetValueCollection(i).ToString().ToUpper();
                        lblTriGiaTinhThueVaThuKhac5.Text = vad1ac_Hang.KQ1.listAttribute[2].GetValueCollection(i).ToString().ToUpper();
                        lblSoLuongTinhThueVaThuKhac5.Text = vad1ac_Hang.KQ1.listAttribute[3].GetValueCollection(i).ToString().ToUpper();
                        lblMaDonViTinhChuanDanhThueVaThuKhac5.Text = vad1ac_Hang.KQ1.listAttribute[4].GetValueCollection(i).ToString().ToUpper();
                        lblThueSuatThueVaThuKhac5.Text = vad1ac_Hang.KQ1.listAttribute[5].GetValueCollection(i).ToString().ToUpper();
                        lblSoTienThueVaThuKhac5.Text = vad1ac_Hang.KQ1.listAttribute[6].GetValueCollection(i).ToString().ToUpper();
                        lblMaMienGiamThueVaThuKhac5.Text = vad1ac_Hang.KQ1.listAttribute[7].GetValueCollection(i).ToString().ToUpper();
                        lblDieuKhoanMienGiamThueVaThuKhac5.Text = vad1ac_Hang.KQ1.listAttribute[8].GetValueCollection(i).ToString().ToUpper();
                        lblSoTienGiamThueVaThuKhac5.Text = vad1ac_Hang.KQ1.listAttribute[9].GetValueCollection(i).ToString().ToUpper();
                        break;
                }
            }
        }
    }
}
