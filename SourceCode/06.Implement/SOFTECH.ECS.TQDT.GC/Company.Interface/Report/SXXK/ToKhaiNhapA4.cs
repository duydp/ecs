﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.KDT;

namespace Company.Interface.Report.SXXK
{
    public partial class ToKhaiNhapA4 : DevExpress.XtraReports.UI.XtraReport
    {
        public Report.ReportViewTKNA4Form report;
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public int temp = 1;
        public bool BanLuuHaiQuan = false;
        public ToKhaiNhapA4()
        {
            InitializeComponent();
        }
        public void BindReport(bool inMaHang)
        {
            lblCucHaiQuan.Text = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();
            if (BanLuuHaiQuan)
                lblBanLuuHaiQuan.Text = "Bản lưu Hải quan";
            else
                lblBanLuuHaiQuan.Text = "Bản lưu người khai Hải quan";
            lblThongBaoMienThue.Text = GlobalSettings.TieuDeInDinhMuc;
            this.PrintingSystem.ShowMarginsWarning = false;
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTriGiaTT = 0;
            DateTime minDate = new DateTime(1900, 1, 1);
            //
            //if (this.TKMD.SoTiepNhan != 0)
            //    this.lblSoTiepNhan.Text = "Số TNDKDT: " + this.TKMD.SoTiepNhan;
            //
            lblChiCucHaiQuan.Text = GlobalSettings.TEN_HAI_QUAN_NGAN.ToUpper();
            if (this.TKMD.SoToKhai > 0)
                lblSoToKhai.Text = this.TKMD.SoToKhai + "";
            if (this.TKMD.NgayDangKy > minDate)
                lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy");
            if (this.TKMD.SoLuongPLTK > 0)
                lblSoPLTK.Text = this.TKMD.SoLuongPLTK.ToString();
            lblMaDoanhNghiep1.Text = this.ToStringForReport(this.TKMD.MaDoanhNghiep);
            lblTenDoanhNghiep1.Text = this.TKMD.TenDoanhNghiep.ToUpper() + "\r\n" + GlobalSettings.DIA_CHI.ToUpper();

            lblTenDoiTac.Text = this.TKMD.TenDonViDoiTac;
            lblNguoiUyThac.Text = "";
            lblMaNguoiUyThac.Text = "";
            lblMaDaiLyTTHQ.Text = this.TKMD.MaDaiLyTTHQ;
            lblTenDaiLyTTHQ.Text = this.TKMD.TenDaiLyTTHQ;
            lblSoGP.Text = "Số : "+ this.TKMD.SoGiayPhep;
            if (this.TKMD.NgayGiayPhep > minDate)
                lblNgayGP.Text = "Ngày: " + this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
            else
                lblNgayGP.Text = "Ngày: ";
            if (this.TKMD.NgayHetHanGiayPhep > minDate)
                lblNgayHHGP.Text = "Ngày hết hạn: " + this.TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
            else
                lblNgayHHGP.Text = "Ngày hết hạn: ";
            if (this.TKMD.SoHopDong.Length > 36) 
                lblSoHopDong.Font = new Font("Times New Roman", 6.5f);
            lblSoHD.Text = "Số : " + this.TKMD.SoHopDong;
            if (this.TKMD.NgayHopDong > minDate)
                lblNgayHD.Text = "Ngày: " + this.TKMD.NgayHopDong.ToString("dd/MM/yyyy");
            else
                lblNgayHD.Text = "Ngày: ";
            if (this.TKMD.NgayHetHanHopDong > minDate)
                lblNgayHHHD.Text = "Ngày hết hạn: " + this.TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
            else
                lblNgayHHHD.Text = "Ngày hết hạn: ";
            lblSoHoaDon.Text ="Số : " + this.TKMD.SoHoaDonThuongMai;
            if (this.TKMD.NgayHoaDonThuongMai > minDate)
                lblNgayHoaDon.Text = "Ngày: " + this.TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
            else
                lblNgayHoaDon.Text = "Ngày: ";
            lblSoPTVT.Text = "Tên, số hiệu: " + this.TKMD.SoHieuPTVT;
            if (this.TKMD.NgayDenPTVT > minDate)
                lblNgayDenPTVT.Text = "Ngày đến: " + this.TKMD.NgayDenPTVT.ToString("dd/MM/yyyy");
            else
                lblNgayDenPTVT.Text = "Ngày đến: ";
            lblSoVanTaiDon.Text = this.TKMD.SoVanDon;
            if (this.TKMD.NgayVanDon > minDate)
                lblNgayVanTaiDon.Text = "Ngày: " + this.TKMD.NgayVanDon.ToString("dd/MM/yyyy");
            else
                lblNgayVanTaiDon.Text = "Ngày: ";
            lblMaNuoc.Text = ToStringForReport(this.TKMD.NuocXK_ID);
            lblTenNuoc.Text = Nuoc.GetName(this.TKMD.NuocXK_ID);
            lblDiaDiemXepHang.Text = this.TKMD.DiaDiemXepHang;
            lblMaDiaDiemDoHang.Text = ToStringForReport(this.TKMD.CuaKhau_ID);
            lblDiaDiemDoHang.Text = CuaKhau.GetName(this.TKMD.CuaKhau_ID);
            lblDKGH.Text = this.TKMD.DKGH_ID;
            lblNgoaiTe.Text = ToStringForReport(this.TKMD.NguyenTe_ID);
            lblTyGiaTT.Text = this.TKMD.TyGiaTinhThue.ToString("G10");
            lblPTTT.Text = this.TKMD.PTTT_ID;
            string st = "";
            if (this.TKMD.PhiBaoHiem > 0)
                st = "I = " + this.TKMD.PhiBaoHiem.ToString("N2");
            if (this.TKMD.PhiVanChuyen > 0)
                st += " F = " + this.TKMD.PhiVanChuyen.ToString("N2");
            if (this.TKMD.PhiKhac > 0)
                st += " Phí khác = " + this.TKMD.PhiKhac.ToString("N2");
            lblPhiBaoHiem.Text = st;
            if (TKMD.SoKien > 0)
                lblTrongLuong.Text = "TỔNG CỘNG=" + TKMD.SoKien.ToString("n0") + " KIỆN";
            if (TKMD.TrongLuongNet > 0)
                lblTrongLuong.Text += "; NW=" + TKMD.TrongLuongNet.ToString("N2") + " KG";
            if (TKMD.TrongLuong > 0)
                lblTrongLuong.Text += "; GW=" + TKMD.TrongLuong.ToString("N2") + " KG";
            if (TKMD.SoContainer20 > 0)
                lblTrongLuong.Text += "; = " + TKMD.SoContainer20 + "X20' ";
            //else
            //    lblSoKienTrongLuong.Text += "; =.....X20' ";
            if (TKMD.SoContainer40 > 0)
                lblTrongLuong.Text += TKMD.SoContainer40 + "X40'";
            //else
            //    lblSoKienTrongLuong.Text += "; =.....X40'";
            if (this.TKMD.HMDCollection.Count <= 3)
            {
                if (this.TKMD.HMDCollection.Count >= 1)
                {
                    HangMauDich hmd = this.TKMD.HMDCollection[0];
                    if (hmd.TenHang.Length >= 40) TenHang1.Font = new Font("Times New Roman", 7f);
                    TenHang1.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang1.Text += " / " + hmd.MaPhu;
                        TenHang1.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS1.Text = hmd.MaHS;
                    XuatXu1.Text = hmd.NuocXX_ID;
                    Luong1.Text = hmd.SoLuong.ToString("G15");
                    DVT1.Text = DonViTinh.GetName(hmd.DVT_ID);
                    DonGiaNT1.Text = hmd.DonGiaKB.ToString("G10");
                    TriGiaNT1.Text = hmd.TriGiaKB.ToString("N2");
                    //TriGiaTT1.Text = hmd.TriGiaTT.ToString("N0");
                    //if (hmd.ThueSuatGiam.Trim() == "")
                    //    ThueSuatXNK1.Text = hmd.ThueSuatXNK.ToString("N0");
                    //else
                    //    ThueSuatXNK1.Text = hmd.ThueSuatGiam;
                    //TienThueXNK1.Text = hmd.ThueXNK.ToString("N0");
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                    //tongTienThueXNK += hmd.ThueXNK;
                }
                if (this.TKMD.HMDCollection.Count >= 2)
                {
                    HangMauDich hmd = this.TKMD.HMDCollection[1];
                    if (hmd.TenHang.Length >= 40) TenHang2.Font = new Font("Times New Roman", 7f);
                    TenHang2.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang2.Text += " / " + hmd.MaPhu;
                        TenHang2.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS2.Text = hmd.MaHS;
                    XuatXu2.Text = hmd.NuocXX_ID;
                    Luong2.Text = hmd.SoLuong.ToString("G15");
                    DVT2.Text = DonViTinh.GetName(hmd.DVT_ID);
                    DonGiaNT2.Text = hmd.DonGiaKB.ToString("G10");
                    TriGiaNT2.Text = hmd.TriGiaKB.ToString("N2");
                    //TriGiaTT2.Text = hmd.TriGiaTT.ToString("N0");
                    //if (hmd.ThueSuatGiam.Trim() == "")
                    //    ThueSuatXNK2.Text = hmd.ThueSuatXNK.ToString("N0");
                    //else
                    //    ThueSuatXNK2.Text = hmd.ThueSuatGiam;
                    //TienThueXNK2.Text = hmd.ThueXNK.ToString("N0");
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                    //tongTienThueXNK += hmd.ThueXNK;
                }
                if (this.TKMD.HMDCollection.Count == 3)
                {
                    HangMauDich hmd = this.TKMD.HMDCollection[2];
                    if (hmd.TenHang.Length >= 40) TenHang3.Font = new Font("Times New Roman", 7f);
                    TenHang3.Text = hmd.TenHang;
                    if (inMaHang)
                    {
                        TenHang3.Text += " / " + hmd.MaPhu;
                        TenHang3.Font = new Font("Times New Roman", 7f);
                    }
                    MaHS3.Text = hmd.MaHS;
                    XuatXu3.Text = hmd.NuocXX_ID;
                    Luong3.Text = hmd.SoLuong.ToString("G15");
                    DVT3.Text = DonViTinh.GetName(hmd.DVT_ID);
                    DonGiaNT3.Text = hmd.DonGiaKB.ToString("G10");
                    TriGiaNT3.Text = hmd.TriGiaKB.ToString("N2");
                    //TriGiaTT3.Text = hmd.TriGiaTT.ToString("N0");
                    //if (hmd.ThueSuatGiam.Trim() == "")
                    //    ThueSuatXNK3.Text = hmd.ThueSuatXNK.ToString("N0");
                    //else
                    //    ThueSuatXNK3.Text = hmd.ThueSuatGiam;
                    //TienThueXNK3.Text = hmd.ThueXNK.ToString("N0");
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                    //tongTienThueXNK += hmd.ThueXNK;
                }
            }
            else
            {
                TenHang1.Text = "NGUYÊN PHỤ LIỆU GIA CÔNG";
                TenHang2.Text = "(CÓ PHỤ LỤC ĐÍNH KÈM)";
                foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                {
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                    tongTienThueXNK += hmd.ThueXNK;
                    tongTriGiaTT += hmd.TriGiaTT;
                }
                //XuatXu1.Text = Nuoc.GetName(this.TKMD.HMDCollection[0].NuocXX_ID);
                TriGiaNT1.Text = tongTriGiaNT.ToString("N2");
                //TriGiaTT1.Text = tongTriGiaTT.ToString("N0");
                //TienThueXNK1.Text = tongTienThueXNK.ToString("N0");
            }
            tongTriGiaNT += this.TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen;
            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");
            //lblTongThueXNK.Text = tongTienThueXNK.ToString("N0");
            //lblTongThueXNKSo.Text = this.TinhTongThueHMD().ToString("N0");
            //string s = Company.BLL.Utils.VNCurrency.ToString(this.TinhTongThueHMD()).Trim();
            //s = s[0].ToString().ToUpper() + s.Substring(1);
            //lblTongThueXNKChu.Text = s.Replace("  ", " ");
            XRControl control = new XRControl();
            int i = 5;
            try
            {
                foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
                {
                    if (ct.LoaiCT < 5)
                    {
                        control = this.Detail.Controls["lblSoBanChinh" + ct.LoaiCT];
                        control.Text = ct.SoBanChinh + "";
                        control = this.Detail.Controls["lblSoBanSao" + ct.LoaiCT];
                        control.Text = ct.SoBanSao + "";
                    }
                    else
                    {
                        if (i == 7) return;
                        control = this.Detail.Controls["lblTenChungTu" + i];
                        control.Text = ct.TenChungTu;
                        control = this.Detail.Controls["lblSoBanChinh" + i];
                        control.Text = ct.SoBanChinh + "";
                        control = this.Detail.Controls["lblSoBanSao" + i];
                        control.Text = ct.SoBanSao + "";
                        i++;
                    }
                }
            }
            catch { }
        }
        public decimal TinhTongThueHMD()
        {
            decimal tong = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                tong += hmd.ThueXNK;
            return tong;
        }
        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + "    ";
            temp += s[s.Length - 1];
            return temp;
        }
        public void setVisibleImage(bool t)
        {
            //ptbImage.Visible = t;
            //lblBanLuuHaiQuan.Visible = t;
        }
        public void setNhomHang(string tenNhomHang)
        {
            switch (temp)
            {
                case 1:
                    TenHang1.Text = tenNhomHang;
                    break;
                case 2:
                    TenHang2.Text = tenNhomHang;
                    break;
                case 3:
                    TenHang3.Text = tenNhomHang;
                    break;
            }


        }

        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }
    }
}
