﻿namespace Company.Interface.Report.SXXK
{
    partial class ToKhaiNhapA4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiNhapA4));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.lblMienThue1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lblMienThue2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable58 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow75 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable57 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow74 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable52 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable56 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow73 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable54 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable53 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable51 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable55 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow72 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable50 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable49 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable48 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable47 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable46 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable45 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable44 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable42 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable41 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable40 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable39 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable38 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable37 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDaiLyTTHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable36 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable35 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable34 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoBanChinh5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable33 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable32 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTenChungTu6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable31 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTenChungTu5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable30 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable29 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable25 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTongThueXNKChu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable28 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable27 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable26 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTablesaaa = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTongThueXNKSo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable23 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable22 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongThueXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTienThueGTGT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTriGiaThuKhac = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable20 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable19 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDaiLyTTHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDKGH = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTyGiaTT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgoaiTe = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPTTT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNguoiUyThac = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoiUyThac = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNuoc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNuoc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaDiemXepHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemDoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaDiemDoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDoiTac = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDenPTVT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoPTVT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayVanTaiDon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoVanTaiDon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDoanhNghiep1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDoanhNghiep1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrKhac = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrSXXK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTNK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrGC = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDT = new DevExpress.XtraReports.UI.XRLabel();
            this.xrKD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoGP = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHHGP = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayGP = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHHHD = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHD = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoHD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChiCucHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCucHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoPLTK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoPLTK1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTrangthai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBanLuuHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTiepNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaTTGTGT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatGTGT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueGTGT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable43 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable24 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TenHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTrongLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhiBaoHiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTriGiaNT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable21 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHHHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHHGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTablesaaa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMienThue1,
            this.xrLine3,
            this.xrLine2,
            this.xrLine1,
            this.lblMienThue2,
            this.xrTable58,
            this.xrTable57,
            this.xrTable52,
            this.xrTable56,
            this.xrTable54,
            this.xrTable53,
            this.xrTable51,
            this.xrTable55,
            this.xrTable50,
            this.xrTable49,
            this.xrTable48,
            this.xrTable47,
            this.xrTable46,
            this.xrTable45,
            this.xrTable44,
            this.xrTable42,
            this.xrTable41,
            this.xrLabel29,
            this.xrLabel27,
            this.xrLabel26,
            this.xrLabel25,
            this.xrLabel24,
            this.xrLabel22,
            this.xrLabel21,
            this.xrTable40,
            this.xrLabel20,
            this.xrTable39,
            this.xrTable38,
            this.xrTable37,
            this.lblTenDaiLyTTHQ,
            this.xrTable36,
            this.xrTable35,
            this.xrTable34,
            this.lblSoBanChinh5,
            this.lblSoBanSao5,
            this.xrTable33,
            this.xrTable32,
            this.xrTable31,
            this.xrTable30,
            this.xrTable29,
            this.xrTable25,
            this.xrTable28,
            this.xrTable27,
            this.xrTable26,
            this.xrTablesaaa,
            this.xrTable23,
            this.xrTable22,
            this.xrTable20,
            this.xrLabel40,
            this.xrTable19,
            this.xrTable16,
            this.xrTable14,
            this.xrTable9,
            this.xrTable7,
            this.xrTable5,
            this.xrTable4,
            this.lblBanLuuHaiQuan,
            this.xrLabel2,
            this.xrLabel1,
            this.xrLabel23,
            this.lblSoBanChinh6,
            this.lblSoBanSao6,
            this.lblSoBanChinh4,
            this.lblSoBanChinh3,
            this.lblSoBanChinh2,
            this.lblSoBanChinh1,
            this.lblSoBanSao1,
            this.lblSoBanSao2,
            this.lblSoBanSao3,
            this.lblSoBanSao4,
            this.lblSoTiepNhan,
            this.xrTable2,
            this.xrTable43,
            this.xrTable24,
            this.xrTable1,
            this.xrTable3,
            this.xrTable21});
            this.Detail.Height = 2268;
            this.Detail.Name = "Detail";
            this.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // lblMienThue1
            // 
            this.lblMienThue1.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.lblMienThue1.Location = new System.Drawing.Point(58, 775);
            this.lblMienThue1.Name = "lblMienThue1";
            this.lblMienThue1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMienThue1.ParentStyleUsing.UseBackColor = false;
            this.lblMienThue1.ParentStyleUsing.UseFont = false;
            this.lblMienThue1.ParentStyleUsing.UseForeColor = false;
            this.lblMienThue1.Size = new System.Drawing.Size(266, 75);
            this.lblMienThue1.Text = "Mien thue 1";
            this.lblMienThue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine3
            // 
            this.xrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine3.Location = new System.Drawing.Point(25, 660);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.Size = new System.Drawing.Size(767, 10);
            // 
            // xrLine2
            // 
            this.xrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine2.Location = new System.Drawing.Point(25, 630);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.Size = new System.Drawing.Size(767, 10);
            // 
            // xrLine1
            // 
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine1.Location = new System.Drawing.Point(25, 600);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Size = new System.Drawing.Size(767, 10);
            // 
            // lblMienThue2
            // 
            this.lblMienThue2.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.lblMienThue2.Location = new System.Drawing.Point(342, 775);
            this.lblMienThue2.Name = "lblMienThue2";
            this.lblMienThue2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMienThue2.ParentStyleUsing.UseBackColor = false;
            this.lblMienThue2.ParentStyleUsing.UseFont = false;
            this.lblMienThue2.ParentStyleUsing.UseForeColor = false;
            this.lblMienThue2.Size = new System.Drawing.Size(266, 75);
            this.lblMienThue2.Text = "Mien Thue 2";
            this.lblMienThue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable58
            // 
            this.xrTable58.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable58.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable58.Location = new System.Drawing.Point(25, 2061);
            this.xrTable58.Name = "xrTable58";
            this.xrTable58.ParentStyleUsing.UseBorders = false;
            this.xrTable58.ParentStyleUsing.UseFont = false;
            this.xrTable58.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow75});
            this.xrTable58.Size = new System.Drawing.Size(767, 170);
            // 
            // xrTableRow75
            // 
            this.xrTableRow75.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell216,
            this.xrTableCell217,
            this.xrTableCell218});
            this.xrTableRow75.Name = "xrTableRow75";
            this.xrTableRow75.Size = new System.Drawing.Size(767, 170);
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell216.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell216.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell216.Multiline = true;
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell216.ParentStyleUsing.UseBorders = false;
            this.xrTableCell216.ParentStyleUsing.UseFont = false;
            this.xrTableCell216.Size = new System.Drawing.Size(384, 170);
            this.xrTableCell216.Text = "36. Cán bộ kiểm tra thuế ( Ký, ghi rõ họ tên, ngày, tháng, năm).";
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell217.Location = new System.Drawing.Point(384, 0);
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell217.ParentStyleUsing.UseFont = false;
            this.xrTableCell217.Size = new System.Drawing.Size(192, 170);
            this.xrTableCell217.Text = "37. Ghi chép khác của hải quan.";
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell218.Location = new System.Drawing.Point(576, 0);
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 8, 0, 100F);
            this.xrTableCell218.ParentStyleUsing.UseFont = false;
            this.xrTableCell218.Size = new System.Drawing.Size(191, 170);
            this.xrTableCell218.Text = "38. Xác nhận đã làm thủ tục hải quan (Ký, đóng dấu, ghi rõ họ tên).";
            // 
            // xrTable57
            // 
            this.xrTable57.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable57.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable57.Location = new System.Drawing.Point(25, 2016);
            this.xrTable57.Name = "xrTable57";
            this.xrTable57.ParentStyleUsing.UseBorders = false;
            this.xrTable57.ParentStyleUsing.UseFont = false;
            this.xrTable57.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow74});
            this.xrTable57.Size = new System.Drawing.Size(767, 45);
            // 
            // xrTableRow74
            // 
            this.xrTableRow74.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell215});
            this.xrTableRow74.Name = "xrTableRow74";
            this.xrTableRow74.Size = new System.Drawing.Size(767, 45);
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell215.CanGrow = false;
            this.xrTableCell215.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell215.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell215.Multiline = true;
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell215.ParentStyleUsing.UseBorders = false;
            this.xrTableCell215.ParentStyleUsing.UseFont = false;
            this.xrTableCell215.Size = new System.Drawing.Size(767, 45);
            this.xrTableCell215.Text = resources.GetString("xrTableCell215.Text");
            this.xrTableCell215.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable52
            // 
            this.xrTable52.BorderColor = System.Drawing.Color.DarkGray;
            this.xrTable52.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable52.Location = new System.Drawing.Point(25, 1853);
            this.xrTable52.Name = "xrTable52";
            this.xrTable52.ParentStyleUsing.UseBorderColor = false;
            this.xrTable52.ParentStyleUsing.UseBorders = false;
            this.xrTable52.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow67,
            this.xrTableRow68,
            this.xrTableRow69});
            this.xrTable52.Size = new System.Drawing.Size(566, 90);
            this.xrTable52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell198,
            this.xrTableCell199});
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.Size = new System.Drawing.Size(566, 30);
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell198.Size = new System.Drawing.Size(25, 30);
            this.xrTableCell198.Text = "1";
            this.xrTableCell198.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Location = new System.Drawing.Point(25, 0);
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell199.Size = new System.Drawing.Size(541, 30);
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell200,
            this.xrTableCell201});
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Size = new System.Drawing.Size(566, 30);
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell200.Size = new System.Drawing.Size(25, 30);
            this.xrTableCell200.Text = "2";
            this.xrTableCell200.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Location = new System.Drawing.Point(25, 0);
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell201.Size = new System.Drawing.Size(541, 30);
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell202,
            this.xrTableCell203});
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.Size = new System.Drawing.Size(566, 30);
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell202.Size = new System.Drawing.Size(25, 30);
            this.xrTableCell202.Text = "3";
            this.xrTableCell202.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Location = new System.Drawing.Point(25, 0);
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell203.Size = new System.Drawing.Size(541, 30);
            this.xrTableCell203.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTable56
            // 
            this.xrTable56.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable56.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable56.Location = new System.Drawing.Point(25, 1812);
            this.xrTable56.Name = "xrTable56";
            this.xrTable56.ParentStyleUsing.UseBorders = false;
            this.xrTable56.ParentStyleUsing.UseFont = false;
            this.xrTable56.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow73});
            this.xrTable56.Size = new System.Drawing.Size(767, 42);
            this.xrTable56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow73
            // 
            this.xrTableRow73.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell211,
            this.xrTableCell213,
            this.xrTableCell214});
            this.xrTableRow73.Name = "xrTableRow73";
            this.xrTableRow73.Size = new System.Drawing.Size(767, 42);
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell211.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell211.ParentStyleUsing.UseFont = false;
            this.xrTableCell211.Size = new System.Drawing.Size(25, 42);
            this.xrTableCell211.Text = "SỐ TT";
            this.xrTableCell211.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell213.Location = new System.Drawing.Point(25, 0);
            this.xrTableCell213.Multiline = true;
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell213.ParentStyleUsing.UseFont = false;
            this.xrTableCell213.Size = new System.Drawing.Size(541, 42);
            this.xrTableCell213.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell214.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell214.Location = new System.Drawing.Point(566, 0);
            this.xrTableCell214.Multiline = true;
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell214.ParentStyleUsing.UseBorders = false;
            this.xrTableCell214.ParentStyleUsing.UseFont = false;
            this.xrTableCell214.Size = new System.Drawing.Size(201, 42);
            this.xrTableCell214.Text = "33.Tổng số tiền phải điều chỉnh sau khi kiểm tra(Tăng/ Giảm):";
            this.xrTableCell214.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable54
            // 
            this.xrTable54.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable54.Location = new System.Drawing.Point(50, 1812);
            this.xrTable54.Name = "xrTable54";
            this.xrTable54.ParentStyleUsing.UseBorders = false;
            this.xrTable54.ParentStyleUsing.UseFont = false;
            this.xrTable54.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow71});
            this.xrTable54.Size = new System.Drawing.Size(541, 17);
            this.xrTable54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow71
            // 
            this.xrTableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell209,
            this.xrTableCell210});
            this.xrTableRow71.Name = "xrTableRow71";
            this.xrTableRow71.Size = new System.Drawing.Size(541, 17);
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell209.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell209.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell209.ParentStyleUsing.UseBorders = false;
            this.xrTableCell209.ParentStyleUsing.UseFont = false;
            this.xrTableCell209.Size = new System.Drawing.Size(360, 17);
            this.xrTableCell209.Text = "TIền thuế GTGT ( hoặc TTĐB)";
            this.xrTableCell209.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell210.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell210.Location = new System.Drawing.Point(360, 0);
            this.xrTableCell210.Multiline = true;
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell210.ParentStyleUsing.UseBorders = false;
            this.xrTableCell210.ParentStyleUsing.UseFont = false;
            this.xrTableCell210.Size = new System.Drawing.Size(181, 17);
            this.xrTableCell210.Text = "Thu khác";
            this.xrTableCell210.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable53
            // 
            this.xrTable53.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable53.Location = new System.Drawing.Point(50, 1829);
            this.xrTable53.Name = "xrTable53";
            this.xrTable53.ParentStyleUsing.UseBorders = false;
            this.xrTable53.ParentStyleUsing.UseFont = false;
            this.xrTable53.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow70});
            this.xrTable53.Size = new System.Drawing.Size(541, 25);
            this.xrTable53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell204,
            this.xrTableCell205,
            this.xrTableCell206,
            this.xrTableCell207,
            this.xrTableCell208});
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.Size = new System.Drawing.Size(541, 25);
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell204.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell204.ParentStyleUsing.UseBorders = false;
            this.xrTableCell204.Size = new System.Drawing.Size(137, 25);
            this.xrTableCell204.Text = "Trị giá tính thuế ( VNĐ)";
            this.xrTableCell204.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell205.Location = new System.Drawing.Point(137, 0);
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell205.ParentStyleUsing.UseBorders = false;
            this.xrTableCell205.Size = new System.Drawing.Size(68, 25);
            this.xrTableCell205.Text = "Thuế suất(%)";
            this.xrTableCell205.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell206.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell206.Location = new System.Drawing.Point(205, 0);
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell206.ParentStyleUsing.UseBorders = false;
            this.xrTableCell206.ParentStyleUsing.UseFont = false;
            this.xrTableCell206.Size = new System.Drawing.Size(155, 25);
            this.xrTableCell206.Text = "Tiền thuế";
            this.xrTableCell206.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell207.Location = new System.Drawing.Point(360, 0);
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell207.ParentStyleUsing.UseBorders = false;
            this.xrTableCell207.Size = new System.Drawing.Size(47, 25);
            this.xrTableCell207.Text = "Tỷ lệ";
            this.xrTableCell207.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell208.Location = new System.Drawing.Point(407, 0);
            this.xrTableCell208.Multiline = true;
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell208.ParentStyleUsing.UseBorders = false;
            this.xrTableCell208.ParentStyleUsing.UseFont = false;
            this.xrTableCell208.Size = new System.Drawing.Size(134, 25);
            this.xrTableCell208.Text = "Số tiền";
            this.xrTableCell208.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable51
            // 
            this.xrTable51.BorderColor = System.Drawing.Color.Black;
            this.xrTable51.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable51.Location = new System.Drawing.Point(25, 1854);
            this.xrTable51.Name = "xrTable51";
            this.xrTable51.ParentStyleUsing.UseBorderColor = false;
            this.xrTable51.ParentStyleUsing.UseBorders = false;
            this.xrTable51.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow66});
            this.xrTable51.Size = new System.Drawing.Size(767, 120);
            this.xrTable51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow66
            // 
            this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell191,
            this.xrTableCell192,
            this.xrTableCell193,
            this.xrTableCell194,
            this.xrTableCell195,
            this.xrTableCell196,
            this.xrTableCell197});
            this.xrTableRow66.Name = "xrTableRow66";
            this.xrTableRow66.Size = new System.Drawing.Size(767, 120);
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell191.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell191.ParentStyleUsing.UseBorders = false;
            this.xrTableCell191.Size = new System.Drawing.Size(25, 120);
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell192.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell192.Location = new System.Drawing.Point(25, 0);
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 5, 100F);
            this.xrTableCell192.ParentStyleUsing.UseBorders = false;
            this.xrTableCell192.ParentStyleUsing.UseFont = false;
            this.xrTableCell192.Size = new System.Drawing.Size(137, 120);
            this.xrTableCell192.Text = "Cộng : ";
            this.xrTableCell192.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell193.Location = new System.Drawing.Point(162, 0);
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell193.ParentStyleUsing.UseBorders = false;
            this.xrTableCell193.Size = new System.Drawing.Size(68, 120);
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell194.Location = new System.Drawing.Point(230, 0);
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell194.ParentStyleUsing.UseBorders = false;
            this.xrTableCell194.Size = new System.Drawing.Size(155, 120);
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell195.Location = new System.Drawing.Point(385, 0);
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell195.ParentStyleUsing.UseBorders = false;
            this.xrTableCell195.Size = new System.Drawing.Size(47, 120);
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell196.Location = new System.Drawing.Point(432, 0);
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell196.ParentStyleUsing.UseBorders = false;
            this.xrTableCell196.Size = new System.Drawing.Size(134, 120);
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell197.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell197.Location = new System.Drawing.Point(566, 0);
            this.xrTableCell197.Multiline = true;
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 2, 2, 100F);
            this.xrTableCell197.ParentStyleUsing.UseBorders = false;
            this.xrTableCell197.ParentStyleUsing.UseFont = false;
            this.xrTableCell197.Size = new System.Drawing.Size(201, 120);
            this.xrTableCell197.Text = resources.GetString("xrTableCell197.Text");
            // 
            // xrTable55
            // 
            this.xrTable55.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable55.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable55.Location = new System.Drawing.Point(25, 1973);
            this.xrTable55.Name = "xrTable55";
            this.xrTable55.ParentStyleUsing.UseBorders = false;
            this.xrTable55.ParentStyleUsing.UseFont = false;
            this.xrTable55.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow72});
            this.xrTable55.Size = new System.Drawing.Size(767, 45);
            // 
            // xrTableRow72
            // 
            this.xrTableRow72.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell212});
            this.xrTableRow72.Name = "xrTableRow72";
            this.xrTableRow72.Size = new System.Drawing.Size(767, 45);
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell212.CanGrow = false;
            this.xrTableCell212.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell212.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell212.Multiline = true;
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell212.ParentStyleUsing.UseBorders = false;
            this.xrTableCell212.ParentStyleUsing.UseFont = false;
            this.xrTableCell212.Size = new System.Drawing.Size(767, 45);
            this.xrTableCell212.Text = resources.GetString("xrTableCell212.Text");
            this.xrTableCell212.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable50
            // 
            this.xrTable50.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable50.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable50.Location = new System.Drawing.Point(25, 1651);
            this.xrTable50.Name = "xrTable50";
            this.xrTable50.ParentStyleUsing.UseBorders = false;
            this.xrTable50.ParentStyleUsing.UseFont = false;
            this.xrTable50.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow65});
            this.xrTable50.Size = new System.Drawing.Size(767, 42);
            this.xrTable50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell185,
            this.xrTableCell186,
            this.xrTableCell187,
            this.xrTableCell188,
            this.xrTableCell189,
            this.xrTableCell190});
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Size = new System.Drawing.Size(767, 42);
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell185.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell185.ParentStyleUsing.UseFont = false;
            this.xrTableCell185.Size = new System.Drawing.Size(25, 42);
            this.xrTableCell185.Text = "SỐ TT";
            this.xrTableCell185.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell186.Location = new System.Drawing.Point(25, 0);
            this.xrTableCell186.Multiline = true;
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell186.ParentStyleUsing.UseFont = false;
            this.xrTableCell186.Size = new System.Drawing.Size(137, 42);
            this.xrTableCell186.Text = "Mã số hàng hóa";
            this.xrTableCell186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell187.Location = new System.Drawing.Point(162, 0);
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell187.ParentStyleUsing.UseFont = false;
            this.xrTableCell187.Size = new System.Drawing.Size(67, 42);
            this.xrTableCell187.Text = "Lượng";
            this.xrTableCell187.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell188.Location = new System.Drawing.Point(229, 0);
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell188.ParentStyleUsing.UseFont = false;
            this.xrTableCell188.Size = new System.Drawing.Size(67, 42);
            this.xrTableCell188.Text = "Xuất xứ";
            this.xrTableCell188.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Location = new System.Drawing.Point(296, 0);
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell189.Size = new System.Drawing.Size(137, 42);
            this.xrTableCell189.Text = "Đơn giá tính thuế";
            this.xrTableCell189.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel42});
            this.xrTableCell190.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell190.Location = new System.Drawing.Point(433, 0);
            this.xrTableCell190.Multiline = true;
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell190.ParentStyleUsing.UseFont = false;
            this.xrTableCell190.Size = new System.Drawing.Size(334, 42);
            this.xrTableCell190.Text = "xrTableCell190";
            this.xrTableCell190.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel42
            // 
            this.xrLabel42.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel42.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel42.Location = new System.Drawing.Point(0, 0);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.ParentStyleUsing.UseBorders = false;
            this.xrLabel42.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel42.ParentStyleUsing.UseFont = false;
            this.xrLabel42.Size = new System.Drawing.Size(332, 18);
            this.xrLabel42.Text = "Tiền thuế nhập khẩu";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable49
            // 
            this.xrTable49.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable49.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable49.Location = new System.Drawing.Point(25, 1692);
            this.xrTable49.Name = "xrTable49";
            this.xrTable49.ParentStyleUsing.UseBorders = false;
            this.xrTable49.ParentStyleUsing.UseFont = false;
            this.xrTable49.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow64});
            this.xrTable49.Size = new System.Drawing.Size(767, 120);
            this.xrTable49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell177,
            this.xrTableCell178,
            this.xrTableCell179,
            this.xrTableCell180,
            this.xrTableCell181,
            this.xrTableCell182,
            this.xrTableCell183,
            this.xrTableCell184});
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Size = new System.Drawing.Size(767, 120);
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell177.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell177.ParentStyleUsing.UseFont = false;
            this.xrTableCell177.Size = new System.Drawing.Size(25, 120);
            this.xrTableCell177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell178.Location = new System.Drawing.Point(25, 0);
            this.xrTableCell178.Multiline = true;
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 5, 100F);
            this.xrTableCell178.ParentStyleUsing.UseFont = false;
            this.xrTableCell178.Size = new System.Drawing.Size(137, 120);
            this.xrTableCell178.Text = "Cộng :";
            this.xrTableCell178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell179.Location = new System.Drawing.Point(162, 0);
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell179.ParentStyleUsing.UseFont = false;
            this.xrTableCell179.Size = new System.Drawing.Size(67, 120);
            this.xrTableCell179.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell180.Location = new System.Drawing.Point(229, 0);
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell180.ParentStyleUsing.UseFont = false;
            this.xrTableCell180.Size = new System.Drawing.Size(67, 120);
            this.xrTableCell180.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell181.Location = new System.Drawing.Point(296, 0);
            this.xrTableCell181.Multiline = true;
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell181.ParentStyleUsing.UseFont = false;
            this.xrTableCell181.Size = new System.Drawing.Size(137, 120);
            this.xrTableCell181.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell182.Location = new System.Drawing.Point(433, 0);
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell182.ParentStyleUsing.UseFont = false;
            this.xrTableCell182.Size = new System.Drawing.Size(133, 120);
            this.xrTableCell182.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell183.Location = new System.Drawing.Point(566, 0);
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell183.ParentStyleUsing.UseFont = false;
            this.xrTableCell183.Size = new System.Drawing.Size(68, 120);
            this.xrTableCell183.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell184.Location = new System.Drawing.Point(634, 0);
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell184.ParentStyleUsing.UseFont = false;
            this.xrTableCell184.Size = new System.Drawing.Size(133, 120);
            this.xrTableCell184.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable48
            // 
            this.xrTable48.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable48.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable48.Location = new System.Drawing.Point(458, 1668);
            this.xrTable48.Name = "xrTable48";
            this.xrTable48.ParentStyleUsing.UseBorders = false;
            this.xrTable48.ParentStyleUsing.UseFont = false;
            this.xrTable48.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow63});
            this.xrTable48.Size = new System.Drawing.Size(334, 25);
            this.xrTable48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell174,
            this.xrTableCell175,
            this.xrTableCell176});
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Size = new System.Drawing.Size(334, 25);
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell174.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell174.ParentStyleUsing.UseFont = false;
            this.xrTableCell174.Size = new System.Drawing.Size(133, 25);
            this.xrTableCell174.Text = "Trị giá tính thuế(VNĐ)";
            this.xrTableCell174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Location = new System.Drawing.Point(133, 0);
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell175.Size = new System.Drawing.Size(68, 25);
            this.xrTableCell175.Text = "Thuế suất(%)";
            this.xrTableCell175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell176.Location = new System.Drawing.Point(201, 0);
            this.xrTableCell176.Multiline = true;
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell176.ParentStyleUsing.UseFont = false;
            this.xrTableCell176.Size = new System.Drawing.Size(133, 25);
            this.xrTableCell176.Text = "Tiền thuế";
            this.xrTableCell176.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable47
            // 
            this.xrTable47.BorderColor = System.Drawing.Color.DarkGray;
            this.xrTable47.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable47.Location = new System.Drawing.Point(25, 1693);
            this.xrTable47.Name = "xrTable47";
            this.xrTable47.ParentStyleUsing.UseBorderColor = false;
            this.xrTable47.ParentStyleUsing.UseBorders = false;
            this.xrTable47.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow60,
            this.xrTableRow61,
            this.xrTableRow62});
            this.xrTable47.Size = new System.Drawing.Size(767, 90);
            this.xrTable47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell168,
            this.xrTableCell169});
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell168.Size = new System.Drawing.Size(25, 30);
            this.xrTableCell168.Text = "1";
            this.xrTableCell168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Location = new System.Drawing.Point(25, 0);
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell169.Size = new System.Drawing.Size(742, 30);
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell170,
            this.xrTableCell171});
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell170.Size = new System.Drawing.Size(26, 30);
            this.xrTableCell170.Text = "2";
            this.xrTableCell170.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Location = new System.Drawing.Point(26, 0);
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell171.Size = new System.Drawing.Size(741, 30);
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell172,
            this.xrTableCell173});
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell172.Size = new System.Drawing.Size(25, 30);
            this.xrTableCell172.Text = "3";
            this.xrTableCell172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Location = new System.Drawing.Point(25, 0);
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell173.Size = new System.Drawing.Size(742, 30);
            this.xrTableCell173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTable46
            // 
            this.xrTable46.Location = new System.Drawing.Point(25, 1522);
            this.xrTable46.Name = "xrTable46";
            this.xrTable46.ParentStyleUsing.UseBorders = false;
            this.xrTable46.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow59});
            this.xrTable46.Size = new System.Drawing.Size(767, 101);
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell166,
            this.xrTableCell167});
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Size = new System.Drawing.Size(767, 101);
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell166.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 5, 0, 100F);
            this.xrTableCell166.ParentStyleUsing.UseBorders = false;
            this.xrTableCell166.Size = new System.Drawing.Size(384, 101);
            this.xrTableCell166.Text = "31. Đại diện doanh nghiệp (Ký ghi rõ họ tên).";
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell167.CanGrow = false;
            this.xrTableCell167.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell167.Location = new System.Drawing.Point(384, 0);
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 5, 0, 100F);
            this.xrTableCell167.ParentStyleUsing.UseBorders = false;
            this.xrTableCell167.ParentStyleUsing.UseFont = false;
            this.xrTableCell167.Size = new System.Drawing.Size(383, 101);
            this.xrTableCell167.Text = "32.Cán bộ kiểm hóa (Ký , ghi rõ họ tên). ";
            // 
            // xrTable45
            // 
            this.xrTable45.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable45.Location = new System.Drawing.Point(25, 1622);
            this.xrTable45.Name = "xrTable45";
            this.xrTable45.ParentStyleUsing.UseBorders = false;
            this.xrTable45.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow58});
            this.xrTable45.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell165});
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell165.CanGrow = false;
            this.xrTableCell165.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell165.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell165.ParentStyleUsing.UseBorders = false;
            this.xrTableCell165.ParentStyleUsing.UseFont = false;
            this.xrTableCell165.Size = new System.Drawing.Size(767, 30);
            this.xrTableCell165.Text = "II- PHẦN KIỂM TRA THUẾ";
            this.xrTableCell165.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable44
            // 
            this.xrTable44.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable44.Location = new System.Drawing.Point(25, 1183);
            this.xrTable44.Name = "xrTable44";
            this.xrTable44.ParentStyleUsing.UseBorders = false;
            this.xrTable44.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow57});
            this.xrTable44.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell164});
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell164.CanGrow = false;
            this.xrTableCell164.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell164.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell164.ParentStyleUsing.UseBorders = false;
            this.xrTableCell164.ParentStyleUsing.UseFont = false;
            this.xrTableCell164.Size = new System.Drawing.Size(767, 25);
            this.xrTableCell164.Text = "30. Phần ghi kết quả kiểm tra của Hải quan";
            this.xrTableCell164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable42
            // 
            this.xrTable42.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable42.Location = new System.Drawing.Point(25, 1207);
            this.xrTable42.Name = "xrTable42";
            this.xrTable42.ParentStyleUsing.UseBorders = false;
            this.xrTable42.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow55});
            this.xrTable42.Size = new System.Drawing.Size(767, 23);
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell162});
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Size = new System.Drawing.Size(767, 23);
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell162.CanGrow = false;
            this.xrTableCell162.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell162.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell162.ParentStyleUsing.UseBorders = false;
            this.xrTableCell162.ParentStyleUsing.UseFont = false;
            this.xrTableCell162.Size = new System.Drawing.Size(767, 23);
            this.xrTableCell162.Text = "Người quyết định hình thức kiểm tra: (ghi rõ họ tên): ";
            this.xrTableCell162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable41
            // 
            this.xrTable41.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable41.Location = new System.Drawing.Point(25, 1229);
            this.xrTable41.Name = "xrTable41";
            this.xrTable41.ParentStyleUsing.UseBorders = false;
            this.xrTable41.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow54});
            this.xrTable41.Size = new System.Drawing.Size(767, 23);
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell161});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Size = new System.Drawing.Size(767, 23);
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell161.CanGrow = false;
            this.xrTableCell161.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell161.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell161.ParentStyleUsing.UseBorders = false;
            this.xrTableCell161.ParentStyleUsing.UseFont = false;
            this.xrTableCell161.Size = new System.Drawing.Size(767, 23);
            this.xrTableCell161.Text = "Hình thức kiểm tra : ";
            this.xrTableCell161.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.BorderWidth = 0;
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.Location = new System.Drawing.Point(175, 1229);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel29.ParentStyleUsing.UseFont = false;
            this.xrLabel29.Size = new System.Drawing.Size(92, 23);
            this.xrLabel29.Text = "Miễn kiểm tra";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.Location = new System.Drawing.Point(158, 1235);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.ParentStyleUsing.UseBorders = false;
            this.xrLabel27.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel27.ParentStyleUsing.UseFont = false;
            this.xrLabel27.Size = new System.Drawing.Size(10, 10);
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel26
            // 
            this.xrLabel26.BorderWidth = 0;
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.Location = new System.Drawing.Point(350, 1229);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel26.ParentStyleUsing.UseFont = false;
            this.xrLabel26.Size = new System.Drawing.Size(133, 23);
            this.xrLabel26.Text = "Kiểm tra xác suất. Tỷ lệ :";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel25
            // 
            this.xrLabel25.BorderWidth = 0;
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.Location = new System.Drawing.Point(483, 1229);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel25.ParentStyleUsing.UseFont = false;
            this.xrLabel25.Size = new System.Drawing.Size(59, 23);
            this.xrLabel25.Text = ".............%";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.BorderWidth = 0;
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.Location = new System.Drawing.Point(608, 1229);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel24.ParentStyleUsing.UseFont = false;
            this.xrLabel24.Size = new System.Drawing.Size(92, 23);
            this.xrLabel24.Text = "Kiểm tra toàn bộ";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.Location = new System.Drawing.Point(333, 1235);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.ParentStyleUsing.UseBorders = false;
            this.xrLabel22.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel22.ParentStyleUsing.UseFont = false;
            this.xrLabel22.Size = new System.Drawing.Size(10, 10);
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.Location = new System.Drawing.Point(592, 1235);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.ParentStyleUsing.UseBorders = false;
            this.xrLabel21.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel21.ParentStyleUsing.UseFont = false;
            this.xrLabel21.Size = new System.Drawing.Size(10, 10);
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable40
            // 
            this.xrTable40.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable40.Location = new System.Drawing.Point(25, 1251);
            this.xrTable40.Name = "xrTable40";
            this.xrTable40.ParentStyleUsing.UseBorders = false;
            this.xrTable40.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow53});
            this.xrTable40.Size = new System.Drawing.Size(767, 23);
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell160});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Size = new System.Drawing.Size(767, 23);
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell160.CanGrow = false;
            this.xrTableCell160.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell160.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell160.ParentStyleUsing.UseBorders = false;
            this.xrTableCell160.ParentStyleUsing.UseFont = false;
            this.xrTableCell160.Size = new System.Drawing.Size(767, 23);
            this.xrTableCell160.Text = "Địa điểm kiểm tra :";
            this.xrTableCell160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.BorderWidth = 0;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.Location = new System.Drawing.Point(333, 1251);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel20.ParentStyleUsing.UseFont = false;
            this.xrLabel20.Size = new System.Drawing.Size(459, 23);
            this.xrLabel20.Text = "Thời gian kiểm tra: Từ:                           giờ,ngày                       " +
                "  Đến:                giờ, ngày";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable39
            // 
            this.xrTable39.BorderColor = System.Drawing.Color.DarkGray;
            this.xrTable39.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable39.Location = new System.Drawing.Point(33, 1292);
            this.xrTable39.Name = "xrTable39";
            this.xrTable39.ParentStyleUsing.UseBorderColor = false;
            this.xrTable39.ParentStyleUsing.UseBorders = false;
            this.xrTable39.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51,
            this.xrTableRow52});
            this.xrTable39.Size = new System.Drawing.Size(751, 225);
            this.xrTable39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell151});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell151.Size = new System.Drawing.Size(751, 25);
            this.xrTableCell151.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell152});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell152.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell153});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell153.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell154});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell154.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell155});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell155.Size = new System.Drawing.Size(751, 25);
            this.xrTableCell155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell156});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell156.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell157});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell157.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell158});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell158.Size = new System.Drawing.Size(751, 25);
            this.xrTableCell158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell159});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell159.Size = new System.Drawing.Size(751, 25);
            // 
            // xrTable38
            // 
            this.xrTable38.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable38.Location = new System.Drawing.Point(25, 1125);
            this.xrTable38.Name = "xrTable38";
            this.xrTable38.ParentStyleUsing.UseBorders = false;
            this.xrTable38.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow43});
            this.xrTable38.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell150});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell150.CanGrow = false;
            this.xrTableCell150.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell150.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell150.ParentStyleUsing.UseBorders = false;
            this.xrTableCell150.ParentStyleUsing.UseFont = false;
            this.xrTableCell150.Size = new System.Drawing.Size(767, 30);
            this.xrTableCell150.Text = "B-PHẦN DÀNH CHO KIỂM TRA CỦA HẢI QUAN";
            this.xrTableCell150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable37
            // 
            this.xrTable37.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable37.Location = new System.Drawing.Point(25, 1154);
            this.xrTable37.Name = "xrTable37";
            this.xrTable37.ParentStyleUsing.UseBorders = false;
            this.xrTable37.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow42});
            this.xrTable37.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell149});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell149.CanGrow = false;
            this.xrTableCell149.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell149.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell149.ParentStyleUsing.UseBorders = false;
            this.xrTableCell149.ParentStyleUsing.UseFont = false;
            this.xrTableCell149.Size = new System.Drawing.Size(767, 30);
            this.xrTableCell149.Text = "I-PHẦN KIỂM TRA HÀNG HÓA ";
            this.xrTableCell149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTenDaiLyTTHQ
            // 
            this.lblTenDaiLyTTHQ.BorderWidth = 0;
            this.lblTenDaiLyTTHQ.CanGrow = false;
            this.lblTenDaiLyTTHQ.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblTenDaiLyTTHQ.Location = new System.Drawing.Point(33, 483);
            this.lblTenDaiLyTTHQ.Name = "lblTenDaiLyTTHQ";
            this.lblTenDaiLyTTHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDaiLyTTHQ.ParentStyleUsing.UseBorderWidth = false;
            this.lblTenDaiLyTTHQ.ParentStyleUsing.UseFont = false;
            this.lblTenDaiLyTTHQ.Size = new System.Drawing.Size(375, 42);
            this.lblTenDaiLyTTHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable36
            // 
            this.xrTable36.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable36.Location = new System.Drawing.Point(408, 1088);
            this.xrTable36.Name = "xrTable36";
            this.xrTable36.ParentStyleUsing.UseBorders = false;
            this.xrTable36.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow41});
            this.xrTable36.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell142});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell142.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell142.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 8, 0, 0, 100F);
            this.xrTableCell142.ParentStyleUsing.UseBorders = false;
            this.xrTableCell142.ParentStyleUsing.UseFont = false;
            this.xrTableCell142.Size = new System.Drawing.Size(384, 23);
            this.xrTableCell142.Text = " (Người khai báo ghi rõ họ tên, chức danh, ký tên và đóng dấu)";
            this.xrTableCell142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable35
            // 
            this.xrTable35.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable35.Location = new System.Drawing.Point(408, 930);
            this.xrTable35.Name = "xrTable35";
            this.xrTable35.ParentStyleUsing.UseBorders = false;
            this.xrTable35.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow40});
            this.xrTable35.Size = new System.Drawing.Size(384, 159);
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell144});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Size = new System.Drawing.Size(384, 159);
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell144.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell144.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell144.Multiline = true;
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 10, 0, 100F);
            this.xrTableCell144.ParentStyleUsing.UseBorders = false;
            this.xrTableCell144.ParentStyleUsing.UseFont = false;
            this.xrTableCell144.Size = new System.Drawing.Size(384, 159);
            this.xrTableCell144.Text = "29. Tôi xin cam đoan, chụi trách nhiệm trước pháp luật về những nội dung khai báo" +
                " trên tờ khai này.\r\n                                                       Ngày." +
                "........tháng.........năm........\r\n\r\n\r\n";
            // 
            // xrTable34
            // 
            this.xrTable34.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable34.Location = new System.Drawing.Point(25, 133);
            this.xrTable34.Name = "xrTable34";
            this.xrTable34.ParentStyleUsing.UseBorders = false;
            this.xrTable34.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow39});
            this.xrTable34.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell148});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell148.CanGrow = false;
            this.xrTableCell148.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell148.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell148.ParentStyleUsing.UseBorders = false;
            this.xrTableCell148.ParentStyleUsing.UseFont = false;
            this.xrTableCell148.Size = new System.Drawing.Size(767, 30);
            this.xrTableCell148.Text = "A-PHẦN DÀNH CHO NGƯỜI KHAI HẢI QUAN KÊ KHAI VÀ TÍNH THUẾ";
            this.xrTableCell148.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanChinh5
            // 
            this.lblSoBanChinh5.Location = new System.Drawing.Point(208, 1046);
            this.lblSoBanChinh5.Name = "lblSoBanChinh5";
            this.lblSoBanChinh5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh5.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanSao5
            // 
            this.lblSoBanSao5.Location = new System.Drawing.Point(317, 1046);
            this.lblSoBanSao5.Name = "lblSoBanSao5";
            this.lblSoBanSao5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao5.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanSao5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable33
            // 
            this.xrTable33.Location = new System.Drawing.Point(25, 1088);
            this.xrTable33.Name = "xrTable33";
            this.xrTable33.ParentStyleUsing.UseBorders = false;
            this.xrTable33.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow38});
            this.xrTable33.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell146,
            this.xrTableCell147});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell146.CanGrow = false;
            this.xrTableCell146.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell146.Multiline = true;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell146.ParentStyleUsing.UseBorders = false;
            this.xrTableCell146.Size = new System.Drawing.Size(145, 23);
            this.xrTableCell146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell147.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell147.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell147.Multiline = true;
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell147.ParentStyleUsing.UseBorders = false;
            this.xrTableCell147.ParentStyleUsing.UseFont = false;
            this.xrTableCell147.Size = new System.Drawing.Size(239, 23);
            this.xrTableCell147.Text = ":         ......................             ...................";
            this.xrTableCell147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable32
            // 
            this.xrTable32.Location = new System.Drawing.Point(25, 1065);
            this.xrTable32.Name = "xrTable32";
            this.xrTable32.ParentStyleUsing.UseBorders = false;
            this.xrTable32.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow37});
            this.xrTable32.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTenChungTu6,
            this.xrTableCell145});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Size = new System.Drawing.Size(384, 23);
            // 
            // lblTenChungTu6
            // 
            this.lblTenChungTu6.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.lblTenChungTu6.CanGrow = false;
            this.lblTenChungTu6.Location = new System.Drawing.Point(0, 0);
            this.lblTenChungTu6.Multiline = true;
            this.lblTenChungTu6.Name = "lblTenChungTu6";
            this.lblTenChungTu6.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.lblTenChungTu6.ParentStyleUsing.UseBorders = false;
            this.lblTenChungTu6.Size = new System.Drawing.Size(145, 23);
            this.lblTenChungTu6.Text = "-";
            this.lblTenChungTu6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell145.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell145.Multiline = true;
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell145.ParentStyleUsing.UseFont = false;
            this.xrTableCell145.Size = new System.Drawing.Size(239, 23);
            this.xrTableCell145.Text = ":         ......................             ...................";
            this.xrTableCell145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable31
            // 
            this.xrTable31.Location = new System.Drawing.Point(25, 1043);
            this.xrTable31.Name = "xrTable31";
            this.xrTable31.ParentStyleUsing.UseBorders = false;
            this.xrTable31.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable31.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTenChungTu5,
            this.xrTableCell143});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Size = new System.Drawing.Size(384, 23);
            // 
            // lblTenChungTu5
            // 
            this.lblTenChungTu5.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.lblTenChungTu5.CanGrow = false;
            this.lblTenChungTu5.Location = new System.Drawing.Point(0, 0);
            this.lblTenChungTu5.Multiline = true;
            this.lblTenChungTu5.Name = "lblTenChungTu5";
            this.lblTenChungTu5.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.lblTenChungTu5.ParentStyleUsing.UseBorders = false;
            this.lblTenChungTu5.Size = new System.Drawing.Size(145, 23);
            this.lblTenChungTu5.Text = "-";
            this.lblTenChungTu5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell143.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell143.Multiline = true;
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell143.ParentStyleUsing.UseFont = false;
            this.xrTableCell143.Size = new System.Drawing.Size(239, 23);
            this.xrTableCell143.Text = ":         ......................             ...................";
            this.xrTableCell143.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable30
            // 
            this.xrTable30.Location = new System.Drawing.Point(25, 1021);
            this.xrTable30.Name = "xrTable30";
            this.xrTable30.ParentStyleUsing.UseBorders = false;
            this.xrTable30.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow35});
            this.xrTable30.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell140,
            this.xrTableCell141});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell140.CanGrow = false;
            this.xrTableCell140.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell140.Multiline = true;
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell140.ParentStyleUsing.UseBorders = false;
            this.xrTableCell140.Size = new System.Drawing.Size(145, 23);
            this.xrTableCell140.Text = "- Vân tải đơn";
            this.xrTableCell140.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell141.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell141.Multiline = true;
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell141.ParentStyleUsing.UseFont = false;
            this.xrTableCell141.Size = new System.Drawing.Size(239, 23);
            this.xrTableCell141.Text = ":         ......................             ...................";
            this.xrTableCell141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable29
            // 
            this.xrTable29.Location = new System.Drawing.Point(25, 999);
            this.xrTable29.Name = "xrTable29";
            this.xrTable29.ParentStyleUsing.UseBorders = false;
            this.xrTable29.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34});
            this.xrTable29.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell138,
            this.xrTableCell139});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell138.CanGrow = false;
            this.xrTableCell138.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell138.Multiline = true;
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell138.ParentStyleUsing.UseBorders = false;
            this.xrTableCell138.Size = new System.Drawing.Size(145, 23);
            this.xrTableCell138.Text = "- Bản kê chi tiết";
            this.xrTableCell138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell139.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell139.Multiline = true;
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell139.ParentStyleUsing.UseFont = false;
            this.xrTableCell139.Size = new System.Drawing.Size(239, 23);
            this.xrTableCell139.Text = ":         ......................             ...................";
            this.xrTableCell139.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable25
            // 
            this.xrTable25.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable25.Location = new System.Drawing.Point(100, 908);
            this.xrTable25.Name = "xrTable25";
            this.xrTable25.ParentStyleUsing.UseBorders = false;
            this.xrTable25.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30});
            this.xrTable25.Size = new System.Drawing.Size(675, 22);
            this.xrTable25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTongThueXNKChu});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Size = new System.Drawing.Size(675, 22);
            // 
            // lblTongThueXNKChu
            // 
            this.lblTongThueXNKChu.CanGrow = false;
            this.lblTongThueXNKChu.Location = new System.Drawing.Point(0, 0);
            this.lblTongThueXNKChu.Multiline = true;
            this.lblTongThueXNKChu.Name = "lblTongThueXNKChu";
            this.lblTongThueXNKChu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueXNKChu.ParentStyleUsing.UseBorders = false;
            this.lblTongThueXNKChu.Size = new System.Drawing.Size(675, 22);
            this.lblTongThueXNKChu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable28
            // 
            this.xrTable28.Location = new System.Drawing.Point(25, 978);
            this.xrTable28.Name = "xrTable28";
            this.xrTable28.ParentStyleUsing.UseBorders = false;
            this.xrTable28.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33});
            this.xrTable28.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell136,
            this.xrTableCell137});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell136.CanGrow = false;
            this.xrTableCell136.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell136.Multiline = true;
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell136.ParentStyleUsing.UseBorders = false;
            this.xrTableCell136.Size = new System.Drawing.Size(145, 23);
            this.xrTableCell136.Text = "- Hóa đơn thương mại";
            this.xrTableCell136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell137.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell137.Multiline = true;
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell137.ParentStyleUsing.UseFont = false;
            this.xrTableCell137.Size = new System.Drawing.Size(239, 23);
            this.xrTableCell137.Text = ":         ......................             ...................";
            this.xrTableCell137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable27
            // 
            this.xrTable27.Location = new System.Drawing.Point(25, 956);
            this.xrTable27.Name = "xrTable27";
            this.xrTable27.ParentStyleUsing.UseBorders = false;
            this.xrTable27.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32});
            this.xrTable27.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell132,
            this.xrTableCell135});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Size = new System.Drawing.Size(384, 23);
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell132.CanGrow = false;
            this.xrTableCell132.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell132.Multiline = true;
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell132.ParentStyleUsing.UseBorders = false;
            this.xrTableCell132.Size = new System.Drawing.Size(145, 23);
            this.xrTableCell132.Text = "- Hợp đồng thương mại";
            this.xrTableCell132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell135.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell135.Multiline = true;
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell135.ParentStyleUsing.UseFont = false;
            this.xrTableCell135.Size = new System.Drawing.Size(239, 23);
            this.xrTableCell135.Text = ":         ......................             ...................";
            this.xrTableCell135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable26
            // 
            this.xrTable26.Location = new System.Drawing.Point(25, 930);
            this.xrTable26.Name = "xrTable26";
            this.xrTable26.ParentStyleUsing.UseBorders = false;
            this.xrTable26.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
            this.xrTable26.Size = new System.Drawing.Size(384, 27);
            this.xrTable26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell131,
            this.xrTableCell134});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Size = new System.Drawing.Size(384, 27);
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell131.CanGrow = false;
            this.xrTableCell131.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell131.Multiline = true;
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell131.ParentStyleUsing.UseBorders = false;
            this.xrTableCell131.Size = new System.Drawing.Size(145, 27);
            this.xrTableCell131.Text = "28. Chứng từ kèm:                                                     \r\n         " +
                "                                              ";
            this.xrTableCell131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell134.Multiline = true;
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell134.Size = new System.Drawing.Size(239, 27);
            this.xrTableCell134.Text = "Bản chính                  Bản sao\r\n";
            this.xrTableCell134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrTablesaaa
            // 
            this.xrTablesaaa.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTablesaaa.Location = new System.Drawing.Point(374, 887);
            this.xrTablesaaa.Name = "xrTablesaaa";
            this.xrTablesaaa.ParentStyleUsing.UseBorders = false;
            this.xrTablesaaa.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29});
            this.xrTablesaaa.Size = new System.Drawing.Size(400, 22);
            this.xrTablesaaa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTongThueXNKSo});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Size = new System.Drawing.Size(400, 22);
            // 
            // lblTongThueXNKSo
            // 
            this.lblTongThueXNKSo.CanGrow = false;
            this.lblTongThueXNKSo.Location = new System.Drawing.Point(0, 0);
            this.lblTongThueXNKSo.Multiline = true;
            this.lblTongThueXNKSo.Name = "lblTongThueXNKSo";
            this.lblTongThueXNKSo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueXNKSo.ParentStyleUsing.UseBorders = false;
            this.lblTongThueXNKSo.Size = new System.Drawing.Size(400, 22);
            this.lblTongThueXNKSo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable23
            // 
            this.xrTable23.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable23.Location = new System.Drawing.Point(25, 884);
            this.xrTable23.Name = "xrTable23";
            this.xrTable23.ParentStyleUsing.UseBorders = false;
            this.xrTable23.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27});
            this.xrTable23.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell133});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Size = new System.Drawing.Size(767, 25);
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell133.CanGrow = false;
            this.xrTableCell133.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell133.Multiline = true;
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell133.ParentStyleUsing.UseBorders = false;
            this.xrTableCell133.Size = new System.Drawing.Size(767, 25);
            this.xrTableCell133.Text = "27. Tổng số tiền thuế và thu khác (ô 24+25+26) : Bằng số:........................" +
                "................................................................................" +
                ".........................\r\n";
            this.xrTableCell133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable22
            // 
            this.xrTable22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable22.Location = new System.Drawing.Point(25, 856);
            this.xrTable22.Name = "xrTable22";
            this.xrTable22.ParentStyleUsing.UseBorders = false;
            this.xrTable22.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26});
            this.xrTable22.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell127,
            this.lblTongThueXNK,
            this.xrTableCell128,
            this.lblTongTienThueGTGT,
            this.lblTongTriGiaThuKhac});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell127.CanGrow = false;
            this.xrTableCell127.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell127.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 5, 0, 0, 100F);
            this.xrTableCell127.ParentStyleUsing.UseBorders = false;
            this.xrTableCell127.ParentStyleUsing.UseFont = false;
            this.xrTableCell127.Size = new System.Drawing.Size(188, 30);
            this.xrTableCell127.Text = "Cộng :";
            this.xrTableCell127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblTongThueXNK
            // 
            this.lblTongThueXNK.Location = new System.Drawing.Point(188, 0);
            this.lblTongThueXNK.Name = "lblTongThueXNK";
            this.lblTongThueXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongThueXNK.Size = new System.Drawing.Size(119, 30);
            this.lblTongThueXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell128.CanGrow = false;
            this.xrTableCell128.Location = new System.Drawing.Point(307, 0);
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell128.ParentStyleUsing.UseBorders = false;
            this.xrTableCell128.Size = new System.Drawing.Size(164, 30);
            this.xrTableCell128.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTongTienThueGTGT
            // 
            this.lblTongTienThueGTGT.CanGrow = false;
            this.lblTongTienThueGTGT.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongTienThueGTGT.Location = new System.Drawing.Point(471, 0);
            this.lblTongTienThueGTGT.Name = "lblTongTienThueGTGT";
            this.lblTongTienThueGTGT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 5, 0, 0, 100F);
            this.lblTongTienThueGTGT.ParentStyleUsing.UseFont = false;
            this.lblTongTienThueGTGT.Size = new System.Drawing.Size(120, 30);
            this.lblTongTienThueGTGT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblTongTriGiaThuKhac
            // 
            this.lblTongTriGiaThuKhac.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTriGiaThuKhac.CanGrow = false;
            this.lblTongTriGiaThuKhac.Location = new System.Drawing.Point(591, 0);
            this.lblTongTriGiaThuKhac.Name = "lblTongTriGiaThuKhac";
            this.lblTongTriGiaThuKhac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaThuKhac.ParentStyleUsing.UseBorders = false;
            this.lblTongTriGiaThuKhac.Size = new System.Drawing.Size(176, 30);
            this.lblTongTriGiaThuKhac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable20
            // 
            this.xrTable20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable20.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable20.Location = new System.Drawing.Point(50, 725);
            this.xrTable20.Name = "xrTable20";
            this.xrTable20.ParentStyleUsing.UseBorders = false;
            this.xrTable20.ParentStyleUsing.UseFont = false;
            this.xrTable20.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
            this.xrTable20.Size = new System.Drawing.Size(742, 42);
            this.xrTable20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell104,
            this.xrTableCell111,
            this.xrTableCell109,
            this.xrTableCell112,
            this.xrTableCell113,
            this.xrTableCell106,
            this.xrTableCell108,
            this.xrTableCell114});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Size = new System.Drawing.Size(742, 42);
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell104.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell104.Multiline = true;
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell104.ParentStyleUsing.UseFont = false;
            this.xrTableCell104.Size = new System.Drawing.Size(120, 42);
            this.xrTableCell104.Text = "Trị giá tình thuế";
            this.xrTableCell104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Location = new System.Drawing.Point(120, 0);
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell111.Size = new System.Drawing.Size(43, 42);
            this.xrTableCell111.Text = "Thuế suất (%)";
            this.xrTableCell111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Location = new System.Drawing.Point(163, 0);
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell109.Size = new System.Drawing.Size(120, 42);
            this.xrTableCell109.Text = "Tiền thuế";
            this.xrTableCell109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Location = new System.Drawing.Point(283, 0);
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell112.Size = new System.Drawing.Size(121, 42);
            this.xrTableCell112.Text = "Trị giá tình thuế";
            this.xrTableCell112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Location = new System.Drawing.Point(404, 0);
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell113.Size = new System.Drawing.Size(42, 42);
            this.xrTableCell113.Text = "Thuế suất (%)";
            this.xrTableCell113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell106.Location = new System.Drawing.Point(446, 0);
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell106.ParentStyleUsing.UseFont = false;
            this.xrTableCell106.Size = new System.Drawing.Size(121, 42);
            this.xrTableCell106.Text = "Tiền thuế";
            this.xrTableCell106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell108.Location = new System.Drawing.Point(567, 0);
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell108.ParentStyleUsing.UseFont = false;
            this.xrTableCell108.Size = new System.Drawing.Size(45, 42);
            this.xrTableCell108.Text = "Tỷ lệ(%)";
            this.xrTableCell108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Location = new System.Drawing.Point(612, 0);
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell114.Size = new System.Drawing.Size(130, 42);
            this.xrTableCell114.Text = "Số tiền";
            this.xrTableCell114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel40.Location = new System.Drawing.Point(25, 692);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.ParentStyleUsing.UseBorders = false;
            this.xrLabel40.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel40.ParentStyleUsing.UseFont = false;
            this.xrLabel40.Size = new System.Drawing.Size(25, 75);
            this.xrLabel40.Text = "SỐ TT";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable19
            // 
            this.xrTable19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable19.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable19.Location = new System.Drawing.Point(50, 692);
            this.xrTable19.Name = "xrTable19";
            this.xrTable19.ParentStyleUsing.UseBorders = false;
            this.xrTable19.ParentStyleUsing.UseFont = false;
            this.xrTable19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.xrTable19.Size = new System.Drawing.Size(742, 34);
            this.xrTable19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.xrTableCell107,
            this.xrTableCell110});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Size = new System.Drawing.Size(742, 34);
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell105.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell105.Multiline = true;
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell105.ParentStyleUsing.UseFont = false;
            this.xrTableCell105.Size = new System.Drawing.Size(283, 34);
            this.xrTableCell105.Text = "24.THUẾ NHẬP KHẨU";
            this.xrTableCell105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell107.Location = new System.Drawing.Point(283, 0);
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell107.ParentStyleUsing.UseFont = false;
            this.xrTableCell107.Size = new System.Drawing.Size(284, 34);
            this.xrTableCell107.Text = "25. THUẾ GTGT(HOẶC TTĐB)";
            this.xrTableCell107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell110.Location = new System.Drawing.Point(567, 0);
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell110.ParentStyleUsing.UseFont = false;
            this.xrTableCell110.Size = new System.Drawing.Size(175, 34);
            this.xrTableCell110.Text = "26.THU KHÁC";
            this.xrTableCell110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable16
            // 
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable16.Location = new System.Drawing.Point(25, 533);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.ParentStyleUsing.UseBorders = false;
            this.xrTable16.ParentStyleUsing.UseFont = false;
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.xrTable16.Size = new System.Drawing.Size(767, 41);
            this.xrTable16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell88,
            this.xrTableCell80,
            this.xrTableCell89,
            this.xrTableCell81,
            this.xrTableCell83,
            this.xrTableCell91,
            this.xrTableCell90,
            this.xrTableCell92});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Size = new System.Drawing.Size(767, 41);
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell88.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell88.ParentStyleUsing.UseFont = false;
            this.xrTableCell88.Size = new System.Drawing.Size(25, 41);
            this.xrTableCell88.Text = "SỐ TT";
            this.xrTableCell88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell80.Location = new System.Drawing.Point(25, 0);
            this.xrTableCell80.Multiline = true;
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell80.ParentStyleUsing.UseFont = false;
            this.xrTableCell80.Size = new System.Drawing.Size(283, 41);
            this.xrTableCell80.Text = "17. TÊN HÀNG \r\n QUY CÁCH PHẨM CHẤT";
            this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell89.Location = new System.Drawing.Point(308, 0);
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell89.ParentStyleUsing.UseFont = false;
            this.xrTableCell89.Size = new System.Drawing.Size(84, 41);
            this.xrTableCell89.Text = "18. MÃ SỐ HÀNG HÓA";
            this.xrTableCell89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell81.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell81.ParentStyleUsing.UseFont = false;
            this.xrTableCell81.Size = new System.Drawing.Size(66, 41);
            this.xrTableCell81.Text = "19. XUẤT XỨ";
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell83.Location = new System.Drawing.Point(458, 0);
            this.xrTableCell83.Multiline = true;
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell83.ParentStyleUsing.UseFont = false;
            this.xrTableCell83.Size = new System.Drawing.Size(67, 41);
            this.xrTableCell83.Text = "20.LƯỢNG\r\n      ";
            this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell91.Location = new System.Drawing.Point(525, 0);
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell91.ParentStyleUsing.UseFont = false;
            this.xrTableCell91.Size = new System.Drawing.Size(67, 41);
            this.xrTableCell91.Text = "21. ĐƠN VỊ TÍNH";
            this.xrTableCell91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell90.Location = new System.Drawing.Point(592, 0);
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell90.ParentStyleUsing.UseFont = false;
            this.xrTableCell90.Size = new System.Drawing.Size(80, 41);
            this.xrTableCell90.Text = "22. ĐƠN GIÁ NGUYÊN TỆ";
            this.xrTableCell90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell92.Location = new System.Drawing.Point(672, 0);
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell92.ParentStyleUsing.UseFont = false;
            this.xrTableCell92.Size = new System.Drawing.Size(95, 41);
            this.xrTableCell92.Text = "23. TRỊ GIÁ NGUYÊN TỆ ";
            this.xrTableCell92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable14
            // 
            this.xrTable14.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable14.Location = new System.Drawing.Point(25, 442);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.ParentStyleUsing.UseBorders = false;
            this.xrTable14.ParentStyleUsing.UseFont = false;
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18});
            this.xrTable14.Size = new System.Drawing.Size(767, 92);
            this.xrTable14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell65,
            this.xrTableCell79,
            this.xrTableCell82,
            this.xrTableCell87});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Size = new System.Drawing.Size(767, 92);
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell65.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable15,
            this.lblMaDaiLyTTHQ,
            this.xrLabel41});
            this.xrTableCell65.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell65.ParentStyleUsing.UseBorders = false;
            this.xrTableCell65.Size = new System.Drawing.Size(392, 92);
            this.xrTableCell65.Text = "xrTableCell24";
            // 
            // xrTable15
            // 
            this.xrTable15.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable15.Location = new System.Drawing.Point(130, 1);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.ParentStyleUsing.UseBorders = false;
            this.xrTable15.ParentStyleUsing.UseFont = false;
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19});
            this.xrTable15.Size = new System.Drawing.Size(260, 25);
            this.xrTable15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell70,
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell78});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Size = new System.Drawing.Size(260, 25);
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell66.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell66.ParentStyleUsing.UseBorders = false;
            this.xrTableCell66.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell66.Text = " ";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell67.Location = new System.Drawing.Point(20, 0);
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell67.ParentStyleUsing.UseBorders = false;
            this.xrTableCell67.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell67.Text = " ";
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell68.Location = new System.Drawing.Point(40, 0);
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell68.ParentStyleUsing.UseBorders = false;
            this.xrTableCell68.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell68.Text = " ";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell69.Location = new System.Drawing.Point(60, 0);
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell69.ParentStyleUsing.UseBorders = false;
            this.xrTableCell69.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell69.Text = " ";
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell70.Location = new System.Drawing.Point(80, 0);
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell70.ParentStyleUsing.UseBorders = false;
            this.xrTableCell70.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell70.Text = " ";
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell71.Location = new System.Drawing.Point(100, 0);
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell71.ParentStyleUsing.UseBorders = false;
            this.xrTableCell71.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell71.Text = " ";
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell72.Location = new System.Drawing.Point(120, 0);
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell72.ParentStyleUsing.UseBorders = false;
            this.xrTableCell72.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell72.Text = " ";
            this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell73.Location = new System.Drawing.Point(140, 0);
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell73.ParentStyleUsing.UseBorders = false;
            this.xrTableCell73.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell73.Text = " ";
            this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell74.Location = new System.Drawing.Point(160, 0);
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell74.ParentStyleUsing.UseBorders = false;
            this.xrTableCell74.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell74.Text = " ";
            this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell75.Location = new System.Drawing.Point(180, 0);
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell75.ParentStyleUsing.UseBorders = false;
            this.xrTableCell75.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell75.Text = " ";
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell76.Location = new System.Drawing.Point(200, 0);
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell76.ParentStyleUsing.UseBorders = false;
            this.xrTableCell76.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell76.Text = " ";
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell77.Location = new System.Drawing.Point(220, 0);
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell77.ParentStyleUsing.UseBorders = false;
            this.xrTableCell77.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell77.Text = " ";
            this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell78.Location = new System.Drawing.Point(240, 0);
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell78.ParentStyleUsing.UseBorders = false;
            this.xrTableCell78.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell78.Text = " ";
            this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDaiLyTTHQ
            // 
            this.lblMaDaiLyTTHQ.BorderWidth = 0;
            this.lblMaDaiLyTTHQ.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaDaiLyTTHQ.Location = new System.Drawing.Point(112, 0);
            this.lblMaDaiLyTTHQ.Name = "lblMaDaiLyTTHQ";
            this.lblMaDaiLyTTHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblMaDaiLyTTHQ.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaDaiLyTTHQ.ParentStyleUsing.UseFont = false;
            this.lblMaDaiLyTTHQ.Size = new System.Drawing.Size(260, 24);
            this.lblMaDaiLyTTHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel41
            // 
            this.xrLabel41.BorderWidth = 0;
            this.xrLabel41.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel41.Location = new System.Drawing.Point(8, 0);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel41.ParentStyleUsing.UseFont = false;
            this.xrLabel41.Size = new System.Drawing.Size(108, 41);
            this.xrLabel41.Text = "4. Đại lý làm thủ tục hải quan";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell79.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblDKGH,
            this.xrLabel43});
            this.xrTableCell79.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell79.ParentStyleUsing.UseBorders = false;
            this.xrTableCell79.Size = new System.Drawing.Size(95, 92);
            this.xrTableCell79.Text = "xrTableCell79";
            this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblDKGH
            // 
            this.lblDKGH.BackColor = System.Drawing.Color.Ivory;
            this.lblDKGH.BorderWidth = 0;
            this.lblDKGH.Location = new System.Drawing.Point(8, 42);
            this.lblDKGH.Name = "lblDKGH";
            this.lblDKGH.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDKGH.ParentStyleUsing.UseBackColor = false;
            this.lblDKGH.ParentStyleUsing.UseBorderWidth = false;
            this.lblDKGH.Size = new System.Drawing.Size(83, 42);
            this.lblDKGH.Tag = "DKGH";
            this.lblDKGH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDKGH.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // xrLabel43
            // 
            this.xrLabel43.BorderWidth = 0;
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel43.Location = new System.Drawing.Point(8, 0);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel43.ParentStyleUsing.UseFont = false;
            this.xrLabel43.Size = new System.Drawing.Size(83, 41);
            this.xrLabel43.Text = "14. Điều kiện giao hàng:";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell82.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel39,
            this.lblTyGiaTT,
            this.xrTable17,
            this.xrLabel46,
            this.lblNgoaiTe});
            this.xrTableCell82.Location = new System.Drawing.Point(487, 0);
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell82.ParentStyleUsing.UseBorders = false;
            this.xrTableCell82.Size = new System.Drawing.Size(146, 92);
            this.xrTableCell82.Text = "xrTableCell82";
            this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel39
            // 
            this.xrLabel39.BorderWidth = 0;
            this.xrLabel39.CanGrow = false;
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel39.Location = new System.Drawing.Point(5, 52);
            this.xrLabel39.Multiline = true;
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel39.ParentStyleUsing.UseFont = false;
            this.xrLabel39.Size = new System.Drawing.Size(58, 33);
            this.xrLabel39.Text = "Tỷ giá \r\ntính thuế:";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTyGiaTT
            // 
            this.lblTyGiaTT.BorderWidth = 0;
            this.lblTyGiaTT.CanGrow = false;
            this.lblTyGiaTT.Location = new System.Drawing.Point(61, 52);
            this.lblTyGiaTT.Name = "lblTyGiaTT";
            this.lblTyGiaTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTyGiaTT.ParentStyleUsing.UseBorderWidth = false;
            this.lblTyGiaTT.Size = new System.Drawing.Size(75, 33);
            this.lblTyGiaTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable17
            // 
            this.xrTable17.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable17.Location = new System.Drawing.Point(85, 0);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.ParentStyleUsing.UseBorders = false;
            this.xrTable17.ParentStyleUsing.UseFont = false;
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.xrTable17.Size = new System.Drawing.Size(60, 25);
            this.xrTable17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell84,
            this.xrTableCell85,
            this.xrTableCell86});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Size = new System.Drawing.Size(60, 25);
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell84.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell84.ParentStyleUsing.UseBorders = false;
            this.xrTableCell84.Size = new System.Drawing.Size(21, 25);
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell85.Location = new System.Drawing.Point(21, 0);
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell85.ParentStyleUsing.UseBorders = false;
            this.xrTableCell85.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell86.Location = new System.Drawing.Point(41, 0);
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell86.ParentStyleUsing.UseBorders = false;
            this.xrTableCell86.Size = new System.Drawing.Size(19, 25);
            // 
            // xrLabel46
            // 
            this.xrLabel46.BorderWidth = 0;
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel46.Location = new System.Drawing.Point(5, 0);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel46.ParentStyleUsing.UseFont = false;
            this.xrLabel46.Size = new System.Drawing.Size(73, 41);
            this.xrLabel46.Text = "15. Đồng tiền thanh toán:";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNgoaiTe
            // 
            this.lblNgoaiTe.BorderWidth = 0;
            this.lblNgoaiTe.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgoaiTe.Location = new System.Drawing.Point(85, 0);
            this.lblNgoaiTe.Multiline = true;
            this.lblNgoaiTe.Name = "lblNgoaiTe";
            this.lblNgoaiTe.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.lblNgoaiTe.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgoaiTe.ParentStyleUsing.UseFont = false;
            this.lblNgoaiTe.Size = new System.Drawing.Size(61, 25);
            this.lblNgoaiTe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell87.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblPTTT,
            this.xrLabel49});
            this.xrTableCell87.Location = new System.Drawing.Point(633, 0);
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell87.ParentStyleUsing.UseBorders = false;
            this.xrTableCell87.Size = new System.Drawing.Size(134, 92);
            this.xrTableCell87.Text = "xrTableCell87";
            this.xrTableCell87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblPTTT
            // 
            this.lblPTTT.BackColor = System.Drawing.Color.Ivory;
            this.lblPTTT.BorderWidth = 0;
            this.lblPTTT.CanGrow = false;
            this.lblPTTT.Location = new System.Drawing.Point(4, 42);
            this.lblPTTT.Name = "lblPTTT";
            this.lblPTTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPTTT.ParentStyleUsing.UseBackColor = false;
            this.lblPTTT.ParentStyleUsing.UseBorderWidth = false;
            this.lblPTTT.Size = new System.Drawing.Size(121, 42);
            this.lblPTTT.Tag = "PTTT";
            this.lblPTTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblPTTT.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // xrLabel49
            // 
            this.xrLabel49.BorderWidth = 0;
            this.xrLabel49.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel49.Location = new System.Drawing.Point(4, 0);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel49.ParentStyleUsing.UseFont = false;
            this.xrLabel49.Size = new System.Drawing.Size(92, 41);
            this.xrLabel49.Text = "16. Phương thức thanh toán:";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable9
            // 
            this.xrTable9.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable9.Location = new System.Drawing.Point(25, 350);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.ParentStyleUsing.UseBorders = false;
            this.xrTable9.ParentStyleUsing.UseFont = false;
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable9.Size = new System.Drawing.Size(767, 94);
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell52,
            this.xrTableCell53,
            this.xrTableCell54});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Size = new System.Drawing.Size(767, 94);
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell38.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10,
            this.lblMaNguoiUyThac,
            this.lblNguoiUyThac,
            this.xrLabel36});
            this.xrTableCell38.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell38.ParentStyleUsing.UseBorders = false;
            this.xrTableCell38.Size = new System.Drawing.Size(392, 94);
            this.xrTableCell38.Text = "xrTableCell24";
            // 
            // xrTable10
            // 
            this.xrTable10.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable10.Location = new System.Drawing.Point(130, 0);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.ParentStyleUsing.UseBorders = false;
            this.xrTable10.ParentStyleUsing.UseFont = false;
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable10.Size = new System.Drawing.Size(260, 25);
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Size = new System.Drawing.Size(260, 25);
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell39.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell39.ParentStyleUsing.UseBorders = false;
            this.xrTableCell39.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell39.Text = " ";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell40.Location = new System.Drawing.Point(20, 0);
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell40.ParentStyleUsing.UseBorders = false;
            this.xrTableCell40.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell40.Text = " ";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell41.Location = new System.Drawing.Point(40, 0);
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell41.ParentStyleUsing.UseBorders = false;
            this.xrTableCell41.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell41.Text = " ";
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell42.Location = new System.Drawing.Point(60, 0);
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell42.ParentStyleUsing.UseBorders = false;
            this.xrTableCell42.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell42.Text = " ";
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell43.Location = new System.Drawing.Point(80, 0);
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell43.ParentStyleUsing.UseBorders = false;
            this.xrTableCell43.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell43.Text = " ";
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell44.Location = new System.Drawing.Point(100, 0);
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell44.ParentStyleUsing.UseBorders = false;
            this.xrTableCell44.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell44.Text = " ";
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell45.Location = new System.Drawing.Point(120, 0);
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell45.ParentStyleUsing.UseBorders = false;
            this.xrTableCell45.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell45.Text = " ";
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell46.Location = new System.Drawing.Point(140, 0);
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell46.ParentStyleUsing.UseBorders = false;
            this.xrTableCell46.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell46.Text = " ";
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell47.Location = new System.Drawing.Point(160, 0);
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell47.ParentStyleUsing.UseBorders = false;
            this.xrTableCell47.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell47.Text = " ";
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell48.Location = new System.Drawing.Point(180, 0);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell48.ParentStyleUsing.UseBorders = false;
            this.xrTableCell48.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell48.Text = " ";
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell49.Location = new System.Drawing.Point(200, 0);
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell49.ParentStyleUsing.UseBorders = false;
            this.xrTableCell49.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell49.Text = " ";
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell50.Location = new System.Drawing.Point(220, 0);
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell50.ParentStyleUsing.UseBorders = false;
            this.xrTableCell50.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell50.Text = " ";
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell51.Location = new System.Drawing.Point(240, 0);
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell51.ParentStyleUsing.UseBorders = false;
            this.xrTableCell51.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell51.Text = " ";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblMaNguoiUyThac
            // 
            this.lblMaNguoiUyThac.BorderWidth = 0;
            this.lblMaNguoiUyThac.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNguoiUyThac.Location = new System.Drawing.Point(112, 0);
            this.lblMaNguoiUyThac.Name = "lblMaNguoiUyThac";
            this.lblMaNguoiUyThac.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblMaNguoiUyThac.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaNguoiUyThac.ParentStyleUsing.UseFont = false;
            this.lblMaNguoiUyThac.Size = new System.Drawing.Size(260, 24);
            this.lblMaNguoiUyThac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNguoiUyThac
            // 
            this.lblNguoiUyThac.BorderWidth = 0;
            this.lblNguoiUyThac.CanGrow = false;
            this.lblNguoiUyThac.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblNguoiUyThac.Location = new System.Drawing.Point(8, 33);
            this.lblNguoiUyThac.Name = "lblNguoiUyThac";
            this.lblNguoiUyThac.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblNguoiUyThac.ParentStyleUsing.UseBorderWidth = false;
            this.lblNguoiUyThac.ParentStyleUsing.UseFont = false;
            this.lblNguoiUyThac.Size = new System.Drawing.Size(375, 58);
            this.lblNguoiUyThac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel36
            // 
            this.xrLabel36.BorderWidth = 0;
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.Location = new System.Drawing.Point(8, 0);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel36.ParentStyleUsing.UseFont = false;
            this.xrLabel36.Size = new System.Drawing.Size(102, 25);
            this.xrLabel36.Text = "3. Người ủy thác";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell52.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMaNuoc,
            this.xrLabel35,
            this.xrTable11,
            this.lblTenNuoc});
            this.xrTableCell52.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell52.ParentStyleUsing.UseBorders = false;
            this.xrTableCell52.Size = new System.Drawing.Size(116, 94);
            this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaNuoc
            // 
            this.lblMaNuoc.BorderWidth = 0;
            this.lblMaNuoc.CanGrow = false;
            this.lblMaNuoc.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNuoc.Location = new System.Drawing.Point(70, 67);
            this.lblMaNuoc.Name = "lblMaNuoc";
            this.lblMaNuoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.lblMaNuoc.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaNuoc.ParentStyleUsing.UseFont = false;
            this.lblMaNuoc.Size = new System.Drawing.Size(50, 25);
            this.lblMaNuoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel35
            // 
            this.xrLabel35.BorderWidth = 0;
            this.xrLabel35.CanGrow = false;
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.Location = new System.Drawing.Point(8, 0);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel35.ParentStyleUsing.UseFont = false;
            this.xrLabel35.Size = new System.Drawing.Size(108, 25);
            this.xrLabel35.Text = "11. Nước xuất khẩu:";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable11
            // 
            this.xrTable11.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable11.Location = new System.Drawing.Point(75, 68);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.ParentStyleUsing.UseBorders = false;
            this.xrTable11.ParentStyleUsing.UseFont = false;
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.xrTable11.Size = new System.Drawing.Size(40, 25);
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55,
            this.xrTableCell56});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Size = new System.Drawing.Size(40, 25);
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell55.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell55.ParentStyleUsing.UseBorders = false;
            this.xrTableCell55.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell56.Location = new System.Drawing.Point(20, 0);
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell56.ParentStyleUsing.UseBorders = false;
            this.xrTableCell56.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTenNuoc
            // 
            this.lblTenNuoc.BorderWidth = 0;
            this.lblTenNuoc.CanGrow = false;
            this.lblTenNuoc.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNuoc.Location = new System.Drawing.Point(8, 33);
            this.lblTenNuoc.Name = "lblTenNuoc";
            this.lblTenNuoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNuoc.ParentStyleUsing.UseBorderWidth = false;
            this.lblTenNuoc.ParentStyleUsing.UseFont = false;
            this.lblTenNuoc.Size = new System.Drawing.Size(100, 34);
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell53.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable12,
            this.xrLabel37,
            this.lblDiaDiemXepHang});
            this.xrTableCell53.Location = new System.Drawing.Point(508, 0);
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell53.ParentStyleUsing.UseBorders = false;
            this.xrTableCell53.Size = new System.Drawing.Size(125, 94);
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable12
            // 
            this.xrTable12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTable12.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable12.Location = new System.Drawing.Point(45, 68);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.ParentStyleUsing.UseBorders = false;
            this.xrTable12.ParentStyleUsing.UseFont = false;
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
            this.xrTable12.Size = new System.Drawing.Size(80, 25);
            this.xrTable12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell59,
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell60});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Size = new System.Drawing.Size(80, 25);
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell59.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Location = new System.Drawing.Point(20, 0);
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell57.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Location = new System.Drawing.Point(40, 0);
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell58.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell60.Location = new System.Drawing.Point(60, 0);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell60.ParentStyleUsing.UseBorders = false;
            this.xrTableCell60.Size = new System.Drawing.Size(20, 25);
            // 
            // xrLabel37
            // 
            this.xrLabel37.BorderWidth = 0;
            this.xrLabel37.CanGrow = false;
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel37.Location = new System.Drawing.Point(8, 0);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel37.ParentStyleUsing.UseFont = false;
            this.xrLabel37.Size = new System.Drawing.Size(100, 33);
            this.xrLabel37.Text = "12. Cảng, địa điểm xếp hàng:";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblDiaDiemXepHang
            // 
            this.lblDiaDiemXepHang.BorderWidth = 0;
            this.lblDiaDiemXepHang.CanGrow = false;
            this.lblDiaDiemXepHang.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaDiemXepHang.Location = new System.Drawing.Point(8, 33);
            this.lblDiaDiemXepHang.Name = "lblDiaDiemXepHang";
            this.lblDiaDiemXepHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaDiemXepHang.ParentStyleUsing.UseBorderWidth = false;
            this.lblDiaDiemXepHang.ParentStyleUsing.UseFont = false;
            this.lblDiaDiemXepHang.Size = new System.Drawing.Size(108, 34);
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell54.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblMaDiaDiemDoHang,
            this.xrTable13,
            this.xrLabel38,
            this.lblDiaDiemDoHang});
            this.xrTableCell54.Location = new System.Drawing.Point(633, 0);
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell54.ParentStyleUsing.UseBorders = false;
            this.xrTableCell54.Size = new System.Drawing.Size(134, 94);
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblMaDiaDiemDoHang
            // 
            this.lblMaDiaDiemDoHang.BorderWidth = 0;
            this.lblMaDiaDiemDoHang.CanGrow = false;
            this.lblMaDiaDiemDoHang.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDiaDiemDoHang.Location = new System.Drawing.Point(56, 67);
            this.lblMaDiaDiemDoHang.Multiline = true;
            this.lblMaDiaDiemDoHang.Name = "lblMaDiaDiemDoHang";
            this.lblMaDiaDiemDoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.lblMaDiaDiemDoHang.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaDiaDiemDoHang.ParentStyleUsing.UseFont = false;
            this.lblMaDiaDiemDoHang.Size = new System.Drawing.Size(80, 25);
            this.lblMaDiaDiemDoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable13
            // 
            this.xrTable13.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable13.Location = new System.Drawing.Point(53, 68);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.ParentStyleUsing.UseBorders = false;
            this.xrTable13.ParentStyleUsing.UseFont = false;
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17});
            this.xrTable13.Size = new System.Drawing.Size(80, 25);
            this.xrTable13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Size = new System.Drawing.Size(80, 25);
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell61.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell61.ParentStyleUsing.UseBorders = false;
            this.xrTableCell61.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell62.Location = new System.Drawing.Point(20, 0);
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell62.ParentStyleUsing.UseBorders = false;
            this.xrTableCell62.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell63.Location = new System.Drawing.Point(40, 0);
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell63.ParentStyleUsing.UseBorders = false;
            this.xrTableCell63.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell64.Location = new System.Drawing.Point(60, 0);
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell64.ParentStyleUsing.UseBorders = false;
            this.xrTableCell64.Size = new System.Drawing.Size(20, 25);
            // 
            // xrLabel38
            // 
            this.xrLabel38.BorderWidth = 0;
            this.xrLabel38.CanGrow = false;
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel38.Location = new System.Drawing.Point(4, 0);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel38.ParentStyleUsing.UseFont = false;
            this.xrLabel38.Size = new System.Drawing.Size(100, 33);
            this.xrLabel38.Text = "12. Cảng, địa điểm dỡ hàng:";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblDiaDiemDoHang
            // 
            this.lblDiaDiemDoHang.BorderWidth = 0;
            this.lblDiaDiemDoHang.CanGrow = false;
            this.lblDiaDiemDoHang.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaDiemDoHang.Location = new System.Drawing.Point(4, 33);
            this.lblDiaDiemDoHang.Multiline = true;
            this.lblDiaDiemDoHang.Name = "lblDiaDiemDoHang";
            this.lblDiaDiemDoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaDiemDoHang.ParentStyleUsing.UseBorderWidth = false;
            this.lblDiaDiemDoHang.ParentStyleUsing.UseFont = false;
            this.lblDiaDiemDoHang.Size = new System.Drawing.Size(121, 34);
            this.lblDiaDiemDoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable7
            // 
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable7.Location = new System.Drawing.Point(25, 256);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.ParentStyleUsing.UseBorders = false;
            this.xrTable7.ParentStyleUsing.UseFont = false;
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable7.Size = new System.Drawing.Size(767, 94);
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell23});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Size = new System.Drawing.Size(767, 94);
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8,
            this.lblTenDoiTac,
            this.xrLabel31});
            this.xrTableCell24.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell24.ParentStyleUsing.UseBorders = false;
            this.xrTableCell24.Size = new System.Drawing.Size(392, 94);
            this.xrTableCell24.Text = "xrTableCell24";
            // 
            // xrTable8
            // 
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable8.Location = new System.Drawing.Point(130, 0);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.ParentStyleUsing.UseBorders = false;
            this.xrTable8.ParentStyleUsing.UseFont = false;
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable8.Size = new System.Drawing.Size(260, 25);
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36,
            this.xrTableCell37});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Size = new System.Drawing.Size(260, 25);
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell25.ParentStyleUsing.UseBorders = false;
            this.xrTableCell25.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell26.Location = new System.Drawing.Point(20, 0);
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell26.ParentStyleUsing.UseBorders = false;
            this.xrTableCell26.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell27.Location = new System.Drawing.Point(40, 0);
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell27.ParentStyleUsing.UseBorders = false;
            this.xrTableCell27.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell28.Location = new System.Drawing.Point(60, 0);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell28.ParentStyleUsing.UseBorders = false;
            this.xrTableCell28.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell29.Location = new System.Drawing.Point(80, 0);
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell29.ParentStyleUsing.UseBorders = false;
            this.xrTableCell29.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell29.Text = " ";
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell30.Location = new System.Drawing.Point(100, 0);
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell30.ParentStyleUsing.UseBorders = false;
            this.xrTableCell30.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell30.Text = " ";
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell31.Location = new System.Drawing.Point(120, 0);
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell31.ParentStyleUsing.UseBorders = false;
            this.xrTableCell31.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell31.Text = " ";
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell32.Location = new System.Drawing.Point(140, 0);
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell32.ParentStyleUsing.UseBorders = false;
            this.xrTableCell32.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell32.Text = " ";
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell33.Location = new System.Drawing.Point(160, 0);
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell33.ParentStyleUsing.UseBorders = false;
            this.xrTableCell33.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell33.Text = " ";
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Location = new System.Drawing.Point(180, 0);
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell34.ParentStyleUsing.UseBorders = false;
            this.xrTableCell34.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell34.Text = " ";
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell35.Location = new System.Drawing.Point(200, 0);
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell35.ParentStyleUsing.UseBorders = false;
            this.xrTableCell35.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell35.Text = " ";
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell36.Location = new System.Drawing.Point(220, 0);
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell36.ParentStyleUsing.UseBorders = false;
            this.xrTableCell36.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell36.Text = " ";
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell37.Location = new System.Drawing.Point(240, 0);
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell37.ParentStyleUsing.UseBorders = false;
            this.xrTableCell37.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell37.Text = " ";
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblTenDoiTac
            // 
            this.lblTenDoiTac.BorderColor = System.Drawing.Color.Black;
            this.lblTenDoiTac.BorderWidth = 0;
            this.lblTenDoiTac.CanGrow = false;
            this.lblTenDoiTac.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenDoiTac.Location = new System.Drawing.Point(8, 33);
            this.lblTenDoiTac.Multiline = true;
            this.lblTenDoiTac.Name = "lblTenDoiTac";
            this.lblTenDoiTac.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenDoiTac.ParentStyleUsing.UseBorderColor = false;
            this.lblTenDoiTac.ParentStyleUsing.UseBorderWidth = false;
            this.lblTenDoiTac.ParentStyleUsing.UseFont = false;
            this.lblTenDoiTac.Size = new System.Drawing.Size(375, 58);
            this.lblTenDoiTac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel31
            // 
            this.xrLabel31.BorderWidth = 0;
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.Location = new System.Drawing.Point(8, 0);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel31.ParentStyleUsing.UseFont = false;
            this.xrLabel31.Size = new System.Drawing.Size(102, 25);
            this.xrLabel31.Text = "2. Người xuất khẩu";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNgayHoaDon,
            this.lblSoHoaDon,
            this.xrLabel32});
            this.xrTableCell6.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.ParentStyleUsing.UseBorders = false;
            this.xrTableCell6.Size = new System.Drawing.Size(116, 94);
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayHoaDon
            // 
            this.lblNgayHoaDon.BorderWidth = 0;
            this.lblNgayHoaDon.CanGrow = false;
            this.lblNgayHoaDon.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHoaDon.Location = new System.Drawing.Point(8, 64);
            this.lblNgayHoaDon.Name = "lblNgayHoaDon";
            this.lblNgayHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHoaDon.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayHoaDon.ParentStyleUsing.UseFont = false;
            this.lblNgayHoaDon.Size = new System.Drawing.Size(102, 20);
            this.lblNgayHoaDon.Text = "Ngày";
            // 
            // lblSoHoaDon
            // 
            this.lblSoHoaDon.BorderWidth = 0;
            this.lblSoHoaDon.CanGrow = false;
            this.lblSoHoaDon.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHoaDon.Location = new System.Drawing.Point(8, 25);
            this.lblSoHoaDon.Multiline = true;
            this.lblSoHoaDon.Name = "lblSoHoaDon";
            this.lblSoHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoHoaDon.ParentStyleUsing.UseBorderWidth = false;
            this.lblSoHoaDon.ParentStyleUsing.UseFont = false;
            this.lblSoHoaDon.Size = new System.Drawing.Size(102, 40);
            this.lblSoHoaDon.Text = "Số : ";
            // 
            // xrLabel32
            // 
            this.xrLabel32.BorderWidth = 0;
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.Location = new System.Drawing.Point(0, 0);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel32.ParentStyleUsing.UseFont = false;
            this.xrLabel32.Size = new System.Drawing.Size(113, 25);
            this.xrLabel32.Text = "8. Hóa đơn thương mại";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNgayDenPTVT,
            this.lblSoPTVT,
            this.xrLabel33});
            this.xrTableCell7.Location = new System.Drawing.Point(508, 0);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell7.ParentStyleUsing.UseBorders = false;
            this.xrTableCell7.Size = new System.Drawing.Size(125, 94);
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayDenPTVT
            // 
            this.lblNgayDenPTVT.BorderWidth = 0;
            this.lblNgayDenPTVT.CanGrow = false;
            this.lblNgayDenPTVT.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDenPTVT.Location = new System.Drawing.Point(8, 64);
            this.lblNgayDenPTVT.Name = "lblNgayDenPTVT";
            this.lblNgayDenPTVT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDenPTVT.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayDenPTVT.ParentStyleUsing.UseFont = false;
            this.lblNgayDenPTVT.Size = new System.Drawing.Size(108, 20);
            this.lblNgayDenPTVT.Text = "Ngày đến:  ";
            // 
            // lblSoPTVT
            // 
            this.lblSoPTVT.BorderWidth = 0;
            this.lblSoPTVT.CanGrow = false;
            this.lblSoPTVT.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoPTVT.Location = new System.Drawing.Point(8, 25);
            this.lblSoPTVT.Name = "lblSoPTVT";
            this.lblSoPTVT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoPTVT.ParentStyleUsing.UseBorderWidth = false;
            this.lblSoPTVT.ParentStyleUsing.UseFont = false;
            this.lblSoPTVT.Size = new System.Drawing.Size(108, 40);
            this.lblSoPTVT.Text = "Tên, số hiệu :";
            // 
            // xrLabel33
            // 
            this.xrLabel33.BorderWidth = 0;
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel33.Location = new System.Drawing.Point(8, 0);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel33.ParentStyleUsing.UseFont = false;
            this.xrLabel33.Size = new System.Drawing.Size(108, 25);
            this.xrLabel33.Text = "9. Phương tiện vận tải";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNgayVanTaiDon,
            this.lblSoVanTaiDon,
            this.xrLabel34});
            this.xrTableCell23.Location = new System.Drawing.Point(633, 0);
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell23.ParentStyleUsing.UseBorders = false;
            this.xrTableCell23.Size = new System.Drawing.Size(134, 94);
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblNgayVanTaiDon
            // 
            this.lblNgayVanTaiDon.BorderWidth = 0;
            this.lblNgayVanTaiDon.CanGrow = false;
            this.lblNgayVanTaiDon.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayVanTaiDon.Location = new System.Drawing.Point(5, 64);
            this.lblNgayVanTaiDon.Name = "lblNgayVanTaiDon";
            this.lblNgayVanTaiDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayVanTaiDon.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayVanTaiDon.ParentStyleUsing.UseFont = false;
            this.lblNgayVanTaiDon.Size = new System.Drawing.Size(120, 20);
            this.lblNgayVanTaiDon.Text = "Ngày :";
            // 
            // lblSoVanTaiDon
            // 
            this.lblSoVanTaiDon.BorderWidth = 0;
            this.lblSoVanTaiDon.CanGrow = false;
            this.lblSoVanTaiDon.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoVanTaiDon.Location = new System.Drawing.Point(5, 25);
            this.lblSoVanTaiDon.Name = "lblSoVanTaiDon";
            this.lblSoVanTaiDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoVanTaiDon.ParentStyleUsing.UseBorderWidth = false;
            this.lblSoVanTaiDon.ParentStyleUsing.UseFont = false;
            this.lblSoVanTaiDon.Size = new System.Drawing.Size(120, 40);
            this.lblSoVanTaiDon.Text = "Số :";
            // 
            // xrLabel34
            // 
            this.xrLabel34.BorderWidth = 0;
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.Location = new System.Drawing.Point(5, 0);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel34.ParentStyleUsing.UseFont = false;
            this.xrLabel34.Size = new System.Drawing.Size(117, 25);
            this.xrLabel34.Text = "10. Vận tải đơn";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5.Location = new System.Drawing.Point(25, 162);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.ParentStyleUsing.UseBorders = false;
            this.xrTable5.ParentStyleUsing.UseFont = false;
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable5.Size = new System.Drawing.Size(767, 94);
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell4,
            this.xrTableCell11});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Size = new System.Drawing.Size(767, 94);
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblTenDoanhNghiep1,
            this.lblMaDoanhNghiep1,
            this.xrTable6,
            this.xrLabel11});
            this.xrTableCell8.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell8.ParentStyleUsing.UseBorders = false;
            this.xrTableCell8.Size = new System.Drawing.Size(392, 94);
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTenDoanhNghiep1
            // 
            this.lblTenDoanhNghiep1.BorderWidth = 0;
            this.lblTenDoanhNghiep1.CanGrow = false;
            this.lblTenDoanhNghiep1.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenDoanhNghiep1.Location = new System.Drawing.Point(8, 33);
            this.lblTenDoanhNghiep1.Multiline = true;
            this.lblTenDoanhNghiep1.Name = "lblTenDoanhNghiep1";
            this.lblTenDoanhNghiep1.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenDoanhNghiep1.ParentStyleUsing.UseBorderWidth = false;
            this.lblTenDoanhNghiep1.ParentStyleUsing.UseFont = false;
            this.lblTenDoanhNghiep1.Size = new System.Drawing.Size(375, 58);
            this.lblTenDoanhNghiep1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDoanhNghiep1
            // 
            this.lblMaDoanhNghiep1.BorderWidth = 0;
            this.lblMaDoanhNghiep1.CanGrow = false;
            this.lblMaDoanhNghiep1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDoanhNghiep1.Location = new System.Drawing.Point(132, 0);
            this.lblMaDoanhNghiep1.Name = "lblMaDoanhNghiep1";
            this.lblMaDoanhNghiep1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.lblMaDoanhNghiep1.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaDoanhNghiep1.ParentStyleUsing.UseFont = false;
            this.lblMaDoanhNghiep1.Size = new System.Drawing.Size(255, 25);
            this.lblMaDoanhNghiep1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable6
            // 
            this.xrTable6.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable6.Location = new System.Drawing.Point(130, 0);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.ParentStyleUsing.UseBorders = false;
            this.xrTable6.ParentStyleUsing.UseFont = false;
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable6.Size = new System.Drawing.Size(260, 25);
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell12,
            this.xrTableCell17,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell14,
            this.xrTableCell18,
            this.xrTableCell20,
            this.xrTableCell19,
            this.xrTableCell22,
            this.xrTableCell21,
            this.xrTableCell5,
            this.xrTableCell13});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Size = new System.Drawing.Size(260, 25);
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell10.ParentStyleUsing.UseBorders = false;
            this.xrTableCell10.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell12.Location = new System.Drawing.Point(20, 0);
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell12.ParentStyleUsing.UseBorders = false;
            this.xrTableCell12.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell17.Location = new System.Drawing.Point(40, 0);
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell17.ParentStyleUsing.UseBorders = false;
            this.xrTableCell17.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Location = new System.Drawing.Point(60, 0);
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell15.ParentStyleUsing.UseBorders = false;
            this.xrTableCell15.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.Location = new System.Drawing.Point(80, 0);
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell16.ParentStyleUsing.UseBorders = false;
            this.xrTableCell16.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell14.Location = new System.Drawing.Point(100, 0);
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell14.ParentStyleUsing.UseBorders = false;
            this.xrTableCell14.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell18.Location = new System.Drawing.Point(120, 0);
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell18.ParentStyleUsing.UseBorders = false;
            this.xrTableCell18.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell20.Location = new System.Drawing.Point(140, 0);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell20.ParentStyleUsing.UseBorders = false;
            this.xrTableCell20.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell19.Location = new System.Drawing.Point(160, 0);
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell19.ParentStyleUsing.UseBorders = false;
            this.xrTableCell19.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell22.Location = new System.Drawing.Point(180, 0);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell22.ParentStyleUsing.UseBorders = false;
            this.xrTableCell22.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell21.Location = new System.Drawing.Point(200, 0);
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell21.ParentStyleUsing.UseBorders = false;
            this.xrTableCell21.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Location = new System.Drawing.Point(220, 0);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.ParentStyleUsing.UseBorders = false;
            this.xrTableCell5.Size = new System.Drawing.Size(20, 25);
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Location = new System.Drawing.Point(240, 0);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell13.ParentStyleUsing.UseBorders = false;
            this.xrTableCell13.Size = new System.Drawing.Size(20, 25);
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel11
            // 
            this.xrLabel11.BorderWidth = 0;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.Location = new System.Drawing.Point(6, 0);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel11.ParentStyleUsing.UseFont = false;
            this.xrLabel11.Size = new System.Drawing.Size(102, 25);
            this.xrLabel11.Text = "1. Người nhập khẩu";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrKhac,
            this.xrTN,
            this.xrSXXK,
            this.xrTNK,
            this.xrGC,
            this.xrDT,
            this.xrKD,
            this.xrLabel19,
            this.xrLabel18,
            this.xrLabel17,
            this.xrLabel16,
            this.xrLabel15,
            this.xrLabel14,
            this.xrLabel13});
            this.xrTableCell9.Location = new System.Drawing.Point(392, 0);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell9.ParentStyleUsing.UseBorders = false;
            this.xrTableCell9.Size = new System.Drawing.Size(116, 94);
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrKhac
            // 
            this.xrKhac.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrKhac.CanGrow = false;
            this.xrKhac.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrKhac.Location = new System.Drawing.Point(42, 67);
            this.xrKhac.Name = "xrKhac";
            this.xrKhac.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrKhac.ParentStyleUsing.UseBorders = false;
            this.xrKhac.ParentStyleUsing.UseFont = false;
            this.xrKhac.Size = new System.Drawing.Size(13, 13);
            this.xrKhac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrKhac.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.xrKD_PreviewDoubleClick);
            // 
            // xrTN
            // 
            this.xrTN.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTN.CanGrow = false;
            this.xrTN.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTN.Location = new System.Drawing.Point(5, 67);
            this.xrTN.Name = "xrTN";
            this.xrTN.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTN.ParentStyleUsing.UseBorders = false;
            this.xrTN.ParentStyleUsing.UseFont = false;
            this.xrTN.Size = new System.Drawing.Size(13, 13);
            this.xrTN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTN.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.xrKD_PreviewDoubleClick);
            // 
            // xrSXXK
            // 
            this.xrSXXK.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrSXXK.CanGrow = false;
            this.xrSXXK.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrSXXK.Location = new System.Drawing.Point(5, 45);
            this.xrSXXK.Name = "xrSXXK";
            this.xrSXXK.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrSXXK.ParentStyleUsing.UseBorders = false;
            this.xrSXXK.ParentStyleUsing.UseFont = false;
            this.xrSXXK.Size = new System.Drawing.Size(13, 13);
            this.xrSXXK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrSXXK.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.xrKD_PreviewDoubleClick);
            // 
            // xrTNK
            // 
            this.xrTNK.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTNK.CanGrow = false;
            this.xrTNK.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTNK.Location = new System.Drawing.Point(67, 45);
            this.xrTNK.Name = "xrTNK";
            this.xrTNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTNK.ParentStyleUsing.UseBorders = false;
            this.xrTNK.ParentStyleUsing.UseFont = false;
            this.xrTNK.Size = new System.Drawing.Size(13, 13);
            this.xrTNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTNK.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.xrKD_PreviewDoubleClick);
            // 
            // xrGC
            // 
            this.xrGC.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrGC.CanGrow = false;
            this.xrGC.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrGC.Location = new System.Drawing.Point(82, 24);
            this.xrGC.Name = "xrGC";
            this.xrGC.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrGC.ParentStyleUsing.UseBorders = false;
            this.xrGC.ParentStyleUsing.UseFont = false;
            this.xrGC.Size = new System.Drawing.Size(13, 13);
            this.xrGC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrGC.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.xrKD_PreviewDoubleClick);
            // 
            // xrDT
            // 
            this.xrDT.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrDT.CanGrow = false;
            this.xrDT.Font = new System.Drawing.Font("Times New Roman", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDT.Location = new System.Drawing.Point(42, 24);
            this.xrDT.Name = "xrDT";
            this.xrDT.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrDT.ParentStyleUsing.UseBorders = false;
            this.xrDT.ParentStyleUsing.UseFont = false;
            this.xrDT.Size = new System.Drawing.Size(13, 13);
            this.xrDT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrDT.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.xrKD_PreviewDoubleClick);
            // 
            // xrKD
            // 
            this.xrKD.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrKD.CanGrow = false;
            this.xrKD.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrKD.Location = new System.Drawing.Point(5, 24);
            this.xrKD.Name = "xrKD";
            this.xrKD.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrKD.ParentStyleUsing.UseBorders = false;
            this.xrKD.ParentStyleUsing.UseBorderWidth = false;
            this.xrKD.ParentStyleUsing.UseFont = false;
            this.xrKD.Size = new System.Drawing.Size(13, 13);
            this.xrKD.Text = "X";
            this.xrKD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrKD.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.xrKD_PreviewDoubleClick);
            // 
            // xrLabel19
            // 
            this.xrLabel19.BorderWidth = 0;
            this.xrLabel19.CanGrow = false;
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.Location = new System.Drawing.Point(17, 67);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel19.ParentStyleUsing.UseFont = false;
            this.xrLabel19.Size = new System.Drawing.Size(25, 11);
            this.xrLabel19.Text = "TN";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.BorderWidth = 0;
            this.xrLabel18.CanGrow = false;
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.Location = new System.Drawing.Point(79, 46);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel18.ParentStyleUsing.UseFont = false;
            this.xrLabel18.Size = new System.Drawing.Size(28, 11);
            this.xrLabel18.Text = "NTK";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.BorderWidth = 0;
            this.xrLabel17.CanGrow = false;
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.Location = new System.Drawing.Point(17, 45);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel17.ParentStyleUsing.UseFont = false;
            this.xrLabel17.Size = new System.Drawing.Size(41, 11);
            this.xrLabel17.Text = "SXXK";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.BorderWidth = 0;
            this.xrLabel16.CanGrow = false;
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.Location = new System.Drawing.Point(96, 25);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel16.ParentStyleUsing.UseFont = false;
            this.xrLabel16.Size = new System.Drawing.Size(25, 11);
            this.xrLabel16.Text = "GC";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.BorderWidth = 0;
            this.xrLabel15.CanGrow = false;
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.Location = new System.Drawing.Point(55, 25);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel15.ParentStyleUsing.UseFont = false;
            this.xrLabel15.Size = new System.Drawing.Size(25, 11);
            this.xrLabel15.Text = "ĐT";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.BorderWidth = 0;
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.Location = new System.Drawing.Point(17, 25);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel14.ParentStyleUsing.UseFont = false;
            this.xrLabel14.Size = new System.Drawing.Size(25, 11);
            this.xrLabel14.Text = "KD";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.BorderWidth = 0;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.Location = new System.Drawing.Point(5, 0);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel13.ParentStyleUsing.UseFont = false;
            this.xrLabel13.Size = new System.Drawing.Size(102, 25);
            this.xrLabel13.Text = "5. Loại hình:";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblSoGP,
            this.lblNgayHHGP,
            this.xrLabel28,
            this.lblNgayGP});
            this.xrTableCell4.Location = new System.Drawing.Point(508, 0);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell4.ParentStyleUsing.UseBorders = false;
            this.xrTableCell4.Size = new System.Drawing.Size(125, 94);
            // 
            // lblSoGP
            // 
            this.lblSoGP.BorderWidth = 0;
            this.lblSoGP.CanGrow = false;
            this.lblSoGP.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoGP.Location = new System.Drawing.Point(8, 26);
            this.lblSoGP.Name = "lblSoGP";
            this.lblSoGP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoGP.ParentStyleUsing.UseBorderWidth = false;
            this.lblSoGP.ParentStyleUsing.UseFont = false;
            this.lblSoGP.Size = new System.Drawing.Size(107, 33);
            this.lblSoGP.Text = "Số  : ";
            this.lblSoGP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNgayHHGP
            // 
            this.lblNgayHHGP.BorderWidth = 0;
            this.lblNgayHHGP.CanGrow = false;
            this.lblNgayHHGP.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHHGP.Location = new System.Drawing.Point(8, 75);
            this.lblNgayHHGP.Name = "lblNgayHHGP";
            this.lblNgayHHGP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHHGP.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayHHGP.ParentStyleUsing.UseFont = false;
            this.lblNgayHHGP.Size = new System.Drawing.Size(116, 17);
            this.lblNgayHHGP.Text = "Ngày hết hạn: ";
            this.lblNgayHHGP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel28
            // 
            this.xrLabel28.BorderWidth = 0;
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.Location = new System.Drawing.Point(5, 0);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel28.ParentStyleUsing.UseFont = false;
            this.xrLabel28.Size = new System.Drawing.Size(113, 25);
            this.xrLabel28.Text = "6. Giấy phép(nếu có)";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNgayGP
            // 
            this.lblNgayGP.BorderWidth = 0;
            this.lblNgayGP.CanGrow = false;
            this.lblNgayGP.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayGP.Location = new System.Drawing.Point(8, 58);
            this.lblNgayGP.Name = "lblNgayGP";
            this.lblNgayGP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayGP.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayGP.ParentStyleUsing.UseFont = false;
            this.lblNgayGP.Size = new System.Drawing.Size(108, 17);
            this.lblNgayGP.Text = "Ngày: ";
            this.lblNgayGP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNgayHHHD,
            this.lblNgayHD,
            this.lblSoHD,
            this.xrLabel30});
            this.xrTableCell11.Location = new System.Drawing.Point(633, 0);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell11.ParentStyleUsing.UseBorders = false;
            this.xrTableCell11.Size = new System.Drawing.Size(134, 94);
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblNgayHHHD
            // 
            this.lblNgayHHHD.BorderWidth = 0;
            this.lblNgayHHHD.CanGrow = false;
            this.lblNgayHHHD.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHHHD.Location = new System.Drawing.Point(4, 75);
            this.lblNgayHHHD.Name = "lblNgayHHHD";
            this.lblNgayHHHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHHHD.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayHHHD.ParentStyleUsing.UseFont = false;
            this.lblNgayHHHD.Size = new System.Drawing.Size(121, 16);
            this.lblNgayHHHD.Text = "Ngày hết hạn:";
            this.lblNgayHHHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNgayHD
            // 
            this.lblNgayHD.BorderWidth = 0;
            this.lblNgayHD.CanGrow = false;
            this.lblNgayHD.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHD.Location = new System.Drawing.Point(4, 58);
            this.lblNgayHD.Name = "lblNgayHD";
            this.lblNgayHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHD.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayHD.ParentStyleUsing.UseFont = false;
            this.lblNgayHD.Size = new System.Drawing.Size(121, 15);
            this.lblNgayHD.Text = "Ngày:";
            this.lblNgayHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoHD
            // 
            this.lblSoHD.BorderWidth = 0;
            this.lblSoHD.CanGrow = false;
            this.lblSoHD.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHD.Location = new System.Drawing.Point(4, 26);
            this.lblSoHD.Name = "lblSoHD";
            this.lblSoHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoHD.ParentStyleUsing.UseBorderWidth = false;
            this.lblSoHD.ParentStyleUsing.UseFont = false;
            this.lblSoHD.Size = new System.Drawing.Size(121, 33);
            this.lblSoHD.Text = "Số  :";
            this.lblSoHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel30
            // 
            this.xrLabel30.BorderWidth = 0;
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.Location = new System.Drawing.Point(5, 0);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel30.ParentStyleUsing.UseFont = false;
            this.xrLabel30.Size = new System.Drawing.Size(113, 25);
            this.xrLabel30.Text = "7. Hợp đồng";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.Location = new System.Drawing.Point(25, 50);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.ParentStyleUsing.UseBorders = false;
            this.xrTable4.ParentStyleUsing.UseFont = false;
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable4.Size = new System.Drawing.Size(767, 84);
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.lblTrangthai});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Size = new System.Drawing.Size(767, 84);
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblChiCucHaiQuan,
            this.lblCucHaiQuan,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8});
            this.xrTableCell1.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.Size = new System.Drawing.Size(245, 84);
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblChiCucHaiQuan
            // 
            this.lblChiCucHaiQuan.BorderWidth = 0;
            this.lblChiCucHaiQuan.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChiCucHaiQuan.Location = new System.Drawing.Point(108, 58);
            this.lblChiCucHaiQuan.Multiline = true;
            this.lblChiCucHaiQuan.Name = "lblChiCucHaiQuan";
            this.lblChiCucHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHaiQuan.ParentStyleUsing.UseBorderWidth = false;
            this.lblChiCucHaiQuan.ParentStyleUsing.UseFont = false;
            this.lblChiCucHaiQuan.Size = new System.Drawing.Size(134, 16);
            this.lblChiCucHaiQuan.Text = "chi cuc";
            this.lblChiCucHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblCucHaiQuan
            // 
            this.lblCucHaiQuan.BorderWidth = 0;
            this.lblCucHaiQuan.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCucHaiQuan.Location = new System.Drawing.Point(92, 34);
            this.lblCucHaiQuan.Name = "lblCucHaiQuan";
            this.lblCucHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCucHaiQuan.ParentStyleUsing.UseBorderWidth = false;
            this.lblCucHaiQuan.ParentStyleUsing.UseFont = false;
            this.lblCucHaiQuan.Size = new System.Drawing.Size(150, 16);
            this.lblCucHaiQuan.Text = "cuc";
            this.lblCucHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.BorderWidth = 0;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.Location = new System.Drawing.Point(8, 58);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel10.ParentStyleUsing.UseFont = false;
            this.xrLabel10.Size = new System.Drawing.Size(100, 16);
            this.xrLabel10.Text = "Chi cục Hải quan :";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.BorderWidth = 0;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.Location = new System.Drawing.Point(9, 34);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel9.ParentStyleUsing.UseFont = false;
            this.xrLabel9.Size = new System.Drawing.Size(84, 16);
            this.xrLabel9.Text = "Cục Hải quan :";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.BorderWidth = 0;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.Location = new System.Drawing.Point(10, 10);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel8.ParentStyleUsing.UseFont = false;
            this.xrLabel8.Size = new System.Drawing.Size(150, 16);
            this.xrLabel8.Text = "TỔNG CỤC HẢI QUAN";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblSoPLTK,
            this.lblNgayDangKy,
            this.lblMaHaiQuan,
            this.lblSoToKhai,
            this.xrLabel3,
            this.lblSoPLTK1,
            this.lblNgayDangKy1,
            this.xrLabel7});
            this.xrTableCell2.Location = new System.Drawing.Point(245, 0);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell2.Size = new System.Drawing.Size(260, 84);
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoPLTK
            // 
            this.lblSoPLTK.BorderWidth = 0;
            this.lblSoPLTK.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoPLTK.Location = new System.Drawing.Point(158, 58);
            this.lblSoPLTK.Name = "lblSoPLTK";
            this.lblSoPLTK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoPLTK.ParentStyleUsing.UseBorderWidth = false;
            this.lblSoPLTK.ParentStyleUsing.UseFont = false;
            this.lblSoPLTK.Size = new System.Drawing.Size(66, 16);
            this.lblSoPLTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.BorderWidth = 0;
            this.lblNgayDangKy.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDangKy.Location = new System.Drawing.Point(105, 38);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayDangKy.ParentStyleUsing.UseFont = false;
            this.lblNgayDangKy.Size = new System.Drawing.Size(125, 16);
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblMaHaiQuan
            // 
            this.lblMaHaiQuan.BackColor = System.Drawing.Color.Ivory;
            this.lblMaHaiQuan.BorderWidth = 0;
            this.lblMaHaiQuan.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblMaHaiQuan.Location = new System.Drawing.Point(197, 15);
            this.lblMaHaiQuan.Name = "lblMaHaiQuan";
            this.lblMaHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.lblMaHaiQuan.ParentStyleUsing.UseBackColor = false;
            this.lblMaHaiQuan.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaHaiQuan.ParentStyleUsing.UseFont = false;
            this.lblMaHaiQuan.Size = new System.Drawing.Size(53, 16);
            this.lblMaHaiQuan.Tag = "Hải quan";
            this.lblMaHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblMaHaiQuan.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.BorderWidth = 0;
            this.lblSoToKhai.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoToKhai.Location = new System.Drawing.Point(92, 15);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhai.ParentStyleUsing.UseBorderWidth = false;
            this.lblSoToKhai.ParentStyleUsing.UseFont = false;
            this.lblSoToKhai.Size = new System.Drawing.Size(49, 16);
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.BorderWidth = 0;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.Location = new System.Drawing.Point(8, 15);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel3.ParentStyleUsing.UseFont = false;
            this.xrLabel3.Size = new System.Drawing.Size(75, 16);
            this.xrLabel3.Text = "Tờ khai số : ";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoPLTK1
            // 
            this.lblSoPLTK1.BorderWidth = 0;
            this.lblSoPLTK1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoPLTK1.Location = new System.Drawing.Point(9, 59);
            this.lblSoPLTK1.Name = "lblSoPLTK1";
            this.lblSoPLTK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoPLTK1.ParentStyleUsing.UseBorderWidth = false;
            this.lblSoPLTK1.ParentStyleUsing.UseFont = false;
            this.lblSoPLTK1.Size = new System.Drawing.Size(150, 16);
            this.lblSoPLTK1.Text = "Số lượng phụ lục tờ khai :";
            this.lblSoPLTK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblNgayDangKy1
            // 
            this.lblNgayDangKy1.BorderWidth = 0;
            this.lblNgayDangKy1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDangKy1.Location = new System.Drawing.Point(8, 38);
            this.lblNgayDangKy1.Name = "lblNgayDangKy1";
            this.lblNgayDangKy1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy1.ParentStyleUsing.UseBorderWidth = false;
            this.lblNgayDangKy1.ParentStyleUsing.UseFont = false;
            this.lblNgayDangKy1.Size = new System.Drawing.Size(92, 16);
            this.lblNgayDangKy1.Text = "Ngày đăng ký : ";
            this.lblNgayDangKy1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.BorderWidth = 0;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.Location = new System.Drawing.Point(83, 15);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel7.ParentStyleUsing.UseFont = false;
            this.xrLabel7.Size = new System.Drawing.Size(117, 17);
            this.xrLabel7.Text = ".................../NK/........./";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTrangthai
            // 
            this.lblTrangthai.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel6,
            this.xrLabel5});
            this.lblTrangthai.Location = new System.Drawing.Point(505, 0);
            this.lblTrangthai.Name = "lblTrangthai";
            this.lblTrangthai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTrangthai.Size = new System.Drawing.Size(262, 84);
            this.lblTrangthai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.BorderWidth = 0;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.Location = new System.Drawing.Point(95, 12);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel6.ParentStyleUsing.UseFont = false;
            this.xrLabel6.Size = new System.Drawing.Size(92, 17);
            this.xrLabel6.Text = "(Ký, ghi rõ họ tên)";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.BorderWidth = 0;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.Location = new System.Drawing.Point(9, 12);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel5.ParentStyleUsing.UseFont = false;
            this.xrLabel5.Size = new System.Drawing.Size(83, 17);
            this.xrLabel5.Text = "Cán bộ đăng ký";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblBanLuuHaiQuan
            // 
            this.lblBanLuuHaiQuan.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBanLuuHaiQuan.Location = new System.Drawing.Point(316, 33);
            this.lblBanLuuHaiQuan.Name = "lblBanLuuHaiQuan";
            this.lblBanLuuHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanLuuHaiQuan.ParentStyleUsing.UseFont = false;
            this.lblBanLuuHaiQuan.Size = new System.Drawing.Size(216, 17);
            this.lblBanLuuHaiQuan.Text = "Bản lưu người khai Hải quan";
            this.lblBanLuuHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.Location = new System.Drawing.Point(718, 33);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.ParentStyleUsing.UseFont = false;
            this.xrLabel2.Size = new System.Drawing.Size(74, 17);
            this.xrLabel2.Text = "HQ/2002-NK";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.Location = new System.Drawing.Point(261, 8);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.ParentStyleUsing.UseFont = false;
            this.xrLabel1.Size = new System.Drawing.Size(317, 25);
            this.xrLabel1.Text = "TỜ KHAI HÀNG HÓA NHẬP KHẨU ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.Location = new System.Drawing.Point(25, 8);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.ParentStyleUsing.UseFont = false;
            this.xrLabel23.Size = new System.Drawing.Size(175, 25);
            this.xrLabel23.Text = "HẢI QUAN VIỆT NAM";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoBanChinh6
            // 
            this.lblSoBanChinh6.Location = new System.Drawing.Point(208, 1069);
            this.lblSoBanChinh6.Name = "lblSoBanChinh6";
            this.lblSoBanChinh6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh6.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanSao6
            // 
            this.lblSoBanSao6.Location = new System.Drawing.Point(317, 1069);
            this.lblSoBanSao6.Name = "lblSoBanSao6";
            this.lblSoBanSao6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao6.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanSao6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanChinh4
            // 
            this.lblSoBanChinh4.Location = new System.Drawing.Point(208, 1024);
            this.lblSoBanChinh4.Name = "lblSoBanChinh4";
            this.lblSoBanChinh4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh4.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanChinh3
            // 
            this.lblSoBanChinh3.Location = new System.Drawing.Point(208, 1002);
            this.lblSoBanChinh3.Name = "lblSoBanChinh3";
            this.lblSoBanChinh3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh3.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanChinh2
            // 
            this.lblSoBanChinh2.Location = new System.Drawing.Point(208, 981);
            this.lblSoBanChinh2.Name = "lblSoBanChinh2";
            this.lblSoBanChinh2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh2.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanChinh1
            // 
            this.lblSoBanChinh1.Location = new System.Drawing.Point(208, 959);
            this.lblSoBanChinh1.Name = "lblSoBanChinh1";
            this.lblSoBanChinh1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh1.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanSao1
            // 
            this.lblSoBanSao1.Location = new System.Drawing.Point(317, 959);
            this.lblSoBanSao1.Name = "lblSoBanSao1";
            this.lblSoBanSao1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao1.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanSao1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanSao2
            // 
            this.lblSoBanSao2.Location = new System.Drawing.Point(317, 981);
            this.lblSoBanSao2.Name = "lblSoBanSao2";
            this.lblSoBanSao2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao2.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanSao2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanSao3
            // 
            this.lblSoBanSao3.Location = new System.Drawing.Point(317, 1002);
            this.lblSoBanSao3.Name = "lblSoBanSao3";
            this.lblSoBanSao3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao3.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanSao3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoBanSao4
            // 
            this.lblSoBanSao4.Location = new System.Drawing.Point(317, 1024);
            this.lblSoBanSao4.Name = "lblSoBanSao4";
            this.lblSoBanSao4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao4.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanSao4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoTiepNhan
            // 
            this.lblSoTiepNhan.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblSoTiepNhan.Location = new System.Drawing.Point(650, 8);
            this.lblSoTiepNhan.Name = "lblSoTiepNhan";
            this.lblSoTiepNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTiepNhan.ParentStyleUsing.UseFont = false;
            this.lblSoTiepNhan.Size = new System.Drawing.Size(142, 25);
            // 
            // xrTable2
            // 
            this.xrTable2.BorderColor = System.Drawing.Color.DarkGray;
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable2.Location = new System.Drawing.Point(25, 767);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.ParentStyleUsing.UseBorderColor = false;
            this.xrTable2.ParentStyleUsing.UseBorders = false;
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12});
            this.xrTable2.Size = new System.Drawing.Size(767, 90);
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell123,
            this.TriGiaTT1,
            this.ThueSuatXNK1,
            this.TienThueXNK1,
            this.TriGiaTTGTGT1,
            this.ThueSuatGTGT1,
            this.TienThueGTGT1,
            this.TyLeThuKhac1,
            this.TriGiaThuKhac1});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell123.ParentStyleUsing.UseBorders = false;
            this.xrTableCell123.Size = new System.Drawing.Size(25, 30);
            this.xrTableCell123.Text = "1";
            this.xrTableCell123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaTT1
            // 
            this.TriGiaTT1.CanGrow = false;
            this.TriGiaTT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaTT1.Location = new System.Drawing.Point(25, 0);
            this.TriGiaTT1.Name = "TriGiaTT1";
            this.TriGiaTT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT1.ParentStyleUsing.UseFont = false;
            this.TriGiaTT1.Size = new System.Drawing.Size(120, 30);
            this.TriGiaTT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK1
            // 
            this.ThueSuatXNK1.CanGrow = false;
            this.ThueSuatXNK1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.ThueSuatXNK1.Location = new System.Drawing.Point(145, 0);
            this.ThueSuatXNK1.Name = "ThueSuatXNK1";
            this.ThueSuatXNK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK1.ParentStyleUsing.UseFont = false;
            this.ThueSuatXNK1.Size = new System.Drawing.Size(43, 30);
            this.ThueSuatXNK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK1
            // 
            this.TienThueXNK1.CanGrow = false;
            this.TienThueXNK1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TienThueXNK1.Location = new System.Drawing.Point(188, 0);
            this.TienThueXNK1.Name = "TienThueXNK1";
            this.TienThueXNK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK1.ParentStyleUsing.UseFont = false;
            this.TienThueXNK1.Size = new System.Drawing.Size(120, 30);
            this.TienThueXNK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaTTGTGT1
            // 
            this.TriGiaTTGTGT1.CanGrow = false;
            this.TriGiaTTGTGT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaTTGTGT1.Location = new System.Drawing.Point(308, 0);
            this.TriGiaTTGTGT1.Name = "TriGiaTTGTGT1";
            this.TriGiaTTGTGT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT1.ParentStyleUsing.UseFont = false;
            this.TriGiaTTGTGT1.Size = new System.Drawing.Size(120, 30);
            this.TriGiaTTGTGT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatGTGT1
            // 
            this.ThueSuatGTGT1.CanGrow = false;
            this.ThueSuatGTGT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.ThueSuatGTGT1.Location = new System.Drawing.Point(428, 0);
            this.ThueSuatGTGT1.Name = "ThueSuatGTGT1";
            this.ThueSuatGTGT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT1.ParentStyleUsing.UseFont = false;
            this.ThueSuatGTGT1.Size = new System.Drawing.Size(42, 30);
            this.ThueSuatGTGT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueGTGT1
            // 
            this.TienThueGTGT1.CanGrow = false;
            this.TienThueGTGT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TienThueGTGT1.Location = new System.Drawing.Point(470, 0);
            this.TienThueGTGT1.Name = "TienThueGTGT1";
            this.TienThueGTGT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT1.ParentStyleUsing.UseFont = false;
            this.TienThueGTGT1.Size = new System.Drawing.Size(121, 30);
            this.TienThueGTGT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac1
            // 
            this.TyLeThuKhac1.CanGrow = false;
            this.TyLeThuKhac1.Location = new System.Drawing.Point(591, 0);
            this.TyLeThuKhac1.Name = "TyLeThuKhac1";
            this.TyLeThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac1.Size = new System.Drawing.Size(45, 30);
            this.TyLeThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac1
            // 
            this.TriGiaThuKhac1.CanGrow = false;
            this.TriGiaThuKhac1.Location = new System.Drawing.Point(636, 0);
            this.TriGiaThuKhac1.Name = "TriGiaThuKhac1";
            this.TriGiaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac1.Size = new System.Drawing.Size(131, 30);
            this.TriGiaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell124,
            this.TriGiaTT2,
            this.ThueSuatXNK2,
            this.TienThueXNK2,
            this.TriGiaTTGTGT2,
            this.ThueSuatGTGT2,
            this.TienThueGTGT2,
            this.TyLeThuKhac2,
            this.TriGiaThuKhac2});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell124.Size = new System.Drawing.Size(25, 30);
            this.xrTableCell124.Text = "2";
            this.xrTableCell124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaTT2
            // 
            this.TriGiaTT2.CanGrow = false;
            this.TriGiaTT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaTT2.Location = new System.Drawing.Point(25, 0);
            this.TriGiaTT2.Name = "TriGiaTT2";
            this.TriGiaTT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT2.ParentStyleUsing.UseFont = false;
            this.TriGiaTT2.Size = new System.Drawing.Size(120, 30);
            this.TriGiaTT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK2
            // 
            this.ThueSuatXNK2.CanGrow = false;
            this.ThueSuatXNK2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.ThueSuatXNK2.Location = new System.Drawing.Point(145, 0);
            this.ThueSuatXNK2.Name = "ThueSuatXNK2";
            this.ThueSuatXNK2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK2.ParentStyleUsing.UseFont = false;
            this.ThueSuatXNK2.Size = new System.Drawing.Size(43, 30);
            this.ThueSuatXNK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK2
            // 
            this.TienThueXNK2.CanGrow = false;
            this.TienThueXNK2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TienThueXNK2.Location = new System.Drawing.Point(188, 0);
            this.TienThueXNK2.Name = "TienThueXNK2";
            this.TienThueXNK2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK2.ParentStyleUsing.UseFont = false;
            this.TienThueXNK2.Size = new System.Drawing.Size(120, 30);
            this.TienThueXNK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaTTGTGT2
            // 
            this.TriGiaTTGTGT2.CanGrow = false;
            this.TriGiaTTGTGT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaTTGTGT2.Location = new System.Drawing.Point(308, 0);
            this.TriGiaTTGTGT2.Name = "TriGiaTTGTGT2";
            this.TriGiaTTGTGT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT2.ParentStyleUsing.UseFont = false;
            this.TriGiaTTGTGT2.Size = new System.Drawing.Size(120, 30);
            this.TriGiaTTGTGT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatGTGT2
            // 
            this.ThueSuatGTGT2.CanGrow = false;
            this.ThueSuatGTGT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.ThueSuatGTGT2.Location = new System.Drawing.Point(428, 0);
            this.ThueSuatGTGT2.Name = "ThueSuatGTGT2";
            this.ThueSuatGTGT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT2.ParentStyleUsing.UseFont = false;
            this.ThueSuatGTGT2.Size = new System.Drawing.Size(42, 30);
            this.ThueSuatGTGT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueGTGT2
            // 
            this.TienThueGTGT2.CanGrow = false;
            this.TienThueGTGT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TienThueGTGT2.Location = new System.Drawing.Point(470, 0);
            this.TienThueGTGT2.Name = "TienThueGTGT2";
            this.TienThueGTGT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT2.ParentStyleUsing.UseFont = false;
            this.TienThueGTGT2.Size = new System.Drawing.Size(121, 30);
            this.TienThueGTGT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac2
            // 
            this.TyLeThuKhac2.CanGrow = false;
            this.TyLeThuKhac2.Location = new System.Drawing.Point(591, 0);
            this.TyLeThuKhac2.Name = "TyLeThuKhac2";
            this.TyLeThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac2.Size = new System.Drawing.Size(45, 30);
            this.TyLeThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac2
            // 
            this.TriGiaThuKhac2.CanGrow = false;
            this.TriGiaThuKhac2.Location = new System.Drawing.Point(636, 0);
            this.TriGiaThuKhac2.Name = "TriGiaThuKhac2";
            this.TriGiaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac2.Size = new System.Drawing.Size(131, 30);
            this.TriGiaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell125,
            this.TriGiaTT3,
            this.ThueSuatXNK3,
            this.TienThueXNK3,
            this.TriGiaTTGTGT3,
            this.ThueSuatGTGT3,
            this.TienThueGTGT3,
            this.TyLeThuKhac3,
            this.TriGiaThuKhac3});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell125.Size = new System.Drawing.Size(25, 30);
            this.xrTableCell125.Text = "3";
            this.xrTableCell125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaTT3
            // 
            this.TriGiaTT3.CanGrow = false;
            this.TriGiaTT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaTT3.Location = new System.Drawing.Point(25, 0);
            this.TriGiaTT3.Name = "TriGiaTT3";
            this.TriGiaTT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT3.ParentStyleUsing.UseFont = false;
            this.TriGiaTT3.Size = new System.Drawing.Size(120, 30);
            this.TriGiaTT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK3
            // 
            this.ThueSuatXNK3.CanGrow = false;
            this.ThueSuatXNK3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.ThueSuatXNK3.Location = new System.Drawing.Point(145, 0);
            this.ThueSuatXNK3.Name = "ThueSuatXNK3";
            this.ThueSuatXNK3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK3.ParentStyleUsing.UseFont = false;
            this.ThueSuatXNK3.Size = new System.Drawing.Size(43, 30);
            this.ThueSuatXNK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK3
            // 
            this.TienThueXNK3.CanGrow = false;
            this.TienThueXNK3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TienThueXNK3.Location = new System.Drawing.Point(188, 0);
            this.TienThueXNK3.Name = "TienThueXNK3";
            this.TienThueXNK3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK3.ParentStyleUsing.UseFont = false;
            this.TienThueXNK3.Size = new System.Drawing.Size(120, 30);
            this.TienThueXNK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaTTGTGT3
            // 
            this.TriGiaTTGTGT3.CanGrow = false;
            this.TriGiaTTGTGT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaTTGTGT3.Location = new System.Drawing.Point(308, 0);
            this.TriGiaTTGTGT3.Name = "TriGiaTTGTGT3";
            this.TriGiaTTGTGT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTTGTGT3.ParentStyleUsing.UseFont = false;
            this.TriGiaTTGTGT3.Size = new System.Drawing.Size(120, 30);
            this.TriGiaTTGTGT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatGTGT3
            // 
            this.ThueSuatGTGT3.CanGrow = false;
            this.ThueSuatGTGT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.ThueSuatGTGT3.Location = new System.Drawing.Point(428, 0);
            this.ThueSuatGTGT3.Name = "ThueSuatGTGT3";
            this.ThueSuatGTGT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatGTGT3.ParentStyleUsing.UseFont = false;
            this.ThueSuatGTGT3.Size = new System.Drawing.Size(42, 30);
            this.ThueSuatGTGT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueGTGT3
            // 
            this.TienThueGTGT3.CanGrow = false;
            this.TienThueGTGT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TienThueGTGT3.Location = new System.Drawing.Point(470, 0);
            this.TienThueGTGT3.Name = "TienThueGTGT3";
            this.TienThueGTGT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueGTGT3.ParentStyleUsing.UseFont = false;
            this.TienThueGTGT3.Size = new System.Drawing.Size(121, 30);
            this.TienThueGTGT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac3
            // 
            this.TyLeThuKhac3.CanGrow = false;
            this.TyLeThuKhac3.Location = new System.Drawing.Point(591, 0);
            this.TyLeThuKhac3.Name = "TyLeThuKhac3";
            this.TyLeThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac3.Size = new System.Drawing.Size(45, 30);
            this.TyLeThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac3
            // 
            this.TriGiaThuKhac3.CanGrow = false;
            this.TriGiaThuKhac3.Location = new System.Drawing.Point(636, 0);
            this.TriGiaThuKhac3.Name = "TriGiaThuKhac3";
            this.TriGiaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac3.Size = new System.Drawing.Size(131, 30);
            this.TriGiaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTable43
            // 
            this.xrTable43.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable43.Location = new System.Drawing.Point(25, 1273);
            this.xrTable43.Name = "xrTable43";
            this.xrTable43.ParentStyleUsing.UseBorders = false;
            this.xrTable43.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow56});
            this.xrTable43.Size = new System.Drawing.Size(767, 250);
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell163});
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Size = new System.Drawing.Size(767, 250);
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell163.CanGrow = false;
            this.xrTableCell163.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell163.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 2, 0, 100F);
            this.xrTableCell163.ParentStyleUsing.UseBorders = false;
            this.xrTableCell163.ParentStyleUsing.UseFont = false;
            this.xrTableCell163.Size = new System.Drawing.Size(767, 250);
            this.xrTableCell163.Text = "Kết quả kiểm tra: ";
            // 
            // xrTable24
            // 
            this.xrTable24.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable24.Location = new System.Drawing.Point(25, 908);
            this.xrTable24.Name = "xrTable24";
            this.xrTable24.ParentStyleUsing.UseBorders = false;
            this.xrTable24.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28});
            this.xrTable24.Size = new System.Drawing.Size(767, 23);
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell129});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Size = new System.Drawing.Size(767, 23);
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell129.CanGrow = false;
            this.xrTableCell129.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.xrTableCell129.ParentStyleUsing.UseBorders = false;
            this.xrTableCell129.Size = new System.Drawing.Size(767, 23);
            this.xrTableCell129.Text = resources.GetString("xrTableCell129.Text");
            this.xrTableCell129.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.BorderColor = System.Drawing.Color.Black;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable1.Location = new System.Drawing.Point(25, 575);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.ParentStyleUsing.UseBackColor = false;
            this.xrTable1.ParentStyleUsing.UseBorderColor = false;
            this.xrTable1.ParentStyleUsing.UseBorders = false;
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3});
            this.xrTable1.Size = new System.Drawing.Size(767, 90);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.TenHang1,
            this.MaHS1,
            this.XuatXu1,
            this.Luong1,
            this.DVT1,
            this.DonGiaNT1,
            this.TriGiaNT1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell93.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell93.ParentStyleUsing.UseBorders = false;
            this.xrTableCell93.Size = new System.Drawing.Size(25, 30);
            this.xrTableCell93.Text = "1";
            this.xrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang1
            // 
            this.TenHang1.BackColor = System.Drawing.Color.Ivory;
            this.TenHang1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang1.CanGrow = false;
            this.TenHang1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TenHang1.Location = new System.Drawing.Point(25, 0);
            this.TenHang1.Name = "TenHang1";
            this.TenHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.TenHang1.ParentStyleUsing.UseBackColor = false;
            this.TenHang1.ParentStyleUsing.UseBorders = false;
            this.TenHang1.ParentStyleUsing.UseFont = false;
            this.TenHang1.Size = new System.Drawing.Size(283, 30);
            this.TenHang1.Tag = "Tên hàng 1";
            this.TenHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang1.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS1
            // 
            this.MaHS1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.MaHS1.Location = new System.Drawing.Point(308, 0);
            this.MaHS1.Name = "MaHS1";
            this.MaHS1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS1.ParentStyleUsing.UseBorders = false;
            this.MaHS1.ParentStyleUsing.UseFont = false;
            this.MaHS1.Size = new System.Drawing.Size(84, 30);
            this.MaHS1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // XuatXu1
            // 
            this.XuatXu1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.XuatXu1.Location = new System.Drawing.Point(392, 0);
            this.XuatXu1.Name = "XuatXu1";
            this.XuatXu1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu1.ParentStyleUsing.UseBorders = false;
            this.XuatXu1.ParentStyleUsing.UseFont = false;
            this.XuatXu1.Size = new System.Drawing.Size(66, 30);
            this.XuatXu1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong1
            // 
            this.Luong1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.Luong1.Location = new System.Drawing.Point(458, 0);
            this.Luong1.Name = "Luong1";
            this.Luong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong1.ParentStyleUsing.UseBorders = false;
            this.Luong1.ParentStyleUsing.UseFont = false;
            this.Luong1.Size = new System.Drawing.Size(67, 30);
            this.Luong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT1
            // 
            this.DVT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DVT1.Location = new System.Drawing.Point(525, 0);
            this.DVT1.Name = "DVT1";
            this.DVT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT1.ParentStyleUsing.UseBorders = false;
            this.DVT1.ParentStyleUsing.UseFont = false;
            this.DVT1.Size = new System.Drawing.Size(67, 30);
            this.DVT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT1
            // 
            this.DonGiaNT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DonGiaNT1.Location = new System.Drawing.Point(592, 0);
            this.DonGiaNT1.Name = "DonGiaNT1";
            this.DonGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT1.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT1.ParentStyleUsing.UseFont = false;
            this.DonGiaNT1.Size = new System.Drawing.Size(80, 30);
            this.DonGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT1
            // 
            this.TriGiaNT1.BackColor = System.Drawing.Color.Ivory;
            this.TriGiaNT1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT1.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaNT1.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT1.Name = "TriGiaNT1";
            this.TriGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT1.ParentStyleUsing.UseBackColor = false;
            this.TriGiaNT1.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT1.ParentStyleUsing.UseFont = false;
            this.TriGiaNT1.Size = new System.Drawing.Size(95, 30);
            this.TriGiaNT1.Tag = "Trị giá NT 1";
            this.TriGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT1.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell94,
            this.TenHang2,
            this.MaHS2,
            this.XuatXu2,
            this.Luong2,
            this.DVT2,
            this.DonGiaNT2,
            this.TriGiaNT2});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell94.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell94.ParentStyleUsing.UseBorders = false;
            this.xrTableCell94.Size = new System.Drawing.Size(25, 30);
            this.xrTableCell94.Text = "2";
            this.xrTableCell94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang2
            // 
            this.TenHang2.BackColor = System.Drawing.Color.Ivory;
            this.TenHang2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang2.CanGrow = false;
            this.TenHang2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TenHang2.Location = new System.Drawing.Point(25, 0);
            this.TenHang2.Name = "TenHang2";
            this.TenHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.TenHang2.ParentStyleUsing.UseBackColor = false;
            this.TenHang2.ParentStyleUsing.UseBorders = false;
            this.TenHang2.ParentStyleUsing.UseFont = false;
            this.TenHang2.Size = new System.Drawing.Size(283, 30);
            this.TenHang2.Tag = "Tên hàng 2";
            this.TenHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang2.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS2
            // 
            this.MaHS2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.MaHS2.Location = new System.Drawing.Point(308, 0);
            this.MaHS2.Name = "MaHS2";
            this.MaHS2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS2.ParentStyleUsing.UseBorders = false;
            this.MaHS2.ParentStyleUsing.UseFont = false;
            this.MaHS2.Size = new System.Drawing.Size(84, 30);
            this.MaHS2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // XuatXu2
            // 
            this.XuatXu2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.XuatXu2.Location = new System.Drawing.Point(392, 0);
            this.XuatXu2.Name = "XuatXu2";
            this.XuatXu2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu2.ParentStyleUsing.UseBorders = false;
            this.XuatXu2.ParentStyleUsing.UseFont = false;
            this.XuatXu2.Size = new System.Drawing.Size(66, 30);
            this.XuatXu2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong2
            // 
            this.Luong2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.Luong2.Location = new System.Drawing.Point(458, 0);
            this.Luong2.Name = "Luong2";
            this.Luong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong2.ParentStyleUsing.UseBorders = false;
            this.Luong2.ParentStyleUsing.UseFont = false;
            this.Luong2.Size = new System.Drawing.Size(67, 30);
            this.Luong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT2
            // 
            this.DVT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DVT2.Location = new System.Drawing.Point(525, 0);
            this.DVT2.Name = "DVT2";
            this.DVT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT2.ParentStyleUsing.UseBorders = false;
            this.DVT2.ParentStyleUsing.UseFont = false;
            this.DVT2.Size = new System.Drawing.Size(67, 30);
            this.DVT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT2
            // 
            this.DonGiaNT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DonGiaNT2.Location = new System.Drawing.Point(592, 0);
            this.DonGiaNT2.Name = "DonGiaNT2";
            this.DonGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT2.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT2.ParentStyleUsing.UseFont = false;
            this.DonGiaNT2.Size = new System.Drawing.Size(80, 30);
            this.DonGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT2
            // 
            this.TriGiaNT2.BackColor = System.Drawing.Color.Ivory;
            this.TriGiaNT2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT2.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaNT2.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT2.Name = "TriGiaNT2";
            this.TriGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT2.ParentStyleUsing.UseBackColor = false;
            this.TriGiaNT2.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT2.ParentStyleUsing.UseFont = false;
            this.TriGiaNT2.Size = new System.Drawing.Size(95, 30);
            this.TriGiaNT2.Tag = "Trị giá NT 2";
            this.TriGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT2.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell95,
            this.TenHang3,
            this.MaHS3,
            this.XuatXu3,
            this.Luong3,
            this.DVT3,
            this.DonGiaNT3,
            this.TriGiaNT3});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell95.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell95.ParentStyleUsing.UseBorders = false;
            this.xrTableCell95.Size = new System.Drawing.Size(25, 30);
            this.xrTableCell95.Text = "3";
            this.xrTableCell95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TenHang3
            // 
            this.TenHang3.BackColor = System.Drawing.Color.Ivory;
            this.TenHang3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TenHang3.CanGrow = false;
            this.TenHang3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TenHang3.Location = new System.Drawing.Point(25, 0);
            this.TenHang3.Name = "TenHang3";
            this.TenHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.TenHang3.ParentStyleUsing.UseBackColor = false;
            this.TenHang3.ParentStyleUsing.UseBorders = false;
            this.TenHang3.ParentStyleUsing.UseFont = false;
            this.TenHang3.Size = new System.Drawing.Size(283, 30);
            this.TenHang3.Tag = "Tên hàng 3";
            this.TenHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang3.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS3
            // 
            this.MaHS3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.MaHS3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.MaHS3.Location = new System.Drawing.Point(308, 0);
            this.MaHS3.Name = "MaHS3";
            this.MaHS3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS3.ParentStyleUsing.UseBorders = false;
            this.MaHS3.ParentStyleUsing.UseFont = false;
            this.MaHS3.Size = new System.Drawing.Size(84, 30);
            this.MaHS3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // XuatXu3
            // 
            this.XuatXu3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.XuatXu3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.XuatXu3.Location = new System.Drawing.Point(392, 0);
            this.XuatXu3.Name = "XuatXu3";
            this.XuatXu3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu3.ParentStyleUsing.UseBorders = false;
            this.XuatXu3.ParentStyleUsing.UseFont = false;
            this.XuatXu3.Size = new System.Drawing.Size(66, 30);
            this.XuatXu3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong3
            // 
            this.Luong3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.Luong3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.Luong3.Location = new System.Drawing.Point(458, 0);
            this.Luong3.Name = "Luong3";
            this.Luong3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong3.ParentStyleUsing.UseBorders = false;
            this.Luong3.ParentStyleUsing.UseFont = false;
            this.Luong3.Size = new System.Drawing.Size(67, 30);
            this.Luong3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT3
            // 
            this.DVT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DVT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DVT3.Location = new System.Drawing.Point(525, 0);
            this.DVT3.Name = "DVT3";
            this.DVT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT3.ParentStyleUsing.UseBorders = false;
            this.DVT3.ParentStyleUsing.UseFont = false;
            this.DVT3.Size = new System.Drawing.Size(67, 30);
            this.DVT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT3
            // 
            this.DonGiaNT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.DonGiaNT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.DonGiaNT3.Location = new System.Drawing.Point(592, 0);
            this.DonGiaNT3.Name = "DonGiaNT3";
            this.DonGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT3.ParentStyleUsing.UseBorders = false;
            this.DonGiaNT3.ParentStyleUsing.UseFont = false;
            this.DonGiaNT3.Size = new System.Drawing.Size(80, 30);
            this.DonGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT3
            // 
            this.TriGiaNT3.BackColor = System.Drawing.Color.Ivory;
            this.TriGiaNT3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.TriGiaNT3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.TriGiaNT3.Location = new System.Drawing.Point(672, 0);
            this.TriGiaNT3.Name = "TriGiaNT3";
            this.TriGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT3.ParentStyleUsing.UseBackColor = false;
            this.TriGiaNT3.ParentStyleUsing.UseBorders = false;
            this.TriGiaNT3.ParentStyleUsing.UseFont = false;
            this.TriGiaNT3.Size = new System.Drawing.Size(95, 30);
            this.TriGiaNT3.Tag = "Trị giá NT 3";
            this.TriGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.TriGiaNT3.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable3.Location = new System.Drawing.Point(25, 665);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.ParentStyleUsing.UseBorders = false;
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable3.Size = new System.Drawing.Size(767, 30);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTrongLuong,
            this.lblPhiBaoHiem,
            this.xrTableCell3,
            this.lblTongTriGiaNT});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Size = new System.Drawing.Size(767, 30);
            // 
            // lblTrongLuong
            // 
            this.lblTrongLuong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTrongLuong.CanGrow = false;
            this.lblTrongLuong.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTrongLuong.Location = new System.Drawing.Point(0, 0);
            this.lblTrongLuong.Name = "lblTrongLuong";
            this.lblTrongLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(8, 2, 0, 0, 100F);
            this.lblTrongLuong.ParentStyleUsing.UseBorders = false;
            this.lblTrongLuong.ParentStyleUsing.UseFont = false;
            this.lblTrongLuong.Size = new System.Drawing.Size(367, 30);
            this.lblTrongLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblPhiBaoHiem
            // 
            this.lblPhiBaoHiem.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblPhiBaoHiem.Location = new System.Drawing.Point(367, 0);
            this.lblPhiBaoHiem.Name = "lblPhiBaoHiem";
            this.lblPhiBaoHiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 2, 0, 0, 100F);
            this.lblPhiBaoHiem.ParentStyleUsing.UseFont = false;
            this.lblPhiBaoHiem.Size = new System.Drawing.Size(225, 30);
            this.lblPhiBaoHiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.CanGrow = false;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell3.Location = new System.Drawing.Point(592, 0);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 5, 0, 0, 100F);
            this.xrTableCell3.ParentStyleUsing.UseFont = false;
            this.xrTableCell3.Size = new System.Drawing.Size(79, 30);
            this.xrTableCell3.Text = "Cộng:     ";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblTongTriGiaNT
            // 
            this.lblTongTriGiaNT.BackColor = System.Drawing.Color.Ivory;
            this.lblTongTriGiaNT.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTongTriGiaNT.CanGrow = false;
            this.lblTongTriGiaNT.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblTongTriGiaNT.Location = new System.Drawing.Point(671, 0);
            this.lblTongTriGiaNT.Name = "lblTongTriGiaNT";
            this.lblTongTriGiaNT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaNT.ParentStyleUsing.UseBackColor = false;
            this.lblTongTriGiaNT.ParentStyleUsing.UseBorders = false;
            this.lblTongTriGiaNT.ParentStyleUsing.UseFont = false;
            this.lblTongTriGiaNT.Size = new System.Drawing.Size(96, 30);
            this.lblTongTriGiaNT.Tag = "Tổng trị giá NT";
            this.lblTongTriGiaNT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTongTriGiaNT.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // xrTable21
            // 
            this.xrTable21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTable21.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable21.Location = new System.Drawing.Point(25, 767);
            this.xrTable21.Name = "xrTable21";
            this.xrTable21.ParentStyleUsing.UseBorders = false;
            this.xrTable21.ParentStyleUsing.UseFont = false;
            this.xrTable21.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.xrTable21.Size = new System.Drawing.Size(767, 90);
            this.xrTable21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell126,
            this.xrTableCell115,
            this.xrTableCell116,
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell119,
            this.xrTableCell120,
            this.xrTableCell121,
            this.xrTableCell122});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Size = new System.Drawing.Size(767, 90);
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell126.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell126.ParentStyleUsing.UseBorders = false;
            this.xrTableCell126.Size = new System.Drawing.Size(25, 90);
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell115.Location = new System.Drawing.Point(25, 0);
            this.xrTableCell115.Multiline = true;
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell115.ParentStyleUsing.UseFont = false;
            this.xrTableCell115.Size = new System.Drawing.Size(120, 90);
            this.xrTableCell115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Location = new System.Drawing.Point(145, 0);
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell116.Size = new System.Drawing.Size(43, 90);
            this.xrTableCell116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Location = new System.Drawing.Point(188, 0);
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell117.Size = new System.Drawing.Size(120, 90);
            this.xrTableCell117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Location = new System.Drawing.Point(308, 0);
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell118.Size = new System.Drawing.Size(121, 90);
            this.xrTableCell118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Location = new System.Drawing.Point(429, 0);
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell119.Size = new System.Drawing.Size(42, 90);
            this.xrTableCell119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell120.Location = new System.Drawing.Point(471, 0);
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell120.ParentStyleUsing.UseFont = false;
            this.xrTableCell120.Size = new System.Drawing.Size(121, 90);
            this.xrTableCell120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell121.Location = new System.Drawing.Point(592, 0);
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell121.ParentStyleUsing.UseFont = false;
            this.xrTableCell121.Size = new System.Drawing.Size(45, 90);
            this.xrTableCell121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Location = new System.Drawing.Point(637, 0);
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell122.Size = new System.Drawing.Size(130, 90);
            this.xrTableCell122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoHopDong
            // 
            this.lblSoHopDong.CanGrow = false;
            this.lblSoHopDong.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoHopDong.Location = new System.Drawing.Point(186, 18);
            this.lblSoHopDong.Multiline = true;
            this.lblSoHopDong.Name = "lblSoHopDong";
            this.lblSoHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblSoHopDong.ParentStyleUsing.UseFont = false;
            this.lblSoHopDong.Size = new System.Drawing.Size(100, 33);
            this.lblSoHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayHHHopDong
            // 
            this.lblNgayHHHopDong.Location = new System.Drawing.Point(186, 58);
            this.lblNgayHHHopDong.Name = "lblNgayHHHopDong";
            this.lblNgayHHHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayHHHopDong.Size = new System.Drawing.Size(66, 17);
            this.lblNgayHHHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayHopDong
            // 
            this.lblNgayHopDong.Location = new System.Drawing.Point(183, 42);
            this.lblNgayHopDong.Name = "lblNgayHopDong";
            this.lblNgayHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayHopDong.Size = new System.Drawing.Size(83, 16);
            // 
            // lblSoGiayPhep
            // 
            this.lblSoGiayPhep.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoGiayPhep.Location = new System.Drawing.Point(3, 33);
            this.lblSoGiayPhep.Name = "lblSoGiayPhep";
            this.lblSoGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblSoGiayPhep.ParentStyleUsing.UseFont = false;
            this.lblSoGiayPhep.Size = new System.Drawing.Size(91, 33);
            // 
            // lblNgayGiayPhep
            // 
            this.lblNgayGiayPhep.Location = new System.Drawing.Point(95, 33);
            this.lblNgayGiayPhep.Name = "lblNgayGiayPhep";
            this.lblNgayGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayGiayPhep.Size = new System.Drawing.Size(91, 17);
            // 
            // lblNgayHHGiayPhep
            // 
            this.lblNgayHHGiayPhep.Location = new System.Drawing.Point(103, 58);
            this.lblNgayHHGiayPhep.Name = "lblNgayHHGiayPhep";
            this.lblNgayHHGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lblNgayHHGiayPhep.Size = new System.Drawing.Size(75, 17);
            this.lblNgayHHGiayPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTenDoanhNghiep
            // 
            this.lblTenDoanhNghiep.BorderWidth = 0;
            this.lblTenDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenDoanhNghiep.Location = new System.Drawing.Point(17, 30);
            this.lblTenDoanhNghiep.Multiline = true;
            this.lblTenDoanhNghiep.Name = "lblTenDoanhNghiep";
            this.lblTenDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenDoanhNghiep.ParentStyleUsing.UseBorderWidth = false;
            this.lblTenDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblTenDoanhNghiep.Size = new System.Drawing.Size(353, 58);
            this.lblTenDoanhNghiep.Text = "CÔNG TY CỔ PHẦN DỆT MAY 19/3\r\n60 MẸ NHU THANH KHÊ ĐÀ NẴNG\r\nMID CODE: VNMARTEX478D" +
                "AN";
            this.lblTenDoanhNghiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblMaDoanhNghiep
            // 
            this.lblMaDoanhNghiep.BorderWidth = 0;
            this.lblMaDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaDoanhNghiep.Location = new System.Drawing.Point(111, 2);
            this.lblMaDoanhNghiep.Name = "lblMaDoanhNghiep";
            this.lblMaDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblMaDoanhNghiep.ParentStyleUsing.UseBorderWidth = false;
            this.lblMaDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblMaDoanhNghiep.Size = new System.Drawing.Size(260, 24);
            // 
            // xrLabel12
            // 
            this.xrLabel12.BorderWidth = 0;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.Location = new System.Drawing.Point(8, 0);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel12.ParentStyleUsing.UseBorderWidth = false;
            this.xrLabel12.ParentStyleUsing.UseFont = false;
            this.xrLabel12.Size = new System.Drawing.Size(102, 17);
            this.xrLabel12.Text = "5. Loại hình: ";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // ToKhaiNhapA4
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.Margins = new System.Drawing.Printing.Margins(5, 15, 19, 24);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            ((System.ComponentModel.ISupportInitialize)(this.xrTable58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTablesaaa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRLabel lblCucHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy1;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel lblMaDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblSoPLTK1;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiUyThac;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoiTac;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblTenDaiLyTTHQ;
        private DevExpress.XtraReports.UI.XRLabel lblSoPTVT;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHoaDon;
        private DevExpress.XtraReports.UI.XRLabel lblSoHoaDon;
        private DevExpress.XtraReports.UI.XRLabel lblSoHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayVanTaiDon;
        private DevExpress.XtraReports.UI.XRLabel lblSoGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblSoVanTaiDon;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDenPTVT;
        private DevExpress.XtraReports.UI.XRLabel lblDiaDiemXepHang;
        private DevExpress.XtraReports.UI.XRLabel lblMaNuoc;
        private DevExpress.XtraReports.UI.XRLabel lblTenNuoc;
        private DevExpress.XtraReports.UI.XRLabel lblDiaDiemDoHang;
        private DevExpress.XtraReports.UI.XRLabel lblDKGH;
        private DevExpress.XtraReports.UI.XRLabel lblPTTT;
        private DevExpress.XtraReports.UI.XRLabel lblTyGiaTT;
        private DevExpress.XtraReports.UI.XRLabel lblMaDaiLyTTHQ;
        private DevExpress.XtraReports.UI.XRLabel lblMaNguoiUyThac;
        private DevExpress.XtraReports.UI.XRLabel lblMaDiaDiemDoHang;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell TenHang1;
        private DevExpress.XtraReports.UI.XRTableCell MaHS1;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu1;
        private DevExpress.XtraReports.UI.XRTableCell Luong1;
        private DevExpress.XtraReports.UI.XRTableCell DVT1;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell TenHang2;
        private DevExpress.XtraReports.UI.XRTableCell MaHS2;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu2;
        private DevExpress.XtraReports.UI.XRTableCell Luong2;
        private DevExpress.XtraReports.UI.XRTableCell DVT2;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell TenHang3;
        private DevExpress.XtraReports.UI.XRTableCell MaHS3;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu3;
        private DevExpress.XtraReports.UI.XRTableCell Luong3;
        private DevExpress.XtraReports.UI.XRTableCell DVT3;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT3;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT1;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK1;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT2;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK2;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT3;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK3;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK3;
        private DevExpress.XtraReports.UI.XRLabel lblSoTiepNhan;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh6;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao6;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao5;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh5;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh4;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh3;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh2;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh1;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao1;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao2;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao3;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao4;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTriGiaNT;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT1;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT1;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT1;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT2;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT2;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT2;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTTGTGT3;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatGTGT3;
        private DevExpress.XtraReports.UI.XRTableCell TienThueGTGT3;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblBanLuuHaiQuan;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell lblTrangthai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHGP;
        private DevExpress.XtraReports.UI.XRLabel lblNgayGP;
        private DevExpress.XtraReports.UI.XRLabel lblSoGP;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHHD;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHD;
        private DevExpress.XtraReports.UI.XRLabel lblSoHD;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRLabel lblNgoaiTe;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTable xrTable19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRTable xrTable20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTable xrTable22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell lblTongThueXNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTienThueGTGT;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTriGiaThuKhac;
        private DevExpress.XtraReports.UI.XRTable xrTable23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTable xrTablesaaa;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell lblTongThueXNKSo;
        private DevExpress.XtraReports.UI.XRTable xrTable24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTable xrTable25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell lblTongThueXNKChu;
        private DevExpress.XtraReports.UI.XRTable xrTable26;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTable xrTable28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTable xrTable27;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTable xrTable32;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell lblTenChungTu6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTable xrTable31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell lblTenChungTu5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTable xrTable30;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTable xrTable29;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTable xrTable33;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTable xrTable34;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTable xrTable35;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTable xrTable36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRLabel xrKD;
        private DevExpress.XtraReports.UI.XRLabel xrGC;
        private DevExpress.XtraReports.UI.XRLabel xrDT;
        private DevExpress.XtraReports.UI.XRLabel xrTNK;
        private DevExpress.XtraReports.UI.XRLabel xrKhac;
        private DevExpress.XtraReports.UI.XRLabel xrTN;
        private DevExpress.XtraReports.UI.XRLabel xrSXXK;
        private DevExpress.XtraReports.UI.XRLabel lblMaDoanhNghiep1;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoanhNghiep1;
        private DevExpress.XtraReports.UI.XRTableCell lblPhiBaoHiem;
        private DevExpress.XtraReports.UI.XRTable xrTable38;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTable xrTable37;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTable xrTable44;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTable xrTable43;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTable xrTable42;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTable xrTable41;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRTable xrTable40;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRTable xrTable39;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTable xrTable46;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTable xrTable45;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTable xrTable50;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRTable xrTable49;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTable xrTable48;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTable xrTable47;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTable xrTable55;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTable xrTable52;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTable xrTable56;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTable xrTable54;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTable xrTable53;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTable xrTable51;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTable xrTable58;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTable xrTable57;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRLabel lblMienThue2;
        private DevExpress.XtraReports.UI.XRTable xrTable21;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRLabel lblMaHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblSoPLTK;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lblMienThue1;

    }
}
