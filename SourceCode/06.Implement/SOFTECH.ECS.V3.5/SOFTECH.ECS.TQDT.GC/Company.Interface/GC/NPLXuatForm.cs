using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Company.Interface.GC
{
    public partial class NPLXuatForm : BaseForm
    {
        public DataSet ds = new DataSet();
        public NPLXuatForm()
        {
            InitializeComponent();
        }

        private void NPLXuatForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = ds.Tables[0];
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["TenDVT"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void menuExportExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.FileName = "Danh sách Nguyên phụ liệu xuất_" + DateTime.Now.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
            sf.Filter = "Excel File | *.xls ";

            if (sf.ShowDialog(this) == DialogResult.OK)
            {
                Janus.Windows.GridEX.Export.GridEXExporter grdExport = new Janus.Windows.GridEX.Export.GridEXExporter();
                grdExport.GridEX = dgList;

                try
                {
                    System.IO.Stream str = sf.OpenFile();
                    grdExport.Export(str);
                    str.Close();
                    if (ShowMessage("Bạn có muốn mở File này không", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sf.FileName);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi trong quá trình mở File" + ex.Message, false);
                }
            }
            else
            {
                ShowMessage("Xuất Excel không thành công", false);
            }
        }
    }
}