﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class Consignment
    {
        /// <summary>
        /// .....
        /// </summary>
        [XmlElement("container")]
        public string Container { get; set; }
        /// <summary>
        /// PTVT
        /// </summary>
        [XmlElement("ArrivalTransportMeans")]
        public ArrivalTransportMeans ArrivalTransportMeans { get; set; }
        /// <summary>
        /// Hang VT
        /// </summary>
        [XmlElement("Carrier")]
        public NameBase Carrier { get; set; }
        /// <summary>
        /// Người giao
        /// </summary>
        [XmlElement("Consignor")]
        public NameBase Consignor { get; set; }
        /// <summary>
        /// người nhận
        /// </summary>
        [XmlElement("Consignee")]
        public NameBase Consignee { get; set; }
        /// <summary>
        /// Người được thông báo
        /// </summary>
        [XmlElement("NotifyParty")]
        public NameBase NotifyParty { get; set; }
        /// <summary>
        /// Cảng
        /// </summary>
        [XmlElement("LoadingLocation")]
        public LoadingLocation LoadingLocation { get; set; }
        /// <summary>
        /// Cảng dỡ hàng
        /// </summary>
        [XmlElement("UnloadingLocation")]
        public UnloadingLocation UnloadingLocation { get; set; }
        /// <summary>
        /// Địa điểm giao hàng
        /// </summary>
        [XmlElement("DeliveryDestination")]
        public DeliveryDestination DeliveryDestination { get; set; }
        /// <summary>
        /// Số kiện 
        /// </summary>
        [XmlElement("ConsignmentItemPackaging")]
        public Packaging ConsignmentItemPackaging { get; set; }


        /// <summary>
        /// 
        /// </summary>
        [XmlElement("TransportEquipment")]
        public List<TransportEquipment> TransportEquipments { get; set; }
        /// <summary>
        /// Chi tiết hàng đóng gói
        /// </summary>
        [XmlElement("ConsignmentItem")]
        public ConsignmentItem ConsignmentItem { get; set; }

    }
}
