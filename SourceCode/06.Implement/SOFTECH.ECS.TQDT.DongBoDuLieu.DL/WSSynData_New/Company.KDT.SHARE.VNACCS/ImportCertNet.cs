﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;

namespace Company.KDT.SHARE.VNACCS
{
    public class ImportCertNet
    {
        public static string VNPT_CA_CER = "vnptca.cer";
        public static string MIC_NATIONAL_ROOT_CA_CRT = "MICNationalRootCA.crt";
        public static string TEST_VNACCS_VCIS_CA_CER = "Test_VNACCS_VCIS_CA.cer";

        public static string BTC_CER = "BTC.crt";
        public static string EDICONN_CER = "EDICONN.cer";
        public static string INTRCTV_CER = "INTRCTV.cer";
        public static string ROOTCA_CER = "ROOTCA.crt";

        public static void InstallCertificate(string cerFileName, StoreName storeType)
        {
            try
            {
                X509Certificate2 certificate = new X509Certificate2(cerFileName);
                X509Store store = new X509Store(storeType, StoreLocation.LocalMachine);

                store.Open(OpenFlags.ReadWrite);
                store.Add(certificate);
                store.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        public static bool InstallCA()
        {
            try
            {
                bool isInstallCA = false;
                isInstallCA = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsInstallCA") == "True" ? true : false;

                if (!isInstallCA)
                {
                    //Trusted Root Certification Authorities
                    ImportCertNet.InstallCertificate(AppDomain.CurrentDomain.BaseDirectory + "CertificateAuthority\\" + TEST_VNACCS_VCIS_CA_CER, System.Security.Cryptography.X509Certificates.StoreName.Root);
                    ImportCertNet.InstallCertificate(AppDomain.CurrentDomain.BaseDirectory + "CertificateAuthority\\" + MIC_NATIONAL_ROOT_CA_CRT, System.Security.Cryptography.X509Certificates.StoreName.Root);

                    // Trusted Root Certification Authorities Update 19/10/2018
                    ImportCertNet.InstallCertificate(AppDomain.CurrentDomain.BaseDirectory + "CertificateAuthority\\" + EDICONN_CER, System.Security.Cryptography.X509Certificates.StoreName.Root);
                    ImportCertNet.InstallCertificate(AppDomain.CurrentDomain.BaseDirectory + "CertificateAuthority\\" + ROOTCA_CER, System.Security.Cryptography.X509Certificates.StoreName.Root);

                    ImportCertNet.InstallCertificate(AppDomain.CurrentDomain.BaseDirectory + "CertificateAuthority\\" + BTC_CER, System.Security.Cryptography.X509Certificates.StoreName.Root);
                    ImportCertNet.InstallCertificate(AppDomain.CurrentDomain.BaseDirectory + "CertificateAuthority\\" + INTRCTV_CER, System.Security.Cryptography.X509Certificates.StoreName.Root);

                    //Intermediate Certification Authorities
                    ImportCertNet.InstallCertificate(AppDomain.CurrentDomain.BaseDirectory + "CertificateAuthority\\" + VNPT_CA_CER, System.Security.Cryptography.X509Certificates.StoreName.CertificateAuthority);
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }
    }
}
