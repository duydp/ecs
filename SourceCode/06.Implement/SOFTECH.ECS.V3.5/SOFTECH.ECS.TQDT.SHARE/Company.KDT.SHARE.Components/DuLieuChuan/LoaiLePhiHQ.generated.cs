using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
	public partial class LoaiLePhiHQ
	{
		#region Properties.
		
		public string MaLePhi { set; get; }
		public string TenLePhi { set; get; }
		public string MoTa { set; get; }
		public string TrangThai { set; get; }
		public DateTime DateCreated { set; get; }
		public DateTime DateModified { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static LoaiLePhiHQ Load(string maLePhi)
		{
			const string spName = "[dbo].[p_HaiQuan_LoaiLePhiHQ_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaLePhi", SqlDbType.VarChar, maLePhi);
			LoaiLePhiHQ entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new LoaiLePhiHQ();
				if (!reader.IsDBNull(reader.GetOrdinal("MaLePhi"))) entity.MaLePhi = reader.GetString(reader.GetOrdinal("MaLePhi"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenLePhi"))) entity.TenLePhi = reader.GetString(reader.GetOrdinal("TenLePhi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MoTa"))) entity.MoTa = reader.GetString(reader.GetOrdinal("MoTa"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetString(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateCreated"))) entity.DateCreated = reader.GetDateTime(reader.GetOrdinal("DateCreated"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateModified"))) entity.DateModified = reader.GetDateTime(reader.GetOrdinal("DateModified"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<LoaiLePhiHQ> SelectCollectionAll()
		{
			List<LoaiLePhiHQ> collection = new List<LoaiLePhiHQ>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				LoaiLePhiHQ entity = new LoaiLePhiHQ();
				
				if (!reader.IsDBNull(reader.GetOrdinal("MaLePhi"))) entity.MaLePhi = reader.GetString(reader.GetOrdinal("MaLePhi"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenLePhi"))) entity.TenLePhi = reader.GetString(reader.GetOrdinal("TenLePhi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MoTa"))) entity.MoTa = reader.GetString(reader.GetOrdinal("MoTa"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetString(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateCreated"))) entity.DateCreated = reader.GetDateTime(reader.GetOrdinal("DateCreated"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateModified"))) entity.DateModified = reader.GetDateTime(reader.GetOrdinal("DateModified"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<LoaiLePhiHQ> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<LoaiLePhiHQ> collection = new List<LoaiLePhiHQ>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				LoaiLePhiHQ entity = new LoaiLePhiHQ();
				
				if (!reader.IsDBNull(reader.GetOrdinal("MaLePhi"))) entity.MaLePhi = reader.GetString(reader.GetOrdinal("MaLePhi"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenLePhi"))) entity.TenLePhi = reader.GetString(reader.GetOrdinal("TenLePhi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MoTa"))) entity.MoTa = reader.GetString(reader.GetOrdinal("MoTa"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetString(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateCreated"))) entity.DateCreated = reader.GetDateTime(reader.GetOrdinal("DateCreated"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateModified"))) entity.DateModified = reader.GetDateTime(reader.GetOrdinal("DateModified"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_HaiQuan_LoaiLePhiHQ_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_HaiQuan_LoaiLePhiHQ_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_HaiQuan_LoaiLePhiHQ_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_HaiQuan_LoaiLePhiHQ_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertLoaiLePhiHQ(string tenLePhi, string moTa, string trangThai, DateTime dateCreated, DateTime dateModified)
		{
			LoaiLePhiHQ entity = new LoaiLePhiHQ();	
			entity.TenLePhi = tenLePhi;
			entity.MoTa = moTa;
			entity.TrangThai = trangThai;
			entity.DateCreated = dateCreated;
			entity.DateModified = dateModified;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_HaiQuan_LoaiLePhiHQ_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaLePhi", SqlDbType.VarChar, MaLePhi);
			db.AddInParameter(dbCommand, "@TenLePhi", SqlDbType.NVarChar, TenLePhi);
			db.AddInParameter(dbCommand, "@MoTa", SqlDbType.NVarChar, MoTa);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.VarChar, TrangThai);
			db.AddInParameter(dbCommand, "@DateCreated", SqlDbType.DateTime, DateCreated.Year <= 1753 ? DBNull.Value : (object) DateCreated);
			db.AddInParameter(dbCommand, "@DateModified", SqlDbType.DateTime, DateModified.Year <= 1753 ? DBNull.Value : (object) DateModified);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<LoaiLePhiHQ> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LoaiLePhiHQ item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateLoaiLePhiHQ(string maLePhi, string tenLePhi, string moTa, string trangThai, DateTime dateCreated, DateTime dateModified)
		{
			LoaiLePhiHQ entity = new LoaiLePhiHQ();			
			entity.MaLePhi = maLePhi;
			entity.TenLePhi = tenLePhi;
			entity.MoTa = moTa;
			entity.TrangThai = trangThai;
			entity.DateCreated = dateCreated;
			entity.DateModified = dateModified;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_HaiQuan_LoaiLePhiHQ_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaLePhi", SqlDbType.VarChar, MaLePhi);
			db.AddInParameter(dbCommand, "@TenLePhi", SqlDbType.NVarChar, TenLePhi);
			db.AddInParameter(dbCommand, "@MoTa", SqlDbType.NVarChar, MoTa);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.VarChar, TrangThai);
			db.AddInParameter(dbCommand, "@DateCreated", SqlDbType.DateTime, DateCreated.Year == 1753 ? DBNull.Value : (object) DateCreated);
			db.AddInParameter(dbCommand, "@DateModified", SqlDbType.DateTime, DateModified.Year == 1753 ? DBNull.Value : (object) DateModified);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<LoaiLePhiHQ> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LoaiLePhiHQ item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateLoaiLePhiHQ(string maLePhi, string tenLePhi, string moTa, string trangThai, DateTime dateCreated, DateTime dateModified)
		{
			LoaiLePhiHQ entity = new LoaiLePhiHQ();			
			entity.MaLePhi = maLePhi;
			entity.TenLePhi = tenLePhi;
			entity.MoTa = moTa;
			entity.TrangThai = trangThai;
			entity.DateCreated = dateCreated;
			entity.DateModified = dateModified;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_HaiQuan_LoaiLePhiHQ_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaLePhi", SqlDbType.VarChar, MaLePhi);
			db.AddInParameter(dbCommand, "@TenLePhi", SqlDbType.NVarChar, TenLePhi);
			db.AddInParameter(dbCommand, "@MoTa", SqlDbType.NVarChar, MoTa);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.VarChar, TrangThai);
			db.AddInParameter(dbCommand, "@DateCreated", SqlDbType.DateTime, DateCreated.Year == 1753 ? DBNull.Value : (object) DateCreated);
			db.AddInParameter(dbCommand, "@DateModified", SqlDbType.DateTime, DateModified.Year == 1753 ? DBNull.Value : (object) DateModified);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<LoaiLePhiHQ> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LoaiLePhiHQ item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteLoaiLePhiHQ(string maLePhi)
		{
			LoaiLePhiHQ entity = new LoaiLePhiHQ();
			entity.MaLePhi = maLePhi;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_HaiQuan_LoaiLePhiHQ_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaLePhi", SqlDbType.VarChar, MaLePhi);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_HaiQuan_LoaiLePhiHQ_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<LoaiLePhiHQ> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LoaiLePhiHQ item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}