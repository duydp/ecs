﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Thông báo thuế
    /// </summary>
    /// 
    public class AdditionalDocument : IssueBase
    {
        /// <summary>
        /// Mã thông báo thuế (= 006)
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }
        
        /// <summary>
        /// Hợp đồng - Giấy phép - Vận đơn
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }

        /// <summary>
        /// Ngày hết hạn thông báo thuế 
        /// / hết hạn hợp đồng
        /// </summary>
        [XmlElement("expire")]
        public string Expire { get; set; }
        
        [XmlElement("isDebt")]
        public string IsDebt { get; set; }

        [XmlElement("submit")]
        public string Submit { get; set; }

        [XmlElement("DutyTaxFee")]
        public List<DutyTaxFee> DutyTaxFees { get; set; }        
        /// <summary>
        /// Chương, loại, khoản, mục
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }
        /// <summary>
        ///  ....  
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }
        /// <summary>
        /// Ma xac dinh loai chung tu thanh khoan (tieu huy, bieu tang, to khai tham chieu ...) 
        /// </summary>
        [XmlElement("documentType")]
        public string DocumentType { get; set; }
        /// <summary>
        /// Vi du: tieu huy
        /// </summary>
        [XmlElement("documentReference")]
        public string DocumentReference { get; set; }
        /// <summary>
        /// <!--So chung tu (co the la bo so to khai .... -->
        /// </summary>
        [XmlElement("description")]
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("GoodItems")]
        public List<GoodsItem> GoodItems { get; set; }


        /// <summary>
        /// Phiếu kho
        /// </summary>
        [XmlElement("CustomsGoodsItem")]
        public List<CustomsGoodsItem> CustomsGoodsItem { get; set; }

        /// <summary>
        /// Giờ làm thủ tục HH:MM
        /// </summary>
        [XmlElement("hour")]
        public string Hour { get; set; }

        /// <summary>
        /// Ghi chú khác
        /// </summary>
        [XmlElement("content")]
        public string Content { get; set; }

        /// <summary>
        /// Tuyến đường
        /// </summary>
        [XmlElement("route")]
        public string Route { get; set; }

        /// <summary>
        /// Số điện thoại hải quan nơi đi
        /// </summary>
        [XmlElement("tel")]
        public string Tel { get; set; }

        /// <summary>
        /// Số Fax hải quan nơi đi
        /// </summary>
        [XmlElement("fax")]
        public string Fax { get; set; }

        /// <summary>
        /// Thời gian vận chuyển
        /// </summary>
        [XmlElement("time")]
        public string Time { get; set; }

        /// <summary>
        /// Kilomet vận chuyển
        /// </summary>
        [XmlElement("km")]
        public string Km { get; set; }

        /// <summary>
        /// AttachedFile
        /// </summary>
        [XmlElement("AttachedFile")]
        public AttachedFile AttachedFile { get; set; }

        /// <summary>
        /// Số thứ tự 
        /// </summary>
        [XmlElement("sequence")]
        public string Sequence { get; set; }

        /// <summary>
        /// Số phiếu nhập
        /// </summary>
        [XmlElement("identification")]
        public string Identification { get; set; }

        /// <summary>
        /// Tên người giao hàng
        /// </summary>
        [XmlElement("nameConsignor")]
        public string NameConsignor { get; set; }

        /// <summary>
        /// Mã người giao hàng
        /// </summary>
        [XmlElement("identityConsignor")]
        public string IdentityConsignor { get; set; }

        /// <summary>
        /// Tên người nhận hàng
        /// </summary>
        [XmlElement("nameConsignee")]
        public string NameConsignee { get; set; }

        /// <summary>
        /// Mã người nhận hàng
        /// </summary>
        [XmlElement("identityConsignee")]
        public string IdentityConsignee { get; set; }

        /// <summary>
        /// Mã người nhận hàng
        /// </summary>
        [XmlElement("DeclarationDocument")]
        public DeclarationDocument DeclarationDocument { get; set; }

        /// <summary>
        /// Mã người nhận hàng
        /// </summary>
        [XmlElement("AdditionalDocument")]
        public AdditionalDocument AdditionalDocumentSub { get; set; }

        ///// <summary>
        ///// Mã người nhận hàng
        ///// </summary>
        //[XmlElement("content")]
        //public string Content { get; set; }
    }
}
