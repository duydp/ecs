using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.Components.AnDinhThue
{
    public partial class AnDinhThue
    {
        enum SacThueType
        {
            XNK,
            VAT,
            TTDB,
            TVCBPG
        }

        public List<ChiTiet> ChiTietCollections = new List<ChiTiet>();
        public ChiTiet ThueXNK;
        public ChiTiet ThueVAT;
        public ChiTiet ThueTTDB;
        public ChiTiet ThueTVCBPG;

        public void LoadChiTiet()
        {
            ChiTietCollections = (List<ChiTiet>)ChiTiet.SelectCollectionBy_IDAnDinhThue(this.ID);

            foreach (ChiTiet item in ChiTietCollections)
            {
                if (item.SacThue != null && item.SacThue.Trim() == SacThueType.XNK.ToString())
                    ThueXNK = item;
                else if (item.SacThue != null && item.SacThue.Trim() == SacThueType.VAT.ToString())
                    ThueVAT = item;
                else if (item.SacThue != null && item.SacThue.Trim() == SacThueType.TTDB.ToString())
                    ThueTTDB = item;
                else if (item.SacThue != null && item.SacThue.Trim() == SacThueType.TVCBPG.ToString())
                    ThueTVCBPG = item;
            }

            if (ThueXNK == null) ThueXNK = new ChiTiet();
            if (ThueVAT == null) ThueVAT = new ChiTiet();
            if (ThueTTDB == null) ThueTTDB = new ChiTiet();
            if (ThueTVCBPG == null) ThueTVCBPG = new ChiTiet();
        }
    }
}