﻿namespace Company.Interface.VNACCS.Vouchers
{
    partial class VNACC_BillOfLadingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgListVDG_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_BillOfLadingForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.uiGroupBox25 = new Janus.Windows.EditControls.UIGroupBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.dateNgayTN = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.ctrCoQuanHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSoTN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListVDG = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteVDG = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaNguoiPH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoLuongVDN = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoVanDonGoc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cbbLoaiHH = new Janus.Windows.EditControls.UIComboBox();
            this.cbbPhanLoai = new Janus.Windows.EditControls.UIComboBox();
            this.clcNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAddVDG = new Janus.Windows.EditControls.UIButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.txtTenDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdFeedBack1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedBack");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateGuidString1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdFeedBack = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedBack");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateGuidString = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox25)).BeginInit();
            this.uiGroupBox25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListVDG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 706), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 31);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 706);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 682);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 682);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(203, 31);
            this.grbMain.Size = new System.Drawing.Size(1142, 706);
            // 
            // uiGroupBox25
            // 
            this.uiGroupBox25.AutoScroll = true;
            this.uiGroupBox25.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox25.Controls.Add(this.linkLabel1);
            this.uiGroupBox25.Controls.Add(this.dateNgayTN);
            this.uiGroupBox25.Controls.Add(this.ctrCoQuanHaiQuan);
            this.uiGroupBox25.Controls.Add(this.lblTrangThai);
            this.uiGroupBox25.Controls.Add(this.label15);
            this.uiGroupBox25.Controls.Add(this.label8);
            this.uiGroupBox25.Controls.Add(this.label14);
            this.uiGroupBox25.Controls.Add(this.label9);
            this.uiGroupBox25.Controls.Add(this.txtSoTN);
            this.uiGroupBox25.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox25.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox25.Name = "uiGroupBox25";
            this.uiGroupBox25.Size = new System.Drawing.Size(1136, 92);
            this.uiGroupBox25.TabIndex = 0;
            this.uiGroupBox25.Text = "Thông tin chung";
            this.uiGroupBox25.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(599, 62);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(175, 13);
            this.linkLabel1.TabIndex = 0;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Hướng dẫn Khai báo Tách vận đơn ";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // dateNgayTN
            // 
            this.dateNgayTN.Location = new System.Drawing.Point(433, 21);
            this.dateNgayTN.Name = "dateNgayTN";
            this.dateNgayTN.ReadOnly = false;
            this.dateNgayTN.Size = new System.Drawing.Size(127, 21);
            this.dateNgayTN.TabIndex = 26;
            this.dateNgayTN.TagName = "";
            this.dateNgayTN.Value = new System.DateTime(2018, 8, 3, 0, 0, 0, 0);
            this.dateNgayTN.WhereCondition = "";
            // 
            // ctrCoQuanHaiQuan
            // 
            this.ctrCoQuanHaiQuan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrCoQuanHaiQuan.Appearance.Options.UseBackColor = true;
            this.ctrCoQuanHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrCoQuanHaiQuan.Code = "";
            this.ctrCoQuanHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrCoQuanHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrCoQuanHaiQuan.IsOnlyWarning = false;
            this.ctrCoQuanHaiQuan.IsValidate = true;
            this.ctrCoQuanHaiQuan.Location = new System.Drawing.Point(149, 58);
            this.ctrCoQuanHaiQuan.Name = "ctrCoQuanHaiQuan";
            this.ctrCoQuanHaiQuan.Name_VN = "";
            this.ctrCoQuanHaiQuan.SetOnlyWarning = false;
            this.ctrCoQuanHaiQuan.SetValidate = false;
            this.ctrCoQuanHaiQuan.ShowColumnCode = true;
            this.ctrCoQuanHaiQuan.ShowColumnName = true;
            this.ctrCoQuanHaiQuan.Size = new System.Drawing.Size(411, 20);
            this.ctrCoQuanHaiQuan.TabIndex = 27;
            this.ctrCoQuanHaiQuan.TagCode = "";
            this.ctrCoQuanHaiQuan.TagName = "";
            this.ctrCoQuanHaiQuan.Where = null;
            this.ctrCoQuanHaiQuan.WhereCondition = "";
            this.ctrCoQuanHaiQuan.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EnterHandle(this.ucCategory_OnEnter);
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(707, 25);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(87, 13);
            this.lblTrangThai.TabIndex = 0;
            this.lblTrangThai.Text = "Chưa khai báo";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 25);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Số tiếp nhận";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(599, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Trạng thái: ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 62);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Hải quan";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(307, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Ngày tiếp nhận";
            // 
            // txtSoTN
            // 
            this.txtSoTN.Location = new System.Drawing.Point(149, 21);
            this.txtSoTN.MaxLength = 12;
            this.txtSoTN.Name = "txtSoTN";
            this.txtSoTN.Size = new System.Drawing.Size(149, 21);
            this.txtSoTN.TabIndex = 20;
            this.txtSoTN.Text = "0";
            this.txtSoTN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox25);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1142, 706);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox6.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox6.Controls.Add(this.uiGroupBox8);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 184);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(1136, 519);
            this.uiGroupBox6.TabIndex = 22;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.dgListVDG);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 203);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1130, 271);
            this.uiGroupBox4.TabIndex = 25;
            this.uiGroupBox4.Text = "Danh sách vận đơn gốc khai báo";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListVDG
            // 
            this.dgListVDG.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListVDG.AlternatingColors = true;
            this.dgListVDG.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListVDG.ColumnAutoResize = true;
            dgListVDG_DesignTimeLayout.LayoutString = resources.GetString("dgListVDG_DesignTimeLayout.LayoutString");
            this.dgListVDG.DesignTimeLayout = dgListVDG_DesignTimeLayout;
            this.dgListVDG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListVDG.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListVDG.FrozenColumns = 3;
            this.dgListVDG.GroupByBoxVisible = false;
            this.dgListVDG.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListVDG.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListVDG.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListVDG.Location = new System.Drawing.Point(3, 17);
            this.dgListVDG.Margin = new System.Windows.Forms.Padding(0);
            this.dgListVDG.Name = "dgListVDG";
            this.dgListVDG.RecordNavigator = true;
            this.dgListVDG.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListVDG.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListVDG.Size = new System.Drawing.Size(1124, 251);
            this.dgListVDG.TabIndex = 15;
            this.dgListVDG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListVDG.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListVDG_RowDoubleClick);
            this.dgListVDG.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListVDG_LoadingRow);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.btnClose);
            this.uiGroupBox5.Controls.Add(this.btnDeleteVDG);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox5.Location = new System.Drawing.Point(3, 474);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1130, 42);
            this.uiGroupBox5.TabIndex = 24;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(1032, 13);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(86, 23);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDeleteVDG
            // 
            this.btnDeleteVDG.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteVDG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteVDG.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteVDG.Image")));
            this.btnDeleteVDG.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteVDG.Location = new System.Drawing.Point(940, 13);
            this.btnDeleteVDG.Name = "btnDeleteVDG";
            this.btnDeleteVDG.Size = new System.Drawing.Size(86, 23);
            this.btnDeleteVDG.TabIndex = 13;
            this.btnDeleteVDG.Text = "Xóa";
            this.btnDeleteVDG.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteVDG.Click += new System.EventHandler(this.btnDeleteVDG_Click);
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.AutoScroll = true;
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.txtMaNguoiPH);
            this.uiGroupBox8.Controls.Add(this.txtSoLuongVDN);
            this.uiGroupBox8.Controls.Add(this.txtSoVanDonGoc);
            this.uiGroupBox8.Controls.Add(this.cbbLoaiHH);
            this.uiGroupBox8.Controls.Add(this.cbbPhanLoai);
            this.uiGroupBox8.Controls.Add(this.clcNgay);
            this.uiGroupBox8.Controls.Add(this.label11);
            this.uiGroupBox8.Controls.Add(this.label5);
            this.uiGroupBox8.Controls.Add(this.label4);
            this.uiGroupBox8.Controls.Add(this.btnAddVDG);
            this.uiGroupBox8.Controls.Add(this.label6);
            this.uiGroupBox8.Controls.Add(this.label3);
            this.uiGroupBox8.Controls.Add(this.label2);
            this.uiGroupBox8.Controls.Add(this.label1);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox8.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(1130, 195);
            this.uiGroupBox8.TabIndex = 22;
            this.uiGroupBox8.Text = "Thông tin vận đơn gốc";
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtMaNguoiPH
            // 
            this.txtMaNguoiPH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguoiPH.Location = new System.Drawing.Point(198, 119);
            this.txtMaNguoiPH.Name = "txtMaNguoiPH";
            this.txtMaNguoiPH.Size = new System.Drawing.Size(620, 21);
            this.txtMaNguoiPH.TabIndex = 11;
            this.txtMaNguoiPH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaNguoiPH.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtSoLuongVDN
            // 
            this.txtSoLuongVDN.DecimalDigits = 20;
            this.txtSoLuongVDN.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongVDN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongVDN.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuongVDN.Location = new System.Drawing.Point(543, 20);
            this.txtSoLuongVDN.MaxLength = 15;
            this.txtSoLuongVDN.Name = "txtSoLuongVDN";
            this.txtSoLuongVDN.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongVDN.Size = new System.Drawing.Size(195, 21);
            this.txtSoLuongVDN.TabIndex = 7;
            this.txtSoLuongVDN.Text = "0";
            this.txtSoLuongVDN.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongVDN.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongVDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongVDN.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtSoVanDonGoc
            // 
            this.txtSoVanDonGoc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDonGoc.Location = new System.Drawing.Point(150, 20);
            this.txtSoVanDonGoc.Name = "txtSoVanDonGoc";
            this.txtSoVanDonGoc.Size = new System.Drawing.Size(244, 21);
            this.txtSoVanDonGoc.TabIndex = 6;
            this.txtSoVanDonGoc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoVanDonGoc.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // cbbLoaiHH
            // 
            this.cbbLoaiHH.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiHH.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbLoaiHH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Hàng container";
            uiComboBoxItem5.Value = "1";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Hàng rời";
            uiComboBoxItem6.Value = "0";
            this.cbbLoaiHH.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem5,
            uiComboBoxItem6});
            this.cbbLoaiHH.Location = new System.Drawing.Point(146, 84);
            this.cbbLoaiHH.Name = "cbbLoaiHH";
            this.cbbLoaiHH.Size = new System.Drawing.Size(248, 21);
            this.cbbLoaiHH.TabIndex = 10;
            this.cbbLoaiHH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbLoaiHH.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // cbbPhanLoai
            // 
            this.cbbPhanLoai.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbPhanLoai.FlatBorderColor = System.Drawing.SystemColors.Control;
            this.cbbPhanLoai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Tách vận đơn cơ học";
            uiComboBoxItem7.Value = "1";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.FormatStyle.ForeColor = System.Drawing.SystemColors.WindowText;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Tách vận đơn lý thuyết ";
            uiComboBoxItem8.Value = "2";
            this.cbbPhanLoai.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem7,
            uiComboBoxItem8});
            this.cbbPhanLoai.Location = new System.Drawing.Point(543, 51);
            this.cbbPhanLoai.Name = "cbbPhanLoai";
            this.cbbPhanLoai.Size = new System.Drawing.Size(195, 21);
            this.cbbPhanLoai.TabIndex = 9;
            this.cbbPhanLoai.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbPhanLoai.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // clcNgay
            // 
            this.clcNgay.CustomFormat = "dd/MM/yyyy";
            this.clcNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgay.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgay.DropDownCalendar.Name = "";
            this.clcNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgay.Location = new System.Drawing.Point(150, 51);
            this.clcNgay.Name = "clcNgay";
            this.clcNgay.Nullable = true;
            this.clcNgay.NullButtonText = "Xóa";
            this.clcNgay.ShowNullButton = true;
            this.clcNgay.Size = new System.Drawing.Size(131, 21);
            this.clcNgay.TabIndex = 8;
            this.clcNgay.TodayButtonText = "Hôm nay";
            this.clcNgay.Value = new System.DateTime(2018, 7, 27, 0, 0, 0, 0);
            this.clcNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgay.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 89);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Loại hàng hóa :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(400, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Phân loại tách vận đơn :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(400, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Số lượng vận đơn nhánh :";
            // 
            // btnAddVDG
            // 
            this.btnAddVDG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddVDG.Image = ((System.Drawing.Image)(resources.GetObject("btnAddVDG.Image")));
            this.btnAddVDG.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddVDG.Location = new System.Drawing.Point(198, 153);
            this.btnAddVDG.Name = "btnAddVDG";
            this.btnAddVDG.Size = new System.Drawing.Size(86, 23);
            this.btnAddVDG.TabIndex = 12;
            this.btnAddVDG.Text = "Ghi";
            this.btnAddVDG.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddVDG.Click += new System.EventHandler(this.btnAddVDG_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(303, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(415, 28);
            this.label6.TabIndex = 0;
            this.label6.Text = "Sau khi nhập Thông tin vận đơn gốc và nhấn nút Ghi \r\nPhần mềm sẽ Tự động Tách thà" +
                "nh các vận đơn nhánh cho doanh nghiêp.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(172, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Mã người phát hành vận đơn gốc :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Ngày vận đơn gốc :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số vận đơn gốc :";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label68);
            this.uiGroupBox3.Controls.Add(this.label67);
            this.uiGroupBox3.Controls.Add(this.txtTenDoanhNghiep);
            this.uiGroupBox3.Controls.Add(this.txtMaDoanhNghiep);
            this.uiGroupBox3.Controls.Add(this.label16);
            this.uiGroupBox3.Controls.Add(this.label17);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 100);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1136, 84);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.Text = "Thông tin doanh nghiệp XNK";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Red;
            this.label68.Location = new System.Drawing.Point(824, 23);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(16, 16);
            this.label68.TabIndex = 0;
            this.label68.Text = "*";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Red;
            this.label67.Location = new System.Drawing.Point(311, 54);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(16, 16);
            this.label67.TabIndex = 0;
            this.label67.Text = "*";
            // 
            // txtTenDoanhNghiep
            // 
            this.txtTenDoanhNghiep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoanhNghiep.Location = new System.Drawing.Point(156, 21);
            this.txtTenDoanhNghiep.Name = "txtTenDoanhNghiep";
            this.txtTenDoanhNghiep.Size = new System.Drawing.Size(662, 21);
            this.txtTenDoanhNghiep.TabIndex = 4;
            this.txtTenDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDoanhNghiep.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtMaDoanhNghiep
            // 
            this.txtMaDoanhNghiep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDoanhNghiep.Location = new System.Drawing.Point(156, 52);
            this.txtMaDoanhNghiep.Name = "txtMaDoanhNghiep";
            this.txtMaDoanhNghiep.Size = new System.Drawing.Size(149, 21);
            this.txtMaDoanhNghiep.TabIndex = 5;
            this.txtMaDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaDoanhNghiep.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(22, 56);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Mã doanh nghiệp :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(22, 25);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(100, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Tên doanh nghiệp :";
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave,
            this.cmdSend,
            this.cmdFeedBack,
            this.cmdResult,
            this.cmdUpdateGuidString});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.Id = new System.Guid("db3f14b4-f472-45b2-95e5-5df0dc360a22");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.LockCommandBars = true;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave1,
            this.cmdSend1,
            this.cmdFeedBack1,
            this.cmdResult1,
            this.cmdUpdateGuidString1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(506, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdFeedBack1
            // 
            this.cmdFeedBack1.Key = "cmdFeedBack";
            this.cmdFeedBack1.Name = "cmdFeedBack1";
            // 
            // cmdResult1
            // 
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            // 
            // cmdUpdateGuidString1
            // 
            this.cmdUpdateGuidString1.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString1.Name = "cmdUpdateGuidString1";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "Lưu lại";
            // 
            // cmdSend
            // 
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdFeedBack
            // 
            this.cmdFeedBack.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedBack.Image")));
            this.cmdFeedBack.Key = "cmdFeedBack";
            this.cmdFeedBack.Name = "cmdFeedBack";
            this.cmdFeedBack.Text = "Nhận phản hồi";
            // 
            // cmdResult
            // 
            this.cmdResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult.Image")));
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "Kết quả xử lý";
            // 
            // cmdUpdateGuidString
            // 
            this.cmdUpdateGuidString.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateGuidString.Image")));
            this.cmdUpdateGuidString.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Name = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Text = "Cập nhật chuỗi phản hồi";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1348, 28);
            // 
            // VNACC_BillOfLadingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1348, 740);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_BillOfLadingForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Khai báo Tách vận đơn";
            this.Load += new System.EventHandler(this.VNACC_BillOfLadingForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VNACC_BillOfLadingForm_FormClosing);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox25)).EndInit();
            this.uiGroupBox25.ResumeLayout(false);
            this.uiGroupBox25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListVDG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            this.uiGroupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label67;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoanhNghiep;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiep;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox25;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dateNgayTN;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrCoQuanHaiQuan;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTN;
        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedBack1;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedBack;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.GridEX dgListVDG;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnDeleteVDG;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiPH;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongVDN;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDonGoc;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiHH;
        private Janus.Windows.EditControls.UIComboBox cbbPhanLoai;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgay;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIButton btnAddVDG;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
    }
}