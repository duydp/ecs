-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_AnHanThue_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_AnHanThue_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_AnHanThue_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_AnHanThue_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_AnHanThue_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_AnHanThue_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_AnHanThue_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_AnHanThue_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_AnHanThue_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_AnHanThue_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_AnHanThue_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_AnHanThue_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_AnHanThue_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_AnHanThue_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_AnHanThue_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_AnHanThue_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_AnHanThue_SelectBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_AnHanThue_SelectBy_TKMD_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_AnHanThue_DeleteBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_AnHanThue_DeleteBy_TKMD_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnHanThue_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnHanThue_Insert]
	@ID bigint,
	@TKMD_ID bigint,
	@IsAnHan bit,
	@LyDoAnHan nvarchar(255),
	@SoNgay bigint
AS
INSERT INTO [dbo].[t_KDT_AnHanThue]
(
	[ID],
	[TKMD_ID],
	[IsAnHan],
	[LyDoAnHan],
	[SoNgay]
)
VALUES
(
	@ID,
	@TKMD_ID,
	@IsAnHan,
	@LyDoAnHan,
	@SoNgay
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnHanThue_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnHanThue_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@IsAnHan bit,
	@LyDoAnHan nvarchar(255),
	@SoNgay bigint
AS

UPDATE
	[dbo].[t_KDT_AnHanThue]
SET
	[TKMD_ID] = @TKMD_ID,
	[IsAnHan] = @IsAnHan,
	[LyDoAnHan] = @LyDoAnHan,
	[SoNgay] = @SoNgay
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnHanThue_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnHanThue_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@IsAnHan bit,
	@LyDoAnHan nvarchar(255),
	@SoNgay bigint
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_AnHanThue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_AnHanThue] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[IsAnHan] = @IsAnHan,
			[LyDoAnHan] = @LyDoAnHan,
			[SoNgay] = @SoNgay
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_AnHanThue]
	(
			[ID],
			[TKMD_ID],
			[IsAnHan],
			[LyDoAnHan],
			[SoNgay]
	)
	VALUES
	(
			@ID,
			@TKMD_ID,
			@IsAnHan,
			@LyDoAnHan,
			@SoNgay
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnHanThue_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnHanThue_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_AnHanThue]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnHanThue_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnHanThue_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_AnHanThue]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnHanThue_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnHanThue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_AnHanThue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnHanThue_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnHanThue_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[IsAnHan],
	[LyDoAnHan],
	[SoNgay]
FROM
	[dbo].[t_KDT_AnHanThue]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnHanThue_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnHanThue_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[IsAnHan],
	[LyDoAnHan],
	[SoNgay]
FROM
	[dbo].[t_KDT_AnHanThue]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnHanThue_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnHanThue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[IsAnHan],
	[LyDoAnHan],
	[SoNgay]
FROM [dbo].[t_KDT_AnHanThue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnHanThue_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Monday, December 03, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnHanThue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[IsAnHan],
	[LyDoAnHan],
	[SoNgay]
FROM
	[dbo].[t_KDT_AnHanThue]	

GO

