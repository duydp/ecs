﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.Maper;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.Interface.VNACCS;

namespace Company.Interface
{
    public partial class VNACC_ChuyenCuaKhau : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiVanChuyen TKVC = new KDT_VNACC_ToKhaiVanChuyen();
        public VNACC_ChuyenCuaKhau()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclarationOnTransportation.OLA.ToString());
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdHangHoa":
                    showHangHoa();
                    break;
                case "cmdContainer":
                    showContainer();
                    break;
                case "cmdSoTKXuat":
                    showTKXuat();
                    break;
                case "cmdLuu":
                    save();
                    break;
                case "cmdOLA":
                    SendVnaccs(false);
                    break;
                case "cmdInTamOLA":
                    showInTamOLA();
                    break;
                case "cmdTrungChuyen":
                    showTrungChuyen();
                    break;
                case "cmdOLC":
                    SendVnaccsOLC(false,false);
                    break;
                case "cmdBOT":
                    SendVnaccsOLC(true,false);
                    break;
                case "cmdKetQuaHQ":
                    ShowKetQuaTraVe();
                    break;
                case "cmdKetQuaXuLy":
                    ShowKetQuaXuLy();
                    break;
                case "cmdKhaiBaoSua":
                    if (ShowMessage("Bạn có muốn chuyển sang trạng thái sửa ? ", true) == "Yes")
                    {
                        TKVC.TrangThaiXuLy = EnumTrangThaiXuLy.KhaiBaoSua;
                        TKVC.Update();
                        setCommandStatus();
                    }
                    break;

            }
        }
        private void showInTamOLA()
        {
            try
            {
                MsgLog msg = MsgLog.GetThongTinToKhaiTam(TKVC.InputMessageID, "OLA");
                if (msg != null && msg.ID > 0)
                    ProcessReport.ShowReport(msg.ID, "VAS5030", new ResponseVNACCS(msg.Log_Messages).ResponseData);
                else
                    ShowMessage("Không tìm thấy bản khai tạm", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void showHangHoa()
        {
            try
            {
                VNACC_ChuyenCuaKhau_HangHoaForm frm = new VNACC_ChuyenCuaKhau_HangHoaForm();
                frm.TKVC = this.TKVC;
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void showTKXuat()
        {
            try
            {
                VNACC_ChuyenCuaKhau_SoTKXuat frm = new VNACC_ChuyenCuaKhau_SoTKXuat();
                frm.TKVC = this.TKVC;
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void showContainer()
        {
            try
            {
                VNACC_ChuyenCuaKhau_Container frm = new VNACC_ChuyenCuaKhau_Container();
                frm.TKVC = this.TKVC;
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void showTrungChuyen()
        {
            try
            {
                VNACC_ChuyenCuaKhau_TrungChuyenForm frm = new VNACC_ChuyenCuaKhau_TrungChuyenForm();
                frm.TKVC = this.TKVC;
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void save()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //if (!ValidateForm(false))
                //    return;
                if (TKVC.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam)
                {
                    if (ShowMessage("Tờ khai đã khai báo, bạn có muốn lưu lại tờ khai để khai báo lại ?", true) == "Yes")
                    {
                        TKVC.TrangThaiXuLy = EnumTrangThaiXuLy.ChuaKhaiBao;
                    }
                    else
                        return;
                }


                GetToKhai();

                if (TKVC.VanDonCollection.Count == 0)
                {
                    ShowMessage("Chưa nhập thông tin vận đơn", false);
                }
                else
                {
                    TKVC.InsertUpdateFull();
                    ShowMessage("Lưu tờ khai thành công", false);
                }
                this.Cursor = Cursors.Default;
                setCommandStatus();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

        }

        private void GetToKhai()
        {
            try
            {
                TKVC.CoBaoXuatNhapKhau = cboPhanLoaiXuatNhap.SelectedItem.Value.ToString();
                TKVC.CoQuanHaiQuan = ctrCoQuanHaiQuan.Code;
                TKVC.MaNguoiVC = txtMaNhaVanChuyen.Text;
                TKVC.TenNguoiVC = txtTenNha_VC.Text;
                TKVC.DiaChiNguoiVC = txtDiaChiNha_VC.Text;
                TKVC.SoHopDongVC = txtSoHopDong.Text;
                TKVC.NgayHopDongVC = clcNgayHD.Value;
                TKVC.NgayHetHanHopDongVC = clcNgayHHHD.Value;
                TKVC.MaPhuongTienVC = ctrMaPhuongTien.Code;
                TKVC.MaMucDichVC = ctrMaMucDich.Code;
                TKVC.LoaiHinhVanTai = ctrLoaiHinhVT.Code;
                TKVC.NgayDuKienBatDauVC = clcNgayDuKienBatDau.Value;
                TKVC.GioDuKienBatDauVC = txtGioBatDau.Text;
                TKVC.NgayDuKienKetThucVC = clcNgayDuKienKetThuc.Value;
                TKVC.GioDuKienKetThucVC = txtGioKetThuc.Text;
                TKVC.MaDiaDiemXepHang = ctrMaXepHang.Code;
                TKVC.MaViTriXepHang = ctrViTriXepHang.Code;
                TKVC.MaCangCuaKhauGaXepHang = ctrCangXepHang.Code;
                TKVC.DiaDiemXepHang = txtTenDiaDiemXepHang.Text;
                TKVC.MaDiaDiemDoHang = ctrMaDoHang.Code;
                TKVC.MaViTriDoHang = ctrViTriDoHang.Code;
                TKVC.MaCangCuaKhauGaDoHang = ctrCangDoHang.Code;
                TKVC.DiaDiemDoHang = txtTenDiaDiemDoHang.Text;
                TKVC.TuyenDuongVC = txtTuyenDuong.Text;
                TKVC.LoaiBaoLanh = (cbbLoaiBaoLanh.SelectedValue == null) ? "" : cbbLoaiBaoLanh.SelectedValue.ToString();
                TKVC.MaNganHangBaoLanh = ctrMaNganHang.Code;
                TKVC.NamPhatHanhBaoLanh = Convert.ToInt32(txtNamPhatHanhBL.Text);
                TKVC.KyHieuChungTuBaoLanh = txtKyHieuCTBaoLanh.Text;
                TKVC.SoChungTuBaoLanh = txtSoCTBaoLanh.Text;
                TKVC.SoTienBaoLanh = Convert.ToDecimal(txtSoTienBaoLanh.Text);
                TKVC.GhiChu = txtGhiChu.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }
        private void SetToKhai()
        {
            try
            {
                txtSoToKhaiVC.Text = (TKVC.SoToKhaiVC == 0) ? "" : Convert.ToString(TKVC.SoToKhaiVC);
                clcNgayDangKy.Value = TKVC.NgayDangKy;
                cboPhanLoaiXuatNhap.SelectedValue = (TKVC.CoBaoXuatNhapKhau == null) ? "E" : TKVC.CoBaoXuatNhapKhau;
                ctrCoQuanHaiQuan.Code = TKVC.CoQuanHaiQuan;
                txtMaNhaVanChuyen.Text = TKVC.MaNguoiVC;
                txtTenNha_VC.Text = TKVC.TenNguoiVC;
                txtDiaChiNha_VC.Text = TKVC.DiaChiNguoiVC;
                txtSoHopDong.Text = TKVC.SoHopDongVC;
                clcNgayHD.Value = TKVC.NgayHopDongVC;
                clcNgayHHHD.Value = TKVC.NgayHetHanHopDongVC;
                ctrMaPhuongTien.Code = TKVC.MaPhuongTienVC;
                ctrMaMucDich.Code = TKVC.MaMucDichVC;
                ctrLoaiHinhVT.Code = TKVC.LoaiHinhVanTai;
                clcNgayDuKienBatDau.Value = TKVC.NgayDuKienBatDauVC;
                txtGioBatDau.Text = TKVC.GioDuKienBatDauVC;
                clcNgayDuKienKetThuc.Value = TKVC.NgayDuKienKetThucVC;
                txtGioKetThuc.Text = TKVC.GioDuKienKetThucVC;

                ctrMaXepHang.Code = TKVC.MaDiaDiemXepHang;
                ctrViTriXepHang.Code = TKVC.MaViTriXepHang;
                ctrCangXepHang.Code = TKVC.MaCangCuaKhauGaXepHang;
                txtTenDiaDiemXepHang.Text = TKVC.DiaDiemXepHang;

                ctrMaDoHang.Code = TKVC.MaDiaDiemDoHang;
                ctrViTriDoHang.Code = TKVC.MaViTriDoHang;
                ctrCangDoHang.Code = TKVC.MaCangCuaKhauGaDoHang;
                txtTenDiaDiemDoHang.Text = TKVC.DiaDiemDoHang;

                txtTuyenDuong.Text = TKVC.TuyenDuongVC;
                cbbLoaiBaoLanh.SelectedValue = TKVC.LoaiBaoLanh;
                ctrMaNganHang.Code = TKVC.MaNganHangBaoLanh;
                txtNamPhatHanhBL.Text = TKVC.NamPhatHanhBaoLanh.ToString();
                txtKyHieuCTBaoLanh.Text = TKVC.KyHieuChungTuBaoLanh;
                txtSoCTBaoLanh.Text = TKVC.SoChungTuBaoLanh;
                txtSoTienBaoLanh.Text = TKVC.SoTienBaoLanh.ToString();
                txtGhiChu.Text = TKVC.GhiChu;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }            


        }

        private void VNACC_ChuyenCuaKhau_Load(object sender, EventArgs e)
        {
            try
            {
                this.SetIDControl();
                string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;// Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(MaHaiQuan, "MaHQ");// MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();

                if (TKVC != null)
                {
                    TuDongCapNhatThongTin();
                    SetToKhai();
                    setCommandStatus();
                }
                ctrMaPhuongTien_Leave(null, null);
                if (TKVC.TrangThaiXuLy == "0")
                    cmdTrungChuyen.Visible = cmdTrungChuyen1.Visible = Janus.Windows.UI.InheritableBoolean.False;
                if (string.IsNullOrEmpty(TKVC.InputMessageID))
                    cmdKetQuaHQ.Visible = cmdKetQuaHQ1.Visible = Janus.Windows.UI.InheritableBoolean.False;
                else
                    cmdKetQuaHQ.Visible = cmdKetQuaHQ1.Visible = Janus.Windows.UI.InheritableBoolean.True;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void ctrMaPhuongTien_Leave(object sender, EventArgs e)
        {
            try
            {
                if (ctrMaPhuongTien.Code == "4")
                {
                    ctrCangDoHang.CategoryType = ECategory.A620;
                    ctrCangXepHang.CategoryType = ECategory.A620;
                }
                else if (ctrMaPhuongTien.Code == "5")
                {
                    ctrCangDoHang.CategoryType = ECategory.A601;
                    ctrCangXepHang.CategoryType = ECategory.A601;
                }
                else
                {
                    ctrCangDoHang.CategoryType = ECategory.A016;
                    ctrCangXepHang.CategoryType = ECategory.A016;
                }
                ctrCangDoHang.Where = "I";
                ctrCangDoHang.ReLoadData();
                ctrCangXepHang.Where = "I";
                ctrCangXepHang.ReLoadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void setCommandStatus()
        {
            if (TKVC.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao || TKVC.TrangThaiXuLy == null || TKVC.TrangThaiXuLy == EnumTrangThaiXuLy.TuChoi)
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdKhaiBaoHuy.Visible = cmdKhaiBaoHuy1.Visible = cmdBOT.Visible = cmdBOT1.Visible = cmdKhaiBaoSua.Visible = cmdKhaiBaoSua1.Visible = cmdOLC.Visible = cmdOLC1.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdOLA.Visible = Janus.Windows.UI.InheritableBoolean.True;
                
                chkDungChungSoQuanLy.Visible = false;
            }
            else if (TKVC.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam)
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdKhaiBaoHuy.Visible = cmdKhaiBaoHuy1.Visible = cmdBOT.Visible = cmdBOT1.Visible = cmdKhaiBaoSua.Visible = cmdKhaiBaoSua1.Visible = cmdOLA.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdOLC1.Visible = cmdOLC.Visible = Janus.Windows.UI.InheritableBoolean.True;
                
                chkDungChungSoQuanLy.Visible = true;

            }
            else if (TKVC.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoSua)
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdBOT.Visible = cmdBOT1.Visible = Janus.Windows.UI.InheritableBoolean.True;
                cmdKhaiBaoHuy1.Visible = cmdKhaiBaoHuy.Visible = Janus.Windows.UI.InheritableBoolean.True;
                cmdOLC.Visible = cmdOLC1.Visible = cmdOLA.Visible = Janus.Windows.UI.InheritableBoolean.False;
                chkDungChungSoQuanLy.Visible = false;
                cmdKhaiBaoSua.Visible = cmdKhaiBaoSua1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKVC.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc)
            {
                cmdLuu.Enabled = cmdLuu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdKhaiBaoHuy1.Visible = cmdKhaiBaoHuy.Visible = Janus.Windows.UI.InheritableBoolean.True;
                cmdOLC.Visible = cmdOLC1.Visible = cmdOLA.Visible = Janus.Windows.UI.InheritableBoolean.False;
                cmdKhaiBaoSua1.Visible = cmdKhaiBaoSua.Visible = Janus.Windows.UI.InheritableBoolean.True;
                chkDungChungSoQuanLy.Visible = false;
            }

            if (string.IsNullOrEmpty(TKVC.InputMessageID))
                cmdKetQuaHQ.Visible = cmdKetQuaHQ1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            else
                cmdKetQuaHQ.Visible = cmdKetQuaHQ1.Visible = Janus.Windows.UI.InheritableBoolean.True;

        }
        private void SendVnaccs(bool KhaiBaoSua)
        {
            try
            {
                if (TKVC.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {
                    OLA ola = VNACCMaperFromObject.OLAMapper(TKVC);
                    if (ola == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    MessagesSend msg;
                    if (!KhaiBaoSua)
                    {
                        TKVC.InputMessageID = HelperVNACCS.NewInputMSGID();
                        TKVC.InsertUpdateFull();
                        msg = MessagesSend.Load<OLA>(ola, TKVC.InputMessageID);

                    }
                    else
                    {
                        msg = MessagesSend.Load<OLA>(ola, true, EnumNghiepVu.IDA01, TKVC.InputMessageID);
                    }

                    MsgLog.SaveMessages(msg, TKVC.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = false;
                    f.inputMSGID = TKVC.InputMessageID;
                    f.ShowDialog(this);
                    if (f.DialogResult == DialogResult.Cancel)
                    {
                        this.ShowMessage(f.msgFeedBack, false);
                    }
                    else if (f.DialogResult == DialogResult.No)
                    {
                    }
                    else
                    {
                        string ketqua = "Khai báo thông tin thành công";
                        CapNhatThongTin(f.feedback.ResponseData);
                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                        {
                            try
                            {
                                decimal SoToKhai = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
                                ketqua = ketqua + Environment.NewLine;
                                ketqua += "Số tờ khai: " + SoToKhai;
                            }
                            catch (System.Exception ex)
                            {
                                ketqua += Environment.NewLine + "Lỗi cập nhật số tờ khai: " + Environment.NewLine + ex.Message;
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                        this.ShowMessageTQDT(ketqua, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SendVnaccsOLC(bool KhaiBaoSua,bool isHuy)
        {
            try
            {
                if (TKVC.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {

                    MessagesSend msg;
                    if (!KhaiBaoSua)
                    {
                        OLC OLC = VNACCMaperFromObject.OLCMapper(TKVC, chkDungChungSoQuanLy.Checked);
                        if (OLC == null)
                        {
                            this.ShowMessage("Lỗi khi tạo messages !", false);
                            return;
                        }
                        msg = MessagesSend.Load<OLC>(OLC, TKVC.InputMessageID);
                    }
                    else
                    {
                        //IDE IDE = VNACCMaperFromObject.IDEMapper(TKVC.SoToKhaiVC, string.Empty);
                        //if (IDE == null)
                        //{
                        //    this.ShowMessage("Lỗi khi tạo messages !", false);
                        //    return;
                        //}
                        //msg = MessagesSend.Load<IDE>(IDE, TKVC.InputMessageID);
                        COT cot = VNACCMaperFromObject.cotMapper(TKVC, isHuy);
                        if (cot == null)
                        {
                            this.ShowMessage("Lỗi khi tạo messages !", false);
                            return;
                        }
                        msg = MessagesSend.Load<COT>(cot, TKVC.InputMessageID);

                    }

                    MsgLog.SaveMessages(msg, TKVC.ID, EnumThongBao.SendMess, "");
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true;
                    f.isRep = true;
                    f.inputMSGID = TKVC.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result)
                    {
                        TKVC.TrangThaiXuLy = isHuy ? EnumTrangThaiXuLy.TuChoi : EnumTrangThaiXuLy.KhaiBaoSua;
                        TuDongCapNhatThongTin();

                    }
                    else if (f.DialogResult == DialogResult.Cancel)
                    {
                        ShowMessage(f.msgFeedBack, false);
                    }

                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        #region Cập nhật thông tin
        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            CapNhatThongTin(msgResult, string.Empty);
        }
        private void CapNhatThongTin(ReturnMessages msgResult, string MaNV)
        {
            ProcessMessages.GetDataResult_TKVC(msgResult, MaNV, TKVC);
            TKVC.InsertUpdateFull();
            SetToKhai();
            setCommandStatus();
        }
        private void TuDongCapNhatThongTin()
        {
            if (TKVC != null && TKVC.SoToKhaiVC > 0 && TKVC.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan = '{0}'", TKVC.SoToKhaiVC.ToString()), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        if (msgPb.TrangThai == EnumTrangThaiXuLyMessage.ChuaXuLy)
                        {
                            ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                            CapNhatThongTin(msgReturn);
                            //msgPb.Delete();
                            msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                        }
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }
            }
        }
        #endregion


        private void ShowKetQuaTraVe()
        {
            try
            {
                SendmsgVNACCFrm f = new SendmsgVNACCFrm(null);
                f.isSend = false;
                f.isRep = true;
                f.inputMSGID = TKVC.InputMessageID;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }            
        }
        private bool SetIDControl()
        {
            bool isValid = true;
            txtSoToKhaiVC.Tag = "OLT";
            cboPhanLoaiXuatNhap.Tag = "IEI";
            ctrCoQuanHaiQuan.TagName = "CHC";
            txtMaNhaVanChuyen.Tag = "TRC";
            txtTenNha_VC.Tag = "TRN";
            txtDiaChiNha_VC.Tag = "TRA";
            txtSoHopDong.Tag = "TCN";
            clcNgayHD.TagName = "TCD";
            clcNgayHHHD.TagName = "EDC";
            ctrMaPhuongTien.TagName = "BYA";
            ctrMaMucDich.TagName = "OBJ";
            ctrLoaiHinhVT.TagName = "USS";
            clcNgayDuKienBatDau.TagName = "SDT";
            txtGioBatDau.Tag = "SDH";
            clcNgayDuKienKetThuc.TagName = "EDT";
            txtGioKetThuc.Tag = "EDH";
            ctrMaXepHang.TagCode = "DPR";
            ctrViTriXepHang.TagName = "BRT";
            ctrCangXepHang.TagCode = "PRT";
            txtTenDiaDiemXepHang.Tag = "DPN";
            ctrMaDoHang.TagCode = "ARR";
            ctrViTriDoHang.TagName = "ARB";
            ctrCangDoHang.TagCode = "ARP";
            txtTenDiaDiemDoHang.Tag = "ARN";
            txtTuyenDuong.Tag = "ROU";
            cbbLoaiBaoLanh.Tag = "TOS";
            ctrMaNganHang.TagName = "SBC";
            txtNamPhatHanhBL.Tag = "RYA";
            txtKyHieuCTBaoLanh.Tag = "SCM";
            txtSoCTBaoLanh.Tag = "SCN";
            txtSoTienBaoLanh.Tag = "SEP";
            txtGhiChu.Tag = "NTA";


            try
            {

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }
        private void ShowKetQuaXuLy()
        {
            try
            {
                VNACC_KetQuaXuLy f = new VNACC_KetQuaXuLy(TKVC.ID, "TKVC");
                f.master_id = TKVC.ID;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
      
    }
}
