﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.Maper;
using Company.KDT.SHARE.VNACCS.LogMessages;

namespace Company.Interface
{
    public partial class VNACC_GiayPhepForm_SFA : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_GiayPhep_SFA GiayPhep = new KDT_VNACC_GiayPhep_SFA();

        public VNACC_GiayPhepForm_SFA()
        {
            InitializeComponent();

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_OGAProcedure.SFA.ToString());
        }

        private void VNACC_GiayPhepForm_Load(object sender, EventArgs e)
        {
            SetIDControl();

            SetMaxLengthControl();

            ucBenDen.Code = "";
            ucBenDen.ReLoadData();

            if (GiayPhep.ID != 0)
            {
                SetGiayPhep();
            }

            ValidateForm(true);

            SetAutoRemoveUnicodeAndUpperCaseControl();
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThemHang":
                    this.ShowThemHang();
                    break;
                case "cmdLuu":
                    this.SaveGiayPhep();
                    break;
                case "cmdChiThiHaiQuan":
                    ShowChiThiHaiQuan();
                    break;

                case "cmdKhaiBao":
                    SendVnaccsIDC(false);
                    break;
                case "cmdKetQuaHQ":
                    ShowKetQuaTraVe();
                    break;
            }
        }
        private void ShowKetQuaTraVe()
        {
            SendmsgVNACCFrm f = new SendmsgVNACCFrm(null);
            f.isSend = false;
            f.isRep = true;
            f.inputMSGID = GiayPhep.InputMessageID;
            f.ShowDialog(this);
        }

        private void ShowChiThiHaiQuan()
        {
            VNACC_ChiThiHaiQuanForm f = new VNACC_ChiThiHaiQuanForm();
            f.Master_ID = GiayPhep.ID;
            f.LoaiThongTin = ELoaiThongTin.GP_SFA;
            f.ShowDialog();
        }

        private void ShowThemHang()
        {
            VNACC_GiayPhep_SFA_HangForm f = new VNACC_GiayPhep_SFA_HangForm();
            f.GiayPhep_SFA = GiayPhep;
            f.ShowDialog();
        }

        private void SaveGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (!ValidateForm(false))
                    return;

                GetGiayPhep();

                GiayPhep.InsertUpdateFull();

                Helper.Controls.MessageBoxControlV.ShowMessage(Company.KDT.SHARE.Components.ThongBao.APPLICATION_SAVE_DATA_SUCCESS_0Param, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                errorProvider.Clear();

                GiayPhep.MaNguoiKhai = txtMaNguoiKhai.Text;
                GiayPhep.MaDonViCapPhep = ucMaDonViCapPhep.Code;
                GiayPhep.SoDonXinCapPhep = txtSoDonXinCapPhep.Text != "" ? Convert.ToInt32(txtSoDonXinCapPhep.Text) : 0;
                GiayPhep.ChucNangChungTu = Convert.ToInt32(ucChucNangChungTu.Code);
                GiayPhep.LoaiGiayPhep = ucLoaiGiayPhep.Code;

                GiayPhep.TenThuongNhanXuatKhau = txtTenThuongNhanXuatKhau.Text;
                GiayPhep.MaBuuChinhXuatKhau = txtMaBuuChinhXuatKhau.Text;
                GiayPhep.SoNhaTenDuongXuatKhau = txtSoNhaTenDuongXuatKhau.Text;
                GiayPhep.PhuongXaXuatKhau = txtPhuongXaXuatKhau.Text;
                GiayPhep.QuanHuyenXuatKhau = txtQuanHuyenXuatKhau.Text;
                GiayPhep.TinhThanhPhoXuatKhau = txtTinhThanhPhoXuatKhau.Text;
                GiayPhep.MaQuocGiaXuatKhau = ucMaQuocGiaXuatKhau.Code;
                GiayPhep.SoDienThoaiXuatKhau = txtSoDienThoaiXuatKhau.Text;
                GiayPhep.SoFaxXuatKhau = txtSoFaxXuatKhau.Text;
                GiayPhep.EmailXuatKhau = txtEmailXuatKhau.Text;

                GiayPhep.SoHopDong = txtSoHopDong.Text;
                GiayPhep.SoVanDon = txtSoVanDon.Text;
                GiayPhep.MaBenDi = ucBenDi.Code;
                GiayPhep.TenBenDi = ucBenDi.Name_VN;

                GiayPhep.MaThuongNhanNhapKhau = txtMaThuongNhanNhapKhau.Text;
                GiayPhep.TenThuongNhanNhapKhau = txtTenThuongNhanNhapKhau.Text;
                GiayPhep.MaBuuChinhNhapKhau = txtMaBuuChinhNhapKhau.Text;
                GiayPhep.SoNhaTenDuongNhapKhau = txtSoNhaTenDuongNhapKhau.Text;
                GiayPhep.MaQuocGiaNhapKhau = ucMaQuocGiaNhapKhau.Code;
                GiayPhep.SoDienThoaiNhapKhau = txtSoDienThoaiNhapKhau.Text;
                GiayPhep.SoFaxNhapKhau = txtSoFaxNhapKhau.Text;
                GiayPhep.EmailNhapKhau = txtEmailNhapKhau.Text;

                GiayPhep.MaBenDen = ucBenDen.Code;
                GiayPhep.TenBenDen = ucBenDen.Name_VN;

                GiayPhep.ThoiGianNhapKhauDuKien = dtThoiGianNhapKhauDuKien.Value;
                GiayPhep.GiaTriHangHoa = Convert.ToDecimal(txtGiaTriHangHoa.Value);
                //GiayPhep.DonViTienTe = txtDonViTienTe.Text;
                GiayPhep.DiaDiemTapKetHangHoa = txtDiaDiemTapKetHangHoa.Text;
                GiayPhep.ThoiGianKiemTra = dtThoiGianKiemTra.Value;
                GiayPhep.DiaDiemKiemTra = txtDiaDiemKiemTra.Text;
                GiayPhep.HoSoLienQuan = txtHoSoLienQuan.Text;
                GiayPhep.GhiChu = txtGhiChu.Text;
                GiayPhep.DaiDienThuongNhanNhapKhau = txtDaiDienThuongNhanNhapKhau.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void SetGiayPhep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                errorProvider.Clear();

                txtMaNguoiKhai.Text = GiayPhep.MaNguoiKhai;
                ucMaDonViCapPhep.Code = GiayPhep.MaDonViCapPhep;
                txtSoDonXinCapPhep.Text = GiayPhep.SoDonXinCapPhep.ToString();
                ucChucNangChungTu.Code = GiayPhep.ChucNangChungTu.ToString();
                ucLoaiGiayPhep.Code = GiayPhep.LoaiGiayPhep;

                txtTenThuongNhanXuatKhau.Text = GiayPhep.TenThuongNhanXuatKhau;
                txtMaBuuChinhXuatKhau.Text = GiayPhep.MaBuuChinhXuatKhau;
                txtSoNhaTenDuongXuatKhau.Text = GiayPhep.SoNhaTenDuongXuatKhau;
                txtPhuongXaXuatKhau.Text = GiayPhep.PhuongXaXuatKhau;
                txtQuanHuyenXuatKhau.Text = GiayPhep.QuanHuyenXuatKhau;
                txtTinhThanhPhoXuatKhau.Text = GiayPhep.TinhThanhPhoXuatKhau;
                ucMaQuocGiaXuatKhau.Code = GiayPhep.MaQuocGiaXuatKhau;
                txtSoDienThoaiXuatKhau.Text = GiayPhep.SoDienThoaiXuatKhau;
                txtSoFaxXuatKhau.Text = GiayPhep.SoFaxXuatKhau;
                txtEmailXuatKhau.Text = GiayPhep.EmailXuatKhau;

                txtSoHopDong.Text = GiayPhep.SoHopDong;
                txtSoVanDon.Text = GiayPhep.SoVanDon;
                ucBenDi.Code = GiayPhep.MaBenDi;
                //txtTenBenDi.Text = GiayPhep.TenBenDi;

                txtMaThuongNhanNhapKhau.Text = GiayPhep.MaThuongNhanNhapKhau;
                txtTenThuongNhanNhapKhau.Text = GiayPhep.TenThuongNhanNhapKhau;
                txtMaBuuChinhNhapKhau.Text = GiayPhep.MaBuuChinhNhapKhau;
                txtSoNhaTenDuongNhapKhau.Text = GiayPhep.SoNhaTenDuongNhapKhau;
                ucMaQuocGiaNhapKhau.Code = GiayPhep.MaQuocGiaNhapKhau;
                txtSoDienThoaiNhapKhau.Text = GiayPhep.SoDienThoaiNhapKhau;
                txtSoFaxNhapKhau.Text = GiayPhep.SoFaxNhapKhau;
                txtEmailNhapKhau.Text = GiayPhep.EmailNhapKhau;

                ucBenDen.Code = GiayPhep.MaBenDen;
                //txtTenBenDen.Text = GiayPhep.TenBenDen;
                dtThoiGianNhapKhauDuKien.Value = GiayPhep.ThoiGianNhapKhauDuKien;
                txtGiaTriHangHoa.Value = GiayPhep.GiaTriHangHoa;
                //txtDonViTienTe.Text = GiayPhep.DonViTienTe;
                txtDiaDiemTapKetHangHoa.Text = GiayPhep.DiaDiemTapKetHangHoa;
                dtThoiGianKiemTra.Value = GiayPhep.ThoiGianKiemTra;
                txtDiaDiemKiemTra.Text = GiayPhep.DiaDiemKiemTra;
                txtHoSoLienQuan.Text = GiayPhep.HoSoLienQuan;
                txtGhiChu.Text = GiayPhep.GhiChu;
                txtDaiDienThuongNhanNhapKhau.Text = GiayPhep.DaiDienThuongNhanNhapKhau;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtMaNguoiKhai.Tag = "SMC";
                ucMaDonViCapPhep.TagName = "APP";
                txtSoDonXinCapPhep.Tag = "APN";
                ucChucNangChungTu.TagName = "FNC";
                ucLoaiGiayPhep.TagName = "APT";
                txtTenThuongNhanXuatKhau.Tag = "EXN";
                txtMaBuuChinhXuatKhau.Tag = "EPC";
                txtSoNhaTenDuongXuatKhau.Tag = "EXA";
                txtPhuongXaXuatKhau.Tag = "EXS";
                txtQuanHuyenXuatKhau.Tag = "EXD";
                txtTinhThanhPhoXuatKhau.Tag = "EXC";
                ucMaQuocGiaXuatKhau.TagName = "ECC";
                txtSoDienThoaiXuatKhau.Tag = "EPN";
                txtSoFaxXuatKhau.Tag = "EXF";
                txtEmailXuatKhau.Tag = "EXE";
                txtSoHopDong.Tag = "CNN";
                txtSoVanDon.Tag = "BLN";
                ucBenDi.TagCode = "DPC";
                ucBenDi.TagName = "DPN";
                txtMaThuongNhanNhapKhau.Tag = "IMC";
                txtTenThuongNhanNhapKhau.Tag = "IMN";
                txtMaBuuChinhNhapKhau.Tag = "IPC";
                txtSoNhaTenDuongNhapKhau.Tag = "IMA";
                ucMaQuocGiaNhapKhau.TagName = "ICC";
                txtSoDienThoaiNhapKhau.Tag = "IPN";
                txtSoFaxNhapKhau.Tag = "IMF";
                txtEmailNhapKhau.Tag = "IME";
                ucBenDen.TagCode = "APC";
                ucBenDen.TagName = "ARN";
                dtThoiGianNhapKhauDuKien.Tag = "EDI";
                txtGiaTriHangHoa.Tag = "TLV";
                //DonViTienTe.Tag = "CU";
                txtDiaDiemTapKetHangHoa.Tag = "PC";
                dtThoiGianKiemTra.Tag = "DI";
                txtDiaDiemKiemTra.Tag = "PI";
                txtHoSoLienQuan.Tag = "AD";
                txtGhiChu.Tag = "RMK";
                txtDaiDienThuongNhanNhapKhau.Tag = "PIC";

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            bool warning = false;

            try
            {
                Cursor = Cursors.WaitCursor;

                isValid &= ValidateControl.ValidateNull(txtMaNguoiKhai, errorProvider, "Mã người khai", isOnlyWarning);
                
                ucMaDonViCapPhep.ShowColumnCode = true; ucMaDonViCapPhep.ShowColumnName = false;
                ucMaDonViCapPhep.SetValidate = !isOnlyWarning; ucMaDonViCapPhep.SetOnlyWarning = isOnlyWarning;
                warning = ucMaDonViCapPhep.IsOnlyWarning;
                isValid &= ucMaDonViCapPhep.IsValidate; //"Mã đơn vị cấp phép"

                ucMaDonViCapPhep.SetValidate = !isOnlyWarning; ucMaDonViCapPhep.SetOnlyWarning = isOnlyWarning;
                isValid &= ucLoaiGiayPhep.IsValidate; //"Loại giấy phép"

                isValid &= ValidateControl.ValidateNull(txtTenThuongNhanXuatKhau, errorProvider, "Thương nhân xuất khẩu", isOnlyWarning);

                ucBenDi.ShowColumnCode = true; ucBenDi.ShowColumnName = false;
                ucBenDi.SetValidate = !isOnlyWarning; ucBenDi.SetOnlyWarning = isOnlyWarning;
                warning = ucBenDi.IsOnlyWarning;
                isValid &= ucBenDi.IsValidate; //"Mã bến đi");
                
                isValid &= ValidateControl.ValidateNull(txtMaThuongNhanNhapKhau, errorProvider, "Mã thương nhân nhập khẩu", isOnlyWarning);

                ucBenDen.ShowColumnCode = true; ucBenDen.ShowColumnName = false;
                ucBenDen.SetValidate = !isOnlyWarning; ucBenDen.SetOnlyWarning = isOnlyWarning;
                warning = ucBenDen.IsOnlyWarning = isOnlyWarning;
                isValid &= ucBenDen.IsValidate; //"Mã bến đến");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtMaNguoiKhai.MaxLength = 13;
                //ucMaDonViCapPhep.MaxLength = 6;
                txtSoDonXinCapPhep.MaxLength = 12;
                //txtChucNangCuaChungTu.MaxLength = 1;
                //ucLoaiGiayPhep.MaxLength = 4;
                txtTenThuongNhanXuatKhau.MaxLength = 70;
                txtMaBuuChinhXuatKhau.MaxLength = 9;
                txtSoNhaTenDuongXuatKhau.MaxLength = 35;
                txtPhuongXaXuatKhau.MaxLength = 35;
                txtQuanHuyenXuatKhau.MaxLength = 35;
                txtTinhThanhPhoXuatKhau.MaxLength = 35;
                //txtMaQuocGiaXuatKhau.MaxLength = 2;
                txtSoDienThoaiXuatKhau.MaxLength = 20;
                txtSoFaxXuatKhau.MaxLength = 20;
                txtEmailXuatKhau.MaxLength = 210;
                txtSoHopDong.MaxLength = 35;
                txtSoVanDon.MaxLength = 35;
                //txtMaBenDi.MaxLength = 5;
                //txtTenBenDi.MaxLength = 35;
                txtMaThuongNhanNhapKhau.MaxLength = 13;
                txtTenThuongNhanNhapKhau.MaxLength = 300;
                txtMaBuuChinhNhapKhau.MaxLength = 7;
                txtSoNhaTenDuongNhapKhau.MaxLength = 300;
                //txtMaQuocGiaNhapKhau.MaxLength = 2;
                txtSoDienThoaiNhapKhau.MaxLength = 20;
                txtSoFaxNhapKhau.MaxLength = 20;
                txtEmailNhapKhau.MaxLength = 210;
                //txtMaBenDen.MaxLength = 6;
                //txtTenBenDen.MaxLength = 35;
                //txtThoiGianNhapKhauDuKien.MaxLength = 8;
                txtGiaTriHangHoa.MaxLength = 19;
                //txtDonViTienTe.MaxLength = 3;
                txtDiaDiemTapKetHangHoa.MaxLength = 70;
                //txtThoiGianKiemTra.MaxLength = 8;
                txtDiaDiemKiemTra.MaxLength = 70;
                txtHoSoLienQuan.MaxLength = 750;
                txtGhiChu.MaxLength = 996;
                txtDaiDienThuongNhanNhapKhau.MaxLength = 300;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        #region Send VNACCS

        /// <summary>
        /// Khai báo thông tin
        /// </summary>
        /// <param name="KhaiBaoSua"></param>
        private void SendVnaccsIDC(bool KhaiBaoSua)
        {
            try
            {
                if (GiayPhep.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }

                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến Hải quan? ", true) == "Yes")
                {
                    GiayPhep.InputMessageID = HelperVNACCS.NewInputMSGID(); //Tạo mới GUID ID
                    GiayPhep.InsertUpdateFull(); //Lưu thông tin GUID ID vừa tạo

                    MessagesSend msg; //Form khai báo
                    SFA seaObj = VNACCMaperFromObject.SFAMapper(GiayPhep); //Set Mapper
                    if (seaObj == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    msg = MessagesSend.Load<SFA>(seaObj, GiayPhep.InputMessageID);

                    MsgLog.SaveMessages(msg, GiayPhep.ID, EnumThongBao.SendMess, ""); //Lưu thông tin trước khai báo
                    SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                    f.isSend = true; //Có khai báo
                    f.isRep = true; //Có nhận phản hồi
                    f.inputMSGID = GiayPhep.InputMessageID;
                    f.ShowDialog(this);
                    if (f.result) //Có kêt quả trả về
                    {
                        string ketqua = "Khai báo thông tin thành công";

                        if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                        {
                            try
                            {
                                decimal soTiepNhan = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(16, 12));
                                ketqua = ketqua + Environment.NewLine;
                                ketqua += "Số đơn xin cấp phép: " + soTiepNhan;

                                GiayPhep.SoDonXinCapPhep = soTiepNhan;
                            }
                            catch (System.Exception ex)
                            {
                                ketqua += Environment.NewLine + "Lỗi cập nhật số giấp phép: " + Environment.NewLine + ex.Message;
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }

                        TuDongCapNhatThongTin();
                    }
                    else if (f.DialogResult == DialogResult.Cancel)
                    {
                        ShowMessage(f.msgFeedBack, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        #endregion

        #region Cập nhật thông tin

        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            ProcessMessages.GetDataResult_GiayPhep(msgResult, "", GiayPhep);
            GiayPhep.InsertUpdateFull();
            SetGiayPhep();
            //setCommandStatus();
        }

        private void TuDongCapNhatThongTin()
        {
            if (GiayPhep != null && GiayPhep.SoDonXinCapPhep > 0 && GiayPhep.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan = '{0}' And MessagesInputID = '{1}'", GiayPhep.SoDonXinCapPhep.ToString(), GiayPhep.InputMessageID), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }
            }
        }

        #endregion

        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            txtMaNguoiKhai.TextChanged += new EventHandler(SetTextChanged_Handler);
            ucMaDonViCapPhep.TextChanged += new EventHandler(SetTextChanged_Handler);
            ucLoaiGiayPhep.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTenThuongNhanXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaBuuChinhXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoNhaTenDuongXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtPhuongXaXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtQuanHuyenXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtTinhThanhPhoXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucMaQuocGiaXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoDienThoaiXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoFaxXuatKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoHopDong.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoVanDon.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucBenDi.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucBenDi.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaThuongNhanNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtMaBuuChinhNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            //ucMaQuocGiaNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoDienThoaiNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtSoFaxNhapKhau.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtMaBenDen.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtTenBenDen.TextChanged += new EventHandler(SetTextChanged_Handler);
            //txtDonViTienTe.TextChanged += new EventHandler(SetTextChanged_Handler);            
            txtDiaDiemTapKetHangHoa.TextChanged += new EventHandler(SetTextChanged_Handler);
            txtDiaDiemKiemTra.TextChanged += new EventHandler(SetTextChanged_Handler);

            txtMaNguoiKhai.CharacterCasing = CharacterCasing.Upper;
            ucMaDonViCapPhep.IsUpperCase = true;
            ucLoaiGiayPhep.IsUpperCase = true;
            txtTenThuongNhanXuatKhau.CharacterCasing = CharacterCasing.Upper;
            txtMaBuuChinhXuatKhau.CharacterCasing = CharacterCasing.Upper;
            txtSoNhaTenDuongXuatKhau.CharacterCasing = CharacterCasing.Upper;
            txtPhuongXaXuatKhau.CharacterCasing = CharacterCasing.Upper;
            txtQuanHuyenXuatKhau.CharacterCasing = CharacterCasing.Upper;
            txtTinhThanhPhoXuatKhau.CharacterCasing = CharacterCasing.Upper;
            ucMaQuocGiaXuatKhau.IsUpperCase = true;
            txtSoDienThoaiXuatKhau.CharacterCasing = CharacterCasing.Upper;
            txtSoFaxXuatKhau.CharacterCasing = CharacterCasing.Upper;
            txtSoHopDong.CharacterCasing = CharacterCasing.Upper;
            txtSoVanDon.CharacterCasing = CharacterCasing.Upper;
            ucBenDi.IsUpperCase = true;
            //txtTenBenDi.CharacterCasing = CharacterCasing.Upper;
            txtMaThuongNhanNhapKhau.CharacterCasing = CharacterCasing.Upper;
            txtMaBuuChinhNhapKhau.CharacterCasing = CharacterCasing.Upper;
            ucMaQuocGiaNhapKhau.IsUpperCase = true;
            txtSoDienThoaiNhapKhau.CharacterCasing = CharacterCasing.Upper;
            txtSoFaxNhapKhau.CharacterCasing = CharacterCasing.Upper;
            ucBenDen.IsUpperCase = true;
            //txtTenBenDen.CharacterCasing = CharacterCasing.Upper;
            //ucDonViTienTe.CharacterCasing = CharacterCasing.Upper;
            txtDiaDiemTapKetHangHoa.CharacterCasing = CharacterCasing.Upper;
            txtDiaDiemKiemTra.CharacterCasing = CharacterCasing.Upper;
        }

    }
}
