﻿using System;
using System.Drawing;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT.GC;
using Company.Interface.GC;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using System.Data;
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class SelectHangMauDichFormGCCT : BaseForm
    {
        //-----------------------------------------------------------------------------------------

        public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT;
        public List<HangChuyenTiep> HCTCollection = new List<HangChuyenTiep>();
        //-----------------------------------------------------------------------------------------

        public SelectHangMauDichFormGCCT()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["ID_NuocXX"].Text = this.Nuoc_GetName(e.Row.Cells["ID_NuocXX"].Value.ToString());
            e.Row.Cells["ID_DVT"].Text = this.DonViTinh_GetName(e.Row.Cells["ID_DVT"].Value);
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool checkHangExit(long HMD_ID)
        {
            foreach (HangChuyenTiep hct in HCTCollection)
            {
                if (hct.ID == HMD_ID) return true;
            }
            return false;
        }

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = TKCT.HCTCollection;
            if (TKCT.TrangThaiXuLy != -1)
            {
                //this.btnChonAll.Enabled = false;
                //this.btnChonNhieuHang.Enabled = false;
            }
        }

        private void btnChonAll_Click(object sender, EventArgs e)
        {
            GridEXRow[] items = dgList.GetRows();
            if (dgList.GetRows().Length < 0) return;
            if (items.Length <= 0) return;
            {
                foreach (GridEXRow i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangChuyenTiep HCT = (HangChuyenTiep)i.DataRow;
                        if (!checkHangExit(HCT.ID))
                            HCTCollection.Add(HCT);
                    }
                }
            }
            this.Close();
        }

        private void btnChonNhieuHang_Click(object sender, EventArgs e)
        {
            GridEXRow[] items = dgList.GetCheckedRows();
            if (dgList.GetRows().Length < 0) return;
            if (items.Length <= 0) return;
            {
                foreach (GridEXRow item in items)
                {
                    if (item.IsChecked)
                    {
                        HangChuyenTiep HCT = (HangChuyenTiep)item.DataRow;
                        if (!checkHangExit(HCT.ID))
                            HCTCollection.Add(HCT);
                    }
                }
            }
            this.Close();
        }




    }
}
