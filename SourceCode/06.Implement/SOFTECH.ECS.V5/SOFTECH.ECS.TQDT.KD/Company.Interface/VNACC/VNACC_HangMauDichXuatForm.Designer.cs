﻿namespace Company.Interface
{
    partial class VNACC_HangMauDichXuatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_HangMauDichXuatForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.txtDieuKhoanMienGiam = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtMaMienGiamThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.ctrMaTTTuyetDoi = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPhanLoai_TS_ThueXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDVTTuyetDoi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueSuatTuyetDoi = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtThueSuat = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ctrMaTTCuaDonGiaTinhThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTTriGiaTinhThueS = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTMaMienGiam = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTSoTienThueXuatKhau = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaTTTriGiaTinhThue = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDonGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienMienGiam = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTriGiaTinhThueS = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTriGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDV_SL_TrongDonGiaTinhThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoDMMienThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtSoDongDMMienThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.editBox2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ucCategory6 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.numericEditBox2 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox6 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label25 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.editBox1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ucCategory5 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label27 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.numericEditBox1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox5 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label23 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.txtMaVanBan5 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaVanBan4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaVanBan3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaVanBan2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaVanBan1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ctrMaTTDonGia = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrDVTLuong2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrDVTLuong1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ctrMaSoHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtMaQuanLy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTriGiaHoaDon = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDonGiaHoaDon = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuong2 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuong1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSoTTDongHangTKTNTX = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDVTDonGia = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 614), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 614);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 590);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 590);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(776, 614);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.grList);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 412);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(776, 202);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Danh sách hàng";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // grList
            // 
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.FrozenColumns = 4;
            this.grList.GroupByBoxVisible = false;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Location = new System.Drawing.Point(0, 17);
            this.grList.Name = "grList";
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.Size = new System.Drawing.Size(776, 182);
            this.grList.TabIndex = 0;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.DoubleClick += new System.EventHandler(this.grList_DoubleClick);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox2.Controls.Add(this.txtMaVanBan5);
            this.uiGroupBox2.Controls.Add(this.txtMaVanBan4);
            this.uiGroupBox2.Controls.Add(this.txtMaVanBan3);
            this.uiGroupBox2.Controls.Add(this.txtMaVanBan2);
            this.uiGroupBox2.Controls.Add(this.txtMaVanBan1);
            this.uiGroupBox2.Controls.Add(this.ctrMaTTDonGia);
            this.uiGroupBox2.Controls.Add(this.ctrDVTLuong2);
            this.uiGroupBox2.Controls.Add(this.ctrDVTLuong1);
            this.uiGroupBox2.Controls.Add(this.ctrMaSoHang);
            this.uiGroupBox2.Controls.Add(this.txtMaQuanLy);
            this.uiGroupBox2.Controls.Add(this.txtTriGiaHoaDon);
            this.uiGroupBox2.Controls.Add(this.txtDonGiaHoaDon);
            this.uiGroupBox2.Controls.Add(this.txtSoLuong2);
            this.uiGroupBox2.Controls.Add(this.txtSoLuong1);
            this.uiGroupBox2.Controls.Add(this.txtTenHang);
            this.uiGroupBox2.Controls.Add(this.label19);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.txtSoTTDongHangTKTNTX);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.txtDVTDonGia);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(776, 412);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Controls.Add(this.uiTab1);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 153);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(770, 224);
            this.uiGroupBox4.TabIndex = 41;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(3, 8);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(764, 213);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2});
            this.uiTab1.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.txtDieuKhoanMienGiam);
            this.uiTabPage1.Controls.Add(this.label30);
            this.uiTabPage1.Controls.Add(this.txtMaMienGiamThue);
            this.uiTabPage1.Controls.Add(this.label20);
            this.uiTabPage1.Controls.Add(this.ctrMaTTTuyetDoi);
            this.uiTabPage1.Controls.Add(this.label12);
            this.uiTabPage1.Controls.Add(this.txtPhanLoai_TS_ThueXuatKhau);
            this.uiTabPage1.Controls.Add(this.txtMaDVTTuyetDoi);
            this.uiTabPage1.Controls.Add(this.txtThueSuatTuyetDoi);
            this.uiTabPage1.Controls.Add(this.txtThueSuat);
            this.uiTabPage1.Controls.Add(this.ctrMaTTCuaDonGiaTinhThue);
            this.uiTabPage1.Controls.Add(this.ctrMaTTTriGiaTinhThueS);
            this.uiTabPage1.Controls.Add(this.ctrMaTTMaMienGiam);
            this.uiTabPage1.Controls.Add(this.ctrMaTTSoTienThueXuatKhau);
            this.uiTabPage1.Controls.Add(this.ctrMaTTTriGiaTinhThue);
            this.uiTabPage1.Controls.Add(this.label15);
            this.uiTabPage1.Controls.Add(this.label13);
            this.uiTabPage1.Controls.Add(this.txtDonGiaTinhThue);
            this.uiTabPage1.Controls.Add(this.txtSoTienMienGiam);
            this.uiTabPage1.Controls.Add(this.label14);
            this.uiTabPage1.Controls.Add(this.label17);
            this.uiTabPage1.Controls.Add(this.label10);
            this.uiTabPage1.Controls.Add(this.txtTriGiaTinhThueS);
            this.uiTabPage1.Controls.Add(this.txtSoTienThue);
            this.uiTabPage1.Controls.Add(this.label26);
            this.uiTabPage1.Controls.Add(this.label22);
            this.uiTabPage1.Controls.Add(this.label18);
            this.uiTabPage1.Controls.Add(this.txtTriGiaTinhThue);
            this.uiTabPage1.Controls.Add(this.txtDV_SL_TrongDonGiaTinhThue);
            this.uiTabPage1.Controls.Add(this.txtSoDMMienThue);
            this.uiTabPage1.Controls.Add(this.label21);
            this.uiTabPage1.Controls.Add(this.txtSoDongDMMienThue);
            this.uiTabPage1.Controls.Add(this.label16);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(762, 191);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Thuế xuất khẩu";
            // 
            // txtDieuKhoanMienGiam
            // 
            this.txtDieuKhoanMienGiam.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDieuKhoanMienGiam.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDieuKhoanMienGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDieuKhoanMienGiam.Location = new System.Drawing.Point(328, 164);
            this.txtDieuKhoanMienGiam.MaxLength = 12;
            this.txtDieuKhoanMienGiam.Name = "txtDieuKhoanMienGiam";
            this.txtDieuKhoanMienGiam.ReadOnly = true;
            this.txtDieuKhoanMienGiam.Size = new System.Drawing.Size(390, 21);
            this.txtDieuKhoanMienGiam.TabIndex = 53;
            this.txtDieuKhoanMienGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDieuKhoanMienGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(7, 168);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(315, 13);
            this.label30.TabIndex = 54;
            this.label30.Text = "Điều khoản miễn / Giảm / Không chịu thuế nhập khẩu (Pháp luật)";
            // 
            // txtMaMienGiamThue
            // 
            this.txtMaMienGiamThue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaMienGiamThue.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaMienGiamThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaMienGiamThue.Location = new System.Drawing.Point(224, 140);
            this.txtMaMienGiamThue.MaxLength = 12;
            this.txtMaMienGiamThue.Name = "txtMaMienGiamThue";
            this.txtMaMienGiamThue.Size = new System.Drawing.Size(77, 21);
            this.txtMaMienGiamThue.TabIndex = 9;
            this.txtMaMienGiamThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaMienGiamThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(7, 144);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(211, 13);
            this.label20.TabIndex = 29;
            this.label20.Text = "Mã miễn/ Giảm/ Không chịu thuế xuất khẩu";
            // 
            // ctrMaTTTuyetDoi
            // 
            this.ctrMaTTTuyetDoi.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTTuyetDoi.Code = "";
            this.ctrMaTTTuyetDoi.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTTuyetDoi.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTTuyetDoi.IsValidate = false;
            this.ctrMaTTTuyetDoi.Location = new System.Drawing.Point(422, 5);
            this.ctrMaTTTuyetDoi.Name = "ctrMaTTTuyetDoi";
            this.ctrMaTTTuyetDoi.Name_VN = "";
            this.ctrMaTTTuyetDoi.ShowColumnCode = true;
            this.ctrMaTTTuyetDoi.ShowColumnName = false;
            this.ctrMaTTTuyetDoi.Size = new System.Drawing.Size(79, 26);
            this.ctrMaTTTuyetDoi.TabIndex = 3;
            this.ctrMaTTTuyetDoi.TagName = "";
            this.ctrMaTTTuyetDoi.WhereCondition = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(143, 12);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Mức thuế tuyệt đối";
            // 
            // txtPhanLoai_TS_ThueXuatKhau
            // 
            this.txtPhanLoai_TS_ThueXuatKhau.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtPhanLoai_TS_ThueXuatKhau.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtPhanLoai_TS_ThueXuatKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhanLoai_TS_ThueXuatKhau.Location = new System.Drawing.Point(476, 117);
            this.txtPhanLoai_TS_ThueXuatKhau.MaxLength = 12;
            this.txtPhanLoai_TS_ThueXuatKhau.Name = "txtPhanLoai_TS_ThueXuatKhau";
            this.txtPhanLoai_TS_ThueXuatKhau.ReadOnly = true;
            this.txtPhanLoai_TS_ThueXuatKhau.Size = new System.Drawing.Size(159, 21);
            this.txtPhanLoai_TS_ThueXuatKhau.TabIndex = 20;
            this.txtPhanLoai_TS_ThueXuatKhau.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPhanLoai_TS_ThueXuatKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDVTTuyetDoi
            // 
            this.txtMaDVTTuyetDoi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaDVTTuyetDoi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaDVTTuyetDoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDVTTuyetDoi.Location = new System.Drawing.Point(382, 8);
            this.txtMaDVTTuyetDoi.MaxLength = 12;
            this.txtMaDVTTuyetDoi.Name = "txtMaDVTTuyetDoi";
            this.txtMaDVTTuyetDoi.Size = new System.Drawing.Size(36, 21);
            this.txtMaDVTTuyetDoi.TabIndex = 2;
            this.txtMaDVTTuyetDoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDVTTuyetDoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueSuatTuyetDoi
            // 
            this.txtThueSuatTuyetDoi.DecimalDigits = 20;
            this.txtThueSuatTuyetDoi.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThueSuatTuyetDoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuatTuyetDoi.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtThueSuatTuyetDoi.Location = new System.Drawing.Point(247, 8);
            this.txtThueSuatTuyetDoi.MaxLength = 15;
            this.txtThueSuatTuyetDoi.Name = "txtThueSuatTuyetDoi";
            this.txtThueSuatTuyetDoi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThueSuatTuyetDoi.Size = new System.Drawing.Size(132, 21);
            this.txtThueSuatTuyetDoi.TabIndex = 1;
            this.txtThueSuatTuyetDoi.Text = "0";
            this.txtThueSuatTuyetDoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThueSuatTuyetDoi.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtThueSuatTuyetDoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueSuat
            // 
            this.txtThueSuat.DecimalDigits = 20;
            this.txtThueSuat.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtThueSuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuat.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtThueSuat.Location = new System.Drawing.Point(70, 8);
            this.txtThueSuat.MaxLength = 15;
            this.txtThueSuat.Name = "txtThueSuat";
            this.txtThueSuat.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThueSuat.Size = new System.Drawing.Size(52, 21);
            this.txtThueSuat.TabIndex = 0;
            this.txtThueSuat.Text = "0";
            this.txtThueSuat.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThueSuat.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtThueSuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ctrMaTTCuaDonGiaTinhThue
            // 
            this.ctrMaTTCuaDonGiaTinhThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTCuaDonGiaTinhThue.Code = "";
            this.ctrMaTTCuaDonGiaTinhThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTCuaDonGiaTinhThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTCuaDonGiaTinhThue.IsValidate = false;
            this.ctrMaTTCuaDonGiaTinhThue.Location = new System.Drawing.Point(700, 61);
            this.ctrMaTTCuaDonGiaTinhThue.Name = "ctrMaTTCuaDonGiaTinhThue";
            this.ctrMaTTCuaDonGiaTinhThue.Name_VN = "";
            this.ctrMaTTCuaDonGiaTinhThue.ShowColumnCode = true;
            this.ctrMaTTCuaDonGiaTinhThue.ShowColumnName = false;
            this.ctrMaTTCuaDonGiaTinhThue.Size = new System.Drawing.Size(64, 26);
            this.ctrMaTTCuaDonGiaTinhThue.TabIndex = 14;
            this.ctrMaTTCuaDonGiaTinhThue.TagName = "";
            this.ctrMaTTCuaDonGiaTinhThue.WhereCondition = "";
            // 
            // ctrMaTTTriGiaTinhThueS
            // 
            this.ctrMaTTTriGiaTinhThueS.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTTriGiaTinhThueS.Code = "";
            this.ctrMaTTTriGiaTinhThueS.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTTriGiaTinhThueS.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTTriGiaTinhThueS.IsValidate = false;
            this.ctrMaTTTriGiaTinhThueS.Location = new System.Drawing.Point(640, 33);
            this.ctrMaTTTriGiaTinhThueS.Name = "ctrMaTTTriGiaTinhThueS";
            this.ctrMaTTTriGiaTinhThueS.Name_VN = "";
            this.ctrMaTTTriGiaTinhThueS.ShowColumnCode = true;
            this.ctrMaTTTriGiaTinhThueS.ShowColumnName = false;
            this.ctrMaTTTriGiaTinhThueS.Size = new System.Drawing.Size(79, 26);
            this.ctrMaTTTriGiaTinhThueS.TabIndex = 11;
            this.ctrMaTTTriGiaTinhThueS.TagName = "";
            this.ctrMaTTTriGiaTinhThueS.WhereCondition = "";
            // 
            // ctrMaTTMaMienGiam
            // 
            this.ctrMaTTMaMienGiam.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTMaMienGiam.Code = "";
            this.ctrMaTTMaMienGiam.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTMaMienGiam.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTMaMienGiam.IsValidate = false;
            this.ctrMaTTMaMienGiam.Location = new System.Drawing.Point(278, 61);
            this.ctrMaTTMaMienGiam.Name = "ctrMaTTMaMienGiam";
            this.ctrMaTTMaMienGiam.Name_VN = "";
            this.ctrMaTTMaMienGiam.ShowColumnCode = true;
            this.ctrMaTTMaMienGiam.ShowColumnName = false;
            this.ctrMaTTMaMienGiam.Size = new System.Drawing.Size(78, 26);
            this.ctrMaTTMaMienGiam.TabIndex = 18;
            this.ctrMaTTMaMienGiam.TagName = "";
            this.ctrMaTTMaMienGiam.WhereCondition = "";
            // 
            // ctrMaTTSoTienThueXuatKhau
            // 
            this.ctrMaTTSoTienThueXuatKhau.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTSoTienThueXuatKhau.Code = "";
            this.ctrMaTTSoTienThueXuatKhau.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTSoTienThueXuatKhau.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTSoTienThueXuatKhau.IsValidate = false;
            this.ctrMaTTSoTienThueXuatKhau.Location = new System.Drawing.Point(640, 89);
            this.ctrMaTTSoTienThueXuatKhau.Name = "ctrMaTTSoTienThueXuatKhau";
            this.ctrMaTTSoTienThueXuatKhau.Name_VN = "";
            this.ctrMaTTSoTienThueXuatKhau.ShowColumnCode = true;
            this.ctrMaTTSoTienThueXuatKhau.ShowColumnName = false;
            this.ctrMaTTSoTienThueXuatKhau.Size = new System.Drawing.Size(79, 26);
            this.ctrMaTTSoTienThueXuatKhau.TabIndex = 16;
            this.ctrMaTTSoTienThueXuatKhau.TagName = "";
            this.ctrMaTTSoTienThueXuatKhau.WhereCondition = "";
            // 
            // ctrMaTTTriGiaTinhThue
            // 
            this.ctrMaTTTriGiaTinhThue.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTTriGiaTinhThue.Code = "";
            this.ctrMaTTTriGiaTinhThue.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTTriGiaTinhThue.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTTriGiaTinhThue.IsValidate = false;
            this.ctrMaTTTriGiaTinhThue.Location = new System.Drawing.Point(277, 32);
            this.ctrMaTTTriGiaTinhThue.Name = "ctrMaTTTriGiaTinhThue";
            this.ctrMaTTTriGiaTinhThue.Name_VN = "";
            this.ctrMaTTTriGiaTinhThue.ShowColumnCode = true;
            this.ctrMaTTTriGiaTinhThue.ShowColumnName = false;
            this.ctrMaTTTriGiaTinhThue.Size = new System.Drawing.Size(79, 26);
            this.ctrMaTTTriGiaTinhThue.TabIndex = 5;
            this.ctrMaTTTriGiaTinhThue.TagName = "";
            this.ctrMaTTTriGiaTinhThue.WhereCondition = "";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(7, 11);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "Thuế suất";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(125, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "%";
            // 
            // txtDonGiaTinhThue
            // 
            this.txtDonGiaTinhThue.DecimalDigits = 20;
            this.txtDonGiaTinhThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDonGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaTinhThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDonGiaTinhThue.Location = new System.Drawing.Point(476, 64);
            this.txtDonGiaTinhThue.MaxLength = 15;
            this.txtDonGiaTinhThue.Name = "txtDonGiaTinhThue";
            this.txtDonGiaTinhThue.ReadOnly = true;
            this.txtDonGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDonGiaTinhThue.Size = new System.Drawing.Size(159, 21);
            this.txtDonGiaTinhThue.TabIndex = 12;
            this.txtDonGiaTinhThue.Text = "0";
            this.txtDonGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDonGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDonGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTienMienGiam
            // 
            this.txtSoTienMienGiam.DecimalDigits = 20;
            this.txtSoTienMienGiam.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienMienGiam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienMienGiam.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTienMienGiam.Location = new System.Drawing.Point(146, 64);
            this.txtSoTienMienGiam.MaxLength = 15;
            this.txtSoTienMienGiam.Name = "txtSoTienMienGiam";
            this.txtSoTienMienGiam.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienMienGiam.Size = new System.Drawing.Size(126, 21);
            this.txtSoTienMienGiam.TabIndex = 6;
            this.txtSoTienMienGiam.Text = "0";
            this.txtSoTienMienGiam.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienMienGiam.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienMienGiam.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(7, 69);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(141, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "Số tiền giảm thuế xuất khẩu";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(372, 41);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(96, 13);
            this.label17.TabIndex = 27;
            this.label17.Text = "Trị giá tính thuế(S)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(7, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "Trị giá tính thuế(M)";
            // 
            // txtTriGiaTinhThueS
            // 
            this.txtTriGiaTinhThueS.DecimalDigits = 20;
            this.txtTriGiaTinhThueS.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaTinhThueS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThueS.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTriGiaTinhThueS.Location = new System.Drawing.Point(476, 36);
            this.txtTriGiaTinhThueS.MaxLength = 15;
            this.txtTriGiaTinhThueS.Name = "txtTriGiaTinhThueS";
            this.txtTriGiaTinhThueS.ReadOnly = true;
            this.txtTriGiaTinhThueS.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThueS.Size = new System.Drawing.Size(159, 21);
            this.txtTriGiaTinhThueS.TabIndex = 10;
            this.txtTriGiaTinhThueS.Text = "0";
            this.txtTriGiaTinhThueS.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThueS.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaTinhThueS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTienThue
            // 
            this.txtSoTienThue.DecimalDigits = 20;
            this.txtSoTienThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTienThue.Location = new System.Drawing.Point(476, 92);
            this.txtSoTienThue.MaxLength = 15;
            this.txtSoTienThue.Name = "txtSoTienThue";
            this.txtSoTienThue.ReadOnly = true;
            this.txtSoTienThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienThue.Size = new System.Drawing.Size(159, 21);
            this.txtSoTienThue.TabIndex = 15;
            this.txtSoTienThue.Text = "0";
            this.txtSoTienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(372, 124);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(100, 13);
            this.label26.TabIndex = 22;
            this.label26.Text = "Phân loại thuế xuất";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(372, 97);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 13);
            this.label22.TabIndex = 22;
            this.label22.Text = "Số tiền thuế";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(372, 68);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(90, 13);
            this.label18.TabIndex = 22;
            this.label18.Text = "Đơn giá tính thuế";
            // 
            // txtTriGiaTinhThue
            // 
            this.txtTriGiaTinhThue.DecimalDigits = 20;
            this.txtTriGiaTinhThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtTriGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtTriGiaTinhThue.Location = new System.Drawing.Point(113, 35);
            this.txtTriGiaTinhThue.MaxLength = 15;
            this.txtTriGiaTinhThue.Name = "txtTriGiaTinhThue";
            this.txtTriGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaTinhThue.Size = new System.Drawing.Size(159, 21);
            this.txtTriGiaTinhThue.TabIndex = 4;
            this.txtTriGiaTinhThue.Text = "0";
            this.txtTriGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaTinhThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDV_SL_TrongDonGiaTinhThue
            // 
            this.txtDV_SL_TrongDonGiaTinhThue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDV_SL_TrongDonGiaTinhThue.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDV_SL_TrongDonGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDV_SL_TrongDonGiaTinhThue.Location = new System.Drawing.Point(640, 64);
            this.txtDV_SL_TrongDonGiaTinhThue.MaxLength = 12;
            this.txtDV_SL_TrongDonGiaTinhThue.Name = "txtDV_SL_TrongDonGiaTinhThue";
            this.txtDV_SL_TrongDonGiaTinhThue.ReadOnly = true;
            this.txtDV_SL_TrongDonGiaTinhThue.Size = new System.Drawing.Size(56, 21);
            this.txtDV_SL_TrongDonGiaTinhThue.TabIndex = 13;
            this.txtDV_SL_TrongDonGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDV_SL_TrongDonGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoDMMienThue
            // 
            this.txtSoDMMienThue.DecimalDigits = 20;
            this.txtSoDMMienThue.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoDMMienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDMMienThue.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoDMMienThue.Location = new System.Drawing.Point(175, 89);
            this.txtSoDMMienThue.MaxLength = 15;
            this.txtSoDMMienThue.Name = "txtSoDMMienThue";
            this.txtSoDMMienThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoDMMienThue.Size = new System.Drawing.Size(126, 21);
            this.txtSoDMMienThue.TabIndex = 7;
            this.txtSoDMMienThue.Text = "0";
            this.txtSoDMMienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDMMienThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoDMMienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(7, 93);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(169, 13);
            this.label21.TabIndex = 27;
            this.label21.Text = "Số danh mục miễn thuế xuất khẩu";
            // 
            // txtSoDongDMMienThue
            // 
            this.txtSoDongDMMienThue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoDongDMMienThue.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoDongDMMienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDongDMMienThue.Location = new System.Drawing.Point(247, 113);
            this.txtSoDongDMMienThue.MaxLength = 12;
            this.txtSoDongDMMienThue.Name = "txtSoDongDMMienThue";
            this.txtSoDongDMMienThue.Size = new System.Drawing.Size(54, 21);
            this.txtSoDongDMMienThue.TabIndex = 8;
            this.txtSoDongDMMienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoDongDMMienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(7, 117);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(228, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "Số dòng tương ứng trong danh mục miễn thuế";
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.uiGroupBox6);
            this.uiTabPage2.Controls.Add(this.uiGroupBox5);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(762, 191);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Tiền lệ phí và bảo hiểm";
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.editBox2);
            this.uiGroupBox6.Controls.Add(this.ucCategory6);
            this.uiGroupBox6.Controls.Add(this.label29);
            this.uiGroupBox6.Controls.Add(this.label28);
            this.uiGroupBox6.Controls.Add(this.numericEditBox2);
            this.uiGroupBox6.Controls.Add(this.numericEditBox6);
            this.uiGroupBox6.Controls.Add(this.label25);
            this.uiGroupBox6.Location = new System.Drawing.Point(323, 3);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(300, 117);
            this.uiGroupBox6.TabIndex = 20;
            this.uiGroupBox6.Text = "Tiền bảo hiểm";
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // editBox2
            // 
            this.editBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.editBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.editBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editBox2.Location = new System.Drawing.Point(95, 19);
            this.editBox2.MaxLength = 12;
            this.editBox2.Name = "editBox2";
            this.editBox2.Size = new System.Drawing.Size(159, 21);
            this.editBox2.TabIndex = 0;
            this.editBox2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.editBox2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ucCategory6
            // 
            this.ucCategory6.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucCategory6.Code = "";
            this.ucCategory6.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategory6.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategory6.IsValidate = false;
            this.ucCategory6.Location = new System.Drawing.Point(201, 46);
            this.ucCategory6.Name = "ucCategory6";
            this.ucCategory6.Name_VN = "";
            this.ucCategory6.ShowColumnCode = true;
            this.ucCategory6.ShowColumnName = false;
            this.ucCategory6.Size = new System.Drawing.Size(79, 26);
            this.ucCategory6.TabIndex = 10;
            this.ucCategory6.TagName = "";
            this.ucCategory6.WhereCondition = "";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(6, 23);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(44, 13);
            this.label29.TabIndex = 18;
            this.label29.Text = "Đơn giá";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(6, 84);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(58, 13);
            this.label28.TabIndex = 18;
            this.label28.Text = "Khoản tiền";
            // 
            // numericEditBox2
            // 
            this.numericEditBox2.DecimalDigits = 20;
            this.numericEditBox2.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.numericEditBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericEditBox2.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.numericEditBox2.Location = new System.Drawing.Point(95, 80);
            this.numericEditBox2.MaxLength = 15;
            this.numericEditBox2.Name = "numericEditBox2";
            this.numericEditBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericEditBox2.Size = new System.Drawing.Size(185, 21);
            this.numericEditBox2.TabIndex = 7;
            this.numericEditBox2.Text = "0";
            this.numericEditBox2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numericEditBox2.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericEditBox2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // numericEditBox6
            // 
            this.numericEditBox6.DecimalDigits = 20;
            this.numericEditBox6.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.numericEditBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericEditBox6.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.numericEditBox6.Location = new System.Drawing.Point(95, 49);
            this.numericEditBox6.MaxLength = 15;
            this.numericEditBox6.Name = "numericEditBox6";
            this.numericEditBox6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericEditBox6.Size = new System.Drawing.Size(100, 21);
            this.numericEditBox6.TabIndex = 9;
            this.numericEditBox6.Text = "0";
            this.numericEditBox6.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numericEditBox6.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericEditBox6.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(6, 53);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(49, 13);
            this.label25.TabIndex = 19;
            this.label25.Text = "Số lượng";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.editBox1);
            this.uiGroupBox5.Controls.Add(this.ucCategory5);
            this.uiGroupBox5.Controls.Add(this.label27);
            this.uiGroupBox5.Controls.Add(this.label24);
            this.uiGroupBox5.Controls.Add(this.numericEditBox1);
            this.uiGroupBox5.Controls.Add(this.numericEditBox5);
            this.uiGroupBox5.Controls.Add(this.label23);
            this.uiGroupBox5.Location = new System.Drawing.Point(28, 3);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(274, 117);
            this.uiGroupBox5.TabIndex = 20;
            this.uiGroupBox5.Text = "Tiền lệ phí";
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // editBox1
            // 
            this.editBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.editBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.editBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editBox1.Location = new System.Drawing.Point(84, 19);
            this.editBox1.MaxLength = 12;
            this.editBox1.Name = "editBox1";
            this.editBox1.Size = new System.Drawing.Size(185, 21);
            this.editBox1.TabIndex = 0;
            this.editBox1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.editBox1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ucCategory5
            // 
            this.ucCategory5.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucCategory5.Code = "";
            this.ucCategory5.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategory5.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategory5.IsValidate = false;
            this.ucCategory5.Location = new System.Drawing.Point(190, 46);
            this.ucCategory5.Name = "ucCategory5";
            this.ucCategory5.Name_VN = "";
            this.ucCategory5.ShowColumnCode = true;
            this.ucCategory5.ShowColumnName = true;
            this.ucCategory5.Size = new System.Drawing.Size(79, 26);
            this.ucCategory5.TabIndex = 8;
            this.ucCategory5.TagName = "";
            this.ucCategory5.WhereCondition = "";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(6, 23);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(44, 13);
            this.label27.TabIndex = 18;
            this.label27.Text = "Đơn giá";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(6, 84);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(58, 13);
            this.label24.TabIndex = 18;
            this.label24.Text = "Khoản tiền";
            // 
            // numericEditBox1
            // 
            this.numericEditBox1.DecimalDigits = 20;
            this.numericEditBox1.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.numericEditBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericEditBox1.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.numericEditBox1.Location = new System.Drawing.Point(84, 80);
            this.numericEditBox1.MaxLength = 15;
            this.numericEditBox1.Name = "numericEditBox1";
            this.numericEditBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericEditBox1.Size = new System.Drawing.Size(185, 21);
            this.numericEditBox1.TabIndex = 7;
            this.numericEditBox1.Text = "0";
            this.numericEditBox1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numericEditBox1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericEditBox1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // numericEditBox5
            // 
            this.numericEditBox5.DecimalDigits = 20;
            this.numericEditBox5.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.numericEditBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericEditBox5.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.numericEditBox5.Location = new System.Drawing.Point(84, 49);
            this.numericEditBox5.MaxLength = 15;
            this.numericEditBox5.Name = "numericEditBox5";
            this.numericEditBox5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericEditBox5.Size = new System.Drawing.Size(100, 21);
            this.numericEditBox5.TabIndex = 7;
            this.numericEditBox5.Text = "0";
            this.numericEditBox5.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numericEditBox5.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericEditBox5.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(6, 53);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(49, 13);
            this.label23.TabIndex = 19;
            this.label23.Text = "Số lượng";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Controls.Add(this.btnXoa);
            this.uiGroupBox3.Controls.Add(this.btnGhi);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox3.Location = new System.Drawing.Point(3, 377);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(770, 32);
            this.uiGroupBox3.TabIndex = 40;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(697, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 23);
            this.btnClose.TabIndex = 25;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(614, 6);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(70, 23);
            this.btnXoa.TabIndex = 24;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.ImageIndex = 4;
            this.btnGhi.Location = new System.Drawing.Point(533, 6);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(70, 23);
            this.btnGhi.TabIndex = 23;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // txtMaVanBan5
            // 
            this.txtMaVanBan5.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaVanBan5.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaVanBan5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaVanBan5.Location = new System.Drawing.Point(682, 126);
            this.txtMaVanBan5.MaxLength = 12;
            this.txtMaVanBan5.Name = "txtMaVanBan5";
            this.txtMaVanBan5.Size = new System.Drawing.Size(26, 21);
            this.txtMaVanBan5.TabIndex = 16;
            this.txtMaVanBan5.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaVanBan5.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaVanBan4
            // 
            this.txtMaVanBan4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaVanBan4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaVanBan4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaVanBan4.Location = new System.Drawing.Point(650, 126);
            this.txtMaVanBan4.MaxLength = 12;
            this.txtMaVanBan4.Name = "txtMaVanBan4";
            this.txtMaVanBan4.Size = new System.Drawing.Size(26, 21);
            this.txtMaVanBan4.TabIndex = 15;
            this.txtMaVanBan4.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaVanBan4.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaVanBan3
            // 
            this.txtMaVanBan3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaVanBan3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaVanBan3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaVanBan3.Location = new System.Drawing.Point(618, 126);
            this.txtMaVanBan3.MaxLength = 12;
            this.txtMaVanBan3.Name = "txtMaVanBan3";
            this.txtMaVanBan3.Size = new System.Drawing.Size(26, 21);
            this.txtMaVanBan3.TabIndex = 14;
            this.txtMaVanBan3.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaVanBan3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaVanBan2
            // 
            this.txtMaVanBan2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaVanBan2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaVanBan2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaVanBan2.Location = new System.Drawing.Point(586, 126);
            this.txtMaVanBan2.MaxLength = 12;
            this.txtMaVanBan2.Name = "txtMaVanBan2";
            this.txtMaVanBan2.Size = new System.Drawing.Size(26, 21);
            this.txtMaVanBan2.TabIndex = 13;
            this.txtMaVanBan2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaVanBan2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaVanBan1
            // 
            this.txtMaVanBan1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaVanBan1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaVanBan1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaVanBan1.Location = new System.Drawing.Point(557, 126);
            this.txtMaVanBan1.MaxLength = 12;
            this.txtMaVanBan1.Name = "txtMaVanBan1";
            this.txtMaVanBan1.Size = new System.Drawing.Size(26, 21);
            this.txtMaVanBan1.TabIndex = 12;
            this.txtMaVanBan1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaVanBan1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ctrMaTTDonGia
            // 
            this.ctrMaTTDonGia.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ctrMaTTDonGia.Code = "";
            this.ctrMaTTDonGia.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaTTDonGia.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaTTDonGia.IsValidate = false;
            this.ctrMaTTDonGia.Location = new System.Drawing.Point(708, 94);
            this.ctrMaTTDonGia.Name = "ctrMaTTDonGia";
            this.ctrMaTTDonGia.Name_VN = "";
            this.ctrMaTTDonGia.ShowColumnCode = true;
            this.ctrMaTTDonGia.ShowColumnName = false;
            this.ctrMaTTDonGia.Size = new System.Drawing.Size(64, 26);
            this.ctrMaTTDonGia.TabIndex = 10;
            this.ctrMaTTDonGia.TagName = "";
            this.ctrMaTTDonGia.WhereCondition = "";
            // 
            // ctrDVTLuong2
            // 
            this.ctrDVTLuong2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTLuong2.Code = "";
            this.ctrDVTLuong2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTLuong2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTLuong2.IsValidate = false;
            this.ctrDVTLuong2.Location = new System.Drawing.Point(331, 94);
            this.ctrDVTLuong2.Name = "ctrDVTLuong2";
            this.ctrDVTLuong2.Name_VN = "";
            this.ctrDVTLuong2.ShowColumnCode = true;
            this.ctrDVTLuong2.ShowColumnName = false;
            this.ctrDVTLuong2.Size = new System.Drawing.Size(79, 26);
            this.ctrDVTLuong2.TabIndex = 6;
            this.ctrDVTLuong2.TagName = "";
            this.ctrDVTLuong2.WhereCondition = "";
            // 
            // ctrDVTLuong1
            // 
            this.ctrDVTLuong1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTLuong1.Code = "";
            this.ctrDVTLuong1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTLuong1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTLuong1.IsValidate = false;
            this.ctrDVTLuong1.Location = new System.Drawing.Point(330, 67);
            this.ctrDVTLuong1.Name = "ctrDVTLuong1";
            this.ctrDVTLuong1.Name_VN = "";
            this.ctrDVTLuong1.ShowColumnCode = true;
            this.ctrDVTLuong1.ShowColumnName = false;
            this.ctrDVTLuong1.Size = new System.Drawing.Size(79, 26);
            this.ctrDVTLuong1.TabIndex = 4;
            this.ctrDVTLuong1.TagName = "";
            this.ctrDVTLuong1.WhereCondition = "";
            // 
            // ctrMaSoHang
            // 
            this.ctrMaSoHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A506;
            this.ctrMaSoHang.Code = "";
            this.ctrMaSoHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaSoHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaSoHang.IsValidate = false;
            this.ctrMaSoHang.Location = new System.Drawing.Point(105, 41);
            this.ctrMaSoHang.Name = "ctrMaSoHang";
            this.ctrMaSoHang.Name_VN = "";
            this.ctrMaSoHang.ShowColumnCode = true;
            this.ctrMaSoHang.ShowColumnName = false;
            this.ctrMaSoHang.Size = new System.Drawing.Size(159, 26);
            this.ctrMaSoHang.TabIndex = 1;
            this.ctrMaSoHang.TagName = "";
            this.ctrMaSoHang.WhereCondition = "";
            // 
            // txtMaQuanLy
            // 
            this.txtMaQuanLy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaQuanLy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaQuanLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaQuanLy.Location = new System.Drawing.Point(105, 16);
            this.txtMaQuanLy.MaxLength = 12;
            this.txtMaQuanLy.Name = "txtMaQuanLy";
            this.txtMaQuanLy.Size = new System.Drawing.Size(159, 21);
            this.txtMaQuanLy.TabIndex = 0;
            this.txtMaQuanLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaQuanLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGiaHoaDon
            // 
            this.txtTriGiaHoaDon.BackColor = System.Drawing.Color.White;
            this.txtTriGiaHoaDon.DecimalDigits = 20;
            this.txtTriGiaHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaHoaDon.FormatString = "G20";
            this.txtTriGiaHoaDon.Location = new System.Drawing.Point(499, 69);
            this.txtTriGiaHoaDon.Name = "txtTriGiaHoaDon";
            this.txtTriGiaHoaDon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaHoaDon.Size = new System.Drawing.Size(143, 21);
            this.txtTriGiaHoaDon.TabIndex = 7;
            this.txtTriGiaHoaDon.Text = "0";
            this.txtTriGiaHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTriGiaHoaDon.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTriGiaHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTriGiaHoaDon.VisualStyleManager = this.vsmMain;
            // 
            // txtDonGiaHoaDon
            // 
            this.txtDonGiaHoaDon.DecimalDigits = 20;
            this.txtDonGiaHoaDon.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtDonGiaHoaDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaHoaDon.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDonGiaHoaDon.Location = new System.Drawing.Point(499, 96);
            this.txtDonGiaHoaDon.MaxLength = 15;
            this.txtDonGiaHoaDon.Name = "txtDonGiaHoaDon";
            this.txtDonGiaHoaDon.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDonGiaHoaDon.Size = new System.Drawing.Size(143, 21);
            this.txtDonGiaHoaDon.TabIndex = 8;
            this.txtDonGiaHoaDon.Text = "0";
            this.txtDonGiaHoaDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDonGiaHoaDon.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDonGiaHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDonGiaHoaDon.VisualStyleManager = this.vsmMain;
            // 
            // txtSoLuong2
            // 
            this.txtSoLuong2.DecimalDigits = 20;
            this.txtSoLuong2.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong2.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong2.Location = new System.Drawing.Point(105, 97);
            this.txtSoLuong2.MaxLength = 15;
            this.txtSoLuong2.Name = "txtSoLuong2";
            this.txtSoLuong2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong2.Size = new System.Drawing.Size(159, 21);
            this.txtSoLuong2.TabIndex = 5;
            this.txtSoLuong2.Text = "0";
            this.txtSoLuong2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong2.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoLuong1
            // 
            this.txtSoLuong1.DecimalDigits = 20;
            this.txtSoLuong1.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuong1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong1.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoLuong1.Location = new System.Drawing.Point(105, 70);
            this.txtSoLuong1.MaxLength = 15;
            this.txtSoLuong1.Name = "txtSoLuong1";
            this.txtSoLuong1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuong1.Size = new System.Drawing.Size(159, 21);
            this.txtSoLuong1.TabIndex = 3;
            this.txtSoLuong1.Text = "0";
            this.txtSoLuong1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuong1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuong1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuong1.VisualStyleManager = this.vsmMain;
            // 
            // txtTenHang
            // 
            this.txtTenHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHang.Location = new System.Drawing.Point(330, 16);
            this.txtTenHang.MaxLength = 255;
            this.txtTenHang.Multiline = true;
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(442, 49);
            this.txtTenHang.TabIndex = 2;
            this.txtTenHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHang.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(407, 131);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(136, 13);
            this.label19.TabIndex = 29;
            this.label19.Text = "Mã văn bản pháp luật khác";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(413, 73);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 34;
            this.label8.Text = "Trị giá hóa đơn";
            // 
            // txtSoTTDongHangTKTNTX
            // 
            this.txtSoTTDongHangTKTNTX.DecimalDigits = 20;
            this.txtSoTTDongHangTKTNTX.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTTDongHangTKTNTX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTTDongHangTKTNTX.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtSoTTDongHangTKTNTX.Location = new System.Drawing.Point(330, 126);
            this.txtSoTTDongHangTKTNTX.MaxLength = 15;
            this.txtSoTTDongHangTKTNTX.Name = "txtSoTTDongHangTKTNTX";
            this.txtSoTTDongHangTKTNTX.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTTDongHangTKTNTX.Size = new System.Drawing.Size(54, 21);
            this.txtSoTTDongHangTKTNTX.TabIndex = 11;
            this.txtSoTTDongHangTKTNTX.Text = "0";
            this.txtSoTTDongHangTKTNTX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTTDongHangTKTNTX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoTTDongHangTKTNTX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(270, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Đơn vị tính";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(413, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Đơn giá hóa đơn";
            // 
            // txtDVTDonGia
            // 
            this.txtDVTDonGia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtDVTDonGia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDVTDonGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVTDonGia.Location = new System.Drawing.Point(648, 97);
            this.txtDVTDonGia.MaxLength = 12;
            this.txtDVTDonGia.Name = "txtDVTDonGia";
            this.txtDVTDonGia.Size = new System.Drawing.Size(56, 21);
            this.txtDVTDonGia.TabIndex = 9;
            this.txtDVTDonGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDVTDonGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(270, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Đơn vị tính";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 130);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(307, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Số thứ tự dòng hàng trên tờ khai tạm nhập tái xuất tương ứng";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Số lượng (2)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(270, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Tên hàng";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Số lượng (1)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(7, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Mã số hàng hóa";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Mã quản lý riêng";
            // 
            // VNACC_HangMauDichXuatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 620);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_HangMauDichXuatForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "thông tin hàng hóa";
            this.Load += new System.EventHandler(this.VNACC_HangMauDichXuatForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            this.uiTabPage1.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaHoaDon;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaHoaDon;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTTDongHangTKTNTX;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong2;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHang;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaQuanLy;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDVTTuyetDoi;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtThueSuatTuyetDoi;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVTDonGia;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaMienGiamThue;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoDMMienThue;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDongDMMienThue;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.GridEX.GridEX grList;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienMienGiam;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThue;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtThueSuat;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaSoHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTTuyetDoi;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTTriGiaTinhThue;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTDonGia;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTLuong2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTLuong1;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaVanBan5;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaVanBan4;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaVanBan3;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaVanBan2;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaVanBan1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTTriGiaTinhThueS;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaTinhThueS;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTCuaDonGiaTinhThue;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTMaMienGiam;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaTTSoTienThueXuatKhau;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaTinhThue;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienThue;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.EditBox txtDV_SL_TrongDonGiaTinhThue;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox5;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox6;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucCategory5;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucCategory6;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhanLoai_TS_ThueXuatKhau;
        private System.Windows.Forms.Label label26;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.GridEX.EditControls.EditBox editBox2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.GridEX.EditControls.EditBox editBox1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.GridEX.EditControls.EditBox txtDieuKhoanMienGiam;
        private System.Windows.Forms.Label label30;
    }
}