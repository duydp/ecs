﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
//using Company.Interface.GC;
//using Company.Interface.SXXK;

using Janus.Windows.EditControls;
using Janus.Windows.ExplorerBar;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Dock;

namespace DongBoDuLieu
{
    partial class MainForm
    {
        private UICommandManager cmMain;
        private UIRebar TopRebar1;
        private UICommand cmdHeThong;
        private UICommand cmdThoat;
        private UICommand cmdThoat1;
        private UIRebar BottomRebar1;
        private UIRebar LeftRebar1;
        private UIRebar RightRebar1;
        private UIPanelManager pmMain;
        private UICommandBar cmbMenu;
        private UICommand cmdLoaiHinh;
        private UIPanel pnlSXXK;
        private UIPanelInnerContainer pnlSXXKContainer;
        private UIPanel pnlGiaCong;
        private UIPanelInnerContainer pnlGiaCongContainer;
        private UIPanel pnlKinhDoanh;
        private UIPanelInnerContainer pnlKinhDoanhContainer;
        private UIPanel pnlDauTu;
        internal ImageList ilSmall;
        internal ImageList ilMedium;
        internal ImageList ilLarge;
        private UICommand cmdReceiveAll;
        private Janus.Windows.ExplorerBar.ExplorerBar expGiaCong;
        private Janus.Windows.ExplorerBar.ExplorerBar expKD;
        private Janus.Windows.UI.CommandBars.UICommand cmdThoat2;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup9 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem20 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup10 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem21 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem22 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem23 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup11 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem24 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem25 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem26 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup12 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem27 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem28 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem29 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup13 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem30 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem31 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup14 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem32 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem33 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem34 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup2 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem2 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem3 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup3 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem4 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem5 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel4 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel5 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel6 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbMenu = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdHeThong1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHeThong");
            this.cmdCuaSo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaSo");
            this.Command01 = new Janus.Windows.UI.CommandBars.UICommand("Command0");
            this.QuanTri1 = new Janus.Windows.UI.CommandBars.UICommand("QuanTri");
            this.cmdHeThong = new Janus.Windows.UI.CommandBars.UICommand("cmdHeThong");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdCauHinh1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinh");
            this.Separator3 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.LoginUser1 = new Janus.Windows.UI.CommandBars.UICommand("LoginUser");
            this.cmdChangePass1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChangePass");
            this.Separator4 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdThoat2 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.cmdThoat = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.cmdLoaiHinh = new Janus.Windows.UI.CommandBars.UICommand("cmdLoaiHinh");
            this.cmdReceiveAll = new Janus.Windows.UI.CommandBars.UICommand("cmdReceiveAll");
            this.NhacNho = new Janus.Windows.UI.CommandBars.UICommand("NhacNho");
            this.DongBoDuLieu = new Janus.Windows.UI.CommandBars.UICommand("DongBoDuLieu");
            this.cmdImport = new Janus.Windows.UI.CommandBars.UICommand("cmdImport");
            this.cmdImportNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportNPL");
            this.cmdImportSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportSP");
            this.cmdImportDM1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportDM");
            this.cmdImportToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportToKhai");
            this.cmdImportHangHoa1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportHangHoa");
            this.cmdNPLNhapTon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLNhapTon");
            this.cmdImportNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdImportNPL");
            this.cmdImportSP = new Janus.Windows.UI.CommandBars.UICommand("cmdImportSP");
            this.cmdImportDM = new Janus.Windows.UI.CommandBars.UICommand("cmdImportDM");
            this.cmdImportTTDM = new Janus.Windows.UI.CommandBars.UICommand("cmdImportTTDM");
            this.cmdImportToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdImportToKhai");
            this.cmdImportHangHoa = new Janus.Windows.UI.CommandBars.UICommand("cmdImportHangHoa");
            this.Command1 = new Janus.Windows.UI.CommandBars.UICommand("Command1");
            this.cmd20071 = new Janus.Windows.UI.CommandBars.UICommand("cmd2007");
            this.cmd20031 = new Janus.Windows.UI.CommandBars.UICommand("cmd2003");
            this.cmdVN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdVN");
            this.cmdEL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdEL");
            this.cmd2007 = new Janus.Windows.UI.CommandBars.UICommand("cmd2007");
            this.cmd2003 = new Janus.Windows.UI.CommandBars.UICommand("cmd2003");
            this.Command0 = new Janus.Windows.UI.CommandBars.UICommand("Command0");
            this.cmdAbout1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAbout");
            this.cmdHelp = new Janus.Windows.UI.CommandBars.UICommand("cmdHelp");
            this.cmdAbout = new Janus.Windows.UI.CommandBars.UICommand("cmdAbout");
            this.cmdNPLNhapTon = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLNhapTon");
            this.cmdDanhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdDanhMuc");
            this.cmdHaiQuan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHaiQuan");
            this.cmdNuoc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNuoc");
            this.cmdMaHS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdMaHS");
            this.cmdNguyenTe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNguyenTe");
            this.cmdDVT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDVT");
            this.cmdPTTT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPTTT");
            this.cmdPTVT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPTVT");
            this.cmdDKGH1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDKGH");
            this.cmdCuaKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaKhau");
            this.cmdMaHS = new Janus.Windows.UI.CommandBars.UICommand("cmdMaHS");
            this.cmdNuoc = new Janus.Windows.UI.CommandBars.UICommand("cmdNuoc");
            this.cmdHaiQuan = new Janus.Windows.UI.CommandBars.UICommand("cmdHaiQuan");
            this.cmdNguyenTe = new Janus.Windows.UI.CommandBars.UICommand("cmdNguyenTe");
            this.cmdDVT = new Janus.Windows.UI.CommandBars.UICommand("cmdDVT");
            this.cmdPTTT = new Janus.Windows.UI.CommandBars.UICommand("cmdPTTT");
            this.cmdPTVT = new Janus.Windows.UI.CommandBars.UICommand("cmdPTVT");
            this.cmdDKGH = new Janus.Windows.UI.CommandBars.UICommand("cmdDKGH");
            this.cmdCuaKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaKhau");
            this.ThongSoKetNoi = new Janus.Windows.UI.CommandBars.UICommand("ThongSoKetNoi");
            this.TLThongTinDNHQ = new Janus.Windows.UI.CommandBars.UICommand("TLThongTinDNHQ");
            this.cmdThietLapIn = new Janus.Windows.UI.CommandBars.UICommand("cmdThietLapIn");
            this.cmdExportExccel = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdImportExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdImportExcel");
            this.cmdDongBoPhongKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdDongBoPhongKhai");
            this.cmdImportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportExcel");
            this.cmdExportExccel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdCauHinh = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinh");
            this.ThongSoKetNoi1 = new Janus.Windows.UI.CommandBars.UICommand("ThongSoKetNoi");
            this.TLThongTinDNHQ1 = new Janus.Windows.UI.CommandBars.UICommand("TLThongTinDNHQ");
            this.cmdCauHinhToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinhToKhai");
            this.QuanTri = new Janus.Windows.UI.CommandBars.UICommand("QuanTri");
            this.QuanLyNguoiDung1 = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNguoiDung");
            this.QuanLyNguoiDung = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNguoiDung");
            this.QuanLyNhom = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNhom");
            this.LoginUser = new Janus.Windows.UI.CommandBars.UICommand("LoginUser");
            this.cmdChangePass = new Janus.Windows.UI.CommandBars.UICommand("cmdChangePass");
            this.cmdDoiMatKhauHQ = new Janus.Windows.UI.CommandBars.UICommand("cmdDoiMatKhauHQ");
            this.cmdCuaSo = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaSo");
            this.cmdCloseAll1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAll");
            this.cmdCloseAllButThis1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAllButThis");
            this.cmdCloseAll = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAll");
            this.cmdCloseAllButThis = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAllButThis");
            this.cmdAutoUpdate = new Janus.Windows.UI.CommandBars.UICommand("cmdAutoUpdate");
            this.cmdNhapXML = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapXML");
            this.cmdNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNPL");
            this.cmdSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSP");
            this.cmdDM1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDM");
            this.cmdTK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTK");
            this.cmdNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdNPL");
            this.cmdSP = new Janus.Windows.UI.CommandBars.UICommand("cmdSP");
            this.cmdDM = new Janus.Windows.UI.CommandBars.UICommand("cmdDM");
            this.cmdTK = new Janus.Windows.UI.CommandBars.UICommand("cmdTK");
            this.cmdXuatDuLieu = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieu");
            this.cmdHUNGNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGNPL");
            this.cmdHUNGSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGSP");
            this.cmdHUNGDM1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGDM");
            this.cmdHUNGTK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGTK");
            this.cmdXuatNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatNPL");
            this.cmdXuatDinhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDinhMuc");
            this.cmdXuatSanPham = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatSanPham");
            this.cmdXuatToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhai");
            this.cmdVN = new Janus.Windows.UI.CommandBars.UICommand("cmdVN");
            this.cmdEL = new Janus.Windows.UI.CommandBars.UICommand("cmdEL");
            this.cmdHUNGNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGNPL");
            this.cmdHUNGSP = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGSP");
            this.cmdHUNGDM = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGDM");
            this.cmdHUNGTK = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGTK");
            this.cmdActivate = new Janus.Windows.UI.CommandBars.UICommand("cmdActivate");
            this.cmdXuatDuLieuDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieuDN");
            this.cmdXuatNPLDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatNPLDN");
            this.cmdXuatSPDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatSPDN");
            this.cmdXuatDMDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDMDN");
            this.cmdXuatTKDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatTKDN");
            this.cmdXuatNPLDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatNPLDN");
            this.cmdXuatSPDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatSPDN");
            this.cmXuat = new Janus.Windows.UI.CommandBars.UICommand("cmXuat");
            this.cmdXuatDMDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDMDN");
            this.cmdXuatTKDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatTKDN");
            this.cmdNhapDuLieu = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDuLieu");
            this.cmdNhapNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapNPL");
            this.cmdNhapSanPham1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapSanPham");
            this.cmdNhapDinhMuc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDinhMuc");
            this.cmdNhapToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhai");
            this.cmdNhapNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapNPL");
            this.cmdNhapSanPham = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapSanPham");
            this.cmdNhapDinhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDinhMuc");
            this.cmdNhapToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhai");
            this.QuanlyMess = new Janus.Windows.UI.CommandBars.UICommand("QuanlyMess");
            this.cmdConfig = new Janus.Windows.UI.CommandBars.UICommand("cmdConfig");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdBackUp = new Janus.Windows.UI.CommandBars.UICommand("cmdBackUp");
            this.cmdRestore = new Janus.Windows.UI.CommandBars.UICommand("cmdRestore");
            this.cmdExportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdThoat1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.pmMain = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expDongBo = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlSXXK = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlSXXKContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.pnlGiaCong = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlGiaCongContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expGiaCong = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlKinhDoanh = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlKinhDoanhContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expKD = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlDauTu = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlDauTuContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expDT = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlSend = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlSendContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.ilSmall = new System.Windows.Forms.ImageList(this.components);
            this.ilMedium = new System.Windows.Forms.ImageList(this.components);
            this.ilLarge = new System.Windows.Forms.ImageList(this.components);
            this.statusBar = new Janus.Windows.UI.StatusBar.UIStatusBar();
            this.cmdThoat3 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expDongBo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSXXK)).BeginInit();
            this.pnlSXXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGiaCong)).BeginInit();
            this.pnlGiaCong.SuspendLayout();
            this.pnlGiaCongContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expGiaCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKinhDoanh)).BeginInit();
            this.pnlKinhDoanh.SuspendLayout();
            this.pnlKinhDoanhContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expKD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDauTu)).BeginInit();
            this.pnlDauTu.SuspendLayout();
            this.pnlDauTuContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSend)).BeginInit();
            this.pnlSend.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.BackColor = System.Drawing.SystemColors.Control;
            this.grbMain.Location = new System.Drawing.Point(203, 29);
            this.grbMain.Size = new System.Drawing.Size(603, 471);
            this.grbMain.Visible = false;
            // 
            // cmMain
            // 
            this.cmMain.AllowClose = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowCustomize = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowMerge = false;
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbMenu});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHeThong,
            this.cmdThoat,
            this.cmdLoaiHinh,
            this.cmdReceiveAll,
            this.NhacNho,
            this.DongBoDuLieu,
            this.cmdImport,
            this.cmdImportNPL,
            this.cmdImportSP,
            this.cmdImportDM,
            this.cmdImportTTDM,
            this.cmdImportToKhai,
            this.cmdImportHangHoa,
            this.Command1,
            this.cmd2007,
            this.cmd2003,
            this.Command0,
            this.cmdHelp,
            this.cmdAbout,
            this.cmdNPLNhapTon,
            this.cmdDanhMuc,
            this.cmdMaHS,
            this.cmdNuoc,
            this.cmdHaiQuan,
            this.cmdNguyenTe,
            this.cmdDVT,
            this.cmdPTTT,
            this.cmdPTVT,
            this.cmdDKGH,
            this.cmdCuaKhau,
            this.ThongSoKetNoi,
            this.TLThongTinDNHQ,
            this.cmdThietLapIn,
            this.cmdExportExccel,
            this.cmdImportExcel,
            this.cmdDongBoPhongKhai,
            this.cmdCauHinh,
            this.cmdCauHinhToKhai,
            this.QuanTri,
            this.QuanLyNguoiDung,
            this.QuanLyNhom,
            this.LoginUser,
            this.cmdChangePass,
            this.cmdDoiMatKhauHQ,
            this.cmdCuaSo,
            this.cmdCloseAll,
            this.cmdCloseAllButThis,
            this.cmdAutoUpdate,
            this.cmdNhapXML,
            this.cmdNPL,
            this.cmdSP,
            this.cmdDM,
            this.cmdTK,
            this.cmdXuatDuLieu,
            this.cmdXuatNPL,
            this.cmdXuatDinhMuc,
            this.cmdXuatSanPham,
            this.cmdXuatToKhai,
            this.cmdVN,
            this.cmdEL,
            this.cmdHUNGNPL,
            this.cmdHUNGSP,
            this.cmdHUNGDM,
            this.cmdHUNGTK,
            this.cmdActivate,
            this.cmdXuatDuLieuDN,
            this.cmdXuatNPLDN,
            this.cmdXuatSPDN,
            this.cmXuat,
            this.cmdXuatDMDN,
            this.cmdXuatTKDN,
            this.cmdNhapDuLieu,
            this.cmdNhapNPL,
            this.cmdNhapSanPham,
            this.cmdNhapDinhMuc,
            this.cmdNhapToKhai,
            this.QuanlyMess,
            this.cmdConfig});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("eae49f54-3bfa-4a6a-8b46-89b443ba80cd");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.LockCommandBars = true;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmbMenu
            // 
            this.cmbMenu.AllowClose = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.AllowCustomize = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.AllowMerge = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.CommandBarType = Janus.Windows.UI.CommandBars.CommandBarType.Menu;
            this.cmbMenu.CommandManager = this.cmMain;
            this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHeThong1,
            this.cmdCuaSo1,
            this.Command01,
            this.QuanTri1});
            this.cmbMenu.Key = "cmbMenu";
            this.cmbMenu.Location = new System.Drawing.Point(0, 0);
            this.cmbMenu.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.cmbMenu.MergeRowOrder = 0;
            this.cmbMenu.Name = "cmbMenu";
            this.cmbMenu.RowIndex = 0;
            this.cmbMenu.Size = new System.Drawing.Size(809, 26);
            this.cmbMenu.Text = "cmbMenu";
            // 
            // cmdHeThong1
            // 
            this.cmdHeThong1.Key = "cmdHeThong";
            this.cmdHeThong1.Name = "cmdHeThong1";
            this.cmdHeThong1.Text = "Hệ thốn&g";
            // 
            // cmdCuaSo1
            // 
            this.cmdCuaSo1.Key = "cmdCuaSo";
            this.cmdCuaSo1.Name = "cmdCuaSo1";
            this.cmdCuaSo1.Text = "&Cửa sổ";
            // 
            // Command01
            // 
            this.Command01.Key = "Command0";
            this.Command01.Name = "Command01";
            this.Command01.Text = "&Trợ giúp";
            // 
            // QuanTri1
            // 
            this.QuanTri1.Key = "QuanTri";
            this.QuanTri1.Name = "QuanTri1";
            this.QuanTri1.Text = "&Quản trị";
            // 
            // cmdHeThong
            // 
            this.cmdHeThong.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.Separator1,
            this.cmdCauHinh1,
            this.Separator3,
            this.LoginUser1,
            this.cmdChangePass1,
            this.Separator4,
            this.cmdThoat2});
            this.cmdHeThong.Key = "cmdHeThong";
            this.cmdHeThong.Name = "cmdHeThong";
            this.cmdHeThong.Text = "&Hệ thống";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdCauHinh1
            // 
            this.cmdCauHinh1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCauHinh1.Image")));
            this.cmdCauHinh1.Key = "cmdCauHinh";
            this.cmdCauHinh1.Name = "cmdCauHinh1";
            // 
            // Separator3
            // 
            this.Separator3.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator3.Key = "Separator";
            this.Separator3.Name = "Separator3";
            // 
            // LoginUser1
            // 
            this.LoginUser1.Key = "LoginUser";
            this.LoginUser1.Name = "LoginUser1";
            // 
            // cmdChangePass1
            // 
            this.cmdChangePass1.Key = "cmdChangePass";
            this.cmdChangePass1.Name = "cmdChangePass1";
            this.cmdChangePass1.Text = "Đổi mật khẩu đăng nhập";
            // 
            // Separator4
            // 
            this.Separator4.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator4.Key = "Separator";
            this.Separator4.Name = "Separator4";
            // 
            // cmdThoat2
            // 
            this.cmdThoat2.Key = "cmdThoat";
            this.cmdThoat2.Name = "cmdThoat2";
            // 
            // cmdThoat
            // 
            this.cmdThoat.Image = ((System.Drawing.Image)(resources.GetObject("cmdThoat.Image")));
            this.cmdThoat.Key = "cmdThoat";
            this.cmdThoat.Name = "cmdThoat";
            this.cmdThoat.Text = "Thoát";
            // 
            // cmdLoaiHinh
            // 
            this.cmdLoaiHinh.Key = "cmdLoaiHinh";
            this.cmdLoaiHinh.Name = "cmdLoaiHinh";
            this.cmdLoaiHinh.Text = "Loại hình";
            // 
            // cmdReceiveAll
            // 
            this.cmdReceiveAll.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdReceiveAll.Icon")));
            this.cmdReceiveAll.Key = "cmdReceiveAll";
            this.cmdReceiveAll.Name = "cmdReceiveAll";
            this.cmdReceiveAll.Text = "Cập nhật thông tin";
            // 
            // NhacNho
            // 
            this.NhacNho.Key = "NhacNho";
            this.NhacNho.Name = "NhacNho";
            this.NhacNho.Text = "Nhắc nhở";
            // 
            // DongBoDuLieu
            // 
            this.DongBoDuLieu.Icon = ((System.Drawing.Icon)(resources.GetObject("DongBoDuLieu.Icon")));
            this.DongBoDuLieu.Key = "DongBoDuLieu";
            this.DongBoDuLieu.Name = "DongBoDuLieu";
            this.DongBoDuLieu.Text = "Đồng bộ dữ liệu";
            // 
            // cmdImport
            // 
            this.cmdImport.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImportNPL1,
            this.cmdImportSP1,
            this.cmdImportDM1,
            this.cmdImportToKhai1,
            this.cmdImportHangHoa1,
            this.cmdNPLNhapTon1});
            this.cmdImport.Key = "cmdImport";
            this.cmdImport.Name = "cmdImport";
            this.cmdImport.Text = "Import from Excel";
            // 
            // cmdImportNPL1
            // 
            this.cmdImportNPL1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportNPL1.Icon")));
            this.cmdImportNPL1.Key = "cmdImportNPL";
            this.cmdImportNPL1.Name = "cmdImportNPL1";
            this.cmdImportNPL1.Text = "Nguyên phụ liệu";
            // 
            // cmdImportSP1
            // 
            this.cmdImportSP1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportSP1.Icon")));
            this.cmdImportSP1.Key = "cmdImportSP";
            this.cmdImportSP1.Name = "cmdImportSP1";
            this.cmdImportSP1.Text = "Sản phẩm";
            // 
            // cmdImportDM1
            // 
            this.cmdImportDM1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportDM1.Icon")));
            this.cmdImportDM1.Key = "cmdImportDM";
            this.cmdImportDM1.Name = "cmdImportDM1";
            this.cmdImportDM1.Text = "Định mức";
            // 
            // cmdImportToKhai1
            // 
            this.cmdImportToKhai1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportToKhai1.Icon")));
            this.cmdImportToKhai1.Key = "cmdImportToKhai";
            this.cmdImportToKhai1.Name = "cmdImportToKhai1";
            this.cmdImportToKhai1.Text = "Tờ khai";
            // 
            // cmdImportHangHoa1
            // 
            this.cmdImportHangHoa1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportHangHoa1.Icon")));
            this.cmdImportHangHoa1.Key = "cmdImportHangHoa";
            this.cmdImportHangHoa1.Name = "cmdImportHangHoa1";
            this.cmdImportHangHoa1.Text = "Hàng của tờ khai";
            // 
            // cmdNPLNhapTon1
            // 
            this.cmdNPLNhapTon1.Key = "cmdNPLNhapTon";
            this.cmdNPLNhapTon1.Name = "cmdNPLNhapTon1";
            // 
            // cmdImportNPL
            // 
            this.cmdImportNPL.Key = "cmdImportNPL";
            this.cmdImportNPL.Name = "cmdImportNPL";
            this.cmdImportNPL.Text = "Import NPL";
            // 
            // cmdImportSP
            // 
            this.cmdImportSP.Key = "cmdImportSP";
            this.cmdImportSP.Name = "cmdImportSP";
            this.cmdImportSP.Text = "Import Sản phẩm";
            // 
            // cmdImportDM
            // 
            this.cmdImportDM.Key = "cmdImportDM";
            this.cmdImportDM.Name = "cmdImportDM";
            this.cmdImportDM.Text = "Import Định mức";
            // 
            // cmdImportTTDM
            // 
            this.cmdImportTTDM.Key = "cmdImportTTDM";
            this.cmdImportTTDM.Name = "cmdImportTTDM";
            this.cmdImportTTDM.Text = "Import thông tin định mức";
            // 
            // cmdImportToKhai
            // 
            this.cmdImportToKhai.Key = "cmdImportToKhai";
            this.cmdImportToKhai.Name = "cmdImportToKhai";
            this.cmdImportToKhai.Text = "Import tờ khai";
            // 
            // cmdImportHangHoa
            // 
            this.cmdImportHangHoa.Key = "cmdImportHangHoa";
            this.cmdImportHangHoa.Name = "cmdImportHangHoa";
            this.cmdImportHangHoa.Text = "Import hàng tờ khai";
            // 
            // Command1
            // 
            this.Command1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmd20071,
            this.cmd20031,
            this.cmdVN1,
            this.cmdEL1});
            this.Command1.Key = "Command1";
            this.Command1.Name = "Command1";
            this.Command1.Text = "Giao diện";
            // 
            // cmd20071
            // 
            this.cmd20071.Key = "cmd2007";
            this.cmd20071.Name = "cmd20071";
            // 
            // cmd20031
            // 
            this.cmd20031.Key = "cmd2003";
            this.cmd20031.Name = "cmd20031";
            // 
            // cmdVN1
            // 
            this.cmdVN1.Checked = Janus.Windows.UI.InheritableBoolean.False;
            this.cmdVN1.Key = "cmdVN";
            this.cmdVN1.Name = "cmdVN1";
            this.cmdVN1.Text = "Tiếng Việt";
            // 
            // cmdEL1
            // 
            this.cmdEL1.Key = "cmdEL";
            this.cmdEL1.Name = "cmdEL1";
            this.cmdEL1.Text = "Tiếng Anh";
            // 
            // cmd2007
            // 
            this.cmd2007.Key = "cmd2007";
            this.cmd2007.Name = "cmd2007";
            this.cmd2007.Text = "Office 2007";
            // 
            // cmd2003
            // 
            this.cmd2003.Key = "cmd2003";
            this.cmd2003.Name = "cmd2003";
            this.cmd2003.Text = "Office 2003";
            // 
            // Command0
            // 
            this.Command0.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdAbout1});
            this.Command0.Key = "Command0";
            this.Command0.Name = "Command0";
            this.Command0.Text = "Trợ giúp";
            // 
            // cmdAbout1
            // 
            this.cmdAbout1.Image = ((System.Drawing.Image)(resources.GetObject("cmdAbout1.Image")));
            this.cmdAbout1.Key = "cmdAbout";
            this.cmdAbout1.Name = "cmdAbout1";
            this.cmdAbout1.Text = "&Thông tin sản phẩm";
            // 
            // cmdHelp
            // 
            this.cmdHelp.Key = "cmdHelp";
            this.cmdHelp.Name = "cmdHelp";
            this.cmdHelp.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
            this.cmdHelp.Text = "Hướng dẫn sử dụng";
            // 
            // cmdAbout
            // 
            this.cmdAbout.Key = "cmdAbout";
            this.cmdAbout.Name = "cmdAbout";
            this.cmdAbout.Text = "Thông tin sản phẩm";
            // 
            // cmdNPLNhapTon
            // 
            this.cmdNPLNhapTon.Key = "cmdNPLNhapTon";
            this.cmdNPLNhapTon.Name = "cmdNPLNhapTon";
            this.cmdNPLNhapTon.Text = "NPL nhập tồn";
            // 
            // cmdDanhMuc
            // 
            this.cmdDanhMuc.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHaiQuan1,
            this.cmdNuoc1,
            this.cmdMaHS1,
            this.cmdNguyenTe1,
            this.cmdDVT1,
            this.cmdPTTT1,
            this.cmdPTVT1,
            this.cmdDKGH1,
            this.cmdCuaKhau1});
            this.cmdDanhMuc.Key = "cmdDanhMuc";
            this.cmdDanhMuc.Name = "cmdDanhMuc";
            this.cmdDanhMuc.Text = "DanhMuc";
            // 
            // cmdHaiQuan1
            // 
            this.cmdHaiQuan1.Key = "cmdHaiQuan";
            this.cmdHaiQuan1.Name = "cmdHaiQuan1";
            // 
            // cmdNuoc1
            // 
            this.cmdNuoc1.Key = "cmdNuoc";
            this.cmdNuoc1.Name = "cmdNuoc1";
            // 
            // cmdMaHS1
            // 
            this.cmdMaHS1.Key = "cmdMaHS";
            this.cmdMaHS1.Name = "cmdMaHS1";
            // 
            // cmdNguyenTe1
            // 
            this.cmdNguyenTe1.Key = "cmdNguyenTe";
            this.cmdNguyenTe1.Name = "cmdNguyenTe1";
            // 
            // cmdDVT1
            // 
            this.cmdDVT1.Key = "cmdDVT";
            this.cmdDVT1.Name = "cmdDVT1";
            // 
            // cmdPTTT1
            // 
            this.cmdPTTT1.Key = "cmdPTTT";
            this.cmdPTTT1.Name = "cmdPTTT1";
            // 
            // cmdPTVT1
            // 
            this.cmdPTVT1.Key = "cmdPTVT";
            this.cmdPTVT1.Name = "cmdPTVT1";
            // 
            // cmdDKGH1
            // 
            this.cmdDKGH1.Key = "cmdDKGH";
            this.cmdDKGH1.Name = "cmdDKGH1";
            // 
            // cmdCuaKhau1
            // 
            this.cmdCuaKhau1.Key = "cmdCuaKhau";
            this.cmdCuaKhau1.Name = "cmdCuaKhau1";
            // 
            // cmdMaHS
            // 
            this.cmdMaHS.Key = "cmdMaHS";
            this.cmdMaHS.Name = "cmdMaHS";
            this.cmdMaHS.Text = "Mã HS";
            // 
            // cmdNuoc
            // 
            this.cmdNuoc.Key = "cmdNuoc";
            this.cmdNuoc.Name = "cmdNuoc";
            this.cmdNuoc.Text = "Nước";
            // 
            // cmdHaiQuan
            // 
            this.cmdHaiQuan.Key = "cmdHaiQuan";
            this.cmdHaiQuan.Name = "cmdHaiQuan";
            this.cmdHaiQuan.Text = "Đơn vị Hải quan";
            // 
            // cmdNguyenTe
            // 
            this.cmdNguyenTe.Key = "cmdNguyenTe";
            this.cmdNguyenTe.Name = "cmdNguyenTe";
            this.cmdNguyenTe.Text = "Nguyên tệ";
            // 
            // cmdDVT
            // 
            this.cmdDVT.Key = "cmdDVT";
            this.cmdDVT.Name = "cmdDVT";
            this.cmdDVT.Text = "Đơn vị tính";
            // 
            // cmdPTTT
            // 
            this.cmdPTTT.Key = "cmdPTTT";
            this.cmdPTTT.Name = "cmdPTTT";
            this.cmdPTTT.Text = "Phương thức thanh toán";
            // 
            // cmdPTVT
            // 
            this.cmdPTVT.Key = "cmdPTVT";
            this.cmdPTVT.Name = "cmdPTVT";
            this.cmdPTVT.Text = "Phương thức vận tải";
            // 
            // cmdDKGH
            // 
            this.cmdDKGH.Key = "cmdDKGH";
            this.cmdDKGH.Name = "cmdDKGH";
            this.cmdDKGH.Text = "Điều kiện giao hàng";
            // 
            // cmdCuaKhau
            // 
            this.cmdCuaKhau.Key = "cmdCuaKhau";
            this.cmdCuaKhau.Name = "cmdCuaKhau";
            this.cmdCuaKhau.Text = "Cửa khẩu";
            // 
            // ThongSoKetNoi
            // 
            this.ThongSoKetNoi.Image = ((System.Drawing.Image)(resources.GetObject("ThongSoKetNoi.Image")));
            this.ThongSoKetNoi.Key = "ThongSoKetNoi";
            this.ThongSoKetNoi.Name = "ThongSoKetNoi";
            this.ThongSoKetNoi.Text = "Thiết lập thông số kết nối";
            // 
            // TLThongTinDNHQ
            // 
            this.TLThongTinDNHQ.Image = ((System.Drawing.Image)(resources.GetObject("TLThongTinDNHQ.Image")));
            this.TLThongTinDNHQ.Key = "TLThongTinDNHQ";
            this.TLThongTinDNHQ.Name = "TLThongTinDNHQ";
            this.TLThongTinDNHQ.Text = "Thiết lập thông tin doanh nghiệp và hải quan";
            // 
            // cmdThietLapIn
            // 
            this.cmdThietLapIn.Key = "cmdThietLapIn";
            this.cmdThietLapIn.Name = "cmdThietLapIn";
            this.cmdThietLapIn.Text = "Thiết lập thông số in báo cáo";
            // 
            // cmdExportExccel
            // 
            this.cmdExportExccel.Key = "cmdExportExccel";
            this.cmdExportExccel.Name = "cmdExportExccel";
            this.cmdExportExccel.Text = "Xuất dữ liệu ra file Excel";
            // 
            // cmdImportExcel
            // 
            this.cmdImportExcel.Key = "cmdImportExcel";
            this.cmdImportExcel.Name = "cmdImportExcel";
            this.cmdImportExcel.Text = "Nhập dữ liêu từ file Excel";
            // 
            // cmdDongBoPhongKhai
            // 
            this.cmdDongBoPhongKhai.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImportExcel1,
            this.cmdExportExccel1});
            this.cmdDongBoPhongKhai.Key = "cmdDongBoPhongKhai";
            this.cmdDongBoPhongKhai.Name = "cmdDongBoPhongKhai";
            this.cmdDongBoPhongKhai.Text = "Đồng bộ dữ liệu với phòng khai";
            // 
            // cmdImportExcel1
            // 
            this.cmdImportExcel1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportExcel1.Icon")));
            this.cmdImportExcel1.Key = "cmdImportExcel";
            this.cmdImportExcel1.Name = "cmdImportExcel1";
            // 
            // cmdExportExccel1
            // 
            this.cmdExportExccel1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdExportExccel1.Icon")));
            this.cmdExportExccel1.Key = "cmdExportExccel";
            this.cmdExportExccel1.Name = "cmdExportExccel1";
            // 
            // cmdCauHinh
            // 
            this.cmdCauHinh.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ThongSoKetNoi1,
            this.TLThongTinDNHQ1});
            this.cmdCauHinh.Key = "cmdCauHinh";
            this.cmdCauHinh.Name = "cmdCauHinh";
            this.cmdCauHinh.Text = "Cấu hình hệ thống";
            // 
            // ThongSoKetNoi1
            // 
            this.ThongSoKetNoi1.Key = "ThongSoKetNoi";
            this.ThongSoKetNoi1.Name = "ThongSoKetNoi1";
            this.ThongSoKetNoi1.Text = "Cấu hình thông số kết nối";
            // 
            // TLThongTinDNHQ1
            // 
            this.TLThongTinDNHQ1.Key = "TLThongTinDNHQ";
            this.TLThongTinDNHQ1.Name = "TLThongTinDNHQ1";
            this.TLThongTinDNHQ1.Text = "Cấu hình thông tin doanh nghiệp và hải quan";
            // 
            // cmdCauHinhToKhai
            // 
            this.cmdCauHinhToKhai.Key = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai.Name = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai.Text = "Cấu hình tham số mặc định của tờ khai";
            // 
            // QuanTri
            // 
            this.QuanTri.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.QuanLyNguoiDung1});
            this.QuanTri.Key = "QuanTri";
            this.QuanTri.Name = "QuanTri";
            this.QuanTri.Text = "Quản trị";
            // 
            // QuanLyNguoiDung1
            // 
            this.QuanLyNguoiDung1.Image = ((System.Drawing.Image)(resources.GetObject("QuanLyNguoiDung1.Image")));
            this.QuanLyNguoiDung1.Key = "QuanLyNguoiDung";
            this.QuanLyNguoiDung1.Name = "QuanLyNguoiDung1";
            // 
            // QuanLyNguoiDung
            // 
            this.QuanLyNguoiDung.Key = "QuanLyNguoiDung";
            this.QuanLyNguoiDung.Name = "QuanLyNguoiDung";
            this.QuanLyNguoiDung.Text = "Quản lý người dùng";
            // 
            // QuanLyNhom
            // 
            this.QuanLyNhom.Key = "QuanLyNhom";
            this.QuanLyNhom.Name = "QuanLyNhom";
            this.QuanLyNhom.Text = "Quản lý nhóm người dùng";
            // 
            // LoginUser
            // 
            this.LoginUser.Image = ((System.Drawing.Image)(resources.GetObject("LoginUser.Image")));
            this.LoginUser.Key = "LoginUser";
            this.LoginUser.Name = "LoginUser";
            this.LoginUser.Text = "Đăng nhập người dùng khác";
            // 
            // cmdChangePass
            // 
            this.cmdChangePass.Image = ((System.Drawing.Image)(resources.GetObject("cmdChangePass.Image")));
            this.cmdChangePass.Key = "cmdChangePass";
            this.cmdChangePass.Name = "cmdChangePass";
            this.cmdChangePass.Text = "Đổi mật khẩu";
            // 
            // cmdDoiMatKhauHQ
            // 
            this.cmdDoiMatKhauHQ.Key = "cmdDoiMatKhauHQ";
            this.cmdDoiMatKhauHQ.Name = "cmdDoiMatKhauHQ";
            this.cmdDoiMatKhauHQ.Text = "Đổi mật khẩu kết nối với hải quan";
            // 
            // cmdCuaSo
            // 
            this.cmdCuaSo.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCloseAll1,
            this.cmdCloseAllButThis1});
            this.cmdCuaSo.Key = "cmdCuaSo";
            this.cmdCuaSo.Name = "cmdCuaSo";
            this.cmdCuaSo.Text = "Cửa sổ";
            // 
            // cmdCloseAll1
            // 
            this.cmdCloseAll1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCloseAll1.Image")));
            this.cmdCloseAll1.Key = "cmdCloseAll";
            this.cmdCloseAll1.Name = "cmdCloseAll1";
            // 
            // cmdCloseAllButThis1
            // 
            this.cmdCloseAllButThis1.Image = ((System.Drawing.Image)(resources.GetObject("cmdCloseAllButThis1.Image")));
            this.cmdCloseAllButThis1.Key = "cmdCloseAllButThis";
            this.cmdCloseAllButThis1.Name = "cmdCloseAllButThis1";
            // 
            // cmdCloseAll
            // 
            this.cmdCloseAll.Key = "cmdCloseAll";
            this.cmdCloseAll.Name = "cmdCloseAll";
            this.cmdCloseAll.Text = "Đóng tất cả";
            // 
            // cmdCloseAllButThis
            // 
            this.cmdCloseAllButThis.Key = "cmdCloseAllButThis";
            this.cmdCloseAllButThis.Name = "cmdCloseAllButThis";
            this.cmdCloseAllButThis.Text = "Đóng tất cả trừ cửa sổ đang kích hoạt";
            // 
            // cmdAutoUpdate
            // 
            this.cmdAutoUpdate.Key = "cmdAutoUpdate";
            this.cmdAutoUpdate.Name = "cmdAutoUpdate";
            this.cmdAutoUpdate.Text = "&Cập nhập chương trình";
            // 
            // cmdNhapXML
            // 
            this.cmdNhapXML.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdNPL1,
            this.cmdSP1,
            this.cmdDM1,
            this.cmdTK1});
            this.cmdNhapXML.Key = "cmdNhapXML";
            this.cmdNhapXML.Name = "cmdNhapXML";
            this.cmdNhapXML.Text = "Nhập dữ liệu từ đại lý";
            // 
            // cmdNPL1
            // 
            this.cmdNPL1.Key = "cmdNPL";
            this.cmdNPL1.Name = "cmdNPL1";
            // 
            // cmdSP1
            // 
            this.cmdSP1.Key = "cmdSP";
            this.cmdSP1.Name = "cmdSP1";
            // 
            // cmdDM1
            // 
            this.cmdDM1.Key = "cmdDM";
            this.cmdDM1.Name = "cmdDM1";
            // 
            // cmdTK1
            // 
            this.cmdTK1.Key = "cmdTK";
            this.cmdTK1.Name = "cmdTK1";
            // 
            // cmdNPL
            // 
            this.cmdNPL.Key = "cmdNPL";
            this.cmdNPL.Name = "cmdNPL";
            this.cmdNPL.Text = "Nguyên phụ liệu";
            // 
            // cmdSP
            // 
            this.cmdSP.Key = "cmdSP";
            this.cmdSP.Name = "cmdSP";
            this.cmdSP.Text = "Sản phẩm";
            // 
            // cmdDM
            // 
            this.cmdDM.Key = "cmdDM";
            this.cmdDM.Name = "cmdDM";
            this.cmdDM.Text = "Định mức";
            // 
            // cmdTK
            // 
            this.cmdTK.Key = "cmdTK";
            this.cmdTK.Name = "cmdTK";
            this.cmdTK.Text = "Tờ khai";
            // 
            // cmdXuatDuLieu
            // 
            this.cmdXuatDuLieu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHUNGNPL1,
            this.cmdHUNGSP1,
            this.cmdHUNGDM1,
            this.cmdHUNGTK1});
            this.cmdXuatDuLieu.Key = "cmdXuatDuLieu";
            this.cmdXuatDuLieu.Name = "cmdXuatDuLieu";
            this.cmdXuatDuLieu.Text = "Xuất dữ liệu cho phòng khai";
            // 
            // cmdHUNGNPL1
            // 
            this.cmdHUNGNPL1.Key = "cmdHUNGNPL";
            this.cmdHUNGNPL1.Name = "cmdHUNGNPL1";
            // 
            // cmdHUNGSP1
            // 
            this.cmdHUNGSP1.Key = "cmdHUNGSP";
            this.cmdHUNGSP1.Name = "cmdHUNGSP1";
            // 
            // cmdHUNGDM1
            // 
            this.cmdHUNGDM1.Key = "cmdHUNGDM";
            this.cmdHUNGDM1.Name = "cmdHUNGDM1";
            // 
            // cmdHUNGTK1
            // 
            this.cmdHUNGTK1.Key = "cmdHUNGTK";
            this.cmdHUNGTK1.Name = "cmdHUNGTK1";
            // 
            // cmdXuatNPL
            // 
            this.cmdXuatNPL.Key = "cmdXuatNPL";
            this.cmdXuatNPL.Name = "cmdXuatNPL";
            this.cmdXuatNPL.Text = "Nguyên phụ liệu";
            // 
            // cmdXuatDinhMuc
            // 
            this.cmdXuatDinhMuc.Key = "cmdXuatDinhMuc";
            this.cmdXuatDinhMuc.Name = "cmdXuatDinhMuc";
            this.cmdXuatDinhMuc.Text = "Định mức";
            // 
            // cmdXuatSanPham
            // 
            this.cmdXuatSanPham.Key = "cmdXuatSanPham";
            this.cmdXuatSanPham.Name = "cmdXuatSanPham";
            this.cmdXuatSanPham.Text = "Sản phẩm";
            // 
            // cmdXuatToKhai
            // 
            this.cmdXuatToKhai.Key = "cmdXuatToKhai";
            this.cmdXuatToKhai.Name = "cmdXuatToKhai";
            this.cmdXuatToKhai.Text = "Tờ khai";
            // 
            // cmdVN
            // 
            this.cmdVN.Key = "cmdVN";
            this.cmdVN.Name = "cmdVN";
            this.cmdVN.Text = "English";
            // 
            // cmdEL
            // 
            this.cmdEL.Key = "cmdEL";
            this.cmdEL.Name = "cmdEL";
            this.cmdEL.Text = "English";
            // 
            // cmdHUNGNPL
            // 
            this.cmdHUNGNPL.Key = "cmdHUNGNPL";
            this.cmdHUNGNPL.Name = "cmdHUNGNPL";
            this.cmdHUNGNPL.Text = "Nguyên phụ liệu";
            // 
            // cmdHUNGSP
            // 
            this.cmdHUNGSP.Key = "cmdHUNGSP";
            this.cmdHUNGSP.Name = "cmdHUNGSP";
            this.cmdHUNGSP.Text = "Sản phẩm";
            // 
            // cmdHUNGDM
            // 
            this.cmdHUNGDM.Key = "cmdHUNGDM";
            this.cmdHUNGDM.Name = "cmdHUNGDM";
            this.cmdHUNGDM.Text = "Định mức";
            // 
            // cmdHUNGTK
            // 
            this.cmdHUNGTK.Key = "cmdHUNGTK";
            this.cmdHUNGTK.Name = "cmdHUNGTK";
            this.cmdHUNGTK.Text = "Tờ khai";
            // 
            // cmdActivate
            // 
            this.cmdActivate.Key = "cmdActivate";
            this.cmdActivate.Name = "cmdActivate";
            this.cmdActivate.Shortcut = System.Windows.Forms.Shortcut.CtrlF10;
            this.cmdActivate.Text = "Kích hoạt phần mềm";
            // 
            // cmdXuatDuLieuDN
            // 
            this.cmdXuatDuLieuDN.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdXuatNPLDN1,
            this.cmdXuatSPDN1,
            this.cmdXuatDMDN1,
            this.cmdXuatTKDN1});
            this.cmdXuatDuLieuDN.Key = "cmdXuatDuLieuDN";
            this.cmdXuatDuLieuDN.Name = "cmdXuatDuLieuDN";
            this.cmdXuatDuLieuDN.Text = "Xuất dữ liệu cho doanh nghiệp";
            // 
            // cmdXuatNPLDN1
            // 
            this.cmdXuatNPLDN1.Key = "cmdXuatNPLDN";
            this.cmdXuatNPLDN1.Name = "cmdXuatNPLDN1";
            // 
            // cmdXuatSPDN1
            // 
            this.cmdXuatSPDN1.Key = "cmdXuatSPDN";
            this.cmdXuatSPDN1.Name = "cmdXuatSPDN1";
            // 
            // cmdXuatDMDN1
            // 
            this.cmdXuatDMDN1.Key = "cmdXuatDMDN";
            this.cmdXuatDMDN1.Name = "cmdXuatDMDN1";
            // 
            // cmdXuatTKDN1
            // 
            this.cmdXuatTKDN1.Key = "cmdXuatTKDN";
            this.cmdXuatTKDN1.Name = "cmdXuatTKDN1";
            // 
            // cmdXuatNPLDN
            // 
            this.cmdXuatNPLDN.Key = "cmdXuatNPLDN";
            this.cmdXuatNPLDN.Name = "cmdXuatNPLDN";
            this.cmdXuatNPLDN.Text = "NPL";
            // 
            // cmdXuatSPDN
            // 
            this.cmdXuatSPDN.Key = "cmdXuatSPDN";
            this.cmdXuatSPDN.Name = "cmdXuatSPDN";
            this.cmdXuatSPDN.Text = "SP";
            // 
            // cmXuat
            // 
            this.cmXuat.Key = "cmXuat";
            this.cmXuat.Name = "cmXuat";
            this.cmXuat.Text = "Xuất dữ liệu cho doanh nghiệp";
            // 
            // cmdXuatDMDN
            // 
            this.cmdXuatDMDN.Key = "cmdXuatDMDN";
            this.cmdXuatDMDN.Name = "cmdXuatDMDN";
            this.cmdXuatDMDN.Text = "ĐM";
            // 
            // cmdXuatTKDN
            // 
            this.cmdXuatTKDN.Key = "cmdXuatTKDN";
            this.cmdXuatTKDN.Name = "cmdXuatTKDN";
            this.cmdXuatTKDN.Text = "TK";
            // 
            // cmdNhapDuLieu
            // 
            this.cmdNhapDuLieu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdNhapNPL1,
            this.cmdNhapSanPham1,
            this.cmdNhapDinhMuc1,
            this.cmdNhapToKhai1});
            this.cmdNhapDuLieu.Key = "cmdNhapDuLieu";
            this.cmdNhapDuLieu.Name = "cmdNhapDuLieu";
            this.cmdNhapDuLieu.Text = "Nhập dữ liệu từ doanh nghiệp";
            // 
            // cmdNhapNPL1
            // 
            this.cmdNhapNPL1.Key = "cmdNhapNPL";
            this.cmdNhapNPL1.Name = "cmdNhapNPL1";
            // 
            // cmdNhapSanPham1
            // 
            this.cmdNhapSanPham1.Key = "cmdNhapSanPham";
            this.cmdNhapSanPham1.Name = "cmdNhapSanPham1";
            // 
            // cmdNhapDinhMuc1
            // 
            this.cmdNhapDinhMuc1.Key = "cmdNhapDinhMuc";
            this.cmdNhapDinhMuc1.Name = "cmdNhapDinhMuc1";
            // 
            // cmdNhapToKhai1
            // 
            this.cmdNhapToKhai1.Key = "cmdNhapToKhai";
            this.cmdNhapToKhai1.Name = "cmdNhapToKhai1";
            // 
            // cmdNhapNPL
            // 
            this.cmdNhapNPL.Key = "cmdNhapNPL";
            this.cmdNhapNPL.Name = "cmdNhapNPL";
            this.cmdNhapNPL.Text = "Nguyên phụ liệu";
            // 
            // cmdNhapSanPham
            // 
            this.cmdNhapSanPham.Key = "cmdNhapSanPham";
            this.cmdNhapSanPham.Name = "cmdNhapSanPham";
            this.cmdNhapSanPham.Text = "Sản phẩm";
            // 
            // cmdNhapDinhMuc
            // 
            this.cmdNhapDinhMuc.Key = "cmdNhapDinhMuc";
            this.cmdNhapDinhMuc.Name = "cmdNhapDinhMuc";
            this.cmdNhapDinhMuc.Text = "Định mức";
            // 
            // cmdNhapToKhai
            // 
            this.cmdNhapToKhai.Key = "cmdNhapToKhai";
            this.cmdNhapToKhai.Name = "cmdNhapToKhai";
            this.cmdNhapToKhai.Text = "Tờ khai";
            // 
            // QuanlyMess
            // 
            this.QuanlyMess.Key = "QuanlyMess";
            this.QuanlyMess.Name = "QuanlyMess";
            this.QuanlyMess.Text = "Quản lý Message khai báo";
            // 
            // cmdConfig
            // 
            this.cmdConfig.Key = "cmdConfig";
            this.cmdConfig.Name = "cmdConfig";
            this.cmdConfig.Text = "Thiết lập cấu hình Doanh nghiệp";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 24);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 494);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbMenu});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.cmbMenu);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(809, 26);
            // 
            // cmdBackUp
            // 
            this.cmdBackUp.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBackUp.Icon")));
            this.cmdBackUp.Key = "cmdBackUp";
            this.cmdBackUp.Name = "cmdBackUp";
            this.cmdBackUp.Text = "Sao lưu dữ liệu";
            // 
            // cmdRestore
            // 
            this.cmdRestore.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdRestore.Icon")));
            this.cmdRestore.Key = "cmdRestore";
            this.cmdRestore.Name = "cmdRestore";
            this.cmdRestore.Text = "Phục hồi dữ liệu";
            // 
            // cmdExportExcel1
            // 
            this.cmdExportExcel1.Key = "cmdExportExccel";
            this.cmdExportExcel1.Name = "cmdExportExcel1";
            // 
            // cmdThoat1
            // 
            this.cmdThoat1.Key = "cmdThoat";
            this.cmdThoat1.Name = "cmdThoat1";
            // 
            // pmMain
            // 
            this.pmMain.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(247)))));
            this.pmMain.ContainerControl = this;
            this.pmMain.DefaultPanelSettings.ActiveCaptionMode = Janus.Windows.UI.Dock.ActiveCaptionMode.Never;
            this.pmMain.DefaultPanelSettings.CaptionDisplayMode = Janus.Windows.UI.Dock.PanelCaptionDisplayMode.Text;
            this.pmMain.DefaultPanelSettings.CaptionHeight = 30;
            this.pmMain.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.pmMain.DefaultPanelSettings.DarkCaptionFormatStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.pmMain.DefaultPanelSettings.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.Window;
            this.pmMain.DefaultPanelSettings.TabStateStyles.FormatStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.pmMain.TabbedMdi = true;
            this.pmMain.TabbedMdiSettings.TabStateStyles.SelectedFormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.pmMain.VisualStyleManager = this.vsmMain;
            this.uiPanel0.Id = new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea");
            this.uiPanel0.StaticGroup = true;
            this.uiPanel1.Id = new System.Guid("ff6e7c0b-1418-48c4-93db-38c36983d83e");
            this.uiPanel0.Panels.Add(this.uiPanel1);
            this.pmMain.Panels.Add(this.uiPanel0);
            // 
            // Design Time Panel Info:
            // 
            this.pmMain.BeginPanelInfo();
            this.pmMain.AddDockPanelInfo(new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, Janus.Windows.UI.Dock.PanelDockStyle.Left, true, new System.Drawing.Size(200, 471), true);
            this.pmMain.AddDockPanelInfo(new System.Guid("ff6e7c0b-1418-48c4-93db-38c36983d83e"), new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), -1, true);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("96d876c4-e448-4823-b699-fcff62ff56b1"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, true, new System.Drawing.Point(88, 116), new System.Drawing.Size(0, 6), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("2b3e5f09-9a24-4b99-bf7e-8ee886f8383d"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("d5e59413-5184-45bc-bbc5-9b40a268e6ec"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("52dc898d-e5c5-4c3e-964e-6134d41411e2"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("038f8df0-b141-4aac-bb44-6015ce71b26f"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("ab8bee13-8397-4584-b8e9-5cfc2b506e10"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("adc6599d-0d45-4f54-a9f5-4903d12e3180"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, true, new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("ff6e7c0b-1418-48c4-93db-38c36983d83e"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("aa65e349-23e9-4c90-b1e3-2ee334b109d8"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.GroupStyle = Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator;
            this.uiPanel0.Location = new System.Drawing.Point(3, 29);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.SelectedPanel = this.uiPanel1;
            this.uiPanel0.Size = new System.Drawing.Size(200, 471);
            this.uiPanel0.TabIndex = 4;
            this.uiPanel0.Text = "Panel 0";
            // 
            // uiPanel1
            // 
            this.uiPanel1.CloseButtonVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel1.Image = ((System.Drawing.Image)(resources.GetObject("uiPanel1.Image")));
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(0, 0);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(196, 399);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Đồng bộ dữ liệu";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.expDongBo);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(194, 368);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // expDongBo
            // 
            this.expDongBo.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem20.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem20.Image")));
            explorerBarItem20.Key = "itemDongBo";
            explorerBarItem20.Text = "Đồng bộ dữ liệu";
            explorerBarGroup9.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem20});
            explorerBarGroup9.Key = "Group1";
            explorerBarGroup9.Text = "Đồng bộ dữ liệu";
            this.expDongBo.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup9});
            this.expDongBo.ImageSize = new System.Drawing.Size(20, 20);
            this.expDongBo.Location = new System.Drawing.Point(0, 0);
            this.expDongBo.Name = "expDongBo";
            this.expDongBo.Size = new System.Drawing.Size(194, 368);
            this.expDongBo.TabIndex = 0;
            this.expDongBo.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expDongBo.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expDongBo_ItemClick);
            // 
            // pnlSXXK
            // 
            this.pnlSXXK.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlSXXK.Icon")));
            this.pnlSXXK.InnerContainer = this.pnlSXXKContainer;
            this.pnlSXXK.Location = new System.Drawing.Point(0, 0);
            this.pnlSXXK.Name = "pnlSXXK";
            this.pnlSXXK.Size = new System.Drawing.Size(209, 335);
            this.pnlSXXK.TabIndex = 4;
            this.pnlSXXK.Text = "Loại hình SXXK";
            // 
            // pnlSXXKContainer
            // 
            this.pnlSXXKContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlSXXKContainer.Name = "pnlSXXKContainer";
            this.pnlSXXKContainer.Size = new System.Drawing.Size(209, 335);
            this.pnlSXXKContainer.TabIndex = 0;
            // 
            // pnlGiaCong
            // 
            this.pnlGiaCong.Closed = true;
            this.pnlGiaCong.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlGiaCong.Icon")));
            this.pnlGiaCong.InnerContainer = this.pnlGiaCongContainer;
            this.pnlGiaCong.Location = new System.Drawing.Point(0, 0);
            this.pnlGiaCong.Name = "pnlGiaCong";
            this.pnlGiaCong.Size = new System.Drawing.Size(209, 335);
            this.pnlGiaCong.TabIndex = 4;
            this.pnlGiaCong.Text = "Loại hình gia công";
            // 
            // pnlGiaCongContainer
            // 
            this.pnlGiaCongContainer.Controls.Add(this.expGiaCong);
            this.pnlGiaCongContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlGiaCongContainer.Name = "pnlGiaCongContainer";
            this.pnlGiaCongContainer.Size = new System.Drawing.Size(209, 335);
            this.pnlGiaCongContainer.TabIndex = 0;
            // 
            // expGiaCong
            // 
            this.expGiaCong.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expGiaCong.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expGiaCong.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expGiaCong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expGiaCong.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem21.Key = "hdgcNhap";
            explorerBarItem21.Text = "Khai báo";
            explorerBarItem22.Key = "hdgcManage";
            explorerBarItem22.Text = "Theo dõi";
            explorerBarItem23.Key = "hdgcRegisted";
            explorerBarItem23.Text = "Đã đăng ký";
            explorerBarGroup10.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem21,
            explorerBarItem22,
            explorerBarItem23});
            explorerBarGroup10.Key = "grpHopDong";
            explorerBarGroup10.Text = "Hợp đồng";
            explorerBarItem24.Key = "dmSend";
            explorerBarItem24.Text = "Khai báo";
            explorerBarItem25.Key = "dmManage";
            explorerBarItem25.Text = "Theo dõi";
            explorerBarItem26.Key = "dmRegisted";
            explorerBarItem26.Text = "Đã đăng ký";
            explorerBarGroup11.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem24,
            explorerBarItem25,
            explorerBarItem26});
            explorerBarGroup11.Key = "grpDinhMuc";
            explorerBarGroup11.Text = "Định mức";
            explorerBarItem27.Key = "pkgcNhap";
            explorerBarItem27.Text = "Khai báo";
            explorerBarItem28.Key = "pkgcManage";
            explorerBarItem28.Text = "Theo dõi";
            explorerBarItem29.Key = "pkgcRegisted";
            explorerBarItem29.Text = "Đã đăng ký";
            explorerBarGroup12.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem27,
            explorerBarItem28,
            explorerBarItem29});
            explorerBarGroup12.Key = "grpPhuKien";
            explorerBarGroup12.Text = "Phụ kiện";
            explorerBarItem30.Key = "tkNhapKhau_GC";
            explorerBarItem30.Text = "Nhập khẩu";
            explorerBarItem31.Key = "tkXuatKhau_GC";
            explorerBarItem31.Text = "Xuất khẩu";
            explorerBarGroup13.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem30,
            explorerBarItem31});
            explorerBarGroup13.Key = "grpToKhai";
            explorerBarGroup13.Text = "Tờ khai";
            explorerBarItem32.Key = "tkGCCTNhap";
            explorerBarItem32.Text = "Tờ khai GCCT nhập";
            explorerBarItem33.Key = "tkGCCTXuat";
            explorerBarItem33.Text = "Tờ khai GCCT xuất";
            explorerBarItem34.Key = "theodoiTKCT";
            explorerBarItem34.Text = "Theo dõi";
            explorerBarGroup14.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem32,
            explorerBarItem33,
            explorerBarItem34});
            explorerBarGroup14.Key = "grpGCCT";
            explorerBarGroup14.Text = "Tờ khai GCCT";
            this.expGiaCong.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup10,
            explorerBarGroup11,
            explorerBarGroup12,
            explorerBarGroup13,
            explorerBarGroup14});
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expGiaCong.ImageSize = new System.Drawing.Size(16, 16);
            this.expGiaCong.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expGiaCong.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expGiaCong.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expGiaCong.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expGiaCong.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expGiaCong.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expGiaCong.Location = new System.Drawing.Point(0, 0);
            this.expGiaCong.Name = "expGiaCong";
            this.expGiaCong.Size = new System.Drawing.Size(209, 335);
            this.expGiaCong.TabIndex = 1;
            this.expGiaCong.Text = "explorerBar2";
            this.expGiaCong.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expGiaCong.VisualStyleManager = this.vsmMain;
            // 
            // pnlKinhDoanh
            // 
            this.pnlKinhDoanh.Closed = true;
            this.pnlKinhDoanh.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlKinhDoanh.Icon")));
            this.pnlKinhDoanh.InnerContainer = this.pnlKinhDoanhContainer;
            this.pnlKinhDoanh.Location = new System.Drawing.Point(0, 0);
            this.pnlKinhDoanh.Name = "pnlKinhDoanh";
            this.pnlKinhDoanh.Size = new System.Drawing.Size(209, 335);
            this.pnlKinhDoanh.TabIndex = 4;
            this.pnlKinhDoanh.Text = "Loại hình kinh doanh";
            // 
            // pnlKinhDoanhContainer
            // 
            this.pnlKinhDoanhContainer.Controls.Add(this.expKD);
            this.pnlKinhDoanhContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlKinhDoanhContainer.Name = "pnlKinhDoanhContainer";
            this.pnlKinhDoanhContainer.Size = new System.Drawing.Size(209, 335);
            this.pnlKinhDoanhContainer.TabIndex = 0;
            // 
            // expKD
            // 
            this.expKD.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expKD.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKD.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expKD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expKD.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem2.Key = "tkNhapKhau_KD";
            explorerBarItem2.Text = "Nhập khẩu";
            explorerBarItem3.Key = "tkXuatKhau_KD";
            explorerBarItem3.Text = "Xuất khẩu";
            explorerBarGroup2.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem2,
            explorerBarItem3});
            explorerBarGroup2.Key = "grpToKhai";
            explorerBarGroup2.Text = "Tờ khai";
            this.expKD.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup2});
            this.expKD.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expKD.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expKD.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expKD.ImageSize = new System.Drawing.Size(16, 16);
            this.expKD.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expKD.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expKD.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expKD.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expKD.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKD.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expKD.Location = new System.Drawing.Point(0, 0);
            this.expKD.Name = "expKD";
            this.expKD.Size = new System.Drawing.Size(209, 335);
            this.expKD.TabIndex = 1;
            this.expKD.Text = "explorerBar1";
            this.expKD.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expKD.VisualStyleManager = this.vsmMain;
            // 
            // pnlDauTu
            // 
            this.pnlDauTu.Closed = true;
            this.pnlDauTu.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlDauTu.Icon")));
            this.pnlDauTu.InnerContainer = this.pnlDauTuContainer;
            this.pnlDauTu.Location = new System.Drawing.Point(0, 0);
            this.pnlDauTu.Name = "pnlDauTu";
            this.pnlDauTu.Size = new System.Drawing.Size(209, 335);
            this.pnlDauTu.TabIndex = 4;
            this.pnlDauTu.Text = "Loại hình đầu tư";
            // 
            // pnlDauTuContainer
            // 
            this.pnlDauTuContainer.Controls.Add(this.expDT);
            this.pnlDauTuContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlDauTuContainer.Name = "pnlDauTuContainer";
            this.pnlDauTuContainer.Size = new System.Drawing.Size(209, 335);
            this.pnlDauTuContainer.TabIndex = 0;
            // 
            // expDT
            // 
            this.expDT.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expDT.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expDT.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expDT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expDT.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem4.Key = "tkNhapKhau_DT";
            explorerBarItem4.Text = "Nhập khẩu";
            explorerBarItem5.Key = "tkXuatKhau_DT";
            explorerBarItem5.Text = "Xuất khẩu";
            explorerBarGroup3.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem4,
            explorerBarItem5});
            explorerBarGroup3.Key = "grpToKhai";
            explorerBarGroup3.Text = "Tờ khai";
            this.expDT.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup3});
            this.expDT.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expDT.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expDT.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expDT.ImageSize = new System.Drawing.Size(16, 16);
            this.expDT.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expDT.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expDT.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expDT.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expDT.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expDT.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expDT.Location = new System.Drawing.Point(0, 0);
            this.expDT.Name = "expDT";
            this.expDT.Size = new System.Drawing.Size(209, 335);
            this.expDT.TabIndex = 2;
            this.expDT.Text = "explorerBar1";
            this.expDT.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expDT.VisualStyleManager = this.vsmMain;
            // 
            // pnlSend
            // 
            this.pnlSend.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlSend.Icon")));
            this.pnlSend.InnerContainer = this.pnlSendContainer;
            this.pnlSend.Location = new System.Drawing.Point(0, 0);
            this.pnlSend.Name = "pnlSend";
            this.pnlSend.Size = new System.Drawing.Size(209, 335);
            this.pnlSend.TabIndex = 4;
            this.pnlSend.Text = "Khai báo / Theo dõi tờ khai";
            // 
            // pnlSendContainer
            // 
            this.pnlSendContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlSendContainer.Name = "pnlSendContainer";
            this.pnlSendContainer.Size = new System.Drawing.Size(209, 335);
            this.pnlSendContainer.TabIndex = 0;
            // 
            // ilSmall
            // 
            this.ilSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilSmall.ImageStream")));
            this.ilSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.ilSmall.Images.SetKeyName(0, "");
            this.ilSmall.Images.SetKeyName(1, "");
            this.ilSmall.Images.SetKeyName(2, "");
            this.ilSmall.Images.SetKeyName(3, "");
            this.ilSmall.Images.SetKeyName(4, "");
            this.ilSmall.Images.SetKeyName(5, "");
            this.ilSmall.Images.SetKeyName(6, "");
            this.ilSmall.Images.SetKeyName(7, "");
            this.ilSmall.Images.SetKeyName(8, "");
            this.ilSmall.Images.SetKeyName(9, "");
            this.ilSmall.Images.SetKeyName(10, "");
            this.ilSmall.Images.SetKeyName(11, "");
            this.ilSmall.Images.SetKeyName(12, "");
            this.ilSmall.Images.SetKeyName(13, "");
            this.ilSmall.Images.SetKeyName(14, "");
            this.ilSmall.Images.SetKeyName(15, "");
            this.ilSmall.Images.SetKeyName(16, "");
            this.ilSmall.Images.SetKeyName(17, "");
            this.ilSmall.Images.SetKeyName(18, "");
            this.ilSmall.Images.SetKeyName(19, "");
            this.ilSmall.Images.SetKeyName(20, "");
            this.ilSmall.Images.SetKeyName(21, "");
            this.ilSmall.Images.SetKeyName(22, "");
            this.ilSmall.Images.SetKeyName(23, "");
            this.ilSmall.Images.SetKeyName(24, "");
            this.ilSmall.Images.SetKeyName(25, "");
            this.ilSmall.Images.SetKeyName(26, "");
            this.ilSmall.Images.SetKeyName(27, "");
            this.ilSmall.Images.SetKeyName(28, "");
            this.ilSmall.Images.SetKeyName(29, "");
            this.ilSmall.Images.SetKeyName(30, "");
            this.ilSmall.Images.SetKeyName(31, "");
            this.ilSmall.Images.SetKeyName(32, "");
            this.ilSmall.Images.SetKeyName(33, "");
            this.ilSmall.Images.SetKeyName(34, "");
            // 
            // ilMedium
            // 
            this.ilMedium.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilMedium.ImageStream")));
            this.ilMedium.TransparentColor = System.Drawing.Color.Transparent;
            this.ilMedium.Images.SetKeyName(0, "");
            this.ilMedium.Images.SetKeyName(1, "");
            this.ilMedium.Images.SetKeyName(2, "");
            this.ilMedium.Images.SetKeyName(3, "");
            // 
            // ilLarge
            // 
            this.ilLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilLarge.ImageStream")));
            this.ilLarge.TransparentColor = System.Drawing.Color.Transparent;
            this.ilLarge.Images.SetKeyName(0, "");
            this.ilLarge.Images.SetKeyName(1, "");
            this.ilLarge.Images.SetKeyName(2, "");
            this.ilLarge.Images.SetKeyName(3, "");
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 503);
            this.statusBar.Name = "statusBar";
            uiStatusBarPanel4.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            uiStatusBarPanel4.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel4.Key = "DoanhNghiep";
            uiStatusBarPanel4.ProgressBarValue = 0;
            uiStatusBarPanel4.Width = 342;
            uiStatusBarPanel5.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            uiStatusBarPanel5.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel5.Key = "HaiQuan";
            uiStatusBarPanel5.ProgressBarValue = 0;
            uiStatusBarPanel5.ToggleKeyValue = Janus.Windows.UI.StatusBar.ToggleKeyValue.NumLock;
            uiStatusBarPanel5.Width = 342;
            uiStatusBarPanel6.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel6.Key = "System";
            uiStatusBarPanel6.PanelType = Janus.Windows.UI.StatusBar.StatusBarPanelType.ProgressBar;
            uiStatusBarPanel6.ProgressBarValue = 0;
            this.statusBar.Panels.AddRange(new Janus.Windows.UI.StatusBar.UIStatusBarPanel[] {
            uiStatusBarPanel4,
            uiStatusBarPanel5,
            uiStatusBarPanel6});
            this.statusBar.PanelsBorderColor = System.Drawing.SystemColors.ControlDark;
            this.statusBar.Size = new System.Drawing.Size(809, 23);
            this.statusBar.TabIndex = 7;
            this.statusBar.VisualStyleManager = this.vsmMain;
            // 
            // cmdThoat3
            // 
            this.cmdThoat3.Key = "cmdThoat";
            this.cmdThoat3.Name = "cmdThoat3";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(809, 526);
            this.Controls.Add(this.uiPanel0);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Đồng bộ dữ liệu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.statusBar, 0);
            this.Controls.SetChildIndex(this.uiPanel0, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expDongBo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSXXK)).EndInit();
            this.pnlSXXK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlGiaCong)).EndInit();
            this.pnlGiaCong.ResumeLayout(false);
            this.pnlGiaCongContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expGiaCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKinhDoanh)).EndInit();
            this.pnlKinhDoanh.ResumeLayout(false);
            this.pnlKinhDoanhContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expKD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDauTu)).EndInit();
            this.pnlDauTu.ResumeLayout(false);
            this.pnlDauTuContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSend)).EndInit();
            this.pnlSend.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private UICommand cmdThoat3;
        private UIPanelInnerContainer pnlDauTuContainer;
        private ExplorerBar expDT;
        private UIPanel pnlSend;
        private UIPanelInnerContainer pnlSendContainer;
        private NotifyIcon notifyIcon1;
        private BackgroundWorker backgroundWorker1;
        private UICommand Command0;
        private UICommand cmdAbout1;
        private UICommand cmdHelp;
        private UICommand cmdAbout;
        private UICommand Command1;
        private UICommand cmd20071;
        private UICommand cmd2007;
        private UICommand cmd2003;
        private UICommand cmd20031;
        private UICommand cmdBackUp;
        private UICommand cmdRestore;
        private UICommand ThongSoKetNoi;
        private UICommand TLThongTinDNHQ;
        private UICommand cmdThietLapIn;
        private UICommand cmdExportExccel;
        private UICommand cmdExportExcel1;
        private UICommand cmdImportExcel;
        private UICommand cmdDongBoPhongKhai;
        private UICommand cmdImportExcel1;
        private UICommand cmdExportExccel1;
        private UICommand cmdCauHinh1;
        private UICommand cmdCauHinh;
        private UICommand ThongSoKetNoi1;
        private UICommand TLThongTinDNHQ1;
        private UICommand cmdCauHinhToKhai;
        public Janus.Windows.UI.StatusBar.UIStatusBar statusBar;
        private UICommand QuanTri;
        private UICommand QuanLyNguoiDung1;
        private UICommand QuanLyNguoiDung;
        private UICommand QuanLyNhom;
        private UICommand LoginUser1;
        private UICommand LoginUser;
        private UICommand cmdChangePass1;
        private UICommand cmdChangePass;
        private UICommand cmdDoiMatKhauHQ;
        private UICommand cmdCuaSo;
        private UICommand cmdCloseAll1;
        private UICommand cmdCloseAllButThis1;
        private UICommand cmdCloseAll;
        private UICommand cmdCloseAllButThis;
        private UIPanelGroup uiPanel0;
        private UIPanel uiPanel1;
        private UIPanelInnerContainer uiPanel1Container;
        private UICommand Separator1;
        private UICommand Separator3;
        private UICommand Separator4;
        private UICommand cmdAutoUpdate;
        private UICommand cmdNhapXML;
        private UICommand cmdNPL1;
        private UICommand cmdSP1;
        private UICommand cmdDM1;
        private UICommand cmdTK1;
        private UICommand cmdNPL;
        private UICommand cmdSP;
        private UICommand cmdDM;
        private UICommand cmdTK;
        private OpenFileDialog openFileDialog1;
        private SaveFileDialog saveFileDialog1;
        private UICommand cmdXuatDuLieu;
        private UICommand cmdXuatNPL;
        private UICommand cmdXuatDinhMuc;
        private UICommand cmdXuatSanPham;
        private UICommand cmdXuatToKhai;
        private UICommand cmdVN;
        private UICommand cmdEL;
        private UICommand cmdVN1;
        private UICommand cmdEL1;
        private UICommand cmdHUNGNPL;
        private UICommand cmdHUNGNPL1;
        private UICommand cmdHUNGSP1;
        private UICommand cmdHUNGDM1;
        private UICommand cmdHUNGSP;
        private UICommand cmdHUNGDM;
        private UICommand cmdHUNGTK;
        private UICommand cmdHUNGTK1;
        private UICommand cmdActivate;
        private System.Windows.Forms.Timer timer1;
        private UICommand cmdXuatDuLieuDN;
        private UICommand cmdXuatNPLDN;
        private UICommand cmdXuatSPDN;
        private UICommand cmXuat;
        private UICommand cmdXuatNPLDN1;
        private UICommand cmdXuatSPDN1;
        private UICommand cmdXuatDMDN1;
        private UICommand cmdXuatTKDN1;
        private UICommand cmdXuatDMDN;
        private UICommand cmdXuatTKDN;
        private UICommand cmdNhapDuLieu;
        private UICommand cmdNhapNPL1;
        private UICommand cmdNhapSanPham1;
        private UICommand cmdNhapDinhMuc1;
        private UICommand cmdNhapToKhai1;
        private UICommand cmdNhapNPL;
        private UICommand cmdNhapSanPham;
        private UICommand cmdNhapDinhMuc;
        private UICommand cmdNhapToKhai;
        private UICommand QuanlyMess;
        private UICommand cmdConfig;
        private UICommand cmdHeThong1;
        private UICommand cmdCuaSo1;
        private UICommand Command01;
        private UICommand QuanTri1;
        private UICommand NhacNho;
        private UICommand DongBoDuLieu;
        private UICommand cmdImport;
        private UICommand cmdImportNPL1;
        private UICommand cmdImportSP1;
        private UICommand cmdImportDM1;
        private UICommand cmdImportToKhai1;
        private UICommand cmdImportHangHoa1;
        private UICommand cmdNPLNhapTon1;
        private UICommand cmdImportNPL;
        private UICommand cmdImportSP;
        private UICommand cmdImportDM;
        private UICommand cmdImportTTDM;
        private UICommand cmdImportToKhai;
        private UICommand cmdImportHangHoa;
        private UICommand cmdNPLNhapTon;
        private UICommand cmdDanhMuc;
        private UICommand cmdHaiQuan1;
        private UICommand cmdNuoc1;
        private UICommand cmdMaHS1;
        private UICommand cmdNguyenTe1;
        private UICommand cmdDVT1;
        private UICommand cmdPTTT1;
        private UICommand cmdPTVT1;
        private UICommand cmdDKGH1;
        private UICommand cmdCuaKhau1;
        private UICommand cmdMaHS;
        private UICommand cmdNuoc;
        private UICommand cmdHaiQuan;
        private UICommand cmdNguyenTe;
        private UICommand cmdDVT;
        private UICommand cmdPTTT;
        private UICommand cmdPTVT;
        private UICommand cmdDKGH;
        private UICommand cmdCuaKhau;
        private ExplorerBar expDongBo;
    }
}
