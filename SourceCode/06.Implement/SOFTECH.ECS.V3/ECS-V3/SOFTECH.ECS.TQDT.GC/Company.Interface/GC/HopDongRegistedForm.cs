﻿using System;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using Infragistics.Excel;
using System.Collections.Generic;


namespace Company.Interface.GC
{
    public partial class HopDongRegistedForm : BaseForm
    {
        public HopDong HopDongSelected = new HopDong();
        public bool IsBrowseForm = false;
        public bool IsBrowseFormCT = false;
        public HopDongRegistedForm()
        {
            InitializeComponent();
        }
        //TODO: Cao Huu Tu updated 26-08-2011:tao VietNam HDcollection
        private List<HopDong> hdcollection = new List<HopDong>();


        public void BindData()
        {
            hdcollection = Company.GC.BLL.Globals.GetDanhSachHopDongDaDangKy(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, txtSoHopDong.Text.Trim(), Convert.ToInt32(txtNamDangKy.Value));
            dgList.DataSource = hdcollection;
        }
        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
        }

        private void setDataToComboUserKB()
        {
            DataTable dt = new Company.QuanTri.User().SelectAll().Tables[0];
            DataRow dr = dt.NewRow();
            dr["USER_NAME"] = -1;
            dr["HO_TEN"] = "--Tất cả--";
            dt.Rows.InsertAt(dr, 0);
            cbUserKB.DataSource = dt;
            cbUserKB.DisplayMember = dt.Columns["HO_TEN"].ToString();
            cbUserKB.ValueMember = dt.Columns["USER_NAME"].ToString();
        }

        //-----------------------------------------------------------------------------------------

        private void HopDongRegistedForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            txtNamDangKy.Text = "";
            BindData();
            setDataToComboUserKB();
            cbUserKB.SelectedIndex = 0;
            //dgList.Tables[0].Columns["MaDoanhNghiep"].Visible = GlobalSettings.DAI_LY_TTHQ;
        }

        //-----------------------------------------------------------------------------------------


        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                this.HopDongSelected = (HopDong)e.Row.DataRow;
                HopDongRegistedDetailForm f = new HopDongRegistedDetailForm();
                f.HD = this.HopDongSelected;
                f.Show();

            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["NgayGiaHan"].Value != DBNull.Value)
                {
                    if (Convert.ToDateTime(e.Row.Cells["NgayGiaHan"].Value).Year == 1900)
                    {
                        e.Row.Cells["NgayGiaHan"].Text = "";
                    }
                }
                if (e.Row.Cells["NgayDangKy"].Value != DBNull.Value)
                {
                    if (Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value).Year == 1900)
                    {
                        e.Row.Cells["NgayDangKy"].Text = "";
                    }
                }
            }

            if (Convert.ToInt32(e.Row.Cells["TrangThaiThanhKhoan"].Value) == 0)
                e.Row.Cells["TrangThaiThanhKhoan"].Text = "Chưa thanh khoản";
            else if (Convert.ToInt32(e.Row.Cells["TrangThaiThanhKhoan"].Value) == 1)
                e.Row.Cells["TrangThaiThanhKhoan"].Text = "Đã chạy thanh khoản";
        }


        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (cbUserKB.SelectedValue != null && cbUserKB.SelectedIndex != 0)
            {
                if (txtSoHopDong.TextLength > 0)
                    dgList.DataSource = new HopDong().GetHopDongFromUserNameKhaiBao(cbUserKB.SelectedItem.Value.ToString(), txtSoHopDong.Text.Trim()).Tables[0];
                else
                    dgList.DataSource = new HopDong().GetHopDongFromUserNameKhaiBao(cbUserKB.SelectedItem.Value.ToString()).Tables[0];
            }
            else
                BindData();
        }

        //TODO: updated 27-08-2011: bổ sung nút xuất EXCEL
        private void bttXuatExcel_Click(object sender, EventArgs e)
        {
            int stt = 1;
            //Lấy đường dẫn file gốc
            string destFile = "";
            string fileName = "DanhSach_HopDong_DaDangKy.xls";
            string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

            //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
            try
            {
                Workbook workBook = Workbook.Load(sourcePath);
                Worksheet workSheet = workBook.Worksheets[0];

                int row = 1;

                foreach (HopDong hopdong in hdcollection)
                {
                    workSheet.Rows[row].Cells[0].Value = stt;
                    workSheet.Rows[row].Cells[0].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[0].CellFormat.Font.Height = 10 * 20;
                    workSheet.Rows[row].Cells[0].CellFormat.Alignment = HorizontalCellAlignment.Center;

                    workSheet.Rows[row].Cells[1].Value = hopdong.SoHopDong.ToString();
                    workSheet.Rows[row].Cells[1].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[1].CellFormat.Font.Height = 10 * 20;


                    workSheet.Rows[row].Cells[2].Value = hopdong.NgayKy.ToString("dd-MM-yyyy");
                    workSheet.Rows[row].Cells[2].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[2].CellFormat.Font.Height = 10 * 20;
                    workSheet.Rows[row].Cells[2].CellFormat.Alignment = HorizontalCellAlignment.Center;

                    workSheet.Rows[row].Cells[3].Value = hopdong.NgayDangKy.ToString("dd-MM-yyyy");
                    workSheet.Rows[row].Cells[3].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[3].CellFormat.Font.Height = 10 * 20;
                    workSheet.Rows[row].Cells[3].CellFormat.Alignment = HorizontalCellAlignment.Center;

                    workSheet.Rows[row].Cells[4].Value = hopdong.NgayHetHan.ToString("dd-MM-yyyy");
                    workSheet.Rows[row].Cells[4].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[4].CellFormat.Font.Height = 10 * 20;
                    workSheet.Rows[row].Cells[4].CellFormat.Alignment = HorizontalCellAlignment.Center;

                    if (hopdong.NgayGiaHan.Year != 1900)
                    {
                        workSheet.Rows[row].Cells[5].Value = hopdong.NgayGiaHan.ToString("dd-MM-yyyy");
                        workSheet.Rows[row].Cells[5].CellFormat.Font.Name = "Times New Roman";
                        workSheet.Rows[row].Cells[5].CellFormat.Font.Height = 10 * 20;
                        workSheet.Rows[row].Cells[5].CellFormat.Alignment = HorizontalCellAlignment.Center;
                    }

                    workSheet.Rows[row].Cells[6].Value = hopdong.NuocThue_ID.ToString();
                    workSheet.Rows[row].Cells[6].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[6].CellFormat.Font.Height = 10 * 20;
                    workSheet.Rows[row].Cells[6].CellFormat.Alignment = HorizontalCellAlignment.Center;

                    workSheet.Rows[row].Cells[7].Value = hopdong.NguyenTe_ID.ToString();
                    workSheet.Rows[row].Cells[7].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[7].CellFormat.Font.Height = 10 * 20;
                    workSheet.Rows[row].Cells[7].CellFormat.Alignment = HorizontalCellAlignment.Center;

                    workSheet.Rows[row].Cells[8].Value = hopdong.NuocThue_ID.ToString();
                    workSheet.Rows[row].Cells[8].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[8].CellFormat.Font.Height = 10 * 20;
                    workSheet.Rows[row].Cells[8].CellFormat.Alignment = HorizontalCellAlignment.Center;

                    workSheet.Rows[row].Cells[9].Value = hopdong.DiaChiDoiTac.ToString();
                    workSheet.Rows[row].Cells[9].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[row].Cells[9].CellFormat.Font.Height = 10 * 20;

                    row++;
                    stt++;
                }
                //chỉ đúng đường dẫn cần lưu                
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                string dateNow = DateTime.Now.ToShortDateString().Replace("/", "");
                saveFileDialog1.FileName = "Danh sach hop dong da dang ky " + DateTime.Now.ToString("dd-MM-yyyy");
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    destFile = saveFileDialog1.FileName;
                    //Ghi nội dung file gốc và file cần lưu
                    try
                    {
                        byte[] sourceFile = Globals.ReadFile(sourcePath);
                        Globals.WriteFile(destFile, sourceFile);
                        workBook.Save(destFile);
                        ShowMessage("Save file Thành công", false);
                        System.Diagnostics.Process.Start(destFile);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(ex.Message, false);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi: Không xuất Excel.\t\nChi tiết lỗi: " + ex.Message, false);
                return;
            }
        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }

    }
}

