﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class Material
    {
        [XmlElement("Commodity")]
        public List<Commodity> Commodity { get; set; }
        [XmlElement("GoodsMeasure")]
        public List<GoodsMeasure> GoodsMeasure { get; set; }
        [XmlElement("DetailMaterialExport")]
        public List<DetailMaterialExport> DetailMaterialExport { get; set; }
    }
}
