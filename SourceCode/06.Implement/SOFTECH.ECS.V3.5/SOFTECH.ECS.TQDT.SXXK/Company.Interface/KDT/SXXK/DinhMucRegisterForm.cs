﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.BLL.KDT.SXXK;
using Company.Interface.KDT.SXXK;

namespace Company.Interface.KDT.GC
{
    public partial class DinhMucGCRegisterForm : Company.Interface.BaseForm
    {
        public KDT_LenhSanXuat_SP SP = new KDT_LenhSanXuat_SP();
        public SXXK_DinhMuc DM = new SXXK_DinhMuc();
        public Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu NPL = new Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu();
        public bool isAdd = true;
        public DinhMucGCRegisterForm()
        {
            InitializeComponent();
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void DinhMucGCRegisterForm_Load(object sender, EventArgs e)
        {
            BindDataLenhSanXuat();
        }
        private void LoadData()
        {
            try
            {
                cbbMaNPL.DataSource = Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                cbbMaNPL.DisplayMember = "Ma";
                cbbMaNPL.ValueMember = "Ma";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataSP()
        {
            try
            {
                grListSP.Refetch();
                grListSP.DataSource = KDT_LenhSanXuat_SP.SelectCollectionDynamic("LenhSanXuat_ID = " + cbbLenhSanXuat.Value, "");
                grListSP.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataLenhSanXuat()
        {
            try
            {
                cbbLenhSanXuat.DataSource = KDT_LenhSanXuat.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "ID");
                cbbLenhSanXuat.DisplayMember = "SoLenhSanXuat";
                cbbLenhSanXuat.ValueMember = "ID";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataNPL()
        {
            try
            {
                List<SXXK_DinhMuc> DMCollection = new List<SXXK_DinhMuc>();
                DMCollection = SXXK_DinhMuc.SelectCollectionDynamic(" MaSanPham ='" + SP.MaSanPham + "' AND LenhSanXuat_ID =" + cbbLenhSanXuat.Value + "", "");
                foreach (SXXK_DinhMuc item in DMCollection)
                {
                    if (item.TyLeHaoHut > 0)
                    {
                        item.DinhMucSuDung = item.DinhMucSuDung + item.DinhMucSuDung * item.TyLeHaoHut / 100;
                        item.TyLeHaoHut = 0;
                    }
                }
                dgListNPL.Refetch();
                dgListNPL.DataSource = DMCollection;
                dgListNPL.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                BindDataLenhSanXuat();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                grListSP.Refetch();
                grListSP.DataSource = KDT_LenhSanXuat_SP.SelectCollectionDynamic(" Ma LIKE '%" + txtMaSP.Text + "%' AND KDT_LenhSanXuat_SP =" + cbbLenhSanXuat.Value + "", "");
                grListSP.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaNPL_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaNPL.Value != null)
                {
                    List<Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu> NPLCollection = Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                    foreach (Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu item in NPLCollection)
                    {
                        if (item.Ma == cbbMaNPL.Value.ToString())
                        {
                            txtTenNPL.Text = item.Ten.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnCoppy_Click(object sender, EventArgs e)
        {
            try
            {
                if (SP == null)
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN SẢN PHẨM ĐỂ SAO CHÉP ĐỊNH MỨC", false);
                    return;
                }
                if (cbbLenhSanXuat.Value.ToString().Trim() == "")
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN LỆNH SẢN XUẤT CỦA SẢN PHẨM .", false);
                    return;
                }
                List<Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu> NPLCollection = Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                SaoChepDinhMucSXXKForm f = new SaoChepDinhMucSXXKForm();
                f.SP = SP;
                f.ShowDialog(this);
                if (f.DMCollectionCopy.Count > 0)
                {
                    foreach (Company.BLL.SXXK.DinhMuc item in f.DMCollectionCopy)
                    {
                        DM = new SXXK_DinhMuc();
                        DM.MaSanPham = SP.MaSanPham;
                        DM.TenSanPham = SP.TenSanPham;
                        DM.MaNguyenPhuLieu = item.MaNguyenPhuLieu.ToString();
                        foreach (Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu ite in NPLCollection)
                        {
                            if (ite.Ma ==item.MaNguyenPhuLieu)
                            {
                                DM.TenNPL = ite.Ten.ToString();
                                break;
                            }   
                        }
                        DM.DinhMucSuDung = item.DinhMucSuDung;
                        DM.TyLeHaoHut = item.TyLeHaoHut;
                        DM.LenhSanXuat_ID = Convert.ToInt64(cbbLenhSanXuat.Value);
                        DM.SoTiepNhan = Convert.ToDecimal(txtSoTiepNhan.Text.ToString());
                        DM.NgayTiepNhan = clcNgayTiepNhan.Value;
                        DM.GuidString = txtGuidString.Text.ToString();
                        DM.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        DM.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        DM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        DM.GhiChu = String.Empty;
                        DM.InsertUpdate();
                    }
                    ShowMessageTQDT("Sao chép thành công", false);
                }
                BindDataSP();
                BindDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDeleteSP_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                List<KDT_LenhSanXuat_SP> ItemColl = new List<KDT_LenhSanXuat_SP>();
                if (grListSP.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_LenhSanXuat_SP)i.GetRow().DataRow);
                        }
                    }
                    foreach (KDT_LenhSanXuat_SP item in ItemColl)
                    {
                        List<SXXK_DinhMuc> DMCollection = SXXK_DinhMuc.SelectCollectionDynamic("MaSanPham ='" + item.MaSanPham + "' AND LenhSanXuat_ID = " + cbbLenhSanXuat.Value + "", "");
                        foreach (SXXK_DinhMuc itemss in DMCollection)
                        {
                            itemss.Delete();
                        }
                    }
                    BindDataSP();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetData()
        {
            try
            {
                DM.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                DM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                DM.MaSanPham = SP.MaSanPham;
                DM.TenSanPham = SP.TenSanPham;
                DM.MaNguyenPhuLieu = cbbMaNPL.Value.ToString();
                DM.TenNPL = txtTenNPL.Text;
                DM.DinhMucSuDung = Convert.ToDecimal(txtDinhMuc.Text.ToString());
                DM.LenhSanXuat_ID = Convert.ToInt64(cbbLenhSanXuat.Value);
                DM.SoTiepNhan = Convert.ToDecimal(txtSoTiepNhan.Text.ToString());
                DM.NgayTiepNhan = clcNgayTiepNhan.Value;
                DM.GuidString = txtGuidString.Text.ToString();
                DM.GhiChu = String.Empty;
                string Status = DM.TrangThaiXuLy.ToString();
                switch (Status)
                {
                    case "1":
                        DM.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                        break;
                    case "0":
                        DM.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void setCommandStatus()
        {
            if (DM.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || DM.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                txtSoTiepNhan.Text = DM.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = DM.NgayTiepNhan;
                txtGuidString.Text = DM.GuidString.ToString().ToUpper();
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (DM.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || DM.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {

                clcNgayTiepNhan.Value = DateTime.Now;
                lblTrangThai.Text = DM.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                txtGuidString.Text = DM.GuidString.ToString().ToUpper();
                this.OpenType = OpenFormType.Edit;
            }
            else if (DM.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                txtSoTiepNhan.Text = DM.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = DM.NgayTiepNhan;
                txtGuidString.Text = DM.GuidString.ToString().ToUpper();
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
            }
            else if (DM.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || DM.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || DM.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {


                lblTrangThai.Text = setText("Đã khai báo", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (DM.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                txtSoTiepNhan.Text = DM.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = DM.NgayTiepNhan;
                txtGuidString.Text = DM.GuidString.ToString().ToUpper();
                lblTrangThai.Text = setText("Đã sửa , Chưa khai báo", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (DM.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {

                txtSoTiepNhan.Text = DM.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = DM.NgayTiepNhan;
                txtGuidString.Text = DM.GuidString.ToString().ToUpper();
                lblTrangThai.Text = setText("Đang hủy", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }

        }
        private void SetData()
        {
            try
            {
                cbbMaNPL.Value = DM.MaNguyenPhuLieu;
                txtTenNPL.Text = DM.TenNPL;
                txtDinhMuc.Text = DM.DinhMucSuDung.ToString();
                cbbLenhSanXuat.Value = DM.LenhSanXuat_ID;
                cbbMaNPL_ValueChanged(null,null);
                //txtSoTiepNhan.Text = DM.SoTiepNhan.ToString();
                //clcNgayTiepNhan.Value = DM.NgayTiepNhan.Year == 1900 ? DateTime.Now : DM.NgayTiepNhan;
                //txtGuidString.Text = DM.GuidString.ToString().ToUpper();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateChoose(cbbMaNPL, errorProvider, "MÃ NGUYÊN PHỤ LIỆU", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNPL, errorProvider, "TÊN/MÔ TẢ NGUYÊN PHỤ LIỆU", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDinhMuc, errorProvider, "ĐỊNH MỨC SỬ DỤNG", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbbLenhSanXuat, errorProvider, "LỆNH SẢN XUẤT", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetData();
                DM.InsertUpdate();
                DM = new SXXK_DinhMuc();
                cbbMaNPL.Text = String.Empty;
                txtTenNPL.Text = String.Empty;
                txtDinhMuc.Text = String.Empty;
                BindDataNPL();
                BindDataSP();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgListNPL.SelectedItems;
                List<SXXK_DinhMuc> ItemColl = new List<SXXK_DinhMuc>();
                if (dgListNPL.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((SXXK_DinhMuc)i.GetRow().DataRow);
                        }
                    }
                    foreach (SXXK_DinhMuc item in ItemColl)
                    {
                        item.Delete();
                    }
                    BindDataNPL();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void grListSP_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SP = new KDT_LenhSanXuat_SP();
                        SP = (KDT_LenhSanXuat_SP)i.GetRow().DataRow;
                        Janus.Windows.GridEX.GridEXFormatStyle style = new Janus.Windows.GridEX.GridEXFormatStyle();
                        List<SXXK_DinhMuc> DMCollection = new List<SXXK_DinhMuc>();
                        DMCollection = SXXK_DinhMuc.SelectCollectionDynamic("MaSanPham ='" + SP.MaSanPham + "' AND LenhSanXuat_ID =" + cbbLenhSanXuat.Value + "", "");
                        if (DMCollection.Count == 0)
                        {
                            style.BackColor = Color.GreenYellow;
                            i.GetRow().Cells["MaSanPham"].FormatStyle = style;
                        }
                        else
                        {
                            txtSoTiepNhan.Text = DMCollection[0].SoTiepNhan.ToString();
                            clcNgayTiepNhan.Value = DMCollection[0].NgayTiepNhan.Year == 1900 || DMCollection[0].NgayTiepNhan.Year == 1 ? DateTime.Now : DMCollection[0].NgayTiepNhan;
                            txtGuidString.Text = String.IsNullOrEmpty(DMCollection[0].GuidString) ? String.Empty : DMCollection[0].GuidString.ToString().ToUpper();

                            string TrangThai = DMCollection[0].TrangThaiXuLy.ToString();
                            switch (TrangThai)
                            {
                                case "-1":
                                    style.BackColor = Color.Yellow;
                                    i.GetRow().Cells["MaSanPham"].FormatStyle = style;
                                    lblTrangThai.Text = TrangThaiXuLy.strCHUAKHAIBAO;
                                    break;
                                case "0":
                                    style.BackColor = Color.Transparent;
                                    i.GetRow().Cells["MaSanPham"].FormatStyle = style;
                                    lblTrangThai.Text = TrangThaiXuLy.strCHODUYET;
                                    break;
                                case "1":
                                    style.BackColor = Color.White;
                                    i.GetRow().Cells["MaSanPham"].FormatStyle = style;
                                    lblTrangThai.Text = TrangThaiXuLy.strDADUYET;
                                    break;
                                case "5":
                                    style.BackColor = Color.Violet;
                                    i.GetRow().Cells["MaSanPham"].FormatStyle = style;
                                    lblTrangThai.Text = TrangThaiXuLy.strSUATKDADUYET;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                BindDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListNPL_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgListNPL.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DM = new SXXK_DinhMuc();
                        DM = (SXXK_DinhMuc)i.GetRow().DataRow;
                    }
                }
                SetData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListSP_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                    SP = new KDT_LenhSanXuat_SP();
                    SP = (KDT_LenhSanXuat_SP)e.Row.DataRow;
                    Janus.Windows.GridEX.GridEXFormatStyle style = new Janus.Windows.GridEX.GridEXFormatStyle();
                    List<SXXK_DinhMuc> DMCollection = new List<SXXK_DinhMuc>();
                    DMCollection = SXXK_DinhMuc.SelectCollectionDynamic(" MaSanPham ='" + SP.MaSanPham + "' AND LenhSanXuat_ID =" + cbbLenhSanXuat.Value + "", "");
                    if (DMCollection.Count == 0)
                    {
                        style.BackColor = Color.GreenYellow;
                        e.Row.Cells["MaSanPham"].FormatStyle = style;
                    }
                    else
                    {
                        string TrangThai =  DMCollection[0].TrangThaiXuLy.ToString();
                        switch (TrangThai)
                        {
                            case "-1":
                                style.BackColor = Color.Yellow;
                                e.Row.Cells["MaSanPham"].FormatStyle = style;
                                break;
                            case "0":
                                style.BackColor = Color.Transparent;
                                e.Row.Cells["MaSanPham"].FormatStyle = style;
                                break;
                            case "1":
                                style.BackColor = Color.White;
                                e.Row.Cells["MaSanPham"].FormatStyle = style;
                                break;
                            case "5":
                                style.BackColor = Color.Violet;
                                e.Row.Cells["MaSanPham"].FormatStyle = style;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListNPL_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    DM = new SXXK_DinhMuc();
                    DM = (SXXK_DinhMuc)e.Row.DataRow;
                    SetData();
                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbLenhSanXuat_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                BindDataSP();
                LoadData();
                if (cbbLenhSanXuat.Value != null)
                {
                    KDT_LenhSanXuat lenhSX = new KDT_LenhSanXuat();
                    lenhSX = KDT_LenhSanXuat.Load(Convert.ToInt64(cbbLenhSanXuat.Value));
                    if (lenhSX.ID > 0)
                    {
                        clcTuNgay.Value = lenhSX.TuNgay;
                        clcDenNgay.Value = lenhSX.DenNgay;
                        txtPO.Text = lenhSX.SoDonHang;
                        cbbTinhTrang.SelectedValue = lenhSX.TinhTrang;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (cbbLenhSanXuat.Value.ToString().Trim() == "")
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN LỆNH SẢN XUẤT CỦA SẢN PHẨM .", false);
                    return;
                }
                List<KDT_LenhSanXuat_SP> SPCollection = new List<KDT_LenhSanXuat_SP>();
                SPCollection = KDT_LenhSanXuat_SP.SelectCollectionDynamic("LenhSanXuat_ID = " + cbbLenhSanXuat.Value, "");
                if (SPCollection.Count == 0)
                {
                    ShowMessage("LỆNH SẢN XUẤT NÀY CHƯA CÓ DANH SÁCH SẢN PHẨM . DOANH NGHIỆP HÃY CẬP NHẬT DANH SÁCH SẢN PHẨM CHO LỆNH SẢN XUẤT NÀY.", false);
                    return;
                }
                ImportKDTDMForm f = new ImportKDTDMForm();
                DinhMucDangKy dmDangKy = new DinhMucDangKy();
                f.dmDangKy = dmDangKy;
                f.ShowDialog();
                if (f.dmDangKy.DMCollection.Count > 0)
                {
                    foreach (Company.BLL.KDT.SXXK.DinhMuc item in f.dmDangKy.DMCollection)
                    {
                        foreach (KDT_LenhSanXuat_SP ite in SPCollection)
                        {
                            if (ite.MaSanPham == item.MaSanPham)
                            {
                                DM = new SXXK_DinhMuc();
                                DM.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                                DM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                                DM.MaSanPham = item.MaSanPham;
                                DM.TenSanPham = item.TenSP;
                                DM.MaNguyenPhuLieu = item.MaNguyenPhuLieu.ToString();
                                DM.TenNPL = item.TenNPL;
                                DM.DinhMucSuDung = item.DinhMucSuDung;
                                DM.TyLeHaoHut = item.TyLeHaoHut;
                                DM.LenhSanXuat_ID = Convert.ToInt64(cbbLenhSanXuat.Value);
                                DM.SoTiepNhan = Convert.ToDecimal(txtSoTiepNhan.Text.ToString());
                                DM.NgayTiepNhan = clcNgayTiepNhan.Value;
                                DM.GuidString = txtGuidString.Text.ToString();
                                DM.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                DM.GhiChu = "";
                                DM.InsertUpdate();
                            }
                        }
                    }
                    ShowMessageTQDT("NHẬP ĐỊNH MỨC TỪ EXCEL THÀNH CÔNG ", false);
                }
                BindDataSP();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnIpExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (SP == null || String.IsNullOrEmpty(SP.MaSanPham.ToString()))
                {
                    ShowMessage("DOANH NGHIỆP CHƯA CHỌN SẢN PHẨM ĐỂ NHẬP ĐỊNH MỨC TỪ FILE EXCEL", false);
                    return;
                }
                ImportKDTDMForm f = new ImportKDTDMForm();
                DinhMucDangKy dmDangKy = new DinhMucDangKy();
                f.dmDangKy = dmDangKy;
                f.ShowDialog();
                if (f.dmDangKy.DMCollection.Count > 0)
                {
                    foreach (Company.BLL.KDT.SXXK.DinhMuc item in f.dmDangKy.DMCollection)
                    {
                        // KIỂM TRA CHỈ NHẬP EXCEL CHO MÃ SP ĐÃ CHỌN BỎ QUA CÁC MÃ KHÁC NẾU CÓ
                        if (item.MaSanPham == SP.MaSanPham)
                        {
                            DM = new SXXK_DinhMuc();
                            DM.MaSanPham = item.MaSanPham;
                            DM.TenSanPham = item.TenSP;
                            DM.MaNguyenPhuLieu = item.MaNguyenPhuLieu.ToString();
                            DM.TenNPL = item.TenNPL;
                            DM.DinhMucSuDung = item.DinhMucSuDung;
                            DM.TyLeHaoHut = item.TyLeHaoHut;
                            DM.LenhSanXuat_ID = Convert.ToInt64(cbbLenhSanXuat.Value);
                            DM.SoTiepNhan = Convert.ToDecimal(txtSoTiepNhan.Text.ToString());
                            DM.NgayTiepNhan = clcNgayTiepNhan.Value;
                            DM.GuidString = txtGuidString.Text.ToString();
                            DM.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                            DM.GhiChu = "";
                            DM.InsertUpdate();
                        }
                    }
                    ShowMessageTQDT("NHẬP ĐỊNH MỨC TỪ EXCEL THÀNH CÔNG ", false);
                }
                BindDataSP();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                KDT_LenhSanXuat lenhSX = new KDT_LenhSanXuat();
                lenhSX = KDT_LenhSanXuat.Load(Convert.ToInt64(cbbLenhSanXuat.Value));
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH CHI TIẾT ĐỊNH MỨC SẢN PHẨM CỦA LỆNH SẢN XUẤT (" + lenhSX.SoLenhSanXuat.ToString() + ").xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    grListSP.Tables[0].Columns.Add(new GridEXColumn("MaNguyenPhuLieu", ColumnType.Text, EditType.NoEdit) { Caption = "MÃ NPL" });
                    grListSP.Tables[0].Columns.Add(new GridEXColumn("TenNPL", ColumnType.Text, EditType.NoEdit) { Caption = "TÊN NPL" });
                    grListSP.Tables[0].Columns.Add(new GridEXColumn("DinhMucSuDung", ColumnType.Text, EditType.NoEdit) { Caption = "ĐỊNH MỨC SỬ DỤNG" });
                    grListSP.Tables[0].Columns.Add(new GridEXColumn("TyLeHaoHut", ColumnType.Text, EditType.NoEdit) { Caption = "TỶ LỆ HAO HỤT" });
                    grListSP.Tables[0].Columns.Add(new GridEXColumn("IsFromVietNam", ColumnType.Text, EditType.NoEdit) { Caption = "MUA VN" });
                    grListSP.Refresh();
                    grListSP.DataSource = SXXK_DinhMuc.SelectCollectionDynamic(" LenhSanXuat_ID =" + cbbLenhSanXuat.Value + "", "");
                    grListSP.Refetch();
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = grListSP;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                    grListSP.Tables[0].Columns.Remove("MaNguyenPhuLieu");
                    grListSP.Tables[0].Columns.Remove("TenNPL");
                    grListSP.Tables[0].Columns.Remove("DinhMucSuDung");
                    grListSP.Tables[0].Columns.Remove("TyLeHaoHut");
                    grListSP.Tables[0].Columns.Remove("IsFromVietNam");
                    BindDataSP();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH ĐỊNH MỨC CỦA SẢN PHẨM (" + SP.MaSanPham.ToString() + ").xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgListNPL;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
