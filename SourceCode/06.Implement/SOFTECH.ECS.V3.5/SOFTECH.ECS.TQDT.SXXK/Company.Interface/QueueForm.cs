using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using System.IO;
using Company.BLL.SXXK.ThanhKhoan;


namespace Company.Interface
{
    public partial class QueueForm : BaseForm
    {
        public HangDoiCollection HDCollection = new HangDoiCollection();
        long index = 1;
        public QueueForm()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //if (GlobalSettings.NGON_NGU == "0")
            //{
            //    notifyIcon1.Text = "Có " + this.HDCollection.Count + " tờ khai trong hàng đợi";
            //}
            //else
            //{
            //    if (this.HDCollection.Count >= 1)
            //        notifyIcon1.Text = "There are " + this.HDCollection.Count + " declaration forms in queue ";
            //    else
            //         notifyIcon1.Text = "There is " + this.HDCollection.Count + " declaration form in queue ";
            //}
            //string xmlCurrent = "";
            //MsgSend sendXML = new MsgSend();
            //if (HDCollection.Count == 0)
            //{
            //    this.Hide();
            //}
            //else
            //{
                //System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QueueForm));
                //this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("Gear01")));
                //System.Threading.Thread.Sleep(1000);
                //if (true)
                //{
                //    HangDoi hd = HDCollection[0];
                //    long id = hd.ID;
                //    switch (hd.LoaiToKhai)
                //    {
                        //#region Tờ khai chuyển tiếp
                        //case "Tờ khai chuyển tiếp":
                        //    {
                        //        ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
                        //        tkct.ID = id;
                        //        tkct.Load();
                        //        bool ret = false;
                        //        if (hd.ChucNang == "Khai báo")
                        //        {
                        //            try
                        //            {
                        //                ret = tkct.WSSend() > 0;
                        //                notifyIcon1.ShowBalloonTip(5000, "Kết quả khai báo", "Đã khai báo thành công tờ khai chuyển tiếp", ToolTipIcon.Info);
                        //            }
                        //            catch { ret = false; }
                        //        }
                        //        else if (hd.ChucNang == "Nhận thông tin")
                        //        {
                        //            try
                        //            {
                        //                tkct.WSRequestStatus();
                        //                ret = true;
                        //                notifyIcon1.ShowBalloonTip(5000, "Kết quả lấy thông tin phản hồi", "Đã lấy thông tin phản hổi của tờ khai chuyển tiếp thành công", ToolTipIcon.Info);
                        //            }
                        //            catch
                        //            {
                        //                ret = false;
                        //            }
                        //        }
                        //        else if (hd.ChucNang == "Hủy khai báo")
                        //        {
                        //            long soTN = tkct.SoTiepNhan;
                        //            try
                        //            {
                        //                ret = tkct.WSCancel();
                        //                notifyIcon1.ShowBalloonTip(5000, "Kết quả hủy khai báo", "Đã hủy khai báo tờ khai chuyển tiếp có số tiếp nhận " + soTN + " thành công", ToolTipIcon.Info);
                        //            }
                        //            catch { ret = false; }
                        //        }
                        //        if (ret)
                        //        {
                        //            HDCollection.RemoveAt(0);
                        //            dgList.Refetch();
                        //            if (HDCollection.Count == 0) 
                        //                this.Hide();
                        //        }  
                        //        break;
                        //    }
                        //#endregion
                        //#region Tờ khai mậu dịch
                        //case "Tờ khai mậu dịch":
                        //    {
                        //        ToKhaiMauDich tkmd= new ToKhaiMauDich();
                        //        tkmd.ID = id;
                        //        tkmd.Load();
                        //        bool ret = false; ;
                              
                        //        if (hd.ChucNang == "Khai báo")
                        //        {
                        //            try
                        //            {
                        //                tkmd.LoadChungTuTKCollection();
                        //                tkmd.LoadHMDCollection();
                        //                if (tkmd.MaLoaiHinh.StartsWith("NSX"))
                        //                {
                        //                    //if (tkmd.SoTiepNhan == 0)
                        //                      xmlCurrent= tkmd.WSSendXMLNHAP(hd.PassWord);
                        //                    //else
                        //                     // xmlCurrent = tkmd.WSUpdateXMLNHAP(hd.PassWord);
                        //                }
                        //                else if (tkmd.MaLoaiHinh.StartsWith("XSX"))
                        //                {

                        //                   // if (tkmd.SoTiepNhan == 0)
                        //                        xmlCurrent = tkmd.WSSendXMLXuat(hd.PassWord);
                        //                    //else
                        //                       // xmlCurrent = tkmd.WSUpdateXMLXUAT(hd.PassWord);
                        //                }
                        //                ret = true;                                        
                        //                sendXML.LoaiHS = "TK";
                        //                sendXML.master_id = tkmd.ID;
                        //                sendXML.msg = xmlCurrent;
                        //                sendXML.func = 1;
                        //                xmlCurrent = "";
                        //                sendXML.InsertUpdate();
                        //                HangDoi hdXacnhan = new HangDoi();
                        //                hdXacnhan.ID = tkmd.ID;
                        //                hdXacnhan.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                        //                hdXacnhan.TrangThai = tkmd.TrangThaiXuLy;
                        //                hdXacnhan.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                        //                hdXacnhan.PassWord = hd.PassWord;
                        //                HDCollection.Add(hdXacnhan);
                        //            }
                        //            catch { ret = false; }
                        //        }
                        //        else if (hd.ChucNang == "Nhận thông tin")
                        //        {
                        //            try
                        //            {
                        //                xmlCurrent = tkmd.WSRequestXML(hd.PassWord);
                        //                ret = true;
                        //                sendXML.LoaiHS = "TK";
                        //                sendXML.master_id = tkmd.ID;
                        //                sendXML.msg = xmlCurrent;
                        //                sendXML.func = 2;
                        //                xmlCurrent = "";
                        //                sendXML.InsertUpdate();                                        
                        //                HangDoi hdXacnhan = new HangDoi();
                        //                hdXacnhan.ID = tkmd.ID;
                        //                hdXacnhan.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                        //                hdXacnhan.TrangThai = tkmd.TrangThaiXuLy;
                        //                hdXacnhan.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                        //                hdXacnhan.PassWord = hd.PassWord;
                        //                HDCollection.Add(hdXacnhan);
                        //            }
                        //            catch
                        //            {
                        //                ret = false;
                        //            }
                        //        }
                        //        else if (hd.ChucNang == "Hủy khai báo")
                        //        {
                        //            long soTN = tkmd.SoTiepNhan;
                        //            try
                        //            {
                        //                if (tkmd.MaLoaiHinh.StartsWith("NSX"))
                        //                    xmlCurrent = tkmd.WSCancelXMLNhap(hd.PassWord);
                        //                else if (tkmd.MaLoaiHinh.StartsWith("XSX"))
                        //                    xmlCurrent = tkmd.WSCancelXMLXuat(hd.PassWord);
                        //                ret = true;
                        //                sendXML.LoaiHS = "TK";
                        //                sendXML.master_id = tkmd.ID;
                        //                sendXML.msg = xmlCurrent;
                        //                sendXML.func = 3;
                        //                xmlCurrent = "";
                        //                sendXML.InsertUpdate();                                        
                        //                HangDoi hdXacnhan = new HangDoi();
                        //                hdXacnhan.ID = tkmd.ID;
                        //                hdXacnhan.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                        //                hdXacnhan.TrangThai = tkmd.TrangThaiXuLy;
                        //                hdXacnhan.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                        //                hdXacnhan.PassWord = hd.PassWord;
                        //                HDCollection.Add(hdXacnhan);
                        //            }
                        //            catch { ret = false; }
                        //        }
                        //        else if (hd.ChucNang == "Xác nhận thông tin")
                        //        {
                        //            long soTN = tkmd.SoTiepNhan;
                        //            try
                        //            {
                        //                sendXML.LoaiHS = "TK";
                        //                sendXML.master_id = tkmd.ID;
                        //                sendXML.Load();
                        //                xmlCurrent = tkmd.LayPhanHoi(hd.PassWord,sendXML.msg);
                                       
                        //                if (xmlCurrent == "")
                        //                {
                        //                    if(sendXML.func==1)
                        //                        notifyIcon1.ShowBalloonTip(5000, "Kết quả khai báo tờ khai", "Đã khai báo thành công tờ khai mậu dịch", ToolTipIcon.Info);                                        
                        //                    else if(sendXML.func==2)
                        //                        notifyIcon1.ShowBalloonTip(5000, "Kết quả lấy trạng thái tờ khai", "Đã lấy trạng thái của tờ khai mậu dịch thành công", ToolTipIcon.Info);
                        //                    else if(sendXML.func==3)
                        //                        notifyIcon1.ShowBalloonTip(5000, "Kết quả hủy khai báo tờ khai", "Đã hủy khai báo tờ khai mậu dịch có số tiếp nhận " + soTN + " thành công", ToolTipIcon.Info);
                        //                    sendXML.Delete();
                        //                    ret = true;
                        //                }                                        
                        //            }
                        //            catch(Exception ex)
                        //            {
                        //                ret = false;
                        //                string[] msg = ex.Message.Split('|');
                        //                if (msg.Length == 2)
                        //                {
                        //                    if (msg[1] == "DOTNET_LEVEL")
                        //                    {
                        //                        ;
                        //                    }
                        //                    else
                        //                    {
                        //                        if (sendXML.func == 1)
                        //                            notifyIcon1.ShowBalloonTip(10000, "Thông báo lỗi", "Có lỗi trong khai báo tờ khai : " + msg[0], ToolTipIcon.Error);
                        //                        else if (sendXML.func == 2)
                        //                            notifyIcon1.ShowBalloonTip(10000, "Thông báo lỗi", "Có lỗi trong nhận trạng thái tờ khai : " + msg[0], ToolTipIcon.Error);
                        //                        else if (sendXML.func == 3)
                        //                            notifyIcon1.ShowBalloonTip(10000, "Thông báo lỗi", "Có lỗi khi hủy khai báo tờ khai : " + msg[0], ToolTipIcon.Error);
                        //                        sendXML.Delete();
                        //                    }
                        //                }
                        //            }
                        //        }            

                        //        if (ret)
                        //        {
                        //            HDCollection.RemoveAt(0);
                        //            try
                        //            {
                        //                dgList.Refetch();
                        //            }
                        //            catch { dgList.Refresh(); }
                        //            if (HDCollection.Count == 0)
                        //                this.Hide();
                        //        }
                        //        break;
                        //    }
                        //#endregion
                        //#region Danh mục nguyên phụ liệu
                        //case "Danh mục nguyên phụ liệu":
                        //    {
                        //        NguyenPhuLieuDangKy npldk = NguyenPhuLieuDangKy.Load(id);
                        //        bool ret = false;
                        //        if (hd.ChucNang == "Khai báo")
                        //        {
                        //            string[] danhsachDaDangKy = new string[0];
                        //            try
                        //            {
                        //                xmlCurrent = npldk.WSSendXML(hd.PassWord);                                        
                        //                ret = true;
                        //                sendXML.LoaiHS = "NPL";
                        //                sendXML.master_id = npldk.ID;
                        //                sendXML.msg = xmlCurrent;
                        //                sendXML.func = 1;
                        //                xmlCurrent = "";
                        //                sendXML.InsertUpdate();
                        //                HangDoi hdXacnhan = new HangDoi();
                        //                hdXacnhan.ID = npldk.ID;
                        //                hdXacnhan.LoaiToKhai = LoaiToKhai.DANH_MUC_NGUYEN_PHU_LIEU;
                        //                hdXacnhan.TrangThai = npldk.TrangThaiXuLy;
                        //                hdXacnhan.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                        //                hdXacnhan.PassWord = hd.PassWord;
                        //                HDCollection.Add(hdXacnhan);
                        //            }
                        //            catch
                        //            {
                        //                ret = false;
                        //            }
                        //        }
                        //        else if (hd.ChucNang == "Nhận thông tin")
                        //        {
                        //            try
                        //            {
                        //                xmlCurrent = npldk.WSRequestXML(hd.PassWord);
                        //                ret = true;                                        
                        //                sendXML.LoaiHS = "NPL";
                        //                sendXML.master_id = npldk.ID;
                        //                sendXML.msg = xmlCurrent;
                        //                sendXML.func = 2;
                        //                xmlCurrent = "";
                        //                sendXML.InsertUpdate();
                        //                HangDoi hdXacnhan = new HangDoi();
                        //                hdXacnhan.ID = npldk.ID;
                        //                hdXacnhan.LoaiToKhai = LoaiToKhai.DANH_MUC_NGUYEN_PHU_LIEU;
                        //                hdXacnhan.TrangThai = npldk.TrangThaiXuLy;
                        //                hdXacnhan.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                        //                hdXacnhan.PassWord = hd.PassWord;
                        //                HDCollection.Add(hdXacnhan);
                        //            }
                        //            catch
                        //            {
                        //                ret = false;
                        //            }
                        //        }
                        //        else if (hd.ChucNang == "Hủy khai báo")
                        //        {
                        //            long soTN = npldk.SoTiepNhan;
                        //            try
                        //            {
                        //                xmlCurrent = npldk.WSCancelXML(hd.PassWord);
                        //                ret = true;
                        //                sendXML.LoaiHS = "NPL";
                        //                sendXML.master_id = npldk.ID;
                        //                sendXML.msg = xmlCurrent;
                        //                sendXML.func =3;
                        //                xmlCurrent = "";
                        //                sendXML.InsertUpdate();
                        //                HangDoi hdXacnhan = new HangDoi();
                        //                hdXacnhan.ID = npldk.ID;
                        //                hdXacnhan.LoaiToKhai = LoaiToKhai.DANH_MUC_NGUYEN_PHU_LIEU;
                        //                hdXacnhan.TrangThai = npldk.TrangThaiXuLy;
                        //                hdXacnhan.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                        //                hdXacnhan.PassWord = hd.PassWord;
                        //                HDCollection.Add(hdXacnhan);
                        //            }
                        //            catch
                        //            {
                        //                ret = false;
                        //            }                                    
                        //        }
                        //        else if (hd.ChucNang == "Xác nhận thông tin")
                        //        {
                        //            long soTN = npldk.SoTiepNhan;
                        //            try
                        //            {
                        //                sendXML.LoaiHS = "NPL";
                        //                sendXML.master_id = npldk.ID;
                        //                sendXML.Load();
                        //                xmlCurrent = npldk.LayPhanHoi(hd.PassWord, sendXML.msg);                                                                                                                        
                        //                if (xmlCurrent == "")
                        //                {
                        //                    if (sendXML.func == 1)
                        //                        notifyIcon1.ShowBalloonTip(5000, "Kết quả khai báo", "Đã khai báo thành công danh mục nguyên phụ liệu", ToolTipIcon.Info);
                        //                    else if (sendXML.func == 2)
                        //                        notifyIcon1.ShowBalloonTip(5000, "Kết quả lấy trạng thái", "Đã lấy thông tin phản hồi của danh mục nguyên phụ liệu thành công", ToolTipIcon.Info);
                        //                    else if (sendXML.func == 3)
                        //                        notifyIcon1.ShowBalloonTip(5000, "Kết quả hủy khai báo", "Đã hủy khai báo danh mục nguyên phụ liệu có số tiếp nhận " + soTN + " thành công", ToolTipIcon.Info);
                        //                    sendXML.Delete();
                        //                    ret = true;
                        //                }
                        //            }
                        //            catch (Exception ex)
                        //            {
                        //                ret = false;
                        //                string[] msg = ex.Message.Split('|');
                        //                if (msg.Length == 2)
                        //                {
                        //                    if (msg[1] == "DOTNET_LEVEL")
                        //                    {
                        //                        ;
                        //                    }
                        //                    else
                        //                    {
                        //                        ret = true;
                        //                        if (sendXML.func == 1)
                        //                            notifyIcon1.ShowBalloonTip(10000, "Thông báo lỗi", "Có lỗi trong khai báo nguyên phụ liệu : " + msg[0], ToolTipIcon.Error);
                        //                        else if (sendXML.func == 2)
                        //                            notifyIcon1.ShowBalloonTip(10000, "Thông báo lỗi", "Có lỗi trong nhận trạng thái nguyên phụ liệu  : " + msg[0], ToolTipIcon.Error);
                        //                        else if (sendXML.func == 3)
                        //                            notifyIcon1.ShowBalloonTip(10000, "Thông báo lỗi", "Có lỗi khi hủy khai báo nguyên phụ liệu  : " + msg[0], ToolTipIcon.Error);
                        //                        sendXML.Delete();
                        //                    }
                        //                }
                        //            }
                        //        }            
                        //        if (ret)
                        //        {
                        //            HDCollection.RemoveAt(0);
                        //            try
                        //            {
                        //                dgList.Refetch();
                        //            }
                        //            catch { dgList.Refresh(); }
                        //            if (HDCollection.Count == 0)
                        //                this.Hide();
                        //        }
                        //        break;
                        //    }
                        //#endregion
                        //#region Danh mục sản phẩm
                        //case "Danh mục sản phẩm":
                        //    {
                        //        SanPhamDangKy spdk = SanPhamDangKy.Load(id);
                        //        bool ret = false;
                        //        if (hd.ChucNang == "Khai báo")
                        //        {
                        //            string[] danhsachDaDangKy = new string[0];
                        //            try
                        //            {                                      
                        //                xmlCurrent=   spdk.WSSendXML(hd.PassWord);
                        //                ret = true;                                        
                        //                sendXML.LoaiHS = "SP";
                        //                sendXML.master_id = spdk.ID;
                        //                sendXML.msg = xmlCurrent;
                        //                sendXML.func = 1;
                        //                xmlCurrent = "";
                        //                sendXML.InsertUpdate();
                        //                HangDoi hdXacnhan = new HangDoi();
                        //                hdXacnhan.ID = spdk.ID;
                        //                hdXacnhan.LoaiToKhai = LoaiToKhai.DANH_MUC_SAN_PHAM;
                        //                hdXacnhan.TrangThai = spdk.TrangThaiXuLy;
                        //                hdXacnhan.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                        //                hdXacnhan.PassWord = hd.PassWord;
                        //                HDCollection.Add(hdXacnhan);
                        //            }
                        //            catch
                        //            {
                        //                ret = false;
                        //            }
                        //        }
                        //        else if (hd.ChucNang == "Nhận thông tin")
                        //        {
                        //            try
                        //            {
                        //                xmlCurrent = spdk.WSRequestXML(hd.PassWord);
                        //                ret = true;
                        //                sendXML.LoaiHS = "SP";
                        //                sendXML.master_id = spdk.ID;
                        //                sendXML.msg = xmlCurrent;
                        //                sendXML.func = 2;
                        //                xmlCurrent = "";
                        //                sendXML.InsertUpdate();
                        //                HangDoi hdXacnhan = new HangDoi();
                        //                hdXacnhan.ID = spdk.ID;
                        //                hdXacnhan.LoaiToKhai = LoaiToKhai.DANH_MUC_SAN_PHAM;
                        //                hdXacnhan.TrangThai = spdk.TrangThaiXuLy;
                        //                hdXacnhan.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                        //                hdXacnhan.PassWord = hd.PassWord;
                        //                HDCollection.Add(hdXacnhan);                                        
                        //            }
                        //            catch
                        //            {
                        //                ret = false;
                        //            }
                        //        }
                        //        else if (hd.ChucNang == "Hủy khai báo")
                        //        {
                        //            long soTN = spdk.SoTiepNhan;
                        //            try
                        //            {
                        //                xmlCurrent = spdk.WSCancelXML(hd.PassWord);
                        //                ret = true;                                        
                        //                sendXML.LoaiHS = "SP";
                        //                sendXML.master_id = spdk.ID;
                        //                sendXML.msg = xmlCurrent;
                        //                sendXML.func = 3;
                        //                xmlCurrent = "";
                        //                sendXML.InsertUpdate();
                        //                HangDoi hdXacnhan = new HangDoi();
                        //                hdXacnhan.ID = spdk.ID;
                        //                hdXacnhan.LoaiToKhai = LoaiToKhai.DANH_MUC_SAN_PHAM;
                        //                hdXacnhan.TrangThai = spdk.TrangThaiXuLy;
                        //                hdXacnhan.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                        //                hdXacnhan.PassWord = hd.PassWord;
                        //                HDCollection.Add(hdXacnhan);
                        //            }
                        //            catch
                        //            {
                        //                ret = false;
                        //            }
                        //        }
                        //        else if (hd.ChucNang == "Xác nhận thông tin")
                        //        {
                        //            long soTN = spdk.SoTiepNhan;
                        //            try
                        //            {
                        //                sendXML.LoaiHS = "SP";
                        //                sendXML.master_id = spdk.ID;
                        //                sendXML.Load();
                        //                xmlCurrent = spdk.LayPhanHoi(hd.PassWord, sendXML.msg);                                                                                
                        //                if (xmlCurrent == "")
                        //                {
                        //                    if (sendXML.func == 1)
                        //                        notifyIcon1.ShowBalloonTip(5000, "Kết quả khai báo", "Đã khai báo thành công danh mục sản phẩm", ToolTipIcon.Info);                                        
                        //                    else if (sendXML.func == 2)
                        //                        notifyIcon1.ShowBalloonTip(5000, "Kết quả lấy trạng thái", "Đã lấy thông tin phản hồi của danh mục sản phẩm thành công", ToolTipIcon.Info);
                        //                    else if (sendXML.func == 3)
                        //                        notifyIcon1.ShowBalloonTip(5000, "Kết quả hủy khai báo", "Đã hủy khai báo danh mục sản phẩm có số tiếp nhận " + soTN + " thành công", ToolTipIcon.Info);
                        //                    sendXML.Delete();
                        //                    ret = true;
                        //                }
                        //            }
                        //            catch (Exception ex)
                        //            {
                        //                ret = false;
                        //                string[] msg = ex.Message.Split('|');
                        //                if (msg.Length == 2)
                        //                {
                        //                    if (msg[1] == "DOTNET_LEVEL")
                        //                    {
                        //                        ;
                        //                    }
                        //                    else
                        //                    {
                        //                        ret = true;
                        //                        if (sendXML.func == 1)
                        //                            notifyIcon1.ShowBalloonTip(5000, "Thông báo lỗi", "Có lỗi trong khai báo sản phẩm : " + msg[0], ToolTipIcon.Error);
                        //                        else if (sendXML.func == 2)
                        //                            notifyIcon1.ShowBalloonTip(5000, "Thông báo lỗi", "Có lỗi trong nhận trạng thái sản phẩm: " + msg[0], ToolTipIcon.Error);
                        //                        else if (sendXML.func == 3)
                        //                            notifyIcon1.ShowBalloonTip(5000, "Thông báo lỗi", "Có lỗi khi hủy khai báo sản phẩm : " + msg[0], ToolTipIcon.Error);
                        //                        sendXML.Delete();
                        //                    }
                        //                }
                        //            }
                        //        }            
                        //        if (ret)
                        //        {
                        //            HDCollection.RemoveAt(0);
                        //            try
                        //            {
                        //                dgList.Refetch();
                        //            }
                        //            catch { dgList.Refresh(); }
                        //            if (HDCollection.Count == 0)
                        //                this.Hide();
                        //        }
                        //        break;
                        //    }
                        //#endregion
                        //#region Định mức
                        //case "Tờ khai định mức":
                        //    {
                        //        Company.BLL.KDT.SXXK.DinhMucDangKy dmdk = Company.BLL.KDT.SXXK.DinhMucDangKy.Load(id);
                        //        bool ret = false;
                        //        if (hd.ChucNang == "Khai báo")
                        //        {
                        //            try
                        //            {                                       
                        //                xmlCurrent=dmdk.WSSendXML(hd.PassWord);
                        //                ret = true;
                        //                sendXML.LoaiHS = "DM";
                        //                sendXML.master_id = dmdk.ID;
                        //                sendXML.msg = xmlCurrent;
                        //                sendXML.func = 1;
                        //                xmlCurrent = "";
                        //                sendXML.InsertUpdate();
                        //                HangDoi hdXacnhan = new HangDoi();
                        //                hdXacnhan.ID = dmdk.ID;
                        //                hdXacnhan.LoaiToKhai = LoaiToKhai.DINH_MUC;
                        //                hdXacnhan.TrangThai = dmdk.TrangThaiXuLy;
                        //                hdXacnhan.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                        //                hdXacnhan.PassWord = hd.PassWord;
                        //                HDCollection.Add(hdXacnhan);                                        
                        //            }
                        //            catch 
                        //            {
                        //                ret = false;
                        //            }
                        //        }
                        //        else if (hd.ChucNang == "Nhận thông tin")
                        //        {
                        //            try
                        //            {                                      
                        //                xmlCurrent=    dmdk.WSRequestXML(hd.PassWord);
                        //                ret = true;
                        //                sendXML.LoaiHS = "DM";
                        //                sendXML.master_id = dmdk.ID;
                        //                sendXML.msg = xmlCurrent;
                        //                sendXML.func = 2;
                        //                xmlCurrent = "";
                        //                sendXML.InsertUpdate();
                        //                HangDoi hdXacnhan = new HangDoi();
                        //                hdXacnhan.ID = dmdk.ID;
                        //                hdXacnhan.LoaiToKhai = LoaiToKhai.DINH_MUC;
                        //                hdXacnhan.TrangThai = dmdk.TrangThaiXuLy;
                        //                hdXacnhan.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                        //                hdXacnhan.PassWord = hd.PassWord;
                        //                HDCollection.Add(hdXacnhan);                                           
                        //            }
                        //            catch
                        //            {
                        //                ret = false;
                        //            }
                        //        }
                        //        else if (hd.ChucNang == "Hủy khai báo")
                        //        {
                        //            long soTN = dmdk.SoTiepNhan;
                        //            try
                        //            {                                      
                        //                xmlCurrent=dmdk.WSCancelXML(hd.PassWord);
                        //                ret = true;
                        //                sendXML.LoaiHS = "DM";
                        //                sendXML.master_id = dmdk.ID;
                        //                sendXML.msg = xmlCurrent;
                        //                sendXML.func = 3;
                        //                xmlCurrent = "";
                        //                sendXML.InsertUpdate();
                        //                HangDoi hdXacnhan = new HangDoi();
                        //                hdXacnhan.ID = dmdk.ID;
                        //                hdXacnhan.LoaiToKhai = LoaiToKhai.DINH_MUC;
                        //                hdXacnhan.TrangThai = dmdk.TrangThaiXuLy;
                        //                hdXacnhan.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                        //                hdXacnhan.PassWord = hd.PassWord;
                        //                HDCollection.Add(hdXacnhan);                                        
                        //            }
                        //            catch
                        //            {
                        //                ret = false;
                        //            }
                        //        }
                        //        else if (hd.ChucNang == "Xác nhận thông tin")
                        //        {
                        //            long soTN = dmdk.SoTiepNhan;
                        //            try
                        //            {
                        //                sendXML.LoaiHS = "DM";
                        //                sendXML.master_id = dmdk.ID;
                        //                sendXML.Load();
                        //                xmlCurrent = dmdk.LayPhanHoi(hd.PassWord, sendXML.msg);
                        //                if (xmlCurrent == "")
                        //                {
                        //                    if (sendXML.func == 1)
                        //                        notifyIcon1.ShowBalloonTip(5000, "Kết quả khai báo", "Đã khai báo thành công tờ khai định mức SXXK", ToolTipIcon.Info);
                        //                    else if (sendXML.func == 2)
                        //                        notifyIcon1.ShowBalloonTip(5000, "Kết quả lấy thông tin phản hồi", "Đã lấy thông tin phản hồi của tờ khai định mức SXXK thành công", ToolTipIcon.Info);
                        //                    else if (sendXML.func == 3)
                        //                        notifyIcon1.ShowBalloonTip(5000, "Kết quả hủy khai báo", "Đã hủy khai báo tờ khai định mức SXXK có số tiếp nhận " + soTN + " thành công", ToolTipIcon.Info);
                        //                    sendXML.Delete();
                        //                    ret = true;
                        //                }
                        //            }
                        //            catch (Exception ex)
                        //            {
                        //                ret = false;
                        //                string[] msg = ex.Message.Split('|');
                        //                if (msg.Length == 2)
                        //                {
                        //                    if (msg[1] == "DOTNET_LEVEL")
                        //                    {
                        //                        ;
                        //                    }
                        //                    else
                        //                    {
                        //                        ret = true;
                        //                        if (sendXML.func == 1)
                        //                            notifyIcon1.ShowBalloonTip(10000, "Thông báo lỗi", "Có lỗi trong khai báo định mức : " + msg[0], ToolTipIcon.Error);
                        //                        else if (sendXML.func == 2)
                        //                            notifyIcon1.ShowBalloonTip(10000, "Thông báo lỗi", "Có lỗi trong nhận trạng thái định mức : " + msg[0], ToolTipIcon.Error);
                        //                        else if (sendXML.func == 3)
                        //                            notifyIcon1.ShowBalloonTip(10000, "Thông báo lỗi", "Có lỗi khi hủy khai báo định mức : " + msg[0], ToolTipIcon.Error);
                        //                        sendXML.Delete();
                        //                    }
                        //                }
                        //            }
                        //        }            
                        //        if (ret)
                        //        {
                        //            HDCollection.RemoveAt(0);
                        //            try
                        //            {
                        //                dgList.Refetch();
                        //            }
                        //            catch { dgList.Refresh(); }
                        //            if (HDCollection.Count == 0)
                        //                this.Hide();
                        //        }
                        //        break;
                        //    }                      
                        //#endregion
                        //#region Thanh khoản
                        //case "Hồ sơ thanh khoản123" :
                        //    {
                        //        return;
                        //        HoSoThanhLyDangKy hstl = new HoSoThanhLyDangKy();
                        //        hstl.ID = hd.ID;
                        //        bool ret = false;
                        //        hstl.Load();
                        //        hstl.LoadBKCollection();
                        //        if (hd.ChucNang == "Khai báo")
                        //        {
                        //            try
                        //            {
                        //                hstl.WSSend();
                        //                ret = true;
                        //                notifyIcon1.ShowBalloonTip(5000, "Kết quả khai báo", "Đã khai báo thành công hồ sơ thanh khoản", ToolTipIcon.Info);
                        //            }
                        //            catch { ret = false; ; }
                        //        }
                        //        else if (hd.ChucNang == "Nhận thông tin")
                        //        {
                        //            try
                        //            {
                        //                hstl.WSRequest();                                        
                        //                ret = true;
                        //                notifyIcon1.ShowBalloonTip(5000, "Kết quả lấy thông tin phản hồi", "Đã lấy thông tin phản hồi của hồ sơ thanh lý thành công", ToolTipIcon.Info);
                        //            }
                        //            catch
                        //            {
                        //                ret = false;
                        //            }
                        //        }
                        //        else if (hd.ChucNang == "Hủy khai báo")
                        //        {

                        //            try
                        //            {
                        //                hstl.WSCancel();
                        //                ret = true;
                        //                notifyIcon1.ShowBalloonTip(5000, "Kết quả hủy khai báo", "Đã hủy khai báo hồ sơ thanh lý có số tiếp nhận " + hstl.SoTiepNhan.ToString() + " thành công", ToolTipIcon.Info);
                        //            }
                        //            catch
                        //            {
                        //                ret = false;
                        //            }
                        //        }
                        //        else if (hd.ChucNang == "Đồng bộ dữ liệu với hải quan")
                        //        {

                        //            try
                        //            {
                        //                if (hd.LanThanhLy == 0)
                        //                {
                        //                    LanThanhLyBase.DongBoDuLieuHaiQuan(hd.MaHaiQuan, GlobalSettings.MA_DON_VI);
                        //                }
                        //                else
                        //                    LanThanhLyBase.DongBoDuLieuHaiQuan(hd.MaHaiQuan, GlobalSettings.MA_DON_VI,hd.LanThanhLy);
                        //                ret = true;
                        //                notifyIcon1.ShowBalloonTip(5000, "Kết quả hủy khai báo", "Đã hủy khai báo hồ sơ thanh lý có số tiếp nhận " + hstl.SoTiepNhan.ToString() + " thành công", ToolTipIcon.Info);
                        //            }
                        //            catch
                        //            {
                        //                ret = false;
                        //            }
                        //        }
                        //        if (ret)
                        //        {
                        //            HDCollection.RemoveAt(0);
                        //            dgList.Refetch();
                        //            if (HDCollection.Count == 0)
                        //                this.Hide();
                        //        }
                        //        break;                                
                        //    } 
                        //       #endregion                       
                       // default: GiaCong(); break;
                //    }
                //}
                //System.Threading.Thread.Sleep(1000);
                //this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            //}
        }

        private void GiaCong()
        {
            //HangDoi hd = HDCollection[0];
            //long id = hd.ID;
            //switch (hd.LoaiToKhai)
            //{
            //    case "Tờ khai hợp đồng gia công" :
            //    {
            //        #region HopDong
            //        Company.BLL.KDT.GC.HopDong hopdong= Company.BLL.KDT.GC.HopDong.Load(id);
            //        if (id > 0)
            //        {
            //            bool ret = true;
            //            if (hd.ChucNang == "Khai báo")
            //            {
            //                try
            //                {
            //                    hopdong.WSSend();
            //                    notifyIcon1.ShowBalloonTip(5000, "Kết quả khai báo", "Đã khai báo thành công hợp đồng gia công", ToolTipIcon.Info);
            //                }
            //                catch
            //                {
            //                    ret = false;
            //                }
            //            }
            //            else if (hd.ChucNang == "Nhận thông tin")
            //            {
            //                try
            //                {
            //                    hopdong.WSRequest();
            //                    ret = true;
            //                    notifyIcon1.ShowBalloonTip(5000, "Kết quả nhận dữ liệu", "Đã nhận dữ liệu của hợp đồng gia công thành công", ToolTipIcon.Info);
            //                }
            //                catch
            //                {
            //                    ret = false;
            //                }
            //            }
            //            else if (hd.ChucNang == "Hủy khai báo")
            //            {
            //                try
            //                {
            //                    long soTN = hopdong.SoTiepNhan;
            //                    hopdong.WSCancel();
            //                    notifyIcon1.ShowBalloonTip(5000, "Kết quả hủy khai báo", "Đã hủy khai báo hợp đồng gia công có số tiếp nhận " + soTN + " thành công", ToolTipIcon.Info);
            //                }
            //                catch
            //                {
            //                    ret = false;
            //                }
            //            }
            //            if (ret)
            //            {
            //                HDCollection.RemoveAt(0);
            //                dgList.Refetch();
            //                if (HDCollection.Count == 0)
            //                    this.Hide();
            //            }
                        
            //        }
            //        else
            //        {
            //            #region Dong bo voi hai quan
            //            bool ret = true;
            //            if (hd.ChucNang == "Nhận thông tin")
            //            {
            //                if (hd.SoHopDong!=null && hd.SoHopDong.Trim().Length > 0)
            //                {
            //                    try
            //                    {
            //                        Company.BLL.GC.HopDong hdDuyet = new Company.BLL.GC.HopDong();
            //                        hdDuyet.NgayKy = hd.NgayKy;
            //                        hdDuyet.SoHopDong = hd.SoHopDong;
            //                        hdDuyet.MaHaiQuan = hd.MaHaiQuan;
            //                        hdDuyet.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //                        hdDuyet.DongBoHaiQuan();
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        ret = false;
            //                    }
            //                }
            //                else //cap nhat tat ca
            //                {
            //                    try
            //                    {
            //                        Company.BLL.GC.HopDong hdDuyet = new Company.BLL.GC.HopDong();
            //                        hdDuyet.MaHaiQuan = hd.MaHaiQuan;
            //                        hdDuyet.MaDoanhNghiep = GlobalSettings.MA_DON_VI;                                   
            //                        if (hdDuyet.GetHopDongOnHaiQuan(index))
            //                        {
            //                            notifyIcon1.ShowBalloonTip(5000, "Kết quả nhận dữ liệu", "Đã nhận dữ liệu của hợp đồng " + hdDuyet.SoHopDong, ToolTipIcon.Info);
            //                            index++;
            //                            ret = false;
            //                        }
            //                        else
            //                        {
            //                            index = 1;
            //                            ret = true;
            //                        }                                    
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        ret = false;
            //                    }
            //                }
            //                if (ret)
            //                {
            //                    HDCollection.RemoveAt(0);
            //                    dgList.Refetch();
            //                    if (HDCollection.Count == 0)
            //                        this.Hide();
            //                }
            //            }
                        
                                            
            //            #endregion Dong bo voi hai quan
            //        }
                    
            //        #endregion HopDong
            //        break;
            //    }
            //    case "Định mức hợp đồng gia công":
            //    {
            //        #region Dinh muc
            //        Company.BLL.KDT.GC.DinhMucDangKy dmdk = Company.BLL.KDT.GC.DinhMucDangKy.Load(id);
            //        if (id > 0)
            //        {
            //            bool ret = true;
            //            if (hd.ChucNang == "Khai báo")
            //            {
            //                try
            //                {
            //                    dmdk.WSSend();
            //                    notifyIcon1.ShowBalloonTip(5000, "Kết quả khai báo", "Đã khai báo thành công danh sách định mức sản phẩm gia công", ToolTipIcon.Info);
            //                }
            //                catch
            //                {
            //                    ret = false;
            //                }
            //            }
            //            else if (hd.ChucNang == "Nhận thông tin")
            //            {
            //                try
            //                {
            //                    dmdk.WSRequest();
            //                    notifyIcon1.ShowBalloonTip(5000, "Kết quả nhận dữ liệu", "Đã nhận dữ liệu của định mức hợp đồng gia công thành công", ToolTipIcon.Info);
            //                }
            //                catch
            //                {
            //                    ret = false;
            //                }
            //            }
            //            else if (hd.ChucNang == "Hủy khai báo")
            //            {
            //                try
            //                {
            //                    long soTN = dmdk.SoTiepNhan;
            //                    dmdk.WSCancel();
            //                    notifyIcon1.ShowBalloonTip(5000, "Kết quả hủy khai báo", "Đã hủy khai báo định mức hợp đồng gia công có số tiếp nhận " + soTN + " thành công", ToolTipIcon.Info);
            //                }
            //                catch
            //                {
            //                    ret = false;
            //                }
            //            }
            //            if (ret)
            //            {
            //                HDCollection.RemoveAt(0);
            //                dgList.Refetch();
            //                if (HDCollection.Count == 0)
            //                    this.Hide();
            //            }
            //        }
            //        else
            //        {
            //            bool ret = true;
            //            if (hd.ChucNang == "Nhận thông tin")
            //            {
            //                if (hd.SoHopDong.Trim().Length > 0)
            //                {
            //                    try
            //                    {
            //                        Company.BLL.GC.DinhMuc.DongBoHaiQuan(hd.MaHaiQuan, GlobalSettings.MA_DON_VI, hd.SoHopDong, hd.NgayKy);                                    
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        ret = false;
            //                    }
            //                }
            //                else //cap nhat tat ca
            //                {
            //                    try
            //                    {
            //                        Company.BLL.GC.DinhMuc dmDuyet = new Company.BLL.GC.DinhMuc();
            //                        dmDuyet.MaHaiQuan = hd.MaHaiQuan;
            //                        dmDuyet.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //                        if (dmDuyet.GetDinhMucOnHaiQuan(index))
            //                        {
            //                            notifyIcon1.ShowBalloonTip(5000, "Kết quả nhận dữ liệu", "Đã nhận dữ liệu của định mức ", ToolTipIcon.Info);
            //                            index++;
            //                            ret = false;
            //                        }
            //                        else
            //                        {
            //                            index = 1;
            //                            ret = true;
            //                        }
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        ret = false;
            //                    }
            //                }
            //            }
            //            if (ret)
            //            {
            //                HDCollection.RemoveAt(0);
            //                dgList.Refetch();
            //                if (HDCollection.Count == 0)
            //                    this.Hide();
            //            }
            //        }
            //        #endregion Dinh muc
            //        break;
            //    }
            //    case "Phụ kiện hợp đồng gia công":
            //    {
            //        #region PHU_KIEN
            //        Company.BLL.KDT.GC.PhuKienDangKy pkdk = Company.BLL.KDT.GC.PhuKienDangKy.Load(id);
            //        if (id > 0)
            //        {
            //            bool ret = true;
            //            if (hd.ChucNang == "Khai báo")
            //            {
            //                try
            //                {
            //                    pkdk.WSSend();
            //                    notifyIcon1.ShowBalloonTip(5000, "Kết quả khai báo", "Đã khai báo thành công danh sách phụ kiện sản phẩm gia công", ToolTipIcon.Info);
            //                }
            //                catch
            //                {
            //                    ret = false;
            //                }
            //            }
            //            else if (hd.ChucNang == "Nhận thông tin")
            //            {
            //                try
            //                {
            //                    pkdk.WSRequest();
            //                    notifyIcon1.ShowBalloonTip(5000, "Kết quả nhận dữ liệu", "Đã nhận dữ liệu của phụ kiện hợp đồng gia công thành công", ToolTipIcon.Info);
            //                }
            //                catch
            //                {
            //                    ret = false;
            //                }
            //            }
            //            else if (hd.ChucNang == "Hủy khai báo")
            //            {
            //                try
            //                {
            //                    long soTN = pkdk.SoTiepNhan;
            //                    pkdk.WSCancel();
            //                    notifyIcon1.ShowBalloonTip(5000, "Kết quả hủy khai báo", "Đã hủy khai báo phụ kiện hợp đồng gia công có số tiếp nhận " + soTN + " thành công", ToolTipIcon.Info);
            //                }
            //                catch
            //                {
            //                    ret = false;
            //                }
            //            }
            //            if (ret)
            //            {
            //                HDCollection.RemoveAt(0);
            //                dgList.Refetch();
            //                if (HDCollection.Count == 0)
            //                    this.Hide();
            //            }
            //        }
            //        else
            //        {
            //            bool ret = true;
            //            if (hd.ChucNang == "Nhận thông tin")
            //            {
            //                if (hd.SoHopDong.Trim().Length > 0)
            //                {
            //                    try
            //                    {
            //                        Company.BLL.GC.PhuKienDangKy.DongBoHaiQuan(hd.MaHaiQuan, GlobalSettings.MA_DON_VI, hd.SoHopDong, hd.NgayKy);
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        ret = false;
            //                    }
            //                }
            //            }
            //            if (ret)
            //            {
            //                HDCollection.RemoveAt(0);
            //                dgList.Refetch();
            //                if (HDCollection.Count == 0)
            //                    this.Hide();
            //            }
            //        }
            //        #endregion PHU_KIEN:
            //        break;
            //    }
            //}

        }
        private void QueueForm_Load(object sender, EventArgs e)
        {
            //timer1.Start();
            //dgList.DataSource = this.HDCollection;
            
            
        }

        private void QueueForm_FormClosing(object sender, FormClosingEventArgs e)
        {
                this.Hide();
                e.Cancel = true;
                
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            
                //switch (Convert.ToInt32(e.Row.Cells["TinhTrang"].Value))
                //{
                //    case -1:
                //        e.Row.Cells["TinhTrang"].Text = "Chưa khai báo";
                //        break;
                //    case 0:
                //        e.Row.Cells["TinhTrang"].Text = "Chờ duyệt";
                //        break;
                //    case 1:
                //        e.Row.Cells["TinhTrang"].Text = "Đã duyệt";
                //        break;
                //}

        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
        }


        private void Exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }


        private void HideItem_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
        }

        public void RefreshQueue()
        {
            //try
            //{
            //    //dgList.Refetch();
            //}
            //catch { dgList.Refresh(); }
        }

        private void CleanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //this.HDCollection.Clear();
            //RefreshQueue();
        }

        private void dgList_FormattingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {

        }
    }
}
