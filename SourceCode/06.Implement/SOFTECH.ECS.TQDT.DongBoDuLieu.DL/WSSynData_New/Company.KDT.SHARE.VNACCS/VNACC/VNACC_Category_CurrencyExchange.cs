using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
    public partial class VNACC_Category_CurrencyExchange
    {
        protected static List<VNACC_Category_CurrencyExchange> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_CurrencyExchange> collection = new List<VNACC_Category_CurrencyExchange>();
            while (reader.Read())
            {
                VNACC_Category_CurrencyExchange entity = new VNACC_Category_CurrencyExchange();
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CurrencyCode"))) entity.CurrencyCode = reader.GetString(reader.GetOrdinal("CurrencyCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("CurrencyName"))) entity.CurrencyName = reader.GetString(reader.GetOrdinal("CurrencyName"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_CurrencyExchange> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [CurrencyCode], [CurrencyName] FROM [dbo].[t_VNACC_Category_CurrencyExchange]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }

        public static List<VNACC_Category_CurrencyExchange> SelectCollectionBy(List<VNACC_Category_CurrencyExchange> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_CurrencyExchange o)
            {
                return o.TableID.Contains(tableID);
            });
        }
    }
}