using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using GemBox.Spreadsheet;
namespace Company.Interface.SXXK
{
    public partial class ImportHSTKTKNForm : BaseForm
    {
        public Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection TKMDCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection();
        public Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection TKMDReadCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection();
        public Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection TKMDChuaCoCollection = new Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection();
        public string maLoaiHinh = "N";

        public ImportHSTKTKNForm()
        {
            InitializeComponent();
        }

        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }

        private void ImportHSTKTKNForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            //openFileDialog1.InitialDirectory = Application.StartupPath;
            openFileDialog1.RestoreDirectory = true;

            cboDateFormat.SelectedIndex = 1;
        }

        private void btnReadFile_Click(object sender, EventArgs e)
        {
            try
            {
                cvError.Validate();
                if (!cvError.IsValid) return;
                int beginRow = Convert.ToInt32(txtRow.Value) - 1;
                if (beginRow < 0)
                {
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                    }
                    else
                    {
                        error.SetError(txtRow, "Begin row must be greater than zero ");
                    }
                    error.SetIconPadding(txtRow, 8);
                    return;

                }
                this.Cursor = Cursors.WaitCursor;
                btnReadFile.Enabled = false;
                Workbook wb = new Workbook();
                Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text, true);
                }
                catch
                {
                    MLMessages("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                    this.Cursor = Cursors.Default;
                    btnReadFile.Enabled = true;
                    return;
                }
                try
                {
                    ws = wb.Worksheets[txtSheet.Text];
                }
                catch
                {
                    MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"", "MSG_EXC01", txtSheet.Text, false);
                    this.Cursor = Cursors.Default;
                    btnReadFile.Enabled = true;
                    return;
                }

                TKMDChuaCoCollection.Clear();
                this.TKMDReadCollection.Clear();
                WorksheetRowCollection wsrc = ws.Rows;
                char soTKColumn = Convert.ToChar(txtSTKColumn.Text);
                int soTKCol = ConvertCharToInt(soTKColumn);
                char maLHColumn = Convert.ToChar(txtMLHColumn.Text);
                int maLHCol = ConvertCharToInt(maLHColumn);
                char ngayDKColumn = Convert.ToChar(txtNgayDKColumn.Text);
                int ngayDKCol = ConvertCharToInt(ngayDKColumn);
                char ngayHTColumn = Convert.ToChar(txtNgayHoanThanhColumn.Text);
                int ngayHTCol = ConvertCharToInt(ngayHTColumn);

                int soTKChuaCo = 0;
                string thongBao = "";

                //Hungtq update 23/03/2011.
                System.Globalization.CultureInfo cul = null;
                if (cboDateFormat.SelectedItem.ToString() == "dd/MM/yyyy")
                    cul = new System.Globalization.CultureInfo("vi-VN");
                else if (cboDateFormat.SelectedItem.ToString() == "MM/dd/yyyy")
                    cul = new System.Globalization.CultureInfo("en-US");

                //DataSet dsTKMD = new Company.BLL.KDT.ToKhaiMauDich().SelectAll();
                Company.BLL.SXXK.ToKhai.ToKhaiMauDich ctrlTKMD = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                Company.BLL.SXXK.ToKhai.ToKhaiMauDichCollection tkmdTempCollection = null;
                //DataRow[] rows = null;
                string query = "";
                StringBuilder sb = new StringBuilder();

                foreach (WorksheetRow wsr in wsrc)
                {
                    if (wsr.Index >= beginRow)
                    {
                        try
                        {
                            Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkmd = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                            tkmd.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            tkmd.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;

                            if (wsr.Cells[soTKCol].Value == null)
                                break;

                            tkmd.SoToKhai = Convert.ToInt32(wsr.Cells[soTKCol].Value.ToString().Split(new char[] { '/' })[0]);

                            if (wsr.Cells[ngayDKCol].Value.GetType().Name == "String")
                                tkmd.NgayDangKy = Convert.ToDateTime(wsr.Cells[ngayDKCol].Value, cul);
                            else if (wsr.Cells[ngayDKCol].Value.GetType().Name == "Double")
                                tkmd.NgayDangKy = System.DateTime.FromOADate((double)wsr.Cells[ngayDKCol].Value);
                            DateTime ngayDKTemp = tkmd.NgayDangKy;
                            if (wsr.Cells[ngayHTCol].Value.GetType().Name == "String")
                                tkmd.NgayHoanThanh = Convert.ToDateTime(wsr.Cells[ngayHTCol].Value, cul);
                            else if (wsr.Cells[ngayHTCol].Value.GetType().Name == "Double")
                                tkmd.NgayHoanThanh = System.DateTime.FromOADate((double)wsr.Cells[ngayHTCol].Value);
                            DateTime ngayHoanThanhTemp = tkmd.NgayHoanThanh;

                            tkmd.NamDangKy = (short)tkmd.NgayDangKy.Year;

                            //if (maLoaiHinh == "N")
                            //    tkmd.MaLoaiHinh = "NSX03";//Convert.ToString(wsr.Cells[maLHCol].Value).Trim();
                            //else
                            //    tkmd.MaLoaiHinh = "XSX03";


                            query = string.Format("SoToKhai = {0} and MaLoaiHinh like '{1}%' and NamDangKy = {2} and MaHaiQuan = '{3}'",
                                tkmd.SoToKhai, maLoaiHinh, tkmd.NamDangKy, tkmd.MaHaiQuan);

                            tkmdTempCollection = ctrlTKMD.SelectCollectionDynamic(query, "");

                            //rows = dsTKMD.Tables[0].Select(query);

                            if (tkmdTempCollection != null && tkmdTempCollection.Count != 0)// có TK trong đã đăng ký thì mới thêm vào
                            {
                                tkmd.NGAY_THN_THX = ngayHoanThanhTemp;

                                //Cap nhat Ma loai hinh TK
                                tkmd.MaLoaiHinh = tkmdTempCollection[0].MaLoaiHinh;

                                this.TKMDReadCollection.Add(tkmd);
                            }
                            //if (tkmd.IsExist)// có TK trong đã đăng ký thì mới thêm vào
                            //{
                            //    tkmd.NGAY_THN_THX = ngayHoanThanhTemp;
                            //    this.TKMDReadCollection.Add(tkmd);
                            //}
                            else
                            {
                                soTKChuaCo++;
                                thongBao += "Tờ khai số: " + tkmd.SoToKhai + ", ngày đk: " + tkmd.NgayDangKy + "\n";
                                this.TKMDChuaCoCollection.Add(tkmd);
                            }

                        }
                        catch
                        {
                            if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\n Bạn có muốn tiếp tục không?", "MSG_EXC06", "", true) != "Yes")
                            {
                                this.TKMDReadCollection.Clear();
                                this.Cursor = Cursors.Default;
                                btnReadFile.Enabled = true;
                                return;
                            }
                        }
                    }
                }
                if (soTKChuaCo > 0)
                {
                    //string title = "Tổng cộng có: " + soTKChuaCo + "chưa có trong hệ thống: <br>";
                    thongBao = "Tổng cộng có: " + soTKChuaCo + "tờ khai chưa có trong hệ thống:\r\n" + thongBao;
                    MLMessages(thongBao, "", "", false);
                }
                this.Cursor = Cursors.Default;
                btnReadFile.Enabled = true;

                //Kiem tra thong tin du lieu to khai co ton tai?                
                //for (int i = 0; i < TKMDChuaCoCollection.Count; i++)
                //{
                //    if (TKMDChuaCoCollection[i].SoToKhai > 0)
                //    {
                //        query = string.Format("SoToKhai = {0} and MaLoaiHinh like 'N%' and NgayDangKy = '{1}' and MaHaiQuan = '{2}'",
                //            TKMDChuaCoCollection[i].SoToKhai, TKMDChuaCoCollection[i].NgayDangKy.ToString("MM/dd/yyyy"), TKMDChuaCoCollection[i].MaHaiQuan);

                //        rows = dsTKMD.Tables[0].Select(query);

                //        if (rows.Length == 0)
                //        {
                //            sb.Append(TKMDChuaCoCollection[i].SoToKhai.ToString());
                //            sb.Append(", ");
                //            sb.Append(TKMDChuaCoCollection[i].MaLoaiHinh);
                //            sb.Append(", ");
                //            sb.Append(TKMDChuaCoCollection[i].NgayDangKy.ToShortDateString());

                //            if (i < TKMDChuaCoCollection.Count - 1)
                //            {
                //                sb.Append(", ");
                //                sb.AppendLine();
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); Cursor = Cursors.Default; }
        }

        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                //if (e.Row.RowType == RowType.Record)
                //    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

        }
    }
}