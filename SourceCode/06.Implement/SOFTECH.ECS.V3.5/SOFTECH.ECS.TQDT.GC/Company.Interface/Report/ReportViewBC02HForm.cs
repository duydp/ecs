﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC02HForm : BaseForm
    {
        public DataSet ds = new DataSet();
        private BangKe02H_HQGC BC02 = new BangKe02H_HQGC();
        public HopDong HD = new HopDong();
        public ReportViewBC02HForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            //for (int i = 0; i < this.ds.Tables.Count; i++)
            //{
            //    cbPage.Items.Add("Trang " + (i + 1), i);
            //}
            //cbPage.SelectedIndex = 0;
            BC02.HD = this.HD;
            this.BC02.BindReport("");
            printControl1.PrintingSystem = this.BC02.PrintingSystem;
            this.BC02.CreateDocument();

        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.BC01.First = (cbPage.SelectedIndex == 0);
            //this.BC01.Last = (cbPage.SelectedIndex == (ds.Tables.Count - 1));
            //this.BC01.HD = this.HD;
            //this.BC01.BindReport("");
            //printControl1.PrintingSystem = this.BC01.PrintingSystem;
            //this.BC01.CreateDocument();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch 
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }   
        }

        private void btnExportAll_Click(object sender, EventArgs e)
        {

        }

        private void btnPrintAll_Click(object sender, EventArgs e)
        {

        }

        private void btnNext_Click(object sender, EventArgs e)
        {

        }

   
    }
}