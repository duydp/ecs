﻿namespace Company.Interface.Report.SXXK
{
    partial class rpCCK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpCCK));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl15 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl13 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox8 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox7 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lbl8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox6 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox5 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox4 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox3 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.lbl19 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl17 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl11 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.lbl24 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaythang = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl27 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl22 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl20 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Height = 0;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel42,
            this.lbl3,
            this.lbl5,
            this.lbl15,
            this.lbl16,
            this.xrLabel25,
            this.lbl18,
            this.xrLabel23,
            this.xrLabel22,
            this.xrLabel21,
            this.lbl13,
            this.lbl14,
            this.xrPictureBox8,
            this.xrPictureBox7,
            this.lbl8,
            this.lbl7,
            this.lbl9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.lbl6,
            this.lbl2,
            this.lbl1,
            this.xrPictureBox6,
            this.xrPictureBox5,
            this.xrPictureBox4,
            this.xrPictureBox3,
            this.xrPictureBox1,
            this.xrLine2,
            this.lbl19,
            this.lbl17,
            this.lbl12,
            this.lbl11,
            this.lbl10,
            this.xrLabel19,
            this.xrLabel18,
            this.xrLabel17,
            this.xrLabel16,
            this.xrLabel15,
            this.lbl4,
            this.xrLabel1});
            this.ReportHeader.Height = 581;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.ParentStyleUsing.UseBorders = false;
            // 
            // xrLabel42
            // 
            this.xrLabel42.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel42.Location = new System.Drawing.Point(58, 142);
            this.xrLabel42.Multiline = true;
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.ParentStyleUsing.UseFont = false;
            this.xrLabel42.Size = new System.Drawing.Size(75, 25);
            this.xrLabel42.Text = "Kính gửi:";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl3
            // 
            this.lbl3.BorderColor = System.Drawing.Color.Black;
            this.lbl3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl3.Location = new System.Drawing.Point(133, 142);
            this.lbl3.Name = "lbl3";
            this.lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.lbl3.ParentStyleUsing.UseBackColor = false;
            this.lbl3.ParentStyleUsing.UseBorderColor = false;
            this.lbl3.ParentStyleUsing.UseBorders = false;
            this.lbl3.ParentStyleUsing.UseFont = false;
            this.lbl3.Size = new System.Drawing.Size(592, 25);
            this.lbl3.Text = "- CHI CỤC HẢI QUAN ĐÀ NẴNG";
            this.lbl3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl3.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl5
            // 
            this.lbl5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl5.Location = new System.Drawing.Point(33, 200);
            this.lbl5.Multiline = true;
            this.lbl5.Name = "lbl5";
            this.lbl5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl5.ParentStyleUsing.UseFont = false;
            this.lbl5.Size = new System.Drawing.Size(692, 42);
            this.lbl5.Text = "# đề nghị được làm thủ tục Hải quan/Kiểm tra Hải quan tai: địa điểm kiểm tra hàng" +
                " hóa tập trung:";
            this.lbl5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl5.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl15
            // 
            this.lbl15.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl15.Location = new System.Drawing.Point(242, 425);
            this.lbl15.Multiline = true;
            this.lbl15.Name = "lbl15";
            this.lbl15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl15.ParentStyleUsing.UseFont = false;
            this.lbl15.Size = new System.Drawing.Size(483, 25);
            this.lbl15.Text = "(con-ten-nơ)";
            this.lbl15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl15.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl16
            // 
            this.lbl16.BorderColor = System.Drawing.Color.Black;
            this.lbl16.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl16.Location = new System.Drawing.Point(283, 450);
            this.lbl16.Name = "lbl16";
            this.lbl16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl16.ParentStyleUsing.UseBackColor = false;
            this.lbl16.ParentStyleUsing.UseBorderColor = false;
            this.lbl16.ParentStyleUsing.UseBorders = false;
            this.lbl16.ParentStyleUsing.UseFont = false;
            this.lbl16.Size = new System.Drawing.Size(442, 25);
            this.lbl16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl16.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel25
            // 
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.Location = new System.Drawing.Point(142, 450);
            this.xrLabel25.Multiline = true;
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.ParentStyleUsing.UseFont = false;
            this.xrLabel25.Size = new System.Drawing.Size(141, 25);
            this.xrLabel25.Text = "số hiệu con-ten-nơ:";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl18
            // 
            this.lbl18.BorderColor = System.Drawing.Color.Black;
            this.lbl18.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl18.Location = new System.Drawing.Point(242, 500);
            this.lbl18.Name = "lbl18";
            this.lbl18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl18.ParentStyleUsing.UseBackColor = false;
            this.lbl18.ParentStyleUsing.UseBorderColor = false;
            this.lbl18.ParentStyleUsing.UseBorders = false;
            this.lbl18.ParentStyleUsing.UseFont = false;
            this.lbl18.Size = new System.Drawing.Size(483, 25);
            this.lbl18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl18.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.Location = new System.Drawing.Point(142, 500);
            this.xrLabel23.Multiline = true;
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.ParentStyleUsing.UseFont = false;
            this.xrLabel23.Size = new System.Drawing.Size(100, 25);
            this.xrLabel23.Text = "với chiều dài:";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.Location = new System.Drawing.Point(142, 400);
            this.xrLabel22.Multiline = true;
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.ParentStyleUsing.UseFont = false;
            this.xrLabel22.Size = new System.Drawing.Size(183, 25);
            this.xrLabel22.Text = "Trọng lượng hoặc số kiện:";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.Location = new System.Drawing.Point(142, 425);
            this.xrLabel21.Multiline = true;
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.ParentStyleUsing.UseFont = false;
            this.xrLabel21.Size = new System.Drawing.Size(50, 25);
            this.xrLabel21.Text = "Gồm:";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl13
            // 
            this.lbl13.BorderColor = System.Drawing.Color.Black;
            this.lbl13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl13.Location = new System.Drawing.Point(325, 400);
            this.lbl13.Name = "lbl13";
            this.lbl13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl13.ParentStyleUsing.UseBackColor = false;
            this.lbl13.ParentStyleUsing.UseBorderColor = false;
            this.lbl13.ParentStyleUsing.UseBorders = false;
            this.lbl13.ParentStyleUsing.UseFont = false;
            this.lbl13.Size = new System.Drawing.Size(400, 25);
            this.lbl13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl13.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl14
            // 
            this.lbl14.BorderColor = System.Drawing.Color.Black;
            this.lbl14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl14.Location = new System.Drawing.Point(192, 425);
            this.lbl14.Name = "lbl14";
            this.lbl14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl14.ParentStyleUsing.UseBackColor = false;
            this.lbl14.ParentStyleUsing.UseBorderColor = false;
            this.lbl14.ParentStyleUsing.UseBorders = false;
            this.lbl14.ParentStyleUsing.UseFont = false;
            this.lbl14.Size = new System.Drawing.Size(50, 25);
            this.lbl14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbl14.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrPictureBox8
            // 
            this.xrPictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox8.Image")));
            this.xrPictureBox8.Location = new System.Drawing.Point(125, 425);
            this.xrPictureBox8.Name = "xrPictureBox8";
            this.xrPictureBox8.Size = new System.Drawing.Size(16, 25);
            this.xrPictureBox8.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            // 
            // xrPictureBox7
            // 
            this.xrPictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox7.Image")));
            this.xrPictureBox7.Location = new System.Drawing.Point(125, 400);
            this.xrPictureBox7.Name = "xrPictureBox7";
            this.xrPictureBox7.Size = new System.Drawing.Size(16, 25);
            this.xrPictureBox7.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            // 
            // lbl8
            // 
            this.lbl8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl8.Location = new System.Drawing.Point(592, 267);
            this.lbl8.Name = "lbl8";
            this.lbl8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl8.ParentStyleUsing.UseFont = false;
            this.lbl8.Size = new System.Drawing.Size(133, 25);
            this.lbl8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl8.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl7
            // 
            this.lbl7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl7.Location = new System.Drawing.Point(317, 267);
            this.lbl7.Name = "lbl7";
            this.lbl7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl7.ParentStyleUsing.UseFont = false;
            this.lbl7.Size = new System.Drawing.Size(108, 25);
            this.lbl7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl7.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl9
            // 
            this.lbl9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl9.Location = new System.Drawing.Point(267, 292);
            this.lbl9.Name = "lbl9";
            this.lbl9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl9.ParentStyleUsing.UseFont = false;
            this.lbl9.Size = new System.Drawing.Size(158, 25);
            this.lbl9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl9.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.Location = new System.Drawing.Point(100, 292);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.ParentStyleUsing.UseFont = false;
            this.xrLabel8.Size = new System.Drawing.Size(167, 25);
            this.xrLabel8.Text = "tới cửa khẩu nhập ngày:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.Location = new System.Drawing.Point(425, 267);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.ParentStyleUsing.UseFont = false;
            this.xrLabel7.Size = new System.Drawing.Size(167, 25);
            this.xrLabel7.Text = "chuyên chở trên PTVT:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.Location = new System.Drawing.Point(100, 267);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.ParentStyleUsing.UseFont = false;
            this.xrLabel6.Size = new System.Drawing.Size(217, 25);
            this.xrLabel6.Text = "Cho lô hàng thuộc vận tải đơn:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl6
            // 
            this.lbl6.BorderColor = System.Drawing.Color.Black;
            this.lbl6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl6.Location = new System.Drawing.Point(100, 242);
            this.lbl6.Name = "lbl6";
            this.lbl6.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.lbl6.ParentStyleUsing.UseBackColor = false;
            this.lbl6.ParentStyleUsing.UseBorderColor = false;
            this.lbl6.ParentStyleUsing.UseBorders = false;
            this.lbl6.ParentStyleUsing.UseFont = false;
            this.lbl6.Size = new System.Drawing.Size(625, 25);
            this.lbl6.Text = "- Chi Cục Hải quan Quản Lý Hàng XNK ngoài KCN";
            this.lbl6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl6.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl2
            // 
            this.lbl2.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2.Location = new System.Drawing.Point(125, 83);
            this.lbl2.Multiline = true;
            this.lbl2.Name = "lbl2";
            this.lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl2.ParentStyleUsing.UseFont = false;
            this.lbl2.Size = new System.Drawing.Size(517, 34);
            this.lbl2.Text = "ĐƠN ĐỀ NGHỊ CHUYỂN CỬA KHẨU";
            this.lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl1
            // 
            this.lbl1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.Location = new System.Drawing.Point(33, 33);
            this.lbl1.Multiline = true;
            this.lbl1.Name = "lbl1";
            this.lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lbl1.ParentStyleUsing.UseFont = false;
            this.lbl1.Size = new System.Drawing.Size(259, 25);
            this.lbl1.Text = "Số:          /NKCN";
            this.lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl1.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrPictureBox6
            // 
            this.xrPictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox6.Image")));
            this.xrPictureBox6.Location = new System.Drawing.Point(125, 350);
            this.xrPictureBox6.Name = "xrPictureBox6";
            this.xrPictureBox6.Size = new System.Drawing.Size(16, 25);
            this.xrPictureBox6.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            // 
            // xrPictureBox5
            // 
            this.xrPictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox5.Image")));
            this.xrPictureBox5.Location = new System.Drawing.Point(125, 525);
            this.xrPictureBox5.Name = "xrPictureBox5";
            this.xrPictureBox5.Size = new System.Drawing.Size(16, 25);
            this.xrPictureBox5.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            // 
            // xrPictureBox4
            // 
            this.xrPictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox4.Image")));
            this.xrPictureBox4.Location = new System.Drawing.Point(125, 475);
            this.xrPictureBox4.Name = "xrPictureBox4";
            this.xrPictureBox4.Size = new System.Drawing.Size(16, 25);
            this.xrPictureBox4.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            // 
            // xrPictureBox3
            // 
            this.xrPictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox3.Image")));
            this.xrPictureBox3.Location = new System.Drawing.Point(125, 325);
            this.xrPictureBox3.Name = "xrPictureBox3";
            this.xrPictureBox3.Size = new System.Drawing.Size(16, 25);
            this.xrPictureBox3.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.Location = new System.Drawing.Point(125, 375);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.Size = new System.Drawing.Size(16, 25);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            // 
            // xrLine2
            // 
            this.xrLine2.Location = new System.Drawing.Point(467, 50);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.Size = new System.Drawing.Size(141, 9);
            // 
            // lbl19
            // 
            this.lbl19.BorderColor = System.Drawing.Color.Black;
            this.lbl19.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl19.Location = new System.Drawing.Point(258, 525);
            this.lbl19.Multiline = true;
            this.lbl19.Name = "lbl19";
            this.lbl19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 3, 0, 100F);
            this.lbl19.ParentStyleUsing.UseBackColor = false;
            this.lbl19.ParentStyleUsing.UseBorderColor = false;
            this.lbl19.ParentStyleUsing.UseBorders = false;
            this.lbl19.ParentStyleUsing.UseFont = false;
            this.lbl19.Size = new System.Drawing.Size(467, 50);
            this.lbl19.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl17
            // 
            this.lbl17.BorderColor = System.Drawing.Color.Black;
            this.lbl17.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl17.Location = new System.Drawing.Point(325, 475);
            this.lbl17.Name = "lbl17";
            this.lbl17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl17.ParentStyleUsing.UseBackColor = false;
            this.lbl17.ParentStyleUsing.UseBorderColor = false;
            this.lbl17.ParentStyleUsing.UseBorders = false;
            this.lbl17.ParentStyleUsing.UseFont = false;
            this.lbl17.Size = new System.Drawing.Size(400, 25);
            this.lbl17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl17.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl12
            // 
            this.lbl12.BorderColor = System.Drawing.Color.Black;
            this.lbl12.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl12.Location = new System.Drawing.Point(200, 375);
            this.lbl12.Name = "lbl12";
            this.lbl12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl12.ParentStyleUsing.UseBackColor = false;
            this.lbl12.ParentStyleUsing.UseBorderColor = false;
            this.lbl12.ParentStyleUsing.UseBorders = false;
            this.lbl12.ParentStyleUsing.UseFont = false;
            this.lbl12.Size = new System.Drawing.Size(525, 25);
            this.lbl12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl12.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl11
            // 
            this.lbl11.BorderColor = System.Drawing.Color.Black;
            this.lbl11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl11.Location = new System.Drawing.Point(233, 350);
            this.lbl11.Name = "lbl11";
            this.lbl11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl11.ParentStyleUsing.UseBackColor = false;
            this.lbl11.ParentStyleUsing.UseBorderColor = false;
            this.lbl11.ParentStyleUsing.UseBorders = false;
            this.lbl11.ParentStyleUsing.UseFont = false;
            this.lbl11.Size = new System.Drawing.Size(492, 25);
            this.lbl11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl11.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl10
            // 
            this.lbl10.BorderColor = System.Drawing.Color.Black;
            this.lbl10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl10.Location = new System.Drawing.Point(217, 325);
            this.lbl10.Name = "lbl10";
            this.lbl10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl10.ParentStyleUsing.UseBackColor = false;
            this.lbl10.ParentStyleUsing.UseBorderColor = false;
            this.lbl10.ParentStyleUsing.UseBorders = false;
            this.lbl10.ParentStyleUsing.UseFont = false;
            this.lbl10.Size = new System.Drawing.Size(508, 25);
            this.lbl10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl10.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel19
            // 
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.Location = new System.Drawing.Point(142, 525);
            this.xrLabel19.Multiline = true;
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.ParentStyleUsing.UseFont = false;
            this.xrLabel19.Size = new System.Drawing.Size(116, 25);
            this.xrLabel19.Text = "Tên khách hàng:";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.Location = new System.Drawing.Point(142, 375);
            this.xrLabel18.Multiline = true;
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.ParentStyleUsing.UseFont = false;
            this.xrLabel18.Size = new System.Drawing.Size(58, 25);
            this.xrLabel18.Text = "Trị giá:";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.Location = new System.Drawing.Point(142, 475);
            this.xrLabel17.Multiline = true;
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.ParentStyleUsing.UseFont = false;
            this.xrLabel17.Size = new System.Drawing.Size(183, 25);
            this.xrLabel17.Text = "Tuyến đường vận chuyển:";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.Location = new System.Drawing.Point(142, 350);
            this.xrLabel16.Multiline = true;
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.ParentStyleUsing.UseFont = false;
            this.xrLabel16.Size = new System.Drawing.Size(91, 25);
            this.xrLabel16.Text = "Lượng hàng:";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.Location = new System.Drawing.Point(142, 325);
            this.xrLabel15.Multiline = true;
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.ParentStyleUsing.UseFont = false;
            this.xrLabel15.Size = new System.Drawing.Size(75, 25);
            this.xrLabel15.Text = "Tên hàng:";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl4
            // 
            this.lbl4.BorderColor = System.Drawing.Color.Black;
            this.lbl4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl4.Location = new System.Drawing.Point(133, 167);
            this.lbl4.Name = "lbl4";
            this.lbl4.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.lbl4.ParentStyleUsing.UseBackColor = false;
            this.lbl4.ParentStyleUsing.UseBorderColor = false;
            this.lbl4.ParentStyleUsing.UseBorders = false;
            this.lbl4.ParentStyleUsing.UseFont = false;
            this.lbl4.Size = new System.Drawing.Size(592, 25);
            this.lbl4.Text = "- CHI CỤC HẢI QUAN BƯU ĐIỆN ĐÀ NẴNG";
            this.lbl4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl4.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.Location = new System.Drawing.Point(358, 8);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.ParentStyleUsing.UseFont = false;
            this.xrLabel1.Size = new System.Drawing.Size(366, 42);
            this.xrLabel1.Text = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM\r\nĐộc Lập - Tự Do - Hạnh Phúc";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbl24,
            this.lblNgaythang,
            this.lbl28,
            this.xrLabel35,
            this.lbl27,
            this.lbl26,
            this.xrLabel32,
            this.lbl25,
            this.xrLabel30,
            this.lbl22,
            this.lbl20,
            this.lbl21});
            this.ReportFooter.Height = 452;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // lbl24
            // 
            this.lbl24.BorderColor = System.Drawing.Color.Black;
            this.lbl24.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl24.Location = new System.Drawing.Point(108, 250);
            this.lbl24.Name = "lbl24";
            this.lbl24.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.lbl24.ParentStyleUsing.UseBackColor = false;
            this.lbl24.ParentStyleUsing.UseBorderColor = false;
            this.lbl24.ParentStyleUsing.UseBorders = false;
            this.lbl24.ParentStyleUsing.UseFont = false;
            this.lbl24.Size = new System.Drawing.Size(617, 25);
            this.lbl24.Text = "Chi Cục Hải Quan cửa khẩu";
            this.lbl24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl24.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblNgaythang
            // 
            this.lblNgaythang.BorderColor = System.Drawing.Color.Black;
            this.lblNgaythang.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaythang.Location = new System.Drawing.Point(358, 167);
            this.lblNgaythang.Name = "lblNgaythang";
            this.lblNgaythang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblNgaythang.ParentStyleUsing.UseBackColor = false;
            this.lblNgaythang.ParentStyleUsing.UseBorderColor = false;
            this.lblNgaythang.ParentStyleUsing.UseBorders = false;
            this.lblNgaythang.ParentStyleUsing.UseFont = false;
            this.lblNgaythang.Size = new System.Drawing.Size(367, 25);
            this.lblNgaythang.Text = ".................,ngày...........tháng.........năm.............";
            this.lblNgaythang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblNgaythang.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl28
            // 
            this.lbl28.BorderColor = System.Drawing.Color.Black;
            this.lbl28.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl28.Location = new System.Drawing.Point(308, 342);
            this.lbl28.Multiline = true;
            this.lbl28.Name = "lbl28";
            this.lbl28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl28.ParentStyleUsing.UseBackColor = false;
            this.lbl28.ParentStyleUsing.UseBorderColor = false;
            this.lbl28.ParentStyleUsing.UseBorders = false;
            this.lbl28.ParentStyleUsing.UseFont = false;
            this.lbl28.Size = new System.Drawing.Size(417, 42);
            this.lbl28.Text = "Chi Cục Hải Quan QL Hàng XNK Ngoài KCN";
            this.lbl28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lbl28.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel35
            // 
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.Location = new System.Drawing.Point(325, 317);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.ParentStyleUsing.UseFont = false;
            this.xrLabel35.Size = new System.Drawing.Size(50, 25);
            this.xrLabel35.Text = ", ngày";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl27
            // 
            this.lbl27.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl27.Location = new System.Drawing.Point(375, 317);
            this.lbl27.Name = "lbl27";
            this.lbl27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl27.ParentStyleUsing.UseFont = false;
            this.lbl27.Size = new System.Drawing.Size(350, 25);
            this.lbl27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl27.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl26
            // 
            this.lbl26.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl26.Location = new System.Drawing.Point(225, 317);
            this.lbl26.Name = "lbl26";
            this.lbl26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl26.ParentStyleUsing.UseFont = false;
            this.lbl26.Size = new System.Drawing.Size(100, 25);
            this.lbl26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl26.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel32
            // 
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.Location = new System.Drawing.Point(33, 317);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.ParentStyleUsing.UseFont = false;
            this.xrLabel32.Size = new System.Drawing.Size(192, 25);
            this.xrLabel32.Text = "Thuộc tờ khai Hải Quan số:";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl25
            // 
            this.lbl25.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl25.Location = new System.Drawing.Point(33, 275);
            this.lbl25.Multiline = true;
            this.lbl25.Name = "lbl25";
            this.lbl25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl25.ParentStyleUsing.UseFont = false;
            this.lbl25.Size = new System.Drawing.Size(692, 42);
            this.lbl25.Text = "Đề nghị làm thủ tục chuyển cửa khẩu cho lô hàng trên của #";
            this.lbl25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl25.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel30
            // 
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.Location = new System.Drawing.Point(33, 250);
            this.xrLabel30.Multiline = true;
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.ParentStyleUsing.UseFont = false;
            this.xrLabel30.Size = new System.Drawing.Size(75, 25);
            this.xrLabel30.Text = "Kính gửi:";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl22
            // 
            this.lbl22.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl22.Location = new System.Drawing.Point(100, 117);
            this.lbl22.Multiline = true;
            this.lbl22.Name = "lbl22";
            this.lbl22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl22.ParentStyleUsing.UseFont = false;
            this.lbl22.Size = new System.Drawing.Size(625, 42);
            this.lbl22.Text = "2. Vận chuyển lô hàng đúng tuyến đường và thời gian.";
            this.lbl22.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl20
            // 
            this.lbl20.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl20.Location = new System.Drawing.Point(33, 8);
            this.lbl20.Multiline = true;
            this.lbl20.Name = "lbl20";
            this.lbl20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl20.ParentStyleUsing.UseFont = false;
            this.lbl20.Size = new System.Drawing.Size(692, 42);
            this.lbl20.Text = "# cam kết chịu trách nhiệm trước pháp luật về:";
            this.lbl20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl20.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl21
            // 
            this.lbl21.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl21.Location = new System.Drawing.Point(100, 50);
            this.lbl21.Multiline = true;
            this.lbl21.Name = "lbl21";
            this.lbl21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl21.ParentStyleUsing.UseFont = false;
            this.lbl21.Size = new System.Drawing.Size(625, 67);
            this.lbl21.Text = resources.GetString("lbl21.Text");
            this.lbl21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl21.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox2.Image")));
            this.xrPictureBox2.Location = new System.Drawing.Point(125, 325);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.Size = new System.Drawing.Size(16, 25);
            // 
            // rpCCK
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.ReportHeader,
            this.ReportFooter});
            this.Margins = new System.Drawing.Printing.Margins(34, 30, 65, 78);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel lbl4;
        private DevExpress.XtraReports.UI.XRLabel lbl21;
        private DevExpress.XtraReports.UI.XRLabel lbl20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel lbl19;
        private DevExpress.XtraReports.UI.XRLabel lbl17;
        private DevExpress.XtraReports.UI.XRLabel lbl12;
        private DevExpress.XtraReports.UI.XRLabel lbl11;
        private DevExpress.XtraReports.UI.XRLabel lbl10;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox5;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox4;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox3;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox6;
        private DevExpress.XtraReports.UI.XRLabel lbl1;
        private DevExpress.XtraReports.UI.XRLabel lbl2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel lbl6;
        private DevExpress.XtraReports.UI.XRLabel lbl8;
        private DevExpress.XtraReports.UI.XRLabel lbl7;
        private DevExpress.XtraReports.UI.XRLabel lbl9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel lbl13;
        private DevExpress.XtraReports.UI.XRLabel lbl14;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox8;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox7;
        private DevExpress.XtraReports.UI.XRLabel lbl15;
        private DevExpress.XtraReports.UI.XRLabel lbl16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel lbl18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel lbl22;
        private DevExpress.XtraReports.UI.XRLabel lbl25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel lbl28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel lbl27;
        private DevExpress.XtraReports.UI.XRLabel lbl26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel lblNgaythang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel lbl3;
        private DevExpress.XtraReports.UI.XRLabel lbl5;
        private DevExpress.XtraReports.UI.XRLabel lbl24;
    }
}
