﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.BLL.KDT.SXXK;
using Company.Interface.Report.GC.TT39;

namespace Company.Interface.KDT.SXXK
{
    public partial class DinhMucThucTeManagerForm : Company.Interface.BaseForm
    {
        public KDT_SXXK_DinhMucThucTeDangKy dinhMucThucTeDangKy = new KDT_SXXK_DinhMucThucTeDangKy();
        public string where = "";
        public DinhMucThucTeManagerForm()
        {
            InitializeComponent();
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    dinhMucThucTeDangKy = (KDT_SXXK_DinhMucThucTeDangKy)e.Row.DataRow;
                    dinhMucThucTeDangKy.SPCollection = KDT_SXXK_DinhMucThucTe_SP.SelectCollectionBy_DinhMucThucTe_ID(dinhMucThucTeDangKy.ID);
                    foreach (KDT_SXXK_DinhMucThucTe_SP item in dinhMucThucTeDangKy.SPCollection)
                    {
                        item.DMCollection = KDT_SXXK_DinhMucThucTe_DinhMuc.SelectCollectionBy_DinhMucThucTeSP_ID(item.ID);
                    }
                    DinhMucThucTeSendForm f = new DinhMucThucTeSendForm();
                    f.dinhMucThucTeDangKy = dinhMucThucTeDangKy;
                    f.ShowDialog(this);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1=1";
                if (!String.IsNullOrEmpty(txtMaDDSX.Text))
                    where += " AND SoLenhSanXuat LIKE '%" + txtMaDDSX.Text + "%'";
                if (!String.IsNullOrEmpty(txtSoTiepNhan.Text))
                    where += " AND SoTiepNhan LIKE %" + txtSoTiepNhan.Text + "%";
                if (!String.IsNullOrEmpty(txtNamTiepNhan.Text))
                    where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Text + "";
                if (!String.IsNullOrEmpty(cbStatus.SelectedValue.ToString()))
                    where += " AND TrangThaiXuLy =" + cbStatus.SelectedValue;
                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = clcDenNgay.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (NgayTiepNhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.DA_DUYET)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.CHUA_KHAI_BAO)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.CHO_DUYET)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.CHO_HUY)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ hủy";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.DA_HUY)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.KHONG_PHE_DUYET)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.SUA_KHAI_BAO)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo sửa";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.HUY_KHAI_BAO)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo hủy";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.SUATKDADUYET)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đang sửa";
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                grList.Refetch();
                grList.DataSource = KDT_SXXK_DinhMucThucTeDangKy.SelectCollectionDynamic(GetSearchWhere(), "");
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void ckbTimKiem_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;
        }

        private void DinhMucThucTeManagerForm_Load(object sender, EventArgs e)
        {
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;

            cbStatus.SelectedValue = -1;
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH ĐỊNH MỨC THỰC TẾ .xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = grList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_SXXK_DinhMucThucTeDangKy> ItemColl = new List<KDT_SXXK_DinhMucThucTeDangKy>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_SXXK_DinhMucThucTeDangKy)i.GetRow().DataRow);
                        }
                    }
                    foreach (KDT_SXXK_DinhMucThucTeDangKy item in ItemColl)
                    {
                        if (item.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            ShowMessage("Định mức thực tế này đã khai báo đến HQ không thể xóa", false);
                            return;
                        }
                        else
                        {
                            item.SPCollection = KDT_SXXK_DinhMucThucTe_SP.SelectCollectionBy_DinhMucThucTe_ID(item.ID);
                            foreach (KDT_SXXK_DinhMucThucTe_SP sp in item.SPCollection)
                            {
                                sp.DMCollection = KDT_SXXK_DinhMucThucTe_DinhMuc.SelectCollectionBy_DinhMucThucTeSP_ID(sp.ID);
                                foreach (KDT_SXXK_DinhMucThucTe_DinhMuc ite in sp.DMCollection)
                                {
                                    ite.Delete();
                                }
                                sp.Delete();
                            }
                            item.Delete();
                        }
                    }
                    btnSearch_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (grList.GetRows().Length < 1) return;
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_SXXK_DinhMucThucTeDangKy> DinhMucDangKyCollection = new List<KDT_SXXK_DinhMucThucTeDangKy>();
                if (items.Count < 0) return;
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        KDT_SXXK_DinhMucThucTeDangKy dm = (KDT_SXXK_DinhMucThucTeDangKy)row.GetRow().DataRow;
                        DinhMucDangKyCollection.Add(dm);
                    }
                }
                List<KDT_SXXK_DinhMucThucTe_DinhMuc> DinhMucCollection = new List<KDT_SXXK_DinhMucThucTe_DinhMuc>();
                foreach (KDT_SXXK_DinhMucThucTeDangKy item in DinhMucDangKyCollection)
                {
                    item.SPCollection = KDT_SXXK_DinhMucThucTe_SP.SelectCollectionBy_DinhMucThucTe_ID(item.ID);
                    foreach (KDT_SXXK_DinhMucThucTe_SP ite in item.SPCollection)
                    {
                        ite.DMCollection = KDT_SXXK_DinhMucThucTe_DinhMuc.SelectCollectionBy_DinhMucThucTeSP_ID(ite.ID);
                        foreach (KDT_SXXK_DinhMucThucTe_DinhMuc it in ite.DMCollection)
                        {
                            DinhMucCollection.Add(it);
                        }
                    }
                }
                Report_16_ĐMTT_GSQL f = new Report_16_ĐMTT_GSQL();
                f.BindReport(DinhMucCollection, clcTuNgay.Value, clcDenNgay.Value);
                f.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
