﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Company.KDT.SHARE.VNACCS
{
    public class MIMEPartSign
    {
        public string GuidStr { get; set; }
        public string Content_Type { get; set; }
        public string Name { get; set; }
        public string Content_Transfer_Encoding { get; set; }
        public string Content_Disposition { get; set; }
        public string FileName { get; set; }
        public string Content { get; set; }


        public MIMEPartSign(string guid, string content)
        {
            this.GuidStr = guid;
            this.Content_Type = "application/x-pkcs7-signature";
            this.Name = "smime.p7s";
            this.Content_Transfer_Encoding = "base64";
            this.Content_Disposition = "attachment";
            this.FileName = "smime.p7s";
            this.Content = content;
        }

        public StringBuilder ToEDI()
        {
            StringBuilder header = new StringBuilder();
            if (this.Content != null && this.Content.Length > 0)
            {
                //MemoryStream ms = new MemoryStream(this.Content);
                header.Append(GuidStr).AppendLine();
                string cont_type = string.Format(@"Content-Type: {0}; name=""{1}""", Content_Type, Name);
                header.Append(cont_type).AppendLine();
                header.Append("Content-Transfer-Encoding: " + this.Content_Transfer_Encoding).AppendLine();
                string Disposition = string.Format(@"Content-Disposition: {0}; filename=""{1}""", Content_Disposition, FileName);
                header.Append(Disposition).AppendLine();
                header.AppendLine();

                header.Append(signConvert());
                header.AppendLine();
                header.Append(GuidStr + "--");
            }
            else
            {

            }
            return header;
        }
        public StringBuilder signConvert()
        {
            StringBuilder build = new StringBuilder();
            List<string> listtext = SplitByLength(this.Content, 76);
            for (int i = 0; i < listtext.Count; i++)
            {
                build.Append(listtext[i]);
                build.AppendLine();
            }
            return build;
        }
        private static List<byte[]> splitByteArray(string longString)
        {
            byte[] source = Convert.FromBase64String(longString);
            List<byte[]> result = new List<byte[]>();

            for (int i = 0; i < source.Length; i += 128)
            {
                byte[] buffer = new byte[128];
                Buffer.BlockCopy(source, i, buffer, 0, 128);
                result.Add(buffer);
            }
            return result;
        }
        public  List<string> SplitByLength(string str, int maxLength)
        {
            int index = 0;
            List<string>  strlist = new List<string>();
            while (index + maxLength <= str.Length || (index+maxLength > str.Length && index < str.Length))
            {
                if (index + maxLength > str.Length && index < str.Length)
                {
                    strlist.Add(str.Substring(index, str.Length - index));
                    index += maxLength;
                }
                else
                {
                    strlist.Add(str.Substring(index, maxLength));
                    index += maxLength;
                }
            }
            return strlist;
        }
    }
}
