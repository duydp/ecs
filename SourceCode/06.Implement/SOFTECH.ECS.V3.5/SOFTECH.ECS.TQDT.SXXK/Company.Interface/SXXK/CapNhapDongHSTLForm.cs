﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ThanhKhoan;
using Company.BLL.KDT.SXXK;


namespace Company.Interface.SXXK
{
    public partial class CapNhapDongHSTLForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public CapNhapDongHSTLForm()
        {
            InitializeComponent();
        }

        private void DongHSTLForm_Load(object sender, EventArgs e)
        {
            txtSoHoSo.Value = this.HSTL.SoHoSo;
            txtSoQD.Text = this.HSTL.SoQuyetDinh;
            ccNgayKetThuc.Value = this.HSTL.NgayKetThuc;
            ccNgayQD.Value = this.HSTL.NgayQuyetDinh;
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            this.HSTL.SoHoSo = Convert.ToInt32(txtSoHoSo.Value);
            this.HSTL.SoQuyetDinh = txtSoQD.Text;
            this.HSTL.NgayKetThuc = ccNgayKetThuc.Value;
            this.HSTL.NgayQuyetDinh = ccNgayQD.Value;
            try
            {
                HSTL.Update();
               // ShowMessage("Cập nhật thành công.", false);
                MLMessages("Cập nhật thành công.", "MSG_THK19", "", false);
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
            finally
            {
                this.Close();
            }
            
        }

    }
}