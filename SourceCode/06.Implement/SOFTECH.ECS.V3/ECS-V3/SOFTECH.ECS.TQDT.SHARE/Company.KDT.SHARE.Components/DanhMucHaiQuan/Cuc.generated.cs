using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.DanhMucHaiQuan
{
	public partial class Cuc
	{
		#region Properties.
		
		public string ID { set; get; }
		public string Ten { set; get; }
		public string IPService { set; get; }
		public string ServicePathV1 { set; get; }
		public string ServicePathV2 { set; get; }
		public string ServicePathV3 { set; get; }
		public string IPServiceCKS { set; get; }
		public string ServicePathCKS { set; get; }
		public string IsDefault { set; get; }
		public DateTime DateCreated { set; get; }
		public DateTime DateModified { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<Cuc> ConvertToCollection(IDataReader reader)
		{
			IList<Cuc> collection = new List<Cuc>();
			while (reader.Read())
			{
				Cuc entity = new Cuc();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetString(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
				if (!reader.IsDBNull(reader.GetOrdinal("IPService"))) entity.IPService = reader.GetString(reader.GetOrdinal("IPService"));
				if (!reader.IsDBNull(reader.GetOrdinal("ServicePathV1"))) entity.ServicePathV1 = reader.GetString(reader.GetOrdinal("ServicePathV1"));
				if (!reader.IsDBNull(reader.GetOrdinal("ServicePathV2"))) entity.ServicePathV2 = reader.GetString(reader.GetOrdinal("ServicePathV2"));
				if (!reader.IsDBNull(reader.GetOrdinal("ServicePathV3"))) entity.ServicePathV3 = reader.GetString(reader.GetOrdinal("ServicePathV3"));
				if (!reader.IsDBNull(reader.GetOrdinal("IPServiceCKS"))) entity.IPServiceCKS = reader.GetString(reader.GetOrdinal("IPServiceCKS"));
				if (!reader.IsDBNull(reader.GetOrdinal("ServicePathCKS"))) entity.ServicePathCKS = reader.GetString(reader.GetOrdinal("ServicePathCKS"));
				if (!reader.IsDBNull(reader.GetOrdinal("IsDefault"))) entity.IsDefault = reader.GetString(reader.GetOrdinal("IsDefault"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateCreated"))) entity.DateCreated = reader.GetDateTime(reader.GetOrdinal("DateCreated"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateModified"))) entity.DateModified = reader.GetDateTime(reader.GetOrdinal("DateModified"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static Cuc Load(string id)
		{
			const string spName = "[dbo].[p_HaiQuan_Cuc_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.NVarChar, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<Cuc> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<Cuc> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<Cuc> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_HaiQuan_Cuc_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_HaiQuan_Cuc_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_HaiQuan_Cuc_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_HaiQuan_Cuc_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertCuc(string ten, string iPService, string servicePathV1, string servicePathV2, string servicePathV3, string iPServiceCKS, string servicePathCKS, string isDefault, DateTime dateCreated, DateTime dateModified)
		{
			Cuc entity = new Cuc();	
			entity.Ten = ten;
			entity.IPService = iPService;
			entity.ServicePathV1 = servicePathV1;
			entity.ServicePathV2 = servicePathV2;
			entity.ServicePathV3 = servicePathV3;
			entity.IPServiceCKS = iPServiceCKS;
			entity.ServicePathCKS = servicePathCKS;
			entity.IsDefault = isDefault;
			entity.DateCreated = dateCreated;
			entity.DateModified = dateModified;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_HaiQuan_Cuc_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.NVarChar, ID);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@IPService", SqlDbType.VarChar, IPService);
			db.AddInParameter(dbCommand, "@ServicePathV1", SqlDbType.VarChar, ServicePathV1);
			db.AddInParameter(dbCommand, "@ServicePathV2", SqlDbType.VarChar, ServicePathV2);
			db.AddInParameter(dbCommand, "@ServicePathV3", SqlDbType.VarChar, ServicePathV3);
			db.AddInParameter(dbCommand, "@IPServiceCKS", SqlDbType.VarChar, IPServiceCKS);
			db.AddInParameter(dbCommand, "@ServicePathCKS", SqlDbType.VarChar, ServicePathCKS);
			db.AddInParameter(dbCommand, "@IsDefault", SqlDbType.VarChar, IsDefault);
			db.AddInParameter(dbCommand, "@DateCreated", SqlDbType.DateTime, DateCreated.Year <= 1753 ? DBNull.Value : (object) DateCreated);
			db.AddInParameter(dbCommand, "@DateModified", SqlDbType.DateTime, DateModified.Year <= 1753 ? DBNull.Value : (object) DateModified);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<Cuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Cuc item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateCuc(string id, string ten, string iPService, string servicePathV1, string servicePathV2, string servicePathV3, string iPServiceCKS, string servicePathCKS, string isDefault, DateTime dateCreated, DateTime dateModified)
		{
			Cuc entity = new Cuc();			
			entity.ID = id;
			entity.Ten = ten;
			entity.IPService = iPService;
			entity.ServicePathV1 = servicePathV1;
			entity.ServicePathV2 = servicePathV2;
			entity.ServicePathV3 = servicePathV3;
			entity.IPServiceCKS = iPServiceCKS;
			entity.ServicePathCKS = servicePathCKS;
			entity.IsDefault = isDefault;
			entity.DateCreated = dateCreated;
			entity.DateModified = dateModified;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_HaiQuan_Cuc_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.NVarChar, ID);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@IPService", SqlDbType.VarChar, IPService);
			db.AddInParameter(dbCommand, "@ServicePathV1", SqlDbType.VarChar, ServicePathV1);
			db.AddInParameter(dbCommand, "@ServicePathV2", SqlDbType.VarChar, ServicePathV2);
			db.AddInParameter(dbCommand, "@ServicePathV3", SqlDbType.VarChar, ServicePathV3);
			db.AddInParameter(dbCommand, "@IPServiceCKS", SqlDbType.VarChar, IPServiceCKS);
			db.AddInParameter(dbCommand, "@ServicePathCKS", SqlDbType.VarChar, ServicePathCKS);
			db.AddInParameter(dbCommand, "@IsDefault", SqlDbType.VarChar, IsDefault);
			db.AddInParameter(dbCommand, "@DateCreated", SqlDbType.DateTime, DateCreated.Year <= 1753 ? DBNull.Value : (object) DateCreated);
			db.AddInParameter(dbCommand, "@DateModified", SqlDbType.DateTime, DateModified.Year <= 1753 ? DBNull.Value : (object) DateModified);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<Cuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Cuc item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateCuc(string id, string ten, string iPService, string servicePathV1, string servicePathV2, string servicePathV3, string iPServiceCKS, string servicePathCKS, string isDefault, DateTime dateCreated, DateTime dateModified)
		{
			Cuc entity = new Cuc();			
			entity.ID = id;
			entity.Ten = ten;
			entity.IPService = iPService;
			entity.ServicePathV1 = servicePathV1;
			entity.ServicePathV2 = servicePathV2;
			entity.ServicePathV3 = servicePathV3;
			entity.IPServiceCKS = iPServiceCKS;
			entity.ServicePathCKS = servicePathCKS;
			entity.IsDefault = isDefault;
			entity.DateCreated = dateCreated;
			entity.DateModified = dateModified;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_HaiQuan_Cuc_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.NVarChar, ID);
			db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
			db.AddInParameter(dbCommand, "@IPService", SqlDbType.VarChar, IPService);
			db.AddInParameter(dbCommand, "@ServicePathV1", SqlDbType.VarChar, ServicePathV1);
			db.AddInParameter(dbCommand, "@ServicePathV2", SqlDbType.VarChar, ServicePathV2);
			db.AddInParameter(dbCommand, "@ServicePathV3", SqlDbType.VarChar, ServicePathV3);
			db.AddInParameter(dbCommand, "@IPServiceCKS", SqlDbType.VarChar, IPServiceCKS);
			db.AddInParameter(dbCommand, "@ServicePathCKS", SqlDbType.VarChar, ServicePathCKS);
			db.AddInParameter(dbCommand, "@IsDefault", SqlDbType.VarChar, IsDefault);
			db.AddInParameter(dbCommand, "@DateCreated", SqlDbType.DateTime, DateCreated.Year <= 1753 ? DBNull.Value : (object) DateCreated);
			db.AddInParameter(dbCommand, "@DateModified", SqlDbType.DateTime, DateModified.Year <= 1753 ? DBNull.Value : (object) DateModified);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<Cuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Cuc item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteCuc(string id)
		{
			Cuc entity = new Cuc();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_HaiQuan_Cuc_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.NVarChar, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_HaiQuan_Cuc_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<Cuc> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (Cuc item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}