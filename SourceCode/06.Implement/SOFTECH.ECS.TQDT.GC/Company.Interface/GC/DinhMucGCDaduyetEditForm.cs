﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;
using Company.GC.BLL.DuLieuChuan;

namespace Company.Interface.GC
{
    public partial class DinhMucGCDaduyetEditForm : BaseForm
    {
        public Company.GC.BLL.GC.DinhMuc DMDetail = new Company.GC.BLL.GC.DinhMuc();                
        // public DinhMucDangKy dmdk=new DinhMucDangKy();
        Company.GC.BLL.GC.DinhMucCollection dmColl = new Company.GC.BLL.GC.DinhMucCollection();
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        public bool isDuyet = false;
        public DinhMucGCDaduyetEditForm()
        {
            InitializeComponent();
            this._DonViTinh = DonViTinh.SelectAll();
            txtDonViTinhSP.DataSource = this._DonViTinh;
            txtDonViTinhSP.DisplayMember = "Ten";
            txtDonViTinhSP.ValueMember = "ID";
            txtDonViTinhSP.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            txtDonViTinhNPL.DataSource = this._DonViTinh;
            txtDonViTinhNPL.DisplayMember = "Ten";
            txtDonViTinhNPL.ValueMember = "ID";
            txtDonViTinhNPL.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
        }

        private void txtMaSP_Leave(object sender, EventArgs e)
        {
            if (txtMaSP.Text.Trim() == "")
                return;                     
            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
            sp.Ma = txtMaSP.Text.Trim();
            sp.HopDong_ID = HD.ID ;
            sp.Load();
            if (sp != null && sp.Ten.Length > 0)
            {
                txtMaSP.Text = sp.Ma;
                txtTenSP.Text = sp.Ten.Trim();
                txtMaHSSP.Text = sp.MaHS.Trim();
                txtDonViTinhSP.SelectedValue = sp.DVT_ID; ;
                txtMaNPL.Focus();
            }
            else
            {
                error.SetError(txtMaSP,setText("Không tồn tại sản phẩm này.","This value is not exist"));
                txtMaSP.Clear();
                txtMaSP.Focus();
                return;
            }
        }

        private void txtMaSP_ButtonClick(object sender, EventArgs e)
        {
            SanPhamRegistedGCForm f = new SanPhamRegistedGCForm();
            f.isDisplayAll = 0;   
            //HopDong hd=new HopDong();
            //hd.ID = this.HD.ID;  
            f.SanPhamSelected.HopDong_ID = HD.ID;
            f.ShowDialog();
            if (f.SanPhamSelected != null && f.SanPhamSelected.Ten != "")
            {
                txtMaSP.Text = f.SanPhamSelected.Ma.Trim();
                txtTenSP.Text = f.SanPhamSelected.Ten.Trim();
                txtMaHSSP.Text = f.SanPhamSelected.MaHS.Trim();
                txtDonViTinhSP.SelectedValue = f.SanPhamSelected.DVT_ID;
                txtMaNPL.Focus();
            }
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {            
            Company.Interface.KDT.GC.DinhMucSelectNguyenPhuLieuForm f = new Company.Interface.KDT.GC.DinhMucSelectNguyenPhuLieuForm();            
            f.isBrower = true;
            f.npl.HopDong_ID = this.HD.ID ;
            //HopDong hd = new HopDong();
            //hd.ID = dmdk.ID_HopDong;
            f.HD = this.HD;
            f.ShowDialog();
            if (f.npl!=null && f.npl.Ma != "")
            {
                txtMaNPL.Text = f.npl.Ma.Trim();
                txtTenNPL.Text = f.npl.Ten.Trim();
                txtMaHSNPL.Text = f.npl.MaHS.Trim();
                txtDonViTinhNPL.SelectedValue = f.npl.DVT_ID;
                txtDinhMuc.Focus();
            }
        }

        private void DinhMucGCEditForm_Load(object sender, EventArgs e)
        {
            txtDinhMuc.DecimalDigits = Convert.ToInt32(GlobalSettings.SoThapPhan.DinhMuc);
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH  ;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            // 1. Kiểm tra dữ liệu.
            //cvError.Validate();
            //if (!cvError.IsValid) return;

            //if (!this.existNPL)
            //{
            //    error.SetIconPadding(txtMaNPL, -8);
            //    error.SetError(txtMaNPL, "Không tồn tại nguyên phụ liệu này.");
            //    return;
            //}
            //if (!this.existSP)
            //{
            //    error.SetIconPadding(txtMaSP, -8);
            //    error.SetError(txtMaSP, "Không tồn tại sản phẩm này.");
            //    return;
            //}

            //// 2. Cập nhật dữ liệu.
            //this.DMDetail.MaSanPham = txtMaSP.Text;
            //this.DMDetail.MaNguyenPhuLieu = txtMaNPL.Text;
            //this.DMDetail.DinhMucSuDung = Convert.ToDecimal(txtDinhMuc.Value);
            //this.DMDetail.TyLeHaoHut = Convert.ToDecimal(txtTyLeHH.Value);

            //// 3. Đóng.
            //this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            txtMaSP.Text = "";
            txtTenSP.Text = "";
            txtMaHSSP.Text = "";
            txtDinhMuc.Text = "0";
            txtMaHSNPL.Text = "";
            txtMaNPL.Text = "";
            txtTenNPL.Text = "";
            txtTyLeHH.Text = "0.000";
            //txtGhiChu.Text = "";
            txtNPLtucung.Text = "0";
            this.DMDetail = new Company.GC.BLL.GC.DinhMuc();
        }
        

        private void btnAdd_Click(object sender, EventArgs e)
        {
            txtMaSP_Leave(null, null);
            txtMaNPL_Leave(null, null);
            cvError.Validate();
            if (!cvError.IsValid) return;
            if (Convert.ToDecimal(txtDinhMuc.Text) == 0)
            {
                error.SetError(txtDinhMuc, setText("Định mức phải lớn hơn 0","This value must be greater than 0"));
                return;
            }            
            checkExitsDinhMucAndSTTHang();
        }
        private void checkExitsDinhMucAndSTTHang()
        {
            
            Company.GC.BLL.GC.DinhMuc dmDuyet = new Company.GC.BLL.GC.DinhMuc();
            dmDuyet.HopDong_ID = this.HD.ID ;
            dmDuyet.MaSanPham = txtMaSP.Text.Trim();
            dmDuyet.MaNguyenPhuLieu = txtMaNPL.Text.Trim();
            if (dmDuyet.CheckExitsDinhMucSanPhamNguyenPhuLieu())
            {
                showMsg("MSG_WRN15", new String[] {txtMaSP.Text, txtMaNPL.Text});
                //ShowMessage("Sản phẩm : " + txtMaSP.Text.Trim() + " và nguyên phụ liệu : " + txtMaNPL.Text.Trim() + " đã được khai báo định mức.", false);
                return;
            }
          
            DMDetail.HopDong_ID = this.HD.ID;
            DMDetail.MaSanPham = txtMaSP.Text.Trim();
            DMDetail.MaNguyenPhuLieu = txtMaNPL.Text.Trim();
            DMDetail.TyLeHaoHut = Convert.ToDecimal(txtTyLeHH.Text);
            DMDetail.DinhMucSuDung = Convert.ToDecimal(txtDinhMuc.Text);            
            DMDetail.NPL_TuCungUng = Convert.ToDecimal(txtNPLtucung.Text);            
            DMDetail.TenNPL = txtTenNPL.Text.Trim();
            DMDetail.TenSanPham = txtTenSP.Text.Trim();
            try
            {
                DMDetail.Insert();
                dmColl.Add(DMDetail);
                dgList.DataSource = dmColl;
                dgList.Refetch();               
                reset();
            }
            catch(Exception ex){
                showMsg("MSG_2702004", ex.Message);
                //Message("MSG_THK63", ex.Message, false);
                //ShowMessage("Có lỗi khi thêm mới : "+ex.Message, false);
            }               
        }
        private void reset()
        {
            txtMaNPL.Focus();
            txtMaNPL.Text = "";
            txtTenNPL.Text = "";
            txtTyLeHH.Text ="0";
            txtDinhMuc.Text = "0";
            txtTenNPL.Text = "";            
            txtMaHSNPL.Text = "";
            txtGhiChu.Text = "";
            txtNPLtucung.Text = "0";          
            DMDetail = new Company.GC.BLL.GC.DinhMuc();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {              
                DMDetail = (Company.GC.BLL.GC.DinhMuc)e.Row.DataRow;
                txtMaSP.Text = DMDetail.MaSanPham;
                txtMaNPL.Text = DMDetail.MaNguyenPhuLieu;                
                //load thong tin sp
                txtMaNPL_Leave(null, null);
                txtMaSP_Leave(null, null);
                txtDinhMuc.Text = DMDetail.DinhMucSuDung.ToString();
                txtTyLeHH.Text = DMDetail.TyLeHaoHut.ToString();
                txtNPLtucung.Text = DMDetail.NPL_TuCungUng.ToString();
                txtDinhMuc.Focus();
            }
        }

        private void dgList_DeletingRecord(object sender, Janus.Windows.GridEX.RowActionCancelEventArgs e)
        {
            
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            if (txtMaNPL.Text.Trim() == "")
                return;
            Company.GC.BLL.GC.NguyenPhuLieu npl = (new Company.GC.BLL.GC.NguyenPhuLieu());
            npl.Ma = txtMaNPL.Text.Trim();
            npl.HopDong_ID = this.HD.ID  ;
            npl.Load();
            if (npl != null && npl.Ten.Length > 0)
            {
                txtMaNPL.Text = npl.Ma;
                txtTenNPL.Text = npl.Ten.Trim();
                txtMaHSNPL.Text = npl.MaHS.Trim();
                txtDonViTinhNPL.SelectedValue = npl.DVT_ID;
                txtDinhMuc.Focus();
            }
            else
            {
                error.SetError(txtMaNPL,setText("Không tồn tại nguyên phụ liệu này.","This value is not exist"));
                txtMaNPL.Clear();
                txtMaNPL.Focus();
                return;
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //if (ShowMessage("Bạn có muốn xóa định mức của sản phẩm này không?", true) == "Yes")
            if (showMsg("MSG_2702005", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.GC.DinhMuc dm = (Company.GC.BLL.GC.DinhMuc)row.GetRow().DataRow;
                        dm.Delete();
                    }
                }                   
            }
            else
            {
                e.Cancel = true;
            }            
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                Company.GC.BLL.GC.DinhMuc DM = (Company.GC.BLL.GC.DinhMuc)e.Row.DataRow;
                if (DM.TenNPL != null && DM.TenNPL.Trim().Length == 0)
                {
                    Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
                    NPL.HopDong_ID = DM.HopDong_ID;
                    NPL.Ma = DM.MaNguyenPhuLieu;
                    NPL.Load();
                    DM.TenNPL = NPL.Ten;
                }

               
            }
        }
    }
}