using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.SXXK
{
    public partial class SaoChepDinhMucForm : BaseForm
    {
        public BLL.SXXK.DinhMucCollection collectionCopy = new Company.BLL.SXXK.DinhMucCollection();
        public DinhMucDangKy dmdk = new DinhMucDangKy();
        public SaoChepDinhMucForm()
        {
            InitializeComponent();
            dmdk.DMCollection = new DinhMucCollection();
        }
        public string MaSP = "";
        private void btnClose_Click(object sender, EventArgs e)
        {
            collectionCopy.Clear();
            this.Close();
        }
     
     
        private void txtMaSP_ButtonClick(object sender, EventArgs e)
        {
            Company.Interface.SXXK.DinhMucRegistedForm f = new Company.Interface.SXXK.DinhMucRegistedForm();
            f.isBrower = true;
            f.maSP = "";
            f.ShowDialog();
            if (f.maSP != "")
            {
                txtMaSP.Text = f.maSP.Trim();
                BLL.SXXK.DinhMuc dmCopy = new Company.BLL.SXXK.DinhMuc();
                collectionCopy = dmCopy.SelectCollectionDynamic("masanpham='" + f.maSP + "' and mahaiquan='" + GlobalSettings.MA_HAI_QUAN + "' and madoanhnghiep='" + GlobalSettings.MA_DON_VI + "'", "");
                foreach (DinhMuc dmSend in dmdk.DMCollection)
                {
                    foreach (BLL.SXXK.DinhMuc dmSaoChep in collectionCopy)
                    {
                        if (dmSaoChep.MaNguyenPhuLieu.Trim() == dmSend.MaNguyenPhuLieu.Trim() && dmSend.MaSanPham.Trim() == MaSP.Trim())
                        {
                          //  ShowMessage("Định mức của nguyên phụ liệu : " + dmSaoChep.MaNguyenPhuLieu + " đã được khai báo trên lưới nên không được sao chép", false);
                            MLMessages("Định mức của nguyên phụ liệu : " + dmSaoChep.MaNguyenPhuLieu + " đã được khai báo trên lưới nên không được sao chép", "MSG_PUB10", "", false);
                            collectionCopy.Remove(dmSaoChep);
                            break;
                        }
                    }
                }
                dgList.DataSource = collectionCopy;
                try
                {
                    dgList.Refetch();
                }
                catch { dgList.Refresh(); }
            }
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            foreach (BLL.SXXK.DinhMuc dm in collectionCopy)
            {
                if (dm.DinhMucSuDung == 0)
                {
                    //ShowMessage("Định mức của nguyên phụ liệu :'"+dm.MaNguyenPhuLieu+"' không được = 0", false);
                    MLMessages("Định mức của nguyên phụ liệu :'" + dm.MaNguyenPhuLieu + "' không được = 0", "MSG_PUB10", "", false);
                    return;
                }
            }
            this.Close();
        }

        private void txtMaSP_Leave(object sender, EventArgs e)
        {
            if (collectionCopy.Count != 0) return;
            cvError.Validate();
            if (!cvError.IsValid) return;
            Company.BLL.SXXK.ThongTinDinhMuc dm = new Company.BLL.SXXK.ThongTinDinhMuc();
            dm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            dm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            dm.MaSanPham = txtMaSP.Text.Trim();
            if (!dm.Load())
            {
                //  ShowMessage("Sản phẩm này chưa có định mức", false);
                MLMessages("Sản phẩm này chưa có định mức", "MSG_DMC05", "", false);
                return;
            }
            BLL.SXXK.DinhMuc dmCopy = new Company.BLL.SXXK.DinhMuc();
            collectionCopy = dmCopy.SelectCollectionDynamic("masanpham='" + txtMaSP.Text.Trim() + "' and mahaiquan='" + GlobalSettings.MA_HAI_QUAN + "' and madoanhnghiep='" + GlobalSettings.MA_DON_VI + "'", "");
            foreach (DinhMuc dmSend in dmdk.DMCollection)
            {
                foreach(BLL.SXXK.DinhMuc dmSaoChep in collectionCopy)
                {
                    if (dmSaoChep.MaNguyenPhuLieu.Trim() == dmSend.MaNguyenPhuLieu.Trim() && dmSend.MaSanPham.Trim() == MaSP.Trim())
                    {
                        //ShowMessage("Định mức của nguyên phụ liệu : " + dmSaoChep.MaNguyenPhuLieu +" đã được khai báo trên lưới nên không được sao chép", false);
                        MLMessages("Định mức của nguyên phụ liệu : " + dmSaoChep.MaNguyenPhuLieu + " đã được khai báo trên lưới nên không được sao chép", "MSG_PUB10", "", false);
                        collectionCopy.Remove(dmSaoChep);
                        break;
                    }
                }
            }
            dgList.DataSource = collectionCopy;
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
        }

        private void SaoChepDinhMucForm_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
        }

       
     
    }
}