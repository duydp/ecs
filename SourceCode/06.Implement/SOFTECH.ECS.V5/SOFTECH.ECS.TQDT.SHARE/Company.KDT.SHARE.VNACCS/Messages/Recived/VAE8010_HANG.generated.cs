﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAE8010_HANG : BasicVNACC
    {
        public static int TongSoByte { get; set; }
        public PropertiesAttribute D01 { get; set; }
        public PropertiesAttribute D02 { get; set; }
        public PropertiesAttribute D03 { get; set; }
        public PropertiesAttribute D04 { get; set; }
        public PropertiesAttribute D05 { get; set; }
        public PropertiesAttribute D06 { get; set; }
        public PropertiesAttribute D07 { get; set; }
        public PropertiesAttribute D08 { get; set; }
        public PropertiesAttribute D09 { get; set; }
        public PropertiesAttribute D10 { get; set; }

        public VAE8010_HANG()
        {
            D01 = new PropertiesAttribute(3, typeof(string));
            D02 = new PropertiesAttribute(600, typeof(string));
            D03 = new PropertiesAttribute(15, typeof(int));
            D04 = new PropertiesAttribute(4, typeof(string));
            D05 = new PropertiesAttribute(15, typeof(int));
            D06 = new PropertiesAttribute(4, typeof(string));
            D07 = new PropertiesAttribute(15, typeof(int));
            D08 = new PropertiesAttribute(4, typeof(string));
            D09 = new PropertiesAttribute(20, typeof(string));
            D10 = new PropertiesAttribute(20, typeof(string));
            TongSoByte = 720;
        }

    }
    public partial class EnumGroupID
    {
    }

}
