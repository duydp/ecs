﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace KeyServer
{
    public partial class Product
    {
                
        public static List<Product> LoadByPermission(int PermissionId)
        {
            const string spName = "[dbo].[p_LM_Product_LoadbyPermission]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Permission", SqlDbType.NVarChar, PermissionId);
            IDataReader reader = db.ExecuteReader(dbCommand);
            List<Product> collection = new List<Product>();

            while (reader.Read())
            {
                Product entity = new Product();

                if (!reader.IsDBNull(reader.GetOrdinal("Product_ID"))) entity.Product_ID = reader.GetString(reader.GetOrdinal("Product_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Group_ID"))) entity.Group_ID = reader.GetInt64(reader.GetOrdinal("Group_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Name"))) entity.Name = reader.GetString(reader.GetOrdinal("Name"));
                if (!reader.IsDBNull(reader.GetOrdinal("Description"))) entity.Description = reader.GetString(reader.GetOrdinal("Description"));
                if (!reader.IsDBNull(reader.GetOrdinal("Prefix"))) entity.Prefix = reader.GetString(reader.GetOrdinal("Prefix"));
                if (!reader.IsDBNull(reader.GetOrdinal("Creator"))) entity.Creator = reader.GetString(reader.GetOrdinal("Creator"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreatedDate"))) entity.CreatedDate = reader.GetDateTime(reader.GetOrdinal("CreatedDate"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;		
        }

    }
}
