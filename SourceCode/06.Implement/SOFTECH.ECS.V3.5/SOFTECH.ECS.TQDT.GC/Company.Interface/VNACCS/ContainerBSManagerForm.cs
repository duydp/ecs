﻿using System;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.VNACCS;
namespace Company.Interface.VNACCS
{
    public partial class ContainerBSManagerForm : BaseForm
    {

        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        private List<KDT_ContainerDangKy> ListContDK = new List<KDT_ContainerDangKy>();
        private KDT_ContainerDangKy ContDK = new KDT_ContainerDangKy();
        public KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();

        public ContainerBSManagerForm()
        {
            InitializeComponent();
        }

        private void ContainerBSManagerForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (TKMD.SoLuongCont == 0)
                {
                    this.Text = "Danh sách Lấy phản hồi hàng hóa đủ diều kiện qua KVGS";
                    cmdThemTKTX.Text = cmdThemTKTX1.Text = "Lấy phản hồi Danh sách hàng hóa đủ điều kiện qua KVGS";
                }
                LoadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void LoadData()
        {
            try
            {
                ListContDK = KDT_ContainerDangKy.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID, "");
                dgList.DataSource = ListContDK;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (trangthai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;
                    case "-3":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                        break;
                    case "6":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã có danh sách hàng qua KVGS";
                        break;
                    case "7":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa có phản hồi qua KVGS";
                        break;
                    case "8":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã có phản hồi qua KVGS";
                        break;
                }

            }

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<KDT_ContainerDangKy> ItemColl = new List<KDT_ContainerDangKy>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessageTQDT(" Thông báo từ hệ thống ", "Doanh nghiệp có muốn xóa Danh sách Container này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_ContainerDangKy)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_ContainerDangKy item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.DeleteFull(item.ID);
                    }
                    dgList.Refetch();
                }
                LoadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                    ContDK = KDT_ContainerDangKy.LoadFull(id);
                    ContainerBSForm frm = new ContainerBSForm();
                    frm.ContDK = ContDK;
                    frm.TKMD = TKMD;
                    frm.ShowDialog();
                    LoadData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThemTKTX":
                    ThemCont();
                    break;
                case "cmdDelete":
                    btnXoa_Click(null, null);
                    break;
            }

        }
        private void ThemCont()
        {
            try
            {
                ContDK = new KDT_ContainerDangKy();
                ContDK.MaDoanhNghiep = MaDoanhNghiep;
                ContDK.MaHQ = MaHaiQuan;
                ContDK.TKMD_ID = TKMD.ID;
                ContainerBSForm frm = new ContainerBSForm();
                frm.ContDK = ContDK;
                frm.TKMD = TKMD;
                frm.ShowDialog();
                LoadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }



    }
}
