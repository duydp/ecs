using System.Data;
using System.Data.Common;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public class LoaiHinhMauDich : BaseClass
    {
        public static DataTable SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_LoaiHinhMauDich ORDER BY Ten";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public static string GetTenVietTac(string maLoaiHinh)
        {
            //string query = "SELECT Ten_VT FROM t_HaiQuan_LoaiHinhMauDich WHERE ID = '" + maLoaiHinh + "'";
            //DbCommand dbCommand = db.GetSqlStringCommand(query);
            //return db.ExecuteScalar(dbCommand).ToString();

            try
            {
                string query = string.Format("[ID] = '{0}'", maLoaiHinh.Trim());

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["LoaiHinhMauDich"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["Ten_VT"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return string.Empty;
        }
        public static DataTable SelectBy_Nhom(string nhom)
        {
            string NhomTaiChe;
            string NhomGC;
            string NhomCheXuat = "CX";
            string NhomChungTuCheXuat = "C{0}C";

            if (nhom.Substring(0, 1).ToUpper() == "N")
            {
                NhomTaiChe = "NTA";
                NhomGC = "'NGC06','NGC09','NDT11'";
                NhomCheXuat = "N" + NhomCheXuat;
                NhomChungTuCheXuat = string.Format(NhomChungTuCheXuat, "N");
            }
            else
            {
                NhomTaiChe = "XTA";
                NhomGC = "'XGC06','XGC09','XDT11'";
                NhomCheXuat = "X" + NhomCheXuat;
                NhomChungTuCheXuat = string.Format(NhomChungTuCheXuat, "X");
            }

            string query = string.Format("SELECT * FROM t_HaiQuan_LoaiHinhMauDich WHERE LEFT([ID], 3) = '{0}' OR ID LIKE '{1}%' OR ID in ({2}) OR ID like '{3}%' OR ID like '{4}%'  ORDER BY ID", nhom, NhomTaiChe, NhomGC, NhomCheXuat, NhomChungTuCheXuat);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public static DataTable SelectBy_NhomOne(string one)
        {
            string query = string.Format("SELECT * FROM t_HaiQuan_LoaiHinhMauDich WHERE LEFT([ID], 1) = '{0}' ORDER BY Ten", one);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        //HungTQ, Update 30052010
        public static string GetName(string maLoaiHinh)
        {
            //string query = "SELECT Ten FROM t_HaiQuan_LoaiHinhMauDich WHERE ID = '" + maLoaiHinh + "'";
            //DbCommand dbCommand = db.GetSqlStringCommand(query);
            //return db.ExecuteScalar(dbCommand).ToString();

            try
            {
                string query = string.Format("[ID] = '{0}'", maLoaiHinh.Trim());

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["LoaiHinhMauDich"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["Ten"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return string.Empty;
        }

        //Hungtq, 14/10/2011.
        public static DataTable SelectByMa(string ma)
        {
            string query = string.Format("SELECT * FROM t_HaiQuan_LoaiHinhMauDich WHERE ID = '{0}' ORDER BY Ten", ma);
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

    }
}