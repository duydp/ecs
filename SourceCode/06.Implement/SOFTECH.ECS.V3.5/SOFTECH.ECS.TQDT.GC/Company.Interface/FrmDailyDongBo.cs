﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Company.Interface
{
    public partial class FrmDailyDongBo : BaseForm
    {
        public List<Company.Interface.wssyndata.DaiLy> listDaiLy = new List<Company.Interface.wssyndata.DaiLy>();
        public string userDaily="";
        public FrmDailyDongBo()
        {
            InitializeComponent();
        }

        private void FrmDailyDongBo_Load(object sender, EventArgs e)
        {
            grList.DataSource= listDaiLy;
            grList.Refetch();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
                Janus.Windows.GridEX.GridEXRow[] listChecked = grList.GetCheckedRows();
               
                if (listChecked.Count() > 0)
                {
                    for (int i = 0; i < listChecked.Length; i++)
                    {
                        userDaily += "," + listChecked[i].Cells["ID"].Value.ToString();
                    }
                    this.Close();
                }
                else
                {
                    ShowMessage("Chọn User để thực hiện đồng bộ dữ liệu",false);
                }

        }
    }
}
