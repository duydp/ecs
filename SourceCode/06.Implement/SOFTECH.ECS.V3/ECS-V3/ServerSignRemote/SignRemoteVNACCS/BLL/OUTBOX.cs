using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace SignRemote
{
    public partial class OUTBOX
    {
#if !SERVICE_SIGN
        public static void InsertDataSignLocal(string guidId, string signData, string signCert)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            OUTBOX.InsertOUTBOX(string.Empty, string.Empty, string.Empty, signData, signCert, DateTime.Now, -1, guidId);
        }
        public static OUTBOX GetContentSignLocal(string cnnString, string guidId)
        {
            SqlConnection cnn = new SqlConnection(cnnString);
            cnn.Open();
            string cmdText = string.Format("Select top(1) * from t_MSG_OUTBOX where GUIDSTR='{0}' order by CREATE_TIME desc", guidId);
            SqlCommand sqlCmd = new SqlCommand(cmdText, cnn);
            IDataReader reader = sqlCmd.ExecuteReader();
            OUTBOX outbox = null;
            if (reader.Read())
            {
                outbox = new OUTBOX();
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_DATA"))) outbox.MSG_SIGN_DATA = reader.GetString(reader.GetOrdinal("MSG_SIGN_DATA"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_CERT"))) outbox.MSG_SIGN_CERT = reader.GetString(reader.GetOrdinal("MSG_SIGN_CERT"));
            }
            reader.Close();
            cmdText = string.Format("Delete t_MSG_OUTBOX where  GUIDSTR='{0}'", guidId);
            sqlCmd.CommandText = cmdText;
            sqlCmd.ExecuteNonQuery();
            return outbox;
        }
#endif
    }

}