﻿namespace Company.Interface.Controls
{
    partial class CuaKhauHControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout cbList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CuaKhauHControl));
            this.txtID = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cbList = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.ds = new System.Data.DataSet();
            this.dtCuaKhau = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.rfvTen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            ((System.ComponentModel.ISupportInitialize)(this.cbList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCuaKhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).BeginInit();
            this.SuspendLayout();
            // 
            // txtID
            // 
            this.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtID.Location = new System.Drawing.Point(0, 0);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(49, 21);
            this.txtID.TabIndex = 0;
            this.txtID.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtID.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtID.Leave += new System.EventHandler(this.txtMaLoaiHinh_Leave);
            // 
            // cbList
            // 
            this.cbList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbList.ComboStyle = Janus.Windows.GridEX.ComboStyle.DropDownList;
            this.cbList.DataMember = "CuaKhau";
            this.cbList.DataSource = this.ds;
            cbList_DesignTimeLayout.LayoutString = resources.GetString("cbList_DesignTimeLayout.LayoutString");
            this.cbList.DesignTimeLayout = cbList_DesignTimeLayout;
            this.cbList.DisplayMember = "Ten";
            this.cbList.Location = new System.Drawing.Point(55, 0);
            this.cbList.Name = "cbList";
            this.cbList.SelectedIndex = -1;
            this.cbList.SelectedItem = null;
            this.cbList.Size = new System.Drawing.Size(177, 21);
            this.cbList.TabIndex = 1;
            this.cbList.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.cbList.ValueMember = "ID";
            this.cbList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbList.ValueChanged += new System.EventHandler(this.cbList_ValueChanged);
            // 
            // ds
            // 
            this.ds.DataSetName = "DuLieuChuan";
            this.ds.Tables.AddRange(new System.Data.DataTable[] {
            this.dtCuaKhau});
            // 
            // dtCuaKhau
            // 
            this.dtCuaKhau.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2});
            this.dtCuaKhau.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "ID"}, true)});
            this.dtCuaKhau.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn1};
            this.dtCuaKhau.TableName = "CuaKhau";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "ID";
            // 
            // dataColumn2
            // 
            this.dataColumn2.AllowDBNull = false;
            this.dataColumn2.ColumnName = "Ten";
            // 
            // rfvTen
            // 
            this.rfvTen.ControlToValidate = this.cbList;
            this.rfvTen.ErrorMessage = "\"Cửa khẩu\" không được bỏ trống.";
            this.rfvTen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTen.Icon")));
            // 
            // CuaKhauHControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.cbList);
            this.Controls.Add(this.txtID);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CuaKhauHControl";
            this.Size = new System.Drawing.Size(235, 25);
            this.Load += new System.EventHandler(this.CuaKhauControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cbList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCuaKhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Janus.Windows.GridEX.EditControls.EditBox txtID;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbList;
        private System.Data.DataSet ds;
        private System.Data.DataTable dtCuaKhau;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTen;
    }
}