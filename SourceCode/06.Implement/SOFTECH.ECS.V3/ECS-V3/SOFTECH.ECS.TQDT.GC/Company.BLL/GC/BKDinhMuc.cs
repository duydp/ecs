using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;

namespace Company.GC.BLL.GC
{
    public partial class BKDinhMuc  
    {
        public DataSet getDinhMuc(long IDHopDong,string maSP)
        {
            Database db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string strSQL = " select * from t_GC_BKDinhMuc Where OldHD_ID =@OldHD_ID and MaSP=@MaSP";
            DbCommand dbcommand = db.GetSqlStringCommand(strSQL);
            db.AddInParameter(dbcommand, "@OldHD_ID", DbType.Int64, IDHopDong);
            db.AddInParameter(dbcommand, "@MaSP", DbType.String , maSP);
            return db.ExecuteDataSet(dbcommand);
        }

        public int DeleteTK(long IDHopDong)
        {

            string strSQL = " Delete from t_GC_BKDinhMuc Where OldHD_ID =@OldHD_ID  ";
            DbCommand dbCommand = this.db.GetSqlStringCommand(strSQL);
            db.AddInParameter(dbCommand, "@OldHD_ID", DbType.Int64, IDHopDong);
            return this.db.ExecuteNonQuery(dbCommand);
        }
        public bool UpdateGrid(BKDinhMucCollection  collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BKDinhMuc  item in collection)
                    {
                        BKDinhMuc bk = new BKDinhMuc();
                        bk.ID = item.ID;
                        bk.DinhMucSD = item.DinhMucSD;
                        bk.DinhMucTH = item.DinhMucTH;
                        bk.DVT = item.DVT;
                        bk.GhiChu = item.GhiChu;
                        bk.MaNPL  = item.MaNPL;
                        bk.OldHD_ID = item.OldHD_ID;
                        bk.MaSP = item.MaSP;
                        bk.NguonNL = item.NguonNL;
                        bk.STT = item.STT;
                        bk.TenNPL = item.TenNPL;
                        bk.TyLeHH = item.TyLeHH; 
                        if (bk.UpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool UpdateDataSetFromGrid(DataSet ds)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (DataRow dr in ds.Tables[0].Rows )
                    {
                        BKDinhMuc bk = new BKDinhMuc();
                        bk.ID = Convert.ToInt64(dr["ID"]);
                        bk.DinhMucSD = Convert.ToDecimal(dr["DinhMucSD"]);
                        bk.DinhMucTH = Convert.ToDecimal(dr["DinhMucTH"]);
                        bk.DVT = dr["DVT"].ToString();
                        bk.GhiChu = dr["GhiChu"].ToString();
                        bk.MaNPL = dr["MaNPL"].ToString();
                        bk.OldHD_ID = Convert.ToInt64(dr["OldHD_ID"]);
                        bk.MaSP = dr["MaSP"].ToString();
                        bk.NguonNL = dr["NguonNL"].ToString();
                        bk.STT = Convert.ToInt32(dr["STT"]);
                        bk.TenNPL = dr["TenNPL"].ToString();
                        bk.TyLeHH = Convert.ToDecimal(dr["TyLeHH"]);
                        if (bk.UpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
    }
}