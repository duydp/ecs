﻿using System;
using Company.BLL;
using Company.BLL.SXXK ;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.SXXK;
using NguyenPhuLieu=Company.BLL.SXXK.NguyenPhuLieu;
using SanPham=Company.BLL.SXXK.SanPham;
using Company.KDT.SHARE.Components;

namespace Company.Interface.SXXK
{
    public partial class DinhMucEditForm : BaseForm
    {
        public DinhMuc DMDetail = new DinhMuc();
        public DinhMucCollection DMCollection =new DinhMucCollection();
        DinhMuc dinhmuc = new DinhMuc();
       // public DinhMucDangKy dmDangKy;
        private bool existNPL;
        private bool existSP;
        public string maSP = "";
        SanPhamRegistedForm fsp = new SanPhamRegistedForm();
        NguyenPhuLieuRegistedForm fnpl = new NguyenPhuLieuRegistedForm();
        public DinhMucEditForm()
        {
            InitializeComponent();
            //fsp.status = true;
            //fnpl.status = true;
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            NguyenPhuLieu nplSXXK = new NguyenPhuLieu();
            nplSXXK.MaHaiQuan = this.MaHaiQuan.Trim();
            nplSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            nplSXXK.Ma = txtMaNPL.Text.Trim();
            if (nplSXXK.Load())
            {
                this.existNPL = true;
                txtMaNPL.Text = nplSXXK.Ma.Trim();
                txtTenNPL.Text = nplSXXK.Ten.Trim();
                txtMaHSNPL.Text = nplSXXK.MaHS;
                txtDonViTinhNPL.Text = DonViTinh.GetName(nplSXXK.DVT_ID);
                error.SetError(txtMaNPL, string.Empty);
            }
            else
            {
                this.existNPL = false;
                error.SetIconPadding(txtMaNPL, -8);

                //error.SetError(txtMaNPL, "Không tồn tại nguyên phụ liệu này.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaNPL, "Không tồn tại nguyên phụ liệu này.");
                }
                else
                {
                    error.SetError(txtMaNPL, "This material haven't exist.");
                }
                
                txtTenNPL.Text = txtMaHSNPL.Text = txtDonViTinhNPL.Text = string.Empty;
            }
        }

        private void txtMaSP_ButtonClick(object sender, EventArgs e)
        {
            if (fsp == null)
            {
                fsp = new SanPhamRegistedForm ();
                //fsp.status = true;
            } 
             fsp.CalledForm = this.Name;
            fsp.ShowDialog(this);

            if (fsp.SanPhamSelected.Ma.Trim().Length > 0)
            {
                existSP = true;
                txtMaSP.Text = fsp.SanPhamSelected.Ma.Trim();
                txtTenSP.Text = fsp.SanPhamSelected.Ten.Trim();
                txtMaHSSP.Text = fsp.SanPhamSelected.MaHS;
                txtDonViTinhSP.Text = DonViTinh.GetName(fsp.SanPhamSelected.DVT_ID);
            }
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {
            if (fnpl == null)
            {
                fnpl = new NguyenPhuLieuRegistedForm ();
               // fnpl.status = true;
            }
            fnpl.CalledForm = this.Name;
            fnpl.ShowDialog(this);
            if (fnpl.NguyenPhuLieuSelected.Ma.Trim().Length > 0)
            {
                existNPL = true;
                txtMaNPL.Text = fnpl.NguyenPhuLieuSelected.Ma.Trim();
                txtTenNPL.Text = fnpl.NguyenPhuLieuSelected.Ten.Trim();
                txtMaHSNPL.Text = fnpl.NguyenPhuLieuSelected.MaHS;
                txtDonViTinhNPL.Text = DonViTinh.GetName(fnpl.NguyenPhuLieuSelected.DVT_ID);
            }
        }
        private bool CheckDMExit(string maSP, string maNPL)
        {

            foreach (DinhMuc dm in DMCollection)
            {
                if (dm.MaSanPHam.Trim() == maSP.Trim() && dm.MaNguyenPhuLieu.Trim() == maNPL.Trim())
                {
                    //ShowMessage("Định mức này đã được khai báo trên lưới.", false);

                    MLMessages("Định mức này đã được khai báo trên lưới.", "MSG_ADD01", "", false);
                  
                    return true;
                }
            }
            return false;
        }
        private void DinhMucEditForm_Load(object sender, EventArgs e)
        {            
            txtDinhMuc.DecimalDigits = GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH;
            if (this.OpenType == OpenFormType.Edit)
            {
                btnAdd.Visible = false;
                SanPham spSXXK = new SanPham();
                spSXXK.MaHaiQuan = this.MaHaiQuan.Trim();
                spSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                spSXXK.Ma = DMDetail.MaSanPHam.Trim();
                if (spSXXK.Load())
                {
                    this.existSP = true;
                    txtMaSP.Text = spSXXK.Ma.Trim();
                    txtTenSP.Text = spSXXK.Ten.Trim();
                    txtMaHSSP.Text = spSXXK.MaHS;
                    txtDonViTinhSP.Text = DonViTinh.GetName(spSXXK.DVT_ID);
                }
                else
                {
                    Company.BLL.KDT.SXXK.SanPham spKDT = Company.BLL.KDT.SXXK.SanPham.Load(DMDetail.MaSanPHam.Trim());
                    this.existSP = true;
                    txtMaSP.Text = spKDT.Ma.Trim();
                    txtTenSP.Text = spKDT.Ten.Trim();
                    txtMaHSSP.Text = spKDT.MaHS;
                    txtDonViTinhSP.Text = DonViTinh.GetName(spKDT.DVT_ID);
                }
                NguyenPhuLieu nplSXXK = new NguyenPhuLieu();
                nplSXXK.MaHaiQuan = this.MaHaiQuan.Trim();
                nplSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                nplSXXK.Ma = DMDetail.MaNguyenPhuLieu.Trim();
                if (nplSXXK.Load())
                {
                    this.existNPL = true;
                    txtMaNPL.Text = nplSXXK.Ma;
                    txtTenNPL.Text = nplSXXK.Ten;
                    txtMaHSNPL.Text = nplSXXK.MaHS;
                    txtDonViTinhNPL.Text = DonViTinh.GetName(nplSXXK.DVT_ID);
                }
                else
                {
                    Company.BLL.KDT.SXXK.NguyenPhuLieu nplKDT = Company.BLL.KDT.SXXK.NguyenPhuLieu.Load(DMDetail.MaSanPHam.Trim());
                    this.existNPL = true;
                    txtMaNPL.Text = nplKDT.Ma.Trim();
                    txtTenNPL.Text = nplKDT.Ten.Trim();
                    txtMaHSNPL.Text = nplKDT.MaHS;
                    txtDonViTinhNPL.Text = DonViTinh.GetName(nplKDT.DVT_ID);
                }
                txtDinhMuc.Value = DMDetail.DinhMucSuDung;
                txtTyLeHH.Value = DMDetail.TyLeHaoHut;

            }
            else
            {
                txtMaSP.Text = this.maSP.Trim();
                btnDelete.Visible = false;
            }
        }

        private void txtMaSP_Leave(object sender, EventArgs e)
        {
            SanPham spSXXK = new SanPham();
            spSXXK.MaHaiQuan = this.MaHaiQuan.Trim();
            spSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            spSXXK.Ma = txtMaSP.Text.Trim();
            if (spSXXK.Load())
            {
                this.existSP = true;                

                txtMaSP.Text = spSXXK.Ma.Trim();
                txtTenSP.Text = spSXXK.Ten.Trim();
                txtMaHSSP.Text = spSXXK.MaHS;
                txtDonViTinhSP.Text = DonViTinh.GetName(spSXXK.DVT_ID);                
            }
            else
            {
                this.existSP = false;
                error.SetIconPadding(txtMaSP, -8);
                //error.SetError(txtMaSP, "Không tồn tại sản phẩm này.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaSP, "Không tồn tại sản phẩm này.");
                }
                else
                {
                    error.SetError(txtMaSP, "This product hasn't exist.");
                }

                txtTenSP.Text = txtMaHSSP.Text = txtDonViTinhSP.Text = string.Empty;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            if (DMCollection.Count == 0)
            {
                //ShowMessage("Bạn chưa nhập định mức.", false);
                MLMessages("Bạn chưa nhập định mức.", "MSG_PUB11", "", false);
                return;
            }
            //kiem tra xem da dang ky dinh muc chua
            Company.BLL.SXXK.ThongTinDinhMuc dm = new Company.BLL.SXXK.ThongTinDinhMuc();
            dm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN ;
            dm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;            
            dm.MaSanPham = txtMaSP.Text.Trim();
            //if (dm.Load())
            //{
            //    ShowMessage("Sản phẩm này đã được đăng ký.", false);
            //    return;
            //}

            if (this.OpenType == OpenFormType.Insert)
            {
                // 2. Cập nhật dữ liệu.               
                try
                {

                    if (dinhmuc.InsertUpdateGrid(DMCollection))
                    {
                       // ShowMessage("Cập nhật thành công !", false);
                        MLMessages("Cập nhật thành công !", "MSG_SAV02", "", false);
                        this.Close();
                    }
                    else
                        //ShowMessage("Cập nhật không thành công", false);
                    MLMessages("Cập nhật không thành công !", "MSG_SAV01", "", false);
                }
                catch
                {
                    MLMessages("Cập nhật không thành công !", "MSG_SAV01", "", false);
                } 
            }
           
            // 3. Đóng.
           // this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;

            if (!this.existNPL)
            {
                error.SetIconPadding(txtMaNPL, -8);
               // error.SetError(txtMaNPL, "Không tồn tại nguyên phụ liệu này.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaNPL, "Không tồn tại nguyên phụ liệu này.");
                }
                else
                {
                    error.SetError(txtMaNPL, "This material haven't exist.");
                }
                return;
            }
            if (!this.existSP)
            {
                error.SetIconPadding(txtMaSP, -8);
               // error.SetError(txtMaSP, "Không tồn tại sản phẩm này.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaSP, "Không tồn tại sản phẩm này.");
                }
                else
                {
                    error.SetError(txtMaSP, "This product hasn't exist.");
                }
                return;
            }
            if (Convert.ToDecimal(txtDinhMuc.Text) <= 0)
            {
                error.SetIconPadding(txtDinhMuc, -8);
                //error.SetError(txtDinhMuc, "Định mức phải lớn hơn 0.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtDinhMuc, "Định mức phải lớn hơn 0.");
                }
                else
                {
                    error.SetError(txtDinhMuc, "The norm must be greater than zero.");
                }

                return;
            }
            if (CheckDMExit(txtMaSP.Text.Trim(), txtMaNPL.Text.Trim()))
            {
                return;
            }
            
            Company.BLL.SXXK.ThongTinDinhMuc dm = new Company.BLL.SXXK.ThongTinDinhMuc();            
            dm.MaDoanhNghiep = GlobalSettings.MA_DON_VI ;
            dm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN ;
            dm.MaSanPham = txtMaSP.Text.Trim();
            //if (dm.Load())
            //{
            //    ShowMessage("Sản phẩm này đã được đăng ký.", false);
            //    return;
            //}

            if (this.OpenType == OpenFormType.Insert)
            {
                // 2. Cập nhật dữ liệu.
                NguyenPhuLieu nplup = new NguyenPhuLieu();
                this.DMDetail = new DinhMuc();
                //this.DMCollection = new DinhMucCollection();
                this.DMDetail.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;

                this.DMDetail.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                this.DMDetail.MaSanPHam = txtMaSP.Text.Trim();
                this.DMDetail.MaNguyenPhuLieu = txtMaNPL.Text.Trim();
                this.DMDetail.DinhMucSuDung = Convert.ToDecimal(txtDinhMuc.Value);
                this.DMDetail.TyLeHaoHut = Convert.ToDecimal(txtTyLeHH.Value);
                this.DMDetail.IsFromVietNam = chIsVietNam.Checked;
                nplup.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                nplup.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                nplup.Ma = txtMaNPL.Text.Trim();
                nplup.Load();
                this.DMDetail.TenNPL = nplup.Ten.Trim();
                this.DMCollection.Add(DMDetail);
                dgList.DataSource = this.DMCollection;
                dgList.Refetch();
            }
            txtMaNPL.Text = "";
            txtMaHSNPL.Text = "";
            txtTenNPL.Text = "";
            txtDinhMuc.Value = 0;
            txtTyLeHH.Value = 0;
            txtMaNPL.Focus();
        }

    }
}