﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class SMA : BasicVNACC
    {
        public List<SMA_HANGHOA> ListHangHoa = new List<SMA_HANGHOA>();

        public StringBuilder BuilEdiMessagesSMA(StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<SMA>(StrBuild, true, GlobalVNACC.PathConfig, "SMA");
            foreach (SMA_HANGHOA item in ListHangHoa)
            {
                item.BuildEdiMessages<SMA_HANGHOA>(StrBuild, true, GlobalVNACC.PathConfig, "SMA_HANGHOA");
            }
            return str;
        }

    }
}
