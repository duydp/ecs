﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;

#endif
using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;


//using Company.BLL.KDT;

namespace Company.Interface.Report.SXXK
{
    public partial class BangkeSoContainer_TT196 : DevExpress.XtraReports.UI.XtraReport
    {
        public string phieu;
        public string soTN ;
        public string ngayTN ;
        public string maHaiQuan;
        int STT = 0;
        public    ToKhaiMauDich tkmd = new ToKhaiMauDich();

        public BangkeSoContainer_TT196()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            DataTable dt =  tkmd.GetContainerInfo(tkmd.ID);
            this.DataSource = dt;
            lblnguoikhaihq.Text = tkmd.TenDoanhNghiep;
            lblMaDoanhNghiep.Text = tkmd.MaDoanhNghiep;             
            if (tkmd.NgayDangKy.Year > 1900)
                lblNgayKy.Text = tkmd.NgayDangKy.ToString("dd/MM/yyy hh:mm:ss");
            else
                lblNgayKy.Text = "";
            if (tkmd.SoToKhai > 0)
                lblSoToKhai.Text = tkmd.SoToKhai + "";
            else
                lblSoToKhai.Text = "";
            lblHieuKien.DataBindings.Add("Text",this.DataSource, dt.TableName + ".SoHieu");
            lblSoseal.DataBindings.Add("Text",this.DataSource, dt.TableName + ".Seal_No");
           // lblHieuKien.DataBindings.Add("", dt, ".SoHieu", "");
          //  lblHieuKien.DataBindings.Add("", dt, ".SoHieu", "");
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
           this.STT++;
            lblSTT.Text =this.STT+"";
        }        
    }
}
