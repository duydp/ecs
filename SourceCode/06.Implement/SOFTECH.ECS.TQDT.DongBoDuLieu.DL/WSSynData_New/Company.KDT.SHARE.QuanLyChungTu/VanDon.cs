using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;


namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class VanDon
    {
        public List<Container> ContainerCollection = new List<Container>();
        public List<HangVanDonDetail> ListHangOfVanDon = new List<HangVanDonDetail>();
        public void LoadContainerCollection()
        {
            ContainerCollection = (List<Container>)Container.SelectCollectionBy_VanDon_ID(this.ID);
        }
        public void LoadListHangOfVanDon()
        {
            ListHangOfVanDon = (List<HangVanDonDetail>)HangVanDonDetail.SelectCollectionDynamic("VanDon_ID=" + this.ID, "");
        }
        public DataTable ConvertListToDataSet(DataTable dsHMD)
        {
            DataTable tmp = dsHMD.Copy();
            tmp.Rows.Clear();
            tmp.Columns.Add("GhiChu");
            dsHMD.Columns.Add("GhiChu");
            dsHMD.Columns.Add("HMD_ID");
            dsHMD.Columns.Add("MaChuyenNganh");
            dsHMD.Columns.Add("MaNguyenTe");
            dsHMD.Columns.Add("LoaiKien");
            dsHMD.Columns.Add("SoHieuContainer");
            dsHMD.Columns.Add("SoHieuKien");
            dsHMD.Columns.Add("SoKien");
            dsHMD.Columns.Add("TrongLuongTinh");
            dsHMD.Columns.Add("KichThuocHoacTheTich");


            tmp.Columns["ID"].ColumnName = "HMD_ID";
            tmp.Columns["HMD_ID"].Caption = "HMD_ID";
            tmp.Columns.Add("ID");
            tmp.Columns.Add("MaChuyenNganh");
            tmp.Columns.Add("MaNguyenTe");
            tmp.Columns.Add("LoaiKien");
            tmp.Columns.Add("SoHieuContainer");
            tmp.Columns.Add("SoHieuKien");
            tmp.Columns.Add("SoKien");
            tmp.Columns.Add("TrongLuongTinh");
            tmp.Columns.Add("KichThuocHoacTheTich");

            foreach (HangVanDonDetail item in ListHangOfVanDon)
            {
                DataRow[] row = dsHMD.Select("ID=" + item.HMD_ID);
                if (row != null && row.Length != 0)
                {
                    row[0]["HMD_ID"] = row[0]["ID"];
                    row[0]["MaChuyenNganh"] = item.MaChuyenNganh;
                    row[0]["MaNguyenTe"] = item.MaNguyenTe;
                    row[0]["ID"] = item.ID.ToString();

                    row[0]["SoThuTuHang"] = item.SoThuTuHang;
                    row[0]["MaHS"] = item.MaHS;
                    row[0]["MaPhu"] = item.MaPhu;
                    row[0]["TenHang"] = item.TenHang;
                    row[0]["NuocXX_ID"] = item.NuocXX_ID;
                    row[0]["DVT_ID"] = item.DVT_ID;
                    row[0]["SoLuong"] = item.SoLuong;
                    row[0]["DonGiaKB"] = item.DonGiaKB;
                    row[0]["TriGiaKB"] = item.TriGiaKB;

                    #region V3
                    row[0]["TrongLuong"] = item.TrongLuong;
                    row[0]["LoaiKien"] = item.LoaiKien;
                    row[0]["SoHieuKien"] = item.SoHieuKien;
                    row[0]["SoHieuContainer"] = item.SoHieuContainer;
                    row[0]["SoKien"] = item.SoKien;
                    row[0]["TrongLuongTinh"] = item.TrongLuongTinh;
                    row[0]["KichThuocHoacTheTich"] = item.KichThuocHoacTheTich;
                    #endregion V3
                    
                    tmp.ImportRow(row[0]);
                }
            }
            return tmp;

        }
        //---------------------------------------------------------------------------------------------

        public static VanDon Load(string soVanDon, DateTime ngayVanDon, long idTKMD, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_VanDon_LoadBy]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, soVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, ngayVanDon);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, idTKMD);

            VanDon entity = null;
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new VanDon();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("HangRoi"))) entity.HangRoi = reader.GetBoolean(reader.GetOrdinal("HangRoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangVT"))) entity.MaHangVT = reader.GetString(reader.GetOrdinal("MaHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangVT"))) entity.TenHangVT = reader.GetString(reader.GetOrdinal("TenHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenPTVT"))) entity.TenPTVT = reader.GetString(reader.GetOrdinal("TenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT"))) entity.QuocTichPTVT = reader.GetString(reader.GetOrdinal("QuocTichPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXuat_ID"))) entity.NuocXuat_ID = reader.GetString(reader.GetOrdinal("NuocXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHang"))) entity.MaNguoiNhanHang = reader.GetString(reader.GetOrdinal("MaNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHang"))) entity.TenNguoiNhanHang = reader.GetString(reader.GetOrdinal("TenNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiGiaoHang"))) entity.MaNguoiGiaoHang = reader.GetString(reader.GetOrdinal("MaNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiGiaoHang"))) entity.TenNguoiGiaoHang = reader.GetString(reader.GetOrdinal("TenNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauNhap_ID"))) entity.CuaKhauNhap_ID = reader.GetString(reader.GetOrdinal("CuaKhauNhap_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauXuat"))) entity.CuaKhauXuat = reader.GetString(reader.GetOrdinal("CuaKhauXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHangTrungGian"))) entity.MaNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("MaNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHangTrungGian"))) entity.TenNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("TenNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangXepHang"))) entity.MaCangXepHang = reader.GetString(reader.GetOrdinal("MaCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangXepHang"))) entity.TenCangXepHang = reader.GetString(reader.GetOrdinal("TenCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangDoHang"))) entity.MaCangDoHang = reader.GetString(reader.GetOrdinal("MaCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangDoHang"))) entity.TenCangDoHang = reader.GetString(reader.GetOrdinal("TenCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemGiaoHang"))) entity.DiaDiemGiaoHang = reader.GetString(reader.GetOrdinal("DiaDiemGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDi"))) entity.NoiDi = reader.GetString(reader.GetOrdinal("NoiDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuChuyenDi"))) entity.SoHieuChuyenDi = reader.GetString(reader.GetOrdinal("SoHieuChuyenDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKhoiHanh"))) entity.NgayKhoiHanh = reader.GetDateTime(reader.GetOrdinal("NgayKhoiHanh"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongSoKien"))) entity.TongSoKien = reader.GetInt32(reader.GetOrdinal("TongSoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKien"))) entity.LoaiKien = reader.GetString(reader.GetOrdinal("LoaiKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuKien"))) entity.SoHieuKien = reader.GetString(reader.GetOrdinal("SoHieuKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemChuyenTai"))) entity.DiaDiemChuyenTai = reader.GetString(reader.GetOrdinal("DiaDiemChuyenTai"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("MoTaKhac"))) entity.MoTaKhac = reader.GetString(reader.GetOrdinal("MoTaKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_NuocPhatHanh"))) entity.ID_NuocPhatHanh = reader.GetString(reader.GetOrdinal("ID_NuocPhatHanh"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_VanDon_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year <= 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
            db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
            db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
            db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
            db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
            db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
            db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.NVarChar, TenCangXepHang);
            db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
            db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.NVarChar, TenCangDoHang);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
            db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year <= 1753 ? DBNull.Value : (object)NgayKhoiHanh);
            db.AddInParameter(dbCommand, "@TongSoKien", SqlDbType.Int, TongSoKien);
            db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.NVarChar, LoaiKien);
            db.AddInParameter(dbCommand, "@SoHieuKien", SqlDbType.NVarChar, SoHieuKien);
            db.AddInParameter(dbCommand, "@DiaDiemChuyenTai", SqlDbType.NVarChar, DiaDiemChuyenTai);
            db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, LoaiVanDon);
            db.AddInParameter(dbCommand, "@MoTaKhac", SqlDbType.NVarChar, MoTaKhac);
            db.AddInParameter(dbCommand, "@ID_NuocPhatHanh", SqlDbType.VarChar, ID_NuocPhatHanh);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_VanDon_Update]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year <= 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
            db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
            db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
            db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
            db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
            db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
            db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.NVarChar, TenCangXepHang);
            db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
            db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.NVarChar, TenCangDoHang);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
            db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year <= 1753 ? DBNull.Value : (object)NgayKhoiHanh);
            db.AddInParameter(dbCommand, "@TongSoKien", SqlDbType.Int, TongSoKien);
            db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.NVarChar, LoaiKien);
            db.AddInParameter(dbCommand, "@SoHieuKien", SqlDbType.NVarChar, SoHieuKien);
            db.AddInParameter(dbCommand, "@DiaDiemChuyenTai", SqlDbType.NVarChar, DiaDiemChuyenTai);
            db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, LoaiVanDon);
            db.AddInParameter(dbCommand, "@MoTaKhac", SqlDbType.NVarChar, MoTaKhac);
            db.AddInParameter(dbCommand, "@ID_NuocPhatHanh", SqlDbType.VarChar, ID_NuocPhatHanh);
            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateBy(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "p_KDT_VanDon_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year <= 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
            db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
            db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
            db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
            db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
            db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
            db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.NVarChar, TenCangXepHang);
            db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
            db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.NVarChar, TenCangDoHang);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
            db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year <= 1753 ? DBNull.Value : (object)NgayKhoiHanh);
            db.AddInParameter(dbCommand, "@TongSoKien", SqlDbType.Int, TongSoKien);
            db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.NVarChar, LoaiKien);
            db.AddInParameter(dbCommand, "@SoHieuKien", SqlDbType.NVarChar, SoHieuKien);
            db.AddInParameter(dbCommand, "@DiaDiemChuyenTai", SqlDbType.NVarChar, DiaDiemChuyenTai);
            db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, LoaiVanDon);
            db.AddInParameter(dbCommand, "@MoTaKhac", SqlDbType.NVarChar, MoTaKhac);
            db.AddInParameter(dbCommand, "@ID_NuocPhatHanh", SqlDbType.VarChar, ID_NuocPhatHanh);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public void InsertUpdateFullTrasaction(SqlTransaction transaction)
        {
            if (this.ID == 0)
                this.Insert(transaction);
            else
                this.Update(transaction);
            foreach (Container item in ContainerCollection)
            {
                if (item.ID == 0)
                    item.Insert(transaction);
                else
                    item.Update(transaction);
            }
        }

        public void InsertUpdateFullTrasaction(SqlTransaction transaction, SqlDatabase db)
        {
            //Kiem tra ton tai To khai tren Database Target
            VanDon vdTemp = VanDon.Load(this.SoVanDon, this.NgayVanDon, this.TKMD_ID, db);

            if (vdTemp == null || vdTemp.ID == 0)
                this.ID = Insert(transaction, db);
            else
            {
                this.ID = vdTemp.ID;
                Update(transaction, db);
            }

            foreach (Container item in ContainerCollection)
            {
                item.VanDon_ID = this.ID;

                item.InsertUpdateBy(transaction, db);
            }
        }





        public XmlNode ConvertVanDonToXML(XmlDocument doc)
        {
            throw new NotImplementedException();
        }
    }
}