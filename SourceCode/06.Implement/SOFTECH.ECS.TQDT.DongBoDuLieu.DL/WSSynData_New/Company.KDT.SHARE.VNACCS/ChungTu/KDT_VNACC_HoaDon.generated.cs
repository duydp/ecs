using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_HoaDon : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public decimal SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public string PhanLoaiXuatNhap { set; get; }
		public string MaDaiLyHQ { set; get; }
		public string SoHoaDon { set; get; }
		public DateTime NgayLapHoaDon { set; get; }
		public string DiaDiemLapHoaDon { set; get; }
		public string PhuongThucThanhToan { set; get; }
		public string PhanLoaiLienQuan { set; get; }
		public string MaNguoiXNK { set; get; }
		public string TenNguoiXNK { set; get; }
		public string MaBuuChinh_NguoiXNK { set; get; }
		public string DiaChi_NguoiXNK { set; get; }
		public string SoDienThoai_NguoiXNK { set; get; }
		public string NguoiLapHoaDon { set; get; }
		public string MaNguoiGuiNhan { set; get; }
		public string TenNguoiGuiNhan { set; get; }
		public string MaBuuChinhNguoiGuiNhan { set; get; }
		public string DiaChiNguoiNhanGui1 { set; get; }
		public string DiaChiNguoiNhanGui2 { set; get; }
		public string DiaChiNguoiNhanGui3 { set; get; }
		public string DiaChiNguoiNhanGui4 { set; get; }
		public string SoDienThoaiNhanGui { set; get; }
		public string NuocSoTaiNhanGui { set; get; }
		public string MaKyHieu { set; get; }
		public string PhanLoaiVanChuyen { set; get; }
		public string TenPTVC { set; get; }
		public string SoHieuChuyenDi { set; get; }
		public string MaDiaDiemXepHang { set; get; }
		public string TenDiaDiemXepHang { set; get; }
		public DateTime ThoiKyXephang { set; get; }
		public string MaDiaDiemDoHang { set; get; }
		public string TenDiaDiemDoHang { set; get; }
		public string MaDiaDiemTrungChuyen { set; get; }
		public string TenDiaDiemTrungChuyen { set; get; }
		public decimal TrongLuongGross { set; get; }
		public string MaDVT_TrongLuongGross { set; get; }
		public decimal TrongLuongThuan { set; get; }
		public string MaDVT_TrongLuongThuan { set; get; }
		public decimal TongTheTich { set; get; }
		public string MaDVT_TheTich { set; get; }
		public decimal TongSoKienHang { set; get; }
		public string MaDVT_KienHang { set; get; }
		public string GhiChuChuHang { set; get; }
		public string SoPL { set; get; }
		public string NganHangLC { set; get; }
		public decimal TriGiaFOB { set; get; }
		public string MaTT_FOB { set; get; }
		public decimal SoTienFOB { set; get; }
		public string MaTT_TienFOB { set; get; }
		public decimal PhiVanChuyen { set; get; }
		public string MaTT_PhiVC { set; get; }
		public string NoiThanhToanPhiVC { set; get; }
		public decimal ChiPhiXepHang1 { set; get; }
		public string MaTT_ChiPhiXepHang1 { set; get; }
		public string LoaiChiPhiXepHang1 { set; get; }
		public decimal ChiPhiXepHang2 { set; get; }
		public string MaTT_ChiPhiXepHang2 { set; get; }
		public string LoaiChiPhiXepHang2 { set; get; }
		public decimal PhiVC_DuongBo { set; get; }
		public string MaTT_PhiVC_DuongBo { set; get; }
		public decimal PhiBaoHiem { set; get; }
		public string MaTT_PhiBaoHiem { set; get; }
		public decimal SoTienPhiBaoHiem { set; get; }
		public string MaTT_TienPhiBaoHiem { set; get; }
		public decimal SoTienKhauTru { set; get; }
		public string MaTT_TienKhauTru { set; get; }
		public string LoaiKhauTru { set; get; }
		public decimal SoTienKhac { set; get; }
		public string MaTT_TienKhac { set; get; }
		public string LoaiSoTienKhac { set; get; }
		public decimal TongTriGiaHoaDon { set; get; }
		public string MaTT_TongTriGia { set; get; }
		public string DieuKienGiaHoaDon { set; get; }
		public string DiaDiemGiaoHang { set; get; }
		public string GhiChuDacBiet { set; get; }
		public decimal TongSoDongHang { set; get; }
		public string TrangThaiXuLy { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_HoaDon> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_HoaDon> collection = new List<KDT_VNACC_HoaDon>();
			while (reader.Read())
			{
				KDT_VNACC_HoaDon entity = new KDT_VNACC_HoaDon();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetDecimal(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiXuatNhap"))) entity.PhanLoaiXuatNhap = reader.GetString(reader.GetOrdinal("PhanLoaiXuatNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyHQ"))) entity.MaDaiLyHQ = reader.GetString(reader.GetOrdinal("MaDaiLyHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDon"))) entity.SoHoaDon = reader.GetString(reader.GetOrdinal("SoHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayLapHoaDon"))) entity.NgayLapHoaDon = reader.GetDateTime(reader.GetOrdinal("NgayLapHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemLapHoaDon"))) entity.DiaDiemLapHoaDon = reader.GetString(reader.GetOrdinal("DiaDiemLapHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuongThucThanhToan"))) entity.PhuongThucThanhToan = reader.GetString(reader.GetOrdinal("PhuongThucThanhToan"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiLienQuan"))) entity.PhanLoaiLienQuan = reader.GetString(reader.GetOrdinal("PhanLoaiLienQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiXNK"))) entity.MaNguoiXNK = reader.GetString(reader.GetOrdinal("MaNguoiXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiXNK"))) entity.TenNguoiXNK = reader.GetString(reader.GetOrdinal("TenNguoiXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinh_NguoiXNK"))) entity.MaBuuChinh_NguoiXNK = reader.GetString(reader.GetOrdinal("MaBuuChinh_NguoiXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChi_NguoiXNK"))) entity.DiaChi_NguoiXNK = reader.GetString(reader.GetOrdinal("DiaChi_NguoiXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoai_NguoiXNK"))) entity.SoDienThoai_NguoiXNK = reader.GetString(reader.GetOrdinal("SoDienThoai_NguoiXNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiLapHoaDon"))) entity.NguoiLapHoaDon = reader.GetString(reader.GetOrdinal("NguoiLapHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiGuiNhan"))) entity.MaNguoiGuiNhan = reader.GetString(reader.GetOrdinal("MaNguoiGuiNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiGuiNhan"))) entity.TenNguoiGuiNhan = reader.GetString(reader.GetOrdinal("TenNguoiGuiNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhNguoiGuiNhan"))) entity.MaBuuChinhNguoiGuiNhan = reader.GetString(reader.GetOrdinal("MaBuuChinhNguoiGuiNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiNhanGui1"))) entity.DiaChiNguoiNhanGui1 = reader.GetString(reader.GetOrdinal("DiaChiNguoiNhanGui1"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiNhanGui2"))) entity.DiaChiNguoiNhanGui2 = reader.GetString(reader.GetOrdinal("DiaChiNguoiNhanGui2"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiNhanGui3"))) entity.DiaChiNguoiNhanGui3 = reader.GetString(reader.GetOrdinal("DiaChiNguoiNhanGui3"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiNhanGui4"))) entity.DiaChiNguoiNhanGui4 = reader.GetString(reader.GetOrdinal("DiaChiNguoiNhanGui4"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiNhanGui"))) entity.SoDienThoaiNhanGui = reader.GetString(reader.GetOrdinal("SoDienThoaiNhanGui"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocSoTaiNhanGui"))) entity.NuocSoTaiNhanGui = reader.GetString(reader.GetOrdinal("NuocSoTaiNhanGui"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKyHieu"))) entity.MaKyHieu = reader.GetString(reader.GetOrdinal("MaKyHieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiVanChuyen"))) entity.PhanLoaiVanChuyen = reader.GetString(reader.GetOrdinal("PhanLoaiVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenPTVC"))) entity.TenPTVC = reader.GetString(reader.GetOrdinal("TenPTVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuChuyenDi"))) entity.SoHieuChuyenDi = reader.GetString(reader.GetOrdinal("SoHieuChuyenDi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiemXepHang"))) entity.MaDiaDiemXepHang = reader.GetString(reader.GetOrdinal("MaDiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDiaDiemXepHang"))) entity.TenDiaDiemXepHang = reader.GetString(reader.GetOrdinal("TenDiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiKyXephang"))) entity.ThoiKyXephang = reader.GetDateTime(reader.GetOrdinal("ThoiKyXephang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiemDoHang"))) entity.MaDiaDiemDoHang = reader.GetString(reader.GetOrdinal("MaDiaDiemDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDiaDiemDoHang"))) entity.TenDiaDiemDoHang = reader.GetString(reader.GetOrdinal("TenDiaDiemDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiemTrungChuyen"))) entity.MaDiaDiemTrungChuyen = reader.GetString(reader.GetOrdinal("MaDiaDiemTrungChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDiaDiemTrungChuyen"))) entity.TenDiaDiemTrungChuyen = reader.GetString(reader.GetOrdinal("TenDiaDiemTrungChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongGross"))) entity.TrongLuongGross = reader.GetDecimal(reader.GetOrdinal("TrongLuongGross"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVT_TrongLuongGross"))) entity.MaDVT_TrongLuongGross = reader.GetString(reader.GetOrdinal("MaDVT_TrongLuongGross"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongThuan"))) entity.TrongLuongThuan = reader.GetDecimal(reader.GetOrdinal("TrongLuongThuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVT_TrongLuongThuan"))) entity.MaDVT_TrongLuongThuan = reader.GetString(reader.GetOrdinal("MaDVT_TrongLuongThuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTheTich"))) entity.TongTheTich = reader.GetDecimal(reader.GetOrdinal("TongTheTich"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVT_TheTich"))) entity.MaDVT_TheTich = reader.GetString(reader.GetOrdinal("MaDVT_TheTich"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoKienHang"))) entity.TongSoKienHang = reader.GetDecimal(reader.GetOrdinal("TongSoKienHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDVT_KienHang"))) entity.MaDVT_KienHang = reader.GetString(reader.GetOrdinal("MaDVT_KienHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuChuHang"))) entity.GhiChuChuHang = reader.GetString(reader.GetOrdinal("GhiChuChuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoPL"))) entity.SoPL = reader.GetString(reader.GetOrdinal("SoPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("NganHangLC"))) entity.NganHangLC = reader.GetString(reader.GetOrdinal("NganHangLC"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaFOB"))) entity.TriGiaFOB = reader.GetDecimal(reader.GetOrdinal("TriGiaFOB"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTT_FOB"))) entity.MaTT_FOB = reader.GetString(reader.GetOrdinal("MaTT_FOB"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienFOB"))) entity.SoTienFOB = reader.GetDecimal(reader.GetOrdinal("SoTienFOB"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTT_TienFOB"))) entity.MaTT_TienFOB = reader.GetString(reader.GetOrdinal("MaTT_TienFOB"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTT_PhiVC"))) entity.MaTT_PhiVC = reader.GetString(reader.GetOrdinal("MaTT_PhiVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiThanhToanPhiVC"))) entity.NoiThanhToanPhiVC = reader.GetString(reader.GetOrdinal("NoiThanhToanPhiVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiXepHang1"))) entity.ChiPhiXepHang1 = reader.GetDecimal(reader.GetOrdinal("ChiPhiXepHang1"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTT_ChiPhiXepHang1"))) entity.MaTT_ChiPhiXepHang1 = reader.GetString(reader.GetOrdinal("MaTT_ChiPhiXepHang1"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiChiPhiXepHang1"))) entity.LoaiChiPhiXepHang1 = reader.GetString(reader.GetOrdinal("LoaiChiPhiXepHang1"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChiPhiXepHang2"))) entity.ChiPhiXepHang2 = reader.GetDecimal(reader.GetOrdinal("ChiPhiXepHang2"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTT_ChiPhiXepHang2"))) entity.MaTT_ChiPhiXepHang2 = reader.GetString(reader.GetOrdinal("MaTT_ChiPhiXepHang2"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiChiPhiXepHang2"))) entity.LoaiChiPhiXepHang2 = reader.GetString(reader.GetOrdinal("LoaiChiPhiXepHang2"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiVC_DuongBo"))) entity.PhiVC_DuongBo = reader.GetDecimal(reader.GetOrdinal("PhiVC_DuongBo"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTT_PhiVC_DuongBo"))) entity.MaTT_PhiVC_DuongBo = reader.GetString(reader.GetOrdinal("MaTT_PhiVC_DuongBo"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTT_PhiBaoHiem"))) entity.MaTT_PhiBaoHiem = reader.GetString(reader.GetOrdinal("MaTT_PhiBaoHiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienPhiBaoHiem"))) entity.SoTienPhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("SoTienPhiBaoHiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTT_TienPhiBaoHiem"))) entity.MaTT_TienPhiBaoHiem = reader.GetString(reader.GetOrdinal("MaTT_TienPhiBaoHiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhauTru"))) entity.SoTienKhauTru = reader.GetDecimal(reader.GetOrdinal("SoTienKhauTru"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTT_TienKhauTru"))) entity.MaTT_TienKhauTru = reader.GetString(reader.GetOrdinal("MaTT_TienKhauTru"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKhauTru"))) entity.LoaiKhauTru = reader.GetString(reader.GetOrdinal("LoaiKhauTru"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhac"))) entity.SoTienKhac = reader.GetDecimal(reader.GetOrdinal("SoTienKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTT_TienKhac"))) entity.MaTT_TienKhac = reader.GetString(reader.GetOrdinal("MaTT_TienKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiSoTienKhac"))) entity.LoaiSoTienKhac = reader.GetString(reader.GetOrdinal("LoaiSoTienKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaHoaDon"))) entity.TongTriGiaHoaDon = reader.GetDecimal(reader.GetOrdinal("TongTriGiaHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTT_TongTriGia"))) entity.MaTT_TongTriGia = reader.GetString(reader.GetOrdinal("MaTT_TongTriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("DieuKienGiaHoaDon"))) entity.DieuKienGiaHoaDon = reader.GetString(reader.GetOrdinal("DieuKienGiaHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemGiaoHang"))) entity.DiaDiemGiaoHang = reader.GetString(reader.GetOrdinal("DiaDiemGiaoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuDacBiet"))) entity.GhiChuDacBiet = reader.GetString(reader.GetOrdinal("GhiChuDacBiet"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoDongHang"))) entity.TongSoDongHang = reader.GetDecimal(reader.GetOrdinal("TongSoDongHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetString(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_HoaDon> collection, long id)
        {
            foreach (KDT_VNACC_HoaDon item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_HoaDon VALUES(@SoTiepNhan, @NgayTiepNhan, @PhanLoaiXuatNhap, @MaDaiLyHQ, @SoHoaDon, @NgayLapHoaDon, @DiaDiemLapHoaDon, @PhuongThucThanhToan, @PhanLoaiLienQuan, @MaNguoiXNK, @TenNguoiXNK, @MaBuuChinh_NguoiXNK, @DiaChi_NguoiXNK, @SoDienThoai_NguoiXNK, @NguoiLapHoaDon, @MaNguoiGuiNhan, @TenNguoiGuiNhan, @MaBuuChinhNguoiGuiNhan, @DiaChiNguoiNhanGui1, @DiaChiNguoiNhanGui2, @DiaChiNguoiNhanGui3, @DiaChiNguoiNhanGui4, @SoDienThoaiNhanGui, @NuocSoTaiNhanGui, @MaKyHieu, @PhanLoaiVanChuyen, @TenPTVC, @SoHieuChuyenDi, @MaDiaDiemXepHang, @TenDiaDiemXepHang, @ThoiKyXephang, @MaDiaDiemDoHang, @TenDiaDiemDoHang, @MaDiaDiemTrungChuyen, @TenDiaDiemTrungChuyen, @TrongLuongGross, @MaDVT_TrongLuongGross, @TrongLuongThuan, @MaDVT_TrongLuongThuan, @TongTheTich, @MaDVT_TheTich, @TongSoKienHang, @MaDVT_KienHang, @GhiChuChuHang, @SoPL, @NganHangLC, @TriGiaFOB, @MaTT_FOB, @SoTienFOB, @MaTT_TienFOB, @PhiVanChuyen, @MaTT_PhiVC, @NoiThanhToanPhiVC, @ChiPhiXepHang1, @MaTT_ChiPhiXepHang1, @LoaiChiPhiXepHang1, @ChiPhiXepHang2, @MaTT_ChiPhiXepHang2, @LoaiChiPhiXepHang2, @PhiVC_DuongBo, @MaTT_PhiVC_DuongBo, @PhiBaoHiem, @MaTT_PhiBaoHiem, @SoTienPhiBaoHiem, @MaTT_TienPhiBaoHiem, @SoTienKhauTru, @MaTT_TienKhauTru, @LoaiKhauTru, @SoTienKhac, @MaTT_TienKhac, @LoaiSoTienKhac, @TongTriGiaHoaDon, @MaTT_TongTriGia, @DieuKienGiaHoaDon, @DiaDiemGiaoHang, @GhiChuDacBiet, @TongSoDongHang, @TrangThaiXuLy, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_HoaDon SET SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, PhanLoaiXuatNhap = @PhanLoaiXuatNhap, MaDaiLyHQ = @MaDaiLyHQ, SoHoaDon = @SoHoaDon, NgayLapHoaDon = @NgayLapHoaDon, DiaDiemLapHoaDon = @DiaDiemLapHoaDon, PhuongThucThanhToan = @PhuongThucThanhToan, PhanLoaiLienQuan = @PhanLoaiLienQuan, MaNguoiXNK = @MaNguoiXNK, TenNguoiXNK = @TenNguoiXNK, MaBuuChinh_NguoiXNK = @MaBuuChinh_NguoiXNK, DiaChi_NguoiXNK = @DiaChi_NguoiXNK, SoDienThoai_NguoiXNK = @SoDienThoai_NguoiXNK, NguoiLapHoaDon = @NguoiLapHoaDon, MaNguoiGuiNhan = @MaNguoiGuiNhan, TenNguoiGuiNhan = @TenNguoiGuiNhan, MaBuuChinhNguoiGuiNhan = @MaBuuChinhNguoiGuiNhan, DiaChiNguoiNhanGui1 = @DiaChiNguoiNhanGui1, DiaChiNguoiNhanGui2 = @DiaChiNguoiNhanGui2, DiaChiNguoiNhanGui3 = @DiaChiNguoiNhanGui3, DiaChiNguoiNhanGui4 = @DiaChiNguoiNhanGui4, SoDienThoaiNhanGui = @SoDienThoaiNhanGui, NuocSoTaiNhanGui = @NuocSoTaiNhanGui, MaKyHieu = @MaKyHieu, PhanLoaiVanChuyen = @PhanLoaiVanChuyen, TenPTVC = @TenPTVC, SoHieuChuyenDi = @SoHieuChuyenDi, MaDiaDiemXepHang = @MaDiaDiemXepHang, TenDiaDiemXepHang = @TenDiaDiemXepHang, ThoiKyXephang = @ThoiKyXephang, MaDiaDiemDoHang = @MaDiaDiemDoHang, TenDiaDiemDoHang = @TenDiaDiemDoHang, MaDiaDiemTrungChuyen = @MaDiaDiemTrungChuyen, TenDiaDiemTrungChuyen = @TenDiaDiemTrungChuyen, TrongLuongGross = @TrongLuongGross, MaDVT_TrongLuongGross = @MaDVT_TrongLuongGross, TrongLuongThuan = @TrongLuongThuan, MaDVT_TrongLuongThuan = @MaDVT_TrongLuongThuan, TongTheTich = @TongTheTich, MaDVT_TheTich = @MaDVT_TheTich, TongSoKienHang = @TongSoKienHang, MaDVT_KienHang = @MaDVT_KienHang, GhiChuChuHang = @GhiChuChuHang, SoPL = @SoPL, NganHangLC = @NganHangLC, TriGiaFOB = @TriGiaFOB, MaTT_FOB = @MaTT_FOB, SoTienFOB = @SoTienFOB, MaTT_TienFOB = @MaTT_TienFOB, PhiVanChuyen = @PhiVanChuyen, MaTT_PhiVC = @MaTT_PhiVC, NoiThanhToanPhiVC = @NoiThanhToanPhiVC, ChiPhiXepHang1 = @ChiPhiXepHang1, MaTT_ChiPhiXepHang1 = @MaTT_ChiPhiXepHang1, LoaiChiPhiXepHang1 = @LoaiChiPhiXepHang1, ChiPhiXepHang2 = @ChiPhiXepHang2, MaTT_ChiPhiXepHang2 = @MaTT_ChiPhiXepHang2, LoaiChiPhiXepHang2 = @LoaiChiPhiXepHang2, PhiVC_DuongBo = @PhiVC_DuongBo, MaTT_PhiVC_DuongBo = @MaTT_PhiVC_DuongBo, PhiBaoHiem = @PhiBaoHiem, MaTT_PhiBaoHiem = @MaTT_PhiBaoHiem, SoTienPhiBaoHiem = @SoTienPhiBaoHiem, MaTT_TienPhiBaoHiem = @MaTT_TienPhiBaoHiem, SoTienKhauTru = @SoTienKhauTru, MaTT_TienKhauTru = @MaTT_TienKhauTru, LoaiKhauTru = @LoaiKhauTru, SoTienKhac = @SoTienKhac, MaTT_TienKhac = @MaTT_TienKhac, LoaiSoTienKhac = @LoaiSoTienKhac, TongTriGiaHoaDon = @TongTriGiaHoaDon, MaTT_TongTriGia = @MaTT_TongTriGia, DieuKienGiaHoaDon = @DieuKienGiaHoaDon, DiaDiemGiaoHang = @DiaDiemGiaoHang, GhiChuDacBiet = @GhiChuDacBiet, TongSoDongHang = @TongSoDongHang, TrangThaiXuLy = @TrangThaiXuLy, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HoaDon WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiXuatNhap", SqlDbType.VarChar, "PhanLoaiXuatNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDaiLyHQ", SqlDbType.VarChar, "MaDaiLyHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHoaDon", SqlDbType.VarChar, "SoHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayLapHoaDon", SqlDbType.DateTime, "NgayLapHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemLapHoaDon", SqlDbType.NVarChar, "DiaDiemLapHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongThucThanhToan", SqlDbType.NVarChar, "PhuongThucThanhToan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiLienQuan", SqlDbType.VarChar, "PhanLoaiLienQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiXNK", SqlDbType.VarChar, "MaNguoiXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiXNK", SqlDbType.NVarChar, "TenNguoiXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinh_NguoiXNK", SqlDbType.VarChar, "MaBuuChinh_NguoiXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChi_NguoiXNK", SqlDbType.NVarChar, "DiaChi_NguoiXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoai_NguoiXNK", SqlDbType.VarChar, "SoDienThoai_NguoiXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiLapHoaDon", SqlDbType.NVarChar, "NguoiLapHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiGuiNhan", SqlDbType.VarChar, "MaNguoiGuiNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiGuiNhan", SqlDbType.NVarChar, "TenNguoiGuiNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhNguoiGuiNhan", SqlDbType.VarChar, "MaBuuChinhNguoiGuiNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiNhanGui1", SqlDbType.NVarChar, "DiaChiNguoiNhanGui1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiNhanGui2", SqlDbType.NVarChar, "DiaChiNguoiNhanGui2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiNhanGui3", SqlDbType.NVarChar, "DiaChiNguoiNhanGui3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiNhanGui4", SqlDbType.NVarChar, "DiaChiNguoiNhanGui4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNhanGui", SqlDbType.VarChar, "SoDienThoaiNhanGui", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocSoTaiNhanGui", SqlDbType.VarChar, "NuocSoTaiNhanGui", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKyHieu", SqlDbType.NVarChar, "MaKyHieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiVanChuyen", SqlDbType.VarChar, "PhanLoaiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenPTVC", SqlDbType.NVarChar, "TenPTVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuChuyenDi", SqlDbType.VarChar, "SoHieuChuyenDi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, "TenDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiKyXephang", SqlDbType.DateTime, "ThoiKyXephang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemDoHang", SqlDbType.NVarChar, "TenDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemTrungChuyen", SqlDbType.NVarChar, "MaDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemTrungChuyen", SqlDbType.NVarChar, "TenDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrongLuongGross", SqlDbType.Decimal, "TrongLuongGross", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT_TrongLuongGross", SqlDbType.VarChar, "MaDVT_TrongLuongGross", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrongLuongThuan", SqlDbType.Decimal, "TrongLuongThuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT_TrongLuongThuan", SqlDbType.NVarChar, "MaDVT_TrongLuongThuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTheTich", SqlDbType.Decimal, "TongTheTich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT_TheTich", SqlDbType.NVarChar, "MaDVT_TheTich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoKienHang", SqlDbType.Decimal, "TongSoKienHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT_KienHang", SqlDbType.VarChar, "MaDVT_KienHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuChuHang", SqlDbType.NVarChar, "GhiChuChuHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoPL", SqlDbType.VarChar, "SoPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NganHangLC", SqlDbType.NVarChar, "NganHangLC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaFOB", SqlDbType.Decimal, "TriGiaFOB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_FOB", SqlDbType.VarChar, "MaTT_FOB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienFOB", SqlDbType.Decimal, "SoTienFOB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_TienFOB", SqlDbType.VarChar, "MaTT_TienFOB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhiVanChuyen", SqlDbType.Decimal, "PhiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_PhiVC", SqlDbType.VarChar, "MaTT_PhiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiThanhToanPhiVC", SqlDbType.NVarChar, "NoiThanhToanPhiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChiPhiXepHang1", SqlDbType.Decimal, "ChiPhiXepHang1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_ChiPhiXepHang1", SqlDbType.NVarChar, "MaTT_ChiPhiXepHang1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChiPhiXepHang1", SqlDbType.NVarChar, "LoaiChiPhiXepHang1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChiPhiXepHang2", SqlDbType.Decimal, "ChiPhiXepHang2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_ChiPhiXepHang2", SqlDbType.NVarChar, "MaTT_ChiPhiXepHang2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChiPhiXepHang2", SqlDbType.NVarChar, "LoaiChiPhiXepHang2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhiVC_DuongBo", SqlDbType.Decimal, "PhiVC_DuongBo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_PhiVC_DuongBo", SqlDbType.VarChar, "MaTT_PhiVC_DuongBo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhiBaoHiem", SqlDbType.Decimal, "PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_PhiBaoHiem", SqlDbType.VarChar, "MaTT_PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienPhiBaoHiem", SqlDbType.Decimal, "SoTienPhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_TienPhiBaoHiem", SqlDbType.VarChar, "MaTT_TienPhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienKhauTru", SqlDbType.Decimal, "SoTienKhauTru", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_TienKhauTru", SqlDbType.VarChar, "MaTT_TienKhauTru", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiKhauTru", SqlDbType.NVarChar, "LoaiKhauTru", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienKhac", SqlDbType.Decimal, "SoTienKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_TienKhac", SqlDbType.VarChar, "MaTT_TienKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiSoTienKhac", SqlDbType.NVarChar, "LoaiSoTienKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaHoaDon", SqlDbType.Decimal, "TongTriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_TongTriGia", SqlDbType.VarChar, "MaTT_TongTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DieuKienGiaHoaDon", SqlDbType.VarChar, "DieuKienGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, "DiaDiemGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuDacBiet", SqlDbType.NVarChar, "GhiChuDacBiet", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoDongHang", SqlDbType.Decimal, "TongSoDongHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiXuatNhap", SqlDbType.VarChar, "PhanLoaiXuatNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDaiLyHQ", SqlDbType.VarChar, "MaDaiLyHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHoaDon", SqlDbType.VarChar, "SoHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayLapHoaDon", SqlDbType.DateTime, "NgayLapHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemLapHoaDon", SqlDbType.NVarChar, "DiaDiemLapHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongThucThanhToan", SqlDbType.NVarChar, "PhuongThucThanhToan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiLienQuan", SqlDbType.VarChar, "PhanLoaiLienQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiXNK", SqlDbType.VarChar, "MaNguoiXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiXNK", SqlDbType.NVarChar, "TenNguoiXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinh_NguoiXNK", SqlDbType.VarChar, "MaBuuChinh_NguoiXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChi_NguoiXNK", SqlDbType.NVarChar, "DiaChi_NguoiXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoai_NguoiXNK", SqlDbType.VarChar, "SoDienThoai_NguoiXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiLapHoaDon", SqlDbType.NVarChar, "NguoiLapHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiGuiNhan", SqlDbType.VarChar, "MaNguoiGuiNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiGuiNhan", SqlDbType.NVarChar, "TenNguoiGuiNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhNguoiGuiNhan", SqlDbType.VarChar, "MaBuuChinhNguoiGuiNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiNhanGui1", SqlDbType.NVarChar, "DiaChiNguoiNhanGui1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiNhanGui2", SqlDbType.NVarChar, "DiaChiNguoiNhanGui2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiNhanGui3", SqlDbType.NVarChar, "DiaChiNguoiNhanGui3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiNhanGui4", SqlDbType.NVarChar, "DiaChiNguoiNhanGui4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNhanGui", SqlDbType.VarChar, "SoDienThoaiNhanGui", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocSoTaiNhanGui", SqlDbType.VarChar, "NuocSoTaiNhanGui", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKyHieu", SqlDbType.NVarChar, "MaKyHieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiVanChuyen", SqlDbType.VarChar, "PhanLoaiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenPTVC", SqlDbType.NVarChar, "TenPTVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuChuyenDi", SqlDbType.VarChar, "SoHieuChuyenDi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, "TenDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiKyXephang", SqlDbType.DateTime, "ThoiKyXephang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemDoHang", SqlDbType.NVarChar, "TenDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemTrungChuyen", SqlDbType.NVarChar, "MaDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemTrungChuyen", SqlDbType.NVarChar, "TenDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrongLuongGross", SqlDbType.Decimal, "TrongLuongGross", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT_TrongLuongGross", SqlDbType.VarChar, "MaDVT_TrongLuongGross", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrongLuongThuan", SqlDbType.Decimal, "TrongLuongThuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT_TrongLuongThuan", SqlDbType.NVarChar, "MaDVT_TrongLuongThuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTheTich", SqlDbType.Decimal, "TongTheTich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT_TheTich", SqlDbType.NVarChar, "MaDVT_TheTich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoKienHang", SqlDbType.Decimal, "TongSoKienHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT_KienHang", SqlDbType.VarChar, "MaDVT_KienHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuChuHang", SqlDbType.NVarChar, "GhiChuChuHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoPL", SqlDbType.VarChar, "SoPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NganHangLC", SqlDbType.NVarChar, "NganHangLC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaFOB", SqlDbType.Decimal, "TriGiaFOB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_FOB", SqlDbType.VarChar, "MaTT_FOB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienFOB", SqlDbType.Decimal, "SoTienFOB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_TienFOB", SqlDbType.VarChar, "MaTT_TienFOB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhiVanChuyen", SqlDbType.Decimal, "PhiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_PhiVC", SqlDbType.VarChar, "MaTT_PhiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiThanhToanPhiVC", SqlDbType.NVarChar, "NoiThanhToanPhiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChiPhiXepHang1", SqlDbType.Decimal, "ChiPhiXepHang1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_ChiPhiXepHang1", SqlDbType.NVarChar, "MaTT_ChiPhiXepHang1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChiPhiXepHang1", SqlDbType.NVarChar, "LoaiChiPhiXepHang1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChiPhiXepHang2", SqlDbType.Decimal, "ChiPhiXepHang2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_ChiPhiXepHang2", SqlDbType.NVarChar, "MaTT_ChiPhiXepHang2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChiPhiXepHang2", SqlDbType.NVarChar, "LoaiChiPhiXepHang2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhiVC_DuongBo", SqlDbType.Decimal, "PhiVC_DuongBo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_PhiVC_DuongBo", SqlDbType.VarChar, "MaTT_PhiVC_DuongBo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhiBaoHiem", SqlDbType.Decimal, "PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_PhiBaoHiem", SqlDbType.VarChar, "MaTT_PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienPhiBaoHiem", SqlDbType.Decimal, "SoTienPhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_TienPhiBaoHiem", SqlDbType.VarChar, "MaTT_TienPhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienKhauTru", SqlDbType.Decimal, "SoTienKhauTru", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_TienKhauTru", SqlDbType.VarChar, "MaTT_TienKhauTru", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiKhauTru", SqlDbType.NVarChar, "LoaiKhauTru", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienKhac", SqlDbType.Decimal, "SoTienKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_TienKhac", SqlDbType.VarChar, "MaTT_TienKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiSoTienKhac", SqlDbType.NVarChar, "LoaiSoTienKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaHoaDon", SqlDbType.Decimal, "TongTriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_TongTriGia", SqlDbType.VarChar, "MaTT_TongTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DieuKienGiaHoaDon", SqlDbType.VarChar, "DieuKienGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, "DiaDiemGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuDacBiet", SqlDbType.NVarChar, "GhiChuDacBiet", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoDongHang", SqlDbType.Decimal, "TongSoDongHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_HoaDon VALUES(@SoTiepNhan, @NgayTiepNhan, @PhanLoaiXuatNhap, @MaDaiLyHQ, @SoHoaDon, @NgayLapHoaDon, @DiaDiemLapHoaDon, @PhuongThucThanhToan, @PhanLoaiLienQuan, @MaNguoiXNK, @TenNguoiXNK, @MaBuuChinh_NguoiXNK, @DiaChi_NguoiXNK, @SoDienThoai_NguoiXNK, @NguoiLapHoaDon, @MaNguoiGuiNhan, @TenNguoiGuiNhan, @MaBuuChinhNguoiGuiNhan, @DiaChiNguoiNhanGui1, @DiaChiNguoiNhanGui2, @DiaChiNguoiNhanGui3, @DiaChiNguoiNhanGui4, @SoDienThoaiNhanGui, @NuocSoTaiNhanGui, @MaKyHieu, @PhanLoaiVanChuyen, @TenPTVC, @SoHieuChuyenDi, @MaDiaDiemXepHang, @TenDiaDiemXepHang, @ThoiKyXephang, @MaDiaDiemDoHang, @TenDiaDiemDoHang, @MaDiaDiemTrungChuyen, @TenDiaDiemTrungChuyen, @TrongLuongGross, @MaDVT_TrongLuongGross, @TrongLuongThuan, @MaDVT_TrongLuongThuan, @TongTheTich, @MaDVT_TheTich, @TongSoKienHang, @MaDVT_KienHang, @GhiChuChuHang, @SoPL, @NganHangLC, @TriGiaFOB, @MaTT_FOB, @SoTienFOB, @MaTT_TienFOB, @PhiVanChuyen, @MaTT_PhiVC, @NoiThanhToanPhiVC, @ChiPhiXepHang1, @MaTT_ChiPhiXepHang1, @LoaiChiPhiXepHang1, @ChiPhiXepHang2, @MaTT_ChiPhiXepHang2, @LoaiChiPhiXepHang2, @PhiVC_DuongBo, @MaTT_PhiVC_DuongBo, @PhiBaoHiem, @MaTT_PhiBaoHiem, @SoTienPhiBaoHiem, @MaTT_TienPhiBaoHiem, @SoTienKhauTru, @MaTT_TienKhauTru, @LoaiKhauTru, @SoTienKhac, @MaTT_TienKhac, @LoaiSoTienKhac, @TongTriGiaHoaDon, @MaTT_TongTriGia, @DieuKienGiaHoaDon, @DiaDiemGiaoHang, @GhiChuDacBiet, @TongSoDongHang, @TrangThaiXuLy, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_HoaDon SET SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, PhanLoaiXuatNhap = @PhanLoaiXuatNhap, MaDaiLyHQ = @MaDaiLyHQ, SoHoaDon = @SoHoaDon, NgayLapHoaDon = @NgayLapHoaDon, DiaDiemLapHoaDon = @DiaDiemLapHoaDon, PhuongThucThanhToan = @PhuongThucThanhToan, PhanLoaiLienQuan = @PhanLoaiLienQuan, MaNguoiXNK = @MaNguoiXNK, TenNguoiXNK = @TenNguoiXNK, MaBuuChinh_NguoiXNK = @MaBuuChinh_NguoiXNK, DiaChi_NguoiXNK = @DiaChi_NguoiXNK, SoDienThoai_NguoiXNK = @SoDienThoai_NguoiXNK, NguoiLapHoaDon = @NguoiLapHoaDon, MaNguoiGuiNhan = @MaNguoiGuiNhan, TenNguoiGuiNhan = @TenNguoiGuiNhan, MaBuuChinhNguoiGuiNhan = @MaBuuChinhNguoiGuiNhan, DiaChiNguoiNhanGui1 = @DiaChiNguoiNhanGui1, DiaChiNguoiNhanGui2 = @DiaChiNguoiNhanGui2, DiaChiNguoiNhanGui3 = @DiaChiNguoiNhanGui3, DiaChiNguoiNhanGui4 = @DiaChiNguoiNhanGui4, SoDienThoaiNhanGui = @SoDienThoaiNhanGui, NuocSoTaiNhanGui = @NuocSoTaiNhanGui, MaKyHieu = @MaKyHieu, PhanLoaiVanChuyen = @PhanLoaiVanChuyen, TenPTVC = @TenPTVC, SoHieuChuyenDi = @SoHieuChuyenDi, MaDiaDiemXepHang = @MaDiaDiemXepHang, TenDiaDiemXepHang = @TenDiaDiemXepHang, ThoiKyXephang = @ThoiKyXephang, MaDiaDiemDoHang = @MaDiaDiemDoHang, TenDiaDiemDoHang = @TenDiaDiemDoHang, MaDiaDiemTrungChuyen = @MaDiaDiemTrungChuyen, TenDiaDiemTrungChuyen = @TenDiaDiemTrungChuyen, TrongLuongGross = @TrongLuongGross, MaDVT_TrongLuongGross = @MaDVT_TrongLuongGross, TrongLuongThuan = @TrongLuongThuan, MaDVT_TrongLuongThuan = @MaDVT_TrongLuongThuan, TongTheTich = @TongTheTich, MaDVT_TheTich = @MaDVT_TheTich, TongSoKienHang = @TongSoKienHang, MaDVT_KienHang = @MaDVT_KienHang, GhiChuChuHang = @GhiChuChuHang, SoPL = @SoPL, NganHangLC = @NganHangLC, TriGiaFOB = @TriGiaFOB, MaTT_FOB = @MaTT_FOB, SoTienFOB = @SoTienFOB, MaTT_TienFOB = @MaTT_TienFOB, PhiVanChuyen = @PhiVanChuyen, MaTT_PhiVC = @MaTT_PhiVC, NoiThanhToanPhiVC = @NoiThanhToanPhiVC, ChiPhiXepHang1 = @ChiPhiXepHang1, MaTT_ChiPhiXepHang1 = @MaTT_ChiPhiXepHang1, LoaiChiPhiXepHang1 = @LoaiChiPhiXepHang1, ChiPhiXepHang2 = @ChiPhiXepHang2, MaTT_ChiPhiXepHang2 = @MaTT_ChiPhiXepHang2, LoaiChiPhiXepHang2 = @LoaiChiPhiXepHang2, PhiVC_DuongBo = @PhiVC_DuongBo, MaTT_PhiVC_DuongBo = @MaTT_PhiVC_DuongBo, PhiBaoHiem = @PhiBaoHiem, MaTT_PhiBaoHiem = @MaTT_PhiBaoHiem, SoTienPhiBaoHiem = @SoTienPhiBaoHiem, MaTT_TienPhiBaoHiem = @MaTT_TienPhiBaoHiem, SoTienKhauTru = @SoTienKhauTru, MaTT_TienKhauTru = @MaTT_TienKhauTru, LoaiKhauTru = @LoaiKhauTru, SoTienKhac = @SoTienKhac, MaTT_TienKhac = @MaTT_TienKhac, LoaiSoTienKhac = @LoaiSoTienKhac, TongTriGiaHoaDon = @TongTriGiaHoaDon, MaTT_TongTriGia = @MaTT_TongTriGia, DieuKienGiaHoaDon = @DieuKienGiaHoaDon, DiaDiemGiaoHang = @DiaDiemGiaoHang, GhiChuDacBiet = @GhiChuDacBiet, TongSoDongHang = @TongSoDongHang, TrangThaiXuLy = @TrangThaiXuLy, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HoaDon WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiXuatNhap", SqlDbType.VarChar, "PhanLoaiXuatNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDaiLyHQ", SqlDbType.VarChar, "MaDaiLyHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHoaDon", SqlDbType.VarChar, "SoHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayLapHoaDon", SqlDbType.DateTime, "NgayLapHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemLapHoaDon", SqlDbType.NVarChar, "DiaDiemLapHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongThucThanhToan", SqlDbType.NVarChar, "PhuongThucThanhToan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiLienQuan", SqlDbType.VarChar, "PhanLoaiLienQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiXNK", SqlDbType.VarChar, "MaNguoiXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiXNK", SqlDbType.NVarChar, "TenNguoiXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinh_NguoiXNK", SqlDbType.VarChar, "MaBuuChinh_NguoiXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChi_NguoiXNK", SqlDbType.NVarChar, "DiaChi_NguoiXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoai_NguoiXNK", SqlDbType.VarChar, "SoDienThoai_NguoiXNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiLapHoaDon", SqlDbType.NVarChar, "NguoiLapHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiGuiNhan", SqlDbType.VarChar, "MaNguoiGuiNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiGuiNhan", SqlDbType.NVarChar, "TenNguoiGuiNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhNguoiGuiNhan", SqlDbType.VarChar, "MaBuuChinhNguoiGuiNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiNhanGui1", SqlDbType.NVarChar, "DiaChiNguoiNhanGui1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiNhanGui2", SqlDbType.NVarChar, "DiaChiNguoiNhanGui2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiNhanGui3", SqlDbType.NVarChar, "DiaChiNguoiNhanGui3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiNhanGui4", SqlDbType.NVarChar, "DiaChiNguoiNhanGui4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNhanGui", SqlDbType.VarChar, "SoDienThoaiNhanGui", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocSoTaiNhanGui", SqlDbType.VarChar, "NuocSoTaiNhanGui", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKyHieu", SqlDbType.NVarChar, "MaKyHieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiVanChuyen", SqlDbType.VarChar, "PhanLoaiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenPTVC", SqlDbType.NVarChar, "TenPTVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuChuyenDi", SqlDbType.VarChar, "SoHieuChuyenDi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, "TenDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiKyXephang", SqlDbType.DateTime, "ThoiKyXephang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemDoHang", SqlDbType.NVarChar, "TenDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemTrungChuyen", SqlDbType.NVarChar, "MaDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiemTrungChuyen", SqlDbType.NVarChar, "TenDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrongLuongGross", SqlDbType.Decimal, "TrongLuongGross", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT_TrongLuongGross", SqlDbType.VarChar, "MaDVT_TrongLuongGross", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrongLuongThuan", SqlDbType.Decimal, "TrongLuongThuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT_TrongLuongThuan", SqlDbType.NVarChar, "MaDVT_TrongLuongThuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTheTich", SqlDbType.Decimal, "TongTheTich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT_TheTich", SqlDbType.NVarChar, "MaDVT_TheTich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoKienHang", SqlDbType.Decimal, "TongSoKienHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDVT_KienHang", SqlDbType.VarChar, "MaDVT_KienHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuChuHang", SqlDbType.NVarChar, "GhiChuChuHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoPL", SqlDbType.VarChar, "SoPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NganHangLC", SqlDbType.NVarChar, "NganHangLC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaFOB", SqlDbType.Decimal, "TriGiaFOB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_FOB", SqlDbType.VarChar, "MaTT_FOB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienFOB", SqlDbType.Decimal, "SoTienFOB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_TienFOB", SqlDbType.VarChar, "MaTT_TienFOB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhiVanChuyen", SqlDbType.Decimal, "PhiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_PhiVC", SqlDbType.VarChar, "MaTT_PhiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiThanhToanPhiVC", SqlDbType.NVarChar, "NoiThanhToanPhiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChiPhiXepHang1", SqlDbType.Decimal, "ChiPhiXepHang1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_ChiPhiXepHang1", SqlDbType.NVarChar, "MaTT_ChiPhiXepHang1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChiPhiXepHang1", SqlDbType.NVarChar, "LoaiChiPhiXepHang1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChiPhiXepHang2", SqlDbType.Decimal, "ChiPhiXepHang2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_ChiPhiXepHang2", SqlDbType.NVarChar, "MaTT_ChiPhiXepHang2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChiPhiXepHang2", SqlDbType.NVarChar, "LoaiChiPhiXepHang2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhiVC_DuongBo", SqlDbType.Decimal, "PhiVC_DuongBo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_PhiVC_DuongBo", SqlDbType.VarChar, "MaTT_PhiVC_DuongBo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhiBaoHiem", SqlDbType.Decimal, "PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_PhiBaoHiem", SqlDbType.VarChar, "MaTT_PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienPhiBaoHiem", SqlDbType.Decimal, "SoTienPhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_TienPhiBaoHiem", SqlDbType.VarChar, "MaTT_TienPhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienKhauTru", SqlDbType.Decimal, "SoTienKhauTru", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_TienKhauTru", SqlDbType.VarChar, "MaTT_TienKhauTru", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiKhauTru", SqlDbType.NVarChar, "LoaiKhauTru", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienKhac", SqlDbType.Decimal, "SoTienKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_TienKhac", SqlDbType.VarChar, "MaTT_TienKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiSoTienKhac", SqlDbType.NVarChar, "LoaiSoTienKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaHoaDon", SqlDbType.Decimal, "TongTriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTT_TongTriGia", SqlDbType.VarChar, "MaTT_TongTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DieuKienGiaHoaDon", SqlDbType.VarChar, "DieuKienGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, "DiaDiemGiaoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuDacBiet", SqlDbType.NVarChar, "GhiChuDacBiet", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoDongHang", SqlDbType.Decimal, "TongSoDongHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiXuatNhap", SqlDbType.VarChar, "PhanLoaiXuatNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDaiLyHQ", SqlDbType.VarChar, "MaDaiLyHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHoaDon", SqlDbType.VarChar, "SoHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayLapHoaDon", SqlDbType.DateTime, "NgayLapHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemLapHoaDon", SqlDbType.NVarChar, "DiaDiemLapHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongThucThanhToan", SqlDbType.NVarChar, "PhuongThucThanhToan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiLienQuan", SqlDbType.VarChar, "PhanLoaiLienQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiXNK", SqlDbType.VarChar, "MaNguoiXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiXNK", SqlDbType.NVarChar, "TenNguoiXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinh_NguoiXNK", SqlDbType.VarChar, "MaBuuChinh_NguoiXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChi_NguoiXNK", SqlDbType.NVarChar, "DiaChi_NguoiXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoai_NguoiXNK", SqlDbType.VarChar, "SoDienThoai_NguoiXNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiLapHoaDon", SqlDbType.NVarChar, "NguoiLapHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiGuiNhan", SqlDbType.VarChar, "MaNguoiGuiNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiGuiNhan", SqlDbType.NVarChar, "TenNguoiGuiNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhNguoiGuiNhan", SqlDbType.VarChar, "MaBuuChinhNguoiGuiNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiNhanGui1", SqlDbType.NVarChar, "DiaChiNguoiNhanGui1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiNhanGui2", SqlDbType.NVarChar, "DiaChiNguoiNhanGui2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiNhanGui3", SqlDbType.NVarChar, "DiaChiNguoiNhanGui3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiNhanGui4", SqlDbType.NVarChar, "DiaChiNguoiNhanGui4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNhanGui", SqlDbType.VarChar, "SoDienThoaiNhanGui", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocSoTaiNhanGui", SqlDbType.VarChar, "NuocSoTaiNhanGui", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKyHieu", SqlDbType.NVarChar, "MaKyHieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiVanChuyen", SqlDbType.VarChar, "PhanLoaiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenPTVC", SqlDbType.NVarChar, "TenPTVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuChuyenDi", SqlDbType.VarChar, "SoHieuChuyenDi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, "TenDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiKyXephang", SqlDbType.DateTime, "ThoiKyXephang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, "MaDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemDoHang", SqlDbType.NVarChar, "TenDiaDiemDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemTrungChuyen", SqlDbType.NVarChar, "MaDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiemTrungChuyen", SqlDbType.NVarChar, "TenDiaDiemTrungChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrongLuongGross", SqlDbType.Decimal, "TrongLuongGross", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT_TrongLuongGross", SqlDbType.VarChar, "MaDVT_TrongLuongGross", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrongLuongThuan", SqlDbType.Decimal, "TrongLuongThuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT_TrongLuongThuan", SqlDbType.NVarChar, "MaDVT_TrongLuongThuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTheTich", SqlDbType.Decimal, "TongTheTich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT_TheTich", SqlDbType.NVarChar, "MaDVT_TheTich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoKienHang", SqlDbType.Decimal, "TongSoKienHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDVT_KienHang", SqlDbType.VarChar, "MaDVT_KienHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuChuHang", SqlDbType.NVarChar, "GhiChuChuHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoPL", SqlDbType.VarChar, "SoPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NganHangLC", SqlDbType.NVarChar, "NganHangLC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaFOB", SqlDbType.Decimal, "TriGiaFOB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_FOB", SqlDbType.VarChar, "MaTT_FOB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienFOB", SqlDbType.Decimal, "SoTienFOB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_TienFOB", SqlDbType.VarChar, "MaTT_TienFOB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhiVanChuyen", SqlDbType.Decimal, "PhiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_PhiVC", SqlDbType.VarChar, "MaTT_PhiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiThanhToanPhiVC", SqlDbType.NVarChar, "NoiThanhToanPhiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChiPhiXepHang1", SqlDbType.Decimal, "ChiPhiXepHang1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_ChiPhiXepHang1", SqlDbType.NVarChar, "MaTT_ChiPhiXepHang1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChiPhiXepHang1", SqlDbType.NVarChar, "LoaiChiPhiXepHang1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChiPhiXepHang2", SqlDbType.Decimal, "ChiPhiXepHang2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_ChiPhiXepHang2", SqlDbType.NVarChar, "MaTT_ChiPhiXepHang2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChiPhiXepHang2", SqlDbType.NVarChar, "LoaiChiPhiXepHang2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhiVC_DuongBo", SqlDbType.Decimal, "PhiVC_DuongBo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_PhiVC_DuongBo", SqlDbType.VarChar, "MaTT_PhiVC_DuongBo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhiBaoHiem", SqlDbType.Decimal, "PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_PhiBaoHiem", SqlDbType.VarChar, "MaTT_PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienPhiBaoHiem", SqlDbType.Decimal, "SoTienPhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_TienPhiBaoHiem", SqlDbType.VarChar, "MaTT_TienPhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienKhauTru", SqlDbType.Decimal, "SoTienKhauTru", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_TienKhauTru", SqlDbType.VarChar, "MaTT_TienKhauTru", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiKhauTru", SqlDbType.NVarChar, "LoaiKhauTru", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienKhac", SqlDbType.Decimal, "SoTienKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_TienKhac", SqlDbType.VarChar, "MaTT_TienKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiSoTienKhac", SqlDbType.NVarChar, "LoaiSoTienKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaHoaDon", SqlDbType.Decimal, "TongTriGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTT_TongTriGia", SqlDbType.VarChar, "MaTT_TongTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DieuKienGiaHoaDon", SqlDbType.VarChar, "DieuKienGiaHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, "DiaDiemGiaoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuDacBiet", SqlDbType.NVarChar, "GhiChuDacBiet", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoDongHang", SqlDbType.Decimal, "TongSoDongHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_HoaDon Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HoaDon_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_HoaDon> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_HoaDon> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_HoaDon> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HoaDon_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HoaDon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HoaDon_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HoaDon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_HoaDon(decimal soTiepNhan, DateTime ngayTiepNhan, string phanLoaiXuatNhap, string maDaiLyHQ, string soHoaDon, DateTime ngayLapHoaDon, string diaDiemLapHoaDon, string phuongThucThanhToan, string phanLoaiLienQuan, string maNguoiXNK, string tenNguoiXNK, string maBuuChinh_NguoiXNK, string diaChi_NguoiXNK, string soDienThoai_NguoiXNK, string nguoiLapHoaDon, string maNguoiGuiNhan, string tenNguoiGuiNhan, string maBuuChinhNguoiGuiNhan, string diaChiNguoiNhanGui1, string diaChiNguoiNhanGui2, string diaChiNguoiNhanGui3, string diaChiNguoiNhanGui4, string soDienThoaiNhanGui, string nuocSoTaiNhanGui, string maKyHieu, string phanLoaiVanChuyen, string tenPTVC, string soHieuChuyenDi, string maDiaDiemXepHang, string tenDiaDiemXepHang, DateTime thoiKyXephang, string maDiaDiemDoHang, string tenDiaDiemDoHang, string maDiaDiemTrungChuyen, string tenDiaDiemTrungChuyen, decimal trongLuongGross, string maDVT_TrongLuongGross, decimal trongLuongThuan, string maDVT_TrongLuongThuan, decimal tongTheTich, string maDVT_TheTich, decimal tongSoKienHang, string maDVT_KienHang, string ghiChuChuHang, string soPL, string nganHangLC, decimal triGiaFOB, string maTT_FOB, decimal soTienFOB, string maTT_TienFOB, decimal phiVanChuyen, string maTT_PhiVC, string noiThanhToanPhiVC, decimal chiPhiXepHang1, string maTT_ChiPhiXepHang1, string loaiChiPhiXepHang1, decimal chiPhiXepHang2, string maTT_ChiPhiXepHang2, string loaiChiPhiXepHang2, decimal phiVC_DuongBo, string maTT_PhiVC_DuongBo, decimal phiBaoHiem, string maTT_PhiBaoHiem, decimal soTienPhiBaoHiem, string maTT_TienPhiBaoHiem, decimal soTienKhauTru, string maTT_TienKhauTru, string loaiKhauTru, decimal soTienKhac, string maTT_TienKhac, string loaiSoTienKhac, decimal tongTriGiaHoaDon, string maTT_TongTriGia, string dieuKienGiaHoaDon, string diaDiemGiaoHang, string ghiChuDacBiet, decimal tongSoDongHang, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_HoaDon entity = new KDT_VNACC_HoaDon();	
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.PhanLoaiXuatNhap = phanLoaiXuatNhap;
			entity.MaDaiLyHQ = maDaiLyHQ;
			entity.SoHoaDon = soHoaDon;
			entity.NgayLapHoaDon = ngayLapHoaDon;
			entity.DiaDiemLapHoaDon = diaDiemLapHoaDon;
			entity.PhuongThucThanhToan = phuongThucThanhToan;
			entity.PhanLoaiLienQuan = phanLoaiLienQuan;
			entity.MaNguoiXNK = maNguoiXNK;
			entity.TenNguoiXNK = tenNguoiXNK;
			entity.MaBuuChinh_NguoiXNK = maBuuChinh_NguoiXNK;
			entity.DiaChi_NguoiXNK = diaChi_NguoiXNK;
			entity.SoDienThoai_NguoiXNK = soDienThoai_NguoiXNK;
			entity.NguoiLapHoaDon = nguoiLapHoaDon;
			entity.MaNguoiGuiNhan = maNguoiGuiNhan;
			entity.TenNguoiGuiNhan = tenNguoiGuiNhan;
			entity.MaBuuChinhNguoiGuiNhan = maBuuChinhNguoiGuiNhan;
			entity.DiaChiNguoiNhanGui1 = diaChiNguoiNhanGui1;
			entity.DiaChiNguoiNhanGui2 = diaChiNguoiNhanGui2;
			entity.DiaChiNguoiNhanGui3 = diaChiNguoiNhanGui3;
			entity.DiaChiNguoiNhanGui4 = diaChiNguoiNhanGui4;
			entity.SoDienThoaiNhanGui = soDienThoaiNhanGui;
			entity.NuocSoTaiNhanGui = nuocSoTaiNhanGui;
			entity.MaKyHieu = maKyHieu;
			entity.PhanLoaiVanChuyen = phanLoaiVanChuyen;
			entity.TenPTVC = tenPTVC;
			entity.SoHieuChuyenDi = soHieuChuyenDi;
			entity.MaDiaDiemXepHang = maDiaDiemXepHang;
			entity.TenDiaDiemXepHang = tenDiaDiemXepHang;
			entity.ThoiKyXephang = thoiKyXephang;
			entity.MaDiaDiemDoHang = maDiaDiemDoHang;
			entity.TenDiaDiemDoHang = tenDiaDiemDoHang;
			entity.MaDiaDiemTrungChuyen = maDiaDiemTrungChuyen;
			entity.TenDiaDiemTrungChuyen = tenDiaDiemTrungChuyen;
			entity.TrongLuongGross = trongLuongGross;
			entity.MaDVT_TrongLuongGross = maDVT_TrongLuongGross;
			entity.TrongLuongThuan = trongLuongThuan;
			entity.MaDVT_TrongLuongThuan = maDVT_TrongLuongThuan;
			entity.TongTheTich = tongTheTich;
			entity.MaDVT_TheTich = maDVT_TheTich;
			entity.TongSoKienHang = tongSoKienHang;
			entity.MaDVT_KienHang = maDVT_KienHang;
			entity.GhiChuChuHang = ghiChuChuHang;
			entity.SoPL = soPL;
			entity.NganHangLC = nganHangLC;
			entity.TriGiaFOB = triGiaFOB;
			entity.MaTT_FOB = maTT_FOB;
			entity.SoTienFOB = soTienFOB;
			entity.MaTT_TienFOB = maTT_TienFOB;
			entity.PhiVanChuyen = phiVanChuyen;
			entity.MaTT_PhiVC = maTT_PhiVC;
			entity.NoiThanhToanPhiVC = noiThanhToanPhiVC;
			entity.ChiPhiXepHang1 = chiPhiXepHang1;
			entity.MaTT_ChiPhiXepHang1 = maTT_ChiPhiXepHang1;
			entity.LoaiChiPhiXepHang1 = loaiChiPhiXepHang1;
			entity.ChiPhiXepHang2 = chiPhiXepHang2;
			entity.MaTT_ChiPhiXepHang2 = maTT_ChiPhiXepHang2;
			entity.LoaiChiPhiXepHang2 = loaiChiPhiXepHang2;
			entity.PhiVC_DuongBo = phiVC_DuongBo;
			entity.MaTT_PhiVC_DuongBo = maTT_PhiVC_DuongBo;
			entity.PhiBaoHiem = phiBaoHiem;
			entity.MaTT_PhiBaoHiem = maTT_PhiBaoHiem;
			entity.SoTienPhiBaoHiem = soTienPhiBaoHiem;
			entity.MaTT_TienPhiBaoHiem = maTT_TienPhiBaoHiem;
			entity.SoTienKhauTru = soTienKhauTru;
			entity.MaTT_TienKhauTru = maTT_TienKhauTru;
			entity.LoaiKhauTru = loaiKhauTru;
			entity.SoTienKhac = soTienKhac;
			entity.MaTT_TienKhac = maTT_TienKhac;
			entity.LoaiSoTienKhac = loaiSoTienKhac;
			entity.TongTriGiaHoaDon = tongTriGiaHoaDon;
			entity.MaTT_TongTriGia = maTT_TongTriGia;
			entity.DieuKienGiaHoaDon = dieuKienGiaHoaDon;
			entity.DiaDiemGiaoHang = diaDiemGiaoHang;
			entity.GhiChuDacBiet = ghiChuDacBiet;
			entity.TongSoDongHang = tongSoDongHang;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_HoaDon_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@PhanLoaiXuatNhap", SqlDbType.VarChar, PhanLoaiXuatNhap);
			db.AddInParameter(dbCommand, "@MaDaiLyHQ", SqlDbType.VarChar, MaDaiLyHQ);
			db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.VarChar, SoHoaDon);
			db.AddInParameter(dbCommand, "@NgayLapHoaDon", SqlDbType.DateTime, NgayLapHoaDon.Year <= 1753 ? DBNull.Value : (object) NgayLapHoaDon);
			db.AddInParameter(dbCommand, "@DiaDiemLapHoaDon", SqlDbType.NVarChar, DiaDiemLapHoaDon);
			db.AddInParameter(dbCommand, "@PhuongThucThanhToan", SqlDbType.NVarChar, PhuongThucThanhToan);
			db.AddInParameter(dbCommand, "@PhanLoaiLienQuan", SqlDbType.VarChar, PhanLoaiLienQuan);
			db.AddInParameter(dbCommand, "@MaNguoiXNK", SqlDbType.VarChar, MaNguoiXNK);
			db.AddInParameter(dbCommand, "@TenNguoiXNK", SqlDbType.NVarChar, TenNguoiXNK);
			db.AddInParameter(dbCommand, "@MaBuuChinh_NguoiXNK", SqlDbType.VarChar, MaBuuChinh_NguoiXNK);
			db.AddInParameter(dbCommand, "@DiaChi_NguoiXNK", SqlDbType.NVarChar, DiaChi_NguoiXNK);
			db.AddInParameter(dbCommand, "@SoDienThoai_NguoiXNK", SqlDbType.VarChar, SoDienThoai_NguoiXNK);
			db.AddInParameter(dbCommand, "@NguoiLapHoaDon", SqlDbType.NVarChar, NguoiLapHoaDon);
			db.AddInParameter(dbCommand, "@MaNguoiGuiNhan", SqlDbType.VarChar, MaNguoiGuiNhan);
			db.AddInParameter(dbCommand, "@TenNguoiGuiNhan", SqlDbType.NVarChar, TenNguoiGuiNhan);
			db.AddInParameter(dbCommand, "@MaBuuChinhNguoiGuiNhan", SqlDbType.VarChar, MaBuuChinhNguoiGuiNhan);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhanGui1", SqlDbType.NVarChar, DiaChiNguoiNhanGui1);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhanGui2", SqlDbType.NVarChar, DiaChiNguoiNhanGui2);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhanGui3", SqlDbType.NVarChar, DiaChiNguoiNhanGui3);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhanGui4", SqlDbType.NVarChar, DiaChiNguoiNhanGui4);
			db.AddInParameter(dbCommand, "@SoDienThoaiNhanGui", SqlDbType.VarChar, SoDienThoaiNhanGui);
			db.AddInParameter(dbCommand, "@NuocSoTaiNhanGui", SqlDbType.VarChar, NuocSoTaiNhanGui);
			db.AddInParameter(dbCommand, "@MaKyHieu", SqlDbType.NVarChar, MaKyHieu);
			db.AddInParameter(dbCommand, "@PhanLoaiVanChuyen", SqlDbType.VarChar, PhanLoaiVanChuyen);
			db.AddInParameter(dbCommand, "@TenPTVC", SqlDbType.NVarChar, TenPTVC);
			db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.VarChar, SoHieuChuyenDi);
			db.AddInParameter(dbCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, MaDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, TenDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@ThoiKyXephang", SqlDbType.DateTime, ThoiKyXephang.Year <= 1753 ? DBNull.Value : (object) ThoiKyXephang);
			db.AddInParameter(dbCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, MaDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemDoHang", SqlDbType.NVarChar, TenDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@MaDiaDiemTrungChuyen", SqlDbType.NVarChar, MaDiaDiemTrungChuyen);
			db.AddInParameter(dbCommand, "@TenDiaDiemTrungChuyen", SqlDbType.NVarChar, TenDiaDiemTrungChuyen);
			db.AddInParameter(dbCommand, "@TrongLuongGross", SqlDbType.Decimal, TrongLuongGross);
			db.AddInParameter(dbCommand, "@MaDVT_TrongLuongGross", SqlDbType.VarChar, MaDVT_TrongLuongGross);
			db.AddInParameter(dbCommand, "@TrongLuongThuan", SqlDbType.Decimal, TrongLuongThuan);
			db.AddInParameter(dbCommand, "@MaDVT_TrongLuongThuan", SqlDbType.NVarChar, MaDVT_TrongLuongThuan);
			db.AddInParameter(dbCommand, "@TongTheTich", SqlDbType.Decimal, TongTheTich);
			db.AddInParameter(dbCommand, "@MaDVT_TheTich", SqlDbType.NVarChar, MaDVT_TheTich);
			db.AddInParameter(dbCommand, "@TongSoKienHang", SqlDbType.Decimal, TongSoKienHang);
			db.AddInParameter(dbCommand, "@MaDVT_KienHang", SqlDbType.VarChar, MaDVT_KienHang);
			db.AddInParameter(dbCommand, "@GhiChuChuHang", SqlDbType.NVarChar, GhiChuChuHang);
			db.AddInParameter(dbCommand, "@SoPL", SqlDbType.VarChar, SoPL);
			db.AddInParameter(dbCommand, "@NganHangLC", SqlDbType.NVarChar, NganHangLC);
			db.AddInParameter(dbCommand, "@TriGiaFOB", SqlDbType.Decimal, TriGiaFOB);
			db.AddInParameter(dbCommand, "@MaTT_FOB", SqlDbType.VarChar, MaTT_FOB);
			db.AddInParameter(dbCommand, "@SoTienFOB", SqlDbType.Decimal, SoTienFOB);
			db.AddInParameter(dbCommand, "@MaTT_TienFOB", SqlDbType.VarChar, MaTT_TienFOB);
			db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Decimal, PhiVanChuyen);
			db.AddInParameter(dbCommand, "@MaTT_PhiVC", SqlDbType.VarChar, MaTT_PhiVC);
			db.AddInParameter(dbCommand, "@NoiThanhToanPhiVC", SqlDbType.NVarChar, NoiThanhToanPhiVC);
			db.AddInParameter(dbCommand, "@ChiPhiXepHang1", SqlDbType.Decimal, ChiPhiXepHang1);
			db.AddInParameter(dbCommand, "@MaTT_ChiPhiXepHang1", SqlDbType.NVarChar, MaTT_ChiPhiXepHang1);
			db.AddInParameter(dbCommand, "@LoaiChiPhiXepHang1", SqlDbType.NVarChar, LoaiChiPhiXepHang1);
			db.AddInParameter(dbCommand, "@ChiPhiXepHang2", SqlDbType.Decimal, ChiPhiXepHang2);
			db.AddInParameter(dbCommand, "@MaTT_ChiPhiXepHang2", SqlDbType.NVarChar, MaTT_ChiPhiXepHang2);
			db.AddInParameter(dbCommand, "@LoaiChiPhiXepHang2", SqlDbType.NVarChar, LoaiChiPhiXepHang2);
			db.AddInParameter(dbCommand, "@PhiVC_DuongBo", SqlDbType.Decimal, PhiVC_DuongBo);
			db.AddInParameter(dbCommand, "@MaTT_PhiVC_DuongBo", SqlDbType.VarChar, MaTT_PhiVC_DuongBo);
			db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Decimal, PhiBaoHiem);
			db.AddInParameter(dbCommand, "@MaTT_PhiBaoHiem", SqlDbType.VarChar, MaTT_PhiBaoHiem);
			db.AddInParameter(dbCommand, "@SoTienPhiBaoHiem", SqlDbType.Decimal, SoTienPhiBaoHiem);
			db.AddInParameter(dbCommand, "@MaTT_TienPhiBaoHiem", SqlDbType.VarChar, MaTT_TienPhiBaoHiem);
			db.AddInParameter(dbCommand, "@SoTienKhauTru", SqlDbType.Decimal, SoTienKhauTru);
			db.AddInParameter(dbCommand, "@MaTT_TienKhauTru", SqlDbType.VarChar, MaTT_TienKhauTru);
			db.AddInParameter(dbCommand, "@LoaiKhauTru", SqlDbType.NVarChar, LoaiKhauTru);
			db.AddInParameter(dbCommand, "@SoTienKhac", SqlDbType.Decimal, SoTienKhac);
			db.AddInParameter(dbCommand, "@MaTT_TienKhac", SqlDbType.VarChar, MaTT_TienKhac);
			db.AddInParameter(dbCommand, "@LoaiSoTienKhac", SqlDbType.NVarChar, LoaiSoTienKhac);
			db.AddInParameter(dbCommand, "@TongTriGiaHoaDon", SqlDbType.Decimal, TongTriGiaHoaDon);
			db.AddInParameter(dbCommand, "@MaTT_TongTriGia", SqlDbType.VarChar, MaTT_TongTriGia);
			db.AddInParameter(dbCommand, "@DieuKienGiaHoaDon", SqlDbType.VarChar, DieuKienGiaHoaDon);
			db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
			db.AddInParameter(dbCommand, "@GhiChuDacBiet", SqlDbType.NVarChar, GhiChuDacBiet);
			db.AddInParameter(dbCommand, "@TongSoDongHang", SqlDbType.Decimal, TongSoDongHang);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_HoaDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HoaDon item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_HoaDon(long id, decimal soTiepNhan, DateTime ngayTiepNhan, string phanLoaiXuatNhap, string maDaiLyHQ, string soHoaDon, DateTime ngayLapHoaDon, string diaDiemLapHoaDon, string phuongThucThanhToan, string phanLoaiLienQuan, string maNguoiXNK, string tenNguoiXNK, string maBuuChinh_NguoiXNK, string diaChi_NguoiXNK, string soDienThoai_NguoiXNK, string nguoiLapHoaDon, string maNguoiGuiNhan, string tenNguoiGuiNhan, string maBuuChinhNguoiGuiNhan, string diaChiNguoiNhanGui1, string diaChiNguoiNhanGui2, string diaChiNguoiNhanGui3, string diaChiNguoiNhanGui4, string soDienThoaiNhanGui, string nuocSoTaiNhanGui, string maKyHieu, string phanLoaiVanChuyen, string tenPTVC, string soHieuChuyenDi, string maDiaDiemXepHang, string tenDiaDiemXepHang, DateTime thoiKyXephang, string maDiaDiemDoHang, string tenDiaDiemDoHang, string maDiaDiemTrungChuyen, string tenDiaDiemTrungChuyen, decimal trongLuongGross, string maDVT_TrongLuongGross, decimal trongLuongThuan, string maDVT_TrongLuongThuan, decimal tongTheTich, string maDVT_TheTich, decimal tongSoKienHang, string maDVT_KienHang, string ghiChuChuHang, string soPL, string nganHangLC, decimal triGiaFOB, string maTT_FOB, decimal soTienFOB, string maTT_TienFOB, decimal phiVanChuyen, string maTT_PhiVC, string noiThanhToanPhiVC, decimal chiPhiXepHang1, string maTT_ChiPhiXepHang1, string loaiChiPhiXepHang1, decimal chiPhiXepHang2, string maTT_ChiPhiXepHang2, string loaiChiPhiXepHang2, decimal phiVC_DuongBo, string maTT_PhiVC_DuongBo, decimal phiBaoHiem, string maTT_PhiBaoHiem, decimal soTienPhiBaoHiem, string maTT_TienPhiBaoHiem, decimal soTienKhauTru, string maTT_TienKhauTru, string loaiKhauTru, decimal soTienKhac, string maTT_TienKhac, string loaiSoTienKhac, decimal tongTriGiaHoaDon, string maTT_TongTriGia, string dieuKienGiaHoaDon, string diaDiemGiaoHang, string ghiChuDacBiet, decimal tongSoDongHang, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_HoaDon entity = new KDT_VNACC_HoaDon();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.PhanLoaiXuatNhap = phanLoaiXuatNhap;
			entity.MaDaiLyHQ = maDaiLyHQ;
			entity.SoHoaDon = soHoaDon;
			entity.NgayLapHoaDon = ngayLapHoaDon;
			entity.DiaDiemLapHoaDon = diaDiemLapHoaDon;
			entity.PhuongThucThanhToan = phuongThucThanhToan;
			entity.PhanLoaiLienQuan = phanLoaiLienQuan;
			entity.MaNguoiXNK = maNguoiXNK;
			entity.TenNguoiXNK = tenNguoiXNK;
			entity.MaBuuChinh_NguoiXNK = maBuuChinh_NguoiXNK;
			entity.DiaChi_NguoiXNK = diaChi_NguoiXNK;
			entity.SoDienThoai_NguoiXNK = soDienThoai_NguoiXNK;
			entity.NguoiLapHoaDon = nguoiLapHoaDon;
			entity.MaNguoiGuiNhan = maNguoiGuiNhan;
			entity.TenNguoiGuiNhan = tenNguoiGuiNhan;
			entity.MaBuuChinhNguoiGuiNhan = maBuuChinhNguoiGuiNhan;
			entity.DiaChiNguoiNhanGui1 = diaChiNguoiNhanGui1;
			entity.DiaChiNguoiNhanGui2 = diaChiNguoiNhanGui2;
			entity.DiaChiNguoiNhanGui3 = diaChiNguoiNhanGui3;
			entity.DiaChiNguoiNhanGui4 = diaChiNguoiNhanGui4;
			entity.SoDienThoaiNhanGui = soDienThoaiNhanGui;
			entity.NuocSoTaiNhanGui = nuocSoTaiNhanGui;
			entity.MaKyHieu = maKyHieu;
			entity.PhanLoaiVanChuyen = phanLoaiVanChuyen;
			entity.TenPTVC = tenPTVC;
			entity.SoHieuChuyenDi = soHieuChuyenDi;
			entity.MaDiaDiemXepHang = maDiaDiemXepHang;
			entity.TenDiaDiemXepHang = tenDiaDiemXepHang;
			entity.ThoiKyXephang = thoiKyXephang;
			entity.MaDiaDiemDoHang = maDiaDiemDoHang;
			entity.TenDiaDiemDoHang = tenDiaDiemDoHang;
			entity.MaDiaDiemTrungChuyen = maDiaDiemTrungChuyen;
			entity.TenDiaDiemTrungChuyen = tenDiaDiemTrungChuyen;
			entity.TrongLuongGross = trongLuongGross;
			entity.MaDVT_TrongLuongGross = maDVT_TrongLuongGross;
			entity.TrongLuongThuan = trongLuongThuan;
			entity.MaDVT_TrongLuongThuan = maDVT_TrongLuongThuan;
			entity.TongTheTich = tongTheTich;
			entity.MaDVT_TheTich = maDVT_TheTich;
			entity.TongSoKienHang = tongSoKienHang;
			entity.MaDVT_KienHang = maDVT_KienHang;
			entity.GhiChuChuHang = ghiChuChuHang;
			entity.SoPL = soPL;
			entity.NganHangLC = nganHangLC;
			entity.TriGiaFOB = triGiaFOB;
			entity.MaTT_FOB = maTT_FOB;
			entity.SoTienFOB = soTienFOB;
			entity.MaTT_TienFOB = maTT_TienFOB;
			entity.PhiVanChuyen = phiVanChuyen;
			entity.MaTT_PhiVC = maTT_PhiVC;
			entity.NoiThanhToanPhiVC = noiThanhToanPhiVC;
			entity.ChiPhiXepHang1 = chiPhiXepHang1;
			entity.MaTT_ChiPhiXepHang1 = maTT_ChiPhiXepHang1;
			entity.LoaiChiPhiXepHang1 = loaiChiPhiXepHang1;
			entity.ChiPhiXepHang2 = chiPhiXepHang2;
			entity.MaTT_ChiPhiXepHang2 = maTT_ChiPhiXepHang2;
			entity.LoaiChiPhiXepHang2 = loaiChiPhiXepHang2;
			entity.PhiVC_DuongBo = phiVC_DuongBo;
			entity.MaTT_PhiVC_DuongBo = maTT_PhiVC_DuongBo;
			entity.PhiBaoHiem = phiBaoHiem;
			entity.MaTT_PhiBaoHiem = maTT_PhiBaoHiem;
			entity.SoTienPhiBaoHiem = soTienPhiBaoHiem;
			entity.MaTT_TienPhiBaoHiem = maTT_TienPhiBaoHiem;
			entity.SoTienKhauTru = soTienKhauTru;
			entity.MaTT_TienKhauTru = maTT_TienKhauTru;
			entity.LoaiKhauTru = loaiKhauTru;
			entity.SoTienKhac = soTienKhac;
			entity.MaTT_TienKhac = maTT_TienKhac;
			entity.LoaiSoTienKhac = loaiSoTienKhac;
			entity.TongTriGiaHoaDon = tongTriGiaHoaDon;
			entity.MaTT_TongTriGia = maTT_TongTriGia;
			entity.DieuKienGiaHoaDon = dieuKienGiaHoaDon;
			entity.DiaDiemGiaoHang = diaDiemGiaoHang;
			entity.GhiChuDacBiet = ghiChuDacBiet;
			entity.TongSoDongHang = tongSoDongHang;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_HoaDon_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@PhanLoaiXuatNhap", SqlDbType.VarChar, PhanLoaiXuatNhap);
			db.AddInParameter(dbCommand, "@MaDaiLyHQ", SqlDbType.VarChar, MaDaiLyHQ);
			db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.VarChar, SoHoaDon);
			db.AddInParameter(dbCommand, "@NgayLapHoaDon", SqlDbType.DateTime, NgayLapHoaDon.Year <= 1753 ? DBNull.Value : (object) NgayLapHoaDon);
			db.AddInParameter(dbCommand, "@DiaDiemLapHoaDon", SqlDbType.NVarChar, DiaDiemLapHoaDon);
			db.AddInParameter(dbCommand, "@PhuongThucThanhToan", SqlDbType.NVarChar, PhuongThucThanhToan);
			db.AddInParameter(dbCommand, "@PhanLoaiLienQuan", SqlDbType.VarChar, PhanLoaiLienQuan);
			db.AddInParameter(dbCommand, "@MaNguoiXNK", SqlDbType.VarChar, MaNguoiXNK);
			db.AddInParameter(dbCommand, "@TenNguoiXNK", SqlDbType.NVarChar, TenNguoiXNK);
			db.AddInParameter(dbCommand, "@MaBuuChinh_NguoiXNK", SqlDbType.VarChar, MaBuuChinh_NguoiXNK);
			db.AddInParameter(dbCommand, "@DiaChi_NguoiXNK", SqlDbType.NVarChar, DiaChi_NguoiXNK);
			db.AddInParameter(dbCommand, "@SoDienThoai_NguoiXNK", SqlDbType.VarChar, SoDienThoai_NguoiXNK);
			db.AddInParameter(dbCommand, "@NguoiLapHoaDon", SqlDbType.NVarChar, NguoiLapHoaDon);
			db.AddInParameter(dbCommand, "@MaNguoiGuiNhan", SqlDbType.VarChar, MaNguoiGuiNhan);
			db.AddInParameter(dbCommand, "@TenNguoiGuiNhan", SqlDbType.NVarChar, TenNguoiGuiNhan);
			db.AddInParameter(dbCommand, "@MaBuuChinhNguoiGuiNhan", SqlDbType.VarChar, MaBuuChinhNguoiGuiNhan);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhanGui1", SqlDbType.NVarChar, DiaChiNguoiNhanGui1);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhanGui2", SqlDbType.NVarChar, DiaChiNguoiNhanGui2);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhanGui3", SqlDbType.NVarChar, DiaChiNguoiNhanGui3);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhanGui4", SqlDbType.NVarChar, DiaChiNguoiNhanGui4);
			db.AddInParameter(dbCommand, "@SoDienThoaiNhanGui", SqlDbType.VarChar, SoDienThoaiNhanGui);
			db.AddInParameter(dbCommand, "@NuocSoTaiNhanGui", SqlDbType.VarChar, NuocSoTaiNhanGui);
			db.AddInParameter(dbCommand, "@MaKyHieu", SqlDbType.NVarChar, MaKyHieu);
			db.AddInParameter(dbCommand, "@PhanLoaiVanChuyen", SqlDbType.VarChar, PhanLoaiVanChuyen);
			db.AddInParameter(dbCommand, "@TenPTVC", SqlDbType.NVarChar, TenPTVC);
			db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.VarChar, SoHieuChuyenDi);
			db.AddInParameter(dbCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, MaDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, TenDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@ThoiKyXephang", SqlDbType.DateTime, ThoiKyXephang.Year <= 1753 ? DBNull.Value : (object) ThoiKyXephang);
			db.AddInParameter(dbCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, MaDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemDoHang", SqlDbType.NVarChar, TenDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@MaDiaDiemTrungChuyen", SqlDbType.NVarChar, MaDiaDiemTrungChuyen);
			db.AddInParameter(dbCommand, "@TenDiaDiemTrungChuyen", SqlDbType.NVarChar, TenDiaDiemTrungChuyen);
			db.AddInParameter(dbCommand, "@TrongLuongGross", SqlDbType.Decimal, TrongLuongGross);
			db.AddInParameter(dbCommand, "@MaDVT_TrongLuongGross", SqlDbType.VarChar, MaDVT_TrongLuongGross);
			db.AddInParameter(dbCommand, "@TrongLuongThuan", SqlDbType.Decimal, TrongLuongThuan);
			db.AddInParameter(dbCommand, "@MaDVT_TrongLuongThuan", SqlDbType.NVarChar, MaDVT_TrongLuongThuan);
			db.AddInParameter(dbCommand, "@TongTheTich", SqlDbType.Decimal, TongTheTich);
			db.AddInParameter(dbCommand, "@MaDVT_TheTich", SqlDbType.NVarChar, MaDVT_TheTich);
			db.AddInParameter(dbCommand, "@TongSoKienHang", SqlDbType.Decimal, TongSoKienHang);
			db.AddInParameter(dbCommand, "@MaDVT_KienHang", SqlDbType.VarChar, MaDVT_KienHang);
			db.AddInParameter(dbCommand, "@GhiChuChuHang", SqlDbType.NVarChar, GhiChuChuHang);
			db.AddInParameter(dbCommand, "@SoPL", SqlDbType.VarChar, SoPL);
			db.AddInParameter(dbCommand, "@NganHangLC", SqlDbType.NVarChar, NganHangLC);
			db.AddInParameter(dbCommand, "@TriGiaFOB", SqlDbType.Decimal, TriGiaFOB);
			db.AddInParameter(dbCommand, "@MaTT_FOB", SqlDbType.VarChar, MaTT_FOB);
			db.AddInParameter(dbCommand, "@SoTienFOB", SqlDbType.Decimal, SoTienFOB);
			db.AddInParameter(dbCommand, "@MaTT_TienFOB", SqlDbType.VarChar, MaTT_TienFOB);
			db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Decimal, PhiVanChuyen);
			db.AddInParameter(dbCommand, "@MaTT_PhiVC", SqlDbType.VarChar, MaTT_PhiVC);
			db.AddInParameter(dbCommand, "@NoiThanhToanPhiVC", SqlDbType.NVarChar, NoiThanhToanPhiVC);
			db.AddInParameter(dbCommand, "@ChiPhiXepHang1", SqlDbType.Decimal, ChiPhiXepHang1);
			db.AddInParameter(dbCommand, "@MaTT_ChiPhiXepHang1", SqlDbType.NVarChar, MaTT_ChiPhiXepHang1);
			db.AddInParameter(dbCommand, "@LoaiChiPhiXepHang1", SqlDbType.NVarChar, LoaiChiPhiXepHang1);
			db.AddInParameter(dbCommand, "@ChiPhiXepHang2", SqlDbType.Decimal, ChiPhiXepHang2);
			db.AddInParameter(dbCommand, "@MaTT_ChiPhiXepHang2", SqlDbType.NVarChar, MaTT_ChiPhiXepHang2);
			db.AddInParameter(dbCommand, "@LoaiChiPhiXepHang2", SqlDbType.NVarChar, LoaiChiPhiXepHang2);
			db.AddInParameter(dbCommand, "@PhiVC_DuongBo", SqlDbType.Decimal, PhiVC_DuongBo);
			db.AddInParameter(dbCommand, "@MaTT_PhiVC_DuongBo", SqlDbType.VarChar, MaTT_PhiVC_DuongBo);
			db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Decimal, PhiBaoHiem);
			db.AddInParameter(dbCommand, "@MaTT_PhiBaoHiem", SqlDbType.VarChar, MaTT_PhiBaoHiem);
			db.AddInParameter(dbCommand, "@SoTienPhiBaoHiem", SqlDbType.Decimal, SoTienPhiBaoHiem);
			db.AddInParameter(dbCommand, "@MaTT_TienPhiBaoHiem", SqlDbType.VarChar, MaTT_TienPhiBaoHiem);
			db.AddInParameter(dbCommand, "@SoTienKhauTru", SqlDbType.Decimal, SoTienKhauTru);
			db.AddInParameter(dbCommand, "@MaTT_TienKhauTru", SqlDbType.VarChar, MaTT_TienKhauTru);
			db.AddInParameter(dbCommand, "@LoaiKhauTru", SqlDbType.NVarChar, LoaiKhauTru);
			db.AddInParameter(dbCommand, "@SoTienKhac", SqlDbType.Decimal, SoTienKhac);
			db.AddInParameter(dbCommand, "@MaTT_TienKhac", SqlDbType.VarChar, MaTT_TienKhac);
			db.AddInParameter(dbCommand, "@LoaiSoTienKhac", SqlDbType.NVarChar, LoaiSoTienKhac);
			db.AddInParameter(dbCommand, "@TongTriGiaHoaDon", SqlDbType.Decimal, TongTriGiaHoaDon);
			db.AddInParameter(dbCommand, "@MaTT_TongTriGia", SqlDbType.VarChar, MaTT_TongTriGia);
			db.AddInParameter(dbCommand, "@DieuKienGiaHoaDon", SqlDbType.VarChar, DieuKienGiaHoaDon);
			db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
			db.AddInParameter(dbCommand, "@GhiChuDacBiet", SqlDbType.NVarChar, GhiChuDacBiet);
			db.AddInParameter(dbCommand, "@TongSoDongHang", SqlDbType.Decimal, TongSoDongHang);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_HoaDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HoaDon item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_HoaDon(long id, decimal soTiepNhan, DateTime ngayTiepNhan, string phanLoaiXuatNhap, string maDaiLyHQ, string soHoaDon, DateTime ngayLapHoaDon, string diaDiemLapHoaDon, string phuongThucThanhToan, string phanLoaiLienQuan, string maNguoiXNK, string tenNguoiXNK, string maBuuChinh_NguoiXNK, string diaChi_NguoiXNK, string soDienThoai_NguoiXNK, string nguoiLapHoaDon, string maNguoiGuiNhan, string tenNguoiGuiNhan, string maBuuChinhNguoiGuiNhan, string diaChiNguoiNhanGui1, string diaChiNguoiNhanGui2, string diaChiNguoiNhanGui3, string diaChiNguoiNhanGui4, string soDienThoaiNhanGui, string nuocSoTaiNhanGui, string maKyHieu, string phanLoaiVanChuyen, string tenPTVC, string soHieuChuyenDi, string maDiaDiemXepHang, string tenDiaDiemXepHang, DateTime thoiKyXephang, string maDiaDiemDoHang, string tenDiaDiemDoHang, string maDiaDiemTrungChuyen, string tenDiaDiemTrungChuyen, decimal trongLuongGross, string maDVT_TrongLuongGross, decimal trongLuongThuan, string maDVT_TrongLuongThuan, decimal tongTheTich, string maDVT_TheTich, decimal tongSoKienHang, string maDVT_KienHang, string ghiChuChuHang, string soPL, string nganHangLC, decimal triGiaFOB, string maTT_FOB, decimal soTienFOB, string maTT_TienFOB, decimal phiVanChuyen, string maTT_PhiVC, string noiThanhToanPhiVC, decimal chiPhiXepHang1, string maTT_ChiPhiXepHang1, string loaiChiPhiXepHang1, decimal chiPhiXepHang2, string maTT_ChiPhiXepHang2, string loaiChiPhiXepHang2, decimal phiVC_DuongBo, string maTT_PhiVC_DuongBo, decimal phiBaoHiem, string maTT_PhiBaoHiem, decimal soTienPhiBaoHiem, string maTT_TienPhiBaoHiem, decimal soTienKhauTru, string maTT_TienKhauTru, string loaiKhauTru, decimal soTienKhac, string maTT_TienKhac, string loaiSoTienKhac, decimal tongTriGiaHoaDon, string maTT_TongTriGia, string dieuKienGiaHoaDon, string diaDiemGiaoHang, string ghiChuDacBiet, decimal tongSoDongHang, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_HoaDon entity = new KDT_VNACC_HoaDon();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.PhanLoaiXuatNhap = phanLoaiXuatNhap;
			entity.MaDaiLyHQ = maDaiLyHQ;
			entity.SoHoaDon = soHoaDon;
			entity.NgayLapHoaDon = ngayLapHoaDon;
			entity.DiaDiemLapHoaDon = diaDiemLapHoaDon;
			entity.PhuongThucThanhToan = phuongThucThanhToan;
			entity.PhanLoaiLienQuan = phanLoaiLienQuan;
			entity.MaNguoiXNK = maNguoiXNK;
			entity.TenNguoiXNK = tenNguoiXNK;
			entity.MaBuuChinh_NguoiXNK = maBuuChinh_NguoiXNK;
			entity.DiaChi_NguoiXNK = diaChi_NguoiXNK;
			entity.SoDienThoai_NguoiXNK = soDienThoai_NguoiXNK;
			entity.NguoiLapHoaDon = nguoiLapHoaDon;
			entity.MaNguoiGuiNhan = maNguoiGuiNhan;
			entity.TenNguoiGuiNhan = tenNguoiGuiNhan;
			entity.MaBuuChinhNguoiGuiNhan = maBuuChinhNguoiGuiNhan;
			entity.DiaChiNguoiNhanGui1 = diaChiNguoiNhanGui1;
			entity.DiaChiNguoiNhanGui2 = diaChiNguoiNhanGui2;
			entity.DiaChiNguoiNhanGui3 = diaChiNguoiNhanGui3;
			entity.DiaChiNguoiNhanGui4 = diaChiNguoiNhanGui4;
			entity.SoDienThoaiNhanGui = soDienThoaiNhanGui;
			entity.NuocSoTaiNhanGui = nuocSoTaiNhanGui;
			entity.MaKyHieu = maKyHieu;
			entity.PhanLoaiVanChuyen = phanLoaiVanChuyen;
			entity.TenPTVC = tenPTVC;
			entity.SoHieuChuyenDi = soHieuChuyenDi;
			entity.MaDiaDiemXepHang = maDiaDiemXepHang;
			entity.TenDiaDiemXepHang = tenDiaDiemXepHang;
			entity.ThoiKyXephang = thoiKyXephang;
			entity.MaDiaDiemDoHang = maDiaDiemDoHang;
			entity.TenDiaDiemDoHang = tenDiaDiemDoHang;
			entity.MaDiaDiemTrungChuyen = maDiaDiemTrungChuyen;
			entity.TenDiaDiemTrungChuyen = tenDiaDiemTrungChuyen;
			entity.TrongLuongGross = trongLuongGross;
			entity.MaDVT_TrongLuongGross = maDVT_TrongLuongGross;
			entity.TrongLuongThuan = trongLuongThuan;
			entity.MaDVT_TrongLuongThuan = maDVT_TrongLuongThuan;
			entity.TongTheTich = tongTheTich;
			entity.MaDVT_TheTich = maDVT_TheTich;
			entity.TongSoKienHang = tongSoKienHang;
			entity.MaDVT_KienHang = maDVT_KienHang;
			entity.GhiChuChuHang = ghiChuChuHang;
			entity.SoPL = soPL;
			entity.NganHangLC = nganHangLC;
			entity.TriGiaFOB = triGiaFOB;
			entity.MaTT_FOB = maTT_FOB;
			entity.SoTienFOB = soTienFOB;
			entity.MaTT_TienFOB = maTT_TienFOB;
			entity.PhiVanChuyen = phiVanChuyen;
			entity.MaTT_PhiVC = maTT_PhiVC;
			entity.NoiThanhToanPhiVC = noiThanhToanPhiVC;
			entity.ChiPhiXepHang1 = chiPhiXepHang1;
			entity.MaTT_ChiPhiXepHang1 = maTT_ChiPhiXepHang1;
			entity.LoaiChiPhiXepHang1 = loaiChiPhiXepHang1;
			entity.ChiPhiXepHang2 = chiPhiXepHang2;
			entity.MaTT_ChiPhiXepHang2 = maTT_ChiPhiXepHang2;
			entity.LoaiChiPhiXepHang2 = loaiChiPhiXepHang2;
			entity.PhiVC_DuongBo = phiVC_DuongBo;
			entity.MaTT_PhiVC_DuongBo = maTT_PhiVC_DuongBo;
			entity.PhiBaoHiem = phiBaoHiem;
			entity.MaTT_PhiBaoHiem = maTT_PhiBaoHiem;
			entity.SoTienPhiBaoHiem = soTienPhiBaoHiem;
			entity.MaTT_TienPhiBaoHiem = maTT_TienPhiBaoHiem;
			entity.SoTienKhauTru = soTienKhauTru;
			entity.MaTT_TienKhauTru = maTT_TienKhauTru;
			entity.LoaiKhauTru = loaiKhauTru;
			entity.SoTienKhac = soTienKhac;
			entity.MaTT_TienKhac = maTT_TienKhac;
			entity.LoaiSoTienKhac = loaiSoTienKhac;
			entity.TongTriGiaHoaDon = tongTriGiaHoaDon;
			entity.MaTT_TongTriGia = maTT_TongTriGia;
			entity.DieuKienGiaHoaDon = dieuKienGiaHoaDon;
			entity.DiaDiemGiaoHang = diaDiemGiaoHang;
			entity.GhiChuDacBiet = ghiChuDacBiet;
			entity.TongSoDongHang = tongSoDongHang;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HoaDon_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@PhanLoaiXuatNhap", SqlDbType.VarChar, PhanLoaiXuatNhap);
			db.AddInParameter(dbCommand, "@MaDaiLyHQ", SqlDbType.VarChar, MaDaiLyHQ);
			db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.VarChar, SoHoaDon);
			db.AddInParameter(dbCommand, "@NgayLapHoaDon", SqlDbType.DateTime, NgayLapHoaDon.Year <= 1753 ? DBNull.Value : (object) NgayLapHoaDon);
			db.AddInParameter(dbCommand, "@DiaDiemLapHoaDon", SqlDbType.NVarChar, DiaDiemLapHoaDon);
			db.AddInParameter(dbCommand, "@PhuongThucThanhToan", SqlDbType.NVarChar, PhuongThucThanhToan);
			db.AddInParameter(dbCommand, "@PhanLoaiLienQuan", SqlDbType.VarChar, PhanLoaiLienQuan);
			db.AddInParameter(dbCommand, "@MaNguoiXNK", SqlDbType.VarChar, MaNguoiXNK);
			db.AddInParameter(dbCommand, "@TenNguoiXNK", SqlDbType.NVarChar, TenNguoiXNK);
			db.AddInParameter(dbCommand, "@MaBuuChinh_NguoiXNK", SqlDbType.VarChar, MaBuuChinh_NguoiXNK);
			db.AddInParameter(dbCommand, "@DiaChi_NguoiXNK", SqlDbType.NVarChar, DiaChi_NguoiXNK);
			db.AddInParameter(dbCommand, "@SoDienThoai_NguoiXNK", SqlDbType.VarChar, SoDienThoai_NguoiXNK);
			db.AddInParameter(dbCommand, "@NguoiLapHoaDon", SqlDbType.NVarChar, NguoiLapHoaDon);
			db.AddInParameter(dbCommand, "@MaNguoiGuiNhan", SqlDbType.VarChar, MaNguoiGuiNhan);
			db.AddInParameter(dbCommand, "@TenNguoiGuiNhan", SqlDbType.NVarChar, TenNguoiGuiNhan);
			db.AddInParameter(dbCommand, "@MaBuuChinhNguoiGuiNhan", SqlDbType.VarChar, MaBuuChinhNguoiGuiNhan);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhanGui1", SqlDbType.NVarChar, DiaChiNguoiNhanGui1);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhanGui2", SqlDbType.NVarChar, DiaChiNguoiNhanGui2);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhanGui3", SqlDbType.NVarChar, DiaChiNguoiNhanGui3);
			db.AddInParameter(dbCommand, "@DiaChiNguoiNhanGui4", SqlDbType.NVarChar, DiaChiNguoiNhanGui4);
			db.AddInParameter(dbCommand, "@SoDienThoaiNhanGui", SqlDbType.VarChar, SoDienThoaiNhanGui);
			db.AddInParameter(dbCommand, "@NuocSoTaiNhanGui", SqlDbType.VarChar, NuocSoTaiNhanGui);
			db.AddInParameter(dbCommand, "@MaKyHieu", SqlDbType.NVarChar, MaKyHieu);
			db.AddInParameter(dbCommand, "@PhanLoaiVanChuyen", SqlDbType.VarChar, PhanLoaiVanChuyen);
			db.AddInParameter(dbCommand, "@TenPTVC", SqlDbType.NVarChar, TenPTVC);
			db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.VarChar, SoHieuChuyenDi);
			db.AddInParameter(dbCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, MaDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemXepHang", SqlDbType.NVarChar, TenDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@ThoiKyXephang", SqlDbType.DateTime, ThoiKyXephang.Year <= 1753 ? DBNull.Value : (object) ThoiKyXephang);
			db.AddInParameter(dbCommand, "@MaDiaDiemDoHang", SqlDbType.VarChar, MaDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@TenDiaDiemDoHang", SqlDbType.NVarChar, TenDiaDiemDoHang);
			db.AddInParameter(dbCommand, "@MaDiaDiemTrungChuyen", SqlDbType.NVarChar, MaDiaDiemTrungChuyen);
			db.AddInParameter(dbCommand, "@TenDiaDiemTrungChuyen", SqlDbType.NVarChar, TenDiaDiemTrungChuyen);
			db.AddInParameter(dbCommand, "@TrongLuongGross", SqlDbType.Decimal, TrongLuongGross);
			db.AddInParameter(dbCommand, "@MaDVT_TrongLuongGross", SqlDbType.VarChar, MaDVT_TrongLuongGross);
			db.AddInParameter(dbCommand, "@TrongLuongThuan", SqlDbType.Decimal, TrongLuongThuan);
			db.AddInParameter(dbCommand, "@MaDVT_TrongLuongThuan", SqlDbType.NVarChar, MaDVT_TrongLuongThuan);
			db.AddInParameter(dbCommand, "@TongTheTich", SqlDbType.Decimal, TongTheTich);
			db.AddInParameter(dbCommand, "@MaDVT_TheTich", SqlDbType.NVarChar, MaDVT_TheTich);
			db.AddInParameter(dbCommand, "@TongSoKienHang", SqlDbType.Decimal, TongSoKienHang);
			db.AddInParameter(dbCommand, "@MaDVT_KienHang", SqlDbType.VarChar, MaDVT_KienHang);
			db.AddInParameter(dbCommand, "@GhiChuChuHang", SqlDbType.NVarChar, GhiChuChuHang);
			db.AddInParameter(dbCommand, "@SoPL", SqlDbType.VarChar, SoPL);
			db.AddInParameter(dbCommand, "@NganHangLC", SqlDbType.NVarChar, NganHangLC);
			db.AddInParameter(dbCommand, "@TriGiaFOB", SqlDbType.Decimal, TriGiaFOB);
			db.AddInParameter(dbCommand, "@MaTT_FOB", SqlDbType.VarChar, MaTT_FOB);
			db.AddInParameter(dbCommand, "@SoTienFOB", SqlDbType.Decimal, SoTienFOB);
			db.AddInParameter(dbCommand, "@MaTT_TienFOB", SqlDbType.VarChar, MaTT_TienFOB);
			db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Decimal, PhiVanChuyen);
			db.AddInParameter(dbCommand, "@MaTT_PhiVC", SqlDbType.VarChar, MaTT_PhiVC);
			db.AddInParameter(dbCommand, "@NoiThanhToanPhiVC", SqlDbType.NVarChar, NoiThanhToanPhiVC);
			db.AddInParameter(dbCommand, "@ChiPhiXepHang1", SqlDbType.Decimal, ChiPhiXepHang1);
			db.AddInParameter(dbCommand, "@MaTT_ChiPhiXepHang1", SqlDbType.NVarChar, MaTT_ChiPhiXepHang1);
			db.AddInParameter(dbCommand, "@LoaiChiPhiXepHang1", SqlDbType.NVarChar, LoaiChiPhiXepHang1);
			db.AddInParameter(dbCommand, "@ChiPhiXepHang2", SqlDbType.Decimal, ChiPhiXepHang2);
			db.AddInParameter(dbCommand, "@MaTT_ChiPhiXepHang2", SqlDbType.NVarChar, MaTT_ChiPhiXepHang2);
			db.AddInParameter(dbCommand, "@LoaiChiPhiXepHang2", SqlDbType.NVarChar, LoaiChiPhiXepHang2);
			db.AddInParameter(dbCommand, "@PhiVC_DuongBo", SqlDbType.Decimal, PhiVC_DuongBo);
			db.AddInParameter(dbCommand, "@MaTT_PhiVC_DuongBo", SqlDbType.VarChar, MaTT_PhiVC_DuongBo);
			db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Decimal, PhiBaoHiem);
			db.AddInParameter(dbCommand, "@MaTT_PhiBaoHiem", SqlDbType.VarChar, MaTT_PhiBaoHiem);
			db.AddInParameter(dbCommand, "@SoTienPhiBaoHiem", SqlDbType.Decimal, SoTienPhiBaoHiem);
			db.AddInParameter(dbCommand, "@MaTT_TienPhiBaoHiem", SqlDbType.VarChar, MaTT_TienPhiBaoHiem);
			db.AddInParameter(dbCommand, "@SoTienKhauTru", SqlDbType.Decimal, SoTienKhauTru);
			db.AddInParameter(dbCommand, "@MaTT_TienKhauTru", SqlDbType.VarChar, MaTT_TienKhauTru);
			db.AddInParameter(dbCommand, "@LoaiKhauTru", SqlDbType.NVarChar, LoaiKhauTru);
			db.AddInParameter(dbCommand, "@SoTienKhac", SqlDbType.Decimal, SoTienKhac);
			db.AddInParameter(dbCommand, "@MaTT_TienKhac", SqlDbType.VarChar, MaTT_TienKhac);
			db.AddInParameter(dbCommand, "@LoaiSoTienKhac", SqlDbType.NVarChar, LoaiSoTienKhac);
			db.AddInParameter(dbCommand, "@TongTriGiaHoaDon", SqlDbType.Decimal, TongTriGiaHoaDon);
			db.AddInParameter(dbCommand, "@MaTT_TongTriGia", SqlDbType.VarChar, MaTT_TongTriGia);
			db.AddInParameter(dbCommand, "@DieuKienGiaHoaDon", SqlDbType.VarChar, DieuKienGiaHoaDon);
			db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
			db.AddInParameter(dbCommand, "@GhiChuDacBiet", SqlDbType.NVarChar, GhiChuDacBiet);
			db.AddInParameter(dbCommand, "@TongSoDongHang", SqlDbType.Decimal, TongSoDongHang);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_HoaDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HoaDon item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_HoaDon(long id)
		{
			KDT_VNACC_HoaDon entity = new KDT_VNACC_HoaDon();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HoaDon_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HoaDon_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_HoaDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HoaDon item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}