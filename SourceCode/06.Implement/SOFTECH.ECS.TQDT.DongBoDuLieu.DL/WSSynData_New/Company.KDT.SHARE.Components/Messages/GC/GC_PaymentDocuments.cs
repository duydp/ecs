﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Người khai HQ
    /// </summary>
    public class GC_PaymentDocuments 
    {

        [XmlElement("PaymentDocument")]
        public GC_PaymentDocument PaymentDocument { get; set; }
    

    }
    public class GC_PaymentDocument
    {

        [XmlElement("reference")]
        public string Reference { get; set; }

        [XmlElement("issue")]
        public string Issue { get; set; }

        [XmlElement("type")]
        public string Type { get; set; }

        [XmlElement("issueLocation")]
        public string IssueLocation { get; set; }

        [XmlElement("quantity")]
        public string Quantity { get; set; }


    }
}
