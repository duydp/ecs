﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;


namespace KeyServer
{
    /// <summary>
    /// XML thông tin khách hàng ECS
    /// </summary>
    [XmlRoot("SOFTECH_ECS")]
    public class ECS_InforCustomer
    {
        /// <summary>
        /// Loại sản phẩm
        /// </summary>
        [XmlElement("productID")]
        public string Product_ID { set; get; }
        /// <summary>
        /// Mã máy
        /// </summary>
        [XmlElement("machineCode")]
        public string MachineCode { set; get; }
        /// <summary>
        /// RecordCount
        /// </summary>
        [XmlElement("recordCount")]
        public int RecordCount { set; get; }
        /// <summary>
        /// Mã doanh nghiệp
        /// </summary>
        [XmlElement("id")]
        public string Id { set; get; }
        /// <summary>
        /// Tên doanh nghiệp
        /// </summary>
        [XmlElement("name")]
        public string Name { set; get; }
        /// <summary>
        /// Địa chỉ doanh nghiệp
        /// </summary>
        [XmlElement("add")]
        public string Address { set; get; }
        /// <summary>
        /// số điện thoại doanh nghiệp
        /// </summary>
        [XmlElement("phone")]
        public string Phone { set; get; }
        /// <summary>
        /// email doanh nghiệp
        /// </summary>
        [XmlElement("email")]
        public string Email { set; get; }
        /// <summary>
        /// người liên hệ doanh nghiệp
        /// </summary>
        [XmlElement("contact")]
        public string Contact_Person { set; get; }
        /// <summary>
        /// Mã hải quan khai báo
        /// </summary>
        [XmlElement("id_customs")]
        public string Id_Customs { set; get; }
        /// <summary>
        /// địa chỉ hải quan khai báo
        /// </summary>
        [XmlElement("ip_customs")]
        public string IP_Customs { set; get; }
        /// <summary>
        /// Tên server đang sử dụng
        /// </summary>
        [XmlElement("servername")]
        public string ServerName { set; get; }
        /// <summary>
        /// Tên database đang sử dụng
        /// </summary>
        [XmlElement("dataName")]
        public string Data_Name { set; get; }
        /// <summary>
        /// user đăng nhập vào server
        /// </summary>
        [XmlElement("dataUser")]
        public string Data_User { set; get; }
        /// <summary>
        /// pass đăng nhập vào server
        /// </summary>
        [XmlElement("dataPw")]
        public string Data_Pass { set; get; }
        /// <summary>
        /// Phiên bản server
        /// </summary>
        [XmlElement("dataVersion")]
        public string Data_Version { set; get; }
        /// <summary>
        /// Phiên bản phần mềm
        /// </summary>
        [XmlElement("appVersion")]
        public string App_Version { set; get; }
        /// <summary>
        /// Ngày update
        /// </summary>
        [XmlElement("lastCheck")]
        public DateTime LastCheck { set; get; }
        /// <summary>
        /// dự phòng
        /// </summary>
        [XmlElement("temp1")]
        public string temp1 { set; get; }
        
    }
}
