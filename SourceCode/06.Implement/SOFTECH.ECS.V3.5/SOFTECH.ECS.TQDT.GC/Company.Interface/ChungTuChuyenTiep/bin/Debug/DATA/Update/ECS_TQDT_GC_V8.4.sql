/*
Run this script on:

        192.168.72.100.ECS_TQDT_GC_V4    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_KD_V4

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 03/21/2013 10:54:41 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[t_KDT_GiayPhep]'
GO
ALTER TABLE [dbo].[t_KDT_GiayPhep] ADD
[LoaiGiayPhep] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HinhThucTruLui] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_Insert]
	@SoGiayPhep varchar(100),
	@NgayGiayPhep datetime,
	@NgayHetHan datetime,
	@NguoiCap nvarchar(100),
	@NoiCap nvarchar(100),
	@MaDonViDuocCap varchar(50),
	@TenDonViDuocCap nvarchar(100),
	@MaCoQuanCap varchar(50),
	@TenQuanCap nvarchar(100),
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@LoaiGiayPhep varchar(10),
	@HinhThucTruLui nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GiayPhep]
(
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[LoaiGiayPhep],
	[HinhThucTruLui]
)
VALUES 
(
	@SoGiayPhep,
	@NgayGiayPhep,
	@NgayHetHan,
	@NguoiCap,
	@NoiCap,
	@MaDonViDuocCap,
	@TenDonViDuocCap,
	@MaCoQuanCap,
	@TenQuanCap,
	@ThongTinKhac,
	@MaDoanhNghiep,
	@TKMD_ID,
	@GuidStr,
	@LoaiKB,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThai,
	@NamTiepNhan,
	@LoaiGiayPhep,
	@HinhThucTruLui
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_Update]
	@ID bigint,
	@SoGiayPhep varchar(100),
	@NgayGiayPhep datetime,
	@NgayHetHan datetime,
	@NguoiCap nvarchar(100),
	@NoiCap nvarchar(100),
	@MaDonViDuocCap varchar(50),
	@TenDonViDuocCap nvarchar(100),
	@MaCoQuanCap varchar(50),
	@TenQuanCap nvarchar(100),
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@LoaiGiayPhep varchar(10),
	@HinhThucTruLui nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_GiayPhep]
SET
	[SoGiayPhep] = @SoGiayPhep,
	[NgayGiayPhep] = @NgayGiayPhep,
	[NgayHetHan] = @NgayHetHan,
	[NguoiCap] = @NguoiCap,
	[NoiCap] = @NoiCap,
	[MaDonViDuocCap] = @MaDonViDuocCap,
	[TenDonViDuocCap] = @TenDonViDuocCap,
	[MaCoQuanCap] = @MaCoQuanCap,
	[TenQuanCap] = @TenQuanCap,
	[ThongTinKhac] = @ThongTinKhac,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[GuidStr] = @GuidStr,
	[LoaiKB] = @LoaiKB,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThai] = @TrangThai,
	[NamTiepNhan] = @NamTiepNhan,
	[LoaiGiayPhep] = @LoaiGiayPhep,
	[HinhThucTruLui] = @HinhThucTruLui
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_InsertUpdate]
	@ID bigint,
	@SoGiayPhep varchar(100),
	@NgayGiayPhep datetime,
	@NgayHetHan datetime,
	@NguoiCap nvarchar(100),
	@NoiCap nvarchar(100),
	@MaDonViDuocCap varchar(50),
	@TenDonViDuocCap nvarchar(100),
	@MaCoQuanCap varchar(50),
	@TenQuanCap nvarchar(100),
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@LoaiGiayPhep varchar(10),
	@HinhThucTruLui nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GiayPhep] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GiayPhep] 
		SET
			[SoGiayPhep] = @SoGiayPhep,
			[NgayGiayPhep] = @NgayGiayPhep,
			[NgayHetHan] = @NgayHetHan,
			[NguoiCap] = @NguoiCap,
			[NoiCap] = @NoiCap,
			[MaDonViDuocCap] = @MaDonViDuocCap,
			[TenDonViDuocCap] = @TenDonViDuocCap,
			[MaCoQuanCap] = @MaCoQuanCap,
			[TenQuanCap] = @TenQuanCap,
			[ThongTinKhac] = @ThongTinKhac,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[GuidStr] = @GuidStr,
			[LoaiKB] = @LoaiKB,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThai] = @TrangThai,
			[NamTiepNhan] = @NamTiepNhan,
			[LoaiGiayPhep] = @LoaiGiayPhep,
			[HinhThucTruLui] = @HinhThucTruLui
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GiayPhep]
		(
			[SoGiayPhep],
			[NgayGiayPhep],
			[NgayHetHan],
			[NguoiCap],
			[NoiCap],
			[MaDonViDuocCap],
			[TenDonViDuocCap],
			[MaCoQuanCap],
			[TenQuanCap],
			[ThongTinKhac],
			[MaDoanhNghiep],
			[TKMD_ID],
			[GuidStr],
			[LoaiKB],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThai],
			[NamTiepNhan],
			[LoaiGiayPhep],
			[HinhThucTruLui]
		)
		VALUES 
		(
			@SoGiayPhep,
			@NgayGiayPhep,
			@NgayHetHan,
			@NguoiCap,
			@NoiCap,
			@MaDonViDuocCap,
			@TenDonViDuocCap,
			@MaCoQuanCap,
			@TenQuanCap,
			@ThongTinKhac,
			@MaDoanhNghiep,
			@TKMD_ID,
			@GuidStr,
			@LoaiKB,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThai,
			@NamTiepNhan,
			@LoaiGiayPhep,
			@HinhThucTruLui
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GiayPhep]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_DeleteBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_GiayPhep]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[LoaiGiayPhep],
	[HinhThucTruLui]
FROM
	[dbo].[t_KDT_GiayPhep]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_SelectBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[LoaiGiayPhep],
	[HinhThucTruLui]
FROM
	[dbo].[t_KDT_GiayPhep]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[LoaiGiayPhep],
	[HinhThucTruLui]
FROM
	[dbo].[t_KDT_GiayPhep]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GiayPhep] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[LoaiGiayPhep],
	[HinhThucTruLui]
FROM [dbo].[t_KDT_GiayPhep] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '8.4'
      ,[Date] = getdate()
      ,[Notes] = ''
      GO

