-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]
	@LoaiChungTu varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@TieuDe nvarchar(210),
	@SoToKhai numeric(12, 0),
	@GhiChu nvarchar(996),
	@PhanLoaiThuTucKhaiBao varchar(3),
	@SoDienThoaiNguoiKhaiBao varchar(20),
	@SoQuanLyTrongNoiBoDoanhNghiep varchar(20),
	@MaKetQuaXuLy varchar(75),
	@TenThuTucKhaiBao nvarchar(210),
	@NgayKhaiBao datetime,
	@TrangThaiKhaiBao varchar(30),
	@NgaySuaCuoiCung datetime,
	@TenNguoiKhaiBao nvarchar(300),
	@DiaChiNguoiKhaiBao nvarchar(300),
	@SoDeLayTepDinhKem numeric(16, 0),
	@NgayHoanThanhKiemTraHoSo datetime,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@TongDungLuong numeric(18, 2),
	@PhanLuong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMD_ID bigint,
	@NgayBatDau datetime,
	@NgayCapNhatCuoiCung datetime,
	@CanCuPhapLenh nvarchar(996),
	@CoBaoXoa varchar(1),
	@GhiChuHaiQuan nvarchar(996),
	@HinhThucTimKiem varchar(1),
	@LyDoChinhSua nvarchar(300),
	@MaDiaDiemDen varchar(6),
	@MaNhaVanChuyen varchar(6),
	@MaPhanLoaiDangKy varchar(1),
	@MaPhanLoaiXuLy varchar(1),
	@MaThongTinXuat varchar(7),
	@NgayCapPhep datetime,
	@NgayThongBao datetime,
	@NguoiGui varchar(5),
	@NoiDungChinhSua nvarchar(300),
	@PhuongTienVanChuyen varchar(12),
	@SoChuyenDiBien varchar(10),
	@SoSeri varchar(3),
	@SoTiepNhanToKhaiPhoThong numeric(12, 0),
	@TrangThaiXuLyHaiQuan varchar(1),
	@NguoiCapNhatCuoiCung varchar(8),
	@NhomXuLyHoSoID int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ChungTuDinhKem]
(
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
)
VALUES 
(
	@LoaiChungTu,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@TrangThaiXuLy,
	@CoQuanHaiQuan,
	@NhomXuLyHoSo,
	@TieuDe,
	@SoToKhai,
	@GhiChu,
	@PhanLoaiThuTucKhaiBao,
	@SoDienThoaiNguoiKhaiBao,
	@SoQuanLyTrongNoiBoDoanhNghiep,
	@MaKetQuaXuLy,
	@TenThuTucKhaiBao,
	@NgayKhaiBao,
	@TrangThaiKhaiBao,
	@NgaySuaCuoiCung,
	@TenNguoiKhaiBao,
	@DiaChiNguoiKhaiBao,
	@SoDeLayTepDinhKem,
	@NgayHoanThanhKiemTraHoSo,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TongDungLuong,
	@PhanLuong,
	@HuongDan,
	@TKMD_ID,
	@NgayBatDau,
	@NgayCapNhatCuoiCung,
	@CanCuPhapLenh,
	@CoBaoXoa,
	@GhiChuHaiQuan,
	@HinhThucTimKiem,
	@LyDoChinhSua,
	@MaDiaDiemDen,
	@MaNhaVanChuyen,
	@MaPhanLoaiDangKy,
	@MaPhanLoaiXuLy,
	@MaThongTinXuat,
	@NgayCapPhep,
	@NgayThongBao,
	@NguoiGui,
	@NoiDungChinhSua,
	@PhuongTienVanChuyen,
	@SoChuyenDiBien,
	@SoSeri,
	@SoTiepNhanToKhaiPhoThong,
	@TrangThaiXuLyHaiQuan,
	@NguoiCapNhatCuoiCung,
	@NhomXuLyHoSoID
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]
	@ID bigint,
	@LoaiChungTu varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@TieuDe nvarchar(210),
	@SoToKhai numeric(12, 0),
	@GhiChu nvarchar(996),
	@PhanLoaiThuTucKhaiBao varchar(3),
	@SoDienThoaiNguoiKhaiBao varchar(20),
	@SoQuanLyTrongNoiBoDoanhNghiep varchar(20),
	@MaKetQuaXuLy varchar(75),
	@TenThuTucKhaiBao nvarchar(210),
	@NgayKhaiBao datetime,
	@TrangThaiKhaiBao varchar(30),
	@NgaySuaCuoiCung datetime,
	@TenNguoiKhaiBao nvarchar(300),
	@DiaChiNguoiKhaiBao nvarchar(300),
	@SoDeLayTepDinhKem numeric(16, 0),
	@NgayHoanThanhKiemTraHoSo datetime,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@TongDungLuong numeric(18, 2),
	@PhanLuong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMD_ID bigint,
	@NgayBatDau datetime,
	@NgayCapNhatCuoiCung datetime,
	@CanCuPhapLenh nvarchar(996),
	@CoBaoXoa varchar(1),
	@GhiChuHaiQuan nvarchar(996),
	@HinhThucTimKiem varchar(1),
	@LyDoChinhSua nvarchar(300),
	@MaDiaDiemDen varchar(6),
	@MaNhaVanChuyen varchar(6),
	@MaPhanLoaiDangKy varchar(1),
	@MaPhanLoaiXuLy varchar(1),
	@MaThongTinXuat varchar(7),
	@NgayCapPhep datetime,
	@NgayThongBao datetime,
	@NguoiGui varchar(5),
	@NoiDungChinhSua nvarchar(300),
	@PhuongTienVanChuyen varchar(12),
	@SoChuyenDiBien varchar(10),
	@SoSeri varchar(3),
	@SoTiepNhanToKhaiPhoThong numeric(12, 0),
	@TrangThaiXuLyHaiQuan varchar(1),
	@NguoiCapNhatCuoiCung varchar(8),
	@NhomXuLyHoSoID int
AS

UPDATE
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]
SET
	[LoaiChungTu] = @LoaiChungTu,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[NhomXuLyHoSo] = @NhomXuLyHoSo,
	[TieuDe] = @TieuDe,
	[SoToKhai] = @SoToKhai,
	[GhiChu] = @GhiChu,
	[PhanLoaiThuTucKhaiBao] = @PhanLoaiThuTucKhaiBao,
	[SoDienThoaiNguoiKhaiBao] = @SoDienThoaiNguoiKhaiBao,
	[SoQuanLyTrongNoiBoDoanhNghiep] = @SoQuanLyTrongNoiBoDoanhNghiep,
	[MaKetQuaXuLy] = @MaKetQuaXuLy,
	[TenThuTucKhaiBao] = @TenThuTucKhaiBao,
	[NgayKhaiBao] = @NgayKhaiBao,
	[TrangThaiKhaiBao] = @TrangThaiKhaiBao,
	[NgaySuaCuoiCung] = @NgaySuaCuoiCung,
	[TenNguoiKhaiBao] = @TenNguoiKhaiBao,
	[DiaChiNguoiKhaiBao] = @DiaChiNguoiKhaiBao,
	[SoDeLayTepDinhKem] = @SoDeLayTepDinhKem,
	[NgayHoanThanhKiemTraHoSo] = @NgayHoanThanhKiemTraHoSo,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TongDungLuong] = @TongDungLuong,
	[PhanLuong] = @PhanLuong,
	[HuongDan] = @HuongDan,
	[TKMD_ID] = @TKMD_ID,
	[NgayBatDau] = @NgayBatDau,
	[NgayCapNhatCuoiCung] = @NgayCapNhatCuoiCung,
	[CanCuPhapLenh] = @CanCuPhapLenh,
	[CoBaoXoa] = @CoBaoXoa,
	[GhiChuHaiQuan] = @GhiChuHaiQuan,
	[HinhThucTimKiem] = @HinhThucTimKiem,
	[LyDoChinhSua] = @LyDoChinhSua,
	[MaDiaDiemDen] = @MaDiaDiemDen,
	[MaNhaVanChuyen] = @MaNhaVanChuyen,
	[MaPhanLoaiDangKy] = @MaPhanLoaiDangKy,
	[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
	[MaThongTinXuat] = @MaThongTinXuat,
	[NgayCapPhep] = @NgayCapPhep,
	[NgayThongBao] = @NgayThongBao,
	[NguoiGui] = @NguoiGui,
	[NoiDungChinhSua] = @NoiDungChinhSua,
	[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
	[SoChuyenDiBien] = @SoChuyenDiBien,
	[SoSeri] = @SoSeri,
	[SoTiepNhanToKhaiPhoThong] = @SoTiepNhanToKhaiPhoThong,
	[TrangThaiXuLyHaiQuan] = @TrangThaiXuLyHaiQuan,
	[NguoiCapNhatCuoiCung] = @NguoiCapNhatCuoiCung,
	[NhomXuLyHoSoID] = @NhomXuLyHoSoID
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_InsertUpdate]
	@ID bigint,
	@LoaiChungTu varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@TrangThaiXuLy varchar(50),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@TieuDe nvarchar(210),
	@SoToKhai numeric(12, 0),
	@GhiChu nvarchar(996),
	@PhanLoaiThuTucKhaiBao varchar(3),
	@SoDienThoaiNguoiKhaiBao varchar(20),
	@SoQuanLyTrongNoiBoDoanhNghiep varchar(20),
	@MaKetQuaXuLy varchar(75),
	@TenThuTucKhaiBao nvarchar(210),
	@NgayKhaiBao datetime,
	@TrangThaiKhaiBao varchar(30),
	@NgaySuaCuoiCung datetime,
	@TenNguoiKhaiBao nvarchar(300),
	@DiaChiNguoiKhaiBao nvarchar(300),
	@SoDeLayTepDinhKem numeric(16, 0),
	@NgayHoanThanhKiemTraHoSo datetime,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@TongDungLuong numeric(18, 2),
	@PhanLuong nvarchar(50),
	@HuongDan varchar(4000),
	@TKMD_ID bigint,
	@NgayBatDau datetime,
	@NgayCapNhatCuoiCung datetime,
	@CanCuPhapLenh nvarchar(996),
	@CoBaoXoa varchar(1),
	@GhiChuHaiQuan nvarchar(996),
	@HinhThucTimKiem varchar(1),
	@LyDoChinhSua nvarchar(300),
	@MaDiaDiemDen varchar(6),
	@MaNhaVanChuyen varchar(6),
	@MaPhanLoaiDangKy varchar(1),
	@MaPhanLoaiXuLy varchar(1),
	@MaThongTinXuat varchar(7),
	@NgayCapPhep datetime,
	@NgayThongBao datetime,
	@NguoiGui varchar(5),
	@NoiDungChinhSua nvarchar(300),
	@PhuongTienVanChuyen varchar(12),
	@SoChuyenDiBien varchar(10),
	@SoSeri varchar(3),
	@SoTiepNhanToKhaiPhoThong numeric(12, 0),
	@TrangThaiXuLyHaiQuan varchar(1),
	@NguoiCapNhatCuoiCung varchar(8),
	@NhomXuLyHoSoID int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ChungTuDinhKem] 
		SET
			[LoaiChungTu] = @LoaiChungTu,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[NhomXuLyHoSo] = @NhomXuLyHoSo,
			[TieuDe] = @TieuDe,
			[SoToKhai] = @SoToKhai,
			[GhiChu] = @GhiChu,
			[PhanLoaiThuTucKhaiBao] = @PhanLoaiThuTucKhaiBao,
			[SoDienThoaiNguoiKhaiBao] = @SoDienThoaiNguoiKhaiBao,
			[SoQuanLyTrongNoiBoDoanhNghiep] = @SoQuanLyTrongNoiBoDoanhNghiep,
			[MaKetQuaXuLy] = @MaKetQuaXuLy,
			[TenThuTucKhaiBao] = @TenThuTucKhaiBao,
			[NgayKhaiBao] = @NgayKhaiBao,
			[TrangThaiKhaiBao] = @TrangThaiKhaiBao,
			[NgaySuaCuoiCung] = @NgaySuaCuoiCung,
			[TenNguoiKhaiBao] = @TenNguoiKhaiBao,
			[DiaChiNguoiKhaiBao] = @DiaChiNguoiKhaiBao,
			[SoDeLayTepDinhKem] = @SoDeLayTepDinhKem,
			[NgayHoanThanhKiemTraHoSo] = @NgayHoanThanhKiemTraHoSo,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TongDungLuong] = @TongDungLuong,
			[PhanLuong] = @PhanLuong,
			[HuongDan] = @HuongDan,
			[TKMD_ID] = @TKMD_ID,
			[NgayBatDau] = @NgayBatDau,
			[NgayCapNhatCuoiCung] = @NgayCapNhatCuoiCung,
			[CanCuPhapLenh] = @CanCuPhapLenh,
			[CoBaoXoa] = @CoBaoXoa,
			[GhiChuHaiQuan] = @GhiChuHaiQuan,
			[HinhThucTimKiem] = @HinhThucTimKiem,
			[LyDoChinhSua] = @LyDoChinhSua,
			[MaDiaDiemDen] = @MaDiaDiemDen,
			[MaNhaVanChuyen] = @MaNhaVanChuyen,
			[MaPhanLoaiDangKy] = @MaPhanLoaiDangKy,
			[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
			[MaThongTinXuat] = @MaThongTinXuat,
			[NgayCapPhep] = @NgayCapPhep,
			[NgayThongBao] = @NgayThongBao,
			[NguoiGui] = @NguoiGui,
			[NoiDungChinhSua] = @NoiDungChinhSua,
			[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
			[SoChuyenDiBien] = @SoChuyenDiBien,
			[SoSeri] = @SoSeri,
			[SoTiepNhanToKhaiPhoThong] = @SoTiepNhanToKhaiPhoThong,
			[TrangThaiXuLyHaiQuan] = @TrangThaiXuLyHaiQuan,
			[NguoiCapNhatCuoiCung] = @NguoiCapNhatCuoiCung,
			[NhomXuLyHoSoID] = @NhomXuLyHoSoID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ChungTuDinhKem]
		(
			[LoaiChungTu],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[TrangThaiXuLy],
			[CoQuanHaiQuan],
			[NhomXuLyHoSo],
			[TieuDe],
			[SoToKhai],
			[GhiChu],
			[PhanLoaiThuTucKhaiBao],
			[SoDienThoaiNguoiKhaiBao],
			[SoQuanLyTrongNoiBoDoanhNghiep],
			[MaKetQuaXuLy],
			[TenThuTucKhaiBao],
			[NgayKhaiBao],
			[TrangThaiKhaiBao],
			[NgaySuaCuoiCung],
			[TenNguoiKhaiBao],
			[DiaChiNguoiKhaiBao],
			[SoDeLayTepDinhKem],
			[NgayHoanThanhKiemTraHoSo],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TongDungLuong],
			[PhanLuong],
			[HuongDan],
			[TKMD_ID],
			[NgayBatDau],
			[NgayCapNhatCuoiCung],
			[CanCuPhapLenh],
			[CoBaoXoa],
			[GhiChuHaiQuan],
			[HinhThucTimKiem],
			[LyDoChinhSua],
			[MaDiaDiemDen],
			[MaNhaVanChuyen],
			[MaPhanLoaiDangKy],
			[MaPhanLoaiXuLy],
			[MaThongTinXuat],
			[NgayCapPhep],
			[NgayThongBao],
			[NguoiGui],
			[NoiDungChinhSua],
			[PhuongTienVanChuyen],
			[SoChuyenDiBien],
			[SoSeri],
			[SoTiepNhanToKhaiPhoThong],
			[TrangThaiXuLyHaiQuan],
			[NguoiCapNhatCuoiCung],
			[NhomXuLyHoSoID]
		)
		VALUES 
		(
			@LoaiChungTu,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@TrangThaiXuLy,
			@CoQuanHaiQuan,
			@NhomXuLyHoSo,
			@TieuDe,
			@SoToKhai,
			@GhiChu,
			@PhanLoaiThuTucKhaiBao,
			@SoDienThoaiNguoiKhaiBao,
			@SoQuanLyTrongNoiBoDoanhNghiep,
			@MaKetQuaXuLy,
			@TenThuTucKhaiBao,
			@NgayKhaiBao,
			@TrangThaiKhaiBao,
			@NgaySuaCuoiCung,
			@TenNguoiKhaiBao,
			@DiaChiNguoiKhaiBao,
			@SoDeLayTepDinhKem,
			@NgayHoanThanhKiemTraHoSo,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TongDungLuong,
			@PhanLuong,
			@HuongDan,
			@TKMD_ID,
			@NgayBatDau,
			@NgayCapNhatCuoiCung,
			@CanCuPhapLenh,
			@CoBaoXoa,
			@GhiChuHaiQuan,
			@HinhThucTimKiem,
			@LyDoChinhSua,
			@MaDiaDiemDen,
			@MaNhaVanChuyen,
			@MaPhanLoaiDangKy,
			@MaPhanLoaiXuLy,
			@MaThongTinXuat,
			@NgayCapPhep,
			@NgayThongBao,
			@NguoiGui,
			@NoiDungChinhSua,
			@PhuongTienVanChuyen,
			@SoChuyenDiBien,
			@SoSeri,
			@SoTiepNhanToKhaiPhoThong,
			@TrangThaiXuLyHaiQuan,
			@NguoiCapNhatCuoiCung,
			@NhomXuLyHoSoID
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
FROM
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
FROM [dbo].[t_KDT_VNACC_ChungTuDinhKem] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Sunday, April 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiChungTu],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[TrangThaiXuLy],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[TieuDe],
	[SoToKhai],
	[GhiChu],
	[PhanLoaiThuTucKhaiBao],
	[SoDienThoaiNguoiKhaiBao],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[MaKetQuaXuLy],
	[TenThuTucKhaiBao],
	[NgayKhaiBao],
	[TrangThaiKhaiBao],
	[NgaySuaCuoiCung],
	[TenNguoiKhaiBao],
	[DiaChiNguoiKhaiBao],
	[SoDeLayTepDinhKem],
	[NgayHoanThanhKiemTraHoSo],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TongDungLuong],
	[PhanLuong],
	[HuongDan],
	[TKMD_ID],
	[NgayBatDau],
	[NgayCapNhatCuoiCung],
	[CanCuPhapLenh],
	[CoBaoXoa],
	[GhiChuHaiQuan],
	[HinhThucTimKiem],
	[LyDoChinhSua],
	[MaDiaDiemDen],
	[MaNhaVanChuyen],
	[MaPhanLoaiDangKy],
	[MaPhanLoaiXuLy],
	[MaThongTinXuat],
	[NgayCapPhep],
	[NgayThongBao],
	[NguoiGui],
	[NoiDungChinhSua],
	[PhuongTienVanChuyen],
	[SoChuyenDiBien],
	[SoSeri],
	[SoTiepNhanToKhaiPhoThong],
	[TrangThaiXuLyHaiQuan],
	[NguoiCapNhatCuoiCung],
	[NhomXuLyHoSoID]
FROM
	[dbo].[t_KDT_VNACC_ChungTuDinhKem]	

GO

