﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.Interface
{
    public partial class FrmQuerySQLUpdate : BaseForm
    {
        private DataSet _data = new DataSet();

        public FrmQuerySQLUpdate()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtQuery.Clear();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            string str = txtQuery.Text;
            Query(str);
            
        }

        private void Query(string strquery)
        {
            Cursor = Cursors.WaitCursor;

            //Clear content.
           txtMessage.Clear();
            _data.Clear();

            bool ret = false;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                   // txtQuery.SelectAll();
                    string sql = GetQueryString();

                    SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(strquery);

                    if (strquery.Trim().ToUpper().StartsWith("SELECT"))
                    {
                        _data = db.ExecuteDataSet(dbCommand);
                        grdResult.DataSource = _data;
                        grdResult.DataMember = "Table";

                        txtMessage.Text = grdResult.RowCount + " record(s).";
                    }
                    else if (strquery.Trim().ToUpper().StartsWith("INSERT") ||
                        strquery.Trim().ToUpper().StartsWith("UPDATE") ||
                        strquery.Trim().ToUpper().StartsWith("DELETE"))
                    {
                        txtMessage.Text = db.ExecuteNonQuery(dbCommand) + " record(s).";
                    }

                    //Active Tab Grid after have result.
                    uiTabPage4.Selected = true;

                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    txtMessage.Text = ex.Message + "\r\n\n" + ex.StackTrace;
                    uiTabPage2.Selected = true;
                }
                finally
                {
                    connection.Close();

                    Cursor = Cursors.Default;
                }
            }
        }

        private string GetQueryString()
        {
            string[] lines = txtQuery.Lines;
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < lines.Length; i++)
            {
                if (!lines[i].StartsWith("--"))
                {
                    sb.AppendLine(lines[i]);
                }
            }

            return sb.ToString();
        }

        private void FrmQuerySQL_Load(object sender, EventArgs e)
        {
            //this.btnQuery.Image = Company.Interface.Properties.Resources.bt_play;
            //this.btnClear.Image = Company.Interface.Properties.Resources.clear;
            //this.btnClose.Image = Company.Interface.Properties.Resources.exit;

           // uiGroupBox1.Height = (int)(((double)(this.Parent.Height) * (double)75) / 100);
#if (KD_V3)
             comboBoxTable.Items.Add("t_kdt_tokhaimaudich");
#elif(GC_V3 )
            comboBoxTable.Items.Add("t_kdt_tokhaimaudich");
            comboBoxTable.Items.Add("t_KDT_GC_ToKhaiChuyenTiep");
            comboBoxTable.Items.Add("t_KDT_GC_DinhMucDangKy");
            comboBoxTable.Items.Add("t_KDT_GC_HopDong");
           
            
#elif(SXXK_V3)
             comboBoxTable.Items.Add("t_kdt_tokhaimaudich");
            comboBoxTable.Items.Add("t_KDT_SXXK_DinhMucDangky");
            comboBoxTable.Items.Add("t_KDT_SXXK_NguyenphulieuDangKy");
            comboBoxTable.Items.Add("t_KDT_SXXK_SanPhamDangKy");
            
#endif

        }

       

        private void btnSelect_Click(object sender, EventArgs e)
        {

            string table = comboBoxTable.Text;
            string value = txtID.Text.Trim();
            string colum = "ID";
            if(rbtnID.Checked == true)
            {
                colum = "ID";
            }
            else if(rbtnSoTN.Checked == true)
            {
                colum = "Sotiepnhan";
            }
            else if(rbtnSoTK.Checked == true)
            {
                colum = "ID";
            }

            if (table != "" && value != "")
            {
                try
                {
                    string str = "SELECT * from " + table + " Where "+colum+"=" + value;
                   // txtQuery.Text = txtQuery.Text+str;
                    Query(str);

                    txtSTN.Text = grdResult.Rows[0].Cells["sotiepnhan"].Value.ToString();
                    txtNgayTiepNhan.Text = grdResult.Rows[0].Cells["NgayTiepNhan"].Value.ToString();
                    txtTT_XuLy.Text = grdResult.Rows[0].Cells["Trangthaixuly"].Value.ToString();
                    txtReferenceid.Text = grdResult.Rows[0].Cells["Guidstr"].Value.ToString();
                    txtNgayDangKy.Text = grdResult.Rows[0].Cells["Ngaydangky"].Value.ToString();
                }
                catch (Exception exx)
                {
                    MessageBox.Show(colum+" không tồn tại");
                }
            }
            
            
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            string table = comboBoxTable.Text;
            string value = txtID.Text;
            string colum="";
            if (rbtnID.Checked == true)
            {
                colum = "ID";
            }
            else if (rbtnSoTN.Checked == true)
            {
                colum = "Sotiepnhan";
            }
            else if (rbtnSoTK.Checked == true)
            {
                colum = "ID";
            }

            if (table != "" && value != "")
            {
                DialogResult myDialogResult;
                myDialogResult = MessageBox.Show("Bạn muốn cập nhật cho bảng " + table + " với "+colum+" = " + value,"Cập nhật thông tin tờ khai", MessageBoxButtons.YesNo);

                if (myDialogResult == DialogResult.Yes)
                {
                    string str = "UPDATE " + table + " set sotiepnhan=" + txtSTN.Text + ", ngaytiepnhan='" + txtNgayTiepNhan.Text + "',trangthaixuly=" + txtTT_XuLy.Text + ",Guidstr='" + txtReferenceid.Text + "', ngaydangky='"+txtNgayDangKy.Text+"' where "+colum+"=" + value + "";
                    txtQuery.Text = txtQuery.Text + "\\n" + str;
                    Query(str);
                    MessageBox.Show("Cập nhật thành công.","Thông báo");
                    btnSelect_Click(null, null);
                }
            }
            else
            {
                MessageBox.Show("Chọn điều kiện để cập nhật", "Cảnh báo");
                txtID.Focus();
            }
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {
            string id = txtID.Text.Trim();
            if (id == "")
            {
                btnSelect.Enabled = false;
            }
            btnSelect.Enabled = true;
        }

        private void txtID_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                btnSelect_Click(null,null);

            }

        }

        private void txtQuery_KeyUp(object sender, KeyEventArgs e)
        {
            string str = txtQuery.Text;
            if (e.KeyData == Keys.F5)
            {
                Query(str);
            }
            else if (e.Control && e.KeyCode == Keys.A)
            {
                txtQuery.SelectAll();
            }

        }

      

      

     

           

      

      
    }
}
