﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;


namespace Company.Interface
{
    public partial class VNACC_TK_TKTriGiaForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        DataTable dt = new DataTable();
        DataSet dsMaTen = new DataSet();
        DataSet dsMaTienTe = new DataSet();
        DataSet dsMaPhanLoai = new DataSet();
        public VNACC_TK_TKTriGiaForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclaration_CustomsClearance.IDA.ToString());
        }

        private void VNACC_TK_TKTriGiaForm_Load(object sender, EventArgs e)
        {
            SetIDControl();
            LoadData();
        }
        private void LoadData()
        {

            dsMaTen = VNACC_Category_Common.SelectDynamic("ReferenceDB='A401'", "");
            dsMaTienTe = VNACC_Category_CurrencyExchange.SelectAll();
            dsMaPhanLoai = VNACC_Category_Common.SelectDynamic("ReferenceDB='E012'", "");
            dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(TKMD.KhoanDCCollection);
            grList.DropDowns["drdMaTen"].DataSource = dsMaTen.Tables[0];
            grList.DropDowns["drdMaTT"].DataSource = dsMaTienTe.Tables[0];
            grList.DropDowns["drdMaPhanLoai"].DataSource = dsMaPhanLoai.Tables[0];
            grList.DataSource = dt;

            SetKhaiTriGia();
        }
        private void SetKhaiTriGia()
        {
            ctrMaPhanLoaiTriGia.Code = TKMD.MaPhanLoaiTriGia;
            txtSoTiepNhanTKTriGia.Text = Convert.ToString(TKMD.SoTiepNhanTKTriGia);
            ctrMaTTHieuChinhTriGia.Code = TKMD.MaTTHieuChinhTriGia;
            txtGiaHieuChinhTriGia.Text = Convert.ToString(TKMD.GiaHieuChinhTriGia);
            ctrMaPhanLoaiPhiVC.Code = TKMD.MaPhanLoaiPhiVC;
            txtPhiVanChuyen.Text = Convert.ToString(TKMD.PhiVanChuyen);
            ctrMaTTPhiVC.Code = TKMD.MaTTPhiVC;
            ctrMaPhanLoaiPhiBH.Code = TKMD.MaPhanLoaiPhiBH;
            txtPhiBaoHiem.Text = Convert.ToString(TKMD.PhiBaoHiem);
            ctrMaTTPhiBH.Code = TKMD.MaTTPhiBH;
            txtChiTietKhaiTriGia.Text = TKMD.ChiTietKhaiTriGia;
            txtTongHeSoPhanBoTG.Text = TKMD.TongHeSoPhanBoTG.ToString();
            txtSoDangKyBH.Text = TKMD.SoDangKyBH;
            txtMaPhanLoaiTongGiaCoBan.Text = TKMD.MaPhanLoaiTongGiaCoBan;
        }

        private void GetKhaiTriGia()
        {
            TKMD.MaPhanLoaiTriGia = ctrMaPhanLoaiTriGia.Code;
            TKMD.SoTiepNhanTKTriGia = Convert.ToDecimal(txtSoTiepNhanTKTriGia.Value);
            TKMD.MaTTHieuChinhTriGia = ctrMaTTHieuChinhTriGia.Code;
            TKMD.GiaHieuChinhTriGia = Convert.ToDecimal(txtGiaHieuChinhTriGia.Value);
            TKMD.MaPhanLoaiPhiVC = ctrMaPhanLoaiPhiVC.Code;
            TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
            TKMD.MaTTPhiVC = ctrMaTTPhiVC.Code;
            TKMD.MaPhanLoaiPhiBH = ctrMaPhanLoaiPhiBH.Code;
            TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
            TKMD.MaTTPhiBH = ctrMaTTPhiBH.Code;
            TKMD.ChiTietKhaiTriGia = txtChiTietKhaiTriGia.Text;
            TKMD.SoDangKyBH = txtSoDangKyBH.Text;
            TKMD.TongHeSoPhanBoTG = Convert.ToDecimal(txtTongHeSoPhanBoTG.Value);
        }


        private void Save()
        {
            GetKhaiTriGia();
            int sott = 1;
            TKMD.KhoanDCCollection.Clear();
            foreach (DataRow i in dt.Rows)
            {
                KDT_VNACC_TK_KhoanDieuChinh dc = new KDT_VNACC_TK_KhoanDieuChinh();
                if (i["ID"].ToString().Length != 0)
                {
                    dc.ID = Convert.ToInt32(i["ID"].ToString());
                    dc.TKMD_ID = Convert.ToInt32(i["TKMD_ID"].ToString());
                }
                dc.SoTT = sott;
                dc.MaTenDieuChinh = i["MaTenDieuChinh"].ToString();
                dc.MaPhanLoaiDieuChinh = i["MaPhanLoaiDieuChinh"].ToString();
                dc.MaTTDieuChinhTriGia = i["MaTTDieuChinhTriGia"].ToString();
                dc.TriGiaKhoanDieuChinh = Convert.ToDecimal(i["TriGiaKhoanDieuChinh"].ToString());
                dc.TongHeSoPhanBo = Convert.ToDecimal(i["TongHeSoPhanBo"].ToString());

                TKMD.KhoanDCCollection.Add(dc);
                sott++;
            }
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                Save();
                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }

        }



        private void grList_DeletingRecord(object sender, Janus.Windows.GridEX.RowActionCancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }
        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                ctrMaPhanLoaiTriGia.Tag = "VD1"; //Mã phân loại khai trị giá
                txtSoTiepNhanTKTriGia.Tag = "VD2"; //Số tiếp nhận tờ khai trị giá tổng hợp
                ctrMaTTHieuChinhTriGia.Tag = "VCC"; //Mã tiền tệ của giá cơ sở hiệu chỉnh trị giá
                txtGiaHieuChinhTriGia.Tag = "VPC"; //Giá cơ sở để hiệu chỉnh trị giá
                ctrMaPhanLoaiPhiVC.Tag = "FR1"; //Mã phân loại phí vận chuyển
                ctrMaTTPhiVC.Tag = "FR2"; //Mã tiền tệ phí vận chuyển
                txtPhiVanChuyen.Tag = "FR3"; //Phí vận chuyển
                ctrMaPhanLoaiPhiBH.Tag = "IN1"; //Mã phân loại bảo hiểm
                ctrMaTTPhiBH.Tag = "IN2"; //Mã tiền tệ của tiền bảo hiểm
                txtPhiBaoHiem.Tag = "IN3"; //Phí bảo hiểm
                txtSoDangKyBH.Tag = "IN4"; //Số đăng ký bảo hiểm tổng hợp
                //txtMaTenDieuChinh.Tag = "VR_"; //Mã tên khoản điều chỉnh
                //txtMaPhanLoaiDieuChinh.Tag = "VI_"; //Mã phân loại điều chỉnh trị giá
                //txtMaTTDieuChinhTriGia.Tag = "VC_"; //Mã đồng tiền của khoản điều chỉnh trị giá
                //txtTriGiaKhoanDieuChinh.Tag = "VP_"; //Trị giá khoản điều chỉnh
                //txtTongHeSoPhanBo.Tag = "VT_"; //Tổng hệ số phân bổ trị giá khoản điều chỉnh
                txtChiTietKhaiTriGia.Tag = "VLD"; //Chi tiết khai trị giá
                txtTongHeSoPhanBoTG.Tag = "TP"; //Tổng hệ số phân bổ trị giá


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
                if (ID > 0)
                {
                    if (ShowMessage("Bạn có muốn xóa khoản điều chỉnh này không?", true) == "Yes")
                    {
                        KDT_VNACC_TK_KhoanDieuChinh.DeleteDynamic("ID=" + ID);
                    }
                }
                grList.CurrentRow.Delete();
                grList.Refetch();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

    }
}
