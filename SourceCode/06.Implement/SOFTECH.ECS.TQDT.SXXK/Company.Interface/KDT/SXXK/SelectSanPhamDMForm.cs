using System;
using System.Data;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.SXXK;
using Company.BLL.DuLieuChuan;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.SXXK
{
    public partial class SelectSanPhamDMForm : BaseForm
    {
        public SanPham SanPhamSelected = new SanPham();

        public bool status = true;//0 mo moi hoan toan//1 da co roi mo len
        private DataSet dsRegistedList;
        public SelectSanPhamDMForm()
        {
            InitializeComponent();
            this.ctrDonViHaiQuan.ValueChanged -= new Company.Interface.Controls.DonViHaiQuanControl.ValueChangedEventHandler(this.ctrDonViHaiQuan_ValueChanged);                        
        }
        public void BindDanhSachSP()
        {
            string where = "TrangThaiXuLy<>1";
                where += " and MaHaiQuan='"+GlobalSettings.MA_HAI_QUAN+"' AND MaDoanhNghiep = '"+ GlobalSettings.MA_DON_VI+"' ";
            Company.BLL.KDT.SXXK.SanPhamDangKyCollection collection = Company.BLL.KDT.SXXK.SanPhamDangKy.SelectCollectionDynamic(where, "");
            Company.BLL.KDT.SXXK.SanPhamDangKy spdk = new Company.BLL.KDT.SXXK.SanPhamDangKy();
            spdk.ID = 0;
            spdk.SoTiepNhan = 0;
            collection.Add(spdk);
        }
        public void BindData()
        {
            Company.BLL.SXXK.DinhMuc dm = new Company.BLL.SXXK.DinhMuc();
            DataSet ds = dm.getSanPhamChuacoDinhMuc(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            dgList.DataSource = ds.Tables[0];                              
            
        }

        private void dsSanPham_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["TrangThaiXL"].Text == "0" && e.Row.Cells["ID"].Text != "0")
                    e.Row.Cells["TrangThaiXL"].Text = "Chờ duyệt";
                else if (e.Row.Cells["TrangThaiXL"].Text == "-1" && e.Row.Cells["ID"].Text != "0")
                    e.Row.Cells["TrangThaiXL"].Text = "Chưa khai báo";

                if (e.Row.Cells["ID"].Text != "0")
                {
                    e.Row.Cells["ThongTin"].Text = "Danh sách sản phẩm " + e.Row.Cells["ID"].Text;                    
                }
                else
                {
                    e.Row.Cells["ThongTin"].Text = "Danh sách đã duyệt";                    
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        
        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            if (this.CalledForm == "DinhMucSendForm")
            {
                ctrDonViHaiQuan.Enabled = false;
            }
            lblHint.Visible = this.CalledForm != string.Empty;
        }

        //-----------------------------------------------------------------------------------------
        
        private void SanPhamRegistedForm_Load(object sender, EventArgs e)
        {
            if (status)
            {
                this.khoitao_DuLieuChuan();              
                this.BindData();
                // Doanh nghiệp / Đại lý TTHQ.
                dgList.Tables[0].Columns["MaDoanhNghiep"].Visible = GlobalSettings.DAI_LY_TTHQ;                
                status = false;
            }
        }

        //-----------------------------------------------------------------------------------------
        
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------
        
   
        //-----------------------------------------------------------------------------------------
        
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (lblHint.Visible)
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string maSP = e.Row.Cells["Ma"].Text;
                    this.SanPhamSelected.Ma = maSP;
                    this.SanPhamSelected.MaHaiQuan = GlobalSettings.MA_HAI_QUAN; //this.MaHaiQuan.Trim().Trim(); Hungtq comment 16/12/2010
                    this.SanPhamSelected.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.SanPhamSelected.Ten = e.Row.Cells["Ten"].Text;
                    this.SanPhamSelected.MaHS = e.Row.Cells["MaHS"].Text;
                    this.SanPhamSelected.DVT_ID = e.Row.Cells["DVT_ID"].Text;
                    this.SanPhamSelected.Load();
                    this.Hide();
                }
            }
            else 
            {

            }
        }

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {                                             
            dgList.DataSource = new SanPham().SearchBy_MaHaiQuan(ctrDonViHaiQuan.Ma).Tables[0];                             
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        

        //-----------------------------------------------------------------------------------------
    }
}