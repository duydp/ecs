using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Globalization;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data.SqlClient;
using Logger;

namespace Company.Interface.Report.SXXK
{
    public partial class BangKeToKhaiNhapDVTK : DevExpress.XtraReports.UI.XtraReport
    {
        public int LanThanhLy;
        public int NamThanhLy;
        public string MaDoanhNghiep;
        public BangKeToKhaiNhapDVTK()
        {
            InitializeComponent();
        }
        public void BindReport(string where)
        {
            DetailReport.Font = new Font("Times New Roman", GlobalSettings.FontBCXNT);
            DataTable dataSource = new DataTable();
            dataSource = GetDataSource();
            //DetailReport.DataSource = dataSource;
            lblSTT.DataBindings.Add("Text", this.DataSource,"STT");
            lblSoToKhai.DataBindings.Add("Text", this.DataSource,"SoToKhai");
            lblTenNPL.DataBindings.Add("Text", this.DataSource,"TenNPL");
            lblDVT.DataBindings.Add("Text", this.DataSource,"DVT");
            lblLuongNK.DataBindings.Add("Text", this.DataSource,"LuongNhap");
            lblLuongDVTK.DataBindings.Add("Text", this.DataSource,"LuongDVTK");
            lblSoTienThue.DataBindings.Add("Text", this.DataSource,"TienThueHoan", "{0:N0}");
            lblHDNK.DataBindings.Add("Text", this.DataSource,"SoHopDong");
            lblSoCTTT.DataBindings.Add("Text", this.DataSource, "ChungTuTT");
            lblTriGiaTT.DataBindings.Add("Text", this.DataSource,"TriGiaTT");
            lblGhiChu.DataBindings.Add("Text", this.DataSource,"GhiChu");
        }
        private DataTable GetDataSource()
        {
            try
            {
                string spName = "[p_SXXK_BKToKhaiNhapDVTK]";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
                db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.BigInt, LanThanhLy);
                db.AddInParameter(dbCommand, "@NamThanhLy", SqlDbType.Int, NamThanhLy);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
                db.AddInParameter(dbCommand, "@SapXepNgayDangKyTKN", SqlDbType.VarChar, GlobalSettings.SoThapPhan.SapXepTheoTK == 0 ? 0 : 1);
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
    }
}
