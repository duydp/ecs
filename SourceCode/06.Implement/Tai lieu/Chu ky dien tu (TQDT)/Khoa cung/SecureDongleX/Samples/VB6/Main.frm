VERSION 5.00
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SecureDongle X VB6 Sample"
   ClientHeight    =   5760
   ClientLeft      =   3930
   ClientTop       =   2250
   ClientWidth     =   9255
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5760
   ScaleWidth      =   9255
   Begin VB.CommandButton cmdReadMap 
      Caption         =   "Read with Map"
      Height          =   495
      Left            =   7680
      TabIndex        =   6
      Top             =   4920
      Width           =   1335
   End
   Begin VB.CommandButton cmdWriteMap 
      Caption         =   "Write with Map"
      Height          =   495
      Left            =   7680
      TabIndex        =   5
      Top             =   4320
      Width           =   1335
   End
   Begin VB.CommandButton cmdTransform 
      Caption         =   "Tranform Data"
      Height          =   495
      Left            =   7680
      TabIndex        =   1
      Top             =   1920
      Width           =   1335
   End
   Begin VB.CommandButton cmdDecrypt 
      Caption         =   "Decrypt Data"
      Height          =   495
      Left            =   7680
      TabIndex        =   4
      Top             =   3720
      Width           =   1335
   End
   Begin VB.CommandButton cmdEncrypt 
      Caption         =   "Encrypt Data"
      Height          =   495
      Left            =   7680
      TabIndex        =   3
      Top             =   3120
      Width           =   1335
   End
   Begin VB.CommandButton cmdRead 
      Caption         =   "Read Data"
      Height          =   495
      Left            =   7680
      TabIndex        =   2
      Top             =   2520
      Width           =   1335
   End
   Begin VB.ListBox lstOutput 
      Height          =   4155
      ItemData        =   "Main.frx":0000
      Left            =   240
      List            =   "Main.frx":0002
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   1320
      Width           =   7215
   End
   Begin VB.CommandButton cmdWrite 
      Caption         =   "Write Data"
      Height          =   495
      Left            =   7680
      TabIndex        =   0
      Top             =   1320
      Width           =   1335
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "SecureDongle X  VB6 SAMPLE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   8
      Top             =   360
      Width           =   8535
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim retcode, handle, block_index As Integer
Dim uid As Long, hid  As Long
Dim buffer As String
Dim seed As String
Dim stri As String
Dim key512 As String * 512
Dim plainText As String
Dim data As String
Dim bufferLength, startIndex As Integer
Dim curline As Integer

Private Sub cmdWrite_Click()
' Write to SecureDongle X with specified UID
  retcode = SDX_Find()
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error Finding SecureDongle X: " & Hex(retcode)
    curline = curline + 1
  End If
  
  If (retcode = 0) Then
    lstOutput.List(curline) = "No SecureDongle X plugged"
    curline = curline + 1
    Exit Sub
  Else
    lstOutput.List(curline) = "Found SecureDongle X: " & Hex(retcode)
    curline = curline + 1
  End If
  
  stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
  uid = Val(stri)
  
  retcode = SDX_Open(1, uid, hid)
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error: " & Hex(retcode)
    curline = curline + 1
    Exit Sub
  Else
    lstOutput.List(curline) = "Succeeded Opening SecureDongle X, UID: " & (uid)
    curline = curline + 1
  End If
  
  handle = retcode
  seed = InputBox("Please input write block index (0-4)", "Input", "0")
  block_index = Val(seed)
  stri = InputBox("Please input data to be written (i.e. helloworld)", "Input", "SDX - Sample Data")
  retcode = SDX_Write(handle, block_index, stri)
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error: " & Hex(retcode)
    curline = curline + 1
    Exit Sub
  End If
  lstOutput.List(curline) = "Write: " & (stri)
  curline = curline + 1
  
  SDX_Close handle
End Sub

Private Sub cmdTransform_Click()
' Transform data using specified SecureDongle X
  retcode = SDX_Find()
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error Finding SecureDongle X: " & Hex(retcode)
    curline = curline + 1
  End If
  
  If (retcode = 0) Then
    lstOutput.List(curline) = "No SecureDongle X plugged"
    curline = curline + 1
    Exit Sub
  Else
    lstOutput.List(curline) = "Found SecureDongle X: " & Hex(retcode)
    curline = curline + 1
  End If
  
  stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
  uid = Val(stri)
  
  retcode = SDX_Open(1, uid, hid)
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error: " & Hex(retcode)
    curline = curline + 1
    Exit Sub
  Else
    lstOutput.List(curline) = "Succeeded Opening SecureDongle X, UID: " & (uid)
    curline = curline + 1
  End If
  
  handle = retcode
  stri = InputBox("Please input data (i.e. helloworld)", "Input", "SDXTransform Test")
    
  retcode = SDX_Transform(handle, Len(stri), stri)
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error: " & Hex(retcode)
    curline = curline + 1
    Exit Sub
  End If
  lstOutput.List(curline) = "Transform result: " & (stri)
  curline = curline + 1
   
  SDX_Close handle
End Sub

Private Sub cmdRead_Click()
' Read SecureDongle X with specified UID
  retcode = SDX_Find()
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error Finding SecureDongle X: " & Hex(retcode)
    curline = curline + 1
  End If
  
  If (retcode = 0) Then
    lstOutput.List(curline) = "No SecureDongle X plugged"
    curline = curline + 1
    Exit Sub
  Else
    lstOutput.List(curline) = "Found SecureDongle X: " & Hex(retcode)
    curline = curline + 1
  End If
  
  stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
  uid = Val(stri)
  
  retcode = SDX_Open(1, uid, hid)
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error: " & Hex(retcode)
    curline = curline + 1
    Exit Sub
  Else
    lstOutput.List(curline) = "Succeeded Opening SecureDongle X, UID: " & (uid)
    curline = curline + 1
  End If
      
  handle = retcode
  seed = InputBox("Please input Read block index (0-4)", "Input")
  block_index = Val(seed)
  buffer = String(512, 0)
  
  retcode = SDX_Read(handle, block_index, buffer)
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error: " & Hex(retcode)
    curline = curline + 1
    Exit Sub
  End If
  lstOutput.List(curline) = "Read: " & (buffer)
  curline = curline + 1
  
  SDX_Close handle
End Sub

Private Sub cmdEncrypt_Click()
' Encrypt using RSA and store to SecureDongle X with specified UID
  retcode = SDX_Find()
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error Finding SecureDongle X: " & Hex(retcode)
    curline = curline + 1
  End If
  
  If (retcode = 0) Then
    lstOutput.List(curline) = "No SecureDongle X plugged"
    curline = curline + 1
    Exit Sub
  Else
    lstOutput.List(curline) = "Found SecureDongle X: " & Hex(retcode)
    curline = curline + 1
  End If
  
  stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
  uid = Val(stri)
  
  retcode = SDX_Open(1, uid, hid)
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error: " & Hex(retcode)
    curline = curline + 1
    Exit Sub
  Else
    lstOutput.List(curline) = "Succeeded Opening SecureDongle X, UID: " & (uid)
    curline = curline + 1
  End If
      
  handle = retcode
  seed = InputBox("Please enter start index (0-2559)", "Input", "0")
  startIndex = Val(seed)
  data = InputBox("Please enter data to encrypt", "Input", "SDX Test RSA Encrypt")
  bufferLength = Len(data)
  key512 = String(512, 0)
  retcode = SDX_RSAEncrypt(handle, startIndex, data, bufferLength, key512)
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error: " & Hex(retcode)
    curline = curline + 1
    Exit Sub
  End If
  lstOutput.List(curline) = "Write success, size: " & bufferLength & ". Key to decrypt is stored inside key512 variable."
  curline = curline + 1
  lstOutput.List(curline) = "You can try reading the contents of the SDX"
  curline = curline + 1
  
  SDX_Close handle
End Sub

Private Sub cmdDecrypt_Click()
' Read from SecureDongle X with specified UID and decrypt using RSA
  retcode = SDX_Find()
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error Finding SecureDongle X: " & Hex(retcode)
    curline = curline + 1
  End If
  
  If (retcode = 0) Then
    lstOutput.List(curline) = "No SecureDongle X plugged"
    curline = curline + 1
    Exit Sub
  Else
    lstOutput.List(curline) = "Found SecureDongle X: " & Hex(retcode)
    curline = curline + 1
  End If
  
  stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
  uid = Val(stri)
  
  retcode = SDX_Open(1, uid, hid)
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error: " & Hex(retcode)
    curline = curline + 1
    Exit Sub
  Else
    lstOutput.List(curline) = "Succeeded Opening SecureDongle X, UID: " & (uid)
    curline = curline + 1
  End If
      
  handle = retcode
  seed = InputBox("Please enter start index (0-2559)", "Input", "0")
  startIndex = Val(seed)
    
  stri = InputBox("Please enter length of data to decrypt", "Input", bufferLength)
  bufferLength = Val(stri)
    
  plainText = String(512, 0)
  retcode = SDX_RSADecrypt(handle, startIndex, plainText, bufferLength, key512)
        
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error: " & Hex(retcode)
    curline = curline + 1
    Exit Sub
  End If
  lstOutput.List(curline) = "Decrypted Data: " & plainText
  curline = curline + 1
  
  SDX_Close handle
End Sub

Private Sub cmdWriteMap_Click()
' Write to SecureDongle X with specified UID
' This is a sample of simple Data Mapping to put plain text data not in order
' For example, in 5-byte data, byte 1 will be put in SDX memory location 10, byte 2 will be put in SDX memory location 1
  retcode = SDX_Find()
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error Finding SecureDongle X: " & Hex(retcode)
    curline = curline + 1
  End If
  
  If (retcode = 0) Then
    lstOutput.List(curline) = "No SecureDongle X plugged"
    curline = curline + 1
    Exit Sub
  Else
    lstOutput.List(curline) = "Found SecureDongle X: " & Hex(retcode)
    curline = curline + 1
  End If
  
  stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
  uid = Val(stri)
  
  retcode = SDX_Open(1, uid, hid)
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error: " & Hex(retcode)
    curline = curline + 1
    Exit Sub
  Else
    lstOutput.List(curline) = "Succeeded Opening SecureDongle X, UID: " & (uid)
    curline = curline + 1
  End If
  
  handle = retcode
  seed = InputBox("Please input write block index(0-4)", "Input", "0")
  block_index = Val(seed)
  stri = InputBox("Please input data to be written (i.e. helloworld)", "Input", "SDX - Sample Map Encryption")
  
  lstOutput.List(curline) = "Original data: " & (stri)
  curline = curline + 1
  lstOutput.List(curline) = "Length = " & Len(stri)
  curline = curline + 1
  
  '****************** ENCRYPTION SAMPLE *******************
  Dim temp(512) As Byte
  Dim i As Integer, sze As Integer, indx As Integer
  If Len(stri) > 512 Then
    lstOutput.List(curline) = "Error: Data size max is 512"
    curline = curline + 1
    Exit Sub
  End If
  
  'Create a 512-byte Full Map with randomized content
  For i = 0 To 512
    If (Rnd Mod 2) = 0 Then
      temp(i) = (Rnd Mod 26) + 65
    Else
      temp(i) = (Rnd Mod 26) + 97
    End If
  Next i
  
  'Calculate where to put the data, and put it there
  For i = 0 To Len(stri) - 1
    sze = i * (Len(stri) - 1)
    indx = sze Mod 512
    temp(indx) = Asc(Mid$(stri, i + 1, 1))
  Next i
  stri = ""
  For i = 0 To 512
    stri = stri + Chr(temp(i))
  Next i
  '********************************************************
  
  'Write the Mapped data
  retcode = SDX_Write(handle, block_index, stri)
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error: " & Hex(retcode)
    curline = curline + 1
    Exit Sub
  End If
  lstOutput.List(curline) = "Data Written: " & (stri)
  curline = curline + 1
  
  SDX_Close handle
End Sub

Private Sub cmdReadMap_Click()
' Read from SecureDongle X with specified UID
' This is the sample to read the Mapped data
  retcode = SDX_Find()
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error Finding SecureDongle X: " & Hex(retcode)
    curline = curline + 1
  End If
  
  If (retcode = 0) Then
    lstOutput.List(curline) = "No SecureDongle X plugged"
    curline = curline + 1
    Exit Sub
  Else
    lstOutput.List(curline) = "Found SecureDongle X: " & Hex(retcode)
    curline = curline + 1
  End If
  
  stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
  uid = Val(stri)
  
  retcode = SDX_Open(1, uid, hid)
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error: " & Hex(retcode)
    curline = curline + 1
    Exit Sub
  Else
    lstOutput.List(curline) = "Succeeded Opening SecureDongle X, UID: " & (uid)
    curline = curline + 1
  End If
      
  Dim datasize As Integer
  handle = retcode
  seed = InputBox("Please input Read block index (0-4)", "Input")
  block_index = Val(seed)
  seed = InputBox("Please enter size of the data to be read (1-512)", "Input")
  datasize = Val(seed)
  If datasize < 1 Or datasize > 512 Then
    lstOutput.List(curline) = "Data size wrong"
    curline = curline + 1
    Exit Sub
  End If
  
  buffer = String(512, 0)
  retcode = SDX_Read(handle, block_index, buffer)
  If (retcode < 0) Then
    lstOutput.List(curline) = "Error: " & Hex(retcode)
    curline = curline + 1
    Exit Sub
  End If
 
  '******************* DECRYPTION **********************
  Dim buff As String
  Dim i As Integer, sze As Integer, indx As Integer
  buff = ""
  For i = 0 To datasize - 1
    sze = i * (datasize - 1)
    indx = sze Mod 512
    buff = buff + Mid$(buffer, indx + 1, 1)
  Next i
  '*****************************************************
  
  lstOutput.List(curline) = "Read: " & (buff)
  curline = curline + 1
  
  SDX_Close handle
End Sub
