using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using GemBox.Spreadsheet;
namespace Company.Interface.SXXK
{
    public partial class ImportHSTK_NPLTonForm : BaseForm
    {
        public Company.BLL.SXXK.ThanhKhoan.NPLNhapTonCollection nplNhapTonCollection = new BLL.SXXK.ThanhKhoan.NPLNhapTonCollection();
        public string maLoaiHinh = "N";

        public ImportHSTK_NPLTonForm()
        {
            InitializeComponent();
        }

        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }

        private void ImportHSTK_NPLTonForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            //openFileDialog1.InitialDirectory = Application.StartupPath;
            openFileDialog1.RestoreDirectory = true;

            cboDateFormat.SelectedIndex = 0;
        }

        private void btnReadFile_Click(object sender, EventArgs e)
        {
            try
            {
                cvError.Validate();
                if (!cvError.IsValid) return;
                int beginRow = Convert.ToInt32(txtRow.Value) - 1;
                if (beginRow < 0)
                {
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                    }
                    else
                    {
                        error.SetError(txtRow, "Begin row must be greater than zero ");
                    }
                    error.SetIconPadding(txtRow, 8);
                    return;

                }

                this.Cursor = Cursors.WaitCursor;
                btnReadFile.Enabled = false;
                Workbook wb = new Workbook();
                Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text, true);
                }
                catch
                {
                    MLMessages("Lỗi khi đọc file. Bạn hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                    this.Cursor = Cursors.Default;
                    btnReadFile.Enabled = true;
                    return;
                }
                try
                {
                    ws = wb.Worksheets[txtSheet.Text];
                }
                catch
                {
                    MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"", "MSG_EXC01", txtSheet.Text, false);
                    this.Cursor = Cursors.Default;
                    btnReadFile.Enabled = true;
                    return;
                }

                this.nplNhapTonCollection.Clear();
                WorksheetRowCollection wsrc = ws.Rows;

                char soTKColumn = Convert.ToChar(txtSTKColumn.Text);
                int soTKCol = ConvertCharToInt(soTKColumn);

                char maLHColumn = Convert.ToChar(txtMLHColumn.Text);
                int maLHCol = ConvertCharToInt(maLHColumn);

                char ngayDKColumn = Convert.ToChar(txtNgayDKColumn.Text);
                int ngayDKCol = ConvertCharToInt(ngayDKColumn);

                char maNPLColumn = Convert.ToChar(txtMaNPL.Text);
                int maNPLCol = ConvertCharToInt(maNPLColumn);

                char tonNPLColumn = Convert.ToChar(txtLuongTon.Text);
                int tonNPLCol = ConvertCharToInt(tonNPLColumn);

                char nhapNPLColumn = Convert.ToChar('G');
                int nhapNPLCol = ConvertCharToInt(nhapNPLColumn);

                int soChuaCo = 0;
                int soDaCo = 0;
                int soTuExcel = 0;
                string thongBao = "";
                string thongTinTonChuaCo = "";

                //Hungtq update 23/03/2011.
                System.Globalization.CultureInfo cul = null;
                if (cboDateFormat.SelectedItem.ToString() == "dd/MM/yyyy")
                    cul = new System.Globalization.CultureInfo("vi-VN");
                else if (cboDateFormat.SelectedItem.ToString() == "MM/dd/yyyy")
                    cul = new System.Globalization.CultureInfo("en-US");

                foreach (WorksheetRow wsr in wsrc)
                {
                    if (wsr.Index >= beginRow)
                    {
                        try
                        {
                            if (wsr.Cells[maNPLCol].Value == null)
                                break;

                            Company.BLL.SXXK.ThanhKhoan.NPLNhapTon nplNhapTon = new BLL.SXXK.ThanhKhoan.NPLNhapTon();
                            nplNhapTon.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            nplNhapTon.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            try
                            {
                                nplNhapTon.SoToKhai = Convert.ToInt32(wsr.Cells[soTKCol].Value);

                                nplNhapTon.MaLoaiHinh = Convert.ToString(wsr.Cells[maLHCol].Value);

                                if (nplNhapTon.MaLoaiHinh.Substring(0, 1) != "N")
                                    continue;
                            }
                            catch (Exception) { nplNhapTon.SoToKhai = 0; }

                            if (nplNhapTon.SoToKhai != 0)
                            {
                                soTuExcel++;
                                //nplNhapTon.MaLoaiHinh = "NSX03";

                                if (wsr.Cells[ngayDKCol].Value.GetType().Name == "String")
                                    nplNhapTon.NgayDangKy = Convert.ToDateTime(wsr.Cells[ngayDKCol].Value, cul);
                                else if (wsr.Cells[ngayDKCol].Value.GetType().Name == "Double")
                                    nplNhapTon.NgayDangKy = System.DateTime.FromOADate((double)wsr.Cells[ngayDKCol].Value);

                                nplNhapTon.NamDangKy = (short)nplNhapTon.NgayDangKy.Year;
                                nplNhapTon.MaNPL = Convert.ToString(wsr.Cells[maNPLCol].Value);

                                //Test
                                //if(nplNhapTon.MaNPL=="W1" && nplNhapTon.SoToKhai==2452)
                                //{
                                //}

                                try
                                {
                                    nplNhapTon.Ton = Math.Round(Convert.ToDecimal(wsr.Cells[tonNPLCol].Value), GlobalSettings.SoThapPhan.LuongNPL);
                                }
                                catch (Exception)
                                {
                                    nplNhapTon.Ton = 0;
                                }

                                if (chkHienThiTon0.Checked)
                                {
                                    if (nplNhapTon.Load())// có thông tin tồn thì mới cập nhật
                                    {
                                        nplNhapTon.Ton = Math.Round(Convert.ToDecimal(wsr.Cells[tonNPLCol].Value), GlobalSettings.SoThapPhan.LuongNPL);
                                        //nplNhapTon.Luong = nplNhapTon.Ton;
                                        this.nplNhapTonCollection.Add(nplNhapTon);
                                        soDaCo++;
                                    }
                                    else
                                    {
                                        soChuaCo++;
                                        thongTinTonChuaCo += "\r\n; MaNPL = " + nplNhapTon.MaNPL + ", SoToKhai = " + nplNhapTon.SoToKhai + ", NamDangKy = " + nplNhapTon.NamDangKy;
                                    }
                                }
                                else
                                {
                                    if (nplNhapTon.Ton != 0)
                                    {
                                        if (nplNhapTon.Load())// có thông tin tồn thì mới cập nhật
                                        {
                                            nplNhapTon.Ton = Math.Round(Convert.ToDecimal(wsr.Cells[tonNPLCol].Value), GlobalSettings.SoThapPhan.LuongNPL);
                                            //nplNhapTon.Luong = nplNhapTon.Ton;
                                            this.nplNhapTonCollection.Add(nplNhapTon);
                                            soDaCo++;
                                        }
                                        else
                                        {
                                            soChuaCo++;
                                            thongTinTonChuaCo += "\r\n; MaNPL = " + nplNhapTon.MaNPL + ", SoToKhai = " + nplNhapTon.SoToKhai + ", NamDangKy = " + nplNhapTon.NamDangKy;
                                        }
                                    }
                                    //else
                                    //{
                                    //    decimal nhapTemp = 0;
                                    //    try
                                    //    {
                                    //        nhapTemp = Math.Round(Convert.ToDecimal(wsr.Cells[nhapNPLCol].Value), GlobalSettings.SoThapPhan.LuongNPL);
                                    //    }
                                    //    catch (Exception)
                                    //    {
                                    //        nhapTemp = 0;
                                    //    }
                                    //    if (nhapTemp != 0)
                                    //    {
                                    //        if (nplNhapTon.Load())// có thông tin tồn thì mới cập nhật
                                    //        {
                                    //            nplNhapTon.Ton = nhapTemp;
                                    //            //nplNhapTon.Luong = nhapTemp;
                                    //            this.nplNhapTonCollection.Add(nplNhapTon);
                                    //        }
                                    //    }
                                    //}
                                }
                            }
                        }
                        catch
                        {
                            if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\n Bạn có muốn tiếp tục không?", "MSG_EXC06", "", true) != "Yes")
                            {
                                this.nplNhapTonCollection.Clear();
                                this.Cursor = Cursors.Default;
                                btnReadFile.Enabled = true;
                                return;
                            }
                        }
                    }
                }
                //if (soTKChuaCo > 0)
                {
                    thongBao = "Tổng cộng có: " + soTuExcel + " dữ liệu tồn từ file excel";
                    thongBao += ", " + soDaCo + " tồn đã có";
                    thongBao += ", " + soChuaCo + " tồn chưa có";
                    if (soChuaCo > 0)
                        thongBao += thongTinTonChuaCo;
                    MLMessages(thongBao, "", "", false);
                }
                this.Cursor = Cursors.Default;
                btnReadFile.Enabled = true;


            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                //if (e.Row.RowType == RowType.Record)
                //    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

        }
    }
}