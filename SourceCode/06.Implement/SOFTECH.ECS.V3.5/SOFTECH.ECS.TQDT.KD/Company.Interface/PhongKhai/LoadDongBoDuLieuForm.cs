﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using Company.KD.BLL;
using Company.KD.BLL.KDT;


namespace Company.Interface.PhongKhai
{
    public partial class LoadDongBoDuLieuForm : Company.Interface.BaseForm
    {
        private int value = 0;
        public LoadDongBoDuLieuForm()
        {
            InitializeComponent();
        }
        public static bool chkErrConn = false;
        
        
        //Lấy dữ liệu Khai điện tử về từ hệ thống Hải Quan.
       public DataSet GetDataKhaiDT(string sql, string MaHaiQuan, string database)
        {
            DataSet ds = new DataSet();
            try
            {
                ds = new DoanhNghiep().GetKhaiDT(sql);
            }
            catch (Exception ex)
            {
                ShowMessage("Không kết nối được với hệ thống chi cục Hải quan, đồng bộ dữ liệu Gia Công không thành công", false);
            }
            return ds;
        }
        
        #region KHAIDT
        public void UpdateHMDKDT(string MaHaiQuan, string MaDN)
        {
            try{
                ToKhaiMauDich tkmd=new ToKhaiMauDich();
                tkmd.DongBoDuLieuKhaiDT(GlobalSettings.MA_DON_VI,GlobalSettings.MA_HAI_QUAN,"KhaiDT");               
            }
            catch (Exception ex)
            {
                ShowMessage(" Lỗi : " + ex.Message, false);
            }
        }
        #endregion
        

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (ShowMessage("Bạn có chắc chắn là cập nhật không ?", true) == "Yes")
            {
                if (txtDatabase.Text.Trim() == "SXXK")
                {
                    lblTrangthai.Text = "Đang đồng bộ dữ liệu .....";
                    backgroundWorker1.RunWorkerAsync(lblTrangthai);
                    
                 
                }
            }
        }

     

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DongBoDuLieuByDNForm_Load(object sender, EventArgs e)
        {
            timer1.Start();
            timer1.Enabled = true;
            lblTrangthai.Text = " ";
            txtMaHQ.Text = GlobalSettings.MA_HAI_QUAN;
            txtMaDN.Text = GlobalSettings.MA_DON_VI;
            txtTenDN.Text = GlobalSettings.TEN_DON_VI;
            txtDatabase.Text = "KhaiDT";
            if (txtDatabase.Text.Trim() == "KhaiDT")
            {
                lblTrangthai.Text = "Đang đồng bộ dữ liệu từ hệ thống Hải quan...";
                backgroundWorker1.RunWorkerAsync(lblTrangthai);
            }
           
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

            UpdateHMDKDT(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());            
            //updateSPSXXK(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
            if (chkErrConn == true)
                ShowMessage("Không kết nối được với hệ thống chi cục Hải quan, đồng bộ dữ liệu không thành công", false);
            //updateDMSXXK(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
          
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.step = 5;
            this.value = 100;
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }
        private int step = 1;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (uiProgressBar1.Value == 100) this.Close();
            if (this.value < 100)
            {
                this.value+=step;
                if (value > 99)
                    value = 100;

            }
            else
            {
                if (uiProgressBar1.Value < 99) this.value = uiProgressBar1.Value + 1;
            }
            uiProgressBar1.Value = this.value;

        }

        private void uiProgressBar1_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void LoadDongBoDuLieuForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (uiProgressBar1.Value < 100) e.Cancel = true; 
        }
    }
}

