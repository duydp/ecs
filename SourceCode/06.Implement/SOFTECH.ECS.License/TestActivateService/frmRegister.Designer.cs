﻿namespace TestActivateService
{
    partial class frmRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegister));
            this.txtFile = new Janus.Windows.GridEX.EditControls.EditBox();
            this.radioFile = new System.Windows.Forms.RadioButton();
            this.radioCDKey = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCDKey = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtCodeActivate = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnActivate = new Janus.Windows.EditControls.UIButton();
            this.btnCancel = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtFile
            // 
            this.txtFile.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtFile.Enabled = false;
            this.txtFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFile.Location = new System.Drawing.Point(223, 203);
            this.txtFile.Name = "txtFile";
            this.txtFile.ReadOnly = true;
            this.txtFile.Size = new System.Drawing.Size(245, 21);
            this.txtFile.TabIndex = 47;
            this.txtFile.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtFile.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtFile.ButtonClick += new System.EventHandler(this.txtFile_ButtonClick);
            // 
            // radioFile
            // 
            this.radioFile.AutoSize = true;
            this.radioFile.BackColor = System.Drawing.Color.Transparent;
            this.radioFile.ForeColor = System.Drawing.Color.Black;
            this.radioFile.Location = new System.Drawing.Point(317, 153);
            this.radioFile.Name = "radioFile";
            this.radioFile.Size = new System.Drawing.Size(117, 17);
            this.radioFile.TabIndex = 45;
            this.radioFile.TabStop = true;
            this.radioFile.Text = "Chọn tệp kích hoạt";
            this.radioFile.UseVisualStyleBackColor = false;
            this.radioFile.CheckedChanged += new System.EventHandler(this.radioCDKey_CheckedChanged);
            // 
            // radioCDKey
            // 
            this.radioCDKey.AutoSize = true;
            this.radioCDKey.BackColor = System.Drawing.Color.Transparent;
            this.radioCDKey.Checked = true;
            this.radioCDKey.ForeColor = System.Drawing.Color.Black;
            this.radioCDKey.Location = new System.Drawing.Point(223, 153);
            this.radioCDKey.Name = "radioCDKey";
            this.radioCDKey.Size = new System.Drawing.Size(90, 17);
            this.radioCDKey.TabIndex = 46;
            this.radioCDKey.TabStop = true;
            this.radioCDKey.Text = "Nhập CD Key";
            this.radioCDKey.UseVisualStyleBackColor = false;
            this.radioCDKey.CheckedChanged += new System.EventHandler(this.radioCDKey_CheckedChanged);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(146, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(349, 18);
            this.label5.TabIndex = 44;
            this.label5.Text = "Khi kích hoạt phần mềm bằng CD Key, vui lòng kiểm tra kết nối internet.";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCDKey
            // 
            this.txtCDKey.BackColor = System.Drawing.Color.White;
            this.txtCDKey.Location = new System.Drawing.Point(223, 176);
            this.txtCDKey.MaxLength = 25;
            this.txtCDKey.Name = "txtCDKey";
            this.txtCDKey.Size = new System.Drawing.Size(245, 20);
            this.txtCDKey.TabIndex = 42;
            this.txtCDKey.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            // 
            // txtCodeActivate
            // 
            this.txtCodeActivate.BackColor = System.Drawing.SystemColors.Info;
            this.txtCodeActivate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodeActivate.Location = new System.Drawing.Point(223, 126);
            this.txtCodeActivate.MaxLength = 25;
            this.txtCodeActivate.Name = "txtCodeActivate";
            this.txtCodeActivate.ReadOnly = true;
            this.txtCodeActivate.Size = new System.Drawing.Size(245, 21);
            this.txtCodeActivate.TabIndex = 43;
            this.txtCodeActivate.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(146, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "Tệp kích hoạt:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(176, 181);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 41;
            this.label2.Text = "CD key:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(150, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 39;
            this.label1.Text = "Mã kích hoạt:";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(12, 108);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(128, 128);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 38;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(501, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 37;
            this.pictureBox1.TabStop = false;
            // 
            // btnActivate
            // 
            this.btnActivate.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnActivate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActivate.Location = new System.Drawing.Point(317, 235);
            this.btnActivate.Name = "btnActivate";
            this.btnActivate.Size = new System.Drawing.Size(82, 23);
            this.btnActivate.TabIndex = 35;
            this.btnActivate.Text = "Kích hoạt";
            this.btnActivate.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnActivate.Click += new System.EventHandler(this.btnActivate_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(403, 235);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 23);
            this.btnCancel.TabIndex = 36;
            this.btnCancel.Text = "Thoát";
            this.btnCancel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 266);
            this.Controls.Add(this.txtFile);
            this.Controls.Add(this.radioFile);
            this.Controls.Add(this.radioCDKey);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCDKey);
            this.Controls.Add(this.txtCodeActivate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnActivate);
            this.Controls.Add(this.btnCancel);
            this.Name = "frmRegister";
            this.Text = "frmRegister";
            this.Load += new System.EventHandler(this.frmRegister_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Janus.Windows.GridEX.EditControls.EditBox txtFile;
        private System.Windows.Forms.RadioButton radioFile;
        private System.Windows.Forms.RadioButton radioCDKey;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtCDKey;
        private Janus.Windows.GridEX.EditControls.EditBox txtCodeActivate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Janus.Windows.EditControls.UIButton btnActivate;
        private Janus.Windows.EditControls.UIButton btnCancel;
    }
}