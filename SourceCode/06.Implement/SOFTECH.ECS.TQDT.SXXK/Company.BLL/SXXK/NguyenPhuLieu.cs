﻿using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Transactions;
using System;
using Company.BLL.SXXK.ToKhai;
using System.Data.SqlClient;

namespace Company.BLL.SXXK
{
    public partial class NguyenPhuLieu
    {
        public void Saved(NguyenPhuLieuCollection collection)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (NguyenPhuLieu item in collection)
                    {
                        InsertUpdateTransaction(transaction);
                    }
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public DataSet SearchBy_MaHaiQuan(string maHaiQuan)
        {
            string where = string.Format("MaHaiQuan = '{0}'", maHaiQuan.PadRight(6));
            return this.SelectDynamic(where, "Ma");
        }

        //public DataSet WS_GetDanhSachDaDangKy(string maHaiQuan, string maDoanhNghiep)
        //{
        //    SXXKService service = new SXXKService();
        //    return service.NguyenPhuLieu_GetDanhSach(maHaiQuan, maDoanhNghiep);
        //}
        public static NguyenPhuLieuCollection GetNPLXML(string maHaiQuan, string maDN)
        {

            string strName = " select *  from t_SXXK_NguyenPhuLieu where MaHaiQuan='" + maHaiQuan + "' AND MaDoanhNghiep='" + maDN + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(strName);
            IDataReader reader = db.ExecuteReader(dbCommand);
            NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();
            while (reader.Read())
            {
                NguyenPhuLieu entity = new NguyenPhuLieu();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public bool UpdateRegistedToDatabase(string maHaiQuan, string maDoanhNghiep, DataSet ds)
        {
            NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.MaHaiQuan = maHaiQuan;
                npl.MaDoanhNghiep = maDoanhNghiep;
                npl.Ma = row["Ma"].ToString().Trim();
                npl.Ten = row["Ten"].ToString().Trim();
                npl.MaHS = row["MaHS"].ToString().Trim();
                int k = npl.MaHS.Length;
                //if (npl.MaHS.Length < 8)
                //{
                //    for (int i = 1; i <= 10 - k; ++i)
                //        npl.MaHS += "0";
                //}
                npl.DVT_ID = row["DVT_ID"].ToString();
                collection.Add(npl);
            }
            NguyenPhuLieu NPL = new NguyenPhuLieu();
            return NPL.InsertUpdate(collection);
        }

        public static void NhapNPLXML(NguyenPhuLieuCollection NPLColl)
        {
            NguyenPhuLieu npl = new NguyenPhuLieu();
            try { npl.InsertUpdate(NPLColl); }
            catch { }
        }

        public bool UpdateRegistedToDatabaseMoi(string maHaiQuan, string maDoanhNghiep, DataSet ds)
        {
            //NguyenPhuLieuCollection collection = new NguyenPhuLieuCollection();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.MaHaiQuan = maHaiQuan;
                npl.MaDoanhNghiep = maDoanhNghiep;
                npl.Ma = row["Ma"].ToString().Trim();
                npl.Ten = row["Ten"].ToString().Trim();
                npl.MaHS = row["MaHS"].ToString().Trim();
                int k = npl.MaHS.Length;
                //if (npl.MaHS.Length < 8)
                //{
                //    for (int i = 1; i <= 10 - k; ++i)
                //        npl.MaHS += "0";
                //}
                npl.DVT_ID = row["DVT_ID"].ToString();
                try
                {
                    npl.Insert();
                }
                catch { }
            }
            return true;
        }

        public DataTable GetSoHopDongGiaCong()
        {
            SqlDatabase dbGC = (SqlDatabase)DatabaseFactory.CreateDatabase("ECS_GC");
            string sql = "SELECT ID,SoHopDong FROM t_KDT_GC_HopDong";
            DbCommand dbcommand = dbGC.GetSqlStringCommand(sql);
            return dbGC.ExecuteDataSet(dbcommand).Tables[0];
        }
        public DataTable GetDanhSachNPLCuaHopDong(long idHopDong)
        {
            SqlDatabase dbGC = (SqlDatabase)DatabaseFactory.CreateDatabase("ECS_GC");
            string sql = "SELECT a.Ma,a.Ten,a.MaHS,b.Ten as DVT FROM t_GC_NguyenPhuLieu a INNER JOIN t_HaiQuan_DonViTinh b ON a.DVT_ID = b.ID WHERE HopDong_ID =" + idHopDong;
            DbCommand dbcommand = dbGC.GetSqlStringCommand(sql);
            return dbGC.ExecuteDataSet(dbcommand).Tables[0];

        }
        public void InsertNPLMapping(DataTable dt, long idHD)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (TransactionScope trans = new TransactionScope())
            {
                try
                {
                    string sql = "DELETE FROM t_SXXK_NPLMapping WHERE SoHopDong ='" + idHD + "'";
                    DbCommand dbcommand = db.GetSqlStringCommand(sql);
                    db.ExecuteNonQuery(dbcommand);
                    sql = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        sql = "INSERT INTO t_SXXK_NPLMapping VALUES ('" + dr["Ma"].ToString() + "', '" + dr["MaNPLMoi"].ToString() + "','" + idHD + "')";
                        dbcommand = db.GetSqlStringCommand(sql);
                        db.ExecuteNonQuery(dbcommand);
                    }
                    trans.Complete();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public static string GetNPLTuongUng(string maMoi, string idHD)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "SELECT MaNPL FROM t_SXXK_NPLMapping WHERE SoHopDong ='" + idHD + "' AND MaNPLMoi = '" + maMoi + "'";
            DbCommand dbcommand = db.GetSqlStringCommand(sql);
            string temp = maMoi;
            try
            {
                if (Convert.ToString(db.ExecuteScalar(dbcommand)) != "")
                    temp = Convert.ToString(db.ExecuteScalar(dbcommand));
            }
            catch
            { }
            return temp;

        }
        public static string GetNPLTuongUng(string maMoi, string idHD, Company.BLL.SXXK.ThanhKhoan.NPLNhapTon NPLNhapTon)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "SELECT MaNPL FROM t_SXXK_NPLMapping WHERE SoHopDong ='" + idHD + "' AND MaNPLMoi = '" + maMoi + "'";
            DbCommand dbcommand = db.GetSqlStringCommand(sql);
            DataTable dt = db.ExecuteDataSet(dbcommand).Tables[0];
            DataTable dtNPLNhapTon = NPLNhapTon.SelectBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy().Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataRow drNhapTon in dtNPLNhapTon.Rows)
                {
                    if (dr["MaNPL"].ToString().Trim().ToUpper() == drNhapTon["MaNPL"].ToString().Trim().ToUpper())
                    {
                        return dr["MaNPL"].ToString();
                    }
                }
            }
            return maMoi;


        }
        public DataTable GetDanhSachNPL(string soHopDong)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sql = "SELECT b.*, a.MaNPLMoi,a.SoHopDong FROM t_SXXK_NPLMapping a INNER JOIN t_SXXK_NguyenPhuLieu b ON a.MaNPL = b.Ma WHERE SoHopDong = '" + soHopDong + "'";
            DbCommand dbcommand = db.GetSqlStringCommand(sql);
            DataTable dt = db.ExecuteDataSet(dbcommand).Tables[0];
            if (dt.Rows.Count == 0)
            {
                dt = this.SelectAll().Tables[0];
                dt.Columns.Add("MaNPLMoi", typeof(string));
                dt.Columns.Add("SoHopDong", typeof(string));
            }

            return dt;

        }

        public static NguyenPhuLieu getNguyenPhuLieu(string maHaiQuan, string maDoanhNghiep, string maNPL)
        {
            string sql = "SELECT * from t_SXXK_NguyenPhuLieu " +
                        " where  MaHaiQuan='" + maHaiQuan + "' AND MaDoanhNghiep = '" + maDoanhNghiep + "' AND Ma = '" + maNPL + "'";
            Database db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand dbc = db.GetSqlStringCommand(sql);


            IDataReader reader = db.ExecuteReader(dbc);
            NguyenPhuLieu entity = null;
            while (reader.Read())
            {
                entity = new NguyenPhuLieu();
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));

            }
            return entity;
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_SXXK_NguyenPhuLieu_Delete";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_SXXK_NguyenPhuLieu_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public bool InsertUpdate(NguyenPhuLieuCollection collection, SqlTransaction transaction)
        {
            bool ret = true;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();

                try
                {
                    //bool ret01 = true;
                    foreach (NguyenPhuLieu item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            //ret01 = false;
                            break;
                        }
                    }
                    //if (ret01)
                    //{
                    //    transaction.Commit();
                    //    ret = true;
                    //}
                    //else
                    //{
                    //    transaction.Rollback();
                    //    ret = false;
                    //}
                }
                catch(Exception ex)
                {
                    ret = false;
                    //transaction.Rollback();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw;

                }
                finally
                {
                    connection.Close();

                }
            }
            return ret;
        }

        //DATLMQ bổ sung Tìm kiếm Nguyên phụ liệu theo UserName khai báo ngày 24/06/2011
        public DataSet GetNPLFromUserName(string userName)
        {
            string query = "SELECT npl.* FROM t_kdt_sxxk_NguyenPhuLieu npl INNER JOIN t_kdt_sxxk_NguyenPhuLieuDangKy npldk " +
                           "ON npldk.id = npl.Master_id WHERE npldk.id in (SELECT ID_DK FROM t_kdt_sxxk_LogKhaiBao WHERE " +
                           "LoaiKhaiBao = 'NPL' AND UserNameKhaiBao = '" + userName + "')";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(query);
            return this.db.ExecuteDataSet(dbCommand);
        }
    }
}
