using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.SXXK.ThanhKhoan
{
	public partial class BCXuatNhapTon 
	{
        public DataTable GetToKhaiNhapDaPhanBo(DateTime minDate, DateTime maxDate)
        {
            string sql = 
                         "SELECT DISTINCT SoToKhaiNhap as SoToKhai ,Year(NgayDangKyNhap) as NamDangKy ,MaHaiQuan, MaLoaiHinhNhap as MaLoaiHinh FROM t_SXXK_BCXuatNhapTon " +
                         "WHERE NgayDangKyNhap BETWEEN @MinDate AND @MaxDate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MinDate", DbType.DateTime, minDate);
            db.AddInParameter(dbCommand, "@MaxDate", DbType.DateTime, maxDate);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public DataTable GetToKhaiXuatDaPhanBo(DateTime minDate, DateTime maxDate)
        {
            string sql = 
                         "SELECT DISTINCT SoToKhaiXuat as SoToKhai,Year(NgayDangKyXuat) as NamDangKy, MaHaiQuan, MaLoaiHinhXuat as MaLoaiHinh FROM t_SXXK_BCXuatNhapTon " +
                         "WHERE NgayDangKyXuat BETWEEN @MinDate AND @MaxDate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@MinDate", DbType.DateTime, minDate);
            db.AddInParameter(dbCommand, "@MaxDate", DbType.DateTime, maxDate);
            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public DataSet GetPhanBoToKhaiXuat(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan)
        {
            string sp = "p_SXXK_GetPhanBoToKhaiXuat";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(sp);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, soToKhai);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHaiQuan);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, maLoaiHinh);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, namDangKy);
            return db.ExecuteDataSet(dbCommand);
        }
	}	
}