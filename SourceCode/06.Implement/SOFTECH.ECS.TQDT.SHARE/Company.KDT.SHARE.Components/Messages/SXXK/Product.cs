﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class Product
    {
        /// <summary>
        /// Thông tin hàng
        /// </summary>
        [XmlElement("Commodity")]
        public Commodity Commodity { get; set; }
        /// <summary>
        /// Số lượng và đơn vị tính
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }
        //[XmlElement("CurrencyExchange")]
        //public CurrencyExchange CurrencyEX { get; set; }
        //[XmlElement("Origin")]
        //public Origin Orig { get; set; }
        //[XmlElement("CustomsValue")]
        //public CustomsValue CustomVLue { get; set; }
        //[XmlElement("status")]
        //public string Status { get; set; }
    }
}
