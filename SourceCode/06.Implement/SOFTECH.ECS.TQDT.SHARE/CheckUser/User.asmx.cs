﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.Data;
using CheckUser.Bll;

namespace CheckUser
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
        [WebMethod]
        public bool CheckTinhTrang(string maMayKichHoat)
        {
            UserInfor entity = new UserInfor();
            IList<UserInfor> listEntity = UserInfor.SelectCollectionDynamic("MaMayKichHoat like '"+maMayKichHoat+"'","ID");
            if (listEntity.Count <= 0)
            {
                return false;
            }
            else
            {
                if (listEntity[listEntity.Count].NgayHetHan.CompareTo(System.DateTime.Today) > 0)
                    return true;
                else 
                    return false;
            }
        }
        [WebMethod]
        public UserInfor getInfor(string maMayKichHoat)
        {
            try
            {
                IList<UserInfor> listEntity = UserInfor.SelectCollectionDynamic("MaMayKichHoat like '" + maMayKichHoat + "'", "ID");
                if (listEntity.Count <= 0)
                {
                    return null;
                }
                else
                {
                    return listEntity[listEntity.Count -1];
                }
            }
            catch (Exception ex)
            {
                Console.Out.Write(ex.Message);
                return null;
            }
 
        }
        [WebMethod]
        public UserInfor InsertUser(UserInfor entity)
        {
            //entity.KeyLicense = this.GenerateKey("Express", entity.MaMayKichHoat);
            //entity.NgayHetHan = System.DateTime.Today.AddMonths(6);
            if (entity.Insert() > 0)
            {
                return entity;
            }
            else
                return null;
        }
        private String GenerateKey(string pPassPhrase, string pClientName)
        {
                string strRegkey = Encrypt(ref pPassPhrase, pClientName.ToUpper());
                if (String.IsNullOrEmpty(strRegkey) == false)
                {
                    strRegkey = strRegkey.Insert(4, "-");
                    strRegkey = strRegkey.Insert(9, "-");
                    strRegkey = strRegkey.Insert(14, "-");
                    strRegkey = strRegkey.Insert(19, "-");
                    strRegkey = strRegkey.Insert(24, "-");
                    strRegkey = strRegkey.Insert(29, "-");
                    strRegkey = strRegkey.Insert(34, "-");
                    return  strRegkey;
                }
                else
                {
                    string loi = "";
                    return loi;
                }
        }
        private string Encrypt(ref string pPassPhrase, string pTextToEncrypt)
        {
            if (pPassPhrase.Length > 16)
            {
                //'limitation of the encryption mechanism
                pPassPhrase = pPassPhrase.Substring(0, 16);
            }

            if (pTextToEncrypt.Trim().Length == 0)
            {
                //'the Text to encrypt not set!!!
                return string.Empty;
            }
            Authentication.Encryption.Data skey = new Authentication.Encryption.Data(pPassPhrase);
            Authentication.Encryption.Symmetric sym = new Authentication.Encryption.Symmetric(Authentication.Encryption.Symmetric.Provider.Rijndael, true);
            Authentication.Encryption.Data objEncryptedData;
            objEncryptedData = sym.Encrypt(new Authentication.Encryption.Data(pTextToEncrypt), skey);
            return objEncryptedData.ToHex();
        }
        
    }
}
