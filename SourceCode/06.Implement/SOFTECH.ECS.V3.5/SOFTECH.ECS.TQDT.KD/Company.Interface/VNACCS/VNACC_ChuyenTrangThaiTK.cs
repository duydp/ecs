﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using Company.BLL.KDT;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.VNACCS
{
    public partial class VNACC_ChuyenTrangThaiTK : BaseForm
    {
        KDT_VNACC_ToKhaiMauDich tkmd;
        public VNACC_ChuyenTrangThaiTK(KDT_VNACC_ToKhaiMauDich tk)
        {
            InitializeComponent();
            this.tkmd = tk;
            this.Text = this.Text + " " + this.tkmd.ID.ToString();
            this.lblCaption.Text = this.lblCaption.Text + " " + this.tkmd.ID.ToString();
            this.lblMaToKhai.Text = this.tkmd.ID.ToString();
            this.lblMaLoaiHinh.Text = this.tkmd.MaLoaiHinh;
            this.cbPL.SelectedValue = this.tkmd.MaPhanLoaiKiemTra;

            //Hungtq, cap nhat 30/07/2010. Neu to khai chuyen trang thai da co So to khai, ngay dang ky thi tu dong hien thi So to khai va ngay dang ky do tren form.
            if (tkmd.SoToKhai > 0)
            {
                txtSoToKhai.Text = tkmd.SoToKhai.ToString();
                ccNgayDayKy.Text = tkmd.NgayDangKy.ToString();
            }
        }

        private void btnThuchien_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.validData())
                {
                    this.getData();
                    //this.tkmd.LoadHMDCollection();

                    /*Company.BLL.SXXK.ToKhai.ToKhaiMauDich tkDaDangKy = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    tkDaDangKy.MaHaiQuan = tkmd.MaHaiQuan;
                    tkDaDangKy.NamDangKy = (short)tkmd.NgayDangKy.Year;
                    tkDaDangKy.MaLoaiHinh = tkmd.MaLoaiHinh;
                    tkDaDangKy.SoToKhai = tkmd.SoToKhai;
                    tkDaDangKy.PhanLuong = tkmd.PhanLuong;
                    //Hungtq, Bo sung ngay dang ky to khai. 30/07/2010.
                    tkDaDangKy.NgayDangKy = tkmd.NgayDangKy;
                     */
                    KDT_VNACC_ToKhaiMauDich tkDaDangKy = KDT_VNACC_ToKhaiMauDich.Load(tkmd.ID);
                    tkDaDangKy.ID = tkmd.ID;
                    tkDaDangKy.CoQuanHaiQuan = tkmd.CoQuanHaiQuan;
                    tkDaDangKy.NgayDangKy = tkmd.NgayDangKy;
                    tkDaDangKy.MaLoaiHinh = tkmd.MaLoaiHinh;
                    tkDaDangKy.SoToKhai = tkmd.SoToKhai;
                    tkDaDangKy.MaPhanLoaiKiemTra = tkmd.MaPhanLoaiKiemTra;

                    this.tkmd.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET.ToString();
                    //this.tkmd.TransgferDataToSXXK();

                    tkDaDangKy.Update(null);

                    /*#region  Ghi vào Kết quả xử lý

                    KetQuaXuLy kqxl = new KetQuaXuLy();

                    kqxl.ItemID = this.tkmd.ID;
                    //Hungtq update
                    
                    kqxl.LoaiChungTu = KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxl.LoaiThongDiep = KetQuaXuLy.LoaiThongDiep_ToKhaiChuyenTT;

                    string tenluong = "Xanh";
                    if (this.tkmd.MaKetQuaKiemTra == TrangThaiPhanLuong.LUONG_VANG.ToString())
                        tenluong = "Vàng";
                    else if (this.tkmd.MaKetQuaKiemTra == TrangThaiPhanLuong.LUONG_DO.ToString())
                        tenluong = "Đỏ";

                    kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}", this.tkmd.SoToKhai, this.tkmd.NgayDangKy.ToShortDateString(), this.tkmd.MaLoaiHinh.Trim(), this.tkmd.CoQuanHaiQuan.Trim(), tenluong);
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();
                    Company.KDT.SHARE.Components.MessageTypes msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiNhap;
                    if (tkmd.MaLoaiHinh.Substring(0, 1) == "X")
                        msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiXuat;
                    #endregion
                     */
                    if (MLMessages("Chuyển trạng thái thành công", "MSG_THK105", "", false) == "Cancel")
                    {
                        this.Close();
                    }
                    
                }
                else
                {
                    // ShowMessage("Dữ liệu nhập vào chưa hợp lệ! \nVui lòng nhập lại.", false);
                    MLMessages("Dữ liệu nhập vào chưa hợp lệ! \nVui lòng nhập lại.", "MSG_THK107", "", false);
                }
            }
            catch (Exception ex)
            {
                MLMessages(ex.ToString(), "", "", false);
            }
        }

        private bool validData()
        {
            bool value = true;
            if (txtSoToKhai.Text == "") value = false;
            return value;
        }

        private void getData()
        {
            this.tkmd.SoToKhai = Convert.ToDecimal(txtSoToKhai.Text);
            this.tkmd.NgayDangKy = ccNgayDayKy.Value;
            this.tkmd.MaPhanLoaiKiemTra = cbPL.SelectedValue.ToString();
            if (this.tkmd.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO.ToString())
            {
                //this.tkmd.NgayTiepNhan = this.tkmd.NgayDangKy;
                this.tkmd.NgayDangKy = this.tkmd.NgayDangKy;
            }
        }

        private void btnHuybo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}