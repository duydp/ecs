using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ThanhKhoan;
using Company.Interface.SXXK;
using Company.BLL.SXXK;
using Infragistics.Excel;
using System.Reflection;
using Company.Interface.Report ;
using Company.Interface.Report.SXXK;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;
//using Company.BLL.KDT.SXXK ;
namespace Company.Interface
{
    public partial class ThongkeHangTonForm : BaseForm
    {
        private NPLNhapTonThucTe NPLTon = new NPLNhapTonThucTe();
        //public IList<NPLNhapTonThucTe> Collection = new List<NPLNhapTonThucTe>();
        public Company.BLL.KDT.SXXK.NPLNhapTonCollection Collection = new Company.BLL.KDT.SXXK.NPLNhapTonCollection();
        ToKhaiMauDichCollection TKMDChuaPhanBoCollection = new ToKhaiMauDichCollection();
        public double ValueOld = 0;
        public ThongkeHangTonForm()
        {
            InitializeComponent();
        }
        
        public bool check = true ;

        private void ThongkeHangTonForm_Load(object sender, EventArgs e)
        {
            txtNamTiepNhan.Value = DateTime.Today.Year;
            dgList.Tables[0].Columns["LuongTon"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgListNPLTK.Tables[0].Columns["Luong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgListNPLTK.Tables[0].Columns["Ton"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgListPhanBo.Tables[0].Columns["DinhMucChung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgListPhanBo.Tables[0].Columns["SoLuongXuat"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
            dgListPhanBo.Tables[0].Columns["NhuCau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgListPhanBo.Tables[0].Columns["LuongTonDau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgListPhanBo.Tables[0].Columns["LuongPhanBo"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgListPhanBo.Tables[0].Columns["LuongCungUng"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgListPhanBo.Tables[0].Columns["LuongTonCuoi"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;


            dgListToKhai.Visible=false;
            dglistTKTon.Visible = false;
           
            loaiHinhMauDichHControl1.Nhom = "XSX";
            loaiHinhMauDichHControl1.loadData();
            loaiHinhMauDichHControl1.Ma = "XSX01";
            BindDataNPLTonThucTe();
            searchToKhaiTonThucTe();
        }
        private void BindDataToKhaiNPLTon()
        {
            //Collection = NPLNhapTonThucTe.SelectCollectionDynamic("MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "' and MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'", "");
            Collection = new Company.BLL.KDT.SXXK.NPLNhapTon().SelectCollectionDynamic(" MaDoanhNghiep = '" + MaDoanhNghiep + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'",null);
            dgListNPLTK.DataSource = Collection;
            dgListNPLTK.Refetch();
        }
        
        private void dgListNPLTK_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
            npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            npl.Ma = e.Row.Cells["MaNPL"].Text;
            npl.Load();
            e.Row.Cells["TenNPL"].Text = npl.Ten;
        }
        private void SelectSanPhamKiemTra()
        {
            SelectSanPhamThongKeForm fSPX = new SelectSanPhamThongKeForm();            
            fSPX.ShowDialog(this);
            if (fSPX.table==null || fSPX.table.Rows.Count == 0)
            {
                return;
            }
            dgListSanPham.Visible = true;
            dgListSPTON.Visible = true;
            dgListToKhai.Visible = false;
            dglistTKTon.Visible = false;
            dgListSanPham.DataSource = fSPX.table;
            try
            {
                dgListSanPham.Refetch();
            }
            catch{
                dgListSanPham.Refresh();
            }
        }

        private void SelectToKhaiKiemTra()
        {
            SelectToKhaiMauDichThongKeForm f = new SelectToKhaiMauDichThongKeForm();
            f.ShowDialog(this);
            //if (f.table==null || f.table.Rows.Count == 0)
            //{
            //    return;
            //}
            dgListToKhai.DataSource = f.table;
            dgListToKhai.Visible = true;
            dglistTKTon.Visible = true;
            dgListSanPham.Visible = false;
            dgListSPTON.Visible = false;
            try
            {
                dgListToKhai.Refetch();
            }
            catch
            {
                dgListToKhai.Refresh();
            }
        }

        private void KiemTraLuongTon()
        {
            try
            {
            
            this.Cursor = Cursors.WaitCursor;
            DataTable dataTon = new DataTable();
            //dataTon = NPLNhapTonThucTe.SelectBy_MaHaiQuan_AND_MaDoanhNghiep(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI).Tables[0];
            dataTon = new Company.BLL.KDT.SXXK.NPLNhapTon().SelectNPLTon();
            DataTable TableResult = new DataTable();
            TableResult.Columns.Add("MaSP");
            TableResult.Columns.Add("MaNPL");
            TableResult.Columns.Add("TenNPL");
            TableResult.Columns.Add("LuongTonDau");
            TableResult.Columns.Add("LuongSuDung");
            TableResult.Columns.Add("LuongTonCuoi");            
            bool CheckAm = false;
            if (dgListSanPham.Visible == true)//Kiểm tra số lượng bên sản phẩm
            {
                GridEXRow[] rowCollection = dgListSanPham.GetRows();
                if (rowCollection.Length == 0)
                {
                    this.Cursor = Cursors.Default;
                    //ShowMessage("Bạn chưa chọn danh sách sản phẩm để kiểm tra.", false);
                    MLMessages("Bạn chưa chọn danh sách sản phẩm để kiểm tra.", "MSG_PUB01", "", false);
                    return;
                }
                foreach (GridEXRow row in rowCollection)
                {
                    string maSP = row.Cells["MaSP"].Text;
                    Decimal SoLuong = Convert.ToDecimal(row.Cells["Luong"].Text);
                    DataTable dsMD = DinhMuc.getDinhMucOfSanPham(maSP, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, SoLuong,GlobalSettings.SoThapPhan.LuongNPL).Tables[0];
                    if (dsMD.Rows.Count == 0)
                    {
                        this.Cursor = Cursors.Default;
                       // ShowMessage("Sản phẩm có mã : " + maSP + " chưa có định mức", false);
                        MLMessages("Sản phẩm có mã : " + maSP + " chưa có định mức", "MSG_DMC06", "", false);
                        return;
                    }
                    foreach (DataRow rowDM in dsMD.Rows)
                    {
                        string MaNPL = rowDM["MaNguyenPhuLieu"].ToString();
                        string TenNPL = rowDM["TenNPL"].ToString();
                        double SoLuongSuDung = Convert.ToDouble(rowDM["SoLuong"].ToString());
                        //DataRow[] rowDataTon = dataTon.Select("Ma='" + MaNPL + "'");
                        DataRow[] rowDataTon = dataTon.Select("MaNPL ='" + MaNPL + "'");
                        if (rowDataTon == null || rowDataTon.Length == 0)
                        {
                            this.Cursor = Cursors.Default;
                           // ShowMessage("Nguyên phụ liệu có mã : " + MaNPL + " thuộc định mức của sản phẩm : "+maSP+" chưa có trong hệ thống NPL tồn.", false);
                            MLMessages("Nguyên phụ liệu có mã : " + MaNPL + " thuộc định mức của sản phẩm : " + maSP + " chưa có trong hệ thống NPL tồn.", "MSG_DMC06", "", false);
                            return;
                        }
                        /*decimal TonDau = Convert.ToDecimal(rowDataTon[0]["LuongTon"].ToString());*/
                        double TonDau = Convert.ToDouble(rowDataTon[0]["TonCuoi"].ToString());
                        double TonCuoi = TonDau - SoLuongSuDung;
                        if (TonCuoi < 0)
                            CheckAm = true;
                        /*rowDataTon[0]["LuongTon"] = TonCuoi;*/
                        rowDataTon[0]["TonCuoi"] = TonCuoi;
                        TableResult.Rows.Add(new string[] { maSP, MaNPL, TenNPL, TonDau.ToString(), SoLuongSuDung.ToString(), TonCuoi.ToString() });
                    }
                }
            }
            else
            {
                GridEXRow[] rowCollection = dgListToKhai.GetDataRows();
                if (rowCollection.Length == 0)
                {
                    this.Cursor = Cursors.Default;
                   // ShowMessage("Bạn chưa chọn danh sách tờ khai để kiểm tra.", false);
                    MLMessages("Bạn chưa chọn danh sách sản phẩm để kiểm tra.", "MSG_PUB01", "", false);
                    return;
                }
                TableResult.Columns.Add("TKMD_ID");   
                foreach (GridEXRow row in rowCollection)
                {
                    if (row.RowType == RowType.Record)
                    {
                        long TKMD_ID = Convert.ToInt64(row.Cells["TKMD_ID"].Text);
                        string maSP = row.Cells["MaSP"].Text;
                        Decimal SoLuong = Convert.ToDecimal(row.Cells["Luong"].Text);
                        DataTable dsMD = DinhMuc.getDinhMucOfSanPham(maSP, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, SoLuong,GlobalSettings.SoThapPhan.LuongNPL).Tables[0];
                        if (dsMD.Rows.Count == 0)
                        {
                            this.Cursor = Cursors.Default;
                            //ShowMessage("Hàng hóa có mã : " + maSP + " trong tờ khai có ID=" + TKMD_ID + " chưa có định mức", false);
                            MLMessages("Hàng hóa có mã : " + maSP + " trong tờ khai có ID=" + TKMD_ID + " chưa có định mức", "MSG_DMC06", "", false);
                            return;
                        }
                        foreach (DataRow rowDM in dsMD.Rows)
                        {
                            string MaNPL = rowDM["MaNguyenPhuLieu"].ToString();
                            string TenNPL = rowDM["TenNPL"].ToString();
                            double SoLuongSuDung = Convert.ToDouble(rowDM["SoLuong"].ToString());
                            /*DataRow[] rowDataTon = dataTon.Select("Ma='" + MaNPL + "'");*/
                            DataRow[] rowDataTon = dataTon.Select("MaNPL='" + MaNPL + "'");
                            if (rowDataTon == null || rowDataTon.Length == 0)
                            {
                                this.Cursor = Cursors.Default;
                                //ShowMessage("Nguyên phụ liệu có mã : " + MaNPL + " chưa có trong hệ thống NPL tồn.", false);
                                MLMessages("Nguyên phụ liệu có mã : " + MaNPL + " thuộc định mức của sản phẩm : " + maSP + " chưa có trong hệ thống NPL tồn.", "MSG_DMC06", "", false);
                                return;
                            }
                            /*decimal TonDau = Convert.ToDecimal(rowDataTon[0]["LuongTon"].ToString());*/
                            double TonDau = Convert.ToDouble(rowDataTon[0]["TonCuoi"].ToString());
                            double TonCuoi = TonDau - SoLuongSuDung;
                            if (TonCuoi < 0)
                                CheckAm = true;
                            /*rowDataTon[0]["LuongTon"] = TonCuoi;*/
                            rowDataTon[0]["TonCuoi"] = TonCuoi;
                            TableResult.Rows.Add(new string[] { maSP, MaNPL, TenNPL, TonDau.ToString(), SoLuongSuDung.ToString(), TonCuoi.ToString(), TKMD_ID.ToString() });
                        }
                    }
                }
            }
            if (dgListToKhai.Visible == true)
            {
                try
                {
                    dglistTKTon.DataSource = TableResult;
                    dglistTKTon.Refetch();
                }
                catch { dglistTKTon.Refresh(); }
            }
            else
            {
                try
                {
                    dgListSPTON.DataSource = TableResult;
                    dgListSPTON.Refetch();
                }
                catch { dgListSPTON.Refresh(); }
            }
            if (CheckAm)
            {
                if (dgListToKhai.Visible == true)
                    //  ShowMessage("Lượng NPL tồn không đủ để xuất hết các tờ khai này.", false);
                    MLMessages("Lượng NPL tồn không đủ để xuất hết các tờ khai này.", "MSG_PB05", "", false);
                else
                    //ShowMessage("Lượng NPL tồn không đủ để xuất hết các sản phẩm này.", false);
                    MLMessages("Lượng NPL tồn không đủ để xuất hết các sản phẩm này.", "MSG_PB06", "", false);
            }
            else
            {
                if (dgListToKhai.Visible == true)
                  //  ShowMessage("Lượng NPL tồn đủ để xuất hết các tờ khai này.", false);
                    MLMessages("Lượng NPL tồn đủ để xuất hết các tờ khai này.", "MSG_PB05", "", false);
                else
                    //ShowMessage("Lượng NPL tồn đủ để xuất hết các sản phẩm này.", false);
                    MLMessages("Lượng NPL tồn đủ để xuất hết các sản phẩm này.", "MSG_PB06", "", false);
            }
            }
            catch (System.Exception ex)
            {
                ShowMessage("Lỗi khi kiểm tra tờ khai xuất : " + ex.Message, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            this.Cursor = Cursors.Default;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {               
                case "cmdSave":
                    this.saveLuongTon();
                    break;
                case "cmdNapLaiData":
                    this.BindDataNPLTonThucTe();
                    break;
                case "cmdKiemTra":
                    this.KiemTraLuongTon();
                    break;
                case "cmdChonSP":
                    this.SelectSanPhamKiemTra();
                    break;
                case "cmdChonToKhai":
                    this.SelectToKhaiKiemTra();
                    break;  
            }
        }
        private void BindDataNPLTonThucTe()
        {
            //dgList.DataSource = NPLNhapTonThucTe.SelectBy_MaHaiQuan_AND_MaDoanhNghiep(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI).Tables[0];
            //dgList.DataSource = new NPLNhapTon().SelectDynamic(string.Format("MaHaiQuan = '{0}' AND MaDoanhNghiep = '{1}'", GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI), null).Tables[0];
            //Collection = new Company.BLL.KDT.SXXK.NPLNhapTon().SelectCollectionDynamic(" MaDoanhNghiep = '" + MaDoanhNghiep + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "' AND LanThanhLy = (select MAX(LanThanhLy) from t_kdt_SXXK_NPLNhapTon)", null);

            dgList.DataSource = new Company.BLL.KDT.SXXK.NPLNhapTon().SelectNPLTon();
            dgList.Refetch();
        }
        private void saveLuongTon()
        {
            //KhanhHN 13/11/2012 - command - Doanh Nghiệp tự điều chỉnh trong tờ khai
//             try
//             {
//                 foreach (NPLNhapTonThucTe NPL in Collection)
//                 {
//                     NPL.ThueXNKTon = Convert.ToDouble(Math.Round(NPL.Ton / NPL.Luong * Convert.ToDecimal(NPL.ThueXNK), MidpointRounding.AwayFromZero)); ;
//                 }
//                 NPLNhapTonThucTe.InsertUpdateCollection(Collection);
//                 try
//                 {
//                     dgListNPLTK.DataSource = Collection;
//                     dgListNPLTK.Refetch();
//                 }
//                 catch { dgListNPLTK.Refresh(); }
//                // ShowMessage("Cập nhật thành công.", false);
//                 MLMessages("Cập nhật thành công.", "MSG_SAV02" ,"", false);
//             }
//             catch (Exception ex)
//             {
//                 ShowMessage(ex.Message, false);
//             }
        }        

        private void LuuTonToKhai_Click(object sender, EventArgs e)
        {
            saveLuongTon();
        }
        private void searchToKhaiTonThucTe()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = "MaDoanhNghiep = '" + MaDoanhNghiep + "'";
            string temp = "";
            where += string.Format(" AND MaHaiQuan = '{0}'", GlobalSettings.MA_HAI_QUAN);

            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoToKhai = " + txtSoTiepNhan.Value;
            }

            if (txtNamTiepNhan.TextLength > 0)
            {
                where += " AND NamDangKy = " + txtNamTiepNhan.Value;
            }
            if (txtMaNPL.Text.Length > 0)
            {
                where += " AND MaNPL LIKE '%" + txtMaNPL.Text.Trim() + "%'";
            }
            
            if (txtTenChuHang.Text.Trim().Length > 0)
            {
                if (txtTenChuHang.Text.Trim() != "*")
                {
                    temp += " AND TenChuHang LIKE '%" + txtTenChuHang.Text.Trim() + "%'";
                }
            }
            else
            {
                temp += " AND TenChuHang LIKE ''";

            }


            where += " AND Cast(SoToKhai as varchar(10))+ MaLoaiHinh +  Cast(NamDangKy as varchar(4))+MaHaiQuan IN " +
            "(SELECT Cast(SoToKhai as varchar(10))+ MaLoaiHinh +  Cast(NamDangKy as varchar(4))+MaHaiQuan FROM dbo.t_SXXK_ToKhaiMauDich WHERE MaLoaiHinh LIKE 'N%'" + temp + ")";

            // Thực hiện tìm kiếm.                        
            Collection = new Company.BLL.KDT.SXXK.NPLNhapTon().SelectCollectionDynamic(where + " AND LanThanhLy = (select MAX(LanThanhLy) from t_kdt_SXXK_NPLNhapTon)", "t_KDT_SXXK_NPLNhapTon.SoToKhai");
            dgListNPLTK.DataSource = Collection;
            dgListNPLTK.Refetch();
        }
        private void TimToKhaiTon_Click(object sender, EventArgs e)
        {
            searchToKhaiTonThucTe();
        }

        private void inDanhSachTon_Click_1(object sender, EventArgs e)
        {
            gridEXPrintDocument1.Print();
        }

        private void ChonSanPhamKiemTra_Click(object sender, EventArgs e)
        {
            SelectSanPhamKiemTra();
        }

        private void ChonToKhai_Click(object sender, EventArgs e)
        {
            SelectToKhaiKiemTra();
        }

        private void KiemTra_Click(object sender, EventArgs e)
        {
            KiemTraLuongTon();
        }

        private void XemPhanBo_Click(object sender, EventArgs e)
        {
            if (txSoTKXuat.Text == "")
            {
              //  ShowMessage("Bạn chưa chọn tờ khai xuất để xem phân bổ.", false);
                MLMessages("Bạn chưa chọn tờ khai xuất để xem phân bổ.", "MSG_PUB23", "", false);
                return;
            }
            if (cbSanPham.Text.Trim() == "")
                dgListPhanBo.DataSource = PhanBoToKhaiNhap.SelectCollectionPhanBoToKhaiXuatOfTKMD(GlobalSettings.MA_HAI_QUAN, loaiHinhMauDichHControl1.Ma, Convert.ToInt32(txSoTKXuat.Text), Convert.ToInt16(txtNamDKXuat.Text), GlobalSettings.SoThapPhan.LuongNPL);
            else
                dgListPhanBo.DataSource = PhanBoToKhaiNhap.SelectCollectionPhanBoToKhaiXuatOfTKMDAndMaSP(GlobalSettings.MA_HAI_QUAN, loaiHinhMauDichHControl1.Ma, Convert.ToInt32(txSoTKXuat.Text), Convert.ToInt16(txtNamDKXuat.Text), cbSanPham.SelectedItem.Value.ToString().Trim(), GlobalSettings.SoThapPhan.LuongNPL);
            dgListPhanBo.Refetch();
            ToKhaiMauDich TKMD = new ToKhaiMauDich();
            TKMD.SoToKhai = Convert.ToInt32(txSoTKXuat.Text);
            TKMD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            TKMD.MaLoaiHinh = loaiHinhMauDichHControl1.Ma;
            TKMD.NamDangKy = Convert.ToInt16(txtNamDKXuat.Text);
            TKMD.Load();
            TKMD.LoadHMDCollection();
            cbSanPham.Items.Clear();
            cbSanPham.Items.Add("", "");
            foreach (HangMauDich HMD in TKMD.HMDCollection)
            {
                cbSanPham.Items.Add(HMD.MaPhu, HMD.MaPhu);
            }
        }

        private void dgListTKChuaPhanBo_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string loaiHinh = (e.Row.Cells["MaLoaiHinh"].Value.ToString());
                DateTime time = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text);
                e.Row.Cells["MaLoaiHinh"].Text = this.LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());
                //e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + GlobalSettings.MA_HAI_QUAN + "/" + time.Year.ToString();
                if (loaiHinh.StartsWith("NSX"))
                {
                    switch (e.Row.Cells["ThanhLy"].Value.ToString().Trim())
                    {
                        case "":
                            //e.Row.Cells["ThanhLy"].Text = "Chưa thanh khoản";
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["ThanhLy"].Text = "Chưa thanh khoản";
                            }
                            else
                            {
                                e.Row.Cells["ThanhLy"].Text = "Not yet liquidated";
                            }
                            break;
                        case "H":
                           // e.Row.Cells["ThanhLy"].Text = "Đã thanh khoản hết";
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["ThanhLy"].Text = "Đã thanh khoản hết";
                            }
                            else
                            {
                                e.Row.Cells["ThanhLy"].Text = "Liquidation of all";
                            }
                            break;
                        case "L":
                            //e.Row.Cells["ThanhLy"].Text = "Thanh khoản một phần";
                            if (GlobalSettings.NGON_NGU == "0")
                            {
                                e.Row.Cells["ThanhLy"].Text = "Thanh khoản một phần";
                            }
                            else
                            {
                                e.Row.Cells["ThanhLy"].Text = "Partial of liquidation";
                            }
                            break;
                    }
                }
                else
                    e.Row.Cells["ThanhLy"].Text = "";
                //string phanluong = e.Row.Cells["PhanLuong"].Text;
                //if (phanluong == "1" || phanluong == "3" || phanluong == "5")
                //    e.Row.Cells["PhanLuong"].Text = "Tờ khai đã được phân luồng xanh";
                //else if (phanluong == "2" || phanluong == "4" || phanluong == "6" || phanluong == "10")
                //{
                //    e.Row.Cells["PhanLuong"].Text = "Tờ khai đã được phân luồng đỏ";
                //}
                //else if (phanluong == "8" || phanluong == "12")
                //    e.Row.Cells["PhanLuong"].Text = "Tờ khai đã được phân luồng vàng";


                DateTime ngayTNX = Convert.ToDateTime(e.Row.Cells["NGAY_THN_THX"].Value.ToString());
                DateTime ngayDK = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value.ToString());
                DateTime today = DateTime.Today;

                if (loaiHinh.StartsWith("NSX"))
                {
                    string thanhly = e.Row.Cells["ThanhLy"].Value.ToString().Trim();
                    TimeSpan time1 = today.Subtract(ngayDK);
                    int ngay = 275 - time1.Days;
                    if (GlobalSettings.NGON_NGU == "0") //Vie
                    {
                        if (thanhly == "H")
                        {
                            e.Row.Cells["ChuY"].Text = "Tờ khai này đã thanh khoản hết";
                            return;
                        }
                        if (ngay > 0)
                            e.Row.Cells["ChuY"].Text = "Còn " + ngay.ToString() + " ngày phải thanh khoản";
                        else if (ngay == 0)
                            e.Row.Cells["ChuY"].Text = "Hết hạn thanh khoản trong ngày hôm nay";
                        else
                            e.Row.Cells["ChuY"].Text = "Đã hết hạn thanh khoản";
                    }
                    else // Eng
                    {
                        if (thanhly == "H")
                        {
                            e.Row.Cells["ChuY"].Text = "This declaration have expired to liquidate";
                            return;
                        }
                        if (ngay > 0)
                            e.Row.Cells["ChuY"].Text = "Remaining " + ngay.ToString() + " days to liquidate";
                        else if (ngay == 0)
                            e.Row.Cells["ChuY"].Text = "Expired liquidation today";
                        else
                            e.Row.Cells["ChuY"].Text = "Expired liquidation";

                    }

                }
                if (loaiHinh.StartsWith("XSX"))
                {

                    TimeSpan time1 = today.Subtract(ngayDK);
                    int ngay = 45 - time1.Days;
                    //if (ngay > 0)
                    //    e.Row.Cells["ChuY"].Text = "Còn " + ngay.ToString() + " ngày phải thanh khoản";
                    //else if (ngay == 0)
                    //    e.Row.Cells["ChuY"].Text = "Hết hạn thanh khoản trong ngày hôm nay";
                    //else
                    //    e.Row.Cells["ChuY"].Text = "Đã hết hạn thanh khoản";
                    if (GlobalSettings.NGON_NGU == "0") //Vie
                    {   
                        if (ngay > 0)
                            e.Row.Cells["ChuY"].Text = "Còn " + ngay.ToString() + " ngày phải thanh khoản";
                        else if (ngay == 0)
                            e.Row.Cells["ChuY"].Text = "Hết hạn thanh khoản trong ngày hôm nay";
                        else
                            e.Row.Cells["ChuY"].Text = "Đã hết hạn thanh khoản";
                    }
                    else // Eng
                    {                       
                        if (ngay > 0)
                            e.Row.Cells["ChuY"].Text = "Remaining " + ngay.ToString() + " days to liquidate";
                        else if (ngay == 0)
                            e.Row.Cells["ChuY"].Text = "Expired liquidation today";
                        else
                            e.Row.Cells["ChuY"].Text = "Expired liquidation";

                    }

                }
                if (ngayTNX.Year <= 1900)
                {
                    e.Row.Cells["NGAY_THN_THX"].Text = "";
                }
            }
        }
        private void BindDataToKhaiChuaPhanBo()
        {
            TKMDChuaPhanBoCollection = ToKhaiMauDich.SelectToKhaiChuaPhanBo(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            dgListTKChuaPhanBo.DataSource = TKMDChuaPhanBoCollection;
            dgListTKChuaPhanBo.Refetch();
        }
        private void BindDataToKhaiDaPhanBo()
        {
            dgToKhaiXuatDaPhanBo.DataSource = ToKhaiMauDich.SelectToKhaiDaPhanBo(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI); ;
            dgToKhaiXuatDaPhanBo.Refetch();
        }
        private void uiTabSanPham_SelectedTabChanged(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {
            this.AcceptButton = null;
            switch (e.Page.Key)
            {
                case "tabQLNPLTon":
                   
                    BindDataNPLTonThucTe();
                    break;
                case "PhanBoToKhaiXuat":
                    BindDataToKhaiChuaPhanBo();
                    break;
                case "tkxDaPhanBo":
                    BindDataToKhaiDaPhanBo();
                    break;
                case "TheoDoiPhanBo":
                    this.AcceptButton = btnTheoDoiPB;
                    break;
                case "ThongKeLuongTon":
                    this.AcceptButton = btnTimKiemTK;
                    break;
            }
        }

        private void PhanBo_Click(object sender, EventArgs e)
        {
            int i = 0;
            this.Cursor = Cursors.WaitCursor;
            int chenhLechNgay = Convert.ToInt32(GlobalSettings.CHENHLECH_THN_THX);
            GridEXRow[] rows = dgListTKChuaPhanBo.GetCheckedRows();
            if (rows.Length == 0)
            {
                ShowMessage("Bạn chưa chọn tờ khai để phân bổ.", false);
                return;
            }
            foreach (GridEXRow row in rows)
            {
                ToKhaiMauDich TKMD = (ToKhaiMauDich)row.DataRow;
                TKMD.LoadHMDCollection();
                try
                {
                    if (TKMD.Xuat_NPL_SP == "S")
                    {
                        PhanBoToKhaiXuat pbTKXuat = new PhanBoToKhaiXuat();
                        pbTKXuat.SoToKhaiXuat = TKMD.SoToKhai;
                        pbTKXuat.MaHaiQuanXuat = TKMD.MaHaiQuan;
                        pbTKXuat.MaLoaiHinhXuat = TKMD.MaLoaiHinh;
                        pbTKXuat.NamDangKyXuat = TKMD.NamDangKy;
                        pbTKXuat.MaDoanhNghiep = TKMD.MaDoanhNghiep;
                        pbTKXuat.PhanBoChoToKhaiXuat(GlobalSettings.SoThapPhan.LuongNPL, chenhLechNgay);
                    }
                    else
                    {
                        SelectNPLTaiXuatForm f = new SelectNPLTaiXuatForm();
                        f.TKMD = TKMD;                        
                        IList<Company.BLL.SXXK.PhanBoToKhaiXuat> pbToKhaiXuatCollection = new List<Company.BLL.SXXK.PhanBoToKhaiXuat>();
                        foreach (Company.BLL.SXXK.ToKhai.HangMauDich HMD in TKMD.HMDCollection)
                        {
                            Company.BLL.SXXK.PhanBoToKhaiXuat pbToKhaiTaiXuat = new Company.BLL.SXXK.PhanBoToKhaiXuat();
                            pbToKhaiTaiXuat.SoToKhaiXuat = TKMD.SoToKhai;
                            pbToKhaiTaiXuat.NamDangKyXuat = (short)TKMD.NgayDangKy.Year;
                            pbToKhaiTaiXuat.MaHaiQuanXuat = TKMD.MaHaiQuan;
                            pbToKhaiTaiXuat.MaLoaiHinhXuat = TKMD.MaLoaiHinh;
                            pbToKhaiTaiXuat.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            pbToKhaiTaiXuat.MaSP = HMD.MaPhu;
                            pbToKhaiTaiXuat.SoLuongXuat = HMD.SoLuong;
                            pbToKhaiXuatCollection.Add(pbToKhaiTaiXuat);
                        }
                        f.pbToKhaiXuatCollection = pbToKhaiXuatCollection;
                        f.ShowDialog(this);
                        if (!f.isPhanBo)
                        {
                            break;
                        }
                    }
                }
                catch(Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                       // MLMessages("Có lỗi : " + ex.Message + " trong tờ khai " + TKMD.SoToKhai,"","", false);
                        ShowMessage("Có lỗi : " + ex.Message + " trong tờ khai " + TKMD.SoToKhai,  false);
                    }
                    else
                    {
                        ShowMessage(" There is error " + ex.Message + " in declaration " + TKMD.SoToKhai, false);
                    }
                    break;
                }
            }
            BindDataToKhaiChuaPhanBo();
            this.Cursor = Cursors.Default;
           // ShowMessage("Đã thực hiện xong.", false);
            MLMessages("Đã thực hiện xong. ", "MSG_PB01", "", false);

        }



        private void dgListTKChuaPhanBo_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void dgListPhanBo_RecordUpdated(object sender, EventArgs e)
        {
            int index=dgListPhanBo.GetRow().RowIndex;
            int indexNPLMax = 0;
            PhanBoToKhaiNhap pbTKNhap = (PhanBoToKhaiNhap)dgListPhanBo.GetRow().DataRow;
            PhanBoToKhaiNhap pbTKNhapGrix = (PhanBoToKhaiNhap)dgListPhanBo.GetRow().DataRow;
            foreach (GridEXRow row in dgListPhanBo.GetDataRows())
            {
                if (row.RowIndex < index && pbTKNhap.MaNPL.Trim().ToUpper() == row.Cells["MaNPL"].Text.Trim().ToUpper())
                {
                    PhanBoToKhaiNhap pbTKNhapBenTren = (PhanBoToKhaiNhap)row.DataRow;
                    if (pbTKNhapBenTren.LuongCungUng > 0)
                    {
                        //ShowMessage("Có tờ khai phía trên đã được phân bổ cung ứng cho nguyên phụ liệu này nên không được khai cung ứng cho tờ khai phía dưới.", false);
                        MLMessages("Có tờ khai phía trên đã được phân bổ cung ứng cho nguyên phụ liệu này nên không được khai cung ứng cho tờ khai phía dưới.", "MSG_PB02", "", false);
                        pbTKNhap.LuongCungUng = ValueOld;
                        dgListPhanBo.Refetch();
                        return;
                    }
                }
            }
            double LuongPhanBo = 0;
            double LuongCungUng=pbTKNhap.LuongCungUng;
            pbTKNhap=PhanBoToKhaiNhap.Load(pbTKNhap.ID);
            if (LuongCungUng > (pbTKNhap.LuongCungUng + pbTKNhap.LuongPhanBo))
            {
                LuongCungUng = Math.Round(pbTKNhap.LuongCungUng + pbTKNhap.LuongPhanBo, GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero);
            }
            pbTKNhapGrix.LuongCungUng = LuongCungUng;
            pbTKNhapGrix.LuongPhanBo = Math.Round(pbTKNhap.LuongCungUng + pbTKNhap.LuongPhanBo, GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero) - Math.Round(LuongCungUng, GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero);
            pbTKNhapGrix.LuongTonCuoi = Math.Round(pbTKNhapGrix.LuongTonDau, GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero) - Math.Round(pbTKNhapGrix.LuongPhanBo, GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero);
          

            foreach (GridEXRow row in dgListPhanBo.GetDataRows())
            {
                if (row.RowIndex > index && pbTKNhap.MaNPL.Trim().ToUpper() == row.Cells["MaNPL"].Text.Trim().ToUpper())
                {                    
                    PhanBoToKhaiNhap pbTKNhapBenDuoi = (PhanBoToKhaiNhap)row.DataRow;
                    LuongPhanBo = Math.Round(Convert.ToDouble(row.Cells["LuongPhanBo"].Text), GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero) +Math.Round(Convert.ToDouble(row.Cells["LuongCungUng"].Text), GlobalSettings.SoThapPhan.LuongNPL, MidpointRounding.AwayFromZero);
                    pbTKNhapBenDuoi.LuongCungUng = LuongPhanBo;
                    pbTKNhapBenDuoi.LuongPhanBo = 0;
                    if (pbTKNhap.SoToKhaiNhap == Convert.ToInt32(row.Cells["SoToKhaiNhap"].Text))
                    {
                        pbTKNhapBenDuoi.LuongTonDau = pbTKNhapGrix.LuongTonCuoi;
                        pbTKNhapBenDuoi.LuongTonCuoi = pbTKNhapGrix.LuongTonCuoi;
                    }
                    else
                    {
                        pbTKNhapBenDuoi.LuongTonCuoi = pbTKNhapBenDuoi.LuongTonDau;
                    }
                }               
            }
           
            dgListPhanBo.Refetch();
        }

        private void dgListPhanBo_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                long TKMD_IDPhanBo = Convert.ToInt64(e.Row.Cells["TKXuat_ID"].Value);
                PhanBoToKhaiXuat pbTKXuat = PhanBoToKhaiXuat.Load(TKMD_IDPhanBo);
                e.Row.Cells["MaSP"].Text = pbTKXuat.MaSP;
                e.Row.Cells["SoLuongXuat"].Text = pbTKXuat.SoLuongXuat + "";
                e.Row.Cells["NhuCau"].Text = pbTKXuat.SoLuongXuat * Convert.ToDecimal(e.Row.Cells["DinhMucChung"].Text) + "";
                e.Row.Cells["TKXuat_ID"].Text = pbTKXuat.MaSP;
            }            
        }

        private void uiButton9_Click(object sender, EventArgs e)
        {
            if (dgListPhanBo.GetDataRows().Length == 0)
            {
                return;
            }
            try
            {
                //if (ShowMessage("Nếu lưu các tờ khai sau tờ khai này sẽ phải phân bổ lại. Bạn có muốn tiếp tục không ?", true) == "Yes")
                if (MLMessages("Nếu lưu các tờ khai sau tờ khai này sẽ phải phân bổ lại. Bạn có muốn tiếp tục không ?", "MSG_PB03", "", true) == "Yes")
                {
                    IList<PhanBoToKhaiNhap> PhanBoToKhaiNhapCollection = new List<PhanBoToKhaiNhap>();
                    foreach (GridEXRow row in dgListPhanBo.GetDataRows())
                    {
                        if (row.RowType == RowType.Record)
                        {
                            PhanBoToKhaiNhap pbTKNhap = (PhanBoToKhaiNhap)row.DataRow;
                            if (pbTKNhap.SoToKhaiNhap > 0 && pbTKNhap.LuongTonCuoi>0)
                                PhanBoToKhaiNhapCollection.Add(pbTKNhap);
                        }
                    }
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.SoToKhai = Convert.ToInt32(txSoTKXuat.Text);
                    TKMD.MaLoaiHinh = loaiHinhMauDichHControl1.Ma;
                    TKMD.NamDangKy = Convert.ToInt16(txtNamDKXuat.Text);
                    TKMD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    TKMD.Load();
                    TKMD.DeletePhanBoCacToKhaiSau(PhanBoToKhaiNhapCollection);
                }
            }
            catch (Exception ex)
            {
                //ShowMessage("Có lỗi khi phân bổ : "+ex.Message, false);
                MLMessages("Có lỗi khi phân bổ : " + ex.Message, "MSG_PB04", "" + ex.Message, false);
            }
        }

        private void dgListPhanBo_EditingCell(object sender, EditingCellEventArgs e)
        {
            ValueOld = Convert.ToDouble(e.Value);
        }

        private void cmdPrint_Click(object sender, EventArgs e)
        {
            ViewPhanBoForm f = new ViewPhanBoForm();
            ToKhaiMauDich TKMD = new ToKhaiMauDich();
            TKMD.SoToKhai = Convert.ToInt32(txSoTKXuat.Value);
            TKMD.NamDangKy = Convert.ToInt16(txtNamDKXuat.Value);
            TKMD.MaLoaiHinh = loaiHinhMauDichHControl1.Ma;
            TKMD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            TKMD.Load();
            f.TKMD = TKMD;
            f.Show();
        }

        private void dgListNPLTK_FormattingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["TenNPL"].Text == "")
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Red;
                }
            }
        }

        private void donViHaiQuanControl1_Load(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgListSanPham.Visible)
            {
                if(dgListSanPham.GetRow()!=null)
                    dgListSanPham.GetRow().Delete();
                try
                {
                    dgListSanPham.Refetch();
                }
                catch { dgListSanPham.Refresh(); }
            }
            else
            {
                if (dgListToKhai.GetRow() != null)
                    dgListToKhai.GetRow().Delete();
                try
                {
                    dgListToKhai.Refetch();
                }
                catch { dgListToKhai.Refresh(); }
            }
        }

        private void dgListSPTON_LoadingRow(object sender, RowLoadEventArgs e)
        {
            DataRow dr = (DataRow)e.Row.DataRow;
            if ((float)dr[5] < 0)
            {
                e.Row.RowStyle = new Janus.Windows.GridEX.GridEXFormatStyle() { BackColor = Color.Red };
            }
        }

        private void dgListSPTON_RowDoubleClick(object sender, RowActionEventArgs e)
        {

        }

      

      

      
     }
}