﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
   public class ContentInspection
    {
        /// <summary>
        /// Số biên bản kiểm tra
        /// </summary>
        [XmlElement("inspectionNumbers")]
        public string InspectionNumbers { get; set; }

        /// <summary>
        /// Số kết luận kiểm tra
        /// </summary>
        [XmlElement("conclusionNumbers")]
        public string ConclusionNumbers { get; set; }

        /// <summary>
        /// Ngày kiểm tra
        /// </summary>
        [XmlElement("inspectionDate")]
        public string InspectionDate { get; set; }
    }
}
