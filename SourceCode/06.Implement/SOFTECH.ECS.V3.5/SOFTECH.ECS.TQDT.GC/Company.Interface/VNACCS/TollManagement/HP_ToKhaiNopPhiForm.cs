﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
#if GC_V4
using Company.GC.BLL.DataTransferObjectMapper;
using Company.GC.BLL.KDT.SXXK;
#elif SXXK_V4
using Company.BLL.KDT.SXXK;
using Company.BLL.DataTransferObjectMapper;
#elif KD_V4
using Company.KD.BLL.KDT.SXXK;
using Company.KD.BLL.DataTransferObjectMapper;
#endif
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.Messages;
using Company.Interface.Report;

namespace Company.Interface.VNACCS.TollManagement
{
    public partial class HP_ToKhaiNopPhiForm : Company.Interface.BaseForm
    {
        public T_KDT_THUPHI_TOKHAI tokhai = new T_KDT_THUPHI_TOKHAI();
        public T_KDT_THUPHI_TOKHAI_DSCONTAINER container = new T_KDT_THUPHI_TOKHAI_DSCONTAINER();
        public T_KDT_THUPHI_TOKHAI_FILE file = new T_KDT_THUPHI_TOKHAI_FILE();
        public T_KDT_THUPHI_THONGBAO TB = new T_KDT_THUPHI_THONGBAO();
        public bool isAdd = true;
        bool isAddNew = false;
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        string filebase64 = "";
        long filesize = 0;
        public long size;
        public byte[] data;
        public System.IO.FileInfo fin;
        public bool IsContainer = true;
        public HP_ToKhaiNopPhiForm()
        {
            InitializeComponent();
        }

        private void HP_ToKhaiNopPhiForm_Load(object sender, EventArgs e)
        {
            if (tokhai.ID > 0)
            {
                SetInformation();
                tokhai.ContainerCollection = T_KDT_THUPHI_TOKHAI_DSCONTAINER.SelectCollectionBy_TK_ID(tokhai.ID);
                tokhai.FileCollection = T_KDT_THUPHI_TOKHAI_FILE.SelectCollectionBy_TK_ID(tokhai.ID);
                BindDataFile();
                BindDataContainer();
            }
            else
            {
                txtMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
                txtTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
                clcNgayCT.Value = DateTime.Now;
                txtSoCT.Text = DateTime.Now.ToString("yyMMddhhmmss");
                if (IsContainer)
                {
                    cbbLoaiHangHoa.SelectedValue = "100";
                }
                else
                {
                    cbbLoaiHangHoa.SelectedValue = "101";
                }
                tokhai.TrangThaiXuLy = TrangThaiToKhaiNopPhi.CHUA_KHAI_BAO;
            }
            setCommandStatus();
        }
        private void setCommandStatus()
        {
            if (tokhai.TrangThaiXuLy == TrangThaiToKhaiNopPhi.DUOC_CHAP_NHAN)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidstring.Enabled = cmdUpdateGuidstring.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDeleteContainer.Enabled = false;
                btnDeleteFile.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdPrint.Enabled = cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                txtSoTiepNhan.Text = tokhai.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = tokhai.NgayTiepNhan;
                lblStatus.Text = setText("Được chấp nhận", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (tokhai.TrangThaiXuLy == TrangThaiToKhaiNopPhi.DA_CO_SO_TIEP_NHAN)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidstring.Enabled = cmdUpdateGuidstring.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDeleteContainer.Enabled = false;
                btnDeleteFile.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdPrint.Enabled = cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                txtSoTiepNhan.Text = tokhai.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = tokhai.NgayTiepNhan;
                lblStatus.Text = setText("Đã có số tiếp nhận", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (tokhai.TrangThaiXuLy == TrangThaiToKhaiNopPhi.CHUA_KHAI_BAO)
            {
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdPrint.Enabled = cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidstring.Enabled = cmdUpdateGuidstring1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDeleteContainer.Enabled = true;
                btnDeleteFile.Enabled = true;

                clcNgayTiepNhan.Value = DateTime.Now;
                lblStatus.Text = tokhai.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (tokhai.TrangThaiXuLy == TrangThaiToKhaiNopPhi.DA_CO_THONG_BAO_NOP_PHI)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdPrint.Enabled = cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDeleteContainer.Enabled = false;
                btnDeleteFile.Enabled = false;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidstring.Enabled = cmdUpdateGuidstring1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblStatus.Text = setText("Đã có thông báo nộp phí", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (tokhai.TrangThaiXuLy == TrangThaiToKhaiNopPhi.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdPrint.Enabled = cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDeleteContainer.Enabled = false;
                btnDeleteFile.Enabled = false;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidstring.Enabled = cmdUpdateGuidstring1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblStatus.Text = setText("Đã khai báo", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (tokhai.TrangThaiXuLy == TrangThaiToKhaiNopPhi.DA_THUC_HIEN_NOP_PHI)
            {
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidstring.Enabled = cmdUpdateGuidstring.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDeleteContainer.Enabled = false;
                btnDeleteFile.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdPrint.Enabled = cmdPrint1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = tokhai.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = tokhai.NgayTiepNhan;
                lblStatus.Text = setText("Đã thực hiện nộp phí", "Not declared");
                this.OpenType = OpenFormType.View;
            }
        }
        private void txtSoToKhai_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                VNACC_ToKhaiManager f = new VNACC_ToKhaiManager();
                f.IsShowChonToKhai = true;
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.OK)
                {
                    KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();
                    TKMD = f.TKMDDuocChon;
                    txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                    clcNgayToKhai.Value = TKMD.NgayDangKy;
                    if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                    {
                        ctrMaLoaiHinh.CategoryType = ECategory.E001;
                        ctrMaLoaiHinh.ReLoadData();
                    }
                    else
                    {
                        ctrMaLoaiHinh.CategoryType = ECategory.E002;
                        ctrMaLoaiHinh.ReLoadData();
                    }
                    ctrMaLoaiHinh.Code = TKMD.MaLoaiHinh;
                    txtMaHQ.Text = TKMD.CoQuanHaiQuan;
                    ctrMaDDLuuKho.Code = TKMD.MaDDLuuKho;
                    ctrMaPhuongThucVT.Code = TKMD.MaPhuongThucVT;
                    if (TKMD.MaLoaiHinh.ToString().Contains("G"))
                    {
                        cbbNhomLoaiHinh.SelectedValue = "TF001";
                    }
                    else
                    {
                        cbbNhomLoaiHinh.SelectedValue = "TF003";
                    }
                    if (TKMD.VanDonCollection.Count >= 1)
                        txtSoVanDon.Text = TKMD.VanDonCollection[0].LoaiDinhDanh;
                    // Lấy danh sách Container nếu có
                    if (TKMD.SoToKhai.ToString().Substring(0, 1) == "1")
                    {
                        List<KDT_ContainerDangKy> containerCollection = KDT_ContainerDangKy.SelectCollectionDynamic("TKMD_ID =" + TKMD.ID + " AND TrangThaiXuLy IN (2,3)", "");
                        if (containerCollection.Count >= 1)
                        {
                            foreach (KDT_ContainerDangKy item in containerCollection)
                            {
                                item.ListCont = KDT_ContainerBS.SelectCollectionDynamic("Master_id = " + item.ID, "");
                                foreach (KDT_ContainerBS items in item.ListCont)
                                {
                                    T_KDT_THUPHI_TOKHAI_DSCONTAINER cont = new T_KDT_THUPHI_TOKHAI_DSCONTAINER();
                                    cont.TK_ID = tokhai.ID;
                                    cont.SoVanDon = txtSoVanDon.Text;
                                    if (cbbLoaiHangHoa.SelectedValue.ToString() == "100")
                                    {
                                        cont.SoContainer = items.SoContainer;
                                        cont.SoSeal = items.SoSeal;
                                        cont.Loai = "20";
                                        cont.TinhChat = "KHO";
                                        cont.GhiChu = items.GhiChu;
                                        cont.SoLuong = 1;
                                    }
                                    else
                                    {
                                        cont.TongTrongLuong = TKMD.TrongLuong;
                                        cont.DVT = TKMD.MaDVTTrongLuong;
                                    }
                                    tokhai.ContainerCollection.Add(cont);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (TKMD.ContainerTKMD.SoContainerCollection.Count > 0)
                        {
                            foreach (KDT_VNACC_TK_SoContainer item in TKMD.ContainerTKMD.SoContainerCollection)
                            {
                                T_KDT_THUPHI_TOKHAI_DSCONTAINER cont = new T_KDT_THUPHI_TOKHAI_DSCONTAINER();
                                cont.TK_ID = tokhai.ID;
                                cont.SoVanDon = txtSoVanDon.Text;
                                if (cbbLoaiHangHoa.SelectedValue.ToString() == "100")
                                {
                                    cont.SoContainer = item.SoContainer;
                                    cont.SoSeal = String.Empty;
                                    cont.Loai = "20";
                                    cont.TinhChat = "KHO";
                                    cont.GhiChu = String.Empty;
                                    cont.SoLuong = 1;
                                }
                                else
                                {
                                    cont.TongTrongLuong = TKMD.TrongLuong;
                                    cont.DVT = TKMD.MaDVTTrongLuong;
                                }
                                tokhai.ContainerCollection.Add(cont);
                            }
                        }
                    }
                    BindDataContainer();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateFormContainer(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtSoVanDon, errorProvider, "SỐ VẬN ĐƠN", isOnlyWarning);
                if (cbbLoaiHangHoa.SelectedValue.ToString() == "100")
                {
                    isValid &= ValidateControl.ValidateNull(txtSoContainer, errorProvider, "SỐ CONTAINER", isOnlyWarning);
                    isValid &= ValidateControl.ValidateNull(txtSoSeal, errorProvider, "SỐ SEAL", isOnlyWarning);
                    isValid &= ValidateControl.ValidateNull(cbbLoaiContainer, errorProvider, "LOẠI CONTAINER", isOnlyWarning);
                    isValid &= ValidateControl.ValidateNull(cbbTinhChatContainer, errorProvider, "TÍNH CHẤT CONTAINER", isOnlyWarning);
                    isValid &= ValidateControl.ValidateNull(txtSoLuong, errorProvider, "SỐ LƯỢNG", isOnlyWarning);
                }
                else
                {
                    isValid &= ValidateControl.ValidateNull(txtTongTrongLuong, errorProvider, "TỔNG TRỌNG LƯỢNG", isOnlyWarning);
                    isValid &= ValidateControl.ValidateNull(cbbDVT, errorProvider, "ĐVT", isOnlyWarning);
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void GetContainer()
        {
            try
            {
                container.SoVanDon = txtSoVanDon.Text;
                if (cbbLoaiHangHoa.SelectedValue.ToString() == "100")
                {
                    container.SoContainer = txtSoContainer.Text;
                    container.SoSeal = txtSoSeal.Text;
                    container.TinhChat = cbbTinhChatContainer.SelectedValue.ToString();
                    container.Loai = cbbLoaiContainer.SelectedValue.ToString();
                    container.SoLuong = Convert.ToInt32(txtSoLuong.Text);
                }
                else
                {
                    container.TongTrongLuong = Convert.ToDecimal(txtTongTrongLuong.Text);
                    container.DVT = cbbDVT.SelectedValue.ToString();
                }
                container.GhiChu = txtGhiChu.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetContainer()
        {
            try
            {
                txtSoVanDon.Text = container.SoVanDon;
                if (cbbLoaiHangHoa.SelectedValue.ToString() == "100")
                {
                    txtSoContainer.Text = container.SoContainer;
                    txtSoSeal.Text = container.SoSeal;
                    cbbTinhChatContainer.SelectedValue = container.TinhChat;
                    cbbLoaiContainer.SelectedValue = container.Loai;
                    txtSoLuong.Text = container.SoLuong.ToString();
                }
                else
                {
                    txtTongTrongLuong.Text = container.TongTrongLuong.ToString();
                    cbbDVT.SelectedValue = container.DVT;
                }
                txtGhiChu.Text = container.GhiChu;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataContainer()
        {
            try
            {
                dgListContainer.Refresh();
                dgListContainer.DataSource = tokhai.ContainerCollection;
                dgListContainer.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataFile()
        {
            try
            {
                dgListFile.Refresh();
                dgListFile.DataSource = tokhai.FileCollection;
                dgListFile.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnAddContainer_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateFormContainer(false))
                    return;
                GetContainer();
                if (isAdd)
                    tokhai.ContainerCollection.Add(container);
                container = new T_KDT_THUPHI_TOKHAI_DSCONTAINER();
                isAdd = true;
                txtSoContainer.Text = String.Empty;
                txtSoSeal.Text = String.Empty;
                txtSoLuong.Text = String.Empty;
                txtTongTrongLuong.Text = String.Empty;
                BindDataContainer();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAddExcel_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDeleteContainer_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgListContainer.SelectedItems;
                List<T_KDT_THUPHI_TOKHAI_DSCONTAINER> ItemColl = new List<T_KDT_THUPHI_TOKHAI_DSCONTAINER>();
                if (dgListContainer.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessageTQDT(" Thông báo từ hệ thống ", "DOANH NGHIỆP CÓ MUỐN XÓA CONTAINER NÀY KHÔNG?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((T_KDT_THUPHI_TOKHAI_DSCONTAINER)i.GetRow().DataRow);
                        }

                    }
                    foreach (T_KDT_THUPHI_TOKHAI_DSCONTAINER item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        tokhai.ContainerCollection.Remove(item);
                    }

                }
                BindDataContainer();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        /// <summary>
        /// Tinh tong dung luong
        /// </summary>
        /// <param name="FileInBytes"></param>
        /// <returns></returns>
        private string CalculateFileSize(decimal FileInBytes)
        {
            string strSize = "00";
            if (FileInBytes < 1024)
                strSize = FileInBytes + " B";//Byte
            else if (FileInBytes > 1024 & FileInBytes < 1048576)
                strSize = Math.Round((FileInBytes / 1024), 2) + " KB";//Kilobyte
            else if (FileInBytes > 1048576 & FileInBytes < 107341824)
                strSize = Math.Round((FileInBytes / 1024) / 1024, 2) + " MB";//Megabyte
            else if (FileInBytes > 107341824 & FileInBytes < 1099511627776)
                strSize = Math.Round(((FileInBytes / 1024) / 1024) / 1024, 2) + " GB";//Gigabyte
            else
                strSize = Math.Round((((FileInBytes / 1024) / 1024) / 1024) / 1024, 2) + " TB";//Terabyte
            return strSize;
        }
        private bool ValidateFormFile(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtTenFile, errorProvider, "TÊN FILE", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiFile, errorProvider, "LOẠI FILE", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void btnAddFile_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateFormFile(false))
                    return;
                file.TenFile = fin.Name;
                file.FileSize = filesize;
                file.Content = filebase64;
                file.Loai = cbbLoaiFile.SelectedValue.ToString();
                if (isAddNew)
                    tokhai.FileCollection.Add(file);
                file = new T_KDT_THUPHI_TOKHAI_FILE();
                txtTenFile.Text = String.Empty;
                isAddNew = true;
                BindDataFile();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog OpenFileDialog = new OpenFileDialog();

                OpenFileDialog.FileName = "";
                OpenFileDialog.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.bmp;*.tiff)|*.jpg;*.jpeg;*.gif;*.bmp;*.tiff|"
                                        + "Text files (*.txt;*.xml;*xsl)|*.txt;*.xml;*xsl|"
                                        + "Application File (*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;)|*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;";

                OpenFileDialog.Multiselect = false;
                if (OpenFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fin = new System.IO.FileInfo(OpenFileDialog.FileName);
                    if (fin.Extension.ToUpper() != ".jpg".ToUpper()
                      && fin.Extension.ToUpper() != ".jpeg".ToUpper()
                      && fin.Extension.ToUpper() != ".gif".ToUpper()
                      && fin.Extension.ToUpper() != ".tiff".ToUpper()
                      && fin.Extension.ToUpper() != ".txt".ToUpper()
                      && fin.Extension.ToUpper() != ".xml".ToUpper()
                      && fin.Extension.ToUpper() != ".xsl".ToUpper()
                      && fin.Extension.ToUpper() != ".csv".ToUpper()
                      && fin.Extension.ToUpper() != ".doc".ToUpper()
                      && fin.Extension.ToUpper() != ".mdb".ToUpper()
                      && fin.Extension.ToUpper() != ".pdf".ToUpper()
                      && fin.Extension.ToUpper() != ".ppt".ToUpper()
                      && fin.Extension.ToUpper() != ".xls".ToUpper())
                    {
                        ShowMessage("Tệp tin " + fin.Name + "Không đúng định dạng tiếp nhận của Hải Quan", false);
                    }
                    else
                    {
                        file = new T_KDT_THUPHI_TOKHAI_FILE();
                        isAddNew = true;
                        System.IO.FileStream fs = new System.IO.FileStream(OpenFileDialog.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                        size = 0;
                        for (int i = 0; i < tokhai.FileCollection.Count; i++)
                        {
                            size += Convert.ToInt64(tokhai.FileCollection[i].FileSize);
                        }
                        size += fs.Length;
                        if (size > 3145728)
                        {
                            this.ShowMessage(string.Format("Tổng dung lượng cho phép {0}\r\nDung lượng bạn nhập {1} đã vượt mức cho phép.", CalculateFileSize(3145728), CalculateFileSize(size)), false);
                            return;
                        }
                        byte[] data = new byte[fs.Length];
                        fs.Read(data, 0, data.Length);
                        filebase64 = System.Convert.ToBase64String(data);
                        filesize = fs.Length;

                        txtTenFile.Text = fin.Name;
                        txtFileSize.Text = CalculateFileSize(filesize).ToString();
                        lblTotalSize.Text = CalculateFileSize(size);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnViewFile_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                string path = Application.StartupPath + "\\Temp";
                System.IO.FileStream fs;
                byte[] bytes;
                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }

                if (dgListFile.GetRow() == null)
                {
                    ShowMessage("CHƯA CHỌN FILE ĐỂ XEM", false);
                    return;
                }
                T_KDT_THUPHI_TOKHAI_FILE fileData = (T_KDT_THUPHI_TOKHAI_FILE)dgListFile.GetRow().DataRow;
                string fileName = path + "\\" + fileData.TenFile;

                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }
                fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                bytes = System.Convert.FromBase64String(fileData.Content);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
                System.Diagnostics.Process.Start(fileName);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnDeleteFile_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("DOANH NGHIỆP CÓ MUỐN XÓA FILE ĐÍNH KÈM NÀY KHÔNG ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgListFile.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            file = (T_KDT_THUPHI_TOKHAI_FILE)i.GetRow().DataRow;
                            tokhai.FileCollection.Remove(file);
                            if (file.ID > 0)
                            {
                                file.Delete();
                            }
                        }
                    }
                    ShowMessage("XÓA THÀNH CÔNG", false);
                    BindDataFile();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend":
                    Send();
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdFeedback":
                    Feedback();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdUpdateGuidstringing":
                    UpdateGuidstring();
                    break;
                case "cmdPrint":
                    Print();
                    break;
            }
        }

        private void Print()
        {
            try
            {
                //ReportViewNoticeOfPayment f = new ReportViewNoticeOfPayment();
                //f.TK = tokhai;
                //f.TB = TB;
                //f.BindReport();
                //f.ShowPreview();
                HP_ThongBaoNoPhiForm f = new HP_ThongBaoNoPhiForm();
                f.TK = tokhai;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtMaDoanhNghiep, errorProvider, "MÃ DOANH NGHIỆP", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenDoanhNghiep, errorProvider, "TÊN DOANH NGHIỆP", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoToKhai, errorProvider, "SỐ TỜ KHAI", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcNgayToKhai, errorProvider, "NGÀY TỜ KHAI", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHQ, errorProvider, "MÃ HQ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrMaLoaiHinh, errorProvider, "MÃ LOẠI HÌNH", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrMaPhuongThucVT, errorProvider, "PHƯƠNG THỨC VẬN TẢI", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrMaDDLuuKho, errorProvider, "ĐỊA ĐIỂM LƯU KHO", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbNhomLoaiHinh, errorProvider, "NHÓM LOẠI HÌNH", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoCT, errorProvider, "SỐ TK NỘP PHÍ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcNgayCT, errorProvider, "NGÀY TỜ KHAI NỘP PHÍ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbHinhThucTT, errorProvider, "HÌNH THỨC THANH TOÁN", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiHangHoa, errorProvider, "LOẠI HÀNG HÓA", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(cbbDDNP, errorProvider, "ĐIỂM THU PHÍ", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void GetInformation()
        {
            try
            {
                tokhai.NgayTiepNhan = clcNgayTiepNhan.Value;
                tokhai.MaDoanhNghiep = txtMaDoanhNghiep.Text;
                tokhai.TenDoanhNghiep = txtTenDoanhNghiep.Text;
                tokhai.SoTK = txtSoToKhai.Text;
                tokhai.NgayTK = clcNgayToKhai.Value;
                tokhai.MaHQ = txtMaHQ.Text;
                tokhai.MaLoaiHinh = ctrMaLoaiHinh.Code;
                tokhai.MaPTVC = Convert.ToInt32(ctrMaPhuongThucVT.Code);
                tokhai.MaDiaDiemLuuKho = ctrMaDDLuuKho.Code;
                tokhai.DiemThuPhi = cbbDDNP.Code;
                tokhai.NhomLoaiHinh = cbbNhomLoaiHinh.SelectedValue.ToString();
                tokhai.SoTKNP = txtSoCT.Text;
                tokhai.NgayTKNP = clcNgayCT.Value;
                tokhai.HinhThucTT = cbbHinhThucTT.SelectedValue.ToString();
                tokhai.LoaiHangHoa = Convert.ToInt32(cbbLoaiHangHoa.SelectedValue.ToString());
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetInformation()
        {
            try
            {
                txtSoTiepNhan.Text = tokhai.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = tokhai.NgayTiepNhan;
                txtMaDoanhNghiep.Text = tokhai.MaDoanhNghiep;
                txtTenDoanhNghiep.Text = tokhai.TenDoanhNghiep;
                txtSoToKhai.Text = tokhai.SoTK;
                clcNgayToKhai.Value = tokhai.NgayTK;
                txtMaHQ.Text = tokhai.MaHQ;
                if (tokhai.SoTK.ToString().Substring(0, 1) == "1")
                {
                    ctrMaLoaiHinh.CategoryType = ECategory.E001;
                    ctrMaLoaiHinh.ReLoadData();
                }
                else
                {
                    ctrMaLoaiHinh.CategoryType = ECategory.E002;
                    ctrMaLoaiHinh.ReLoadData();
                }
                ctrMaLoaiHinh.Code = tokhai.MaLoaiHinh;
                ctrMaDDLuuKho.Code = tokhai.MaDiaDiemLuuKho;
                cbbLoaiHangHoa.SelectedValue = tokhai.LoaiHangHoa;
                cbbNhomLoaiHinh.SelectedValue = tokhai.NhomLoaiHinh;
                ctrMaPhuongThucVT.Code = tokhai.MaPTVC.ToString();
                txtSoCT.Text = tokhai.SoTKNP;
                clcNgayCT.Value = tokhai.NgayTKNP;
                cbbHinhThucTT.SelectedValue = tokhai.HinhThucTT;
                cbbDDNP.Code = tokhai.DiemThuPhi;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void UpdateGuidstring()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("T_KDT_ThuPhi_ToKhai", "", Convert.ToInt32(tokhai.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = tokhai.ID;
                form.DeclarationIssuer = tokhai.LoaiHangHoa == 100 ? DeclarationIssuer.RegisterHangContainer : DeclarationIssuer.RegisterHangRoi;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = tokhai.LoaiHangHoa == 100 ? LoaiKhaiBao.RegisterHangContainer : LoaiKhaiBao.RegisterHangRoi;
            sendXML.master_id = tokhai.ID;
            if (!sendXML.Load())
            {
                ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                return;
            }
            while (isFeedBack)
            {
                string reference = tokhai.GuidStr.ToUpper();
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = tokhai.LoaiHangHoa == 100 ? DeclarationIssuer.RegisterHangContainer : DeclarationIssuer.RegisterHangRoi,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = tokhai.LoaiHangHoa == 100 ? DeclarationIssuer.RegisterHangContainer : DeclarationIssuer.RegisterHangRoi,
                    //Type = DeclarationIssuer.RegisterHangContainer,
                };
                //subjectBase.Type = DeclarationIssuer.RegisterHangContainer;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = tokhai.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(tokhai.MaDiaDiemLuuKho.Substring(0, 4))),
                                                  Identity = tokhai.MaDiaDiemLuuKho.Substring(0, 4)
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                sendForm.IsPort = true;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        isFeedBack = false;
                        setCommandStatus();
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                }
            }
        }

        private void Save()
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetInformation();
                if (tokhai.ContainerCollection.Count == 0)
                {
                    ShowMessageTQDT("DOANH NGHIỆP CHƯA NHẬP DANH SÁCH CONTAINER", false);
                    return;
                }
                //if (tokhai.FileCollection.Count == 0)
                //{
                //    ShowMessageTQDT("DOANH NGHIỆP CHƯA NHẬP FILE ĐÍNH KÈM", false);
                //    return;
                //}
                tokhai.InsertUpdateFull();
                ShowMessage("Lưu thành công !", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Send()
        {
            if (ShowMessageTQDT(" Thông báo từ hệ thống ", "Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (tokhai.ID == 0)
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ", "Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    tokhai = T_KDT_THUPHI_TOKHAI.Load(tokhai.ID);
                    tokhai.ContainerCollection = T_KDT_THUPHI_TOKHAI_DSCONTAINER.SelectCollectionBy_TK_ID(tokhai.ID);
                    tokhai.FileCollection = T_KDT_THUPHI_TOKHAI_FILE.SelectCollectionBy_TK_ID(tokhai.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = tokhai.LoaiHangHoa == 100 ? LoaiKhaiBao.RegisterHangContainer : LoaiKhaiBao.RegisterHangRoi;
                sendXML.master_id = tokhai.ID;

                if (sendXML.Load())
                {
                    ShowMessageTQDT(" Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    string returnMessage = string.Empty;
                    tokhai.GuidStr = Guid.NewGuid().ToString().ToUpper();
                    TKNP_HangContainer register_TKInformation = new TKNP_HangContainer();
                    register_TKInformation = Mapper_V4.ToDataTransferRegisterTK(tokhai, GlobalSettings.TEN_DON_VI);

                    ObjectSend msgSend = new ObjectSend(
                        new NameBase()
                        {
                            Name = tokhai.TenDoanhNghiep,
                            Identity = tokhai.MaDoanhNghiep,
                        },
                        new NameBase()
                        {
                            Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(tokhai.MaDiaDiemLuuKho.Substring(0, 4))),
                            Identity = tokhai.MaDiaDiemLuuKho.Substring(0, 4)
                        },
                        new Subject()
                        {
                            Type = tokhai.LoaiHangHoa == 100 ? DeclarationIssuer.RegisterHangContainer : DeclarationIssuer.RegisterHangRoi,
                            Function = DeclarationFunction.KHAI_BAO,
                            Reference = tokhai.GuidStr.ToUpper(),
                        },
                        register_TKInformation
                        );
                    tokhai.TrangThaiXuLy = TrangThaiToKhaiNopPhi.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    sendForm.IsPort = true;
                    sendForm.IsPortSend = true;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(tokhai.ID, tokhai.LoaiHangHoa == 100 ? MessageTitle.RegisterHangContainer : MessageTitle.RegisterHangRoi);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = tokhai.LoaiHangHoa == 100 ? LoaiKhaiBao.RegisterHangContainer : LoaiKhaiBao.RegisterHangRoi;
                        sendXML.master_id = tokhai.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = Convert.ToInt32(msgSend.Subject.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            ShowMessageTQDT(msgInfor, false);
                            setCommandStatus();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            Feedback();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.RegisterTKSendHandler(tokhai, TB, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }

        private void dgListContainer_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    container = new T_KDT_THUPHI_TOKHAI_DSCONTAINER();
                    container = (T_KDT_THUPHI_TOKHAI_DSCONTAINER)e.Row.DataRow;
                    txtSoVanDon.Text = container.SoVanDon;
                    if (cbbLoaiHangHoa.SelectedValue.ToString() == "100")
                    {
                        txtSoContainer.Text = container.SoContainer;
                        txtSoSeal.Text = container.SoSeal;
                        cbbTinhChatContainer.SelectedValue = container.TinhChat;
                        cbbLoaiContainer.SelectedValue = container.Loai;
                        txtSoLuong.Text = container.SoLuong.ToString();
                    }
                    else
                    {
                        txtTongTrongLuong.Text = container.TongTrongLuong.ToString();
                        cbbDVT.SelectedValue = container.DVT;
                    }
                    txtGhiChu.Text = container.GhiChu;
                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListFile_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                file = (T_KDT_THUPHI_TOKHAI_FILE)dgListFile.GetRow().DataRow;
                txtFileSize.Text = CalculateFileSize(file.FileSize); ;
                txtTenFile.Text = file.TenFile;
                cbbLoaiFile.SelectedValue = file.Loai;
                isAddNew = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbLoaiHangHoa_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbLoaiHangHoa.SelectedValue.ToString() == "100")
                {
                    grbHangRoi.Enabled = false;
                    grbHangContainer.Enabled = true;
                }
                else
                {
                    grbHangContainer.Enabled = false;
                    grbHangRoi.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListContainer_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    int Loai = Convert.ToInt32(e.Row.Cells["Loai"].Value);
                    string TinhChat = Convert.ToInt32(e.Row.Cells["TinhChat"].Value).ToString();
                    switch (Loai)
                    {
                        case 20:
                            e.Row.Cells["Loai"].Text = "Container 20 feet";
                            break;
                        case 40:
                            e.Row.Cells["Loai"].Text = "Container 40 feet";
                            break;
                        case 99:
                            e.Row.Cells["Loai"].Text = "Container loại khác";
                            break;
                        default:
                            break;
                    }
                    switch (TinhChat)
                    {
                        case "KHO":
                            e.Row.Cells["TinhChat"].Text = "Container hàng khô";
                            break;
                        case "LANH":
                            e.Row.Cells["TinhChat"].Text = "Container hàng lạnh";
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListFile_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    int Loai = Convert.ToInt32(e.Row.Cells["Loai"].Value);
                    switch (Loai)
                    {
                        case 1:
                            e.Row.Cells["Loai"].Text = "Tờ khai Hải quan";
                            break;
                        case 2:
                            e.Row.Cells["Loai"].Text = "Vận đơn";
                            break;
                        case 9:
                            e.Row.Cells["Loai"].Text = "Chứng từ khác";
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
