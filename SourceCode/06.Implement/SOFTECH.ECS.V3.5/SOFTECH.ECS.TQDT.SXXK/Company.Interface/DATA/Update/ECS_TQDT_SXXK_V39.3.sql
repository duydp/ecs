GO
ALTER TABLE t_KDT_SXXK_MsgSend 
ALTER COLUMN msg NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_MsgSend_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_MsgSend_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_MsgSend_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_MsgSend_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_MsgSend_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_MsgSend_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_MsgSend_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_MsgSend_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_Insert]
	@master_id bigint,
	@func int,
	@LoaiHS varchar(50),
	@msg nvarchar(max)
AS
INSERT INTO [dbo].[t_KDT_SXXK_MsgSend]
(
	[master_id],
	[func],
	[LoaiHS],
	[msg]
)
VALUES
(
	@master_id,
	@func,
	@LoaiHS,
	@msg
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_Update]
	@master_id bigint,
	@func int,
	@LoaiHS varchar(50),
	@msg nvarchar(max)
AS

UPDATE
	[dbo].[t_KDT_SXXK_MsgSend]
SET
	[func] = @func,
	[msg] = @msg
WHERE
	[master_id] = @master_id
	AND [LoaiHS] = @LoaiHS

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_InsertUpdate]
	@master_id bigint,
	@func int,
	@LoaiHS varchar(50),
	@msg nvarchar(max)
AS
IF EXISTS(SELECT [master_id], [LoaiHS] FROM [dbo].[t_KDT_SXXK_MsgSend] WHERE [master_id] = @master_id AND [LoaiHS] = @LoaiHS)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_MsgSend] 
		SET
			[func] = @func,
			[msg] = @msg
		WHERE
			[master_id] = @master_id
			AND [LoaiHS] = @LoaiHS
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_SXXK_MsgSend]
	(
			[master_id],
			[func],
			[LoaiHS],
			[msg]
	)
	VALUES
	(
			@master_id,
			@func,
			@LoaiHS,
			@msg
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_Delete]
	@master_id bigint,
	@LoaiHS varchar(50)
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_MsgSend]
WHERE
	[master_id] = @master_id
	AND [LoaiHS] = @LoaiHS

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_MsgSend] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_Load]
	@master_id bigint,
	@LoaiHS varchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[master_id],
	[func],
	[LoaiHS],
	[msg]
FROM
	[dbo].[t_KDT_SXXK_MsgSend]
WHERE
	[master_id] = @master_id
	AND [LoaiHS] = @LoaiHS
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[master_id],
	[func],
	[LoaiHS],
	[msg]
FROM [dbo].[t_KDT_SXXK_MsgSend] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_MsgSend_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_MsgSend_SelectAll]




AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[master_id],
	[func],
	[LoaiHS],
	[msg]
FROM
	[dbo].[t_KDT_SXXK_MsgSend]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '39.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('39.3',GETDATE(), N'CẬP NHẬT TABLE MSG_SEND')
END