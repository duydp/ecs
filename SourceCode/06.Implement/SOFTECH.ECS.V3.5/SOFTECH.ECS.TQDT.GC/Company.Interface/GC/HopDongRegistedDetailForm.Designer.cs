﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Company.Controls.CustomValidation;
using Janus.Windows.CalendarCombo;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX.EditControls;
using DonViHaiQuanControl=Company.Interface.Controls.DonViHaiQuanControl;

namespace Company.Interface.GC
{
    partial class HopDongRegistedDetailForm
    {
        private UIGroupBox uiGroupBox1;
        private ImageList ImageList1;
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HopDongRegistedDetailForm));
            Janus.Windows.GridEX.GridEXLayout dgThongTinHopDong_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgThongTinHopDong_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgNguyenPhuLieu_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgNguyenPhuLieu_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgSanPham_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgSanPham_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgThietBi_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgThietBi_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgHangMau_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgHangMau_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgNPLCungUng_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgNPLCungUng_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgNPLHuy_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgNPLHuy_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgTKN_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgTKN_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgTKX_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgTKX_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgPhuKien_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgPhuKien_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgTKCT_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgTKCT_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgTKCTXuat_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgTKCTXuat_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout dgThanhKhoanTT38_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgThanhKhoanMoi_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgThanhKhoan_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnXuatNhapTon = new Janus.Windows.EditControls.UIButton();
            this.btnDangKyNPLCungUng = new Janus.Windows.EditControls.UIButton();
            this.btnNThem = new Janus.Windows.EditControls.UIButton();
            this.btnXuatNhapTon_Thietbi = new Janus.Windows.EditControls.UIButton();
            this.btnNXoa = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnTinhLuongTon = new Janus.Windows.EditControls.UIButton();
            this.lblGhiChu = new System.Windows.Forms.Label();
            this.tabHopDong = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage8 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgThongTinHopDong = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dtpDateTo = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtpDateFrom = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTenDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnExportTotal = new Janus.Windows.EditControls.UIButton();
            this.lblTrangThaiThanhKhoan = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nguyenTeControl1 = new Company.Interface.Controls.NguyenTeControl();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ccNgayGiaHan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.nuocThue = new Company.Interface.Controls.NuocHControl();
            this.label1 = new System.Windows.Forms.Label();
            this.ccNgayKetThucHD = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ccNgayKyHD = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tpNguyenPhuLieu = new Janus.Windows.UI.Tab.UITabPage();
            this.dgNguyenPhuLieu = new Janus.Windows.GridEX.GridEX();
            this.ctmNPL = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.xemTKN = new System.Windows.Forms.ToolStripMenuItem();
            this.xemTKTXNPL = new System.Windows.Forms.ToolStripMenuItem();
            this.xemPKChuaNPL = new System.Windows.Forms.ToolStripMenuItem();
            this.xemTKCTNPLItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xemTKXuatSPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox18 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchNPL = new Janus.Windows.EditControls.UIButton();
            this.label22 = new System.Windows.Forms.Label();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnXuatExcelNPL = new Janus.Windows.EditControls.UIButton();
            this.btnXemTKNNPL = new Janus.Windows.EditControls.UIButton();
            this.btnXemPKNPL = new Janus.Windows.EditControls.UIButton();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.btnXemTKTXNPL = new Janus.Windows.EditControls.UIButton();
            this.btnInNPL = new Janus.Windows.EditControls.UIButton();
            this.btnXemTKCTNPL = new Janus.Windows.EditControls.UIButton();
            this.tpSanPham = new Janus.Windows.UI.Tab.UITabPage();
            this.dgSanPham = new Janus.Windows.GridEX.GridEX();
            this.ctmSP = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.xemTKXSP = new System.Windows.Forms.ToolStripMenuItem();
            this.xemPKSP = new System.Windows.Forms.ToolStripMenuItem();
            this.xemDMItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xemTKCTSPItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inDMSPItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox17 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchSP = new Janus.Windows.EditControls.UIButton();
            this.label13 = new System.Windows.Forms.Label();
            this.txtMaSP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnXemTKXSP = new Janus.Windows.EditControls.UIButton();
            this.btnXuatExcelSanPham = new Janus.Windows.EditControls.UIButton();
            this.btnXemDMSP = new Janus.Windows.EditControls.UIButton();
            this.btnXemPKSP = new Janus.Windows.EditControls.UIButton();
            this.btnInSP = new Janus.Windows.EditControls.UIButton();
            this.btnXemTKCTSP = new Janus.Windows.EditControls.UIButton();
            this.btnInDMSP = new Janus.Windows.EditControls.UIButton();
            this.tpThietBi = new Janus.Windows.UI.Tab.UITabPage();
            this.dgThietBi = new Janus.Windows.GridEX.GridEX();
            this.ctmTB = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.xemTKNTB = new System.Windows.Forms.ToolStripMenuItem();
            this.xemTKXTB = new System.Windows.Forms.ToolStripMenuItem();
            this.xemTKChuyenTiepTB = new System.Windows.Forms.ToolStripMenuItem();
            this.xemPKTB = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox19 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchTB = new Janus.Windows.EditControls.UIButton();
            this.label14 = new System.Windows.Forms.Label();
            this.txtMaTB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnXemTKNTB = new Janus.Windows.EditControls.UIButton();
            this.btnXemTKXTB = new Janus.Windows.EditControls.UIButton();
            this.btnXemPKTB = new Janus.Windows.EditControls.UIButton();
            this.tpHangMau = new Janus.Windows.UI.Tab.UITabPage();
            this.dgHangMau = new Janus.Windows.GridEX.GridEX();
            this.cmtHM = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox9 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox20 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchHM = new Janus.Windows.EditControls.UIButton();
            this.label15 = new System.Windows.Forms.Label();
            this.txtMaHM = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.uiButton4 = new Janus.Windows.EditControls.UIButton();
            this.uiButton3 = new Janus.Windows.EditControls.UIButton();
            this.tpNPLCU = new Janus.Windows.UI.Tab.UITabPage();
            this.dgNPLCungUng = new Janus.Windows.GridEX.GridEX();
            this.cmtNPL_CU = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripXuatExcel_NPLCU = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox10 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox21 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchNPLCU = new Janus.Windows.EditControls.UIButton();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMaNPLCU = new Janus.Windows.GridEX.EditControls.EditBox();
            this.tpNPLHuy = new Janus.Windows.UI.Tab.UITabPage();
            this.dgNPLHuy = new Janus.Windows.GridEX.GridEX();
            this.cmtNPLHuy = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripXuatExcel_NPLHuy = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox22 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSeachNPKH = new Janus.Windows.EditControls.UIButton();
            this.label17 = new System.Windows.Forms.Label();
            this.txtNPLHuy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgTKN = new Janus.Windows.GridEX.GridEX();
            this.ctmTKN = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.xemChiTietTKN = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExportExcelTKN = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox12 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox23 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchTKNK = new Janus.Windows.EditControls.UIButton();
            this.label18 = new System.Windows.Forms.Label();
            this.txtSoTKNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgTKX = new Janus.Windows.GridEX.GridEX();
            this.ctmTKX = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.xemChiTietTKX = new System.Windows.Forms.ToolStripMenuItem();
            this.xemLuongNPLTKX = new System.Windows.Forms.ToolStripMenuItem();
            this.xemNPLCungUngItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExportExcelTKX = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox13 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox24 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchTKXK = new Janus.Windows.EditControls.UIButton();
            this.label19 = new System.Windows.Forms.Label();
            this.txtSoTKXK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnXemChiTietTK = new Janus.Windows.EditControls.UIButton();
            this.btnXemLuongNPLXuatTK = new Janus.Windows.EditControls.UIButton();
            this.btnXemLuongNPLCungUngTK = new Janus.Windows.EditControls.UIButton();
            this.btnVanDonChoTKxuat = new Janus.Windows.EditControls.UIButton();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgPhuKien = new Janus.Windows.GridEX.GridEX();
            this.ctmPK = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.xemChiTietPK = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox14 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox25 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchPK = new Janus.Windows.EditControls.UIButton();
            this.lbl = new System.Windows.Forms.Label();
            this.txtSoPK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgTKCT = new Janus.Windows.GridEX.GridEX();
            this.ctmTKCT = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.xemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExportExcelTKCT = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox15 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox26 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchTKCTN = new Janus.Windows.EditControls.UIButton();
            this.label21 = new System.Windows.Forms.Label();
            this.txtSoTKCTN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiTabPage7 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgTKCTXuat = new Janus.Windows.GridEX.GridEX();
            this.ctmTKCTXuat = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExportExcelTKCTX = new System.Windows.Forms.ToolStripMenuItem();
            this.uiGroupBox16 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox27 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchTKCTX = new Janus.Windows.EditControls.UIButton();
            this.label23 = new System.Windows.Forms.Label();
            this.txtSoTKCTX = new Janus.Windows.GridEX.EditControls.EditBox();
            this.tpThanhKhoanTT38 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgThanhKhoanTT38 = new Janus.Windows.GridEX.GridEX();
            this.tpThanhKhoanMoi = new Janus.Windows.UI.Tab.UITabPage();
            this.dgThanhKhoanMoi = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage6 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgThanhKhoan = new Janus.Windows.GridEX.GridEX();
            this.uiCommandManager1 = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdThanhKhoan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThanhKhoan");
            this.cmdXuLyHD1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuLyHD");
            this.cmdProcessQuyetToan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdProcessQuyetToan");
            this.cmdResetThanhKhoan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResetThanhKhoan");
            this.LuuThongTin1 = new Janus.Windows.UI.CommandBars.UICommand("LuuThongTin");
            this.cmdBangKeNPLCU1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBangKeNPLCU");
            this.cmdBangKeNPLHuy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBangKeNPLHuy");
            this.cmdCanDoiTuCungUng1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCanDoiTuCungUng");
            this.cmdLayToKhaiNhapSXXK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLayToKhaiNhapSXXK");
            this.cmdThanhKhoan = new Janus.Windows.UI.CommandBars.UICommand("cmdThanhKhoan");
            this.cmdXuLyHD = new Janus.Windows.UI.CommandBars.UICommand("cmdXuLyHD");
            this.cmdProcessQuyetToan = new Janus.Windows.UI.CommandBars.UICommand("cmdProcessQuyetToan");
            this.cmdLayToKhaiNhapSXXK = new Janus.Windows.UI.CommandBars.UICommand("cmdLayToKhaiNhapSXXK");
            this.cmdXuatExcelDinhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatExcelDinhMuc");
            this.LuuThongTin = new Janus.Windows.UI.CommandBars.UICommand("LuuThongTin");
            this.cmdBangKeNPLCU = new Janus.Windows.UI.CommandBars.UICommand("cmdBangKeNPLCU");
            this.cmdCanDoiTuCungUng = new Janus.Windows.UI.CommandBars.UICommand("cmdCanDoiTuCungUng");
            this.cmdBangKeNPLHuy = new Janus.Windows.UI.CommandBars.UICommand("cmdBangKeNPLHuy");
            this.cmdResetThanhKhoan = new Janus.Windows.UI.CommandBars.UICommand("cmdResetThanhKhoan");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.gridEXPrintNPL = new Janus.Windows.GridEX.GridEXPrintDocument();
            this.gridEXPrintSP = new Janus.Windows.GridEX.GridEXPrintDocument();
            this.gridEXExporterSP = new Janus.Windows.GridEX.Export.GridEXExporter(this.components);
            this.gridEXExporterNPL = new Janus.Windows.GridEX.Export.GridEXExporter(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabHopDong)).BeginInit();
            this.tabHopDong.SuspendLayout();
            this.uiTabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgThongTinHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.tpNguyenPhuLieu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNguyenPhuLieu)).BeginInit();
            this.ctmNPL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).BeginInit();
            this.uiGroupBox18.SuspendLayout();
            this.tpSanPham.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSanPham)).BeginInit();
            this.ctmSP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).BeginInit();
            this.uiGroupBox17.SuspendLayout();
            this.tpThietBi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgThietBi)).BeginInit();
            this.ctmTB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox19)).BeginInit();
            this.uiGroupBox19.SuspendLayout();
            this.tpHangMau.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgHangMau)).BeginInit();
            this.cmtHM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).BeginInit();
            this.uiGroupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox20)).BeginInit();
            this.uiGroupBox20.SuspendLayout();
            this.tpNPLCU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLCungUng)).BeginInit();
            this.cmtNPL_CU.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).BeginInit();
            this.uiGroupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox21)).BeginInit();
            this.uiGroupBox21.SuspendLayout();
            this.tpNPLHuy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLHuy)).BeginInit();
            this.cmtNPLHuy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox22)).BeginInit();
            this.uiGroupBox22.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKN)).BeginInit();
            this.ctmTKN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).BeginInit();
            this.uiGroupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox23)).BeginInit();
            this.uiGroupBox23.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).BeginInit();
            this.ctmTKX.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).BeginInit();
            this.uiGroupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox24)).BeginInit();
            this.uiGroupBox24.SuspendLayout();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPhuKien)).BeginInit();
            this.ctmPK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).BeginInit();
            this.uiGroupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox25)).BeginInit();
            this.uiGroupBox25.SuspendLayout();
            this.uiTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKCT)).BeginInit();
            this.ctmTKCT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox15)).BeginInit();
            this.uiGroupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox26)).BeginInit();
            this.uiGroupBox26.SuspendLayout();
            this.uiTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKCTXuat)).BeginInit();
            this.ctmTKCTXuat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).BeginInit();
            this.uiGroupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox27)).BeginInit();
            this.uiGroupBox27.SuspendLayout();
            this.tpThanhKhoanTT38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgThanhKhoanTT38)).BeginInit();
            this.tpThanhKhoanMoi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgThanhKhoanMoi)).BeginInit();
            this.uiTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgThanhKhoan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(0, 32);
            this.grbMain.Size = new System.Drawing.Size(1204, 686);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox1.Controls.Add(this.tabHopDong);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 32);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1204, 686);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnXuatNhapTon);
            this.uiGroupBox6.Controls.Add(this.btnDangKyNPLCungUng);
            this.uiGroupBox6.Controls.Add(this.btnNThem);
            this.uiGroupBox6.Controls.Add(this.btnXuatNhapTon_Thietbi);
            this.uiGroupBox6.Controls.Add(this.btnNXoa);
            this.uiGroupBox6.Controls.Add(this.btnXoa);
            this.uiGroupBox6.Controls.Add(this.btnTinhLuongTon);
            this.uiGroupBox6.Controls.Add(this.lblGhiChu);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 632);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(1204, 54);
            this.uiGroupBox6.TabIndex = 5;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnXuatNhapTon
            // 
            this.btnXuatNhapTon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXuatNhapTon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatNhapTon.Image = ((System.Drawing.Image)(resources.GetObject("btnXuatNhapTon.Image")));
            this.btnXuatNhapTon.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXuatNhapTon.Location = new System.Drawing.Point(772, 20);
            this.btnXuatNhapTon.Name = "btnXuatNhapTon";
            this.btnXuatNhapTon.Size = new System.Drawing.Size(194, 23);
            this.btnXuatNhapTon.TabIndex = 0;
            this.btnXuatNhapTon.Text = "Quyết toán hợp đồng NPL-SP";
            this.btnXuatNhapTon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXuatNhapTon.Click += new System.EventHandler(this.btnXuatNhapTon_Click);
            // 
            // btnDangKyNPLCungUng
            // 
            this.btnDangKyNPLCungUng.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDangKyNPLCungUng.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDangKyNPLCungUng.Image = ((System.Drawing.Image)(resources.GetObject("btnDangKyNPLCungUng.Image")));
            this.btnDangKyNPLCungUng.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDangKyNPLCungUng.Location = new System.Drawing.Point(434, 20);
            this.btnDangKyNPLCungUng.Name = "btnDangKyNPLCungUng";
            this.btnDangKyNPLCungUng.Size = new System.Drawing.Size(174, 23);
            this.btnDangKyNPLCungUng.TabIndex = 0;
            this.btnDangKyNPLCungUng.Text = "Đăng ký NPL Cung ứng";
            this.btnDangKyNPLCungUng.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDangKyNPLCungUng.Click += new System.EventHandler(this.btnDangKyNPLCungUng_Click);
            // 
            // btnNThem
            // 
            this.btnNThem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNThem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNThem.Image = ((System.Drawing.Image)(resources.GetObject("btnNThem.Image")));
            this.btnNThem.ImageSize = new System.Drawing.Size(20, 20);
            this.btnNThem.Location = new System.Drawing.Point(972, 20);
            this.btnNThem.Name = "btnNThem";
            this.btnNThem.Size = new System.Drawing.Size(70, 23);
            this.btnNThem.TabIndex = 2;
            this.btnNThem.Text = "Thêm";
            this.btnNThem.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnNThem.Click += new System.EventHandler(this.btnNThem_Click_1);
            // 
            // btnXuatNhapTon_Thietbi
            // 
            this.btnXuatNhapTon_Thietbi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXuatNhapTon_Thietbi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatNhapTon_Thietbi.Image = ((System.Drawing.Image)(resources.GetObject("btnXuatNhapTon_Thietbi.Image")));
            this.btnXuatNhapTon_Thietbi.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXuatNhapTon_Thietbi.Location = new System.Drawing.Point(614, 20);
            this.btnXuatNhapTon_Thietbi.Name = "btnXuatNhapTon_Thietbi";
            this.btnXuatNhapTon_Thietbi.Size = new System.Drawing.Size(152, 23);
            this.btnXuatNhapTon_Thietbi.TabIndex = 0;
            this.btnXuatNhapTon_Thietbi.Text = "Quyết toán hợp bị";
            this.btnXuatNhapTon_Thietbi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXuatNhapTon_Thietbi.Click += new System.EventHandler(this.btnXuatNhapTon_Thietbi_Click);
            // 
            // btnNXoa
            // 
            this.btnNXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnNXoa.Image")));
            this.btnNXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnNXoa.Location = new System.Drawing.Point(1124, 20);
            this.btnNXoa.Name = "btnNXoa";
            this.btnNXoa.Size = new System.Drawing.Size(70, 23);
            this.btnNXoa.TabIndex = 4;
            this.btnNXoa.Text = "Đóng";
            this.btnNXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnNXoa.Click += new System.EventHandler(this.btnNXoa_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(1048, 20);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(70, 23);
            this.btnXoa.TabIndex = 3;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnTinhLuongTon
            // 
            this.btnTinhLuongTon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTinhLuongTon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTinhLuongTon.Image = ((System.Drawing.Image)(resources.GetObject("btnTinhLuongTon.Image")));
            this.btnTinhLuongTon.ImageSize = new System.Drawing.Size(20, 20);
            this.btnTinhLuongTon.ImageVerticalAlignment = Janus.Windows.EditControls.ImageVerticalAlignment.Empty;
            this.btnTinhLuongTon.Location = new System.Drawing.Point(12, 20);
            this.btnTinhLuongTon.Name = "btnTinhLuongTon";
            this.btnTinhLuongTon.Size = new System.Drawing.Size(190, 23);
            this.btnTinhLuongTon.TabIndex = 0;
            this.btnTinhLuongTon.Text = "Tính lượng tồn nhu cầu";
            this.btnTinhLuongTon.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTinhLuongTon.Click += new System.EventHandler(this.btnTinhLuongTon_Click);
            // 
            // lblGhiChu
            // 
            this.lblGhiChu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblGhiChu.AutoSize = true;
            this.lblGhiChu.BackColor = System.Drawing.Color.Transparent;
            this.lblGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGhiChu.ForeColor = System.Drawing.Color.Red;
            this.lblGhiChu.Location = new System.Drawing.Point(12, 25);
            this.lblGhiChu.Name = "lblGhiChu";
            this.lblGhiChu.Size = new System.Drawing.Size(466, 13);
            this.lblGhiChu.TabIndex = 1;
            this.lblGhiChu.Text = "Các dòng màu đỏ là các sản phẩm có định mức chưa duyệt hoặc chưa có định mức";
            // 
            // tabHopDong
            // 
            this.tabHopDong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabHopDong.BackColor = System.Drawing.Color.Transparent;
            this.tabHopDong.Location = new System.Drawing.Point(0, 1);
            this.tabHopDong.Name = "tabHopDong";
            this.tabHopDong.Size = new System.Drawing.Size(1204, 625);
            this.tabHopDong.TabIndex = 0;
            this.tabHopDong.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage8,
            this.tpNguyenPhuLieu,
            this.tpSanPham,
            this.tpThietBi,
            this.tpHangMau,
            this.tpNPLCU,
            this.tpNPLHuy,
            this.uiTabPage1,
            this.uiTabPage2,
            this.uiTabPage3,
            this.uiTabPage4,
            this.uiTabPage7,
            this.tpThanhKhoanTT38,
            this.tpThanhKhoanMoi,
            this.uiTabPage6});
            this.tabHopDong.TabsStateStyles.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.tabHopDong.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            this.tabHopDong.VisualStyleManager = this.vsmMain;
            this.tabHopDong.SelectedTabChanged += new Janus.Windows.UI.Tab.TabEventHandler(this.tabHopDong_SelectedTabChanged);
            // 
            // uiTabPage8
            // 
            this.uiTabPage8.Controls.Add(this.uiGroupBox3);
            this.uiTabPage8.Controls.Add(this.uiGroupBox4);
            this.uiTabPage8.Key = "tpThongTinHopDong";
            this.uiTabPage8.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage8.Name = "uiTabPage8";
            this.uiTabPage8.Size = new System.Drawing.Size(1202, 603);
            this.uiTabPage8.TabStop = true;
            this.uiTabPage8.Text = "Thông tin hợp đồng";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dgThongTinHopDong);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 160);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1202, 443);
            this.uiGroupBox3.TabIndex = 13;
            this.uiGroupBox3.Text = "Thông tin chi tiết";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgThongTinHopDong
            // 
            this.dgThongTinHopDong.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgThongTinHopDong.AlternatingColors = true;
            this.dgThongTinHopDong.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgThongTinHopDong.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgThongTinHopDong.ColumnAutoResize = true;
            dgThongTinHopDong_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgThongTinHopDong_DesignTimeLayout_Reference_0.Instance")));
            dgThongTinHopDong_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgThongTinHopDong_DesignTimeLayout_Reference_0});
            dgThongTinHopDong_DesignTimeLayout.LayoutString = resources.GetString("dgThongTinHopDong_DesignTimeLayout.LayoutString");
            this.dgThongTinHopDong.DesignTimeLayout = dgThongTinHopDong_DesignTimeLayout;
            this.dgThongTinHopDong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgThongTinHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgThongTinHopDong.GroupByBoxVisible = false;
            this.dgThongTinHopDong.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThongTinHopDong.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThongTinHopDong.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgThongTinHopDong.ImageList = this.ImageList1;
            this.dgThongTinHopDong.Location = new System.Drawing.Point(3, 17);
            this.dgThongTinHopDong.Name = "dgThongTinHopDong";
            this.dgThongTinHopDong.RecordNavigator = true;
            this.dgThongTinHopDong.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgThongTinHopDong.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThongTinHopDong.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgThongTinHopDong.Size = new System.Drawing.Size(1196, 423);
            this.dgThongTinHopDong.TabIndex = 0;
            this.dgThongTinHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgThongTinHopDong.VisualStyleManager = this.vsmMain;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox4.Controls.Add(this.txtDiaChi);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.txtTenDoiTac);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.btnExportTotal);
            this.uiGroupBox4.Controls.Add(this.lblTrangThaiThanhKhoan);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.nguyenTeControl1);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox4.Controls.Add(this.ccNgayGiaHan);
            this.uiGroupBox4.Controls.Add(this.nuocThue);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Controls.Add(this.ccNgayKetThucHD);
            this.uiGroupBox4.Controls.Add(this.ccNgayKyHD);
            this.uiGroupBox4.Controls.Add(this.label5);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1202, 160);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.Text = "Thông tin chung";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Controls.Add(this.dtpDateTo);
            this.uiGroupBox2.Controls.Add(this.dtpDateFrom);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Location = new System.Drawing.Point(676, 16);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(240, 100);
            this.uiGroupBox2.TabIndex = 18;
            this.uiGroupBox2.Text = "Quyết toán hợp đồng";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(10, 34);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Ngày  đầu kỳ :";
            // 
            // dtpDateTo
            // 
            this.dtpDateTo.CustomFormat = "dd/MM/yyyy";
            this.dtpDateTo.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dtpDateTo.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.dtpDateTo.DropDownCalendar.Name = "";
            this.dtpDateTo.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpDateTo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDateTo.Location = new System.Drawing.Point(130, 53);
            this.dtpDateTo.Name = "dtpDateTo";
            this.dtpDateTo.NullButtonText = "Xóa";
            this.dtpDateTo.ShowNullButton = true;
            this.dtpDateTo.Size = new System.Drawing.Size(95, 21);
            this.dtpDateTo.TabIndex = 8;
            this.dtpDateTo.TodayButtonText = "Hôm nay";
            this.dtpDateTo.Value = new System.DateTime(2015, 12, 31, 0, 0, 0, 0);
            this.dtpDateTo.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtpDateFrom
            // 
            this.dtpDateFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpDateFrom.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dtpDateFrom.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.dtpDateFrom.DropDownCalendar.Name = "";
            this.dtpDateFrom.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtpDateFrom.EditStyle = Janus.Windows.CalendarCombo.EditStyle.Free;
            this.dtpDateFrom.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDateFrom.Location = new System.Drawing.Point(130, 28);
            this.dtpDateFrom.Name = "dtpDateFrom";
            this.dtpDateFrom.Nullable = true;
            this.dtpDateFrom.NullButtonText = "Xóa";
            this.dtpDateFrom.ShowNullButton = true;
            this.dtpDateFrom.Size = new System.Drawing.Size(95, 21);
            this.dtpDateFrom.TabIndex = 4;
            this.dtpDateFrom.TodayButtonText = "Hôm nay";
            this.dtpDateFrom.Value = new System.DateTime(2015, 1, 1, 0, 0, 0, 0);
            this.dtpDateFrom.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(10, 57);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Ngày cuối kỳ :";
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.Location = new System.Drawing.Point(430, 94);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(196, 21);
            this.txtDiaChi.TabIndex = 16;
            this.txtDiaChi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(350, 99);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Địa chỉ đối tác";
            // 
            // txtTenDoiTac
            // 
            this.txtTenDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoiTac.Location = new System.Drawing.Point(148, 94);
            this.txtTenDoiTac.Name = "txtTenDoiTac";
            this.txtTenDoiTac.Size = new System.Drawing.Size(162, 21);
            this.txtTenDoiTac.TabIndex = 14;
            this.txtTenDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(673, 130);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(383, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Chọn ngày Quyết toán trước khi Chọn Quyết toán hợp đồng NPL -SP";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(28, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Tên đối tác";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(26, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Trạng thái thanh khoản";
            // 
            // btnExportTotal
            // 
            this.btnExportTotal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportTotal.Image = ((System.Drawing.Image)(resources.GetObject("btnExportTotal.Image")));
            this.btnExportTotal.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportTotal.Location = new System.Drawing.Point(430, 125);
            this.btnExportTotal.Name = "btnExportTotal";
            this.btnExportTotal.Size = new System.Drawing.Size(124, 23);
            this.btnExportTotal.TabIndex = 2;
            this.btnExportTotal.Text = "Xuất Excel";
            this.btnExportTotal.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportTotal.Click += new System.EventHandler(this.btnExportTotal_Click);
            // 
            // lblTrangThaiThanhKhoan
            // 
            this.lblTrangThaiThanhKhoan.AutoSize = true;
            this.lblTrangThaiThanhKhoan.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThaiThanhKhoan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThaiThanhKhoan.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThaiThanhKhoan.Location = new System.Drawing.Point(145, 130);
            this.lblTrangThaiThanhKhoan.Name = "lblTrangThaiThanhKhoan";
            this.lblTrangThaiThanhKhoan.Size = new System.Drawing.Size(110, 13);
            this.lblTrangThaiThanhKhoan.TabIndex = 12;
            this.lblTrangThaiThanhKhoan.Text = "Chưa thanh khoản";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(26, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Ngày kết thúc";
            // 
            // nguyenTeControl1
            // 
            this.nguyenTeControl1.BackColor = System.Drawing.Color.Transparent;
            this.nguyenTeControl1.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.nguyenTeControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nguyenTeControl1.Location = new System.Drawing.Point(430, 69);
            this.nguyenTeControl1.Ma = "";
            this.nguyenTeControl1.Name = "nguyenTeControl1";
            this.nguyenTeControl1.ReadOnly = false;
            this.nguyenTeControl1.Size = new System.Drawing.Size(196, 22);
            this.nguyenTeControl1.TabIndex = 10;
            this.nguyenTeControl1.VisualStyleManager = null;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(28, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Ngày gia hạn";
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDong.Location = new System.Drawing.Point(148, 16);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(162, 21);
            this.txtSoHopDong.TabIndex = 0;
            this.txtSoHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ccNgayGiaHan
            // 
            // 
            // 
            // 
            this.ccNgayGiaHan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayGiaHan.DropDownCalendar.Name = "";
            this.ccNgayGiaHan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayGiaHan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayGiaHan.IsNullDate = true;
            this.ccNgayGiaHan.Location = new System.Drawing.Point(148, 67);
            this.ccNgayGiaHan.Name = "ccNgayGiaHan";
            this.ccNgayGiaHan.Nullable = true;
            this.ccNgayGiaHan.NullButtonText = "Xóa";
            this.ccNgayGiaHan.ShowDropDown = false;
            this.ccNgayGiaHan.ShowNullButton = true;
            this.ccNgayGiaHan.Size = new System.Drawing.Size(95, 21);
            this.ccNgayGiaHan.TabIndex = 8;
            this.ccNgayGiaHan.TodayButtonText = "Hôm nay";
            this.ccNgayGiaHan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // nuocThue
            // 
            this.nuocThue.BackColor = System.Drawing.Color.Transparent;
            this.nuocThue.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.nuocThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nuocThue.Location = new System.Drawing.Point(430, 41);
            this.nuocThue.Ma = "";
            this.nuocThue.Name = "nuocThue";
            this.nuocThue.ReadOnly = false;
            this.nuocThue.Size = new System.Drawing.Size(196, 22);
            this.nuocThue.TabIndex = 6;
            this.nuocThue.VisualStyleManager = null;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số hợp đồng";
            // 
            // ccNgayKetThucHD
            // 
            // 
            // 
            // 
            this.ccNgayKetThucHD.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayKetThucHD.DropDownCalendar.Name = "";
            this.ccNgayKetThucHD.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayKetThucHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayKetThucHD.IsNullDate = true;
            this.ccNgayKetThucHD.Location = new System.Drawing.Point(148, 42);
            this.ccNgayKetThucHD.Name = "ccNgayKetThucHD";
            this.ccNgayKetThucHD.Nullable = true;
            this.ccNgayKetThucHD.NullButtonText = "Xóa";
            this.ccNgayKetThucHD.ShowDropDown = false;
            this.ccNgayKetThucHD.ShowNullButton = true;
            this.ccNgayKetThucHD.Size = new System.Drawing.Size(95, 21);
            this.ccNgayKetThucHD.TabIndex = 4;
            this.ccNgayKetThucHD.TodayButtonText = "Hôm nay";
            this.ccNgayKetThucHD.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // ccNgayKyHD
            // 
            // 
            // 
            // 
            this.ccNgayKyHD.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayKyHD.DropDownCalendar.Name = "";
            this.ccNgayKyHD.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayKyHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayKyHD.IsNullDate = true;
            this.ccNgayKyHD.Location = new System.Drawing.Point(430, 14);
            this.ccNgayKyHD.Name = "ccNgayKyHD";
            this.ccNgayKyHD.Nullable = true;
            this.ccNgayKyHD.NullButtonText = "Xóa";
            this.ccNgayKyHD.ShowDropDown = false;
            this.ccNgayKyHD.ShowNullButton = true;
            this.ccNgayKyHD.Size = new System.Drawing.Size(99, 21);
            this.ccNgayKyHD.TabIndex = 2;
            this.ccNgayKyHD.TodayButtonText = "Hôm nay";
            this.ccNgayKyHD.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(320, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Nguyên tệ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(320, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Nước thuê";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(320, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ngày ký";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tpNguyenPhuLieu
            // 
            this.tpNguyenPhuLieu.Controls.Add(this.dgNguyenPhuLieu);
            this.tpNguyenPhuLieu.Controls.Add(this.uiGroupBox7);
            this.tpNguyenPhuLieu.Key = "tpNPL";
            this.tpNguyenPhuLieu.Location = new System.Drawing.Point(1, 21);
            this.tpNguyenPhuLieu.Name = "tpNguyenPhuLieu";
            this.tpNguyenPhuLieu.Size = new System.Drawing.Size(1202, 603);
            this.tpNguyenPhuLieu.TabStop = true;
            this.tpNguyenPhuLieu.Text = "NPL";
            // 
            // dgNguyenPhuLieu
            // 
            this.dgNguyenPhuLieu.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgNguyenPhuLieu.AlternatingColors = true;
            this.dgNguyenPhuLieu.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgNguyenPhuLieu.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNguyenPhuLieu.ColumnAutoResize = true;
            this.dgNguyenPhuLieu.ContextMenuStrip = this.ctmNPL;
            dgNguyenPhuLieu_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgNguyenPhuLieu_DesignTimeLayout_Reference_0.Instance")));
            dgNguyenPhuLieu_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgNguyenPhuLieu_DesignTimeLayout_Reference_0});
            dgNguyenPhuLieu_DesignTimeLayout.LayoutString = resources.GetString("dgNguyenPhuLieu_DesignTimeLayout.LayoutString");
            this.dgNguyenPhuLieu.DesignTimeLayout = dgNguyenPhuLieu_DesignTimeLayout;
            this.dgNguyenPhuLieu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgNguyenPhuLieu.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNguyenPhuLieu.FrozenColumns = 3;
            this.dgNguyenPhuLieu.GroupByBoxVisible = false;
            this.dgNguyenPhuLieu.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNguyenPhuLieu.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNguyenPhuLieu.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNguyenPhuLieu.ImageList = this.ImageList1;
            this.dgNguyenPhuLieu.Location = new System.Drawing.Point(0, 68);
            this.dgNguyenPhuLieu.Name = "dgNguyenPhuLieu";
            this.dgNguyenPhuLieu.RecordNavigator = true;
            this.dgNguyenPhuLieu.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgNguyenPhuLieu.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNguyenPhuLieu.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNguyenPhuLieu.Size = new System.Drawing.Size(1202, 535);
            this.dgNguyenPhuLieu.TabIndex = 281;
            this.dgNguyenPhuLieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgNguyenPhuLieu.VisualStyleManager = this.vsmMain;
            this.dgNguyenPhuLieu.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgNguyenPhuLieu_LoadingRow);
            // 
            // ctmNPL
            // 
            this.ctmNPL.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xemTKN,
            this.xemTKTXNPL,
            this.xemPKChuaNPL,
            this.xemTKCTNPLItem,
            this.xemTKXuatSPToolStripMenuItem});
            this.ctmNPL.Name = "ctmNPL";
            this.ctmNPL.Size = new System.Drawing.Size(248, 114);
            // 
            // xemTKN
            // 
            this.xemTKN.Image = ((System.Drawing.Image)(resources.GetObject("xemTKN.Image")));
            this.xemTKN.Name = "xemTKN";
            this.xemTKN.Size = new System.Drawing.Size(247, 22);
            this.xemTKN.Text = "Xem tờ khai nhập NPL";
            this.xemTKN.Click += new System.EventHandler(this.xemTKN_Click);
            // 
            // xemTKTXNPL
            // 
            this.xemTKTXNPL.Image = ((System.Drawing.Image)(resources.GetObject("xemTKTXNPL.Image")));
            this.xemTKTXNPL.Name = "xemTKTXNPL";
            this.xemTKTXNPL.Size = new System.Drawing.Size(247, 22);
            this.xemTKTXNPL.Text = "Xem tờ khai tái xuất NPL";
            this.xemTKTXNPL.Click += new System.EventHandler(this.xemTKTXNPL_Click);
            // 
            // xemPKChuaNPL
            // 
            this.xemPKChuaNPL.Image = ((System.Drawing.Image)(resources.GetObject("xemPKChuaNPL.Image")));
            this.xemPKChuaNPL.Name = "xemPKChuaNPL";
            this.xemPKChuaNPL.Size = new System.Drawing.Size(247, 22);
            this.xemPKChuaNPL.Text = "Xem phụ kiện liên quan đến NPL";
            this.xemPKChuaNPL.Click += new System.EventHandler(this.xemPKChuaNPL_Click);
            // 
            // xemTKCTNPLItem
            // 
            this.xemTKCTNPLItem.Image = ((System.Drawing.Image)(resources.GetObject("xemTKCTNPLItem.Image")));
            this.xemTKCTNPLItem.Name = "xemTKCTNPLItem";
            this.xemTKCTNPLItem.Size = new System.Drawing.Size(247, 22);
            this.xemTKCTNPLItem.Text = "Xem TK chuyển tiếp NPL";
            this.xemTKCTNPLItem.Click += new System.EventHandler(this.xemTKCTNPLItem_Click);
            // 
            // xemTKXuatSPToolStripMenuItem
            // 
            this.xemTKXuatSPToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("xemTKXuatSPToolStripMenuItem.Image")));
            this.xemTKXuatSPToolStripMenuItem.Name = "xemTKXuatSPToolStripMenuItem";
            this.xemTKXuatSPToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.xemTKXuatSPToolStripMenuItem.Text = "Xem TK xuất SP";
            this.xemTKXuatSPToolStripMenuItem.Click += new System.EventHandler(this.xemTKXuatSPToolStripMenuItem_Click);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.uiGroupBox18);
            this.uiGroupBox7.Controls.Add(this.btnXuatExcelNPL);
            this.uiGroupBox7.Controls.Add(this.btnXemTKNNPL);
            this.uiGroupBox7.Controls.Add(this.btnXemPKNPL);
            this.uiGroupBox7.Controls.Add(this.uiButton1);
            this.uiGroupBox7.Controls.Add(this.btnXemTKTXNPL);
            this.uiGroupBox7.Controls.Add(this.btnInNPL);
            this.uiGroupBox7.Controls.Add(this.btnXemTKCTNPL);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(1202, 68);
            this.uiGroupBox7.TabIndex = 280;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox18
            // 
            this.uiGroupBox18.AutoScroll = true;
            this.uiGroupBox18.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox18.Controls.Add(this.btnSearchNPL);
            this.uiGroupBox18.Controls.Add(this.label22);
            this.uiGroupBox18.Controls.Add(this.txtMaNPL);
            this.uiGroupBox18.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox18.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox18.Name = "uiGroupBox18";
            this.uiGroupBox18.Size = new System.Drawing.Size(497, 57);
            this.uiGroupBox18.TabIndex = 287;
            this.uiGroupBox18.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchNPL
            // 
            this.btnSearchNPL.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchNPL.Image")));
            this.btnSearchNPL.Location = new System.Drawing.Point(378, 17);
            this.btnSearchNPL.Name = "btnSearchNPL";
            this.btnSearchNPL.Size = new System.Drawing.Size(89, 23);
            this.btnSearchNPL.TabIndex = 87;
            this.btnSearchNPL.Text = "Tìm kiếm";
            this.btnSearchNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchNPL.Click += new System.EventHandler(this.btnSearchNPL_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(22, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 13);
            this.label22.TabIndex = 86;
            this.label22.Text = "Mã NPL";
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaNPL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNPL.BackColor = System.Drawing.Color.White;
            this.txtMaNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNPL.Location = new System.Drawing.Point(119, 18);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.Size = new System.Drawing.Size(242, 21);
            this.txtMaNPL.TabIndex = 85;
            this.txtMaNPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaNPL.TextChanged += new System.EventHandler(this.btnSearchNPL_Click);
            // 
            // btnXuatExcelNPL
            // 
            this.btnXuatExcelNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatExcelNPL.Image = ((System.Drawing.Image)(resources.GetObject("btnXuatExcelNPL.Image")));
            this.btnXuatExcelNPL.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXuatExcelNPL.Location = new System.Drawing.Point(949, 40);
            this.btnXuatExcelNPL.Name = "btnXuatExcelNPL";
            this.btnXuatExcelNPL.Size = new System.Drawing.Size(82, 23);
            this.btnXuatExcelNPL.TabIndex = 278;
            this.btnXuatExcelNPL.Text = "Xuất Excel";
            this.btnXuatExcelNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXuatExcelNPL.Click += new System.EventHandler(this.btnXuatExcelNPL_Click);
            // 
            // btnXemTKNNPL
            // 
            this.btnXemTKNNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemTKNNPL.Image = ((System.Drawing.Image)(resources.GetObject("btnXemTKNNPL.Image")));
            this.btnXemTKNNPL.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXemTKNNPL.Location = new System.Drawing.Point(512, 40);
            this.btnXemTKNNPL.Name = "btnXemTKNNPL";
            this.btnXemTKNNPL.Size = new System.Drawing.Size(167, 23);
            this.btnXemTKNNPL.TabIndex = 267;
            this.btnXemTKNNPL.Text = "Xem tờ khai Nhập NPL";
            this.btnXemTKNNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemTKNNPL.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemPKNPL
            // 
            this.btnXemPKNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemPKNPL.Image = ((System.Drawing.Image)(resources.GetObject("btnXemPKNPL.Image")));
            this.btnXemPKNPL.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXemPKNPL.Location = new System.Drawing.Point(512, 11);
            this.btnXemPKNPL.Name = "btnXemPKNPL";
            this.btnXemPKNPL.Size = new System.Drawing.Size(167, 23);
            this.btnXemPKNPL.TabIndex = 268;
            this.btnXemPKNPL.Text = "Xem PK liên quan NPL";
            this.btnXemPKNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemPKNPL.Click += new System.EventHandler(this.btn_Click);
            // 
            // uiButton1
            // 
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Image = ((System.Drawing.Image)(resources.GetObject("uiButton1.Image")));
            this.uiButton1.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton1.Location = new System.Drawing.Point(864, 11);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(167, 23);
            this.uiButton1.TabIndex = 277;
            this.uiButton1.Text = "Tính lượng nhu cầu";
            this.uiButton1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // btnXemTKTXNPL
            // 
            this.btnXemTKTXNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemTKTXNPL.Image = ((System.Drawing.Image)(resources.GetObject("btnXemTKTXNPL.Image")));
            this.btnXemTKTXNPL.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXemTKTXNPL.Location = new System.Drawing.Point(685, 11);
            this.btnXemTKTXNPL.Name = "btnXemTKTXNPL";
            this.btnXemTKTXNPL.Size = new System.Drawing.Size(173, 23);
            this.btnXemTKTXNPL.TabIndex = 269;
            this.btnXemTKTXNPL.Text = "Xem tờ khai Tái Xuất NPL";
            this.btnXemTKTXNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemTKTXNPL.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnInNPL
            // 
            this.btnInNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInNPL.Image = ((System.Drawing.Image)(resources.GetObject("btnInNPL.Image")));
            this.btnInNPL.ImageSize = new System.Drawing.Size(20, 20);
            this.btnInNPL.Location = new System.Drawing.Point(864, 40);
            this.btnInNPL.Name = "btnInNPL";
            this.btnInNPL.Size = new System.Drawing.Size(82, 23);
            this.btnInNPL.TabIndex = 276;
            this.btnInNPL.Text = "In NPL";
            this.btnInNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnInNPL.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemTKCTNPL
            // 
            this.btnXemTKCTNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemTKCTNPL.Image = ((System.Drawing.Image)(resources.GetObject("btnXemTKCTNPL.Image")));
            this.btnXemTKCTNPL.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXemTKCTNPL.Location = new System.Drawing.Point(685, 40);
            this.btnXemTKCTNPL.Name = "btnXemTKCTNPL";
            this.btnXemTKCTNPL.Size = new System.Drawing.Size(173, 23);
            this.btnXemTKCTNPL.TabIndex = 270;
            this.btnXemTKCTNPL.Text = "Xem tờ khai Chuyển tiếp NPL";
            this.btnXemTKCTNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemTKCTNPL.Click += new System.EventHandler(this.btn_Click);
            // 
            // tpSanPham
            // 
            this.tpSanPham.Controls.Add(this.dgSanPham);
            this.tpSanPham.Controls.Add(this.uiGroupBox5);
            this.tpSanPham.Key = "tpSP";
            this.tpSanPham.Location = new System.Drawing.Point(1, 21);
            this.tpSanPham.Name = "tpSanPham";
            this.tpSanPham.Size = new System.Drawing.Size(1202, 603);
            this.tpSanPham.TabStop = true;
            this.tpSanPham.Text = "SP";
            // 
            // dgSanPham
            // 
            this.dgSanPham.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgSanPham.AlternatingColors = true;
            this.dgSanPham.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgSanPham.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgSanPham.ColumnAutoResize = true;
            this.dgSanPham.ContextMenuStrip = this.ctmSP;
            dgSanPham_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgSanPham_DesignTimeLayout_Reference_0.Instance")));
            dgSanPham_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgSanPham_DesignTimeLayout_Reference_0});
            dgSanPham_DesignTimeLayout.LayoutString = resources.GetString("dgSanPham_DesignTimeLayout.LayoutString");
            this.dgSanPham.DesignTimeLayout = dgSanPham_DesignTimeLayout;
            this.dgSanPham.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgSanPham.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgSanPham.GroupByBoxVisible = false;
            this.dgSanPham.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgSanPham.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgSanPham.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgSanPham.ImageList = this.ImageList1;
            this.dgSanPham.Location = new System.Drawing.Point(0, 68);
            this.dgSanPham.Name = "dgSanPham";
            this.dgSanPham.RecordNavigator = true;
            this.dgSanPham.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgSanPham.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgSanPham.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgSanPham.Size = new System.Drawing.Size(1202, 535);
            this.dgSanPham.TabIndex = 279;
            this.dgSanPham.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgSanPham.VisualStyleManager = this.vsmMain;
            this.dgSanPham.UpdatingCell += new Janus.Windows.GridEX.UpdatingCellEventHandler(this.dgSanPham_UpdatingCell);
            this.dgSanPham.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgSanPham_LoadingRow);
            // 
            // ctmSP
            // 
            this.ctmSP.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xemTKXSP,
            this.xemPKSP,
            this.xemDMItem,
            this.xemTKCTSPItem,
            this.inDMSPItem});
            this.ctmSP.Name = "ctmSP";
            this.ctmSP.Size = new System.Drawing.Size(239, 114);
            // 
            // xemTKXSP
            // 
            this.xemTKXSP.Image = ((System.Drawing.Image)(resources.GetObject("xemTKXSP.Image")));
            this.xemTKXSP.Name = "xemTKXSP";
            this.xemTKXSP.Size = new System.Drawing.Size(238, 22);
            this.xemTKXSP.Text = "Xem tờ khai xuất SP";
            this.xemTKXSP.Click += new System.EventHandler(this.xemTKXSP_Click);
            // 
            // xemPKSP
            // 
            this.xemPKSP.Image = ((System.Drawing.Image)(resources.GetObject("xemPKSP.Image")));
            this.xemPKSP.Name = "xemPKSP";
            this.xemPKSP.Size = new System.Drawing.Size(238, 22);
            this.xemPKSP.Text = "Xem phụ kiện liên quan đến SP";
            this.xemPKSP.Click += new System.EventHandler(this.xemPKSP_Click);
            // 
            // xemDMItem
            // 
            this.xemDMItem.Image = ((System.Drawing.Image)(resources.GetObject("xemDMItem.Image")));
            this.xemDMItem.Name = "xemDMItem";
            this.xemDMItem.Size = new System.Drawing.Size(238, 22);
            this.xemDMItem.Text = "Xem định mức SP";
            this.xemDMItem.Click += new System.EventHandler(this.xemDMItem_Click);
            // 
            // xemTKCTSPItem
            // 
            this.xemTKCTSPItem.Image = ((System.Drawing.Image)(resources.GetObject("xemTKCTSPItem.Image")));
            this.xemTKCTSPItem.Name = "xemTKCTSPItem";
            this.xemTKCTSPItem.Size = new System.Drawing.Size(238, 22);
            this.xemTKCTSPItem.Text = "Xem TK chuyển tiếp SP";
            this.xemTKCTSPItem.Click += new System.EventHandler(this.xemTKCTSPItem_Click);
            // 
            // inDMSPItem
            // 
            this.inDMSPItem.Image = ((System.Drawing.Image)(resources.GetObject("inDMSPItem.Image")));
            this.inDMSPItem.Name = "inDMSPItem";
            this.inDMSPItem.Size = new System.Drawing.Size(238, 22);
            this.inDMSPItem.Text = "In định mức SP";
            this.inDMSPItem.Click += new System.EventHandler(this.inDMSPItem_Click);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.uiGroupBox17);
            this.uiGroupBox5.Controls.Add(this.btnXemTKXSP);
            this.uiGroupBox5.Controls.Add(this.btnXuatExcelSanPham);
            this.uiGroupBox5.Controls.Add(this.btnXemDMSP);
            this.uiGroupBox5.Controls.Add(this.btnXemPKSP);
            this.uiGroupBox5.Controls.Add(this.btnInSP);
            this.uiGroupBox5.Controls.Add(this.btnXemTKCTSP);
            this.uiGroupBox5.Controls.Add(this.btnInDMSP);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1202, 68);
            this.uiGroupBox5.TabIndex = 278;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox17
            // 
            this.uiGroupBox17.AutoScroll = true;
            this.uiGroupBox17.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox17.Controls.Add(this.btnSearchSP);
            this.uiGroupBox17.Controls.Add(this.label13);
            this.uiGroupBox17.Controls.Add(this.txtMaSP);
            this.uiGroupBox17.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox17.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox17.Name = "uiGroupBox17";
            this.uiGroupBox17.Size = new System.Drawing.Size(497, 57);
            this.uiGroupBox17.TabIndex = 288;
            this.uiGroupBox17.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchSP
            // 
            this.btnSearchSP.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchSP.Image")));
            this.btnSearchSP.Location = new System.Drawing.Point(378, 17);
            this.btnSearchSP.Name = "btnSearchSP";
            this.btnSearchSP.Size = new System.Drawing.Size(89, 23);
            this.btnSearchSP.TabIndex = 87;
            this.btnSearchSP.Text = "Tìm kiếm";
            this.btnSearchSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchSP.Click += new System.EventHandler(this.btnSearchSP_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(22, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 13);
            this.label13.TabIndex = 86;
            this.label13.Text = "Mã sản phẩm";
            // 
            // txtMaSP
            // 
            this.txtMaSP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaSP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSP.BackColor = System.Drawing.Color.White;
            this.txtMaSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSP.Location = new System.Drawing.Point(119, 18);
            this.txtMaSP.Name = "txtMaSP";
            this.txtMaSP.Size = new System.Drawing.Size(242, 21);
            this.txtMaSP.TabIndex = 85;
            this.txtMaSP.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaSP.TextChanged += new System.EventHandler(this.btnSearchSP_Click);
            // 
            // btnXemTKXSP
            // 
            this.btnXemTKXSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemTKXSP.Image = ((System.Drawing.Image)(resources.GetObject("btnXemTKXSP.Image")));
            this.btnXemTKXSP.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXemTKXSP.Location = new System.Drawing.Point(514, 10);
            this.btnXemTKXSP.Name = "btnXemTKXSP";
            this.btnXemTKXSP.Size = new System.Drawing.Size(167, 23);
            this.btnXemTKXSP.TabIndex = 270;
            this.btnXemTKXSP.Text = "Xem tờ khai Xuất SP";
            this.btnXemTKXSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemTKXSP.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXuatExcelSanPham
            // 
            this.btnXuatExcelSanPham.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatExcelSanPham.Image = ((System.Drawing.Image)(resources.GetObject("btnXuatExcelSanPham.Image")));
            this.btnXuatExcelSanPham.Location = new System.Drawing.Point(873, 39);
            this.btnXuatExcelSanPham.Name = "btnXuatExcelSanPham";
            this.btnXuatExcelSanPham.Size = new System.Drawing.Size(87, 23);
            this.btnXuatExcelSanPham.TabIndex = 276;
            this.btnXuatExcelSanPham.Text = "Xuất Excel";
            this.btnXuatExcelSanPham.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXuatExcelSanPham.Click += new System.EventHandler(this.btnXuatExcelSanPham_Click);
            // 
            // btnXemDMSP
            // 
            this.btnXemDMSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemDMSP.Image = ((System.Drawing.Image)(resources.GetObject("btnXemDMSP.Image")));
            this.btnXemDMSP.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXemDMSP.Location = new System.Drawing.Point(687, 39);
            this.btnXemDMSP.Name = "btnXemDMSP";
            this.btnXemDMSP.Size = new System.Drawing.Size(87, 23);
            this.btnXemDMSP.TabIndex = 271;
            this.btnXemDMSP.Text = "Xem ĐM";
            this.btnXemDMSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemDMSP.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemPKSP
            // 
            this.btnXemPKSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemPKSP.Image = ((System.Drawing.Image)(resources.GetObject("btnXemPKSP.Image")));
            this.btnXemPKSP.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXemPKSP.Location = new System.Drawing.Point(687, 10);
            this.btnXemPKSP.Name = "btnXemPKSP";
            this.btnXemPKSP.Size = new System.Drawing.Size(180, 23);
            this.btnXemPKSP.TabIndex = 272;
            this.btnXemPKSP.Text = "Xem PK liên quan SP";
            this.btnXemPKSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemPKSP.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnInSP
            // 
            this.btnInSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInSP.Image = ((System.Drawing.Image)(resources.GetObject("btnInSP.Image")));
            this.btnInSP.ImageSize = new System.Drawing.Size(20, 20);
            this.btnInSP.Location = new System.Drawing.Point(873, 10);
            this.btnInSP.Name = "btnInSP";
            this.btnInSP.Size = new System.Drawing.Size(87, 23);
            this.btnInSP.TabIndex = 275;
            this.btnInSP.Text = "In SP";
            this.btnInSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnInSP.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemTKCTSP
            // 
            this.btnXemTKCTSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemTKCTSP.Image = ((System.Drawing.Image)(resources.GetObject("btnXemTKCTSP.Image")));
            this.btnXemTKCTSP.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXemTKCTSP.Location = new System.Drawing.Point(514, 39);
            this.btnXemTKCTSP.Name = "btnXemTKCTSP";
            this.btnXemTKCTSP.Size = new System.Drawing.Size(167, 23);
            this.btnXemTKCTSP.TabIndex = 273;
            this.btnXemTKCTSP.Text = "Xem tờ khai Chuyển Tiếp SP";
            this.btnXemTKCTSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemTKCTSP.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnInDMSP
            // 
            this.btnInDMSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInDMSP.Image = ((System.Drawing.Image)(resources.GetObject("btnInDMSP.Image")));
            this.btnInDMSP.ImageSize = new System.Drawing.Size(20, 20);
            this.btnInDMSP.Location = new System.Drawing.Point(780, 39);
            this.btnInDMSP.Name = "btnInDMSP";
            this.btnInDMSP.Size = new System.Drawing.Size(87, 23);
            this.btnInDMSP.TabIndex = 274;
            this.btnInDMSP.Text = "In ĐM";
            this.btnInDMSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnInDMSP.Click += new System.EventHandler(this.btnInDMSP_Click);
            // 
            // tpThietBi
            // 
            this.tpThietBi.Controls.Add(this.dgThietBi);
            this.tpThietBi.Controls.Add(this.uiGroupBox8);
            this.tpThietBi.Key = "tpTB";
            this.tpThietBi.Location = new System.Drawing.Point(1, 21);
            this.tpThietBi.Name = "tpThietBi";
            this.tpThietBi.Size = new System.Drawing.Size(1202, 603);
            this.tpThietBi.TabStop = true;
            this.tpThietBi.Text = "TB";
            // 
            // dgThietBi
            // 
            this.dgThietBi.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgThietBi.AlternatingColors = true;
            this.dgThietBi.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgThietBi.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgThietBi.ColumnAutoResize = true;
            this.dgThietBi.ContextMenuStrip = this.ctmTB;
            dgThietBi_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgThietBi_DesignTimeLayout_Reference_0.Instance")));
            dgThietBi_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgThietBi_DesignTimeLayout_Reference_0});
            dgThietBi_DesignTimeLayout.LayoutString = resources.GetString("dgThietBi_DesignTimeLayout.LayoutString");
            this.dgThietBi.DesignTimeLayout = dgThietBi_DesignTimeLayout;
            this.dgThietBi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgThietBi.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgThietBi.GroupByBoxVisible = false;
            this.dgThietBi.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThietBi.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThietBi.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgThietBi.ImageList = this.ImageList1;
            this.dgThietBi.Location = new System.Drawing.Point(0, 61);
            this.dgThietBi.Name = "dgThietBi";
            this.dgThietBi.RecordNavigator = true;
            this.dgThietBi.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgThietBi.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThietBi.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgThietBi.Size = new System.Drawing.Size(1202, 542);
            this.dgThietBi.TabIndex = 274;
            this.dgThietBi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgThietBi.VisualStyleManager = this.vsmMain;
            this.dgThietBi.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgThietBi_LoadingRow);
            // 
            // ctmTB
            // 
            this.ctmTB.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xemTKNTB,
            this.xemTKXTB,
            this.xemTKChuyenTiepTB,
            this.xemPKTB});
            this.ctmTB.Name = "ctmTB";
            this.ctmTB.Size = new System.Drawing.Size(261, 92);
            // 
            // xemTKNTB
            // 
            this.xemTKNTB.Image = ((System.Drawing.Image)(resources.GetObject("xemTKNTB.Image")));
            this.xemTKNTB.Name = "xemTKNTB";
            this.xemTKNTB.Size = new System.Drawing.Size(260, 22);
            this.xemTKNTB.Text = "Xem tờ khai nhập TB này";
            this.xemTKNTB.Click += new System.EventHandler(this.xemTKNTB_Click);
            // 
            // xemTKXTB
            // 
            this.xemTKXTB.Image = ((System.Drawing.Image)(resources.GetObject("xemTKXTB.Image")));
            this.xemTKXTB.Name = "xemTKXTB";
            this.xemTKXTB.Size = new System.Drawing.Size(260, 22);
            this.xemTKXTB.Text = "Xem tờ khai xuất TB này";
            this.xemTKXTB.Click += new System.EventHandler(this.xemTKXTB_Click);
            // 
            // xemTKChuyenTiepTB
            // 
            this.xemTKChuyenTiepTB.Image = ((System.Drawing.Image)(resources.GetObject("xemTKChuyenTiepTB.Image")));
            this.xemTKChuyenTiepTB.Name = "xemTKChuyenTiepTB";
            this.xemTKChuyenTiepTB.Size = new System.Drawing.Size(260, 22);
            this.xemTKChuyenTiepTB.Text = "Xem TK chuyển tiếp TB";
            this.xemTKChuyenTiepTB.Click += new System.EventHandler(this.xemTKChuyenTiepTB_Click);
            // 
            // xemPKTB
            // 
            this.xemPKTB.Image = ((System.Drawing.Image)(resources.GetObject("xemPKTB.Image")));
            this.xemPKTB.Name = "xemPKTB";
            this.xemPKTB.Size = new System.Drawing.Size(260, 22);
            this.xemPKTB.Text = "Xem phụ kiện liên quan đến TB này";
            this.xemPKTB.Click += new System.EventHandler(this.xemPKTB_Click);
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox8.Controls.Add(this.uiGroupBox19);
            this.uiGroupBox8.Controls.Add(this.btnXemTKNTB);
            this.uiGroupBox8.Controls.Add(this.btnXemTKXTB);
            this.uiGroupBox8.Controls.Add(this.btnXemPKTB);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox8.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(1202, 61);
            this.uiGroupBox8.TabIndex = 273;
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox19
            // 
            this.uiGroupBox19.AutoScroll = true;
            this.uiGroupBox19.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox19.Controls.Add(this.btnSearchTB);
            this.uiGroupBox19.Controls.Add(this.label14);
            this.uiGroupBox19.Controls.Add(this.txtMaTB);
            this.uiGroupBox19.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox19.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox19.Name = "uiGroupBox19";
            this.uiGroupBox19.Size = new System.Drawing.Size(497, 50);
            this.uiGroupBox19.TabIndex = 288;
            this.uiGroupBox19.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchTB
            // 
            this.btnSearchTB.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchTB.Image")));
            this.btnSearchTB.Location = new System.Drawing.Point(378, 17);
            this.btnSearchTB.Name = "btnSearchTB";
            this.btnSearchTB.Size = new System.Drawing.Size(89, 23);
            this.btnSearchTB.TabIndex = 87;
            this.btnSearchTB.Text = "Tìm kiếm";
            this.btnSearchTB.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchTB.Click += new System.EventHandler(this.btnSearchTB_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(22, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 13);
            this.label14.TabIndex = 86;
            this.label14.Text = "Mã thiết bị";
            // 
            // txtMaTB
            // 
            this.txtMaTB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaTB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaTB.BackColor = System.Drawing.Color.White;
            this.txtMaTB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaTB.Location = new System.Drawing.Point(119, 18);
            this.txtMaTB.Name = "txtMaTB";
            this.txtMaTB.Size = new System.Drawing.Size(242, 21);
            this.txtMaTB.TabIndex = 85;
            this.txtMaTB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaTB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaTB.TextChanged += new System.EventHandler(this.btnSearchTB_Click);
            // 
            // btnXemTKNTB
            // 
            this.btnXemTKNTB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemTKNTB.Image = ((System.Drawing.Image)(resources.GetObject("btnXemTKNTB.Image")));
            this.btnXemTKNTB.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXemTKNTB.Location = new System.Drawing.Point(508, 11);
            this.btnXemTKNTB.Name = "btnXemTKNTB";
            this.btnXemTKNTB.Size = new System.Drawing.Size(167, 23);
            this.btnXemTKNTB.TabIndex = 270;
            this.btnXemTKNTB.Text = "Xem tờ khai Nhập TB";
            this.btnXemTKNTB.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemTKNTB.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemTKXTB
            // 
            this.btnXemTKXTB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemTKXTB.Image = ((System.Drawing.Image)(resources.GetObject("btnXemTKXTB.Image")));
            this.btnXemTKXTB.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXemTKXTB.Location = new System.Drawing.Point(681, 11);
            this.btnXemTKXTB.Name = "btnXemTKXTB";
            this.btnXemTKXTB.Size = new System.Drawing.Size(167, 23);
            this.btnXemTKXTB.TabIndex = 272;
            this.btnXemTKXTB.Text = "Xem tờ khai Xuất TB";
            this.btnXemTKXTB.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemTKXTB.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemPKTB
            // 
            this.btnXemPKTB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemPKTB.Image = ((System.Drawing.Image)(resources.GetObject("btnXemPKTB.Image")));
            this.btnXemPKTB.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXemPKTB.Location = new System.Drawing.Point(508, 35);
            this.btnXemPKTB.Name = "btnXemPKTB";
            this.btnXemPKTB.Size = new System.Drawing.Size(167, 23);
            this.btnXemPKTB.TabIndex = 271;
            this.btnXemPKTB.Text = "Xem PK liên quan TB ";
            this.btnXemPKTB.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemPKTB.Click += new System.EventHandler(this.btn_Click);
            // 
            // tpHangMau
            // 
            this.tpHangMau.Controls.Add(this.dgHangMau);
            this.tpHangMau.Controls.Add(this.uiGroupBox9);
            this.tpHangMau.Key = "tpHangMau";
            this.tpHangMau.Location = new System.Drawing.Point(1, 21);
            this.tpHangMau.Name = "tpHangMau";
            this.tpHangMau.Size = new System.Drawing.Size(1202, 603);
            this.tpHangMau.TabStop = true;
            this.tpHangMau.Text = "HM";
            // 
            // dgHangMau
            // 
            this.dgHangMau.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgHangMau.AlternatingColors = true;
            this.dgHangMau.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgHangMau.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgHangMau.ColumnAutoResize = true;
            this.dgHangMau.ContextMenuStrip = this.cmtHM;
            dgHangMau_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgHangMau_DesignTimeLayout_Reference_0.Instance")));
            dgHangMau_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgHangMau_DesignTimeLayout_Reference_0});
            dgHangMau_DesignTimeLayout.LayoutString = resources.GetString("dgHangMau_DesignTimeLayout.LayoutString");
            this.dgHangMau.DesignTimeLayout = dgHangMau_DesignTimeLayout;
            this.dgHangMau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgHangMau.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgHangMau.GroupByBoxVisible = false;
            this.dgHangMau.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHangMau.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgHangMau.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgHangMau.ImageList = this.ImageList1;
            this.dgHangMau.Location = new System.Drawing.Point(0, 71);
            this.dgHangMau.Name = "dgHangMau";
            this.dgHangMau.RecordNavigator = true;
            this.dgHangMau.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgHangMau.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgHangMau.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgHangMau.Size = new System.Drawing.Size(1202, 532);
            this.dgHangMau.TabIndex = 278;
            this.dgHangMau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgHangMau.VisualStyleManager = this.vsmMain;
            // 
            // cmtHM
            // 
            this.cmtHM.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5});
            this.cmtHM.Name = "ctmTB";
            this.cmtHM.Size = new System.Drawing.Size(268, 92);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem2.Image")));
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(267, 22);
            this.toolStripMenuItem2.Text = "Xem tờ khai nhập HM này";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem3.Image")));
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(267, 22);
            this.toolStripMenuItem3.Text = "Xem tờ khai xuất HM này";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem4.Image")));
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(267, 22);
            this.toolStripMenuItem4.Text = "Xem TK chuyển tiếp HM";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem5.Image")));
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(267, 22);
            this.toolStripMenuItem5.Text = "Xem phụ kiện liên quan đến HM này";
            // 
            // uiGroupBox9
            // 
            this.uiGroupBox9.AutoScroll = true;
            this.uiGroupBox9.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox9.Controls.Add(this.uiGroupBox20);
            this.uiGroupBox9.Controls.Add(this.uiButton2);
            this.uiGroupBox9.Controls.Add(this.uiButton4);
            this.uiGroupBox9.Controls.Add(this.uiButton3);
            this.uiGroupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox9.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox9.Name = "uiGroupBox9";
            this.uiGroupBox9.Size = new System.Drawing.Size(1202, 71);
            this.uiGroupBox9.TabIndex = 277;
            this.uiGroupBox9.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox20
            // 
            this.uiGroupBox20.AutoScroll = true;
            this.uiGroupBox20.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox20.Controls.Add(this.btnSearchHM);
            this.uiGroupBox20.Controls.Add(this.label15);
            this.uiGroupBox20.Controls.Add(this.txtMaHM);
            this.uiGroupBox20.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox20.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox20.Name = "uiGroupBox20";
            this.uiGroupBox20.Size = new System.Drawing.Size(497, 60);
            this.uiGroupBox20.TabIndex = 288;
            this.uiGroupBox20.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchHM
            // 
            this.btnSearchHM.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchHM.Image")));
            this.btnSearchHM.Location = new System.Drawing.Point(378, 17);
            this.btnSearchHM.Name = "btnSearchHM";
            this.btnSearchHM.Size = new System.Drawing.Size(89, 23);
            this.btnSearchHM.TabIndex = 87;
            this.btnSearchHM.Text = "Tìm kiếm";
            this.btnSearchHM.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchHM.Click += new System.EventHandler(this.btnSearchHM_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(22, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 13);
            this.label15.TabIndex = 86;
            this.label15.Text = "Mã hàng mẫu";
            // 
            // txtMaHM
            // 
            this.txtMaHM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaHM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaHM.BackColor = System.Drawing.Color.White;
            this.txtMaHM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHM.Location = new System.Drawing.Point(119, 18);
            this.txtMaHM.Name = "txtMaHM";
            this.txtMaHM.Size = new System.Drawing.Size(242, 21);
            this.txtMaHM.TabIndex = 85;
            this.txtMaHM.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHM.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHM.TextChanged += new System.EventHandler(this.btnSearchHM_Click);
            // 
            // uiButton2
            // 
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Image = ((System.Drawing.Image)(resources.GetObject("uiButton2.Image")));
            this.uiButton2.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton2.Location = new System.Drawing.Point(681, 11);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(167, 23);
            this.uiButton2.TabIndex = 276;
            this.uiButton2.Text = "Xem tờ khai Xuất HM";
            this.uiButton2.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // uiButton4
            // 
            this.uiButton4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton4.Image = ((System.Drawing.Image)(resources.GetObject("uiButton4.Image")));
            this.uiButton4.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton4.Location = new System.Drawing.Point(508, 11);
            this.uiButton4.Name = "uiButton4";
            this.uiButton4.Size = new System.Drawing.Size(167, 23);
            this.uiButton4.TabIndex = 274;
            this.uiButton4.Text = "Xem tờ khai Nhập HM";
            this.uiButton4.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // uiButton3
            // 
            this.uiButton3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton3.Image = ((System.Drawing.Image)(resources.GetObject("uiButton3.Image")));
            this.uiButton3.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton3.Location = new System.Drawing.Point(508, 40);
            this.uiButton3.Name = "uiButton3";
            this.uiButton3.Size = new System.Drawing.Size(167, 23);
            this.uiButton3.TabIndex = 275;
            this.uiButton3.Text = "Xem PK liên quan HM";
            this.uiButton3.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // tpNPLCU
            // 
            this.tpNPLCU.Controls.Add(this.dgNPLCungUng);
            this.tpNPLCU.Controls.Add(this.uiGroupBox10);
            this.tpNPLCU.Key = "tpNPLCU";
            this.tpNPLCU.Location = new System.Drawing.Point(1, 21);
            this.tpNPLCU.Name = "tpNPLCU";
            this.tpNPLCU.Size = new System.Drawing.Size(1202, 603);
            this.tpNPLCU.TabStop = true;
            this.tpNPLCU.Text = "NPL CU";
            // 
            // dgNPLCungUng
            // 
            this.dgNPLCungUng.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgNPLCungUng.AlternatingColors = true;
            this.dgNPLCungUng.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgNPLCungUng.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNPLCungUng.ColumnAutoResize = true;
            this.dgNPLCungUng.ContextMenuStrip = this.cmtNPL_CU;
            dgNPLCungUng_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgNPLCungUng_DesignTimeLayout_Reference_0.Instance")));
            dgNPLCungUng_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgNPLCungUng_DesignTimeLayout_Reference_0});
            dgNPLCungUng_DesignTimeLayout.LayoutString = resources.GetString("dgNPLCungUng_DesignTimeLayout.LayoutString");
            this.dgNPLCungUng.DesignTimeLayout = dgNPLCungUng_DesignTimeLayout;
            this.dgNPLCungUng.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgNPLCungUng.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgNPLCungUng.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCungUng.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNPLCungUng.GroupByBoxVisible = false;
            this.dgNPLCungUng.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCungUng.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgNPLCungUng.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCungUng.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNPLCungUng.Hierarchical = true;
            this.dgNPLCungUng.ImageList = this.ImageList1;
            this.dgNPLCungUng.Location = new System.Drawing.Point(0, 62);
            this.dgNPLCungUng.Name = "dgNPLCungUng";
            this.dgNPLCungUng.RecordNavigator = true;
            this.dgNPLCungUng.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgNPLCungUng.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNPLCungUng.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgNPLCungUng.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCungUng.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgNPLCungUng.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgNPLCungUng.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLCungUng.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgNPLCungUng.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNPLCungUng.Size = new System.Drawing.Size(1202, 541);
            this.dgNPLCungUng.TabIndex = 279;
            this.dgNPLCungUng.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgNPLCungUng.VisualStyleManager = this.vsmMain;
            this.dgNPLCungUng.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgNPLCungUng.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgNPLCungUng_DeletingRecords);
            // 
            // cmtNPL_CU
            // 
            this.cmtNPL_CU.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripXuatExcel_NPLCU});
            this.cmtNPL_CU.Name = "ctmTKCT";
            this.cmtNPL_CU.Size = new System.Drawing.Size(129, 26);
            // 
            // toolStripXuatExcel_NPLCU
            // 
            this.toolStripXuatExcel_NPLCU.Image = ((System.Drawing.Image)(resources.GetObject("toolStripXuatExcel_NPLCU.Image")));
            this.toolStripXuatExcel_NPLCU.Name = "toolStripXuatExcel_NPLCU";
            this.toolStripXuatExcel_NPLCU.Size = new System.Drawing.Size(128, 22);
            this.toolStripXuatExcel_NPLCU.Text = "Xuất Excel";
            this.toolStripXuatExcel_NPLCU.Click += new System.EventHandler(this.toolStripXuatExcel_NPLCU_Click);
            // 
            // uiGroupBox10
            // 
            this.uiGroupBox10.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox10.Controls.Add(this.uiGroupBox21);
            this.uiGroupBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox10.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox10.Name = "uiGroupBox10";
            this.uiGroupBox10.Size = new System.Drawing.Size(1202, 62);
            this.uiGroupBox10.TabIndex = 278;
            this.uiGroupBox10.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox21
            // 
            this.uiGroupBox21.AutoScroll = true;
            this.uiGroupBox21.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox21.Controls.Add(this.btnSearchNPLCU);
            this.uiGroupBox21.Controls.Add(this.label16);
            this.uiGroupBox21.Controls.Add(this.txtMaNPLCU);
            this.uiGroupBox21.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox21.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox21.Name = "uiGroupBox21";
            this.uiGroupBox21.Size = new System.Drawing.Size(497, 51);
            this.uiGroupBox21.TabIndex = 288;
            this.uiGroupBox21.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchNPLCU
            // 
            this.btnSearchNPLCU.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchNPLCU.Image")));
            this.btnSearchNPLCU.Location = new System.Drawing.Point(385, 16);
            this.btnSearchNPLCU.Name = "btnSearchNPLCU";
            this.btnSearchNPLCU.Size = new System.Drawing.Size(89, 23);
            this.btnSearchNPLCU.TabIndex = 88;
            this.btnSearchNPLCU.Text = "Tìm kiếm";
            this.btnSearchNPLCU.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchNPLCU.Click += new System.EventHandler(this.btnSearchNPLCU_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(22, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 13);
            this.label16.TabIndex = 86;
            this.label16.Text = "Mã NPL";
            // 
            // txtMaNPLCU
            // 
            this.txtMaNPLCU.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaNPLCU.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNPLCU.BackColor = System.Drawing.Color.White;
            this.txtMaNPLCU.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNPLCU.Location = new System.Drawing.Point(119, 18);
            this.txtMaNPLCU.Name = "txtMaNPLCU";
            this.txtMaNPLCU.Size = new System.Drawing.Size(242, 21);
            this.txtMaNPLCU.TabIndex = 85;
            this.txtMaNPLCU.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNPLCU.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaNPLCU.TextChanged += new System.EventHandler(this.btnSearchNPLCU_Click);
            // 
            // tpNPLHuy
            // 
            this.tpNPLHuy.Controls.Add(this.dgNPLHuy);
            this.tpNPLHuy.Controls.Add(this.uiGroupBox11);
            this.tpNPLHuy.Key = "tpNPLHuy";
            this.tpNPLHuy.Location = new System.Drawing.Point(1, 21);
            this.tpNPLHuy.Name = "tpNPLHuy";
            this.tpNPLHuy.Size = new System.Drawing.Size(1202, 603);
            this.tpNPLHuy.TabStop = true;
            this.tpNPLHuy.Text = "NPL Hủy";
            // 
            // dgNPLHuy
            // 
            this.dgNPLHuy.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgNPLHuy.AlternatingColors = true;
            this.dgNPLHuy.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgNPLHuy.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNPLHuy.ColumnAutoResize = true;
            this.dgNPLHuy.ContextMenuStrip = this.cmtNPLHuy;
            dgNPLHuy_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgNPLHuy_DesignTimeLayout_Reference_0.Instance")));
            dgNPLHuy_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgNPLHuy_DesignTimeLayout_Reference_0});
            dgNPLHuy_DesignTimeLayout.LayoutString = resources.GetString("dgNPLHuy_DesignTimeLayout.LayoutString");
            this.dgNPLHuy.DesignTimeLayout = dgNPLHuy_DesignTimeLayout;
            this.dgNPLHuy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgNPLHuy.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgNPLHuy.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLHuy.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNPLHuy.GroupByBoxVisible = false;
            this.dgNPLHuy.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLHuy.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgNPLHuy.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLHuy.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNPLHuy.Hierarchical = true;
            this.dgNPLHuy.ImageList = this.ImageList1;
            this.dgNPLHuy.Location = new System.Drawing.Point(0, 62);
            this.dgNPLHuy.Name = "dgNPLHuy";
            this.dgNPLHuy.RecordNavigator = true;
            this.dgNPLHuy.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgNPLHuy.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNPLHuy.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgNPLHuy.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLHuy.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgNPLHuy.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgNPLHuy.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNPLHuy.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgNPLHuy.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNPLHuy.Size = new System.Drawing.Size(1202, 541);
            this.dgNPLHuy.TabIndex = 279;
            this.dgNPLHuy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgNPLHuy.VisualStyleManager = this.vsmMain;
            // 
            // cmtNPLHuy
            // 
            this.cmtNPLHuy.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripXuatExcel_NPLHuy});
            this.cmtNPLHuy.Name = "ctmTKCT";
            this.cmtNPLHuy.Size = new System.Drawing.Size(129, 26);
            // 
            // toolStripXuatExcel_NPLHuy
            // 
            this.toolStripXuatExcel_NPLHuy.Image = ((System.Drawing.Image)(resources.GetObject("toolStripXuatExcel_NPLHuy.Image")));
            this.toolStripXuatExcel_NPLHuy.Name = "toolStripXuatExcel_NPLHuy";
            this.toolStripXuatExcel_NPLHuy.Size = new System.Drawing.Size(128, 22);
            this.toolStripXuatExcel_NPLHuy.Text = "Xuất Excel";
            this.toolStripXuatExcel_NPLHuy.Click += new System.EventHandler(this.toolStripXuatExcel_NPLHuy_Click);
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox11.Controls.Add(this.uiGroupBox22);
            this.uiGroupBox11.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox11.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(1202, 62);
            this.uiGroupBox11.TabIndex = 278;
            this.uiGroupBox11.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox22
            // 
            this.uiGroupBox22.AutoScroll = true;
            this.uiGroupBox22.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox22.Controls.Add(this.btnSeachNPKH);
            this.uiGroupBox22.Controls.Add(this.label17);
            this.uiGroupBox22.Controls.Add(this.txtNPLHuy);
            this.uiGroupBox22.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox22.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox22.Name = "uiGroupBox22";
            this.uiGroupBox22.Size = new System.Drawing.Size(497, 51);
            this.uiGroupBox22.TabIndex = 288;
            this.uiGroupBox22.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSeachNPKH
            // 
            this.btnSeachNPKH.Image = ((System.Drawing.Image)(resources.GetObject("btnSeachNPKH.Image")));
            this.btnSeachNPKH.Location = new System.Drawing.Point(385, 16);
            this.btnSeachNPKH.Name = "btnSeachNPKH";
            this.btnSeachNPKH.Size = new System.Drawing.Size(89, 23);
            this.btnSeachNPKH.TabIndex = 88;
            this.btnSeachNPKH.Text = "Tìm kiếm";
            this.btnSeachNPKH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSeachNPKH.Click += new System.EventHandler(this.btnSeachNPKH_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(22, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 13);
            this.label17.TabIndex = 86;
            this.label17.Text = "Mã NPL";
            // 
            // txtNPLHuy
            // 
            this.txtNPLHuy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtNPLHuy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtNPLHuy.BackColor = System.Drawing.Color.White;
            this.txtNPLHuy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNPLHuy.Location = new System.Drawing.Point(119, 18);
            this.txtNPLHuy.Name = "txtNPLHuy";
            this.txtNPLHuy.Size = new System.Drawing.Size(242, 21);
            this.txtNPLHuy.TabIndex = 85;
            this.txtNPLHuy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNPLHuy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNPLHuy.TextChanged += new System.EventHandler(this.btnSeachNPKH_Click);
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.dgTKN);
            this.uiTabPage1.Controls.Add(this.uiGroupBox12);
            this.uiTabPage1.Key = "tpTKNK";
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(1202, 603);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Tờ khai NK";
            // 
            // dgTKN
            // 
            this.dgTKN.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKN.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgTKN.AlternatingColors = true;
            this.dgTKN.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKN.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKN.ColumnAutoResize = true;
            this.dgTKN.ContextMenuStrip = this.ctmTKN;
            dgTKN_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgTKN_DesignTimeLayout_Reference_0.Instance")));
            dgTKN_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgTKN_DesignTimeLayout_Reference_0});
            dgTKN_DesignTimeLayout.LayoutString = resources.GetString("dgTKN_DesignTimeLayout.LayoutString");
            this.dgTKN.DesignTimeLayout = dgTKN_DesignTimeLayout;
            this.dgTKN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTKN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKN.GroupByBoxVisible = false;
            this.dgTKN.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKN.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKN.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKN.ImageList = this.ImageList1;
            this.dgTKN.Location = new System.Drawing.Point(0, 62);
            this.dgTKN.Name = "dgTKN";
            this.dgTKN.RecordNavigator = true;
            this.dgTKN.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKN.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKN.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKN.Size = new System.Drawing.Size(1202, 541);
            this.dgTKN.TabIndex = 279;
            this.dgTKN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgTKN.VisualStyleManager = this.vsmMain;
            this.dgTKN.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKN_RowDoubleClick);
            this.dgTKN.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgTKN_DeletingRecords);
            this.dgTKN.RecordUpdated += new System.EventHandler(this.dgTKN_RecordUpdated);
            this.dgTKN.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKN_LoadingRow);
            // 
            // ctmTKN
            // 
            this.ctmTKN.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xemChiTietTKN,
            this.menuExportExcelTKN});
            this.ctmTKN.Name = "ctmTKN";
            this.ctmTKN.Size = new System.Drawing.Size(199, 48);
            // 
            // xemChiTietTKN
            // 
            this.xemChiTietTKN.Image = ((System.Drawing.Image)(resources.GetObject("xemChiTietTKN.Image")));
            this.xemChiTietTKN.Name = "xemChiTietTKN";
            this.xemChiTietTKN.Size = new System.Drawing.Size(198, 22);
            this.xemChiTietTKN.Text = "Xem chi tiết tờ khai này";
            this.xemChiTietTKN.Click += new System.EventHandler(this.xemChiTietTKN_Click);
            // 
            // menuExportExcelTKN
            // 
            this.menuExportExcelTKN.Image = ((System.Drawing.Image)(resources.GetObject("menuExportExcelTKN.Image")));
            this.menuExportExcelTKN.Name = "menuExportExcelTKN";
            this.menuExportExcelTKN.Size = new System.Drawing.Size(198, 22);
            this.menuExportExcelTKN.Text = "Xuất Excel";
            this.menuExportExcelTKN.Click += new System.EventHandler(this.menuExportExcelTKN_Click);
            // 
            // uiGroupBox12
            // 
            this.uiGroupBox12.AutoScroll = true;
            this.uiGroupBox12.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox12.Controls.Add(this.uiGroupBox23);
            this.uiGroupBox12.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox12.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox12.Name = "uiGroupBox12";
            this.uiGroupBox12.Size = new System.Drawing.Size(1202, 62);
            this.uiGroupBox12.TabIndex = 278;
            this.uiGroupBox12.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox23
            // 
            this.uiGroupBox23.AutoScroll = true;
            this.uiGroupBox23.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox23.Controls.Add(this.btnSearchTKNK);
            this.uiGroupBox23.Controls.Add(this.label18);
            this.uiGroupBox23.Controls.Add(this.txtSoTKNK);
            this.uiGroupBox23.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox23.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox23.Name = "uiGroupBox23";
            this.uiGroupBox23.Size = new System.Drawing.Size(497, 51);
            this.uiGroupBox23.TabIndex = 288;
            this.uiGroupBox23.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchTKNK
            // 
            this.btnSearchTKNK.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchTKNK.Image")));
            this.btnSearchTKNK.Location = new System.Drawing.Point(378, 17);
            this.btnSearchTKNK.Name = "btnSearchTKNK";
            this.btnSearchTKNK.Size = new System.Drawing.Size(89, 23);
            this.btnSearchTKNK.TabIndex = 87;
            this.btnSearchTKNK.Text = "Tìm kiếm";
            this.btnSearchTKNK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchTKNK.Click += new System.EventHandler(this.btnSearchTKNK_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(22, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 13);
            this.label18.TabIndex = 86;
            this.label18.Text = "Số tờ khai";
            // 
            // txtSoTKNK
            // 
            this.txtSoTKNK.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoTKNK.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoTKNK.BackColor = System.Drawing.Color.White;
            this.txtSoTKNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTKNK.Location = new System.Drawing.Point(119, 18);
            this.txtSoTKNK.Name = "txtSoTKNK";
            this.txtSoTKNK.Size = new System.Drawing.Size(242, 21);
            this.txtSoTKNK.TabIndex = 85;
            this.txtSoTKNK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTKNK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTKNK.TextChanged += new System.EventHandler(this.btnSearchTKNK_Click);
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.dgTKX);
            this.uiTabPage2.Controls.Add(this.uiGroupBox13);
            this.uiTabPage2.Key = "tpTKX";
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(1202, 603);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Tờ khai XK";
            // 
            // dgTKX
            // 
            this.dgTKX.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKX.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgTKX.AlternatingColors = true;
            this.dgTKX.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKX.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKX.ColumnAutoResize = true;
            this.dgTKX.ContextMenuStrip = this.ctmTKX;
            dgTKX_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgTKX_DesignTimeLayout_Reference_0.Instance")));
            dgTKX_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgTKX_DesignTimeLayout_Reference_0});
            dgTKX_DesignTimeLayout.LayoutString = resources.GetString("dgTKX_DesignTimeLayout.LayoutString");
            this.dgTKX.DesignTimeLayout = dgTKX_DesignTimeLayout;
            this.dgTKX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTKX.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKX.GroupByBoxVisible = false;
            this.dgTKX.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKX.ImageList = this.ImageList1;
            this.dgTKX.Location = new System.Drawing.Point(0, 72);
            this.dgTKX.Name = "dgTKX";
            this.dgTKX.RecordNavigator = true;
            this.dgTKX.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKX.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKX.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKX.Size = new System.Drawing.Size(1202, 531);
            this.dgTKX.TabIndex = 279;
            this.dgTKX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgTKX.VisualStyleManager = this.vsmMain;
            this.dgTKX.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKX_RowDoubleClick);
            this.dgTKX.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgTKX_DeletingRecords);
            this.dgTKX.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKN_LoadingRow);
            // 
            // ctmTKX
            // 
            this.ctmTKX.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xemChiTietTKX,
            this.xemLuongNPLTKX,
            this.xemNPLCungUngItem,
            this.menuExportExcelTKX});
            this.ctmTKX.Name = "ctmTKX";
            this.ctmTKX.Size = new System.Drawing.Size(295, 92);
            // 
            // xemChiTietTKX
            // 
            this.xemChiTietTKX.Image = ((System.Drawing.Image)(resources.GetObject("xemChiTietTKX.Image")));
            this.xemChiTietTKX.Name = "xemChiTietTKX";
            this.xemChiTietTKX.Size = new System.Drawing.Size(294, 22);
            this.xemChiTietTKX.Text = "Xem chi tiết tờ khai này";
            this.xemChiTietTKX.Click += new System.EventHandler(this.xemChiTietTKX_Click);
            // 
            // xemLuongNPLTKX
            // 
            this.xemLuongNPLTKX.Image = ((System.Drawing.Image)(resources.GetObject("xemLuongNPLTKX.Image")));
            this.xemLuongNPLTKX.Name = "xemLuongNPLTKX";
            this.xemLuongNPLTKX.Size = new System.Drawing.Size(294, 22);
            this.xemLuongNPLTKX.Text = "Xem lượng NPL xuât tờ khai này";
            this.xemLuongNPLTKX.Click += new System.EventHandler(this.xemLuongNPLTKX_Click);
            // 
            // xemNPLCungUngItem
            // 
            this.xemNPLCungUngItem.Image = ((System.Drawing.Image)(resources.GetObject("xemNPLCungUngItem.Image")));
            this.xemNPLCungUngItem.Name = "xemNPLCungUngItem";
            this.xemNPLCungUngItem.Size = new System.Drawing.Size(294, 22);
            this.xemNPLCungUngItem.Text = "Xem lượng NPL cung ứng của tờ khai này";
            this.xemNPLCungUngItem.Click += new System.EventHandler(this.xemNPLCungUngItem_Click);
            // 
            // menuExportExcelTKX
            // 
            this.menuExportExcelTKX.Image = ((System.Drawing.Image)(resources.GetObject("menuExportExcelTKX.Image")));
            this.menuExportExcelTKX.Name = "menuExportExcelTKX";
            this.menuExportExcelTKX.Size = new System.Drawing.Size(294, 22);
            this.menuExportExcelTKX.Text = "Xuất Excel";
            this.menuExportExcelTKX.Click += new System.EventHandler(this.menuExportExcelTKX_Click);
            // 
            // uiGroupBox13
            // 
            this.uiGroupBox13.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox13.Controls.Add(this.uiGroupBox24);
            this.uiGroupBox13.Controls.Add(this.btnXemChiTietTK);
            this.uiGroupBox13.Controls.Add(this.btnXemLuongNPLXuatTK);
            this.uiGroupBox13.Controls.Add(this.btnXemLuongNPLCungUngTK);
            this.uiGroupBox13.Controls.Add(this.btnVanDonChoTKxuat);
            this.uiGroupBox13.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox13.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox13.Name = "uiGroupBox13";
            this.uiGroupBox13.Size = new System.Drawing.Size(1202, 72);
            this.uiGroupBox13.TabIndex = 278;
            this.uiGroupBox13.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox24
            // 
            this.uiGroupBox24.AutoScroll = true;
            this.uiGroupBox24.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox24.Controls.Add(this.btnSearchTKXK);
            this.uiGroupBox24.Controls.Add(this.label19);
            this.uiGroupBox24.Controls.Add(this.txtSoTKXK);
            this.uiGroupBox24.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox24.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox24.Name = "uiGroupBox24";
            this.uiGroupBox24.Size = new System.Drawing.Size(497, 61);
            this.uiGroupBox24.TabIndex = 288;
            this.uiGroupBox24.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchTKXK
            // 
            this.btnSearchTKXK.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchTKXK.Image")));
            this.btnSearchTKXK.Location = new System.Drawing.Point(378, 17);
            this.btnSearchTKXK.Name = "btnSearchTKXK";
            this.btnSearchTKXK.Size = new System.Drawing.Size(89, 23);
            this.btnSearchTKXK.TabIndex = 87;
            this.btnSearchTKXK.Text = "Tìm kiếm";
            this.btnSearchTKXK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchTKXK.Click += new System.EventHandler(this.btnSearchTKXK_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(22, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 13);
            this.label19.TabIndex = 86;
            this.label19.Text = "Số tờ khai";
            // 
            // txtSoTKXK
            // 
            this.txtSoTKXK.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoTKXK.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoTKXK.BackColor = System.Drawing.Color.White;
            this.txtSoTKXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTKXK.Location = new System.Drawing.Point(119, 18);
            this.txtSoTKXK.Name = "txtSoTKXK";
            this.txtSoTKXK.Size = new System.Drawing.Size(242, 21);
            this.txtSoTKXK.TabIndex = 85;
            this.txtSoTKXK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTKXK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTKXK.TextChanged += new System.EventHandler(this.btnSearchTKXK_Click);
            // 
            // btnXemChiTietTK
            // 
            this.btnXemChiTietTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemChiTietTK.Image = ((System.Drawing.Image)(resources.GetObject("btnXemChiTietTK.Image")));
            this.btnXemChiTietTK.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXemChiTietTK.Location = new System.Drawing.Point(506, 42);
            this.btnXemChiTietTK.Name = "btnXemChiTietTK";
            this.btnXemChiTietTK.Size = new System.Drawing.Size(113, 23);
            this.btnXemChiTietTK.TabIndex = 270;
            this.btnXemChiTietTK.Text = "Xem chi tiết";
            this.btnXemChiTietTK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemChiTietTK.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemLuongNPLXuatTK
            // 
            this.btnXemLuongNPLXuatTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemLuongNPLXuatTK.Image = ((System.Drawing.Image)(resources.GetObject("btnXemLuongNPLXuatTK.Image")));
            this.btnXemLuongNPLXuatTK.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXemLuongNPLXuatTK.Location = new System.Drawing.Point(506, 13);
            this.btnXemLuongNPLXuatTK.Name = "btnXemLuongNPLXuatTK";
            this.btnXemLuongNPLXuatTK.Size = new System.Drawing.Size(197, 23);
            this.btnXemLuongNPLXuatTK.TabIndex = 272;
            this.btnXemLuongNPLXuatTK.Text = "Xem lượng NPL xuất của tờ khai";
            this.btnXemLuongNPLXuatTK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemLuongNPLXuatTK.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnXemLuongNPLCungUngTK
            // 
            this.btnXemLuongNPLCungUngTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemLuongNPLCungUngTK.Image = ((System.Drawing.Image)(resources.GetObject("btnXemLuongNPLCungUngTK.Image")));
            this.btnXemLuongNPLCungUngTK.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXemLuongNPLCungUngTK.Location = new System.Drawing.Point(709, 13);
            this.btnXemLuongNPLCungUngTK.Name = "btnXemLuongNPLCungUngTK";
            this.btnXemLuongNPLCungUngTK.Size = new System.Drawing.Size(212, 23);
            this.btnXemLuongNPLCungUngTK.TabIndex = 271;
            this.btnXemLuongNPLCungUngTK.Text = "Xem lượng NPL cung ứng của tờ khai";
            this.btnXemLuongNPLCungUngTK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemLuongNPLCungUngTK.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnVanDonChoTKxuat
            // 
            this.btnVanDonChoTKxuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVanDonChoTKxuat.Image = ((System.Drawing.Image)(resources.GetObject("btnVanDonChoTKxuat.Image")));
            this.btnVanDonChoTKxuat.ImageSize = new System.Drawing.Size(20, 20);
            this.btnVanDonChoTKxuat.Location = new System.Drawing.Point(625, 42);
            this.btnVanDonChoTKxuat.Name = "btnVanDonChoTKxuat";
            this.btnVanDonChoTKxuat.Size = new System.Drawing.Size(296, 23);
            this.btnVanDonChoTKxuat.TabIndex = 271;
            this.btnVanDonChoTKxuat.Text = "Nhập vận đơn cho tờ khai xuất (BC09/HSTK)";
            this.btnVanDonChoTKxuat.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnVanDonChoTKxuat.Click += new System.EventHandler(this.btnVanDonChoTKxuat_Click);
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.dgPhuKien);
            this.uiTabPage3.Controls.Add(this.uiGroupBox14);
            this.uiTabPage3.Key = "tpPhuKien";
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(1202, 603);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "Phụ kiện";
            // 
            // dgPhuKien
            // 
            this.dgPhuKien.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgPhuKien.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgPhuKien.AlternatingColors = true;
            this.dgPhuKien.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgPhuKien.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgPhuKien.ColumnAutoResize = true;
            this.dgPhuKien.ContextMenuStrip = this.ctmPK;
            dgPhuKien_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgPhuKien_DesignTimeLayout_Reference_0.Instance")));
            dgPhuKien_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgPhuKien_DesignTimeLayout_Reference_0});
            dgPhuKien_DesignTimeLayout.LayoutString = resources.GetString("dgPhuKien_DesignTimeLayout.LayoutString");
            this.dgPhuKien.DesignTimeLayout = dgPhuKien_DesignTimeLayout;
            this.dgPhuKien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgPhuKien.EditorsControlStyle.ButtonAppearance = Janus.Windows.GridEX.ButtonAppearance.Regular;
            this.dgPhuKien.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgPhuKien.GroupByBoxVisible = false;
            this.dgPhuKien.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgPhuKien.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgPhuKien.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgPhuKien.ImageList = this.ImageList1;
            this.dgPhuKien.Location = new System.Drawing.Point(0, 64);
            this.dgPhuKien.Name = "dgPhuKien";
            this.dgPhuKien.RecordNavigator = true;
            this.dgPhuKien.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgPhuKien.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgPhuKien.Size = new System.Drawing.Size(1202, 539);
            this.dgPhuKien.TabIndex = 279;
            this.dgPhuKien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgPhuKien.VisualStyleManager = this.vsmMain;
            this.dgPhuKien.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgPhuKien_RowDoubleClick_1);
            this.dgPhuKien.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgPhuKien_DeletingRecords);
            this.dgPhuKien.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgPhuKien_LoadingRow_1);
            // 
            // ctmPK
            // 
            this.ctmPK.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xemChiTietPK});
            this.ctmPK.Name = "ctmPK";
            this.ctmPK.Size = new System.Drawing.Size(187, 26);
            // 
            // xemChiTietPK
            // 
            this.xemChiTietPK.Image = ((System.Drawing.Image)(resources.GetObject("xemChiTietPK.Image")));
            this.xemChiTietPK.Name = "xemChiTietPK";
            this.xemChiTietPK.Size = new System.Drawing.Size(186, 22);
            this.xemChiTietPK.Text = "Xem chi tiết phụ kiện";
            this.xemChiTietPK.Click += new System.EventHandler(this.xemChiTietPK_Click);
            // 
            // uiGroupBox14
            // 
            this.uiGroupBox14.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox14.Controls.Add(this.uiGroupBox25);
            this.uiGroupBox14.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox14.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox14.Name = "uiGroupBox14";
            this.uiGroupBox14.Size = new System.Drawing.Size(1202, 64);
            this.uiGroupBox14.TabIndex = 278;
            this.uiGroupBox14.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox25
            // 
            this.uiGroupBox25.AutoScroll = true;
            this.uiGroupBox25.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox25.Controls.Add(this.btnSearchPK);
            this.uiGroupBox25.Controls.Add(this.lbl);
            this.uiGroupBox25.Controls.Add(this.txtSoPK);
            this.uiGroupBox25.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox25.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox25.Name = "uiGroupBox25";
            this.uiGroupBox25.Size = new System.Drawing.Size(497, 53);
            this.uiGroupBox25.TabIndex = 288;
            this.uiGroupBox25.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchPK
            // 
            this.btnSearchPK.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchPK.Image")));
            this.btnSearchPK.Location = new System.Drawing.Point(378, 17);
            this.btnSearchPK.Name = "btnSearchPK";
            this.btnSearchPK.Size = new System.Drawing.Size(89, 23);
            this.btnSearchPK.TabIndex = 87;
            this.btnSearchPK.Text = "Tìm kiếm";
            this.btnSearchPK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchPK.Click += new System.EventHandler(this.btnSearchPK_Click);
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl.Location = new System.Drawing.Point(22, 22);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(62, 13);
            this.lbl.TabIndex = 86;
            this.lbl.Text = "Số phụ kiện";
            // 
            // txtSoPK
            // 
            this.txtSoPK.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoPK.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoPK.BackColor = System.Drawing.Color.White;
            this.txtSoPK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoPK.Location = new System.Drawing.Point(119, 18);
            this.txtSoPK.Name = "txtSoPK";
            this.txtSoPK.Size = new System.Drawing.Size(242, 21);
            this.txtSoPK.TabIndex = 85;
            this.txtSoPK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoPK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoPK.TextChanged += new System.EventHandler(this.btnSearchPK_Click);
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.Controls.Add(this.dgTKCT);
            this.uiTabPage4.Controls.Add(this.uiGroupBox15);
            this.uiTabPage4.Key = "tpTKCTNhap";
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(1202, 603);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "TKCT nhập";
            // 
            // dgTKCT
            // 
            this.dgTKCT.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKCT.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgTKCT.AlternatingColors = true;
            this.dgTKCT.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKCT.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKCT.ColumnAutoResize = true;
            this.dgTKCT.ContextMenuStrip = this.ctmTKCT;
            dgTKCT_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgTKCT_DesignTimeLayout_Reference_0.Instance")));
            dgTKCT_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgTKCT_DesignTimeLayout_Reference_0});
            dgTKCT_DesignTimeLayout.LayoutString = resources.GetString("dgTKCT_DesignTimeLayout.LayoutString");
            this.dgTKCT.DesignTimeLayout = dgTKCT_DesignTimeLayout;
            this.dgTKCT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTKCT.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKCT.GroupByBoxVisible = false;
            this.dgTKCT.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKCT.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKCT.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKCT.ImageList = this.ImageList1;
            this.dgTKCT.Location = new System.Drawing.Point(0, 63);
            this.dgTKCT.Name = "dgTKCT";
            this.dgTKCT.RecordNavigator = true;
            this.dgTKCT.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKCT.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKCT.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKCT.Size = new System.Drawing.Size(1202, 540);
            this.dgTKCT.TabIndex = 280;
            this.dgTKCT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgTKCT.VisualStyleManager = this.vsmMain;
            this.dgTKCT.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKCT_RowDoubleClick);
            this.dgTKCT.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgTKCT_DeletingRecords);
            this.dgTKCT.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKCT_LoadingRow);
            // 
            // ctmTKCT
            // 
            this.ctmTKCT.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xemToolStripMenuItem,
            this.menuExportExcelTKCT});
            this.ctmTKCT.Name = "ctmTKCT";
            this.ctmTKCT.Size = new System.Drawing.Size(264, 48);
            // 
            // xemToolStripMenuItem
            // 
            this.xemToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("xemToolStripMenuItem.Image")));
            this.xemToolStripMenuItem.Name = "xemToolStripMenuItem";
            this.xemToolStripMenuItem.Size = new System.Drawing.Size(263, 22);
            this.xemToolStripMenuItem.Text = "Xem chi tiết tờ khai chuyên tiếp này";
            this.xemToolStripMenuItem.Click += new System.EventHandler(this.xemToolStripMenuItem_Click);
            // 
            // menuExportExcelTKCT
            // 
            this.menuExportExcelTKCT.Image = ((System.Drawing.Image)(resources.GetObject("menuExportExcelTKCT.Image")));
            this.menuExportExcelTKCT.Name = "menuExportExcelTKCT";
            this.menuExportExcelTKCT.Size = new System.Drawing.Size(263, 22);
            this.menuExportExcelTKCT.Text = "Xuất Excel";
            this.menuExportExcelTKCT.Click += new System.EventHandler(this.menuExportExcelTKCT_Click);
            // 
            // uiGroupBox15
            // 
            this.uiGroupBox15.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox15.Controls.Add(this.uiGroupBox26);
            this.uiGroupBox15.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox15.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox15.Name = "uiGroupBox15";
            this.uiGroupBox15.Size = new System.Drawing.Size(1202, 63);
            this.uiGroupBox15.TabIndex = 279;
            this.uiGroupBox15.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox26
            // 
            this.uiGroupBox26.AutoScroll = true;
            this.uiGroupBox26.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox26.Controls.Add(this.btnSearchTKCTN);
            this.uiGroupBox26.Controls.Add(this.label21);
            this.uiGroupBox26.Controls.Add(this.txtSoTKCTN);
            this.uiGroupBox26.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox26.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox26.Name = "uiGroupBox26";
            this.uiGroupBox26.Size = new System.Drawing.Size(497, 52);
            this.uiGroupBox26.TabIndex = 288;
            this.uiGroupBox26.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchTKCTN
            // 
            this.btnSearchTKCTN.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchTKCTN.Image")));
            this.btnSearchTKCTN.Location = new System.Drawing.Point(378, 17);
            this.btnSearchTKCTN.Name = "btnSearchTKCTN";
            this.btnSearchTKCTN.Size = new System.Drawing.Size(89, 23);
            this.btnSearchTKCTN.TabIndex = 87;
            this.btnSearchTKCTN.Text = "Tìm kiếm";
            this.btnSearchTKCTN.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchTKCTN.Click += new System.EventHandler(this.btnSearchTKCTN_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(22, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 13);
            this.label21.TabIndex = 86;
            this.label21.Text = "Số tờ khai";
            // 
            // txtSoTKCTN
            // 
            this.txtSoTKCTN.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoTKCTN.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoTKCTN.BackColor = System.Drawing.Color.White;
            this.txtSoTKCTN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTKCTN.Location = new System.Drawing.Point(119, 18);
            this.txtSoTKCTN.Name = "txtSoTKCTN";
            this.txtSoTKCTN.Size = new System.Drawing.Size(242, 21);
            this.txtSoTKCTN.TabIndex = 85;
            this.txtSoTKCTN.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTKCTN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTKCTN.TextChanged += new System.EventHandler(this.btnSearchTKCTN_Click);
            // 
            // uiTabPage7
            // 
            this.uiTabPage7.Controls.Add(this.dgTKCTXuat);
            this.uiTabPage7.Controls.Add(this.uiGroupBox16);
            this.uiTabPage7.Key = "tpTKCTXuat";
            this.uiTabPage7.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage7.Name = "uiTabPage7";
            this.uiTabPage7.Size = new System.Drawing.Size(1202, 603);
            this.uiTabPage7.TabStop = true;
            this.uiTabPage7.Text = "TKCT Xuất";
            // 
            // dgTKCTXuat
            // 
            this.dgTKCTXuat.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKCTXuat.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgTKCTXuat.AlternatingColors = true;
            this.dgTKCTXuat.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKCTXuat.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKCTXuat.ColumnAutoResize = true;
            this.dgTKCTXuat.ContextMenuStrip = this.ctmTKCTXuat;
            dgTKCTXuat_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgTKCTXuat_DesignTimeLayout_Reference_0.Instance")));
            dgTKCTXuat_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgTKCTXuat_DesignTimeLayout_Reference_0});
            dgTKCTXuat_DesignTimeLayout.LayoutString = resources.GetString("dgTKCTXuat_DesignTimeLayout.LayoutString");
            this.dgTKCTXuat.DesignTimeLayout = dgTKCTXuat_DesignTimeLayout;
            this.dgTKCTXuat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTKCTXuat.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKCTXuat.GroupByBoxVisible = false;
            this.dgTKCTXuat.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKCTXuat.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKCTXuat.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKCTXuat.ImageList = this.ImageList1;
            this.dgTKCTXuat.Location = new System.Drawing.Point(0, 64);
            this.dgTKCTXuat.Name = "dgTKCTXuat";
            this.dgTKCTXuat.RecordNavigator = true;
            this.dgTKCTXuat.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKCTXuat.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKCTXuat.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKCTXuat.Size = new System.Drawing.Size(1202, 539);
            this.dgTKCTXuat.TabIndex = 280;
            this.dgTKCTXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgTKCTXuat.VisualStyleManager = this.vsmMain;
            this.dgTKCTXuat.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgTKCTXuat_RowDoubleClick);
            this.dgTKCTXuat.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgTKCTXuat_DeletingRecords);
            this.dgTKCTXuat.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKCT_LoadingRow);
            // 
            // ctmTKCTXuat
            // 
            this.ctmTKCTXuat.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.menuExportExcelTKCTX});
            this.ctmTKCTXuat.Name = "ctmTKCT";
            this.ctmTKCTXuat.Size = new System.Drawing.Size(264, 48);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem1.Image")));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(263, 22);
            this.toolStripMenuItem1.Text = "Xem chi tiết tờ khai chuyên tiếp này";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // menuExportExcelTKCTX
            // 
            this.menuExportExcelTKCTX.Image = ((System.Drawing.Image)(resources.GetObject("menuExportExcelTKCTX.Image")));
            this.menuExportExcelTKCTX.Name = "menuExportExcelTKCTX";
            this.menuExportExcelTKCTX.Size = new System.Drawing.Size(263, 22);
            this.menuExportExcelTKCTX.Text = "Xuất Excel";
            this.menuExportExcelTKCTX.Click += new System.EventHandler(this.menuExportExcelTKCTX_Click);
            // 
            // uiGroupBox16
            // 
            this.uiGroupBox16.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox16.Controls.Add(this.uiGroupBox27);
            this.uiGroupBox16.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox16.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox16.Name = "uiGroupBox16";
            this.uiGroupBox16.Size = new System.Drawing.Size(1202, 64);
            this.uiGroupBox16.TabIndex = 279;
            this.uiGroupBox16.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox27
            // 
            this.uiGroupBox27.AutoScroll = true;
            this.uiGroupBox27.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox27.Controls.Add(this.btnSearchTKCTX);
            this.uiGroupBox27.Controls.Add(this.label23);
            this.uiGroupBox27.Controls.Add(this.txtSoTKCTX);
            this.uiGroupBox27.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox27.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox27.Name = "uiGroupBox27";
            this.uiGroupBox27.Size = new System.Drawing.Size(497, 53);
            this.uiGroupBox27.TabIndex = 288;
            this.uiGroupBox27.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchTKCTX
            // 
            this.btnSearchTKCTX.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchTKCTX.Image")));
            this.btnSearchTKCTX.Location = new System.Drawing.Point(378, 17);
            this.btnSearchTKCTX.Name = "btnSearchTKCTX";
            this.btnSearchTKCTX.Size = new System.Drawing.Size(89, 23);
            this.btnSearchTKCTX.TabIndex = 87;
            this.btnSearchTKCTX.Text = "Tìm kiếm";
            this.btnSearchTKCTX.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchTKCTX.Click += new System.EventHandler(this.btnSearchTKCTX_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(22, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 13);
            this.label23.TabIndex = 86;
            this.label23.Text = "Số tờ khai";
            // 
            // txtSoTKCTX
            // 
            this.txtSoTKCTX.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoTKCTX.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtSoTKCTX.BackColor = System.Drawing.Color.White;
            this.txtSoTKCTX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTKCTX.Location = new System.Drawing.Point(119, 18);
            this.txtSoTKCTX.Name = "txtSoTKCTX";
            this.txtSoTKCTX.Size = new System.Drawing.Size(242, 21);
            this.txtSoTKCTX.TabIndex = 85;
            this.txtSoTKCTX.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTKCTX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTKCTX.TextChanged += new System.EventHandler(this.btnSearchTKCTX_Click);
            // 
            // tpThanhKhoanTT38
            // 
            this.tpThanhKhoanTT38.Controls.Add(this.dgThanhKhoanTT38);
            this.tpThanhKhoanTT38.Key = "tpThanhKhoanTT38";
            this.tpThanhKhoanTT38.Location = new System.Drawing.Point(1, 21);
            this.tpThanhKhoanTT38.Name = "tpThanhKhoanTT38";
            this.tpThanhKhoanTT38.Size = new System.Drawing.Size(1202, 603);
            this.tpThanhKhoanTT38.TabStop = true;
            this.tpThanhKhoanTT38.Text = "BC TT38";
            // 
            // dgThanhKhoanTT38
            // 
            this.dgThanhKhoanTT38.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThanhKhoanTT38.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgThanhKhoanTT38.AlternatingColors = true;
            this.dgThanhKhoanTT38.AutomaticSort = false;
            this.dgThanhKhoanTT38.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgThanhKhoanTT38.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgThanhKhoanTT38.ColumnAutoResize = true;
            dgThanhKhoanTT38_DesignTimeLayout.LayoutString = resources.GetString("dgThanhKhoanTT38_DesignTimeLayout.LayoutString");
            this.dgThanhKhoanTT38.DesignTimeLayout = dgThanhKhoanTT38_DesignTimeLayout;
            this.dgThanhKhoanTT38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgThanhKhoanTT38.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgThanhKhoanTT38.GroupByBoxVisible = false;
            this.dgThanhKhoanTT38.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThanhKhoanTT38.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThanhKhoanTT38.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgThanhKhoanTT38.ImageList = this.ImageList1;
            this.dgThanhKhoanTT38.Location = new System.Drawing.Point(0, 0);
            this.dgThanhKhoanTT38.Name = "dgThanhKhoanTT38";
            this.dgThanhKhoanTT38.RecordNavigator = true;
            this.dgThanhKhoanTT38.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThanhKhoanTT38.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgThanhKhoanTT38.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThanhKhoanTT38.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgThanhKhoanTT38.Size = new System.Drawing.Size(1202, 603);
            this.dgThanhKhoanTT38.TabIndex = 22;
            this.dgThanhKhoanTT38.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgThanhKhoanTT38.VisualStyleManager = this.vsmMain;
            this.dgThanhKhoanTT38.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgThanhKhoanTT38_RowDoubleClick);
            // 
            // tpThanhKhoanMoi
            // 
            this.tpThanhKhoanMoi.Controls.Add(this.dgThanhKhoanMoi);
            this.tpThanhKhoanMoi.Key = "tpThanhKhoanMoi";
            this.tpThanhKhoanMoi.Location = new System.Drawing.Point(1, 21);
            this.tpThanhKhoanMoi.Name = "tpThanhKhoanMoi";
            this.tpThanhKhoanMoi.Size = new System.Drawing.Size(1202, 603);
            this.tpThanhKhoanMoi.TabStop = true;
            this.tpThanhKhoanMoi.Text = "BC TK mới";
            // 
            // dgThanhKhoanMoi
            // 
            this.dgThanhKhoanMoi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThanhKhoanMoi.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgThanhKhoanMoi.AlternatingColors = true;
            this.dgThanhKhoanMoi.AutomaticSort = false;
            this.dgThanhKhoanMoi.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgThanhKhoanMoi.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgThanhKhoanMoi.ColumnAutoResize = true;
            dgThanhKhoanMoi_DesignTimeLayout.LayoutString = resources.GetString("dgThanhKhoanMoi_DesignTimeLayout.LayoutString");
            this.dgThanhKhoanMoi.DesignTimeLayout = dgThanhKhoanMoi_DesignTimeLayout;
            this.dgThanhKhoanMoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgThanhKhoanMoi.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgThanhKhoanMoi.GroupByBoxVisible = false;
            this.dgThanhKhoanMoi.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThanhKhoanMoi.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThanhKhoanMoi.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgThanhKhoanMoi.ImageList = this.ImageList1;
            this.dgThanhKhoanMoi.Location = new System.Drawing.Point(0, 0);
            this.dgThanhKhoanMoi.Name = "dgThanhKhoanMoi";
            this.dgThanhKhoanMoi.RecordNavigator = true;
            this.dgThanhKhoanMoi.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThanhKhoanMoi.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgThanhKhoanMoi.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThanhKhoanMoi.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgThanhKhoanMoi.Size = new System.Drawing.Size(1202, 603);
            this.dgThanhKhoanMoi.TabIndex = 21;
            this.dgThanhKhoanMoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgThanhKhoanMoi.VisualStyleManager = this.vsmMain;
            this.dgThanhKhoanMoi.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgThanhKhoanMoi_RowDoubleClick);
            // 
            // uiTabPage6
            // 
            this.uiTabPage6.Controls.Add(this.dgThanhKhoan);
            this.uiTabPage6.Key = "tpThanhKhoan";
            this.uiTabPage6.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage6.Name = "uiTabPage6";
            this.uiTabPage6.Size = new System.Drawing.Size(1054, 359);
            this.uiTabPage6.TabStop = true;
            this.uiTabPage6.TabVisible = false;
            this.uiTabPage6.Text = "Báo cáo TK cũ";
            // 
            // dgThanhKhoan
            // 
            this.dgThanhKhoan.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThanhKhoan.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgThanhKhoan.AlternatingColors = true;
            this.dgThanhKhoan.AutomaticSort = false;
            this.dgThanhKhoan.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgThanhKhoan.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgThanhKhoan.ColumnAutoResize = true;
            dgThanhKhoan_DesignTimeLayout.LayoutString = resources.GetString("dgThanhKhoan_DesignTimeLayout.LayoutString");
            this.dgThanhKhoan.DesignTimeLayout = dgThanhKhoan_DesignTimeLayout;
            this.dgThanhKhoan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgThanhKhoan.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgThanhKhoan.GroupByBoxVisible = false;
            this.dgThanhKhoan.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThanhKhoan.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThanhKhoan.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgThanhKhoan.ImageList = this.ImageList1;
            this.dgThanhKhoan.Location = new System.Drawing.Point(0, 0);
            this.dgThanhKhoan.Name = "dgThanhKhoan";
            this.dgThanhKhoan.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThanhKhoan.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgThanhKhoan.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThanhKhoan.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgThanhKhoan.Size = new System.Drawing.Size(1054, 359);
            this.dgThanhKhoan.TabIndex = 20;
            this.dgThanhKhoan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgThanhKhoan.VisualStyleManager = this.vsmMain;
            this.dgThanhKhoan.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick_1);
            // 
            // uiCommandManager1
            // 
            this.uiCommandManager1.AllowCustomize = Janus.Windows.UI.InheritableBoolean.False;
            this.uiCommandManager1.BottomRebar = this.BottomRebar1;
            this.uiCommandManager1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.uiCommandManager1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThanhKhoan,
            this.cmdXuLyHD,
            this.cmdProcessQuyetToan,
            this.cmdLayToKhaiNhapSXXK,
            this.cmdXuatExcelDinhMuc,
            this.LuuThongTin,
            this.cmdBangKeNPLCU,
            this.cmdCanDoiTuCungUng,
            this.cmdBangKeNPLHuy,
            this.cmdResetThanhKhoan});
            this.uiCommandManager1.ContainerControl = this;
            this.uiCommandManager1.Id = new System.Guid("7b61c354-7040-41ec-a33d-b4f402602227");
            this.uiCommandManager1.LeftRebar = this.LeftRebar1;
            this.uiCommandManager1.LockCommandBars = true;
            this.uiCommandManager1.RightRebar = this.RightRebar1;
            this.uiCommandManager1.TopRebar = this.TopRebar1;
            this.uiCommandManager1.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.uiCommandManager1.VisualStyleManager = this.vsmMain;
            this.uiCommandManager1.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.uiCommandManager1_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.uiCommandManager1;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 424);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(786, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.uiCommandManager1;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThanhKhoan1,
            this.cmdXuLyHD1,
            this.cmdProcessQuyetToan1,
            this.cmdResetThanhKhoan1,
            this.LuuThongTin1,
            this.cmdBangKeNPLCU1,
            this.cmdBangKeNPLHuy1,
            this.cmdCanDoiTuCungUng1,
            this.cmdLayToKhaiNhapSXXK1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(1204, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdThanhKhoan1
            // 
            this.cmdThanhKhoan1.Image = ((System.Drawing.Image)(resources.GetObject("cmdThanhKhoan1.Image")));
            this.cmdThanhKhoan1.Key = "cmdThanhKhoan";
            this.cmdThanhKhoan1.Name = "cmdThanhKhoan1";
            this.cmdThanhKhoan1.Shortcut = System.Windows.Forms.Shortcut.CtrlK;
            this.cmdThanhKhoan1.Text = "Thanh &khoản";
            // 
            // cmdXuLyHD1
            // 
            this.cmdXuLyHD1.Image = ((System.Drawing.Image)(resources.GetObject("cmdXuLyHD1.Image")));
            this.cmdXuLyHD1.Key = "cmdXuLyHD";
            this.cmdXuLyHD1.Name = "cmdXuLyHD1";
            this.cmdXuLyHD1.Shortcut = System.Windows.Forms.Shortcut.CtrlX;
            this.cmdXuLyHD1.Text = "&Xứ lý hợp đồng";
            // 
            // cmdProcessQuyetToan1
            // 
            this.cmdProcessQuyetToan1.Image = ((System.Drawing.Image)(resources.GetObject("cmdProcessQuyetToan1.Image")));
            this.cmdProcessQuyetToan1.Key = "cmdProcessQuyetToan";
            this.cmdProcessQuyetToan1.Name = "cmdProcessQuyetToan1";
            // 
            // cmdResetThanhKhoan1
            // 
            this.cmdResetThanhKhoan1.Image = ((System.Drawing.Image)(resources.GetObject("cmdResetThanhKhoan1.Image")));
            this.cmdResetThanhKhoan1.Key = "cmdResetThanhKhoan";
            this.cmdResetThanhKhoan1.Name = "cmdResetThanhKhoan1";
            // 
            // LuuThongTin1
            // 
            this.LuuThongTin1.Image = ((System.Drawing.Image)(resources.GetObject("LuuThongTin1.Image")));
            this.LuuThongTin1.Key = "LuuThongTin";
            this.LuuThongTin1.Name = "LuuThongTin1";
            // 
            // cmdBangKeNPLCU1
            // 
            this.cmdBangKeNPLCU1.Key = "cmdBangKeNPLCU";
            this.cmdBangKeNPLCU1.Name = "cmdBangKeNPLCU1";
            this.cmdBangKeNPLCU1.Text = "BK NPL Cung ứng";
            this.cmdBangKeNPLCU1.Visible = Janus.Windows.UI.InheritableBoolean.True;
            // 
            // cmdBangKeNPLHuy1
            // 
            this.cmdBangKeNPLHuy1.Key = "cmdBangKeNPLHuy";
            this.cmdBangKeNPLHuy1.Name = "cmdBangKeNPLHuy1";
            // 
            // cmdCanDoiTuCungUng1
            // 
            this.cmdCanDoiTuCungUng1.Key = "cmdCanDoiTuCungUng";
            this.cmdCanDoiTuCungUng1.Name = "cmdCanDoiTuCungUng1";
            // 
            // cmdLayToKhaiNhapSXXK1
            // 
            this.cmdLayToKhaiNhapSXXK1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdLayToKhaiNhapSXXK1.Icon")));
            this.cmdLayToKhaiNhapSXXK1.Key = "cmdLayToKhaiNhapSXXK";
            this.cmdLayToKhaiNhapSXXK1.Name = "cmdLayToKhaiNhapSXXK1";
            this.cmdLayToKhaiNhapSXXK1.Text = "Đồng &bộ tờ khai nhập SXXK";
            // 
            // cmdThanhKhoan
            // 
            this.cmdThanhKhoan.Key = "cmdThanhKhoan";
            this.cmdThanhKhoan.Name = "cmdThanhKhoan";
            this.cmdThanhKhoan.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            this.cmdThanhKhoan.Text = "Thanh Khoan";
            // 
            // cmdXuLyHD
            // 
            this.cmdXuLyHD.Key = "cmdXuLyHD";
            this.cmdXuLyHD.Name = "cmdXuLyHD";
            this.cmdXuLyHD.Text = "Xử lý hợp đồng";
            // 
            // cmdProcessQuyetToan
            // 
            this.cmdProcessQuyetToan.Key = "cmdProcessQuyetToan";
            this.cmdProcessQuyetToan.Name = "cmdProcessQuyetToan";
            this.cmdProcessQuyetToan.Text = "Xử lý quyết toán hợp đồng ";
            // 
            // cmdLayToKhaiNhapSXXK
            // 
            this.cmdLayToKhaiNhapSXXK.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdLayToKhaiNhapSXXK.Icon")));
            this.cmdLayToKhaiNhapSXXK.Key = "cmdLayToKhaiNhapSXXK";
            this.cmdLayToKhaiNhapSXXK.Name = "cmdLayToKhaiNhapSXXK";
            this.cmdLayToKhaiNhapSXXK.Text = "Đồng bộ tờ khai nhập SXXK";
            // 
            // cmdXuatExcelDinhMuc
            // 
            this.cmdXuatExcelDinhMuc.Key = "cmdXuatExcelDinhMuc";
            this.cmdXuatExcelDinhMuc.Name = "cmdXuatExcelDinhMuc";
            this.cmdXuatExcelDinhMuc.Text = "Xuất Excel định mức sản phẩm";
            // 
            // LuuThongTin
            // 
            this.LuuThongTin.Key = "LuuThongTin";
            this.LuuThongTin.Name = "LuuThongTin";
            this.LuuThongTin.Text = "Lưu thông tin hợp đồng";
            // 
            // cmdBangKeNPLCU
            // 
            this.cmdBangKeNPLCU.Image = ((System.Drawing.Image)(resources.GetObject("cmdBangKeNPLCU.Image")));
            this.cmdBangKeNPLCU.Key = "cmdBangKeNPLCU";
            this.cmdBangKeNPLCU.Name = "cmdBangKeNPLCU";
            this.cmdBangKeNPLCU.Text = "Bảng kê NPL Cung ứng";
            this.cmdBangKeNPLCU.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdCanDoiTuCungUng
            // 
            this.cmdCanDoiTuCungUng.Image = ((System.Drawing.Image)(resources.GetObject("cmdCanDoiTuCungUng.Image")));
            this.cmdCanDoiTuCungUng.Key = "cmdCanDoiTuCungUng";
            this.cmdCanDoiTuCungUng.Name = "cmdCanDoiTuCungUng";
            this.cmdCanDoiTuCungUng.Text = "Cân đối NPL tự cung ứng";
            // 
            // cmdBangKeNPLHuy
            // 
            this.cmdBangKeNPLHuy.Image = ((System.Drawing.Image)(resources.GetObject("cmdBangKeNPLHuy.Image")));
            this.cmdBangKeNPLHuy.Key = "cmdBangKeNPLHuy";
            this.cmdBangKeNPLHuy.Name = "cmdBangKeNPLHuy";
            this.cmdBangKeNPLHuy.Text = "BK NPL Hủy";
            // 
            // cmdResetThanhKhoan
            // 
            this.cmdResetThanhKhoan.Key = "cmdResetThanhKhoan";
            this.cmdResetThanhKhoan.Name = "cmdResetThanhKhoan";
            this.cmdResetThanhKhoan.Text = "Quyết toán lại";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.uiCommandManager1;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 28);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 396);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.uiCommandManager1;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(786, 28);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 396);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.uiCommandManager1;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1204, 32);
            // 
            // gridEXPrintNPL
            // 
            this.gridEXPrintNPL.CardColumnsPerPage = 1;
            this.gridEXPrintNPL.DocumentName = "DanhSachNPL";
            this.gridEXPrintNPL.FitColumns = Janus.Windows.GridEX.FitColumnsMode.SizingColumns;
            this.gridEXPrintNPL.FooterDistance = 50;
            this.gridEXPrintNPL.HeaderDistance = 50;
            this.gridEXPrintNPL.PageHeaderCenter = "DANH SÁCH NGUYÊN PHỤ LIỆU ĐĂNG KÝ CỦA HỢP ĐỒNG";
            this.gridEXPrintNPL.PageHeaderFormatStyle.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold);
            this.gridEXPrintNPL.PageHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEXPrintNPL.PageHeaderFormatStyle.LineAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEXPrintNPL.PageHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEXPrintNPL.RepeatHeaders = false;
            // 
            // gridEXPrintSP
            // 
            this.gridEXPrintSP.CardColumnsPerPage = 1;
            this.gridEXPrintSP.DocumentName = "DanhSachSP";
            this.gridEXPrintSP.FitColumns = Janus.Windows.GridEX.FitColumnsMode.SizingColumns;
            this.gridEXPrintSP.FooterDistance = 50;
            this.gridEXPrintSP.HeaderDistance = 50;
            this.gridEXPrintSP.PageHeaderCenter = "DANH SÁCH SẢN PHẨM ĐĂNG KÝ";
            this.gridEXPrintSP.PageHeaderFormatStyle.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold);
            this.gridEXPrintSP.PageHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEXPrintSP.PageHeaderFormatStyle.LineAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEXPrintSP.PageHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEXPrintSP.RepeatHeaders = false;
            // 
            // gridEXExporterSP
            // 
            this.gridEXExporterSP.SheetName = "SP";
            // 
            // gridEXExporterNPL
            // 
            this.gridEXExporterNPL.SheetName = "NPL";
            // 
            // HopDongRegistedDetailForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(1204, 718);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "HopDongRegistedDetailForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Chi tiết hợp đồng GC";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.HopDong_Form_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabHopDong)).EndInit();
            this.tabHopDong.ResumeLayout(false);
            this.uiTabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgThongTinHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.tpNguyenPhuLieu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNguyenPhuLieu)).EndInit();
            this.ctmNPL.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).EndInit();
            this.uiGroupBox18.ResumeLayout(false);
            this.uiGroupBox18.PerformLayout();
            this.tpSanPham.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgSanPham)).EndInit();
            this.ctmSP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox17)).EndInit();
            this.uiGroupBox17.ResumeLayout(false);
            this.uiGroupBox17.PerformLayout();
            this.tpThietBi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgThietBi)).EndInit();
            this.ctmTB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox19)).EndInit();
            this.uiGroupBox19.ResumeLayout(false);
            this.uiGroupBox19.PerformLayout();
            this.tpHangMau.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgHangMau)).EndInit();
            this.cmtHM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox9)).EndInit();
            this.uiGroupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox20)).EndInit();
            this.uiGroupBox20.ResumeLayout(false);
            this.uiGroupBox20.PerformLayout();
            this.tpNPLCU.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLCungUng)).EndInit();
            this.cmtNPL_CU.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox10)).EndInit();
            this.uiGroupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox21)).EndInit();
            this.uiGroupBox21.ResumeLayout(false);
            this.uiGroupBox21.PerformLayout();
            this.tpNPLHuy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNPLHuy)).EndInit();
            this.cmtNPLHuy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox22)).EndInit();
            this.uiGroupBox22.ResumeLayout(false);
            this.uiGroupBox22.PerformLayout();
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKN)).EndInit();
            this.ctmTKN.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox12)).EndInit();
            this.uiGroupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox23)).EndInit();
            this.uiGroupBox23.ResumeLayout(false);
            this.uiGroupBox23.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).EndInit();
            this.ctmTKX.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).EndInit();
            this.uiGroupBox13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox24)).EndInit();
            this.uiGroupBox24.ResumeLayout(false);
            this.uiGroupBox24.PerformLayout();
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPhuKien)).EndInit();
            this.ctmPK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox14)).EndInit();
            this.uiGroupBox14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox25)).EndInit();
            this.uiGroupBox25.ResumeLayout(false);
            this.uiGroupBox25.PerformLayout();
            this.uiTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKCT)).EndInit();
            this.ctmTKCT.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox15)).EndInit();
            this.uiGroupBox15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox26)).EndInit();
            this.uiGroupBox26.ResumeLayout(false);
            this.uiGroupBox26.PerformLayout();
            this.uiTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKCTXuat)).EndInit();
            this.ctmTKCTXuat.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).EndInit();
            this.uiGroupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox27)).EndInit();
            this.uiGroupBox27.ResumeLayout(false);
            this.uiGroupBox27.PerformLayout();
            this.tpThanhKhoanTT38.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgThanhKhoanTT38)).EndInit();
            this.tpThanhKhoanMoi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgThanhKhoanMoi)).EndInit();
            this.uiTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgThanhKhoan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.Tab.UITab tabHopDong;
        private Janus.Windows.UI.Tab.UITabPage tpNguyenPhuLieu;
        private Janus.Windows.UI.Tab.UITabPage tpSanPham;
        private Janus.Windows.UI.Tab.UITabPage tpThietBi;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private ContextMenuStrip ctmNPL;
        private ContextMenuStrip ctmSP;
        private ContextMenuStrip ctmTB;
        private ContextMenuStrip ctmTKN;
        private ContextMenuStrip ctmTKX;
        private ContextMenuStrip ctmPK;
        private ContextMenuStrip ctmTKCT;
        private ToolStripMenuItem xemTKN;
        private ToolStripMenuItem xemTKXSP;
        private ToolStripMenuItem xemPKSP;
        private ToolStripMenuItem xemTKNTB;
        private ToolStripMenuItem xemPKTB;
        private ToolStripMenuItem xemChiTietTKN;
        private ToolStripMenuItem xemChiTietTKX;
        private ToolStripMenuItem xemLuongNPLTKX;
        private ToolStripMenuItem xemChiTietPK;
        private ToolStripMenuItem xemToolStripMenuItem;
        private ToolStripMenuItem xemTKTXNPL;
        private ToolStripMenuItem xemPKChuaNPL;
        private ToolStripMenuItem xemTKXTB;
        private ToolStripMenuItem xemDMItem;
        private ToolStripMenuItem xemNPLCungUngItem;
        private Janus.Windows.UI.Tab.UITabPage tpNPLCU;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage6;
        private Janus.Windows.GridEX.GridEX dgThanhKhoan;
        private Janus.Windows.UI.CommandBars.UICommandManager uiCommandManager1;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThanhKhoan1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThanhKhoan;
        private UIButton btnNXoa;
        private UIButton btnNThem;
        private UIButton btnXoa;
        private UIButton btnXemTKTXNPL;
        private UIButton btnXemPKNPL;
        private UIButton btnXemTKNNPL;
        private UIButton btnXemPKSP;
        private UIButton btnXemTKXSP;
        private UIButton btnXemTKXTB;
        private UIButton btnXemPKTB;
        private UIButton btnXemTKNTB;
        private UIButton btnXemLuongNPLXuatTK;
        private UIButton btnXemLuongNPLCungUngTK;
        private UIButton btnXemChiTietTK;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuLyHD1;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuLyHD;
        private UIButton btnXemTKCTNPL;
        private UIButton btnXemTKCTSP;
        private ToolStripMenuItem xemTKCTSPItem;
        private ToolStripMenuItem inDMSPItem;
        private ToolStripMenuItem xemTKCTNPLItem;
        private UIButton btnInDMSP;
        private UIButton btnInNPL;
        private UIButton btnInSP;
        private Janus.Windows.GridEX.GridEXPrintDocument gridEXPrintNPL;
        private Janus.Windows.GridEX.GridEXPrintDocument gridEXPrintSP;
        private UIButton uiButton1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage7;
        private ContextMenuStrip ctmTKCTXuat;
        private ToolStripMenuItem toolStripMenuItem1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLayToKhaiNhapSXXK1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLayToKhaiNhapSXXK;
        private Janus.Windows.UI.Tab.UITabPage tpThanhKhoanMoi;
        private Janus.Windows.GridEX.GridEX dgThanhKhoanMoi;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuatExcelDinhMuc;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage8;
        private Label lblTrangThaiThanhKhoan;
        private Label label8;
        private Company.Interface.Controls.NguyenTeControl nguyenTeControl1;
        private Company.Interface.Controls.NuocHControl nuocThue;
        private Label label5;
        private Label label3;
        private Label label6;
        private CalendarCombo ccNgayGiaHan;
        private CalendarCombo ccNgayKyHD;
        private EditBox txtSoHopDong;
        private Label label2;
        private Label label4;
        private Label label1;
        private CalendarCombo ccNgayKetThucHD;
        private UIGroupBox uiGroupBox3;
        private UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.GridEX dgThongTinHopDong;
        private ToolStripMenuItem xemTKXuatSPToolStripMenuItem;
        private Label lblGhiChu;
        private UIButton btnTinhLuongTon;
        private UIButton btnXuatExcelSanPham;
        private Janus.Windows.GridEX.Export.GridEXExporter gridEXExporterSP;
        private Janus.Windows.GridEX.Export.GridEXExporter gridEXExporterNPL;
        private UIButton btnXuatExcelNPL;
        private EditBox txtTenDoiTac;
        private Label label7;
        private EditBox txtDiaChi;
        private Label label9;
        private Janus.Windows.UI.CommandBars.UICommand LuuThongTin1;
        private Janus.Windows.UI.CommandBars.UICommand LuuThongTin;
        private UIButton btnVanDonChoTKxuat;
        private Janus.Windows.UI.CommandBars.UICommand cmdBangKeNPLCU1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBangKeNPLCU;
        private Janus.Windows.UI.CommandBars.UICommand cmdCanDoiTuCungUng1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCanDoiTuCungUng;
        private ToolStripMenuItem xemTKChuyenTiepTB;
        private UIButton btnXuatNhapTon;
        private UIButton btnXuatNhapTon_Thietbi;
        private Janus.Windows.UI.Tab.UITabPage tpThanhKhoanTT38;
        private Janus.Windows.GridEX.GridEX dgThanhKhoanTT38;
        private Janus.Windows.UI.Tab.UITabPage tpHangMau;
        private UIButton uiButton2;
        private UIButton uiButton3;
        private UIButton uiButton4;
        private ContextMenuStrip cmtHM;
        private ToolStripMenuItem toolStripMenuItem2;
        private ToolStripMenuItem toolStripMenuItem3;
        private ToolStripMenuItem toolStripMenuItem4;
        private ToolStripMenuItem toolStripMenuItem5;
        private Janus.Windows.UI.Tab.UITabPage tpNPLHuy;
        private Janus.Windows.UI.CommandBars.UICommand cmdBangKeNPLHuy1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBangKeNPLHuy;
        private ContextMenuStrip cmtNPL_CU;
        private ToolStripMenuItem toolStripXuatExcel_NPLCU;
        private ContextMenuStrip cmtNPLHuy;
        private ToolStripMenuItem toolStripXuatExcel_NPLHuy;
        private Janus.Windows.UI.CommandBars.UICommand cmdResetThanhKhoan1;
        private Janus.Windows.UI.CommandBars.UICommand cmdResetThanhKhoan;
        private UIButton btnDangKyNPLCungUng;
        private Label label11;
        private Label label10;
        private CalendarCombo dtpDateTo;
        private CalendarCombo dtpDateFrom;
        private Label label12;
        private ToolStripMenuItem menuExportExcelTKN;
        private ToolStripMenuItem menuExportExcelTKX;
        private ToolStripMenuItem menuExportExcelTKCT;
        private ToolStripMenuItem menuExportExcelTKCTX;
        private Janus.Windows.UI.CommandBars.UICommand cmdProcessQuyetToan;
        private Janus.Windows.UI.CommandBars.UICommand cmdProcessQuyetToan1;
        private UIButton btnExportTotal;
        private UIGroupBox uiGroupBox2;
        private UIGroupBox uiGroupBox5;
        private UIGroupBox uiGroupBox6;
        private Janus.Windows.GridEX.GridEX dgSanPham;
        private UIButton btnXemDMSP;
        private UIGroupBox uiGroupBox7;
        private Janus.Windows.GridEX.GridEX dgNguyenPhuLieu;
        private UIGroupBox uiGroupBox8;
        private Janus.Windows.GridEX.GridEX dgThietBi;
        private UIGroupBox uiGroupBox9;
        private UIGroupBox uiGroupBox11;
        private UIGroupBox uiGroupBox12;
        private UIGroupBox uiGroupBox13;
        private UIGroupBox uiGroupBox14;
        private Janus.Windows.GridEX.GridEX dgHangMau;
        private Janus.Windows.GridEX.GridEX dgNPLCungUng;
        private Janus.Windows.GridEX.GridEX dgNPLHuy;
        private Janus.Windows.GridEX.GridEX dgTKN;
        private Janus.Windows.GridEX.GridEX dgTKX;
        private UIGroupBox uiGroupBox15;
        private UIGroupBox uiGroupBox16;
        private Janus.Windows.GridEX.GridEX dgPhuKien;
        private Janus.Windows.GridEX.GridEX dgTKCT;
        private Janus.Windows.GridEX.GridEX dgTKCTXuat;
        private UIGroupBox uiGroupBox18;
        private UIButton btnSearchNPL;
        private Label label22;
        private EditBox txtMaNPL;
        private UIGroupBox uiGroupBox17;
        private UIButton btnSearchSP;
        private Label label13;
        private EditBox txtMaSP;
        private UIGroupBox uiGroupBox19;
        private UIButton btnSearchTB;
        private Label label14;
        private EditBox txtMaTB;
        private UIGroupBox uiGroupBox20;
        private UIButton btnSearchHM;
        private Label label15;
        private EditBox txtMaHM;
        private UIGroupBox uiGroupBox10;
        private UIGroupBox uiGroupBox21;
        private Label label16;
        private EditBox txtMaNPLCU;
        private UIGroupBox uiGroupBox22;
        private Label label17;
        private EditBox txtNPLHuy;
        private UIGroupBox uiGroupBox23;
        private UIButton btnSearchTKNK;
        private Label label18;
        private EditBox txtSoTKNK;
        private UIGroupBox uiGroupBox24;
        private UIButton btnSearchTKXK;
        private Label label19;
        private EditBox txtSoTKXK;
        private UIGroupBox uiGroupBox25;
        private UIButton btnSearchPK;
        private Label lbl;
        private EditBox txtSoPK;
        private UIGroupBox uiGroupBox26;
        private UIButton btnSearchTKCTN;
        private Label label21;
        private EditBox txtSoTKCTN;
        private UIGroupBox uiGroupBox27;
        private UIButton btnSearchTKCTX;
        private Label label23;
        private EditBox txtSoTKCTX;
        private UIButton btnSearchNPLCU;
        private UIButton btnSeachNPKH;
        
        
    }
}
