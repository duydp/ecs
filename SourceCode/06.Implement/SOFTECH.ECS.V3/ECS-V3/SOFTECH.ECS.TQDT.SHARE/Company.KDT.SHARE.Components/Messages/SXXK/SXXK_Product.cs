﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Bảng kê nguyên phụ liệu thanh khoản
    /// </summary>
    public class SXXK_Product
    {
        #region Cách theo hàng LanNT
        ///// <summary>
        ///// Thông tin hàng
        ///// </summary>
        //[XmlElement("Commodity")]
        //public Commodity Commodity { get; set; }
        ///// <summary>
        ///// Số lượng và đơn vị tính
        ///// </summary>
        //[XmlElement("GoodsMeasure")]
        //public SXXK_GoodsMeasure GoodsMeasure { get; set; }
        #endregion
        #region Cách hàng trong- số lượng
        /// <summary>
        /// Thông tin hàng
        /// </summary>
        [XmlElement("Commodity")]
        public List<Commodity> Commoditys { get; set; }
        /// <summary>
        /// Số lượng và đơn vị tính
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public List<SXXK_GoodsMeasure> GoodsMeasures { get; set; }
        #endregion

        [XmlElement("DetailMaterialExport")]
        public List<SXXK_GoodsMeasure> DetailMaterialExports { get; set; }
        /// <summary>
        /// Mã sản phẩm khai báo sửa (NPL)
        /// </summary>
        [XmlElement("preIdentification")]
        public string PreIdentification { get; set; }

    }
}
