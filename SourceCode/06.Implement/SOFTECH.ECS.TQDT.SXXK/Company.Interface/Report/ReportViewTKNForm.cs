﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace Company.Interface.Report
{
    
    public partial class ReportViewTKNForm : BaseForm
    {
        public ToKhaiNhap ToKhaiChinhReport = new ToKhaiNhap();
        public PhuLucToKhaiNhap PhuLucReport = new PhuLucToKhaiNhap();
        private System.Drawing.Printing.Margins MarginTKN = new System.Drawing.Printing.Margins();
        private System.Drawing.Printing.Margins MarginPhuLuc = new System.Drawing.Printing.Margins();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public XRControl Cell = new XRControl();
        public ReportViewTKNForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            this.MarginTKN = GlobalSettings.MarginTKN;
            this.MarginPhuLuc = GlobalSettings.MarginPhuLuc;
            cboToKhai.SelectedIndex = 0;
            if (this.TKMD.HMDCollection.Count > 3)
            {
                int count = (this.TKMD.HMDCollection.Count - 1) / 9 + 1;
                for (int i = 0; i < count; i++)
                    this.AddItemComboBox();
            }
            this.ToKhaiChinhReport.Margins = this.MarginTKN;
            this.ToKhaiChinhReport.TKMD = this.TKMD;
            
            this.ToKhaiChinhReport.BindReport(chkInMaHang.Checked);
            printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
            this.ToKhaiChinhReport.CreateDocument();
            
        }
        public void AddItemComboBox()
        {
            cboToKhai.Items.Add("Phụ lục " + cboToKhai.Items.Count, cboToKhai.Items.Count);
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.Margins = this.MarginTKN;
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.report = this;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                HangMauDichCollection HMDReportCollection = new HangMauDichCollection();
                int begin = (cboToKhai.SelectedIndex - 1) * 9;
                int end = cboToKhai.SelectedIndex * 9;
                if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                for (int i = begin; i < end; i++)
                    HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                this.PhuLucReport = new PhuLucToKhaiNhap();
                this.PhuLucReport.report = this;
                this.PhuLucReport.MaLoaiHinh = this.TKMD.MaLoaiHinh;
                this.PhuLucReport.SoToKhai = this.TKMD.SoToKhai;
                if(this.TKMD.NgayDangKy!= new DateTime(1900,1,1))
                    this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
                this.PhuLucReport.Margins = this.MarginPhuLuc;
                this.PhuLucReport.HMDCollection = HMDReportCollection;
                this.PhuLucReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.MarginTKN  = printControl1.PrintingSystem.PageMargins;
                this.ToKhaiChinhReport.setVisibleImage(chkHinhNen.Checked);
                this.ToKhaiChinhReport.Margins = this.MarginTKN;
                GlobalSettings.MarginTKN = this.MarginTKN;
                GlobalSettings.RefreshKey();
                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.PrintingSystem.PrintDlg();
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.ToKhaiChinhReport.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.MarginPhuLuc = printControl1.PrintingSystem.PageMargins;
                this.PhuLucReport.setVisibleImage(chkHinhNen.Checked);
                this.PhuLucReport.Margins = this.MarginPhuLuc;
                GlobalSettings.MarginPhuLuc = this.MarginPhuLuc;
                GlobalSettings.RefreshKey();
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.PrintingSystem.PrintDlg();
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.PhuLucReport.setVisibleImage(true);
                this.PhuLucReport.CreateDocument();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //switch (cboExport.SelectedValue.ToString())
            //{
            //    case "pdf":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportPdf, new object[] { true });
            //        break;
            //    case "excel":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            //        break;

            //}
        }

        private void btnQuickPrint_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.ptbImage.Visible = false;
                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.PrintDirect, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.ToKhaiChinhReport.ptbImage.Visible = true;
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                //this.PhuLucReport.xrPictureBox1.Visible = false;
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.PrintDirect, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                } 
                //this.PhuLucReport.xrPictureBox1.Visible = true;
                this.PhuLucReport.CreateDocument();
            }
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.setNhomHang(this.Cell, txtTenNhomHang.Text);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.PhuLucReport.setNhomHang(this.Cell, txtTenNhomHang.Text);
                this.PhuLucReport.CreateDocument();
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.setVisibleImage(chkHinhNen.Checked);
                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                this.ToKhaiChinhReport.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.PhuLucReport.setVisibleImage(chkHinhNen.Checked);
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                this.PhuLucReport.setVisibleImage(true);
                this.PhuLucReport.CreateDocument();
            }
        }

        private void chkInMaHang_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                this.PhuLucReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }

        private void chkHinhNen_CheckedChanged(object sender, EventArgs e)
        {

        }

 

        private void chkInBanLuuHaiQuan_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.ToKhaiChinhReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                //this.PhuLucReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.PhuLucReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }
    }
}