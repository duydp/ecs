﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class HopDongThuongMaiDetail
    {

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_HopDongTM_ID(long hopDongTM_ID, SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_HopDongThuongMaiDetail_DeleteBy_HopDongTM_ID]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDongTM_ID", SqlDbType.BigInt, hopDongTM_ID);

            return db.ExecuteNonQuery(dbCommand, transaction);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "p_KDT_HopDongThuongMaiDetail_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
            db.AddInParameter(dbCommand, "@HopDongTM_ID", SqlDbType.BigInt, HopDongTM_ID);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Float, DonGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Float, TriGiaKB);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

    }
}
