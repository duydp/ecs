using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Company.GC.BLL.Utils;
using Company.Interface.Report;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.GC
{
    public partial class HopDongManageForm : BaseForm
    {
        public HopDong HopDongSelected = new HopDong();
        private List<HopDong> collection = new List<HopDong>();
        public bool IsBrowseForm = false;
        public bool IsDaDuyet = false;
        public bool isKhaiBoSung = false;
        private string xmlCurrent = "";
        string msgInfor = string.Empty;
        FeedBackContent feedbackContent = null;
        public HopDongManageForm()
        {
            InitializeComponent();
            //dgList.RootTable.Columns[7].Visible = false;
            cbStatus.SelectedIndex = 0;
        }

        public void BindData()
        {
            string where = string.Format("MaDoanhNghiep = '{0}' and TrangThaiXuLy={2}", GlobalSettings.MA_DON_VI, cbStatus.SelectedValue.ToString());
            Company.GC.BLL.KDT.GC.HopDong hd = new HopDong();
            collection = Company.GC.BLL.KDT.GC.HopDong.SelectCollectionDynamic(where, "");
            dgList.DataSource = collection;
        }

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbStatus.SelectedIndex = 0;
            // Đơn vị Hải quan.
            //if (this.HopDongSelected.MaHaiQuan.Trim().Length == 0)
            //    GlobalSett1ings.MA_HAI_QUAN = GlobalSetting1s.MA_HAI_QUAN;
            //else
            //    GlobalS1ettings.MA_HAI_QUAN = this.HopDongSelected.MaHaiQuan;            

        }

        //-----------------------------------------------------------------------------------------

        private void HopDongManageForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();

            if (IsBrowseForm)
            {
                label5.Visible = this.IsBrowseForm;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmMain.CommandBars.Clear();
                if (IsDaDuyet)
                {
                    cbStatus.SelectedValue = "1";
                    cbStatus.ReadOnly = true;
                }
            }
            else
            {
                setCommandStatus();
            }

            //An nut Xac nhan
            XacNhan.Visible = Janus.Windows.UI.InheritableBoolean.False;

            btnSearch_Click(null, null);
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (!IsBrowseForm)
                {
                    long ID = (long)Convert.ToInt32(e.Row.Cells["ID"].Text);

                    HopDongEditForm hopdong = new HopDongEditForm();
                    hopdong.HD = HopDong.Load(ID);
                    if (hopdong.HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || hopdong.HD.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || hopdong.HD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        hopdong.OpenType = OpenFormType.Edit;
                    else
                        hopdong.OpenType = OpenFormType.View;
                    hopdong.ShowDialog();
                    btnSearch.PerformClick();
                }
                else
                {
                    HopDongSelected = (HopDong)e.Row.DataRow;
                    this.Close();
                }
            }

        }

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void LaySoTiepNhanDT()
        {
            if (dgList.GetRow() == null)
            {
                //MLMessages("Chưa chọn hợp đồng.","MSG_WRN11","", false);
                showMsg("MSG_2702052");
                return;
            }
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            HopDongSelected = (HopDong)dgList.GetRow().DataRow;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "HD";
                sendXML.master_id = HopDongSelected.ID;
                if (!sendXML.Load())
                {
                    showMsg("MSG_STN01");
                    //MLMessages("Hợp đồng không có phản hồi do chưa gửi thông tin tới hải quan.","MSG_STN01","", false);
                    return;
                }
            }

            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                {
                    xmlCurrent = HopDongSelected.LayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không","MSG_STN02","", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(password);
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", HopDongSelected.SoTiepNhan);
                    //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + HopDongSelected.SoTiepNhan,"MSG_SEN02",HopDongSelected.SoTiepNhan.ToString(), false);                    
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702042");
                    //MLMessages("Đã hủy hợp đồng này","MSG_CAN01","", false);                    
                }
                else if (sendXML.func == 2)
                {
                    if (HopDongSelected.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        //showMsg("MSG_2702018");
                        string mess = "";

                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_2702018", mess);
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.","MSG_SEN03","", false);                        
                    }
                    else if (HopDongSelected.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN04", mess);
                        //MLMessages("Hải quan chưa xử lý danh sách sản phẩm này!","MSG_SEN04","", false);
                    }
                    else if (HopDongSelected.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        showMsg("MSG_SEN05");
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu","MSG_SEN05","", false);

                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                this.btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0],"MSG_SEN09",msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message.Trim());
                            //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin hợp đồng. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoi(string pass)
        {
            HopDongSelected = (HopDong)dgList.GetRow().DataRow;
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            try
            {

                sendXML.LoaiHS = "HD";
                sendXML.master_id = HopDongSelected.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;

                //HUNGTQ BỔ SUNG XML LAYPHANHOIDADUYET 10/11/2010
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi((int)MessgaseType.ThongTin, (int)MessageFunctions.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, HopDongSelected.GUIDSTR));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProgram();
                docNPL.Load(path + @"\B03GiaCong\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI;
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI;

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN;
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = HopDongSelected.GUIDSTR.Trim().ToUpper();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                //xmlCurrent = HopDongSelected.LayPhanHoi(pass, sendXML.msg);
                xmlCurrent = HopDongSelected.TQDTLayPhanHoi(pass, xml.InnerXml);

                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không","MSG_STN02","", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(pass);
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", HopDongSelected.SoTiepNhan);
                    //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + HopDongSelected.SoTiepNhan,"MSG_SEN02",HopDongSelected.SoTiepNhan.ToString(), false);                    
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702042");
                    //MLMessages("Đã hủy hợp đồng này","MSG_CAN01","", false);
                }
                else if (sendXML.func == 2)
                {
                    if (HopDongSelected.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        //showMsg("MSG_2702018");
                        string mess = "";

                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        //showMsg("MSG_2702018", mess);
                        try
                        {
                            //if (HopDongSelected.PhanLuong == "1")
                            //    ShowMessageTQDT("Hợp đồng đã được phân luồng XANH \r\n\r\n HƯỚNG DẪN CỦA HẢI QUAN : " + HopDongSelected.HUONGDAN.ToString(), false);
                            //else if (HopDongSelected.PhanLuong == "2")
                            //    ShowMessageTQDT("Hợp đồng đã được phân luồng VÀNG \r\n\r\n HƯỚNG DẪN CỦA HẢI QUAN : " + HopDongSelected.HUONGDAN.ToString(), false);
                            //else
                            //    ShowMessageTQDT("Hợp đồng đã được phân luồng ĐỎ \r\n\r\n HƯỚNG DẪN CỦA HẢI QUAN : " + HopDongSelected.HUONGDAN.ToString(), false);
                            ShowMessage("Hợp đồng đã được duyệt chính thức", false);
                        }
                        catch (Exception ex1k) { ShowMessageTQDT("Lỗi :" + ex1k.Message, false); }
                    }
                    else if (HopDongSelected.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        //showMsg("MSG_SEN04", mess);
                        //MLMessages("Hải quan chưa xử lý danh sách sản phẩm này!","MSG_SEN04","", false);

                        if (mess != "")
                            ShowMessage(mess, false);
                        else
                            ShowMessage("Hải quan chưa xử lý danh sách hợp đồng này!", false);
                    }
                    else if (HopDongSelected.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        showMsg("MSG_SEN05");
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu","MSG_SEN05","", false);                        
                    }
                    else if (HopDongSelected.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        showMsg("MSG_CT_CANCEL");
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            Globals.ShowMessageTQDT(msg[0], false);
                            //showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0],"MSG_SEN09",msg[0], false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message.Trim());
                            //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin hợp đồng. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells[6].Text == TrangThaiXuLy.CHO_DUYET.ToString())
                    e.Row.Cells[6].Text = "Chờ duyệt";
                //if (GlobalSettings.NGON_NGU == "1") { e.Row.Cells[6].Text = "Wait to approve"; }
                else if (e.Row.Cells[6].Text == TrangThaiXuLy.CHUA_KHAI_BAO.ToString())
                    e.Row.Cells[6].Text = "Chưa khai báo";
                else if (e.Row.Cells[6].Text == TrangThaiXuLy.DA_DUYET.ToString())
                    e.Row.Cells[6].Text = "Đã duyệt";
                else if (e.Row.Cells[6].Text == TrangThaiXuLy.KHONG_PHE_DUYET.ToString())
                    e.Row.Cells[6].Text = "Không phê duyệt";
                else if (e.Row.Cells[6].Text == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY.ToString())
                    e.Row.Cells[6].Text = "Chờ hủy";
                else if (e.Row.Cells[6].Text == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY.ToString())
                    e.Row.Cells[6].Text = "Đã hủy";
                else if (e.Row.Cells[6].Text == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET.ToString())
                    e.Row.Cells[6].Text = "Sửa";
                if (dgList.RootTable.Columns[7].Visible == true)
                {
                    if (e.Row.Cells[7].Value.ToString() == "1")
                    {
                        e.Row.Cells[7].Text = "Đã duyệt";
                    }
                    else
                        e.Row.Cells[7].Text = "Chưa duyệt";
                }
            }
        }
        public void search()
        {
            string where = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' ";
            if (txtSoTiepNhan.Text.Trim().Length > 0)
            {
                where += " and sotiepnhan like '%" + txtSoTiepNhan.Text.Trim() + "%'";
            }
            if (txtSoHopDong.Text.Trim().Length > 0)
            {
                where += " and sohopdong like '%" + txtSoHopDong.Text.Trim() + "%'";
            }
            if (ccNgayKyHD.Text.Length > 0)
            {
                where += " and NgayKy='" + ccNgayKyHD.Value.Month.ToString() + "/" + ccNgayKyHD.Value.Day.ToString() + "/" + ccNgayKyHD.Value.Year + "'";
            }
            if (txtNamTiepNhan.Text.Trim().Length > 0)
            {
                where += " and year(ngaytiepnhan)=" + txtNamTiepNhan.Text.Trim();
            }
            if (cbStatus.Text.Length > 0)
                where += " and TrangThaiXuLy=" + cbStatus.SelectedValue.ToString();

            Company.GC.BLL.KDT.GC.HopDong hd = new HopDong();
            collection = HopDong.SelectCollectionDynamic(where, "");
            dgList.DataSource = collection;
            setCommandStatus();
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }



        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "KhaiBao": /*khaibaoHD()*/ SendV3(); break;
                case "NhanDuLieu": /*nhandulieuHD()*/ FeedBackV3(); break;
                case "Huy": /*HuyNhieuHD()*/ CancelV3(); break;
                //DATLMQ comment ngày 24/03/2011
                //case "XacNhan": LaySoTiepNhanDT(); break;
                case "XacNhan": nhandulieuHD(); break;
                case "cmdXuatHopDong": XuatDuLieuHopDongChoPhongKhai(); break;
                case "cmdCSDaDuyet": ChuyenTrangThai(); break;
                case "InPhieuTN": this.inPhieuTN(); break;
            }

        }
        private void inPhieuTN()
        {
            if (!(dgList.GetRows().Length > 0 && dgList.GetRow().RowType == RowType.Record)) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
            phieuTNForm.TenPhieu = "HỢP ĐỒNG";
            Company.Interface.Report.GC.PhieuTN phieuTNAll = new Company.Interface.Report.GC.PhieuTN();
            string[,] arrPhieuTN = new string[items.Count, 2];
            int j = 0;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    HopDong hdDangKySelected = (HopDong)i.GetRow().DataRow;
                    arrPhieuTN[j, 0] = hdDangKySelected.SoTiepNhan.ToString();
                    arrPhieuTN[j, 1] = hdDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                    j++;
                    break;
                }
            }
            phieuTNForm.phieuTN = arrPhieuTN;
            phieuTNForm.Show();

        }
        private void ChuyenTrangThai()
        {
            if (dgList.SelectedItems.Count > 0)
            {
                List<HopDong> hdColl = new List<HopDong>();
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    hdColl.Add((HopDong)grItem.GetRow().DataRow);
                }
                for (int i = 0; i < hdColl.Count; i++)
                {
                    hdColl[i].LoadCollection();
                    //string msg = "Bạn có muốn chuyển trạng thái của hợp đồng được chọn sang đã duyệt không?";
                    //msg += "\n\nSố thứ tự của hợp đồng: " + hdColl[i].ID.ToString();
                    //msg += "\nCó " + hdColl[i].NhomSPCollection.Count.ToString() + " sản phẩm gia công";
                    //msg += "\nCó " + hdColl[i].NPLCollection.Count.ToString() + " nguyên phụ liệu đăng ký";
                    //msg += "\nCó " + hdColl[i].SPCollection.Count.ToString() + " sản phẩm đăng ký";
                    //msg += "\nCó " + hdColl[i].TBCollection.Count.ToString() + " thiết bị đăng ký đăng ký";

                    string[] args = new string[5];
                    args[0] = hdColl[i].ID.ToString();
                    args[1] = hdColl[i].NhomSPCollection.Count.ToString();
                    args[2] = hdColl[i].NPLCollection.Count.ToString();
                    args[3] = hdColl[i].SPCollection.Count.ToString();
                    args[4] = hdColl[i].TBCollection.Count.ToString();

                    if (showMsg("MSG_0203070", args, true) == "Yes")
                    {
                        if (hdColl[i].TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        {
                            hdColl[i].NgayTiepNhan = DateTime.Today;
                        }
                        hdColl[i].TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                        hdColl[i].Update();
                        Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty,
                            hdColl[i].ID,
                            hdColl[i].GUIDSTR, Company.KDT.SHARE.Components.MessageTypes.HopDong, Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                            string.Format("Trước khi chuyển trạng thái ID={0},Trạng thái ={1},Số tiếp nhận{2},Hợp đồng={3}",
                              hdColl[i].TrangThaiXuLy, hdColl[i].ID, hdColl[i].SoTiepNhan, hdColl[i].SoHopDong));

                    }
                }
                this.search();
            }
            else
            {
                showMsg("MSG_2702052");

                //MLMessages("Chưa có dữ liệu được chọn!","MSG_WRN11","", false);
            }
        }

        private void XuatDuLieuHopDongChoPhongKhai()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                showMsg("MSG_2702052");
                //MLMessages("Chưa có dữ liệu được chọn để xuất !", "MSG_WRN11", "", false);
                return;
            }
            try
            {
                List<HopDong> col = new List<HopDong>();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<HopDong>));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    int sohopdong = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            HopDong HD = (HopDong)i.GetRow().DataRow;
                            HD.LoadCollection();
                            col.Add(HD);
                            sohopdong++;
                        }
                    }
                    serializer.Serialize(fs, col);
                    showMsg("MSG_2702054", sohopdong);
                    //MLMessages("Xuất ra file thành công " + sohopdong + " hợp đồng.", "MSG_EXC05", sohopdong.ToString(), false);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void nhandulieuHD()
        {
            if (dgList.GetRow() == null)
            {
                showMsg("MSG_2702052");
                //MLMessages("Chưa có dữ liệu được chọn!", "MSG_WRN11", "", false);
                return;
            }
            HopDongSelected = (HopDong)dgList.GetRow().DataRow;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "HD";
            sendXML.master_id = HopDongSelected.ID;
            if (sendXML.Load())
            {
                //showMsg("MSG_WRN05");
                //MLMessages("Hợp đồng đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.","MSG_STN11","", false);
                //return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                //DATLMQ comment ngày 24/03/2011
                //xmlCurrent = HopDongSelected.WSDownLoad(password);
                this.Cursor = Cursors.Default;

                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "HD";
                sendXML.master_id = HopDongSelected.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN09", msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message.Trim());
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận thông tin hợp đồng . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }
        private void uiCommandBar1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void khaibaoHD()
        {
            if (dgList.GetRow() == null)
            {
                showMsg("MSG_2702052");
                //MLMessages("Chưa chọn hợp đồng!", "MSG_WRN11", "", false);
                return;
            }
            HopDongSelected = (HopDong)dgList.GetRow().DataRow;
            HopDongSelected = HopDong.Load(HopDongSelected.ID);
            HopDongSelected.LoadCollection();
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "HD";
            sendXML.master_id = HopDongSelected.ID;
            if (sendXML.Load())
            {
                // showMsg("MSG_WRN05");

                MLMessages("Hợp đồng đã gửi đến Hải quan.\nHãy chọn chức năng 'Nhận dữ liệu' cho Hợp đồng này.", "MSG_STN11", "", false);
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;

                xmlCurrent = HopDongSelected.WSSend(password);

                this.Cursor = Cursors.Default;

                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "HD";
                sendXML.master_id = HopDongSelected.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();

                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0],"MSG_SEN09",msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message.Trim());
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo thông tin hợp đồng . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }
        private void HuyNhieuHD()
        {
            if (dgList.GetRow() == null)
            {
                showMsg("MSG_2702052");
                //MLMessages("Chưa chọn hợp đồng!", "MSG_WRN11", "", false);
                return;
            }
            HopDongSelected = (HopDong)dgList.GetRow().DataRow;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "HD";
            sendXML.master_id = HopDongSelected.ID;
            if (sendXML.Load())
            {
                //showMsg("MSG_WRN05");
                ////MLMessages("Hợp đồng đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_STN11", "", false);
                //return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = HopDongSelected.WSCancel(password);
                this.Cursor = Cursors.Default;

                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "HD";
                sendXML.master_id = HopDongSelected.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN09", msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message.Trim());
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy thông tin hợp đồng . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
        }

        private void setCommandStatus()
        {
            //dgList.RootTable.Columns[7].Visible = false;
            if (cbStatus.SelectedValue == null) return;
            if ((cbStatus.SelectedValue.ToString() == "-1") || (cbStatus.SelectedValue.ToString() == "10") || (cbStatus.SelectedValue.ToString() == "2"))
            {
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdXuatHopDong.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnXoa.Enabled = true;
                cmdCSDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (cbStatus.SelectedValue.ToString() == "0")
            {
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdXuatHopDong.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnXoa.Enabled = false;
                cmdCSDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            else if ((cbStatus.SelectedValue.ToString() == "11") || (cbStatus.SelectedValue.ToString() == "5"))
            {
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdXuatHopDong.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnXoa.Enabled = false;
                cmdCSDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.True;

            }
            else
            {
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                KhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdXuatHopDong.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnXoa.Enabled = false;
                cmdCSDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }

            XacNhan.Visible = Janus.Windows.UI.InheritableBoolean.False;
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa hợp đồng này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                int j = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HopDong hd = (Company.GC.BLL.KDT.GC.HopDong)i.GetRow().DataRow;
                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "HD";
                        sendXML.master_id = hd.ID;
                        if (sendXML.Load())
                        {
                            j = i.Position + 1;
                            showMsg("MSG_2702012", i.Position + 1);
                            //MLMessages("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", "MSG_STN09",j.ToString() , false);
                        }
                        else
                        {
                            if (hd.ID > 0)
                            {
                                hd.Delete();
                            }
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (collection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            int j = 0;
            if (items.Count <= 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa hợp đồng này không?", true) == "Yes")
            {

                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HopDong hd = (Company.GC.BLL.KDT.GC.HopDong)i.GetRow().DataRow;
                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "HD";
                        sendXML.master_id = hd.ID;
                        if (sendXML.Load())
                        {
                            j = i.Position + 1;
                            showMsg("MSG_2702012", i.Position + 1);
                            //MLMessages("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.", "MSG_STN09",j.ToString() , false);
                        }
                        else
                        {
                            if (hd.ID > 0)
                            {
                                hd.Delete();
                            }
                            //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
                            try
                            {
                                string where = "1 = 1";
                                where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", hd.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.HopDong);
                                List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                                if (listLog.Count > 0)
                                {
                                    long idLog = listLog[0].IDLog;
                                    string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                                    long idDK = listLog[0].ID_DK;
                                    string guidstr = listLog[0].GUIDSTR_DK;
                                    string userKhaiBao = listLog[0].UserNameKhaiBao;
                                    DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                                    string userSuaDoi = GlobalSettings.UserLog;
                                    DateTime ngaySuaDoi = DateTime.Now;
                                    string ghiChu = listLog[0].GhiChu;
                                    bool isDelete = true;
                                    Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                                userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                                }
                            }
                            catch (Exception ex)
                            {
                                ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                                return;
                            }
                        }
                    }
                }
                this.search();
            }
        }

        private void btnXemNPLDK_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                showMsg("MSG_0203075");
                return;
            }
            if (items.Count > 1)
            {
                showMsg("MSG_0203076");
                return;
            }
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    Company.GC.BLL.KDT.GC.HopDong hd = (Company.GC.BLL.KDT.GC.HopDong)i.GetRow().DataRow;
                    ReportViewBC01_DKNVLForm Report08 = new ReportViewBC01_DKNVLForm();
                    Report08.HD = hd;
                    Report08.Show();
                }
            }
        }

        private void btnResultHistory_Click(object sender, EventArgs e)
        {

            if (dgList.GetRow() == null)
            {
                showMsg("MSG_0203075");
                return;
            }
            HopDongSelected = (HopDong)dgList.GetRow().DataRow;
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = HopDongSelected.ID;
            form.DeclarationIssuer = DeclarationIssuer.HOP_DONG_GIA_CONG;
            form.ShowDialog(this);
            //Globals.ShowKetQuaXuLyBoSung(HopDongSelected.GUIDSTR);
        }

        //-----------------------------------------------------------------------------------------
        #region Send V3 Create by LANNT
        private void SendV3()
        {
            if (dgList.GetRow() == null)
            {
                showMsg("MSG_2702052");
                //MLMessages("Chưa chọn hợp đồng!", "MSG_WRN11", "", false);
                return;
            }
            HopDongSelected = (HopDong)dgList.GetRow().DataRow;
            HopDongSelected = HopDong.Load(HopDongSelected.ID);

            HopDongSelected.LoadCollection();
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.HopDong;
            sendXML.master_id = HopDongSelected.ID;
            if (sendXML.Load())
            {
                // showMsg("MSG_WRN05");

                MLMessages("Hợp đồng đã gửi đến Hải quan.\nHãy chọn chức năng 'Nhận dữ liệu' cho Hợp đồng này.", "MSG_STN11", "", false);
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                return;
            }
            try
            {
                HopDongSelected.GUIDSTR = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.GC_HopDong hd = Company.GC.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_GC_HopDong(HopDongSelected, GlobalSettings.DIA_CHI);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = HopDongSelected.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HopDongSelected.MaHaiQuan),
                                     Identity = HopDongSelected.MaHaiQuan
                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.HOP_DONG_GIA_CONG,
                                    Function = hd.Function,
                                    Reference = HopDongSelected.GUIDSTR,
                                }
                                ,
                                hd);
                HopDongSelected.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);
                if (isSend && HopDongSelected.TrangThaiXuLy != TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    sendForm.Message.XmlSaveMessage(HopDongSelected.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHopDong);
                    XacNhan.Enabled = XacNhan1.Enabled = XacNhan2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.HopDong;
                    sendXML.master_id = HopDongSelected.ID;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    HopDongSelected.Update();
                    FeedBackV3();

                    btnSearch_Click(null, null);
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                {
                    sendForm.Message.XmlSaveMessage(HopDongSelected.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQTuChoiHopDong, msgInfor);
                    ShowMessageTQDT(msgInfor, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.HopDongSendHandler(HopDongSelected, ref msgInfor, e);
        }
        private void FeedBackV3()
        {
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            bool isFeedBack = true;
            if (dgList.GetRow() != null)
                HopDongSelected = (HopDong)dgList.GetRow().DataRow;
            else
                return;
            string reference = HopDongSelected.GUIDSTR;

            Company.KDT.SHARE.Components.SubjectBase subjectBase = new Company.KDT.SHARE.Components.SubjectBase()
            {
                Issuer = Company.KDT.SHARE.Components.DeclarationIssuer.HOP_DONG_GIA_CONG,
                Reference = reference,
                Function = Company.KDT.SHARE.Components.DeclarationFunction.HOI_TRANG_THAI,
                Type = Company.KDT.SHARE.Components.DeclarationIssuer.HOP_DONG_GIA_CONG,

            };

            ObjectSend msgSend = new ObjectSend(
                                        new Company.KDT.SHARE.Components.NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = HopDongSelected.MaDoanhNghiep
                                        },
                                          new Company.KDT.SHARE.Components.NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HopDongSelected.MaHaiQuan.Trim()),
                                              Identity = HopDongSelected.MaHaiQuan
                                          }, subjectBase, null);

            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes"; ;
                    }
                    else
                    {
                        if (HopDongSelected.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                        {
                            if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                           feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                            {
                                isFeedBack = false;
                                ShowMessageTQDT(msgInfor, false);
                            }
                            else
                            {
                                isFeedBack = true;
                                ShowMessageTQDT(msgInfor, false);
                            }
                            count--;
                        }
                        else if (HopDongSelected.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || HopDongSelected.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            ShowMessageTQDT(msgInfor, false);
                            isFeedBack = false;
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                        else isFeedBack = false;

                        this.search();
                    }
                }
            }

        }
        /// <summary>
        /// Hủy Thông Tin đến Hải Quan
        /// </summary>
        private void CancelV3()
        {
            //Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            if (dgList.GetRow() != null)
            {
                HopDongSelected = (HopDong)dgList.GetRow().DataRow;
                //sendXML.LoaiHS = LoaiKhaiBao.HopDong;
                //sendXML.master_id = HopDongSelected.ID;
                //if (sendXML.Load())
                //{
                //    MLMessages("Đã gửi thông tin hủy khai báo đến hải quan. Bấm nút [lấy phản hồi] để nhận thông tin phản hồi", "", "", false);
                //    XacNhan.Enabled = XacNhan1.Enabled = XacNhan2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //    return;
                //}
            }
            else
            {
                MLMessages("Chưa chọn thông tin để hủy.", "MSG_CNL01", "", false);
                return;
            }
            Company.KDT.SHARE.Components.DeclarationBase npl = Company.GC.BLL.DataTransferObjectMapper.Mapper.HuyKhaiBao(Company.KDT.SHARE.Components.DeclarationIssuer.HOP_DONG_GIA_CONG, HopDongSelected.GUIDSTR, HopDongSelected.SoTiepNhan, HopDongSelected.MaHaiQuan, HopDongSelected.NgayTiepNhan);
            ObjectSend msgSend = new ObjectSend(
                           new Company.KDT.SHARE.Components.NameBase()
                           {
                               Name = GlobalSettings.TEN_DON_VI,
                               Identity = HopDongSelected.MaDoanhNghiep
                           }
                             , new Company.KDT.SHARE.Components.NameBase()
                             {
                                 Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HopDongSelected.MaHaiQuan),
                                 Identity = HopDongSelected.MaHaiQuan
                             }
                          ,
                            new Company.KDT.SHARE.Components.SubjectBase()
                            {
                                Type = Company.KDT.SHARE.Components.DeclarationIssuer.HOP_DONG_GIA_CONG,
                                Function = Company.KDT.SHARE.Components.DeclarationFunction.HUY,
                                Reference = HopDongSelected.GUIDSTR,
                            }
                           ,
                            npl);
            SendMessageForm sendForm = new SendMessageForm();
            HopDongSelected.TrangThaiXuLy = TrangThaiXuLy.CHO_HUY;
            sendForm.Send += SendMessage;
            bool isSend = sendForm.DoSend(msgSend);
            if (isSend && HopDongSelected.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                sendForm.Message.XmlSaveMessage(HopDongSelected.ID, Company.KDT.SHARE.Components.MessageTitle.HuyKhaiBaoHopDong);
                HopDongSelected.Update();
                FeedBackV3();
            }
            else if (!string.IsNullOrEmpty(msgInfor))
                ShowMessageTQDT(msgInfor, false);
        }
        #endregion
    }
}