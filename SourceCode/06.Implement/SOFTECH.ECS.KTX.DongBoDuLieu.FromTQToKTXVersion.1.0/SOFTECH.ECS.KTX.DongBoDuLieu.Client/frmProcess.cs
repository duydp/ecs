﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Xml;

namespace DongBo
{
    public partial class frmProcess : Form
    {
        public frmProcess()
        {
            InitializeComponent();
        }
        public enum LoaiDuLieu
        {
            ToKhai,
            VanDon,
            VanDonCT,
            CO,
            COCT,
            HoaDonThuongMai,
            HoaDonThuongMaiCT,
            HopDongThuongMai,
            HopDongThuongMaiCT,
            GiayPhep,
            GiayPhepCT,
            ChungTuDangAnh,
            ChungTuDangAnhCT,
            ChuyenCK
        }
        public LoaiDuLieu TrangThaiGetDuLieu;
        public string LblNoiDungDB { set { lblNoiDung.Text = value; } }
        public DataTable dt = new DataTable();
        public string result = string.Empty;
        public string NoiDungQuerry = string.Empty;
        public string Condition = string.Empty;
        private string Loadmethod(LoaiDuLieu loai)
        {
            try
            {
                
                XmlDocument method = new XmlDocument();
                switch (loai)
                {
                    case LoaiDuLieu.ToKhai:
                        method.Load(Application.StartupPath + "\\QueryToKhaiMauDich\\ToKhai.xml");
                        break;
                    case LoaiDuLieu.VanDon:
                        method.Load(Application.StartupPath + "\\QueryToKhaiMauDich\\VanDon.xml");
                        break;
                    case LoaiDuLieu.VanDonCT:
                        method.Load(Application.StartupPath + "\\QueryToKhaiMauDich\\VanDonCT.xml");
                        break;
                    case LoaiDuLieu.HoaDonThuongMai:
                        method.Load(Application.StartupPath + "\\QueryToKhaiMauDich\\HoaDonThuongMai.xml");
                        break;
                    case LoaiDuLieu.HoaDonThuongMaiCT:
                        method.Load(Application.StartupPath + "\\QueryToKhaiMauDich\\HoaDonThuongMaiCT.xml");
                        break;
                    case LoaiDuLieu.HopDongThuongMai:
                        method.Load(Application.StartupPath + "\\QueryToKhaiMauDich\\HopDongThuongMai.xml");
                        break;
                    case LoaiDuLieu.HopDongThuongMaiCT:
                        method.Load(Application.StartupPath + "\\QueryToKhaiMauDich\\HopDongThuongMaiCT.xml");
                        break;
                    case LoaiDuLieu.GiayPhep:
                        method.Load(Application.StartupPath + "\\QueryToKhaiMauDich\\GiayPhep.xml");
                        break;
                    case LoaiDuLieu.GiayPhepCT:
                        method.Load(Application.StartupPath + "\\QueryToKhaiMauDich\\GiayPhepCT.xml");
                        break;
                    case LoaiDuLieu.CO:
                        method.Load(Application.StartupPath + "\\QueryToKhaiMauDich\\CO.xml");
                        break;
                    case LoaiDuLieu.COCT:
                        method.Load(Application.StartupPath + "\\QueryToKhaiMauDich\\COCT.xml");
                        break;
                    case LoaiDuLieu.ChungTuDangAnh:
                        method.Load(Application.StartupPath + "\\QueryToKhaiMauDich\\ChungTuAnh.xml");
                        break;
                    case LoaiDuLieu.ChungTuDangAnhCT:
                        method.Load(Application.StartupPath + "\\QueryToKhaiMauDich\\ChungTuAnhCT.xml");
                        break;
                    case LoaiDuLieu.ChuyenCK:
                        method.Load(Application.StartupPath + "\\QueryToKhaiMauDich\\ChuyenCK.xml");
                        break;

                }
                XmlNamespaceManager namespaceManager = new XmlNamespaceManager(method.NameTable);
                namespaceManager.AddNamespace("Soap", "http://schemas.xmlsoap.org/soap/envelope/");
                XmlNode getdataNode = method.SelectSingleNode("Soap:Envelope/Soap:Body",namespaceManager).FirstChild;
                XmlNodeList ParameterNode = getdataNode.ChildNodes;

                ParameterNode[0].InnerText = ParameterNode[0].InnerText + Condition;
                ParameterNode[1].InnerText = GlobalSettings.MaHaiQuan;
                ParameterNode[2].InnerText = "SLXNK";
                ParameterNode[3].InnerText = GlobalSettings.MaHaiQuan == "C34C"? "" : "daibangtungcanh";
                ParameterNode[4].InnerText = GlobalSettings.MaDoanhNghiep;
                ParameterNode[5].InnerText = GlobalSettings.MatKhau;

                return method.InnerXml;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
                return string.Empty;
            }
        }
        private void LoadServices(string messagesQuery)
        {
            WebClient client = new WebClient();
            client.Headers.Add(HttpRequestHeader.ContentType, "text/xml");
            client.Headers.Add("SOAPAction", "http://tempuri.org/GetData");
            client.UploadProgressChanged += new UploadProgressChangedEventHandler(client_UploadProgressChanged);
            client.UploadStringCompleted += new UploadStringCompletedEventHandler(client_UploadStringCompleted);
            byte[] data = System.Text.Encoding.ASCII.GetBytes(NoiDungQuerry);
            client.UploadStringAsync(new Uri(GlobalSettings.DiaChiWSDongBo), messagesQuery);
        }

        void client_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error==null)
            {
                result = e.Result;
                this.Close();
            }
            else
            {
                result = string.Empty;
                MessageBox.Show(e.Error.Message);
                this.Close();
            }
        }


        void client_UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {

            lblTrangThai.Text = string.Format("{0}    uploaded {1} of {2} bytes. {3} % complete...",
                (string)e.UserState,
                e.BytesSent,
                e.TotalBytesToSend,
                (e.ProgressPercentage - 50)*2 );
            prcbar.Value = (e.ProgressPercentage - 50)*2;


        }

        private void frmProcess_Load(object sender, EventArgs e)
        {
            switch (TrangThaiGetDuLieu)
            {
                case LoaiDuLieu.ToKhai:
                    lblNoiDung.Text = "Đang đồng bộ thông tin tờ khai";
                    break;
                case LoaiDuLieu.VanDon:
                    lblNoiDung.Text = "Đang đồng bộ thông tin vận đơn";
                    break;
                case LoaiDuLieu.VanDonCT:
                    lblNoiDung.Text = "Đang đồng bộ thông tin Container";
                    break;
                case LoaiDuLieu.HoaDonThuongMai:
                    lblNoiDung.Text = "Đang đồng bộ thông chứng từ hóa đơn thương mại";
                    break;
                case LoaiDuLieu.HoaDonThuongMaiCT:
                    lblNoiDung.Text = "Đang đồng bộ thông hàng chứng từ hóa đơn thương mại";
                    break;
                case LoaiDuLieu.HopDongThuongMai:
                    lblNoiDung.Text = "Đang đồng bộ thông tin chứng từ hợp đồng thương mại";
                    break;
                case LoaiDuLieu.HopDongThuongMaiCT:
                    lblNoiDung.Text = "Đang đồng bộ thông tin hàng chứng từ hợp đồng thương mại";
                    break;
                case LoaiDuLieu.CO:
                    lblNoiDung.Text = "Đang đồng bộ thông tin chứng từ CO";
                    break;
                case LoaiDuLieu.ChungTuDangAnh:
                    lblNoiDung.Text = "Đang đồng bộ thông tin Chứng từ dạng ảnh";
                    break;
                case LoaiDuLieu.ChungTuDangAnhCT:
                    lblNoiDung.Text = "Đang đồng bộ thông tin nội dung Chứng từ dạng ảnh";
                    break;
                case LoaiDuLieu.GiayPhep:
                    lblNoiDung.Text = "Đang đồng bộ thông tin chứng từ giấy phép";
                    break;
                case LoaiDuLieu.GiayPhepCT:
                    lblNoiDung.Text = "Đang đồng bộ thông tin hàng chứng từ giấy phép";
                    break;
                case LoaiDuLieu.ChuyenCK:
                    lblNoiDung.Text = "Đang đồng bộ thông tin hàng chứng từ chuyển cửa khẩu";
                    break;
            }
            if (!string.IsNullOrEmpty(this.Condition))
            {
                this.LoadServices(Loadmethod(TrangThaiGetDuLieu));
            }
            else
            {
                MessageBox.Show("Điều kiện where sai");
                this.Close();
            }


        }

    }
}
