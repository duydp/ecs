
/****** Object:  UserDefinedFunction [dbo].[f_KiemTra_TienTrinh_ThanhLy]    Script Date: 05/14/2014 08:52:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[f_KiemTra_TienTrinh_ThanhLy]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[f_KiemTra_TienTrinh_ThanhLy]
GO


/****** Object:  UserDefinedFunction [dbo].[f_KiemTra_TienTrinh_ThanhLy]    Script Date: 05/14/2014 08:52:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[f_KiemTra_TienTrinh_ThanhLy]
(
	@MaHaiQuan VARCHAR(10),
	@MaDoanhNghiep VARCHAR(50),
	@SoToKhai INT,
	@MaLoaihinh VARCHAR(10),
	@NamDangKy INT,
	@MaNPL NVARCHAR(255),
	@LuongTonHienTai NUMERIC(18,8)
	
)
RETURNS NVARCHAR(500)
AS 
    
/*
@SOFTECH
HUNGTQ, CREATED 04/08/2011.
Kiem tra tien trinh thuc hien chay thanh ly: . 
Gia tri dung phai la so ton cuoi cua lan thanh ly dau = so ton dau cua lan thanh ly tiep theo. 
*/
	
BEGIN

--SET @MaHaiQuan = 'C34C'
--SET @MaDoanhNghiep = '0400101556'
--SET @SoToKhai = 152
--SET @MaLoaihinh = 'NSX01'
--SET @NamDangKy = 2010
--SET @MaNPL = 'NEPLUNG'
			
DECLARE
	@LanThanhLy INT,
	@Luong NUMERIC(18,8),
	@Luong_BanLuu NUMERIC(18,8),
	@Luong_NhapTon NUMERIC(18,8),
	@TonDau NUMERIC(18,8),
	@TonCuoi NUMERIC(18,8),
	@TonCuoi_BanLuu NUMERIC(18,8),
	@TonDauThueXNK NUMERIC(18,8),
	@TonCuoiThueXNK NUMERIC(18,8),
	@cnt1 INT,--Bien dem
	@str NVARCHAR(500),
	@ketqua NVARCHAR(500), --hien thi ket qua
	@SaiSoLuong INT,
	@Valid BIT,
	@Dem INT,
	@isDongHoSo INT
	-- Đếm số dòng thanh lý
	SELECT  @Dem = COUNT(*)
	FROM      [dbo].[t_KDT_SXXK_NPLNhapTon]
	WHERE     SoToKhai = @SoToKhai
			AND MaLoaiHinh = @MaLoaiHinh
			AND NamDangKy = @NamDangKy
			AND MaNPL = @MaNPL
			AND TonCuoi < TonDau
			AND LanThanhLy IN ( SELECT  LanThanhLy
								FROM    t_KDT_SXXK_HoSoThanhLyDangKy )
			AND MaHaiQuan = @MaHaiQuan
			AND MaDoanhNghiep = @MaDoanhNghiep
/*<I> Cap nhat lech luong nhap do bi lam tron so thap phan*/	
DECLARE curThanhLy CURSOR FOR
	SELECT  nplTon.LanThanhLy, Luong, TonDau, TonCuoi, TonDauThueXNK, TonCuoiThueXNK
	,CASE 
			WHEN  EXISTS(SELECT TrangThaiThanhKhoan  FROM t_KDT_SXXK_HoSoThanhLyDangKy hs WHERE hs.LanThanhLy = nplTon.LanThanhLy AND hs.TrangThaiThanhKhoan = 401) THEN 1
			ELSE  0
			END AS IsDongHoSo
	FROM      [dbo].[t_KDT_SXXK_NPLNhapTon] nplTon
	WHERE     SoToKhai = @SoToKhai
			AND MaLoaiHinh = @MaLoaiHinh
			AND NamDangKy = @NamDangKy
			AND MaNPL = @MaNPL
			AND TonCuoi < TonDau
			AND LanThanhLy IN ( SELECT  LanThanhLy
								FROM    t_KDT_SXXK_HoSoThanhLyDangKy )
			AND MaHaiQuan = @MaHaiQuan
			AND MaDoanhNghiep = @MaDoanhNghiep
	ORDER BY TonCuoi DESC
	
	
	
--BEGIN TRANSACTION T1 ;

	SET @Valid = 1
	SET @SaiSoLuong = 0	
    SET @cnt1 = 0;
    
	SET @str = '....................................................................' + char(10)
	SET @str = @str + '		STT' + ' - ' + + 'SoToKhai' + ' - ' + 'MaLoaiHinh' + ' - ' + 'NamDangKy' + ' - ' + 'MaNPL' + char(10)
	SET @str = @str + STR(@cnt1) + ' - ' + STR(@SoToKhai) + ' - ' + @MaLoaiHinh + ' - ' + str(@NamDangKy)+ ' - ' + @MaNPL + char(10)
	--PRINT @str								
	
    OPEN curThanhLy 
    FETCH NEXT FROM curThanhLy INTO @LanThanhLy, @Luong, @TonDau, @TonCuoi, @TonDauThueXNK, @TonCuoiThueXNK, @isDongHoSo
	--Fetch next record
    WHILE @@FETCH_STATUS = 0 
		BEGIN
							
			SET @cnt1 = @cnt1 + 1
						
			--PRINT STR(@cnt1) + '	-> Lan thanh ly : ' + cast(@LanThanhLy AS VARCHAR(50)) + '	Luong' + cast(@Luong AS VARCHAR(50)) + '	-	TonDau: ' + cast(@TonDau AS VARCHAR(50)) + '	-	TonCuoi: ' + cast(@TonCuoi AS VARCHAR(50))
										
			IF (@cnt1 = 1)
				BEGIN
					
					--Kiem tra so luong nhap dang ky ban dau so voi luong ton dau.
					IF (@Luong <> @TonDau)
						BEGIN
							SET @ketqua = N'Sai lượng nhập và lượng tồn đầu tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
							SET @Valid = 0
						END				
					
						  
					IF(@Valid = 1)
						BEGIN
							--Kiem tra so luong nhap dang ky ban dau so voi luong nhap luu trong ton.
							SELECT  @SaiSoLuong =  COUNT(*), @Luong_NhapTon = nt.Luong
								  FROM      [dbo].t_SXXK_ThanhLy_NPLNhapTon nt
											INNER JOIN dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu
																				   AND nt.SoToKhai = h.SoToKhai
																				   AND nt.MaLoaiHinh = h.MaLoaiHinh
																				   AND nt.NamDangKy = h.NamDangKy
																				   AND nt.MaHaiQuan = h.MaHaiQuan
								  WHERE     nt.SoToKhai = @SoToKhai
											AND nt.MaLoaiHinh = @MaLoaiHinh
											AND nt.NamDangKy = @NamDangKy
											AND nt.MaNPL = @MaNPL
											AND nt.MaHaiQuan = @MaHaiQuan
											AND nt.MaDoanhNghiep = @MaDoanhNghiep
											AND nt.Luong <> h.SoLuong
								  GROUP BY nt.Luong
						  
							IF (@SaiSoLuong > 0 AND @Luong_NhapTon <> @Luong)
								BEGIN
									SET @ketqua = N'Sai lượng nhập tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
									SET @Valid = 0
								END
							ELSE
								BEGIN	  
									SET @ketqua = N'Thanh lý đúng'
									SET @Valid = 1
									SET @TonCuoi_BanLuu = @TonCuoi
								END
						END
				END
			IF(@cnt1 = @Dem)
			BEGIN
				IF(@isDongHoSo != 1 AND  @LuongTonHienTai <> @TonDau)
				BEGIN
					SET @str = @str +  '	=> Sai so lieu ton: ' + cast(@TonDau AS VARCHAR(50)) + ' <> ' + cast(@LuongTonHienTai AS VARCHAR(50))
							--PRINT @str
							
							SET @ketqua = N'Sai số liệu hiện tại' + cast(@LanThanhLy AS VARCHAR(50))
				END
				IF(@isDongHoSo = 1 AND  @LuongTonHienTai <> @TonCuoi)
				BEGIN
					SET @str = @str +  '	=> Sai so lieu ton: ' + cast(@TonDau AS VARCHAR(50)) + ' <> ' + cast(@LuongTonHienTai AS VARCHAR(50))
							--PRINT @str
							
							SET @ketqua = N'Sai số liệu hiện tại' + cast(@LanThanhLy AS VARCHAR(50))
				END
			END
			--Kiem tra so ton
			ELSE IF (@cnt1 > 1 AND @Valid = 1) --kiem tra tu dong thu 2 tro di
				BEGIN

					SET @str = STR(@cnt1) + '	-> Lan thanh ly : ' + cast(@LanThanhLy AS VARCHAR(50)) + '	Luong' + cast(@Luong AS VARCHAR(50)) + '	-	TonDau' + cast(@TonDau AS VARCHAR(50)) + '	-	TonCuoi' + cast(@TonCuoi AS VARCHAR(50))
					
					IF (@TonDau <> @TonCuoi_BanLuu)
						BEGIN
							
							SET @str = @str +  '	=> Sai so lieu ton: ' + cast(@TonDau AS VARCHAR(50)) + ' <> ' + cast(@TonCuoi_BanLuu AS VARCHAR(50))
							--PRINT @str
							
							SET @ketqua = N'Sai tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
						END
					ELSE
						SET @ketqua = N'Thanh lý đúng'
				END						
			
			--Luu lai ton cuoi cua row truoc do
			SET @TonCuoi_BanLuu = @TonCuoi
				
			FETCH NEXT FROM curThanhLy INTO @LanThanhLy, @Luong, @TonDau, @TonCuoi, @TonDauThueXNK, @TonCuoiThueXNK, @isDongHoSo
        END
        
    CLOSE curThanhLy --Close cursor
    DEALLOCATE curThanhLy --Deallocate cursor
	
	--PRINT @ketqua
	
    --IF @@ERROR != 0 
    --    BEGIN
            --PRINT 'Qua trinh thuc hien xay ra loi.'
            --PRINT STR(@@ERROR)
            ----ROLLBACK TRANSACTION T1 ;
            --PRINT 'ROLLBACK TRANSACTION T1'
  --      END
  --  ELSE 
		--BEGIN
			----COMMIT TRANSACTION T1 ;	
			--PRINT ''
			----PRINT 'COMMIT TRANSACTION T1'
		--END
	
	RETURN @ketqua
	
END	

--SELECT ECS_TQ_SXXK_HOATHO_2011.dbo.[f_KiemTra_TienTrinh_ThanhLy]('C34C', '0400101556', 1782, 'NSX01', 2010, 'ÐINHTAN')


GO




/****** Object:  View [dbo].[v_HangTon]    Script Date: 05/12/2014 20:10:59 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_HangTon]'))
DROP VIEW [dbo].[v_HangTon]
GO


/****** Object:  View [dbo].[v_HangTon]    Script Date: 05/12/2014 20:10:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_HangTon]
AS
SELECT     CASE WHEN t .MaLoaiHinh LIKE '%V%' THEN
                          (SELECT     TOP 1 SoTKVNACCS
                            FROM          t_VNACCS_CapSoToKhai
                            WHERE      SoTK = t .SoToKhai) ELSE t .SoToKhai END AS SoToKhaiVNACCS, t.MaDoanhNghiep, t.MaHaiQuan, t.SoToKhai, t.MaLoaiHinh, t.NamDangKy, t.MaNPL, 
                      hmd.TenHang AS TenNPL, hmd.DVT_ID, dvt.Ten AS TenDVT, hmd.NuocXX_ID, hmd.TriGiaKB, t.Luong, t.Ton, t.ThueXNK, t.ThueXNKTon, t.ThueTTDB, t.ThueVAT, 
                      t.ThueCLGia, t.PhuThu,
                          (SELECT     NgayDangKy
                            FROM          dbo.t_SXXK_ToKhaiMauDich
                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaHaiQuan = t.MaHaiQuan) AND 
                                                   (MaDoanhNghiep = t.MaDoanhNghiep)) AS NgayDangKy,
                          (SELECT     COUNT(*) AS Expr1
                            FROM          dbo.t_KDT_SXXK_NPLNhapTon
                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaNPL = t.MaNPL) AND (TonCuoi < TonDau) AND 
                                                   (LanThanhLy IN
                                                       (SELECT     LanThanhLy
                                                         FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy)) AND (MaDoanhNghiep = t.MaDoanhNghiep)) AS SoLanThanhLy,
                          (SELECT     MAX(LanThanhLy) AS Expr1
                            FROM          dbo.t_KDT_SXXK_NPLNhapTon AS t_KDT_SXXK_NPLNhapTon_2
                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaNPL = t.MaNPL) AND (TonCuoi < TonDau) AND 
                                                   (LanThanhLy IN
                                                       (SELECT     LanThanhLy
                                                         FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy AS t_KDT_SXXK_HoSoThanhLyDangKy_3)) AND (MaDoanhNghiep = t.MaDoanhNghiep)) 
                      AS LanThanhLy,
                          (SELECT     TrangThaiThanhKhoan
                            FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy AS t_KDT_SXXK_HoSoThanhLyDangKy_2
                            WHERE      (LanThanhLy =
                                                       (SELECT     MAX(LanThanhLy) AS Expr1
                                                         FROM          dbo.t_KDT_SXXK_NPLNhapTon AS t_KDT_SXXK_NPLNhapTon_1
                                                         WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaNPL = t.MaNPL) AND 
                                                                                (TonCuoi < TonDau) AND (LanThanhLy IN
                                                                                    (SELECT     LanThanhLy
                                                                                      FROM          dbo.t_KDT_SXXK_HoSoThanhLyDangKy AS t_KDT_SXXK_HoSoThanhLyDangKy_1)) AND 
                                                                                (MaDoanhNghiep = t.MaDoanhNghiep)))) AS TrangThaiThanhKhoan,
                          (SELECT     COUNT(*) AS Expr1
                            FROM          dbo.t_SXXK_ThanhLy_NPLNhapTon AS nt INNER JOIN
                                                   dbo.t_SXXK_HangMauDich AS h ON nt.MaNPL = h.MaPhu AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND 
                                                   nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan AND nt.Luong <> h.SoLuong
                            WHERE      (nt.SoToKhai = t.SoToKhai) AND (nt.MaLoaiHinh = t.MaLoaiHinh) AND (nt.NamDangKy = t.NamDangKy) AND (nt.MaNPL = t.MaNPL) AND 
                                                   (nt.MaHaiQuan = t.MaHaiQuan) AND (nt.MaDoanhNghiep = t.MaDoanhNghiep)) AS SaiSoLuong,
                          (SELECT     h.SoLuong
                            FROM          dbo.t_SXXK_ThanhLy_NPLNhapTon AS nt INNER JOIN
                                                   dbo.t_SXXK_HangMauDich AS h ON nt.MaNPL = h.MaPhu AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND 
                                                   nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan AND nt.Luong <> h.SoLuong
                            WHERE      (nt.SoToKhai = t.SoToKhai) AND (nt.MaLoaiHinh = t.MaLoaiHinh) AND (nt.NamDangKy = t.NamDangKy) AND (nt.MaNPL = t.MaNPL) AND 
                                                   (nt.MaHaiQuan = t.MaHaiQuan) AND (nt.MaDoanhNghiep = t.MaDoanhNghiep)) AS SoLuongDangKy, dbo.f_KiemTra_TienTrinh_ThanhLy(t.MaHaiQuan, 
                      t.MaDoanhNghiep, t.SoToKhai, t.MaLoaiHinh, t.NamDangKy, t.MaNPL, t.Ton) AS TienTrinhChayThanhLy,
                          (SELECT     ThanhLy
                            FROM          dbo.t_SXXK_ToKhaiMauDich AS tk
                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaHaiQuan = t.MaHaiQuan) AND 
                                                   (MaDoanhNghiep = t.MaDoanhNghiep)) AS ThanhLy, CASE WHEN
                          ((SELECT     COUNT(*)
                              FROM         [dbo].t_SXXK_ThanhLy_NPLNhapTon nt INNER JOIN
                                                    dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu AND nt.SoToKhai = h.SoToKhai AND nt.MaLoaiHinh = h.MaLoaiHinh AND 
                                                    nt.NamDangKy = h.NamDangKy AND nt.MaHaiQuan = h.MaHaiQuan
                              WHERE     nt.SoToKhai = t .SoToKhai AND nt.MaLoaiHinh = t .MaLoaiHinh AND nt.NamDangKy = t .NamDangKy AND nt.MaNPL = t .MaNPL AND 
                                                    nt.MaHaiQuan = t .MaHaiQuan AND nt.MaDoanhNghiep = t .MaDoanhNghiep AND nt.Luong <> h.SoLuong) = 0 AND
                          (SELECT     COUNT(*)
                            FROM          [dbo].[t_KDT_SXXK_NPLNhapTon]
                            WHERE      SoToKhai = t .SoToKhai AND MaLoaiHinh = t .MaLoaiHinh AND NamDangKy = t .NamDangKy AND MaNPL = t .MaNPL AND TonCuoi < TonDau AND 
                                                   LanThanhLy IN
                                                       (SELECT     LanThanhLy
                                                         FROM          t_KDT_SXXK_HoSoThanhLyDangKy) AND MaDoanhNghiep = t .MaDoanhNghiep) = 0 AND (t .Luong <> t .Ton)) 
                      THEN 1 ELSE 0 END AS LechTon,
                          (SELECT     TenChuHang
                            FROM          dbo.t_SXXK_ToKhaiMauDich AS t_SXXK_ToKhaiMauDich_2
                            WHERE      (SoToKhai = t.SoToKhai) AND (MaLoaiHinh = t.MaLoaiHinh) AND (NamDangKy = t.NamDangKy) AND (MaHaiQuan = t.MaHaiQuan) AND 
                                                   (MaDoanhNghiep = t.MaDoanhNghiep)) AS TenChuHang
FROM         dbo.t_SXXK_ThanhLy_NPLNhapTon AS t LEFT OUTER JOIN
                      dbo.t_SXXK_HangMauDich AS hmd ON t.MaHaiQuan = hmd.MaHaiQuan AND t.MaNPL = hmd.MaPhu AND t.SoToKhai = hmd.SoToKhai AND 
                      t.MaLoaiHinh = hmd.MaLoaiHinh AND t.NamDangKy = hmd.NamDangKy LEFT OUTER JOIN
                      dbo.t_HaiQuan_DonViTinh AS dvt ON hmd.DVT_ID = dvt.ID
WHERE     ((CAST(t.SoToKhai AS VARCHAR(10)) + t.MaLoaiHinh + CAST(t.NamDangKy AS VARCHAR(4)) + t.MaHaiQuan) IN
                          (SELECT     CAST(SoToKhai AS VARCHAR(10)) + MaLoaiHinh + CAST(NamDangKy AS VARCHAR(4)) + MaHaiQuan AS Expr1
                            FROM          dbo.t_SXXK_ToKhaiMauDich AS t_SXXK_ToKhaiMauDich_1
                            WHERE      (MaLoaiHinh LIKE 'N%')))

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t"
            Begin Extent = 
               Top = 4
               Left = 0
               Bottom = 281
               Right = 164
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "hmd"
            Begin Extent = 
               Top = 0
               Left = 616
               Bottom = 317
               Right = 780
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dvt"
            Begin Extent = 
               Top = 134
               Left = 198
               Bottom = 317
               Right = 350
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_HangTon'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_HangTon'
GO


ALTER TABLE t_KDT_VNACC_HangMauDich
ALTER COLUMN DonGiaHoaDon NUMERIC(24,6)
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_Insert]    Script Date: 05/14/2014 09:22:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_Insert]    Script Date: 05/14/2014 09:22:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Insert]
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaQuanLy varchar(7),
	@TenHang nvarchar(200),
	@ThueSuat numeric(10, 0),
	@ThueSuatTuyetDoi numeric(14, 4),
	@MaDVTTuyetDoi varchar(4),
	@MaTTTuyetDoi varchar(4),
	@NuocXuatXu varchar(2),
	@SoLuong1 numeric(12, 4),
	@DVTLuong1 varchar(4),
	@SoLuong2 numeric(12, 4),
	@DVTLuong2 varchar(4),
	@TriGiaHoaDon numeric(24, 4),
	@DonGiaHoaDon numeric(24, 6),
	@MaTTDonGia varchar(4),
	@DVTDonGia varchar(3),
	@MaBieuThueNK varchar(3),
	@MaHanNgach varchar(1),
	@MaThueNKTheoLuong varchar(10),
	@MaMienGiamThue varchar(5),
	@SoTienGiamThue numeric(20, 4),
	@TriGiaTinhThue numeric(24, 4),
	@MaTTTriGiaTinhThue varchar(3),
	@SoMucKhaiKhoanDC varchar(5),
	@SoTTDongHangTKTNTX varchar(2),
	@SoDMMienThue varchar(12),
	@SoDongDMMienThue varchar(3),
	@MaMienGiam varchar(5),
	@SoTienMienGiam numeric(20, 4),
	@MaTTSoTienMienGiam varchar(3),
	@MaVanBanPhapQuyKhac1 varchar(2),
	@MaVanBanPhapQuyKhac2 varchar(2),
	@MaVanBanPhapQuyKhac3 varchar(2),
	@MaVanBanPhapQuyKhac4 varchar(2),
	@MaVanBanPhapQuyKhac5 varchar(2),
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(16, 4),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(22, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(21, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam1 varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(20, 4),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(16, 4),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(16, 4),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(20, 4),
	@TienBaoHiem_KhoanTien numeric(20, 4),
	@DieuKhoanMienGiam varchar(60),
	@MaHangHoa varchar(48),
	@Templ_1 varchar(250),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich]
(
	[TKMD_ID],
	[MaSoHang],
	[MaQuanLy],
	[TenHang],
	[ThueSuat],
	[ThueSuatTuyetDoi],
	[MaDVTTuyetDoi],
	[MaTTTuyetDoi],
	[NuocXuatXu],
	[SoLuong1],
	[DVTLuong1],
	[SoLuong2],
	[DVTLuong2],
	[TriGiaHoaDon],
	[DonGiaHoaDon],
	[MaTTDonGia],
	[DVTDonGia],
	[MaBieuThueNK],
	[MaHanNgach],
	[MaThueNKTheoLuong],
	[MaMienGiamThue],
	[SoTienGiamThue],
	[TriGiaTinhThue],
	[MaTTTriGiaTinhThue],
	[SoMucKhaiKhoanDC],
	[SoTTDongHangTKTNTX],
	[SoDMMienThue],
	[SoDongDMMienThue],
	[MaMienGiam],
	[SoTienMienGiam],
	[MaTTSoTienMienGiam],
	[MaVanBanPhapQuyKhac1],
	[MaVanBanPhapQuyKhac2],
	[MaVanBanPhapQuyKhac3],
	[MaVanBanPhapQuyKhac4],
	[MaVanBanPhapQuyKhac5],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam1],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam],
	[MaHangHoa],
	[Templ_1]
)
VALUES 
(
	@TKMD_ID,
	@MaSoHang,
	@MaQuanLy,
	@TenHang,
	@ThueSuat,
	@ThueSuatTuyetDoi,
	@MaDVTTuyetDoi,
	@MaTTTuyetDoi,
	@NuocXuatXu,
	@SoLuong1,
	@DVTLuong1,
	@SoLuong2,
	@DVTLuong2,
	@TriGiaHoaDon,
	@DonGiaHoaDon,
	@MaTTDonGia,
	@DVTDonGia,
	@MaBieuThueNK,
	@MaHanNgach,
	@MaThueNKTheoLuong,
	@MaMienGiamThue,
	@SoTienGiamThue,
	@TriGiaTinhThue,
	@MaTTTriGiaTinhThue,
	@SoMucKhaiKhoanDC,
	@SoTTDongHangTKTNTX,
	@SoDMMienThue,
	@SoDongDMMienThue,
	@MaMienGiam,
	@SoTienMienGiam,
	@MaTTSoTienMienGiam,
	@MaVanBanPhapQuyKhac1,
	@MaVanBanPhapQuyKhac2,
	@MaVanBanPhapQuyKhac3,
	@MaVanBanPhapQuyKhac4,
	@MaVanBanPhapQuyKhac5,
	@SoDong,
	@MaPhanLoaiTaiXacNhanGia,
	@TenNoiXuatXu,
	@SoLuongTinhThue,
	@MaDVTDanhThue,
	@DonGiaTinhThue,
	@DV_SL_TrongDonGiaTinhThue,
	@MaTTDonGiaTinhThue,
	@TriGiaTinhThueS,
	@MaTTTriGiaTinhThueS,
	@MaTTSoTienMienGiam1,
	@MaPhanLoaiThueSuatThue,
	@ThueSuatThue,
	@PhanLoaiThueSuatThue,
	@SoTienThue,
	@MaTTSoTienThueXuatKhau,
	@TienLePhi_DonGia,
	@TienBaoHiem_DonGia,
	@TienLePhi_SoLuong,
	@TienLePhi_MaDVSoLuong,
	@TienBaoHiem_SoLuong,
	@TienBaoHiem_MaDVSoLuong,
	@TienLePhi_KhoanTien,
	@TienBaoHiem_KhoanTien,
	@DieuKhoanMienGiam,
	@MaHangHoa,
	@Templ_1
)

SET @ID = SCOPE_IDENTITY()


GO




GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]    Script Date: 05/14/2014 09:22:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]
GO


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]    Script Date: 05/14/2014 09:22:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaQuanLy varchar(7),
	@TenHang nvarchar(200),
	@ThueSuat numeric(10, 0),
	@ThueSuatTuyetDoi numeric(14, 4),
	@MaDVTTuyetDoi varchar(4),
	@MaTTTuyetDoi varchar(4),
	@NuocXuatXu varchar(2),
	@SoLuong1 numeric(12, 4),
	@DVTLuong1 varchar(4),
	@SoLuong2 numeric(12, 4),
	@DVTLuong2 varchar(4),
	@TriGiaHoaDon numeric(24, 4),
	@DonGiaHoaDon numeric(24, 6),
	@MaTTDonGia varchar(4),
	@DVTDonGia varchar(3),
	@MaBieuThueNK varchar(3),
	@MaHanNgach varchar(1),
	@MaThueNKTheoLuong varchar(10),
	@MaMienGiamThue varchar(5),
	@SoTienGiamThue numeric(20, 4),
	@TriGiaTinhThue numeric(24, 4),
	@MaTTTriGiaTinhThue varchar(3),
	@SoMucKhaiKhoanDC varchar(5),
	@SoTTDongHangTKTNTX varchar(2),
	@SoDMMienThue varchar(12),
	@SoDongDMMienThue varchar(3),
	@MaMienGiam varchar(5),
	@SoTienMienGiam numeric(20, 4),
	@MaTTSoTienMienGiam varchar(3),
	@MaVanBanPhapQuyKhac1 varchar(2),
	@MaVanBanPhapQuyKhac2 varchar(2),
	@MaVanBanPhapQuyKhac3 varchar(2),
	@MaVanBanPhapQuyKhac4 varchar(2),
	@MaVanBanPhapQuyKhac5 varchar(2),
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(16, 4),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(22, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(21, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam1 varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(20, 4),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(16, 4),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(16, 4),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(20, 4),
	@TienBaoHiem_KhoanTien numeric(20, 4),
	@DieuKhoanMienGiam varchar(60),
	@MaHangHoa varchar(48),
	@Templ_1 varchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangMauDich] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[MaSoHang] = @MaSoHang,
			[MaQuanLy] = @MaQuanLy,
			[TenHang] = @TenHang,
			[ThueSuat] = @ThueSuat,
			[ThueSuatTuyetDoi] = @ThueSuatTuyetDoi,
			[MaDVTTuyetDoi] = @MaDVTTuyetDoi,
			[MaTTTuyetDoi] = @MaTTTuyetDoi,
			[NuocXuatXu] = @NuocXuatXu,
			[SoLuong1] = @SoLuong1,
			[DVTLuong1] = @DVTLuong1,
			[SoLuong2] = @SoLuong2,
			[DVTLuong2] = @DVTLuong2,
			[TriGiaHoaDon] = @TriGiaHoaDon,
			[DonGiaHoaDon] = @DonGiaHoaDon,
			[MaTTDonGia] = @MaTTDonGia,
			[DVTDonGia] = @DVTDonGia,
			[MaBieuThueNK] = @MaBieuThueNK,
			[MaHanNgach] = @MaHanNgach,
			[MaThueNKTheoLuong] = @MaThueNKTheoLuong,
			[MaMienGiamThue] = @MaMienGiamThue,
			[SoTienGiamThue] = @SoTienGiamThue,
			[TriGiaTinhThue] = @TriGiaTinhThue,
			[MaTTTriGiaTinhThue] = @MaTTTriGiaTinhThue,
			[SoMucKhaiKhoanDC] = @SoMucKhaiKhoanDC,
			[SoTTDongHangTKTNTX] = @SoTTDongHangTKTNTX,
			[SoDMMienThue] = @SoDMMienThue,
			[SoDongDMMienThue] = @SoDongDMMienThue,
			[MaMienGiam] = @MaMienGiam,
			[SoTienMienGiam] = @SoTienMienGiam,
			[MaTTSoTienMienGiam] = @MaTTSoTienMienGiam,
			[MaVanBanPhapQuyKhac1] = @MaVanBanPhapQuyKhac1,
			[MaVanBanPhapQuyKhac2] = @MaVanBanPhapQuyKhac2,
			[MaVanBanPhapQuyKhac3] = @MaVanBanPhapQuyKhac3,
			[MaVanBanPhapQuyKhac4] = @MaVanBanPhapQuyKhac4,
			[MaVanBanPhapQuyKhac5] = @MaVanBanPhapQuyKhac5,
			[SoDong] = @SoDong,
			[MaPhanLoaiTaiXacNhanGia] = @MaPhanLoaiTaiXacNhanGia,
			[TenNoiXuatXu] = @TenNoiXuatXu,
			[SoLuongTinhThue] = @SoLuongTinhThue,
			[MaDVTDanhThue] = @MaDVTDanhThue,
			[DonGiaTinhThue] = @DonGiaTinhThue,
			[DV_SL_TrongDonGiaTinhThue] = @DV_SL_TrongDonGiaTinhThue,
			[MaTTDonGiaTinhThue] = @MaTTDonGiaTinhThue,
			[TriGiaTinhThueS] = @TriGiaTinhThueS,
			[MaTTTriGiaTinhThueS] = @MaTTTriGiaTinhThueS,
			[MaTTSoTienMienGiam1] = @MaTTSoTienMienGiam1,
			[MaPhanLoaiThueSuatThue] = @MaPhanLoaiThueSuatThue,
			[ThueSuatThue] = @ThueSuatThue,
			[PhanLoaiThueSuatThue] = @PhanLoaiThueSuatThue,
			[SoTienThue] = @SoTienThue,
			[MaTTSoTienThueXuatKhau] = @MaTTSoTienThueXuatKhau,
			[TienLePhi_DonGia] = @TienLePhi_DonGia,
			[TienBaoHiem_DonGia] = @TienBaoHiem_DonGia,
			[TienLePhi_SoLuong] = @TienLePhi_SoLuong,
			[TienLePhi_MaDVSoLuong] = @TienLePhi_MaDVSoLuong,
			[TienBaoHiem_SoLuong] = @TienBaoHiem_SoLuong,
			[TienBaoHiem_MaDVSoLuong] = @TienBaoHiem_MaDVSoLuong,
			[TienLePhi_KhoanTien] = @TienLePhi_KhoanTien,
			[TienBaoHiem_KhoanTien] = @TienBaoHiem_KhoanTien,
			[DieuKhoanMienGiam] = @DieuKhoanMienGiam,
			[MaHangHoa] = @MaHangHoa,
			[Templ_1] = @Templ_1
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich]
		(
			[TKMD_ID],
			[MaSoHang],
			[MaQuanLy],
			[TenHang],
			[ThueSuat],
			[ThueSuatTuyetDoi],
			[MaDVTTuyetDoi],
			[MaTTTuyetDoi],
			[NuocXuatXu],
			[SoLuong1],
			[DVTLuong1],
			[SoLuong2],
			[DVTLuong2],
			[TriGiaHoaDon],
			[DonGiaHoaDon],
			[MaTTDonGia],
			[DVTDonGia],
			[MaBieuThueNK],
			[MaHanNgach],
			[MaThueNKTheoLuong],
			[MaMienGiamThue],
			[SoTienGiamThue],
			[TriGiaTinhThue],
			[MaTTTriGiaTinhThue],
			[SoMucKhaiKhoanDC],
			[SoTTDongHangTKTNTX],
			[SoDMMienThue],
			[SoDongDMMienThue],
			[MaMienGiam],
			[SoTienMienGiam],
			[MaTTSoTienMienGiam],
			[MaVanBanPhapQuyKhac1],
			[MaVanBanPhapQuyKhac2],
			[MaVanBanPhapQuyKhac3],
			[MaVanBanPhapQuyKhac4],
			[MaVanBanPhapQuyKhac5],
			[SoDong],
			[MaPhanLoaiTaiXacNhanGia],
			[TenNoiXuatXu],
			[SoLuongTinhThue],
			[MaDVTDanhThue],
			[DonGiaTinhThue],
			[DV_SL_TrongDonGiaTinhThue],
			[MaTTDonGiaTinhThue],
			[TriGiaTinhThueS],
			[MaTTTriGiaTinhThueS],
			[MaTTSoTienMienGiam1],
			[MaPhanLoaiThueSuatThue],
			[ThueSuatThue],
			[PhanLoaiThueSuatThue],
			[SoTienThue],
			[MaTTSoTienThueXuatKhau],
			[TienLePhi_DonGia],
			[TienBaoHiem_DonGia],
			[TienLePhi_SoLuong],
			[TienLePhi_MaDVSoLuong],
			[TienBaoHiem_SoLuong],
			[TienBaoHiem_MaDVSoLuong],
			[TienLePhi_KhoanTien],
			[TienBaoHiem_KhoanTien],
			[DieuKhoanMienGiam],
			[MaHangHoa],
			[Templ_1]
		)
		VALUES 
		(
			@TKMD_ID,
			@MaSoHang,
			@MaQuanLy,
			@TenHang,
			@ThueSuat,
			@ThueSuatTuyetDoi,
			@MaDVTTuyetDoi,
			@MaTTTuyetDoi,
			@NuocXuatXu,
			@SoLuong1,
			@DVTLuong1,
			@SoLuong2,
			@DVTLuong2,
			@TriGiaHoaDon,
			@DonGiaHoaDon,
			@MaTTDonGia,
			@DVTDonGia,
			@MaBieuThueNK,
			@MaHanNgach,
			@MaThueNKTheoLuong,
			@MaMienGiamThue,
			@SoTienGiamThue,
			@TriGiaTinhThue,
			@MaTTTriGiaTinhThue,
			@SoMucKhaiKhoanDC,
			@SoTTDongHangTKTNTX,
			@SoDMMienThue,
			@SoDongDMMienThue,
			@MaMienGiam,
			@SoTienMienGiam,
			@MaTTSoTienMienGiam,
			@MaVanBanPhapQuyKhac1,
			@MaVanBanPhapQuyKhac2,
			@MaVanBanPhapQuyKhac3,
			@MaVanBanPhapQuyKhac4,
			@MaVanBanPhapQuyKhac5,
			@SoDong,
			@MaPhanLoaiTaiXacNhanGia,
			@TenNoiXuatXu,
			@SoLuongTinhThue,
			@MaDVTDanhThue,
			@DonGiaTinhThue,
			@DV_SL_TrongDonGiaTinhThue,
			@MaTTDonGiaTinhThue,
			@TriGiaTinhThueS,
			@MaTTTriGiaTinhThueS,
			@MaTTSoTienMienGiam1,
			@MaPhanLoaiThueSuatThue,
			@ThueSuatThue,
			@PhanLoaiThueSuatThue,
			@SoTienThue,
			@MaTTSoTienThueXuatKhau,
			@TienLePhi_DonGia,
			@TienBaoHiem_DonGia,
			@TienLePhi_SoLuong,
			@TienLePhi_MaDVSoLuong,
			@TienBaoHiem_SoLuong,
			@TienBaoHiem_MaDVSoLuong,
			@TienLePhi_KhoanTien,
			@TienBaoHiem_KhoanTien,
			@DieuKhoanMienGiam,
			@MaHangHoa,
			@Templ_1
		)		
	END

GO



/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_Update]    Script Date: 05/14/2014 09:23:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_Update]    Script Date: 05/14/2014 09:23:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 08, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaQuanLy varchar(7),
	@TenHang nvarchar(200),
	@ThueSuat numeric(10, 0),
	@ThueSuatTuyetDoi numeric(14, 4),
	@MaDVTTuyetDoi varchar(4),
	@MaTTTuyetDoi varchar(4),
	@NuocXuatXu varchar(2),
	@SoLuong1 numeric(12, 4),
	@DVTLuong1 varchar(4),
	@SoLuong2 numeric(12, 4),
	@DVTLuong2 varchar(4),
	@TriGiaHoaDon numeric(24, 4),
	@DonGiaHoaDon numeric(24, 6),
	@MaTTDonGia varchar(4),
	@DVTDonGia varchar(3),
	@MaBieuThueNK varchar(3),
	@MaHanNgach varchar(1),
	@MaThueNKTheoLuong varchar(10),
	@MaMienGiamThue varchar(5),
	@SoTienGiamThue numeric(20, 4),
	@TriGiaTinhThue numeric(24, 4),
	@MaTTTriGiaTinhThue varchar(3),
	@SoMucKhaiKhoanDC varchar(5),
	@SoTTDongHangTKTNTX varchar(2),
	@SoDMMienThue varchar(12),
	@SoDongDMMienThue varchar(3),
	@MaMienGiam varchar(5),
	@SoTienMienGiam numeric(20, 4),
	@MaTTSoTienMienGiam varchar(3),
	@MaVanBanPhapQuyKhac1 varchar(2),
	@MaVanBanPhapQuyKhac2 varchar(2),
	@MaVanBanPhapQuyKhac3 varchar(2),
	@MaVanBanPhapQuyKhac4 varchar(2),
	@MaVanBanPhapQuyKhac5 varchar(2),
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(16, 4),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(22, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(21, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam1 varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(20, 4),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(16, 4),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(16, 4),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(20, 4),
	@TienBaoHiem_KhoanTien numeric(20, 4),
	@DieuKhoanMienGiam varchar(60),
	@MaHangHoa varchar(48),
	@Templ_1 varchar(250)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangMauDich]
SET
	[TKMD_ID] = @TKMD_ID,
	[MaSoHang] = @MaSoHang,
	[MaQuanLy] = @MaQuanLy,
	[TenHang] = @TenHang,
	[ThueSuat] = @ThueSuat,
	[ThueSuatTuyetDoi] = @ThueSuatTuyetDoi,
	[MaDVTTuyetDoi] = @MaDVTTuyetDoi,
	[MaTTTuyetDoi] = @MaTTTuyetDoi,
	[NuocXuatXu] = @NuocXuatXu,
	[SoLuong1] = @SoLuong1,
	[DVTLuong1] = @DVTLuong1,
	[SoLuong2] = @SoLuong2,
	[DVTLuong2] = @DVTLuong2,
	[TriGiaHoaDon] = @TriGiaHoaDon,
	[DonGiaHoaDon] = @DonGiaHoaDon,
	[MaTTDonGia] = @MaTTDonGia,
	[DVTDonGia] = @DVTDonGia,
	[MaBieuThueNK] = @MaBieuThueNK,
	[MaHanNgach] = @MaHanNgach,
	[MaThueNKTheoLuong] = @MaThueNKTheoLuong,
	[MaMienGiamThue] = @MaMienGiamThue,
	[SoTienGiamThue] = @SoTienGiamThue,
	[TriGiaTinhThue] = @TriGiaTinhThue,
	[MaTTTriGiaTinhThue] = @MaTTTriGiaTinhThue,
	[SoMucKhaiKhoanDC] = @SoMucKhaiKhoanDC,
	[SoTTDongHangTKTNTX] = @SoTTDongHangTKTNTX,
	[SoDMMienThue] = @SoDMMienThue,
	[SoDongDMMienThue] = @SoDongDMMienThue,
	[MaMienGiam] = @MaMienGiam,
	[SoTienMienGiam] = @SoTienMienGiam,
	[MaTTSoTienMienGiam] = @MaTTSoTienMienGiam,
	[MaVanBanPhapQuyKhac1] = @MaVanBanPhapQuyKhac1,
	[MaVanBanPhapQuyKhac2] = @MaVanBanPhapQuyKhac2,
	[MaVanBanPhapQuyKhac3] = @MaVanBanPhapQuyKhac3,
	[MaVanBanPhapQuyKhac4] = @MaVanBanPhapQuyKhac4,
	[MaVanBanPhapQuyKhac5] = @MaVanBanPhapQuyKhac5,
	[SoDong] = @SoDong,
	[MaPhanLoaiTaiXacNhanGia] = @MaPhanLoaiTaiXacNhanGia,
	[TenNoiXuatXu] = @TenNoiXuatXu,
	[SoLuongTinhThue] = @SoLuongTinhThue,
	[MaDVTDanhThue] = @MaDVTDanhThue,
	[DonGiaTinhThue] = @DonGiaTinhThue,
	[DV_SL_TrongDonGiaTinhThue] = @DV_SL_TrongDonGiaTinhThue,
	[MaTTDonGiaTinhThue] = @MaTTDonGiaTinhThue,
	[TriGiaTinhThueS] = @TriGiaTinhThueS,
	[MaTTTriGiaTinhThueS] = @MaTTTriGiaTinhThueS,
	[MaTTSoTienMienGiam1] = @MaTTSoTienMienGiam1,
	[MaPhanLoaiThueSuatThue] = @MaPhanLoaiThueSuatThue,
	[ThueSuatThue] = @ThueSuatThue,
	[PhanLoaiThueSuatThue] = @PhanLoaiThueSuatThue,
	[SoTienThue] = @SoTienThue,
	[MaTTSoTienThueXuatKhau] = @MaTTSoTienThueXuatKhau,
	[TienLePhi_DonGia] = @TienLePhi_DonGia,
	[TienBaoHiem_DonGia] = @TienBaoHiem_DonGia,
	[TienLePhi_SoLuong] = @TienLePhi_SoLuong,
	[TienLePhi_MaDVSoLuong] = @TienLePhi_MaDVSoLuong,
	[TienBaoHiem_SoLuong] = @TienBaoHiem_SoLuong,
	[TienBaoHiem_MaDVSoLuong] = @TienBaoHiem_MaDVSoLuong,
	[TienLePhi_KhoanTien] = @TienLePhi_KhoanTien,
	[TienBaoHiem_KhoanTien] = @TienBaoHiem_KhoanTien,
	[DieuKhoanMienGiam] = @DieuKhoanMienGiam,
	[MaHangHoa] = @MaHangHoa,
	[Templ_1] = @Templ_1
WHERE
	[ID] = @ID


GO

 
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '16.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('16.4',GETDATE(), N' update v_hangton, f_KiemTra_TienTrinh_ThanhLy')
END	
