
/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]    Script Date: 3/18/2022 9:56:54 AM ******/
DROP PROCEDURE [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]
GO

/****** Object:  StoredProcedure [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]    Script Date: 3/18/2022 9:56:54 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[p_SXXK_GetDSTKXChuaThanhLyDate]
(
    @MaDoanhNghiep VARCHAR(14),
    @MaHaiQuan CHAR(6),
    @TuNgay DATETIME,
    @DenNgay DATETIME
)
AS
--DEBUG
--SET @MaDoanhNghiep = '3300542464';
--SET @MaHaiQuan = 'P33D';
--SET @TuNgay = '2021-01-01';
--SET @DenNgay = GETDATE();

IF (@MaHaiQuan IS NOT NULL AND @MaHaiQuan <> '')
BEGIN
    SELECT A.MaHaiQuan,
           CASE
               WHEN A.MaLoaiHinh LIKE '%V%' THEN
               (
                   SELECT TOP (1)
                          SoTKVNACCS
                   FROM dbo.t_VNACCS_CapSoToKhai
                   WHERE SoTK = A.SoToKhai
               )
               ELSE
                   A.SoToKhai
           END AS SoToKhaiVNACCS,
           A.SoToKhai,
                       --    Case When  A.MaLoaiHinh like'%V%' THEN (Select Top 1 Ten_VT from t_HaiQuan_LoaiHinhMauDich where ID = A.MaLoaiHinh)
                       --else A.MaLoaiHinh end as MaLoaiHinh ,
           A.MaLoaiHinh,
           A.NamDangKy,
           A.NgayDangKy,
           A.NGAY_THN_THX,
           A.NgayHoanThanh,
                       --minhnd:thêm số hợp đồng
           CASE
               WHEN A.MaLoaiHinh LIKE '%V%' THEN
               (
                   SELECT TOP (1)
                          GhiChu
                   FROM dbo.t_KDT_VNACC_ToKhaiMauDich
                   WHERE SoToKhai =
                   (
                       SELECT TOP (1)
                              SoTKVNACCS
                       FROM dbo.t_VNACCS_CapSoToKhai
                       WHERE SoTK = A.SoToKhai
                   )
               )
               ELSE
           (
               SELECT TOP (1)
                      SoHopDong
               FROM dbo.t_SXXK_ToKhaiMauDich
               WHERE SoToKhai = A.SoToKhai
           )
           END AS GhiChu,
           e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.               
    FROM dbo.t_SXXK_ToKhaiMauDich A
        LEFT JOIN
        (
            SELECT DISTINCT
                   SoToKhai,
                   MaLoaiHinh,
                   NamDangKy,
                   MaHaiQuan
            FROM dbo.t_KDT_SXXK_BKToKhaiXuat
            WHERE MaHaiQuan = @MaHaiQuan
        ) D
            ON A.SoToKhai = D.SoToKhai
               AND A.MaLoaiHinh = D.MaLoaiHinh
               AND A.NamDangKy = D.NamDangKy
               AND A.MaHaiQuan = D.MaHaiQuan
        --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.       
        LEFT JOIN dbo.t_KDT_ToKhaiMauDich e
            ON A.SoToKhai = e.SoToKhai
               AND A.MaLoaiHinh = e.MaLoaiHinh
               AND A.NamDangKy = YEAR(e.NgayDangKy)
    WHERE A.MaDoanhNghiep = @MaDoanhNghiep
          AND A.MaHaiQuan = @MaHaiQuan
          AND (A.MaLoaiHinh LIKE 'X%'
              --OR A.MaLoaiHinh LIKE 'XGC%'
              --OR A.MaLoaiHinh LIKE 'XCX%'
              ) --Hungtq updated 29/03/2013. Bo sung them lay cac to khai loai hinh co ma XCX
          AND A.NgayDangKy
          BETWEEN @TuNgay AND @DenNgay
          AND
          (
              A.NGAY_THN_THX IS NOT NULL
              OR YEAR(A.NGAY_THN_THX) <> 1900
          )
          AND D.SoToKhai IS NULL
          --HungTQ updated 22/06/2011. Khong lay to khai da Huy, cho Huy, sua to khai, cho duyet to khai sua, khong phe duyet.         
          AND CAST(A.SoToKhai AS VARCHAR(50)) + CAST(A.MaLoaiHinh AS VARCHAR(50)) + CAST(A.NamDangKy AS VARCHAR(50)) NOT IN
              (
                  SELECT CAST(SoToKhai AS VARCHAR(50)) + CAST(MaLoaiHinh AS VARCHAR(50))
                         + CAST(YEAR(NgayDangKy) AS VARCHAR(50))
                  FROM dbo.t_KDT_ToKhaiMauDich
                  WHERE MaHaiQuan = @MaHaiQuan
                        AND MaDoanhNghiep = @MaDoanhNghiep
                        AND TrangThaiXuLy <> 1
                        AND SoToKhai <> 0
              )
    ORDER BY A.NgayDangKy,
             A.SoToKhai,
             A.MaLoaiHinh;

END;
ELSE
BEGIN
    SELECT A.MaHaiQuan,
           CASE
               WHEN A.MaLoaiHinh LIKE '%V%' THEN
               (
                   SELECT TOP 1
                          SoTKVNACCS
                   FROM dbo.t_VNACCS_CapSoToKhai
                   WHERE SoTK = A.SoToKhai
               )
               ELSE
                   A.SoToKhai
           END AS SoToKhaiVNACCS,
           A.SoToKhai,
                       --    Case When  A.MaLoaiHinh like'%V%' THEN (Select Top 1 Ten_VT from t_HaiQuan_LoaiHinhMauDich where ID = A.MaLoaiHinh)
                       --else A.MaLoaiHinh end as MaLoaiHinh,
           A.MaLoaiHinh,
           A.NamDangKy,
           A.NgayDangKy,
           A.NGAY_THN_THX,
           A.NgayHoanThanh,
           A.GhiChu,
           e.PhanLuong --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.               
    FROM dbo.t_SXXK_ToKhaiMauDich A
        LEFT JOIN
        (
            SELECT DISTINCT
                   SoToKhai,
                   MaLoaiHinh,
                   NamDangKy,
                   MaHaiQuan
            FROM dbo.t_KDT_SXXK_BKToKhaiXuat
        ) D
            ON A.SoToKhai = D.SoToKhai
               AND A.MaLoaiHinh = D.MaLoaiHinh
               AND A.NamDangKy = D.NamDangKy
               AND A.MaHaiQuan = D.MaHaiQuan
        --Hungtq updated 03/03/2012. Bo sung them thong tin phan luong to khai.       
        LEFT JOIN dbo.t_KDT_ToKhaiMauDich e
            ON A.SoToKhai = e.SoToKhai
               AND A.MaLoaiHinh = e.MaLoaiHinh
               AND A.NamDangKy = YEAR(e.NgayDangKy)
    WHERE A.MaDoanhNghiep = @MaDoanhNghiep
          AND (A.MaLoaiHinh LIKE 'X%'
              --OR A.MaLoaiHinh LIKE 'XGC%'
              --OR A.MaLoaiHinh LIKE 'XCX%'
              ) --Hungtq updated 29/03/2013. Bo sung them lay cac to khai loai hinh co ma XCX
          AND A.NgayDangKy
          BETWEEN @TuNgay AND @DenNgay
          AND
          (
              A.NGAY_THN_THX IS NOT NULL
              OR YEAR(A.NGAY_THN_THX) <> 1900
          )
          AND D.SoToKhai IS NULL
          --HungTQ updated 22/06/2011. Khong lay to khai da Huy, cho Huy, sua to khai, cho duyet to khai sua, khong phe duyet.         
          AND CAST(A.SoToKhai AS VARCHAR(50)) + CAST(A.MaLoaiHinh AS VARCHAR(50)) + CAST(A.NamDangKy AS VARCHAR(50)) NOT IN
              (
                  SELECT CAST(SoToKhai AS VARCHAR(50)) + CAST(MaLoaiHinh AS VARCHAR(50))
                         + CAST(YEAR(NgayDangKy) AS VARCHAR(50))
                  FROM dbo.t_KDT_ToKhaiMauDich
                  WHERE MaDoanhNghiep = @MaDoanhNghiep
                        AND TrangThaiXuLy <> 1
                        AND SoToKhai <> 0
              )
    ORDER BY A.NgayDangKy,
             A.SoToKhai,
             A.MaLoaiHinh;
END;

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '41.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('41.9',GETDATE(), N'Cập nhật proc lay BKTKX nhieu chi cuc')
END	
