﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CA_QuanLyGCN
{
    public partial class QuyetDinhPhatFrm : Form
    {
        public LoiPhat loiphat = new LoiPhat();
        public DoanhNghiep dn;
        public QuyetDinhPhatFrm()
        {
            InitializeComponent();
        }
      

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                getQuyetDinh();
                if (string.IsNullOrEmpty(loiphat.TenCoSoPhat.Trim()))
                {
                    errorProvider1.SetError(txtTenCoSoBiPhat, "Không được để trống");
                    return;
                }
                if (string.IsNullOrEmpty(loiphat.NguoiLapQuyetDinh.Trim()))
                {
                    errorProvider1.SetError(txtNguoiQuyetDinhPhat, "Không được để trống");
                    return;
                }
                if (string.IsNullOrEmpty(loiphat.GhiChu.Trim()))
                {
                    errorProvider1.SetError(txtGhiChu, "Không được để trống");
                }
                //if (cbbDoanhNghiep.SelectedIndex == null)
                //{
                //    errorProvider1.SetError(cbbDoanhNghiep, "Chọn doanh nghiệp");
                //}

                if (loiphat.Id > 0)
                    loiphat.Update();
                else
                    loiphat.Insert();
                //gdList.DataSource = LoiPhat.SelectAll("ID").Tables[0];
                //gdList.Refetch();
                setQuyetDinh();
                MessageBox.Show("Lưu thông tin thành công !");
                this.DialogResult = DialogResult.OK;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show("Lưu thông lỗi:  " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void getQuyetDinh()
        {
            //loiphat.IdDoanhNghiep =System.Convert.ToInt32( cbbDoanhNghiep.SelectedItem.ToString());
            loiphat.IdDoanhNghiep = this.dn.Id;
            loiphat.TenCoSoPhat = txtTenCoSoBiPhat.Text.Trim();
            loiphat.NgayPhat = clcNgayPhat.Value;
            loiphat.GhiChu = txtGhiChu.Text.Trim();
            loiphat.NguoiLapQuyetDinh = txtNguoiQuyetDinhPhat.Text.Trim();
            loiphat.SoGiayPhat = txtSoQuyetDinh.Text.Trim();
        }
        private void setQuyetDinh()
        {
            if (loiphat.Id > 0)
            {
                //cbbDoanhNghiep.SelectedItem = loiphat.IdDoanhNghiep.ToString();
                txtTenCoSoBiPhat.SelectedValue = loiphat.TenCoSoPhat;
                clcNgayPhat.Value = (DateTime)loiphat.NgayPhat;
                txtGhiChu.Text = loiphat.GhiChu;
                txtNguoiQuyetDinhPhat.Text = loiphat.NguoiLapQuyetDinh;
                txtSoQuyetDinh.Text = loiphat.SoGiayPhat;
            }

        }

        private void QuyetDinhPhatFrm_Load(object sender, EventArgs e)
        {
          
            if (dn.Id > 0)
            {
                cbbDoanhNghiep.Text = dn.Mst;
                txtTenDoanhNghiep.Text = dn.Tendn.Trim().ToUpper();
                txtTenCoSoBiPhat.DataSource = ChungNhan.LoadByDoanhNghiepID((long)dn.Id);
                txtTenCoSoBiPhat.ValueMember = "TenCoSo";
                txtTenCoSoBiPhat.DisplayMember = "TenCoSo";
                
            }
            setQuyetDinh();
            //gdList.DataSource = LoiPhat.SelectAll("ID").Tables[0];
        }
      

       

    }
}
