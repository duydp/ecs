/*
Run this script on:

        ecsteam.ECS_TQDT_SXXK_VERSION    -  This database will be modified

to synchronize it with:

        ecsteam.ECS_TQDT_SXXK

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 11/03/2011 6:45:38 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[GetContractExport]'
GO
-- =============================================
-- Author:		Cao Huu Tu
-- Create date: 2011-10-27
-- Description:	get all chung tu hop dong xuat khau
-- =============================================
CREATE PROCEDURE [dbo].[GetContractExport]
	-- Add the parameters for the stored procedure here	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *from dbo.t_CTTT_HopDongXuatKhau
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_CTTT_HopDongXuatKhauDelete]'
GO
-- =============================================
-- Author:		Cao Huu Tu	
-- Create date: 2011-10-27
-- Description:Delete chung tu hop dong xuat khau voi ID cho truoc
-- =============================================
CREATE PROCEDURE [dbo].[p_CTTT_HopDongXuatKhauDelete]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	delete from t_CTTT_HopDongXuatKhau where dbo.t_CTTT_HopDongXuatKhau.Id = @id
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_Insert_CTTT_HopDongXuatKhau]'
GO
-- =============================================
-- Author:		Cao Huu Tu	
-- Create date: 2011-10-27
-- Description: add a chung tu thanh toan hop dong xuat khau
-- =============================================
CREATE PROCEDURE [dbo].[p_Insert_CTTT_HopDongXuatKhau]
	-- Add the parameters for the stored procedure here	
	@numberdelaration bigint,
	@codeLoaiHinh nvarchar(5),
	@dateRegister datetime,
	@numberContract nvarchar(50),
	@dateSignature datetime,
	@value numeric(18,8),
	@note nvarchar(225)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into dbo.t_CTTT_HopDongXuatKhau values(@numberdelaration,@codeLoaiHinh,@dateRegister,@numberContract,@dateSignature,@value,@note)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_Update_CTTT_HopDongXuatKhau]'
GO
-- =============================================
-- Author:		Cao Huu Tu	
-- Create date: 2011-10-27
-- Description: add a chung tu thanh toan hop dong xuat khau
-- =============================================
CREATE PROCEDURE [dbo].[p_Update_CTTT_HopDongXuatKhau]
	-- Add the parameters for the stored procedure here	
	@id int,
	@numberdelaration bigint,
	@codeLoaiHinh nvarchar(5),
	@dateRegister datetime,
	@numberContract nvarchar(50),
	@dateSignature datetime,
	@value numeric(18,8),
	@note nvarchar(225)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Update statements for procedure here
    update dbo.t_CTTT_HopDongXuatKhau set SoToKhai = @numberdelaration,MaLoaiHinh = @codeLoaiHinh,NgayDangKy =@dateRegister,SoHopDong = @numberContract,NgayKy = @dateSignature,TriGia = @value,GhiChu = @note
    where dbo.t_CTTT_HopDongXuatKhau.Id = @id 
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

UPDATE    dbo.t_HaiQuan_Version SET [Version] ='1.2.4', Date = getdate()