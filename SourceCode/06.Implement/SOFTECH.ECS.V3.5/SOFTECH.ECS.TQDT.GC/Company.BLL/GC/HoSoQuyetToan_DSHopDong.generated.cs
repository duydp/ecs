﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.GC
{
	public partial class HoSoQuyetToan_DSHopDong : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public int Master_ID { set; get; }
		public long HopDong_ID { set; get; }
		public string SoHopDong { set; get; }
		public DateTime NgayKy { set; get; }
		public DateTime NgayHetHan { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
        protected static List<HoSoQuyetToan_DSHopDong> ConvertToCollection(IDataReader reader)
		{
			List<HoSoQuyetToan_DSHopDong> collection = new List<HoSoQuyetToan_DSHopDong>();
			while (reader.Read())
			{
				HoSoQuyetToan_DSHopDong entity = new HoSoQuyetToan_DSHopDong();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt32(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKy"))) entity.NgayKy = reader.GetDateTime(reader.GetOrdinal("NgayKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<HoSoQuyetToan_DSHopDong> collection, long id)
        {
            foreach (HoSoQuyetToan_DSHopDong item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }

		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_HoSoQuyetToan_DSHopDong VALUES(@Master_ID, @HopDong_ID, @SoHopDong, @NgayKy, @NgayHetHan)";
            string update = "UPDATE t_KDT_GC_HoSoQuyetToan_DSHopDong SET Master_ID = @Master_ID, HopDong_ID = @HopDong_ID, SoHopDong = @SoHopDong, NgayKy = @NgayKy, NgayHetHan = @NgayHetHan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_HoSoQuyetToan_DSHopDong WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.Int, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKy", SqlDbType.DateTime, "NgayKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.Int, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKy", SqlDbType.DateTime, "NgayKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_HoSoQuyetToan_DSHopDong VALUES(@Master_ID, @HopDong_ID, @SoHopDong, @NgayKy, @NgayHetHan)";
            string update = "UPDATE t_KDT_GC_HoSoQuyetToan_DSHopDong SET Master_ID = @Master_ID, HopDong_ID = @HopDong_ID, SoHopDong = @SoHopDong, NgayKy = @NgayKy, NgayHetHan = @NgayHetHan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_HoSoQuyetToan_DSHopDong WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.Int, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKy", SqlDbType.DateTime, "NgayKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.Int, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKy", SqlDbType.DateTime, "NgayKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static HoSoQuyetToan_DSHopDong Load(long id)
		{
			const string spName = "[dbo].[p_HoSoQuyetToan_DSHopDong_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<HoSoQuyetToan_DSHopDong> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		//---------------------------------------------------------------------------------------------
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_HoSoQuyetToan_DSHopDong_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_HoSoQuyetToan_DSHopDong_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_HoSoQuyetToan_DSHopDong_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_HoSoQuyetToan_DSHopDong_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertHoSoQuyetToan_DSHopDong(int master_ID, long hopDong_ID, string soHopDong, DateTime ngayKy, DateTime ngayHetHan)
		{
			HoSoQuyetToan_DSHopDong entity = new HoSoQuyetToan_DSHopDong();	
			entity.Master_ID = master_ID;
			entity.HopDong_ID = hopDong_ID;
			entity.SoHopDong = soHopDong;
			entity.NgayKy = ngayKy;
			entity.NgayHetHan = ngayHetHan;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_HoSoQuyetToan_DSHopDong_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.Int, Master_ID);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy.Year <= 1753 ? DBNull.Value : (object) NgayKy);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateHoSoQuyetToan_DSHopDong(long id, int master_ID, long hopDong_ID, string soHopDong, DateTime ngayKy, DateTime ngayHetHan)
		{
			HoSoQuyetToan_DSHopDong entity = new HoSoQuyetToan_DSHopDong();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.HopDong_ID = hopDong_ID;
			entity.SoHopDong = soHopDong;
			entity.NgayKy = ngayKy;
			entity.NgayHetHan = ngayHetHan;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_HoSoQuyetToan_DSHopDong_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.Int, Master_ID);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy.Year <= 1753 ? DBNull.Value : (object) NgayKy);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateHoSoQuyetToan_DSHopDong(long id, int master_ID, long hopDong_ID, string soHopDong, DateTime ngayKy, DateTime ngayHetHan)
		{
			HoSoQuyetToan_DSHopDong entity = new HoSoQuyetToan_DSHopDong();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.HopDong_ID = hopDong_ID;
			entity.SoHopDong = soHopDong;
			entity.NgayKy = ngayKy;
			entity.NgayHetHan = ngayHetHan;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_HoSoQuyetToan_DSHopDong_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.Int, Master_ID);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy.Year <= 1753 ? DBNull.Value : (object) NgayKy);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteHoSoQuyetToan_DSHopDong(long id)
		{
			HoSoQuyetToan_DSHopDong entity = new HoSoQuyetToan_DSHopDong();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_HoSoQuyetToan_DSHopDong_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_HoSoQuyetToan_DSHopDong_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}