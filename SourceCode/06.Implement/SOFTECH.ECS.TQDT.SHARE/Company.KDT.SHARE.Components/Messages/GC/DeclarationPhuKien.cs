﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
   public class DeclarationPhuKien
    {
       /// <summary>
       /// Sản phẩm
       /// </summary>
        [XmlElement("Product")]
       public Product Product { get; set; }
       /// <summary>
       /// Nguyên phụ liệu
       /// </summary>
       [XmlElement("Material")]
        public Product Material { get; set; }
       /// <summary>
       /// Thiết bị
       /// </summary>
       [XmlElement("Equipment")]
       public Equipment Equipment { get; set; }
       /// <summary>
       /// Hàng mẫu
       /// </summary>
       [XmlElement("SampleProduct")]
       public Equipment SampleProduct { get; set; }
    }
}
