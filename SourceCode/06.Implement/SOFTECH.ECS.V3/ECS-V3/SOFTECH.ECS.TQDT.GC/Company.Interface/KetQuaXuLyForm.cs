﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class KetQuaXuLyForm : BaseForm
    {
        public long ItemID { set; get; }
        public string GuidStr { get; set; }
        public KetQuaXuLyForm()
        {
            InitializeComponent();
        }

        private void KetQuaXuLyForm_Load(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(GuidStr))
            dgList.DataSource = Company.KDT.SHARE.Components.KetQuaXuLy.SelectCollectionBy_ItemID(this.ItemID);
            else
                dgList.DataSource = Company.KDT.SHARE.Components.KetQuaXuLy.SelectCollectionBy_ReferenceID(new Guid(this.GuidStr));
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                Company.KDT.SHARE.Components.KetQuaXuLy kqxl = Company.KDT.SHARE.Components.KetQuaXuLy.Load(id);
                this.ShowMessageTQDT("ID: [" + kqxl.ReferenceID + "]\r\n" + kqxl.NoiDung, false);
            }
        }
    }
}
