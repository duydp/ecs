﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseExportValueForm : BaseFormHaveGuidPanel
    {
        public T_KHOKETOAN_PHIEUXNKHO PhieuXNKho;
        public T_KHOKETOAN_PHIEUXNKHO_HANGHOA PhieuXNKHang;
        public HopDong HD;
        public bool isAdd = true;
        public WareHouseExportValueForm()
        {
            InitializeComponent();
            this._DonViTinh = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.SelectAll().Tables[0];
            txtDVT.DataSource = this._DonViTinh;
            txtDVT.DisplayMember = "Ten";
            txtDVT.ValueMember = "ID";
            txtDVT.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);

            txtDonViTinh.DataSource = this._DonViTinh;
            txtDonViTinh.DisplayMember = "Ten";
            txtDonViTinh.ValueMember = "ID";
            txtDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);

            cbbMaKho.DataSource = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
            cbbMaKho.DisplayMember = "MAKHO";
            cbbMaKho.ValueMember = "MAKHO";

            cbbTKCo.DataSource = T_KHOKETOAN_TAIKHOAN.SelectCollectionAll();
            cbbTKCo.DisplayMember = "MATK";
            cbbTKCo.ValueMember = "MATK";

            cbbTKNo.DataSource = T_KHOKETOAN_TAIKHOAN.SelectCollectionAll();
            cbbTKNo.DisplayMember = "MATK";
            cbbTKNo.ValueMember = "MATK";
        }

        private void WareHouseExportValueForm_Load(object sender, EventArgs e)
        {
            cbbMaKho.SelectedIndex = 0;
            cbbTKNo.Value = "155";
            cbbTKCo.Value = "155";
            if(PhieuXNKHang !=null)
                SetHangHoa();
            BindData();
        }
        private void BindData()
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = PhieuXNKho.HangCollection;
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtMaHang, errorProvider, " Mã hàng hóa kho toán ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenHang, errorProvider, " Tên hàng hóa kho kế toán ");
                isValid &= ValidateControl.ValidateNull(txtDVT, errorProvider, " Đơn vị tính ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHangHoa, errorProvider, " Mã hàng hóa ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenHangHoa, errorProvider, " Tên hàng hóa ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDVT, errorProvider, " Đơn vị tính ");

                isValid &= ValidateControl.ValidateZero(txtSoLuong, errorProvider, " Số lượng ");
                isValid &= ValidateControl.ValidateNull(txtDonGiaNT, errorProvider, " Đơn giá nguyên tệ ", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtTriGiaNT, errorProvider, " Trị giá nguyên tệ ");
                isValid &= ValidateControl.ValidateZero(txtDonGiaTT, errorProvider, " Đơn giá tính thuế ");
                isValid &= ValidateControl.ValidateZero(txtTriGiaVND, errorProvider, " Trị giá VNĐ ");
                isValid &= ValidateControl.ValidateChoose(cbbTKNo, errorProvider, " Tài khoản nợ ");
                isValid &= ValidateControl.ValidateChoose(cbbTKCo, errorProvider, " Tài khoản có ");
                isValid &= ValidateControl.ValidateChoose(cbbMaKho, errorProvider, " Mã kho ");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void GetHangHoa()
        {
            try
            {
                if (PhieuXNKHang == null)
                    PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                PhieuXNKHang.LOAIHANG = PhieuXNKho.LOAIHANGHOA;
                PhieuXNKHang.MAHANG = txtMaHang.Text.ToString();
                PhieuXNKHang.TENHANG = txtTenHang.Text.ToString();
                PhieuXNKHang.DVT = txtDonViTinh.Text.ToString();
                PhieuXNKHang.MAHANGMAP = txtMaHangHoa.Text.ToString();
                PhieuXNKHang.TENHANGMAP = txtTenHangHoa.Text.ToString();
                PhieuXNKHang.DVTMAP = txtDVT.Text.ToString();
                PhieuXNKHang.SOLUONG = Convert.ToDecimal(txtSoLuong.Text.ToString());
                PhieuXNKHang.DONGIA = Convert.ToDecimal(txtDonGiaTT.Text.ToString());
                PhieuXNKHang.DONGIANT = Convert.ToDecimal(txtDonGiaNT.Text.ToString());
                PhieuXNKHang.TRIGIANT = Convert.ToDecimal(txtTriGiaNT.Text.ToString());
                PhieuXNKHang.THANHTIEN = Convert.ToDecimal(txtTriGiaVND.Text.ToString());
                PhieuXNKHang.TRIGIANTSAUPB = PhieuXNKHang.THANHTIEN;
                PhieuXNKHang.THUEXNK = Convert.ToDecimal(txtThueXNK.Text.ToString());
                PhieuXNKHang.THUETTDB = Convert.ToDecimal(txtThueTTDB.Text.ToString());
                PhieuXNKHang.THUEKHAC = Convert.ToDecimal(txtThueThuKhac.Text.ToString());
                PhieuXNKHang.MAKHO = cbbMaKho.Value.ToString();
                PhieuXNKHang.TAIKHOANNO = cbbTKNo.Value.ToString();
                PhieuXNKHang.TAIKHOANCO = cbbTKCo.Value.ToString();
                PhieuXNKHang.TYLEQD = Convert.ToDecimal(txtTLQD.Text.ToString());
                PhieuXNKHang.SOLUONGQD = Convert.ToDecimal(txtSoLuongQD.Text.ToString());
                PhieuXNKHang.GHICHU = txtGhiChu.Text.ToString();
                if(isAdd)
                    PhieuXNKho.HangCollection.Add(PhieuXNKHang);                
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetHangHoa()
        {
            try
            {
                txtMaHang.Text = PhieuXNKHang.MAHANG;
                txtTenHang.Text = PhieuXNKHang.TENHANG;
                txtDonViTinh.Text = PhieuXNKHang.DVT;
                txtMaHangHoa.Text = PhieuXNKHang.MAHANGMAP;
                txtTenHangHoa.Text = PhieuXNKHang.TENHANGMAP;
                txtDVT.Text = PhieuXNKHang.DVTMAP;
                txtSoLuong.Text = PhieuXNKHang.SOLUONG.ToString();
                txtDonGiaNT.Text = PhieuXNKHang.DONGIANT.ToString();
                txtDonGiaTT.Text = PhieuXNKHang.DONGIA.ToString();
                txtTriGiaNT.Text = PhieuXNKHang.TRIGIANT.ToString();
                txtTriGiaVND.Text = PhieuXNKHang.THANHTIEN.ToString();
                txtThueXNK.Text = PhieuXNKHang.THUEXNK.ToString();
                txtThueTTDB.Text = PhieuXNKHang.THUETTDB.ToString();
                txtThueThuKhac.Text = PhieuXNKHang.THUEKHAC.ToString();
                cbbMaKho.Value = PhieuXNKHang.MAKHO;
                cbbTKNo.Value = PhieuXNKHang.TAIKHOANNO;
                cbbTKCo.Value = PhieuXNKHang.TAIKHOANCO;
                txtTLQD.Text = PhieuXNKHang.TYLEQD.ToString();
                txtSoLuongQD.Text = PhieuXNKHang.SOLUONGQD.ToString();
                txtGhiChu.Text = PhieuXNKHang.GHICHU;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetHangHoa();
                BindData();
                txtMaHangHoa.Text = String.Empty;
                txtTenHangHoa.Text = String.Empty;
                txtDVT.SelectedValue = String.Empty;
                txtSoLuong.Text = String.Empty;
                txtDonGiaNT.Text = String.Empty;
                txtDonGiaTT.Text = String.Empty;
                txtTriGiaNT.Text = String.Empty;
                txtTriGiaVND.Text = String.Empty;
                txtThueThuKhac.Text = String.Empty;
                txtThueTTDB.Text = String.Empty;
                txtThueXNK.Text = String.Empty;
                txtTLQD.Text = String.Empty;
                txtSoLuongQD.Text = String.Empty;
                txtGhiChu.Text = String.Empty;
                PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                isAdd = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> ItemColl = new List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((T_KHOKETOAN_PHIEUXNKHO_HANGHOA)i.GetRow().DataRow);
                        }

                    }
                    foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        PhieuXNKho.HangCollection.Remove(item);
                    }
                    int k = 1;
                    foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA item in PhieuXNKho.HangCollection)
                    {
                        item.STT = k;
                        k++;
                    }
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    PhieuXNKHang = new T_KHOKETOAN_PHIEUXNKHO_HANGHOA();
                    PhieuXNKHang = (T_KHOKETOAN_PHIEUXNKHO_HANGHOA)e.Row.DataRow;
                    SetHangHoa();
                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (PhieuXNKho.LOAIHANGHOA=="N")
                {
                    WareHouseNPLRegistedForm f = new WareHouseNPLRegistedForm();
                    f.HD = HD;                    
                    f.ShowDialog();
                    if (f.NPLSelected != null)
                    {
                        txtMaHang.Text = f.NPLSelected.MANPL.Trim();
                        txtTenHang.Text = f.NPLSelected.TENNPL.Trim();
                        txtDonViTinh.Text = f.NPLSelected.DVT.ToString().Trim();
                    }
                }
                else if (PhieuXNKho.LOAIHANGHOA == "S")
                {
                    WareHouseSPRegistedForm f = new WareHouseSPRegistedForm();
                    f.HD = HD;
                    f.ShowDialog();
                    if (f.SPSelected != null)
                    {
                        txtMaHang.Text = f.SPSelected.MASP;
                        txtTenHang.Text = f.SPSelected.TENSP.Trim();
                        txtDonViTinh.Text = f.SPSelected.DVT;
                    }
                }
                else if (PhieuXNKho.LOAIHANGHOA == "T")
                {

                }
                else if (PhieuXNKho.LOAIHANGHOA == "H")
                {
                    
                }else
                {

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaHangHoa_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (PhieuXNKho.LOAIHANGHOA == "N")
                {
                    WareHouseNPLRegistedForm f = new WareHouseNPLRegistedForm();
                    f.HD = HD;
                    f.NPLMap = txtMaHang.Text.ToString();
                    f.ShowDialog();
                    if (f.NPLMapSelected != null)
                    {
                        txtMaHangHoa.Text = f.NPLMapSelected.MANPLMAP.Trim();
                        txtTenHangHoa.Text = f.NPLMapSelected.TENNPLMAP.Trim();
                        txtDVT.Text = f.NPLMapSelected.DVT;
                        txtTLQD.Text = f.NPLMapSelected.TYLEQD.ToString();
                    }
                }
                else if (PhieuXNKho.LOAIHANGHOA == "S")
                {
                    WareHouseSPRegistedForm f = new WareHouseSPRegistedForm();
                    f.HD = HD;
                    f.SPMap = txtMaHang.Text.ToString();
                    f.ShowDialog();
                    if (f.SPMapSelected != null)
                    {
                        txtMaHangHoa.Text = f.SPMapSelected.MASPMAP;
                        txtTenHangHoa.Text = f.SPMapSelected.TENSPMAP.Trim();
                        txtDVT.Text = f.SPMapSelected.DVT;
                    }
                }
                else if (PhieuXNKho.LOAIHANGHOA == "T")
                {

                }
                else if (PhieuXNKho.LOAIHANGHOA == "H")
                {

                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtSoLuong_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal TriGiaNT = Convert.ToDecimal(txtSoLuong.Text) * Convert.ToDecimal(txtDonGiaNT.Text);
                txtTriGiaNT.Value = TriGiaNT;

                decimal TriGiaTT = Convert.ToDecimal(txtSoLuong.Text) * Convert.ToDecimal(txtDonGiaTT.Text) + Convert.ToDecimal(txtThueXNK.Text) + Convert.ToDecimal(txtThueTTDB.Text) + Convert.ToDecimal(txtThueThuKhac.Text);
                txtTriGiaVND.Value = TriGiaTT;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtDonGiaNT_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal TriGiaNT = Convert.ToDecimal(txtSoLuong.Text) * Convert.ToDecimal(txtDonGiaNT.Text);
                txtTriGiaNT.Value = TriGiaNT;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtDonGiaTT_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal TriGiaTT = Convert.ToDecimal(txtSoLuong.Text) * Convert.ToDecimal(txtDonGiaTT.Text) + Convert.ToDecimal(txtThueXNK.Text) + Convert.ToDecimal(txtThueTTDB.Text) + Convert.ToDecimal(txtThueThuKhac.Text);
                txtTriGiaVND.Value = TriGiaTT;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtThueXNK_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal TriGiaTT = Convert.ToDecimal(txtSoLuong.Text) * Convert.ToDecimal(txtDonGiaTT.Text) + Convert.ToDecimal(txtThueXNK.Text) + Convert.ToDecimal(txtThueTTDB.Text) + Convert.ToDecimal(txtThueThuKhac.Text);
                txtTriGiaVND.Value = TriGiaTT;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtThueThuKhac_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal TriGiaTT = Convert.ToDecimal(txtSoLuong.Text) * Convert.ToDecimal(txtDonGiaTT.Text) + Convert.ToDecimal(txtThueXNK.Text) + Convert.ToDecimal(txtThueTTDB.Text) + Convert.ToDecimal(txtThueThuKhac.Text);
                txtTriGiaVND.Value = TriGiaTT;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtThueTTDB_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal TriGiaTT = Convert.ToDecimal(txtSoLuong.Text) * Convert.ToDecimal(txtDonGiaTT.Text) + Convert.ToDecimal(txtThueXNK.Text) + Convert.ToDecimal(txtThueTTDB.Text) + Convert.ToDecimal(txtThueThuKhac.Text);
                txtTriGiaVND.Value = TriGiaTT;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
