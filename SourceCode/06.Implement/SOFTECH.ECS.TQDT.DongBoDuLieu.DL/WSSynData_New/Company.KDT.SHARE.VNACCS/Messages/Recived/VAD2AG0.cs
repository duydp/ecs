﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAD2AG0 : BasicVNACC
    {
        public List<VAD1AG_HANG> HangMD { get; set; }
        public void LoadVAD2AG0(string strResult)
        {
            try
            {
                this.GetObject<VAD2AG0>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAD2AG0, false, VAD2AG0.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAD2AG0.TongSoByte);
                while (true)
                {
                    VAD1AG_HANG vad1agHang = new VAD1AG_HANG();
                    vad1agHang.GetObject<VAD1AG_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAD1AG_HANG", false, VAD1AG_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAD1AG_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAD1AG_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAD1AG_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAD1AG_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }

    }
}
