using System;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Controls
{
    public partial class CuaKhauHControl : UserControl
    {
        public CuaKhauHControl()
        {
            this.InitializeComponent();
        }

        public string Ma
        {
            set
            {
                this.txtID.Text = value;
                this.cbList.Value = this.txtID.Text.PadRight(4);
            }
            get { return this.txtID.Text; }
        }
        public string Ten
        {
            get { return this.cbList.Text.Trim(); }
            set { this.cbList.Text = value; }
        }
        public bool ReadOnly
        {
            set
            {
                this.txtID.ReadOnly = value;
                this.cbList.ReadOnly = value;
            }
            get { return this.cbList.ReadOnly; }
        }
        public Janus.Windows.Common.VisualStyleManager VisualStyleManager
        {
            set
            {
                this.txtID.VisualStyleManager = value;
                this.cbList.VisualStyleManager = value;
            }
            get
            {
                return this.txtID.VisualStyleManager;
            }
        }
        public string ErrorMessage
        {
            get { return this.rfvTen.ErrorMessage; }
            set { this.rfvTen.ErrorMessage = value; }
        }

        //public Janus.Windows.GridEX.ComboStyle DropDownStyle
        //{
        //    get { return cbList.ComboStyle; }
        //    set { cbList.ComboStyle = value; }
        //}

        private bool _isLoadData = true;

        public bool IsLoadData
        {
            get { return _isLoadData; }
            set { _isLoadData = value; }
        }

        public event ValueChangedEventHandler ValueChanged;
        public delegate void ValueChangedEventHandler(object sender, EventArgs e);

        public void loadData()
        {
            if (IsLoadData)
            {
                if (this.txtID.Text.Trim().Length == 0) this.txtID.Text = GlobalSettings.CUA_KHAU;
                DataTable dt = CuaKhau.SelectAll().Tables[0];
                dtCuaKhau.Rows.Clear();
                foreach (DataRow row in dt.Rows)
                {
                    this.dtCuaKhau.ImportRow(row);
                }
                this.cbList.Value = this.txtID.Text.PadRight(4);
            }
            else
            {
                cbList.ComboStyle = Janus.Windows.GridEX.ComboStyle.Combo;
                txtID.Text = "";
                cbList.Value = cbList.Text = "";
            }
        }

        private void txtMaLoaiHinh_Leave(object sender, EventArgs e)
        {
            if (IsLoadData)
            {
                this.txtID.Text = this.txtID.Text.Trim();
                this.cbList.Value = this.txtID.Text.PadRight(4);
            }
        }

        private void CuaKhauControl_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                this.loadData();
            }
        }

        private void cbList_ValueChanged(object sender, EventArgs e)
        {
            if (IsLoadData)
            {
                this.txtID.Text = this.cbList.Value.ToString().Trim();
                if (this.ValueChanged != null) this.ValueChanged(sender, e);
            }
        }
    }
}