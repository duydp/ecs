﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.DanhMucChuan;
using System.Windows.Forms;
using System.Data;
using System.IO;
using Company.KDT.SHARE.Components;
using System.Xml;
using Logger;
using Company.KDT.SHARE.Components.Utils;
using System.Drawing;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface
{
    public class Globals
    {
        #region MESSAGE
        private static Company.Controls.KDTMessageBoxControl _KDTMsgBox;
        private static Company.Controls.MessageBoxControl _MsgBox;

        public static string ShowMessageTQDT(string messageHQ, string messageContent, bool showYesNoButton)
        {
            _KDTMsgBox = new Company.Controls.KDTMessageBoxControl();
            _KDTMsgBox.ShowYesNoButton = showYesNoButton;
            _KDTMsgBox.HQMessageString = messageHQ;
            _KDTMsgBox.MessageString = messageContent;
            _KDTMsgBox.ShowDialog();
            string st = _KDTMsgBox.ReturnValue;
            _KDTMsgBox.Dispose();
            return st;
        }
        public static string ShowMessageTQDT(string messageContent, bool showYesNoButton)
        {
            string messageHQ = "Thông báo trả về từ hệ thống Hải quan";
            _KDTMsgBox = new Company.Controls.KDTMessageBoxControl();
            _KDTMsgBox.ShowYesNoButton = showYesNoButton;
            _KDTMsgBox.HQMessageString = messageHQ;
            _KDTMsgBox.MessageString = messageContent;
            _KDTMsgBox.ShowDialog();
            string st = _KDTMsgBox.ReturnValue;
            _KDTMsgBox.Dispose();
            return st;
        }
        public static string ShowMessage(string message, bool showYesNoButton)
        {
            _MsgBox = new Company.Controls.MessageBoxControl();
            _MsgBox.ShowYesNoButton = showYesNoButton;
            _MsgBox.MessageString = message;
            _MsgBox.ShowDialog();
            string st = _MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }

        #endregion

        #region DANH MUC DOI TAC
        /// <summary>
        /// Tra ve 1 Object Doi tac
        /// </summary>
        /// <returns></returns>
        public static DoiTac GetMaDonViObject()
        {
            DoiTac dt = new DoiTac();

            try
            {
                DonViDoiTacForm f = new DonViDoiTacForm();
                f.isBrower = true;
                f.ShowDialog();

                if (f.doiTac != null && f.doiTac.TenCongTy != "")
                {
                    dt = f.doiTac;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return dt;
        }

        /// <summary>
        /// Tra ve chuoi: ten va dia chi doi tac.
        /// </summary>
        /// <returns></returns>
        public static string GetMaDonViString()
        {
            try
            {
                DonViDoiTacForm f = new DonViDoiTacForm();
                f.isBrower = true;
                f.ShowDialog();
                if (f.doiTac != null && f.doiTac.TenCongTy != "")
                {
                    return f.doiTac.TenCongTy + ". " + f.doiTac.DiaChi;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return "";
        }
        #endregion

        #region VALIDATE

        /// <summary>
        /// Kiểm tra thông tin rỗng.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateNull(Company.Interface.Controls.DonViHaiQuanControl control, ErrorProvider err, string fieldName)
        {
            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' không được để trống.";

            err.SetError(control, "");

            if (control.Ma.Trim().Length == 0)
            {
                isValid = false;
                err.SetError(control, msgErr);
                control.Focus();
            }

            return isValid;
        }

        /// <summary>
        /// Kiểm tra thông tin rỗng.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateNull(Janus.Windows.GridEX.EditControls.EditBox control, ErrorProvider err, string fieldName)
        {
            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' không được để trống.";

            err.SetError(control, "");

            if (control.Text.Trim().Length == 0)
            {
                isValid = false;
                err.SetError(control, msgErr);
                control.Focus();
            }

            return isValid;
        }

        /// <summary>
        /// Kiểm tra thông tin rỗng.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateNull(Janus.Windows.GridEX.EditControls.NumericEditBox control, ErrorProvider err, string fieldName)
        {
            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' phải > 0.";

            err.SetError(control, "");

            if (Convert.ToInt32(control.Value) == 0)
            {
                isValid = false;
                err.SetError(control, msgErr);
            }

            return isValid;
        }

        /// <summary>
        /// Kiểm tra thông tin rỗng.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateNull(Janus.Windows.EditControls.UIComboBox control, ErrorProvider err, string fieldName)
        {
            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' không được để trống.";

            err.SetError(control, "");

            if (control.Text.Trim().Length == 0)
            {
                isValid = false;
                err.SetError(control, msgErr);
            }

            return isValid;
        }

        /// <summary>
        /// Kiểm tra thông tin rỗng.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateNull(Janus.Windows.CalendarCombo.CalendarCombo control, ErrorProvider err, string fieldName)
        {
            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' không được để trống.";

            err.SetError(control, "");

            if (control.Text.Trim().Length == 0)
            {
                isValid = false;
                err.SetError(control, msgErr);
            }

            return isValid;
        }

        /// <summary>
        /// Kiểm tra độ dài ký tự cho phép.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateLength(Janus.Windows.EditControls.UIComboBox control, int lengthLimit, ErrorProvider err, string fieldName)
        {
            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' vượt quá ký tự cho phép, phải <= '" + lengthLimit + "' ký tự.";

            err.SetError(control, "");

            if (control.SelectedValue != null)
                isValid = ValidateLength(control.SelectedValue.ToString().Trim(), lengthLimit);
            else
                isValid = false;

            if (isValid == false)
            {
                err.SetError(control, msgErr);
            }

            return isValid;
        }
        public static bool ValidateDate(Janus.Windows.CalendarCombo.CalendarCombo control, ErrorProvider err, string fieldName)
        {
            return ValidateDate(control, new DateTime(1900, 1, 1), new DateTime(9999, 12, 31), err, fieldName);
        }
        public static bool ValidateDate(Janus.Windows.CalendarCombo.CalendarCombo control, DateTime fromDate, DateTime toDate, ErrorProvider err, string fieldName)
        {
            bool isValid = true;
            DateTime dSelect = Convert.ToDateTime(control.Value);
            string msgErr = string.Empty;
            err.SetError(control, "");

            if (DateTime.Compare(dSelect, fromDate) < 0)
            {
                msgErr = "Ngày \"" + fieldName + "\" không cho phép nhỏ hơn " + fromDate.ToString("dd-MM-yyyy");
                isValid = false;
            }
            else if (DateTime.Compare(dSelect, toDate) > 0)
            {
                msgErr = "Ngày \"" + fieldName + "\" không được phép lớn hơn " + toDate.ToString("dd-MM-yyyy");
                isValid = false;
            }

            if (!isValid)
            {
                err.SetError(control, msgErr);
            }

            return isValid;
        }

        /// <summary>
        /// Kiểm tra độ dài ký tự cho phép.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateLength(Janus.Windows.GridEX.EditControls.EditBox control, int lengthLimit, ErrorProvider err, string fieldName)
        {
            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' vượt quá ký tự cho phép, phải <= '" + lengthLimit + "' ký tự.";

            err.SetError(control, "");

            isValid = ValidateLength(control.Text, lengthLimit);

            if (isValid == false)
            {
                err.SetError(control, msgErr);
            }

            return isValid;
        }

        /// <summary>
        /// Kiểm tra độ dài ký tự cho phép.
        /// </summary>
        /// <param name="valueString"></param>
        /// <param name="lengthLimit"></param>
        /// <returns></returns>
        private static bool ValidateLength(string valueString, int lengthLimit)
        {
            return valueString.Length <= lengthLimit;
        }

        #endregion

        #region Ket qua xu ly
        public static void KetQuaXuLy(Company.GC.BLL.KDT.ToKhaiMauDich tkmd)
        {
            KetQuaXuLyForm form = new KetQuaXuLyForm();
            form.ItemID = tkmd.ID;
            form.ShowDialog();
        }
        #endregion

        #region Ket qua xu ly bo sung

        public static void ShowKetQuaXuly(long itemID)
        {
            KetQuaXuLyBoSungForm form = new KetQuaXuLyBoSungForm();
            form.ItemID = itemID;
            form.ShowDialog();
        }
        public static void ShowKetQuaXuLyBoSung(string guidStr)
        {
            KetQuaXuLyBoSungForm form = new KetQuaXuLyBoSungForm();
            form.refId = guidStr;
            form.ShowDialog();

        }
        #endregion

        #region VALUE LIST

        private static System.Data.DataTable dtHS;
        private static System.Data.DataTable dtNguyenTe;
        private static System.Data.DataTable dtDonViTinh;
        private static System.Data.DataTable dtNuoc;

        public static Janus.Windows.GridEX.GridEXValueListItemCollection FillHSValueList(Janus.Windows.GridEX.GridEXColumn col)
        {
            Janus.Windows.GridEX.GridEXValueListItemCollection valueList = new Janus.Windows.GridEX.GridEXValueListItemCollection();

            try
            {
                col.EditType = Janus.Windows.GridEX.EditType.Combo;
                col.HasValueList = true;

                valueList = col.ValueList;

                if (dtHS == null)
                    dtHS = Company.KDT.SHARE.Components.DuLieuChuan.MaHS.SelectAll();

                System.Data.DataView view = dtHS.DefaultView;

                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["HS10So"], (string)row["HS10So"]));
                }
            }
            catch (Exception ex) { }

            return valueList;
        }

        public static Janus.Windows.GridEX.GridEXValueListItemCollection FillNguyenTeValueList(Janus.Windows.GridEX.GridEXColumn col)
        {
            Janus.Windows.GridEX.GridEXValueListItemCollection valueList = new Janus.Windows.GridEX.GridEXValueListItemCollection();

            try
            {
                col.EditType = Janus.Windows.GridEX.EditType.Combo;
                col.HasValueList = true;
                valueList = col.ValueList;

                if (dtNguyenTe == null)
                    dtNguyenTe = Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe.SelectAll().Tables[0];

                System.Data.DataView view = dtNguyenTe.DefaultView;

                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["ID"], (string)row["Ten"]));
                }
            }
            catch (Exception ex) { }

            return valueList;
        }

        public static Janus.Windows.GridEX.GridEXValueListItemCollection FillDonViTinhValueList(Janus.Windows.GridEX.GridEXColumn col)
        {
            Janus.Windows.GridEX.GridEXValueListItemCollection valueList = new Janus.Windows.GridEX.GridEXValueListItemCollection();

            try
            {
                col.EditType = Janus.Windows.GridEX.EditType.Combo;
                col.HasValueList = true;
                valueList = col.ValueList;

                if (dtDonViTinh == null)
                    dtDonViTinh = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.SelectAll().Tables[0];

                System.Data.DataView view = dtDonViTinh.DefaultView;

                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["ID"], (string)row["Ten"]));
                }
            }
            catch (Exception ex) { }

            return valueList;
        }

        public static Janus.Windows.GridEX.GridEXValueListItemCollection FillNuocXXValueList(Janus.Windows.GridEX.GridEXColumn col)
        {
            Janus.Windows.GridEX.GridEXValueListItemCollection valueList = new Janus.Windows.GridEX.GridEXValueListItemCollection();

            try
            {
                col.EditType = Janus.Windows.GridEX.EditType.Combo;
                col.HasValueList = true;
                valueList = col.ValueList;

                if (dtNuoc == null)
                    dtNuoc = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.SelectAll().Tables[0];

                System.Data.DataView view = dtNuoc.DefaultView;

                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["ID"], (string)row["Ten"]));
                }
            }
            catch (Exception ex) { }

            return valueList;
        }

        #endregion

        #region File, Folder

        /// <summary>
        /// Đọc file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>byte[]</returns>
        public static byte[] ReadFile(string filePath)
        {
            byte[] filedata = new byte[0];

            try
            {
                // provide read access to the file
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);

                // Create a byte array of file stream length
                filedata = new byte[fs.Length];

                //Read block of bytes from stream into the byte array
                fs.Read(filedata, 0, System.Convert.ToInt32(fs.Length));

                //Close the File Stream
                fs.Close();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return filedata;
        }

        /// <summary>
        /// Ghi file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="data"></param>
        public static bool WriteFile(string filePath, byte[] data)
        {
            try
            {
                // provide read access to the file
                FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite);

                //Read block of bytes from stream into the byte array
                fs.Write(data, 0, data.Length);

                //Close the File Stream
                fs.Close();

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }

        }
        #endregion
        static Image _ResourceWait;
        public static Image ResourceWait
        {
            get
            {
                return _ResourceWait;
            }
        }
        static Globals()
        {
            try
            {
                System.IO.Stream stream;
                System.Reflection.Assembly assembly;

                assembly = System.Reflection.Assembly.LoadFrom(Application.ExecutablePath);
                stream = assembly.GetManifestResourceStream("Company.Interface.Image.wait.gif");
                _ResourceWait = Image.FromStream(stream);
            }
            catch { }
        }

        public static string ConfigPhongBiPhanHoi(int type, int function, string maHaiQuan, string maDoanhNghiep, string GUIDSTR)
        {
            XmlDocument doc = new XmlDocument();
            string path = Company.GC.BLL.EntityBase.GetPathProgram();
            doc.Load(path + "\\B03GiaCong\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(maHaiQuan));
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = GUIDSTR.ToUpper();

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString().ToUpper());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[0].InnerText = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI"));
            nodeFrom.ChildNodes[1].InnerText = maDoanhNghiep.Trim();

            return doc.InnerXml;
        }

        //DATLMQ Export Excel Thông báo Thuế 06/08/2011
        public static void ExportExcelThongBaoThue(Company.GC.BLL.KDT.ToKhaiMauDich TKMD, string _soQD, DateTime _ngayQD, DateTime _ngayHH, string _TKKB, string _TenKB, string _chuongThueXNK,
                                                    string _loaiThueXNK, string _khoanThueXNK, string _mucThueXNK, string _tieuMucThueXNK, double _soTienThueXNK, string _chuongThueVAT,
                                                    string _loaiThueVAT, string _khoanThueVAT, string _mucThueVAT, string _tieuMucThueVAT, double _soTienThueVAT, string _chuongThueTTDB,
                                                    string _loaiThueTTDB, string _khoanThueTTDB, string _mucThueTTDB, string _tieuMucThueTTDB, double _soTienThueTTDB, string _chuongThueTVCBPG,
                                                    string _loaiThueTVCBPG, string _khoanThueTVCBPG, string _mucThueTVCBPG, string _tieuMucThueTVCBPG, double _soTienThueTVCBPG, int soThapPhan)
        {
            //Lấy đường dẫn file gốc
            string destFile = "";
            string fileName = "Thong_Bao_Thue.xls";

            string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

            //Kiem tra file Excel mau co ton tai?
            if (!System.IO.File.Exists(sourcePath))
            {
                ShowMessage("Không tồn tại file mẫu Excel.", false);
                return;
            }

            //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
            try
            {
                Infragistics.Excel.Workbook workBook = Infragistics.Excel.Workbook.Load(sourcePath);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets[0];

                //Nội dung xuất Excel
                //1. Tên chi cục Hải quan: A2
                workSheet.GetCell("A2").Value = GlobalSettings.TEN_HAI_QUAN;
                //2. Số QD: D3
                workSheet.GetCell("D3").Value = TKMD.SoQD + "/TBT";
                //3. Người Xuất khẩu/Nhập khẩu: F9
                workSheet.GetCell("F9").Value = TKMD.TenDoanhNghiep;
                //4. Mã số thuế: F10
                workSheet.GetCell("F10").Value = TKMD.MaDoanhNghiep;
                //5. Người khai Hải quan: F11
                workSheet.GetCell("F11").Value = TKMD.TenDoanhNghiep;
                //6. Mã số thuế: F12
                workSheet.GetCell("F12").Value = TKMD.MaDoanhNghiep;
                //7. Số tờ khai: C13
                workSheet.GetCell("C13").Value = TKMD.SoToKhai.ToString();
                //8. Mã loại hình: I13
                workSheet.GetCell("I13").Value = TKMD.MaLoaiHinh;
                //9. Ngày đăng ký: O13
                workSheet.GetCell("O13").Value = TKMD.NgayDangKy.ToShortDateString();
                //10. Số Vận Đơn: D15
                workSheet.GetCell("D15").Value = TKMD.SoVanDon;
                //11. Ngày Vận Đơn: J15
                workSheet.GetCell("J15").Value = TKMD.NgayVanDon.ToShortDateString();
                //12. Số Hóa Đơn Thương Mại: F16
                workSheet.GetCell("F16").Value = TKMD.SoHoaDonThuongMai;
                //13. Ngày Hóa Đơn Thương Mại: J16
                workSheet.GetCell("J16").Value = TKMD.NgayHoaDonThuongMai.ToShortDateString();
                //14. Kết quả xử lý: E18
                string tenLuong = "";
                if (TKMD.PhanLuong == "1")
                    tenLuong = "Xanh";
                else if (TKMD.PhanLuong == "2")
                    tenLuong = "Vàng";
                else
                    tenLuong = "Đỏ";
                workSheet.GetCell("E18").Value = "Tờ khai được phân luồng " + tenLuong;
                //15. Chi tiết Thuế
                //Thuế XNK: Chương: F24
                workSheet.GetCell("F24").Value = _chuongThueXNK;
                //Loại: H24
                workSheet.GetCell("H24").Value = _loaiThueXNK;
                //Khoản: J24
                workSheet.GetCell("J24").Value = _khoanThueXNK;
                //Mục: L24
                workSheet.GetCell("L24").Value = _mucThueXNK;
                //Tiểu Mục: N24
                workSheet.GetCell("N24").Value = _tieuMucThueXNK;
                //Số Tiền: P24
                workSheet.GetCell("P24").Value = _soTienThueXNK.ToString("N" + soThapPhan);
                //Thuế VAT: Chương: F26
                workSheet.GetCell("F26").Value = _chuongThueVAT;
                //Loại: H24
                workSheet.GetCell("H26").Value = _loaiThueVAT;
                //Khoản: J24
                workSheet.GetCell("J26").Value = _khoanThueVAT;
                //Mục: L24
                workSheet.GetCell("L26").Value = _mucThueVAT;
                //Tiểu Mục: N24
                workSheet.GetCell("N26").Value = _tieuMucThueVAT;
                //Số Tiền: P24
                workSheet.GetCell("P26").Value = _soTienThueVAT.ToString("N" + soThapPhan);
                //Thuế TTDB: Chương: F28
                workSheet.GetCell("F28").Value = _chuongThueTTDB;
                //Loại: H24
                workSheet.GetCell("H28").Value = _loaiThueTTDB;
                //Khoản: J24
                workSheet.GetCell("J28").Value = _khoanThueTTDB;
                //Mục: L24
                workSheet.GetCell("L28").Value = _mucThueTTDB;
                //Tiểu Mục: N24
                workSheet.GetCell("N28").Value = _tieuMucThueTTDB;
                //Số Tiền: P24
                workSheet.GetCell("P28").Value = _soTienThueTTDB.ToString("N" + soThapPhan);
                //Thuế TVCBPG: Chương: F30
                workSheet.GetCell("F30").Value = _chuongThueTVCBPG;
                //Loại: H24
                workSheet.GetCell("H30").Value = _loaiThueTVCBPG;
                //Khoản: J24
                workSheet.GetCell("J30").Value = _khoanThueTVCBPG;
                //Mục: L24
                workSheet.GetCell("L30").Value = _mucThueTVCBPG;
                //Tiểu Mục: N24
                workSheet.GetCell("N30").Value = _tieuMucThueTVCBPG;
                //Số Tiền: P24
                workSheet.GetCell("P30").Value = _soTienThueTVCBPG.ToString("N" + soThapPhan);
                //Tổng số Tiền Thuế: P32
                double tongTienThue = _soTienThueXNK + _soTienThueVAT + _soTienThueTTDB + _soTienThueTVCBPG;
                workSheet.GetCell("P32").Value = tongTienThue.ToString("N" + soThapPhan);
                //Bằng chữ: C35
                workSheet.GetCell("C35").Value = Company.GC.BLL.Utils.VNCurrency.ToString(tongTienThue);
                //Thời hạn: D37
                workSheet.GetCell("D37").Value = _ngayHH.Subtract(_ngayQD).TotalDays;
                //Kể từ ngày: H37
                workSheet.GetCell("H37").Value = _ngayQD.ToShortDateString();
                //Tài khoản Kho Bạc: L38
                workSheet.GetCell("L38").Value = _TKKB;
                //Tên Kho Bạc: A39
                workSheet.GetCell("A39").Value = _TenKB;

                //Chọn đường dẫn file cần lưu
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                string dateNow = DateTime.Now.ToShortDateString().Replace("/", ""); // +"_" + DateTime.Now.Hour + "h" + DateTime.Now.Minute + "p" + DateTime.Now.Second + "s";
                saveFileDialog1.FileName = "ThongBaoThue_TK_" + TKMD.SoToKhai + "_" + TKMD.MaLoaiHinh + "_" + GlobalSettings.MA_HAI_QUAN + "_" + TKMD.NamDK + "_" + dateNow;
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    destFile = saveFileDialog1.FileName;
                    //Ghi nội dung file gốc vào file cần lưu
                    try
                    {
                        byte[] sourceFile = Globals.ReadFile(sourcePath);
                        Globals.WriteFile(destFile, sourceFile);
                        workBook.Save(destFile);
                        ShowMessage("Lưu file thành công", false);
                        System.Diagnostics.Process.Start(destFile);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(ex.Message, false);
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.LocalLogger.Instance().WriteMessage(e);
                ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.\nChi tiết lỗi:\n" + e.Message, false);
                return;
            }
        }

        public enum ToKhaiType
        {
            Nhap,
            Xuat,
            PhucLucTKN,
            PhuLucTKX
        }
        public static void ExportData(GridEX gridEx)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DinhMucDaDangKy_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                sfNPL.ShowDialog();

                if (sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridEx;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        public static void ExportExcel(ToKhaiType typeTK, long tkmdID, bool banLuuHaiQuan, bool inMaHang, bool mienThueXuatKhau, bool mienThueGTGT, string pls)
        {
            Company.GC.BLL.KDT.ToKhaiMauDich TKMD = new Company.GC.BLL.KDT.ToKhaiMauDich();
            TKMD.ID = tkmdID;
            TKMD.Load();
            TKMD.LoadHMDCollection();
            TKMD.LoadChungTuHaiQuan();

            if (TKMD == null)
            {
                Globals.ShowMessage("Không tồn tại thông tin của tờ khai ID = " + tkmdID + ".", false);
                return;
            }

            if (typeTK == ToKhaiType.Nhap)
                ExportExcelTKN(TKMD, banLuuHaiQuan, inMaHang, mienThueXuatKhau, mienThueGTGT);
            else if (typeTK == ToKhaiType.Xuat)
                ExportExcelTKX(TKMD, banLuuHaiQuan, inMaHang, mienThueXuatKhau, mienThueGTGT);
            else if (typeTK == ToKhaiType.PhucLucTKN)
                ExportExcelPhuLucTKN(TKMD, banLuuHaiQuan, inMaHang, mienThueXuatKhau, mienThueGTGT, pls);
            else if (typeTK == ToKhaiType.PhuLucTKX)
                ExportExcelPhuLucTKX(TKMD, banLuuHaiQuan, inMaHang, mienThueXuatKhau, mienThueGTGT, pls);
        }
        private static void ExportExcelTKN(Company.GC.BLL.KDT.ToKhaiMauDich TKMD, bool banLuuHaiQuan, bool inMaHang, bool mienThueXuatKhau, bool mienThueGTGT)
        {
            bool ok = true;
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongTriGiaTT = 0;
            decimal tongTriGiaTTGTGT = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;
            Infragistics.Excel.WorksheetMergedCellsRegion mergedRegion;
            string tenHang = "";
            /*Số thập phân*/
            int soThapPhanSoLuong = 0;
            int soThapPhanDonGia = 10;
            int soThapPhanTriGiaNguyenTe = 2;
            int soThapPhanTriGiaTinhThue = 0;
            int soThapPhanThueSuat = 0;
            int soThapPhanTienThue = 0;
            int soThapPhanTyLethuKhac = 0;
            int soThapPhanTienThuKhac = 0;
            int soThapPhanTongTienThue = 0;
            int soThapPhanTyGia = 2;

            if (ok)
            {
                //Lấy đường dẫn file gốc
                string destFile = "";
                string fileName = "TKN_TQDT_2009_TT222.xls";

                string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

                //Kiem tra file Excel mau co ton tai?
                if (!System.IO.File.Exists(sourcePath))
                {
                    ShowMessage("Không tồn tại file mẫu Excel.", false);
                    return;
                }

                //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
                try
                {
                    Infragistics.Excel.Workbook workBook = Infragistics.Excel.Workbook.Load(sourcePath);
                    Infragistics.Excel.Worksheet workSheet = workBook.Worksheets[0];
                    DateTime minDate = new DateTime(1900, 1, 1);

                    //Chi cục Hải quan
                    workSheet.GetCell("E5").Value = GlobalSettings.TEN_HAI_QUAN;
                    //Chi cục Hải quan cửa khẩu
                    workSheet.GetCell("H6").Value = TKMD.CuaKhau_ID + "-" + Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(TKMD.CuaKhau_ID);

                    //Số tham chiếu
                    //if (TKMD.ID > 0)
                    //    workSheet.GetCell("R5").Value = TKMD.ID + "";
                    //else
                    //    workSheet.GetCell("R5").Value = "";
                    if (TKMD.SoTiepNhan > 0)
                        workSheet.GetCell("R5").Value = TKMD.SoTiepNhan.ToString();
                    else
                        workSheet.GetCell("R5").Value = TKMD.ID.ToString();
                    //Ngày, giờ gửi
                    if (TKMD.NgayTiepNhan > minDate)
                        workSheet.GetCell("Q6").Value = TKMD.NgayTiepNhan.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("Q6").Value = "";

                    //Số tờ khai
                    if (TKMD.SoToKhai > 0)
                        workSheet.GetCell("AB5").Value = TKMD.SoToKhai + "";
                    else
                        workSheet.GetCell("AB5").Value = "";
                    //Ngày, giờ đăng ký
                    if (TKMD.NgayDangKy > minDate)
                        workSheet.GetCell("AB6").Value = TKMD.NgayDangKy.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("AB6").Value = "";

                    //1.Người xuất khẩu A8
                    workSheet.GetCell("A8").Value = TKMD.TenDonViDoiTac.ToUpper();
                    //2.Người nhập khẩu A10
                    workSheet.GetCell("A10").Value = (TKMD.TenDoanhNghiep + ". " + GlobalSettings.DIA_CHI).ToUpper();
                    //3.Người ủy thác A12
                    workSheet.GetCell("A12").Value = TKMD.TenDonViUT;
                    //4.Đại lý làm thủ tục Hải quan A14
                    workSheet.GetCell("A14").Value = TKMD.MaDaiLyTTHQ + "." + TKMD.TenDaiLyTTHQ;
                    //5. Loại hình P7
                    string stlh = "";
                    stlh = Company.KDT.SHARE.Components.DuLieuChuan.LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                    workSheet.GetCell("P7").Value = TKMD.MaLoaiHinh + stlh;
                    //6. Hóa đơn thương mại M9, Ngày O10
                    workSheet.GetCell("M9").Value = TKMD.SoHoaDonThuongMai;
                    if (TKMD.NgayHoaDonThuongMai > minDate)
                        workSheet.GetCell("O10").Value = TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("O10").Value = "";
                    //7. Giấy phép U8, Ngày U9, Ngày hết hạn U10
                    if (TKMD.SoGiayPhep != "")
                        workSheet.GetCell("U8").Value = "" + TKMD.SoGiayPhep;
                    else
                        workSheet.GetCell("U8").Value = "";

                    if (TKMD.NgayGiayPhep > minDate)
                        workSheet.GetCell("U9").Value = TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("U9").Value = "";

                    if (TKMD.NgayHetHanGiayPhep > minDate)
                        workSheet.GetCell("U10").Value = TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("U10").Value = "";
                    //8. Hợp đồng AA8, Ngày AB9, Ngày hết hạn AB10
                    workSheet.GetCell("AA8").Value = "" + TKMD.SoHopDong;

                    if (TKMD.NgayHopDong > minDate)
                        workSheet.GetCell("AB9").Value = TKMD.NgayHopDong.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("AB9").Value = "";

                    if (TKMD.NgayHetHanHopDong > minDate)
                        workSheet.GetCell("AB10").Value = "" + TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("AB10").Value = "";

                    //9. Vận tải đơn P11, Ngày N12
                    workSheet.GetCell("P11").Value = TKMD.SoVanDon;
                    if (TKMD.NgayVanDon > minDate)
                        workSheet.GetCell("N12").Value = TKMD.NgayVanDon.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("N12").Value = "";
                    //10. Cảng xếp hàng R12
                    workSheet.GetCell("R12").Value = TKMD.DiaDiemXepHang;
                    //11 Cảng dỡ hàng AB11, Tên cảng X12
                    workSheet.GetCell("AB11").Value = TKMD.CuaKhau_ID;
                    workSheet.GetCell("X12").Value = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(TKMD.CuaKhau_ID);
                    //12. Phương tiện vận tải R13, Tên M14
                    workSheet.GetCell("R13").Value = " " + TKMD.SoHieuPTVT;
                    workSheet.GetCell("M14").Value = Company.KDT.SHARE.Components.DuLieuChuan.PhuongThucVanTai.getName(TKMD.PTVT_ID);
                    //13. Nước xuất khẩu AC13, Tên X14
                    workSheet.GetCell("AC13").Value = TKMD.NuocXK_ID;
                    workSheet.GetCell("X14").Value = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(TKMD.NuocXK_ID);
                    //14. Điều kiện giao hàng R15
                    workSheet.GetCell("R15").Value = TKMD.DKGH_ID;
                    //15. Phương thức thanh toán AA15
                    workSheet.GetCell("AA15").Value = TKMD.PTTT_ID;
                    //16. Đồng tiền thanh toán S16
                    workSheet.GetCell("S16").Value = TKMD.NguyenTe_ID;
                    //17. Tỷ giá tính thuế Z16
                    workSheet.GetCell("Z16").Value = TKMD.TyGiaTinhThue.ToString("G10");
                    //18. Kết quả phân luồng và hướng dẫn làm thủ tục Hải quan: A18
                    workSheet.GetCell("A18").Value = TKMD.HUONGDAN;
                    //19. Chứng từ Hải quan trước: X18

                    /*THÔNG TIN HÀNG*/
                    //20. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                    //21. Mã số hàng hóa: K22, K23, K24
                    //22. Xuất xứ: O22, O23, O24
                    //23. Số lượng: R22, R23, R24
                    //24. Đơn vị tính: U22, U23, U24
                    //25. Đơn giá nguyên tệ: X22, X23, X24
                    //26. Trị giá nguyên tệ: AB22, AN23, AB24
                    //Cộng: AD25

                    //Truoc 26. Phi bao hiem B25
                    string st = "";
                    if (TKMD.PhiBaoHiem > 0)
                        st = "I = " + TKMD.PhiBaoHiem.ToString("N2");
                    if (TKMD.PhiVanChuyen > 0)
                        st += " F = " + TKMD.PhiVanChuyen.ToString("N2");
                    if (TKMD.PhiKhac > 0)
                        st += " Phí khác = " + TKMD.PhiKhac.ToString("N2");
                    workSheet.GetCell("B25").Value = st;

                    /*THÔNG TIN THUẾ*/
                    //27. Thuế nhập khẩu
                    //Trị giá tính thuế: B28, B29, B30
                    //Thuế suất (%): F28, F29, F30
                    //Tiền thuế: I28, I29, I30
                    //Cộng: K31
                    //28. Thuế GTGT (hoặc TTĐB)
                    //Trị giá tính thuế: L28, L29, L30
                    //Thuế suất (%): P28, P29, P30
                    //Tiền thuế: S28, S29, S30
                    //29. Thu khác
                    //Tỷ lệ (%): W28, W29, W30
                    //Số tiền: AA28, AA29, AA30
                    //30. Tổng số tiền thuế và thu khác (ô 27+28 + 29) bằng số: A32, Bằng chữ E33

                    if (TKMD.HMDCollection.Count > 0 && TKMD.HMDCollection.Count <= 3)
                    {
                        #region Chi tiet hang hoa

                        Company.GC.BLL.KDT.HangMauDich hmd = null;
                        for (int i = 0; i < TKMD.HMDCollection.Count; i++)
                        {
                            hmd = TKMD.HMDCollection[i];

                            #region THÔNG TIN HÀNG
                            //20. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                            if (!inMaHang)
                            {
                                if (hmd.Ma_HTS.Trim().Length > 0)
                                    tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                                else
                                    tenHang = hmd.TenHang;
                            }
                            else
                            {
                                if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length > 0)
                                    tenHang = hmd.Ma_HTS + ";" + hmd.TenHang + "/" + hmd.MaPhu;
                                else if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length <= 0)
                                    tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                                else if (hmd.Ma_HTS.Trim().Length <= 0 && hmd.MaPhu.Trim().Length > 0)
                                    tenHang = hmd.TenHang + "/" + hmd.MaPhu;
                                else
                                    tenHang = hmd.TenHang;
                            }
                            workSheet.Rows[workSheet.GetCell("B22").RowIndex + i].Cells[1].Value = tenHang;

                            //21. Mã số hàng hóa: K22, K23, K24
                            // Create a merged region
                            mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("K22").RowIndex + i, 11, workSheet.GetCell("K22").RowIndex + i, 13);
                            mergedRegion.Value = hmd.MaHS;
                            mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                            //19. Xuất xứ: O22, O23, O24
                            mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("O22").RowIndex + i, 14, workSheet.GetCell("O22").RowIndex + i, 16);
                            mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                            mergedRegion.Value = hmd.NuocXX_ID;
                            //20. Số lượng: R22, R23, R24 hoac T22
                            workSheet.Rows[workSheet.GetCell("T22").RowIndex + i].Cells[workSheet.GetCell("T22").ColumnIndex].Value = hmd.SoLuong.ToString("N" + soThapPhanSoLuong);
                            workSheet.Rows[workSheet.GetCell("T22").RowIndex + i].Cells[workSheet.GetCell("T22").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //21. Đơn vị tính: U22, U23, U24
                            mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("U22").RowIndex + i, 20, workSheet.GetCell("U22").RowIndex + i, 22);
                            mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                            mergedRegion.Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(hmd.DVT_ID);
                            //22. Đơn giá nguyên tệ: X22, X23, X24 hoac AA22
                            workSheet.Rows[workSheet.GetCell("AA22").RowIndex + i].Cells[workSheet.GetCell("AA22").ColumnIndex].Value = hmd.DonGiaKB.ToString("G" + soThapPhanDonGia);
                            workSheet.Rows[workSheet.GetCell("AA22").RowIndex + i].Cells[workSheet.GetCell("AA22").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //23. Trị giá nguyên tệ: AB22, AN23, AB24 hoac AD22
                            workSheet.Rows[workSheet.GetCell("AD22").RowIndex + i].Cells[workSheet.GetCell("AD22").ColumnIndex].Value = hmd.TriGiaKB.ToString("N" + soThapPhanTriGiaNguyenTe);
                            workSheet.Rows[workSheet.GetCell("AD22").RowIndex + i].Cells[workSheet.GetCell("AD22").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                            tongTriGiaNT += Math.Round(Convert.ToDecimal(hmd.TriGiaKB), soThapPhanTriGiaNguyenTe, MidpointRounding.AwayFromZero);

                            #endregion

                            #region THÔNG TIN THUẾ

                            if (!mienThueXuatKhau)
                            {
                                //27. Thuế nhập khẩu
                                //Trị giá tính thuế: E28
                                workSheet.Rows[workSheet.GetCell("E28").RowIndex + i].Cells[workSheet.GetCell("E28").ColumnIndex].Value = hmd.TriGiaTT.ToString("N" + soThapPhanTriGiaTinhThue);
                                workSheet.Rows[workSheet.GetCell("E28").RowIndex + i].Cells[workSheet.GetCell("E28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Thuế suất (%): H28
                                workSheet.Rows[workSheet.GetCell("H28").RowIndex + i].Cells[workSheet.GetCell("H28").ColumnIndex].Value = hmd.ThueSuatXNK.ToString("N" + soThapPhanThueSuat);
                                workSheet.Rows[workSheet.GetCell("H28").RowIndex + i].Cells[workSheet.GetCell("H28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Tiền thuế: K28
                                workSheet.Rows[workSheet.GetCell("K28").RowIndex + i].Cells[workSheet.GetCell("K28").ColumnIndex].Value = hmd.ThueXNK.ToString("N" + soThapPhanTienThue);
                                workSheet.Rows[workSheet.GetCell("K28").RowIndex + i].Cells[workSheet.GetCell("K28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            }

                            if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                            {
                                decimal TriGiaTTGTGT = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                                if (!mienThueXuatKhau)
                                {
                                    //28. Thuế GTGT
                                    //Trị giá tính thuế: O28
                                    workSheet.Rows[workSheet.GetCell("O28").RowIndex + i].Cells[workSheet.GetCell("O28").ColumnIndex].Value = TriGiaTTGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                    workSheet.Rows[workSheet.GetCell("O28").RowIndex + i].Cells[workSheet.GetCell("O28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                    //Tỷ lệ (%): R28
                                    workSheet.Rows[workSheet.GetCell("R28").RowIndex + i].Cells[workSheet.GetCell("R28").ColumnIndex].Value = hmd.ThueSuatGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                    workSheet.Rows[workSheet.GetCell("R28").RowIndex + i].Cells[workSheet.GetCell("R28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                    //Tiền thuế: V28
                                    workSheet.Rows[workSheet.GetCell("V28").RowIndex + i].Cells[workSheet.GetCell("V28").ColumnIndex].Value = hmd.ThueGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                    workSheet.Rows[workSheet.GetCell("V28").RowIndex + i].Cells[workSheet.GetCell("V28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                                    //29. Thu khác
                                    //Tỷ lệ (%): Z28
                                    workSheet.Rows[workSheet.GetCell("Z28").RowIndex + i].Cells[workSheet.GetCell("Z28").ColumnIndex].Value = hmd.TyLeThuKhac.ToString("N" + soThapPhanTyLethuKhac);
                                    workSheet.Rows[workSheet.GetCell("Z28").RowIndex + i].Cells[workSheet.GetCell("Z28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                    //Số tiền: AD28
                                    workSheet.Rows[workSheet.GetCell("AD28").RowIndex + i].Cells[workSheet.GetCell("AD28").ColumnIndex].Value = hmd.TriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                                    workSheet.Rows[workSheet.GetCell("AD28").RowIndex + i].Cells[workSheet.GetCell("AD28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                }

                                tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                                tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                            {
                                decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                                tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                                tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                            {
                                decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                                tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                                tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                            }

                            //Tiền thuế Xuất nhập khẩu
                            tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);

                            //Tổng tiền thuế và thu khác
                            tongTienThueTatCa += Convert.ToDecimal(hmd.ThueXNK) + Convert.ToDecimal(hmd.TriGiaThuKhac) + Convert.ToDecimal(hmd.ThueTTDB) + Convert.ToDecimal(hmd.ThueGTGT);

                            #endregion
                        }

                        #endregion
                    }
                    //CHI TIET DINH KEM
                    else
                    {
                        #region CHI TIET DINH KEM

                        //17. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                        workSheet.GetCell("B22").Value = "HÀNG HÓA NHẬP";
                        workSheet.GetCell("B23").Value = "(Chi tiết theo phụ lục đính kèm)";

                        foreach (Company.GC.BLL.KDT.HangMauDich hmd in TKMD.HMDCollection)
                        {
                            tongTriGiaNT += Math.Round(Convert.ToDecimal(hmd.TriGiaKB), soThapPhanTriGiaNguyenTe, MidpointRounding.AwayFromZero);
                            tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);
                            tongTriGiaTT += Convert.ToDecimal(hmd.TriGiaTT);
                            if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                            {
                                tongTriGiaTTGTGT += Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                                tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                                tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                            {
                                tongTriGiaTTGTGT += Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                                tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                                tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                            }
                            else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                            {
                                tongTriGiaTTGTGT += Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                                tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                                tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                            }
                            //Tiền thuế Xuất nhập khẩu
                            //tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);

                            //Tổng tiền thuế và thu khác
                            tongTienThueTatCa += Convert.ToDecimal(hmd.ThueXNK) + Convert.ToDecimal(hmd.TriGiaThuKhac) + Convert.ToDecimal(hmd.ThueTTDB) + Convert.ToDecimal(hmd.ThueGTGT);
                        }

                        #endregion
                    }

                    //Linhhtn - Không công phí VC và các phí khác vào Tổng trị giá nguyên tệ
                    //tongTriGiaNT += Convert.ToDecimal(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);

                    //23. Trị giá nguyên tệ: Cộng: AD25
                    workSheet.GetCell("AD25").Value = tongTriGiaNT.ToString("N" + soThapPhanTriGiaNguyenTe);
                    workSheet.GetCell("AD25").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //24. Thuế nhập khẩu: Cộng: K31
                    //if (TKMD.HMDCollection.Count <= 3)
                    //{
                    //    workSheet.GetCell("K31").Value = tongTienThueXNK.ToString("N" + soThapPhanTienThue);
                    //    workSheet.GetCell("K31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}
                    workSheet.GetCell("K31").Value = tongTienThueXNK.ToString("N" + soThapPhanTienThue);
                    workSheet.GetCell("K31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                    //Thuế GTGT
                    //if (tongThueGTGT > 0)
                    //    lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
                    //else
                    //    lblTongTienThueGTGT.Text = "";
                    //if (TKMD.HMDCollection.Count <= 3)
                    //{
                    //    workSheet.GetCell("V31").Value = tongThueGTGT.ToString("N" + soThapPhanTienThue);
                    //    workSheet.GetCell("V31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}
                    workSheet.GetCell("V31").Value = tongThueGTGT.ToString("N" + soThapPhanTienThue);
                    workSheet.GetCell("V31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                    //Thuế thu khác
                    if (tongTriGiaThuKhac > 0 && TKMD.HMDCollection.Count <= 3)
                    {
                        workSheet.GetCell("AD31").Value = tongTriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                        workSheet.GetCell("AD31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    }

                    //26. Tổng số tiền thuế và thu khác (ô 24 + 25) bằng số: A32, Bằng chữ E33
                    //mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("A32").RowIndex, 1, workSheet.GetCell("A32").RowIndex, 20);
                    //mergedRegion.Value = tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);
                    workSheet.GetCell("A32").Value += " " + tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);

                    //Bằng chữ
                    string s = Company.GC.BLL.Utils.VNCurrency.ToString(tongTienThueTatCa).Trim();
                    s = s[0].ToString().ToUpper() + s.Substring(1);
                    workSheet.GetCell("E33").Value = s.Replace("  ", " ");
                    workSheet.GetCell("E33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Left;

                    //31. Tổng trọng lượng F34, Số hiệu kiện, cont: F35, Tổng số container: W35
                    if (TKMD.TrongLuong > 0)
                        workSheet.GetCell("F34").Value = TKMD.TrongLuong + " kg ";
                    else
                        workSheet.GetCell("F34").Value = "";
                    //Số hiệu kiện, cont: F35
                    if (TKMD.SoContainer20 > 3 || TKMD.SoContainer40 > 3)
                        workSheet.GetCell("F35").Value = "danh sách container theo bảng kê đính kèm";
                    else
                    {
                        string soHieuKien = "";
                        Company.KDT.SHARE.QuanLyChungTu.Container objContainer = null;

                        if (TKMD.VanTaiDon != null)
                        {
                            for (int cnt = 0; cnt < TKMD.VanTaiDon.ContainerCollection.Count; cnt++)
                            {
                                objContainer = TKMD.VanTaiDon.ContainerCollection[cnt];

                                soHieuKien += objContainer.SoHieu + "/" + objContainer.Seal_No;

                                if (cnt < TKMD.VanTaiDon.ContainerCollection.Count - 1)
                                {
                                    soHieuKien += "; ";
                                }
                            }
                        }
                        workSheet.GetCell("F35").Value = soHieuKien;
                    }
                    //Tổng số container: W35
                    string tsContainer = "";
                    string cont20 = "", cont40 = "", soKien = "";
                    if (TKMD.SoContainer20 > 0)
                    {
                        cont20 = "Cont20: " + Convert.ToInt32(TKMD.SoContainer20);

                        tsContainer += cont20 + "; ";
                    }
                    else
                        cont20 = "";

                    if (TKMD.SoContainer40 > 0)
                    {
                        cont40 = "Cont40: " + Convert.ToInt32(TKMD.SoContainer40);

                        tsContainer += cont40 + "; ";
                    }
                    else
                        cont40 = "";

                    if (TKMD.SoKien > 0)
                    {
                        soKien = "Tổng số kiện: " + TKMD.SoKien + " kg";
                        tsContainer += soKien;
                    }
                    else
                        soKien = "";

                    workSheet.GetCell("W35").Value = tsContainer;

                    //32. Ghi chép khác: A37
                    workSheet.GetCell("A37").Value = TKMD.DeXuatKhac;
                    if (TKMD.TrongLuongNet > 0 && TKMD.DeXuatKhac.Length > 0)
                        workSheet.GetCell("A37").Value += "; " + "Trọng lượng tịnh: " + TKMD.TrongLuongNet.ToString();
                    else if (TKMD.TrongLuongNet > 0 && TKMD.DeXuatKhac.Equals(""))
                        workSheet.GetCell("A37").Value += "Trọng lượng tịnh: " + TKMD.TrongLuongNet.ToString();
                    //33. Ngày tháng năm: A42
                    if (TKMD.NgayDangKy == new DateTime(1900, 1, 1))
                        workSheet.GetCell("A42").Value = "Ngày " + DateTime.Today.Day + " tháng " + DateTime.Today.Month + " năm " + DateTime.Today.Year;
                    else
                        workSheet.GetCell("A42").Value = "Ngày " + TKMD.NgayTiepNhan.Day + " tháng " + TKMD.NgayTiepNhan.Month + " năm " + TKMD.NgayTiepNhan.Year;

                    //Chọn đường dẫn file cần lưu
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                    string dateNow = DateTime.Now.ToShortDateString().Replace("/", ""); // +"_" + DateTime.Now.Hour + "h" + DateTime.Now.Minute + "p" + DateTime.Now.Second + "s";
                    saveFileDialog1.FileName = "TK" + TKMD.SoToKhai + "_" + TKMD.MaLoaiHinh + "_" + GlobalSettings.MA_HAI_QUAN + "_" + TKMD.NamDK + "_" + dateNow;
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        destFile = saveFileDialog1.FileName;
                        //Ghi nội dung file gốc vào file cần lưu
                        try
                        {
                            byte[] sourceFile = Globals.ReadFile(sourcePath);
                            Globals.WriteFile(destFile, sourceFile);
                            workBook.Save(destFile);
                            ShowMessage("Save file thành công", false);
                            System.Diagnostics.Process.Start(destFile);
                        }
                        catch (Exception ex)
                        {
                            ShowMessage(ex.Message, false);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //clsLocalLogger.getLogger().message(ex);
                    ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.\nChi tiết lỗi:\n" + ex.Message, false);
                    return;
                }
            }
        }
        private static void ExportExcelTKX(Company.GC.BLL.KDT.ToKhaiMauDich TKMD, bool banLuuHaiQuan, bool inMaHang, bool mienThueXuatKhau, bool mienThueGTGT)
        {
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongTriGiaTT = 0;
            decimal tongTriGiaTTGTGT = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;
            Infragistics.Excel.WorksheetMergedCellsRegion mergedRegion;
            string tenHang = "";
            /*Số thập phân*/
            int soThapPhanSoLuong = 0;
            int soThapPhanDonGia = 10;
            int soThapPhanTriGiaNguyenTe = 2;
            int soThapPhanTriGiaTinhThue = 0;
            int soThapPhanThueSuat = 0;
            int soThapPhanTienThue = 0;
            int soThapPhanTyLethuKhac = 0;
            int soThapPhanTienThuKhac = 0;
            int soThapPhanTongTienThue = 0;
            int soThapPhanTyGia = 2;


            //Lấy đường dẫn file gốc
            string destFile = "";
            string fileName = "TKX_TQDT_2009_TT222.xls";

            string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

            //Kiem tra file Excel mau co ton tai?
            if (!System.IO.File.Exists(sourcePath))
            {
                ShowMessage("Không tồn tại file mẫu Excel.", false);
                return;
            }

            //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
            try
            {
                Infragistics.Excel.Workbook workBook = Infragistics.Excel.Workbook.Load(sourcePath);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets[0];
                DateTime minDate = new DateTime(1900, 1, 1);

                #region Ô số 1 -> 16

                //Chi cục Hải quan
                mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("E5").RowIndex, 4, workSheet.GetCell("E5").RowIndex, 11);
                mergedRegion.Value = GlobalSettings.TEN_HAI_QUAN;
                mergedRegion.CellFormat.ShrinkToFit = Infragistics.Excel.ExcelDefaultableBoolean.True;

                //Chi cục Hải quan cửa khẩu
                workSheet.GetCell("H6").Value = TKMD.CuaKhau_ID + "-" + Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(TKMD.CuaKhau_ID);

                //Số tham chiếu
                if (TKMD.ID > 0)
                    workSheet.GetCell("R5").Value = TKMD.ID + "";
                else
                    workSheet.GetCell("R5").Value = "";
                //Ngày, giờ gửi
                if (TKMD.NgayTiepNhan > minDate)
                    workSheet.GetCell("Q6").Value = TKMD.NgayTiepNhan.ToString("dd/MM/yyyy");
                else
                    workSheet.GetCell("Q6").Value = "";

                //Số tờ khai
                if (TKMD.SoToKhai > 0)
                    workSheet.GetCell("AB5").Value = TKMD.SoToKhai + "";
                else
                    workSheet.GetCell("AB5").Value = "";
                //Ngày, giờ đăng ký
                if (TKMD.NgayDangKy > minDate)
                    workSheet.GetCell("AB6").Value = TKMD.NgayDangKy.ToString("dd/MM/yyyy");
                else
                    workSheet.GetCell("AB6").Value = "";

                //1.Người xuất khẩu A8
                workSheet.GetCell("A8").Value = (TKMD.TenDoanhNghiep + ". " + GlobalSettings.DIA_CHI).ToUpper();
                //2.Người nhập khẩu A10
                workSheet.GetCell("A10").Value = TKMD.TenDonViDoiTac.ToUpper();
                //3.Người ủy thác A12
                workSheet.GetCell("A12").Value = TKMD.TenDonViUT;
                //4.Đại lý làm thủ tục Hải quan A14
                workSheet.GetCell("A14").Value = TKMD.MaDaiLyTTHQ + "." + TKMD.TenDaiLyTTHQ;
                //5. Loại hình P7
                string stlh = "";
                stlh = Company.KDT.SHARE.Components.DuLieuChuan.LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                workSheet.GetCell("P7").Value = TKMD.MaLoaiHinh + " - " + stlh;
                //6. Giấy phép: P8, Ngày P9, Ngày hết hạn P10
                if (TKMD.SoGiayPhep != "")
                    workSheet.GetCell("P8").Value = TKMD.SoGiayPhep;
                else
                    workSheet.GetCell("P8").Value = "";

                if (TKMD.NgayGiayPhep > minDate)
                    workSheet.GetCell("P9").Value = TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
                else
                    workSheet.GetCell("P9").Value = "";

                if (TKMD.NgayHetHanGiayPhep > minDate)
                    workSheet.GetCell("P10").Value = TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
                else
                    workSheet.GetCell("P10").Value = "";
                //7. Hợp đồng: X8, Ngày X9, Ngày hết hạn X10
                workSheet.GetCell("X8").Value = TKMD.SoHopDong;
                if (TKMD.NgayHopDong > minDate)
                    workSheet.GetCell("X9").Value = TKMD.NgayHopDong.ToString("dd/MM/yyyy");
                else
                    workSheet.GetCell("X9").Value = "";
                if (TKMD.NgayHetHanHopDong > minDate)
                    workSheet.GetCell("X10").Value = TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
                else
                    workSheet.GetCell("X10").Value = "";
                //8. Hóa đơn thương mại R11, Ngày M12
                workSheet.GetCell("R11").Value = TKMD.SoHoaDonThuongMai;
                if (TKMD.NgayHoaDonThuongMai > minDate)
                    workSheet.GetCell("M12").Value = TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
                else
                    workSheet.GetCell("M12").Value = "";
                //9. Cảng xếp hàng: Y11, Tên cảng U12
                workSheet.GetCell("Y11").Value = TKMD.CuaKhau_ID;
                workSheet.GetCell("U12").Value = Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(TKMD.CuaKhau_ID);
                //10. Nước nhập khẩu: R13, Tên nước N14
                workSheet.GetCell("R13").Value = TKMD.NuocNK_ID;
                workSheet.GetCell("N14").Value = Company.KDT.SHARE.Components.DuLieuChuan.Nuoc.GetName(TKMD.NuocNK_ID);
                //11. Điều kiện giao hàng: S15
                workSheet.GetCell("S15").Value = TKMD.DKGH_ID;
                //12. Phương thức thanh toán AA15
                workSheet.GetCell("AA15").Value = TKMD.PTTT_ID;
                //13. Đồng tiền thanh toán S16
                workSheet.GetCell("S16").Value = TKMD.NguyenTe_ID;
                //14. Tỷ giá tính thuế AA16
                workSheet.GetCell("AA16").Value = TKMD.TyGiaTinhThue.ToString("N" + soThapPhanTyGia);
                //15. Kết quả phân luồng và hướng dẫn làm thủ tục Hải quan: A18
                workSheet.GetCell("A18").Value = TKMD.HUONGDAN;
                //16. Chứng từ Hải quan trước: X18

                #endregion

                /*THÔNG TIN HÀNG*/
                //17. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                //18. Mã số hàng hóa: L22, L23, L24
                //19. Xuất xứ: O22, O23, O24
                //20. Số lượng: T22
                //21. Đơn vị tính: U22, U23, U24
                //22. Đơn giá nguyên tệ: AA22
                //23. Trị giá nguyên tệ: AD22
                //Cộng: AD25
                /*THÔNG TIN THUẾ*/
                //24. Thuế nhập khẩu
                //Trị giá tính thuế: H28
                //Thuế suất (%): N28
                //Tiền thuế: T28
                //Cộng: T31
                //25. Thu khác
                //Tỷ lệ (%): X28
                //Số tiền: AD28

                if (TKMD.HMDCollection.Count > 0 && TKMD.HMDCollection.Count <= 3)
                {
                    #region Chi tiet hang hoa

                    Company.GC.BLL.KDT.HangMauDich hmd = null;
                    for (int i = 0; i < TKMD.HMDCollection.Count; i++)
                    {
                        hmd = TKMD.HMDCollection[i];

                        #region THÔNG TIN HÀNG
                        //17. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                        if (!inMaHang)
                        {
                            if (hmd.Ma_HTS.Trim().Length > 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                            else
                                tenHang = hmd.TenHang;
                        }
                        else
                        {
                            if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length > 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang + "/" + hmd.MaPhu;
                            else if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length <= 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                            else if (hmd.Ma_HTS.Trim().Length <= 0 && hmd.MaPhu.Trim().Length > 0)
                                tenHang = hmd.TenHang + "/" + hmd.MaPhu;
                            else
                                tenHang = hmd.TenHang;
                        }
                        workSheet.Rows[workSheet.GetCell("B22").RowIndex + i].Cells[1].Value = tenHang;

                        //18. Mã số hàng hóa: L22, L23, L24
                        // Create a merged region
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("L22").RowIndex + i, 11, workSheet.GetCell("L22").RowIndex + i, 13);
                        mergedRegion.Value = hmd.MaHS;
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        //19. Xuất xứ: O22, O23, O24
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("O22").RowIndex + i, 14, workSheet.GetCell("O22").RowIndex + i, 16);
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        mergedRegion.Value = hmd.NuocXX_ID;
                        //20. Số lượng: R22, R23, R24 hoac T22
                        workSheet.Rows[workSheet.GetCell("T22").RowIndex + i].Cells[workSheet.GetCell("T22").ColumnIndex].Value = hmd.SoLuong.ToString("N" + soThapPhanSoLuong);
                        workSheet.Rows[workSheet.GetCell("T22").RowIndex + i].Cells[workSheet.GetCell("T22").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        //21. Đơn vị tính: U22, U23, U24
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("U22").RowIndex + i, 20, workSheet.GetCell("U22").RowIndex + i, 22);
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        mergedRegion.Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(hmd.DVT_ID);
                        //22. Đơn giá nguyên tệ: X22, X23, X24 hoac AA22
                        workSheet.Rows[workSheet.GetCell("AA22").RowIndex + i].Cells[workSheet.GetCell("AA22").ColumnIndex].Value = hmd.DonGiaKB.ToString("G" + soThapPhanDonGia);
                        workSheet.Rows[workSheet.GetCell("AA22").RowIndex + i].Cells[workSheet.GetCell("AA22").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        //23. Trị giá nguyên tệ: AB22, AN23, AB24 hoac AD22
                        workSheet.Rows[workSheet.GetCell("AD22").RowIndex + i].Cells[workSheet.GetCell("AD22").ColumnIndex].Value = hmd.TriGiaKB.ToString("N" + soThapPhanTriGiaNguyenTe);
                        workSheet.Rows[workSheet.GetCell("AD22").RowIndex + i].Cells[workSheet.GetCell("AD22").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                        tongTriGiaNT += Math.Round(Convert.ToDecimal(hmd.TriGiaKB), soThapPhanTriGiaNguyenTe, MidpointRounding.AwayFromZero);

                        #endregion

                        #region THÔNG TIN THUẾ

                        if (!mienThueXuatKhau)
                        {
                            //24. Thuế nhập khẩu
                            //Trị giá tính thuế: H28
                            workSheet.Rows[workSheet.GetCell("H28").RowIndex + i].Cells[workSheet.GetCell("H28").ColumnIndex].Value = hmd.TriGiaTT.ToString("N" + soThapPhanTriGiaTinhThue);
                            workSheet.Rows[workSheet.GetCell("H28").RowIndex + i].Cells[workSheet.GetCell("H28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //Thuế suất (%): N28
                            workSheet.Rows[workSheet.GetCell("N28").RowIndex + i].Cells[workSheet.GetCell("N28").ColumnIndex].Value = hmd.ThueSuatXNK.ToString("N" + soThapPhanThueSuat);
                            workSheet.Rows[workSheet.GetCell("N28").RowIndex + i].Cells[workSheet.GetCell("N28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //Tiền thuế: T28
                            workSheet.Rows[workSheet.GetCell("T28").RowIndex + i].Cells[workSheet.GetCell("T28").ColumnIndex].Value = hmd.ThueXNK.ToString("N" + soThapPhanTienThue);
                            workSheet.Rows[workSheet.GetCell("T28").RowIndex + i].Cells[workSheet.GetCell("T28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        }


                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            decimal TriGiaTTGTGT = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            if (!mienThueXuatKhau)
                            {
                                //25. Thu khác
                                //Tỷ lệ (%): X28
                                workSheet.Rows[workSheet.GetCell("X28").RowIndex + i].Cells[workSheet.GetCell("X28").ColumnIndex].Value = hmd.TyLeThuKhac.ToString("N" + soThapPhanTyLethuKhac);
                                workSheet.Rows[workSheet.GetCell("X28").RowIndex + i].Cells[workSheet.GetCell("X28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Số tiền: AD28
                                workSheet.Rows[workSheet.GetCell("AD28").RowIndex + i].Cells[workSheet.GetCell("AD28").ColumnIndex].Value = hmd.TriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                                workSheet.Rows[workSheet.GetCell("AD28").RowIndex + i].Cells[workSheet.GetCell("AD28").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            }

                            tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                        }

                        //Tiền thuế Xuất nhập khẩu
                        tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);

                        //Tổng tiền thuế và thu khác
                        tongTienThueTatCa += Convert.ToDecimal(hmd.ThueXNK) + Convert.ToDecimal(hmd.TriGiaThuKhac) + Convert.ToDecimal(hmd.ThueTTDB) + Convert.ToDecimal(hmd.ThueGTGT);

                        #endregion
                    }

                    #endregion
                }
                //CHI TIET DINH KEM
                else
                {
                    #region CHI TIET DINH KEM

                    //17. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                    workSheet.GetCell("B22").Value = "HÀNG HÓA XUẤT";
                    workSheet.GetCell("B23").Value = "(Chi tiết theo phụ lục đính kèm)";

                    foreach (Company.GC.BLL.KDT.HangMauDich hmd in TKMD.HMDCollection)
                    {
                        tongTriGiaNT += Math.Round(Convert.ToDecimal(hmd.TriGiaKB), soThapPhanTriGiaNguyenTe, MidpointRounding.AwayFromZero);
                        tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);
                        tongTriGiaTT += Convert.ToDecimal(hmd.TriGiaTT);
                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            tongTriGiaTTGTGT += Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            tongTriGiaTTGTGT += Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            tongTriGiaTTGTGT += Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                        }
                    }

                    #endregion
                }

                //Linhhtn - Không công phí VC và các phí khác vào Tổng trị giá nguyên tệ
                //tongTriGiaNT += Convert.ToDecimal(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);

                //23. Trị giá nguyên tệ: Cộng: AD25
                workSheet.GetCell("AD25").Value = tongTriGiaNT.ToString("N" + soThapPhanTriGiaNguyenTe);
                workSheet.GetCell("AD25").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                //24. Thuế nhập khẩu: Cộng: T31
                if (TKMD.HMDCollection.Count <= 3)
                {
                    workSheet.GetCell("T31").Value = tongTienThueXNK.ToString("N" + soThapPhanTienThue);
                    workSheet.GetCell("T31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                }

                //Thuế GTGT
                //if (tongThueGTGT > 0)
                //    lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
                //else
                //    lblTongTienThueGTGT.Text = "";

                //Thuế thu khác
                if (tongTriGiaThuKhac > 0 && TKMD.HMDCollection.Count <= 3)
                {
                    workSheet.GetCell("AD31").Value = tongTriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                    workSheet.GetCell("AD31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                }

                //26. Tổng số tiền thuế và thu khác (ô 24 + 25) bằng số: A32, Bằng chữ E33
                //mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("A32").RowIndex, 1, workSheet.GetCell("A32").RowIndex, 20);
                //mergedRegion.Value = tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);
                workSheet.GetCell("A32").Value += " " + tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);

                //Bằng chữ
                string s = Company.GC.BLL.Utils.VNCurrency.ToString(tongTienThueTatCa).Trim();
                s = s[0].ToString().ToUpper() + s.Substring(1);
                workSheet.GetCell("E33").Value = s.Replace("  ", " ");
                workSheet.GetCell("E33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Left;


                //Truoc 23. Phi bao hiem B25
                string st = "";
                if (TKMD.PhiBaoHiem > 0)
                    st = "I = " + TKMD.PhiBaoHiem.ToString("N2");
                if (TKMD.PhiVanChuyen > 0)
                    st += " F = " + TKMD.PhiVanChuyen.ToString("N2");
                if (TKMD.PhiKhac > 0)
                    st += " Phí khác = " + TKMD.PhiKhac.ToString("N2");
                //workSheet.GetCell("B25").Value = st;

                //27. Tổng trọng lượng F34, Số hiệu kiện, cont: F35, Tổng số container: W35
                if (TKMD.TrongLuong > 0)
                    workSheet.GetCell("F34").Value = TKMD.TrongLuong + " kg ";
                else
                    workSheet.GetCell("F34").Value = "";
                //Số hiệu kiện, cont: F35
                if (TKMD.SoContainer20 > 3 || TKMD.SoContainer40 > 3)
                    workSheet.GetCell("F35").Value = "danh sách container theo bảng kê đính kèm";
                else
                {
                    string soHieuKien = "";
                    Company.KDT.SHARE.QuanLyChungTu.Container objContainer = null;

                    if (TKMD.VanTaiDon != null)
                    {
                        for (int cnt = 0; cnt < TKMD.VanTaiDon.ContainerCollection.Count; cnt++)
                        {
                            objContainer = TKMD.VanTaiDon.ContainerCollection[cnt];

                            soHieuKien += objContainer.SoHieu + "/" + objContainer.Seal_No;

                            if (cnt < TKMD.VanTaiDon.ContainerCollection.Count - 1)
                            {
                                soHieuKien += "; ";
                            }
                        }
                    }
                    workSheet.GetCell("F35").Value = soHieuKien;
                }
                //Tổng số container: W35
                string tsContainer = "";
                string cont20 = "", cont40 = "", soKien = "";
                if (TKMD.SoContainer20 > 0)
                {
                    cont20 = "Cont20: " + Convert.ToInt32(TKMD.SoContainer20);

                    //tsContainer += cont20 + "; ";
                }
                else
                    cont20 = "";

                if (TKMD.SoContainer40 > 0)
                {
                    cont40 = "Cont40: " + Convert.ToInt32(TKMD.SoContainer40);

                    tsContainer += cont40 + "; ";
                }
                else
                    cont40 = "";

                if (TKMD.SoKien > 0)
                {
                    soKien = "Tổng số kiện: " + TKMD.SoKien;

                    tsContainer += soKien;
                }
                else
                    soKien = "";

                workSheet.GetCell("AB34").Value = cont20;
                workSheet.GetCell("AB34").CellFormat.Font.Height = 160; // Size: 160 / 20 = 8
                workSheet.GetCell("W35").Value = tsContainer;

                //28. Ghi chép khác: A37
                workSheet.GetCell("A37").Value = TKMD.DeXuatKhac;
                //29. Xác nhận giải phóng hàng/đưa hàng về bảo quản Y38
                //30. Ngày tháng năm: A43
                if (TKMD.NgayDangKy == new DateTime(1900, 1, 1))
                    workSheet.GetCell("A43").Value = "Ngày " + DateTime.Today.Day + " tháng " + DateTime.Today.Month + " năm " + DateTime.Today.Year;
                else
                    workSheet.GetCell("A43").Value = "Ngày " + TKMD.NgayTiepNhan.Day + " tháng " + TKMD.NgayTiepNhan.Month + " năm " + TKMD.NgayTiepNhan.Year;


                //Chọn đường dẫn file cần lưu
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                string dateNow = DateTime.Now.ToShortDateString().Replace("/", ""); // +"_" + DateTime.Now.Hour + "h" + DateTime.Now.Minute + "p" + DateTime.Now.Second + "s";
                saveFileDialog1.FileName = "TK" + TKMD.SoToKhai + "_" + TKMD.MaLoaiHinh + "_" + GlobalSettings.MA_HAI_QUAN + "_" + (TKMD.NamDK != 0 ? TKMD.NamDK : TKMD.NgayDangKy.Year) + "_" + dateNow;
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    destFile = saveFileDialog1.FileName;
                    //Ghi nội dung file gốc vào file cần lưu
                    try
                    {
                        byte[] sourceFile = Globals.ReadFile(sourcePath);
                        Globals.WriteFile(destFile, sourceFile);
                        workBook.Save(destFile);
                        ShowMessage("Lưu file thành công.\n" + destFile, false);
                        System.Diagnostics.Process.Start(destFile);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(ex.Message, false);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                //clsLocalLogger.getLogger().message(ex);
                ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.\nChi tiết lỗi:\n" + ex.Message, false);
                return;
            }
        }
        private static void ExportExcelPhuLucTKN(Company.GC.BLL.KDT.ToKhaiMauDich TKMD, bool banLuuHaiQuan, bool inMaHang, bool mienThueXuatKhau, bool mienThueGTGT, string _pls)
        {
            bool ok = true;
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongTriGiaTT = 0;
            decimal tongTriGiaTTGTGT = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;
            Infragistics.Excel.WorksheetMergedCellsRegion mergedRegion;
            string tenHang = "";
            /*Số thập phân*/
            int soThapPhanSoLuong = 0;
            int soThapPhanDonGia = 10;
            int soThapPhanTriGiaNguyenTe = 2;
            int soThapPhanTriGiaTinhThue = 0;
            int soThapPhanThueSuat = 0;
            int soThapPhanTienThue = 0;
            int soThapPhanTyLethuKhac = 0;
            int soThapPhanTienThuKhac = 0;
            int soThapPhanTongTienThue = 0;
            int soThapPhanTyGia = 2;
            int index = 0;
            int j = -1;

            if (ok)
            {
                //Lấy đường dẫn file gốc
                string destFile = "";
                string fileName = "PhuLuc_TKN_TQDT_2009_TT22.xls";

                string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

                //Kiem tra file Excel mau co ton tai?
                if (!System.IO.File.Exists(sourcePath))
                {
                    ShowMessage("Không tồn tại file mẫu Excel.", false);
                    return;
                }

                //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
                try
                {
                    Infragistics.Excel.Workbook workBook = Infragistics.Excel.Workbook.Load(sourcePath);
                    Infragistics.Excel.Worksheet workSheet = workBook.Worksheets[0];
                    DateTime minDate = new DateTime(1900, 1, 1);

                    //Chi cục Hải quan: F5
                    workSheet.GetCell("F5").Value = GlobalSettings.TEN_HAI_QUAN;
                    //Chi cục Hải quan cửa khẩu: H6
                    workSheet.GetCell("H6").Value = TKMD.CuaKhau_ID + "-" + Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(TKMD.CuaKhau_ID);
                    //Phụ lục số: S5
                    workSheet.GetCell("S5").Value = _pls;
                    //Số tờ khai: AC5
                    if (TKMD.SoToKhai > 0)
                        workSheet.GetCell("AC5").Value = TKMD.SoToKhai + "";
                    else
                        workSheet.GetCell("AC5").Value = "";
                    //Ngày, giờ đăng ký: U6
                    if (TKMD.NgayDangKy > minDate)
                        workSheet.GetCell("U6").Value = TKMD.NgayDangKy.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("U6").Value = "";
                    //Loại hình: AB6
                    string stlh = "";
                    stlh = Company.KDT.SHARE.Components.DuLieuChuan.LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                    workSheet.GetCell("AB6").Value = TKMD.MaLoaiHinh + " - " + stlh;

                    /*THÔNG TIN HÀNG*/
                    /*THÔNG TIN THUẾ*/
                    //27. Thuế nhập khẩu
                    //Trị giá tính thuế: F23 -> F32
                    //Thuế suất (%): H23 -> H32
                    //Tiền thuế: M23 -> M32
                    //Cộng: M33
                    //28. Thuế GTGT (hoặc TTĐB)
                    //Trị giá tính thuế: R23 -> R32
                    //Thuế suất (%): T23 -> T32
                    //Tiền thuế: Y23 -> Y32
                    //29. Thu khác
                    //Tỷ lệ (%): AB23 -> AB32
                    //Số tiền: AH23 -> AH32
                    //30. Tổng số tiền thuế và thu khác (ô 27+28 + 29) bằng số: O34, Bằng chữ D35

                    #region CHI TIẾT THÔNG TIN HÀNG
                    Company.GC.BLL.KDT.HangMauDich hmd = null;
                    List<Company.GC.BLL.KDT.HangMauDich> HMDReportCollection = new List<Company.GC.BLL.KDT.HangMauDich>();
                    int begin = (Convert.ToInt32(_pls) - 1) * 10;
                    int end = Convert.ToInt32(_pls) * 10;
                    if (end > TKMD.HMDCollection.Count) end = TKMD.HMDCollection.Count;
                    for (int i = begin; i < end; i++)
                    {
                        HMDReportCollection.Add(TKMD.HMDCollection[i]);
                        //for (int i = 0; i < TKMD.HMDCollection.Count; i++)
                        //{
                        hmd = TKMD.HMDCollection[i];
                        j++;
                        #region THÔNG TIN HÀNG
                        try
                        {
                            index = (j + 1) + (Convert.ToInt32(_pls) - 1) * 10;
                        }
                        catch
                        {
                            ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.", false);
                            return;
                        }
                        workSheet.GetCell("A" + (10 + j).ToString()).Value = index;
                        workSheet.GetCell("A" + (23 + j).ToString()).Value = index;
                        //20. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                        if (!inMaHang)
                        {
                            if (hmd.Ma_HTS.Trim().Length > 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                            else
                                tenHang = hmd.TenHang;
                        }
                        else
                        {
                            if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length > 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang + "/" + hmd.MaPhu;
                            else if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length <= 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                            else if (hmd.Ma_HTS.Trim().Length <= 0 && hmd.MaPhu.Trim().Length > 0)
                                tenHang = hmd.TenHang + "/" + hmd.MaPhu;
                            else
                                tenHang = hmd.TenHang;
                        }

                        //20. Tên hàng, quy cách phẩm chất: B10
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("B10").RowIndex + j, 1, workSheet.GetCell("B10").RowIndex + j, 8);
                        mergedRegion.Value = tenHang;
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;

                        //21. Mã số hàng hóa: J10 -> J19
                        // Create a merged region
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("J10").RowIndex + j, 9, workSheet.GetCell("J10").RowIndex + j, 12);
                        mergedRegion.Value = hmd.MaHS;
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        //22. Xuất xứ: N10 -> N19
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("N10").RowIndex + j, 13, workSheet.GetCell("N10").RowIndex + j, 15);
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        mergedRegion.Value = hmd.NuocXX_ID;
                        //23. Số lượng: T10 -> T19
                        workSheet.Rows[workSheet.GetCell("T10").RowIndex + j].Cells[workSheet.GetCell("T10").ColumnIndex].Value = hmd.SoLuong.ToString("N" + soThapPhanSoLuong);
                        workSheet.Rows[workSheet.GetCell("T10").RowIndex + j].Cells[workSheet.GetCell("T10").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        //24. Đơn vị tính: U10 -> U19
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("U10").RowIndex + j, 20, workSheet.GetCell("U10").RowIndex + j, 22);
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        mergedRegion.Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(hmd.DVT_ID);
                        //25. Đơn giá nguyên tệ: AA10 -> AA19
                        workSheet.Rows[workSheet.GetCell("AA10").RowIndex + j].Cells[workSheet.GetCell("AA10").ColumnIndex].Value = hmd.DonGiaKB.ToString("G" + soThapPhanDonGia);
                        workSheet.Rows[workSheet.GetCell("AA10").RowIndex + j].Cells[workSheet.GetCell("AA10").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        //26. Trị giá nguyên tệ: AE10 -> AE19
                        workSheet.Rows[workSheet.GetCell("AE10").RowIndex + j].Cells[workSheet.GetCell("AE10").ColumnIndex].Value = hmd.TriGiaKB.ToString("N" + soThapPhanTriGiaNguyenTe);
                        workSheet.Rows[workSheet.GetCell("AE10").RowIndex + j].Cells[workSheet.GetCell("AE10").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                        tongTriGiaNT += Math.Round(Convert.ToDecimal(hmd.TriGiaKB), soThapPhanTriGiaNguyenTe, MidpointRounding.AwayFromZero);

                        #endregion

                        #region THÔNG TIN THUẾ

                        if (!mienThueXuatKhau)
                        {
                            //27. Thuế nhập khẩu
                            //Trị giá tính thuế: F23
                            workSheet.Rows[workSheet.GetCell("F23").RowIndex + j].Cells[workSheet.GetCell("F23").ColumnIndex].Value = hmd.TriGiaTT.ToString("N" + soThapPhanTriGiaTinhThue);
                            workSheet.Rows[workSheet.GetCell("F23").RowIndex + j].Cells[workSheet.GetCell("F23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //Thuế suất (%): H23
                            workSheet.Rows[workSheet.GetCell("H23").RowIndex + j].Cells[workSheet.GetCell("H23").ColumnIndex].Value = hmd.ThueSuatXNK.ToString("N" + soThapPhanThueSuat);
                            workSheet.Rows[workSheet.GetCell("H23").RowIndex + j].Cells[workSheet.GetCell("H23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //Tiền thuế: M23
                            workSheet.Rows[workSheet.GetCell("M23").RowIndex + j].Cells[workSheet.GetCell("M23").ColumnIndex].Value = hmd.ThueXNK.ToString("N" + soThapPhanTienThue);
                            workSheet.Rows[workSheet.GetCell("M23").RowIndex + j].Cells[workSheet.GetCell("M23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        }


                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            decimal TriGiaTTGTGT = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            if (!mienThueXuatKhau)
                            {
                                //28. Thuế GTGT
                                //Trị giá tính thuế: R23
                                workSheet.Rows[workSheet.GetCell("R23").RowIndex + j].Cells[workSheet.GetCell("R23").ColumnIndex].Value = TriGiaTTGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                workSheet.Rows[workSheet.GetCell("R23").RowIndex + j].Cells[workSheet.GetCell("R23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Tỷ lệ (%): T23
                                workSheet.Rows[workSheet.GetCell("T23").RowIndex + j].Cells[workSheet.GetCell("T23").ColumnIndex].Value = hmd.ThueSuatGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                workSheet.Rows[workSheet.GetCell("T23").RowIndex + j].Cells[workSheet.GetCell("T23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Tiền thuế: Y23
                                workSheet.Rows[workSheet.GetCell("Y23").RowIndex + j].Cells[workSheet.GetCell("Y23").ColumnIndex].Value = hmd.ThueGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                workSheet.Rows[workSheet.GetCell("Y23").RowIndex + j].Cells[workSheet.GetCell("Y23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                                //29. Thu khác
                                //Tỷ lệ (%): AB23
                                workSheet.Rows[workSheet.GetCell("AB23").RowIndex + j].Cells[workSheet.GetCell("AB23").ColumnIndex].Value = hmd.TyLeThuKhac.ToString("N" + soThapPhanTyLethuKhac);
                                workSheet.Rows[workSheet.GetCell("AB23").RowIndex + j].Cells[workSheet.GetCell("AB23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Số tiền: AH23
                                workSheet.Rows[workSheet.GetCell("AH23").RowIndex + j].Cells[workSheet.GetCell("AH23").ColumnIndex].Value = hmd.TriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                                workSheet.Rows[workSheet.GetCell("AH23").RowIndex + j].Cells[workSheet.GetCell("AH23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            }

                            tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                        }

                        //Tiền thuế Xuất nhập khẩu
                        tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);

                        //Tổng tiền thuế và thu khác
                        tongTienThueTatCa += Convert.ToDecimal(hmd.ThueXNK) + Convert.ToDecimal(hmd.TriGiaThuKhac) + Convert.ToDecimal(hmd.ThueTTDB) + Convert.ToDecimal(hmd.ThueGTGT);

                        #endregion

                    }
                    #endregion
                    //}

                    //Linhhtn - Không công phí VC và các phí khác vào Tổng trị giá nguyên tệ
                    //tongTriGiaNT += Convert.ToDecimal(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);

                    //23. Trị giá nguyên tệ: Cộng: AE20
                    workSheet.GetCell("AE20").Value = tongTriGiaNT.ToString("N" + soThapPhanTriGiaNguyenTe);
                    workSheet.GetCell("AE20").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //24. Thuế nhập khẩu: Cộng: K31
                    //if (TKMD.HMDCollection.Count <= 3)
                    //{
                    //    workSheet.GetCell("K31").Value = tongTienThueXNK.ToString("N" + soThapPhanTienThue);
                    //    workSheet.GetCell("K31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}
                    workSheet.GetCell("M33").Value = tongTienThueXNK.ToString("N" + soThapPhanTienThue);
                    workSheet.GetCell("M33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                    //Thuế GTGT
                    //if (tongThueGTGT > 0)
                    //    lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
                    //else
                    //    lblTongTienThueGTGT.Text = "";
                    //if (TKMD.HMDCollection.Count <= 3)
                    //{
                    //    workSheet.GetCell("V31").Value = tongThueGTGT.ToString("N" + soThapPhanTienThue);
                    //    workSheet.GetCell("V31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}
                    workSheet.GetCell("Y33").Value = tongThueGTGT.ToString("N" + soThapPhanTienThue);
                    workSheet.GetCell("Y33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                    //Thuế thu khác
                    //if (tongTriGiaThuKhac > 0 && TKMD.HMDCollection.Count <= 3)
                    //{
                    workSheet.GetCell("AH33").Value = tongTriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                    workSheet.GetCell("AH33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}

                    //26. Tổng số tiền thuế và thu khác (ô 24 + 25) bằng số: O34, Bằng chữ D35
                    //mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("A32").RowIndex, 1, workSheet.GetCell("A32").RowIndex, 20);
                    //mergedRegion.Value = tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);
                    workSheet.GetCell("O34").Value += " " + tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);

                    //Bằng chữ
                    string s = Company.GC.BLL.Utils.VNCurrency.ToString(tongTienThueTatCa).Trim();
                    s = s[0].ToString().ToUpper() + s.Substring(1);
                    workSheet.GetCell("D35").Value = s.Replace("  ", " ");
                    workSheet.GetCell("D35").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Left;

                    //Chọn đường dẫn file cần lưu
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                    string dateNow = DateTime.Now.ToShortDateString().Replace("/", ""); // +"_" + DateTime.Now.Hour + "h" + DateTime.Now.Minute + "p" + DateTime.Now.Second + "s";
                    saveFileDialog1.FileName = "TK" + TKMD.SoToKhai + "_" + TKMD.MaLoaiHinh + "_" + GlobalSettings.MA_HAI_QUAN + "_" + TKMD.NamDK + "_" + dateNow + "PL_So_" + _pls;
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        destFile = saveFileDialog1.FileName;
                        //Ghi nội dung file gốc vào file cần lưu
                        try
                        {
                            byte[] sourceFile = Globals.ReadFile(sourcePath);
                            Globals.WriteFile(destFile, sourceFile);
                            workBook.Save(destFile);
                            ShowMessage("Save file thành công", false);
                            System.Diagnostics.Process.Start(destFile);
                        }
                        catch (Exception ex)
                        {
                            ShowMessage(ex.Message, false);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //clsLocalLogger.getLogger().message(ex);
                    ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.\nChi tiết lỗi:\n" + ex.Message, false);
                    return;
                }
            }
        }
        private static void ExportExcelPhuLucTKX(Company.GC.BLL.KDT.ToKhaiMauDich TKMD, bool banLuuHaiQuan, bool inMaHang, bool mienThueXuatKhau, bool mienThueGTGT, string _pls)
        {
            bool ok = true;
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongTriGiaTT = 0;
            decimal tongTriGiaTTGTGT = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;
            Infragistics.Excel.WorksheetMergedCellsRegion mergedRegion;
            string tenHang = "";
            /*Số thập phân*/
            int soThapPhanSoLuong = 0;
            int soThapPhanDonGia = 10;
            int soThapPhanTriGiaNguyenTe = 2;
            int soThapPhanTriGiaTinhThue = 0;
            int soThapPhanThueSuat = 0;
            int soThapPhanTienThue = 0;
            int soThapPhanTyLethuKhac = 0;
            int soThapPhanTienThuKhac = 0;
            int soThapPhanTongTienThue = 0;
            int soThapPhanTyGia = 2;
            int index = 0;
            int j = -1;

            if (ok)
            {
                //Lấy đường dẫn file gốc
                string destFile = "";
                string fileName = "PhuLuc_TKX_TQDT_2009_TT22.xls";

                string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

                //Kiem tra file Excel mau co ton tai?
                if (!System.IO.File.Exists(sourcePath))
                {
                    ShowMessage("Không tồn tại file mẫu Excel.", false);
                    return;
                }

                //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
                try
                {
                    Infragistics.Excel.Workbook workBook = Infragistics.Excel.Workbook.Load(sourcePath);
                    Infragistics.Excel.Worksheet workSheet = workBook.Worksheets[0];
                    DateTime minDate = new DateTime(1900, 1, 1);

                    //Chi cục Hải quan: F5
                    workSheet.GetCell("F5").Value = GlobalSettings.TEN_HAI_QUAN;
                    //Chi cục Hải quan cửa khẩu: H6
                    workSheet.GetCell("H6").Value = TKMD.CuaKhau_ID + "-" + Company.KDT.SHARE.Components.DuLieuChuan.CuaKhau.GetName(TKMD.CuaKhau_ID);
                    //Phụ lục số: S5
                    workSheet.GetCell("S5").Value = _pls;
                    //Số tờ khai: AC5
                    if (TKMD.SoToKhai > 0)
                        workSheet.GetCell("AC5").Value = TKMD.SoToKhai + "";
                    else
                        workSheet.GetCell("AC5").Value = "";
                    //Ngày, giờ đăng ký: U6
                    if (TKMD.NgayDangKy > minDate)
                        workSheet.GetCell("U6").Value = TKMD.NgayDangKy.ToString("dd/MM/yyyy");
                    else
                        workSheet.GetCell("U6").Value = "";
                    //Loại hình: AB6
                    string stlh = "";
                    stlh = Company.KDT.SHARE.Components.DuLieuChuan.LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                    workSheet.GetCell("AB6").Value = TKMD.MaLoaiHinh + " - " + stlh;

                    /*THÔNG TIN HÀNG*/
                    /*THÔNG TIN THUẾ*/
                    //27. Thuế nhập khẩu
                    //Trị giá tính thuế: F23 -> F32
                    //Thuế suất (%): H23 -> H32
                    //Tiền thuế: M23 -> M32
                    //Cộng: M33
                    //28. Thuế GTGT (hoặc TTĐB)
                    //Trị giá tính thuế: R23 -> R32
                    //Thuế suất (%): T23 -> T32
                    //Tiền thuế: Y23 -> Y32
                    //29. Thu khác
                    //Tỷ lệ (%): AB23 -> AB32
                    //Số tiền: AH23 -> AH32
                    //30. Tổng số tiền thuế và thu khác (ô 27+28 + 29) bằng số: O34, Bằng chữ D35

                    #region CHI TIẾT THÔNG TIN HÀNG
                    Company.GC.BLL.KDT.HangMauDich hmd = null;
                    List<Company.GC.BLL.KDT.HangMauDich> HMDReportCollection = new List<Company.GC.BLL.KDT.HangMauDich>();
                    int begin = (Convert.ToInt32(_pls) - 1) * 10;
                    int end = Convert.ToInt32(_pls) * 10;
                    if (end > TKMD.HMDCollection.Count) end = TKMD.HMDCollection.Count;
                    for (int i = begin; i < end; i++)
                    {
                        HMDReportCollection.Add(TKMD.HMDCollection[i]);
                        //for (int i = 0; i < TKMD.HMDCollection.Count; i++)
                        //{
                        hmd = TKMD.HMDCollection[i];
                        j++;
                        #region THÔNG TIN HÀNG
                        try
                        {
                            index = (j + 1) + (Convert.ToInt32(_pls) - 1) * 10;
                        }
                        catch
                        {
                            ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.", false);
                            return;
                        }
                        workSheet.GetCell("A" + (10 + j).ToString()).Value = index;
                        workSheet.GetCell("A" + (23 + j).ToString()).Value = index;
                        //20. Tên hàng, quy  cách phẩm chất: B22, B23, B24
                        if (!inMaHang)
                        {
                            if (hmd.Ma_HTS.Trim().Length > 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                            else
                                tenHang = hmd.TenHang;
                        }
                        else
                        {
                            if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length > 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang + "/" + hmd.MaPhu;
                            else if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length <= 0)
                                tenHang = hmd.Ma_HTS + ";" + hmd.TenHang;
                            else if (hmd.Ma_HTS.Trim().Length <= 0 && hmd.MaPhu.Trim().Length > 0)
                                tenHang = hmd.TenHang + "/" + hmd.MaPhu;
                            else
                                tenHang = hmd.TenHang;
                        }

                        //20. Tên hàng, quy cách phẩm chất: B10
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("B10").RowIndex + j, 1, workSheet.GetCell("B10").RowIndex + j, 8);
                        mergedRegion.Value = tenHang;
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;

                        //21. Mã số hàng hóa: J10 -> J19
                        // Create a merged region
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("J10").RowIndex + j, 9, workSheet.GetCell("J10").RowIndex + j, 12);
                        mergedRegion.Value = hmd.MaHS;
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        //22. Xuất xứ: N10 -> N19
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("N10").RowIndex + j, 13, workSheet.GetCell("N10").RowIndex + j, 15);
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        mergedRegion.Value = hmd.NuocXX_ID;
                        //23. Số lượng: T10 -> T19
                        workSheet.Rows[workSheet.GetCell("T10").RowIndex + j].Cells[workSheet.GetCell("T10").ColumnIndex].Value = hmd.SoLuong.ToString("N" + soThapPhanSoLuong);
                        workSheet.Rows[workSheet.GetCell("T10").RowIndex + j].Cells[workSheet.GetCell("T10").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        //24. Đơn vị tính: U10 -> U19
                        mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("U10").RowIndex + j, 20, workSheet.GetCell("U10").RowIndex + j, 22);
                        mergedRegion.CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Center;
                        mergedRegion.Value = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName(hmd.DVT_ID);
                        //25. Đơn giá nguyên tệ: AA10 -> AA19
                        workSheet.Rows[workSheet.GetCell("AA10").RowIndex + j].Cells[workSheet.GetCell("AA10").ColumnIndex].Value = hmd.DonGiaKB.ToString("G" + soThapPhanDonGia);
                        workSheet.Rows[workSheet.GetCell("AA10").RowIndex + j].Cells[workSheet.GetCell("AA10").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        //26. Trị giá nguyên tệ: AE10 -> AE19
                        workSheet.Rows[workSheet.GetCell("AE10").RowIndex + j].Cells[workSheet.GetCell("AE10").ColumnIndex].Value = hmd.TriGiaKB.ToString("N" + soThapPhanTriGiaNguyenTe);
                        workSheet.Rows[workSheet.GetCell("AE10").RowIndex + j].Cells[workSheet.GetCell("AE10").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                        tongTriGiaNT += Math.Round(Convert.ToDecimal(hmd.TriGiaKB), soThapPhanTriGiaNguyenTe, MidpointRounding.AwayFromZero);

                        #endregion

                        #region THÔNG TIN THUẾ

                        if (!mienThueXuatKhau)
                        {
                            //27. Thuế nhập khẩu
                            //Trị giá tính thuế: G23
                            workSheet.Rows[workSheet.GetCell("G23").RowIndex + j].Cells[workSheet.GetCell("G23").ColumnIndex].Value = hmd.TriGiaTT.ToString("N" + soThapPhanTriGiaTinhThue);
                            workSheet.Rows[workSheet.GetCell("G23").RowIndex + j].Cells[workSheet.GetCell("G23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //Thuế suất (%): L23
                            workSheet.Rows[workSheet.GetCell("L23").RowIndex + j].Cells[workSheet.GetCell("L23").ColumnIndex].Value = hmd.ThueSuatXNK.ToString("N" + soThapPhanThueSuat);
                            workSheet.Rows[workSheet.GetCell("L23").RowIndex + j].Cells[workSheet.GetCell("L23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            //Tiền thuế: R23
                            workSheet.Rows[workSheet.GetCell("R23").RowIndex + j].Cells[workSheet.GetCell("R23").ColumnIndex].Value = hmd.ThueXNK.ToString("N" + soThapPhanTienThue);
                            workSheet.Rows[workSheet.GetCell("R23").RowIndex + j].Cells[workSheet.GetCell("R23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                        }


                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            decimal TriGiaTTGTGT = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            if (!mienThueXuatKhau)
                            {
                                //28. Thuế GTGT
                                //Trị giá tính thuế: R23
                                //workSheet.Rows[workSheet.GetCell("R23").RowIndex + j].Cells[workSheet.GetCell("R23").ColumnIndex].Value = TriGiaTTGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                //workSheet.Rows[workSheet.GetCell("R23").RowIndex + j].Cells[workSheet.GetCell("R23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                ////Tỷ lệ (%): T23
                                //workSheet.Rows[workSheet.GetCell("T23").RowIndex + j].Cells[workSheet.GetCell("T23").ColumnIndex].Value = hmd.ThueSuatGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                //workSheet.Rows[workSheet.GetCell("T23").RowIndex + j].Cells[workSheet.GetCell("T23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                ////Tiền thuế: Y23
                                //workSheet.Rows[workSheet.GetCell("Y23").RowIndex + j].Cells[workSheet.GetCell("Y23").ColumnIndex].Value = hmd.ThueGTGT.ToString("N" + soThapPhanTyLethuKhac);
                                //workSheet.Rows[workSheet.GetCell("Y23").RowIndex + j].Cells[workSheet.GetCell("Y23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                                //29. Thu khác
                                //Tỷ lệ (%): AB23
                                workSheet.Rows[workSheet.GetCell("AB23").RowIndex + j].Cells[workSheet.GetCell("AB23").ColumnIndex].Value = hmd.TyLeThuKhac.ToString("N" + soThapPhanTyLethuKhac);
                                workSheet.Rows[workSheet.GetCell("AB23").RowIndex + j].Cells[workSheet.GetCell("AB23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                                //Số tiền: AH23
                                workSheet.Rows[workSheet.GetCell("AH23").RowIndex + j].Cells[workSheet.GetCell("AH23").ColumnIndex].Value = hmd.TriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                                workSheet.Rows[workSheet.GetCell("AH23").RowIndex + j].Cells[workSheet.GetCell("AH23").ColumnIndex].CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                            }

                            tongThueGTGT += Convert.ToDecimal(hmd.ThueGTGT);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.TriGiaThuKhac);
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            decimal TriGiaTTTTDB = Convert.ToDecimal(hmd.TriGiaTT) + Convert.ToDecimal(hmd.ThueXNK);
                            tongThueGTGT += Convert.ToDecimal(hmd.ThueTTDB);
                            tongTriGiaThuKhac += Convert.ToDecimal(hmd.ThueGTGT);
                        }

                        //Tiền thuế Xuất nhập khẩu
                        tongTienThueXNK += Convert.ToDecimal(hmd.ThueXNK);

                        //Tổng tiền thuế và thu khác
                        tongTienThueTatCa += Convert.ToDecimal(hmd.ThueXNK) + Convert.ToDecimal(hmd.TriGiaThuKhac) + Convert.ToDecimal(hmd.ThueTTDB) + Convert.ToDecimal(hmd.ThueGTGT);

                        #endregion

                    }
                    #endregion
                    //}

                    //Linhhtn - Không công phí VC và các phí khác vào Tổng trị giá nguyên tệ
                    //tongTriGiaNT += Convert.ToDecimal(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);

                    //23. Trị giá nguyên tệ: Cộng: AE20
                    workSheet.GetCell("AE20").Value = tongTriGiaNT.ToString("N" + soThapPhanTriGiaNguyenTe);
                    workSheet.GetCell("AE20").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //24. Thuế nhập khẩu: Cộng: R33
                    //if (TKMD.HMDCollection.Count <= 3)
                    //{
                    //    workSheet.GetCell("K31").Value = tongTienThueXNK.ToString("N" + soThapPhanTienThue);
                    //    workSheet.GetCell("K31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}
                    workSheet.GetCell("R33").Value = tongTienThueXNK.ToString("N" + soThapPhanTienThue);
                    workSheet.GetCell("R33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                    //Thuế GTGT
                    //if (tongThueGTGT > 0)
                    //    lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
                    //else
                    //    lblTongTienThueGTGT.Text = "";
                    //if (TKMD.HMDCollection.Count <= 3)
                    //{
                    //    workSheet.GetCell("V31").Value = tongThueGTGT.ToString("N" + soThapPhanTienThue);
                    //    workSheet.GetCell("V31").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}
                    //workSheet.GetCell("Y33").Value = tongThueGTGT.ToString("N" + soThapPhanTienThue);
                    //workSheet.GetCell("Y33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;

                    //Thuế thu khác
                    //if (tongTriGiaThuKhac > 0 && TKMD.HMDCollection.Count <= 3)
                    //{
                    workSheet.GetCell("AH33").Value = tongTriGiaThuKhac.ToString("N" + soThapPhanTienThuKhac);
                    workSheet.GetCell("AH33").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Right;
                    //}

                    //26. Tổng số tiền thuế và thu khác (ô 24 + 25) bằng số: N34, Bằng chữ D35
                    //mergedRegion = workSheet.MergedCellsRegions.Add(workSheet.GetCell("A32").RowIndex, 1, workSheet.GetCell("A32").RowIndex, 20);
                    //mergedRegion.Value = tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);
                    workSheet.GetCell("N34").Value += " " + tongTienThueTatCa.ToString("N" + soThapPhanTongTienThue);

                    //Bằng chữ
                    string s = Company.GC.BLL.Utils.VNCurrency.ToString(tongTienThueTatCa).Trim();
                    s = s[0].ToString().ToUpper() + s.Substring(1);
                    workSheet.GetCell("D35").Value = s.Replace("  ", " ");
                    workSheet.GetCell("D35").CellFormat.Alignment = Infragistics.Excel.HorizontalCellAlignment.Left;

                    //Chọn đường dẫn file cần lưu
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                    string dateNow = DateTime.Now.ToShortDateString().Replace("/", ""); // +"_" + DateTime.Now.Hour + "h" + DateTime.Now.Minute + "p" + DateTime.Now.Second + "s";
                    saveFileDialog1.FileName = "TK" + TKMD.SoToKhai + "_" + TKMD.MaLoaiHinh + "_" + GlobalSettings.MA_HAI_QUAN + "_" + TKMD.NamDK + "_" + dateNow + "PL_So_" + _pls;
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        destFile = saveFileDialog1.FileName;
                        //Ghi nội dung file gốc vào file cần lưu
                        try
                        {
                            byte[] sourceFile = Globals.ReadFile(sourcePath);
                            Globals.WriteFile(destFile, sourceFile);
                            workBook.Save(destFile);
                            ShowMessage("Save file thành công", false);
                            System.Diagnostics.Process.Start(destFile);
                        }
                        catch (Exception ex)
                        {
                            ShowMessage(ex.Message, false);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //clsLocalLogger.getLogger().message(ex);
                    ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.\nChi tiết lỗi:\n" + ex.Message, false);
                    return;
                }
            }
        }

        #region ẤN ĐỊNH THUẾ

        /// <summary>
        /// Lấy thông tin XML phản hồi từ Hải quan lưu trong kết quả xử lý và cập nhật lại thông tin Ấn định thuế.
        /// </summary>
        /// <param name="TKMD"></param>
        public static void AnDinhThue_InsertFromXML(Company.GC.BLL.KDT.ToKhaiMauDich TKMD)
        {
            try
            {
                //Thong Bao Thue By DATLMQ 24/01/2011
                bool IsThongBaoThue = false;
                string SoQD = "";
                DateTime NgayQD = new DateTime();
                DateTime NgayHetHan = new DateTime();
                string TaiKhoanKhoBac = "";
                string TenKhoBac = "";
                //Thue XNK
                string ChuongThueXNK = "";
                string LoaiThueXNK = "";
                string KhoanThueXNK = "";
                string MucThueXNK = "";
                string TieuMucThueXNK = "";
                double SoTienThueXNK = 0;
                //Thue VAT
                string ChuongThueVAT = "";
                string LoaiThueVAT = "";
                string KhoanThueVAT = "";
                string MucThueVAT = "";
                string TieuMucThueVAT = "";
                double SoTienThueVAT = 0;
                //Thue TTDB
                string ChuongThueTTDB = "";
                string LoaiThueTTDB = "";
                string KhoanThueTTDB = "";
                string MucThueTTDB = "";
                string TieuMucThueTTDB = "";
                double SoTienThueTTDB = 0;
                //Thue TVCBPG
                string ChuongThueTVCBPG = "";
                string LoaiThueTVCBPG = "";
                string KhoanThueTVCBPG = "";
                string MucThueTVCBPG = "";
                string TieuMucThueTVCBPG = "";
                double SoTienThueTVCBPG = 0;

                XmlDocument docNPL = new XmlDocument();

                //1. Lấy thông tin Ấn định thuế từ XML trả về từ Hải quan.
                List<Company.KDT.SHARE.Components.Message> listMsg = Company.KDT.SHARE.Components.Message.SelectCollectionBy_ItemID(TKMD.ID, string.Empty);

                //2. Lọc thông tin Ấn định thuế
                foreach (Company.KDT.SHARE.Components.Message msg in listMsg)
                {
                    if (msg.MessageFrom != null && msg.MessageFrom.Trim().ToLower() == GlobalSettings.MA_HAI_QUAN.Trim().ToLower())
                    {
                        docNPL.LoadXml(msg.MessageContent);

                        #region BEGIN AN DINH THUE

                        //Convert noi dung da ma hoa cua V3
                        if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                        {
                            FeedBackContent feedbackContent = Helpers.GetFeedBackContent(msg.MessageContent);
                            string noidung = feedbackContent.AdditionalInformations[0].Content.Text;

                            switch (feedbackContent.Function)
                            {
                                case DeclarationFunction.DUYET_LUONG_CHUNG_TU:

                                    string idTax = string.Empty;
                                    string tenKhobac = string.Empty;

                                    foreach (AdditionalInformation item in feedbackContent.AdditionalInformations)
                                    {
                                        if (item.Statement == AdditionalInformationStatement.TAI_KHOAN_KHO_BAC)
                                        {
                                            idTax = item.Content.Text;
                                        }
                                        else if (item.Statement == AdditionalInformationStatement.TEN_KHO_BAC)
                                        {
                                            tenKhobac = item.Content.Text;
                                        }
                                    }

                                    if (idTax != string.Empty && feedbackContent.AdditionalDocument != null)
                                    {
                                        noidung += "\r\nThông báo thuế của hệ thống Hải quan:\r\n";

                                        Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue andinhthue = new Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue();
                                        andinhthue.TaiKhoanKhoBac = idTax;
                                        andinhthue.TKMD_ID = TKMD.ID;
                                        andinhthue.TKMD_Ref = TKMD.GUIDSTR;
                                        andinhthue.NgayQuyetDinh = SingleMessage.GetDate(feedbackContent.AdditionalDocument.Issue);
                                        andinhthue.NgayHetHan = SingleMessage.GetDate(feedbackContent.AdditionalDocument.Expire);
                                        andinhthue.SoQuyetDinh = feedbackContent.AdditionalDocument.Reference.Trim();
                                        andinhthue.TenKhoBac = tenKhobac;
                                        noidung += "Số quyết định: " + andinhthue.SoQuyetDinh;
                                        noidung += "\r\nTài khoản kho bạc: " + idTax;
                                        noidung += "\r\nTên kho bạc: " + tenKhobac;
                                        noidung += "\r\nNgày quyết định: " + andinhthue.NgayQuyetDinh.ToString("dd-MM-yyyy HH:mm:ss");
                                        noidung += "\r\nNgày hết hạn: " + andinhthue.NgayHetHan.ToLongDateString();

                                        andinhthue.Insert();

                                        foreach (DutyTaxFee item in feedbackContent.AdditionalDocument.DutyTaxFees)
                                        {
                                            Company.KDT.SHARE.Components.AnDinhThue.ChiTiet chiTietThue = new Company.KDT.SHARE.Components.AnDinhThue.ChiTiet();
                                            chiTietThue.IDAnDinhThue = andinhthue.ID;

                                            string[] spl = item.AdValoremTaxBase.Split(new char[] { '.' });

                                            if (spl.Length == 2)
                                                if (System.Convert.ToDecimal(spl[1]) == 0)
                                                    chiTietThue.TienThue = System.Convert.ToDecimal(spl[0]);
                                                else
                                                    chiTietThue.TienThue = System.Convert.ToDecimal(item.AdValoremTaxBase);

                                            switch (item.Type.Trim())
                                            {
                                                case DutyTaxFeeType.THUE_XNK:
                                                    chiTietThue.SacThue = "XNK";
                                                    break;
                                                case DutyTaxFeeType.THUE_VAT:
                                                    chiTietThue.SacThue = "VAT";
                                                    break;
                                                case DutyTaxFeeType.THUE_TIEU_THU_DAT_BIET:
                                                    chiTietThue.SacThue = "TTDB";
                                                    break;
                                                case DutyTaxFeeType.THUE_KHAC:
                                                    chiTietThue.SacThue = "TVCBPG";
                                                    break;
                                                default:
                                                    break;
                                            }

                                            foreach (AdditionalInformation additionalInfo in item.AdditionalInformations)
                                            {
                                                if (!string.IsNullOrEmpty(additionalInfo.Content.Text))
                                                    switch (additionalInfo.Statement)
                                                    {
                                                        case "211"://Chuong
                                                            chiTietThue.Chuong = additionalInfo.Content.Text;
                                                            break;
                                                        case "212"://Loai
                                                            chiTietThue.Loai = Convert.ToInt32(additionalInfo.Content.Text);
                                                            break;
                                                        case "213"://Khoan
                                                            chiTietThue.Khoan = Convert.ToInt32(additionalInfo.Content.Text);
                                                            break;
                                                        case "214"://Muc
                                                            chiTietThue.Muc = Convert.ToInt32(additionalInfo.Content.Text);
                                                            break;
                                                        case "215"://Tieu muc
                                                            chiTietThue.TieuMuc = Convert.ToInt32(additionalInfo.Content.Text);
                                                            break;
                                                    }
                                            }
                                            chiTietThue.Insert();
                                        }

                                        Company.KDT.SHARE.Components.Globals.SaveMessage(msg.MessageContent, TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiAnDinhThue, noidung);

                                    }
                                    break;
                            }
                        }
                        else
                        {
                            //DATLMQ bổ sung In ra thông báo thuế 06/08/2011
                            XmlNode xmlNodeAnDinhThue = docNPL.SelectSingleNode("Envelope/Body/Content/Root/AN_DINH_THUE");
                            if (xmlNodeAnDinhThue != null)
                            {
                                IsThongBaoThue = true;
                                SoQD = xmlNodeAnDinhThue.Attributes["SOQD"].Value;
                                NgayQD = Convert.ToDateTime(xmlNodeAnDinhThue.Attributes["NGAYQD"].Value);
                                NgayHetHan = Convert.ToDateTime(xmlNodeAnDinhThue.Attributes["NGAYHH"].Value);
                                TaiKhoanKhoBac = xmlNodeAnDinhThue.Attributes["TKKB"].Value;
                                TenKhoBac = xmlNodeAnDinhThue.Attributes["KHOBAC"].Value;

                                //Kiem tra thong tin an dinh thue cua to khai
                                Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue anDinhThueObj = new Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue();
                                List<Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue> dsADT = (List<Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(TKMD.ID);
                                if (dsADT.Count > 0)
                                {
                                    anDinhThueObj.ID = dsADT[0].ID;
                                }

                                //Luu an dinh thue
                                anDinhThueObj.SoQuyetDinh = SoQD;
                                anDinhThueObj.NgayQuyetDinh = NgayQD;
                                anDinhThueObj.NgayHetHan = NgayHetHan;
                                anDinhThueObj.TaiKhoanKhoBac = TaiKhoanKhoBac;
                                anDinhThueObj.TenKhoBac = TenKhoBac;
                                anDinhThueObj.GhiChu = string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}\r\nHướng dẫn: {5}",
                                    TKMD.SoToKhai, TKMD.NgayDangKy.ToString(), TKMD.MaLoaiHinh.Trim(), TKMD.MaHaiQuan.Trim(), Company.KDT.SHARE.Components.Globals.GetPhanLuong(TKMD.PhanLuong), TKMD.HUONGDAN);
                                anDinhThueObj.TKMD_ID = TKMD.ID;
                                anDinhThueObj.TKMD_Ref = TKMD.GUIDSTR;
                                if (anDinhThueObj.ID == 0)
                                    anDinhThueObj.Insert();
                                else
                                    anDinhThueObj.InsertUpdate();

                                #region Begin Danh sach thue chi tiet
                                Company.KDT.SHARE.Components.AnDinhThue.ChiTiet thueObj = new Company.KDT.SHARE.Components.AnDinhThue.ChiTiet();

                                XmlNodeList xmlNodeThue = docNPL.SelectNodes("Envelope/Body/Content/Root/AN_DINH_THUE/THUE");
                                foreach (XmlNode node in xmlNodeThue)
                                {
                                    if (node.Attributes["SACTHUE"].Value.Equals("XNK"))
                                    {
                                        ChuongThueXNK = node.Attributes["CHUONG"].Value;
                                        SoTienThueXNK = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                                        LoaiThueXNK = node.Attributes["LOAI"].Value;
                                        KhoanThueXNK = node.Attributes["KHOAN"].Value;
                                        MucThueXNK = node.Attributes["MUC"].Value;
                                        TieuMucThueXNK = node.Attributes["TIEUMUC"].Value;

                                        //Tao doi tuong thue XNK
                                        thueObj = new Company.KDT.SHARE.Components.AnDinhThue.ChiTiet();
                                        thueObj.SacThue = "XNK";
                                        thueObj.Chuong = ChuongThueXNK;
                                        thueObj.TienThue = (decimal)SoTienThueXNK;
                                        thueObj.Loai = int.Parse(LoaiThueXNK);
                                        thueObj.Khoan = int.Parse(KhoanThueXNK);
                                        thueObj.Muc = int.Parse(MucThueXNK);
                                        thueObj.TieuMuc = int.Parse(TieuMucThueXNK);
                                        thueObj.IDAnDinhThue = anDinhThueObj.ID;
                                        thueObj.InsertUpdateBy(null);
                                    }
                                    if (node.Attributes["SACTHUE"].Value.Equals("VAT"))
                                    {
                                        ChuongThueVAT = node.Attributes["CHUONG"].Value;
                                        SoTienThueVAT = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                                        LoaiThueVAT = node.Attributes["LOAI"].Value;
                                        KhoanThueVAT = node.Attributes["KHOAN"].Value;
                                        MucThueVAT = node.Attributes["MUC"].Value;
                                        TieuMucThueVAT = node.Attributes["TIEUMUC"].Value;

                                        //Tao doi tuong thue VAT
                                        thueObj = new Company.KDT.SHARE.Components.AnDinhThue.ChiTiet();
                                        thueObj.SacThue = "VAT";
                                        thueObj.Chuong = ChuongThueVAT;
                                        thueObj.TienThue = (decimal)SoTienThueVAT;
                                        thueObj.Loai = int.Parse(LoaiThueVAT);
                                        thueObj.Khoan = int.Parse(KhoanThueVAT);
                                        thueObj.Muc = int.Parse(MucThueVAT);
                                        thueObj.TieuMuc = int.Parse(TieuMucThueVAT);
                                        thueObj.IDAnDinhThue = anDinhThueObj.ID;
                                        thueObj.InsertUpdateBy(null);
                                    }
                                    if (node.Attributes["SACTHUE"].Value.Equals("TTDB"))
                                    {
                                        ChuongThueTTDB = node.Attributes["CHUONG"].Value;
                                        SoTienThueTTDB = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                                        LoaiThueTTDB = node.Attributes["LOAI"].Value;
                                        KhoanThueTTDB = node.Attributes["KHOAN"].Value;
                                        MucThueTTDB = node.Attributes["MUC"].Value;
                                        TieuMucThueTTDB = node.Attributes["TIEUMUC"].Value;

                                        //Tao doi tuong thue Tieu thu dac biet
                                        thueObj = new Company.KDT.SHARE.Components.AnDinhThue.ChiTiet();
                                        thueObj.SacThue = "TTDB";
                                        thueObj.Chuong = ChuongThueTTDB;
                                        thueObj.TienThue = (decimal)SoTienThueTTDB;
                                        thueObj.Loai = int.Parse(LoaiThueTTDB);
                                        thueObj.Khoan = int.Parse(KhoanThueTTDB);
                                        thueObj.Muc = int.Parse(MucThueTTDB);
                                        thueObj.TieuMuc = int.Parse(TieuMucThueTTDB);
                                        thueObj.IDAnDinhThue = anDinhThueObj.ID;
                                        thueObj.InsertUpdateBy(null);
                                    }
                                    if (node.Attributes["SACTHUE"].Value.Equals("TVCBPG"))
                                    {
                                        ChuongThueTVCBPG = node.Attributes["CHUONG"].Value;
                                        SoTienThueTVCBPG = Convert.ToDouble(node.Attributes["TIENTHUE"].Value) / 10000;
                                        LoaiThueTVCBPG = node.Attributes["LOAI"].Value;
                                        KhoanThueTVCBPG = node.Attributes["KHOAN"].Value;
                                        MucThueTVCBPG = node.Attributes["MUC"].Value;
                                        TieuMucThueTVCBPG = node.Attributes["TIEUMUC"].Value;

                                        //Tao doi tuong thue Thu chenh lech gia
                                        thueObj = new Company.KDT.SHARE.Components.AnDinhThue.ChiTiet();
                                        thueObj.SacThue = "TVCBPG";
                                        thueObj.Chuong = ChuongThueTVCBPG;
                                        thueObj.TienThue = (decimal)SoTienThueTVCBPG;
                                        thueObj.Loai = int.Parse(LoaiThueTVCBPG);
                                        thueObj.Khoan = int.Parse(KhoanThueTVCBPG);
                                        thueObj.Muc = int.Parse(MucThueTVCBPG);
                                        thueObj.TieuMuc = int.Parse(TieuMucThueTVCBPG);
                                        thueObj.IDAnDinhThue = anDinhThueObj.ID;
                                        thueObj.InsertUpdateBy(null);
                                    }
                                }


                                #endregion End Danh sach thue chi tiet

                                KetQuaXuLy kqxlAnDinhThue = new KetQuaXuLy();
                                kqxlAnDinhThue.ItemID = TKMD.ID;
                                kqxlAnDinhThue.ReferenceID = new Guid(TKMD.GUIDSTR);
                                kqxlAnDinhThue.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                                kqxlAnDinhThue.LoaiThongDiep = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_AnDinhThueToKhai;
                                kqxlAnDinhThue.NoiDung = string.Format("Ấn định thuế:\r\nSố quyết định: {0}\r\nNgày quyết định: {1}\r\nNgày hết hạn: {2}\r\nTài khoản kho bạc: {3}\r\nKho bạc: {4}", SoQD, NgayQD.ToString(), NgayHetHan.ToString(), TaiKhoanKhoBac, TenKhoBac);
                                kqxlAnDinhThue.Ngay = DateTime.Now;
                                kqxlAnDinhThue.Insert();

                                Company.KDT.SHARE.Components.Globals.SaveMessage(msg.MessageContent, TKMD.ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiAnDinhThue, kqxlAnDinhThue.NoiDung);
                            }
                        }
                        #endregion END AN DINH THUE
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(string.Format("Ấn định thuế tờ khai số: {0}, ngày đăng ký: {1}, mã loại hình: {2}, ID: {3}", TKMD.SoToKhai, TKMD.NgayDangKy.ToString(), TKMD.MaLoaiHinh, TKMD.GUIDSTR), ex); }
        }

        #endregion

    }
}
