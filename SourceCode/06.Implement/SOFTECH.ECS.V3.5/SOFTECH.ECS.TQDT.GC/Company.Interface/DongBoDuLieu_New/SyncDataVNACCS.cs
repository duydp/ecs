﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3;
#if SXXK_V4
using Company.BLL.SXXK;
#endif
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.GC.BLL.GC;
using Company.GC.BLL.VNACC;
using Company.Interface;


namespace Company.BLL.VNACCS
{
    public class SyncVNACCSEventArgs : EventArgs
    {
       
        public Exception Error { get; private set; }
        public string Message { get; private set; }
        public int Percent { get; private set; }
        public object Data { get; private set; }
        public SyncVNACCSEventArgs(string message, int percent, Exception ex, object data)
        {
            Message = message;
            Error = ex;
            Percent = percent;
            Data = data;
        }
        public SyncVNACCSEventArgs(string message, int percent)
            : this(message, percent, null, null)
        {
        }
        public SyncVNACCSEventArgs(Exception ex)
            : this(string.Empty, 0, ex, null)
        {

        }
    }
    public class SyncDataVNACCS
    {
        private Company.Interface.wssyndata.Service myService = new Company.Interface.wssyndata.Service();
        public event EventHandler<SyncVNACCSEventArgs> SyncEventArgs;
        private void OnSync(SyncVNACCSEventArgs e)
        {
            if (SyncEventArgs != null)
            {
                SyncEventArgs(this, e);
            }
        }
        public void SendToKhaiCTQ(List<KDT_VNACC_ToKhaiMauDich> listTKMD, string user, string password)
        {
            int Total = listTKMD.Count;
            int index = 0;
            int sended = 0;
            try
            {
                foreach (KDT_VNACC_ToKhaiMauDich tkmd in listTKMD)
                {
                    index++;
                    tkmd.LoadFull();
                    DataVNACCSSync data = new DataVNACCSSync(tkmd.SoToKhai.ToString(), true, tkmd.NgayDangKy, tkmd.TrangThaiXuLy,tkmd.HopDong_ID);
                    string datastr = Helpers.Serializer(data);
                    string ketqua = string.Empty;

                    //Ham send service
                    ketqua = myService.SendVNACCS(user, password, datastr);

                    if (!string.IsNullOrEmpty(ketqua))
                    {
                        //Warring info
                        if (ketqua == "MK")
                        {
                            OnSync(new SyncVNACCSEventArgs(new Exception("Sai mật khẩu kết nối (PasswordERROR)")));
                            //GlobalSettings.USERNAME_DONGBO = string.Empty;
                            //GlobalSettings.PASSWOR_DONGBO = string.Empty;
                            break;
                        }
                        else OnSync(new SyncVNACCSEventArgs(new Exception(ketqua)));
                    }
                    else
                    {
                        Status sttk = new Status();
                        sttk.GUIDSTR = tkmd.SoToKhai.ToString();
                        sttk.MSG_STATUS = 1;
                        sttk.CREATE_TIME = DateTime.Now;
                        sttk.InsertUpdate();
                        string msg = string.Format("Đã gửi thông tin {0}/{1}/{2}", tkmd.SoToKhai, tkmd.MaLoaiHinh, tkmd.NgayDangKy.Year);
                        int percent = (int)((index * 100) / Total);
                        sended++;
                        OnSync(new SyncVNACCSEventArgs(msg, percent));
                    }
                }
                OnSync(new SyncVNACCSEventArgs(string.Format("Đã gửi được {0}/{1} tờ khai", sended, Total), (int)((index * 100) / Total)));
            }
            catch (System.Exception ex)
            {
                OnSync(new SyncVNACCSEventArgs(ex));

            }
        }
        public void SendToKhai(List<KDT_VNACC_ToKhaiMauDich> listTKMD, string user, string password)
        {
            int Total = listTKMD.Count;
            int index = 0;
            int sended = 0;
            try
            {
                foreach (KDT_VNACC_ToKhaiMauDich tkmd in listTKMD)
                {
                    index++;
                    tkmd.LoadFull();
                    DataVNACCSSync data = new DataVNACCSSync(tkmd.SoToKhai.ToString(),true,tkmd.NgayDangKy,tkmd.HopDong_ID);
                    string ketqua = string.Empty;
                    string datastr = Helpers.Serializer(data);
                    //Ham send service
                    ketqua = myService.SendVNACCS(user, password, datastr);

                    if (!string.IsNullOrEmpty(ketqua))
                    {
                        //Warring info
                        if (ketqua == "MK")
                        {
                            OnSync(new SyncVNACCSEventArgs(new Exception("Sai mật khẩu kết nối (PasswordERROR)")));
                            //GlobalSettings.USERNAME_DONGBO = string.Empty;
                            //GlobalSettings.PASSWOR_DONGBO = string.Empty;
                            break;
                        }
                        else OnSync(new SyncVNACCSEventArgs(new Exception(ketqua)));
                    }
                    else
                    {
                        Status sttk = new Status();
                        sttk.GUIDSTR = tkmd.SoToKhai.ToString();
                        sttk.MSG_STATUS = 1;
                        sttk.CREATE_TIME = DateTime.Now;
                        sttk.InsertUpdate();
                        string msg = string.Format("Đã gửi thông tin {0}/{1}/{2}", tkmd.SoToKhai, tkmd.MaLoaiHinh, tkmd.NgayDangKy.Year);
                        int percent = (int)((index * 100) / Total);
                        sended++;
                        OnSync(new SyncVNACCSEventArgs(msg, percent));
                    }
                }
                OnSync(new SyncVNACCSEventArgs(string.Format("Đã gửi được {0}/{1} tờ khai", sended, Total), (int)((index * 100) / Total)));
            }
            catch (System.Exception ex)
            {
                OnSync(new SyncVNACCSEventArgs(ex));

            }
        }
        public void ReceiveToKhaiCTQ(List<DongBoDuLieu_TrackVNACCS> trackVnaccs, string user, string password)
        {
            int Total = trackVnaccs.Count;
            int index = 0;
            int sended = 0;
            try
            {

                foreach (DongBoDuLieu_TrackVNACCS item in trackVnaccs)
                {

                    index++;
                    DataVNACCSSync data = null;
                    string trangThaiXuLy = "2";
                    // Ham get DataVNACCS tu service
                    string datastr = myService.ReceiveVNACCSCTQ(item.SoToKhai, item.NgayDangKy, user, password, trangThaiXuLy);
                    data = Helpers.Deserialize<DataVNACCSSync>(datastr);
                    //Company.BLL.VNACCS.DataVNACCSSync datatest = data;
                    if (data.exception != null)
                    {
                        OnSync(new SyncVNACCSEventArgs(new Exception(data.exception)));
                    }
                    else
                    {
                        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                        using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                        {
                            connection.Open();
                            SqlTransaction transaction = connection.BeginTransaction();
                            try
                            {
                                if (data.NplCollection != null && data.NplCollection.Count > 0)
                                    new NguyenPhuLieu().InsertUpdate(data.NplCollection);
                                if (data.SPCollection != null && data.SPCollection.Count > 0)
                                    new SanPham().InsertUpdate(data.SPCollection);
                                if (data.DMCollection != null && data.DMCollection.Count > 0)
                                    new DinhMuc().InsertUpdate(data.DMCollection);
                                if (data.msgPB != null && data.msgLog != null)
                                {
                                    long id = data.msgLog.Insert(transaction);
                                    data.msgPB.Master_ID = id;
                                    data.msgPB.InsertUpdate(transaction);
                                    KDT_VNACC_ToKhaiMauDich tkmd = new KDT_VNACC_ToKhaiMauDich();
                                    tkmd = ProcessMessages.XuLyMsg(new ReturnMessages(data.msgLog.Log_Messages), true);
                                    //new Company.KDT.SHARE.VNACCS.LogMessages.MsgLog().ProcessMSG(data.msgLog.Log_Messages, true, transaction);
                                    tkmd.CoQuanHaiQuan = GlobalSettings.MA_HAI_QUAN_VNACCS;
                                    tkmd.InputMessageID = data.msgPB.MessagesInputID;
                                    tkmd.InsertUpdateFull(transaction);
                                    //tkmd.InsertUpdateFullSynDataVNACCS(transaction);
                                }
                                transaction.Commit();

                                string msg = string.Format("Đã nhận tờ khai : {0}", item);
                                int percent = (int)((index * 100) / Total);
                                sended++;
                                OnSync(new SyncVNACCSEventArgs(msg, percent));
                            }
                            catch (System.Exception ex)
                            {
                                transaction.Rollback();
                                throw ex;
                            }
                        }


                    }
                    OnSync(new SyncVNACCSEventArgs(string.Format("Đã nhận được {0}/{1} tờ khai", sended, Total), (int)((index * 100) / Total)));
                }
            }
            catch (System.Exception ex)
            {
                OnSync(new SyncVNACCSEventArgs(ex));
            }


        }
        public void ReceiveToKhai(List<DongBoDuLieu_TrackVNACCS> ListSoToKhai, string user, string password)
        {
            int Total = ListSoToKhai.Count;
            int index = 0;
            int sended = 0;
            try
            {

                foreach (DongBoDuLieu_TrackVNACCS item in ListSoToKhai)
                {

                    index++;
                    DataVNACCSSync data = null;

                    // Ham get DataVNACCS tu service
                    string datastr = myService.ReceiveVNACCS(item.SoToKhai, item.NgayDangKy, user, password);
                    data = Helpers.Deserialize<DataVNACCSSync>(datastr);
                    //Company.BLL.VNACCS.DataVNACCSSync datatest = data;

                    if (data.exception != null)
                    {
                        OnSync(new SyncVNACCSEventArgs(new Exception( data.exception)));
                    }
                    else
                    {
                        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                        using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                        {
                            connection.Open();
                            SqlTransaction transaction = connection.BeginTransaction();
                            try
                            {
                                if (data.NplCollection != null && data.NplCollection.Count > 0)
                                    new NguyenPhuLieu().InsertUpdate(data.NplCollection);
                                if (data.SPCollection != null && data.SPCollection.Count > 0)
                                    new SanPham().InsertUpdate(data.SPCollection);
                                if (data.DMCollection != null && data.DMCollection.Count > 0)
                                    new DinhMuc().InsertUpdate(data.DMCollection);
                                if (data.msgPB != null && data.msgLog != null)
                                {
                                    long id = data.msgLog.Insert(transaction);
                                    data.msgPB.Master_ID = id;
                                    data.msgPB.Insert(transaction);
                                    KDT_VNACC_ToKhaiMauDich tkmd = ProcessMessages.XuLyMsg(new ReturnMessages(data.msgLog.Log_Messages));
                                    tkmd.InsertUpdateFull(transaction);
                                }
                                transaction.Commit();

                                string msg = string.Format("Đã nhận tờ khai : {0}", item);
                                int percent = (int)((index * 100) / Total);
                                sended++;
                                OnSync(new SyncVNACCSEventArgs(msg, percent));
                            }
                            catch (System.Exception ex)
                            {
                                transaction.Rollback();
                                throw ex;
                            }
                        }


                    }
                    OnSync(new SyncVNACCSEventArgs(string.Format("Đã nhận được {0}/{1} tờ khai", sended, Total), (int)((index * 100) / Total)));
                }
            }
            catch (System.Exception ex)
            {
                OnSync(new SyncVNACCSEventArgs(ex));
            }


        }
        

    }
}
