﻿using System;
using System.Drawing;
using Company.BLL.DuLieuChuan;
using Company.BLL.KDT;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.BLL;
using Company.BLL.Utils;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class ManageGiayPhepForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------

        public bool isBrower = false;
        public GiayPhep giayPhep;
        public long TKMD_ID;
        public ManageGiayPhepForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindData()
        {
            if (!isBrower)
                dgList.DataSource = GiayPhep.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'", "");
            else
                dgList.DataSource = GiayPhep.SelectListGiayPhepByMaDanhNghiepAndKhacTKMD(TKMD_ID, GlobalSettings.MA_DON_VI);
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            BindData();
        }

  
      

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<GiayPhep> listGiayPhep = new List<GiayPhep>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        GiayPhep gp = (GiayPhep)i.GetRow().DataRow;
                        gp.Delete();
                    }
                }
            }
           

            BindData();
        }

      
      

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (isBrower)
            {
                giayPhep = (GiayPhep)e.Row.DataRow;
                this.Close();
            }
        }

        private void filterEditor1_Click(object sender, EventArgs e)
        {

        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            BindData();
        }

     


    }
}
