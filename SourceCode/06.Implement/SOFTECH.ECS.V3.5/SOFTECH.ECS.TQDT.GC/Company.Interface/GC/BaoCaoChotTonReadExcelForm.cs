﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Infragistics.Excel;
using Company.KDT.SHARE.VNACCS.Vouchers;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.KDT.SXXK
{
    public partial class BaoCaoChotTonReadExcelForm : Company.Interface.BaseForm
    {
        public Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport TotalInventoryReport = new Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport();
        public KDT_VNACCS_TotalInventoryReport_ContractReference ContractReference = new KDT_VNACCS_TotalInventoryReport_ContractReference();
        public List<Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_Detail> DetailCollection = new List<Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_Detail>();
        public string LoaiHangHoa = "";
        public BaoCaoChotTonReadExcelForm()
        {
            InitializeComponent();
        }

        private void BaoCaoChotTonReadExcelForm_Load(object sender, EventArgs e)
        {

        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    MLMessages("Lỗi khi chọn file. Doanh nghiệp hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                    return null;
                }
                List<String> Collection = new List<string>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                MLMessages("Lỗi khi đọc sheet. Doanh nghiệp hãy kiểm tra lại tên sheet trước khi chọn file.", "MSG_EXC03", "", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog sf = new OpenFileDialog();
                sf.ShowDialog(this);
                txtFilePath.Text = sf.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int beginRow = Convert.ToInt32(txtRow.Value) -1;
            if (beginRow < 0)
            {
                return;
            }

            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);

            }
            catch (Exception ex)
            {
                MLMessages("Lỗi khi đọc file. Doanh nghiệp hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                MLMessages("Không tồn tại sheet \"" + cbbSheetName.Text + "\"MSG_EXC01", "", cbbSheetName.Text, false);
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;

            char MaHHColumn = Convert.ToChar(txtMaHangColumn.Text.Trim());
            int MaHHCol = ConvertCharToInt(MaHHColumn);

            char TenHHColumn = Convert.ToChar(txtTenHangColumn.Text.Trim());
            int TenHHCol = ConvertCharToInt(TenHHColumn);

            char DVTColumn = Convert.ToChar(txtDVTColumn.Text.Trim());
            int DVTCol = ConvertCharToInt(DVTColumn);

            char TonKhoSSColumn = Convert.ToChar(txtTKSSColumn.Text.Trim());
            int TonKhoSSCol = ConvertCharToInt(TonKhoSSColumn);

            char TonKhoTTColumn = Convert.ToChar(txtTKTTColumn.Text.Trim());
            int TonKhoTTCol = ConvertCharToInt(TonKhoTTColumn);

            char SoHopDongColumn = Convert.ToChar(txtSoHopDong.Text.Trim());
            int SoHopDongCol = ConvertCharToInt(SoHopDongColumn);

            string errorTotal = "";
            string errorMaHangHoa = "";
            string errorMaHangHoaExits = "";
            string errorTenHangHoa = "";
            string errorDVT = "";
            string errorDVTExits = "";
            string errorDVTCheck = "";
            string errorSoLuongTonKhoSoSach = "";
            string errorSoLuongTonKhoSoSachValid = "";
            string errorSoLuongTonKhoThucTe = "";
            string errorSoLuongTonKhoThucTeValid = "";
            string errorSoHopDong = "";
            string errorSoHopDongExits = "";
            List<VNACC_Category_QuantityUnit> DVTCollection = VNACC_Category_QuantityUnit.SelectCollectionAll();
            List<GC_NguyenPhuLieu> NPLCollection = new List<GC_NguyenPhuLieu>();
            List<GC_SanPham> SPCollection = new List<GC_SanPham>();
            if (!chkMutilHD.Checked)
            {
                Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
                long ID_HopDong = HopDong.GetID(ContractReference.SoHopDong);
                HD.LoadHD(ID_HopDong);
                NPLCollection = GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(HD.ID);
                SPCollection = GC_SanPham.SelectCollectionBy_HopDong_ID(HD.ID);
                foreach (Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_Detail item in ContractReference.DetailCollection)
                {
                    if (item.ID > 0)
                        item.Delete();
                }
                ContractReference.DetailCollection.Clear();
            }
            else
            {
                foreach (Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_ContractReference item in TotalInventoryReport.ContractCollection)
                {
                    item.DetailCollection = Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_Detail.SelectCollectionBy_ContractReference_ID(item.ID);
                    foreach (Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_Detail ite in item.DetailCollection)
                    {
                        if (item.ID > 0)
                            item.Delete();
                    }
                    item.DetailCollection.Clear();
                }
            }
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        bool isAdd = true;
                        bool isExits = false;
                        if (chkMutilHD.Checked)
                        {
                            string SoHopDong = "";
                            foreach (KDT_VNACCS_TotalInventoryReport_ContractReference items in TotalInventoryReport.ContractCollection)
                            {
                                try
                                {
                                    SoHopDong = (wsr.Cells[SoHopDongCol].Value).ToString().Trim();
                                }
                                catch (Exception)
                                {
                                    errorSoHopDong += "[" + (wsr.Index + 1) + "]-[" + SoHopDong + "]\n";
                                    isAdd = false; 
                                }
                                if (SoHopDong.ToString().Length == 0)
                                {
                                    errorSoHopDong += "[" + (wsr.Index + 1) + "]-[" + SoHopDong + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (items.SoHopDong == SoHopDong)
                                    {
                                        Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_Detail totalInventoryReportDetail = new Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_Detail();
                                        totalInventoryReportDetail.ContractReference_ID = items.ID;
                                        long ID_HopDong = HopDong.GetID(items.SoHopDong);
                                        NPLCollection = new List<GC_NguyenPhuLieu>();
                                        NPLCollection = GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(ID_HopDong);
                                        SPCollection = new List<GC_SanPham>();
                                        SPCollection = GC_SanPham.SelectCollectionBy_HopDong_ID(ID_HopDong);

                                        try
                                        {
                                            totalInventoryReportDetail.MaHangHoa = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                                            if (totalInventoryReportDetail.MaHangHoa.Trim().Length == 0)
                                            {
                                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.MaHangHoa + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                // KIỂM TRA MÃ NPL HOẶC SP ĐÃ ĐĂNG KÝ
                                                if (LoaiHangHoa == "NPL")
                                                {
                                                    foreach (GC_NguyenPhuLieu item in NPLCollection)
                                                    {
                                                        if (item.Ma == totalInventoryReportDetail.MaHangHoa)
                                                        {
                                                            isExits = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    foreach (GC_SanPham item in SPCollection)
                                                    {
                                                        if (item.Ma == totalInventoryReportDetail.MaHangHoa)
                                                        {
                                                            isExits = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            if (!isExits)
                                            {
                                                errorMaHangHoaExits += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.MaHangHoa + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.MaHangHoa + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            totalInventoryReportDetail.TenHangHoa = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                                            if (totalInventoryReportDetail.TenHangHoa.Trim().Length == 0)
                                            {
                                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.TenHangHoa + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.TenHangHoa + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            isExits = false;
                                            totalInventoryReportDetail.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim();
                                            if (totalInventoryReportDetail.DVT.Trim().Length == 0)
                                            {
                                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.DVT + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                                {
                                                    if (item.Code == totalInventoryReportDetail.DVT)
                                                    {
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                                if (isExits)
                                                {
                                                    isExits = false;
                                                    //KIỂM TRA ĐVT VỚI ĐVT ĐÃ ĐĂNG KÝ CỦA NPL HOẶC SẢN PHẨM
                                                    if (LoaiHangHoa == "NPL")
                                                    {
                                                        foreach (GC_NguyenPhuLieu item in NPLCollection)
                                                        {
                                                            if (item.Ma == totalInventoryReportDetail.MaHangHoa)
                                                            {
                                                                if (VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) == totalInventoryReportDetail.DVT)
                                                                {
                                                                    isExits = true;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        foreach (GC_SanPham item in SPCollection)
                                                        {
                                                            if (item.Ma == totalInventoryReportDetail.MaHangHoa)
                                                            {
                                                                if (VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) == totalInventoryReportDetail.DVT)
                                                                {
                                                                    isExits = true;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (!isExits)
                                                    {
                                                        errorDVTCheck += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.MaHangHoa + "]-[" + totalInventoryReportDetail.DVT + "]\n";
                                                        isAdd = false;
                                                    }
                                                }
                                                else
                                                {
                                                    errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.DVT + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                            if (!isExits)
                                            {
                                                errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.DVT + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.DVT + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            isExits = false;
                                            totalInventoryReportDetail.SoLuongTonKhoSoSach = Convert.ToDecimal(wsr.Cells[TonKhoSSCol].Value);
                                            if (totalInventoryReportDetail.SoLuongTonKhoSoSach.ToString().Trim().Length == 0)
                                            {
                                                errorSoLuongTonKhoSoSach += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoSoSach + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                if (Decimal.Round(totalInventoryReportDetail.SoLuongTonKhoSoSach, 4) != totalInventoryReportDetail.SoLuongTonKhoSoSach)
                                                {
                                                    errorSoLuongTonKhoSoSachValid += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoSoSach + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorSoLuongTonKhoSoSach += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoSoSach + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            totalInventoryReportDetail.SoLuongTonKhoThucTe = Convert.ToDecimal(wsr.Cells[TonKhoTTCol].Value);
                                            if (totalInventoryReportDetail.SoLuongTonKhoThucTe.ToString().Trim().Length == 0)
                                            {
                                                errorSoLuongTonKhoThucTe += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoThucTe + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                if (Decimal.Round(totalInventoryReportDetail.SoLuongTonKhoThucTe, 4) != totalInventoryReportDetail.SoLuongTonKhoThucTe)
                                                {
                                                    errorSoLuongTonKhoThucTeValid += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoThucTe + "]\n";
                                                    isAdd = false;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorSoLuongTonKhoThucTe += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoThucTe + "]\n";
                                            isAdd = false;
                                        }
                                        if (isAdd)
                                            items.DetailCollection.Add(totalInventoryReportDetail);
                                    }
                                }
                            }
                        }
                        else
                        {
                            Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_Detail totalInventoryReportDetail = new Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_Detail();
                            totalInventoryReportDetail.MaHangHoa = Convert.ToString(wsr.Cells[MaHHCol].Value).Trim();
                            try
                            {
                                if (totalInventoryReportDetail.MaHangHoa.Trim().Length == 0)
                                {
                                    errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.MaHangHoa + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    // KIỂM TRA MÃ NPL HOẶC SP ĐÃ ĐĂNG KÝ
                                    if (LoaiHangHoa == "NPL")
                                    {
                                        foreach (GC_NguyenPhuLieu item in NPLCollection)
                                        {
                                            if (item.Ma == totalInventoryReportDetail.MaHangHoa)
                                            {
                                                isExits = true;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (GC_SanPham item in SPCollection)
                                        {
                                            if (item.Ma == totalInventoryReportDetail.MaHangHoa)
                                            {
                                                isExits = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.MaHangHoa + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                totalInventoryReportDetail.TenHangHoa = Convert.ToString(wsr.Cells[TenHHCol].Value).Trim();
                                if (totalInventoryReportDetail.TenHangHoa.Trim().Length == 0)
                                {
                                    errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.TenHangHoa + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.TenHangHoa + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                isExits = false;
                                totalInventoryReportDetail.DVT = Convert.ToString(wsr.Cells[DVTCol].Value).Trim();
                                if (totalInventoryReportDetail.DVT.Trim().Length == 0)
                                {
                                    errorDVT += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.DVT + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                    {
                                        if (item.Code == totalInventoryReportDetail.DVT)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                    if (isExits)
                                    {
                                        isExits = false;
                                        //KIỂM TRA ĐVT VỚI ĐVT ĐÃ ĐĂNG KÝ CỦA NPL HOẶC SẢN PHẨM
                                        if (LoaiHangHoa == "NPL")
                                        {
                                            foreach (GC_NguyenPhuLieu item in NPLCollection)
                                            {
                                                if (item.Ma == totalInventoryReportDetail.MaHangHoa)
                                                {
                                                    if (VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) == totalInventoryReportDetail.DVT)
                                                    {
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            foreach (GC_SanPham item in SPCollection)
                                            {
                                                if (item.Ma == totalInventoryReportDetail.MaHangHoa)
                                                {
                                                    if (VNACCS_Mapper.GetCodeVNACC(item.DVT_ID) == totalInventoryReportDetail.DVT)
                                                    {
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if (!isExits)
                                        {
                                            errorDVTCheck += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.MaHangHoa + "]-[" + totalInventoryReportDetail.DVT + "]\n";
                                            isAdd = false;
                                        }
                                    }
                                    else
                                    {
                                        errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.DVT + "]\n";
                                        isAdd = false;
                                    }
                                }
                                if (!isExits)
                                {
                                    errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.DVT + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.DVT + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                totalInventoryReportDetail.SoLuongTonKhoSoSach = Convert.ToDecimal(wsr.Cells[TonKhoSSCol].Value);
                                if (totalInventoryReportDetail.SoLuongTonKhoSoSach.ToString().Trim().Length == 0)
                                {
                                    errorSoLuongTonKhoSoSach += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoSoSach + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(totalInventoryReportDetail.SoLuongTonKhoSoSach, 4) != totalInventoryReportDetail.SoLuongTonKhoSoSach)
                                    {
                                        errorSoLuongTonKhoSoSachValid += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoSoSach + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorSoLuongTonKhoSoSach += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoSoSach + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                totalInventoryReportDetail.SoLuongTonKhoThucTe = Convert.ToDecimal(wsr.Cells[TonKhoTTCol].Value);
                                if (totalInventoryReportDetail.SoLuongTonKhoThucTe.ToString().Trim().Length == 0)
                                {
                                    errorSoLuongTonKhoThucTe += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoThucTe + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(totalInventoryReportDetail.SoLuongTonKhoThucTe, 4) != totalInventoryReportDetail.SoLuongTonKhoThucTe)
                                    {
                                        errorSoLuongTonKhoThucTeValid += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoThucTe + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorSoLuongTonKhoThucTe += "[" + (wsr.Index + 1) + "]-[" + totalInventoryReportDetail.SoLuongTonKhoThucTe + "]\n";
                                isAdd = false;
                            }
                            if (isAdd)
                                DetailCollection.Add(totalInventoryReportDetail);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        return;
                    }
                }
            }
            if (!String.IsNullOrEmpty(errorMaHangHoa))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorMaHangHoaExits))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoaExits + " CHƯA ĐƯỢC ĐĂNG KÝ \n";
            if (!String.IsNullOrEmpty(errorTenHangHoa))
                errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : \n" + errorTenHangHoa + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDVT))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVT + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDVTExits))
                errorTotal += "\n - [STT] -[ĐVT] : \n" + errorDVTExits + " KHÔNG HỢP LỆ. ĐVT PHẢI LÀ ĐVT VNACCS (VÍ DỤ : CÁI/CHIẾC LÀ : PCE)\n";
            if (!String.IsNullOrEmpty(errorDVTCheck))
                errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] - [ĐVT] : \n" + errorDVTCheck + " KHÁC VỚI ĐVT ĐÃ ĐĂNG KÝ CỦA NPL HOẶC SẢN PHẨM \n";
            if (!String.IsNullOrEmpty(errorSoLuongTonKhoSoSach))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG TỒN KHO SỔ SÁCH] : \n" + errorSoLuongTonKhoSoSach + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorSoLuongTonKhoSoSachValid))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG TỒN KHO SỔ SÁCH] : \n" + errorSoLuongTonKhoSoSachValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";
            if (!String.IsNullOrEmpty(errorSoLuongTonKhoThucTe))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG TỒN KHO THỰC TẾ] : " + errorSoLuongTonKhoThucTe + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorSoLuongTonKhoThucTeValid))
                errorTotal += "\n - [STT] -[SỐ LƯỢNG TỒN KHO THỰC TẾ] : \n" + errorSoLuongTonKhoThucTeValid + " CHỈ ĐƯỢC NHẬP TỐI ĐA 4 SỐ THẬP PHÂN (VÍ DỤ : ****,1234)\n";
            if (!String.IsNullOrEmpty(errorSoHopDong))
                errorTotal += "\n - [STT] -[SỐ HỢP ĐỒNG] : \n" + errorSoHopDong + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorSoHopDongExits))
                errorTotal += "\n - [STT] -[SỐ HỢP ĐỒNG] : \n" + errorSoHopDongExits + " CHƯA ĐƯỢC ĐĂNG KÝ\n";

            if (String.IsNullOrEmpty(errorTotal))
            {
                if (!chkMutilHD.Checked)
                {
                    foreach (Company.KDT.SHARE.VNACCS.Vouchers.KDT_VNACCS_TotalInventoryReport_Detail totalInventoryReport_Detail in DetailCollection)
                    {
                        ContractReference.DetailCollection.Add(totalInventoryReport_Detail);
                    }
                }
                ShowMessageTQDT("NHẬP HÀNG TỪ EXCEL THÀNH CÔNG .", false);
            }
            else
            {

                if (chkMutilHD.Checked)
                {
                    ShowMessageTQDT("LỖI NHẬP TỪ EXCEL ." + errorTotal, false);
                    return;
                }
                else
                {
                    ShowMessageTQDT("NHẬP TỪ EXCEL KHÔNG THÀNH CÔNG ." + errorTotal, false);
                    return;
                }
            }

            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkExcelMau_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_TotalInventoryReport();
            }

            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
