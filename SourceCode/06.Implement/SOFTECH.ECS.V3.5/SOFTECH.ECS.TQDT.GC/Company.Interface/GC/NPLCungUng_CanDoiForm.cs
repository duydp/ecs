﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.Interface.KDT.GC;

namespace Company.Interface.GC
{
    public partial class NPLCungUng_CanDoiForm : BaseForm
    {
        public NPLCungUng_CanDoiForm()
        {
            InitializeComponent();
            this.Load += new EventHandler(NPLCungUng_CanDoiForm_Load);
        }

        void NPLCungUng_CanDoiForm_Load(object sender, EventArgs e)
        {
            System.Threading.ThreadPool.QueueUserWorkItem(LoadData);
        }
        DataTable dtSource;
        public long idHopDong;
        public void LoadData(object obj)
        {
            StartProgress();

            dtSource = Company.GC.BLL.KDT.GC.HopDong.CanDoiNPLTuCungUng(idHopDong);

            Bindinglist();
            StopProgress();

        }
        public void StartProgress()
        {
            if (this.InvokeRequired)
                this.Invoke(new MethodInvoker(delegate { toolStripProgressBar1.Style = ProgressBarStyle.Marquee; toolStripStatusLabel1.Text = "Đang load dữ liệu" ;}));
            else
            { toolStripProgressBar1.Style = ProgressBarStyle.Marquee; toolStripStatusLabel1.Text = "Đang load dữ liệu"; }

        }

        public void StopProgress()
        {
            if (this.InvokeRequired)
                this.Invoke(new MethodInvoker(delegate { toolStripProgressBar1.Style = ProgressBarStyle.Continuous; toolStripStatusLabel1.Text = "Hoàn thành"; }));
            else
            { toolStripProgressBar1.Style = ProgressBarStyle.Continuous; toolStripStatusLabel1.Text = "Hoàn thành"; }
        }

        public void Bindinglist()
        {
            if (this.InvokeRequired)
                this.Invoke(new MethodInvoker(delegate { dgList.DataSource = dtSource; dgList.Refetch(); }));
            else
            { 
                dgList.DataSource = dtSource; 
                dgList.Refetch(); 
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.FileName = "BangKeTuCungUng_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
            sfNPL.Filter = "Excel files| *.xls";
             if (sfNPL.ShowDialog(this) == DialogResult.OK && sfNPL.FileName != "")
             {
                 Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                 gridEXExporter1.GridEX = dgList;
                 try
                 {
                     System.IO.Stream str = sfNPL.OpenFile();
                     gridEXExporter1.Export(str);
                     str.Close();
                     if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                     {
                         System.Diagnostics.Process.Start(sfNPL.FileName);
                     }
                 }
                 catch (Exception ex)
                 {
                     ShowMessage("Lỗi mở file " + ex.Message, false);

                 }
             }
        }

        private void btnChuyenKB_Click(object sender, EventArgs e)
        {
            CungUngForm f = new CungUngForm();
            f.isTranfer = true;
            f.HD.ID = idHopDong; 
            f.tbNPLCU = dtSource;
            f.tbNPLDetail = dtSource;
            f.ShowDialog(this);
            //ShowMessage("Chức năng đang được hoàn thiện...", false);
        }

        private void contextMenuStrip1_Click(object sender, EventArgs e)
        {

        }
    }
}
